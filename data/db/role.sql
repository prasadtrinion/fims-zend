INSERT INTO dbo.role
(
  role,
  create_dt_tm,
  update_dt_tm,
  delete_flag
)
VALUES
(
  N'SYSTEM_ADMIN',
  '2017-02-24 13:38:02.423',
  '2017-02-24 13:38:02.423',
  0
);

INSERT INTO dbo.role
(
  role,
  create_dt_tm,
  update_dt_tm,
  delete_flag
)
VALUES
(
  N'CSR',
  '2017-02-24 13:41:46.723',
  '2017-02-24 13:41:46.723',
  0
);

INSERT INTO dbo.role
(
  role,
  create_dt_tm,
  update_dt_tm,
  delete_flag
)
VALUES
(
  N'DSR',
  '2017-02-24 13:41:46.723',
  '2017-02-24 13:41:46.723',
  0
);

COMMIT;
