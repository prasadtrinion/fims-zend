GO
/****** Object:  Table [dbo].[account]    Script Date: 3/16/2017 11:53:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](45) NOT NULL,
	[password] [nvarchar](150) NOT NULL,
	[status] [int] NOT NULL,
	[sso_token] [nvarchar](45) NULL,
	[reset_token] [nvarchar](150) NULL,
	[verification_token] [nvarchar](50) NULL,
	[email_status] [int] NOT NULL,
	[last_logged_in_dt_tm] [datetime2](6) NOT NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[delete_flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[allowance]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allowance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[program_name] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NOT NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[update_dt_tm] [datetime2](6) NOT NULL,
	[create_user_id] [int] NOT NULL,
	[update_user_id] [int] NOT NULL,
	[delete_flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[allowance_claim]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allowance_claim](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[allowance_id] [int] NULL,
	[user_id] [int] NULL,
	[cust_name] [nvarchar](100) NOT NULL,
	[title] [nvarchar](45) NOT NULL,
	[claim_date] [date] NOT NULL,
	[phone_number] [nvarchar](45) NOT NULL,
	[address_line1] [nvarchar](100) NOT NULL,
	[address_line2] [nvarchar](100) NULL,
	[city] [nvarchar](100) NOT NULL,
	[state_code] [nvarchar](10) NOT NULL,
	[country_code] [nvarchar](10) NOT NULL,
	[zip_code] [nvarchar](25) NOT NULL,
	[total_amount] [numeric](10, 2) NOT NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[update_dt_tm] [datetime2](6) NOT NULL,
	[delete_flag] [bit] NOT NULL,
	[received_date] [date] NULL,
	[check_date] [date] NULL,
	[check_amount] [numeric](10, 2) NULL,
	[check_number] [nvarchar](45) NULL,
	[status] [int] NOT NULL,
	[verified_date] [date] NULL,
	[reason_code] [int] NULL,
	[create_account_id] [int] NOT NULL,
	[update_account_id] [int] NOT NULL,
	[submit_date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[allowance_claim_history]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[allowance_claim_history](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [int] NULL,
	[status] [int] NOT NULL,
	[create_dt_tm] [datetime2](6) NULL,
	[create_account_id] [int] NOT NULL,
	[message] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[allowance_claim_notes]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allowance_claim_notes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[claim_id] [int] NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[note] [nvarchar](255) NOT NULL,
	[delete_flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[allowance_claim_product]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allowance_claim_product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_id] [int] NULL,
	[claim_id] [int] NULL,
	[num_installed] [int] NOT NULL,
	[total_amount] [numeric](10, 2) NOT NULL,
	[delete_flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[allowance_product]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[allowance_product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[allowance_id] [int] NULL,
	[product_id] [int] NULL,
	[normal_amount] [numeric](10, 2) NOT NULL,
	[premiere_amount] [numeric](10, 2) NOT NULL,
	[distributor_amount] [numeric](10, 2) NOT NULL,
	[self_amount] [numeric](10, 2) NOT NULL,
	[diversey_amount] [numeric](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[diversey_master]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[diversey_master](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[customer_name] [nvarchar](255) NULL,
	[business_name] [nvarchar](255) NULL,
	[diversey_id] [nvarchar](45) NOT NULL,
	[business_id] [nvarchar](45) NULL,
	[address] [nvarchar](255) NULL,
	[city] [nvarchar](45) NULL,
	[state] [nvarchar](45) NULL,
	[zip] [nvarchar](45) NULL,
	[r12_sales] [nvarchar](45) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[product]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_number] [nvarchar](45) NOT NULL,
	[product_image] [nvarchar](100) NOT NULL,
	[product_type] [int] NOT NULL,
	[description] [varchar](max) NOT NULL,
	[create_dt_tm] [datetime2](6) NULL,
	[update_dt_tm] [datetime2](6) NULL,
	[create_user_id] [int] NULL,
	[update_user_id] [int] NULL,
	[delete_flag] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[reason_code]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reason_code](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](100) NULL,
	[reason_code] [nvarchar](100) NULL,
	[delete_flag] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[role]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[role] [nvarchar](50) NOT NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[update_dt_tm] [datetime2](6) NOT NULL,
	[delete_flag] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_has_role]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_has_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[role_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_profile]    Script Date: 3/16/2017 11:53:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_profile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[email] [nvarchar](45) NULL,
	[first_name] [nvarchar](45) NOT NULL,
	[last_name] [nvarchar](45) NOT NULL,
	[phone_number] [nvarchar](45) NOT NULL,
	[contact_type] [int] NOT NULL,
	[address_line1] [nvarchar](100) NOT NULL,
	[address_line2] [nvarchar](100) NULL,
	[city] [nvarchar](100) NOT NULL,
	[state_code] [nvarchar](10) NOT NULL,
	[country_code] [nvarchar](10) NOT NULL,
	[zip_code] [nvarchar](25) NOT NULL,
	[mailing_list] [bit] NULL,
	[diversey_id] [int] NULL,
	[dsr_type] [int] NULL,
	[company_name] [nvarchar](255) NOT NULL,
	[sales_executive] [nvarchar](255) NOT NULL,
	[dist_phone_number] [nvarchar](45) NOT NULL,
	[dist_address_line1] [nvarchar](100) NOT NULL,
	[dist_address_line2] [nvarchar](100) NULL,
	[dist_city] [nvarchar](100) NOT NULL,
	[dist_state_code] [nvarchar](10) NOT NULL,
	[dist_country_code] [nvarchar](10) NOT NULL,
	[dist_zip_code] [nvarchar](25) NOT NULL,
	[create_dt_tm] [datetime2](6) NOT NULL,
	[update_dt_tm] [datetime2](6) NOT NULL,
	[delete_flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[allowance_claim]  WITH CHECK ADD  CONSTRAINT [FK_2255363E282DEF00] FOREIGN KEY([allowance_id])
REFERENCES [dbo].[allowance] ([id])
GO
ALTER TABLE [dbo].[allowance_claim] CHECK CONSTRAINT [FK_2255363E282DEF00]
GO
ALTER TABLE [dbo].[allowance_claim]  WITH CHECK ADD  CONSTRAINT [FK_2255363EA76ED395] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_profile] ([id])
GO
ALTER TABLE [dbo].[allowance_claim] CHECK CONSTRAINT [FK_2255363EA76ED395]
GO
ALTER TABLE [dbo].[allowance_claim_history]  WITH CHECK ADD  CONSTRAINT [FK_EF44A5317096A49F] FOREIGN KEY([claim_id])
REFERENCES [dbo].[allowance_claim] ([id])
GO
ALTER TABLE [dbo].[allowance_claim_history] CHECK CONSTRAINT [FK_EF44A5317096A49F]
GO
ALTER TABLE [dbo].[allowance_claim_notes]  WITH CHECK ADD  CONSTRAINT [FK_5C280CF57096A49F] FOREIGN KEY([claim_id])
REFERENCES [dbo].[allowance_claim] ([id])
GO
ALTER TABLE [dbo].[allowance_claim_notes] CHECK CONSTRAINT [FK_5C280CF57096A49F]
GO
ALTER TABLE [dbo].[allowance_claim_product]  WITH CHECK ADD  CONSTRAINT [FK_1BB4D1D74584665A] FOREIGN KEY([product_id])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[allowance_claim_product] CHECK CONSTRAINT [FK_1BB4D1D74584665A]
GO
ALTER TABLE [dbo].[allowance_claim_product]  WITH CHECK ADD  CONSTRAINT [FK_1BB4D1D77096A49F] FOREIGN KEY([claim_id])
REFERENCES [dbo].[allowance_claim] ([id])
GO
ALTER TABLE [dbo].[allowance_claim_product] CHECK CONSTRAINT [FK_1BB4D1D77096A49F]
GO
ALTER TABLE [dbo].[allowance_product]  WITH CHECK ADD  CONSTRAINT [FK_E69B42C3282DEF00] FOREIGN KEY([allowance_id])
REFERENCES [dbo].[allowance] ([id])
GO
ALTER TABLE [dbo].[allowance_product] CHECK CONSTRAINT [FK_E69B42C3282DEF00]
GO
ALTER TABLE [dbo].[allowance_product]  WITH CHECK ADD  CONSTRAINT [FK_E69B42C34584665A] FOREIGN KEY([product_id])
REFERENCES [dbo].[product] ([id])
GO
ALTER TABLE [dbo].[allowance_product] CHECK CONSTRAINT [FK_E69B42C34584665A]
GO
ALTER TABLE [dbo].[user_profile]  WITH CHECK ADD  CONSTRAINT [FK_D95AB4059B6B5FBA] FOREIGN KEY([account_id])
REFERENCES [dbo].[account] ([id])
GO
ALTER TABLE [dbo].[user_profile] CHECK CONSTRAINT [FK_D95AB4059B6B5FBA]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' 0(unverified) 1(verified)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'account', @level2type=N'COLUMN',@level2name=N'email_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' 1(email) 2(phone)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_profile', @level2type=N'COLUMN',@level2name=N'contact_type'
GO
