INSERT INTO dbo.reason_code
(
  TYPE,
  reason_code,
  delete_flag
)
VALUES
(
  N'DECLINED',
  N'Duplicate Submission',
  0
);

INSERT INTO dbo.reason_code
(
  TYPE,
  reason_code,
  delete_flag
)
VALUES
(
  N'DECLINED',
  N'Incorrect Number of Products Installed',
  0
);

INSERT INTO dbo.reason_code
(
  TYPE,
  reason_code,
  delete_flag
)
VALUES
(
  N'DECLINED',
  N'Invalid Customer',
  0
);

INSERT INTO dbo.reason_code
(
  TYPE,
  reason_code,
  delete_flag
)
VALUES
(
  N'REJECT',
  N'Additional Information Required',
  0
);

INSERT INTO dbo.reason_code
(
  TYPE,
  reason_code,
  delete_flag
)
VALUES
(
  N'DECLINED',
  N'Resubmission period expired',
  0
);

COMMIT;
