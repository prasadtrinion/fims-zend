CREATE DATABASE  IF NOT EXISTS `zf3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `zf3`;
-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: zf3
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `sso_token` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_token` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_status` int(11) NOT NULL COMMENT ' 0(unverified) 1(verified)',
  `last_logged_in_dt_tm` datetime NOT NULL,
  `create_dt_tm` datetime NOT NULL,
  `delete_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'test@demo.com','$2y$10$i7InSpgxfPKlOTR4nyQ1aOIRRWaqXqUs4ri3fNfEbQuIlRPk26KJC',1,' ',' ',NULL,1,'2018-04-05 07:11:35','2018-04-05 07:11:35',0),(2,'test2@demo.com','$2y$10$/hhsuCk3h21THyCvVxiZQ.Tfl37I/zsTiYySeCF6Vfg5s6v8Rm.qy',1,' ',' ','720ca117de144c81583cdd4d6eda8654',0,'2018-04-05 07:41:56','2018-04-05 07:41:56',0),(3,'test3@demo.com','$2y$10$MmuRvmSbHehA0asq2Ijnnu67fO7iwUS9syVxHDXTR4Va481cPtjim',1,' ',' ','752436e21846cfeeb2098d9a3e4e58ca',0,'2018-04-05 07:51:37','2018-04-05 07:51:37',0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diversey_master`
--

DROP TABLE IF EXISTS `diversey_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diversey_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diversey_id` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `business_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `r12_sales` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `diversey_id_UNIQUE` (`diversey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diversey_master`
--

LOCK TABLES `diversey_master` WRITE;
/*!40000 ALTER TABLE `diversey_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `diversey_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `create_dt_tm` datetime NOT NULL,
  `update_dt_tm` datetime NOT NULL,
  `delete_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'SYSTEM_ADMIN','2017-02-24 13:38:02','2017-02-24 13:38:02',0),(2,'CSR','2017-02-24 13:41:46','2017-02-24 13:41:46',0),(3,'DSR','2017-02-24 13:41:46','2017-02-24 13:41:46',0);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_role`
--

DROP TABLE IF EXISTS `user_has_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_has_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_role`
--

LOCK TABLES `user_has_role` WRITE;
/*!40000 ALTER TABLE `user_has_role` DISABLE KEYS */;
INSERT INTO `user_has_role` VALUES (1,1,1),(2,2,2),(3,2,3);
/*!40000 ALTER TABLE `user_has_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `contact_type` int(11) NOT NULL COMMENT ' 1(email) 2(phone)',
  `address_line1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_line2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `state_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `mailing_list` tinyint(1) DEFAULT NULL,
  `diversey_id` int(11) DEFAULT NULL,
  `dsr_type` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sales_executive` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dist_phone_number` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `dist_address_line1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dist_address_line2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dist_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dist_state_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dist_country_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dist_zip_code` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `create_dt_tm` datetime NOT NULL,
  `update_dt_tm` datetime NOT NULL,
  `delete_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_1_idx` (`account_id`),
  CONSTRAINT `FK_D95AB4059B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_profile`
--

LOCK TABLES `user_profile` WRITE;
/*!40000 ALTER TABLE `user_profile` DISABLE KEYS */;
INSERT INTO `user_profile` VALUES (1,1,'test@demo.com','Application','User','262-631-4001',1,'8310 16th Street','P.O. Box 902','Sturtevant','WI','USA','53177-0902',0,NULL,NULL,'deMO','ABC','262-631-4001','8310 16th Street','P.O. Box 902','Sturtevant','WI','USA','53177-0902','2018-04-05 07:11:35','2018-04-05 07:20:48',0),(2,2,'test2@demo.com','test','demo','262-631-4001',1,'8310 16th Street','P.O. Box 902','Sturtevant','NY','USA','53177-0902',0,NULL,NULL,'deMO','ABC','262-631-4001','8310 16th Street',NULL,'Sturtevant','IA','USA','53177-0902','2018-04-05 07:41:56','2018-04-05 07:41:56',0),(3,3,'test3@demo.com','demo','test','262-631-4001',1,'8310 16th Street','P.O. Box 902','Sturtevant','AK','USA','53177-0902',0,0,NULL,'Morgan','awer','262-631-4001','8310 16th Street','','Sturtevant','AK','USA','53177-0902','2018-04-05 07:51:37','2018-04-05 08:06:21',0);
/*!40000 ALTER TABLE `user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacancy`
--

DROP TABLE IF EXISTS `vacancy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vacancy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `experience` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `create_dt_tm` datetime NOT NULL,
  `delete_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacancy`
--

LOCK TABLES `vacancy` WRITE;
/*!40000 ALTER TABLE `vacancy` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacancy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-05  9:21:11
