<?php
/**
 * Configuration parameters
 */
return [
    
    // PHP ini configuration
    'php_ini_config' => [
        'error_reporting' => E_ALL,
        'display_errors' => false
    ],
    
    // Database configuration
    'database' => [
        
        // For Windows
        // 'driverClass' => 'Doctrine\DBAL\Driver\PDOSqlsrv\Driver',
        
        // For Linux
        // 'driverClass' => 'Lsw\DoctrinePdoDblib\Doctrine\DBAL\Driver\PDODblib\Driver',
        'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
        
        'default' => [
            'host' => 'localhost',
            'port' => 3306,
            'dbname' => 'fims',
            'user' => 'root',
            'password' => 'root'
        ],
        'db2' => [
            'host' => 'localhost',
            'port' => 3306,
            'dbname' => 'fims',
            'user' => 'root',
            'password' => 'root'
        ]
    ],
    
    // Caching
    'config_cache_enabled' => false,
    'module_map_cache_enabled' => false,
    
    // Administrator
    'admin_name' => 'Admin',
    'admin_email' => 'admin@example.com',

    // View template configuration
    'template_config' => [
        'default_app_title' => 'Demo application',
        'js_url_args' => 'v=' . time()
    ],

    // Mailer configuration
    'mailer_smtp_host' => 'smtp.gmail.com',
    'mailer_smtp_port' => '587',
    'mailer_smtp_user' => '',
    'mailer_smtp_password' => '',
    'mailer_smtp_ssl' => 'tls',
    
    // Logger configuration
    'logger_enabled' => true,
    'logger_handler_type' => 'rotating_file',
    'logger_handler_level' => 'debug',
    'logger_handler_filename' => __DIR__ . '/../data/logs/app.log',
    'logger_handler_max_files' => 15,
    
    // Session
    'session_remember_me_ttl' => 86400, // 24 hours
                                        
    // re-captcha
    'recaptha_site_key' => '',
    'recaptha_secret_key' => '',

    //client key
    'client_key' => 'qazwsx123',

    //JWT configuration 
    'jwtKey'              => 'DEMO@TEST123',
   'serverName'          => 'demo.local',
    'jwtEncryptAlgorithm' => 'HS256',
    'jwtUserExpDays'          =>1,  
     //cron script
    'default_reason_code_id'=> 7,
    'no_days_to_decline'=> 15,
    'no_days_to_approve'=> 1,
    
];

