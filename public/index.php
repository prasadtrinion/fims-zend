<?php
use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
// $method = $_SERVER['REQUEST_METHOD'];
// $name = gmdate("YmdHis") . ".txt";
// // echo $_SERVER['REMOTE_ADDR']; 
// if($_SERVER['REMOTE_ADDR'] == "49.206.12.177"){
// if($method !='OPTIONS'){
// /****************************Api Write Code */

// $rawBody = file_get_contents("php://input");
// $postData = json_decode($rawBody,true);
// $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// $link =  $actual_link;
//     try {
//     $connPdo = new PDO("dblib:host=localhost;dbname=uum;", "sa", "sybase");
//     $connPdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
//     } catch (PDOException $e) {
//     echo $e->getMessage() . '</br>';
//     }
    
//     $query = "insert into all_apis (url,data,method) values ('$link','$rawBody','$method')";
    // $connPdo->query($query);
    
// $end_line = "********************************************************************************************";

// try{
// file_put_contents('files/apis.txt', $method.':: '.$link.PHP_EOL , FILE_APPEND | LOCK_EX);
// file_put_contents('files/apis.txt', print_r($rawBody,true).PHP_EOL , FILE_APPEND | LOCK_EX);
// file_put_contents('files/apis.txt', $end_line.PHP_EOL , FILE_APPEND | LOCK_EX);


/****************************Api Write Code */
// }
// }
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server') {
    $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Composer autoloading
include __DIR__ . '/../vendor/autoload.php';

if (! class_exists(Application::class)) {
    throw new RuntimeException(
        "Unable to load application.\n"
        . "- Type `composer install` if you are developing locally.\n"
        . "- Type `vagrant ssh -c 'composer install'` if you are using Vagrant.\n"
        . "- Type `docker-compose run zf composer install` if you are using Docker.\n"
    );
}

// Retrieve configuration
$appConfig = require __DIR__ . '/../config/application.config.php';

// Run the application!
Application::init($appConfig)->run();
