requirejs.config({
  baseUrl: '/js',
  waitSeconds: 15,
  paths: {
    'jquery': [
//      'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min',
      'vendor/jquery.min'
    ],
    'bootstrap': [
//      'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min',
      'vendor/bootstrap.min'
    ],
    'pace': [
      'vendor/pace.min'
    ],
    'jquery-bootbox': [
//      'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min',
      'vendor/bootbox.min'
    ],
    'jquery-validator': [
//      'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min',
      'vendor/jquery.validate.min'
    ],
    'jquery-fileupload': [
//      'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.17.0/js/jquery.fileupload.min',
      'vendor/jquery.fileupload.min'
    ],
    'jquery.ui.widget': [
//      'https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.17.0/js/vendor/jquery.ui.widget.min',
      'vendor/jquery.ui.widget.min'
    ],
    'summernote': [
//      'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.min',
      'vendor/summernote.min'
    ],
    'jquery-mask': [
//      'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min',
      'vendor/jquery.mask.min'
    ],
    'jszip': [
//      'https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min',
      'vendor/jszip.min'
    ],
    'jquery-dataTables': [
//      'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/jquery.dataTables.min',
      'vendor/jquery.dataTables.min'
    ],
    'dataTables-bootstrap': [
//      'https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/dataTables.bootstrap.min',
      'vendor/dataTables.bootstrap.min'
    ],
    'dataTables-buttons': 'vendor/dataTables.buttons.min',
    'buttons-bootstrap': 'vendor/buttons.bootstrap.min',
    'buttons-flash': 'vendor/buttons.flash.min',
    'buttons-html5': 'vendor/buttons.html5.min',
    'dataTables-row-reorder': 'vendor/dataTables.rowReorder.min',
    'select2': 'vendor/select2.full.min',
    'bootstrap-datepicker': 'vendor/bootstrap-datepicker'
  },
  shim: {
    'bootstrap': {
      deps: [
        'jquery'
      ]
    },
    'jquery-bootbox': {
      deps: [
        'bootstrap'
      ]
    },
    'jquery-fileupload': {
      deps: [
        'jquery.ui.widget',
        'jquery.iframe.transport'
      ]
    },
    'dataTables-bootstrap': {
      deps: [
        'jquery-dataTables'
      ]
    },
    'dataTables-buttons': {
      deps: [
        'dataTables-bootstrap'
      ]
    },
    'buttons-bootstrap': {
      deps: [
        'dataTables-buttons'
      ]
    },
    'buttons-flash': {
      deps: [
        'buttons-bootstrap'
      ]
    },
    'buttons-html5': {
      deps: [
        'buttons-bootstrap'
      ]
    },
    'dataTables-row-reorder': {
      deps: [
        'dataTables-bootstrap'
      ]
    },

    'bootstrap-datepicker': {
      deps: [
        'jquery'
      ]
    },
  },
  map: {
    '*': {
      'datatables.net': 'jquery-dataTables',
      'datatables.net-bs': 'dataTables-bootstrap',
      'datatables.net-buttons': 'dataTables-buttons',
      'jquery-ui/ui/widget': 'jquery.ui.widget',
      'jquery.iframe.transport': 'vendor/jquery.iframe-transport'
    }
  }

});

require(['jquery','bootstrap'], function($, bootstrap){
  require(['app/theme']);
});
