define(['jquery', 'app/helpers/form-validate'], function($, validator){
  return {
    validate: function(selector){
      $(selector).validate();
    }
  }
});