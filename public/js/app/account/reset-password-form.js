define(['jquery', 'app/helpers/form-validate'], function($){

  return {
    validate: function(selector){
      $(selector).validate({
        rules:{
          password: {
            required:true,
            is_password: true
          },
          password_confirm: {
            equalTo: "#password",
          }
        },
        messages:{
          password: {
            regex: 'Minimum of 8 characters and at least 1 Capital Letter.'
          }
        }
      });
    }
}});