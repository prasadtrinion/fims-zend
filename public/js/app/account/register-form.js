define([
  'jquery',
  'app/helpers/form-validate',
  'jquery-mask',
  'app/helpers/bootbox'
], function($, validate, mask, bootbox){
  
  /**
   * Public exports object
   */
  var exports = {};
  
  var form = null;
  
  var formName;
  
  var termsPage;
  
  /**
   * Set current page ID
   */
  exports.setName = function(name){
    formName = name;
  }
  
  exports.setTermsPage = function(page){
    termsPage = page;
  }
  
  /**
   * Get validation rules
   */
  function getValidationRules(){
    var rules = {
        firstName: {
          is_person_name: true
        },
        lastName: {
          is_person_name: true
        },
        city: {
          is_city: true
        },
        confirmPassword: {
          required: function(){
            if($("#password").val() != '') {
              return true;
            }else {
              return false;
            }
          },
          equalTo: '#password'
        },
        phoneNumber: {
          required:true,
          is_phone: true
        },
        companyName: {
          is_company_name: true
        },
        salesExecutive: {
          is_person_name: true
        },
        distPhoneNumber: {
          required:true,
          is_phone: true
        },
        distCity: {
          is_city: true
        }
    };

    if(formName == 'account_register'){
      rules.username = {
          remote: {
            url: "/ajax/isUsernameAvailable",
            type: "post"
          }
      };

      rules.password = {
          required: true,
          is_password: true
      };
    } else if(formName == 'account_profile'){
      rules.password = {
          required: false,
          is_password: function(){
            var element = form.find('input[type="password"]');
            
            return (element.val() != '') ? true : false;
          }
      }
    }
    
    return rules;
  }
  
  /**
   * Form validation error placement
   */
  function validationErrorPlacement(error, element) {
    if(element.attr('name') == 'term' || element.attr('name') == 'mailingList'){
      element.parents('label').after(error);
    }
    else {
      element.after(error);
    }
  };
  
  /**
   * Init form validation
   */
  function validate(form){
    $(form).validate({
      rules: getValidationRules(),
      errorPlacement: validationErrorPlacement
    });
  }
  
  function addInputMask(){
    var selectCountry = form.find('select[name="country"]');
    var inputPhoneNumber = form.find('input[name="phoneNumber"]');
    var inputZipCode = form.find('input[name="zipCode"]');

    var selectDistCountry = form.find('select[name="distCountry"]');
    var inputDistPhoneNumber = form.find('input[name="distPhoneNumber"]');
    var inputDistZipCode = form.find('input[name="distZipCode"]');

    inputPhoneNumber.mask('000-000-0000');
    inputDistPhoneNumber.mask('000-000-0000');

    // Initial
    if(selectCountry.val() == 'USA'){
      inputZipCode.mask('00000-0000');
    } else {
      inputZipCode.mask('AAA AAA');
    }

    // On change
    selectCountry.on('change', function(){
      if ($(this).val() == "USA"){
        inputZipCode.mask('00000-0000');
      }else {
        inputZipCode.mask('AAA AAA');
      }
    });

    // Initial
    if(selectDistCountry.val() == 'USA'){
      inputDistZipCode.mask('00000-0000');
    } else {
      inputDistZipCode.mask('AAA AAA');
    }

    // On change
    selectDistCountry.on('change', function(){
      if ($(this).val() == "USA"){
        inputDistZipCode.mask('00000-0000');
      }else {
        inputDistZipCode.mask('AAA AAA');
      }
    });
  };
  
  function populateStateList(countryCode, target)
  {
    var optionsHtml = '<option value="">(None)</option>';

    if(countryCode){
      $.ajax({
        type:'POST',
        url: '/ajax/state-list',
        data:{'country':countryCode},
        success:function(response){
          $.each(response, function(key,value){
            optionsHtml += '<option value="'+key+'">'+value+'</option>';
          });
          target.html(optionsHtml);
        }
      });
    }
    else {
      target.html(optionsHtml);
    }
  };
  
  function toggleStateLabel(countryCode, target)
  {
    switch(countryCode){
      case 'USA':
        target.html('State');
        break;
      case 'CAN':
        target.html('Province');
        break; 
      default:
        target.html('State');
      break;
    }
  };
  
  exports.init = function(params){
    form = $('#' + params.id);
    validate(form);
    addInputMask(form);
    
    // Country - Account information
    form.find('select[name="country"]').on('change', function(){
      var selectState = form.find('select[name="state"]');
      var stateLabelElem = $('#label_state').find('span:eq(0)');
      var countryCode = $(this).val();
      
      populateStateList(countryCode, selectState);
      toggleStateLabel(countryCode, stateLabelElem);
    });
    
    // Country - Business information
    form.find('select[name="distCountry"]').on('change', function(){
      var selectState = form.find('select[name="distState"]');
      var stateLabelElem = $('#label_dist_state').find('span:eq(0)');
      var countryCode = $(this).val();
      
      populateStateList(countryCode, selectState);
      toggleStateLabel(countryCode, stateLabelElem);
    });
    
    $(document).on('click', 'button[data-action]', function(e){
      e.preventDefault();
      
      var action = $(this).data('action');

      if(action == 'submit'){
        handleFormSubmit(e);
      }
    });
  }
  
  function handleFormSubmit(e){
    if(form.valid()){
      if(termsPage){
        bootbox.dialog({
          title: termsPage.title,
          size: 'large',
          message: termsPage.content,
          buttons: {
            cancel: {
              label: 'Cancel'
            },
            success: {
              label: 'Accept',
              callback: function(){
                form.trigger('submit');
              }
            }
          }
        });
      } else {
        form.trigger('submit');
      }
    }
  }
  
  return exports;
});