define(['jquery','app/helpers/bootbox'], function($, bootbox){
  return {
    
    /**
     * Show alert message if user's email is not verified.
     */
    showEmailStatusAlert: function(authUser){
      if(authUser.emailStatus == 0){
        var appendText= 'You have not yet verified your new email address (' + authUser.username + '). Should we <a class="alert-link">re-send the verification email ?</a>';
        $('.wrapper-content')
          .prepend('<div class="alert alert-info">' + appendText + '</div></div>')
          .on('click', '.alert-link', function(e){
            
            // send email verification link
            $.ajax({
              url: '/ajax/send-email-verification',
              beforeSend: function(){
                bootbox.dialog({
                  message: '<p>Sending mail, please wait...</p>'
                });
              },
              success: function(response){
                if(response.result){
                  bootbox.hideAll();
                  bootbox.dialog({
                    message: response.message,
                    buttons: {
                      close: {
                        label: 'Close',
                        className: 'btn btn-primary'
                      }
                    }
                  });
                }
              }
            });
          });
      }
    }
  }
});