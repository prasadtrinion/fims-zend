define(['jquery', 'app/helpers/form-validate'], function($, validator){
  
  return {
    validate: function(selector){
      $(selector).validate({
        rules: {
          username: "required",
          password: {
            required: true
          }
        },
        messages:{
          username: {
            required: "Please enter your email address"
          },
          password: {
            required: "Please enter your password."
          }
        }
      })
    }
  }
});