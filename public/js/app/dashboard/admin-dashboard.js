define([
  'jquery',
  'app/helpers/datagrid'
], function($, datagrid){
  
  var userRole;
  
  function setUserRole(role){
    userRole = role;
  }
  
  /**
   * Allowance claims seeking approval
   */
  function initClaimHistory(params){
    var getDsrName = function(data){
      return data.firstName + ' ' + data.lastName;
    }
    
    $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      serverSide: true,
      ajax: {
        url: '/claim/list',
        type: 'post',
        data: {
          filters: {
            status: 4 // Submitted claims
          }
        }
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/claim/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[5, 'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: 'status', orderable: false, width: '10%'},
               { title: 'Program Name', data: 'programName'},
               { title: 'DSR Name', data: null, render: getDsrName, width: '15%'},
               { title: 'DSR Email', data: 'email'},
               { title: 'Customer Name', data: 'custName', width: '15%'},
               { title: 'Submit Date', data: 'submitDate', width: '10%'},
               { data: 'actionUrl', sortable: false, width: '5%'}]
    });
  }
  
  /**
   * Distributor Accounts Seeking Approval
   */
  function initUnverifiedUserList(params){
    
    function renderStatus(data, row){
      return data.status;
    }
    
    function renderActionButton(data, row){
      var html;
      
      if(userRole == 'SYSTEM_ADMIN'){
        html = '<a href="' + data.editUrl + '" class="btn btn-primary btn-sm">Verify</a>';
      }
      else {
        html = '<a href="' + data.viewUrl + '" class="btn btn-primary btn-sm">View</a>';
      }
      
      return html;
    }
    
    $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      serverSide: true,
      ajax: {
        url: '/users/list',
        type: 'post',
        data: {
          filters: {
            role: 3,
            status: 0
          }
        }
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/users/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[5,'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: null, orderable: false, render: renderStatus, width: '10%'},
               { title: 'Email', name: 'username', data: 'username'},
               { title: 'Company name', name: 'companyName', data: 'companyName', width: '20%'},
               { title: 'First name', name: 'firstName', data: 'firstName', width: '15%'},
               { title: 'Last name', name: 'lastName', data: 'lastName', width: '15%'},
               { title: 'Updated', name: 'updateDtTm', data: 'updateDtTm', width: '10%'},
               { data: null, sortable: false, render: renderActionButton, width: '5%'}
               ]
    });
  }
  
  return {
    'initUnverifiedUserList': initUnverifiedUserList,
    'initClaimHistory': initClaimHistory,
    'setUserRole': setUserRole
  }
});