define([
  'jquery',
  'app/helpers/datagrid'
], function($, datagrid){
  
  var userRole;
  
  function setUserRole(role){
    userRole = role;
  }

  /**
   * Drafted and Rejected claims
   */
  function initDraftedClaims(params){
    var getDsrName = function(data){
      return data.firstName + ' ' + data.lastName;
    }
    
    $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      serverSide: true,
      processing: true,
      ajax: {
        url: '/claim/list',
        type: 'post',
        data: {
          filters: {
            status: [3,15]
          }
        }
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/claim/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[3, 'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: 'status', orderable: false, width: '10%'},
               { title: 'Program Name', data: 'programName'},
               { title: 'Customer Name', data: 'custName', width: '20%'},
               { title: 'Updated Date', data: 'updateDtTm', width: '10%'},
               { title: 'Actions', data: 'actionUrl', sortable: false, width: '5%'}]
    });
  }

  /**
   * Claim History
   */
  function initClaimHistory(params){
    var getDsrName = function(data){
      return data.firstName + ' ' + data.lastName;
    }
    
    $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      serverSide: true,
      processing: true,
      ajax: {
        url: '/claim/list',
        type: 'post'
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/claim/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[3, 'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: 'status', orderable: false, width: '10%'},
               { title: 'Program Name', data: 'programName'},
               { title: 'Customer Name', data: 'custName', width: '20%'},
               { title: 'Submit Date', data: 'submitDate', width: '10%'},
               { title: 'Allowance Amount', data: 'totalAmount', width: '10%'},
               { data: 'actionUrl', sortable: false, width: '5%'}]
    });
  }

  return {
    'initClaimHistory': initClaimHistory,
    'initDraftedClaims': initDraftedClaims,
    'setUserRole': setUserRole
  }
});