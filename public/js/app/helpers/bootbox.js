define([
  'jquery',
  'jquery-bootbox'
], function($, bootbox){
  
  // Set global default settings
  bootbox.setDefaults({
    animate: false,
    onEscape: false,
    className: 'inmodal'
  });
  
  return bootbox;
})