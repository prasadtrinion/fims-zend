define(['jquery', 'jquery-validator'], function($){

    /**
     * Custom validation methods
     */
    var customValidators = {
        'regex': [
          function(value, element, regexpr) {
            return regexpr.test(value);
          }, 
          "Invalid input"
        ],
        'email': [
          function(value, element) {
            var regex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

            return this.optional(element) || regex.test(value);
          },
          'Please enter a valid email address.'
        ],
        'is_password': [
          function(value, element) {
            var regex = /^(?=.*[A-Z]).{8,}/;
            
            return this.optional(element) || regex.test(value);
          },
          'Minimum of 8 characters and at least 1 Capital Letter.'
        ],
        'is_phone': [
          function(value, element) {
            var regex = /^(\d){3}-(\d){3}-(\d){4}$/;
            
            return this.optional(element) || regex.test(value);
          },
          'Please enter a valid phone number.'
        ],
        'filesize':[
            function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param)
            },
            'File size must be less than 2MB'
        ],
        'greaterThanDate': [
          function(value, element, param) {
            var $min = $(param);
            if (this.settings.onfocusout) {
              $min.off(".validate-greaterThanDate").on("blur.validate-greaterThan", function() {
                $(element).valid();
              });
            }
            return new Date(value) > new Date($min.val());
          }, 
          'Must be greater than Start Date'
        ],
        'is_person_name': [
          function(value, element) {
            var regex = /^[a-zA-Z-\s\.\']+$/;
            
            return this.optional(element) || regex.test(value);
          },
          'Please enter a valid name.'
        ],
        'is_company_name': [
          function(value, element) {
            var regex = /^[a-zA-Z0-9-\s\.\']+$/;
            
            return this.optional(element) || regex.test(value);
          },
          'Please enter a valid name.'
        ],
        'is_city': [
          function(value, element) {
            var regex = /^[a-zA-Z\s]+$/;
            
            return this.optional(element) || regex.test(value);
          },
          'Please enter a valid city name.'
        ]
    }
    
    for(var key in customValidators){
      var validator = customValidators[key];
      
      $.validator.addMethod(key, validator[0], validator[1]);
    }
    
    /**
     * Setting default values
     */
    $.validator.setDefaults({
      messages: {
        confirmPassword:{
          equalTo: "Password and Confirm Password Should be same",
        },
        username:{
          remote: "Username already in use! ",
        },
        term: {
          required: "Please accept the Terms."
        }
      }
    });
    
    return this;
});