define([
  'jquery',
  'bootstrap',
  'jszip',
  'jquery-dataTables',
  'dataTables-bootstrap',
  'dataTables-buttons',
  'buttons-bootstrap',
  'buttons-flash',
  'buttons-html5'
], function($){
  
  // Global default settings for DataTable
  $.extend(true, $.fn.DataTable.defaults, {

  });
});