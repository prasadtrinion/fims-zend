define(['jquery','bootstrap','app/helpers/datagrid'], function($){
  return {
    init: function(selector){

    },
    initDatatable: function(selector){
      $(selector).DataTable({
        dom: '<"html5buttons"B>lTfgtip',
        columns: [null,null,null,null,{"orderable" : false}],
        buttons: [
          {
            extend: 'collection',
            text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
            buttons: [
              {
                extend: 'excel',
                filename: 'product-list',
                text: '<i class="fa fa-download"></i>&nbsp;&nbsp;Download xlsx',
                exportOptions: {
                  columns: [1,2,3]
                }
              }
              ],
              fade: 0
          }
          ]
      });
    }
  }
});