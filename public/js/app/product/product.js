define(['jquery', 'jquery-bootbox', 'summernote', 'app/helpers/form-validate','jquery-fileupload'], function ($, bootbox, summernote, validator) {
    var form;
    var onSaveLoadCallBack;
    var fileData = null;

    function save(){
        if(fileData != null )
        {

            var xhr = fileData.submit();
            form.append('<p>Saving...</p><div class="form-group">\
                  <div class="progress"><div class="progress-bar" role="progressbar" style="min-width: 2em;">0%</div>\
                  </div></div>');

            xhr.complete(function() {
                if(typeof onSaveLoadCallBack == 'function'){
                    onSaveLoadCallBack();
                }else {
                    window.location.reload();
                }

            });

            if(typeof onSaveLoadCallBack == 'function'){
                onSaveLoadCallBack();
            }else {
                return false;
            }

        }else {
            var actionUrl = form.attr('action');
            $.ajax({
                url: actionUrl,
                type: 'post',
                data: $(form).serialize(),
                success: function (response) {

                    if (response.status == 401) {
                        document.location.href = response.redirectUrl;
                    }
                    else if (response.status == 'SUCCESS') {
                        if(typeof onSaveLoadCallBack == 'function'){
                            onSaveLoadCallBack();
                        }else {
                            window.location.reload();
                        }
                    }
                }
            });

        }
    }

    function onFormSubmit() {

        // form = $(this).find('form');

        var action = form.data('action');
        var editor = form.find('.note-editor')
        var editorContent = editor.find('.note-editable').text();
        var isEmptyDescription = (editorContent == '' && action != 'delete') ? true : false;

        $(form).validate({
            rules: {
                productNumber: {
                    required: true,
                    regex: /^[a-z0-9\*]+$/i
                },
                description: {
                    required: true
                },
                productImage:{
                    filesize: 2097152
                },
            },
            messages: {
                productNumber: {
                    required: "Please enter your product number"
                },
                description: {
                    required: "Please enter  product description"
                },
            }
        });

        if ($(form).valid() && !isEmptyDescription) {
            return save();
        } else {
            editor.after('<label class="error">This field is required</label>');
            return false;
        }
    }


    function checkPopUp(){

        if(typeof onSaveLoadCallBack == 'function'){
            onSaveLoadCallBack();

        }

    }
    function createProduct(url,action){
        $.ajax({
            url: url,
            success: function (response) {

                if (response.status == 401) {
                    document.location.href = response.redirectUrl;
                }
                else if (response.status == 'SUCCESS') {
                    location.reload();
                }
                else {
                    // callback();
                    var dialog = bootbox.dialog({
                        closeButton: true,
                        title: 'Create New Product',
                        message: response,
                        className: 'inmodal',
                        size: 'medium',
                        buttons: {
                            cancel: {
                                label: 'Cancel',
                                callback: checkPopUp,
                            },
                            confirm: {
                                label: 'Save',
                                callback: onFormSubmit
                            }
                        },

                    });

                    if (action == 'delete') {
                        dialog.find('.modal-title').text('Delete Product');
                        dialog.find('.btn-primary').text('Delete');
                    } else if (action == 'edit') {
                        dialog.find('.modal-title').text('Edit Product');
                    }

                    dialog.find('textarea[name="description"]').summernote({
                        toolbar: [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']]
                        ],
                        height: 100
                    });

                    form = dialog.find('form');
                    form.find('input[type="file"]').val();
                    var uploadUrl = form.attr('action');

                    // Initialize jQuery fileupload plugin
                    dialog.find('input[type="file"]').fileupload({
                        url: uploadUrl,
                        sequentialUploads: true,
                        dataType: 'json',
                        replaceFileInput: false,
                        add: function (e, data) {
                            fileData = data;
                        },
                        progressall: function(e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10) + '%';
                            var progressBar = dialog.find('.progress-bar');
                            progressBar.css(
                                'width',
                                progress
                            ).html(progress);
                        }
                    });

                    dialog.find('a[data-action]').on('click', function (e) {
                        e.preventDefault();
                        $('#image_sh').hide();
                        $('#up_bt').show();
                    });


                }
            }
        });
    }

    return {
        init: function (selector) {


            $(selector).on('click', function (e) {
                e.preventDefault();
                var actionUrl = $(this).data('url');
                var action = $(this).data('action');
                if(action == 'create' || action =='edit' || action =='delete') {

                    createProduct(actionUrl,action);
                }
            });

        },
        create: function(url,action){
            createProduct(url,action);
        },

        setOnFormLoadCallBack: function(callback){
            this.onFormLoadCallBack = callback;
        },
        setOnSaveLoadCallBack: function(callback){
            onSaveLoadCallBack = callback;
        }



    }
});