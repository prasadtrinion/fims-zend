define([
  'jquery',
  'app/helpers/bootbox',
  'jquery-fileupload'
], function($, bootbox, fileupload){
  
  /**
   * Initialize module
   */
  function init(){
    $(document).on('click', 'a[data-action], button[data-action]', function(e) {
      var action = $(this).data('action');
      
      if(action == 'downloadCheckRequestFile'){
        generateList();
      }else if(action == 'uploadChecksPaidFile'){
        uploadList();
      }
    });
  }
  
  /**
   * Modal dialog interface to generate check request file
   */
  function generateList() {
    var dialogHtml = '<form><div class="form-group">\
      <label>Choose country: </label>\
      <select id="country" class="form-control"><option value="USA">United States</option>\
      <option value="CAN">Canada</option></select></div></form>';
    
    var dialogTitle = 'Download check request file';
    var dialog = bootbox.dialog({
      title: dialogTitle,
      message: dialogHtml,
      buttons: {
        cancel: {
          label: 'Cancel'
        },
        success: {
          label: 'Download',
          callback: function() {
            var code = $('#country').val();
            dialog.hide();
            
            var downloadProgress = bootbox.dialog({
              message: '<h3>Please wait do not click "refresh" or "back" button.</p>\
                <p><i class="fa fa-spinner fa-spin"></i> Please wait...</h3>',
                closeButton: false,
                className:'',
                backdrop: false
            });
            
            $.ajax({
              method: 'post',
              data: {
                'countryCode': code
              },
              url: '/check-payment/generate-list',
              success: function(response) {
                downloadProgress.hide();

                if(response.status == 0){
                  var message = 'There are no <strong>Approved</strong> claims to download.';
                }
                else {
                  var message = '<p>Downloading file. Please wait...</p>\
                    <p>If download fails, you can re-download from this url: <br/><a href="' + response.downloadUrl + '">' + response.downloadUrl + '</a></p>';
                  document.location = response.downloadUrl;
                }

                bootbox.dialog({
                  title: dialogTitle,
                  message: message,
                  buttons:{
                    success: {
                      label: 'Ok',
                      callback: function(){
                        window.location.reload();
                      }
                    }
                  }
                });
              }
            });
          },
        }
      }
    });
  }
  
  /**
   * Modal dialog interface to upload check request file
   */
  function uploadList() {
    var dialog = bootbox.dialog({
      title: 'Upload checks paid file',
      message: '<p>Choose a valid Excel (.xlsx) file to upload.</p>\
        <div class="form-group">\
        <label title="Upload image file" class="btn btn-primary">Choose file...\
        <input type="file" name="file" class="hide" accept=".xls,.xlsx"></label>\
        </div>',
      buttons: {
        cancel: {
          'label': 'Cancel'
        },
      }
    });

    var uploadUrl = '/check-payment/upload-list';
    var dialogBody = dialog.find('.modal-body');

    // Initialize jQuery fileupload plugin
    $(dialog).find('input[type="file"]').fileupload({
      url: uploadUrl,
      sequentialUploads: true,
      dataType: 'json',
      add: function (e, data) {
        var inputFile = data.files[0];
        var isValidFile = false;
        var acceptFileTypes = /(\.xls|\.xlsx)$/i;

        if (acceptFileTypes.test(inputFile['name'])) {
          isValidFile = true;
        }

        if(isValidFile){
          var xhr = data.submit();
          dialogBody.html('<p>Uploading file</p><div class="form-group">\
                  <div class="progress"><div class="progress-bar" role="progressbar" style="min-width: 2em;">0%</div>\
                  </div></div>'
          );
          xhr.complete(function(){
            dialog.hide();
          });
        }
        else {
          dialog.hide();
          bootbox.dialog({
            message: 'Please choose a valid Excel (.xlsx) file.',
            backdrop:false,
            closeButton: false,
            buttons: {
              Ok: function(){
                dialog.show();
              }
            }
          });
        }
      },
      progressall: function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10) + '%';
        var progressBar = dialogBody.find('.progress-bar');
        progressBar.css(
          'width',
          progress
        ).html(progress);
      },
      done: updateCheckDetails
    });
  }

  /**
   * Sends ajax request ater uploading CSV file, to import diversey master list
   */
  function updateCheckDetails(e, data){
    var dialog = bootbox.dialog({
      message: '<h3>Updating records. Do not press the "refresh" or "back" button.</p>\
        <p><i class="fa fa-spinner fa-spin"></i> Please wait...</h3>',
        backdrop:false,
        className: null,
        closeButton: false
    });

    $.ajax({
      url: '/check-payment/process-list',
      type: 'post',
      data: {"file": data.result.files[0]},
      success: function(data){
        if(data.status == "SUCCESS"){
          bootbox.hideAll();
          var message = '<h3>' + data.message + '</h3>\
          <p>Inserted: <span class="badge badge-primary">'+ data.updatedCount +'</span> | \
          Skipped: <span class="badge badge-danger">'+ data.skippedCount +'</span>';
          
          var dialog = bootbox.dialog({
            size: 'large',
            animate: false,
            className: '',
            message: message,
            buttons: {
              success: {
                label: 'Ok',
                callback: function(){
                  window.location.reload();
                }
              }
            }
          });
          
          dialog.css({
            'overflow-y': 'auto'
          });
          
          if(data.skippedCount){
            dialog.find('.modal-body').append('<div class="hr-line-dashed"></div>\
                    <p>Records shown below are skipped due to one of the following reasons.</p>\
                    <ul>\
                    <li>Allowance Claim ID doesn\'t exists.</li>\
                    <li>Allowance Claim has already been updated to status "CHECK_PAID".</li>\
                    <li>Check number is empty or invalid.</li>\
                    <li>Check amount is empty or invalid.</li>\
                    <li>Check date is empty or invalid.</li>\
                    </ul>\
                    <table id="skipped_entries" class="table table-bordered table-hover"></table>');
            
            $('#skipped_entries').dataTable({
              dom:'<"html5buttons"B>fgtrip',
              data: data.skippedRows,
              displayLength: 5,
              buttons: [{
                extend: 'excel',
                filename: 'checks-update-skipped',
                text: '<i class="fa fa-download"></i> xlxs',
              }],
              columns: [
                        { title: "Allowance Claim ID", data: 0},
                        { title: "Business Name", data: 1 },
                        { title: "Distributor Name", data: 9 },
                        { title: "Check Number", data: 11 },
                        { title: "Check Amount", data: 12 },
                        { title: "Check Date", data: 13 },
                       ]
            });
          }
        }
        else if(data.error){
          bootbox.alert(data.message);
        }
      }
    });
  }
  
  return {
    'init': init
  }
});