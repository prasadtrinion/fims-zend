define([
  'jquery',
  'jquery-fileupload',
  'app/helpers/bootbox',
  'app/helpers/datagrid'
], function($, fileupload, bootbox, datagrid){
  
  var form;
  
  function getUploadUrl() {
    return '/diversey-master/upload';
  }
  
  function getImportUrl(){
    return '/diversey-master/import';
  }
  
  function getListUrl(){
    return '/diversey-master/list';
  }
  
  /**
   * Initialize module

   * @param params
   */
  function init(params){
    $(document).on('click', 'a[data-action], button[data-action]', function() {
      var action = $(this).data('action');

      switch (action) {
      case 'importDiverseyList':
        uploadDiverseyList();
        break;
      case 'importUsersList':
        uploadUsersList();
        break;
      case 'find_diverseyid':
        populateDiverseyMasterList();
        break;
      }
    });
    
    if(params.formId){
      form = $('#' + params.formId);
    }
  }
    
  /**
   * Modal dialog interface to upload Diversey master list file
   */
  function uploadDiverseyList() {
    var dialog = bootbox.dialog({
      className: 'inmodal',
      title: 'Import Diversey Master list',
      message: '<p>Choose a valid Excel (.xlsx) file to import.</p>\
        <div class="form-group">\
        <label title="Upload image file" class="btn btn-primary">Choose file...\
        <input type="file" name="file" class="hide" accept=".xlx,.xlsx"></label>\
        </div>',
      buttons: {
        cancel: {
          'label': 'Cancel'
        },
      }
    });

    var dialogBody = dialog.find('.modal-body');

    // Initialize jQuery fileupload plugin
    $(dialog).find('input[type="file"]').fileupload({
      url: getUploadUrl(),
      sequentialUploads: true,
      dataType: 'json',
      add: function(e, data) {
        var inputFile = data.files[0];
        var isValidFile = false;
        var acceptFileTypes = /(\.xls|\.xlsx)$/i;

        if (acceptFileTypes.test(inputFile['name'])) {
          isValidFile = true;
        }

        if (isValidFile) {
          var xhr = data.submit();
          dialogBody.html('<p>Uploading file</p><div class="form-group">\
                  <div class="progress"><div class="progress-bar" role="progressbar" style="min-width: 2em;">0%</div>\
                  </div></div>');
          xhr.complete(function() {
            dialog.hide();
          });
        } else {
          dialog.hide();
          bootbox.dialog({
            message: 'Please choose a valid Excel (.xlsx) file.',
            backdrop: false,
            closeButton: false,
            animate: false,
            buttons: {
              Ok: function() {
                dialog.show();
              }
            }
          });
        }
      },
      progressall: function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10) + '%';
        var progressBar = dialogBody.find('.progress-bar');
        progressBar.css(
          'width',
          progress
        ).html(progress);
      },
      done: importDiverseyList
    });
  }

  /**
   * Sends ajax request ater uploading CSV file, to import diversey master list
   */
  function importDiverseyList(e, data) {
    var dialog = bootbox.dialog({
      message: '<h3>Importing data. Do not press the "refresh" or "back" button.</p>\
        <p><i class="fa fa-spinner fa-spin"></i> Please wait...</h3>',
      onEscapeClose: false,
      backdrop: false,
      closeButton: false
    });

    $.ajax({
      url: getImportUrl(),
      type: 'post',
      data: {
        "file": data.result.files[0]
      },
      success: function(data) {
        if (data.status == "SUCCESS") {
          bootbox.hideAll();
          var message = '<h3>' + data.message + '</h3>\
          <p>Inserted rows: <strong>' + data.insertedRows + '</strong></p>\
          <p>Updated rows: <strong>' + data.updatedRows + '</strong></p>\
          <p>Time elapsed: <strong>' + data.elapsedTime + ' secs</strong></p>';
        } else if (data.error) {
          var message = data.error.message;
        }

        bootbox.alert(message);
      }
    });
  }

  /**
   * Modal dialog interface to populate diversey master list
   * Server-side processing for dataTable
   */
  function populateDiverseyMasterList() {
    
    var hasFilters = true;
    var filters = {
            companyName: form.find('input[name="companyName"]').val(),
            country: form.find('select[name="distCountry"]').val(),
            state: form.find('select[name="distState"]').val(),
            city: form.find('input[name="distCity"]').val(),
            zipCode: form.find('input[name="distZipCode"]').val()
    };
    
    var dialog = bootbox.dialog({
      size: 'large',
      onEscape: true,
      className: 'inmodal',
      title: 'Diversey ID Finder',
      message: '<div class="table-responsive" ><table id="diversey_list" class="table table-bordered table-hover table-striped"></table></div>',
    });

    var table = $('#diversey_list').DataTable({
      dom: '<"#filters">lfgtrip',
      processing: true,
      serverSide: true,
      ajax: {
        url: getListUrl(),
        type: 'post',
        data: function(d) {
          if(hasFilters){
            d.filters = filters
          }
        }
      },
      displayLength: 5,
      lengthMenu: [
        [5, 10, 25, 50],
        [5, 10, 25, 50]
      ],
      columns: [{
        title: 'Customer Name',
        name: 'customerName',
        data: 'customerName'
      }, {
        title: 'Diversey ID',
        name: 'diverseyId',
        data: 'diverseyId'
      }, {
        title: 'Business Name',
        name: 'businessName',
        data: 'businessName'
      }, {
        title: 'Address',
        name: 'address',
        data: 'address'
      }, {
        title: 'City',
        name: 'city',
        data: 'city'
      }, {
        title: 'State',
        name: 'state',
        data: 'state'
      }, {
        title: 'Zip',
        name: 'zip',
        data: 'zip'
      }, {
        title: 'Action',
        defaultContent: '<button class="btn btn-primary btn-sm" data-action="assign_diverseyId">Select</button>'
      }]
    }).on('draw.dt',function(){

      // Add clear filter button
      if(hasFilters){
        dialog.find('#filters').html('<button data-action="clear_filters" class="btn btn-link btn-sm pull-right">\
        <i class="fa fa-filter"></i> Clear filters</button>');
      }
    });
    
    dialog.on('click', 'button[data-action]', function(){
      var action = $(this).data('action');
      if(action == 'clear_filters'){
        hasFilters = false;
        table.search('');
        table.draw();
      }
    });
    
    table.on('click', 'button', function() {
      var data = table.row($(this).parents('tr')).data();
      dialog.hide();
      bootbox.confirm('Do you want to import other data from master list to the form?', function(result){
       if(result){
         assignDiverseyId(data, true);
       }else {
         assignDiverseyId(data, false);
       }

         bootbox.hideAll();
      });
    });
  }
  
  /**
   * Assign DiverseyId to User
   */
  function assignDiverseyId(data, updateForm) {
    form.find('input[name="diverseyId"]').val(data.diverseyId);
    form.find('select[name="status"]').val(1);
    
    if(updateForm){
      form.find('input[name="companyName"]').val(data.businessName);
      form.find('input[name="distAddressLine1"]').val(data.address);
      form.find('input[name="distCity"]').val(data.city);
      form.find('input[name="distZipCode"]').val(data.zip).rules('remove');
      
      var country = null;
      
      if(stateList['USA'][data.state]){
        country = 'USA';
      } else if (stateList['CAN'][data.state]){
        country = 'CAN';
      }
      
      if(country){
        form.find('select[name="distCountry"]').val(country);
        var selectState = form.find('select[name="distState"]');
        populateState(country, data, selectState);
      }
    }
  }
  
  function populateState(countryCode, data, target)
  {
    var optionsHtml = '<option value="">(None)</option>';

    if(countryCode){
      $.ajax({
        type:'POST',
        url: '/ajax/state-list',
        data:{'country':countryCode},
        success:function(response){
          $.each(response, function(key,value){
            var attrSelected = (key == data.state) ? 'selected="selected"' : '';
            
            optionsHtml += '<option value="'+key+'"' + attrSelected + '>'+value+'</option>';
          });
          
          target.html(optionsHtml);
        }
      });
    }
    else {
      target.html(optionsHtml);
    }
  };
  
  return {
    'init': init
  }
});