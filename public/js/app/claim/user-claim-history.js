define([
  'jquery',
  'app/helpers/datagrid',
  'bootstrap-datepicker'
], function($, datagrid){
  
  var userRole;
  
  var formFilters = $('form[name="form_filters"]');
  
  var historyTable;
  
  function init() {
    $('#daterange_filter').datepicker({
      format: 'mm/dd/yyyy',
      todayHighlight: true,
      autoClose: true
    });
    
    // Apply/Clear Filters
    formFilters.on('click', 'button[data-action]', function(e){
      e.preventDefault();
      var action = $(this).data('action');

      if(action == 'clear_filters'){
        $(this).get(0).form.reset();
        historyTable.state.clear();
      }

      historyTable.order([[4, 'desc']]).draw();
    });
  }

  function setUserRole(role){
    userRole = role;
  }

  /**
   * Claim History
   */
  function initClaimHistory(params){
    var getDsrName = function(data){
      return data.firstName + ' ' + data.lastName;
    }
    
    historyTable = $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      serverSide: true,
      ajax: {
        url: '/claim/list',
        type: 'post',
        data: function(d) {
          d.filters = {
                  status: formFilters.find('select[name="status"]').val(),
                  fromDate: formFilters.find('input[name="fromDate"]').val(),
                  toDate: formFilters.find('input[name="toDate"]').val(),
                  description: formFilters.find('input[name="description"]').val()
          }
        }
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/claim/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[4, 'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: 'status', orderable: false, width: '10%'},
               { title: 'Program Name', data: 'programName'},
               { title: 'Customer Name', data: 'custName', width: '20%'},
               { title: 'Submit Date', data: 'submitDate', width: '10%'},
               { title: 'Updated Date', data: 'updateDtTm', width: '10%'},
               { title: 'Allowance Amount', data: 'totalAmount', width: '10%'},
               { data: 'actionUrl', sortable: false, width: '5%'}]
    });
  }

  return {
    'init': init,
    'initClaimHistory': initClaimHistory,
    'setUserRole': setUserRole
  }
});