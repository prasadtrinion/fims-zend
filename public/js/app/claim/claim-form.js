define([
    'jquery',
    'app/helpers/form-validate',
    'jquery-mask',
    'bootstrap-datepicker',
    'app/helpers/bootbox',
], function ($, validate, mask, picker, bootbox) {

    /**
     * Public exports object
     */
    var exports = {};

    var form = null;
    var formaction;

    /**
     * Set current page ID
     */
    exports.setName = function (name) {

    }


    /**
     * Form validation error placement
     */
    function validationErrorPlacement(error, element) {
        element.after(error);
    };


    function addInputMask() {
        var selectCountry = form.find('select[name="country"]');
        var inputPhoneNumber = form.find('input[name="phoneNumber"]');
        var inputZipCode = form.find('input[name="zipCode"]');

        inputPhoneNumber.mask('000-000-0000');

        // Initial
        if (selectCountry.val() == 'USA') {
            inputZipCode.mask('00000-0000');
        } else {
            inputZipCode.mask('AAA AAA');
        }

        // On change
        selectCountry.on('change', function () {
            if ($(this).val() == "USA") {
                inputZipCode.mask('00000-0000');
            } else {
                inputZipCode.mask('AAA AAA');
            }
        });

    };

    function populateStateList(countryCode, target) {
        var optionsHtml = '<option value="">(None)</option>';

        if (countryCode) {
            $.ajax({
                type: 'POST',
                url: '/ajax/state-list',
                data: {'country': countryCode},
                success: function (response) {
                    $.each(response, function (key, value) {
                        optionsHtml += '<option value="' + key + '">' + value + '</option>';
                    });
                    target.html(optionsHtml);
                }
            });
        }
        else {
            target.html(optionsHtml);
        }
    };

    function toggleStateLabel(countryCode, target) {
        switch (countryCode) {
            case 'USA':
                target.html('State');
                break;
            case 'CAN':
                target.html('Province');
                break;
            default:
                target.html('State');
                break;
        }
    };

    exports.init = function (params) {
        form = $('#' + params.id);
        addInputMask(form);
        form.find('input[type="submit"]').prop('disabled',false);

        form.find('input[name=submit]').on('click', function (e) {
            formaction = 'submit';
            form.validate({
              rules: {
                custName: {
                  is_person_name: true
                },
                city: {
                  is_city: true
                },
                phoneNumber: {
                  required: true,
                  is_phone: true
                },
              }
            });
        });

        form.find('input[name=draft]').on('click', function (e) {
            formaction = 'draft';
            form.validate().destroy();
        });

        $('#claimDate').datepicker({
            todayHighlight: true,
            autoclose: true
        });
        
        $('#installDate').datepicker({
          todayHighlight: true,
          autoclose: true,
          endDate: '-1d'
        });

        // Country - Account information
        form.find('select[name="country"]').on('change', function () {
            var selectState = form.find('select[name="state"]');
            var stateLabelElem = $('#label_state').find('span:eq(0)');
            var countryCode = $(this).val();

            populateStateList(countryCode, selectState);
            toggleStateLabel(countryCode, stateLabelElem);
        });

        form.find('a[data-action]').on('click', function (e) {
            e.preventDefault();
            var action = $(this).data('action');
            var url = $(this).data('url');
            var row = $(this).parents('tr');
            var productName = row.data('productname');

            if (action == 'delete') {
                $.ajax({
                    url: url,
                    type: 'post',
                    success: function (response) {

                        if (response.status == 'SUCCESS') {
                            row.remove();
                            bootbox.alert('Product <strong>' + productName + '</strong> deleted successfully');
                        }
                    }
                });
            }else if(action =='deleteDraft'){
                var rejectedFlag = $('#rejectFlag').val();
                console.log(rejectedFlag);

                if(parseInt(rejectedFlag) > 0 ){
                    bootbox.alert('<p class="text-danger"> This claims is rejected once  and cannot be deleted. </p>');
                    return false;
                }


                bootbox.confirm('Are you sure you want to delete this claim?', function(result){
                    if(result){
                        $('#deleteClaimForm').submit();
                    }
                });
            }

        });

        $(form).submit(function (e) {
            if (formaction != 'draft') {
                var invalidItems = $('#allowance_products').find('tr.invalid-items');
                var productItems = $('#allowance_products').find('tr.product-items');
                var productsHtml = '<ul>';
                $.each(invalidItems, function (val, key) {
                    var data = $(key).data('productname');
                    productsHtml = productsHtml + '<li>' + data + '</li>';
                });
                productsHtml = productsHtml + '</ul>';
                if (invalidItems.length) {
                    bootbox.alert('<p>Please delete the following products: <p>' + productsHtml);
                    return false;
                }

                var totalInstalled = 0;
                $.each(productItems, function (val, key) {
                    var num = $(key).find('input[name^="num_installed"]').val();
                    totalInstalled = totalInstalled + parseInt(num);

                });

                if (!totalInstalled) {
                    bootbox.alert('<strong style="color:red">Please add the number of product installed for at least one product</strong>');
                    return false;
                }

                if (!productItems.length) {
                    bootbox.alert("<strong style='color:red'>You can't submit this claim because it doesn't associated with any products</strong>");
                    return false;
                }
            }
        });
    }

    return exports;
});