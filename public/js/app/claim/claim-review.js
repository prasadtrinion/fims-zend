define([
    'jquery',
    'app/helpers/form-validate',
    'app/helpers/bootbox',
    'app/helpers/datagrid',
], function ($, validate, bootbox, dataTable) {


    var claimItemsData = [];
    var userAmount = [];

    var claimItemsTable;

    var userType;
    var form;
    function init(params) {
        form = $('#' + params.id);
        initClaimItemsTable();

        $('#declineButton').click(function () {
            var modal = $('#myModal2');
            modal.find('#reasoncodes').show();
            modal.find('#declineReasonCodes').show();
            modal.find('#rejectReasoncodes').hide();
            modal.find('#rejectClaim').hide();
            modal.find('#approveClaim').hide();
            modal.find('#declineClaim').show();
            modal.find('#declineReasonCodes').val('');
            modal.find('textarea').val('');
        });

        $('#myModal2').on('click', '#declineClaim', function () {
            var modal = $('#myModal2');
            var form = modal.find('form');
            form.validate();
            if (form.valid()) {
                var noteMeassage = modal.find('textarea').val();
                $("#message").val(noteMeassage);
                var resoncode = modal.find('#declineReasonCodes').val();
                $("#reasonCode").val(resoncode);
                modal.find('.modal-content').html();
                //form.attr('action','/claim/reject');
                form.submit();
                modal.modal('hide');
            } else {
                return false;
            }
        });

        $('#rejectButton').on('click', function () {
            var modal = $('#myModal2');
            modal.find('#reasoncodes').show();
            modal.find('#declineReasonCodes').hide();
            modal.find('#rejectReasoncodes').show();
            modal.find('#rejectClaim').show();
            modal.find('#declineClaim').hide();
            modal.find('#approveClaim').hide();
            modal.find('#rejectReasoncodes').val('');
            modal.find('textarea').val('');

        });

        $('#myModal2').on('click', '#rejectClaim', function () {
            var modal = $('#myModal2');

            var form = modal.find('form');
            form.validate();
            if (form.valid()) {
                var noteMeassage = modal.find('textarea').val();
                $("#message").val(noteMeassage);
                modal.find('.modal-content').html();
                form.attr('action', '/claim/reject');
                form.submit();
                modal.modal('hide');
            } else {
                return false;
            }
        });

        $('#approveButton').on('click', function (e) {
            var grandTotal = $("#grandTotal").text().replace(',', '');
            grandTotal = parseFloat(grandTotal);

            var invalidItems = $('#exclude_products').find('tr.invalid-items');
            var productsHtml = '<ul>';
            
            $.each(invalidItems, function(val, key){
                var data = $(key).data('productname');
                productsHtml = productsHtml + '<li>'+ data +'</li>';

            });
            
            productsHtml = productsHtml+ '</ul>';
            
            if(invalidItems.length) {
                bootbox.alert('<p>Please delete the following products: <p>' + productsHtml);
                return false;
            }

            var form = $('#reviewForm');
            form.validate();
            
            if (form.valid()) {
                if (grandTotal <= 0) {
                    bootbox.alert('<strong style="color:red">The total amount should be greater than 0 </strong>');
                    return false;
                } else {
                  approveClaim();
                }
            }
        });

        $('#noteModal').on('click', 'button[data-action]', function() {
            var action = $(this).data('action');
            var modal = $('#noteModal');
            var noteText = modal.find('textarea').val();
            var claimId = $('#claimId').val();
            console.log(claimId);
            if (action == 'save') {
                $.ajax({
                    url: '/claim/add-claim-note',
                    type: 'post',
                    data: {
                        'note': noteText,
                        'claimId': claimId
                    },
                    success: function(response) {
                        console.log(response);
                        var noteHtml = '<tr><td valign="top" >' + response.createDtTm + '</td><td valign="top" style="max-width:500px; padding:0px 15px;">' + response.note + '</td><td>\
              <button type="button" data-action="remove" data-url="' + response.delete_url + '"class="btn btn-danger btn-xs">\
              <i class="fa fa-remove"></i>\
              </button>\
              </td></tr>';
                        $('#noteTable').prepend(noteHtml);
                        modal.find('textarea').val('');
                        //modal.find('.modal-content').html(response)
                        modal.modal('hide');
                    }
                });
            } else if (action == 'cancel') {
                modal.find('textarea').val('');
            }
        });

        $('#noteTable').on('click', 'button[data-action]', removeClaimNote);

        form.find('a[data-action]').on('click', function (e) {
            e.preventDefault();
            var action = $(this).data('action');
            var url = $(this).data('url');
            var row = $(this).parents('tr');
            var productName = row.data('productname');

            if(action == 'delete'){
                $.ajax({
                    url: url,
                    type: 'post',
                    success: function (response) {

                        if (response.status == 'SUCCESS') {
                            row.remove();
                            bootbox.alert('Product <strong>'+productName+'</strong> deleted successfully');
                        }
                    }
                });
            }

        });
    }

    /**
     * Approve claim
     */
    function approveClaim() {
      var form = $('#reviewForm');
      var formDataObj = form.serializeArray();
      var actionUrl = form.attr('action');
      var formData = convertSerializeArrayToJson(formDataObj);
      var claimProductToBeUpdated = [];
      var claimItems = claimItemsTable.rows().data();
      var approveMessage = '';

      var grandTotal = $("#grandTotal").text().replace(',', '');
      grandTotal = parseFloat(grandTotal);

      if(claimItems.length) {
        for (var i in claimItems) {
          var data = claimItems[i];
          if (data.id) {
            claimProductToBeUpdated.push({
              "claimProductId": data.id,
              "totalAmount": data.totalAmount,
              "userAmount": userAmount[data.id]
            });
          }
        }
      }

      var postData = {
          "message": approveMessage,
          "grandTotal": grandTotal,
          "ClaimItems": claimProductToBeUpdated
      };

      $.ajax({
        url: actionUrl,
        type: 'post',
        data: postData,
        beforeSend: function( xhr ) {
          bootbox.dialog({
            message: "Saving data. Please wait...",
            closeButton:false
          });
        },
        success: function (response) {
          if (response.status == 401) {
            document.location.href = response.redirectUrl;
          }
          else if (response.status == 'SUCCESS') {
            document.location.href = '/claim/history'
          }
        }
      });
    }
  
    /**
     * Convert serialized array form data to JSON object
     */
    function convertSerializeArrayToJson(data)
    {
        var json = {};
        $.each(data, function(){
            json[this.name] = this.value;
        });

        return json;
    }

    function removeClaimNote(e) {
        var url = $(this).data('url');
        var row = $(this).parents('tr');

        bootbox.dialog({
            message: 'Are you sure you want to remove this Note?',
            buttons: {
                cancel: {
                    label: 'Cancel'
                },
                success: {
                    label: 'Remove',
                    callback: function(result) {
                        if (result) {
                            $.ajax({
                                url: url,
                                type: 'post',
                                success: function(response) {
                                    if (response.status == 'SUCCESS') {
                                        $(row).remove();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    function initClaimItemsTable() {
      
      var grandTotal = 0;

      $.each(claimItemsData, function(i, d){
          var amount = d.standardAmount;

          if (userType == 2) {
            amount = d.premiereAmount;
          }
          
          d.totalAmount = ((parseFloat(amount) * d.numInstalled)).toFixed(2);
          grandTotal += parseFloat(d.totalAmount);
          
          claimItemsData[i] = d;
      });

      $('#grandTotal').html(grandTotal.toFixed(2));
      
      function getAmount(data, type, row) {
          var amount = data.standardAmount;

          if (userType == 2) {
              amount = data.premiereAmount;
          }
          userAmount[data.id] =  amount;
          return amount;
      }

      function renderProductImage(data,type,row){

          return '<img src="/img/product/'+data.productId+'/'+data.productImage+'" width="75px">';
      }

      claimItemsTable = $('#claim_items').DataTable({
          dom: 'lT',
          paging: false,
          ordering: false,
          data: claimItemsData,
          columns: [
              {title: "Product Image", data: null,render: renderProductImage, width: "15%"},
              {title: "Product Number (SKU)", data: 'productNumber', width: "15%"},
              {title: "Description", data: 'description'},
              {title: "No. of Installed", data: 'numInstalled', width: "10%"},
              {title: "Amount", data: null, render: getAmount, width: "10%"},
              {title: "Total Amount", data: 'totalAmount', width: "10%"}
          ]
      });
    }

    function setClaimItems(claimItemJsonData, allowanceItemIds) {
        claimItemsData = claimItemJsonData;
    }

    function setUserType(type) {
        userType = type;
    }

    return {
        'init': init,
        'setClaimItems': setClaimItems,
        'setUserType': setUserType
    }
});