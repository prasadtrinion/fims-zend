define([
  'jquery',
  'app/helpers/datagrid',
  'bootstrap-datepicker'
], function($, datagrid, datepicker){
  
  var userRole;
  
  var formFilters = $('form[name="form_filters"]');
  
  var historyTable;
  
  /**
   * Initialize module
   */
  function init() {
    $('#daterange_filter').datepicker({
      format: 'mm/dd/yyyy',
      todayHighlight: true,
      autoClose: true
    });
    
    // Apply/Clear Filters
    formFilters.on('click', 'button[data-action]', function(e){
      e.preventDefault();
      var action = $(this).data('action');

      if(action == 'clear_filters'){
        $(this).get(0).form.reset();
        historyTable.state.clear();
      }

      historyTable.order([[6, 'desc']]).draw();
    });
  }

  function setUserRole(role){
    userRole = role;
  }

  /**
   * Claim History
   */
  function initClaimHistory(params){
    var getDsrName = function(data){
      return data.firstName + ' ' + data.lastName;
    }
    
    $('#daterange_filter').datepicker({
      format: 'mm/dd/yyyy',
        todayHighlight: true,
        autoClose: true
    });
    
    historyTable = $('#' + params.tableId).DataTable({
      dom: '<"html5buttons"B>lfgtrip',
      processing: true,
      serverSide: true,
      stateSave: true,
      stateSaveParams: function(settings, data){
        data.filters = formFilters.serializeArray();
      },
      stateLoadParams: function(settings, data){
        var filters = data.filters;
        for(var i in filters){
          var name = filters[i].name;
          var value = filters[i].value;

          if(name != 'status'){
            formFilters.find('input[name="'+ name +'"]').val(value);
          }else{
            formFilters.find('select[name="'+ name +'"]').val(value);
          }
        }
      },
      ajax: {
        url: '/claim/list',
        type: 'post',
        data: function(d) {
          d.filters = {
                  status: formFilters.find('select[name="status"]').val(),
                  fromDate: formFilters.find('input[name="fromDate"]').val(),
                  toDate: formFilters.find('input[name="toDate"]').val(),
                  description: formFilters.find('input[name="description"]').val(),
                  companyName: formFilters.find('input[name="companyName"]').val(),
                  distName: formFilters.find('input[name="distName"]').val(),
                  custName: formFilters.find('input[name="custName"]').val()
          }
        }
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i> Export xlsx',
          action: function(e,dt,node,config){
            window.location = '/claim/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      displayLength: 5,
      order: [[6, 'desc']],
      lengthMenu: [[5,10,25],[5,10,25]],
      columns:[{ title: 'Status', data: 'status', orderable: false, width: '10%'},
               { title: 'Program Name', data: 'programName'},
               { title: 'DSR Name', data: null, render: getDsrName, width: '15%'},
               { title: 'DSR Email', data: 'email'},
               { title: 'Customer Name', data: 'custName', width: '15%'},
               { title: 'Submit Date', data: 'submitDate', width: '10%'},
               { title: 'Updated Date', data: 'updateDtTm', width: '10%'},
               { title: 'Actions', data: 'actionUrl', sortable: false, width: '5%'}]
    }).on('xhr.dt', function(e, settings, data, xhr){
      $('#total_approved_amount').html('$' + data.totalApprovedAmount);
    });
  }
  
  return {
    'init': init,
    'initClaimHistory': initClaimHistory,
    'setUserRole': setUserRole
  }
});