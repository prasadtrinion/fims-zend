define([
  'jquery',
  'app/helpers/bootbox',
  'app/helpers/form-validate',
  'jquery-mask',
], function($, bootbox, validate, mask){
  
  /**
   * Public exports object
   */
  var exports = {};
  
  var form = null;
  
  var formName;
  
  function init(params){
    form = $('#' + params.id);
    validate(form);
    addInputMask(form);
    
    // Country - Account information
    form.find('select[name="country"]').on('change', function(){
      var selectState = form.find('select[name="state"]');
      var stateLabelElem = $('#label_state').find('span:eq(0)');
      var countryCode = $(this).val();
      
      populateStateList(countryCode, selectState);
      toggleStateLabel(countryCode, stateLabelElem);
    });
    
    // Country - Business information
    form.find('select[name="distCountry"]').on('change', function(){
      var selectState = form.find('select[name="distState"]');
      var stateLabelElem = $('#label_dist_state').find('span:eq(0)');
      var countryCode = $(this).val();
      
      populateStateList(countryCode, selectState);
      toggleStateLabel(countryCode, stateLabelElem);
    });
    
    // Toggle diverseyID field
    toggleDiverseyIdInput();
    
    $(document).on('click', 'a[data-action]', function(e) {
      e.preventDefault();
      var action = $(this).data('action');

      switch (action) {
        case 'deleteUser':
          deleteUser();
          break;
      }
    });
  }
  
  /**
   * Set current page ID
   */
  function setName(name){
    formName = name;
  }
  
  /**
   * Get validation rules
   */
  function getValidationRules(){
    var inputUsername = form.find('input[name="username"]');
    var inputPassword = form.find('input[name="password"]');
    var selectAccountType = form.find('select[name="accountType"]');
    
    var rules = {
        firstName: {
          is_person_name: true
        },
        lastName: {
          is_person_name: true
        },
        confirmPassword: {
          required: function(){
            if(inputPassword.val() != '') {
              return true;
            }else {
              return false;
            }
          },
          equalTo: '#password'
        },
        companyName: {
          is_company_name: true
        },
        salesExecutive: {
          is_person_name: true
        },
        phoneNumber: {
          required:true,
          is_phone: true
        },
        city: {
          is_city: true
        },
        distPhoneNumber: {
          required:true,
          is_phone: true
        },
        diverseyId: {
          required: function(){
            return (selectAccountType.val() != 3) ? false : true;
          }
        },
        dsrType: {
          required: function(){
            return (selectAccountType.val() != 3) ? false : true;
          }
        },
        distCity: {
          is_city: true
        }
    };

    if(formName == 'admin_user_create'){
      rules.username = {
          remote: {
            url: "/ajax/isUsernameAvailable",
            type: "post"
          }
      };

      rules.password = {
          required: true,
          is_password: true
      };
    } else if(formName == 'admin_user_edit'){
      rules.password = {
          required: false,
          is_password: function(){
            return (inputPassword.val() != '') ? true : false;
          }
      }
      
      inputUsername.on('change', function(){
        if(this.defaultValue != $(this).val()){
          inputUsername.rules('add', {
            remote: {
              url: "/ajax/isUsernameAvailable",
              type: "post"
            }
          });
        } else {
          inputUsername.rules('remove');
        }
      });
    }

    return rules;
  }
  
  /**
   * Form validation error placement
   */
  function validationErrorPlacement(error, element) {
    var name = element.attr('name');
    
    if(name == 'term' || name == 'mailingList'){
      element.parents('label').after(error);
    } else if(name == 'diverseyId'){
      element.parents('.input-group').after(error);
    }
    else {
      element.after(error);
    }
  };
  
  /**
   * Init form validation
   */
  function validate(form){
    $(form).validate({
      rules: getValidationRules(),
      errorPlacement: validationErrorPlacement
    });
  }
  
  function addInputMask(){
    var selectCountry = form.find('select[name="country"]');
    var inputPhoneNumber = form.find('input[name="phoneNumber"]');
    var inputZipCode = form.find('input[name="zipCode"]');

    var selectDistCountry = form.find('select[name="distCountry"]');
    var inputDistPhoneNumber = form.find('input[name="distPhoneNumber"]');
    var inputDistZipCode = form.find('input[name="distZipCode"]');

    inputPhoneNumber.mask('000-000-0000');
    inputDistPhoneNumber.mask('000-000-0000');

    // Initial
    if(selectCountry.val() == 'USA'){
      inputZipCode.mask('00000-0000');
    } else {
      inputZipCode.mask('AAA AAA');
    }

    // On change
    selectCountry.on('change', function(){
      if ($(this).val() == "USA"){
        inputZipCode.mask('00000-0000');
      }else {
        inputZipCode.mask('AAA AAA');
      }
    });

    // Initial
    if(selectDistCountry.val() == 'USA'){
      inputDistZipCode.mask('00000-0000');
    } else {
      inputDistZipCode.mask('AAA AAA');
    }

    // On change
    selectDistCountry.on('change', function(){
      if ($(this).val() == "USA"){
        inputDistZipCode.mask('00000-0000');
      }else {
        inputDistZipCode.mask('AAA AAA');
      }
    });
  };
  
  function populateStateList(countryCode, target)
  {
    var optionsHtml = '<option value="">(None)</option>';

    if(countryCode){
      $.ajax({
        type:'POST',
        url: '/ajax/state-list',
        data:{'country':countryCode},
        success:function(response){
          $.each(response, function(key,value){
            optionsHtml += '<option value="'+key+'">'+value+'</option>';
          });
          target.html(optionsHtml);
        }
      });
    }
    else {
      target.html(optionsHtml);
    }
  };
  
  function toggleStateLabel(countryCode, target)
  {
    switch(countryCode){
      case 'USA':
        target.html('State');
        break;
      case 'CAN':
        target.html('Province');
        break; 
      default:
        target.html('State');
      break;
    }
  };
  
  /**
   * Toggle diversey ID input field based on account type
   * 
   */
  function toggleDiverseyIdInput()
  {
    var selectAccountType = form.find('select[name="accountType"]');
    var inputDiverseyId = form.find('input[name="diverseyId"]');
    var selectDsrType = form.find('select[name="dsrType"]');
    var dsrFieldsContainer = $('#dsr_specific_fields');
    
    if(selectAccountType.val() != 3){
      dsrFieldsContainer.hide();
      inputDiverseyId.val('');
      selectDsrType.val('');
    } else {
      dsrFieldsContainer.show();
    }
    
    selectAccountType.on('change', function(){
      if($(this).val() != 3){
        dsrFieldsContainer.hide();
        inputDiverseyId.val('');
        selectDsrType.val('');
      } else {
        dsrFieldsContainer.show();
      }
    });
  }
  
  function deleteUser(){
    var form = $('#deleteUserForm');
    var claimcount = form.find('input[name="claimCount"]').val();
    
    if(parseInt(claimcount) > 0 ){
      bootbox.alert('<p class="text-danger"> This user has claims in the system and cannot be deleted. </p>');
      return false;
    }

    bootbox.confirm('Are you sure you want to delete this user?', function(result){
      if(result){
        $('#deleteUserForm').submit();
      }
    });
  }
  
  return {
    'init': init,
    'setName': setName
  }
});