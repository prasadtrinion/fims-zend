define([
  'jquery',
  'app/helpers/datagrid'
], function($){
  
  var userRole;

  /**
   * Public export object 
   */
  var exports = {};
  
  var urls = {
      getUsers: '/users/list'
  };
  
  /**
   * Set user role
   */
  exports.setUserRole = function(role){
    userRole = role;
  }
  
  /**
   * Initialize user list datatable
   */
  exports.initUserList = function(params){
    var table = $('#'+ params.tableId).DataTable({
      dom: '<"html5buttons"B>l<"#status_filters">Tfgtrip',
      processing: true,
      serverSide: true,
      ajax: {
        url: urls.getUsers,
        type: 'post',
        data: function(d) {
          var status = $('select[name="status"]').find(':selected').data('value');
          if (typeof status != 'undefined' && status != '') {
            d.filters = status;
          }
        }
      },
      rowCallback: function(row, data, index) {
        $('td:eq(0)', row).html(data.status);

        var actionsMenu = '<div class="btn-group">\
          <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle" aria-expanded="false">\
          <i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span></button>\
          <ul class="dropdown-menu pull-right">\
          <li><a href="' + data.viewUrl + '">View User</a></li>';

        if(userRole == 'SYSTEM_ADMIN'){
          actionsMenu += '<li><a href="' + data.editUrl + '">Edit User</a></li>';
        }
        
        actionsMenu += '</ul></div>';

        $('td:eq(7)', row).html(actionsMenu);
      },
      buttons: [{
        extend: 'collection',
        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
        buttons: [{
          text: '<i class="fa fa-download"></i>&nbsp;&nbsp;Export xlsx',
          action: function(e, dt, node, config) {
            window.location = '/users/export?' + $.param(dt.ajax.params());
          }
        }],
        fade: 0
      }],
      order: [
        [6, 'desc']
      ],
      columns: [{
        title: 'Status',
        data: null,
        orderable: false,
        width: '10%'
      }, {
        title: 'Email',
        name: 'username',
        data: 'username',
        order: [1, 'asc']
      }, {
        title: 'Company Name',
        name: 'companyName',
        data: 'companyName'
      }, {
        title: 'Last Name',
        name: 'lastName',
        data: 'lastName'
      }, {
        title: 'First Name',
        name: 'firstName',
        data: 'firstName'
      }, {
        title: 'Diversey ID',
        name: 'diverseyId',
        data: 'diverseyId',
        width: '15%'
      }, {
        title: 'Updated',
        name: 'updateDtTm',
        data: 'updateDtTm',
        width: '10%'
      }, {
        data: null,
        sortable: false,
        width: '5%'
      }],
    });

    var htmlFilters = '<div class="form-group pull-left">&nbsp;&nbsp;\
      <label>Filter: </label>\
      <select name="status" class="form-control">\
      <option data-value="">Show All</option>\
      <optgroup label="Status"></optgroup>\
      <option data-value=\'{\"status\":\"-1\"}\'>- Rejected</option>\
      <option data-value=\'{\"status\":\"0\"}\'>- Unverified</option>\
      <optgroup label="Role"></optgroup>\
      <option data-value=\'{\"role\":\"2\",\"status\":\"1\"}\'>- Admin</option>\
      <option data-value=\'{\"role\":\"3\",\"status\":\"1\"}\'>- Distributor</option>\
      </select></div>';

    $('#status_filters').append(htmlFilters);

    $('select[name="status"]').on('change', function() {
      table.draw();
    });
  }
  
  return exports;
  
});