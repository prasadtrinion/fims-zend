define([
    'jquery',
    'app/helpers/bootbox',
    'app/helpers/form-validate',
    'app/product/product',
    'select2',
    'app/helpers/datagrid',
    'bootstrap-datepicker',
    'dataTables-row-reorder'
], function ($, bootbox, validator,product,select2,dataTable,datepicker) {
    var form;
    var productItemsTable;
    var selectedProduct = {};
    var rowToUpdate;
    var allowanceProducts = []
    var rowData =null;
    var productData;

    function addToGrid(){

        var  productAllowanceData = {
            'productNumber': selectedProduct.productNumber,
            'description': selectedProduct.description,
            'standardAmount': parseFloat(form.find('#standardAmt').val()).toFixed(2),
            'premiereAmount':  parseFloat(form.find('#premiereAmt').val()).toFixed(2),
            'productId': selectedProduct.id,
        }

        if(rowData!=null){

            var editRow = productItemsTable.row(rowToUpdate).data();
            var tempRow = productItemsTable.rows(function(idx, data, node){
                if(editRow.productId != productAllowanceData.productId ) {
                    return (data.productId == productAllowanceData.productId) ? true : false;
                }
            }).data();

            if(tempRow.length > 0){
                bootbox.alert('<strong style="color:red">Product already associated with this allowance program.</strong>');
                return false;
            }else {
                productItemsTable.row(rowToUpdate).data(productAllowanceData).draw(true);
                rowData = null;
            }
        }else {

            var tempRow = productItemsTable.rows(function(idx, data, node){
                return (data.productId == productAllowanceData.productId) ? true : false;
            }).data();

            if(tempRow.length > 0){
                bootbox.alert('<strong style="color:red">Product already associated with this allowance program.</strong>');
                return false;
            }else {
                productItemsTable.row.add(productAllowanceData).draw(true);
            }
        }
    }

    function associatePopUp(){
        createAllowance('/allowance/associate','associate-create');
    }

    function arraycmp(cmpObj) {
        var count = 0
        $.each(productItemsTable, function(key, value) {
            if ((value.productId == cmpObj.productId)) {
                count = 1;
            }
        });

        return count;
    }

    function onFormSubmit() {
        $(form).validate({
            rules: {
                standardAmt: {
                    required: true,
                    number: true
                },
                premiereAmt: {
                    required: true,
                    number: true
                }
            }
        });

        if ($(form).valid()) {
            return addToGrid();
        } else {
            return false;
        }
    }

    function onLoadCallback(params)
    {
        createAllowance(params.url,params.action);
    }

    function createAllowance(url,action){
        if(action == 'associate-create' || action =='edit') {

            $.ajax({
                url: url,
                success: function (response) {

                    if (response.status == 401) {
                        document.location.href = response.redirectUrl;
                    }
                    else if (response.status == 'SUCCESS') {
                        location.reload();
                    }
                    else {

                        var dialog = bootbox.dialog({
                            closeButton: true,
                            title: 'Associate Product',
                            message: response,
                            className: 'inmodal',
                            size: 'medium',
                            onEscape: false,
                            buttons: {
                                cancel: {
                                    label: 'Cancel'
                                },
                                confirm: {
                                    label: 'Associate',
                                    className: 'btn-primary',
                                    callback: onFormSubmit


                                }
                            }
                        });

                        dialog.init(function(){
                            dialog.removeAttr('tabIndex');

                        });

                        if(action == 'edit')
                        {
                            selectedProduct = {
                                'id': rowData.productId,
                                'productNumber': rowData.productNumber,
                                'description': rowData.description
                            };
                            dialog.find('#product').append('<option value="' + rowData.productId + '" selected>' + rowData.description + '</option>');
                            dialog.find('#standardAmt').val(rowData.standardAmount);
                            dialog.find('#premiereAmt').val(rowData.premiereAmount);
                        }

                        dialog.find('#product').select2({
                            data: productData,
                            escapeMarkup: function(markup) {
                                return markup;
                            },
                            templateResult: formatRepo,
                            templateSelection: formatRepoSelection
                        });

                        dialog.on('click','a[data-action]', function(e){
                            e.preventDefault();
                            var action = $(this).data('action');
                            var url = $(this).data('url');
                            if(action == 'create'){
                                product.setOnSaveLoadCallBack(function(){
                                    onLoadCallback({
                                        'action': 'associate-create',
                                        'url': '/allowance/associate'
                                    });
                                });
                                product.create(url,action);

                            }
                        });

                        form = dialog.find('form');

                        dialog.on('click','#associateProduct', function(e){
                            e.preventDefault();
                            return false;
                            onFormSubmit();
                        });
                    }
                }
            });

        }
    }

    /**
     * Format Product search result
     */
    function formatRepo(repo) {
        if(!repo.id) return;

        repo.text =  '<p><strong>' + repo.description + '</strong></p><p>' + repo.productNumber + '</p>';
        return repo.text;
    }

    function formatRepoSelection(repo) {
        if (typeof repo.description != "undefined") {
            selectedProduct = repo;
        }

        return repo.description || repo.text;
    }

    return {
        init: function (selector) {
            $(selector).on('click', function (e) {
                e.preventDefault();
                var actionUrl = $(this).data('url');
                var action = $(this).data('action');
                rowData = null;
                createAllowance(actionUrl,action);
            });


            $('#Psumbit').click(function(e) {
                var submitdata = [];
                var productData = productItemsTable.rows().data();

                if (productData.length > 0) {

                    for(var i in productData) {
                        var tempData = productData[i];
                        if(tempData.productNumber != null) {
                            submitdata.push({
                                'productNumber': tempData.productNumber,
                                'description': tempData.description,
                                'standardAmount': tempData.standardAmount,
                                'premiereAmount': tempData.premiereAmount,
                                'productId': tempData.productId,
                            });
                        }
                    }
                    productData = JSON.stringify(submitdata);

                    $('#productData').val(productData);
                    return true;
                } else {
                    e.preventDefault();
                    bootbox.alert('<strong style="color:red">Please associate one or more products.</strong>');
                }
            });


            var getActionButtons = function(data, type, row, meta){
                var actionButtons = '';

                var editButton = '<li><a href="#" data-action="edit">Edit</a></li>';
                var removeButton = '<li><a href="#" data-action="remove">Delete</a></li>';

                actionButtons += editButton;

                var row = $(row);

                actionButtons += removeButton;

                return '<div class="btn-group">\
         <button  href="#" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"  aria-expanded="false" title="Actions">\
           <i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>\
        </button>\
        <ul class="dropdown-menu pull-right">' + actionButtons + '</ul>\
      </div>';
            }

            productItemsTable = $('#allowance_products').DataTable({
                paging:   false,
                rowReorder: {
                    dataSrc: 0,
                    update: false
                },
                ordering: false,
                dom: '<"html5buttons"B>lTgt',
                buttons: [{
                    extend: 'collection',
                    text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
                    buttons: [{
                        text: '<i class="fa fa-plus"></i>&nbsp;&nbsp;Associate Product',
                        action: associatePopUp,
                        autoClose: true,
                    }],
                    fade: 0
                }],
                data: allowanceProducts,
                pageLength: 10,

                columns: [
                    { title: "", data: null, defaultContent: '<i class="glyphicon glyphicon-menu-hamburger"></i>', width:"2%"},
                    { title: "Product Number (SKU)", data: 'productNumber', width:"15%" },
                    { title: "Description", data: 'description'},
                    { title: "Standard  Amount", data: 'standardAmount', width:"10%" },
                    { title: "Premiere Amount", data: 'premiereAmount', width:"10%" },
                    { title: "Actions", data: null, width:"5%", className: 'text-center', render: getActionButtons },
                ]
            });


            productItemsTable.on('click', 'a[data-action]', function(e){
                e.preventDefault();

                var action = $(this).data('action');
                var row = $(this).parents('tr');
                var item = productItemsTable.row(row).data();

                switch(action){
                    case 'edit':
                        showOrderItemForm(item, 'edit', row);
                        break;
                    case 'remove':
                        showOrderItemForm(item, 'remove', row);
                        break;
                }
            });

            function showOrderItemForm(item,action,row){

                if(action == 'edit') {
                    rowToUpdate = row;
                    var popupurl = '/allowance/associate';
                    rowData = item;
                    createAllowance(popupurl, action)
                }else if(action == 'remove'){

                    var data = productItemsTable.row(row).data();
                    var message = '<p>This action will remove product <strong>' + data.productNumber + '</strong> from the list. Are you sure you want to continue?</p>';

                    bootbox.dialog({
                        title: 'Confirm product delete',
                        animate: false,
                        message: message,
                        buttons: {
                            cancel: {

                            },
                            success: {
                                label: 'Ok',
                                className: 'btn btn-primary',
                                callback: function(){
                                    productItemsTable.row(row).remove().draw(false);
                                }
                            }
                        }
                    });
                }
            }

            $('#allowance-form').validate({
                rules: {

                    'programName': {
                        required: true,
                    },

                    'description': {
                        required: true,
                    }
                }
            });
        },
        create: function(url,action){
            createAllowance(url,action);
        },
        setAllowanceProducts: function(productData){
            allowanceProducts = productData;

        },
        setProductData: function(productJsonData){
            productData = productJsonData;
        }
    }
});