define(['jquery',
    'app/helpers/bootbox',
    'bootstrap',
    'app/helpers/datagrid'],
    function($,bootbox){
    var allowanceItemsTable;
    return {
        init: function(selector){

        },
        initDatatable: function(selector){
           /* allowanceItemsTable = $(selector).DataTable({
                dom: '<"html5buttons"B>lTfgtip',
                columns: [null,null,null,null,{"orderable" : false}],
                buttons: [
                    {
                        extend: 'collection',
                        text: '<i class="fa fa-navicon"></i>&nbsp;&nbsp;<span class="caret"></span>',
                        buttons: [
                            {
                                extend: 'excel',
                                filename: 'allowance-programs-list',
                                text: '<i class="fa fa-download"></i>&nbsp;&nbsp;Download xlsx',
                                exportOptions: {
                                    columns: [0,1,2,3]
                                }
                            }
                        ],
                        fade: 0
                    }
                ]
            });
*/
            $(selector).on('click','a[data-action]', function(e){
                e.preventDefault();
                //console.log('sadsf');
                var row = $(this).parents('tr');
                var action = $(this).data('action');
                var url = $(this).data('url');
                if(action == 'delete'){
                    var data = allowanceItemsTable.row(row).data();
                    console.log(data);
                    var message = '<p>This action will remove allowance <strong>' + data[0] + '</strong> from the list. Are you sure you want to continue?</p>';

                    bootbox.dialog({
                        title: 'Confirm allowance program delete',
                        animate: false,
                        message: message,
                        buttons: {
                            cancel: {

                            },
                            success: {
                                label: 'Ok',
                                className: 'btn btn-primary',
                                callback: function(){
                                    $.ajax({
                                        url: url,
                                        type: 'post',
                                        success: function (response) {

                                            if (response.status == 401) {
                                                document.location.href = response.redirectUrl;
                                            }
                                            else if (response.status == 'SUCCESS') {

                                                allowanceItemsTable.row(row).remove().draw(false);

                                                    // console.log('reload');
                                                    window.location.reload();
                                            }
                                        }
                                    });


                                }
                            }
                        }
                    });

                }
            });

        }
    }
});