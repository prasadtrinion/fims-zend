<?php
namespace Application\Repository;

use Application\Entity\T083fstaffContribution;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StaffContributionRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('s.f083fid, s.f083fidStaff, m.f034fname as staff, s.f083ffundCode, s.f083fdepartmentCode, s.f083factivityCode, s.f083fepfDeductable, s.f083fsocsoDeductable, s.f083ftaxDeductable,s.f083femployee, s.f083femployer, s.f083fstatus')
            ->from('Application\Entity\T083fstaffContribution', 's')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'm', 'with', 's.f083fidStaff = m.f034fstaffId')
            ->orderBy('s.f083fid','DESC');

        $query = $qb->getQuery();
	
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('s.f083fid, s.f083fidStaff, m.f034fname as staff, s.f083ffundCode, s.f083fdepartmentCode, s.f083factivityCode, s.f083fepfDeductable, s.f083fsocsoDeductable, s.f083ftaxDeductable, s.f083femployee, s.f083femployer, s.f083fstatus')
            ->from('Application\Entity\T083fstaffContribution', 's')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'm', 'with', 's.f083fidStaff = m.f034fstaffId')
            ->where('s.f083fidStaff = :staffId')
            ->setParameter('staffId',$id);
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	
        return $result;
    }

    public function createStaff($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $staff = new T083fstaffContribution();

        $staff->setF083fidStaff((int)$data['f083fidStaff'])
            ->setF083ffundCode($data['f083ffundCode'])
            ->setF083fdepartmentCode($data['f083fdepartmentCode'])
            ->setF083factivityCode((int)$data['f083factivityCode'])
            ->setF083fepfDeductable((int)$data['f083fepfDeductable'])
            ->setF083fsocsoDeductable((int)$data['f083fsocsoDeductable'])
            ->setF083ftaxDeductable((int)$data['f083ftaxDeductable'])
            ->setF083femployee((int)$data['f083femployee'])
            ->setF083femployer((int)$data['f083femployer'])
            ->setF083fstatus((int)$data['f083fstatus'])
            ->setF083fcreatedBy((int)$_SESSION['userId'])
            ->setF083fupdatedBy((int)$_SESSION['userId']);

        $staff->setF083fcreatedDtTm(new \DateTime())
            ->setF083fupdatedDtTm(new \DateTime());
     
        $em->persist($staff);

        $em->flush();
	   
        return $staff;
    }
  

    public function updateStaff($staff, $data = []) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $staff->setF083fidStaff((int)$data['f083fidStaff'])
            ->setF083ffundCode($data['f083ffundCode'])
            ->setF083fdepartmentCode($data['f083fdepartmentCode'])
            ->setF083factivityCode((int)$data['f083factivityCode'])
            ->setF083fepfDeductable((int)$data['f083fepfDeductable'])
            ->setF083fsocsoDeductable((int)$data['f083fsocsoDeductable'])
            ->setF083ftaxDeductable((int)$data['f083ftaxDeductable'])
            ->setF083femployee((int)$data['f083femployee'])
            ->setF083femployer((int)$data['f083femployer'])
            ->setF083fstatus((int)$data['f083fstatus'])
            ->setF083fcreatedBy((int)$_SESSION['userId'])
            ->setF083fupdatedBy((int)$_SESSION['userId']);

        $staff->setF083fcreatedDtTm(new \DateTime())
            ->setF083fupdatedDtTm(new \DateTime());
     
        $em->persist($staff);

        $em->flush();
    }

}
