<?php
namespace Application\Repository;

use Application\Entity\T061fpayrollContribution;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PayrollContributionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f061fid, c.f061fmonth, c.f061fstatus')
            ->from('Application\Entity\T061fpayrollContribution','c');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('c.f061fid, c.f061fmonth, c.f061fstatus')
            ->from('Application\Entity\T061fpayrollContribution','c')
            ->where('c.f061fid = :contibutionId')
            ->setParameter('contibutionId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $contribution = new T061fpayrollContribution();

        $contribution->setF061fmonth($data['f061fmonth'])
             ->setF061fstatus((int)$data['f061fstatus'])
             ->setF061fcreatedBy((int)$_SESSION['userId'])
             ->setF061fupdatedBy((int)$_SESSION['userId']);

        $contribution->setF061fcreatedDtTm(new \DateTime())
                ->setF061fupdatedDtTm(new \DateTime());

 
        $em->persist($contribution);
        $em->flush();
        
        return $contribution;

    }

    /* to edit the data in database*/

    public function updateData($contribution, $data = []) 
    {
        $em = $this->getEntityManager();

        $contribution->setF061fmonth($data['f061fmonth'])
             ->setF061fstatus((int)$data['f061fstatus'])
             ->setF061fcreatedBy((int)$_SESSION['userId'])
             ->setF061fupdatedBy((int)$_SESSION['userId']);

        $contribution->setF061fcreatedDtTm(new \DateTime())
                ->setF061fupdatedDtTm(new \DateTime());

 
        $em->persist($contribution);
        $em->flush();
        
        return $contribution;

    }


}

?>