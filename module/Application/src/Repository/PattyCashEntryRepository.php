<?php
namespace Application\Repository;

use Application\Entity\T123fpattyCashEntry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PattyCashEntryRepository extends EntityRepository 
{
 
    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     

 
        $qb->select("p.f123fid, p.f123fdescription, p.f123frequestedDate, p.f123fdepartmentCode, p.f123famount, p.f123ftransactionType, p.f123fstatus, p.f123fidAllocation, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, dms.f011fdefinitionCode, pca.f122freferenceNo, p.f123freemberasment")
            ->from('Application\Entity\T123fpattyCashEntry', 'p')
             ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f123fdepartmentCode')
              ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f123ftransactionType')
              ->leftjoin('Application\Entity\T122fpattyCashAllocation', 'pca','with','pca.f122fid= p.f123fidAllocation')
             ->orderBy('p.f123fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f123frequestedDate'];
            $item['f123frequestedDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("p.f123fid, p.f123fdescription, p.f123frequestedDate, p.f123fdepartmentCode, p.f123famount, p.f123ftransactionType, p.f123fstatus, p.f123fidAllocation, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, dms.f011fdefinitionCode, pca.f122freferenceNo, p.f123freemberasment")
            ->from('Application\Entity\T123fpattyCashEntry', 'p')
             ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f123fdepartmentCode')
             ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f123ftransactionType')
             ->leftjoin('Application\Entity\T122fpattyCashAllocation', 'pca','with','pca.f122fid= p.f123fidAllocation')
            ->where('p.f123fid = :pattyCashId')
            ->setParameter('pattyCashId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f123frequestedDate'];
            $item['f123frequestedDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $pattyCash = new T123fpattyCashEntry();

        $pattyCash->setF123fdescription($data['f123fdescription'])
        ->setF123frequestedDate(new \DateTime($data['f123frequestedDate']))
        ->setF123fdepartmentCode($data['f123fdepartmentCode'])
        ->setF123famount((float)$data['f123famount'])
        ->setF123ftransactionType((int)$data['f123ftransactionType'])
        ->setF123fidAllocation((int)$data['f123fidAllocation'])
        ->setF123freemberasment((int)0)
        ->setF123fstatus((int)0)
        ->setF123fcreatedBy((int)$_SESSION['userId'])
        ->setF123fupdatedBy((int)$_SESSION['userId']);

        $pattyCash->setF123fcreatedDtTm(new \DateTime())
               ->setF123fupdatedDtTm(new \DateTime());
         
          try{
                $em->persist($pattyCash);
                $em->flush();
        }

        catch(\Exception $e)
        {
            echo $e;
        }
        return $pattyCash;
    }

    public function updateData($pattyCash, $data = []) 
    {
        $em = $this->getEntityManager();

        $pattyCash->setF123fdescription($data['f123fdescription'])
        ->setF123frequestedDate(new \DateTime($data['f123frequestedDate']))
        ->setF123fdepartmentCode($data['f123fdepartmentCode'])
        ->setF123famount((float)$data['f123famount'])
        ->setF123ftransactionType((int)$data['f123ftransactionType'])
        ->setF123fidAllocation((int)$data['f123fidAllocation'])
        ->setF123freemberasment((int)0)
        // ->setF123fstatus((int)$data['f123fstatus'])
        ->setF123fupdatedBy((int)$_SESSION['userId']);

        $pattyCash->setF123fupdatedDtTm(new \DateTime());

        try{
        $em->persist($pattyCash);
        $em->flush();
        }
        catch (\Exception $e){
            echo $e;
        }
    }
    public function getApprovedPattyCashEntry($status)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("p.f123fid, p.f123fdescription, p.f123frequestedDate, p.f123fdepartmentCode, p.f123famount, p.f123ftransactionType, dms.f011fdefinitionCode , p.f123fstatus, p.f123fidAllocation, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, pca.f122freferenceNo, p.f123freemberasment")
            ->from('Application\Entity\T123fpattyCashEntry', 'p')
            ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f123fdepartmentCode')
           ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f123ftransactionType')
           ->leftjoin('Application\Entity\T122fpattyCashAllocation', 'pca','with','pca.f122fid= p.f123fidAllocation')
             //  ->leftjoin('Application\Entity\T014fuser', 'u','with','u.f014fid= p.f122fapprover')
            ->where('p.f123fstatus = :pattyCashId')
            ->andWhere('p.f123freemberasment = 0')
            ->setParameter('pattyCashId',$status)
            ->orderBy('p.f123fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f123frequestedDate'];
            $item['f123frequestedDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

    public function approvePettyCashEntry($data)
    {
        $em = $this->getEntityManager();
        $user_id = $_SESSION['userId'];
        foreach ($data['id'] as $id)
        {
            try
            {
            $status = (int)$data['status'];
            $reason = $data['reason'];

            // print_r($reason);exit;
            $query1 = "update t123fpatty_cash_entry set f123fstatus = $status, f123fupdated_by = $user_id, f123freason = '$reason' where f123fid = $id";
            // print_r($query1);exit;
            $result = $em->getConnection()->executeQuery($query1);
            }
            catch(\Exception $e)
            {
                echo $e;
            }
        }   
    }

    public function pettyCashEntryReambersment($data)
    {
        $em = $this->getEntityManager();
        $user_id = $_SESSION['userId'];
        foreach ($data['id'] as $id)
        {
            try
            {
            $status = (int)$data['reambers'];

            if($status == 1)
            {
                $query2 = "update t123fpatty_cash_entry set f123freemberasment = $status, f123fupdated_by = $user_id where f123fid = $id";
                // print_r($query1);exit;
                $em->getConnection()->executeQuery($query2);
            }
            }
            catch(\Exception $e)
            {
                echo $e;
            }
        }   
    }
} 
?>