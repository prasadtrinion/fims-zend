<?php
namespace Application\Repository;

use Application\Entity\T131loanDeductionRatio;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class LoanDeductionRatioRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("lr.f131fid, lr.f131fdeductionPercentage, lr.f131fstatus")
            ->from('Application\Entity\T131loanDeductionRatio', 'lr')
            ->orderBy('lr.f131fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("lr.f131fid, lr.f131fdeductionPercentage, lr.f131fstatus")
            ->from('Application\Entity\T131loanDeductionRatio', 'lr')
            ->where('lr.f131fid = :blockId')
            ->setParameter('blockId',$id);
             
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
       
     	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $loan_deduction = new T131loanDeductionRatio();

        $loan_deduction->setF131fdeductionPercentage((float)$data['f131fdeductionPercentage'])
        ->setF131fstatus((int)$data['f131fstatus'])
        ->setF131fcreatedBy((int)$_SESSION['userId']);

        $loan_deduction->setF131fcreatedDtTm(new \DateTime());
     
            $em->persist($loan_deduction);
            $em->flush();

        	return $loan_deduction;
    }

    public function updateData($loan_deduction, $data = []) 
    {
        $em = $this->getEntityManager();
        // print_r($loan_deduction);exit;

        $loan_deduction->setF131fdeductionPercentage((float)$data['f131fdeductionPercentage'])
        ->setF131fstatus((int)$data['f131fstatus'])
                ->setF131fupdatedBy((int)$_SESSION['userId']);

        $loan_deduction->setF131fupdatedDtTm(new \DateTime());

        $em->persist($loan_deduction);
        $em->flush();
    }

    public function getLoanDeductionRatioActive($id)
    {
    	$em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("lr.f131fid, lr.f131fdeductionPercentage, lr.f131fstatus")
            ->from('Application\Entity\T131loanDeductionRatio', 'lr')
            ->where('lr.f131fstatus = :loan')
            ->setParameter('loan',$id)
            ->orderBy('lr.f131fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
}
