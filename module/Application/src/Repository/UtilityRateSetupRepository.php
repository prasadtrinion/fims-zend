<?php
namespace Application\Repository;

use Application\Entity\T078futilityRateSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class UtilityRateSetupRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('ur.f078fid, ur.f078fminUnit, ur.f078fmaxUnit, ur.f078famount, ur.f078feffectiveDate, ur.f078fstatus')
            ->from('Application\Entity\T078futilityRateSetup', 'ur')
            ->orderBy('ur.f078feffectiveDate','ASC')
            ->orderBy('ur.f078fid','DESC');
            // ->leftjoin('Application\Entity\T078futility','u','with', 'ur.f078fidUtility=u.f078fid');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        // print_r($result);exit;
        foreach ($result as $utility) {
            $date                        = $utility['f078feffectiveDate'];
            $utility['f078feffectiveDate'] = date("Y-m-d", strtotime($date));
            // $date                        = $utility['f078fmeterReadingOn'];
            // $utility['f078fmeterReadingOn'] = date("Y-m-d", strtotime($date));
            array_push($result1, $utility);
        }

        // $result = array(
        //     'data' => $result,
        // );
        return $result1;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('ur.f078fid, ur.f078fminUnit, ur.f078fmaxUnit, ur.f078famount, ur.f078feffectiveDate, ur.f078fstatus')

            ->from('Application\Entity\T078futilityRateSetup', 'ur')
//            ->leftjoin('Application\Entity\T078futility','u','with', 'ur.f078fidUtility=u.f078fid')

            ->where('ur.f078fid = :utilityId')
            ->setParameter('utilityId',(int)$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078feffectiveDate'];
            $utility['f078feffectiveDate'] = date("Y-m-d", strtotime($date));
            // $date                        = $utility['f078fmeterReadingOn'];
            // $utility['f078fmeterReadingOn'] = date("Y-m-d", strtotime($date));
            array_push($result1, $utility);
        }

        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $utility = new T078futilityRateSetup();

        // $utility->setF078fidUtility((int)$data['f078fidUtility']);
        $utility->setF078fminUnit((float)$data['f078fminUnit'])
                ->setF078fmaxUnit((float)$data['f078fmaxUnit'])
                ->setF078famount((float)$data['f078famount'])
                ->setF078feffectiveDate(new \DateTime($data['f078feffectiveDate']))
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fcreatedBy((int)$_SESSION['userId']);

        $utility->setF078fcreatedDtTm(new \DateTime());


        try{
            $em->persist($utility);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $utility;
    }

    public function updateData($utility, $data = []) 
    {
        $em = $this->getEntityManager();

         // $utility->setF078fidUtility((int)$data['f078fidUtility']);
        $utility->setF078fminUnit((float)$data['f078fminUnit'])
                ->setF078fmaxUnit((float)$data['f078fmaxUnit'])
                ->setF078famount((float)$data['f078famount'])
                ->setF078feffectiveDate(new \DateTime($data['f078feffectiveDate']))
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fupdatedDtTm(new \DateTime());


        
        $em->persist($utility);
        $em->flush();

    }

   

}
