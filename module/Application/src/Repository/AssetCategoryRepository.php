<?php
namespace Application\Repository;

use Application\Entity\T087fassetCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetCategoryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f087fid, c.f087fcategoryCode, c.f087fcategoryDiscription, c.f087fidItemGroup, c.f087fstatus, s.f028fsubCategoryName as itemGroup, c.f087fpercentage')
            ->from('Application\Entity\T087fassetCategory','c')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'c.f087fidItemGroup = s.f028fid')
            ->orderBy('c.f087fid','DESC');
            // ->orderBy('c.f087fcategoryCode','ASC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('c.f087fid, c.f087fcategoryCode, c.f087fcategoryDiscription, c.f087fidItemGroup, c.f087fstatus, s.f028fsubCategoryName as itemGroup, c.f087fpercentage')
            ->from('Application\Entity\T087fassetCategory','c')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'c.f087fidItemGroup = s.f028fid')
            ->orderBy('c.f087fid','DESC')
            ->where('c.f087fstatus = 1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f087fid, c.f087fcategoryCode, c.f087fcategoryDiscription, c.f087fidItemGroup, c.f087fstatus, s.f028fsubCategoryName as itemGroup, c.f087fpercentage')
            ->from('Application\Entity\T087fassetCategory','c')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'c.f087fidItemGroup = s.f028fid')
             ->where('c.f087fid = :assetId')
            ->setParameter('assetId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $asset = new T087fassetCategory();

        $asset->setF087fcategoryCode($data['f087fcategoryCode'])
             ->setF087fcategoryDiscription($data['f087fcategoryDiscription'])
             ->setF087fidItemGroup((int)$data['f087fidItemGroup'])
             ->setF087fpercentage((float)$data['f087fpercentage'])
             ->setF087fstatus((int)$data['f087fstatus'])
             ->setF087fcreatedBy((int)$_SESSION['userId'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fcreatedDtTm(new \DateTime())
                ->setF087fupdatedDtTm(new \DateTime());

 
        $em->persist($asset);
        $em->flush();

        return $asset;

    }

    public function updateData($asset, $data = []) 
    {
        $em = $this->getEntityManager();

         $asset->setF087fcategoryCode($data['f087fcategoryCode'])
             ->setF087fcategoryDiscription($data['f087fcategoryDiscription'])
             ->setF087fidItemGroup((int)$data['f087fidItemGroup'])
             ->setF087fpercentage((float)$data['f087fpercentage'])
             ->setF087fstatus((int)$data['f087fstatus'])
             ->setF087fcreatedBy((int)$_SESSION['userId'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fupdatedDtTm(new \DateTime());
        
        $em->persist($asset);
        $em->flush();

    }

    // public function getActiveList() 
    // {
    //     $em = $this->getEntityManager();

    //     $qb = $em->createQueryBuilder();

    //     // Query
    //     $qb->select('c.f087fid, c.f087fcategoryCode, c.f087fcategoryDiscription, c.f087fidItemGroup, c.f087fstatus')
    //         ->from('Application\Entity\T087fassetCategory','c')
    //         ->orderBy('c.f087fcategoryName')
    //         ->where('c.f087fstatus=1');

    //     $query = $qb->getQuery();

    //     $result = array(
    //         'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
    //     );

    //     return $result;
        
    // }

    public function getAssetByAssetSubCategory($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("i.f088fid as itemId, (i.f088fitemCode+'-'+i.f088fitemDescription) as itemName")
            ->from('Application\Entity\T088fitem','i')
            // ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'c.f087fidItemGroup = s.f028fid')
             ->where('i.f088fidSubCategory = :assetId')
            ->setParameter('assetId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
        
    }

}