<?php
namespace Application\Repository;

use Application\Entity\T085frevenue;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class RevenueRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query


        $qb->select("g.f085fid, g.f085ffundId, g.f085factivityId, g.f085fdepartmentId, g.f085faccountId, g.f085fstatus, g.f085ffeeItem, fi.f066fname as feeItem, f.f057fname as fundName, d.f015fdepartmentName as departmentName, at.f058fname as activityName, ac.f059fname as accountName, (g.f085ffundId+'-'+f.f057fname) as Fund, (g.f085factivityId+'-'+at.f058fname) as Activity, (g.f085fdepartmentId+'-'+d.f015fdepartmentName) as Department, (g.f085faccountId+'-'+ac.f059fname) as Account, (fi.f066fcode+'-'+fi.f066fname) as Feename")
            ->from('Application\Entity\T085frevenue', 'g')
        // $qb->select('g.f085fid, g.f085ffundId, g.f085factivityId, g.f085fdepartmentId, g.f085faccountId, g.f085fstatus, g.f085ffeeItem, fi.f066fname as, f.f057fname as fundName')
        //     ->from('Application\Entity\T085frevenueSetUp', 'g')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f085ffundId = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f085factivityId = at.f058fcompleteCode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f085fdepartmentId = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f085faccountId = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T066ffeeItem', 'fi', 'with', 'g.f085ffeeItem = fi.f066fid')
            ->orderBy('g.f085fid','DESC');
            

        // $qb->select('g.f085fid, g.f085ffundId, g.f085factivityId, g.f085fdepartmentId, g.f085faccountId, g.f085fstatus, g.f085ffeeItem, f.f057fname as fundName')
        //     ->from('Application\Entity\T085frevenueSetUp', 'g')
        //     ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f085ffundId = f.f057fid');
            // ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f085factivityId = at.f058fid')
            // ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f085fdepartmentId = d.f015fid')
            // ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f085faccountId = ac.f059fid')
            // ->leftjoin('Application\Entity\T066ffeeItem', 'f', 'with', 'g.f085ffeeItem = f.f066fid');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("g.f085fid, g.f085ffundId, g.f085factivityId, g.f085fdepartmentId, g.f085faccountId, g.f085fstatus, g.f085ffeeItem, fi.f066fname as feeItem, f.f057fname as fundName, ac.f059fname as account, (fi.f066fcode+'-'+fi.f066fname) as FeeName")
            ->from('Application\Entity\T085frevenue', 'g')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f085ffundId = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f085factivityId = at.f058fcompleteCode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f085fdepartmentId = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f085faccountId = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T066ffeeItem', 'fi', 'with', 'g.f085ffeeItem = fi.f066fid')
            ->where('g.f085fid = :revenueId')
            ->setParameter('revenueId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $revenue = new T085frevenue();

        $revenue->setF085ffeeItem((int)$data['f085ffeeItem'])
            ->setF085fstatus((int) $data['f085fstatus'])
            ->setF085ffundId( $data['f085ffundId'])
            ->setF085fdepartmentId( $data['f085fdepartmentId'])
            ->setF085factivityId( $data['f085factivityId'])
            ->setF085faccountId( $data['f085faccountId'])
            ->setF085fcreatedBy((int)$_SESSION['userId'])
            ->setF085fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF085fcreateDtTm(new \DateTime())
            ->setF085fupdateDtTm(new \DateTime());


        try{
            $em->persist($revenue);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $revenue;
    }

    public function updateData($revenue, $data = []) 
    {
        $em = $this->getEntityManager();

        $revenue->setF085ffeeItem((int)$data['f085ffeeItem'])
            ->setF085fstatus((int) $data['f085fstatus'])
            ->setF085ffundId( $data['f085ffundId'])
            ->setF085fdepartmentId( $data['f085fdepartmentId'])
            ->setF085factivityId( $data['f085factivityId'])
            ->setF085faccountId( $data['f085faccountId'])
            ->setF085fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF085fupdateDtTm(new \DateTime());


        
        $em->persist($revenue);
        $em->flush();

    }

   

}
