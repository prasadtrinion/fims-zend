<?php
namespace Application\Repository;

use Application\Entity\T047fdebtTagging;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DebtTaggingRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("d.f047fid, d.f047fidDebt, c.f021ffirstName as Debtor, d.f047fidStudent,(s.f060ficNumber+'-'+s.f060fname) as studentName, d.f047ftotalDebt, d.f047fdebtDetails, d.f047fstatus")
          ->from('Application\Entity\T047fdebtTagging','d')
          ->leftjoin('Application\Entity\T060fstudent', 's','with','d.f047fidStudent = s.f060fid')
          ->leftjoin('Application\Entity\T021fcustomers','c','with','d.f047fidDebt = c.f021fid')
          ->orderBy('d.f047fid','DESC');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );
        
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('d.f047fid, d.f047fidDebt, c.f021ffirstName as Debtor, d.f047fidStudent, s.f060fname as studentName, d.f047ftotalDebt, d.f047fdebtDetails, d.f047fstatus')
          ->from('Application\Entity\T047fdebtTagging','d')
          ->leftjoin('Application\Entity\T060fstudent', 's','with','d.f047fidStudent = s.f060fid')
          ->leftjoin('Application\Entity\T021fcustomers','c','with','d.f047fidDebt = c.f021fid')
             ->where('d.f047fid = :debtId')
            ->setParameter('debtId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $debt = new T047fdebtTagging();

        $debt->setF047fidDebt((int)$data['f047fidDebt'])
                 ->setF047fidStudent((int)$data['f047fidStudent'])
                 ->setF047ftotalDebt((float)$data['f047ftotalDebt'])
                 ->setF047fdebtDetails($data['f047fdebtDetails'])
                 ->setF047fstatus((int)$data['f047fstatus'])
                 ->setF047fcreatedBy((int)$_SESSION['userId'])
                 ->setF047fupdatedBy((int)$_SESSION['userId']);

        $debt->setF047fcreatedDtTm(new \DateTime())
                ->setF047fupdatedDtTm(new \DateTime());

    try{
        $em->persist($debt);
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
            
        return $debt;

    }

    public function updateData($debt, $data = []) 
    {
        $em = $this->getEntityManager();


        $debt->setF047fidDebt((int)$data['f047fidDebt'])
                 ->setF047fidStudent((int)$data['f047fidStudent'])
                 ->setF047ftotalDebt((float)$data['f047ftotalDebt'])
                 ->setF047fdebtDetails($data['f047fdebtDetails'])
                 ->setF047fstatus((int)$data['f047fstatus'])
                 ->setF047fupdatedBy((int)$_SESSION['userId']);

        $debt->setF047fupdatedDtTm(new \DateTime());

 
        $em->persist($debt);
        $em->flush();

    }

}
