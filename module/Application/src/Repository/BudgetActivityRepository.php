<?php
namespace Application\Repository;

use Application\Entity\T054fbudgetDepartAllocation;
use Application\Entity\T054factivityMaster;
use Application\Entity\T108fbudgetSummary;
use Application\Entity\T054factivityDetails;
use Application\Entity\T015fdepartment;
use Application\Entity\T015ffinancialyear;
use Application\Entity\T014fglcode;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityRepository;

class BudgetActivityRepository extends EntityRepository
{
 	public function getActivity() 
	{

 		$em = $this->getEntityManager();	

		$qb = $em->createQueryBuilder();

		$qb->select("am.f054fid, am.f054fdepartment, am.f054ffund, (fu.f057fcode+' - '+fu.f057fname) as fundName, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f054fdepartment = d.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','am.f054ffund = fu.f057fcode')
		   ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f054fidFinancialyear = f.f110fid')
		   ->orderBy('am.f054fid', 'DESC');

		// $qb->select('am.f054fid, am.f054fdepartment,am.f054ffund,fu.f057fname as fundName, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear')
		//    ->from('Application\Entity\T054factivityMaster', 'am')
		//    ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f054fdepartment = d.f015fdepartmentCode')
		//    ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','am.f054ffund = fu.f057fcode')
		//    ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f054fidFinancialyear = f.f110fid')
		   
		   // ->groupBy('bc.f054fidFinancialyear');
		   // ->groupBy('bc.f054fdepartment');

		$query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
         
    }

    public function getListById($id)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        
          $qb->select('am.f054fid, am.f054fdepartment,am.f054ffund, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, ad.f054fidDetails, ad.f054ffundCode, ad.f054fdepartmentCode, ad.f054factivityCode, ad.f054faccountCode, ad.f054famount, ad.f054fq1,ad.f054fsoCode, ad.f054fq2, ad.f054fq3, ad.f054fq4, ad.f054fbudgetType')
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T054factivityDetails', 'ad', 'with','am.f054fid = ad.f054fidActivityMaster')
		    ->where('ad.f054fstatus!=2')
		   	->andWhere('am.f054fid = :budgetActivityId')
           	->setParameter('budgetActivityId',$id);
            
		$query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
		
	}

	public function getListByApprovedStatus($approvedStatus)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        
		// echo("hi");
		// exit;
           if($approvedStatus == 0)
           {
              $qb->select("am.f054fid, am.f054fdepartment, am.f054ffund, (fu.f057fcode+' - '+fu.f057fname) as fundName, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f054fdepartment = d.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','am.f054ffund = fu.f057fcode')
		   ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f054fidFinancialyear = f.f110fid')
                 ->where('am.f054fapprovalStatus = 0');
                 // ->groupBy('bc.f054fdepartment,bc.f054fidFinancialyear');
            }

            elseif($approvedStatus == 1)
            {
               $qb->select("am.f054fid, am.f054fdepartment, am.f054ffund, (fu.f057fcode+' - '+fu.f057fname) as fundName, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f054fdepartment = d.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','am.f054ffund = fu.f057fcode')
		   ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f054fidFinancialyear = f.f110fid')
		   		->where('am.f054fapprovalStatus = 1');
		   		// ->groupBy('bc.f054fdepartment,bc.f054fidFinancialyear');
            }

            elseif($approvedStatus == 2) 
            {
               $qb->select("am.f054fid, am.f054fdepartment, am.f054ffund, (fu.f057fcode+' - '+fu.f057fname) as fundName, am.f054fidFinancialyear, am.f054ftotalAmount, am.f054fapprovalStatus, am.f054fstatus, am.f054fbalance, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f054fdepartment = d.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','am.f054ffund = fu.f057fcode')
		   ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f054fidFinancialyear = f.f110fid');
		   		// ->groupBy('bc.f054fdepartment,bc.f054fidFinancialyear');
		   		
            }
        	
		$data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
		
		return $data;
		
	}

	// public function createActivity($data) 
	// {
	// 	$em = $this->getEntityManager();
	// 	// print_r($postdata);
	// 	// die();

	// 		$Activity = new T054fbudgetActivity();

	// 		$Activity->setF054fdepartment((int)$data['f054fdepartment'])
	// 				   ->setF054fidFinancialyear((int)$data['f054fidFinancialyear'])
	// 		    	   ->setF054fidGlcode((int)$data['f054fidGlcode'])
	// 		           ->setF054famount((float)$data['f054famount'])
	// 		    	   ->setF054fq1((float)$data['f054fq1'])
	// 		    	   ->setF054fq2((float)$data['f054fq2'])
	// 		    	   ->setF054fq3((float)$data['f054fq3'])
	// 		    	   ->setF054fq4((float)$data['f054fq4'])
	// 		    	   ->setF054fbalance((float)$data['f054famount'])
	// 		    	   ->setF054fapprovalStatus((int)$data['f054fapprovalStatus'])
	// 		    	   ->setF054fstatus((int)$data['f054fstatus'])
	// 		    	   ->setF054fcreatedBy((int)'1')
	//      			   ->setF054fupdatedBy((int)'1');

	// 		$Activity->setF054fcreatedDtTm(new \DateTime())
 //    			       ->setF054fupdatedDtTm(new \DateTime());

		
	// 		$em->persist($Activity);

	// 		try
	// 		{
	// 		$em->flush();
	// 		}
	// 		catch(\Exception $e)
	// 		{
	// 			echo $e;
			// }
			

			// $Allocation = new T054fbudgetDepartAllocation();

			// $Allocation->setF054fidBudgetActivity((int)$Activity->getF054fid())
			// 		   ->setF054fopeningBalance((float)$data['f054famount'])
			// 		   ->setF054fbalanceAmount((float)$data['f054famount'])
			// 		   ->setF054fcrAmount((float)'0')
			// 		   ->setF054fdrAmount((float)'0')
			// 		   ->setF054fstatus((int)$data['f054fstatus'])
			// 		   ->setF054fcreatedBy((int)'1')
	  //    			   ->setF054fupdatedBy((int)'1');

	  //    	$Allocation->setF054fcreatedDtTm(new \DateTime())
   //  			       ->setF054fupdatedDtTm(new \DateTime());	

   //  		$em->persist($Allocation);	 

   //  		$em->flush();

		
		//return $Activity;
	// }

	public function createActivity($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $activity = new T054factivityMaster();

        $activity->setF054fdepartment($data['f054fdepartment'])
					   ->setF054ffund($data['f054ffund'])
					   ->setF054fidFinancialyear((int)$data['f054fidFinancialyear'])
			           ->setF054ftotalAmount((float)$data['f054ftotalAmount'])
			    	   ->setF054fbalance((float)$data['f054ftotalAmount'])
			    	   ->setF054fapprovalStatus((int)$data['f054fapprovalStatus'])
			    	   ->setF054fstatus((int)$data['f054fstatus'])
			    	   ->setF054fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF054fupdatedBy((int)$_SESSION['userId']);

		$activity->setF054fcreatedDtTm(new \DateTime())
    			       ->setF054fupdatedDtTm(new \DateTime());
        
        $em->persist($activity);
        $em->flush();
        
        $activityDetails = $data['activity-details'];

        foreach ($activityDetails as $activityDetail) {

            $activityDetailObj = new T054factivityDetails();

            $activityDetailObj->setF054fidActivityMaster((int)$activity->getF054fid())
			           ->setF054ffundCode( $activityDetail['f054ffundCode'])
			            ->setF054faccountCode( $activityDetail['f054faccountCode'])
			            ->setF054factivityCode( $activityDetail['f054factivityCode'])
			            ->setF054fdepartmentCode( $activityDetail['f054fdepartmentCode'])
			            ->setF054fsoCode( $activityDetail['f054fsoCode'])
                        ->setF054famount((float)$activityDetail['f054famount'])
                        ->setF054fbalanceAmount((float)$activityDetail['f054famount'])
                        ->setF054fbudgetType($activityDetail['f054fbudgetType'])
			    	   ->setF054fq1((float)$activityDetail['f054fq1'])
			    	   ->setF054fq2((float)$activityDetail['f054fq2'])
			    	   ->setF054fq3((float)$activityDetail['f054fq3'])
			    	   ->setF054fq4((float)$activityDetail['f054fq4'])
			    	   ->setF054fstatus((int)$activityDetail['f054fstatus'])
			    	   ->setF054fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF054fupdatedBy((int)$_SESSION['userId']);

			$activityDetailObj->setF054fcreatedDtTm(new \DateTime())
    			       ->setF054fupdatedDtTm(new \DateTime());

        

        $em->persist($activityDetailObj);

        $em->flush();

            
        }
        return $activity;
    }

    public function updateActivity($activity, $data = [])
    {
        $em = $this->getEntityManager();

        $activity->setF054fdepartment($data['f054fdepartment'])
					   ->setF054ffund($data['f054ffund'])
					   ->setF054fidFinancialyear((int)$data['f054fidFinancialyear'])
			           ->setF054ftotalAmount((float)$data['f054ftotalAmount'])
			    	   ->setF054fbalance((float)$data['f054ftotalAmount'])
			    	   ->setF054fapprovalStatus((int)$data['f054fapprovalStatus'])
			    	   ->setF054fstatus((int)$data['f054fstatus'])
	     			   ->setF054fupdatedBy((int)$_SESSION['userId']);

		$activity->setF054fupdatedDtTm(new \DateTime());
        
        $em->persist($activity);

        $em->flush();
        
        return $activity;
    }


    public function updateActivityDetail($activityDetailObj, $activityDetail)
    {
        
        $em = $this->getEntityManager();

        $activityDetailObj->setF054ffundCode($activityDetail['f054ffundCode'])
            ->setF054faccountCode($activityDetail['f054faccountCode'])
            ->setF054factivityCode($activityDetail['f054factivityCode'])
            ->setF054fdepartmentCode($activityDetail['f054fdepartmentCode'])
            ->setF054famount((float)$activityDetail['f054famount'])
			            ->setF054fsoCode( $activityDetail['f054fsoCode'])
						->setF054fbudgetType($activityDetail['f054fbudgetType'])
			    	   ->setF054fq1((float)$activityDetail['f054fq1'])
			    	   ->setF054fq2((float)$activityDetail['f054fq2'])
			    	   ->setF054fq3((float)$activityDetail['f054fq3'])
			    	   ->setF054fq4((float)$activityDetail['f054fq4'])
			    	   ->setF054fstatus((int)$activityDetail['f054fstatus'])
	     			   ->setF054fupdatedBy((int)$_SESSION['userId']);

		$activityDetailObj->setF054fupdatedDtTm(new \DateTime());

        $em->persist($activityDetailObj);

        $em->flush();

            
        
    }
    
    public function createActivityDetail($activityObj, $activityDetail)
    {
        $em = $this->getEntityManager();

        $activityDetailObj = new T054factivityDetails();

        $activityDetailObj->setF054fidActivityMaster((int)$activityObj->getF054fid())
			    	   ->setF054ffundCode( $activityDetail['f054ffundCode'])
            ->setF054faccountCode( $activityDetail['f054faccountCode'])
            ->setF054factivityCode( $activityDetail['f054factivityCode'])
            ->setF054fdepartmentCode( $activityDetail['f054fdepartmentCode'])
                             ->setF054famount((float)$activityDetail['f054famount'])
                             ->setF054fbudgetType($activityDetail['f054fbudgetType'])
			    	   ->setF054fq1((float)$activityDetail['f054fq1'])
			            ->setF054fsoCode( $activityDetail['f054fsoCode'])
						->setF054fq2((float)$activityDetail['f054fq2'])
			    	   ->setF054fq3((float)$activityDetail['f054fq3'])
			    	   ->setF054fq4((float)$activityDetail['f054fq4'])
			    	   ->setF054fstatus((int)$activityDetail['f054fstatus'])
			    	   ->setF054fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF054fupdatedBy((int)$_SESSION['userId']);

		$activityDetailObj->setF054fcreatedDtTm(new \DateTime())
    			       ->setF054fupdatedDtTm(new \DateTime());

        $em->persist($activityDetailObj);

        $em->flush();

        return $activityDetailObj;
        
    }
   

	public function updateApprovalStatus($activities,$status,$reason) 
	{
		$em = $this->getEntityManager();

		foreach ($activities as $activity) 
		{
            $Id = $activity->getF054fid();
            $id=(int)$Id;
            
           $query = "UPDATE t054factivity_master set f054freason='$reason',f054fapproval_status=$status where f054fid = $id"; 
			$result=$em->getConnection()->executeQuery($query);

			if($status == 1){
			$query = "select f054fid_details,f054fid_activity_master,f054ffund_code,f054fid_financialyear,f054fdepartment_code,f054factivity_code,f054faccount_code,f054famount from t054factivity_details inner join t054factivity_master on f054fid_activity_master = f054fid where f054fid = $id"; 
			$result=$em->getConnection()->executeQuery($query)->fetchAll();
			
			
		foreach ($result as $row) 
	{
            $budgetSummary = new T108fbudgetSummary();

			$budgetSummary->setF108fdepartment($row['f054fdepartment_code'])
			->setF108ffund($row['f054ffund_code'])
			->setF108factivity($row['f054factivity_code'])
			->setF108faccount($row['f054faccount_code'])
                         ->setF108fidFinancialYear((int)$row['f054fid_financialyear'])
                         ->setF108fallocatedAmount((float)$row['f054famount'])
                         ->setF108fincrementAmount(0)
                         ->setF108fdecrementAmount(0)
                         ->setF108fverimentFrom(0)
                         ->setF108fverimentTo(0)
                         ->setF108fbudgetExpensed(0)
                         ->setF108fbudgetPending((float)$row['f054famount'])
                         ->setF108fbudgetCommitment(0)
                         ->setF108fstatus((int)1)
                         ->setF108fcreatedBy((int)$_SESSION['userId'])
                         ->setF108fupdatedBy((int)$_SESSION['userId']);

            $budgetSummary->setF108fcreatedDtTm(new \DateTime())
                        ->setF108fupdatedDtTm(new \DateTime());

            $em->persist($budgetSummary);
			$em->flush();
			
            }

		}
	}
    }

    public function updateActivityDetailStatus($deletedIds) 
	{
		$em = $this->getEntityManager();

		foreach ($deletedIds as $deletedId) 
		{
            

           $query = "UPDATE t054factivity_details set f054fstatus = 2 where f054fid_details = $deletedId"; 
            $result=$em->getConnection()->executeQuery($query);
           
            

        }
        return;
        // exit();
    }


    public function getActivityAmount($department,$idFinancialyear)
    {
    	
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b.f054fid,b.f054ftotalAmount')
            ->from('Application\Entity\T054factivityMaster','b')
		   	->leftjoin('Application\Entity\T054factivityDetails','a', 'with','a.f054fidActivityMaster = b.f054fid')
            ->where('b.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('b.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);
            

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

       
        
        return $result;
    }

     public function getActivityDetails($department,$idFinancialyear)
    {
    	
   
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $qb->select("ad.f054ffundCode, f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T054factivityDetails', 'ad', 'with','am.f054fid = ad.f054fidActivityMaster')
		   ->leftjoin('Application\Entity\T057ffund', 'f', 'with','ad.f054ffundCode = f.f057fcode')
            ->where('am.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('am.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = $result[0];
        $result['Fund'] = $result1; 
       
        $qb = $em->createQueryBuilder();
        $qb->select("ad.f054fdepartmentCode, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T054factivityDetails', 'ad', 'with','am.f054fid = ad.f054fidActivityMaster')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','ad.f054fdepartmentCode = d.f015fdepartmentCode')
            ->where('am.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('am.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);

        $query = $qb->getQuery();
        $result2 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result['Department'] = $result2;

        $qb = $em->createQueryBuilder();
        $qb->select("ad.f054factivityCode, a.f058fcompleteCode, a.f058fname,  (a.f058fcompleteCode+ ' - '+ a.f058fname) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T054factivityDetails', 'ad', 'with','am.f054fid = ad.f054fidActivityMaster')
		   ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with','ad.f054factivityCode = a.f058fcompleteCode')
            ->where('am.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('am.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);
        $query = $qb->getQuery();
        $result3 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result['ActivityCode'] = $result3;

        $qb = $em->createQueryBuilder();
        $qb->select("ad.f054faccountCode, a.f059fcompleteCode, a.f059fname,  (a.f059fcompleteCode+ ' - '+ a.f059fname) as originalName")
		   ->from('Application\Entity\T054factivityMaster', 'am')
		   ->leftjoin('Application\Entity\T054factivityDetails', 'ad', 'with','am.f054fid = ad.f054fidActivityMaster')
		   ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with','ad.f054faccountCode = a.f059fcompleteCode')
            ->where('am.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('am.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);
        $query = $qb->getQuery();
        $result4 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result['AccountCode'] = $result4;

        

        return $result;
    }


    

    public function getActivityGlcode($department,$idFinancialyear)
    {
    	
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b.f054fidDetails, b.f054ffundCode, b.f054faccountCode, b.f054factivityCode, b.f054fdepartmentCode, c.f059fname as accountName')
            ->from('Application\Entity\T054factivityDetails','b')
		   	->leftjoin('Application\Entity\T054factivityMaster','a', 'with','b.f054fidActivityMaster = a.f054fid')
		   	->leftjoin('Application\Entity\T059faccountCode','c','with', 'b.f054faccountCode = c.f059fcompleteCode')
            ->where('a.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)
            ->andwhere('a.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('a.f054fapprovalStatus=1');
            
            

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

       
        
        return $result;
    }

    public function getActivityDetailsAmount($postData)
    {
    	$department=$postData['Department'];
    	$Financialyear=$postData['FinancialYear'];
    	$fundCode=$postData['FundCode'];
    	$departmentCode=$postData['DepartmentCode'];
    	$activityCode=$postData['ActivityCode'];
    	$accountCode=$postData['AccountCode'];

    	$idFinancialyear=(int)$Financialyear;

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a.f054famount')
            ->from('Application\Entity\T054factivityMaster','b')
		   	->leftjoin('Application\Entity\T054factivityDetails','a', 'with','a.f054fidActivityMaster = b.f054fid')
            ->where('b.f054fdepartment = :departmentId')
            ->setParameter('departmentId',$department)

            ->andwhere('b.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)

            ->andwhere('a.f054ffundCode = :fundCode')
            ->setParameter('fundCode',$fundCode)

            ->andwhere('a.f054fdepartmentCode = :deptCode')
            ->setParameter('deptCode',$departmentCode)

            ->andwhere('a.f054factivityCode = :activity')
            ->setParameter('activity',$activityCode)

            ->andwhere('a.f054faccountCode = :account')
            ->setParameter('account',$accountCode);

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

       
        
        return $result;
    }

    public function getNonAllocatedCostcenter($idFinancialyear)
    {
    	
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("distinct(c.f051ffund), f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as originalName")
		   ->from('Application\Entity\T051fbudgetCostcenter', 'c')
		   ->leftjoin('Application\Entity\T057ffund', 'f', 'with','c.f051ffund = f.f057fcode')
		   
            ->where('c.f051fidFinancialyear = :financialyearId')
            ->andWhere('c.f051fapprovalStatus = 1')
            ->setParameter('financialyearId',$idFinancialyear);
            

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = $result[0];
        $result['Fund'] = $result1; 
       
        $qb = $em->createQueryBuilder();
        $qb->select("distinct(c.f051fdepartment), d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T051fbudgetCostcenter', 'c')
		   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','c.f051fdepartment = d.f015fdepartmentCode')

			->where('c.f051fidFinancialyear = :financialyearId')
            ->andWhere('c.f051fapprovalStatus = 1')
			
            ->setParameter('financialyearId',$idFinancialyear);
            

        $query = $qb->getQuery();
        $result2 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result['Department'] = $result2;
        
        return $result;
	}
	
	public function getAllGl($idFinancialyear)
    {
    	
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("distinct(d.f054ffundCode) as f054ffundCode, f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as originalName")
		   ->from('Application\Entity\T054factivityDetails', 'd')
		   ->leftjoin('Application\Entity\T057ffund', 'f', 'with','d.f054ffundCode = f.f057fcode')
		   ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','d.f054fidActivityMaster = am.f054fid')
		   
			->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')
			
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('am.f054fapprovalStatus=1');
            

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = $result[0];
		$result['Fund'] = $result1; 

        $qb = $em->createQueryBuilder();
		
		$qb->select("distinct(d.f054fdepartmentCode) as f054fdepartmentCode, de.f015fdepartmentName, de.f015fdepartmentCode, (de.f015fdepartmentCode+' - ' +de.f015fdepartmentName) as originalName")
		   ->from('Application\Entity\T054factivityDetails', 'd')
		   ->leftjoin('Application\Entity\T015fdepartment', 'de', 'with','d.f054fdepartmentCode = de.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','d.f054fidActivityMaster = am.f054fid')
		   
			->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')
			
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('am.f054fapprovalStatus=1');
            

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

		$result['Department'] = $result1; 
		
		$qb = $em->createQueryBuilder();
		
		$qb->select("distinct(d.f054faccountCode) as f054faccountCode, a.f059fname, a.f059fcompleteCode, (a.f059fcompleteCode+' - ' +a.f059fname) as originalName")
		   ->from('Application\Entity\T054factivityDetails', 'd')
		   ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with','d.f054faccountCode = a.f059fcompleteCode')
		   ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','d.f054fidActivityMaster = am.f054fid')
		   
			->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')
			
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('am.f054fapprovalStatus=1');
            

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

		$result['Account'] = $result1; 
		
		$qb = $em->createQueryBuilder();
		
		$qb->select("distinct(d.f054factivityCode) as f054factivityCode, a.f058fname, a.f058fcompleteCode, (a.f058fcompleteCode+' - ' +a.f058fname) as originalName")
		   ->from('Application\Entity\T054factivityDetails', 'd')
		   ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with','d.f054factivityCode = a.f058fcompleteCode')
		   ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','d.f054fidActivityMaster = am.f054fid')
			->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')
			
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('am.f054fapprovalStatus=1');
            

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result['Activity'] = $result1; 
       
       
        return $result;
	}
	

	public function getActivityAccountCodes($data)
    {
    	
	   $idFinancialyear = $data['financialYear'];
	   $department = $data['department'];
	   // $department = substr($Department,0,3);
	   $fund = $data['fund'];
	   $activity = $data['activty'];
	   $financialyear=(int)$idFinancialyear;
        $em = $this->getEntityManager();

//         $query = "select d.f054faccount_code as f054faccountCode, d.f054famount, d.f054fq1, d.f054fq2, d.f054fq3, d.f054fq4, ac.f059fname, ac.f059fcomplete_code as f059fcompleteCode, (ac.f059fcomplete_code+' - ' +ac.f059fname) as accountOriginalName,d.f054factivity_code as f054factivityCode, a.f058fname, a.f058fcomplete_code as f058fcompleteCode, (a.f058fcomplete_code+' - ' +a.f058fname) as activityOriginalName,d.f054fdepartment_code as f054fdepartmentCode, de.f015fdepartment_name as f015fdepartmentName, de.f015fdepartment_code as f015fdepartmentCode, (de.f015fdepartment_code+' - ' +de.f015fdepartment_name) as departmentOriginalName,d.f054ffund_code, f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as fundOriginalName from t054factivity_details as d inner join t059faccount_code as ac on d.f054faccount_code = ac.f059fcomplete_code inner join t058factivity_code as a on d.f054factivity_code = a.f058fcomplete_code inner join t015fdepartment as de on d.f054fdepartment_code = de.f015fdepartment_code inner join t057ffund as f on d.f054ffund_code = f.f057fcode inner join t054factivity_master as am on d.f054fid_activity_master = am.f054fid where am.f054fid_financialyear = $financialyear and am.f054fapproval_status = 1";

// if($department!=''){
// 			$query = $query." and am.f054fdepartment = '$department'";
// 		}
// 		if($fund!=''){
// $query = $query." and am.f054ffund = '$fund'";
// 		}
// 		if($activity!=''){
// $query = $query." and d.f054factivityCode = '$activity'";
// 		}
//         $result=$em->getConnection()->executeQuery($query)->fetchAll();
//         return $result;

		$qb = $em->createQueryBuilder();
		
		$qb->select("d.f054faccountCode,d.f054famount,d.f054fq1,d.f054fq2,d.f054fq3,d.f054fq4, ac.f059fname, ac.f059fcompleteCode, (ac.f059fcompleteCode+' - ' +ac.f059fname) as accountOriginalName,d.f054factivityCode, a.f058fname, a.f058fcompleteCode, (a.f058fcompleteCode+' - ' +a.f058fname) as activityOriginalName,d.f054fdepartmentCode, de.f015fdepartmentName, de.f015fdepartmentCode, (de.f015fdepartmentCode+' - ' +de.f015fdepartmentName) as departmentOriginalName,d.f054ffundCode, f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as fundOriginalName")
		   ->from('Application\Entity\T054factivityDetails', 'd')
		   ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with','d.f054faccountCode = ac.f059fcompleteCode')
		   ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with','d.f054factivityCode = a.f058fcompleteCode')
		   ->leftjoin('Application\Entity\T015fdepartment', 'de', 'with','d.f054fdepartmentCode = de.f015fdepartmentCode')
		   ->leftjoin('Application\Entity\T057ffund', 'f', 'with','d.f054ffundCode = f.f057fcode')

		   ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','d.f054fidActivityMaster = am.f054fid')
		   
            ->where('am.f054fidFinancialyear = :financialyearId')
            ->andWhere('am.f054fapprovalStatus = 1')
            ->setParameter('financialyearId',(int)$idFinancialyear);
        if($department!=''){
			$qb->andWhere("am.f054fdepartment = '$department'");
		}
		if($fund!=''){
			$qb->andWhere("am.f054ffund = '$fund'");
		}
		if($activity!=''){
			$qb->andWhere("d.f054factivityCode = '$activity'");
		}
		// echo $qb;die();
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

		return $result;
		
       
        // print_r($result);exit;
    }


	// public function budgetSummary($department,$idFinancialyear)
 //    {
    	
 //        $em = $this->getEntityManager();

 //        $qb = $em->createQueryBuilder();

 //        $qb->select('b.f054fdepartment as department, b.f054fidFinancialyear as financialYear,SUM(bd.f054famount) as amount, bd.f054ffundCode,bd.f054faccountCode,bd.f054factivityCode,bd.f054fdepartmentCode')
 //            ->from('Application\Entity\T054factivityMaster','b')
 //            ->leftjoin('Application\Entity\T054factivityDetails', 'bd', 'with','b.f054fid = bd.f054fidActivityMaster')
 //            ->where('g.f014fdepartmentId = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('b.f054fidFinancialyear = :financialyearId')
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();

	// 	$activity_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

	// 	$qb = $em->createQueryBuilder();

 //        $qb->select('v.f056fidFinancialYear as financialYear, v.f056fDepartment as department,SUM(v.f056ftotalAmount) as amount, v.f056ffromGlcode as glcode, g.f014faccountId as accountId')
 //            ->from('Application\Entity\T056fverimentMaster','v')
	// 	   	->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'v.f056ffromGlcode = g.f014fid')
	// 	   	->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fid = g.f014faccountId')
 //            ->where('v.f056ffromDepartment = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('v.f056fidFinancialYear = :financialyearId')
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();

	// 	$virement_master_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	// 	$qb = $em->createQueryBuilder();

 //        $qb->select('v.f056fidFinancialYear as financialYear, v.f056ftoDepartment as department,SUM(v.f056famount) as amount, v.f056ftoGlcode as glcode, g.f014faccountId as accountId')
 //            ->from('Application\Entity\T056fverimentDetails','v')
	// 	   	->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'v.f056ftoGlcode = g.f014fid')
	// 	   	->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fid = g.f014faccountId')
 //            ->where('v.f056ftoDepartment = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('v.f056fidFinancialYear = :financialyearId')
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();
		
	// 	$virement_details_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

	// 	$qb = $em->createQueryBuilder();

 //        $qb->select('i.f055fidFinancialYear as financialYear, i.f055fdepartment as department,SUM(id.f055famount) as amount, id.f055fidGlcode as glcode, g.f014faccountId as accountId')
 //            ->from('Application\Entity\T055fincrementMaster','i')
 //            ->leftjoin('Application\Entity\T055fincrementDetails', 'id', 'with','i.f055fid = id.f055fidMaster')
	// 	   	->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'id.f055fidGlcode = g.f014fid')
	// 	   	->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fid = g.f014faccountId')
 //            ->where('i.f055fdepartment = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('i.f055fidFinancialYear = :financialyearId')
 //            ->andwhere("i.f055fadjustmentType = 'Increment'")
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();

	// 	$increment_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

	// 	$qb = $em->createQueryBuilder();

 //        $qb->select('i.f055fidFinancialYear as financialYear, i.f055fdepartment as department,SUM(id.f055famount) as amount, id.f055fidGlcode as glcode, g.f014faccountId as accountId')
 //            ->from('Application\Entity\T055fincrementMaster','i')
 //            ->leftjoin('Application\Entity\T055fincrementDetails', 'id', 'with','i.f055fid = id.f055fidMaster')
	// 	   	->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'id.f055fidGlcode = g.f014fid')
	// 	   	->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fid = g.f014faccountId')
 //            ->where('i.f055fdepartment = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('i.f055fidFinancialYear = :financialyearId')
 //            ->andwhere("i.f055fadjustmentType = 'Decrement'")
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();

	// 	$decrement_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


	// 	$qb = $em->createQueryBuilder();
 
 //        $qb->select('j.f017fidFinancialYear as financialYear, g.f014fdepartmentId as department,SUM(jd.f054fdrAmount) as amount, jd.f054fidGlcode as glcode, g.f014faccountId as accountId')
 //            ->from('Application\Entity\T017fjournal','j')
	// 	   	->leftjoin('Application\Entity\T054factivityDetails', 'jd', 'with', 'jd.f054fidJournal = j.f017fid')
	// 	   	->leftjoin('Application\Entity\T054factivityDetails', 'ac', 'with', 'ac.f054fidGlcode = jd.f054fidGlcode')
	// 	   	->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'jd.f054fidGlcode = g.f014fid')
	// 	   	->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fid = g.f014faccountId')
 //            ->where('g.f014fdepartmentId = :departmentId')
 //            ->setParameter('departmentId',$department)
 //            ->andwhere('j.f017fidFinancialYear = :financialyearId')
	// 		->setParameter('financialyearId',$idFinancialyear)
	// 		->groupby('g.f014faccountId,g.f014fdepartmentId');
			
	// 		// echo $qb;
	// 		// die();

 //        $query = $qb->getQuery();
		
	// 	$journal_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	
	// 	$final_result = array();
	// 	// print_r($activity_result);
	// 	// print_r($virement_master_result);
	// 	// print_r($virement_details_result);
	// 	foreach($activity_result as $activity){
	// 		$activity['virement_add'] = 0;
	// 		$activity['virement_reduce'] = 0;
	// 		$activity['amount_commited'] = 0;
	// 		$activity['amount_expense'] = 0;

	// 		foreach($virement_master_result as $virement){
	// 			if($activity['glcode'] == $virement['glcode']){
	// 				$activity['virement_add'] = $virement['amount'];
	// 			}
	// 		}
	// 		foreach($virement_details_result as $virement){
	// 			if($activity['glcode'] == $virement['glcode']){
	// 				$activity['virement_reduce'] = $virement['amount'];
	// 			}
	// 		}
	// 		foreach($journal_result as $journal){
	// 			if($activity['glcode'] == $journal['glcode']){
	// 				$activity['amount_expense'] = $journal['amount'];
	// 			}
	// 		}
	// 		foreach($increment_result as $increment){
	// 			if($activity['glcode'] == $increment['glcode']){
	// 				$activity['increment_sum'] = $increment['amount'];
	// 			}
	// 		}
	// 		foreach($decrement_result as $decrement){
	// 			if($activity['glcode'] == $decrement['glcode']){
	// 				$activity['decrement_sum'] = $decrement['amount'];
	// 			}
	// 		} 
	// 		$activity['actual_budget'] = (float)($activity['amount'] + $activity['virement_add'] - $activity['virement_reduce']);
	// 		$activity['current_balance'] = (float)($activity['actual_budget'] - $activity['amount_expense']);
	// 		$activity['accountName'] = $this->getAccountName($activity['accountId']);
	// 		$activity['completeCode'] = $this->getCompleteCode($activity['accountId']);

	// 		array_push($final_result,$activity);
	// 	}
		
       
        
 //        return $final_result;
 //    }

	// public function getAccountName($id){
 //        $em = $this->getEntityManager();

	// 	$query2 = "select f059fname from t059faccount_code where f059fid=$id";
	// 	$result = $em->getConnection()->executeQuery($query2)->fetch();
	// 	return $result['f059fname'];
	// }
	// public function getCompleteCode($id){
 //        $em = $this->getEntityManager();

	// 	$query2 = "select f059fcomplete_code from t059faccount_code where f059fid=$id";
	// 	$result = $em->getConnection()->executeQuery($query2)->fetch();
	// 	return $result['f059fcomplete_code'];
	// }
	public function accountJournals($accountId)
    {
		
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('jd.f054fid, jd.f054faccountCode, jd.f054ffundCode,jd.f054factivityCode,jd.f054faccountCode , j.f017fidFinancialYear, jd.f054fsoCode, jd.f054fdrAmount, jd.f054fcrAmount, jd.f054fstatus,j.f017fid, j.f017fdateOfTransaction, j.f017fdescription, jd.f054freferenceNumber, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount,j.f017fapprovedStatus,u.f014fuserName,a.f059fcompleteCode,a.f059fname')
            ->from('Application\Entity\T017fjournal','j')
            ->leftjoin('Application\Entity\T018factivityDetails','jd', 'with', 'j.f017fid = jd.f018fidJournal')
            ->leftjoin('Application\Entity\T059faccountCode','a', 'with', 'a.f059fcompleteCode = jd.f054faccountCode')
             ->leftjoin('Application\Entity\T014fuser','u', 'with', 'j.f017fapprover = u.f014fid');
           $qb ->where('jd.f054faccountCode = :accountId')
            ->setParameter('accountId',$accountId);
			$query = $qb->getQuery();
			$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
			$result1 = array();
			foreach ($result as $journal) {
				$date                        = $journal['f017fdateOfTransaction'];
				$journal['f017fdateOfTransaction'] = date("Y-m-d", strtotime($date));
				array_push($result1, $journal);
			}
        
        return $result1;
    }

	public function budgetFileImport($name)
    {
        $em = $this->getEntityManager();
		$file = file('/var/www/html/fims-zend/public/files/'.$name);
		// print_r($file);
		// die();
		$sql = "truncate table budget_costcenter_dump";
		$em->getConnection()->executeQuery($sql);
		for($i=1;$i<count($file);$i++) {
			$row = explode(",",$file[$i]);
			$financial_year = trim($row[0], '"');
			$cost_center = trim($row[1], '"');
			$fund_code = trim($row[2], '"');
			$account_code = trim($row[3], '"');
			$activity_code = trim($row[4], '"');
			$department_code = trim($row[5], '"');
			$amount = trim($row[6], '"');
			$q1 = trim($row[7], '"');
			$q2 = trim($row[8], '"');
			$q3 = trim($row[9], '"');
			$q4 = trim($row[10], '"');
			$insert_query = "insert into budget_costcenter_dump(budget_year,cost_center,fund_code,account_code,activity_code,department_code,amount,q1,q2,q3,q4) values ('$financial_year','$cost_center','$fund_code','$account_code','$activity_code','$department_code','$amount','$q1','$q2','$q3','$q4')";
			// echo $insert_query."\n";
			$em->getConnection()->executeQuery($insert_query);
			// '$financial_year','$cost_center','$fund_code','$account_code','$activity_code','$department_code','$amount','$q1','$q2','$q3','$q4'
		}


		$em = $this->getEntityManager();
        $workingfine = 0;
        // $query = "select q1,q2,q3,q4,amount from budget_costcenter_dump"; 
        // $sum1_result = $em->getConnection()->executeQuery($query)->fetchAll();
        // foreach($sum1_result as $row)
        // {
        //     if((int)$row['q1']+(int)$row['q2']+(int)$row['q3']+(int)$row['q4'] != (int)$row['amount'])
        //     {
        //         $workingfine = 1;
        //         break;
        //     }
        // }
        // if($workingfine == 0)
        // {
        // $query = "select sum(cast(amount as float)) as total_amount,budget_year,cost_center from budget_costcenter_dump group by budget_year,cost_center"; 
        // $sum_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        // foreach($sum_result as $row) {
        //      $budgetYear = $row['budget_year'];
        //      $departmentCode = $row['cost_center'];
        
        //      $query = "select f051fdepartment,f051famount,f110fyear from t051fbudget_costcenter inner join t110fbudgetyear on f110fid = f051fid_financialyear where f110fyear='$budgetYear' and f051fdepartment = '$departmentCode'";
        //     $costcenter_result = $em->getConnection()->executeQuery($query)->fetch();
        
        
        //      if($costcenter_result['f051famount'] == $row['total_amount']) {
        //          $workingfine = 0;
        //      } else {
        //          $workingfine = 1;
                 
        //          break;
        //      }
        
        //  }
        // }
         if($workingfine == 1) {
             print("please import proper xls there is mismath in amounts allocated ");
         }
         else{
            
            $query = 'select sum(cast(amount as float)) as total_amount,cost_center,budget_year from budget_costcenter_dump inner join t110fbudgetyear on f110fyear = budget_year group by budget_year,cost_center,f110fid';
        $sum_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        for($i=0;$i<count($sum_result);$i++){
            $budgetYear = $sum_result[$i]['budget_year'];
            $costCenter = $sum_result[$i]['cost_center'];
            $query = "select amount,fund_code,account_code,activity_code,department_code,q1,q2,q3,q4 from budget_costcenter_dump where budget_year = '$budgetYear' and cost_center = '$costCenter'";
        $details_result = $em->getConnection()->executeQuery($query)->fetchAll();

        $query = "select f110fid from t110fbudgetyear where f110fyear = '$budgetYear'";
        $fn_result = $em->getConnection()->executeQuery($query)->fetch();
        $sum_result[$i]['f110fid'] = $fn_result['f110fid'];
            $sum_result[$i]['activity-details'] = $details_result;
    }
        //   print_r($sum_result);  
        //   die();
            foreach($sum_result as $data){
                $activity = new T054factivityMaster();

                $activity->setF054fdepartment($data['cost_center'])
                               ->setF054fidFinancialyear((int)$data['f110fid'])
                               ->setF054ftotalAmount((float)$data['total_amount'])
                               ->setF054fbalance((float)$data['total_amount'])
                               ->setF054fapprovalStatus((int)"1")
                               ->setF054fstatus((int)"1")
                               ->setF054fcreatedBy((int)$_SESSION['userId'])
                                ->setF054fupdatedBy((int)$_SESSION['userId']);
        
                $activity->setF054fcreatedDtTm(new \DateTime())
                               ->setF054fupdatedDtTm(new \DateTime());
                
                $em->persist($activity);
                $em->flush();
                
                $activityDetails = $data['activity-details'];
        
                foreach ($activityDetails as $activityDetail) {
        
                    $activityDetailObj = new T054factivityDetails();
        
                    $activityDetailObj->setF054fidActivityMaster((int)$activity->getF054fid())
                               ->setF054ffundCode( $activityDetail['fund_code'])
                                ->setF054faccountCode( $activityDetail['account_code'])
                                ->setF054factivityCode( $activityDetail['activity_code'])
                                ->setF054fdepartmentCode( $activityDetail['department_code'])
                                ->setF054famount((float)$activityDetail['amount'])
                               ->setF054fq1((float)$activityDetail['q1'])
                               ->setF054fq2((float)$activityDetail['q2'])
                               ->setF054fq3((float)$activityDetail['q3'])
                               ->setF054fq4((float)$activityDetail['q4'])
                               ->setF054fstatus((int)$activityDetail['f054fstatus'])
                               ->setF054fcreatedBy((int)$_SESSION['userId'])
                                ->setF054fupdatedBy((int)$_SESSION['userId']);
        
                    $activityDetailObj->setF054fcreatedDtTm(new \DateTime())
                               ->setF054fupdatedDtTm(new \DateTime());
        
                
        
                $em->persist($activityDetailObj);
        
                $em->flush();
        
                    
                }
                
            }
         } 
    }
    
    public function deleteBudgetActivityDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t054factivity_details WHERE f054fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }


    public function budgetAvtivityImportdataasd()
    {
        $em = $this->getEntityManager();

        
            $query2 = "select * from t054factivity_details";
            $res = $em->getConnection()->executeQuery($query2)->fetchAll();


            foreach ($res as $key)
            {
            	$dep = $key['f054fdepartment_code'];

            	$query = "select * from t054factivity_master where f054fdepartment = '$dep'";
            $reres2s = $em->getConnection()->executeQuery($query)->fetch();


            




            $masterkeyid = (int)$reres2s['f054fid'];


            $query = "update t054factivity_details set f054fid_activity_master = $masterkeyid where f054fdepartment_code = '$dep' ";
            $em->getConnection()->executeQuery($query);


            }
        return 1;
    }
    
   
}
