<?php
namespace Application\Repository;

use Application\Entity\T076fpremise;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PremiseRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("p.f076fid, p.f076fname, p.f076faddress1, p.f076faddress2, d.f013fcountryName as countryName, s.f012fstateName as stateName, p.f076fcountry, p.f076fstate, p.f076fcity, p.f076fzip, p.f076fstatus, p.f076fidPremiseType, pt.f037fpremiseType, (pt.f037fpremiseType+'-'+p.f076fname) as PremiseName, p.f076fpremiseCode")
            ->from('Application\Entity\T076fpremise', 'p')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'p.f076fcountry = d.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'p.f076fstate = s.f012fid')
            ->leftjoin('Application\Entity\T037fpremiseTypeSetup','pt','with', 'p.f076fidPremiseType = pt.f037fid')
            ->orderBy('p.f076fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("p.f076fid, p.f076fname, p.f076faddress1, p.f076faddress2, d.f013fcountryName as countryName, s.f012fstateName as stateName, p.f076fcountry, p.f076fstate, p.f076fcity, p.f076fzip, p.f076fstatus, p.f076fidPremiseType, pt.f037fpremiseType, p.f076fpremiseCode, (p.f076fpremiseCode+'-'+p.f076fname) as PremiseName ")
            ->from('Application\Entity\T076fpremise', 'p')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'p.f076fcountry = d.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'p.f076fstate = s.f012fid')
            ->leftjoin('Application\Entity\T037fpremiseTypeSetup','pt','with', 'p.f076fidPremiseType = pt.f037fid')
            ->where('p.f076fid = :premiseId')
            ->setParameter('premiseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $revenueId = (int)$data['f076fidPremiseType'];

        $query = "select * from t037fpremise_type_setup where f037fid = $revenueId"; 
            $revenueResult=$em->getConnection()->executeQuery($query)->fetch();

            // print_r($revenueResult['f037fcode']);exit;

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type'] = "Premise";
        $numberData['premise'] = $revenueResult['f037fcode'];
        $numberData['revenueId'] = $revenueId;
        $number = $configRepository->generateFIMS($numberData);
        // print_r($number);exit();
        
        $premise = new T076fpremise();

        $premise->setF076fname($data['f076fname'])
                ->setF076faddress1($data['f076faddress1'])
                ->setF076faddress2($data['f076faddress2'])
                ->setF076fcountry((int)$data['f076fcountry'])
                ->setF076fstate((int)$data['f076fstate'])
                ->setF076fidPremiseType((int)$data['f076fidPremiseType'])
                ->setF076fcity($data['f076fcity'])
                ->setF076fzip((int)$data['f076fzip'])
                ->setF076fpremiseCode($number)
                ->setF076fstatus((int)$data['f076fstatus'])
                ->setF076fcreatedBy((int)$_SESSION['userId'])
                ->setF076fupdatedBy((int)$_SESSION['userId']);

        $premise->setF076fcreatedDtTm(new \DateTime())
                ->setF076fupdatedDtTm(new \DateTime());


        try{
            $em->persist($premise);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $premise;
    }

    public function updateData($premise, $data = []) 
    {
        $em = $this->getEntityManager();

        $premise->setF076fname($data['f076fname'])
                ->setF076faddress1($data['f076faddress1'])
                ->setF076faddress2($data['f076faddress2'])
                ->setF076fcountry((int)$data['f076fcountry'])
                ->setF076fidPremiseType((int)$data['f076fidPremiseType'])
                ->setF076fstate((int)$data['f076fstate'])
                ->setF076fcity($data['f076fcity'])
                ->setF076fzip((int)$data['f076fzip'])
                ->setF076fstatus((int)$data['f076fstatus'])
                ->setF076fupdatedBy((int)$_SESSION['userId']);

        $premise->setF076fupdatedDtTm(new \DateTime());


        
        $em->persist($premise);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("p.f076fid, p.f076fname, p.f076faddress1, p.f076faddress2, d.f013fcountryName as countryName, s.f012fstateName as stateName, p.f076fcountry, p.f076fstate, p.f076fcity, p.f076fzip, p.f076fstatus, (p.f076fname+'-'+p.f076fcity) as Premise, p.f076fpremiseCode, (p.f076fpremiseCode+'-'+p.f076fname) as premiseName")
            ->from('Application\Entity\T076fpremise', 'p')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'p.f076fcountry = d.f013fid')
            ->leftjoin('Application\Entity\T037fpremiseTypeSetup','ps','with', 'ps.f037fid = p.f076fidPremiseType')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'p.f076fstate = s.f012fid')
            ->orderBy('p.f076fid','DESC')
            ->orderBy('s.f012fstateName','ASC')
            ->where('p.f076fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

   

}