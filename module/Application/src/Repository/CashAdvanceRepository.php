<?php
namespace Application\Repository;

use Application\Entity\T116fcashAdvance;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CashAdvanceRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f116fid, c.f116fstaffType, c.f116fstatus')
            ->from('Application\Entity\T116fcashAdvance','c')
            ->orderBy('c.f116fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f116fid, c.f116fstaffType, c.f116fstatus')
            ->from('Application\Entity\T116fcashAdvance','c')
            ->where('c.f116fid = :cashId')
            ->setParameter('cashId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // print_r("sd");exit;

        $cashAdvance = new T116fcashAdvance();

        $cashAdvance->setF116fstaffType($data['f116fstaffType'])
             ->setF116fstatus((int)$data['f116fstatus'])
             ->setF116fcreatedBy((int)$_SESSION['userId'])
              ->setF116fupdatedBy((int)$_SESSION['userId']);

        $cashAdvance->setF116fcreatedDtTm(new \DateTime());
               
        try
        {
        $em->persist($cashAdvance);
        
        $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    /* to edit the data in database*/

    public function updateData($cashAdvance, $data = []) 
    {
        $em = $this->getEntityManager();

        $cashAdvance->setF116fstaffType($data['f116fstaffType'])
             ->setF116fstatus((int)$data['f116fstatus'])
             ->setF116fupdatedBy((int)$_SESSION['userId']);

        $cashAdvance->setF116fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($cashAdvance);
        
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

                $qb->select('c.f116fid, c.f116fstaffType, c.f116fstatus')
            ->from('Application\Entity\T116fcashAdvance','c')
            ->where('c.f116fstatus = 1')
            ->orderBy('c.f116fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}

?>