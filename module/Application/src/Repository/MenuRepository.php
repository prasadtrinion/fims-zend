<?php
namespace Application\Repository;

use Application\Entity\T011fmenu;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class MenuRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();
        
        // Query
    
        $qb = $em->createQueryBuilder();

        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a');
           

        $query = $qb->getQuery();


        $result= array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;

        
    }
    
    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->where('a.f011fid = :menuId')
           ->setParameter('menuId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $menu = new T011fmenu();

        $menu->setF011fmenuName($data['f011fmenuName'])
            ->setF011fmodule($data['f011fmodule'])
             ->setF011fidParent((int)$data['f011fidParent'])  
             ->setF011forder((int)$data['f011forder'])
             ->setF011fstatus((int)$data['f011fstatus'])
             ->setF011flink($data['f011flink'])
             ->setF011ftype($data['f011ftype'])
             ->setF011fcreatedBy((int)$_SESSION['userId'])
             ->setF011fupdatedBy((int)$_SESSION['userId']);

        $menu->setF011fcreateDtTm(new \DateTime())
                ->setF011fupdateDtTm(new \DateTime());

 
        $em->persist($menu);
        $em->flush();

        return $menu;

    }

    /* to edit the data in database*/

    public function updateData($menu, $data = []) 
    {
        $em = $this->getEntityManager();

        $menu->setF011fmenuName($data['f011fmenuName'])
            ->setF011fmodule($data['f011fmodule'])
             ->setF011fidParent((int)$data['f011fidParent'])  
             ->setF011forder((int)$data['f011forder'])
             ->setF011fstatus((int)$data['f011fstatus'])
             ->setF011flink($data['f011flink'])
             ->setF011ftype($data['f011ftype'])
             ->setF011fcreatedBy((int)$_SESSION['userId'])
             ->setF011fupdatedBy((int)$_SESSION['userId']);
                

        $menu->setF011fUpdateDtTm(new \DateTime());
        
        $em->persist($menu);
        $em->flush();

    }

    public function getParentId() 
    {
        $em = $this->getEntityManager();
    

        $qb->select('b.f011fid as f011fid,b.f011fmenuName as f011fmenuName,a.f011fmodule,a.f011fid as Parent_id, a.f011fmenuName as f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->leftjoin('Application\Entity\T011fmenu', 'b','with', 'a.f011fid=b.f011fidParent');

        $query = $qb->getQuery();


        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;

        
    }
    public function getModule() 
    {
        $em = $this->getEntityManager();
        
        // Query
    
        $qb = $em->createQueryBuilder();

        $qb->select('a.f011fmodule, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->groupBy('a.f011fmodule');
           

        $query = $qb->getQuery();


        $result= array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;

        
    }

    public function getMenuList($module) 
    {
        $em = $this->getEntityManager();

        
        $qb = $em->createQueryBuilder();
        
        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->where('a.f011fmodule = :menuList')
           ->setParameter('menuList',$module)
           ->orderBy('a.f011forder');

           

        $query = $qb->getQuery();


        $result= array(
            // 'module'=>$data['f011fmodule'],
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;

        
    }




    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }





    public function getGroupMenu($module) 
    {
        $em = $this->getEntityManager();
// print_r($module);exit;
        
        $qb = $em->createQueryBuilder();
        
        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->where('a.f011fmodule = :menuList')
           // ->andwhere('a.f011ftype = 1')
           ->setParameter('menuList',$module)
           ->orderBy('a.f011forder');

        $query = $qb->getQuery();
        $setup_result=  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


// print_r($setup_result);exit;
        $qb = $em->createQueryBuilder();
        
        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->where('a.f011fmodule = :menuList')
           ->andwhere('a.f011ftype = 2')
           ->setParameter('menuList',$module)
           ->orderBy('a.f011forder');

           

        $query = $qb->getQuery();


        $transaction_result=  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();
        
        $qb->select('a.f011fid, a.f011fmenuName, a.f011fmodule, a.f011fidParent, a.f011forder, a.f011fstatus, a.f011flink, a.f011ftype')
           ->from('Application\Entity\T011fmenu', 'a')
           ->where('a.f011fmodule = :menuList')
           ->andwhere('a.f011ftype = 3')
           ->setParameter('menuList',$module)
           ->orderBy('a.f011forder');

           

        $query = $qb->getQuery();


        $report_result=  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $return_result = array();
        $return_result['setup'] = $setup_result; 
        $return_result['transaction'] = $transaction_result; 
        $return_result['reports'] = $report_result; 
        
        return $return_result;

        
    }
    
}

?>