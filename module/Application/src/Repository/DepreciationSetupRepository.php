<?php
namespace Application\Repository;

use Application\Entity\T015fdepreciationSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DepreciationSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        // print_r("123456");exit;


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("d.f015fid, d.f015fdepAccountCode, d.f015fapAccountCode, d.f015fstatus, d.f015fdepreciationCode, d.f015fdepreciationDescription, d.f015flifeSpan, d.f015fdepreciationRate, (d.f015fdepreciationCode+'-'+d.f015fdepreciationDescription) as depreciationCode, (d.f015fdepAccountCode+'-'+aco.f059fname) as depAccountName, (d.f015fdepAccountCode+'-'+aco1.f059fname) as apAccountName")
            ->from('Application\Entity\T015fdepreciationSetup','d')
            ->leftjoin('Application\Entity\T059faccountCode', 'aco', 'with', 'd.f015fdepAccountCode = aco.f059fcompleteCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'aco1', 'with', 'd.f015fapAccountCode = aco1.f059fcompleteCode')
            ->orderBy('d.f015fid','DESC');
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select("d.f015fid, d.f015fdepAccountCode, d.f015fapAccountCode, d.f015fstatus, d.f015fdepreciationCode, d.f015fdepreciationDescription, d.f015flifeSpan, d.f015fdepreciationRate, (d.f015fdepreciationCode+'-'+d.f015fdepreciationDescription) as depreciationCode")
            ->from('Application\Entity\T015fdepreciationSetup','d')
            ->where('d.f015fid = :departmentId')
            ->setParameter('departmentId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }
    
    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        // $query1 = "delete from t015fdepreciation_setup"; 
        //     $em->getConnection()->executeQuery($query1);            
        
            $department = new T015fdepreciationSetup();

            $department->setF015fdepAccountCode($data['f015fdepAccountCode'])
                ->setF015fapAccountCode($data['f015fapAccountCode'])
                // ->setF015faccountCode($data['f015faccountCode'])
                ->setF015fdepreciationCode($data['f015fdepreciationCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fdepreciationDescription($data['f015fdepreciationDescription'])
                ->setF015flifeSpan($data['f015flifeSpan'])
                ->setF015fdepreciationRate($data['f015fdepreciationRate'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

            $department->setF015fupdatedDtTm(new \DateTime());
// setF015fdepAccountCode($f015fdepAccountCode)
// setF015fapAccountCode($f015fapAccountCode)
// setF015fupdatedDtTm($f015fupdatedDtTm)
// setF015fstatus($f015fstatus)
// setF015fupdatedBy($f015fupdatedBy)
// setF015fdepreciationCode($f015fdepreciationCode)


            try
            {
                $em->persist($department);
            // print_r($department);exit;

                $em->flush();
            }
            catch(\Exception $e)
            {
                echo $e;
            }
        return $department;
    }

    public function updateData($depriciationObj, $data = []) 
    {
        $em = $this->getEntityManager();

       $depriciationObj->setF015fdepAccountCode($data['f015fdepAccountCode'])
                ->setF015fapAccountCode($data['f015fapAccountCode'])
                // ->setF015faccountCode($data['f015faccountCode'])
                ->setF015fdepreciationCode($data['f015fdepreciationCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fdepreciationDescription($data['f015fdepreciationDescription'])
                ->setF015flifeSpan($data['f015flifeSpan'])
                ->setF015fdepreciationRate($data['f015fdepreciationRate'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

            $depriciationObj->setF015fupdatedDtTm(new \DateTime());
        
        $em->persist($depriciationObj);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015fid, d.f015fdepAccountCode, d.f015fapAccountCode, d.f015faccountCode, d.f015fstatus')
            ->from('Application\Entity\T015fdepreciationSetup','d')
            ->where('d.f015fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function deleteDepreciationSetup($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "delete from t015fdepreciation_setup WHERE f015fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

}

?>