<?php
namespace Application\Repository;

use Application\Entity\T026flegalInformation;
use Application\Entity\T026flegalDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LegalInformationRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('l.f026fid, l.f026fname, l.f026fphone, l.f026femail, l.f026ffax, l.f026address, l.f026fstatus')
            ->from('Application\Entity\T026flegalInformation','l')
            ->orderBy('l.f026fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('l.f026fid, l.f026fname, l.f026fphone, l.f026femail, l.f026ffax, l.f026address, l.f026fstatus, ld.f026fidDetails, ld.f026fidStudent, ld.f026fdescription, s.f060fname as studentName')
            ->from('Application\Entity\T026flegalInformation','l')
            ->leftJoin('Application\Entity\T026flegalDetails', 'ld', Expr\Join::WITH,'l.f026fid = ld.f026fidLegal')
            ->leftJoin('Application\Entity\T060fstudent', 's', Expr\Join::WITH,'ld.f026fidStudent = s.f060fid')
            ->where('l.f026fid = :legalId')
            ->setParameter('legalId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    
    public function createLegal($data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $legal = new T026flegalInformation();

        $legal->setF026fname($data['f026fname'])
             ->setF026fphone($data['f026fphone'])
             ->setF026femail($data['f026femail'])
             ->setF026ffax($data['f026ffax'])
             ->setF026address($data['f026address'])
             // ->setF026fidStudent((int)$data['f026fidStudent'])
             ->setF026fstatus((int)$data['f026fstatus'])
             ->setF026fcreatedBy((int)$_SESSION['userId'])
             ->setF026fupdatedBy((int)$_SESSION['userId']);

        $legal->setF026fcreatedDtTm(new \DateTime())
                ->setF026fupdatedDtTm(new \DateTime());
        
        $em->persist($legal);
        $em->flush();
        
        $legalDetails = $data['legal-details'];

        foreach ($legalDetails as $legalDetail)
        {

            $legalDetailObj = new T026flegalDetails();

            $legalDetailObj->setF026fidLegal($legal->getF026fid())
                       ->setF026fidStudent((int)$legalDetail['f026fidStudent'])
                       ->setF026fdescription($legalDetail['f026fdescription'])
                       ->setF026fstatus((int)$legalDetail['f026fstatus'])
                       ->setF026fcreatedBy((int)$_SESSION['userId'])
                       ->setF026fupdatedBy((int)$_SESSION['userId']);

            $legalDetailObj->setF026fcreatedDtTm(new \DateTime())
                       ->setF026fupdatedDtTm(new \DateTime());
        $em->persist($legalDetailObj);

        $em->flush();            
        }
        return $legal;
    }
   public function updateLegal($legal, $data = []) 
    {
        $em = $this->getEntityManager();

        $legal->setF026fname($data['f026fname'])
             ->setF026fphone($data['f026fphone'])
             ->setF026femail($data['f026femail'])
             ->setF026ffax($data['f026ffax'])
             ->setF026address($data['f026address'])
             // ->setF026fidStudent((int)$data['f026fidStudent'])
             ->setF026fstatus((int)$data['f026fstatus'])
             ->setF026fupdatedBy((int)$_SESSION['userId']);
                

        $legal->setF026fupdatedDtTm(new \DateTime());
        
        $em->persist($legal);
        $em->flush();

        return $legal;
    }


    public function updateLegalDetail($legalDetailObj, $legalDetail)
    {
        
        $em = $this->getEntityManager();

        $legalDetailObj->setF026fidStudent((int)$legalDetail['f026fidStudent'])
                       ->setF026fdescription($legalDetail['f026fdescription'])
                       ->setF026fstatus((int)$legalDetail['f026fstatus'])
                       ->setF026fupdatedBy((int)$_SESSION['userId']);

        $legalDetailObj->setF026fupdatedDtTm(new \DateTime());

        $em->persist($legalDetailObj);
        $em->flush();   
    }
    public function createLegalDetail($legalObj, $legalDetail)
    {
        $em = $this->getEntityManager();

        $legalDetailObj = new T026flegalDetails();

        $legalDetailObj->setF026fidLegal($legalObj->getF026fid())
                       ->setF026fidStudent((int)$legalDetail['f026fidStudent'])
                       ->setF026fdescription($legalDetail['f026fdescription'])
                       ->setF026fstatus((int)$legalDetail['f026fstatus'])
                       ->setF026fcreatedBy((int)$_SESSION['userId'])
                       ->setF026fupdatedBy((int)$_SESSION['userId']);

        $legalDetailObj->setF026fcreatedDtTm(new \DateTime())
                       ->setF026fupdatedDtTm(new \DateTime());

        $em->persist($legalDetailObj);
        $em->flush();
        return $legalDetailObj;   
    }
    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f026fid, l.f026fname, l.f026fphone, l.f026femail, l.f026ffax, l.f026address, l.f026fidStudent, l.f026fstatus')
            ->from('Application\Entity\T026flegalInformation','l')
            ->where('l.f026fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;        
    }
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function deleteLegalInformationDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t026flegal_details WHERE f026fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }    
}

?>