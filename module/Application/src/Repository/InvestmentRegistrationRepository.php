<?php
namespace Application\Repository;

use Application\Entity\T064finvestmentRegistration;
use Application\Entity\T147finvestmentRegistrationWithdraw;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;


class InvestmentRegistrationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

   public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode,r.f064fisWithdraw, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType, r.f064ffinalStatus')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->orderBy('r.f064fid','DESC');
          // ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'r.f064ffundCode = f.f057fid')
          //   ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'r.f064factivityCode = at.f058fid')
          //   ->leftjoin('Application\Entity\T015fdepartment', 'dp', 'with', 'r.f064fdepartmentCode = dp.f015fid')
          //   ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'r.f064faccountCode = ac.f059fid');

        // $qb->select('r.f064fid, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064fidGlcode,  g.f014fglCode as glcode, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundId, r.f064factivityId,  r.f064fdepartmentId, r.f064faccountId')
        //   ->from('Application\Entity\T064finvestmentRegistration','r')
        //   ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
        //   ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
        //   ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
        //   ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
        //   ->leftjoin('Application\Entity\T014fglcode', 'g','with','r.f064fidGlcode = g.f014fid');

          // ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid');
          // ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'r.f064ffundId = f.f057fid')
          //   ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'r.f064factivityId = at.f058fid')
          //   ->leftjoin('Application\Entity\T015fdepartment', 'dp', 'with', 'r.f064fdepartmentId = dp.f015fid')
          //   ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'r.f064faccountId = ac.f059fid')
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('r.f064fid, r.f064fisOnline,r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064ffile, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          // ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'r.f064ffundCode = f.f057fid')
          //   ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'r.f064factivityCode = at.f058fid')
          //   ->leftjoin('Application\Entity\T015fdepartment', 'dp', 'with', 'r.f064fdepartmentCode = dp.f015fid')
          //   ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'r.f064faccountCode = ac.f059fid')
             ->where('r.f064fid = :registrationId')
            ->setParameter('registrationId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f064fmaturityDate'];
            $item['f064fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f064feffectiveDate'];
            $item['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        return $result;
    }

    public function createRegistartion($data) 
    {
        
       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        

        $registration = new T064finvestmentRegistration();

        $registration->setf064freferenceNumber($data['f064freferenceNumber'])
                 ->setF064fname($data['f064fname'])
                 ->setF064fsum((float)$data['f064fsum'])
                 ->setF064fidApplication((int)$data['f064fidApplication'])
                 ->setF064frateOfInterest((float)$data['f064frateOfInterest'])
                 ->setF064fcontactPerson($data['f064fcontactPerson'])
                 ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                 ->setF064fmaturityDate(new \DateTime($data['f064fmaturityDate']))
                 ->setF064fuumBank((int)$data['f064fuumBank'])
                 ->setF064finvestmentBank((int)$data['f064finvestmentBank'])
                 ->setF064finterstToBank((float)$data['f064finterstToBank'])
                 ->setF064finterestDay((int)$data['f064finterestDay'])
                 ->setF064finvestmentStatus((int)$data['f064finvestmentStatus'])
                 ->setF064fpreviousStatus((int)$data['f064fpreviousStatus'])
                 ->setF064fpreviousId((int)$data['f064fpreviousId'])
                 ->setF064fcontactPerson2($data['f064fcontactPerson2'])
                 ->setF064fcontactEmail($data['f064fcontactEmail'])
                 ->setF064fcontactEmail2($data['f064fcontactEmail2'])
                 ->setF064ffundCode($data['f064ffundCode'])
                 ->setF064fisOnline((int) 0)
                 ->setF064fdepartmentCode($data['f064fdepartmentCode'])
                 ->setF064factivityCode($data['f064factivityCode'])
                 ->setF064faccountCode($data['f064faccountCode'])
                 ->setF064fapproved1Status((int)0)
                 ->setF064fapproved2Status((int)0)
                 ->setF064fapproved3Status((int)0)
                 ->setF064fapproved1By((int)1)
                 ->setF064fapproved2By((int)2)
                 ->setF064fapproved3By((int)3)
                 ->setF064fapproved1UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved2UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved3UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fisWithdraw((int) 0)
                 ->setF064freason1('NULL')
                 ->setF064freason2('NULL')
                 ->setF064freason3('NULL')
                 ->setF064freason('NULL')
                 ->setF064ffile($data['f064ffile'])
                 ->setF064fdurationType($data['f064fdurationType'])
                 ->setF064fprofitAmount((float)$data['f064fprofitAmount'])
                 ->setF064fperiodType((int)$data['f064fperiodType'])
                 ->setF064fstatus((int)$data['f064fstatus'])
                 ->setF064fcreatedBy((int)$_SESSION['userId'])
                 ->setF064fupdatedBy((int)$_SESSION['userId']);

        $registration->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());

 // try{
        $em->persist($registration);
        $em->flush();
        
        // print_r($registration);
        // die();
 // }
 // catch(\Exception $e){
 //  // $e->getErrorCode();
 //  // print_r($e);
 //  // exit();
 //    if($e->getErrorCode()==20018)
 //    {
 //      return new JsonModel([
 //                            'status' => 200,
 //                            'message' => 'This User Already in block List',
 //                        ]);
 //    }
  
     
 // }
       
        return $registration;
    }

    public function updateRegistration($registration, $data = [])
    {
        $em = $this->getEntityManager();

        $registration->setf064freferenceNumber($data['f064freferenceNumber'])
                 ->setF064fname($data['f064fname'])
                 ->setF064fsum((float)$data['f064fsum'])
                 ->setF064fidApplication((int)$data['f064fidApplication'])
                 ->setF064frateOfInterest((float)$data['f064frateOfInterest'])
                 ->setF064fcontactPerson($data['f064fcontactPerson'])
                 ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                 ->setF064fmaturityDate(new \DateTime($data['f064fmaturityDate']))
                 ->setF064fuumBank((int)$data['f064fuumBank'])
                 ->setF064finvestmentBank((int)$data['f064finvestmentBank'])
                 ->setF064finterstToBank((float)$data['f064finterstToBank'])
                 ->setF064finterestDay((int)$data['f064finterestDay'])
                 ->setF064finvestmentStatus((int)$data['f064finvestmentStatus'])
                 ->setF064fpreviousStatus((int)$data['f064fpreviousStatus'])
                 ->setF064fpreviousId((int)$data['f064fpreviousId'])
                 ->setF064fcontactPerson2($data['f064fcontactPerson2'])
                 ->setF064fcontactEmail($data['f064fcontactEmail'])
                 ->setF064fcontactEmail2($data['f064fcontactEmail2'])
                 ->setF064ffundCode($data['f064ffundCode'])
                 ->setF064fisOnline((int) "0")
                 ->setF064fdepartmentCode($data['f064fdepartmentCode'])
                ->setF064factivityCode($data['f064factivityCode'])
                ->setF064faccountCode($data['f064faccountCode'])
                 ->setF064fprofitAmount((float)$data['f064fprofitAmount'])
                 ->setF064fdurationType($data['f064fdurationType'])
                 ->setF064fperiodType((int)$data['f064fperiodType'])
                 ->setF064fstatus((int)$data['f064fstatus'])
                 ->setF064fupdatedBy((int)$_SESSION['userId']);

        $registration->setF064fupdatedDtTm(new \DateTime());

      try
      { 
        $em->persist($registration);
        $em->flush();
      }
      catch(\Exception $e)
      {
          echo $e;
      }

        return $registration;
       
    }

    public function updateStatus($registrations,$status)
    {
        $em = $this->getEntityManager();

        foreach ($registrations as $registration) 
        {

            $id = $registration->getF064fid();
            
           $query = "UPDATE t064finvestment_registration set f064finvestment_status='$status' where f064fid = '$id'"; 
            $result=$em->getConnection()->executeQuery($query);
            
        }
        return $result;
    }

    public function getListByApprovedOneStatus($approvedStatus)
    {
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();
        
    // echo("hi");
    // exit;
           if($approvedStatus == 0)
           {
               $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank,  r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode,r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fapproved1Status = 0');
          
            }

            elseif($approvedStatus == 1)
            {
                $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode,r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fapproved1Status = 1');
          
            }

            elseif($approvedStatus == 2) 
            {
                $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid');
            }
          
    $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    $result1 = array();

    foreach ($data as $item) {
            $date                        = $item['f064fmaturityDate'];
            $item['f064fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f064feffectiveDate'];
            $item['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
    
    return $data;
    
  }
  

    public function approveAll($registrations) 
    {
        $em = $this->getEntityManager();

        foreach ($registrations as $registration)
        {

            $registration->setF064fapproved1Status((int)"1")
                        ->setF064fapproved1By((int)"1");

            $registration->setF064fapproved2Status((int)"1")
                        ->setF064fapproved2By((int)"1");

            $registration->setF064fapproved3Status((int)"1")
                        ->setF064fapproved3By((int)"1");
       
            $em->persist($registration);
            $em->flush();
        }
    }

    public function registrationApproval($postData, $journalRepository) 
    {
        
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();
        $registrations=$postData['id'];

        
        foreach ($registrations as $registration) 
        {
        
        $level=(int)$postData['level'];
        $status=(int)$postData['status'];
        $reason=$postData['reason'];
        $user=(int)$_SESSION['userId'];

            switch ($level) 
            {
                case '1':
                    $query = "update t147finvestment_registration_withdraw set f064fapproved1_status = $status, f064fapproved1_updated_by=$user, f064freason1='$reason', f064fapproved1_by=$level where f064fid = $registration";
                    $result =$em->getConnection()->executeQuery($query);

                    $query1 = "select * from t147finvestment_registration_withdraw where f064fid = $registration and (f064fapproved1_status = 2 or f064fapproved2_status = 2 or f064fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();


                    $queryAllApprove = "select * from t147finvestment_registration_withdraw where f064fid = $registration and f064fapproved1_status = 1 and f064fapproved2_status = 1 and f064fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {

                      $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 2, f064ffinal_reason = '$reason' where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 1 where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    break;

                case '2':
                    $query = "update t147finvestment_registration_withdraw set f064fapproved2_status = $status, f064fapproved2_updated_by=$user, f064freason2='$reason', f064fapproved2_by=$level where f064fid = $registration";
                    $result =$em->getConnection()->executeQuery($query);

                    $query1 = "select * from t147finvestment_registration_withdraw where f064fid = $registration and (f064fapproved1_status = 2 or f064fapproved2_status = 2 or f064fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();

                    $queryAllApprove = "select * from t147finvestment_registration_withdraw where f064fid = $registration and f064fapproved1_status = 1 and f064fapproved2_status = 1 and f064fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {
                      $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 2, f064ffinal_reason = '$reason' where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 1 where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    break;

                case '3':
                    $query = "update t147finvestment_registration_withdraw set f064fapproved3_status = $status, f064fapproved3_updated_by=$user, f064freason3='$reason', f064fapproved3_by=$level, f064fis_online=1 where f064fid = $registration";
                    $result =$em->getConnection()->executeQuery($query);

                    $query1 = "select * from t147finvestment_registration_withdraw where f064fid = $registration and (f064fapproved1_status = 2 or f064fapproved2_status = 2 or f064fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();

                    $queryAllApprove = "select * from t147finvestment_registration_withdraw where f064fid = $registration and f064fapproved1_status = 1 and f064fapproved2_status = 1 and f064fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {
                      $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 2, f064ffinal_reason = '$reason' where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t147finvestment_registration_withdraw set f064ffinal_status = 1 where f064fid = $registration";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }
                    break;
            }

             if($status == 1)
              {
                    $query1 = "select * from t147finvestment_registration_withdraw where f064fid = $registration and f064ffinal_status = 1";
                    $result =$em->getConnection()->executeQuery($query1)->fetch();

                  if ($result)
                  {
                    $user_query = "select f014fid_staff from t014fuser where f014fid=".$user;
            $user_result = $em->getConnection()->executeQuery($user_query)->fetch();

            $approver = $user_result['f014fid_staff'];
                    $em = $this->getEntityManager();

                    $qb = $em->createQueryBuilder();
                    
                    $qb->select('f.f015fid, f.f015fname')
                      ->from('Application\Entity\T015ffinancialyear','f')
                      ->where('f.f015fstatus=1');
                    $query = $qb->getQuery();
                    $year = $query->getSingleResult();

                    $financialYear = $year['f015fid'];

                    
                    $data['f017fdescription'] = "Investment Withdrawl";
                    $data['f017fmodule'] = "Investment";
                    $data['f017fapprover'] = $approver;
                    $data['f017ftotalAmount'] = $result['f064fsum'];
                    $data['f017fidFinancialYear'] = $financialYear;
                    $data['f017fapprovedStatus'] = "1";
                    $data['f017fdateOfTransaction'] = $result['f064fmaturity_date'];
                    $data['f017freferenceNumber'] = $result['f064freference_number'];
 
                    $details = array();

                     $detail['f018faccountCode'] =  $result['f064faccount_code'];
                     $detail['f018ffundCode'] =  $result['f064ffund_code'];
                     $detail['f018factivityCode'] =  $result['f064factivity_code'];
                     $detail['f018fdepartmentCode'] =  $result['f064fdepartment_code'];
                     $detail['f018fsoCode'] = "NULL";
                     $detail['f018freferenceNumber'] = $result['f064freference_number'];
                     $detail['f018fdrAmount'] = $result['f064fsum'];
                     $detail['f018fcrAmount'] = 0;
                     $detail['f018fcreated_by'] = $result['f064fcreated_by'];
                     $detail['f018fcreated_dt_tm'] = $result['f064fcreated_dt_tm'];
                     array_push($details,$detail);

                     $data['journal-details'] = $details;

                    
                        $journalRepository->createNewData($data);
                     }
              }
        }
    }

    

    public function getListApproved() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fapproved1Status=1')
          ->andWhere('r.f064fapproved2Status=1')
          ->andWhere('r.f064fapproved3Status=1');
         

        
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();

    foreach ($result as $item) {
            $date                        = $item['f064fmaturityDate'];
            $item['f064fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f064feffectiveDate'];
            $item['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        
        return $result;
        
    }

    public function createApproval($data) 
    {
       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $registration = new T064finvestmentRegistration();

        $registration->setf064freferenceNumber($data['f064freferenceNumber'])
                 ->setF064fname($data['f064fname'])
                 ->setF064fsum((float)$data['f064fsum'])
                 ->setF064fidApplication((int)$data['f064fidApplication'])
                 ->setF064frateOfInterest((float)$data['f064frateOfInterest'])
                 ->setF064fcontactPerson($data['f064fcontactPerson'])
                 ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                 ->setF064fmaturityDate(new \DateTime($data['f064fmaturityDate']))
                 ->setF064fuumBank((int)$data['f064fuumBank'])
                 ->setF064finvestmentBank((int)$data['f064finvestmentBank'])
                 ->setF064finterstToBank((int)$data['f064finterstToBank'])
                 ->setF064finterestDay((int)$data['f064finterestDay'])
                 ->setF064finvestmentStatus((int)$data['f064finvestmentStatus'])
                 ->setF064fpreviousStatus((int)$data['f064fpreviousStatus'])
                 ->setF064fpreviousId((int)$data['f064fpreviousId'])
                 ->setF064fcontactPerson2($data['f064fcontactPerson2'])
                 ->setF064fcontactEmail($data['f064fcontactEmail'])
                 ->setF064fcontactEmail2($data['f064fcontactEmail2'])
                 ->setF064fakrulCode($data['f064fakrulCode'])
                 ->setF064fapproved1Status((int)"1")
                 ->setF064fapproved2Status((int)"1")
                 ->setF064fapproved3Status((int)"1")
                 ->setF064ffinalStatus((int)0)
                 ->setF064fprofitAmount((float)$data['f064fprofitAmount'])
                 ->setF064fperiodType((int)$data['f064fperiodType'])
                 ->setF064fdurationType($data['f064fdurationType'])
                 ->setF064fstatus((int)$data['f064fstatus'])
                 ->setF064fcreatedBy((int)$_SESSION['userId'])
                 ->setF064fupdatedBy((int)$_SESSION['userId']);

        $registration->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());

 try{
        $em->persist($registration);
        $em->flush();
 }
 catch(\Exception $e){
     echo $e;
 }
       
        return $registration;
    }

    public function getInvestmentRegistrationApprovalList($level,$status)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T147finvestmentRegistrationWithdraw','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->orderBy('r.f064fid','DESC');

            
    // echo $level;
    // echo $status;
    // die();
        switch ($level) 
        {
            case 1:
                $qb->where('r.f064fapproved1By = :level1')
                   ->setParameter('level1',$level)
                   ->andWhere('r.f064fapproved1Status = :status1')
                   ->andWhere('r.f064ffinalStatus != 2')
                   ->setParameter('status1',$status)
                   ->andWhere('r.f064fisWithdraw=1');
                break;
            case 2:
                $qb->andWhere('r.f064fapproved2By = :level2')
                   ->setParameter('level2',$level)
                   ->andWhere('r.f064fapproved2Status = :status2')
                   ->andWhere('r.f064fapproved1Status = :status1')
                   ->setParameter('status2',$status)
                   ->andWhere('r.f064fapproved1Status = 1');
                break;
            case 3:
                $qb->where('r.f064fapproved2Status=1')
                   ->andWhere('r.f064fapproved3By = :level3')
                   ->setParameter('level3',$level)
                   ->andWhere('r.f064ffinalStatus != 2')
                   ->setParameter('status3',$status);
                break;
            case 4:
                $qb->where('r.f064fapproved1Status=1')
                   ->andWhere('r.f064ffinalStatus != 2')
                   ->andWhere('r.f064fapproved2Status=1')
                   ->andWhere('r.f064fapproved3Status=1');
                break;
        }
        

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    // public function reinvestment() 
    // {
    //     $em = $this->getEntityManager();

    //     $qb = $em->createQueryBuilder();

    //     // Query
    //    $qb->select('r.f064fid,r.f064fisOnline, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus')
    //       ->from('Application\Entity\T064finvestmentRegistration','r')
    //       ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid');
          


    //     $query = $qb->getQuery();

    //     $results = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        
    //    foreach($results as $result)
    //    {
        
    //      if($result['investmentStatus']!='Active')
    //      {
    //         $em = $this->getEntityManager();

    //         $qb = $em->createQueryBuilder();

    //     // Query
    //         $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode')
    //       ->from('Application\Entity\T064finvestmentRegistration','r')
    //       ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
    //       ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
    //       ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
    //       ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
    //       ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
    //       // ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'r.f064ffundCode = f.f057fid')
    //       //   ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'r.f064factivityCode = at.f058fid')
    //       //   ->leftjoin('Application\Entity\T015fdepartment', 'dp', 'with', 'r.f064fdepartmentCode = dp.f015fid')
    //       //   ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'r.f064faccountCode = ac.f059fid')
    //       // // ->leftjoin('Application\Entity\T061finvestmentInstitution', 'it','with','r.f063finstitutionId= it.f061fid')
    //       ->where('r.f064fapproved1Status=1')
    //       ->andWhere('r.f064fapproved2Status=1')
    //       ->andWhere('r.f064fapproved3Status=1');
         
    //      $query = $qb->getQuery();

    //     $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    //     $result1 = array();

    // foreach ($result as $item) {
    //         $date                        = $item['f064fmaturityDate'];
    //         $item['f064fmaturityDate'] = date("Y-m-d", strtotime($date));

    //         $date                        = $item['f064feffectiveDate'];
    //         $item['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
    //         array_push($result1, $item);
    //     }


        
    //     return $result;
    //      }
    //    }
       
    // }

    public function reinvestment($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $registration = new T064finvestmentRegistration();

        $registration->setf064freferenceNumber($data['f064freferenceNumber'])
                 ->setF064fname($data['f064fname'])
                 ->setF064fsum((float)$data['f064fsum'])
                 ->setF064fidApplication((int)$data['f064fidApplication'])
                 ->setF064frateOfInterest((float)$data['f064frateOfInterest'])
                 ->setF064fcontactPerson($data['f064fcontactPerson'])
                 ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                 ->setF064fmaturityDate(new \DateTime($data['f064fmaturityDate']))
                 ->setF064fuumBank((int)$data['f064fuumBank'])
                 ->setF064finvestmentBank((int)$data['f064finvestmentBank'])
                 ->setF064finterstToBank((float)$data['f064finterstToBank'])
                 ->setF064finterestDay((int)$data['f064finterestDay'])
                 ->setF064finvestmentStatus((int)$data['f064finvestmentStatus'])
                 ->setF064fpreviousStatus((int)$data['f064fpreviousStatus'])
                 ->setF064fpreviousId((int)$data['f064fpreviousId'])
                 ->setF064fcontactPerson2($data['f064fcontactPerson2'])
                 ->setF064fcontactEmail($data['f064fcontactEmail'])
                 ->setF064fcontactEmail2($data['f064fcontactEmail2'])
                 ->setF064ffundCode($data['f064ffundCode'])
                 ->setF064fisOnline((int) 1)
                 ->setF064fdepartmentCode($data['f064fdepartmentCode'])
                 ->setF064factivityCode($data['f064factivityCode'])
                 ->setF064faccountCode($data['f064faccountCode'])
                 ->setF064fapproved1Status((int)0)
                 ->setF064fapproved2Status((int)0)
                 ->setF064fapproved3Status((int)0)
                 ->setF064fapproved1By((int)1)
                 ->setF064fapproved2By((int)2)
                 ->setF064fapproved3By((int)3)
                 ->setF064fapproved1UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved2UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved3UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fisWithdraw((int) 0)
                 ->setF064freason1('NULL')
                 ->setF064freason2('NULL')
                 ->setF064freason3('NULL')
                 ->setF064freason('NULL')
                 ->setF064ffile($data['f064ffile'])
                 ->setF064fprofitAmount((float)$data['f064fprofitAmount'])
                 ->setF064fperiodType((int)$data['f064fperiodType'])
                 ->setF064fdurationType($data['f064fdurationType'])
                 ->setF064fstatus((int)$data['f064fstatus'])
                 ->setF064fcreatedBy((int)$_SESSION['userId'])
                 ->setF064fupdatedBy((int)$_SESSION['userId']);

        $registration->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());

 // try{
        $em->persist($registration);
        $em->flush();
      
       
        return $registration;
    }





        // Query
      
       
    

    public function updateWithdrawlStatus($registrations,$status,$reason)
    {
        $em = $this->getEntityManager();

        foreach ($registrations as $registration) 
        {

            $Id = $registration->getF064fid();
            $id=(int)$Id;
            
           $query = "UPDATE t064finvestment_registration set f064fis_withdraw=$status, f064freason='$reason' where f064fid = $id"; 

          
            $result=$em->getConnection()->executeQuery($query);


            if ($status == 1)
            {

          $qb = $em->createQueryBuilder();
          $qb->select('r.f064fid, r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, r.f064finvestmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064ffile, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->where('r.f064fid = :registrationId')
          ->setParameter('registrationId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $data = $result[0];


        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="FDW";
        $number = $configRepository->generateFIMS($numberData);


        $registration = new T147finvestmentRegistrationWithdraw();

        $registration->setf064freferenceNumber($number)
                 ->setF064fname($data['f064fname'])
                 ->setF064fsum((float)$data['f064fsum'])
                 ->setF064fidApplication((int)$data['f064fidApplication'])
                 ->setF064frateOfInterest((float)$data['f064frateOfInterest'])
                 ->setF064fcontactPerson($data['f064fcontactPerson'])
                 ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                 ->setF064fmaturityDate(new \DateTime($data['f064fmaturityDate']))
                 ->setF064fuumBank((int)$data['f064fuumBank'])
                 ->setF064finvestmentBank((int)$data['f064finvestmentBank'])
                 ->setF064finterstToBank((float)$data['f064finterstToBank'])
                 ->setF064finterestDay((int)$data['f064finterestDay'])
                 ->setF064finvestmentStatus((int)$data['f064finvestmentStatus'])
                 ->setF064fpreviousStatus((int)$data['f064fpreviousStatus'])
                 ->setF064fpreviousId((int)$data['f064fpreviousId'])
                 ->setF064fcontactPerson2($data['f064fcontactPerson2'])
                 ->setF064fcontactEmail($data['f064fcontactEmail'])
                 ->setF064fcontactEmail2($data['f064fcontactEmail2'])
                 ->setF064ffundCode($data['f064ffundCode'])
                 ->setF064fisOnline((int) 1)
                 ->setF064fdepartmentCode($data['f064fdepartmentCode'])
                 ->setF064factivityCode($data['f064factivityCode'])
                 ->setF064faccountCode($data['f064faccountCode'])
                 ->setF064fapproved1Status((int)0)
                 ->setF064fapproved2Status((int)0)
                 ->setF064fapproved3Status((int)0)
                 ->setF064fapproved1By((int)1)
                 ->setF064fapproved2By((int)2)
                 ->setF064fapproved3By((int)3)
                 ->setF064fapproved1UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved2UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fapproved3UpdatedBy((int)$_SESSION['userId'])
                 ->setF064fisWithdraw((int)1)
                 ->setF064freason1('NULL')
                 ->setF064freason2('NULL')
                 ->setF064freason3('NULL')
                 ->setF064freason('NULL')
                 ->setF064ffile($data['f064ffile'])
                 ->setF064fprofitAmount((float)$data['f064fprofitAmount'])
                 ->setF064fperiodType((int)$data['f064fperiodType'])
                 ->setF064fdurationType($data['f064fdurationType'])
                 ->setF064fidRegistration((int)$data['f064fid'])
                 ->setF064fstatus((int)$data['f064fstatus'])
                 ->setF064fcreatedBy((int)$_SESSION['userId'])
                 ->setF064fupdatedBy((int)$_SESSION['userId']);

        $registration->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());

 // try{
        $em->persist($registration);
        $em->flush();
              
            }
            
        }
        return $result;
    }

    public function getRegistrationWithdrawlList($status)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       if($status == 0)
           {
               $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank,  r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType, r.f064fapproved1Status, r.f064fapproved2Status, r.f064fapproved3Status, r.f064ffinalStatus')
          ->from('Application\Entity\T147finvestmentRegistrationWithdraw','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fisWithdraw = 0')
          ->orderBy('r.f064fid','DESC');
          
          }
           elseif($status == 1)
           {
               $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank,  r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode,r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType, r.f064fapproved1Status, r.f064fapproved2Status, r.f064fapproved3Status, r.f064ffinalStatus')
          ->from('Application\Entity\T147finvestmentRegistrationWithdraw','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fisWithdraw = 1')
          ->orWhere('r.f064ffinalStatus = 2')
          ->orderBy('r.f064fid','DESC');
          
          }
          elseif($status == 2)
           {
               $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank,  r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode,r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType, r.f064fapproved1Status, r.f064fapproved2Status, r.f064fapproved3Status, r.f064ffinalStatus')
          ->from('Application\Entity\T147finvestmentRegistrationWithdraw','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fisWithdraw = 2')
          ->orderBy('r.f064fid','DESC');
          
          }
        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f064fmaturityDate'];
            $item['f064fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f064feffectiveDate'];
            $item['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        
        return $result;
        
    }

    public function getWithdrawList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $currentDate = date('Y-m-d H:m:s');


        // Query
       $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType, irw.f064ffinalStatus')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T147finvestmentRegistrationWithdraw', 'irw','with','irw.f064fidRegistration = r.f064fid')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fstatus=1')
          ->andWhere('r.f064fisWithdraw = 0')
          ->orWhere('irw.f064ffinalStatus = 2')
          ->orderBy('r.f064freferenceNumber','ASC');
          
        
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $withdraw) {
            $effective_date                        = $withdraw['f064feffectiveDate'];
            $withdraw['f064feffectiveDate'] = date("Y-m-d", strtotime($effective_date));
  
            $maturity_date                       = $withdraw['f064fmaturityDate'];
            $withdraw['f064fmaturityDate'] = date("Y-m-d", strtotime($maturity_date));

            if($maturity_date > $currentDate)
            {
              array_push($result1, $withdraw);
            }
      
        }
       
        $result = array(
            'data' => $result1,
        );
        
        return $result;
        
    }

    public function getRegistrationList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $currentDate = date('Y-m-d H:m:s');

        // Query
       $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->where('r.f064fstatus=1')
          ->andWhere('r.f064fisWithdraw=0');

          // $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank,  r.f064finvestmentBank,  r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus,  r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode')
          // ->from('Application\Entity\T064finvestmentRegistration','r')
          // ->where('r.f064fstatus=1')
          // ->andWhere('r.f064fisWithdraw=0');
          
        
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $withdraw) {
            $effective_date                        = $withdraw['f064feffectiveDate'];
            $withdraw['f064feffectiveDate'] = date("Y-m-d", strtotime($effective_date));
  
            $maturity_date                       = $withdraw['f064fmaturityDate'];
            $withdraw['f064fmaturityDate'] = date("Y-m-d", strtotime($maturity_date));

  
            if($maturity_date < $currentDate)
            {
              array_push($result1, $withdraw);
            }
      
      
        }
       
        $result = array(
            'data' => $result1,
        );
        
        return $result;
        
    }
    public function getReinvestmentList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('r.f064fid,r.f064fisOnline, r.f064freferenceNumber, r.f064fname, r.f064fsum, r.f064fidApplication, a.f063fdescription as application, r.f064frateOfInterest, r.f064fcontactPerson, r.f064feffectiveDate, r.f064fmaturityDate, r.f064fstatus, r.f064fuumBank, c.f042fbankName as uumBank, r.f064finvestmentBank, b.f042fbankName as investmentBank, r.f064finterstToBank, r.f064finterestDay, r.f064finvestmentStatus, d.f011fdefinitionCode as investmentStatus, r.f064fpreviousId, r.f064fpreviousStatus, r.f064fcontactPerson2, r.f064fcontactEmail, r.f064fcontactEmail2, r.f064ffundCode, r.f064factivityCode,  r.f064fdepartmentCode, r.f064faccountCode,r.f064fisWithdraw, ad.f063fduration, ad.f063fdurationType, r.f064fprofitAmount, r.f064fperiodType, r.f064fdurationType')
          ->from('Application\Entity\T064finvestmentRegistration','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'a','with','r.f064fidApplication= a.f063fid')
          ->leftjoin('Application\Entity\T063fapplicationDetails', 'ad','with','ad.f063fidMaster= a.f063fid')
          ->leftjoin('Application\Entity\T041fbankSetupAccount', 'u','with','r.f064fuumBank= u.f041fid')
          ->leftjoin('Application\Entity\T042fbank', 'c','with','u.f041fidBank = c.f042fid')
          ->leftjoin('Application\Entity\T042fbank', 'b','with','r.f064finvestmentBank = b.f042fid')
          ->leftjoin('Application\Entity\T011fdefinationms','d','with','r.f064finvestmentStatus = d.f011fid')
          ->orderBy('r.f064fid','DESC')
          ->where('r.f064fisOnline=1');
       
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        
        return $result;
        
    }

    public function investmentRegistrationBankIdCheck($postData)
    {
        $applicationId = (int)$postData['applicationId'];
        // $uumBank = (int)$postData['uumBank'];
        $bank = (int)$postData['bank'];

        $em = $this->getEntityManager();

        $query= "SELECT ir.f064fid, ir.f064freference_number, ir.f064fid_application from t064finvestment_registration ir where ir.f064fid_application = $applicationId and ir.f064finvestment_bank = $bank ";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();

        if($application)
        {
            $message = "Investment Already Registered.";
            return $message;
        }

        return 1;
    }

    public function investmentRegistrationDetails()
    {
       $em = $this->getEntityManager();

        // Slect from master where masterid not in (select fo36fidMaster from detailstabol where f063fapproved1_status=1)

         $query= "select distinct(ir.f064fid) as f064fid, ir.f064freference_number, ir.f064fuum_bank, ir.f064fsum, ir.f064fcontact_person, ir.f064fis_withdraw, ir.f064fapproved1_status, ir.f064fapproved2_status, ir.f064fapproved3_status from t064finvestment_registration as ir  where ir.f064fapproved1_status = 0 and ir.f064fis_withdraw = 1 order by ir.f064fid desc";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();

        $application = array(
          "data"=> $application);
        return $application;
    }

    public function investmentRegistrationDetailsForApprovals()
    {
       $em = $this->getEntityManager();

        // Slect from master where masterid not in (select fo36fidMaster from detailstabol where f063fapproved1_status=1)


         $query= "select distinct(ir.f064fid) as f064fid, ir.f064freference_number, ir.f064fuum_bank, ir.f064fsum, ir.f064fcontact_person, ir.f064fis_withdraw, ir.f064fapproved1_status, ir.f064fapproved2_status, ir.f064fapproved3_status, ir.f064freason1, ir.f064freason2, ir.f064freason3, d.f011fdefinition_code as investmentStatus, am.f063fdescription as application from t147finvestment_registration_withdraw as ir left join t011fdefinationms d on d.f011fid = ir.f064finvestment_status left join t063fapplication_master am on am.f063fid = ir.f064fid_application where ir.f064fapproved1_status = 1 and (ir.f064fapproved2_status = 0 or ir.f064fapproved3_status = 0) and ir.f064ffinal_status != 2 order by ir.f064fid desc";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();

        $application = array(
          "data"=> $application);
        return $application;
    }

    public function investmentRegistrationAllApprovals()
    {
       $em = $this->getEntityManager();

        // Slect from master where masterid not in (select fo36fidMaster from detailstabol where f063fapproved1_status=1)

         $query= "select distinct(ir.f064fid) as f064fid, ir.f064freference_number, ir.f064fuum_bank, ir.f064fsum, ir.f064fcontact_person, ir.f064fis_withdraw, ir.f064fapproved1_status, ir.f064fapproved2_status, ir.f064fapproved3_status from t064finvestment_registration as ir  where ir.f064fapproved1_status = 1 and ir.f064fapproved2_status = 1 and ir.f064fapproved3_status = 1 and f064fis_withdraw = 1 order by ir.f064fid desc";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();

        $application = array(
          "data"=> $application);
        return $application;
    }

}
?>