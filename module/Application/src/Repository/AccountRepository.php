<?php
namespace Application\Repository;

use Application\Entity\Account;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AccountRepository extends EntityRepository {

	/**
	 * Function to create the user account
	 * @param array $data
	 *
	 */
	public function createAccount($data) {
		$em = $this->getEntityManager();
		$token = md5(uniqid(time()));

		$accountEntity = new Account();
		$accountEntity->setUsername($data['email'])
			->setVerificationToken($token)
			->setLastLoggedInDtTm(new \DateTime())
			->setCreateDtTm(new \DateTime());

		if (isset($data['password']) && !empty($data['password'])) {
			$accountEntity->setPassword($data['password']);
		}

		if (isset($data['status']) && !empty($data['status'])) {
			$accountEntity->setStatus($data['status']);
		}

		$em->persist($accountEntity);
		$em->flush();

		return $accountEntity;
	}

	/**
	 * Function to check the username exist
	 * @param string $username
	 * return boolean
	 */
	public function isAccountExist($userName) {
		$em = $this->getEntityManager();
		$conn = $em->getConnection();
		$query = "Select username from account where username = :username";

		$stmt = $conn->prepare($query);
		$stmt->bindValue(':username', $userName);
		$stmt->execute();

		$userName = $stmt->fetchColumn();

		//echo $userName;exit;
		if ($userName) {
			return true;
		}
		return false;
	}

	/**
	 * Function to check the username exist
	 * @param string $username
	 * return boolean
	 */
	public function isTokenExist($token) {
		$em = $this->getEntityManager();
		$conn = $em->getConnection();
		$query = "Select username from account where sso_token = :token";

		$stmt = $conn->prepare($query);
		$stmt->bindValue(':token', $token);
		$stmt->execute();

		$userName = $stmt->fetchColumn();

		//echo $userName;exit;
		if ($userName) {
			return true;
		}
		return false;
	}

	/**
	 * Function to get user account by username
	 * @param string $username
	 * return object
	 */
	public function getAccountByUsername($username = null) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('a')
			->from('Application\Entity\Account', 'a')
			->where('a.username = :username')
			->andWhere('a.deleteFlag = :deleteFlag')
			->andWhere('a.status != :status')
			->setParameter('username', $username)
			->setParameter('deleteFlag', 0)
			->setParameter('status',-1);

		$query = $qb->getQuery();

		return $query->getOneOrNullResult();
	}

	/**
	 * Function to update user account
	 * @param object $entity
	 */
	public function update($entity) {
		$em = $this->getEntityManager();
		$em->persist($entity);
		$em->flush();
	}

	/**
	 * Function to get user account by token
	 * @param string $token
	 * @return object
	 */
	public function getAccountByToken($token = null) {
		if ($token) {
			$em = $this->getEntityManager();
			$qb = $em->createQueryBuilder();
			$qb->select('a')
				->from('Application\Entity\Account', 'a')
				->where('a.resetToken = :reset_token')
				->setParameter('reset_token', $token);
			$query = $qb->getQuery();
			return $query->getOneOrNullResult();
		}
	}

	/**
	 * Function to get user account by id
	 * @param integer $accountId
	 *
	 */
	public function getUsersByAccountId($accountId) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('a.id as accountId, a.lastLoggedInDtTm as lastLoginTime,a.emailStatus,a.status,u.id as userId,a.username,u.name,u.phoneNumber,r.role,r.id as roleID')
			->from('Application\Entity\User', 'u')
			->leftjoin('u.account', 'a')
			->leftjoin('Application\Entity\UserHasRole', 'ur', 'with', 'u.id = ur.userId')
			->leftjoin('Application\Entity\Role', 'r', 'with', 'ur.roleId = r.id')
			->where('u.account = :accountId')
			->setParameter('accountId', $accountId);
		$query = $qb->getQuery();
		return $query->getArrayResult();
	}

	
	public function getAllUsers() {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('a.id as accountId, u.id as userId,a.username,u.name,u.phoneNumber,u.email,r.role,r.id as roleID')
			->from('Application\Entity\User', 'u')
			->leftjoin('u.account', 'a')
			->leftjoin('Application\Entity\UserHasRole', 'ur', 'with', 'u.id = ur.userId')
			->leftjoin('Application\Entity\Role', 'r', 'with', 'ur.roleId = r.id')
			->where('a.deleteFlag = :deleteFlag')
			->andWhere('u.deleteFlag = :deleteFlag')
			->setParameter('deleteFlag', 0);
		
		$query = $qb->getQuery();
		
		return $query->getArrayResult();
	}
}
