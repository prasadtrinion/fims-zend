<?php
namespace Application\Repository;

use Application\Entity\T089fassetSubCategories;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetSubCategoriesRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f089fid, c.f089fidCategory, ac.f087fcategoryCode as Category, ac.f087fcategoryDiscription, c.f089fsubcode, c.f089fsubDescription, c.f089fidItemGroup, c.f089faccountNumber, c.f089flifeExpectancy, c.f089fstatus, sc.f028fsubCategoryName as subCategory')
            ->from('Application\Entity\T089fassetSubCategories','c')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with','c.f089fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'sc', 'with','c.f089fidItemGroup = sc.f028fid')
            ->orderBy('c.f089fsubcode','ASC')
            ->orderBy('c.f089faccountNumber','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f089fid, c.f089fidCategory, ac.f087fcategoryCode as Category, c.f089fsubcode, c.f089fsubDescription, c.f089fidItemGroup, c.f089faccountNumber, c.f089flifeExpectancy, c.f089fstatus, sc.f028fsubCategoryName as subCategory')
            ->from('Application\Entity\T089fassetSubCategories','c')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with','c.f089fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'sc', 'with','c.f089fidItemGroup = sc.f028fid')
             ->where('c.f089fid = :subCategoryId')
            ->setParameter('subCategoryId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= 'ASC';
        $number = $configRepository->generateFIMS($numberData);

        $subAsset = new T089fassetSubCategories();

        $subAsset->setF089fidCategory((int)$data['f089fidCategory'])
             ->setF089fsubcode($data['f089fsubcode'])
             ->setF089fsubDescription($data['f089fsubDescription'])
             ->setF089faccountNumber((int)$number)
             ->setF089fidItemGroup((int)$data['f089fidItemGroup'])
             ->setF089flifeExpectancy((int)$data['f089flifeExpectancy'])
             ->setF089fstatus((int)$data['f089fstatus'])
             ->setF089fcreatedBy((int)$_SESSION['userId'])
             ->setF089fupdatedBy((int)$_SESSION['userId']);

        $subAsset->setF089fcreatedDtTm(new \DateTime())
                ->setF089fupdatedDtTm(new \DateTime());

 
        $em->persist($subAsset);
        $em->flush();

        return $subAsset;

    }

    public function updateData($subAsset, $data = []) 
    {
        $em = $this->getEntityManager();

        $subAsset->setF089fidCategory((int)$data['f089fidCategory'])
             ->setF089fsubcode($data['f089fsubcode'])
             ->setF089fsubDescription($data['f089fsubDescription'])
             ->setF089faccountNumber((int)$data['f089faccountNumber'])
             ->setF089fidItemGroup((int)$data['f089fidItemGroup'])
             ->setF089flifeExpectancy((int)$data['f089flifeExpectancy'])
             ->setF089fstatus((int)$data['f089fstatus'])
             ->setF089fupdatedBy((int)$_SESSION['userId']);

        $subAsset->setF089fupdatedDtTm(new \DateTime());
        
        $em->persist($subAsset);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('c.f089fid, c.f089fidCategory, ac.f087fcategoryCode as Category, c.f089fsubcode, c.f089fsubDescription, c.f089fidItemGroup, c.f089faccountNumber, c.f089flifeExpectancy, c.f089fstatus, sc.f028fsubCategoryName as subCategory')
            ->from('Application\Entity\T089fassetSubCategories','c')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with','c.f089fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'sc', 'with','c.f089fidItemGroup = sc.f028fid')
            ->orderBy('c.f089fid','DESC')
            ->where('c.f089fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getAssetSubCategoryByAssetCategory($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("c.f089fid, c.f089fidCategory, ac.f087fcategoryCode as Category, c.f089fsubcode, c.f089fsubDescription, c.f089fidItemGroup, c.f089faccountNumber, c.f089flifeExpectancy, c.f089fstatus, c.f089fid as subCategoryId, (c.f089fsubcode+'-'+c.f089fsubDescription) as subCategoryName")
            ->from('Application\Entity\T089fassetSubCategories','c')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with','c.f089fidCategory = ac.f087fid')
            ->where('c.f089fidCategory = :subCategoryId')
            ->setParameter('subCategoryId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
}