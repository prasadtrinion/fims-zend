<?php
namespace Application\Repository;

use Application\Entity\T091fpayroll;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PayrollRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select('p.f091fid, p.f091fstaffType, s.f034fname as staffName,  p.f091fmonth, p.f091fbank, b.f042fbankName as UUMBank,p.f091fstatus')
            ->from('Application\Entity\T091fpayroll', 'p')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with', 'b.f042fid = p.f091fbank')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'p.f091fstaffType = s.f034fstaffId')
            ->orderBy('p.f091fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('p.f091fid, p.f091fstaffType, s.f034fname as staffName, p.f091fmonth, p.f091fbank, b.f042fbankName as UUMBank,p.f091fstatus')
            ->from('Application\Entity\T091fpayroll', 'p')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with', 'b.f042fid = p.f091fbank')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'p.f091fstaffType = s.f034fstaffId')
            ->where('p.f091fid = :payrollId')
            ->setParameter('payrollId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $payroll = new T091fpayroll();
        $payroll->setF091fstaffType((int)$data['f091fstaffType'])
                ->setF091fmonth($data['f091fmonth'])
                ->setF091fbank((int)$data['f091fbank'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $payroll->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($payroll);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        
        return $payroll;
    }
    public function updateData($payroll, $data = []) 
    {
        $em = $this->getEntityManager();
        $payroll->setF091fstaffType((int)$data['f091fstaffType'])
                ->setF091fmonth($data['f091fmonth'])
                ->setF091fbank((int)$data['f091fbank'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $payroll->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());

        $em->persist($payroll);
        $em->flush();
    }
}
