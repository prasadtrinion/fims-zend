<?php
namespace Application\Repository;

use Application\Entity\T063fpayrollExcemption;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PayrollExcemptionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('e.f063fid, e.f063fcategory, e.f063findividualExpenses, e.f063fspouseExpenses, e.f063fchildExpenses, e.f063fstatus')
            ->from('Application\Entity\T063fpayrollExcemption','e');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('e.f063fid, e.f063fcategory, e.f063findividualExpenses, e.f063fspouseExpenses, e.f063fchildExpenses, e.f063fstatus')
            ->from('Application\Entity\T063fpayrollExcemption','e')
            ->where('e.f063fid = :excemptionId')
            ->setParameter('excemptionId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $excemption = new T063fpayrollExcemption();

        $excemption->setF063fcategory($data['f063fcategory'])
                ->setF063findividualExpenses($data['f063findividualExpenses'])
                ->setF063fspouseExpenses($data['f063fspouseExpenses'])
                ->setF063fchildExpenses($data['f063fchildExpenses'])
                ->setF063fstatus((int)$data['f063fstatus'])
                ->setF063fcreatedBy((int)$_SESSION['userId'])
                ->setF063fupdatedBy((int)$_SESSION['userId']);

        $excemption->setF063fcreatedDtTm(new \DateTime())
                ->setF063fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($excemption);
            $em->flush();
        
        }

        catch(\Exception $e)
        {
            echo $e;
        }
        return $schedule;
    }

    /* to edit the data in database*/

    public function updateData($excemption, $data = []) 
    {
        $em = $this->getEntityManager();
        
         $excemption->setF063fcategory($data['f063fcategory'])
                    ->setF063findividualExpenses($data['f063findividualExpenses'])
                    ->setF063fspouseExpenses($data['f063fspouseExpenses'])
                    ->setF063fchildExpenses($data['f063fchildExpenses'])
                    ->setF063fstatus((int)$data['f063fstatus'])
                    ->setF063fcreatedBy((int)$_SESSION['userId'])
                    ->setF063fupdatedBy((int)$_SESSION['userId']);

        $excemption->setF063fupdatedDtTm(new \DateTime());

 try
 {        $em->persist($excemption);
        $em->flush();

        
}
catch(\Exception $e){
    echo $e;
    }
        return $excemption;

    }


}

?>