<?php
namespace Application\Repository;

use Application\Entity\T133fassetInformation;
use Application\Entity\T085fassetInformation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DuplicateAssetInformationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        // print_r("sdfgh");exit;

        // Query
       $qb->select("a.f133fid, a.f133fassetCode, a.f133fassetDescription, a.f133fidDepartment, a.f133fidAssetCategory, c.f087fcategoryCode as Category, a.f133fAcquisitionDate, a.f133forderNumber, a.f133fstoreCode, a.f133faccountCode, a.f133fapproveDate, a.f133fexpireDate, a.f133fidAssetType, a.f133finstallCost, a.f133faccumDeprCost, a.f133fnetValue, a.f133fyearDeprCost, a.f133fresidualValue, a.f133fstatus,a.f133fassetOwner, a.f133fbarCode, a.f133fserialNumber, a.f133fbrandName, a.f133fcurrentBuilding, a.f133fcurrentRoom, a.f133fcurrentBlock, a.f133fcurrentCampus, a.f133fsubCategory,a.f133fpoNumber, a.f133fgrnNumber, a.f133fsoCode,a.f133freferrer, a.f133fitemName, a.f133fidItem, s.f089fsubcode as subCategory, i.f088fitemCode as itemCode, c.f087fcategoryDiscription, a.f133fcategoryType, a.f133fstaffIncharge")
            ->from('Application\Entity\T133fassetInformation','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f133fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f133fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f133fidItem = i.f088fid')
            ->where('a.f133fstatus != 2')
            ->orderBy('a.f133fid','DESC');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f133fAcquisitionDate'];
            $row['f133fAcquisitionDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
       
          $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f133fcategoryType'];
        $category = (int)$purchase['f133fidAssetCategory'];
        $subCategory = (int)$purchase['f133fsubCategory'];
        $item = (int)$purchase['f133fidItem'];

         $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();


                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
        
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;

        }

        // print_r($result1);exit;
        return $result1;
        // return $result;
        
    }
   
        
    

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("a.f133fid, a.f133fassetCode, a.f133fassetDescription, a.f133fidDepartment, a.f133fidAssetCategory, c.f087fcategoryCode as Category, a.f133fAcquisitionDate, a.f133forderNumber, a.f133fstoreCode, a.f133faccountCode, a.f133fapproveDate, a.f133fexpireDate, a.f133fidAssetType, a.f133finstallCost, a.f133faccumDeprCost, a.f133fnetValue, a.f133fyearDeprCost, a.f133fresidualValue, a.f133fstatus,a.f133fassetOwner, a.f133fbarCode, a.f133fserialNumber, a.f133fbrandName, a.f133fcurrentBuilding, a.f133fcurrentRoom,a.f133fcurrentBlock,a.f133fcurrentCampus,a.f133fsubCategory,a.f133fpoNumber,a.f133fgrnNumber,a.f133fsoCode,a.f133freferrer,a.f133fitemName,a.f133fidItem,s.f089fsubcode as subCategory,i.f088fitemCode as itemCode, c.f087fcategoryDiscription, a.f133fcategoryType, a.f133fstaffIncharge")
            ->from('Application\Entity\T133fassetInformation','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f133fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f133fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f133fidItem = i.f088fid')
            // ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'a.f133fidAssetType = t.f090fid')
            // ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'a.f133faccountCode = ac.f059fcompleteCode')
            ->where('a.f133fid = :assetId')
            ->orderBy('a.f133fid','DESC')
            ->setParameter('assetId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f133fAcquisitionDate'];
            $row['f133fAcquisitionDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }

          $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f133fcategoryType'];
        $category = (int)$purchase['f133fidAssetCategory'];
        $subCategory = (int)$purchase['f133fsubCategory'];
        $item = (int)$purchase['f133fidItem'];


         $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
       
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;

        }

        // print_r($result1);exit;
        return $result1;
        // return $result1;
    }

    public function createAsset($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // $dept = $data['f133fidDepartment'];
        // $subCat = $data['f133fsubCategory'];
        // $Year = date('Y');

        // $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        // $numberData = array();
        // $numberData['type']="Asset";
        // $runningNumber = $configRepository->generateFIMS($numberData);
        // // if($runningNumber<10){
        // //     $runningNumber = '000'.$runningNumber;
        // // }
        // // else if($runningNumber<100){
        // //     $runningNumber = '00'.$runningNumber;
        // // }
        // // else if($runningNumber<1000){
        // //     $runningNumber = '0'.$runningNumber;
        // // }
        // $query  = "select f015fdepartment_code from t015fdepartment where f015fid=$dept";
        // $result = $em->getConnection()->executeQuery($query)->fetch();
        // $deptCode = $result['f015fdepartment_code'];

        // $query  = "select f089fsubcode from t089fasset_sub_categories where f089fid=$subCat";
        // $result = $em->getConnection()->executeQuery($query)->fetch();
        // $subCode = $result['f089fsubcode'];
        
        // $finalCode = 'UUM/'.$dept.'/'.$subCode.$runningNumber.'/'.$Year;

        // print_r($data);exit;
        $asset = new T133fassetInformation();
   
             $asset->setF133fassetDescription($data['f133fassetDescription'])
                // ->setF133fassetCode($data['133fassetCode'])
             ->setF133fidDepartment($data['f133fidDepartment'])
             ->setF133fidAssetCategory((int)$data['f133fidAssetCategory'])
             ->setF133forderNumber($data['f133forderNumber'])
             ->setF133fAcquisitionDate(new \DateTime($data['f133fAcquisitionDate']))
             ->setF133fexpireDate(new \DateTime($data['f133fexpireDate']))
             // ->setF133fstoreCode($data['f133fstoreCode'])
             ->setF133faccountCode($data['f133faccountCode'])
             // ->setF133fidAssetType((int)$data['f133fidAssetType'])
             ->setF133finstallCost((float)$data['f133finstallCost'])
             // ->setF133faccumDeprCost((float)$data['f133faccumDeprCost'])
             ->setF133fnetValue((float)$data['f133fnetValue'])
             ->setF133fyearDeprCost((float)$data['f133fyearDeprCost'])
             // ->setF133fresidualValue((float)$data['f133fresidualValue'])
              ->setF133fassetOwner($data['f133fassetOwner'])
             // ->setF133fbarCode($data['f133fbarCode'])
             ->setF133fserialNumber($data['f133fserialNumber'])
             ->setF133fbrandName($data['f133fbrandName'])
             ->setF133fcurrentBuilding((int)$data['f133fcurrentBuilding'])
             ->setF133fcurrentRoom($data['f133fcurrentRoom'])
             ->setF133fcurrentBlock($data['f133fcurrentBlock'])
             ->setF133fcurrentCampus($data['f133fcurrentCampus'])
             ->setF133fsubCategory((int)$data['f133fsubCategory'])
             ->setF133fpoNumber($data['f133fpoNumber'])
             // ->setF133fgrnNumber((int)$data['f133fgrnNumber'])
             // ->setF133fsoCode($data['f133fsoCode'])
             // ->setF133freferrer($data['f133freferrer'])
             ->setF133fidItem((int)$data['f133fidItem'])
             ->setF133fcategoryType($data['f133fcategoryType'])
             // ->setF133fitemName($data['f133fitemName'])
             ->setF133fstaffIncharge((int)$data['f133fstaffIncharge'])
             ->setF133fstatus((int)0)
             ->setF133fidGrnDetail((int)$data['f133fidGrnDetail'])
             ->setF133fcreatedBy((int)$_SESSION['userId'])
             ->setF133fupdatedBy((int)$_SESSION['userId']);

        $asset->setF133fcreatedDtTm(new \DateTime());


        try{
        $em->persist($asset);

        $em->flush();
// print_r($asset);exit;
       
        }
        catch(\Exception $e){
            echo $e;
        }
        return $asset;
    }
     public function updateAsset($asset,$data = []) 
    {
        $em = $this->getEntityManager();
        

        $asset->setF133fassetCode($data['f133fassetCode'])
             ->setF133fassetDescription($data['f133fassetDescription'])
             ->setF133fidDepartment($data['f133fidDepartment'])
             ->setF133fidAssetCategory((int)$data['f133fidAssetCategory'])
             ->setF133fAcquisitionDate(new \DateTime($data['f133fAcquisitionDate']))
             ->setF133forderNumber($data['f133forderNumber'])
             ->setF133fstoreCode($data['f133fstoreCode'])
             ->setF133faccountCode($data['f133faccountCode'])
             ->setF133fapproveDate(new \DateTime($data['f133fapproveDate']))
             ->setF133fexpireDate(new \DateTime($data['f133fexpireDate']))
             ->setF133fidAssetType((int)$data['f133fidAssetType'])
             ->setF133finstallCost((float)$data['f133finstallCost'])
             ->setF133faccumDeprCost((float)$data['f133faccumDeprCost'])
             ->setF133fnetValue((float)$data['f133fnetValue'])
             ->setF133fyearDeprCost((float)$data['f133fyearDeprCost'])
             ->setF133fresidualValue((float)$data['f133fresidualValue'])
              ->setF133fassetOwner($data['f133fassetOwner'])
               ->setF133fbarCode($data['f133fbarCode'])
               ->setF133fserialNumber($data['f133fserialNumber'])
               ->setF133fbrandName($data['f133fbrandName'])
               ->setF133fcurrentBuilding((int)$data['f133fcurrentBuilding'])
             ->setF133fcurrentRoom($data['f133fcurrentRoom'])
             ->setF133fcurrentBlock($data['f133fcurrentBlock'])
             ->setF133fcurrentCampus($data['f133fcurrentCampus'])
             ->setF133fsubCategory((int)$data['f133fsubCategory'])
             ->setF133fpoNumber($data['f133fpoNumber'])
             ->setF133fgrnNumber((int)$data['f133fgrnNumber'])
             ->setF133fsoCode($data['f133fsoCode'])
             ->setF133freferrer($data['f133freferrer'])
             ->setF133fitemName($data['f133fitemName'])
             ->setF133fidGrnDetail((int)$data['f133fidGrnDetail'])
             ->setF133fidItem((int)$data['f133fidItem'])
             ->setF133fstatus((int)$data['f133fstatus'])
             ->setF133fupdatedBy((int)$_SESSION['userId']);

        $asset  ->setF133fupdatedDtTm(new \DateTime());
        
        try{
        $em->persist($asset);
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
        return $asset;

    }


    public function approveAssetInformationMirrorData($assetData) 
    {
        $em = $this->getEntityManager();
        // $status = $data['status'];
        foreach ($assetData['id'] as $id) 
        {
        	$id = (int)$id;

        	$query1 = "update t133fasset_information set f133fstatus = 2, f133fapproved_date = getdate() where f133fid = $id";
        	$em->getConnection()->executeQuery($query1);





        	$queryAsset = " select * from t133fasset_information where f133fid = $id";
        	$result = $em->getConnection()->executeQuery($queryAsset)->fetch();
// print_r($result);exit;

        	$data['f085fassetDescription'] = $result['f133fasset_description'];
        	$data['f085forderNumber'] = $result['f133forder_number'];
        	$data['f085fidAssetCategory'] = $result['f133fid_asset_category'];
        	$data['f085fidItem'] = $result['f133fid_item'];
        	$data['f085fidDepartment'] = $result['f133fid_department'];
        	$data['f085fsubCategory'] = $result['f133fsub_category'];
            $data['f085fidAssetCategory'] = $result['f133fid_asset_category'];
            $data['f085fAcquisition_date'] = $result['f133f_acquisition_date'];
            $data['f085forderNumber'] = $result['f133forder_number'];
            $data['f085faccountCode'] = $result['f133faccount_code'];
            $data['f085fexpireDate'] = $result['f133fexpire_date'];
            $data['f085finstallCost'] = $result['f133finstall_cost'];
            $data['f085fyearDeprCost'] = $result['f133fyear_depr_cost'];
            $data['f085fassetOwner'] = $result['f133fasset_owner'];
            $data['f085fserialNumber'] = $result['f133fserial_number'];
            $data['f085fbrandName'] = $result['f133fbrand_name'];
            $data['f085fcurrentBuilding'] = $result['f133fcurrent_building'];
            $data['f085fcurrentRoom'] = $result['f133fcurrent_room'];
            $data['f085fcurrentBlock'] = $result['f133fcurrent_block'];
            $data['f085fcurrentCampus'] = $result['f133fcurrent_campus'];
            $data['f085fsubCategory'] = $result['f133fsub_category'];
            $data['f085fidItem'] = $result['f133fid_item'];
            $data['f085fidGrnDetail'] = $result['f133fid_grn_detail'];
            // $data['f133fapprovedDate'] = $result['f133fapproved_date'];
            

        	// print_r($data);exit;


        	if (isset($data))
        	{
        		// print_r($postdata);exit;
        		$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $dept = (int)$data['f085fidDepartment'];
        $subCat = $data['f085fsubCategory'];
        $Year = date('Y');

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="Asset";
        $runningNumber = $configRepository->generateFIMS($numberData);
        // if($runningNumber<10){
        //     $runningNumber = '000'.$runningNumber;
        // }
        // else if($runningNumber<100){
        //     $runningNumber = '00'.$runningNumber;
        // }
        // else if($runningNumber<1000){
        //     $runningNumber = '0'.$runningNumber;
        // }
      $query  = "select f015fdepartment_code from t015fdepartment where f015fid=$dept";
        // exit;
        $result = $em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);exit;
        $deptCode = $result['f015fdepartment_code'];

        $query  = "select f089fsubcode from t089fasset_sub_categories where f089fid=$subCat";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        $subCode = $result['f089fsubcode'];
        // print_r($subCode);exit;
        
        $finalCode = 'UUM/'.$dept.'/'.$subCode.$runningNumber.'/'.$Year;
        // print_r($finalCode);exit;

        $asset = new T085fassetInformation();
   
        $asset->setF085fassetCode($finalCode)
             ->setF085fassetDescription($data['f085fassetDescription'])
             ->setF085fidDepartment($data['f085fidDepartment'])
             ->setF085fidAssetCategory((int)$data['f085fidAssetCategory'])
             ->setF085fAcquisitionDate(new \DateTime($data['f085fAcquisitionDate']))
             ->setF085forderNumber($data['f085forderNumber'])
             ->setF085fstoreCode($data['f085fstoreCode'])
             ->setF085faccountCode($data['f085faccountCode'])
             ->setF085fapproveDate(new \DateTime($data['f085fapproveDate']))
             ->setF085fexpireDate(new \DateTime($data['f085fexpireDate']))
             ->setF085fidAssetType((int)$data['f085fidAssetType'])
             ->setF085finstallCost((float)$data['f085finstallCost'])
             ->setF085faccumDeprCost((float)$data['f085faccumDeprCost'])
             ->setF085fnetValue((float)$data['f085fnetValue'])
             ->setF085fyearDeprCost((float)$data['f085fyearDeprCost'])
             ->setF085fresidualValue((float)$data['f085fresidualValue'])
              ->setF085fassetOwner($data['f085fassetOwner'])
             ->setF085fbarCode($data['f085fbarCode'])
             ->setF085fserialNumber($data['f085fserialNumber'])
             ->setF085fbrandName($data['f085fbrandName'])
             ->setF085fcurrentBuilding((int)$data['f085fcurrentBuilding'])
             ->setF085fcurrentRoom($data['f085fcurrentRoom'])
             ->setF085fcurrentBlock($data['f085fcurrentBlock'])
             ->setF085fcurrentCampus($data['f085fcurrentCampus'])
             ->setF085fsubCategory((int)$data['f085fsubCategory'])
             ->setF085fpoNumber($data['f085fpoNumber'])
             ->setF085fgrnNumber((int)$data['f085fgrnNumber'])
             ->setF085fsoCode($data['f085fsoCode'])
             ->setF085freferrer($data['f085freferrer'])
             ->setF085fidItem((int)$data['f085fidItem'])
             ->setF085fitemName($data['f085fitemName'])
             ->setF085fstatus((int)1)
             ->setF085fcreatedBy((int)$_SESSION['userId'])
             ->setF085fupdatedBy((int)$_SESSION['userId']);

        $asset->setF085fcreatedDtTm(new \DateTime())
                ->setF085fupdatedDtTm(new \DateTime());
                // print_r($asset);exit;
        try
        {
        $em->persist($asset);
        $em->flush();
        // print_r($asset);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        	}



        }
        return 1;
        
    }

    public function getAssetInformation() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("a.f133fid, a.f133fassetCode, a.f133fassetDescription, a.f133fidDepartment, a.f133fidAssetCategory, a.f133fAcquisitionDate, a.f133forderNumber, a.f133fstoreCode, a.f133faccountCode, a.f133fapproveDate, a.f133fexpireDate, a.f133fidAssetType, a.f133finstallCost, a.f133faccumDeprCost, a.f133fnetValue, a.f133fyearDeprCost, a.f133fresidualValue, a.f133fstatus,a.f133fassetOwner, a.f133fbarCode, a.f133fserialNumber, a.f133fbrandName, a.f133fcurrentBuilding, a.f133fcurrentRoom, a.f133fcurrentBlock, a.f133fcurrentCampus, a.f133fsubCategory,a.f133fpoNumber, a.f133fgrnNumber, a.f133fsoCode, a.f133freferrer, a.f133fitemName, a.f133fidItem, a.f133fstaffIncharge")
            ->from('Application\Entity\T133fassetInformation','a');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f133fAcquisitionDate'];
            $row['f133fAcquisitionDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f133fapproveDate'];
            $row['f133fapproveDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f133fexpireDate'];
            $row['f133fexpireDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    public function getAssetInformationMirrorData($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        // print_r("sdfgh");exit;

        // Query
       $qb->select("a.f133fid, a.f133fassetCode, a.f133fassetDescription, a.f133fidDepartment, a.f133fidAssetCategory, c.f087fcategoryCode as Category, a.f133fAcquisitionDate, a.f133forderNumber, a.f133fstoreCode, a.f133faccountCode, a.f133fapproveDate, a.f133fexpireDate, a.f133fidAssetType, a.f133finstallCost, a.f133faccumDeprCost, a.f133fnetValue, a.f133fyearDeprCost, a.f133fresidualValue, a.f133fstatus,a.f133fassetOwner, a.f133fbarCode, a.f133fserialNumber, a.f133fbrandName, a.f133fcurrentBuilding, a.f133fcurrentRoom, a.f133fcurrentBlock, a.f133fcurrentCampus, a.f133fsubCategory,a.f133fpoNumber, a.f133fgrnNumber, a.f133fsoCode,a.f133freferrer, a.f133fitemName, a.f133fidItem, s.f089fsubcode as subCategory, i.f088fitemCode as itemCode, c.f087fcategoryDiscription, a.f133fcategoryType, a.f133fstaffIncharge, a.f133fcreatedBy, a.f133fcreatedDtTm, a.f133fidGrnDetail, gd.f034fquantity, gd.f034fquantityReceived, u.f014fuserName, g.f034freferenceNumber")
            ->from('Application\Entity\T133fassetInformation','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f133fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f133fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f133fidItem = i.f088fid')
            ->leftjoin('Application\Entity\T034fgrnDetails', 'gd', 'with', 'gd.f034fidDetails = a.f133fidGrnDetail')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'u.f014fid = a.f133fcreatedBy')
            ->leftjoin('Application\Entity\T034fgrn', 'g', 'with', 'g.f034fid = gd.f034fidGrn')
            ->where('a.f133fstatus = :assetId')
            ->setParameter('assetId',(int)$id)
            ->orderBy('a.f133fid','DESC');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f133fAcquisitionDate'];
            $row['f133fAcquisitionDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f133fcreatedDtTm'];
            $row['f133fcreatedDtTm'] = date("Y-m-d", strtotime($date));

            
            array_push($result1, $row);
        }
        

          $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f133fcategoryType'];
        $category = (int)$purchase['f133fidAssetCategory'];
        $subCategory = (int)$purchase['f133fsubCategory'];
        $item = (int)$purchase['f133fidItem'];

        $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

        

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
        
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                 

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           

            }
            $i++;

        }

        // print_r($result1);exit;
        return $result1;
        // return $result;
        
    }


     public function getAssetPreRegistrationNotInAsset($id) 
    {
        $em = $this->getEntityManager();
        // $id = $data['id'];
        	// $id = (int)$id;
// print_r($id);exit;
        	 $query1 = "select grnd.*,grnd.f034fid_details, u.f014fuser_name as createdBy, u1.f014fuser_name as updatedBy from t034fgrn_details grnd inner join t014fuser as u on grnd.f034fcreated_by = u.f014fid inner join t014fuser as u1 on grnd.f034fupdated_by = u1.f014fid where grnd.f034fid_details not in (select ai.f133fid_grn_detail from t133fasset_information ai) and grnd.f034fid_grn = $id";
        	// exit;
        	$result1 = $em->getConnection()->executeQuery($query1)->fetchAll();

            // print_r($result1);exit;
    $resultData = array();

             foreach ($result1 as $purchase)
        {
            $date = $purchase['f034fcreated_dt_tm'];
            $purchase['f034fcreated_dt_tm'] = date("Y-m-d", strtotime($date));

            $date = $purchase['f034fupdated_dt_tm'];
            $purchase['f034fupdated_dt_tm'] = date("Y-m-d", strtotime($date));

            array_push($resultData, $purchase);
        }
        // print_r($resultData);exit;

        $result1 = $resultData;




          $i=0;
        foreach ($result1 as $purchase)
        {

        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitem_type'];
        $category = (int)$purchase['f034fitem_category'];
        $subCategory = (int)$purchase['f034fitem_sub_category'];
        $item = (int)$purchase['f034fid_item'];

         $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                 $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

               

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
       
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


       
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           

            }
            $i++;

        }

        // print_r($result1);exit;
        return $result1;

        	// return $result;
    }
    
}

?>


