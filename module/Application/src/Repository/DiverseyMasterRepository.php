<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DiverseyMasterRepository
 *
 */
class DiverseyMasterRepository extends EntityRepository
{
    /**
     * Get customers from diversey master list
     * 
     * @param array $filters Prameters to filter query results
     */
    public function getCustomers($filters = array(), $search = null, $sort = array(), $offset = 0, $limit = 10)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('count(c.id) as total')
            ->from('Application\Entity\DiverseyMaster', 'c');
        
        // Filter
        if (! empty($filters['companyName'])) {
            $qb->andWhere('c.businessName LIKE :companyName')->setParameter('companyName', '%' . $filters['companyName'] .'%');
        }
        
        if (! empty($filters['state'])) {
            $qb->andWhere('c.state = :state')->setParameter('state', $filters['state']);
        }
        
        if (! empty($filters['city'])) {
            $qb->andWhere('c.city LIKE :city')->setParameter('city', '%' . $filters['city'] . '%');
        }
        
        if (! empty($filters['zipCode'])) {
            $qb->andWhere('c.zip LIKE :zipCode')->setParameter('zipCode', '%' . $filters['zipCode'] . '%');
        }
        
        // Search
        if (! empty($search)) {
            $expr = $qb->expr();
            $qb->andWhere($expr->orX(
                $expr->like('c.businessName', ':search'),
                $expr->like('c.state', ':search'),
                $expr->like('c.city', ':search'),
                $expr->like('c.zip', ':search')
            ));
        
            $qb->setParameter('search', "%{$search}%");
        }
        
        // Get total records
        $total = $qb->getQuery()->getSingleScalarResult();

        $qb->select('c');
        
        // Sort
        $column = 'businessName';
        $order = 'desc';
        
        if (! empty($sort)) {
            $column = $sort['column'];
            $order = $sort['order'];
        }
        
        switch ($column) {
            case 'lastName':
                $column = 'c.lastName';
                break;
            case 'firstName':
                $column = 'c.firstName';
                break;
            default:
                $column = 'c.businessName';
        }
        
        $qb->orderBy($column, $order);
        
        // Limit
        if($limit > 0){
            $qb->setFirstResult($offset)->setMaxResults($limit);
        }
        
        $query = $qb->getQuery();
        
        $result = array(
            'total' => $total,
            'data' => $query->getArrayResult()
        );
        
        return $result;
    }
    
    /**
     * Returns doctrine DBAL query builder object for insert query
     * 
     * @returns object QueryBuilder
     */
    public function getInsertQueryBuilder()
    {
        $conn = $this->getEntityManager()->getConnection();
        $qb = $conn->createQueryBuilder();
        
        $qb->insert('diversey_master')->values(array(
            'customer_name' => '?',
            'diversey_id' => '?',
            'business_name' => '?',
            'business_id' => '?',
            'address' => '?',
            'city' => '?',
            'state' => '?',
            'zip' => '?',
            'r12_sales' => '?'
        ));
        
        return $qb;
    }
    
    /**
     * Returns doctrine DBAL query builder object for update query
     * 
     * @returns object QueryBuilder
     */
    public function getUpdateQueryBuilder()
    {
        $conn = $this->getEntityManager()->getConnection();
        $qb = $conn->createQueryBuilder();
        
        $qb->update('diversey_master')
           ->set('customer_name','?')
           ->set('diversey_id', '?')
           ->set('business_name', '?')
           ->set('business_id', '?')
           ->set('address', '?')
           ->set('city', '?')
           ->set('state', '?')
           ->set('zip', '?')
           ->set('r12_sales', '?')
           ->where('diversey_id = ?');
        
        return $qb;
    }
    
    /**
     * Returns customer record for a giver diverseyId
     * 
     * @param unknown $diverseyId
     */
    public function getCustomerByDiverseyId($diverseyId)
    {
        $conn = $this->getEntityManager()->getConnection();
        $qb = $conn->createQueryBuilder();
        $qb->select('c.*')
           ->from('diversey_master','c')
           ->where('c.diversey_id = ?')
           ->setParameter(0, $diverseyId);
        
        $stmt = $qb->execute();
        
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
    /**
     * Insert new row
     * 
     * @param object QueryBuilder
     * @param array $data
     */
    public function insertRow($qb, $data)
    {
        // Set first 8 column values to query builder object
        for ($i = 0; $i <= 8; $i ++) {
            $qb->setParameter($i, trim($data[$i]));
        }
        
        $qb->execute();
    }
    
    /**
     * Update row for a specific diverseyId 
     * 
     * @param array $data
     * @param integer $diverseyId
     */
    public function updateRow($qb, $data, $diverseyId)
    {
        for ($i = 0; $i <= 8; $i ++) {
            $qb->setParameter($i, trim($data[$i]));
        }
        $qb->setParameter(9, $diverseyId);
        $qb->execute();
    }
}
