<?php
namespace Application\Repository;

use Application\Entity\T141fbudgetTransfer;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class BudgetTransferRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $query = "select count(*) as fund_count from t057ffund";
        $temp_result = $em->getConnection()->executeQuery($query)->fetch();
        $count = $temp_result['fund_count'];
        $qb->select('bt.f141fid, bt.f141ffromFund, bt.f141ftoFund, bt.f141fstatus')
            ->from('Application\Entity\T141fbudgetTransfer', 'bt')
            ->orderBy('bt.f141fid','ASC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $final_array = array();
        $k =0 ;
            for($i=0;$i<$count;$i++){
                for($j=0;$j<$count;$j++){
                    $final_array[$i][$j] = $result[$k++];
                }   
            }

        $result = array(
            'data' => $final_array ,
        );
        return $result; 
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
         $qb->select('bt.f141fid, bt.f141ffromFund, bt.f141ftoFund, bt.f141fstatus')
            ->from('Application\Entity\T141fbudgetTransfer', 'bt')
            ->where('bt.f141fid = :fundId')
            ->setParameter('fundId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result,
        );
 
        
        return $result;
    }
     public function getFromTransferFunds($fund) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
         $qb->select("bt.f141fid, bt.f141ffromFund, bt.f141ftoFund as fundCode, bt.f141fstatus,(bt.f141ftoFund +'-'+f.f057fname) as Fund")
            ->from('Application\Entity\T141fbudgetTransfer', 'bt')
           ->leftjoin('Application\Entity\T057ffund', 'f','with', 'bt.f141ftoFund = f.f057fcode')
            ->where("bt.f141ffromFund = '$fund'")
            ->AndWhere("bt.f141fstatus = 1");
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result,
        );

        
        return $result;
    }
     public function getToTransferFunds($fund) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
         $qb->select("bt.f141fid, bt.f141ffromFund as fundCode, bt.f141ftoFund, bt.f141fstatus,(bt.f141ffromFund +'-'+f.f057fname) as Fund")
            ->from('Application\Entity\T141fbudgetTransfer', 'bt')
           ->leftjoin('Application\Entity\T057ffund', 'f','with', 'bt.f141ftoFund = f.f057fcode')
            ->where("bt.f141ftoFund = '$fund'")
            ->AndWhere("bt.f141fstatus = 1");
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result,
        );

        
        return $result;
    }

    public function createbudgetTransfer($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $budgetTransfer = new T141fbudgetTransfer();

        $budgetTransfer->setF141ffromFund($data['f141ffromFund'])
        		->setF141ftoFund($data['f141ftoFund'])
                ->setF141fstatus((int)$data['f141fstatus'])
                ->setF141fcreatedBy((int)$_SESSION['userId']);

        $budgetTransfer->setF141fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($budgetTransfer);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        } 
        return $budgetTransfer;
    }

    public function updatebudgetTransfer($budgetTransfer, $data = []) 
    {
        $em = $this->getEntityManager();
        

        $budgetTransfer->setF141ffromFund($data['f141ffromFund'])
        		->setF141ftoFund($data['f141ftoFund'])
                ->setF141fstatus((int)$data['f141fstatus'])
                ->setF141fupdatedBy((int)$_SESSION['userId']);

        $budgetTransfer->setF141fupdatedDtTm(new \DateTime());

        $em->persist($budgetTransfer);
        $em->flush();
    }
}

