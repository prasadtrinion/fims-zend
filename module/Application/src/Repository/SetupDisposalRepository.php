<?php
namespace Application\Repository;

use Application\Entity\T086fsetupDisposal;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class SetupDisposalRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("d.f086fid, d.f086fdisposalCode, d.f086fdisposalType, d.f086faccountCode, ac.f059fname,ac.f059fcompleteCode,(ac.f059fcompleteCode+'-'+ac.f059fname) as  accountCode, d.f086fstatus")
            ->from('Application\Entity\T086fsetupDisposal','d')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'd.f086faccountCode = ac.f059fcompleteCode')
            ->orderBy('d.f086fid','DESC');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f086fid, d.f086fdisposalCode, d.f086fdisposalType, d.f086faccountCode,  d.f086fstatus')
            ->from('Application\Entity\T086fsetupDisposal','d')
            ->where('d.f086fid = :disposalId')
            ->setParameter('disposalId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createDisposal($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $disposal = new T086fsetupDisposal();

        $disposal->setF086fdisposalCode($data['f086fdisposalCode'])
             ->setF086fdisposalType($data['f086fdisposalType'])
             ->setF086faccountCode($data['f086faccountCode'])
             ->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fcreatedBy((int)$_SESSION['userId']);

        $disposal->setF086fcreatedDtTm(new \DateTime());
        
        $em->persist($disposal);
        $em->flush();
        
        return $disposal;
    }

   public function updateData($disposal, $data = []) 
    {
        $em = $this->getEntityManager();

        $disposal->setF086fdisposalCode($data['f086fdisposalCode'])
             ->setF086fdisposalType($data['f086fdisposalType'])
             ->setF086faccountCode($data['f086faccountCode'])
             ->setF086fstatus($data['f086fstatus'])
             ->setF086fupdatedBy((int)$_SESSION['userId']);

        $disposal->setF086fupdatedDtTm(new \DateTime());
        
        $em->persist($disposal);
        $em->flush();

        
    }


}

?>