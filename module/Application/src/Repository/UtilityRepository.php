<?php
namespace Application\Repository;

use Application\Entity\T078futility;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class UtilityRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("u.f078fid, u.f078fpremise, (p.f076fpremiseCode+'-'+p.f076fname) as premiseName, u.f078finitialReading, u.f078flastReading, u.f078fdescription, u.f078fstatus, u.f078fidRevenue, r.f085fname as revenue, u.f078fdate, u.f078fcreditFund, u.f078fcreditActivity, u.f078fcreditDepartment, u.f078fcreditAccount, u.f078fdebitFund, u.f078fdebitActivity, u.f078fdebitDepartment, u.f078fdebitAccount,(p.f076fname) as meterId")

            ->from('Application\Entity\T078futility', 'u')
            ->leftjoin('Application\Entity\T076fpremise','p','with', 'u.f078fpremise=p.f076fid')
            ->leftjoin('Application\Entity\T085frevenueSetUp','r','with', 'u.f078fidRevenue = r.f085fid')
            ->orderBy('u.f078fid','DESC');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078fdate'];
            $utility['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $utility);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("u.f078fid, u.f078fpremise, ((p.f076fpremiseCode+'-'+p.f076fname) as premiseName, u.f078finitialReading, u.f078flastReading, u.f078fdescription, u.f078fstatus, u.f078fidRevenue, r.f085fname as revenue, u.f078fdate, u.f078fcreditFund, u.f078fcreditActivity, u.f078fcreditDepartment, u.f078fcreditAccount, u.f078fdebitFund, u.f078fdebitActivity, u.f078fdebitDepartment, u.f078fdebitAccount, (p.f076fname+'-'+r.f085fname+'-' +u.f078fdescription) as meterId")

            ->from('Application\Entity\T078futility', 'u')
            ->leftjoin('Application\Entity\T076fpremise','p','with', 'u.f078fpremise=p.f076fid')
            ->leftjoin('Application\Entity\T085frevenueSetUp','r','with', 'u.f078fidRevenue = r.f085fid')

            ->where('u.f078fid = :utilityId')
            ->setParameter('utilityId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
     
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078fdate'];
            $utility['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $utility);
        }

        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $utility = new T078futility();

        $utility->setF078fpremise((int)$data['f078fpremise'])
                ->setF078finitialReading((float)$data['f078finitialReading'])
                ->setF078flastReading((float)$data['f078flastReading'])
                ->setF078fdescription($data['f078fdescription'])
                ->setF078fcreditFund($data['f078fcreditFund'])
                ->setF078fcreditActivity($data['f078fcreditActivity'])
                ->setF078fcreditDepartment($data['f078fcreditDepartment'])
                ->setF078fcreditAccount($data['f078fcreditAccount'])
                ->setF078fdebitFund($data['f078fdebitFund'])
                ->setF078fdebitActivity($data['f078fdebitActivity'])
                ->setF078fdebitDepartment($data['f078fdebitDepartment'])
                ->setF078fdebitAccount($data['f078fdebitAccount'])
                ->setF078fidNumber((int)$data['f078fidNumber'])
                ->setF078fidRevenue((int)$data['f078fidRevenue'])
                ->setF078fdate(new \DateTime($data['f078fdate']))
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fcreatedBy((int)$_SESSION['userId'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fcreatedDtTm(new \DateTime())
                ->setF078fupdatedDtTm(new \DateTime());


        try{
            $em->persist($utility);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $utility;
    }

    public function updateData($utility, $data = []) 
    {
        $em = $this->getEntityManager();

        $utility->setF078fpremise((int)$data['f078fpremise'])
                ->setF078finitialReading((float)$data['f078finitialReading'])
                ->setF078flastReading((float)$data['f078flastReading'])
                ->setF078fidNumber($data['f078fidNumber'])
                ->setF078fdescription($data['f078fdescription'])
                ->setF078fcreditFund($data['f078fcreditFund'])
                ->setF078fcreditActivity($data['f078fcreditActivity'])
                ->setF078fcreditDepartment($data['f078fcreditDepartment'])
                ->setF078fcreditAccount($data['f078fcreditAccount'])
                ->setF078fdebitFund($data['f078fdebitFund'])
                ->setF078fdebitActivity($data['f078fdebitActivity'])
                ->setF078fdebitDepartment($data['f078fdebitDepartment'])
                ->setF078fdate(new \DateTime($data['f078fdate']))
                ->setF078fdebitAccount($data['f078fdebitAccount'])
                ->setF078fidRevenue((int)$data['f078fidRevenue'])

                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fupdatedDtTm(new \DateTime());


        
        $em->persist($utility);
        $em->flush();

    }
    public function getUtilityPremises($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("u.f078fid, u.f078fpremise, u.f078finitialReading, u.f078flastReading, u.f078fdescription, u.f078fstatus, u.f078fidRevenue, u.f078fdate, u.f078fcreditFund, u.f078fcreditActivity, u.f078fcreditDepartment, u.f078fcreditAccount, u.f078fdebitFund, u.f078fdebitActivity, u.f078fdebitDepartment, u.f078fdebitAccount, (p.f076fname+'-'+r.f085fname+'-' +u.f078fdescription) as meterId")
            ->from('Application\Entity\T078futility', 'u')
            ->leftjoin('Application\Entity\T085frevenueSetUp','r','with', 'u.f078fidRevenue = r.f085fid')
            ->leftjoin('Application\Entity\T076fpremise','p','with', 'u.f078fpremise=p.f076fid')

            ->where('u.f078fpremise = :premiseId')
            ->setParameter('premiseId',$id)
            ->orderBy('u.f078fid','DESC');
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
     
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078fdate'];
            $utility['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $utility);
        }

        return $result1;
    }

   

}
