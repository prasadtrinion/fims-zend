<?php
namespace Application\Repository;

use Application\Entity\T064ftaggingSponsors;
use Application\Entity\T011fdefinationms;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TaggingSponsorRepository extends EntityRepository 
{
    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f064fid, s.f064fidSponsor, s.f064fidStudent, s.f064fidSemester, s.f064fagreementNo, s.f064fstartDate, s.f064fendDate, s.f064fextensionDate, s.f064fstatus, s.f064fmetricNumber, s.f064fsponsorCode, s.f064fpassportNumber, s.f064fdocumentRefNo, s.f064fpath, (t.f063fcode+'-'+t.f063ffirstName+' '+t.f063flastName) as sponsor, (st.f060ficNumber+'-'+st.f060fname) as student")
            ->from('Application\Entity\T064ftaggingSponsors', 's')
            ->leftjoin('Application\Entity\T063fsponsor', 't', 'with', 's.f064fidSponsor = t.f063fid')
            ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 's.f064fidStudent= st.f060fid')
            ->orderBy('s.f064fid','DESC');
            
       $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $tagging) 
        {
            $date                        = $tagging['f064fstartDate'];
            $tagging['f064fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $tagging['f064fendDate'];
            $tagging['f064fendDate'] = date("Y-m-d", strtotime($date));

            $date                        = $tagging['f064fextensionDate'];
            $tagging['f064fextensionDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $tagging);
        }

    return $result1;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("s.f064fid, s.f064fidSponsor, s.f064fidStudent, s.f064fidSemester, 
            s.f064fagreementNo, s.f064fstartDate, s.f064fendDate , s.f064fextensionDate, s.f064fstatus, s.f064fmetricNumber, s.f064fsponsorCode, s.f064fpassportNumber, s.f064fdocumentRefNo, s.f064fpath, (t.f063ffirstName+'-'+t.f063flastName) as sponsor, (st.f060ficNumber+'-'+st.f060fname) as student")
            ->from('Application\Entity\T064ftaggingSponsors', 's')
            ->leftjoin('Application\Entity\T063fsponsor', 't', 'with', 's.f064fidSponsor = t.f063fid')
            ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 's.f064fidStudent= st.f060fid')
            ->where('s.f064fid = :taggingId')
            ->setParameter('taggingId',$id);
            
       $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $tagging) 
        {
            $date                        = $tagging['f064fstartDate'];
            $tagging['f064fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $tagging['f064fendDate'];
            $tagging['f064fendDate'] = date("Y-m-d", strtotime($date));

            $date                        = $tagging['f064fextensionDate'];
            $tagging['f064fextensionDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $tagging);
        }

    return $result1;
    }

    public function createNewData($data) 
    {


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="SPC";
        $referenceNumber = $configRepository->generateFIMS($numberData);
        // print_r($referenceNumber);exit;
        
        $sponsor = new T064ftaggingSponsors();

        $sponsor->setF064fidSponsor((int)$data['f064fidSponsor'])
                ->setF064fidStudent((int)$data['f064fidStudent'])
                ->setF064fidSemester((int)$data['f064fidSemester'])
                ->setF064fagreementNo(($data['f064fagreementNo']))
                ->setF064fstartDate(new \DateTime($data['f064fstartDate']))
                ->setF064fendDate(new \DateTime($data['f064fendDate']))
                ->setF064fextensionDate(new \DateTime($data['f064fextensionDate']))
                ->setF064fstatus((int)($data['f064fstatus']))
                ->setF064fmetricNumber((int)$data['f064fmetricNumber'])
                ->setF064fsponsorCode($referenceNumber)
                ->setF064fpassportNumber($data['f064fpassportNumber'])
                ->setF064fdocumentRefNo($data['f064fdocumentRefNo'])
                ->setF064fpath($data['f064fpath'])
                ->setF064fcreatedBy((int)$_SESSION['userId'])
                ->setF064fupdatedBy((int)$_SESSION['userId']);

        $sponsor->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());


        try{
            $em->persist($sponsor);
            $em->flush();
            // print_r($sponsor);exit;
        }
        catch(\Exception $e){
            echo $e;
        }
        return $sponsor;
    }


    public function updateData($sponsor, $data = []) 
    {
        $em = $this->getEntityManager();

        $sponsor->setF064fidSponsor((int)$data['f064fidSponsor'])
                ->setF064fidStudent((int)$data['f064fidStudent'])
                ->setF064fidSemester((int)$data['f064fidSemester'])
                ->setF064fagreementNo(($data['f064fagreementNo']))
                ->setF064fstartDate(new \DateTime($data['f064fstartDate']))
                ->setF064fendDate(new \DateTime($data['f064fendDate']))
                ->setF064fextensionDate(new \DateTime($data['f064fextensionDate']))
                ->setF064fstatus((int)($data['f064fstatus']))
                ->setF064fmetricNumber((int)$data['f064fmetricNumber'])
                ->setF064fsponsorCode($data['f064fsponsorCode'])
                ->setF064fpassportNumber($data['f064fpassportNumber'])
                ->setF064fdocumentRefNo($data['f064fdocumentRefNo'])
                ->setF064fupdatedBy((int)$_SESSION['userId']);

        $sponsor->setF064fupdatedDtTm(new \DateTime());


        
        $em->persist($sponsor);
        $em->flush();

    }
}
