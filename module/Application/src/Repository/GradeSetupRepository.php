<?php
namespace Application\Repository;

use Application\Entity\T046fgradeSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class GradeSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('g.f046fid, g.f046fgradeName, g.f046fgradeFrom,g.f046floanType, g.f046fgradeTo, g.f046fstatus,g.f046fschedule,l.f020floanName,g.f046fgradeMonthFrom, g.f046fgradeMonthTo, g.f046fgradeDescription,g.f046fvehicleCondition')
            ->from('Application\Entity\T046fgradeSetup', 'g')
            ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'g.f046floanType = l.f020fid')

            ->orderBy('g.f046fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        	$result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f046fschedule'];
            $item['f046fschedule'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

     public function getLoanGrades($data) 
    {
        $em = $this->getEntityManager();

        // print_r($data);exit;
        $id_loan = $data['idloantype'];
        $grade = $data['idgrade'];
        $vehicleCondition = (int)$data['vehicleCondition'];
        
        $em = $this->getEntityManager();
        
        $query = "select  f046fgrade_name as f046fgradeName, f046fgrade_from as f046fgradeFrom, f046fgrade_to as f046fgradeTo, f046floan_type as f046floanType, f046fstatus, f046fschedule, f046fgrade_month_from as f046fgradeMonthFrom, f046fgrade_month_to as f046fgradeMonthTo, f046fgrade_description as f046fgradeDescription, f046fvehicle_condition as f046fvehicleCondition from t046fgrade_setup WHERE f046floan_type = $id_loan and f046fgrade_name = '$grade' ";
        if(isset($data['vehicleCondition'])){
            $query = $query." and f046fvehicle_condition = $vehicleCondition";
        }
            $result = $em->getConnection()->executeQuery($query)->fetch();

            print_r($query);exit;
            
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('g.f046fid, g.f046fgradeName, g.f046fgradeFrom,g.f046floanType, g.f046fgradeTo, g.f046fstatus,g.f046fschedule,l.f020floanName, g.f046fgradeMonthFrom, g.f046fgradeMonthTo, g.f046fgradeDescription,g.f046fvehicleCondition')
            ->from('Application\Entity\T046fgradeSetup', 'g')
            ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'g.f046floanType = l.f020fid')
            ->where('g.f046fid = :gradeId')
            ->setParameter('gradeId',$id);
            
        $query = $qb->getQuery();
        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f046fschedule'];
            $item['f046fschedule'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        return $result1;
        
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // if($data['f046fgradeName'] == 'All'){
        //     $data['f046fgradeName'] = 0;
        // }
        $grade = new T046fgradeSetup();
        

        $grade->setF046fgradeName($data['f046fgradeName'])
                ->setF046fgradeFrom((float)$data['f046fgradeFrom'])
                ->setF046fgradeTo((int)$data['f046fgradeTo'])
                ->setF046fvehicleCondition((int)$data['f046fvehicleCondition'])
                ->setF046floanType((int)$data['f046floanType'])
                ->setF046fgradeMonthFrom($data['f046fgradeMonthFrom'])
                ->setF046fgradeMonthTo($data['f046fgradeMonthTo'])
                ->setF046fgradeDescription($data['f046fgradeDescription'])
                ->setF046fstatus((int)$data['f046fstatus'])
                ->setF046fschedule(new \DateTime($data['f046fschedule']))
                ->setF046fcreatedBy((int)$_SESSION['userId'])
                ->setF046fupdatedBy((int)$_SESSION['userId']);

        $grade->setF046fcreatedDtTm(new \DateTime())
                  ->setF046fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($grade);
            
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $grade;
    }

    public function updateData($grade, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $grade->setF046fgradeName($data['f046fgradeName'])
                ->setF046fgradeFrom((float)$data['f046fgradeFrom'])
                ->setF046fgradeTo((int)$data['f046fgradeTo'])
                ->setF046fvehicleCondition((int)$data['f046fvehicleCondition'])
                ->setF046floanType((int)$data['f046floanType'])
                ->setF046fgradeMonthFrom($data['f046fgradeMonthFrom'])
                ->setF046fgradeMonthTo($data['f046fgradeMonthTo'])
                ->setF046fgradeDescription($data['f046fgradeDescription'])
                ->setF046fstatus((int)$data['f046fstatus'])
                ->setF046fupdatedBy((int)$_SESSION['userId']);

        $grade->setF046fupdatedDtTm(new \DateTime());

        
        $em->persist($grade);
        $em->flush();
        
        

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('g.f046fid, g.f046fgradeName, g.f046fgradeFrom, g.f046fgradeTo, g.f046fstatus,g.f046fschedule,g.f046fgradeMonthFrom, g.f046fgradeMonthTo, g.f046fgradeDescription,g.f046fvehicleCondition
            ')
            ->from('Application\Entity\T046fgradeSetup', 'g')
            ->orderBy('g.f046fgradeName')
            ->where('g.f046fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    
}
