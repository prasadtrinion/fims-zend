<?php
namespace Application\Repository;

use Application\Entity\T110fbudgetyear;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetYearRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f110fid,f.f110fyear, f.f110fdescription,f.f110fstatus')
            ->from('Application\Entity\T110fbudgetyear','f')
            ->orderby('f.f110fid','DESC');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
        
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('f.f110fid,f.f110fyear, f.f110fdescription,f.f110fstatus')
            ->from('Application\Entity\T110fbudgetyear','f')
            ->where('f.f110fid = :budgetId')
            ->setParameter('budgetId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $year = date("Y");
        if((int)$year > (int)$data['f110fyear']){
            $data['f110fstatus'] = 0;
        }
        // if($data['f110fstatus'] == 1)
        // {
        //     $query1 = "UPDATE t110fbudgetyear set f110fstatus=0"; 
        //     $result=$em->getConnection()->executeQuery($query1);
        // }

        $budget = new T110fbudgetyear();

        $budget->setF110fyear($data['f110fyear'])
              ->setF110fdescription($data['f110fdescription'])
              ->setF110fstatus((int)$data['f110fstatus'])
              ->setF110fcreatedBy((int)$_SESSION['userId'])
              ->setF110fupdatedBy((int)$_SESSION['userId']);


        $budget->setF110fcreateDtTm(new \DateTime())
                ->setF110fupdateDtTm(new \DateTime());

 
        $em->persist($budget);
        $em->flush();

        return $budget;

    }

    public function updateData($budget, $data = []) 
    {
        $em = $this->getEntityManager();

        $id = $budget->getF110fid();   

        // if($data['f015fstatus'] == 1)

        // {
        //     $query1 = "UPDATE t110fbudgetyear set f110fstatus=0 where f110fid != $id"; 
        //     $result=$em->getConnection()->executeQuery($query1);
        // }
         $year = date("Y");
        if((int)$year > (int)$data['f110fyear']){
            $data['f110fstatus'] = 0;
        }
        $budget->setF110fyear($data['f110fyear'])
              ->setF110fdescription($data['f110fdescription'])
              ->setF110fstatus((int)$data['f110fstatus'])
              ->setF110fcreatedBy((int)$_SESSION['userId'])
              ->setF110fupdatedBy((int)$_SESSION['userId']);
        
        $budget->setF110fupdateDtTm(new \DateTime());



        try
        {
        $em->persist($budget);
        
        $em->flush();
        

        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $budget;


    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f110fid,f.f110fyear, f.f110fdescription,f.f110fstatus')
            ->from('Application\Entity\T110fbudgetyear','f')
            ->where('f.f110fstatus=1');

        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $result;
        
    }

    

 }
 ?>

