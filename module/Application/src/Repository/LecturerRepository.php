<?php
namespace Application\Repository;

use Application\Entity\Lecturer;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LecturerRepository extends EntityRepository 
{

  


    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('g.id','g.name')
            ->from('Application\Entity\Lecturer', 'g');


        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    
    }

    public function getGlcodeDetails($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('g.ffid,  g.ffglCode, g.ffglDescription, g.ffapprovalStatus, g.ffstatus, g.ffcreatedBy, g.ffupdatedBy')
            ->from('Application\Entity\Glcode', 'g')
            ->where('g.ffstatus!=1')
            ->andWhere('g.ffid = :glcodeId')
            ->setParameter('glcodeId',$glcodeId);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }


    public function createNewData($data){

        $em = $this->getEntityManager();
        $glcode = new Glcode();
        $glcode->setFfglCode($data['ffglCode'])
                ->setFfglDescription($data['ffglDescription'])
                ->setFfapprovalStatus($data['ffapprovalStatus'])
                ->setFfcreatedBy((int)$_SESSION['userId'])
                ->setFfupdatedBy((int)$_SESSION['userId']);
        $glcode->setFfcreateDtTm(new \DateTime())
                ->setFfupdateDtTm(new \DateTime());
        $em->persist($glcode);
        $em->flush();
        return $glcode; 
    }

    public function createGlcode($data) 
    {

        $em = $this->getEntityManager();

        $glcode = new Glcode();

        $glcode->setFfglCode($data['ffglCode'])
                ->setFfglDescription($data['ffglDescription'])
                ->setFfapprovalStatus($data['ffapprovalStatus'])
                ->setFfcreatedBy((int)$_SESSION['userId'])
                ->setFfupdatedBy((int)$_SESSION['userId']);

        $glcode->setFfcreateDtTm(new \DateTime())
                ->setFfupdateDtTm(new \DateTime());


        
        $em->persist($glcode);
        $em->flush();

        return $glcode;
    }

    public function updateGlcode($glcode, $data = []) 
    {
        $em = $this->getEntityManager();
        
       $glcode->setFfglCode($data['ffglCode'])
                ->setFfglDescription($data['ffglDescription'])
                ->setFfapprovalStatus($data['ffapprovalStatus'])
                ->setFfcreatedBy((int)$_SESSION['userId'])
                ->setFfupdatedBy((int)$_SESSION['userId']);

        $glcode->setFfupdateDtTm(new \DateTime());


        
        $em->persist($glcode);
        $em->flush();

    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}
