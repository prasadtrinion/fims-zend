<?php
namespace Application\Repository;

use Application\Entity\T109fprocurementLimit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ProcurementLimitRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f109fid, f.f109ftype, f.f109fminLimit, f.f109fmaxLimit, f.f109fstatus')
            ->from('Application\Entity\T109fprocurementLimit','f')
            ->orderBy('f.f109fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('f.f109fid, f.f109ftype, f.f109fminLimit, f.f109fmaxLimit, f.f109fstatus')
            ->from('Application\Entity\T109fprocurementLimit','f')
            ->where('f.f109fid = :procurementId')
            ->setParameter('procurementId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $procurement = new T109fprocurementLimit();

        $procurement->setF109ftype($data['f109ftype'])
             ->setF109fminLimit((float)$data['f109fminLimit'])
             ->setF109fmaxLimit((float)$data['f109fmaxLimit'])
             ->setF109fstatus((int)$data['f109fstatus'])
             ->setF109fcreatedBy((int)$_SESSION['userId'])
             ->setF109fupdatedBy((int)$_SESSION['userId']);

        $procurement->setF109fcreatedDtTm(new \DateTime())
                ->setF109fupdatedDtTm(new \DateTime());
               
        try
        {
            $em->persist($procurement);
        
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

        return $procurement;

    }

    /* to edit the data in database*/

    public function updateData($procurement, $data = []) 
    {
        $em = $this->getEntityManager();

        $procurement->setF109ftype($data['f109ftype'])
             ->setF109fminLimit((float)$data['f109fminLimit'])
             ->setF109fmaxLimit((float)$data['f109fmaxLimit'])
             ->setF109fstatus((int)$data['f109fstatus'])
             ->setF109fcreatedBy((int)$_SESSION['userId'])
             ->setF109fupdatedBy((int)$_SESSION['userId']);

        $procurement->setF109fcreatedDtTm(new \DateTime())
                ->setF109fupdatedDtTm(new \DateTime());
               
        try
        {
            $em->persist($procurement);
        
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    
}

?>