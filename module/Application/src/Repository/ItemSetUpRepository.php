<?php
namespace Application\Repository;

use Application\Entity\T029fitemSetUp;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ItemSetUpRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("i.f029fid,i.f029fitemName,i.f029funit, i.f029fidCategory, c.f027fcategoryName as category, i.f029faccountCode,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, i.f029fidTax, t.f081fcode as tax,  i.f029fidSubCategory, sc.f028fsubCategoryName as subCategory, i.f029fstatus,(c.f027fcategoryName+'-'+sc.f028fsubCategoryName) as f088fitem, i.f029fitemCode")
            ->from('Application\Entity\T029fitemSetUp','i')
            ->leftjoin('Application\Entity\T027fitemCategory','c','with', 'c.f027fid = i.f029fidCategory')
            ->leftjoin('Application\Entity\T028fitemSubCategory','sc','with', 'sc.f028fid = i.f029fidSubCategory')
            ->leftjoin('Application\Entity\T081ftextCode','t','with','t.f081fid = i.f029fidTax')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'a.f059fcompleteCode = i.f029faccountCode')
            ->orderBy('i.f029fid','DESC');
            
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("i.f029fid,i.f029fitemName,i.f029funit, i.f029fidCategory, c.f027fcategoryName as category, i.f029faccountCode,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, i.f029fidTax, t.f081fcode as tax,  i.f029fidSubCategory, sc.f028fsubCategoryName as subCategory, i.f029fstatus, i.f029fitemCode")
            ->from('Application\Entity\T029fitemSetUp','i')
            ->leftjoin('Application\Entity\T027fitemCategory','c','with', 'i.f029fidCategory = c.f027fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory','sc','with', 'i.f029fidSubCategory = sc.f028fid')
            ->leftjoin('Application\Entity\T081ftextCode','t','with','i.f029fidTax = t.f081fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'i.f029faccountCode = a.f059fcompleteCode')
             ->where('i.f029fid = :itemId')
            ->setParameter('itemId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $item = new T029fitemSetUp();

        $item->setF029fitemName($data['f029fitemName'])
             ->setF029funit($data['f029funit'])
             ->setF029fidCategory((int)$data['f029fidCategory'])
             ->setF029faccountCode((int)$data['f029faccountCode'])
             ->setF029fidTax((int)$data['f029fidTax'])
             ->setF029fidSubCategory((int)$data['f029fidSubCategory'])
             ->setF029fitemCode($data['f029fitemCode'])
             ->setF029fstatus((int)$data['f029fstatus'])
             ->setF029fcreatedBy((int)$_SESSION['userId'])
             ->setF029fupdatedBy((int)$_SESSION['userId']);

        $item->setF029fcreatedDtTm(new \DateTime())
                ->setF029fupdatedDtTm(new \DateTime());

	try{ 
        $em->persist($item);
        $em->flush();
	}
	catch(\Exception $e){
		echo $e;
	} 
        return $item;

    }

    public function updateData($item, $data = []) 
    {
        $em = $this->getEntityManager();

        $item->setF029fitemName($data['f029fitemName'])
             ->setF029funit($data['f029funit'])
             ->setF029fidCategory((int)$data['f029fidCategory'])
             ->setF029faccountCode((int)$data['f029faccountCode'])
             ->setF029fidTax((int)$data['f029fidTax'])
             ->setF029fidSubCategory((int)$data['f029fidSubCategory'])
             ->setF029fitemCode($data['f029fitemCode'])
             ->setF029fstatus((int)$data['f029fstatus'])
             ->setF029fupdatedBy((int)$_SESSION['userId']);

        $item->setF029fupdatedDtTm(new \DateTime());
        
        $em->persist($item);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f029fid, i.f029fitemName, i.f029funit, i.f029fidCategory, c.f027fcategoryName as category, i.f029faccountCode, i.f029fidTax, t.f081fcode as tax,  i.f029fidSubCategory, sc.f028fsubCategoryName as subCategory, i.f029fstatus, i.f029fitemCode')
            ->from('Application\Entity\T029fitemSetUp','i')
            ->leftjoin('Application\Entity\T027fitemCategory','c','with', 'i.f029fidCategory = c.f027fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory','sc','with', 'i.f029fidSubCategory = sc.f028fid')
            ->leftjoin('Application\Entity\T081ftextCode','t','with','i.f029fidTax = t.f081fid')
            ->orderBy('i.f029fitemName')
            ->where('i.f029fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;   
    }

    public function getItemSetUpBySubCategory($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("i.f029fid, i.f029fid as itemId, i.f029fitemName, i.f029fitemName as itemName, i.f029funit, i.f029fidCategory, c.f027fcategoryName as category, i.f029faccountCode,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, i.f029fidTax, t.f081fcode as tax,  i.f029fidSubCategory, sc.f028fsubCategoryName as subCategory, i.f029fstatus, i.f029fitemCode")
            ->from('Application\Entity\T029fitemSetUp','i')
            ->leftjoin('Application\Entity\T027fitemCategory','c','with', 'i.f029fidCategory = c.f027fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory','sc','with', 'i.f029fidSubCategory = sc.f028fid')
            ->leftjoin('Application\Entity\T081ftextCode','t','with','i.f029fidTax = t.f081fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'i.f029faccountCode = a.f059fcompleteCode')
             ->where('i.f029fidSubCategory = :itemId')
            ->setParameter('itemId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = array("data"=> $result);
        return $result;
    }

}
