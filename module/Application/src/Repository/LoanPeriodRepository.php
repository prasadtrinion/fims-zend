<?php
namespace Application\Repository;

use Application\Entity\T022floanPeriod;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LoanPeriodRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f022fid, l.f022fidLoanName, ln.f020floanName, l.f022fgrade, l.f022fminimumMonth, l.f022fmaximumMonth, l.f022fstatus, l.f022fcreatedBy, l.f022fupdatedBy')
            ->from('Application\Entity\T022floanPeriod','l')
            ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','l.f022fidLoanName = ln.f020fid')
            ->orderBy('l.f022fid','DESC');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result = array(
            'data' => $result
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f022fid, l.f022fidLoanName, ln.f020floanName, l.f022fgrade, l.f022fminimumMonth, l.f022fmaximumMonth, l.f022fstatus, l.f022fcreatedBy, l.f022fupdatedBy')
            ->from('Application\Entity\T022floanPeriod','l')
            ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','l.f022fidLoanName = ln.f020fid')
            ->where('l.f022fid = :loanId')
            ->setParameter('loanId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $loan = new T022floanPeriod();

        $loan->setF022fidLoanName((int)$data['f022fidLoanName'])
             ->setF022fgrade($data['f022fgrade'])
             ->setF022fminimumMonth((int)$data['f022fminimumMonth'])
             ->setF022fmaximumMonth((int)$data['f022fmaximumMonth'])
             ->setF022fstatus((int)$data['f022fstatus'])
             ->setF022fcreatedBy((int)$_SESSION['userId'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $loan->setF022fcreatedDtTm(new \DateTime())
                ->setF022fupdatedDtTm(new \DateTime());

 try{
        $em->persist($loan);
        $em->flush();
        
}
catch(\Exception $e){
    echo $e;
}
        return $loan;

    }

    /* to edit the data in database*/

    public function updateData($loan, $data = []) 
    {
        $em = $this->getEntityManager();

        $loan->setF022fidLoanName((int)$data['f022fidLoanName'])
        ->setF022fgrade($data['f022fgrade'])
        ->setF022fminimumMonth((int)$data['f022fminimumMonth'])
        ->setF022fmaximumMonth((int)$data['f022fmaximumMonth'])
        ->setF022fstatus((int)$data['f022fstatus'])
             ->setF022fstatus((int)$data['f022fstatus'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $loan->setF022fupdatedDtTm(new \DateTime());

        $em->persist($loan);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>