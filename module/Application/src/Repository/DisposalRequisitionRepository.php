<?php
namespace Application\Repository;

use Application\Entity\T086fdisposalRequisition;
use Application\Entity\T086fdisposalReqDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DisposalRequisitionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f086fid, d.f086fbatchId, d.f086fdescription, d.f086fverificationStatus,d.f086fstatus,d.f086frequestBy,d.f086fisEndorsement,u.f014fuserName')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','d.f086fcreatedBy = u.f014fid')
            ->orderBy('d.f086fid','DESC');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f086fid, d.f086fbatchId, d.f086fdescription as parentDescription,d.f086frequestBy,d.f086fisEndorsement, d.f086fverificationStatus,d.f086fstatus, ad.f086fidDetails,ad.f086fidAsset, ad.f086fassetCode, ad.f086fdescription,ad.f086fnetValue,ad.f086ftype,u.f014fuserName')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T086fdisposalReqDetails', 'ad', 'with','d.f086fid = ad.f086fidDisposal')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','d.f086fcreatedBy = u.f014fid')

            ->where('d.f086fid = :disposalId')
            ->setParameter('disposalId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createDisposal($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="AssetDisposal";
        $number = $configRepository->generateFIMS($numberData);
        $disposal = new T086fdisposalRequisition();


        $disposal->setF086fbatchId($number)
             ->setF086fdescription($data['f086fdescription'])
             ->setF086fverificationStatus($data['f086fverificationStatus'])
             ->setF086fisEndorsement((int)$data['f086fisEndorsement'])
             ->setF086fstatus((int)$data['f086fstatus'])
             ->setF086frequestBy((int)(int)$_SESSION['userId'])
             ->setF086fcreatedBy((int)$_SESSION['userId'])
             ->setF086fupdatedBy((int)$_SESSION['userId']);

        $disposal->setF086fcreatedDtTm(new \DateTime())
                ->setF086fupdatedDtTm(new \DateTime());
        
        try
        { 
            $em->persist($disposal);
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        
        $disposalDetails = $data['disposal-details'];

        foreach ($disposalDetails as $disposalDetail) {

            $disposalDetailsObj = new T086fdisposalReqDetails();

            $disposalDetailsObj->setF086fidDisposal((int)$disposal->getF086fid())
                       ->setF086fassetCode($disposalDetail['f086fassetCode'])
                       ->setF086fidAsset((int)$disposalDetail['f086fidAsset'])
                       ->setF086fdescription($disposalDetail['f086fdescription'])
                       ->setF086fnetValue((float)$disposalDetail['f086fnetValue'])
                       ->setF086ftype($disposalDetail['f086ftype'])
                       ->setF086fstatus((int)$data['f086fstatus'])
                       ->setF086fcreatedBy((int)$_SESSION['userId'])
                       ->setF086fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailsObj->setF086fcreatedDtTm(new \DateTime())
                       ->setF086fupdatedDtTm(new \DateTime());
        

        $em->persist($disposalDetailsObj);

        $em->flush();
        // print_r($disposalDetailsObj);exit;

            
        }
        return $disposal;
    }

   public function updateDisposal($disposal, $data = []) 
    {
        $em = $this->getEntityManager();

         $disposal->setF086fdescription($data['f086fdescription'])
             ->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fisEndorsement((int)$data['f086fisEndorsement'])
             ->setF086fverificationStatus($data['f086fverificationStatus'])
             ->setF086frequestBy((int)$_SESSION['userId'])
             ->setF086fupdatedBy((int)$_SESSION['userId']);

        $disposal->setF086fupdatedDtTm(new \DateTime());
        
        $em->persist($disposal);
        $em->flush();

        return $disposal;
    }


    public function updateDisposalDetail($disposalDetailObj, $disposalDetail)
    {

        $em = $this->getEntityManager();


        $disposalDetailObj->setF086fassetCode($disposalDetail['f086fassetCode'])
                       ->setF086fidAsset((int)$disposalDetail['f086fidAsset'])

                       ->setF086fdescription($disposalDetail['f086fdescription'])
                       ->setF086fnetValue((float)$disposalDetail['f086fnetValue'])
                       ->setF086ftype($disposalDetail['f086ftype'])
                       ->setF086fstatus((int)$data['f086fstatus']);
                       // ->setF086fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailObj->setF086fupdatedDtTm(new \DateTime());

        $em->persist($disposalDetailObj);

        $em->flush();

            
        
    }
    
    public function createDisposalDetail($disposalObj, $disposalDetail)
    {
        $em = $this->getEntityManager();

        $disposalDetailObj = new T086fdisposalReqDetails();

        $disposalDetailObj->setF086fidDisposal($disposalObj->getF086fid())
                       ->setF086fassetCode($disposalDetail['f086fassetCode'])
                       ->setF086fidAsset((int)$disposalDetail['f086fidAsset'])
                       ->setF086fdescription($disposalDetail['f086fdescription'])
                       ->setF086fnetValue((float)$disposalDetail['f086fnetValue'])
                       ->setF086ftype($disposalDetail['f086ftype'])
                       ->setF086fstatus((int)$data['f086fstatus'])
                       ->setF086fcreatedBy((int)$_SESSION['userId'])
                       ->setF086fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailObj->setF086fcreatedDtTm(new \DateTime())
                       ->setF086fupdatedDtTm(new \DateTime());

 
        $em->persist($disposalDetailObj);
        $em->flush();

        return $disposalDetailsObj;
        
    }

    public function getListByVerificationStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select('d.f086fid, d.f086fdisposalName, d.f086fdescription, d.f086fstatus, d.f086fverificationStatus, ad.f086fidDetails, ad.f086fidAsset, ad.f086fdescription')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T086fdisposalReqDetails', 'ad', 'with','d.f086fid = ad.f086fidDisposal')
            ->where('d.f086fverificationStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
            $qb->select('d.f086fid, d.f086fdisposalName, d.f086fdescription, d.f086fstatus, d.f086fverificationStatus, ad.f086fidDetails, ad.f086fidAsset, ad.f086fdescription')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T086fdisposalReqDetails', 'ad', 'with','d.f086fid = ad.f086fidDisposal')
                 ->where('d.f086fverificationStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
            $qb->select('d.f086fid, d.f086fdisposalName, d.f086fdescription, d.f086fstatus, d.f086fverificationStatus, ad.f086fidDetails, ad.f086fidAsset, ad.f086fdescription')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T086fdisposalReqDetails', 'ad', 'with','d.f086fid = ad.f086fidDisposal');
                
            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function updateEndorsementStatus($data) 
    {
        $em = $this->getEntityManager();

         foreach ($data['id'] as $id) {
            $query1 = "update t086fdisposal_requisition set f086fis_endorsement = 1, f086fstatus = 1 where f086fid = $id"; 
            $em->getConnection()->executeQuery($query1);

            $user = $_SESSION['userId'];
            $date = date('Y-m-d h:m:s');
            $query1 = "update t086fdisposal_req_details set f086fupdated_by = $user,f086fupdated_dt_tm='$date' where f086fid_disposal = $id"; 
            $em->getConnection()->executeQuery($query1);


        }
    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
     public function getAllDisposalDetails()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

            $qb->select('ad.f086fidDetails, ad.f086fassetCode, ad.f086fdescription,ad.f086fnetValue,ad.f086ftype,ad.f086fstatus,t.f090fcode as assetType')
            ->from('Application\Entity\T086fdisposalReqDetails','ad')
            ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'ad.f086ftype = t.f090fid')
            ;
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function deleteDisposalReqDetail($id)
    {
        $em = $this->getEntityManager();
        $query = "delete from t086fdisposal_req_details where f086fid_details = $id";
        $em->getConnection()->executeQuery($query);
        return ;
    }

    public function assetNotInDisposalRequistion()
    {

        $em = $this->getEntityManager();

        $query = "SELECT *, f085fasset_code as f085fassetCode from t085fasset_information where f085fid not in (select f086fid_asset from t086fdisposal_req_details)";

        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
}

?>