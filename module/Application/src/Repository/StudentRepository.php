<?php
namespace Application\Repository;

use Application\Entity\T060fstudent;
use Application\Entity\T064ftaggingSponsors;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StudentRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager(); 

        $qb = $em->createQueryBuilder();

        // Query
        // echo "hi";
        // die();
            $query = "select top 50 s.f060fid, s.f060femail, (cast(s.f060fmatric_number as varchar(30))+'-'+s.f060fname) as f060fname,s.f060ftype, s.f060fphone_number as f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fid_program as f060fidProgram, s.f060fid_intake as f060fidIntake, s.f060fid_semester as f060fidSemester, s.f060fstatus, s.f060fstudent_category as f060fstudentCategory,s.f060fmatric_number as f060fmatricNumber,s.f060fic_number as f060ficNumber,s.f060fprogram_code as f060fprogramCode from vw_all_students as s";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

       // $qb->select("s.f060fid, s.f060femail, s.f060fname,s.f060ftype, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fidProgram, s.f060fidIntake, s.f060fidSemester, s.f060fstatus, s.f060fstudentCategory,s.f060fmatricNumber,s.f060ficNumber,s.f060fprogramCode")
       //    ->from('Application\Entity\T060fstudent','s')
       //    ->where("s.f060ftype='UG'")
       //    ->orderBy('s.f060fid','DESC');

        
       //  $query = $qb->getQuery();

        $result = array(
            'data' => $result,
        );
        
        return $result;
        
    }

    public function getStudents($type) 
    {
        $em = $this->getEntityManager(); 

        $qb = $em->createQueryBuilder();
// echo "hi";
// die();
        // Query
        $query = "select top 50 s.f060fid, s.f060femail, (cast(s.f060fmatric_number as varchar(30))+'-'+s.f060fname) as f060fname,s.f060ftype, s.f060fphone_number as f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fid_program as f060fidProgram, s.f060fid_intake as f060fidIntake, s.f060fid_semester as f060fidSemester, s.f060fstatus, s.f060fstudent_category as f060fstudentCategory,s.f060fmatric_number as f060fmatricNumber,s.f060fic_number as f060ficNumber,s.f060fprogram_code as f060fprogramCode from vw_all_students as s where s.f060ftype = '$type'";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
       // $qb->select("s.f060fid, s.f060femail,(s.f060fmatricNumber+'-'+s.f060fname) as f060fname,s.f060ftype, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fidProgram, s.f060fidIntake, s.f060fidSemester, s.f060fstatus, s.f060fstudentCategory,s.f060fmatricNumber,s.f060ficNumber,s.f060fprogramCode, ( s.f060fmatricNumber+'-'+s.f060fname) as Student")
       //    ->from('Application\Entity\T060fstudent','s')
       //    ->where("s.f060ftype = '".$type."'")
       //    ->orderBy('s.f060fid','DESC');

        
       //  $query = $qb->getQuery();

       //  $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("s.f060fid, s.f060femail, s.f060fname,s.f060ftype, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstateName as stateName, s.f060fcountry, c.f013fcountryName as countryName, s.f060fgender, s.f060fidProgram, p.f068fname as program, s.f060fidIntake,s.f060fcitizen,s.f060fregion,s.f060fstudyMode, i.f067fname as intake, s.f060fidSemester, sm.f053fsemester as semester, s.f060fstatus, s.f060fstudentCategory, sc.f011fdefinitionCode as studentCategory,s.f060fmatricNumber,s.f060ficNumber,s.f060fprogramCode, s.f060fpassword, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName, s.f060fdepartment")
          ->from('Application\Entity\T060fstudent','s')
          ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f060fstate = st.f012fid')
          ->leftjoin('Application\Entity\T013fcountry','c','with','s.f060fcountry = c.f013fid')
          ->leftjoin('Application\Entity\T068fprogramme','p','with','s.f060fprogramCode = p.f068fcode')
          ->leftjoin('Application\Entity\T067fintake', 'i', 'with','s.f060fidIntake = i.f067fname')
          ->leftjoin('Application\Entity\T053fsemester', 'sm', 'with','s.f060fidSemester = sm.f053fcode')
          ->leftjoin('Application\Entity\T011fdefinationms','sc','with', 's.f060fstudentCategory = sc.f011fid')
          ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
             ->where('s.f060fid = :semesterId')
            ->setParameter('semesterId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $student = new T060fstudent();

        $student->setF060femail($data['f060femail'])
                 ->setF060fname($data['f060fname'])
                 ->setF060fphoneNumber($data['f060fphoneNumber'])
                 ->setF060faddress($data['f060faddress'])
                 ->setF060fdepartment($data['f060fdepartment'])
                 ->setF060fcity($data['f060fcity'])
                 ->setF060fstate((int)$data['f060fstate'])
                 ->setF060fcountry((int)$data['f060fcountry'])
                 ->setF060fgender($data['f060fgender'])
                 ->setF060ftype($data['f060ftype'])
                 ->setF060fregion($data['f060fregion'])
                 ->setF060fstudentCategory((int)$data['f060fstudentCategory'])
                 ->setF060fstudyMode((int)$data['f060fstudyMode'])
                 ->setF060fcitizen((int)$data['f060fcitizen'])
                 ->setF060fidProgram($data['f060fidProgram'])
                 ->setF060fidIntake($data['f060fidIntake'])
                 ->setF060fmatricNumber($data['f060fmatricNumber'])
                 ->setF060fpassword(md5($data['f060fpassword']))
                 ->setF060ficNumber($data['f060ficNumber'])
                 ->setF060fidSemester($data['f060fidSemester'])  
                 ->setF060fstatus((int)$data['f060fstatus'])
                 ->setF060fprogramCode($data['f060fprogramCode'])
                 ->setF060fcreatedBy((int)$_SESSION['userId'])
                 ->setF060fupdatedBy((int)$_SESSION['userId']);

        $student->setF060fcreatedDtTm(new \DateTime())
                ->setF060fupdatedDtTm(new \DateTime());

 
        $em->persist($student);
        $em->flush();
// print_r($student);
// die();
        return $student;

    }

    public function updateData($student, $data = []) 
    {
        $em = $this->getEntityManager();


        $student->setF060femail($data['f060femail'])
                 ->setF060fname($data['f060fname'])
                 ->setF060fphoneNumber($data['f060fphoneNumber'])
                 ->setF060faddress($data['f060faddress'])
                 ->setF060fdepartment($data['f060fdepartment'])
                 ->setF060fcity($data['f060fcity'])
                 ->setF060fstate((int)$data['f060fstate'])
                 ->setF060fcountry((int)$data['f060fcountry'])
                 ->setF060fgender($data['f060fgender'])
                 ->setF060ftype($data['f060ftype'])
                 ->setF060fregion($data['f060fregion'])
                 ->setF060fstudentCategory((int)$data['f060fstudentCategory'])
                 ->setF060fstudyMode((int)$data['f060fstudyMode'])
                 ->setF060fcitizen((int)$data['f060fcitizen'])
                 ->setF060fidProgram($data['f060fidProgram'])
                 ->setF060fidIntake($data['f060fidIntake'])
                 ->setF060fmatricNumber($data['f060fmatricNumber'])
                 ->setF060ficNumber($data['f060ficNumber'])
                 ->setF060fidSemester($data['f060fidSemester'])  
                 ->setF060fstatus((int)$data['f060fstatus'])
                 ->setF060fprogramCode($data['f060fprogramCode'])
                 ->setF060fupdatedBy((int)$_SESSION['userId']);

        $student->setF060fupdatedDtTm(new \DateTime());

 
        $em->persist($student);
        $em->flush();

    }

    public function getStudent($postData)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

      // print_r($postData);
      // die();
         $qb->select("s.f060fid, s.f060femail,s.f060ftype, s.f060fname, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fidProgram,  s.f060fidIntake, s.f060fidSemester, s.f060fstatus, s.f060fcitizen, s.f060fregion, s.f060fstudentCategory,s.f060fprogramCode,s.f060fmatricNumber")
            ->from('Application\Entity\T060fstudent','s');
            // ->leftjoin('Application\EstudentInvoiceDetailntity\T071finvoice','inv','with', 's.f060fid = inv.f071fidCustomer')
            // ->where("inv.f071finvoiceType = ':STD'");

        if (! empty($postData['graduationType'])) 
        {
            $qb->where("s.f060ftype = '".$postData['graduationType']."'");
        }

        if (! empty($postData['program'])) {
            $qb->andWhere("s.f060fprogramCode ='".$postData['program']."'");
        }

        if (! empty($postData['student'])) {
            $qb->andWhere("s.f060fid ='".$postData['student']."'");
        }

        if (! empty($postData['region'])) {
            $qb->andWhere("s.f060fregion ='".$postData['region']."'");
        }

        if (! empty($postData['intake'])) {
            $qb->andWhere("s.f060fidIntake ='".$postData['intake']."'");
        }

        if (! empty($postData['citizen'])) {
            $qb->andWhere('s.f060fcitizen = :citizenId')
            ->setParameter('citizenId',(int)$postData['citizen']);
        }

      // echo $qb;
      // die();

        $query = $qb->getQuery(); 
        
        $result = $query->getArrayResult();
        // $conditions = array();
        //   if($postData['graduatiostudentInvoiceDetailnType'] == 'UG'){
        //     $query = "select * from vw_ug_t210student";

        //      if (!empty($postData['intake'])) {
        //    $intake = $postData['intake'];

        //    $query1 = " f210sesi_masuk = '$intake'";
        //    array_push($conditions, $query1);
        //    //  $qb->andWhere('s.f060fidIntake = :intakeId')
        //    //     ->setParameter('intakeId', $postData['intake']);
        //     }
        //     if (! empty($postData['program'])) {
        //     $program = $postData['program'];
        //     $query1 = " f210kdprogram = '$program'";
        //    array_push($conditions, $query1);

        //     // $qb->andWhere('s.f060fidProgram = :programId')
        //     //    ->setParameter('programId', $postData['program']);
        //     }
        //      if (! empty($postData['student'])) {
        //     $student = $postData['student'];
        //     $query1 = " f210nokp = '$student'";
        //    array_push($conditions, $query1);

        //     }
        //       if (! empty($postData['region'])) {
        //     $region = $postData['region'];
        //     $query1 = " f210warga = '$region'";
        //    array_push($conditions, $query1);

        //     }
        //     $query = $query." where ".implode(" and", $conditions);
        //     // echo $query;
        //     // die();
        //     // if (! empty($postData['region'])) {
        //     // $student = $postData['f064fidStudent'];
        //     // $query = $query." where f210warga = '$student'";
        //     // }
        // }
        // else{
        // $conditions = array();
        //     $query = "select * from vw_pg_t801studmas";
        //       if (!empty($postData['f048fidIntake'])) {
        //    $intake = $postData['f048fidIntake'];
        //    $query1 = " f210sesi_masuk = '$intake'";
        //    array_push($conditions, $query1);

        //    //  $qb->andWhere('s.f060fidIntake = :intakeId')
        //    //     ->setParameter('intakeId', $postData['intake']);
        //     }
        //     if (! empty($postData['f048fidProgramme'])) {
        //     $program = $postData['f048fidProgramme'];
        //     $query1 = " f210kdprogram = '$program'";
        //    array_push($conditions, $query1);

        //     // $qb->andWhere('s.f060fidProgram = :programId')
        //     //    ->setParameter('programId', $postData['program']);
        //     }
        //      if (! empty($postData['f064fidStudent'])) {
        //     $student = $postData['f064fidStudent'];
        //     $query1 = " f210nokp = '$student'";
        //    array_push($conditions, $query1);

        //     }
        //     $query = $query." where ".implode(" and", $conditions);
            
        // }
        // $result = $em->getConnection()->executeQuery($query)->fetchAll();
       
        
        


        return $result;

    }
    
    public function getRefundableStudent($postData)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $query = "select s.f060fid, s.f060femail,s.f060ftype, s.f060fname, s.f060fphone_number, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstate_name as stateName, s.f060fcountry, c.f013fcountry_name as countryName, s.f060fgender, s.f060fid_program, p.f068fname as program, s.f060fid_intake, i.f067fname as intake, s.f060fid_semester, sm.f053fsemester as semester, s.f060fstatus,f.f066fname, f.f066fcode, f.f066frefundable, iv.f071fid, iv.f071finvoice_number, ivd.f072fid, ivd.f072ftotal, (dep.f015fdepartment_code+'-'+dep.f015fdepartment_name) as DepartmentName from t060fstudent as s inner join t015fdepartment as dep on dep.f015fdepartment_code = s.f060fdepartment inner join t012fstate as st on s.f060fstate = st.f012fid inner join t013fcountry as c on s.f060fcountry = c.f013fid inner join t068fprogramme as p on s.f060fprogram_code = p.f068fcode inner join t067fintake as i on s.f060fid_intake = i.f067fname inner join t053fsemester as sm on s.f060fid_semester = sm.f053fcode inner join t071finvoice as iv on s.f060fid = iv.f071fid_customer inner join t072finvoice_details as ivd  on iv.f071fid = ivd.f072fid_invoice inner join t066ffee_item as f  on f.f066fid = ivd.f072fid_item where iv.f071finvoice_type = 'STD' and f.f066frefundable = 1";
       
        if (! empty($postData['graduationType'])) 
        {
          $graduation_type = $postData['graduationType'];
          $query = $query." and s.f060ftype = '$graduation_type'";
        }

        if (! empty($postData['citizen'])) 
        {
            $citizen_id = $postData['citizen'];
            $query = $query." and s.f060fcitizen = $citizen_id";
            // $qb->andWhere('s.f060fcitizen = :citizenId')
            //    ->setParameter('citizenId', $postData['citizen']);
        }
        
        if (! empty($postData['intake'])) {
            $intake_id = $postData['intake'];
            $query = $query." and s.f060fid_intake = '$intake_id'";
            // $qb->andWhere('s.f060fidIntake = :intakeId')
            //    ->setParameter('intakeId', $postData['intake']);
        }
        
        if (! empty($postData['modeOfStudy'])) {
            $mode = $postData['modeOfStudy'];
            $query = $query." and s.f060fstudy_mode = $mode";
            // $qb->andWhere('s.f060fstudyMode = :studyId')
            //    ->setParameter('studyId', $postData['modeOfStudy']);
        }
        
        if (! empty($postData['program'])) {
            $program_id = $postData['program'];
            $query = $query." and s.f060fprogram_code = '$program_id'";
            // $qb->andWhere('s.f060fidProgram = :programId')
            //    ->setParameter('programId', $postData['program']);
        }

        if (! empty($postData['student'])) {
            $student_id = $postData['student'];
            $query = $query." and s.f060fmatric_number = '$student_id'";
            // $qb->andWhere('s.f060fid = :studId')
            //    ->setParameter('studId', $postData['student']);
        }
        $query = $query." and ivd.f072fid not in (select f065fid_invoice_details from t065fstudent_bond_information)";

// echo $query;
// die();
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        // $query = $qb->getQuery();
        
        // $result = $query->getArrayResult();

        return $result;

    }
    public function sponserStudents($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("s.f060fid, s.f060femail, s.f060fname,s.f060ftype, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstateName as stateName, s.f060fcountry, c.f013fcountryName as countryName, s.f060fgender, s.f060fidProgram, p.f068fname as program, s.f060fidIntake, i.f067fname as intake, s.f060fidSemester, sm.f053fsemester as semester, s.f060fstatus, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
          ->from('Application\Entity\T060fstudent','s')
          ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f060fstate = st.f012fid')
          ->leftjoin('Application\Entity\T013fcountry','c','with','s.f060fcountry = c.f013fid')
          ->leftjoin('Application\Entity\T068fprogramme','p','with','s.f060fprogramCode = p.f068fcode')
          ->leftjoin('Application\Entity\T067fintake', 'i', 'with','s.f060fidIntake = i.f067fname')
          ->leftjoin('Application\Entity\T053fsemester', 'sm', 'with','s.f060fidSemester = sm.f053fcode')
          ->leftjoin('Application\Entity\T064ftaggingSponsors', 'sp', 'with','sp.f064fidStudent = s.f060fid')
          ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
          ->where('sp.f064fidSponsor = :idSponsor')
          ->setParameter('idSponsor',$id);

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );
        
        return $result;
        
    }

    public function studentInvoices($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom,s.f060fname as studentName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
            ->from('Application\Entity\T071finvoice', 'i')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
            ->where('i.f071fprocessed = 0')
            ->andWhere("i.f071finvoistudentInvoiceDetailceType = 'STD'")
            ->andWhere('i.f071fidCustomer = :idStudent')
            ->setParameter('idStudent',$id);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;
        
    }

    public function studentIntakeBatch($postData) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("s.f060fid, s.f060femail, s.f060fname,s.f060ftype, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstateName as stateName, s.f060fcountry, c.f013fcountryName as countryName, s.f060fgender, s.f060fidProgram, p.f068fname as program, s.f060fidIntake, i.f067fname as intake, s.f060fidSemester, sm.f053fsemester as semester, s.f060fstatus, ts.f064fidSponsor, sp.f063fname as sponsor, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
          ->from('Application\Entity\T060fstudent','s')
          ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f060fstate = st.f012fid')
          ->leftjoin('Application\Entity\T013fcountry','c','with','s.f060fcountry = c.f013fid')
          ->leftjoin('Application\Entity\T068fprogramme','p','with','s.f060fprogramCode = p.f068fcode')
          ->leftjoin('Application\Entity\T067fintake', 'i', 'with','s.f060fidIntake = i.f067fname')
          ->leftjoin('Application\Entity\T053fsemester', 'sm', 'with','s.f060fidSemester = sm.f053fcode')
          ->leftjoin('Application\Entity\T064ftaggingSponsors', 'ts', 'with','ts.f064fidStudent = s.f060fid')
          ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
          ->leftjoin('Application\Entity\T063fsponsor', 'sp', 'with','ts.f064fidSponsor = sp.f063fid');
        
        if (! empty($postData['intake'])) {
            $qb->andWhere('s.f060fidIntake = :intakeId')
               ->setParameter('intakeId', $postData['intake']);
        }
        
        if (! empty($postData['semester'])) {
            $qb->andWhere('s.f060fidSemester = :semesterId')
               ->setParameter('semesterId', $postData['semester']);
        }

        if (! empty($postData['program'])) {
            $qb->andWhere('s.f060fidProgram = :programId')
               ->setParameter('programId', $postData['program']);
        }

        if (! empty($postData['sponsor'])) {
            $qb->andWhere('ts.f064fidSponsor = :sponsorId')
               ->setParameter('sponsorId', $postData['sponsor']);
        }
          

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );
        
        return $result;
        
    }

    public function taggingStudentSponsor($postdata) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $sponsor = new T064ftaggingSponsors();

        $sponsor->setF064fidSponsor((int)$postdata['sponsor'])
                ->setF064fidStudent((int)$postdata['student'])
                ->setF064fidSemester((int)$postdata['semester'])
                ->setF064fagreementNo(($postdata['agreementNo']))
                ->setF064fstatus((int)($postdata['status']))
                ->setF064fcreatedBy((int)$_SESSION['userId'])
                ->setF064fupdatedBy((int)$_SESSION['userId']);

        $sponsor->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());
        
        $em->persist($sponsor);
        $em->flush();
        
        return $sponsor;
    }
    public function studentInvoiceDetail($id) 
    {
        $em = $this->getEntityManager(); 

        $qb = $em->createQueryBuilder();

        $qb->select("i.f071fid, i.f071finvoiceNumber, i.f071finvoiceType, i.f071fbillType, s.f060fname as studentName, i.f071fdescription, i.f071fidCustomer, i.f071fidFinancialYear, i.f071finvoiceDate, i.f071fapprovedBy, i.f071fstatus, i.f071finvoiceTotal, i.f071fbalance, i.f071fisRcp, i.f071finvoiceFrom, i.f071fcrAmount, i.f071fdrAmount, i.f071ftotalPaid, i.f071fbalance, i.f071freason,i.f071fstatus, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName ")
            ->from('Application\Entity\T071finvoice', 'i')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
          ->where("i.f071finvoiceType = 'STD'")
          ->andWhere('i.f071fidCustomer = :id')
          ->setParameter('id',(int)$id);
        $query   = $qb->getQuery();
        $invoice  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        // Query
       $qb = $em->createQueryBuilder();
       $qb->select("v.f031fid, v.f031fidInvoice, v.f031ftype, v.f031fidCustomer, v.f031fdescription, v.f031fnarration, v.f031ftotalAmount, v.f031fbalance, v.f031freferenceNumber, v.f031fdate, v.f031fvoucherType, v.f031fvoucherNumber, v.f031fvoucherDays, v.f031fstatus , v.f031freason, v.f031fstatus as ApprovalStatus, i.f071finvoiceNumber, st.f060fname as fullName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
          ->from('Application\Entity\T031fcreditNote', 'v')
          ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'v.f031fidInvoice = i.f071fid')
          ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'v.f031fidCustomer = st.f060fid')
          ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = st.f060fdepartment')
          ->where("v.f031ftype = 'CSTD'")
          ->andWhere('v.f031fidCustomer = :id')
          ->setParameter('id',(int)$id);

        $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


      $qb = $em->createQueryBuilder();
      $qb->select("v.f031fid,v.f031fidInvoice,v.f031ftype,v.f031fidCustomer,v.f031fdescription,v.f031fnarration,v.f031ftotalAmount,v.f031fbalance,v.f031freferenceNumber,v.f031fdate,v.f031fvoucherType,v.f031fvoucherNumber,v.f031fvoucherDays, v.f031fstatus , v.f031freason, v.f031fstatus as ApprovalStatus, i.f071finvoiceNumber, st.f060fname as fullName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName ")
         ->from('Application\Entity\T031fdebitNote', 'v')
         ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'v.f031fidInvoice = i.f071fid')
         ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'v.f031fidCustomer = st.f060fid')
         ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = st.f060fdepartment')
         ->where("v.f031ftype = 'DSTD'")
         ->andWhere('v.f031fidCustomer = :id')
         ->setParameter('id',(int)$id);

      $debit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

      $qb = $em->createQueryBuilder();
      $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate,d.f024fapprovalStatus, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,st.f060fname as fullName,d.f024fapprovalStatus, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
        ->from('Application\Entity\T024fdiscount', 'd')
        ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
        ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'd.f024fidCustomer = st.f060fid')
        ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = st.f060fdepartment')
        ->where("d.f024ftype = 'DCSTD'")
        ->andWhere('d.f024fidCustomer = :id')
        ->setParameter('id',(int)$id);
                 
      $discount = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


      $qb = $em->createQueryBuilder();
      $qb->select("r.f082fid, r.f082freceiptNumber, r.f082fpayeeType, r.f082fidInvoice, r.f082fidCustomer, r.f082fpaymentBank, r.f082fdate, r.f082ftotalAmount, r.f082fdescription,r.f082fstatus as approvalStatus, st.f060fname as fullName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
          ->from('Application\Entity\T082freceipt', 'r')
          ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'r.f082fidCustomer = st.f060fid')
          ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = st.f060fdepartment')
          ->where("r.f082fpayeeType='RSTD'")
          ->andWhere('r.f082fidCustomer = :id')
          ->setParameter('id',(int)$id);

      $receipt = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


      $qb = $em->createQueryBuilder();
      $qb->select("a.f020fid, a.f020fidCustomer, a.f020fdocumentNumber, a.f020famount , a.f020fidNumber, a.f020ftype, a.f020fbalanceAmount, s.f060fname, a.f020fpaymentStatus, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
         ->from('Application\Entity\T020fadvancePayment', 'a')
         ->leftjoin('Application\Entity\T060fstudent','s','with', 'a.f020fidCustomer = s.f060fid')
         ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
         // ->leftjoin('Application\Entity\T021fadvancePaymentDetails','ad','with', 'ad.f021fidAdvance = a.f020fid')
          // ->where("a.f020ftype='RSTD'")
          ->where('a.f020fid = :id')
          ->setParameter('id',(int)$id);
      $query = $qb->getQuery();
      $advancePayment  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


      $query = "select s.f060fid, s.f060femail, s.f060fname, s.f060fphone_number, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstate_name as stateName, s.f060fcountry, c.f013fcountry_name as countryName, s.f060fgender, s.f060fid_program, p.f068fname as program, s.f060fid_intake, i.f067fname as intake, s.f060fid_semester, sm.f053fsemester as semester, s.f060fstatus,f.f066fname, f.f066fcode, f.f066frefundable, iv.f071fid, iv.f071finvoice_number, ivd.f072fid, ivd.f072ftotal, (dep.f015fdepartment_code+'-'+dep.f015fdepartment_name) as DepartmentName from t060fstudent as s inner join t015fdepartment as dep on dep.f015fdepartment_code = s.f060fdepartment inner join t012fstate as st on s.f060fstate = st.f012fid inner join t013fcountry as c on s.f060fcountry = c.f013fid inner join t068fprogramme as p on s.f060fprogram_code = p.f068fcode inner join t067fintake as i on s.f060fid_intake = i.f067fname inner join t053fsemester as sm on s.f060fid_semester = sm.f053fcode inner join t071finvoice as iv on s.f060fid = iv.f071fid_customer inner join t072finvoice_details as ivd  on iv.f071fid = ivd.f072fid_invoice inner join t066ffee_item as f  on f.f066fid = ivd.f072fid_item where iv.f071finvoice_type = 'STD' and f.f066frefundable = 1 and iv.f071fid_customer = $id";

      $refund = $em->getConnection()->executeQuery($query)->fetchAll();
      // print_r($refund);exit;
      $result = $result[0];
       
      $result['invoice'] = $invoice;
      $result['credit'] = $credit;
      $result['debit'] = $debit;
      $result['discount'] = $discount;
      $result['receipt'] = $receipt;
      $result['advance_payment'] = $advancePayment;
      $result['refund'] = $refund;
      return $result;
        
    }

    public function studentInvoicesDetail($postData)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

      // print_r($postData);
      // die();
         $qb->select("s.f060fid, s.f060femail,s.f060ftype, s.f060fname, s.f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, st.f012fstateName as stateName, s.f060fcountry, c.f013fcountryName as countryName, s.f060fgender, s.f060fidProgram, p.f068fname as program, s.f060fidIntake, i.f067fname as intake, s.f060fidSemester, sm.f053fsemester as semester, s.f060fstatus, s.f060fcitizen, d.f011fdefinitionCode as citizen, s.f060fregion, s.f060fstudentCategory, sc.f011fdefinitionCode as studentCategory, s.f060fstudyMode, m.f011fdefinitionCode as studyMode, ( s.f060fmatricNumber+'-'+s.f060fname) as Student,s.f060fprogramCode,s.f060fmatricNumber, inv.f071finvoiceNumber, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName")
            ->from('Application\Entity\T060fstudent','s')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with','dep.f015fdepartmentCode = s.f060fdepartment')
            ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f060fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with','s.f060fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T068fprogramme','p','with','s.f060fprogramCode = p.f068fcode')
            ->leftjoin('Application\Entity\T067fintake', 'i', 'with','s.f060fidIntake = i.f067fname')
            ->leftjoin('Application\Entity\T053fsemester', 'sm', 'with','s.f060fidSemester = sm.f053fcode')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with', 's.f060fcitizen = d.f011fid')
            // ->leftjoin('Application\Entity\T011fdefinationms','r','with', 's.f060fregion = r.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','sc','with', 's.f060fstudentCategory = sc.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','m','with', 's.f060fstudyMode = m.f011fid')
            ->leftjoin('Application\Entity\T071finvoice','inv','with', 's.f060fid = inv.f071fidCustomer')
            ->where("inv.f071finvoiceType = ':STD'");

        if (! empty($postData['graduationType'])) 
        {
            $qb->where("s.f060ftype = '".$postData['graduationType']."'");
        }

        if (! empty($postData['program'])) {
            $qb->andWhere("s.f060fprogramCode ='".$postData['program']."'");
        }

        if (! empty($postData['student'])) {
            $qb->andWhere("s.f060fmatricNumber ='".$postData['student']."'");
        }

        if (! empty($postData['region'])) {
            $qb->andWhere("s.f060fregion ='".$postData['region']."'");
        }

        if (! empty($postData['intake'])) {
            $qb->andWhere("s.f060fidIntake ='".$postData['intake']."'");
        }

        if (! empty($postData['citizen'])) {
            $qb->andWhere('s.f060fcitizen = :citizen')
            ->setParameter('citizen',$postData['citizen']);
        }

      // echo $qb;
      // die();

        $query = $qb->getQuery(); 
        $result = $query->getArrayResult();
        return $result;

    }

  public function getStudentByStatus($id) 
    {
        $em = $this->getEntityManager(); 

        $qb = $em->createQueryBuilder();

        // Query
         $query = "select top 50 s.f060fid, s.f060femail, (cast(s.f060fmatric_number as varchar(30))+'-'+s.f060fname) as f060fname ,s.f060ftype, s.f060fphone_number as f060fphoneNumber, s.f060faddress, s.f060fcity, s.f060fstate, s.f060fcountry, s.f060fgender, s.f060fid_program as f060fidProgram, s.f060fid_intake as f060fidIntake, s.f060fid_semester as f060fidSemester, s.f060fstatus, s.f060fstudent_category as f060fstudentCategory,s.f060fmatric_number as f060fmatricNumber,s.f060fic_number as f060ficNumber,s.f060fprogram_code as f060fprogramCode from vw_all_students as s where s.f060fstatus = $id";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        $result = array(
            'data' => $result,
        );
        
        return $result;
        
    }



}
