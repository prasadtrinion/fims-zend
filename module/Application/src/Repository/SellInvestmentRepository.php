<?php
namespace Application\Repository;

use Application\Entity\T070fsellInvestment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class SellInvestmentRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
        ->from('Application\Entity\T070fsellInvestment','r')
        ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
        ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
        ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
          ->from('Application\Entity\T070fsellInvestment','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
          ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
          ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
             ->where('r.f070fid = :investmentId')
            ->setParameter('investmentId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function getListByApprovedOneStatus($approvedStatus)
    {
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();
        
    // echo("hi");
    // exit;
           if($approvedStatus == 0)
           {
                $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
          ->from('Application\Entity\T070fsellInvestment','r')
          ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
          ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
          ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
                 ->where('r.f070fapproved1Status = 0');
                 // ->andwhere('it.f063fis_online!=1');
            }

            elseif($approvedStatus == 1)
            {
                $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
                ->from('Application\Entity\T070fsellInvestment','r')
                ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
                ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
                ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
                 ->where('r.f070fapproved1Status = 1');
         ;
            }

            elseif($approvedStatus == 2) 
            {
                $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
                ->from('Application\Entity\T070fsellInvestment','r')
                ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
                ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
                ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid');
          
          
            }
          
    $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
    return $data;
    
  }
  public function getListByApprovedTwoStatus($approvedStatus)
    {
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();
        
    // echo("hi");
    // exit;
           if($approvedStatus == 0)
           {
            $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
            ->from('Application\Entity\T070fsellInvestment','r')
            ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
            ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
            ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
                 ->where('r.f070fapproved2Status = 0')
                 ->andWhere('r.f070fapproved1Status!=0');
              
            }

            elseif($approvedStatus == 1)
            {
                $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
                ->from('Application\Entity\T070fsellInvestment','r')
                ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
                ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
                ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
                 ->where('r.f070fapproved2Status = 1');
         
            }

            elseif($approvedStatus == 2) 
            {
                $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
                ->from('Application\Entity\T070fsellInvestment','r')
                ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
                ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
                ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid');
            }
          
    $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
    return $data;
    
  }

    public function createInvestment($postdata) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        foreach($postdata as $data)

        {

            $id = $data['id'];
            // print_r($id);
            // exit();
            
            $query1 = "UPDATE t063finvestment_application set   f063fis_online='1'where f063fid = '$id'"; 
            $result=$em->getConnection()->executeQuery($query1);

            $sellInvestment = new T070fsellInvestment();

            $sellInvestment->setF070fidApplication((int)$data)
                 ->setF070fapproved1Status((int)"0")
                 ->setF070fapproved2Status((int)"0")
                 ->setF070fapprove1Reason($data['f070fapprove1Reason'])
                 ->setF070fapprove2Reason($data['f070fapprove2Reason'])
                 ->setF070fapproved1By((int)$data['f070fapproved1By'])
                 ->setF070fapproved2By((int)$data['f070fapproved2By'])
                 ->setF070fapproved1UpdatedBy((int)$data['f070fapproved1UpdatedBy'])
                 ->setF070fapproved2UpdatedBy((int)$data['f070fapproved2UpdatedBy'])
                 ->setF070fstatus((int)"1")
                 ->setF070fcreatedBy((int)$_SESSION['userId'])
                 ->setF070fupdatedBy((int)$_SESSION['userId']);

        $sellInvestment->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());

        try
        {
        $em->persist($sellInvestment);
        $em->flush();
    }
    catch(\Exception $e){
            echo $e;
        }
        // print_r($sellInvestment);
        // exit();
    }
        return $sellInvestment;


    }

    public function updateData($sellInvestment, $data = []) 
    {
        $em = $this->getEntityManager();


        $sellInvestment->setF070fidApplication((int)$data['f070fidApplication'])
                 ->setF070fapproved1Status((int)"0")
                 ->setF070fapproved2Status((int)"0")
                 ->setF070fapprove1Reason((int)$data['f070fapprove1Reason'])
                 ->setF070fapprove2Reason((int)$data['f070fapprove2Reason'])
                 ->setF070fapproved1By((int)$data['f070fapproved1By'])
                 ->setF070fapproved2By((int)$data['f070fapproved2By'])
                 ->setF070fapproved1UpdatedBy((int)$data['f070fapproved1UpdatedBy'])
                 ->setF070fapproved2UpdatedBy((int)$data['f070fapproved2UpdatedBy'])
                 ->setF070fcreatedBy((int)$_SESSION['userId'])
                 ->setF070fupdatedBy((int)$_SESSION['userId']);

        $sellInvestment->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());

 
        $em->persist($sellInvestment);
        $em->flush();

        return $sellInvestment;

    }

    public function approveOneApplication($applications) 
    {
        $em = $this->getEntityManager();

        foreach ($applications as $application)
        {

            $application->setF070fapproved1Status((int)"1")
                        ->setF070fapproved1By((int)"1");
       
            $em->persist($application);
            $em->flush();
        }
    }

    public function approveTwoApplication($applications) 
    {
        $em = $this->getEntityManager();

        foreach ($applications as $application)
        {

            $application->setF070fapproved2Status((int)"1")
                        ->setF070fapproved2By((int)"1");
       
            $em->persist($application);
            $em->flush();
        }
    }

    public function rejectOneReason($applications,$reason)
    {
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();
        
        foreach ($applications as $application) 
        {

            $id = $application->getF070fid();
            
            $query1 = "UPDATE t070fsell_investment set f070fapprove1_reason='$reason',f070fapproved1_status='2' where f070fid = '$id'"; 
            $result=$em->getConnection()->executeQuery($query1);

            
        }

        return $result;
    }

    public function rejectTwoReason($applications,$reason)
    {
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();

        foreach ($applications as $application) 
        {

            $id = $application->getF070fid();
            
            $query1 = "UPDATE t070fsell_investment set f070fapprove2_reason='$reason',f070fapproved2_status='2' where f070fid = '$id'"; 
            $result=$em->getConnection()->executeQuery($query1);
        }

        return $result;
    }

    public function getSellListApproved() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f070fid,r.f070fidApplication,r.f070fapproved1Status, r.f070fapproved2Status, r.f070fapprove1Reason, r.f070fapprove2Reason, r.f070fapproved1By, r.f070fapproved1UpdatedBy,r.f070fapproved2UpdatedBy, it.f063fid, it.f063fdescription, it.f063finvestmentAmount, itd.f063fduration, itd.f063finstitutionId, i.f061fname as institutionName,  itd.f063fprofitRate, it.f063fstatus, it.f063fapproved1Status, it.f063freason1, it.f063fapproved1By, it.f063fapproved1UpdatedBy')
        ->from('Application\Entity\T070fsellInvestment','r')
        ->leftjoin('Application\Entity\T063fapplicationMaster', 'it','with','r.f070fidApplication= it.f063fid')
        ->leftjoin('Application\Entity\T063fapplicationDetails', 'itd','with','itd.f063fidMaster= it.f063fid')
        ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','itd.f063finstitutionId= i.f061fid')
          ->where('r.f070fapproved1Status=1')
          ->andWhere('r.f070fapproved2Status=1');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        
        return $result;
        
    }
}

?>