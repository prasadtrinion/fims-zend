<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Application\Entity\T081fsalary;
use Application\Entity\T081fsalaryDetails;
use Application\Entity\T082fmonthlyDeductionMaster;
use Application\Entity\T082fmonthlyDeductionDetail;
use Zend\View\Model\JsonModel;

class SalaryCalculationRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    
    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('s.f081fid, s.f081fcode, s.f081fname, s.f081fshortName, s.f081ftypeOfComponent, s.f081fmode, s.f081ffrequency, s.f081fsalaryType, s.f081faccountsCodeDeduction, s.f081faccountsCodeCr, s.f081fpfDeductable, s.f081fsocsoDeductable, s.f081ftaxDeductable, s.f081feisDeductable, s.f081fvendorCode')
            ->from('Application\Entity\T081fsalary', 's')
            ->orderBy('s.f081fid','DESC');

        $query = $qb->getQuery();
    
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('s.f081fid, s.f081fcode, s.f081fname, s.f081fshortName, s.f081ftypeOfComponent, s.f081fmode, s.f081ffrequency, s.f081fsalaryType, s.f081faccountsCodeDeduction, s.f081faccountsCodeCr, s.f081fpfDeductable as f081fepfDeductable, s.f081fsocsoDeductable, s.f081ftaxDeductable, s.f081feisDeductable, s.f081fvendorCode, sd.f081fidDetails, sd.f081faccountCodeDebit, sd.f081faccountCodeCredit, sd.f081fstaffStatus, sd.f081fstaffType')
            ->from('Application\Entity\T081fsalary', 's')
            ->leftjoin('Application\Entity\T081fsalaryDetails', 'sd', 'with', 's.f081fid = sd.f081fidSalary')
            ->where('s.f081fid = :salaryId')
            ->setParameter('salaryId',$id);
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
        return $result;
    }

    public function createSalary($data)
    {
    
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $salary = new T081fsalary();

        $salary->setF081fcode($data['f081fcode'])
            ->setF081fname($data['f081fname'])
            ->setF081fshortName($data['f081fshortName'])
            ->setF081ftypeOfComponent((int)$data['f081ftypeOfComponent'])
            ->setF081fmode((int)$data['f081fmode'])
            ->setF081ffrequency((int)$data['f081ffrequency'])
            ->setF081fsalaryType((int)$data['f081fsalaryType'])
            ->setF081faccountsCodeDeduction($data['f081faccountsCodeDeduction'])
            ->setF081faccountsCodeCr($data['f081faccountsCodeCr'])
            ->setF081fpfDeductable($data['f081fepfDeductable'])
            ->setF081fsocsoDeductable($data['f081fsocsoDeductable'])
            ->setF081ftaxDeductable($data['f081ftaxDeductable'])
            ->setF081feisDeductable($data['f081feisDeductable'])
            ->setF081fvendorCode($data['f081fvendorCode'])
            ->setF081fstatus((int)$data['f081fstatus'])
            ->setF081fcreatedBy((int)$_SESSION['userId'])
            ->setF081fupdatedBy((int)$_SESSION['userId']);

        $salary->setF081fcreatedDtTm(new \DateTime())
            ->setF081fupdatedDtTm(new \DateTime());
     try{
        $em->persist($salary);

        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
        $salaryDetails = $data['salary-details'];

        foreach ($salaryDetails as $salaryDetail)
        {

            $salaryDetailObj = new T081fsalaryDetails();

            $salaryDetailObj->setF081fidSalary((int)$salary->getF081fid())
                   ->setF081faccountCodeDebit($salaryDetail['f081faccountCodeDebit'])
                   ->setF081faccountCodeCredit($salaryDetail['f081faccountCodeCredit'])
                   ->setF081fstaffStatus((int)$salaryDetail['f081fstaffStatus'])
                   ->setF081fstaffType((int)$salaryDetail['f081fstaffType'])
                   ->setF081fstatus((int)$salaryDetail['f081fstatus'])
                   ->setF081fcreatedBy((int)$_SESSION['userId'])
                   ->setF081fupdatedBy((int)$_SESSION['userId']);
                
            $salaryDetailObj->setF081fcreatedDtTm(new \DateTime())
                            ->setF081fupdatedDtTm(new \DateTime());
            
        try{
            $em->persist($salaryDetailObj);
        
            $em->flush();
            }
            catch(\Exception $e)
            {
            echo $e;
            }
        }
        return $salary;
  }

    public function updateSalary($salary, $data = [])
    {
        $em = $this->getEntityManager();

         $salary->setF081fcode($data['f081fcode'])
            ->setF081fname($data['f081fname'])
            ->setF081fshortName($data['f081fshortName'])
            ->setF081ftypeOfComponent((int)$data['f081ftypeOfComponent'])
            ->setF081fmode((int)$data['f081fmode'])
            ->setF081ffrequency((int)$data['f081ffrequency'])
            ->setF081fsalaryType((int)$data['f081fsalaryType'])
            ->setF081faccountsCodeDeduction($data['f081faccountsCodeDeduction'])
            ->setF081faccountsCodeCr($data['f081faccountsCodeCr'])
            ->setF081fpfDeductable($data['f081fepfDeductable'])
            ->setF081fsocsoDeductable($data['f081fsocsoDeductable'])
            ->setF081ftaxDeductable($data['f081ftaxDeductable'])
            ->setF081feisDeductable($data['f081feisDeductable'])
            ->setF081fvendorCode($data['f081fvendorCode'])
            ->setF081fstatus((int)$data['f081fstatus'])
            ->setF081fupdatedBy((int)$_SESSION['userId']);

        $salary->setF081fupdatedDtTm(new \DateTime());
     try{
        $em->persist($salary);

        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
        return $salary;
    }


    public function updateDetail($salaryDetailObj, $salaryDetail)
    {
        
      $em = $this->getEntityManager();
      $salaryDetailObj->setF081faccountCodeDebit($salaryDetail['f081faccountCodeDebit'])
                   ->setF081faccountCodeCredit($salaryDetail['f081faccountCodeCredit'])
                   ->setF081fstaffType((int)$salaryDetail['f081fstaffType'])
                   ->setF081fstaffStatus((int)$salaryDetail['f081fstaffStatus'])
                   ->setF081fstatus((int)"1")
                   ->setF081fcreatedBy((int)$_SESSION['userId']);
                
            $salaryDetailObj->setF081fupdatedDtTm(new \DateTime());
            
        try{
                $em->persist($salaryDetailObj);
        
                $em->flush();
                    }
    catch(\Exception $e){
        echo $e;
    }
                
    }
    
    public function createDetail($salaryObj, $salaryDetail)
    {
        
        $em = $this->getEntityManager();

       $salaryDetailObj = new T081fsalaryDetails();

        $salaryDetailObj->setF081fidSalary((int)$salaryObj->getF081fid())
                   ->setF081faccountCodeDebit($salaryDetail['f081faccountCodeDebit'])
                   ->setF081faccountCodeCredit($salaryDetail['f081faccountCodeCredit'])
                   ->setF081fstaffType((int)$salaryDetail['f081fstaffType'])
                   ->setF081fstaffStatus((int)$salaryDetail['f081fstaffStatus'])
                   ->setF081fstatus((int)"1")
                   ->setF081fcreatedBy((int)$_SESSION['userId'])
                   ->setF081fupdatedBy((int)$_SESSION['userId']);
                
            $salaryDetailObj->setF081fcreatedDtTm(new \DateTime())
                            ->setF081fupdatedDtTm(new \DateTime());
            
        try{
                $em->persist($salaryDetailObj);
        
                $em->flush();
                    }
    catch(\Exception $e)
    {
        echo $e;
    }

        return $salaryDetailObj;
    
    }





    public function getTaxDetails() 
    {
        $em = $this->getEntityManager();
         $query  = "select * from vw_ug_t210student ";
            $taxDetailsResult = $em->getConnection()->executeQuery($query)->fetchAll();


        
    }

    public function fetchDataFromView($data) 
    {

        $em = $this->getEntityManager();
        $month = $data['month'];
//         $process_id = $data['process_id'];
//         $query = "delete from t104femployee_details where financialperiod = '$month' and process_id = $process_id";
//         $em->getConnection()->executeQuery($query);

//          $query = "insert into t104femployee_details (employeeid,employeefirstname,employeemiddlename,employeelastname,employeedateofjoining,employeetype,employeetypedesc,employeecategory,employeecategorydesc,employeedepartment,employeedesignation,employeedesignationname,employeegradeid,employeegradename,employeepaymenttype,employeeaccountbank,employeeaccountnumber,employeetaxstatus,taxmaritalstatus,spouseworking,stafftype,stafftypedesc,dateofbirth,endofservice,confirmationstatus,idsalarycomponent,currency,amount,financialperiod,financialyear,process_id) select vw_employee.employeeid,vw_employee.employeefirstname,vw_employee.employeemiddlename,vw_employee.employeelastname,vw_employee.employeedateofjoining,vw_employee.employeetype,vw_employee.employeetypedesc,vw_employee.employeecategory,vw_employee.employeecategorydesc,vw_employee.employeedepartment,cast (vw_employee.employeedesignation as int),vw_employee.employeedesignationname,vw_employee.employeegradeid,vw_employee.employeegradename,vw_employee.employeepaymenttype,vw_employee.employeeaccountbank,vw_employee.employeeaccountnumber,vw_employee.employeetaxstatus,vw_employee.taxmaritalstatus,vw_employee.spouseworking,vw_employee.stafftype,vw_employee.stafftypedesc,vw_employee.dateofbirth,vw_employee.endofservice,vw_employee.confirmationstatus,vw_employee_salary.idsalarycomponent,vw_employee_salary.currency,cast(vw_employee_salary.amount as int),vw_employee_salary.financialperiod,cast(vw_employee_salary.financialyear as int),cast(vw_employee.stafftype as int) from  vw_employee  inner join vw_employee_salary on vw_employee.employeeid = vw_employee_salary.employeeid where vw_employee_salary.financialperiod = '$month' and vw_employee.stafftype = '$process_id' and vw_employee.employeeid < 600";
//          try{
//     $em->getConnection()->executeQuery($query);
// }
// catch(\Exception $e){
//     echo $e;
// }
// $this->insertToMonthlyPayroll('106');
        $staffs = $data['staff'];
        // $staffs = array('1190','1180','1182','1183');
        foreach ($staffs as $staff) {
                $this->epfCalculation($staff,$month);
                $this->socsoCalculation($staff,$month);
        }
// $this->epfCalculation('1190');
// $this->socsoCalculation('2840');
die();
return;
}
   public function insertToMonthlyPayroll($staffId) 
    {

        $em = $this->getEntityManager();
 
		$current_month = date('M');
		$query = "select distinct(f034fstaff_id),f034fname,employeedepartment,employeetype,employeeaccountbank,employeeaccountnumber,process_id,financialperiod as month from t034fstaff_master inner join t104femployee_details on f034fstaff_id = cast(employeeid as varchar(20)) where f034fstaff_id NOT IN (select distinct(MP_STAFF_ID) from t106fmonthly_payroll) and f034fstaff_id = '$staffId'";
    $result = $em->getConnection()->executeQuery($query)->fetchAll();
       
        for ($i=0;$i<count($result);$i++) {
            $staff = (int)$result[$i]['f034fstaff_id'];
            $query = "select employeeid,financialperiod as month,idsalarycomponent,amount,process_id from t104femployee_details where employeeid = $staff";
            $result1 = $em->getConnection()->executeQuery($query)->fetchAll();
            $result[$i]['details'] = $result1;
        }
        for ($i=0;$i<count($result);$i++) {
            $staff = (int)$result[$i]['f034fstaff_id'];
            $query = "select f082fdeduction_code,f082famount from t082fmonthly_deduction_detail inner join t082fmonthly_deduction_master on f082fid = f082fid_master where f082fid_staff = $staff";
            $result1 = $em->getConnection()->executeQuery($query)->fetchAll();
            $result[$i]['deduction-details'] = $result1;
        }
    foreach ($result as $data) {
        $month = $data['month'];
        $staff_id = $data['f034fstaff_id'];
        $staff_name = $data['f034fname'];
        $department = $data['employeedepartment'];
        $type = $data['employeetype'];
        $bank = $data['employeeaccountbank'];
        $account_number = $data['employeeaccountnumber'];
        $process_type = $data['process_id'];
        $query = "insert into t106fmonthly_payroll(MP_PAY_MONTH,MP_STAFF_ID,MP_DEPT_CODE,MP_STAFF_TYPE,MP_STAFF_BANK_CODE,MP_STAFF_BANK_ACC_NO,MP_PROCESS_TYPE) values('$month','$staff_id','$department','$type','$bank','$account_number',$process_type)";
        $em->getConnection()->executeQuery($query);

        $staff_details = $data['details'];
        $totalamount = 0;
        foreach ($staff_details as $row) {
            $salarycomponent = $row['idsalarycomponent'];
            $amount = $row['amount'];
            $totalamount = $totalamount + (float)$amount;
            $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month','$staff_id','$salarycomponent',$amount,$process_type)";
            $em->getConnection()->executeQuery($query);

        }
        $staff_details = $data['deduction-details'];
        // $totalamount = 0;
        foreach ($staff_details as $row) {
            $salarycomponent = $row['f082fdeduction_code'];
            $amount = $row['f082famount'];
            $totalamount = $totalamount + (float)$amount;
            $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month','$staff_id','$salarycomponent',$amount,$process_type)";
            $em->getConnection()->executeQuery($query);

        }
        $query = "update t106fmonthly_payroll set MP_TOTAL_ALLOWANCE = $totalamount";
        $em->getConnection()->executeQuery($query);
    }      
        return; 
    
    }

    // public function epfCalculation($staffId,$month){
     public function epfCalculation($staffId,$month){
        // print_r("hello");
        // exit();
        $em = $this->getEntityManager();
        // $staffId=$postData['staff'];
        // $month=$postData['month'];
       $id_staff  = $staffId;
       echo "-----------------EPF Calculation Begin------------------\n";
       echo "Staff Id: ".$id_staff."\n";
        // $query = "delete from t107fmonthly_payroll_details where SUBSTRING(MPD_INCOME_CODE,1,1)='D' or SUBSTRING(MPD_INCOME_CODE,1,1)='E'";
        // $em->getConnection()->executeQuery($query);
       $query = "select * from t107fmonthly_payroll_details inner join t081fsalary on MPD_INCOME_CODE = f081fcode where  f081fpf_deductable = '1' and MPD_STAFF_ID = '$id_staff' and MPD_PAY_MONTH='$month'";
        $pf_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        if($pf_result){
        $tmp_amount = 0;
        for ($i=0; $i <count($pf_result) ; $i++) { 
            $tmp_amount = (float)$tmp_amount  + (float)$pf_result[$i]['MPD_PAID_AMT'];
        }
        echo "Total Earning Components Amount: ".$tmp_amount."\n";
            $query = "select f030femployer_contribution,f030femployer_deduction from t030fepf_setup where  cast(f030fstart_amount as int) <= $tmp_amount and cast(f030fend_amount as int) >= $tmp_amount";
        $pf_setup_result = $em->getConnection()->executeQuery($query)->fetch();
        $f030femployer_contribution = $pf_setup_result['f030femployer_contribution'];
        $f030femployer_deduction = $pf_setup_result['f030femployer_deduction'];
        
        $staff_id1 = $pf_result[0]['MPD_STAFF_ID'];
            $month1 = $pf_result[0]['MPD_PAY_MONTH'];
            $process1 = $pf_result[0]['MPD_PROCESS_TYPE'];
        echo "Financial Period: ".$month1."\n";
        echo "Employer Contribution: ".$f030femployer_contribution."\n";
        echo "Employer Deduction: ".$f030femployer_deduction."\n";

         $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month1','$staff_id1','D101',$f030femployer_contribution,$process1)";
            $em->getConnection()->executeQuery($query);
            $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month1','$staff_id1','E101',$f030femployer_deduction,$process1)";
            $em->getConnection()->executeQuery($query);

        
              $query = "update t106fmonthly_payroll set MP_CHARGE_EPF = '$f030femployer_contribution',MP_EPFABLE_AMT=$f030femployer_deduction";
        $em->getConnection()->executeQuery($query);
        
          }
       echo "-----------------EPF Calculation END------------------\n";

    }
    

    public function socsoCalculation($staffId,$month){
        $em = $this->getEntityManager();

       $id_staff  = $staffId;
       echo "------------------SOCSO Calculation Begin-----------------\n";
       echo "Staff Id: ".$id_staff."\n";
       $query = "select * from t107fmonthly_payroll_details inner join t081fsalary on MPD_INCOME_CODE = f081fcode   where  f081fpf_deductable = '1' and MPD_STAFF_ID = '$id_staff' and MPD_PAY_MONTH='$month'";
        $pf_result = $em->getConnection()->executeQuery($query)->fetchAll();
        if($pf_result){

        $tmp_amount = 0;
        for ($i=0; $i <count($pf_result) ; $i++) { 
            $tmp_amount = (float)$tmp_amount  + (float)$pf_result[$i]['MPD_PAID_AMT'];
         }
        echo "Total Earning Components Amount: ".$tmp_amount."\n";
        $query = "select f091femployer_contribution,f091femployer_deduction from t091fsocso_setup where  cast(f091fstart_amount as int) <= $tmp_amount and cast(f091fend_amount as int) >= $tmp_amount";
        $pf_setup_result = $em->getConnection()->executeQuery($query)->fetch();
        if($pf_setup_result){
        $f091femployer_contribution = $pf_setup_result['f091femployer_contribution'];
        $f091femployer_deduction = $pf_setup_result['f091femployer_deduction'];
        $staff_id1 = $pf_result[0]['MPD_STAFF_ID'];
            $month1 = $pf_result[0]['MPD_PAY_MONTH'];
            $process1 = $pf_result[0]['MPD_PROCESS_TYPE'];
        echo "Financial Period: ".$month1."\n";
        echo "Employer Contribution: ".$f091femployer_contribution."\n";
        echo "Employer Deduction: ".$f091femployer_deduction."\n";

            $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month1','$staff_id1','D102',$f091femployer_contribution,$process1)";
            $em->getConnection()->executeQuery($query);
            $query = "insert into t107fmonthly_payroll_details(MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) values('$month1','$staff_id1','E102',$f091femployer_deduction,$process1)";
            $em->getConnection()->executeQuery($query);
        }
        
              $query = "update t106fmonthly_payroll set MP_CHARGE_SOCSO = '$f091femployer_contribution',MP_SOCSOABLE_AMT = $f091femployer_deduction";
        $em->getConnection()->executeQuery($query);
    }
       echo "------------------SOCSO Calculation End-----------------\n";

        }
        
  
    public function kwapCalculation() 
    {

        $em = $this->getEntityManager();
        $query = "select f034fstaff_id from t034fstaff_master";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        // print_r($result);
        // exit();
        for ($i=0;$i<count($result);$i++) 
	{
            $staff = $result[$i]['f034fstaff_id'];
            $id=(int)1;
            $query = "select idsalarycomponent, amount, employeedateofjoining, endofservice from t104femployee_details inner join t081fsalary on idsalarycomponent = f081fcode where employeeid = '$staff' and f081fpf_deductable ='$id'";
            $result1 = $em->getConnection()->executeQuery($query)->fetchAll();
            // $result[$i]['details'] = $result1;
          //  print_r($result1);
          // exit();
        foreach($result1 as $data)
        {
        	$query2 = "select f102fkwap_percentage from t102fkwap";
        	$result2 = $em->getConnection()->executeQuery($query2)->fetchAll();
                
                $percentage = $result2[0]['f102fkwap_percentage'];
		$value= ($percentage)/100;
                print_r($data['amount']);
                $amount=$data['amount']*$value;
		$totalamount= $data['amount']-$amount;

                
                $query = "INSERT INTO t105fmonthly_process(mp_pay_month, mp_staff_id, mp_staff_ic, mp_dept_code, mp_dept_desc, mp_staff_type, mp_pay_date, mp_staff_bank_code, mp_staff_bank_aac_no, mp_payment_type, mp_charge_tax, mp_charge_epf, mp_charge_socso, mp_basic_pay, mp_total_allowance, mp_total_deduction, mp_epfable_amt, mp_taxable_amt, mp_socsoable_amt, mp_nett_salary, mp_add_taxable_amt, mp_tax_category, mp_no_of_child, mp_status) VALUES (NULL, '$staff', NULL, NULL, NULL, NULL, NULL,NULL,NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$totalamount',NULL, NULL, NULL, NULL, NULl, NULL, NULL)";
$result = $em->getConnection()->executeQuery($query);
        }
     }           
        
    
    }

    public function getEmployeePayslipData($data) 
    {
        $staff_id = $data['staff_id'];
        $month = $data['month'];
        $em = $this->getEntityManager();
        $query = "select a.*,b.f034fname from t106fmonthly_payroll as a,t034fstaff_master as b  where a.MP_STAFF_ID = '$staff_id' and a.MP_STAFF_ID = b.f034fstaff_id and a.MP_PAY_MONTH = '$month'";
        $salary = $em->getConnection()->executeQuery($query)->fetchAll();
        for($i=0;$i<count($salary);$i++)
        {
            $query1 = "select t107fmonthly_payroll_details.*,t081fsalary.f081fname, t081fsalary.f081ftype_of_component from t107fmonthly_payroll_details inner join t081fsalary  on f081fcode = MPD_INCOME_CODE where MPD_STAFF_ID = '$staff_id' and MPD_PAY_MONTH = '$month' and t081fsalary.f081ftype_of_component=1";
           $result1 = $em->getConnection()->executeQuery($query1)->fetchAll();

           $query2 = "select t107fmonthly_payroll_details.*,t081fsalary.f081fname, t081fsalary.f081ftype_of_component from t107fmonthly_payroll_details inner join t081fsalary  on f081fcode = MPD_INCOME_CODE where MPD_STAFF_ID = '$staff_id' and MPD_PAY_MONTH = '$month' and t081fsalary.f081ftype_of_component=2";
           $result2 = $em->getConnection()->executeQuery($query2)->fetchAll();

          $query3 = "select t107fmonthly_payroll_details.*,t081fsalary.f081fname, t081fsalary.f081ftype_of_component from t107fmonthly_payroll_details inner join t081fsalary  on f081fcode = MPD_INCOME_CODE where MPD_STAFF_ID = '$staff_id' and MPD_PAY_MONTH = '$month' and t081fsalary.f081ftype_of_component=3";
          $result3 = $em->getConnection()->executeQuery($query3)->fetchAll();

        // $result = $result[0];
       
        $salary[$i]['details1'] = $result1;
        $salary[$i]['details2'] = $result2;
        $salary[$i]['details3'] = $result3;
       
        }
        
        return $salary;
    }

    public function moveSalary($data) 
    {
        $month = $data['month'];
        $delete = $data['delete'];
        $em = $this->getEntityManager();
        if($delete == 'yes'){
        $query = "delete from t106fmonthly_payroll_history";
        $em->getConnection()->executeQuery($query);
        $query = "delete from t107fmonthly_payroll_details_history";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        }
        else{

        //
        $query = "insert into t106fmonthly_payroll_history (MP_PAY_MONTH,MP_STAFF_ID,MP_STAFF_IC,MP_DEPT_CODE,MP_DEPT_DESC,MP_STAFF_TYPE,MP_PAY_DATE,MP_STAFF_BANK_CODE,MP_STAFF_BANK_ACC_NO,MP_PAYMENT_TYPE,MP_CHARGE_TAX,MP_CHARGE_EPF,MP_CHARGE_SOCSO,MP_BASIC_PAY,MP_TOTAL_ALLOWANCE,MP_TOTAL_DEDUCTION,MP_EPFABLE_AMT,MP_TAXABLE_AMT,MP_SOCSOABLE_AMT,MP_NETT_SALARY,MP_ADD_TAXABLE_AMT,MP_TAX_CATEGORY,MP_NO_OF_CHILD,MP_PROCESS_TYPE,MP_STATUS) select MP_PAY_MONTH,MP_STAFF_ID,MP_STAFF_IC,MP_DEPT_CODE,MP_DEPT_DESC,MP_STAFF_TYPE,MP_PAY_DATE,MP_STAFF_BANK_CODE,MP_STAFF_BANK_ACC_NO,MP_PAYMENT_TYPE,MP_CHARGE_TAX,MP_CHARGE_EPF,MP_CHARGE_SOCSO,MP_BASIC_PAY,MP_TOTAL_ALLOWANCE,MP_TOTAL_DEDUCTION,MP_EPFABLE_AMT,MP_TAXABLE_AMT,MP_SOCSOABLE_AMT,MP_NETT_SALARY,MP_ADD_TAXABLE_AMT,MP_TAX_CATEGORY,MP_NO_OF_CHILD,MP_PROCESS_TYPE,MP_STATUS from t106fmonthly_payroll where MP_PAY_MONTH = '$month'";
        $em->getConnection()->executeQuery($query);


        $query = "insert into t107fmonthly_payroll_details_history (MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE) select MPD_PAY_MONTH,MPD_STAFF_ID,MPD_INCOME_CODE,MPD_PAID_AMT,MPD_PROCESS_TYPE from t107fmonthly_payroll_details where MPD_PAY_MONTH = '$month'";
        $em->getConnection()->executeQuery($query);
        
        }
        return $result;
    }

    public function employeeSalaryCalculation($data) 
    {
        $em = $this->getEntityManager();
        $id = (int)$data['employeeId'];

          $pay_date = date('m/Y', time());
          $pay_month = date('m', time());

          if ($pay_month == 1)
          {
             $salary_month = '12/' . (date('Y')-1);
          }
          else
          {
            $salary_month = sprintf('%02d',(date('m')-1)) . '/' . date('Y');
          }

          // print_r($salary_month);
          // exit;

          // $value = substr($pay_date, star)              
        // print_r($pay_date);exit;


        $query  = "select sum(es.f011famount) as earningSum, es.f011femployee_id, es.f011fcurrency from t011femployee_salary as es where es.f011femployee_id = $id and f011ffinancial_period = '$salary_month' and es.f011fid_salary_component = '2'";

        
        $earningResult = $em->getConnection()->executeQuery($query)->fetchAll();


// print_r($);exit;

        $query  = "select sum(es.f011famount) as deductionSum,  es.f011femployee_id, es.f011fcurrency from t011femployee_salary as es where es.f011femployee_id = $id and f011ffinancial_period = '$salary_month' and es.f011fid_salary_component = '1'";
        
        $deductionResult = $em->getConnection()->executeQuery($query)->fetchAll();


        $query  = "select sum(es.f011famount) as taxSum,  es.f011femployee_id, es.f011fcurrency from t011femployee_salary as es where es.f011femployee_id = $id and f011ffinancial_period = '$salary_month' and es.f011fid_salary_component = '3'";
        
        $taxResult = $em->getConnection()->executeQuery($query)->fetchAll();

        // $result['deduction'] = $deductionResult[0];
        // $result['earning'] = $earningResult[0];

        $earningResultTotal = $earningResult[0]['earningSum'];
        $taxResultTotal = $taxResult[0]['taxSum'];
        $deductionResultTotal = $deductionResult[0]['deductionSum'];

        
        $salary = $earningResultTotal - $deductionResultTotal - $taxResultTotal;


        $result['earningSum'] = $earningResult[0]['earningSum'];
        $result['deductionSum'] = $deductionResult[0]['deductionSum'];
        $result['taxSum'] = $taxResult[0]['taxSum'];
        $result['salary'] = $salary;
        $result['employeeId'] = $earningResult[0]['f011femployee_id'];
        $result['currency'] = $earningResult[0]['f011fcurrency'];
                // print_r($salary);exit;
// print_r($earningResult);exit;
        return $result;



        
    }

}

?>