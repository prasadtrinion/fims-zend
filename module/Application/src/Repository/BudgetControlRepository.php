<?php
namespace Application\Repository;

use Application\Entity\T052fbudgetControl;
use Application\Entity\T015fdepartment;
use Application\Entity\T015ffinancialyear;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetControlRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f052fid, b.department,d.f015fdepartmentName as departmentName, b.f052fidFinancialyear,f.f015fname as financialYear, b.f052fstatus, b.f052fidUser, u.f014fuserName as user ')
            ->from('Application\Entity\T052fbudgetControl','b')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.department = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','b.f052fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T014fuser','u', 'with','b.f052fidUser = u.f014fid')
            ->orderBy('b.f052fid', 'DESC');


        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
           $qb->select('b.f052fid, b.department,d.f015fdepartmentName as departmentName, b.f052fidFinancialyear,f.f015fname as financialYear, b.f052fstatus, b.f052fidUser, u.f014fuserName as user ')
            ->from('Application\Entity\T052fbudgetControl','b')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.department = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','b.f052fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T014fuser','u', 'with','b.f052fidUser = u.f014fid')
            ->where('b.f052fid = :controlId')
            ->setParameter('controlId',$id);
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $budgetControl = new T052fbudgetControl();

        $budgetControl->setF052fidDepartment($data['department'])
              ->setF052fidFinancialyear((int)$data['f052fidFinancialyear'])
              ->setF052fidUser((int)$data['f052fidUser'])
              ->setF052fstatus((int)$data['f052fstatus'])
              ->setF052fcreatedBy((int)$_SESSION['userId'])
              ->setF052fupdatedBy((int)$_SESSION['userId']);

        $budgetControl->setF052fcreatedDtTm(new \DateTime())
                        ->setF052fupdatedDtTm(new \DateTime());

 
        $em->persist($budgetControl);
        $em->flush();

       

      return $budgetControl;

    }

    public function updateData($budgetControl, $data = []) 
    {
        $em = $this->getEntityManager();
        // print_r($budgetCostcenter);
        // exit();

       $budgetControl->setF052fidDepartment($data['department'])
              ->setF052fidFinancialyear((int)$data['f052fidFinancialyear'])
              ->setF052fidUser((int)$data['f052fidUser'])
              ->setF052fstatus((int)$data['f052fstatus'])
              ->setF052fcreatedBy((int)$_SESSION['userId'])
              ->setF052fupdatedBy((int)$_SESSION['userId']);

        $budgetControl->setF052fcreatedDtTm(new \DateTime())
                        ->setF052fupdatedDtTm(new \DateTime());
 
        $em->persist($budgetControl);
        $em->flush();


    }

 }
 ?>

