<?php
namespace Application\Repository;

use Application\Entity\T079fcreditorDebtor;
use Application\Entity\T080fcreditorDebtorDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class CreditorDebtorRepository extends EntityRepository 
{

    public function getCreditList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor', 'd')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid')
            ->where('d.f079fcrDrFlag = 1')
            ->orderBy('d.f079fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getDebitList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor', 'd')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid')
            ->where('d.f079fcrDrFlag = 2');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id,$approvedStatus) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
            if($approvedStatus == 0 && isset($approvedStatus)){
                $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor', 'd')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid')
               ->where('d.f079fstatus = 0')
               ->AndWhere('d.f079fcrDrFlag = :cRdRFlag')
               ->setParameter(':cRdRFlag',$id);
            }
            elseif($approvedStatus == 1){
               $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor', 'd')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid')
               ->where('d.f079fstatus = 1')
               ->AndWhere('d.f079fcrDrFlag = :cRdRFlag')
               ->setParameter(':cRdRFlag',$id);
            }
            elseif ($approvedStatus == 2) {
               $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor', 'd')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid')
               ->where('d.f079fcrDrFlag = :cRdRFlag')
               ->setParameter(':cRdRFlag',$id);
            }
            else{
                $qb->select('d.f079fid,d.f079fcustomer,d.f079fdate,d.f079fadvancePayment,d.f079fnarration,d.f079fapprovedBy,d.f079ftotalAmount,d.f079fstatus,d.f079fcrDrFlag,dd.f080fid,dd.f080fidItem,dd.f080fidGlcode,dd.f080fquantity,dd.f080fprice,dd.f080ftotal,dd.f080fstatus,dd.f080fgst,dd.f080fdiscount,c.f021ffirstName as firstName,c.f021flastName as lastName')
            ->from('Application\Entity\T079fcreditorDebtor','d')
            ->leftjoin('Application\Entity\T080fcreditorDebtorDetails','dd', 'with', 'd.f079fid = dd.f080fidCreditorDebtor')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f079fcustomer = c.f021fid');
           $qb ->where('d.f079fid = :debtorId')
            ->setParameter('debtorId',$id);
            }
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $creditorDebtor = new T079fcreditorDebtor();
$data['f079fnarration'] = 'NR';
        $creditorDebtor->setF079fcustomer((int)$data['f079fcustomer'])
                ->setF079fdate(new \DateTime($data['f079fdate']))
                ->setF079fadvancePayment((float)$data['f079fadvancePayment'])
                ->setF079fapprovedBy((int)$data['f079fapprovedBy'])
                ->setF079ftotalAmount((float)$data['f079ftotalAmount'])
                ->setF079fnarration($data['f079fnarration'])
                ->setF079fstatus((int)$data['f079fstatus'])
                ->setF079fcrDrFlag((int)$data['f079fcrDrFlag'])
                ->setF079fcreatedBy((int)$_SESSION['userId'])
                ->setF079fupdatedBy((int)$_SESSION['userId'])
                ->setF079fcreatedDtTm(new \DateTime())
                ->setF079fupdatedDtTm(new \DateTime());

        try{
            $em->persist($creditorDebtor);
            $em->flush();
           // print_r($creditorDebtor);
        }
        catch(\Exception $e){
            echo $e;
        }

        $creditorDebtorDetails = $data['details'];

        foreach ($creditorDebtorDetails as $creditorDebtorDetail) {
        
            $creditorDebtorDetailObj = new T080fcreditorDebtorDetails();
            $creditorDebtorDetailObj->setF080fidItem((int)$creditorDebtorDetail['f080fidItem'])
                              ->setF080fidGlcode((int)$creditorDebtorDetail['f080fidGlcode'])
                              ->setF080fquantity((int)$creditorDebtorDetail['f080fquantity'])
                              ->setF080fprice((float)$creditorDebtorDetail['f080fprice'])
                              ->setF080fdiscount((int)$creditorDebtorDetail['f080fdiscount'])
                              ->setF080fgst((int)$creditorDebtorDetail['f080fgst'])
                              ->setF080ftotal((float)$creditorDebtorDetail['f080ftotal'])
                              ->setF080fstatus((int)$data['f079fstatus'])
                              ->setF080fcreatedBy((int)$_SESSION['userId'])
                              ->setF080fupdatedBy((int)$_SESSION['userId'])
                              ->setF080fidCreditorDebtor($creditorDebtor);
             
            $creditorDebtorDetailObj->setF080fcreatedDtTm(new \DateTime())
                             ->setF080fupdatedDtTm(new \DateTime());
      try{
            $em->persist($creditorDebtorDetailObj);
            $em->flush();
            //print_r($creditorDebtorDetailObj);
        }
        catch(\Exception $e){
            echo $e;
        }
        }
        return $creditorDebtor;
    }

    

    public function approve($data){
        $em = $this->getEntityManager();
        foreach ($data['id'] as $id) {
    
        $query1 = "update t079fcreditor_debtor set f079fstatus = 1 where f079fid = $id"; 
            $em->getConnection()->executeQuery($query1);
            $query2 = "update t080fcreditor_debtor_details set f080fstatus = 1 where f080fid_creditor_debtor = $id"; 
            $em->getConnection()->executeQuery($query2);
            return;
        }
    }

}