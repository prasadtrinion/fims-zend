<?php 
namespace Application\Repository;

use Application\Entity\T071finvoice;
use Application\Entity\T072finvoiceDetails;
use Application\Entity\T017fjournal;
use Application\Entity\T018fjournalDetails;

use Application\Entity\T043fvehicleLoan;
use Application\Entity\T044floanGurantors;
use Application\Entity\T018fblacklistEmp;
use Application\Entity\T082fmonthlyDeductionMaster;
use Application\Entity\T082fmonthlyDeductionDetail;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class VehicleLoanRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('v.f043fid, v.f043fidLoanName, v.f043fidVendor, v.f043freferenceNumber, v.f043ffinalStatus, v.f043fvehicleCondition, v.f043fmanufacturer, v.f043fmodel, v.f043fcc, v.f043fcompanyName, v.f043finsuranceComp, v.f043floanEntitlement, v.f043fpurchasePrice, v.f043floanAmount, v.f043finstallmentPeriod, v.f043fdrivingLicense, v.f043fchasisNo, v.f043fengineNo, v.f043fregistrationNo, v.f043fjkrAmount, v.f043ficNo, v.f043fjkrCertificateNo, v.f043fjkrPeriod, v.f043fapplyDate, v.f043fstartDate, v.f043fendDate, v.f043flastDate, v.f043fempName, v.f043ffirstGuarantor, v.f043fsecondGuarantor, v.f043insuranceAmt, v.f043ftotalLoan, v.f043finterestRate, v.f043fmonthInterestRate, v.f043fprofitRatio, v.f043fmonthlyInstallment, v.f043fprincipal, v.f043fprofitAmount, v.f043floanYear, v.f043finsuranceRate, ln.f020floanName, s.f034fname as employeeName, v.f043fapprover1, v.f043fapprover2, v.f043fapprover3, v.f043fapprover4, v.f043fapprover5, v.f043fapprover6, v.f043fapprover7, v.f043fapprover8,v.f043fpaid,v.f043fbalance')
        ->from('Application\Entity\T043fvehicleLoan', 'v')
           // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
        ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','v.f043fidLoanName = ln.f020fid')
        ->orderBy('v.f043fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );        return $result;

    }

    public function vehicleLoanList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select(' v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate, s.f034fname as staff,l.f044fid,l.f044fidStaff,l.f044fidLoan,l.f044fapprovedDate,l.f044fstatus,ln.f020floanName,ln.f020ftypeOfLoan,ln.f020fpercentage,ln.f020ffund,ln.f020fpaytype,ln.f020faccount,v.f043fpaid,v.f043fbalance')
        ->from('Application\Entity\T043fvehicleLoan', 'v')
        ->leftjoin('Application\Entity\T044floanGurantors', 'l', 'with','v.f043fid = l.f044fidLoan')
            // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
        ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','v.f043fidLoanName = ln.f020fid')
        ->where('l.f044fidStaff = :user')
        ->andWhere("l.f044fstatus = 0")
        ->setParameter('user',(int)$_SESSION['staffId'])
        ->orderBy('v.f043fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );        return $result;

    }
    public function nonEligibleGurantors() 
    {

        $em = $this->getEntityManager();

        $query = "select f044fid_staff from  t044floan_gurantors group by f044fid_staff having count(f044fid_staff)>2 ";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;

    }

    public function employeeLoanList() 
    {
        $session = (int)$_SESSION['staffId'];
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('s.f034fstaffId,s.f034fname as staff,v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate,ln.f020floanName,ln.f020ftypeOfLoan,ln.f020fpercentage,ln.f020ffund,ln.f020fpaytype,ln.f020faccount,v.f043fapprover1,v.f043fapprover2,v.f043fapprover3,v.f043fapprover4,v.f043fapprover5,v.f043fapprover6,v.f043fapprover7,v.f043fapprover8,v.f043freason1,v.f043freason2,v.f043freason3,v.f043freason4,v.f043freason5,v.f043freason6,v.f043freason7,v.f043freason8,v.f043fapproved1Date,v.f043fapproved2Date,v.f043fapproved3Date,v.f043fapproved4Date,v.f043fapproved5Date,v.f043fapproved6Date,v.f043fapproved7Date,v.f043fapproved8Date,v.f043fapproved1By,v.f043fapproved2By,v.f043fapproved3By,v.f043fapproved4By,v.f043fapproved5By,v.f043fapproved6By,v.f043fapproved7By,v.f043fapproved8By,v.f043fstatus,v.f043fpaid,v.f043fbalance')
        ->from('Application\Entity\T043fvehicleLoan', 'v')
            // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
        ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','v.f043fidLoanName = ln.f020fid')
        ->where('v.f043fempName = :sessionId')
        ->setParameter('sessionId',$session)
        ->orderBy('v.f043fid','DESC');  
        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = array();
        foreach ($result1 as $row) {
            $row['f043fstartDate'] = date("d/m/Y",strtotime($row['f043fstartDate']));
            $row['f043fendDate'] = date("d/m/Y",strtotime($row['f043fendDate']));
            array_push($result, $row);
        }

        $result = array(
            'data' => $result
        );
        return $result;

    }

    public function staffLoanList($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("v.f043fid,v.f043fidLoanName,(v.f043freferenceNumber+'('+ln.f020floanName+')' as loanName")
        ->from('Application\Entity\T043fvehicleLoan', 'v')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','v.f043fidLoanName = ln.f020fid')
        ->where('v.f043fempName = :sessionId')
        ->andWhere('v.f043ffinalStatus = 1')
        ->setParameter('sessionId',(int)$id)
        ->orderBy('v.f043fid','DESC');  
        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result1
        );
        return $result;

    }

    public function getLoanAmount($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("v.f043fmonthlyInstallment")
        ->from('Application\Entity\T043fvehicleLoan', 'v')
        ->where('v.f043fid = :sessionId')
        ->setParameter('sessionId',(int)$id)
        ->orderBy('v.f043fid','DESC');  
        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result1
        );
        return $result;

    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate,v.f043fpaid,v.f043fbalance')
        ->from('Application\Entity\T043fvehicleLoan', 'v')
        //    ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
          //  ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','v.f043finsuranceComp = c.f021fid')
          //  ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fid')
           // ->leftjoin('Application\Entity\T034fstaffMaster', 'fg', 'with','v.f043ffirstGuarantor = fg.f034fid')
           // ->leftjoin('Application\Entity\T030fvendorRegistration', 've', 'with','ve.f030fid = v.f043fidVendor')
        ->where('v.f043fid = :vehicleLoanId')
        ->setParameter('vehicleLoanId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = $result[0];
        $qb = $em->createQueryBuilder();
        $qb->select("f.f044fid,f.f044fidStaff as guaranteeId,f.f044fidLoan,s.f034fname,l.f043fidLoanName,l.f043freferenceNumber,f.f044fapprovedDate,f.f044fstatus")
        ->from('Application\Entity\T044floanGurantors','f')
        ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','f.f044fidStaff = s.f034fstaffId')
        ->leftjoin('Application\Entity\T043fvehicleLoan', 'l', 'with','f.f044fidLoan = l.f043fid')
        ->where('f.f044fidLoan = :Id')
        ->setParameter('Id',(int)$id);
        $query = $qb->getQuery();
        $gurantorList = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result['guarantor-list'] = $gurantorList;
        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="LOAN";
        $number = $configRepository->generateFIMS($numberData);
        
        $vehicleLoan = new T043fvehicleLoan();
        $vendorRepository = $em->getRepository("Application\Entity\T030fvendorRegistration");
        $idLoanName = $data['f043fidLoanName'];
        $query = "select f020floan_name from t020floan_name where f020fid = $idLoanName";
        $loan_name_result =$em->getConnection()->executeQuery($query)->fetch();

        // if($data['f043fidLoanName'] == 17){
        //     $data['f043fvehicleCondition'] = 1;
        // }
        
        $vehicleLoan->setF043freferenceNumber($number)
        ->setF043fidLoanName((int)$data['f043fidLoanName'])
        ->setF043fidVendor((int)$data['f043fidVendor'])
        ->setF043fvehicleCondition((int)$data['f043fvehicleCondition'])
        ->setF043fmanufacturer($data['f043fmanufacturer'])  
        ->setF043fmodel($data['f043fmodel'])  
        ->setF043fcc($data['f043fcc'])  
        ->setF043fcompanyName($data['f043fcompanyName'])  
        ->setF043finsuranceComp((int)$data['f043finsuranceComp'])  
        ->setF043floanEntitlement($data['f043floanEntitlement'])  
        ->setF043fpurchasePrice((float)$data['f043fpurchasePrice'])  
        ->setF043floanAmount((float)$data['f043floanAmount'])  
        ->setF043finstallmentPeriod($data['f043finstallmentPeriod'])  
        ->setF043fempName((int)$data['f043fempName'])  
        ->setF043ffirstGuarantor($data['f043ffirstGuarantor'])  
        ->setF043fsecondGuarantor($data['f043fsecondGuarantor'])
        ->setF043fdrivingLicense($data['f043fdrivingLicense'])
        ->setF043fchasisNo($data['f043fchasisNo'])
        ->setF043fengineNo($data['f043fengineNo'])
        ->setF043fregistrationNo($data['f043fregistrationNo'])
        ->setF043fjkrAmount((float)$data['f043fjkrAmount'])
        ->setF043ficNo($data['f043ficNo'])
        ->setF043fjkrCertificateNo($data['f043fjkrCertificateNo']) 
        ->setF043fjkrPeriod($data['f043fjkrPeriod']) 
        ->setF043fstatus((int)$data['f043fstatus'])
        ->setF043fapplyDate(new \DateTime($data['f043fapplyDate']))  
        ->setF043fstartDate(new \DateTime($data['f043fstartDate']))  
        ->setF043fendDate(new \DateTime($data['f043fendDate']))  
        ->setF043flastDate(new \DateTime($data['f043flastDate']))    
        ->setF043fapprover1((int)0)  
        ->setF043fapprover2((int)0)  
        ->setF043fapprover3((int)0)  
        ->setF043fapprover4((int)0)  
        ->setF043fapprover5((int)0)  
        ->setF043fapprover6((int)0)  
        ->setF043fapprover7((int)0)  
        ->setF043fapprover8((int)0)  
        ->setF043freason1($data['f043freason1'])  
        ->setF043freason2($data['f043freason2'])  
        ->setF043freason3($data['f043freason3'])  
        ->setF043freason4($data['f043freason4'])  
        ->setF043freason5($data['f043freason5'])  
        ->setF043freason6($data['f043freason6'])  
        ->setF043freason7($data['f043freason7'])  
        ->setF043freason8($data['f043freason8'])  
        ->setF043fapproved1By((int)1)  
        ->setF043fapproved2By((int)2)  
        ->setF043fapproved3By((int)3)  
        ->setF043fapproved4By((int)4)  
        ->setF043fapproved5By((int)5)  
        ->setF043fapproved6By((int)6)  
        ->setF043fapproved7By((int)7)  
        ->setF043fapproved8By((int)8)
        ->setF043ffinalStatus((int)0)
        ->setF043fapproved1Date(new \DateTime())  
        ->setF043fapproved2Date(new \DateTime())  
        ->setF043fapproved3Date(new \DateTime())  
        ->setF043fapproved4Date(new \DateTime())  
        ->setF043fapproved5Date(new \DateTime())  
        ->setF043fapproved6Date(new \DateTime())  
        ->setF043fapproved7Date(new \DateTime())  
        ->setF043fapproved8Date(new \DateTime())
        ->setF043insuranceAmt((float)$data['f043insuranceAmt'])
        ->setF043ftotalLoan((float)$data['f043ftotalLoan'])
        ->setF043finterestRate((float)$data['f043finterestRate'])
        ->setF043fmonthInterestRate((float)$data['f043fmonthInterestRate'])
        ->setF043fprofitRatio((float)$data['f043fprofitRatio'])
        ->setF043fmonthlyInstallment((float)$data['f043fmonthlyInstallment'])
        ->setF043fprincipal((float)$data['f043fprincipal'])
        ->setF043fprofitAmount((float)$data['f043fprofitAmount'])
        ->setF043floanYear($data['f043floanYear'])
        ->setF043finsuranceRate((float)$data['f043finsuranceRate'])
        ->setF043floanPosting((int)0)
        ->setF043fcreatedBy((int)$_SESSION['userId'])  
        ->setF043fupdatedBy((int)$_SESSION['userId'])  
        ->setF043fcreatedDtTm(new \DateTime())  
        ->setF043fupdatedDtTm(new \DateTime())
        ->setF043fpaid((float)0)
        ->setF043fbalance((float)$data['f043ftotalLoan']); 


        try{

            $em->persist($vehicleLoan);
            $em->flush();
        }
        catch(\Exception $e)
        {echo $e;}    

        $gurantorDetails = $data['guarantor-list'];
        if(!$gurantorDetails){
            $vehicleLoan->setF043fapprover2((int)1);
            try{

                $em->persist($vehicleLoan);
                $em->flush();
            }
            catch(\Exception $e)
            {echo $e;}    

        }
        foreach ($gurantorDetails as $gurantor) {
    # code...

           $loan = new T044floanGurantors();

           $loan->setF044fidStaff((int)$gurantor['guaranteeId'])
           ->setF044fidLoan((int)$vehicleLoan->getF043fid())
           ->setF044fstatus((int)"0")
           ->setF044fcreatedBy((int)$_SESSION['userId'])
           ->setF044fupdatedBy((int)$_SESSION['userId']);

           $loan->setF044fcreatedDtTm(new \DateTime())
           ->setF044fupdatedDtTm(new \DateTime());

           try{

            $em->persist($loan);

            $em->flush();
            
            $employee = $data['f043fempName'];
            $query = "select employeeemail from vw_employee where employeeid = $employee";
            $email_result =$em->getConnection()->executeQuery($query)->fetch();
            $loanName = $loan_name_result['f020floan_name'];


            $message = "<p>Your application submitted to guarantor for approval.</p><br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
            $subject = $loanName.' Application - Application No : '.$number;
            if(isset($email_result['employeeemail'])){
                $email = $email_result['employeeemail'];
                $vendorRepository->sendMail1($username,$message,$subject);
            }
        }
        catch(\Exception $e){
            echo $e;
        }
    }
    
    return $vehicleLoan;

}


public function updateData($vehicleLoan,$data = []) 
{
    $em = $this->getEntityManager();


    $vehicleLoan->setF043freferenceNumber($data['f043freferenceNumber'])
    ->setF043fidLoanName((int)$data['f043fidLoanName'])
    ->setF043fidVendor((int)$data['f043fidVendor'])
    ->setF043fvehicleCondition((int)$data['f043fvehicleCondition'])
    ->setF043fmanufacturer($data['f043fmanufacturer'])  
    ->setF043fmodel($data['f043fmodel'])  
    ->setF043fcc($data['f043fcc'])  
    ->setF043fcompanyName($data['f043fcompanyName'])  
    ->setF043finsuranceComp((int)$data['f043finsuranceComp'])  
    ->setF043floanEntitlement($data['f043floanEntitlement'])  
    ->setF043fpurchasePrice((float)$data['f043fpurchasePrice'])  
    ->setF043floanAmount((float)$data['f043floanAmount'])  
    ->setF043finstallmentPeriod($data['f043finstallmentPeriod'])  
    ->setF043fempName((int)$data['f043fempName'])  
    ->setF043ffirstGuarantor($data['f043ffirstGuarantor'])  
    ->setF043fsecondGuarantor($data['f043fsecondGuarantor'])
    ->setF043fdrivingLicense($data['f043fdrivingLicense'])
    ->setF043fchasisNo($data['f043fchasisNo'])
    ->setF043fengineNo($data['f043fengineNo'])
    ->setF043fregistrationNo($data['f043fregistrationNo'])
    ->setF043fjkrAmount((float)$data['f043fjkrAmount'])
    ->setF043ficNo($data['f043ficNo'])
    ->setF043fjkrCertificateNo($data['f043fjkrCertificateNo']) 
    ->setF043fjkrPeriod($data['f043fjkrPeriod']) 
    ->setF043fstatus((int)$data['f043fstatus'])
    ->setF043fapplyDate(new \DateTime($data['f043fapplyDate']))  
    ->setF043fstartDate(new \DateTime($data['f043fstartDate']))  
    ->setF043fendDate(new \DateTime($data['f043fendDate']))  
    ->setF043flastDate(new \DateTime($data['f043flastDate']))    
    ->setF043insuranceAmt((float)$data['f043insuranceAmt'])
    ->setF043ftotalLoan((float)$data['f043ftotalLoan'])
    ->setF043finterestRate((float)$data['f043finterestRate'])
    ->setF043fmonthInterestRate((float)$data['f043fmonthInterestRate'])
    ->setF043fprofitRatio((float)$data['f043fprofitRatio'])
    ->setF043fmonthlyInstallment((float)$data['f043fmonthlyInstallment'])
    ->setF043fprincipal((float)$data['f043fprincipal'])
    ->setF043fprofitAmount((float)$data['f043fprofitAmount'])
    ->setF043floanYear($data['f043floanYear'])
    ->setF043finsuranceRate((float)$data['f043finsuranceRate'])
    ->setF043fupdatedBy((int)$_SESSION['userId'])  
    ->setF043fupdatedDtTm(new \DateTime()); 

    try{ 
        $em->persist($vehicleLoan);
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    } 
    $id = $vehicleLoan->getF043fid();
    $query = "delete from t044floan_gurantors where f044fid_loan = $id";
    $em->getConnection()->executeQuery($query);
    $gurantorDetails = $data['guarantor-list'];
    foreach ($gurantorDetails as $gurantor) {
    # code...

       $loan = new T044floanGurantors();

       $loan->setF044fidStaff((int)$gurantor['guaranteeId'])
       ->setF044fidLoan((int)$vehicleLoan->getF043fid())
       ->setF044fstatus((int)"0")
       ->setF044fcreatedBy((int)$_SESSION['userId'])
       ->setF044fupdatedBy((int)$_SESSION['userId']);

       $loan->setF044fcreatedDtTm(new \DateTime())
       ->setF044fupdatedDtTm(new \DateTime());

       try{

        $em->persist($loan);
        
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
}
return $vehicleLoan;

}

public function vehicleLoanApproval($postData)
{
    $em = $this->getEntityManager();
    $vendorRepository = $em->getRepository("Application\Entity\T030fvendorRegistration");

    // Mail Code
   $query = "select f020floan_name,f043freference_number,f043femp_name,f043fstart_date,f034fname from t020floan_name inner join t043fvehicle_loan on f043fid_loan_name = f020fid inner join vw_staff_master on f034fstaff_id = f043femp_name where f043fid = $id";
   $loan_name_result =$em->getConnection()->executeQuery($query)->fetch(); 
   $loanName = $loan_name_result['f020floan_name'];
   $number = $loan_name_result['f043freference_number'];
   $staffName = $loan_name_result['f034fname'];
   $staffId = $loan_name_result['f043femp_name'];
   $startDate = date("d/m/Y H:i:s",strtotime($loan_name_result['f043fstart_date']));
   $subject = $loanName.' Application - Application No : '.$number;

   $query = "select f034fname,f034fstaff_id from vw_staff_master where f034fstaff_id = $idStaff";
   $gurantor_result =$em->getConnection()->executeQuery($query)->fetch(); 
   $gurantor_name = $gurantor_result['f034fname'];
// 
    $ids=$postData['id'];
    $levels=$postData['level'];
    $statuses=$postData['status'];
    $reason=$postData['reason'];

    $id=(int)$ids;
    $level=(int)$levels;
    $status=(int)$statuses;

    if($status == 2){ 
        if($level = 11){
            $query = "update t043fvehicle_loan set f043ffinal_status = 2 where f043fid = $id";
            $result =$em->getConnection()->executeQuery($query);
            $query = "select f043femp_name from t043fvehicle_loan where f043fid = $id";
            $result =$em->getConnection()->executeQuery($query)->fetch();
            $staff = $result['f043femp_name'];

            $blacklist = new T018fblacklistEmp();


            $blacklist->setF018fidStaff((int)$staff)
            ->setF018feffectiveDate(new \DateTime())
            ->setF018freleaseDate(new \DateTime('2099-01-01'))
            ->setF018fendDate(new \DateTime('2099-01-01'))
            ->setF018freason("Self Loan Reject")
            ->setF018fstatus((int)1)
            ->setF018fcreatedBy((int)$_SESSION['userId'])
            ->setF018fupdatedBy((int)$_SESSION['userId']);


            $blacklist->setF018fcreatedDtTm(new \DateTime())
            ->setF018fupdatedDtTm(new \DateTime());


            try
            {
                $em->persist($blacklist);
                $em->flush();
            // print_r($blacklist);
            // die();
            }
            catch(\Exception $e)
            {
                echo $e;
            }
        }
        else{

            $query = "update t043fvehicle_loan set f043ffinal_status = 2,f043fapprover3 = 2 where f043fid = $id";
            $result =$em->getConnection()->executeQuery($query);
        }
        // return;
    }


$query = "select employeeemail from vw_employee inner join t043fvehicle_loan on f043femp_name = employeeid where f043fid = $id";
$email_result =$em->getConnection()->executeQuery($query)->fetch();
// if(isset($email_result['employeeemail'])){
//     $email = $email_result['employeeemail'];
//     $vendorRepository->sendMail1($username,$message,$subject);
// }



    switch ($level) 
    {
        case '1':
    

        $query = "update t043fvehicle_loan set f043fapprover1 = $status, f043fapproved1_by=$level, f043freason1='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);

        if($status==2)
        { 
            $query2 = "update t044floan_gurantors set f044fstatus = $status where f044fid_loan = $id";
            $result2 =$em->getConnection()->executeQuery($query2);
        }
        break;
        case '2':
        $query = "update t043fvehicle_loan set f043fapprover2 = $status, f043fapproved2_by=$level, f043freason2='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '3':
        $message = "<p>Your $loanName application verified by Head of Department.<br>Staff No: $staffId<br>Applicant Name: $staffName<br>Application No:$number<br>Date Applied: $startDate<br>Please refer guide and supporting document checklist at UUM PORTAL</p><br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
        $query = "update t043fvehicle_loan set f043fapprover3 = $status, f043fapproved3_by=$level, f043freason3='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '4':
        $message = "<p>$loanName application/supporting document received..<br>Staff No: $staffId<br>Applicant Name: $staffName<br>Application No:$number<br>Date Applied: $startDate<br>Please refer guide and supporting document checklist at UUM PORTAL</p><br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
        $query = "update t043fvehicle_loan set f043fapprover4 = $status, f043fapproved4_by=$level, f043freason4='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '5':
        $message = "<p>Your $loanName has been verified.<br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";

        $query = "update t043fvehicle_loan set f043fapprover5 = $status, f043fapproved5_by=$level, f043freason5='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '6':
        $message = "<p>Your $loanName has been approved.<br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";

        $query = "update t043fvehicle_loan set f043fapprover6 = $status, f043fapproved6_by=$level, f043freason6='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '7':
        $message = "<p>Your $loanName has been approved.<br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";

        $query = "update t043fvehicle_loan set f043fapprover7 = $status, f043fapproved7_by=$level, f043freason7='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '8':
        $message = "<p>Your $loanName has been approved.<br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
        
        $query = "update t043fvehicle_loan set f043fapprover8 = $status, f043fapproved8_by=$level, f043freason8='$reason' where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
        case '9':
        $query = "update t043fvehicle_loan set f043fstatus =1,f043ffinal_status = 0 where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
///////////////////////////////////////////////////////////////////////////////////////////
        $query = "select f043fid_loan_name,f043femp_name,f043ftotal_loan from t043fvehicle_loan  where f043fid = $id";
        $loan_result =$em->getConnection()->executeQuery($query)->fetch();

        $loanAmount = $loan_result['f043ftotal_loan'];
        $staffId = $loan_result['f043femp_name'];
        $idLoanName = $loan_result['f043fid_loan_name'];

        $query = "select f142ffund,f142fdepartment,f142factivity,f142faccount,f142ffactor_amount from t142floan_processing_fee where f142fid_loan_type = $idLoanName and ((f142ffactor = 'Lesser Factor' and f142famount > $loanAmount) or (f142ffactor = 'Greater Factor' and f142famount < $loanAmount))  order by f142feffective_date desc";
        // echo $query;
        // die();
        $processing_result =$em->getConnection()->executeQuery($query)->fetch();

        $query = "select f085fid from t085frevenue_set_up where f085fcode='PRO101'";
        $revenue_result =$em->getConnection()->executeQuery($query)->fetch();

        $query = "select f140fcredit_fund,f140fcredit_department,f140fcredit_activity,f140fcredit_account from t140fglcode_setup inner join t011fdefinationms on f011fid = f140id_voucher_type where f011fdefinition_code='STAFF' and f011fdefinition_desc='PaymentVoucherType'";
        $credit_result =$em->getConnection()->executeQuery($query)->fetch();

        $query = "select f081fid,f081fcode,f081fpercentage from t081ftext_code where f081fcode = 'NR'";
        $tax_result =$em->getConnection()->executeQuery($query)->fetch();

        $masterInvoice = array();
        $masterInvoice['f071finvoiceType'] = 'STA';
        $masterInvoice['f071fbillType'] = 1;
        $masterInvoice['f071finvoiceDate'] = date('Y-m-d');
        $masterInvoice['f071fdescription'] = 'Loan Processing Fee';
        $masterInvoice['f071fidCustomer'] = $staffId;
        $masterInvoice['f071fidRequestInvoice'] = 0;
        $masterInvoice['f071ftype'] = 1;
        $masterInvoice['f071fstatus'] = 1;
        $masterInvoice['f071freason'] = '';
        $masterInvoice['f071finvoiceTotal'] = $processing_result['f142ffactor_amount'];
        $masterInvoice['f071fcrAmount'] = 0;
        $masterInvoice['f071fdrAmount'] = 0;
        $masterInvoice['f071ftotalPaid'] = 0;
        $masterInvoice['f071fdiscount'] = 0;
        $masterInvoice['f071fonlinePayment'] = 0;
        $masterInvoice['f071fisRcp'] = 1;
        $masterInvoice['f071finvoiceFrom'] = 1;

        $masterInvoiceDetails  = array();
        $masterInvoiceDetails['f072fidItem'] = $revenue_result['f085fid'];
        $masterInvoiceDetails['f072fdebitFundCode'] = $processing_result['f142ffund'];
        $masterInvoiceDetails['f072fdebitAccountCode'] = $processing_result['f142fdepartment'];
        $masterInvoiceDetails['f072fdebitActivityCode'] = $processing_result['f142factivity'];
        $masterInvoiceDetails['f072fdebitDepartmentCode'] = $processing_result['f142faccount'];
        $masterInvoiceDetails['f072fcreditFundCode'] = $credit_result['f140fcredit_fund'];
        $masterInvoiceDetails['f072fcreditAccountCode'] = $credit_result['f140fcredit_account'];
        $masterInvoiceDetails['f072fcreditActivityCode'] = $credit_result['f140fcredit_activity'];
        $masterInvoiceDetails['f072fcreditDepartmentCode'] = $credit_result['f140fcredit_department'];
        $masterInvoiceDetails['f072fgstCode'] = $tax_result['f081fid'];
        $masterInvoiceDetails['f072freason'] = '';
        $masterInvoiceDetails['f072fgstValue'] = 0;
        $masterInvoiceDetails['f072fquantity'] = 1;
        $masterInvoiceDetails['f072fprice'] = $processing_result['f142ffactor_amount'];
        $masterInvoiceDetails['f072fidTaxCode'] = $tax_result['f081fid'];
        $masterInvoiceDetails['f072ftotal'] = $processing_result['f142ffactor_amount'];
        $masterInvoiceDetails['f072ftotalExc'] = $processing_result['f142ffactor_amount'];
        $masterInvoiceDetails['f072ftaxAmount'] = 0;
        $masterInvoiceDetails['f072fisRefundable'] = 0;
        $masterInvoiceDetails['f071fstatus'] = 1;
        $temp = array();
        array_push($temp, $masterInvoiceDetails);
        $masterInvoice['invoice-details'] = $temp;
        $invoiceRepository = $em->getRepository("Application\Entity\T071finvoice");
        $journalRepository = $em->getRepository("Application\Entity\T017fjournal");
        $invoice = $invoiceRepository->createNewMasterInvoice($masterInvoice);
        $invoiceId = $invoice->getF071fid();
        $journalData = array();
        $journalData['status'] = 1;
        $journalData['status'] = 1;
        $temp = array();
        array_push($temp, $invoiceId);
        $journalData['id'] =$temp;
        $invoiceRepository->masterInvoiceApprove($journalData,$journalRepository);
///////////////////////////////////////////////////////////////////////////////////////////

        break;
        case '11':
        $query = "update t043fvehicle_loan set f043ffinal_status =1 where f043fid = $id";
        $result =$em->getConnection()->executeQuery($query);
        break;
    }
}

public function getLoanApprovalList($level,$status)
{
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

    $qb->select('v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate,s.f034fname as staff,ln.f020floanName')
    ->from('Application\Entity\T043fvehicleLoan', 'v')
    ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
    ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','v.f043fidLoanName = ln.f020fid');
    // echo $level;
    // echo $status;
    // die();
    switch ($level) 
    {
        case 1:
        $qb->where('v.f043fapproved1By = :level1')
        ->setParameter('level1',$level)
        ->andWhere('v.f043fapprover1 = :status1')
        ->setParameter('status1',$status);
        break;
        case 2:
        $qb->andWhere('v.f043fapproved2By = :level2')
        ->setParameter('level2',$level)
        ->andWhere('v.f043fapprover2 = :status2')
        ->setParameter('status2',$status)
        ->andWhere('v.f043fapprover1 = 1');
        break;
        case 3:
        $qb->where('v.f043fapprover2=1')
        ->andWhere('v.f043fapproved3By = :level3')
        ->setParameter('level3',$level)
        ->andWhere('v.f043fapprover3 = :status3')
        ->setParameter('status3',$status)
        ->andWhere('s.f034freportingTo = :session')
        ->setParameter('session',(int)$_SESSION['staffId']);
        break;
        case 4:
        $qb->where('v.f043fapprover3=1')
        ->andWhere('v.f043fapproved4By = :level4')
        ->setParameter('level4',$level)
        ->andWhere('v.f043fapprover4 = :status4')
        ->setParameter('status4',$status);
        break;
        case 5:
        $qb->where('v.f043fapprover4=1')
        ->andWhere('v.f043fapproved5By = :level5')
        ->setParameter('level5',$level)
        ->andWhere('v.f043fapprover5 = :status5')
        ->setParameter('status5',$status);
        break;
        case 6:
        $qb->where('v.f043fapprover5=1')
        ->andWhere('v.f043fapproved6By = :level6')
        ->setParameter('level6',$level)
        ->andWhere('v.f043fapprover6 = :status6')
        ->setParameter('status6',$status);
        break;
        case 7:
        $qb->where('v.f043fapprover5=1')
        ->andWhere('v.f043fapproved7By = :level7')
        ->setParameter('level7',$level)
        ->andWhere('v.f043fapprover7 = :status7')
        ->setParameter('status7',$status);
        break;
        case 8:
        $qb->where('v.f043fapprover5=1')
        ->andWhere('v.f043fapproved8By = :level8')
        ->setParameter('level8',$level)
        ->andWhere('v.f043fapprover8 = :status8')
        ->setParameter('status8',$status);
        break;
        case 9:
        $qb->where('v.f043fapprover6=1')
        ->andWhere('v.f043fapprover7=1')
        ->andWhere('v.f043fapprover8=1')
        ->andWhere('v.f043fstatus=0')
        ->andWhere('v.f043ffinalStatus=0');
    }


    $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();

    foreach ($result as $item) {
        $date                        = $item['f043fapplyDate'];
        $item['f043fapplyDate'] = date("Y-m-d", strtotime($date));
        $date                        = $item['f043fstartDate'];
        $item['f043fstartDate'] = date("Y-m-d", strtotime($date));
        $date                        = $item['f043fendDate'];
        $item['f043fendDate'] = date("Y-m-d", strtotime($date));
        $date                        = $item['f043flastDate'];
        $item['f043flastDate'] = date("Y-m-d", strtotime($date));

        array_push($result1, $item);
    }
    $result = array(
        'data' => $result1
    );

    return $result;
}

public function loanGurantorApproval($postData) 
{
    $em = $this->getEntityManager();


    $id=(int)$postData['id'];
    $status=(int)$postData['status'];
    $reason=$postData['reason'];
    $idStaff=$_SESSION['staffId'];

    $query = "update t044floan_gurantors set f044fstatus = $status where f044fid_loan = $id and f044fid_staff = $idStaff";
    $result = $em->getConnection()->executeQuery($query)->fetchAll();

    if($status == 2){
       $query = "update t043fvehicle_loan set f043fapprover2 = $status, f043fapproved2_by=2, f043freason2='$reason' where f043fid = $id";
       $em->getConnection()->executeQuery($query);
       return;
   }



   $query = "select * from t044floan_gurantors where f044fid_loan = $id and f044fstatus = 0";

   $loans_result = $em->getConnection()->executeQuery($query)->fetchAll();
// Mail Code
   $query = "select f020floan_name,f043freference_number from t020floan_name inner join t043fvehicle_loan on f043fid_loan_name = f020fid where f043fid = $id";
   $loan_name_result =$em->getConnection()->executeQuery($query)->fetch(); 
   $loanName = $loan_name_result['f020floan_name'];
   $number = $loan_name_result['f043freference_number'];
   $subject = $loanName.' Application - Application No : '.$number;

   $query = "select f034fname,f034fstaff_id from vw_staff_master where f034fstaff_id = $idStaff";
   $gurantor_result =$em->getConnection()->executeQuery($query)->fetch(); 
   $gurantor_name = $gurantor_result['f034fname'];
// 
   if(!$loans_result){
    $query = "update t043fvehicle_loan set f043fapprover2 = $status, f043fapproved2_by=2, f043freason2='$reason' where f043fid = $id";
    $loans_result = $em->getConnection()->executeQuery($query)->fetchAll();
    $message = "<p>Guarantor: $gurantor_name($idStaff) agreed to be your guarantor.<br>Your application submitted to Head of Department for approval.</p><br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
}
else{
    $message = "<p>Guarantor: $gurantor_name($idStaff) agreed to be your guarantor.</p><br><br><p>Loan Unit, Bursar Office<br>Universiti Utara Malaysia</p>";
}
$query = "select employeeemail from vw_employee inner join t043fvehicle_loan on f043femp_name = employeeid where f043fid = $id";
$email_result =$em->getConnection()->executeQuery($query)->fetch();
if(isset($email_result['employeeemail'])){
    $email = $email_result['employeeemail'];
    $vendorRepository->sendMail1($username,$message,$subject);
}
return;


}

public function updateLevel2($data) 
{
    $em = $this->getEntityManager();

    $id=$data->getF043fid();

    $query = "update t043fvehicle_loan set f043fapprover2 =1, f043fapproved2_by=2 where f043fid = $id";
    $em->getConnection()->executeQuery($query);


}

public function getLoanPendingStatus($id) 
{
    $em = $this->getEntityManager();

    $query = "select f043ftotal_loan, f043femp_name from t043fvehicle_loan where f043fid = $id";
    $result = $em->getConnection()->executeQuery($query)->fetch();

    $emp=$result['f043femp_name'];
    $totalLoan = $result['f043ftotal_loan'];

    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

        // Query

    $qb->select('md.f082famount')
    ->from('Application\Entity\T082fmonthlyDeductionMaster', 'm')
    ->leftjoin('Application\Entity\T082fmonthlyDeductionDetail', 'md', 'with','md.f082fidMaster = m.f082fid')
    ->where('m.f082fidStaff = :staff')
    ->setParameter('staff',$emp);

    $query = $qb->getQuery();
    $amounts = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    $sumArray = array();

    foreach ($amounts as $k=>$amount) 
    {
        foreach ($amount as $id=>$value) 
        {
            $sumArray[$id]+=$value;
        }
    }
    if($totalLoan>$sumArray)
    {
        $status = 0;
    }
    else
    {
        $status =1;
    }

    return $status;


}

public function processLoan($data) 
{
    $em = $this->getEntityManager();

    foreach ($data['id'] as $id) {
        $query = "update t043fvehicle_loan set f043floan_posting=1 where f043fid = $id";

        $em->getConnection()->executeQuery($query);

        $query = "select f043floan_amount,f043fmonthly_installment from t043fvehicle_loan where f043fid = $id";
        $loans = $em->getConnection()->executeQuery($query)->fetch();

        $balance_amount = $loans['f043floan_amount'] - $loans['f043fmonthly_installment'];
        $query = "update t043fvehicle_loan set f043floan_amount=$balance_amount where f043fid = $id";
        $em->getConnection()->executeQuery($query);

        // print_r($loans);
        // print_r($balance_amount);
        // exit();
        $query = "select f043fid_vendor, f043femp_name, f043fmonthly_installment, f043fstart_date, f043fend_date from t043fvehicle_loan where f043fid = $id";
        $loan_result = $em->getConnection()->executeQuery($query)->fetch();
        // print_r($loan_result);
        // // print_r($balance_amount);
        // exit();

        if($loan_result){
           $monthlyDeduction = new T082fmonthlyDeductionMaster();

           $monthlyDeduction->setF082fidStaff((int)$loan_result['f043femp_name'])
           ->setF082fname($loan_result['f043femp_name'])
           ->setF082fstatus((int)"0")
           ->setF082fcreatedBy((int)$_SESSION['userId'])
           ->setF082fupdatedBy((int)$_SESSION['userId']);

           $monthlyDeduction->setF082fcreatedDtTm(new \DateTime())
           ->setF082fupdatedDtTm(new \DateTime());

           $em->persist($monthlyDeduction);
           $em->flush();


           $monthlyDetailObj = new T082fmonthlyDeductionDetail();

           $monthlyDetailObj->setF082fidMaster((int)$monthlyDeduction->getF082fid())
           ->setF082fdeductionCode("LOAN")
           ->setF082famount($loan_result['f043fmonthly_installment'])
           ->setF082fstartDate(new \DateTime($loan_result['f043fstart_date']))
           ->setF082fendDate(new \DateTime($loan_result['f043fend_date']))
           ->setF082fstatus((int)"0")
           ->setF082fcreatedBy((int)$_SESSION['userId'])
           ->setF082fupdatedBy((int)$_SESSION['userId']);

           $monthlyDetailObj->setF082fcreatedDtTm(new \DateTime())
           ->setF082fupdatedDtTm(new \DateTime());


           $em->persist($monthlyDetailObj);

           $em->flush();

       }
   }
}

public function updateStatus($vehicle) 
{
    $em = $this->getEntityManager();

    $vehicle->setF043fapprover1(1)  
    ->setF043fapprover2(1);

    $em->persist($vehicle);
    $em->flush();

}

public function getGuarantorList($staff1,$staff2) 
{
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

    $qb->select('d.f011fdefinitionCode as VehicleCondition,c.f021ffirstName as name,s.f034fname as staff ,fg.f034fname as firstGuarantor, sg.f034fname as secondGuarantor,v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate')
    ->from('Application\Entity\T043fvehicleLoan', 'v')
    ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
    ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','v.f043finsuranceComp = c.f021fid')
    ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
    ->leftjoin('Application\Entity\T034fstaffMaster', 'fg', 'with','v.f043ffirstGuarantor = fg.f034fstaffId')
    ->leftjoin('Application\Entity\T034fstaffMaster', 'sg', 'with','v.f043fsecondGuarantor = sg.f034fstaffId');

    if (!$staff1) 
    {
        $qb->where('v.f043ffirstGuarantor = :firstGuarantorId')
        ->setParameter('firstGuarantorId',$staff1);
    }

    if (!$staff2) 
    {
        $qb->andWhere('v.f043fsecondGuarantor = :secondGuarantorId')
        ->setParameter('secondGuarantorId',$staff2);
    }

    $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    return $result;
}

public function approvedVehicleLoans() 
{
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

    $qb->select('s.f034fname as staff ,v.f043fid,v.f043fidLoanName,v.f043fidVendor,v.f043freferenceNumber,v.f043ffinalStatus,v.f043fvehicleCondition,v.f043fmanufacturer,v.f043fmodel,v.f043fcc,v.f043fcompanyName,v.f043finsuranceComp,v.f043floanEntitlement,v.f043fpurchasePrice,v.f043floanAmount,v.f043finstallmentPeriod,v.f043fdrivingLicense,v.f043fchasisNo,v.f043fengineNo,v.f043fregistrationNo,v.f043fjkrAmount,v.f043ficNo,v.f043fjkrCertificateNo,v.f043fjkrPeriod,v.f043fapplyDate,v.f043fstartDate,v.f043fendDate,v.f043flastDate,v.f043fempName,v.f043ffirstGuarantor,v.f043fsecondGuarantor,v.f043insuranceAmt,v.f043ftotalLoan,v.f043finterestRate,v.f043fmonthInterestRate,v.f043fprofitRatio,v.f043fmonthlyInstallment,v.f043fprincipal,v.f043fprofitAmount,v.f043floanYear,v.f043finsuranceRate,v.f043fpaid,v.f043fbalance')
    ->from('Application\Entity\T043fvehicleLoan', 'v')
            // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','v.f043finsuranceComp = c.f021fid')
    ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
    ->where('v.f043floanAmount > 0')
    ->andWhere('v.f043ffinalStatus = 1')
    ->andWhere('v.f043floanPosting = 0');

    $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    return $result;
}

public function getLoanDetails() 
{
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

    $qb->select('')
    ->from('Application\Entity\T043fvehicleLoan', 'v')
    ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
    ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','v.f043finsuranceComp = c.f021fid')
    ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','v.f043fempName = s.f034fstaffId')
    ->leftjoin('Application\Entity\T034fstaffMaster', 'fg', 'with','v.f043ffirstGuarantor = fg.f034fstaffId')
    ->leftjoin('Application\Entity\T034fstaffMaster', 'sg', 'with','v.f043fsecondGuarantor = sg.f034fstaffId');

    if (!$staff1) 
    {
        $qb->where('v.f043ffirstGuarantor = :firstGuarantorId')
        ->setParameter('firstGuarantorId',$staff1);
    }

    if (!$staff2) 
    {
        $qb->andWhere('v.f043fsecondGuarantor = :secondGuarantorId')
        ->setParameter('secondGuarantorId',$staff2);
    }

    $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    return $result;
}

}
