<?php
namespace Application\Repository;

use Application\Entity\T035ftenderQuotation;
use Application\Entity\T036ftenderCommitee;
use Application\Entity\T038ftenderSpec;
use Application\Entity\T037ftenderGeneral;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class TenderQuotationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("t.f035fid, t.f035fquotationNumber, t.f035fidCategory, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as Department, t.f035fstartDate,t.f035fidFinancialYear, t.f035fendDate, t.f035ftitle, t.f035fidDepartment, t.f035ftotalAmount, t.f035ftenderStatus, t.f035ftype, t.f035fstatus, t.f035fopeningDate, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded")
            ->from('Application\Entity\T035ftenderQuotation','t')
		   ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
           ->orderBy('t.f035fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR)
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f035fid,t.f035fquotationNumber,d.f015fdepartmentName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035fidFinancialYear,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus, t.f035fopeningDate,t.f035fidCategory, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded ')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
		   // ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fid = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $tender_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         
        $qb = $em->createQueryBuilder();
        $qb->select('tc.f036fidStaff,tc.f036fname,st.f034fname')
            ->from('Application\Entity\T036ftenderCommitee','tc')
            ->leftjoin('Application\Entity\T034fstaffMaster','st', 'with','st.f034fstaffId = tc.f036fidStaff')

            ->where('tc.f036fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $committee_result = $query->getArrayResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $qb = $em->createQueryBuilder();
        $qb->select('tc.f037fgenspecs,tc.f037fstatus')
            ->from('Application\Entity\T037ftenderGeneral','tc')

            ->where('tc.f037fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $general_result = $query->getArrayResult();


        $qb = $em->createQueryBuilder();
                $qb->select('ts.f038fid,ts.f038fidItem,ts.f038ftaxCode,ts.f038ffundCode,ts.f038fdepartmentCode,ts.f038factivityCode,ts.f038faccountCode,ts.f038fbudgetFundCode,ts.f038fbudgetDepartmentCode,ts.f038fbudgetActivityCode,ts.f038fbudgetAccountCode,ts.f038fquantity,ts.f038fprice,ts.f038ftotal,ts.f038ftotalIncTax,ts.f038frequiredDate,ts.f038fsoCode,ts.f038ftaxAmount,ts.f038fstatus,ts.f038funit,ts.f038fpercentage')
            ->from('Application\Entity\T038ftenderSpec','ts')
            ->where('ts.f038fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $spec_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $spec1 = array();
        foreach ($spec_result as $spec) {
            $date                        = $spec['f038frequiredDate'];
            $spec['f038frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($spec1, $spec);
        }
        $result1 = $result1[0];
       
        $result1['spec-details'] = $spec1;
        $result1['commitee-details'] = $committee_result;
        $result1['general-details'] = $general_result;
        $result1['tender-result'] = $tender_result;
        return $result1;
    }

   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
         $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']=$data['f035fidCategory'];
        $number = $configRepository->generateFIMS($numberData);
        // print_r($number);exit;
        $tender = new T035ftenderQuotation();

        $tender->setF035fquotationNumber($number)
             ->setF035fidCategory($data['f035fidCategory'])
             ->setF035fstartDate(new \DateTime($data['f035fstartDate']))
             ->setF035fendDate(new \DateTime($data['f035fendDate']))
             ->setF035fopeningDate(new \DateTime($data['f035fopeningDate']))
             ->setF035ftitle($data['f035ftitle'])
             ->setF035fidFinancialYear((int)$data['f035fidFinancialYear'])
             ->setF035fidDepartment($data['f035fidDepartment'])
             ->setF035ftotalAmount((float)$data['f035ftotalAmount'])
             ->setF035ftenderStatus($data['f035ftenderStatus'])
             ->setF035ftype($data['f035ftype'])
             ->setF035fsubmitted((int)0)
             ->setF035fshortlisted((int)0)
             ->setF035fawarded((int)0)
             ->setF035fstatus((int)$data['f035fstatus'])
             ->setF035fcreatedBy((int)$_SESSION['userId'])
             ->setF035fupdatedBy((int)$_SESSION['userId']);

        $tender->setF035fcreatedDtTm(new \DateTime())
                ->setF035fupdatedDtTm(new \DateTime());

        try{
        $em->persist($tender);

        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        $specs = $data['spec-details'];
        foreach ($specs as $spec) {
            
        $specObj = new T038ftenderSpec();

        $specObj->setF038fidItem((int) $spec['f038fidItem'])
            ->setF038ffundCode( $spec['f038ffundCode'])
            ->setF038faccountCode( $spec['f038faccountCode'])
            ->setF038factivityCode( $spec['f038factivityCode'])
            ->setF038fdepartmentCode( $spec['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $spec['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $spec['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $spec['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $spec['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($spec['f038fsoCode'])
            ->setF038funit($spec['f038funit'])
            ->setF038fquantity((int) $spec['f038fquantity'])
            ->setF038fprice((float)$spec['f038fprice'])
            ->setF038ftotal((float)$spec['f038ftotal'])
            ->setF038ftotalIncTax((float)$spec['f038ftotalIncTax'])
            ->setF038ftaxCode($spec['f038ftaxCode'])
            ->setF038fpercentage((float) $spec['f038fpercentage'])
            ->setF038ftaxAmount((float) $spec['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fcreatedBy((int)$_SESSION['userId'])
            ->setF038fupdatedBy((int)$_SESSION['userId'])
            ->setF038fidTender((int)$tender->getF035fid());

        $specObj->setF038fcreatedDtTm(new \DateTime())
                ->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($spec['f038frequiredDate']));

        try{
        $em->persist($specObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }

        $commitees = $data['commitee-details'];
        foreach ($commitees as $commitee) {
            
        $commiteeObj = new T036ftenderCommitee();

        $commiteeObj->setF036fidTender((int)$tender->getF035fid())
             ->setF036fidStaff((int)$commitee['f036fidStaff'])
             ->setF036fname($commitee['f036fname'])
             ->setF036fstatus((int)$data['f035fstatus'])
             ->setF036fcreatedBy((int)$_SESSION['userId'])
             ->setF036fupdatedBy((int)$_SESSION['userId']);

        $commiteeObj->setF036fcreatedDtTm(new \DateTime())
                ->setF036fupdatedDtTm(new \DateTime());

        try{
        $em->persist($commiteeObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }
        $generals = $data['general-details'];
        foreach ($generals as $general) {
            
        $generalObj = new T037ftenderGeneral();

        $generalObj->setF037fidTender((int)$tender->getF035fid())
             ->setF037fgenspecs($general['genspecs'])
             ->setF037fstatus((int)$data['f035fstatus'])
             ->setF037fcreatedBy((int)$_SESSION['userId'])
             ->setF037fupdatedBy((int)$_SESSION['userId']);

        $generalObj->setF037fcreatedDtTm(new \DateTime())
                ->setF037fupdatedDtTm(new \DateTime());

        try{
        $em->persist($generalObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }
        return $tender;

    }

    /* to edit the data in database*/

    public function updateData($tender, $data = []) 
    {
        $em = $this->getEntityManager();

        $tender->setF035fidCategory($data['f035fidCategory'])
        ->setF035fstartDate(new \DateTime($data['f035fstartDate']))
        ->setF035fendDate(new \DateTime($data['f035fendDate']))
        ->setF035ftitle($data['f035ftitle'])
        ->setF035fidFinancialYear((int)$data['f035fidFinancialYear'])
        ->setF035fidDepartment($data['f035fidDepartment'])
        ->setF035ftotalAmount((float)$data['f035ftotalAmount'])
        ->setF035ftenderStatus((int)$data['f035ftenderStatus'])
        ->setF035ftype((int)$data['f035ftype'])
        // ->setF035fsubmitted((int)$data['f035fsubmitted'])
        // ->setF035fshortlisted((int)$data['f035fshortlisted'])
        // ->setF035fawarded((int)$data['f035fawarded'])
        ->setF035fstatus((int)$data['f042fstatus'])
        ->setF035fcreatedBy((int)$_SESSION['userId'])
        ->setF035fupdatedBy((int)$_SESSION['userId']);  

   $tender->setF035fupdatedDtTm(new \DateTime());
        
        $em->persist($tender);
        $em->flush();

        return $tender;

    }


    public function createSpec($tender, $spec = []) 
    {
        $em = $this->getEntityManager();

        $specObj = new T038ftenderSpec();

        $specObj->setF038fidItem((int)$spec['f038fidItem'])
            ->setF038ffundCode( $spec['f038ffundCode'])
            ->setF038faccountCode( $spec['f038faccountCode'])
            ->setF038factivityCode( $spec['f038factivityCode'])
            ->setF038fdepartmentCode( $spec['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $spec['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $spec['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $spec['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $spec['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($spec['f038fsoCode'])
            ->setF038funit($spec['f038funit'])
            ->setF038fquantity((int) $spec['f038fquantity'])
            ->setF038fprice((float)$spec['f038fprice'])
            ->setF038ftotal((float)$spec['f038ftotal'])
            ->setF038ftaxCode((int) $spec['f038ftaxCode'])
            ->setF038fpercentage((float) $spec['f038fpercentage'])
            ->setF038ftaxAmount((float) $spec['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fcreatedBy((int)$_SESSION['userId'])
            ->setF038fupdatedBy((int)$_SESSION['userId'])
            ->setF038fidTender($tender->getF035fid());

        $specObj->setF038fcreatedDtTm(new \DateTime())
                ->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($spec['f038frequiredDate']));

        try{
        $em->persist($specObj);
        $em->flush();
        
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    public function updateSpec($specObj, $spec = []) 
    {
        $em = $this->getEntityManager();



        $specObj->setF038fidItem((int) $spec['f038fidItem'])
            ->setF038ffundCode( $spec['f038ffundCode'])
            ->setF038faccountCode( $spec['f038faccountCode'])
            ->setF038factivityCode( $spec['f038factivityCode'])
            ->setF038fdepartmentCode( $spec['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $spec['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $spec['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $spec['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $spec['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($spec['f038fsoCode'])
            ->setF038funit($spec['f038funit'])
            ->setF038fquantity((int) $spec['f038fquantity'])
            ->setF038fprice((float)$spec['f038fprice'])
            ->setF038ftotal((float)$spec['f038ftotal'])
            ->setF038ftaxCode((int) $spec['f038ftaxCode'])
            ->setF038fpercentage((float) $spec['f038fpercentage'])
            ->setF038ftaxAmount((float) $spec['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fupdatedBy((int)$_SESSION['userId']);

        $specObj->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($spec['f038frequiredDate']));

        try{
        $em->persist($specObj);

        $em->flush();
        // print_r($specObj);exit;
        }
        catch(\Exception $e){
            echo $e;
        }

    }

    public function createCommitee($tender, $commitee = []) 
    {
        $em = $this->getEntityManager();

       $commiteeObj = new T036ftenderCommitee();

        $commiteeObj->setF036fidTender((int)$tender->getF035fid())
             ->setF036fidStaff((int)$commitee['f036fidStaff'])
             ->setF036fname($commitee['f036fname'])
             ->setF036fstatus((int)$data['f035fstatus'])
             ->setF036fcreatedBy((int)$_SESSION['userId'])
             ->setF036fupdatedBy((int)$_SESSION['userId']);

        $commiteeObj->setF036fcreatedDtTm(new \DateTime())
                ->setF036fupdatedDtTm(new \DateTime());

        try{
        $em->persist($commiteeObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }

     public function updateCommitee($commiteeObj, $commitee = []) 
    {
        $em = $this->getEntityManager();


        $commiteeObj->setF036fidStaff((int)$commitee['f036fidStaff'])
             ->setF036fname($commitee['f036fname'])
             ->setF036fstatus((int)$data['f035fstatus'])
             ->setF036fupdatedBy((int)$_SESSION['userId']);

        $commiteeObj->setF036fupdatedDtTm(new \DateTime());

        try{
        $em->persist($commiteeObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }

     public function createGeneral($tender, $general = []) 
    {
        $em = $this->getEntityManager();

      $generalObj = new T037ftenderGeneral();

        $generalObj->setF037fidTender((int)$tender->getF035fid())
             ->setF037fgenspecs($general['genspecs'])
             ->setF037fstatus((int)$data['f035fstatus'])
             ->setF037fcreatedBy((int)$_SESSION['userId'])
             ->setF037fupdatedBy((int)$_SESSION['userId']);

        $generalObj->setF037fcreatedDtTm(new \DateTime())
                ->setF037fupdatedDtTm(new \DateTime());

        try{
        $em->persist($generalObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }

     public function updateGeneral($generalObj, $general = []) 
    {
        $em = $this->getEntityManager();


         $generalObj->setF037fidTender((int)$tender->getF035fid())
             ->setF037fgenspecs($general['genspecs'])
             ->setF037fstatus((int)$data['f035fstatus'])
             ->setF037fupdatedBy((int)$_SESSION['userId']);

        $generalObj->setF036fupdatedDtTm(new \DateTime());

        try{
        $em->persist($generalObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('t.f035fid,t.f035fquotationNumber,t.f035fidCategory,d.f015fdepartmentName,i.f027fcategoryName,t.f035fstartDate,t.f035fendDate,t.f035fidFinancialYear,t.f035ftitle,t.f035fidDepartment,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fid')
		   ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fstatus=1');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        return $result1;
        
    }

    // public function getListBasedOnTendor($id) 
    // {
    //     $em = $this->getEntityManager();

    //     $qb = $em->createQueryBuilder();

    //     // Query
    //      $qb->select('tq.f035fid, tq.f035fquotationNumber, tq.f035fidCategory, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle, tq.f035fidDepartment, tq.f035ftotalAmount, tq.f035ftenderStatus, tq.f035ftype, ts.f050fid, ts.f050fidVendor, ts.f050fvendorCode, ts.f050fgrade, ts.f050famount, ts.f050fcompletionDuration, ts.f050fdescription, ts.f050fvendorName, tc.f051fid, tc.f051fidStaff, tc.f051fdescription')
    //         ->from('Application\Entity\T035ftenderQuotation','tq')
    //         ->leftjoin('Application\Entity\T050ftenderSubmission', 'ts','with','ts.f050fidTendor = tq.f035fid')
    //         ->leftjoin('Application\Entity\T051ftenderCommitee', 'tc','with','tc.f051fidSubmission = ts.f050fid')
    //         ->where('tq.f035fid = :tenderId')
    //         ->setParameter('tenderId',$id);
           
    //     $query = $qb->getQuery();

    //     $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    //     return $result;
        
    // }

    public function getListBasedOnTendor($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $qb->select('tq.f035fid, tq.f035fquotationNumber, tq.f035fidCategory, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle,tq.f035fidFinancialYear, tq.f035fidDepartment, tq.f035ftotalAmount, tq.f035ftenderStatus, tq.f035ftype,tq.f035fopeningDate, tq.f035fsubmitted, tq.f035fshortlisted, tq.f035fawarded')

            ->from('Application\Entity\T035ftenderQuotation', 'tq')
            
            
            ->where('tq.f035fid =:tendorId')
            ->setParameter('tendorId', $id);

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = $result1[0]; 
       
        $qb = $em->createQueryBuilder();
        $qb->select('ts.f050fid, ts.f050fidVendor, ts.f050fvendorCode, ts.f050fgrade, ts.f050famount, ts.f050fcompletionDuration,ts.f050fcompletionDuration as f050fcompletion, ts.f050fdescription, ts.f050fvendorName, ts.f050fisShortlisted, ts.f050fisAwarded')

            ->from('Application\Entity\T050ftenderSubmission', 'ts')
            
            ->where('ts.f050fidTendor =:tendorId')
            ->setParameter('tendorId', $id);

        $query = $qb->getQuery();
        $result2 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1['submission-details'] = $result2;

        $qb = $em->createQueryBuilder();
        $qb->select('tc.f051fid, tc.f051fidStaff, tc.f051fdescription, s.f034fname')

            ->from('Application\Entity\T051ftenderCommitee', 'tc')
            ->leftjoin('Application\Entity\T034fstaffMaster','s', 'with','tc.f051fidStaff = s.f034fstaffId')
            
            ->where('tc.f051fidTender =:tendorId')
            ->setParameter('tendorId', $id);

        $query = $qb->getQuery();
        $result3 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1['commitee-details'] = $result3;

        

        return $result1;
    }


    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }


    public function deleteTenderSpec($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t038ftender_spec WHERE f038fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function getTenderSubmission($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f035fid,t.f035fquotationNumber,d.f015fdepartmentName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035fidFinancialYear,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus, t.f035fopeningDate,t.f035fidCategory, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded ')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
           // ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fsubmitted = :tenderId')
            ->andWhere('t.f035fshortlisted = 0')
            ->andWhere('t.f035fawarded = 0')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f035fopeningDate'];
            $item['f035fopeningDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fstartDate'];
            $item['f035fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fendDate'];
            $item['f035fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        return $result1;



        // return $result;

    }

    public function getTenderShortlisted($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f035fid,t.f035fquotationNumber,d.f015fdepartmentName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035fidFinancialYear,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus, t.f035fopeningDate,t.f035fidCategory, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded ')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
           // ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fshortlisted = :tenderId')
            ->andWhere('t.f035fsubmitted = 1')
            ->andWhere('t.f035fawarded = 0')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f035fopeningDate'];
            $item['f035fopeningDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fstartDate'];
            $item['f035fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fendDate'];
            $item['f035fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        return $result1;
        // return $result;

    }

    public function getTenderAwarded($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f035fid,t.f035fquotationNumber,d.f015fdepartmentName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035fidFinancialYear,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus, t.f035fopeningDate,t.f035fidCategory, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded ')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
           // ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fawarded = :tenderId')
            ->setParameter('tenderId',(int)$id)
            ->andWhere('t.f035fsubmitted = 1')
            ->andWhere('t.f035fshortlisted = 1');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f035fopeningDate'];
            $item['f035fopeningDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fstartDate'];
            $item['f035fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f035fendDate'];
            $item['f035fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        return $result1;
        // return $result;

    }
}

?>
