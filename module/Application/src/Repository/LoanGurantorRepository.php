<?php
namespace Application\Repository;

use Application\Entity\T044floanGurantors;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LoanGurantorRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("f.f044fid, f.f044fidStaff, f.f044fidLoan, s.f034fname, l.f043fidLoanName, l.f043freferenceNumber, f.f044fapprovedDate")
            ->from('Application\Entity\T044floanGurantors','f')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','f.f044fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T043fvehicleLoan', 'l', 'with','f.f044fidLoan = l.f043fid');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
      $qb->select("f.f044fid,f.f044fidStaff,f.f044fidLoan,s.f034fname,l.f043fidLoanName,l.f043freferenceNumber,f.f044fapprovedDate")
            ->from('Application\Entity\T044floanGurantors','f')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','f.f044fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T043fvehicleLoan', 'l', 'with','f.f044fidLoan = l.f043fid')
            ->where('f.f044fid = :Id')
            ->setParameter('Id',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $loan = new T044floanGurantors();

        $loan->setF044fidStaff((int)$data['f044fname'])
             ->setF044fidLoan((int)$data['f044fcode'])
             ->setF044fstatus((int)"1")
             ->setF044fcreatedBy((int)$_SESSION['userId'])
             ->setF044fupdatedBy((int)$_SESSION['userId']);

        $loan->setF044fcreatedDtTm(new \DateTime())
                ->setF044fupdatedDtTm(new \DateTime());
               
            try{

        $em->persist($loan);
        
        $em->flush();
        
            }
            catch(\Exception $e){
                echo $e;
            }

        return $loan;

    }

    /* to edit the data in database*/

    public function updateData($loan, $data = []) 
    {
        $em = $this->getEntityManager();

       $loan->setF044fidStaff((int)$data['f044fname'])
             ->setF044fidLoan((int)$data['f044fcode'])
             ->setF044fstatus((int)"1")
             ->setF044fupdatedBy((int)$_SESSION['userId']);

        $loan->setF044fupdatedDtTm(new \DateTime());
        

        try{
        $em->persist($loan);
        
        $em->flush();
 }
 catch(\Exception $e){
    echo $e;
 }

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("f.f044fid, f.f044fname, f.f044fcode,f.f044fcode+' - ' +f.f044fname as f044fname, f.f044fstatus, f.f044fcreatedBy, f.f044fupdatedBy")
            ->from('Application\Entity\T044floanGurantors','f')
            ->where('f.f044fstatus=1')
	    ->orderBy('f044fname');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
}

?>
