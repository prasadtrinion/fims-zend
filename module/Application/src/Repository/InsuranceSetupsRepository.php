<?php
namespace Application\Repository;

use Application\Entity\T114finsuranceSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class InsuranceSetupsRepository extends EntityRepository 
{
 
    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     

 
        $qb->select("fs.f114fid, fs.f114fname, fs.f114fvendor,  fs.f114fstatus, (vr.f030fvendorCode+' - '+vr.f030fcompanyName) as VendorDetails")
            ->from('Application\Entity\T114finsuranceSetup','fs')
            ->leftjoin('Application\Entity\T030fvendorRegistration','vr','with', 'vr.f030fid = fs.f114fid')
             ->orderBy('fs.f114fid','DESC');
            
 		$query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("fs.f114fid, fs.f114fname, fs.f114fvendor,  fs.f114fstatus, (vr.f030fvendorCode+' - '+vr.f030fcompanyName) as VendorDetails")
            ->from('Application\Entity\T114finsuranceSetup','fs')
            ->leftjoin('Application\Entity\T030fvendorRegistration','vr','with', 'vr.f030fid = fs.f114fid')
            ->where('fs.f114fid = :InsuranceId')
            ->setParameter('InsuranceId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $insurance = new T114finsuranceSetup();

        $insurance->setF114fname($data['f114fname'])
        		->setF114fvendor($data['f114fvendor'])
                ->setF114fstatus((int)$data['f114fstatus'])
                ->setF114fcreatedBy((int)$_SESSION['userId']);

        $insurance->setF114fcreatedDtTm(new \DateTime());

        try{
        $em->persist($insurance);
        
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $insurance;
    }

    public function updateData($insurance, $data = []) 
    {
        $em = $this->getEntityManager();
        $insurance->setF114fname($data['f114fname'])
        		->setF114fvendor($data['f114fvendor'])
                ->setF114fstatus((int)$data['f114fstatus'])
                ->setF114fupdatedBy((int)$_SESSION['userId']);

        $insurance->setF114fupdatedDtTm(new \DateTime());
        try{
        $em->persist($insurance);
        $em->flush();
        }
        catch (\Exception $e){
            echo $e;
        }
    }  
} 
?>