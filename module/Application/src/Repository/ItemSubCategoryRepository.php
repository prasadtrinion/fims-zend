<?php
namespace Application\Repository;

use Application\Entity\T028fitemSubCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ItemSubCategoryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f028fid, s.f028fidCategory, c.f027fcategoryName as category, s.f028fcode +'-'+s.f028fsubCategoryName as originalname,s.f028fcode,s.f028fsubCategoryName, s.f028fstatus")
            ->from('Application\Entity\T028fitemSubCategory','s')
            ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
            ->orderBy('s.f028fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f028fid, s.f028fidCategory, c.f027fcategoryName as category, s.f028fcode +'-'+s.f028fsubCategoryName as originalname,s.f028fcode,s.f028fsubCategoryName, s.f028fstatus")
            ->from('Application\Entity\T028fitemSubCategory','s')
            ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
            ->orderBy('s.f028fid','DESC')
            ->where('s.f028fstatus = 1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('s.f028fid, s.f028fidCategory, c.f027fcategoryName as category,  s.f028fsubCategoryName,s.f028fcode, s.f028fstatus')
            ->from('Application\Entity\T028fitemSubCategory','s')
            ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
             ->where('s.f028fid = :subCategoryId')
            ->setParameter('subCategoryId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
     public function getItemSubCategoriesByCategory($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("s.f028fid, s.f028fidCategory, c.f027fcategoryName as category,  s.f028fcode +'-'+s.f028fsubCategoryName as f028fsubCategoryName,s.f028fcode, s.f028fstatus")
            ->from('Application\Entity\T028fitemSubCategory','s')
            ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
             ->where('s.f028fidCategory = :subCategoryId')
            ->setParameter('subCategoryId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $subCategory = new T028fitemSubCategory();

        $subCategory->setF028fidCategory((int)$data['f028fidCategory'])
             ->setF028fsubCategoryName($data['f028fsubCategoryName'])
             ->setF028fcode($data['f028fcode'])
             ->setF028fstatus((int)$data['f028fstatus'])
             ->setF028fcreatedBy((int)$_SESSION['userId'])
             ->setF028fupdatedBy((int)$_SESSION['userId']);

        $subCategory->setF028fcreatedDtTm(new \DateTime())
                ->setF028fupdatedDtTm(new \DateTime());

 
        $em->persist($subCategory);
        $em->flush();

        return $subCategory;

    }

    public function updateData($subCategory, $data = []) 
    {
        $em = $this->getEntityManager();

        $subCategory->setF028fidCategory($data['f028fidCategory'])
             ->setF028fsubCategoryName($data['f028fsubCategoryName'])
             ->setF028fcode($data['f028fcode'])
             ->setF028fstatus((int)$data['f028fstatus'])
             ->setF028fupdatedBy((int)$_SESSION['userId']);

        $subCategory->setF028fupdatedDtTm(new \DateTime());
        
        $em->persist($subCategory);
        $em->flush();

    }
    public function getSubCategory($category)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("s.f028fid, s.f028fidCategory, s.f028fcode +'-'+s.f028fsubCategoryName as originalname,s.f028fcode,s.f028fsubCategoryName, s.f028fstatus, s.f028fid as subCategoryId, (s.f028fcode +'-'+s.f028fsubCategoryName) as subCategoryName")
            ->from('Application\Entity\T028fitemSubCategory', 's')
            // ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
             ->where('s.f028fidCategory = :subCategoryId')
            ->setParameter('subCategoryId',$category)
            ->orderBy('s.f028fsubCategoryName','ASC');

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function getItemBySubCategory($category)
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("i.f088fid, i.f088fidItemGroup, i.f088fidCategory, i.f088fidSubCategory, i.f088fidType, i.f088fitemCode, i.f088fitemDescription, i.f088fstatus, i.f088fid as itemId, (i.f088fitemCode+'-'+ i.f088fitemDescription) as itemName")
            ->from('Application\Entity\T088fitem', 'i')
             ->where('i.f088fidSubCategory = :subCategoryId')
            ->setParameter('subCategoryId',$category)
            ->orderBy('i.f088fitemCode','ASC');

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function getCategoryItem() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f028fid, s.f028fidCategory, c.f027fcategoryName as category, s.f028fcode +'-'+s.f028fsubCategoryName as originalname,s.f028fcode,s.f028fsubCategoryName, s.f028fstatus")
            ->from('Application\Entity\T028fitemSubCategory','s')
            ->leftjoin('Application\Entity\T027fitemCategory', 'c', 'with', 's.f028fidCategory = c.f027fid')
            ->where("c.f027fcategoryName = 'Supply'")
            ->orderBy('s.f028fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
}
