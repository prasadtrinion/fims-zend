<?php
namespace Application\Repository;

use Application\Entity\T071finvoice;
use Application\Entity\T072finvoiceDetails;
use Application\Entity\T017fjournal;
use Application\Entity\T018fjournalDetails;
use Application\Entity\T060fstudentInvoices;
use Application\Repository\JournalRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class MasterInvoiceRepository extends EntityRepository
{

    public function getMasterInvoiceList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder(); 
    $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071fbill_type as f071fbillType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071fbalance,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom,i.f071fcr_amount as f071fcrAmount,i.f071fdr_amount as f071fdrAmount,i.f071ftotal_paid as f071ftotalPaid,i.f071fdiscount, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType,i.f071ftype,i.f071fid_request_invoice as f071fidRequestInvoice, invd.f072fbalance_amount as f072fbalanceAmount from t071finvoice as i inner join t132frevenue_type as rt on cast(i.f071fbill_type as int) = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') inner join t072finvoice_details as invd on invd.f072fid_invoice = i.f071fid";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
       
        $result1 = array();
        foreach ($result as $invoice) {
            $STA                        = $invoice['f034fname'];
            $STD                        = $invoice['f060fname'];
            $OR                        = $invoice['f021ffirst_name'].$invoice['f021flast_name'];

            if($STA!=null){
                $name = $STA;
            }
            if($STD!=null){
                $name = $STD;
            }
            if($OR!=null){
                $name = $OR;
            }
            $invoice['customerName'] = $name;
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        // Query 
        // $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071fbillType,s.f060fname as studentName,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom, i.f071fcrAmount, i.f071fdrAmount, i.f071ftotalPaid, i.f071fbalance, i.f071freason,i.f071fstatus,i.f071ftype,i.f071fidRequestInvoice')
        //     ->from('Application\Entity\T071finvoice', 'i')
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
        //     ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
        //     ->where('i.f071fprocessed = 0')
        //     ->orderby('i.f071fid','DESC')
        //     ;
        // $query   = $qb->getQuery();
        // $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $result1 = array();
        // foreach ($result as $invoice) {
        //     $date                        = $invoice['f071finvoiceDate'];
        //     $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
        //     array_push($result1, $invoice);
        // }

        // $result = array(
        //     'data' => $result1,
        // );

        return $result;

    }

    public function invoicesByType($data)
    {
        $idCustomer = $data['idCustomer'];
        $type = $data['type'];


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

         $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071fbillType,s.f060fname as studentName,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom, i.f071fcrAmount, i.f071fdrAmount, i.f071ftotalPaid, i.f071fbalance, i.f071freason,i.f071ftype,i.f071fidRequestInvoice')
            ->from('Application\Entity\T071finvoice', 'i')
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fidCustomer = :customerId')
            ->orderby('i.f071fid','DESC')
                            
                ->AndWhere('i.f071finvoiceType = :type')
            ->orderby('i.f071fid','DESC')

                ->setParameter('customerId', $idCustomer)
                ->setParameter('type', $type);
            
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getCustomerMasterInvoices($postData)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();


        $id = (int)$postData['UserId'];
        $invoiceType = $postData['type'];
        // print_r($id);exit;

        // Query
        $qb->select('distinct(i.f071fid) as f071fid,i.f071finvoiceNumber,i.f071finvoiceType,s.f060fname as studentName,i.f071fbillType,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fidCustomer,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom,i.f071fcrAmount, i.f071fdrAmount, i.f071ftotalPaid, i.f071fbalance, i.f071ftype, i.f071fidRequestInvoice')
            ->from('Application\Entity\T071finvoice', 'i')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fidCustomer = :customerId')
            ->andWhere('i.f071fprocessed = 0')
            ->andWhere('i.f071fstatus = 1')
            ->andWhere('i.f071fisPaid = 0')
            ->andWhere('i.f071finvoiceType = :invoiceType')
            ->orderby('i.f071fid','DESC')
            ->setParameter('customerId', $id)
            ->setParameter('invoiceType', $invoiceType);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        $result = array(
            'data' => $result1,
        );
        return $result;

    }
    public function customerMasterInvoiceDetails($postData)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();


        $id = (int)$postData['UserId'];
        $invoiceType = $postData['type'];
        // print_r($id);exit;

        // Query
        $qb->select('i.f071fid,i.f071finvoiceNumber,id.f072fid,id.f072fidItem,id.f072fgstCode,id.f072fgstValue,id.f072fdebitFundCode,id.f072fdebitDepartmentCode,id.f072fdebitActivityCode,id.f072fdebitAccountCode,id.f072fcreditFundCode,id.f072fcreditDepartmentCode,id.f072fcreditActivityCode,id.f072fcreditAccountCode,id.f072fbalanceAmount')
            ->from('Application\Entity\T071finvoice', 'i')
                ->leftjoin('Application\Entity\T072finvoiceDetails', 'id', 'with', 'i.f071fid = id.f072fidInvoice')
            ->where('i.f071fidCustomer = :customerId')
            ->andWhere('i.f071fprocessed = 0')
            ->andWhere('i.f071fstatus = 1')
            ->andWhere('i.f071fisPaid = 0')
            ->andWhere('i.f071finvoiceType = :invoiceType')
            ->orderby('i.f071fid','DESC')
            ->setParameter('customerId', $id)
            ->setParameter('invoiceType', $invoiceType);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        $result = array(
            'data' => $result1,
        );
        return $result;

    }
     public function getCustomerMasterInvoices1($postData)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();


        $id = (int)$postData['UserId'];
        $invoiceType = $postData['type'];
        // print_r($id);exit;

        // Query
        $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,s.f060fname as FullName,i.f071fbillType,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fidCustomer,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom,i.f071fcrAmount,i.f071fdrAmount,i.f071ftotalPaid,i.f071fbalance,i.f071ftype,i.f071fidRequestInvoice')
            ->from('Application\Entity\T071finvoice', 'i')
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fidCustomer = :customerId')
            ->andWhere('i.f071fprocessed = 0')
            ->andWhere('i.f071fstatus = 1')
            ->andWhere('i.f071finvoiceType = :invoiceType')
            ->andWhere('i.f071fisPaid = 0')
            ->orderby('i.f071fid','DESC')
            ->setParameter('customerId', $id)
            ->setParameter('invoiceType', $invoiceType);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        $result = array(
            'data' => $result1,
        );
        return $result;

    }

    public function getMasterInvoiceById($id, $approvedStatus)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if ($approvedStatus == 0 && isset($approvedStatus)) {
             $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071fbalance,i.f071finvoice_type as f071finvoiceType,i.f071fbill_type as f071fbillType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType,i.f071ftype,i.f071fid_request_invoice as f071fidRequestInvoice from t071finvoice as i inner join t132frevenue_type as rt on cast(i.f071fbill_type as int) = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 0 and i.f071fis_rcp = $id and i.f071fprocessed = 0 order by i.f071fid DESC";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();

        } elseif ($approvedStatus == 1) {

             $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071fbalance,i.f071finvoice_type as f071finvoiceType,i.f071fbill_type as f071fbillType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType,i.f071ftype,i.f071fid_request_invoice as f071fidRequestInvoice from t071finvoice as i inner join t132frevenue_type as rt on cast(i.f071fbill_type as int) = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 1 and i.f071fis_rcp = $id and i.f071fprocessed = 0 order by i.f071fid DESC";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();

        } elseif ($approvedStatus == 2) {
             $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071fbalance,i.f071finvoice_type as f071finvoiceType,i.f071fbill_type as f071fbillType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType,i.f071ftype,i.f071fid_request_invoice as f071fidRequestInvoice from t071finvoice as i inner join t132frevenue_type as rt on cast(i.f071fbill_type as int) = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 2 and i.f071fis_rcp = $id and i.f071fprocessed = 0 order by i.f071fid DESC";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
        } else {
            $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071fbalance,i.f071finvoiceType,i.f071fbillType,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom,id.f072fid,id.f072fidItem,id.f072fgstCode,id.f072fgstValue,id.f072fdebitFundCode,id.f072fdebitDepartmentCode,id.f072fdebitActivityCode,id.f072fdebitAccountCode,id.f072fcreditFundCode,id.f072fcreditDepartmentCode,id.f072fcreditActivityCode,id.f072fcreditAccountCode,id.f072fquantity,id.f072fprice,id.f072ftotal,id.f072fstatus,id.f072fidTaxCode,id.f072ftotalExc,id.f072ftaxAmount, i.f071freason,i.f071ftype,i.f071fidRequestInvoice, id.f072fbalanceAmount')
                ->from('Application\Entity\T071finvoice', 'i')
                ->leftjoin('Application\Entity\T072finvoiceDetails', 'id', 'with', 'i.f071fid = id.f072fidInvoice')
                ->where('i.f071fid = :invoiceId')
                ->AndWhere('i.f071fprocessed = 0')
            ->orderby('i.f071fid','DESC')

                ->setParameter('invoiceId', $id);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        return $result1;
    }

    public function createMasterInvoiceDetail($invoiceObj, $invoiceDetail)
    {
        $em = $this->getEntityManager();

        $invoiceDetailObj = new T072finvoiceDetails();
        $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
            ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
            ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
            ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
            ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
            ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
            ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
            ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
            ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
            ->setF072fgstCode($invoiceDetail['f072fgstCode'])
            ->setF072fgstValue((int)$invoiceDetail['f072fgstValue'])
            ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
            ->setF072fprice((float)$invoiceDetail['f072fprice'])
            ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
            ->setF072freason($invoiceDetail['f072freason'])
            ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
            ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
            ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
            ->setF072fisRefundable((int) $invoiceDetail['f072fisRefundable'])
            ->setF072fstatus((int) $invoiceDetail['f071fstatus'])
            ->setF072fcreatedBy((int)$_SESSION['userId'])
            ->setF072fupdatedBy((int)$_SESSION['userId'])
            ->setF072fidInvoice($invoiceObj->getF071fid());

        $invoiceDetailObj->setF072fcreatedDtTm(new \DateTime()) 
                         ->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($invoiceDetailObj);
            $em->flush();
            // print_r($invoiceDetailObj);exit;
            
        } catch (\Exception $e) {
            echo $e;
        }
    }
    public function updateMasterInvoice($invoice, $data = [])
    {
        $em = $this->getEntityManager();

         $invoice->setF071finvoiceNumber($data['f071finvoiceNumber'])
            ->setF071finvoiceType($data['f071finvoiceType'])
            ->setF071fbillType((int)$data['f071fbillType'])
            ->setF071finvoiceDate(new \DateTime($data['f071finvoiceDate']))
            // ->setF071fidFinancialYear($data['f071fIdFinancialYear'])
            ->setF071fidFinancialYear(1)
            ->setF071fdescription($data['f071fdescription'])
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            ->setF071fidRequestInvoice((int) $data['f071fidRequestInvoice'])
            ->setF071ftype($data['f071ftype'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071freason($data['f071freason'])
            ->setF071finvoiceTotal((float)$data['f071finvoiceTotal'])
            ->setF071fbalance((float) $data['f071finvoiceTotal'])
            ->setF071fcrAmount((float) $data['f071fcrAmount'])
            ->setF071fdrAmount((float) $data['f071fdrAmount'])
            ->setF071ftotalPaid((float) $data['f071ftotalPaid'])
            ->setF071fdiscount((float) $data['f071fdiscount'])
            ->setF071fonlinePayment((float) $data['f071fonlinePayment'])
            ->setF071fisRcp((int) $data['f071fisRcp'])
            ->setF071finvoiceFrom((int) $data['f071finvoiceFrom'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $invoice->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($invoice);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $invoice;
    }
    public function updateMasterInvoiceDetail($invoiceDetailObj, $invoiceDetail)
    {
        
        $em = $this->getEntityManager();

        $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
        ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
        ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
        ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
        ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
        ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
        ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
        ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
        ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
        ->setF072fgstCode($invoiceDetail['f072fgstCode'])
        ->setF072freason($invoiceDetail['f072freason'])
        ->setF072fgstValue((float)$invoiceDetail['f072fgstValue'])
        ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
        ->setF072fprice((float)$invoiceDetail['f072fprice'])
        ->setF072fbalanceAmount((float)$invoiceDetail['f072ftotal'])
        ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
        ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
        ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
        ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
        ->setF072fstatus((int) $invoiceDetail['f071fstatus'])
            ->setF072fupdatedBy((int)$_SESSION['userId']);

        $invoiceDetailObj->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($invoiceDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public function createNewMasterInvoice($data)
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']=$data['f071finvoiceType'];
        $number = $configRepository->generateFIMS($numberData);
         $financial_query = "select f015fid from t015ffinancialyear where f015fstatus = 1";
         $financial_result = $em->getConnection()->executeQuery($financial_query)->fetch();
         $data['f071fIdFinancialYear'] = $financial_result['f015fid'];
        $invoice = new T071finvoice();
 
        $invoice->setF071finvoiceNumber($number)
            ->setF071finvoiceType($data['f071finvoiceType'])
            ->setF071fbillType((int)$data['f071fbillType'])
            ->setF071finvoiceDate(new \DateTime($data['f071finvoiceDate']))
            ->setF071fidFinancialYear((int)$data['f071fIdFinancialYear'])
            // ->setF071fidFinancialYear((int)1)
            ->setF071fdescription($data['f071fdescription'])
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fidRequestInvoice((int) $data['f071fidRequestInvoice'])
            ->setF071ftype($data['f071ftype'])
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071freason($data['f071freason'])
            ->setF071finvoiceTotal((float)$data['f071finvoiceTotal'])
            ->setF071fbalance((float) $data['f071finvoiceTotal'])
            ->setF071fcrAmount((float) $data['f071fcrAmount'])
            ->setF071fdrAmount((float) $data['f071fdrAmount'])
            ->setF071ftotalPaid((float) $data['f071ftotalPaid'])
            ->setF071fdiscount((float) $data['f071fdiscount'])
            ->setF071fonlinePayment((float) $data['f071fonlinePayment'])
            ->setF071fisRcp((int) $data['f071fisRcp'])
            ->setF071fisPaid((int)0)
            ->setF071finvoiceFrom((int) $data['f071finvoiceFrom'])
            ->setF071fcreatedBy((int)$_SESSION['userId'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $invoice->setF071fcreatedDtTm(new \DateTime())
            ->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($invoice);
            $em->flush();

        } catch (\Exception $e) {
            echo $e;
        }

        $invoiceDetails = $data['invoice-details'];

            // print_r($invoiceDetails);exit;
        foreach ($invoiceDetails as $invoiceDetail)
        {


            $invoiceDetailObj = new T072finvoiceDetails();
            $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
            ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
            ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
            ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
            ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
            ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
            ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
            ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
            ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
            ->setF072fgstCode($invoiceDetail['f072fgstCode'])
            ->setF072freason($invoiceDetail['f072freason'])
            ->setF072fgstValue((float)$invoiceDetail['f072fgstValue'])
            ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
            ->setF072fprice((float)$invoiceDetail['f072fprice'])
            ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
            ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
            ->setF072fbalanceAmount((float)$invoiceDetail['f072ftotal'])
            ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
            ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
            ->setF072fisRefundable((int) $invoiceDetail['f072fisRefundable'])
            ->setF072fstatus((int) $invoiceDetail['f071fstatus'])
                ->setF072fcreatedBy((int)$_SESSION['userId'])
                ->setF072fidInvoice($invoice->getF071fid());

            $invoiceDetailObj->setF072fcreatedDtTm(new \DateTime());
               
            try {
                $em->persist($invoiceDetailObj);
// print_r($invoiceDetailObj);exit;

                $em->flush();



            } catch (\Exception $e) {
                echo $e;
            }
        }
        return $invoice;
    }
    
    public function invoiceCron($postData)
    {

        $em = $this->getEntityManager();

        $feeStructureId = $postData['feeStructureId'];
        foreach($postData['studentId'] as $studentId){

            $year = date('y'); 
            //Student Invoice Number
            $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='STD';
        $number = $configRepository->generateFIMS($numberData);

            $invoice_data['f071finvoiceNumber'] = $number;
            $invoice_data['f071finvoiceType'] = "STD";
            $invoice_data['f071finvoiceDate'] = date('Y-m-d');
            $invoice_data['f071fidCustomer'] = $studentId;
            $invoice_data['f071fstatus'] = 1;
            $invoice_data['f071fisRcp'] = 1;
            $invoice_data['f071finvoiceFrom'] = 2;

            $query = "select f048fid_fee_item as f072fidItem,f048famount as f072fprice,f048fid_glcode as f072fidCreditGlcode,f066fid_glcode as f072fidDebitGlcode,f066fcode as f072fgstCode,f066fid_tax_code as f072fid_tax_code,f066fgst_value as f072fgstValue from t048ffee_structure_master inner join t048ffee_structure_details on f048fid_fee = f048fid inner join t066ffee_item on f066fid = f048fid_fee_item where f048fid = $feeStructureId";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $totalAmount = 0;
            for($i=0;$i<count($result);$i++){
                $result[$i]['f072ftaxAmount'] = (float)($result[$i]['f072fgstValue'] * $result[$i]['f072fprice'])/100;
                $result[$i]['f072ftotal'] = (float)($result[$i]['f072ftaxAmount'] + $result[$i]['f072fprice']);
                $result[$i]['f072ftotalExc'] = (float) $result[$i]['f072fprice'];
                $result[$i]['f072fquantity'] = 1;
                $totalAmount = (float)($totalAmount + $result[$i]['f072ftotalExc']);
            }
            $invoice_data['f071finvoiceTotal'] = $totalAmount;

            $invoice_data['invoice-details'] = $result;
            $this->createNewMasterInvoice($invoice_data);
        }
        return;
    }

    public function masterInvoiceApprove($postData,$journalRepository)
    {
        $em = $this->getEntityManager();
         $reason = $postData['reason'];
        $status = (int)$postData['status'];
        foreach ($postData['id'] as $id)
        {
            $query1 = "UPDATE t071finvoice set f071fstatus = $status, f071freason ='$reason' where f071fid = $id";
            $em->getConnection()->executeQuery($query1);
            $query2 = "update t072finvoice_details set f072fstatus = 1 where f072fid_invoice = $id";
            $em->getConnection()->executeQuery($query2);
           
            $query2 = "select f071fid_financial_year,f071finvoice_date,f071fdescription,f071fapproved_by,f071fcreated_by,f071fcreated_dt_tm,f071finvoice_number, f071freason from t071finvoice where f071fid = $id";
            $invoice_result = $em->getConnection()->executeQuery($query2)->fetch();
            $query2 = "select SUM(f072ftotal) as item_debit,f072fdebit_account_code as account_code,f072fdebit_activity_code as activity_code,f072fdebit_department_code as department_code,f072fdebit_fund_code as fund_code, f072freason from t072finvoice_details where f072fid_invoice = $id group by f072fdebit_account_code,f072fdebit_activity_code,f072fdebit_department_code,f072fdebit_fund_code";
            $debit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
            $query2 = "select SUM(f072ftotal) as item_credit,f072fcredit_account_code as account_code,f072fcredit_activity_code as activity_code,f072fcredit_department_code as department_code,f072fcredit_fund_code as fund_code, f072freason from t072finvoice_details where f072fid_invoice = $id group by f072fcredit_account_code,f072fcredit_activity_code,f072fcredit_department_code,f072fcredit_fund_code";
            $credit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
             $user_query = "select f014fid_staff from t014fuser where f014fid=".$invoice_result['f071fapproved_by'];
            $user_result = $em->getConnection()->executeQuery($user_query)->fetch();
            $approver = $user_result['f014fid_staff'];
           
            $debit_totalAmount = 0;
            foreach($debit_result as $item)
            {
                $totalAmount = $debit_totalAmount + $item['item_debit'];
            }
           
            $data['f017fdescription'] = "Invoice";
            $data['f017fmodule'] = "Invoice";
            $data['f017fapprover'] = $invoice_result['f071fapproved_by'];
            $data['f017fidFinancialYear'] = $invoice_result['f071fid_financial_year'];
            $data['f017ftotalAmount'] = $totalAmount;
            $data['f017freferenceNumber'] = $invoice_result['f071finvoice_number'];
            $data['f017fdateOfTransaction'] = $invoice_result['f071finvoice_date'];
            $data['f017fapprovedStatus'] = "1";
            $details = array();
            foreach($debit_result as $item){
                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $invoice_result['f071finvoice_number'];
                $detail['f018fdrAmount'] = (float) $item['item_debit'];
                $detail['f018fcrAmount'] = 0;
                $detail['f018fcreated_by'] = $invoice_result['f071fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $invoice_result['f071fcreated_dt_tm'];
                array_push($details,$detail);
             }
             foreach($credit_result as $item){
                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $invoice_result['f071finvoice_number'];
                $detail['f018fdrAmount'] = 0;
                $detail['f018fcrAmount'] = (float) $item['item_credit'];
                $detail['f018fcreated_by'] = $invoice_result['f071fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $invoice_result['f071fcreated_dt_tm'];
                array_push($details,$detail);
            }
            $data['journal-details'] = $details;
            if($status == 1){

            $journalRepository->createNewData($data);
            }
        }
    }
    public function generateUtilityInvoice($postData){

        $em = $this->getEntityManager();

        $utilityId = $postData['utilityId'];
            $year = date('y');
            //Student Invoice Number
            $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='OR';
        $number = $configRepository->generateFIMS($numberData);

            $query  = "select t078futility_bill.f078fcalculated_amount,t078futility_bill.f078fid_utility,f077fcustomer,t078futility.f078fcredit_fund,t078futility.f078fcredit_department,t078futility.f078fcredit_activity,t078futility.f078fcredit_account,t078futility.f078fdebit_fund,t078futility.f078fdebit_department,t078futility.f078fdebit_activity,t078futility.f078fdebit_account,t078futility.f078fid_revenue from t078futility_bill inner join t077frecurring_set_up on f077fpremise = t078futility_bill.f078fid_premise inner join t078futility on t078futility.f078fid = f078fid_utility where t078futility_bill.f078fid = $utilityId ";
            $utility_result = $em->getConnection()->executeQuery($query)->fetch();
// print_r($utility_result);
// die();
            if($utility_result){
                        $invoice_data['f071finvoiceNumber'] = $number;
            $invoice_data['f071fdescription'] = "Utility Fee Setup";
            $invoice_data['f071finvoiceTotal'] = $utility_result['f078fcalculated_amount'];
            $invoice_data['f071finvoiceType'] = "OR";
            $invoice_data['f071fbillType'] = "General Bill";
            $invoice_data['f071finvoiceDate'] = date('Y-m-d');
            $invoice_data['f071fidCustomer'] =  $utility_result['f077fcustomer'];
            $invoice_data['f071fstatus'] = 0;
            $invoice_data['f071fisRcp'] = 1;
            $invoice_data['f071finvoiceFrom'] = 2;

            $invoice_details['f072fidItem'] = $utility_result['f078fid_revenue'];
            $invoice_details['f072fgstValue'] = 0;
            $invoice_details['f072fquantity'] = 1;
            $invoice_details['f072fprice'] = $utility_result['f078fcalculated_amount'];
            $invoice_details['f072ftotal'] = $utility_result['f078fcalculated_amount'];
            $invoice_details['f072fgstCode'] = 0;
            $invoice_details['f072fidTaxCode'] = 0;
            $invoice_details['f072ftotalExc'] = $utility_result['f078fcalculated_amount'];
            $invoice_details['f072ftaxAmount'] = 0;
            $invoice_details['f072fdebitFundCode'] = $utility_result['f078fdebit_fund'];
            $invoice_details['f072fdebitDepartmentCode'] = $utility_result['f078fdebit_department'];
            $invoice_details['f072fdebitActivityCode'] = $utility_result['f078fdebit_activity'];
            $invoice_details['f072fdebitAccountCode'] = $utility_result['f078fdebit_account'];
            $invoice_details['f072fcreditFundCode'] = $utility_result['f078fcredit_fund'];
            $invoice_details['f072fcreditDepartmentCode'] = $utility_result['f078fcredit_department'];
            $invoice_details['f072fcreditActivityCode'] = $utility_result['f078fcredit_activity'];
            $invoice_details['f072fcreditAccountCode'] = $utility_result['f078fcredit_account'];

            $invoice_data['invoice-details'][0] = $invoice_details;
            $this->createNewMasterInvoice($invoice_data);
        
        }
        return;
    }

    public function generateRecurringSetupInvoice($postData){

        $em = $this->getEntityManager();

        $id = $postData['id'];
            $year = date('y');
            //Student Invoice Number
                     $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='OR';
        $number = $configRepository->generateFIMS($numberData);

        
            $query  = "select f077fcustomer,f077fpayment from t077frecurring_set_up where f077fid = $id ";
            $recurring_result = $em->getConnection()->executeQuery($query)->fetch();
// print_r($recurring_result);
// die();
            $invoice_data['f071finvoiceNumber'] = $number;
            $invoice_data['f071fdescription'] = "Recurring Setup";
            $invoice_data['f071finvoiceTotal'] = $recurring_result['f077fpayment'];
            $invoice_data['f071finvoiceType'] = "OR";
            $invoice_data['f071fbillType'] = "Recurring Setup";
            $invoice_data['f071finvoiceDate'] = date('Y-m-d');
            $invoice_data['f071fidCustomer'] =  $recurring_result['f077fcustomer'];
            $invoice_data['f071fstatus'] = 1;
            $invoice_data['f071fisRcp'] = 1;
            $invoice_data['f071finvoiceFrom'] = 2;

            $query  = "select f077ffee_setup,f081fcode,f081fid,f081fpercentage,f077famount,f077fcr_fund,f077fcr_department,f077fcr_activity,f077fcr_account,f077fdr_fund,f077fdr_department,f077fdr_activity,f077fdr_account from t077frecurring_setup_details inner join t085frevenue_set_up on f085fid = f077ffee_setup inner join t081ftext_code on f081fid = f085fid_tax_code  where f077fid_recurring_setup = $id ";
            $details_result = $em->getConnection()->executeQuery($query)->fetchAll();
//             print_r($details_result);
// die();
$final_details = array();
        foreach($details_result as $detail){
            $invoice_details['f072fidItem'] = $detail['f077ffee_setup'];
            $invoice_details['f072fgstValue'] = $detail['f081fpercentage'];
            $invoice_details['f072fquantity'] = 1;
            $invoice_details['f072fprice'] = $detail['f077famount'];
            $invoice_details['f072ftotal'] = $detail['f077famount'];
            $invoice_details['f072fgstCode'] = $detail['f081fid'];
            $invoice_details['f072fidTaxCode'] = $detail['f081fid'];
            $invoice_details['f072ftotalExc'] = $detail['f077famount'];
            $invoice_details['f072ftaxAmount'] = 0;
            $invoice_details['f072fdebitFundCode'] = $detail['f077fdr_fund'];
            $invoice_details['f072fdebitDepartmentCode'] = $detail['f077fdr_department'];
            $invoice_details['f072fdebitActivityCode'] = $detail['f077fdr_activity'];
            $invoice_details['f072fdebitAccountCode'] = $detail['f077fdr_account'];
            $invoice_details['f072fcreditFundCode'] = $detail['f077fcr_fund'];
            $invoice_details['f072fcreditDepartmentCode'] = $detail['f077fcr_department'];
            $invoice_details['f072fcreditActivityCode'] = $detail['f077fcr_activity'];
            $invoice_details['f072fcreditAccountCode'] = $detail['f077fcr_account'];
            array_push($final_details,$invoice_details);
        }
            $invoice_data['invoice-details'] = $final_details;
            // print_r($invoice_data);
            // die();
           $invoice =  $this->createNewMasterInvoice($invoice_data);
           $invoice_id = $invoice->getF071fid();
            $journalRepository = $em->getRepository("Application\Entity\T017fjournal");

           $approve_data['id'][0] = $invoice_id;
            $this->masterInvoiceApprove($approve_data,$journalRepository);

        return;
    }

    public function generateStudentInvoice($data)
    {
 
        $em = $this->getEntityManager();
        
          if(!$data){
            $query  = "select top 100 * from vw_all_students";
            $students = $em->getConnection()->executeQuery($query)->fetchAll();
         }
         else{
          $students = $data['id'];  
         }
        // print_r($students);
        // die(); 
           foreach ($students as $student)
           {
               
        $student_id = $student['f060fid'];
            $year = date('y');
            //Student Invoice Number
                         $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='STD';
        $number = $configRepository->generateFIMS($numberData);
       
          
            $query  = "select  DISTINCT(f048fid_details),f048fid,f048fid_fee_item,f048famount,f048fcr_fund,f048fcr_activity,f048fcr_department,f048fcr_account,f048fdr_fund,f048fdr_activity,f048fdr_department,f048fdr_account from vw_all_students,t048ffee_structure_master  inner join t048ffee_structure_details on f048fid_fee = f048fid where f048fid_intake = f060fid_intake  and f048fcitizen = f060fcitizen and  f048fis_program_attached = 0 and f060fid = $student_id";

            $default_result = $em->getConnection()->executeQuery($query)->fetchAll();
            $query  = "select  DISTINCT(f048fid_details),f048fid,f048fid_programme,f048fid_fee_item,f048famount,f048fcr_fund,f048fcr_activity,f048fcr_department,f048fcr_account,f048fdr_fund,f048fdr_activity,f048fdr_department,f048fdr_account,f066fid_tax_code,f066fcode from vw_all_students, t048ffee_structure_master inner join t048ffee_structure_program on f048fid_fee_program = f048fid inner join t048ffee_structure_details on f048fid_fee = f048fid inner join t066ffee_item on f066fid = f048fid_fee_item  where f048fid_intake = f048fid_intake and f048fcitizen = f060fcitizen and f060fprogram_code = f048fid_programme and f060fid = $student_id";
             // print_r($query);
            $student_result = $em->getConnection()->executeQuery($query)->fetchAll();
      
            if(!$student_result)
            {

            $student_result = $default_result;
            }
      print_r($student_result);
      // die();
        if($student_result){
      
            $feeStructureId = $student_result[0]['f048fid'];
            $studentInvoiceRepository = $em->getRepository("Application\Entity\T060fstudentInvoices");
            $studentInvoice = $studentInvoiceRepository->findBy(array('f060fidStudent' => $student_id,'f060fidFeeStructure' => (int)$feeStructureId));
        if(!$studentInvoice){
                $tmpInvoice = new T060fstudentInvoices();
                $tmpInvoice->setF060fidStudent($student_id)
                           ->setF060fidFeeStructure((int)$feeStructureId);
            try {
                $em->persist($tmpInvoice);
                $em->flush();
            } 
            catch (\Exception $e) {
                echo $e;
            }

            $totalAmount = 0;
            foreach($student_result as $row){
                $totalAmount = $totalAmount + $row['f048famount'];
            }
            $invoice_data['f071finvoiceNumber'] = $number;
            $invoice_data['f071fdescription'] = "Student invoice";
            $invoice_data['f071finvoiceTotal'] = $totalAmount;
            $invoice_data['f071finvoiceType'] = "STD";
            $invoice_data['f071fbillType'] = "Student";
            $invoice_data['f071finvoiceDate'] = date('Y-m-d');
            $invoice_data['f071fidCustomer'] =  $student_id;
            $invoice_data['f071fstatus'] = 1;
            $invoice_data['f071fisRcp'] = 1;
            $invoice_data['f071fisPaid'] = 0;
            $invoice_data['f071finvoiceFrom'] = 2;

            $final_details = array();
            foreach($student_result as $detail){
                $invoice_details['f072fidItem'] = $detail['f048fid_fee_item'];
                $invoice_details['f072fgstValue'] = $detail['f066fgst_value'];
                $invoice_details['f072fquantity'] = 1;
                $invoice_details['f072fprice'] = $detail['f048famount'];
                $invoice_details['f072ftotal'] = $detail['f048famount'];
                $invoice_details['f072fgstCode'] = $detail['f066fid_tax_code'];
                $invoice_details['f072fidTaxCode'] = $detail['f066fid_tax_code'];
                $invoice_details['f072ftotalExc'] = $detail['f048famount'];
                $invoice_details['f072ftaxAmount'] = 0;
                $invoice_details['f072fdebitFundCode'] = $detail['f048fdr_fund'];
                $invoice_details['f072fdebitDepartmentCode'] = $detail['f048fdr_department'];
                $invoice_details['f072fdebitActivityCode'] = $detail['f048fdr_activity'];
                $invoice_details['f072fdebitAccountCode'] = $detail['f048fdr_account'];
                $invoice_details['f072fcreditFundCode'] = $detail['f048fcr_fund'];
                $invoice_details['f072fcreditDepartmentCode'] = $detail['f048fcr_department'];
                $invoice_details['f072fcreditActivityCode'] = $detail['f048fcr_activity'];
                $invoice_details['f072fcreditAccountCode'] = $detail['f048fcr_account'];
                array_push($final_details,$invoice_details);
            }
            $invoice_data['invoice-details'] = $final_details;
            // print_r($invoice_data);
            // die();
           $invoice =  $this->createNewMasterInvoice($invoice_data);
          }
        }
    }
    die();


        return;
    }

    public function generateStudentInvoice1($postData){

        $em = $this->getEntityManager();

        $student_id = $postData['studentId'];
            $year = date('y');
            //Student Invoice Number
                           $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='STD';
        $number = $configRepository->generateFIMS($numberData);
          
            $query  = "select  DISTINCT(f048fid_details),f081fid,f048fid_fee_item,f048fid_currency,f048famount,f048fcr_fund,f048fcr_activity,f048fcr_department,f048fcr_account,f048fdr_fund,f048fdr_activity,f048fdr_department,f048fdr_account,f081fcode,f081fpercentage	 from t060fstudent,t048ffee_structure_master  inner join t048ffee_structure_details on f048fid_fee = f048fid inner join t081ftext_code on f081fid = f048fid_fee_item  where f048fid_intake = f060fid_intake and f048fcitizen = f060fcitizen and f048fregion = f060fregion and f048fstudent_category = f060fstudent_category and f048fstudy_mode = f060fstudy_mode  and f048fis_program_attached = 0 and f060fid = $student_id";
            $default_result = $em->getConnection()->executeQuery($query)->fetchAll();
            
            $query  = "select  DISTINCT(f048fid_details),f048fid,f048fid_programme,f048fid_fee_item,f048fid_currency,f048famount,f048fcr_fund,f048fcr_activity,f048fcr_department,f048fcr_account,f048fdr_fund,f048fdr_activity,f048fdr_department,f048fdr_account,f066fid_tax_code,f066fcode,f066fgst_value	 from t060fstudent,t048ffee_structure_master inner join t048ffee_structure_program on f048fid_fee_program = f048fid inner join t048ffee_structure_details on f048fid_fee = f048fid inner join t066ffee_item on f066fid = f048fid_fee_item  where f048fid_intake = f060fid_intake and f048fcitizen = f060fcitizen and f048fregion = f060fregion and f048fstudent_category = f060fstudent_category and f048fstudy_mode = f060fstudy_mode and f060fid_program = f048fid_programme and f060fid = $student_id";
            $student_result = $em->getConnection()->executeQuery($query)->fetchAll();
      
            if(!$student_result){
            $student_result = $default_result;
        }
        // print_r($student_result);
        // die();
            $totalAmount = 0;
            foreach($student_result as $row){
                $totalAmount = $totalAmount + $row['f048famount'];
            }
            $invoice_data['f071finvoiceNumber'] = $number;
            $invoice_data['f071fdescription'] = "Student invoice";
            $invoice_data['f071finvoiceTotal'] = $totalAmount;
            $invoice_data['f071finvoiceType'] = "STD";
            $invoice_data['f071fbillType'] = "Student";
            $invoice_data['f071finvoiceDate'] = date('Y-m-d');
            $invoice_data['f071fidCustomer'] =  $student_id;
            $invoice_data['f071fstatus'] = 1;
            $invoice_data['f071fisRcp'] = 1;
            $invoice_data['f071finvoiceFrom'] = 2;

$final_details = array();
        foreach($student_result as $detail){
            $currency = $detail['f048fid_currency'];
            $query  = "select  c.f062fcur_code,cr.f064fexchange_rate,cr.f064feffective_date,c.f062fid from t062fcurrency as c inner join t064fcurrency_rate_setup as cr on cr.f064fid_currency = c.f062fid  where c.f062fid = $currency order by cr.f064feffective_date DESC";
            $exchange_result = $em->getConnection()->executeQuery($query)->fetch();
            
           if($exchange_result["f062fcur_code"] == "USD"){
               $exchange_rate = $exchange_result['f064fexchange_rate'];
           }
           else{
               $exchange_rate = 1;
           }
            $invoice_details['f072fidItem'] = $detail['f048fid_fee_item'];
            $invoice_details['f072fgstValue'] = $detail['f066fgst_value'];
            $invoice_details['f072fquantity'] = 1;
            $invoice_details['f072fprice'] = $detail['f048famount']*$exchange_rate;
            $invoice_details['f072ftotal'] = $detail['f048famount']*$exchange_rate;
            $invoice_details['f072fgstCode'] = $detail['f066fid_tax_code'];
            $invoice_details['f072fidTaxCode'] = $detail['f066fid_tax_code'];
            $invoice_details['f072ftotalExc'] = $detail['f048famount']*$exchange_rate;
            $invoice_details['f072ftaxAmount'] = 0;
            $invoice_details['f072fdebitFundCode'] = $detail['f048fdr_fund'];
            $invoice_details['f072fdebitDepartmentCode'] = $detail['f048fdr_department'];
            $invoice_details['f072fdebitActivityCode'] = $detail['f048fdr_activity'];
            $invoice_details['f072fdebitAccountCode'] = $detail['f048fdr_account'];
            $invoice_details['f072fcreditFundCode'] = $detail['f048fcr_fund'];
            $invoice_details['f072fcreditDepartmentCode'] = $detail['f048fcr_department'];
            $invoice_details['f072fcreditActivityCode'] = $detail['f048fcr_activity'];
            $invoice_details['f072fcreditAccountCode'] = $detail['f048fcr_account'];
            array_push($final_details,$invoice_details);
        }
            $invoice_data['invoice-details'] = $final_details;
            // print_r($invoice_data);
            // die();
           $invoice =  $this->createNewMasterInvoice($invoice_data);

        return;
    }

     public function paidInvoice($postData)
    {
        $em = $this->getEntityManager();
         
        foreach ($postData['id'] as $id) 
        {

            $val=(int)1;
            $query1 = "UPDATE t071finvoice set f071fis_paid = $val where f071fid = $id";
            $em->getConnection()->executeQuery($query1);
           
        }
    }


    public function deleteMasterInvoiceDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t072finvoice_details WHERE f072fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

   
   public function getMasterInvoiceListMaster()
   {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071fbillType,s.f060fname as studentName,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom, i.f071fcrAmount, i.f071fdrAmount, i.f071ftotalPaid, i.f071fbalance, i.f071freason,i.f071fstatus,i.f071ftype,i.f071fidRequestInvoice, i.f071fdiscount')
            ->from('Application\Entity\T071finvoice', 'i')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fprocessed = 0')
            ->orderby('i.f071fid','DESC');
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $invoice)
        {
            // print_r($invoice['f071fidCustomer']);exit;
            $idInvoice = (int)$invoice['f071fid'];
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));

           $invoiceType = $invoice['f071finvoiceType'];
           $idCustomer = (int)$invoice['f071fidCustomer'];


           switch ($invoiceType)
           {
                case 'STA':
                    $queryCustomer = "select f034fname as name from vw_staff_master where f034fstaff_id = $idCustomer ";
                   break;
               
                case 'STD':
                   $queryCustomer = "select f060fname as name from vw_all_students where f060fid = $idCustomer ";
                   break;

                case 'OR':
                   $queryCustomer = "select f021ffirst_name as name from t021fcustomers where f021fid = $idCustomer ";
                   break;
           }

            $resultCustomer = $em->getConnection()->executeQuery($queryCustomer)->fetch();
           $invoice['staffname'] = $resultCustomer['name'];



            $queryDetail = "select SUM(f072fbalance_amount) as f072fbalance_amount from t072finvoice_details where f072fid_invoice = $idInvoice ";

            $resultDetail = $em->getConnection()->executeQuery($queryDetail)->fetch();
            // print_r($resultDetail);exit;

            $invoice['f072fbalanceAmount'] = $resultDetail['f072fbalance_amount'];

            array_push($result1, $invoice);
        }

        // print_r($result1);exit;

        $result = array(
            'data' => $result1,
        );

        return $result;
   }

}
