<?php
namespace Application\Repository;

use Application\Entity\T037fpremiseTypeSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PremiseTypeSetupRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('p.f037fid, p.f037fpremiseType,p.f037fcode, p.f037fprefixCode, p.f037fstatus')
            ->from('Application\Entity\T037fpremiseTypeSetup','p')
            // ->leftjoin('Application\Entity\T076fpremise','ps','with', 'p.f037fcode = ps.f076fid')
            ->orderBy('p.f037fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('p.f037fid, p.f037fpremiseType,p.f037fcode, p.f037fprefixCode, p.f037fstatus')
            ->from('Application\Entity\T037fpremiseTypeSetup','p')
            ->where('p.f037fid = :premiseId')
            ->setParameter('premiseId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $premise = new T037fpremiseTypeSetup();

        $premise->setF037fpremiseType($data['f037fpremiseType'])
             ->setF037fcode($data['f037fcode'])
             ->setF037fprefixCode($data['f037fprefixCode'])
             ->setF037fstatus($data['f037fstatus'])
             ->setF037fcreatedBy((int)$_SESSION['userId']);

        $premise->setF037fcreatedDtTm(new \DateTime());
               

        $em->persist($premise);
        
        $em->flush();


        return $premise;

    }

    /* to edit the data in database*/

    public function updateData($premise, $data = []) 
    {
        $em = $this->getEntityManager();

        $premise->setF037fpremiseType($data['f037fpremiseType'])
                ->setF037fcode($data['f037fcode'])
                ->setF037fprefixCode($data['f037fprefixCode'])
                ->setF037fstatus($data['f037fstatus'])
                ->setF037fupdatedBy((int)$_SESSION['userId']);

        $premise->setF037fupdatedDtTm(new \DateTime());

        
        $em->persist($premise);
        
        $em->flush();
 

    }
        public function getActiveList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select("p.f037fid,(p.f037fcode+'-'+p.f037fpremiseType) as PremiseType , p.f037fcode, p.f037fprefixCode, p.f037fstatus")
            ->from('Application\Entity\T037fpremiseTypeSetup','p')
            ->orderBy('p.f037fid','DESC')
            ->where('p.f037fstatus = 1');


        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    }


    
}

?>