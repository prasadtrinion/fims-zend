<?php
namespace Application\Repository;

use Application\Entity\T020floanName;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LoanNameRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();
 
        $qb = $em->createQueryBuilder();


        // Query
        $qb->select('l.f020fid, l.f020floanName, l.f020ftypeOfLoan, l.f020fstatus, l.f020fcreatedBy, l.f020fupdatedBy, l.f020fpercentage, l.f020ffund, l.f020fpaytype, l.f020factivity, l.f020faccount, l.f020fdepartment,d.f011fdefinitionCode as LoanType')
        ->from('Application\Entity\T020floanName','l')
        ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','l.f020ftypeOfLoan = d.f011fid')
        ->orderBy('l.f020fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );
        // $query = "SELECT f020fid FROM t020floan_name";
        // $result =$em->getConnection()->executeQuery($query);

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f020fid, l.f020floanName, l.f020ftypeOfLoan, l.f020fstatus, l.f020fcreatedBy, l.f020fupdatedBy, l.f020fpercentage, l.f020ffund, l.f020fpaytype, l.f020factivity, l.f020faccount, l.f020fdepartment')
        ->from('Application\Entity\T020floanName','l')
        ->where('l.f020fid = :loanId')
        ->setParameter('loanId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }



    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $loan = new T020floanName();

        $loan->setF020floanName($data['f020floanName'])
        ->setF020ftypeOfLoan((int)$data['f020ftypeOfLoan'])
        ->setF020fpercentage((float)$data['f020fpercentage'])
        ->setF020ffund($data['f020ffund'])
        ->setF020fdepartment($data['f020fdepartment'])
        ->setF020fpaytype($data['f020fpaytype'])
        ->setF020factivity($data['f020factivity'])
        ->setF020faccount($data['f020faccount'])
        ->setF020fstatus((int)$data['f020fstatus'])
        ->setF020fcreatedBy((int)$_SESSION['userId'])
        ->setF020fupdatedBy((int)$_SESSION['userId']);

        $loan->setF020fcreatedDtTm(new \DateTime())
        ->setF020fupdatedDtTm(new \DateTime());


        $em->persist($loan);
        $em->flush();
        

        return $loan;

    }

    /* to edit the data in database*/

    public function updateData($loan, $data = []) 
    {
        $em = $this->getEntityManager();

        $loan->setF020floanName($data['f020floanName'])
        ->setF020ftypeOfLoan((int)$data['f020ftypeOfLoan'])
        ->setF020fpercentage((float)$data['f020fpercentage'])
        ->setF020ffund($data['f020ffund'])
        ->setF020fdepartment($data['f020fdepartment'])
        ->setF020fpaytype($data['f020fpaytype'])
        ->setF020factivity($data['f020factivity'])
        ->setF020faccount($data['f020faccount'])
        ->setF020fstatus((int)$data['f020fstatus'])
        ->setF020fcreatedBy((int)$_SESSION['userId'])
        ->setF020fupdatedBy((int)$_SESSION['userId']);

        $loan->setF020fupdatedDtTm(new \DateTime());

        $em->persist($loan);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f020fid, l.f020floanName,l.f020ftypeOfLoan as f020ftypeOfLoanId, l.f020fstatus, l.f020fcreatedBy, l.f020fupdatedBy, l.f020fpercentage, l.f020ffund, l.f020fpaytype, l.f020factivity, l.f020faccount, l.f020fdepartment,d.f011fdefinitionCode as f020ftypeOfLoan')
        ->from('Application\Entity\T020floanName','l')
        ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','l.f020ftypeOfLoan = d.f011fid')
        ->where('l.f020fstatus=1')
        ->orderBy('l.f020floanName','ASC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    
}

?>
