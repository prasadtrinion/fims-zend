<?php
namespace Application\Repository;

use Application\Entity\T013fcompany;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CompanyRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f013fid, c.f013fname, c.f013faddress, c.f013femail, c.f013fphoneNumber, c.f013fcity,s.f012fid as f013fstateCode, s.f012fstateName as f013fstateName,d.f013fid as f013fcountryCode ,d.f013fcountryName as f013countryName, c.f013ftax, c.f013ftaxType, c.f013ftaxExcempted, c.f013fregistrationNumber, c.f013fcode, c.f013frefNumber, c.f013ffaxNumber, c.f013fpostCode, c.f013fstatus')
            ->from('Application\Entity\T013fcompany', 'c')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'c.f013fcountryCode = d.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'c.f013fstateCode = s.f012fid')
            ->orderBy('c.f013fid','DESC');    
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }
	public function getListById($id)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('c.f013fid, c.f013fname, c.f013faddress, c.f013femail, c.f013fphoneNumber, c.f013fcity,s.f012fid as f013fstateCode, s.f012fstateName as f013fstateName,d.f013fid as f013fcountryCode ,d.f013fcountryName as f013countryName, c.f013ftax, c.f013ftaxType, c.f013ftaxExcempted, c.f013fregistrationNumber, c.f013fcode, c.f013frefNumber, c.f013ffaxNumber, c.f013fpostCode')
            ->from('Application\Entity\T013fcompany', 'c')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'c.f013fcountryCode = d.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'c.f013fstateCode = s.f012fid')
			->where('c.f013fid =:companyId')
			->setParameter('companyId', $id);

		$query = $qb->getQuery();
		$result = $query->getResult();

		return $result;
	}

	public function createNewData($data) 
	{
		
		$em = $this->getEntityManager();

		$company = new T013fcompany();

		$company->setF013fname($data['f013fname'])
			    ->setF013femail($data['f013femail'])
			    ->setF013fphoneNumber($data['f013fphoneNumber'])
			    ->setF013faddress($data['f013faddress'])
			    ->setF013fcity($data['f013fcity'])
			    ->setF013fstateCode((int)$data['f013fstateCode'])
			    ->setF013fcountryCode((int)$data['f013fcountryCode'])
			    ->setF013ftax((int)$data['f013ftax'])
			    ->setF013ftaxType((int)$data['f013ftaxType'])
			    ->setF013ftaxExcempted((int)$data['f013ftaxExcempted'])
			    ->setF013fregistrationNumber($data['f013fregistrationNumber'])
			    ->setF013fcode($data['f013fcode'])
			    ->setF013frefNumber($data['f013frefNumber'])
			    ->setF013ffaxNumber($data['f013ffaxNumber'])
			    ->setF013fpostCode($data['f013fpostCode'])
			    ->setF013fstatus((int)$data['f013fstatus'])
			    ->setF013fcreatedBy((int)$_SESSION['userId'])
			    ->setF013fupdatedBy((int)$_SESSION['userId']);

		$company->setF013fcreateDtTm(new \DateTime())
    			->setF013fupdateDtTm(new \DateTime());


		try{
		$em->persist($company);
		$em->flush();

		}
		catch(\Exception $e){
			echo $e;
		}
		return $company;
	}

	public function updateData($company, $data = []) 
	{
        $em = $this->getEntityManager();
        
       $company->setF013fname($data['f013fname'])
			    ->setF013femail($data['f013femail'])
			    ->setF013fphoneNumber($data['f013fphoneNumber'])
			    ->setF013faddress($data['f013faddress'])
			    ->setF013fcity($data['f013fcity'])
			    ->setF013fstateCode((int)$data['f013fstateCode'])
			    ->setF013fcountryCode((int)$data['f013fcountryCode'])
			    ->setF013fstatus((int)$data['f013fstatus'])
			    ->setF013ftax((int)$data['f013ftax'])
			    ->setF013ftaxType((int)$data['f013ftaxType'])
			    ->setF013ftaxExcempted((int)$data['f013ftaxExcempted'])
			    ->setF013fregistrationNumber($data['f013fregistrationNumber'])
			    ->setF013fcode($data['f013fcode'])
			    ->setF013frefNumber($data['f013frefNumber'])
			    ->setF013ffaxNumber($data['f013ffaxNumber'])
			    ->setF013fpostCode($data['f013fpostCode'])
			    ->setF013fcreatedBy((int)$_SESSION['userId'])
			    ->setF013fupdatedBy((int)$_SESSION['userId']);
			    

		$company->setF013fUpdateDtTm(new \DateTime());
       
        $em->persist($company);
        $em->flush();
    }


    public function getTaxType() 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('c.f013ftaxType')
            ->from('Application\Entity\T013fcompany', 'c');

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    public function update($entity) 
    {
		$em = $this->getEntityManager();
		$em->persist($entity);
		$em->flush();
	}

	public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f013fid, c.f013fname, c.f013faddress, c.f013femail, c.f013fphoneNumber, c.f013fcity,s.f012fid as f013fstateCode, s.f012fstateName as f013fstateName,d.f013fid as f013fcountryCode ,d.f013fcountryName as f013countryName, c.f013ftax, c.f013ftaxType,  c.f013ftaxExcempted, c.f013fregistrationNumber, c.f013fcode, c.f013frefNumber')
            ->from('Application\Entity\T013fcompany', 'c')
            ->leftjoin('Application\Entity\T013fcountry','d','with', 'c.f013fcountryCode = d.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'c.f013fstateCode = s.f012fid')
            ->where('c.f013fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
}
?>
