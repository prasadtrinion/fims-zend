<?php
namespace Application\Repository;

use Application\Entity\T050fstudentTagging;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StudentTaggingRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("t.f050fid, t.f050fidStudent, t.f050fdueDate, t.f050fpolicyNo, t.f050fidInsurance, t.f050fstatus, t.f050ftaggingStatus, i.f099fname as insurance,  (s.f060ficNumber+'-'+s.f060fname) as Student, d.f011fdefinitionCode as TaggingStatus")
            ->from('Application\Entity\T050fstudentTagging', 't')
            ->leftjoin('Application\Entity\T060fstudent','s','with', 't.f050fidStudent = s.f060fid')
            ->leftjoin('Application\Entity\T099fstudentInsurance','i','with', 't.f050fidInsurance = i.f099fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','t.f050ftaggingStatus = d.f011fid')
            ->orderBy('t.f050fid','DESC');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $studentTagging)
        {
            $date                        = $studentTagging['f050fdueDate'];
            $studentTagging['f050fdueDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $studentTagging);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('t.f050fid, t.f050fidStudent, t.f050fdueDate, t.f050fpolicyNo, t.f050fidInsurance, t.f050fstatus, t.f050ftaggingStatus, i.f099fname as insurance,  s.f060fname as Student, d.f011fdefinitionCode as TaggingStatus')
            ->from('Application\Entity\T050fstudentTagging', 't')
            ->leftjoin('Application\Entity\T060fstudent','s','with', 't.f050fidStudent = s.f060fid')
            ->leftjoin('Application\Entity\T099fstudentInsurance','i','with', 't.f050fidInsurance = i.f099fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','t.f050ftaggingStatus = d.f011fid')
            ->where('t.f050fid = :studentTaggingId')
            ->setParameter('studentTaggingId',$id);
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $studentTagging)
        {
            $date                        = $studentTagging['f050fdueDate'];
            $studentTagging['f050fdueDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $studentTagging);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;

    }

    public function createStudentTagging($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $studentTagging = new T050fstudentTagging();

        $studentTagging->setF050fidStudent((int)$data['f050fidStudent'])
                ->setF050fdueDate(new \DateTime($data['f050fdueDate']))
                ->setF050fpolicyNo($data['f050fpolicyNo'])
                ->setF050fidInsurance($data['f050fidInsurance'])
                ->setF050ftaggingStatus((int)$data['f050ftaggingStatus'])
                ->setF050fstatus((int)$data['f050fstatus'])
                ->setF050fcreatedBy((int)$_SESSION['userId'])
                ->setF050fupdatedBy((int)$_SESSION['userId']);

        $studentTagging->setF050fcreatedDtTm(new \DateTime())
                ->setF050fupdatedDtTm(new \DateTime());


        try{
            $em->persist($studentTagging);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $studentTagging;
    }

    public function updateStudentTagging($studentTagging, $data = []) 
    {
        $em = $this->getEntityManager();

        $studentTagging->setF050fidStudent((int)$data['f050fidStudent'])
                ->setF050fdueDate(new \DateTime($data['f050fdueDate']))
                ->setF050fpolicyNo($data['f050fpolicyNo'])
                ->setF050fidInsurance($data['f050fidInsurance'])
                ->setF050ftaggingStatus((int)$data['f050ftaggingStatus'])
                ->setF050fstatus((int)$data['f050fstatus'])
                ->setF050fupdatedBy((int)$_SESSION['userId']);

        $studentTagging->setF050fupdatedDtTm(new \DateTime());


        
        $em->persist($studentTagging);
        $em->flush();

    }

   

}
