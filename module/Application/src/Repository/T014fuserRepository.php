<?php
namespace Application\Repository;

use Application\Entity\T014fuser;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * T014fuserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class T014fuserRepository extends EntityRepository
{
 
 	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query = "select u.f014fid,u.f014fuser_name as f014fuserName,u.f014fpassword,u.f014femail,u.f014fid_staff as f014fidStaff, u.f014fstatus,s.f034fname,(cast(s.f034fstaff_id as varchar(30))+'-'+s.f034fname) as originalName,s.f034fgrade_id from t014fuser as u inner join vw_staff_master as s on u.f014fid_staff = s.f034fstaff_id order by u.f014fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        // $qb->select('u.f014fid,u.f014fuserName,u.f014fpassword,u.f014femail,u.f014fidRole,u.f014fidStaff, u.f014fstatus,s.f034fname')
        //     ->from('Application\Entity\T014fuser','u')
        //     ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'u.f014fidStaff = s.f0234fstaff_id')
        //     ->orderby('u.f014fid','DESC');

        // $query = $qb->getQuery();

        $result = array(
            'data' => $result
        );

        return $result;
        // cast(u.f014fid_staff as varchar(20))
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u.f014fid, u.f014fuserName,u.f014fpassword,u.f014femail,u.f014femail,u.f014fidStaff, u.f014fstatus')
            ->from('Application\Entity\T014fuser','u')
             ->where('u.f014fid = :id')
            ->setParameter('id',(int)$id);
// echo $qb;
// die();
        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();
        for($i=0;$i<count($result);$i++){
        $qb = $em->createQueryBuilder();

            $qb->select('ur.f023fidRole as f014fidRole,r.f021froleName ')
            ->from('Application\Entity\T023fuserRoles','ur')
            ->leftjoin('Application\Entity\T021frole', 'r', 'with', 'ur.f023fidRole = r.f021fid')
             ->where('ur.f023fidUser = :id')
            ->setParameter('id',(int)$id);
// echo $qb;
// die();
        $query = $qb->getQuery();    
        $result1 = $query->getResult();
        $result['roles'] = $result1;
        }

        return $result;
    }

    public function createNewData($data) 
    {
        
        $em = $this->getEntityManager();

        $user = new T014fuser();

        $user->setF014fuserName($data['f014fuserName'])
            ->setF014femail($data['f014femail'])
            ->setF014fpassword(md5($data['f014fpassword']))
            ->setF014ftoken($data['f014ftoken'])
            ->setF014fidRole((int)$data['f014fidRole'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fcreatedBy((int)$_SESSION['userId'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $user->setF014fcreatedDtTm(new \DateTime())
               ->setF014fupdatedDtTm(new \DateTime());
        try{
        $em->persist($user);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $user;
    }
    public function updateData($user,$data = []) 
    {
        $em = $this->getEntityManager();
        
        $user->setF014fuserName($data['f014fuserName'])
            ->setF014femail($data['f014femail'])
            // ->setF014fpassword(md5("12345"))
            ->setF014fidRole((int)$data['f014fidRole'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fcreatedBy((int)$_SESSION['userId'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $user->setF014fcreatedDtTm(new \DateTime())
               ->setF014fupdatedDtTm(new \DateTime());
        try{
        $em->persist($user);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $user;
    }

    public function getApproveUser() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        // $qb->select('u.f014fid,u.f014fuserName,u.f014fpassword,u.f014femail,u.f014fidRole, u.f014fstatus')
        //     ->from('Application\Entity\T014fuser','u')
        //     ->where('u.f014fstatus=0')
        //     ->orderby('u.f014fid','DESC');

        // $query = $qb->getQuery();

        // $result = $query->getArrayResult();

        $query = "select u.f014fid,u.f014fuser_name as f014fuserName,u.f014fpassword,u.f014femail,u.f014fid_staff as f014fidStaff, u.f014fstatus,s.f034fname from t014fuser as u inner join vw_staff_master as s on u.f014fid_staff = s.f034fstaff_id where u.f014fstatus=0 order by u.f014fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        for($i=0;$i<count($result);$i++){
             $qb = $em->createQueryBuilder();
            $qb->select('r.f021froleName')
            ->from('Application\Entity\T023fuserRoles','ur')
            ->leftjoin('Application\Entity\T021frole', 'r', 'with', 'ur.f023fidRole = r.f021fid')
            ->where('ur.f023fidUser = :id')
            ->setParameter('id',(int)$result[$i]['f014fid']);
        $query = $qb->getQuery();

        $result1 = $query->getResult();
        $tmp = array();
        foreach($result1 as $row){
            array_push($tmp,$row['f021froleName']);
        }
        $role = implode(',',$tmp);
        $result[$i]['role'] = $tmp;
        }
        $result = array(
            'data' => $result,
        );
        
        return $result;
        
    }
    public function approveUser($data) 
    {
        $em = $this->getEntityManager();
        $idUser = $data['userId'];
        $status = $data['status'];
        try{
        $update_query = "update t014fuser set f014fstatus = $status where f014fid = $idUser";
        $em->getConnection()->executeQuery($update_query);
    }
    catch(\Exception $e){
        echo $e;
    }
        return;
        
    }

    public function getRejectUser() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        // $qb->select('u.f014fid,u.f014fuserName,u.f014fpassword,u.f014femail,u.f014fidRole, u.f014fstatus')
        //     ->from('Application\Entity\T014fuser','u')
        //     ->where('u.f014fstatus=1')
        //     ->orderby('u.f014fid','DESC');

        // $query = $qb->getQuery();

        // $result = $query->getArrayResult();

        $query = "select u.f014fid,u.f014fuser_name as f014fuserName,u.f014fpassword,u.f014femail,u.f014fid_staff as f014fidStaff, u.f014fstatus,s.f034fname from t014fuser as u inner join t034fstaff_master as s on cast(u.f014fid_staff as varchar(20)) = s.f034fstaff_id  where u.f014fstatus=1 or u.f014fstatus=0 order by u.f014fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        for($i=0;$i<count($result);$i++){
             $qb = $em->createQueryBuilder();
            $qb->select('r.f021froleName')
            ->from('Application\Entity\T023fuserRoles','ur')
            ->leftjoin('Application\Entity\T021frole', 'r', 'with', 'ur.f023fidRole = r.f021fid')
            ->where('ur.f023fidUser = :id')
            ->setParameter('id',(int)$result[$i]['f014fid']);
        $query = $qb->getQuery();

        $result1 = $query->getResult();
        $tmp = array();
        foreach($result1 as $row){
            array_push($tmp,$row['f021froleName']);
        }
        $role = implode(',',$tmp);
        $result[$i]['role'] = $tmp;
        }
        $result = array(
            'data' => $result,
        );
        
        return $result;
        
    }

    public function updateApproval($users) 
    {
        $em = $this->getEntityManager();

        foreach ($users as $user) 
        {

        $user->setF014fstatus((int)"1")
                 ->setF014fupdatedDtTm(new \DateTime());
       
        $em->persist($user);
        $em->flush();
        }
    }

    public function updateRejection($users) 
    {
        $em = $this->getEntityManager();

        foreach ($users as $user) 
        {

        $user->setF014fstatus((int)"2")
                 ->setF014fupdatedDtTm(new \DateTime());
       
        $em->persist($user);
        $em->flush();
        }
    }

}	