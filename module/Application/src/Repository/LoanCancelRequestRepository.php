<?php
namespace Application\Repository;

use Application\Entity\T070floanCancelRequest;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class LoanCancelRequestRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('l.f070fid, l.f070fname, l.f070fcancelReason, l.f070fidStaff, s.f034fname as staff, l.f070fstatus')
            ->from('Application\Entity\T070floanCancelRequest', 'l')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f070fidStaff = s.f034fstaffId')
            ->orderBy('l.f070fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f070fid, l.f070fname, l.f070fcancelReason, l.f070fidStaff, s.f034fname as staff, l.f070fstatus')
            ->from('Application\Entity\T070floanCancelRequest', 'l')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f070fidStaff = s.f034fstaffId')
            ->where('l.f070fid = :loanId')
            ->setParameter('loanId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $loanCancel = new T070floanCancelRequest();

        $loanCancel->setF070fname($data['f070fname'])
                ->setF070fcancelReason($data['f070fcancelReason'])
                ->setF070fidStaff((int)$data['f070fidStaff'])
                ->setF070fstatus((int)$data['f070fstatus'])
                ->setF070fapprovalStatus((int)"0")
                ->setF070fcreatedBy((int)$_SESSION['userId'])
                ->setF070fupdatedBy((int)$_SESSION['userId']);

        $loanCancel->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());


        try
        {
            $em->persist($loanCancel);
            $em->flush();
            // print_r($loanCancel);
            // die();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $loanCancel;
    }

    public function updateData($loanCancel, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $loanCancel->setF070fname($data['f070fname'])
                ->setF070fcancelReason($data['f070fcancelReason'])
                ->setF070fidStaff((int)$data['f070fidStaff'])
                ->setF070fstatus((int)$data['f070fstatus'])
                ->setF070fcreatedBy((int)$_SESSION['userId'])
                ->setF070fupdatedBy((int)$_SESSION['userId']);

        $loanCancel->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());



        
        $em->persist($loanCancel);
        $em->flush();

    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
               $qb->select('l.f070fid, l.f070fname, l.f070fcancelReason, l.f070fidStaff, s.f034fname as staff, l.f070fstatus, l.f070fapprovalStatus')
            ->from('Application\Entity\T070floanCancelRequest', 'l')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f070fidStaff = s.f034fstaffId')
                 ->where('l.f070fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('l.f070fid, l.f070fname, l.f070fcancelReason, l.f070fidStaff, s.f034fname as staff, l.f070fstatus, l.f070fapprovalStatus')
            ->from('Application\Entity\T070floanCancelRequest', 'l')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f070fidStaff = s.f034fstaffId')
                 ->where('l.f070fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
               $qb->select('l.f070fid, l.f070fname, l.f070fcancelReason, l.f070fidStaff, s.f034fname as staff, l.f070fstatus, l.f070fapprovalStatus')
            ->from('Application\Entity\T070floanCancelRequest', 'l')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f070fidStaff = s.f034fstaffId');

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function loanCancelApproval($loans) 
    {
        $em = $this->getEntityManager();

        foreach ($loans as $loan) 
        {

            $loan->setF070fapprovalStatus((int)"1")
                 ->setF070fUpdatedDtTm(new \DateTime());
       
            $em->persist($loan);
            $em->flush();
        }
    }

    
}