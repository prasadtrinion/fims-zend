<?php
namespace Application\Repository;

use Application\Entity\T030epfCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class EpfCategoryRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select('e.f030fid, e.f030fcategoryName, e.f030fdiscription,  e.f030fstatus')
            ->from('Application\Entity\T030epfCategory', 'e')
             ->orderBy('e.f030fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('e.f030fid, e.f030fcategoryName, e.f030fdiscription,  e.f030fstatus')
            ->from('Application\Entity\T030epfCategory', 'e')
             ->orderBy('e.f030fid','DESC')
            ->where('e.f030fid = :epfId')
            ->setParameter('epfId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $epfCategory = new T030epfCategory();

        $epfCategory->setF030fcategoryName($data['f030fcategoryName'])
                ->setF030fdiscription($data['f030fdiscription'])
                ->setF030fstatus((int)$data['f030fstatus'])
                ->setF030fcreatedBy((int)$_SESSION['userId'])
                ->setF030fupdatedBy((int)$_SESSION['userId']);

        $epfCategory->setF030fcreatedDtTm(new \DateTime())
               ->setF030fupdatedDtTm(new \DateTime());
          
        $em->persist($epfCategory);
        
        $em->flush();
        
        return $epfCategory;
    }

    public function updateData($epfCategory, $data = []) 
    {
        $em = $this->getEntityManager();
            $epfCategory->setF030fcategoryName($data['f030fcategoryName'])
                ->setF030fdiscription($data['f030fdiscription'])
                ->setF030fstatus((int)$data['f030fstatus'])
                ->setF030fcreatedBy((int)$_SESSION['userId'])
                ->setF030fupdatedBy((int)$_SESSION['userId']);

        $epfCategory->setF030fcreatedDtTm(new \DateTime())
               ->setF030fupdatedDtTm(new \DateTime());
        
        $em->persist($epfCategory);
        $em->flush();
    }  
     public function getActiveList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select('e.f030fid, e.f030fcategoryName, e.f030fdiscription,  e.f030fstatus')
            ->from('Application\Entity\T030epfCategory', 'e')
            ->where("e.f030fstatus = 1")
             ->orderBy('e.f030fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }
}