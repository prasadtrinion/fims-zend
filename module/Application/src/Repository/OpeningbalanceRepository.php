<?php
namespace Application\Repository;

use Application\Entity\T016fopeningbalance;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class OpeningbalanceRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select("o.f016fid,o.f016fidFinancialyear, o.f016ffund,(fu.f057fcode+' - ' +fu.f057fname) as  fundName, o.f016factivityCode,(a.f058fcompleteCode+' - ' +a.f058fname) as  activityName, o.f016fdepartment, 
          (d.f015fdepartmentCode +' - '+ d.f015fdepartmentName) as  departmentName, o.f016faccountCode,(ac.f059fcompleteCode +' - '+ ac.f059fname) as  accountName, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceDrAmount, o.f016fbalanceCrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus") 
            ->from('Application\Entity\T016fopeningbalance', 'o')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
            ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','o.f016ffund = fu.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with','o.f016factivityCode = a.f058fcompleteCode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','o.f016fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with','o.f016faccountCode = ac.f059fcompleteCode')


             ->orderBy('o.f016fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

   //  public function getList() 
   //  {

   //      $em = $this->getEntityManager();

   //      $qb = $em->createQueryBuilder();
   //      $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceCrAmount, o.f016fbalanceDrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus')
   //          ->from('Application\Entity\T016fopeningbalance', 'o')
   //          ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
   // //         ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
   //          // ->leftjoin('Application\Entity\T014fglcode', 'g','with', 'g.f014fid=o.f016fidGlcode')
   //          ->groupBy('o.f016fidFinancialyear');

   //      $query = $qb->getQuery();
   //      $result = array(

   //          // 'data' => $query->getSingleResult(),

   //          'data' => $query->getResult(),

   //      );

   //      return $result;
    
   //  } 

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceDrAmount, o.f016fbalanceCrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus')
            ->from('Application\Entity\T016fopeningbalance', 'o')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
            ->where('o.f016fid = :openingbalanceId')
            ->setParameter('openingbalanceId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    // public function getListById($id)
    // {
    //     $em = $this->getEntityManager();
    //     $qb = $em->createQueryBuilder();
    //     $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceCrAmount, o.f016fbalanceDrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus')
    //         ->from('Application\Entity\T016fopeningbalance', 'o')
    //         ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
    //         ->where('o.f016fidFinancialyear = :openingbalanceId')
    //         ->setParameter('openingbalanceId',$id)
    //         ;

    //     $query = $qb->getQuery();
    //     $result = $query->getResult();

    //     return $result;
    // }

    public function getListByApprovedStatus($approvedStatus) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        if($approvedStatus == 0 ){
         
            $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceCrAmount, o.f016fbalanceDrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear,  o.f016fapprovedStatus')
            ->from('Application\Entity\T016fopeningbalance', 'o')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
            ->where('o.f016fapprovedStatus = 0');
            // ->groupBy('o.f016fidFinancialyear');
        }
        elseif ($approvedStatus == 1 ) {
        
            $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceCrAmount, o.f016fbalanceDrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus')
            ->from('Application\Entity\T016fopeningbalance', 'o')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear')
            ->groupBy('o.f016fidFinancialyear');
            // ->where('o.f016fapprovedStatus = 1');
        }
        elseif ($approvedStatus == 2 ) {
        
            $qb->select('o.f016fid,o.f016fidFinancialyear, o.f016ffund, o.f016factivityCode, o.f016fdepartment, o.f016faccountCode, o.f016fdrAmount, o.f016fcrAmount,o.f016fbalanceCrAmount, o.f016fbalanceDrAmount, o.f016fstatus, o.f016fcreatedBy, o.f016fupdatedBy, f.f015fname as financialyear, o.f016fapprovedStatus')
            ->from('Application\Entity\T016fopeningbalance', 'o')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f','with', 'f.f015fid=o.f016fidFinancialyear');
            // ->groupBy('o.f016fidFinancialyear');
        }
       
        

        $query = $qb->getQuery();
        $result = $query->getArrayResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
      
        
        $openingbalance = new T016fopeningbalance();

        $openingbalance->setF016fidFinancialyear((int)$data['f016fidFinancialyear'])
                       ->setF016ffund($data['f016ffund'])
                       ->setF016factivityCode($data['f016factivityCode'])
                       ->setF016fdepartment($data['f016fdepartment'])
                       ->setF016faccountCode($data['f016faccountCode'])
                       ->setF016fdrAmount((float)$data['f016fdrAmount'])
                       ->setF016fcrAmount((float)$data['f016fcrAmount'])
                       ->setF016fbalanceDrAmount((float)$data['f016fbalanceDrAmount'])
                       ->setF016fbalanceCrAmount((float)$data['f016fbalanceCrAmount'])
                       ->setF016fapprovedStatus((int)"0")
                       ->setF016fstatus((int)$data['f016fstatus'])
                       ->setF016fcreatedBy((int)$_SESSION['userId'])
                       ->setF016fupdatedBy((int)$_SESSION['userId']);

        $openingbalance->setF016fcreateDtTm(new \DateTime())
                       ->setF016fupdateDtTm(new \DateTime());
        
        try{
        $em->persist($openingbalance);
        $em->flush();
        }
        catch(\Exception $e)
        {
          echo $e;
          }


        return $openingbalance;
    }

 
    public function updateData($openingbalance, $data = []) 
    {
        $em = $this->getEntityManager();
        
       $openingbalance->setF016fidFinancialyear((int)$data['f016fidFinancialyear'])
                       ->setF016ffund($data['f016ffund'])
                       ->setF016factivityCode($data['f016factivityCode'])
                       ->setF016fdepartment($data['f016fdepartment'])
                       ->setF016faccountCode($data['f016faccountCode'])
                       ->setF016fdrAmount((float)$data['f016fdrAmount'])
                       ->setF016fcrAmount((float)$data['f016fcrAmount'])
                       ->setF016fbalanceDrAmount((float)$data['f016fbalanceDrAmount'])
                       ->setF016fbalanceCrAmount((float)$data['f016fbalanceCrAmount'])
                       ->setF016fstatus((int)$data['f016fstatus'])
                       ->setF016fcreatedBy((int)$_SESSION['userId'])
                       ->setF016fupdatedBy((int)$_SESSION['userId']);

        $openingbalance->setF016fupdateDtTm(new \DateTime());


        
        $em->persist($openingbalance);
        $em->flush();

    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function openingBalanceApprovalStatus($openingbalance,$status) 
    {
        if((int)$status == 0){
            $status = 1;
        }
        $em = $this->getEntityManager();

        foreach ($openingbalance as $opening) 
        {

        $opening->setF016fapprovedStatus((int)$status)
                ->setF016fstatus((int)"1")
                 ->setF016fUpdateDtTm(new \DateTime());
       }
        $em->persist($opening);
        $em->flush();
    }
}



