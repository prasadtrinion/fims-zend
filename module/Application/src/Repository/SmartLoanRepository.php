<?php
namespace Application\Repository;

use Application\Entity\T043fsmartLoan;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SmartLoanRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('sl.f043fid, sl.f043fidVendor, vr.f030fcompanyName as vendor,  sl.f043finsurance, c.f021ffirstName as insuranceComp, sl.f043floanEntitlement, sl.f043fpurchasePrice, sl.f043floanAmount, sl.f043finstallmentPeriod, sl.f043fempId, s.f034fname as empId, sl.f043freference, sl.f043ffirstGuarantor,sm1.f034fname as firstGuarantor, sl.f043fmodel, sl.f043fstatus')
            ->from('Application\Entity\T043fsmartLoan', 'sl')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'sl.f043finsurance = c.f021fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','sl.f043fempId = s.f034fid')
			->leftjoin('Application\Entity\T034fstaffMaster', 'sm1', 'with','sl.f043ffirstGuarantor = sm1.f034fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'sl.f043fidVendor = vr.f030fid ')
            ->orderBy('sl.f043fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('sl.f043fid, sl.f043fidVendor, sl.f043finsurance, sl.f043floanEntitlement, sl.f043fpurchasePrice, sl.f043floanAmount, sl.f043finstallmentPeriod, sl.f043fempId, sl.f043freference, sl.f043ffirstGuarantor, sl.f043fmodel, sl.f043fstatus')
            ->from('Application\Entity\T043fsmartLoan', 'sl')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'sl.f043finsurance = c.f021fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','sl.f043fempId = s.f034fid')
			->leftjoin('Application\Entity\T034fstaffMaster', 'sm1', 'with','sl.f043ffirstGuarantor = sm1.f034fid')
			 ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'vr.f030fid = sl.f043fidVendor')
            ->where('sl.f043fid = :smartLoanId')
            ->setParameter('smartLoanId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $smartLoan = new T043fsmartLoan();

        $smartLoan->setF043fidVendor((int)$data['f043fidVendor'])
                ->setF043finsurance((int)$data['f043finsurance'])
                ->setF043fmodel($data['f043fmodel'])
                ->setF043floanEntitlement($data['f043floanEntitlement'])
                ->setF043fpurchasePrice($data['f043fpurchasePrice'])
                ->setF043floanAmount($data['f043floanAmount'])
                ->setF043finstallmentPeriod($data['f043finstallmentPeriod'])
                ->setF043fempId((int)$data['f043fempId'])
                ->setF043freference($data['f043freference'])
                ->setF043ffirstGuarantor((int)$data['f043ffirstGuarantor'])
                ->setF043fstatus((int)$data['f043fstatus'])
                ->setF043fcreatedBy((int)$_SESSION['userId'])
                ->setF043fupdatedBy((int)$_SESSION['userId']);

        $smartLoan->setF043fcreatedDtTm(new \DateTime())
                ->setF043fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($smartLoan);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $smartLoan;
    }

    public function updateData($smartLoan, $data = []) 
    {
        $em = $this->getEntityManager();
    
         $smartLoan->setF043fidVendor((int)$data['f043fidVendor'])
         		->setF043fmodel($data['f043fmodel'])
                ->setF043finsurance((int)$data['f043finsurance'])
                ->setF043floanEntitlement($data['f043floanEntitlement'])
                ->setF043fpurchasePrice($data['f043fpurchasePrice'])
                ->setF043floanAmount($data['f043floanAmount'])
                ->setF043finstallmentPeriod($data['f043finstallmentPeriod'])
                ->setF043fempId((int)$data['f043fempId'])
                ->setF043freference($data['f043freference'])
                ->setF043ffirstGuarantor((int)$data['f043ffirstGuarantor'])
                ->setF043fstatus((int)$data['f043fstatus'])
                ->setF043fupdatedBy((int)$_SESSION['userId']);

        $smartLoan->setF043fupdatedDtTm(new \DateTime());



        
        $em->persist($smartLoan);
        $em->flush();

    }

    
}