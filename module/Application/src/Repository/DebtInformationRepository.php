<?php
namespace Application\Repository;

use Application\Entity\T098fdebtInformation;
use Application\Entity\T098fdebtDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DebtInformationRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder(); 

        // Query
        $qb->select('d.f098fid, d.f098fcompanyName, d.f098fcontactOfficer, d.f098fphone, d.f098femail, d.f098ffax, d.f098faddress, d.f098fcity, d.f098fstate, s.f012fstateName, d.f098fcountry, c.f013fcountryName, d.f098fstatus')
            ->from('Application\Entity\T098fdebtInformation', 'd')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 'd.f098fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'd.f098fstate = s.f012fid')
            ->orderBy('d.f098fid','DESC');
         
         
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
	public function getListById($id)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();

		$qb->select('d.f098fid, d.f098fcompanyName, d.f098fcontactOfficer, d.f098fphone, d.f098femail, d.f098ffax, d.f098faddress, d.f098fcity, d.f098fstate, s.f012fstateName, d.f098fcountry, c.f013fcountryName, d.f098fstatus, dd.f098fdescription, dd.f098fstartDate, dd.f098fendDate, dd.f098fidDetails')
            ->from('Application\Entity\T098fdebtInformation', 'd')
            ->leftjoin('Application\Entity\T098fdebtDetails','dd','with', 'd.f098fid = dd.f098fidDebt')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 'd.f098fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', 'd.f098fstate = s.f012fid')
			->where('d.f098fid =:debtId')
			->setParameter('debtId', $id);

		$query = $qb->getQuery();
		$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f098fstartDate'];
            $invoice['f098fstartDate'] = date("Y-m-d", strtotime($date));
             $date                        = $invoice['f098fendDate'];
            $invoice['f098fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;
	}

	public function createDebt($data) 
	{

		$em = $this->getEntityManager();

		$debt = new T098fdebtInformation();

		$debt->setF098fcompanyName($data['f098fcompanyName'])
			    ->setF098fcontactOfficer($data['f098fcontactOfficer'])
			    ->setF098fphone($data['f098fphone'])
			    ->setF098femail($data['f098femail'])
			    ->setF098ffax($data['f098ffax'])
			    ->setF098faddress($data['f098faddress'])
			    ->setF098fcity($data['f098fcity'])
			    ->setF098fstate((int)$data['f098fstate'])
			    ->setF098fcountry((int)$data['f098fcountry'])
			    ->setF098fstatus((int)$data['f098fstatus'])
			    ->setF098fcreatedBy((int)$_SESSION['userId'])
			    ->setF098fupdatedBy((int)$_SESSION['userId']);

		$debt->setF098fcreatedDtTm(new \DateTime())
    			->setF098fupdatedDtTm(new \DateTime());


		
		$em->persist($debt);
		$em->flush();

		$debtDetails = $data['debt-details'];

        foreach ($debtDetails as $debtDetail) 
        {

            $debtDetailObj = new T098fdebtDetails();

            $debtDetailObj->setF098fidDebt($debt->getF098fid())
                ->setF098fstartDate(new \DateTime($debtDetail['f098fstartDate']))
                ->setF098fendDate(new \DateTime($debtDetail['f098fendDate']))
                ->setF098fdescription($debtDetail['f098fdescription'])
                ->setF098fstatus((int) $debtDetail['f098fstatus'])
                ->setF098fcreatedBy((int)$_SESSION['userId'])
                ->setF098fupdatedBy((int)$_SESSION['userId']);
                

            $debtDetailObj->setF098fcreatedDtTm(new \DateTime())
                ->setF098fupdatedDtTm(new \DateTime());
            
                $em->persist($debtDetailObj);

                $em->flush();

            
        }

		return $debt;
	}

	public function updateDebt($debt, $data = []) 
	{
        $em = $this->getEntityManager();
        
       $debt->setF098fcompanyName($data['f098fcompanyName'])
			    ->setF098fcontactOfficer($data['f098fcontactOfficer'])
			    ->setF098fphone($data['f098fphone'])
			    ->setF098femail($data['f098femail'])
			    ->setF098ffax($data['f098ffax'])
			    ->setF098faddress($data['f098faddress'])
			    ->setF098fcity($data['f098fcity'])
			    ->setF098fstate((int)$data['f098fstate'])
			    ->setF098fcountry((int)$data['f098fcountry'])
			    ->setF098fstatus((int)$data['f098fstatus'])
			    ->setF098fupdatedBy((int)$_SESSION['userId']);

		$debt->setF098fupdatedDtTm(new \DateTime());


		$em->persist($debt);
		$em->flush();
        return $debt;

    }

    public function updateDebtDetail($debtDetailObj, $debtDetail)
    {
        
        $em = $this->getEntityManager();

        $debtDetailObj->setF098fstartDate(new \DateTime($debtDetail['f098fstartDate']))
                ->setF098fendDate(new \DateTime($debtDetail['f098fendDate']))
                ->setF098fdescription($debtDetail['f098fdescription'])
                ->setF098fstatus((int) $debtDetail['f098fstatus'])
                ->setF098fupdatedBy((int)$_SESSION['userId']);
                

        $debtDetailObj->setF098fupdatedDtTm(new \DateTime());
        try {
            $em->persist($debtDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    
    public function createDebtDetail($debtObj, $debtDetail)
    {
        $em = $this->getEntityManager();
        // print_r($debtObj);exit;

       $debtDetailObj = new T098fDebtDetails();

        $debtDetailObj->setF098fidDebt($debtObj->getF098fid())
                ->setF098fstartDate(new \DateTime($debtDetail['f098fstartDate']))
                ->setF098fendDate(new \DateTime($debtDetail['f098fendDate']))
                ->setF098fdescription($debtDetail['f098fdescription'])
                ->setF098fstatus((int) $debtDetail['f098fstatus'])
                ->setF098fcreatedBy((int)$_SESSION['userId'])
                ->setF098fupdatedBy((int)$_SESSION['userId']);
                

            $debtDetailObj->setF098fcreatedDtTm(new \DateTime())
                ->setF098fupdatedDtTm(new \DateTime());
        try
        {
            $em->persist($debtDetailObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }

    public function deleteDebtInformationDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t098fdebt_details WHERE f098fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }    
}
?>