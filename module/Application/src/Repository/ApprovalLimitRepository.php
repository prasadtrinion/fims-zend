<?php
namespace Application\Repository;

use Application\Entity\T020fapprovalLimit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ApprovalLimitRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f020fid, a.f020fidUser, u.f014fuserName, a.f020fmodule, a.f020ffromAmount, a.f020ftoAmount, a.f020fstatus, a.f020fupdatedBy, a.f020fcreatedBy,d.f011fdefinitionCode as module')
            ->from('Application\Entity\T020fapprovalLimit', 'a')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','a.f020fidUser = u.f014fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','a.f020fmodule = d.f011fid');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f020fid, a.f020fidUser, u.f014fuserName, a.f020fmodule, a.f020ffromAmount, a.f020ftoAmount, a.f020fstatus, a.f020fupdatedBy, a.f020fcreatedBy')
            ->from('Application\Entity\T020fapprovalLimit', 'a')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','a.f020fidUser = u.f014fid')
            ->where('a.f020fid = :approvalLimitId')
            ->setParameter('approvalLimitId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $approval = new T020fapprovalLimit();

        $approval->setF020fidUser($data['f020fidUser'])
             ->setF020fmodule((int)$data['f020fmodule'])  
             ->setF020ffromAmount((float)$data['f020ffromAmount'])
             ->setF020ftoAmount((float)$data['f020ftoAmount'])
             ->setF020fstatus($data['f020fstatus'])
             ->setF020fcreatedBy((int)$_SESSION['userId'])
             ->setF020fupdatedBy((int)$_SESSION['userId']);

        $approval->setF020fcreatedDtTm(new \DateTime())
                ->setF020fupdatedDtTm(new \DateTime());

 
        $em->persist($approval);
        $em->flush();

        return $approval;

    }

    /* to edit the data in database*/

    public function updateData($approval, $data = []) 
    {
        $em = $this->getEntityManager();

         $approval->setF020fidUser($data['f020fidUser'])
             ->setF020fmodule((int)$data['f020fmodule'])  
             ->setF020ffromAmount((float)$data['f020ffromAmount'])
             ->setF020ftoAmount((float)$data['f020ftoAmount'])
             ->setF020fstatus($data['f020fstatus'])
             ->setF020fcreatedBy((int)$_SESSION['userId'])
             ->setF020fupdatedBy((int)$_SESSION['userId']);

        $approval->setF020fupdatedDtTm(new \DateTime());
        
        $em->persist($approval);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>