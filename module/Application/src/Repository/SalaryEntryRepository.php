<?php
namespace Application\Repository;

use Application\Entity\T081fsalaryEntry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SalaryEntryRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select('se.f081fid, se.f081fidStaff, s.f034fname as staff,  se.f081fpaymentType, se.f081fstatus, se.f081ftype')
            ->from('Application\Entity\T081fsalaryEntry', 'se')
             ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 's.f034fstaffId = se.f081fidStaff ')
             ->orderBy('se.f081fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('se.f081fid, se.f081fidStaff, s.f034fstaffId as staff,  se.f081fpaymentType, se.f081fstatus, se.f081ftype')
            ->from('Application\Entity\T081fsalaryEntry', 'se')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 's.f034fstaffId = se.f081fidStaff')
            ->where('se.f081fid = :salaryId')
            ->setParameter('salaryId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $salary = new T081fsalaryEntry();

        $salary->setF081fidStaff((int)$data['f081fidStaff'])
                ->setF081fpaymentType($data['f081fpaymentType'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081ftype($data['f081ftype'])
                ->setF081fcreatedBy((int)$_SESSION['userId'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $salary->setF081fcreatedDtTm(new \DateTime())
                ->setF081fupdatedDtTm(new \DateTime());
          
        $em->persist($salary);
        
        $em->flush();
        
        return $salary;
    }

    public function updateData($salary, $data = []) 
    {
        $em = $this->getEntityManager();

        $salary->setF081fidStaff((int)$data['f081fidStaff'])
                ->setF081fpaymentType($data['f081fpaymentType'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081ftype($data['f081ftype'])
                ->setF081fcreatedBy((int)$_SESSION['userId'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $salary->setF081fcreatedDtTm(new \DateTime())
                ->setF081fupdatedDtTm(new \DateTime());
        
        $em->persist($salary);
        $em->flush();
    }  
}