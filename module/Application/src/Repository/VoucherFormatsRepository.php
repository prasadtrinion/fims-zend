<?php
namespace Application\Repository;

use Application\Entity\T017fvoucherFormats;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class VoucherFormatsRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList(){
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('v.f017fid,IDENTITY(v.f017fidVoucherType),v.f017fvoucherName,IDENTITY(v.f017fparentVoucherType), v.f017feffectiveDate, v.f017fautoNumbering, v.f017fprefix, v.f017fsuffix, v.f017fautoPrintAfterSaving, v.f017fautoNarration, v.f017fautoNarrationDescription,v.f017fupdatedDtTm, v.f017fstatus, IDENTITY(v.f017fupdatedBy)')
            ->from('Application\Entity\T017fvoucherFormats', 'v');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

         $qb->select('v.f017fid,IDENTITY(v.f017fidVoucherType),v.f017fvoucherName,IDENTITY(v.f017fparentVoucherType), v.f017feffectiveDate, v.f017fautoNumbering, v.f017fprefix, v.f017fsuffix, v.f017fautoPrintAfterSaving, v.f017fautoNarration, v.f017fautoNarrationDescription,v.f017fupdatedDtTm, v.f017fstatus, IDENTITY(v.f017fupdatedBy)')
            ->from('Application\Entity\T017fvoucherFormats', 'v');


        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $voucherFormats = new T017fvoucherFormats();

        $voucherFormats->setF017fidVoucherType($data['f017fidVoucherType'])
                       ->setF017fvoucherName($data['f017fvoucherName'])
                       ->setF017fparentVoucherType($data['f017fparentVoucherType'])
                       ->setF017feffectiveDate($data['f017feffectiveDate'])
                       ->setF017fautoNumbering($data['f017fautoNumbering'])
                       ->setF017fautoPrintAfterSaving($data['f017fautoPrintAfterSaving'])
                       ->setF017fautoNarration($data['f017fautoNarration'])
                       ->setF017fautoNarrationDescription($data['f017fautoNarrationDescription'])
                       ->setF017fstatus($data['f017fstatus'])
                       ->setF017fupdatedBy((int)$_SESSION['userId']);

        $voucherFormats->setF016fupdateDtTm(new \DateTime());


        
        $em->persist($voucherFormats);
        $em->flush();

        return $voucherFormats;
    }

    public function updateData($voucherFormats, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $voucherFormats->setF017fidVoucherType($data['f017fidVoucherType'])
                       ->setF017fvoucherName($data['f017fvoucherName'])
                       ->setF017fparentVoucherType($data['f017fparentVoucherType'])
                       ->setF017feffectiveDate($data['f017feffectiveDate'])
                       ->setF017fautoNumbering($data['f017fautoNumbering'])
                       ->setF017fautoPrintAfterSaving($data['f017fautoPrintAfterSaving'])
                       ->setF017fautoNarration($data['f017fautoNarration'])
                       ->setF017fautoNarrationDescription($data['f017fautoNarrationDescription'])
                       ->setF017fstatus($data['f017fstatus'])
                       ->setF017fupdatedBy((int)$_SESSION['userId']);

        $voucherFormats->setF016fupdateDtTm(new \DateTime());

        $em->persist($voucherFormats);
        $em->flush();

    }
}
?>