<?php
namespace Application\Repository;

use Application\Entity\T013fcountry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CountryRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f013fid,c.f013fcountryName , c.f013fshortName, c.f013fiso, c.f013fstatus, c.f013fupdatedBy')
            ->from('Application\Entity\T013fcountry','c')
            ->orderBy('c.f013fid', 'DESC');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('c.f013fid,c.f013fcountryName, c.f013fshortName, c.f013fiso, c.f013fstatus, c.f013fupdatedBy')
            ->from('Application\Entity\T013fcountry','c')
            ->where('c.f013fid = :countryId')
            ->setParameter('countryId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    public function getCountryStates($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('c.f012fid,c.f012fstateName, c.f012fshortName')
            ->from('Application\Entity\T012fstate','c')
            ->where('c.f012fidCountry = :countryId')
            ->orderBy('c.f012fstateName')
            ->setParameter('countryId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $country = new T013fcountry();

        $country->setf013fcountryName($data['f013fcountryName'])
              ->setf013fshortName($data['f013fshortName'])
              ->setf013fiso($data['f013fiso'])
              ->setF013fstatus((int)$data['f013fstatus'])
              ->setF013fupdatedBy((int)$_SESSION['userId']);


        $country->setF013fupdatedDtTm(new \DateTime());

 
        $em->persist($country);
        $em->flush();

        return $country ;

    }

    public function updateData($country, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $country->setf013fcountryName($data['f013fcountryName'])
              ->setf013fshortName($data['f013fshortName'])
              ->setf013fiso($data['f013fiso'])
              ->setF013fstatus((int)$data['f013fstatus'])
              ->setF013fupdatedBy((int)$_SESSION['userId']);

        $country->setF013fupdatedDtTm(new \DateTime());

 
        $em->persist($country);
        $em->flush();
    }

    public function getCountryById($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('s.f012fid,s.f012fstateName')
            ->from('Application\Entity\T012fstate','s')
            ->leftjoin('Application\Entity\T013fcountry','c','with','c.f013fid = s.f012fidCountry')
            ->where('s.f012fidCountry= :countryId')
            ->setParameter('countryId', $id);
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
          $qb->select('c.f013fid,c.f013fcountryName, c.f013fshortName, c.f013fiso, c.f013fstatus, c.f013fupdatedBy')
            ->from('Application\Entity\T013fcountry','c')
            ->orderBy('c.f013fcountryName', 'ASC')
            ->where('c.f013fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

 }
 ?>

