<?php
namespace Application\Repository;

use Application\Entity\T108fbudgetSummary;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetSummaryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList($postData) 
    {
        $departmentCode=$postData['Department'];
        $idFinancialYear=$postData['FinancialYear'];
        $financialYear=(int)$idFinancialYear;
        $fund=$postData['Fund'];
 
        
        $department= substr($departmentCode,0,3);
        
        
        
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query = "select f108fid, f108fdepartment,(t015fdepartment.f015fdepartment_code+' - ' +t015fdepartment.f015fdepartment_name) as department, f108ffund, (t057ffund.f057fcode+' - ' +t057ffund.f057fname) as fund, f108faccount,(t059faccount_code.f059fcomplete_code+' - ' +t059faccount_code.f059fname) as account,  f108factivity, (t058factivity_code.f058fcomplete_code+' - ' +t058factivity_code.f058fname) as activity, f108fid_financial_year, f108fallocated_amount, f108fincrement_amount, f108fdecrement_amount, f108fveriment_from, f108fveriment_to,  f108fbudget_expensed, f108fbudget_pending,f108fbudget_commitment  from t108fbudget_summary inner join t059faccount_code  on t108fbudget_summary.f108faccount = t059faccount_code.f059fcomplete_code inner join t058factivity_code  on t108fbudget_summary.f108factivity = t058factivity_code.f058fcomplete_code inner join t015fdepartment  on t108fbudget_summary.f108fdepartment = t015fdepartment.f015fdepartment_code inner join t057ffund  on t108fbudget_summary.f108ffund = t057ffund.f057fcode where SUBSTRING(f108fdepartment,1,3)='$department' and f108fid_financial_year= $financialYear and f108ffund = '$fund'";
        $result=$em->getConnection()->executeQuery($query)->fetchAll();
         
        // $query = $qb->getQuery();

        // $result = $query->getArrayResult();


        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f108fid, a.f108fdepartment,a.f108ffund,a.f108faccount,a.f108factivity, a.f108fidFinancialYear, a.f108fallocatedAmount, a.f108fincrementAmount,a.f108fdecrementAmount, a.f108fverimentFrom, a.f108fverimentTo, a.f108fbudgetExpensed, a.f108fbudgetPending')
            ->from('Application\Entity\T108fbudgetSummary','a')
             ->where('a.f108fid = :summaryId')
            ->setParameter('summaryId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function getBalanceStatus($postData) 
    {
        $departmentCode=$postData['Department'];
        $idFinancialYear=$postData['FinancialYear'];
        $financialYear=(int)$idFinancialYear;
        $fund=$postData['Fund'];
        $activity=$postData['Activity'];
        $account=$postData['Account'];
        $Amount=$postData['Amount'];
        $amount=(float)$Amount;        
        
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query = "select f108fid, f108fdepartment, f108ffund, f108faccount, f108factivity, f108fid_financial_year, f108fallocated_amount, f108fincrement_amount, f108fdecrement_amount, f108fveriment_from, f108fveriment_to,  f108fbudget_expensed, f108fbudget_pending  from t108fbudget_summary inner join t110fbudgetyear on f108fid_financial_year = f110fid inner join t015ffinancialyear on f015fname = f110fyear where f108fdepartment='$departmentCode' and f015fid= $financialYear and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity'";
        $result=$em->getConnection()->executeQuery($query)->fetch();

        // print_r($result);
        // exit();
        if(!$result){
        $account=substr($account,0,2)."000"; 
        $query = "select f108fid, f108fdepartment, f108ffund, f108faccount, f108factivity, f108fid_financial_year, f108fallocated_amount, f108fincrement_amount, f108fdecrement_amount, f108fveriment_from, f108fveriment_to,  f108fbudget_expensed, f108fbudget_pending  from t108fbudget_summary inner join t110fbudgetyear on f108fid_financial_year = f110fid inner join t015ffinancialyear on f015fname = f110fyear where f108fdepartment='$departmentCode' and f015fid= $financialYear and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity'";
        $result=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);
        // exit();
         if(!$result){
        $account=substr($account,0,1)."0000"; 
        $query = "select f108fid, f108fdepartment, f108ffund, f108faccount, f108factivity, f108fid_financial_year, f108fallocated_amount, f108fincrement_amount, f108fdecrement_amount, f108fveriment_from, f108fveriment_to,  f108fbudget_expensed, f108fbudget_pending  from t108fbudget_summary inner join t110fbudgetyear on f108fid_financial_year = f110fid inner join t015ffinancialyear on f015fname = f110fyear where f108fdepartment='$departmentCode' and f015fid= $financialYear and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity'";
        $result=$em->getConnection()->executeQuery($query)->fetch();
        if(!$result){
            $status=-1;
        }
        else{

        $balance=$result['f108fbudget_pending'];

        if($amount<$balance)
        {
            $status=1;
        }
        else
        {
            $status=0;
        }
        }
        }
        else{

        $balance=$result['f108fbudget_pending'];

        if($amount<$balance)
        {
            $status=1;
        }
        else
        {
            $status=0;
        }
        }
        }
        else{

        $balance=$result['f108fbudget_pending'];

        if($amount<$balance)
        {
            $status=1;
        }
        else
        {
            $status=0;
        }
        }
// echo $status;
// die();
        return $status;
        
    }

}