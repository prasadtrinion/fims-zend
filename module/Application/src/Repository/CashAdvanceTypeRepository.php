<?php
namespace Application\Repository;

use Application\Entity\T051fcashAdvanceType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class CashAdvanceTypeRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder(); 

        // Query

        $qb->select("s.f051fid, s.f051fcashId, s.f051fffund, s.f051factivity,  s.f051faccount, s.f051fdescription, s.f051fstatus, f.f057fcode + '-' + f.f057fname as fund, ac.f059fcompleteCode+ ' - '+ ac.f059fname as account, a.f058fcompleteCode+ ' - ' + a.f058fname as activity")
            ->from('Application\Entity\T051fcashAdvanceType', 's')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 's.f051fffund = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with', 's.f051factivity = a.f058fcompleteCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 's.f051faccount = ac.f059fcompleteCode')
            ->orderBy('s.f051fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('s.f051fid, s.f051fcashId, s.f051fffund as f051ffund, s.f051factivity, s.f051faccount, s.f051fdescription, s.f051fstatus')
            ->from('Application\Entity\T051fcashAdvanceType', 's')
            ->where('s.f051fid = :cashId')
            ->setParameter('cashId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($postData)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        

            $data=array();
            $data['type']='CAT';
            $initialRepository = $em->getRepository('Application\Entity\T011finitialConfig');
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f051fcashId'] = $generateNumber;
                
        $advance = new T051fcashAdvanceType();
        

        $advance->setF051fcashId((int)$postData['f051fcashId'])
                ->setF051fdescription(($postData['f051fdescription']))
                ->setF051fffund($postData['f051ffund'])
                ->setF051factivity($postData['f051factivity'])
                ->setF051faccount($postData['f051faccount'])
                ->setF051fstatus((int)$postData['f051fstatus'])
                ->setF051fcreatedBy((int)$_SESSION['userId']);

        $advance->setF051fcreatedDtTm(new \DateTime());

        try{
        $em->persist($advance);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $advance;
    }

   public function updateData($advance, $data = []) 
    {
        $em = $this->getEntityManager();

        $advance->setF051fdescription(($data['f051fdescription']))
                ->setF051fffund($data['f051ffund'])
                // ->setF051fdepartment($data['f051fdepartment'])
                ->setF051factivity($data['f051factivity'])
                ->setF051faccount($data['f051faccount'])
                ->setF051fstatus((int)$data['f051fstatus'])
                ->setF051fupdatedBy((int)$_SESSION['userId']);

        $advance->setF051fupdatedDtTm(new \DateTime());


        
        $em->persist($advance);
        $em->flush();

        
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('s.f051fid, s.f051fcashId, s.f051fffund, s.f051factivity,  s.f051faccount, s.f051fdescription, s.f051fstatus')
            ->from('Application\Entity\T051fcashAdvanceType', 's')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 's.f051fffund = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with', 's.f051factivity = a.f058fcompleteCode')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 's.f051faccount = ac.f059fcompleteCode')
            ->orderBy('s.f051fdescription','ASC')
            ->where('s.f051fstatus=1');http://uum-api.mtcsb.my/v1/activeList/T051fcashAdvanceType

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}
