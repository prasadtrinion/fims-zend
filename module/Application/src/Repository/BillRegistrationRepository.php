<?php
namespace Application\Repository;

use Application\Entity\T091fbillRegistration;
use Application\Entity\T091fbillRegistrationDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class BillRegistrationRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        // $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason')
        //     ->from('Application\Entity\T091fbillRegistration', 'p')
        //     ->orderBy('p.f091fid','DESC');

        //        $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason')
        //     ->from('Application\Entity\T091fbillRegistration', 'p')
            // ->orderBy('p.f091fid','DESC');
            $query  = "select p.f091fid,p.f091freference_number as f091freferenceNumber, p. f091fapproved_status as f091fapprovedStatus, p.f091fcreated_date as f091fcreatedDate, p.f091ftotal_amount as f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpayment_status as f091fpaymentStatus, p.f091fcontact_person1 as f091fcontactPerson1, p.f091fcontact_person2 as f091fcontactPerson2, p.f091fcontact_person3 as f091fcontactPerson3, p.f091freason, p.f091fid_vendor as f091fidVendor, p.f091fbank_ac_no as f091fbankAcNo, f091femail, p.f091finvoice_date as f091finvoiceDate, p.f091fdescription from t091fbill_registration as p where p.f091fid not in (select  distinct(f094id_bill_registration) from t094fpayment_voucher_details) order by f091fid desc";
         $result =  $em->getConnection()->executeQuery($query)->fetchAll();
         // print_r($result);
         // die()
     //    $query = $qb->getQuery();
	    // $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	   $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f091fcreatedDate'];
            $item['f091fcreatedDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result
        );
        return $result;
    
    }


    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
	

       $qb->select("p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, pd.f091fidDetails, pd.f091fidItem, pd.f091fsoCode, pd.f091ftype, pd.f091fquantity, pd.f091funit, pd.f091fprice, pd.f091total, pd.f091ftaxCode, pd.f091fpercentage, pd.f091ftaxAmount, pd.f091ftotalIncTax, pd.f091ffundCode, pd.f091fdepartmentCode, pd.f091factivityCode, pd.f091faccountCode, pd.f091fbudgetFundCode, pd.f091fbudgetDepartmentCode, pd.f091fbudgetActivityCode, pd.f091fbudgetAccountCode, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription, (vr.f030fcompanyName+'-'+vr.f030fvendorCode) as vendorDetails, pd.f091fidGrnDetails")
            ->from('Application\Entity\T091fbillRegistration', 'p')
            ->leftjoin('Application\Entity\T091fbillRegistrationDetails', 'pd', 'with', 'p.f091fid = pd.f091fidBill')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'vr.f030fid = p.f091fidVendor')
            ->where('p.f091fid = :billId')
            ->setParameter('billId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f091fcreatedDate'];
            $item['f091fcreatedDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
	
        return $result1;
    }

    public function billRegistrationApproval($bills) 
    {

        $em = $this->getEntityManager();

        foreach ($bills['id'] as $bill) 
        {
        // print_r($bill);exit;

            $id = (int)$bill;
            $status = (int)$bills['status'];
            // print_r($id);exit;

            $query  = "update t091fbill_registration set f091fapproved_status = $status , f091fupdated_dt_tm = getdate() where f091fid = $id";
         $result =  $em->getConnection()->executeQuery($query);

	   }
    }
    
    public function getBillRegistrationApprovedList($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration', 'p')
            ->orderBy('p.f091fid','DESC')
            ->where('p.f091fapprovedStatus = 0');
            
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
            $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration', 'p')
            ->orderBy('p.f091fid','DESC')
                ->where('p.f091fapprovedStatus = 1');
                 
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
            $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration', 'p')
            ->orderBy('p.f091fid','DESC');
                
            }
            
            
        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	 $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f091fcreatedDate'];
            $item['f091fcreatedDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        
        return $result1;
        
    }


    public function getBillRegistrationPendingList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f091fid,p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091fcontactPerson1, p.f091fcontactPerson2, p.f091fcontactPerson3, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration', 'p')
            ->where('p.f091fpaymentStatus=0')
            ->andWhere('p.f091fapprovedStatus = 1')
            ->orderBy('p.f091fid','DESC');
	   

        $query = $qb->getQuery();
	    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	   $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f091fcreatedDate'];
            $item['f091fcreatedDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f091finvoiceDate'];
            $item['f091finvoiceDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        
        return $result1;
    
    }

    public function getBillRegistrationTypeList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
     	
	$qb->select('pd.f091ftype')
            ->from('Application\Entity\T091fbillRegistration', 'pd');
        
        $datas = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        print_r($datas);
         
        $qb1 = $em->createQueryBuilder();
$finalarray = array();
$results = array();
        $i=0;
       foreach($datas as $data)
       {
	  
	  if($data['f091ftype'] =='Grn')
	  {
		
            $qb1->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, b.f091fid,b.f091freferenceNumber, b. f091fapprovedStatus, b.f091fcreatedDate, b.f091ftotalAmount, b.f091fstatus, b.f091ftype, b.f091freason, b.f091fidVendor, b.f091fbankAcNo, b.f091femail, b.f091finvoiceDate, b.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration', 'b')
	    ->leftjoin('Application\Entity\T034fgrn', 'p', 'with', 'p.f034freferenceNumber = b.f091freferenceNumber')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fid')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid');
	    
           }
	   else if ($data['f091ftype'] =='Cash')
	   {
           $qb1->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as budgetYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal, b.f091fid,b.f091freferenceNumber, b. f091fapprovedStatus, b.f091fcreatedDate, b.f091ftotalAmount, b.f091fstatus, b.f091ftype, b.f091freason, b.f091fidVendor, b.f091fbankAcNo, b.f091femail, b.f091finvoiceDate, b.f091fdescription')
            ->from('Application\Entity\T091fbillRegistration','b')
            ->leftjoin('Application\Entity\T052fcashApplication', 'c', 'with', 'b.f091freferenceNumber = c.f052fcashAdvance')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid');
            
                 
            }
	 $result = $qb1->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

  $results[$i] = $result;
$i++;
// print_r($results);exit;
       }
       

       
        
    }
    public function createNewData($data)
    {
        $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();
            $year = date('y');
            $Year = date('Y');
            $query  = "SELECT top 1 f091freference_number from t091fbill_registration ORDER BY f091fid DESC";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $dataResult = $result['f091freference_number'];
            $value=substr($dataResult, 3,6);
            $count=$value + 1;
            $number = "BIL" . sprintf("%'06d", $count) . "/" . $year;


            $bill = new T091fbillRegistration();

            $bill->setF091freferenceNumber($number)
            ->setF091fcreatedDate(new \DateTime())
            ->setF091ftotalAmount((float)$data['f091ftotalAmount'])
            ->setF091fapprovedStatus((int)0)
            ->setF091ftype($data['f091ftype'])
            ->setF091fcontactPerson1($data['f091fcontactPerson1'])
            ->setF091fcontactPerson2($data['f091fcontactPerson2'])
            ->setF091fcontactPerson3($data['f091fcontactPerson3'])
            // ->setF091freason($data['f091freason'])
            ->setF091fpaymentStatus((int)0)
            ->setF091fidVendor((int)$data['f091fidVendor'])
            ->setF091fbankAcNo($data['f091fbankAcNo'])
            ->setF091femail($data['f091femail'])
            ->setF091voucherType((int)$data['f091fvoucherType'])
            ->setF091fuumBank((int)$data['f091fuumBank'])
            ->setF091finvoiceDate(new \DateTime($data['f091finvoiceDate']))
            ->setF091fdescription($data['f091fdescription'])
            ->setF091fstatus((int)1)
            ->setF091fcreatedBy((int)$_SESSION['userId'])
            ->setF091fupdatedBy((int)$_SESSION['userId']);

            $bill->setF091fcreatedDtTm(new \DateTime())
            ->setF091fupdatedDtTm(new \DateTime());
        
            $em->persist($bill);
            $em->flush();




            $resultGrns = $data['bill-details'];
            foreach ($resultGrns as $resultGrn) 
            {

                $idGrnDetail = (int)$resultGrn['f091fidGrnDetails'];
                // print_r($data['f091fvoucherType']);exit;
                switch ($data['f091fvoucherType'])
                {
                    case 'Vendor':
                    $query = "select * from t034fgrn_details where f034fid_details = $idGrnDetail";
                    break;
                }
                    $result =  $em->getConnection()->executeQuery($query)->fetch();

                // print_r($resultGrn);exit;
                if (isset($result))
                {
                   
        
                // print_r($result);exit;

                $billDetailObj = new T091fbillRegistrationDetails();



                $billDetailObj->setF091fidBill((int)$bill->getF091fid())
                ->setF091freferenceNumber($result['f091freferenceNumber'])
                ->setF091fidItem((int) $result['f034fid_item'])
                ->setF091ftype($data['f091fvoucherType'])
                ->setF091fsoCode($result['f034fso_code'])
                ->setF091fquantity((int)$result['f034fquantity'])
                ->setF091funit($result['f034funit'])
                ->setF091fprice((float)$result['f034fprice'])
                ->setF091total((float)$result['f034total'])
                ->setF091fpercentage((int) $result['f034fpercentage'])
                ->setF091ftaxCode((int)$result['f034ftax_code'])
                ->setF091fstatus((int)$result['f034fstatus'])
                ->setF091ftaxAmount((float) $result['f034ftax_amount'])
                ->setF091ftotalIncTax((float) $result['f034ftotal_inc_tax'])
                ->setF091ffundCode($result['f034ffund_code'])
                ->setF091fdepartmentCode($result['f034fdepartment_code'])
                ->setF091factivityCode($result['f034factivity_code'])
                ->setF091faccountCode($result['f034faccount_code'])
                ->setF091fbudgetFundCode($result['f034fbudget_fund_code'])
                ->setF091fidGrnDetails((int)$idGrnDetail)
                ->setF091fbudgetDepartmentCode($result['f034fbudget_department_code'])
                ->setF091fbudgetActivityCode($result['f034fbudget_activity_code'])
                ->setF091fbudgetAccountCode($result['f034fbudget_account_code'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);
                

                $billDetailObj->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());
               
                try
                {    
                    $em->persist($billDetailObj);
        
                    $em->flush();

                }
                catch(\Exception $e)
                {
                    echo $e;
                }
            }  
        }
        
      return $billDetailObj;  
    }

    public function getGrnBasedOnVendor($data)
    {
        $vendoraId = (int)$data['vendorId'];
        // $status = (int)$data['status'];
        // print_r($vendoraId);exit;

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear,  p.f034fidSupplier,  p.f034fdescription, p.f034fidDepartment,  p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount,p.f034fapprovedDate,p.f034fapprovedRefNum,p.f034fdonor, p.f034fapprovalStatus, p.f034frating')
            ->from('Application\Entity\T034fgrn', 'p')
            // ->leftjoin('Application\Entity\T034fgrnDetails', 'pd', 'with', 'p.f034fid = pd.f034fidGrn')
            // ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            // ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            // ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where('p.f034fidSupplier = :purchaseId')
            ->andwhere('p.f034fapprovalStatus = 1')
            ->setParameter('purchaseId',$vendoraId);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // print_r($result);exit;
    // $result1 = array();
    //     foreach ($result as $item) {
    //         $date                        = $item['f034forderDate'];
    //         $item['f034forderDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034fapprovedDate'];
    //         $item['f034fapprovedDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034fexpiredDate'];
    //         $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034frequiredDate'];
    //         $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
    //         array_push($result1, $item);
    //     }

    //      $i=0;
    //     foreach ($result1 as $purchase)
    //     {
    //     // print_r($purchase);exit;

    //     $categoryType = $purchase['f034fitemType'];
    //     $category = (int)$purchase['f034fitemCategory'];
    //     $subCategory = (int)$purchase['f034fitemSubCategory'];
    //     $item = (int)$purchase['f034fidItem'];


    //         if($categoryType == 'A')
    //         {
    //             // $em = $this->getEntityManager();

    //             $query = "select (f087fcategory_code+'-'+f087fcategory_discription) as assetCategory from t087fasset_category where f087fid = $category"; 
    //             $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

    //              $query = "select (f089fsubcode+'-'+f089fsub_description) as assetSubCategory from t089fasset_sub_categories where f089fid = $subCategory"; 
    //             $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

    //              $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
    //             $itemResult=$em->getConnection()->executeQuery($query)->fetch();
    //     // print_r($categoryResult);exit;
    //     $result1[$i]['categoryName'] = $categoryResult['assetCategory'];
    //     $result1[$i]['subCategoryName'] = $subCategoryesult['assetSubCategory'];
    //     $result1[$i]['itemName'] = $itemResult['assetItem'];


           
    //         }
    //         elseif($categoryType == 'P')
    //         {
    //             // $em = $this->getEntityManager();

    //              $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
    //             $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

    //              $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
    //             $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

    //              $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $id"; 
    //             $itemResult=$em->getConnection()->executeQuery($query)->fetch();


    //     $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
    //     $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];
    //     $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
    //         }
    //         $i++;

    //     }

    //     // print_r($result1);exit;
    //     return $result1;
        return $result;
    }

    public function getBillRegistrationByVendor($id)
    {
         $vendoraId = (int)$id;
        // $status = (int)$data['status'];
        // print_r($vendoraId);exit;

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

      $qb->select("p.f091fid, p.f091freferenceNumber, p. f091fapprovedStatus, p.f091fcreatedDate, p.f091ftotalAmount, p.f091fstatus, p.f091ftype, p.f091fpaymentStatus, p.f091freason, p.f091fidVendor, p.f091fbankAcNo, p.f091femail, p.f091finvoiceDate, p.f091fdescription")
            ->from('Application\Entity\T091fbillRegistration', 'p')
            // ->leftjoin('Application\Entity\T091fbillRegistrationDetails', 'pd', 'with', 'p.f091fid = pd.f091fidBill')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'vr.f030fid = p.f091fidVendor')
            ->where('p.f091fidVendor = :billId')
            ->setParameter('billId',$vendoraId);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f091fcreatedDate'];
            $item['f091fcreatedDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
            
       return $result1;
    }

    public function updateBillMaster($billObj, $data)
    {

         $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $billObj->setF091fcreatedDate(new \DateTime())
            ->setF091ftotalAmount((float)$data['f091ftotalAmount'])
            ->setF091fapprovedStatus((int)0)
            ->setF091ftype($data['f091ftype'])
            ->setF091fcontactPerson1($data['f091fcontactPerson1'])
            ->setF091fcontactPerson2($data['f091fcontactPerson2'])
            ->setF091fcontactPerson3($data['f091fcontactPerson3'])
            // ->setF091freason($data['f091freason'])
            ->setF091fpaymentStatus((int)0)
            ->setF091fidVendor((int)$data['f091fidVendor'])
            ->setF091fbankAcNo($data['f091fbankAcNo'])
            ->setF091femail($data['f091femail'])
            ->setF091voucherType((int)$data['f091fvoucherType'])
            ->setF091fuumBank((int)$data['f091fuumBank'])
            ->setF091finvoiceDate(new \DateTime($data['f091finvoiceDate']))
            ->setF091fdescription($data['f091fdescription'])
            ->setF091fstatus((int)1)
            ->setF091fupdatedBy((int)$_SESSION['userId']);

            $billObj->setF091fupdatedDtTm(new \DateTime());
        
            $em->persist($billObj);
            $em->flush();
    }


    public function updateBillDetail($detailObj, $detail)
    {
         $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $detailObj->setF091freferenceNumber($detail['f091freferenceNumber'])
                ->setF091fidItem((int) $detail['f091fidItem'])
                ->setF091ftype((int)$detail['f091ftype'])
                ->setF091fsoCode($detail['f091fsoCode'])
                ->setF091fquantity((int)$detail['f091fquantity'])
                ->setF091funit($detail['f091funit'])
                ->setF091fprice((float)$detail['f091fprice'])
                ->setF091total((float)$detail['f091total'])
                ->setF091fpercentage((int) $detail['f091fpercentage'])
                ->setF091ftaxCode((int)$detail['f091ftaxCode'])
                ->setF091fstatus((int)$detail['f091fstatus'])
                ->setF091ftaxAmount((float) $detail['f091ftaxAmount'])
                ->setF091ftotalIncTax((float) $detail['f091ftotalIncTax'])
                ->setF091ffundCode($detail['f091ffundCode'])
                ->setF091fdepartmentCode($detail['f091fdepartmentCode'])
                ->setF091factivityCode($detail['f091factivityCode'])
                ->setF091faccountCode($detail['f091faccountCode'])
                ->setF091fbudgetFundCode($detail['f091fbudgetFundCode'])
                ->setF091fidGrnDetails((int)$detail['f091fidGrnDetails'])
                ->setF091fbudgetDepartmentCode($detail['f091fbudgetDepartmentCode'])
                ->setF091fbudgetActivityCode($detail['f091fbudgetActivityCode'])
                ->setF091fbudgetAccountCode($detail['f091fbudgetAccountCode'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);
                

            $detailObj->setF091fupdatedDtTm(new \DateTime());
               
            try{    
                    $em->persist($detailObj);
        
                    $em->flush();

                }
                catch(\Exception $e){
                    echo $e;
                }

    }


    public function createBillDetail($bill, $detail)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $detailObj = new T091fbillRegistrationDetails();


        $detailObj->setF091fidBill((int)$bill->getF091fid())
                ->setF091freferenceNumber($result['f091freferenceNumber'])
                ->setF091fidItem((int) $result['f091fidItem'])
                ->setF091ftype((int)$result['f091ftype'])
                ->setF091fsoCode($result['f091fsoCode'])
                ->setF091fquantity((int)$result['f091fquantity'])
                ->setF091funit($result['f091funit'])
                ->setF091fprice((float)$result['f091fprice'])
                ->setF091total((float)$result['f091total'])
                ->setF091fpercentage((int) $result['f091fpercentage'])
                ->setF091ftaxCode((int)$result['f091ftaxCode'])
                ->setF091fstatus((int)$result['f091fstatus'])
                ->setF091ftaxAmount((float) $result['f091ftaxAmount'])
                ->setF091ftotalIncTax((float) $result['f091ftotalIncTax'])
                ->setF091ffundCode($result['f091ffundCode'])
                ->setF091fdepartmentCode($result['f091fdepartmentCode'])
                ->setF091factivityCode($result['f091factivityCode'])
                ->setF091faccountCode($result['f091faccountCode'])
                ->setF091fbudgetFundCode($result['f091fbudgetFundCode'])
                ->setF091fidGrnDetails((int)$result['f091fidGrnDetails'])
                ->setF091fbudgetDepartmentCode($result['f091fbudgetDepartmentCode'])
                ->setF091fbudgetActivityCode($result['f091fbudgetActivityCode'])
                ->setF091fbudgetAccountCode($result['f091fbudgetAccountCode'])
                ->setF091fcreatedBy((int)$_SESSION['userId']);
                

            $detailObj->setF091fcreatedDtTm(new \DateTime());
               
            try{    
                    $em->persist($detailObj);
        
                    $em->flush();

                }
                catch(\Exception $e){
                    echo $e;
                }

    }
}
