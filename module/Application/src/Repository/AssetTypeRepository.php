<?php
namespace Application\Repository;

use Application\Entity\T090fassetType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetTypeRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f090fid, a.f090fidSubCategory, a.f090fcode,  a.f090fidItemGroup, a.f090fidCategory, a.f090ftypeCode, a.f090ftypeDescription, a.f090fstatus, c.f089fsubcode as assetSubCategory, c.f089fsubDescription, isc.f028fsubCategoryName as SubCategoryName, ac.f087fcategoryCode as CategoryName, ac.f087fcategoryDiscription')
            ->from('Application\Entity\T090fassetType','a')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'a.f090fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'a.f090fidItemGroup = isc.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'a.f090fidCategory = ac.f087fid')
            ->orderBy('a.f090ftypeCode', 'ASC')
            ->orderBy('a.f090fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f090fid, a.f090fidSubCategory, a.f090fcode,  a.f090fidItemGroup, a.f090fidCategory, a.f090ftypeCode, a.f090ftypeDescription, a.f090fstatus, c.f089fsubcode as assetSubCategory, c.f089fsubDescription, isc.f028fsubCategoryName as SubCategoryName, ac.f087fcategoryCode as CategoryName')
            ->from('Application\Entity\T090fassetType','a')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'a.f090fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'a.f090fidItemGroup = isc.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'a.f090fidCategory = ac.f087fid')
             ->where('a.f090fid = :assetTypeId')
            ->setParameter('assetTypeId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $assetType = new T090fassetType();

        $assetType->setF090fidSubCategory((int)$data['f090fidSubCategory'])
             ->setF090fcode($data['f090fcode'])
             ->setF090fidItemGroup((int)$data['f090fidItemGroup'])
             ->setF090fidCategory((int)$data['f090fidCategory'])
             ->setF090ftypeCode($data['f090ftypeCode'])
             ->setF090ftypeDescription($data['f090ftypeDescription'])
             ->setF090fstatus((int)$data['f090fstatus'])
             ->setF090fcreatedBy((int)$_SESSION['userId'])
             ->setF090fupdatedBy((int)$_SESSION['userId']);

        $assetType->setF090fcreatedDtTm(new \DateTime())
                ->setF090fupdatedDtTm(new \DateTime());

 
        $em->persist($assetType);
        $em->flush();
        
        return $assetType;

    }

    public function updateData($assetType, $data = []) 
    {
        $em = $this->getEntityManager();

        $assetType->setF090fidSubCategory((int)$data['f090fidSubCategory'])
             ->setF090fcode($data['f090fcode'])
             ->setF090fidItemGroup((int)$data['f090fidItemGroup'])
             ->setF090fidCategory((int)$data['f090fidCategory'])
             ->setF090ftypeCode($data['f090ftypeCode'])
             ->setF090ftypeDescription($data['f090ftypeDescription'])
             ->setF090fstatus((int)$data['f090fstatus'])
             ->setF090fupdatedBy((int)$_SESSION['userId']);

        $assetType->setF090fupdatedDtTm(new \DateTime());
        
        $em->persist($assetType);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('a.f090fid, a.f090fidSubCategory, a.f090fcode,  a.f090fidItemGroup, a.f090fidCategory, a.f090ftypeCode, a.f090ftypeDescription, a.f090fstatus, c.f089fsubcode as assetSubCategory, c.f089fsubDescription, isc.f028fsubCategoryName as SubCategoryName, ac.f087fcategoryCode as CategoryName')
            ->from('Application\Entity\T090fassetType','a')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'a.f090fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'a.f090fidItemGroup = isc.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'a.f090fidCategory = ac.f087fid')
            ->orderBy('a.f090fid', 'DESC')
            ->where('a.f090fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getAssetTypeByAssetSubCategory($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f090fid, a.f090fidSubCategory, a.f090fcode,  a.f090fidItemGroup, a.f090fidCategory, a.f090ftypeCode, a.f090ftypeDescription, a.f090fstatus, c.f089fsubcode as assetSubCategory, c.f089fsubDescription, isc.f028fsubCategoryName as SubCategoryName, ac.f087fcategoryCode as CategoryName')
            ->from('Application\Entity\T090fassetType','a')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'a.f090fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'a.f090fidItemGroup = isc.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'a.f090fidCategory = ac.f087fid')
             ->where('a.f090fidSubCategory = :assetTypeId')
            ->setParameter('assetTypeId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

}