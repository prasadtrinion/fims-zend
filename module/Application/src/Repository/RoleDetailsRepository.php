<?php
namespace Application\Repository;

use Application\Entity\T022froleDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class RoleDetailsRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f022fid,r.f021fid as f022fidRole,r.f021froleName as f022froleName,m.f011fid as f022fidMenu,m.f011fmenuName as f022fmenuName,d.f022fstatus')
            ->from('Application\Entity\T022froleDetails','d')
            ->leftjoin('Application\Entity\T011fmenu', 'm','with', 'm.f011fid=d.f022fidMenu')
            ->leftjoin('Application\Entity\T021frole', 'r','with', 'r.f021fid=d.f022fidRole');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f022fid,r.f021fid as f022fidRole,r.f021froleName as f022froleName,m.f011fid as f022fidMenu,m.f011fmenuName as f022fmenuName,d.f022fstatus')
            ->from('Application\Entity\T022froleDetails','d')
            ->leftjoin('Application\Entity\T011fmenu', 'm','with', 'm.f011fid=d.f022fidMenu')
            ->leftjoin('Application\Entity\T021frole', 'r','with', 'r.f021fid=d.f022fidRole')
            ->where('d.f022fid = :roleDetailsId')
            ->setParameter('roleDetailsId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function getRoleMenus($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f022fid,r.f021fid as f022fidRole,r.f021froleName as f022froleName,m.f011fid as f022fidMenu,m.f011fmenuName as f022fmenuName,d.f022fstatus')
            ->from('Application\Entity\T022froleDetails','d')
            ->leftjoin('Application\Entity\T011fmenu', 'm','with', 'm.f011fid=d.f022fidMenu')
            ->leftjoin('Application\Entity\T021frole', 'r','with', 'r.f021fid=d.f022fidRole')
            ->where('r.f021fid = :roleId')
            ->setParameter('roleId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $idRole = (int)$data['f022fidRole'];
        try{
        $query1 = "delete from t022frole_details where f022fid_role = $idRole";
        $em->getConnection()->executeQuery($query1);
        }
        catch(\Exception $e){
            echo $e;
        }
        foreach($data['f022fidMenu'] as $idMenu){
        $roleDetails = new T022froleDetails();

        $roleDetails->setF022fidRole((int)$data['f022fidRole'])
             ->setF022fidMenu((int)$idMenu)
             ->setF022fstatus($data['f022fstatus'])
             ->setF022fcreatedBy((int)$_SESSION['userId'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $roleDetails->setF022fcreatedDtTm(new \DateTime())
                ->setF022fupdatedDtTm(new \DateTime());

        try{
        $em->persist($roleDetails);
        $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        }
        return $roleDetails;

    }

    public function updateData($roleDetails, $data = []) 
    {
        $em = $this->getEntityManager();
        $idRole = $data['f022fidRole'];
        $query1 = "delete from t022frole_details where f022id_role = $idRole";
        $em->getConnection()->executeQuery($query1);
        foreach($data['f022fidMenu'] as $idMenu){
            $roleDetails = new T022froleDetails();
    
            $roleDetails->setF022fidRole($data['f022fidRole'])
                 ->setF022fidMenu((int)$idMenu)
                 ->setF022fstatus($data['f022fstatus'])
                 ->setF022fcreatedBy((int)$_SESSION['userId'])
                 ->setF022fupdatedBy((int)$_SESSION['userId']);
    
            $roleDetails->setF022fcreatedDtTm(new \DateTime())
                    ->setF022fupdatedDtTm(new \DateTime());
    
     
            $em->persist($roleDetails);
            $em->flush();
            }
        $roleDetails->setF022fidRole($data['f022fidRole'])
             ->setF022fidMenu($data['f022fidMenu'])
             ->setF022fstatus($data['f022fstatus'])
             ->setF022fcreatedBy((int)$_SESSION['userId'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $roleDetails->setF022fupdatedDtTm(new \DateTime());
        
        $em->persist($roleDetails);
        $em->flush();

    }



    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>