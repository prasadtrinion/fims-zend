<?php

namespace Application\Repository;

use Application\Entity\T091fnonPoInvoiceMaster;
use Application\Entity\T092fnonPoInvoiceDetails;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityRepository;

class NonPoInvoiceRepository extends EntityRepository
{
 	public function getList() 
	{

 		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();

		$qb->select('i.f091fid, i.f091fidInvoice, v.f071finvoiceNumber as invoice,  i.f091fidVendor, i.f091finvoiceDate, i.f091finvoiceType, i.f091freceivedDate, i.f091famount, i.f091fstatus')
		   ->from('Application\Entity\T091fnonPoInvoiceMaster', 'i')
		   ->leftjoin('Application\Entity\T071finvoice', 'v', 'with','i.f091fidInvoice = v.f071fid');
		   // ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'i.f091fidVendor = u.f014fid')
		   

		$query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        
           $qb->select('i.f091fid, i.f091fidInvoice, v.f071finvoiceNumber as invoice,  i.f091fidVendor, i.f091finvoiceDate, i.f091finvoiceType, i.f091freceivedDate, i.f091famount, i.f091fstatus')
		   ->from('Application\Entity\T091fnonPoInvoiceMaster', 'i')
		   ->leftjoin('Application\Entity\T071finvoice', 'v', 'with','i.f091fidInvoice = v.f071fid')
		   // ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'i.f091fidVendor = u.f014fid')
		   	->where('i.f091fid = :NonPoInvoiceId')
           	->setParameter('NonPoInvoiceId',$id);
            
			
		$data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
		
		return $data;
		
	}

	public function getListByStatus($status)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        

           if($status == 0)
           {
            $qb->select('i.f091fid, i.f091fidInvoice, v.f071finvoiceNumber as invoice,  i.f091fidVendor, i.f091finvoiceDate, i.f091finvoiceType, i.f091freceivedDate, i.f091famount, i.f091fstatus, i.f091fstatus as Approval')
		   ->from('Application\Entity\T091fnonPoInvoiceMaster', 'i')
		   ->leftjoin('Application\Entity\T071finvoice', 'v', 'with','i.f091fidInvoice = v.f071fid')
		   // ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'i.f091fidVendor = u.f014fid')
		   	->where('i.f091fstatus = 0');
            }

            elseif($status == 1)
            {
               $qb->select('i.f091fid, i.f091fidInvoice, v.f071finvoiceNumber as invoice,  i.f091fidVendor, i.f091finvoiceDate, i.f091finvoiceType, i.f091freceivedDate, i.f091famount, i.f091fstatus, i.f091fstatus as Approval')
		   ->from('Application\Entity\T091fnonPoInvoiceMaster', 'i')
		   ->leftjoin('Application\Entity\T071finvoice', 'v', 'with','i.f091fidInvoice = v.f071fid')
		   // ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'i.f091fidVendor = u.f014fid')
		   	->where('i.f091fstatus = 1');
            }

            elseif($status == 2) 
            {
               $qb->select('i.f091fid, i.f091fidInvoice, v.f071finvoiceNumber as invoice,  i.f091fidVendor, i.f091finvoiceDate, i.f091finvoiceType, i.f091freceivedDate, i.f091famount, i.f091fstatus, i.f091fstatus as Approval')
		   		  ->from('Application\Entity\T091fnonPoInvoiceMaster', 'i')
		   		  ->leftjoin('Application\Entity\T071finvoice', 'v', 'with','i.f091fidInvoice = v.f071fid');
            }
        	
		$data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
		
		return $data;
		
	}

	public function createNonInvoice($data) 
	{
		$em = $this->getEntityManager();
		// print_r($postdata);
		// die();

			$NonPoInvoice = new T091fnonPoInvoiceMaster();

			$NonPoInvoice->setf091fidInvoice((int)$data['f091fidInvoice'])
					   ->setf091fidVendor((int)'1')
			    	   ->setf091finvoiceDate(new \DateTime($data['f091finvoiceDate']))
			           ->setf091freceivedDate(new \DateTime($data['f091freceivedDate']))
			           ->setf091finvoiceType((int)$data['f091finvoiceType'])
			    	   ->setf091famount((float)$data['f091famount'])
			    	   ->setf091fstatus((int)'0')
			    	   ->setF091fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF091fupdatedBy((int)$_SESSION['userId']);

			$NonPoInvoice->setF091fcreatedDtTm(new \DateTime())
    			       ->setF091fupdatedDtTm(new \DateTime());

		
			$em->persist($NonPoInvoice);

			$em->flush();
			
			$NonPoInvoiceDetails = $data['NonPoInvoice-Details'];

        	foreach ($NonPoInvoiceDetails as $NonPoInvoiceDetail) {

            

			$NonPoInvoiceDetailObj = new T092fnonPoInvoiceDetails();

			$NonPoInvoiceDetailObj->setF092fidMaster((int)$NonPoInvoice)
					   			->setf092fidItem((int)$NonPoInvoiceDetail['f092fidItem'])
					   			->setf092fidGlcode((int)$NonPoInvoiceDetail['f092fidGlcode'])
					   			->setf092fprice((float)$NonPoInvoiceDetail['f092fprice'])
					   			->setf092fquantity((float)$NonPoInvoiceDetail['f092fquantity'])
					   			->setf092fidTax((int)$NonPoInvoiceDetail['f092fidTax'])
					   			->setf092fgstValue((int)$NonPoInvoiceDetail['f092fgstValue'])
					   			->setf092fdiscount((int)$NonPoInvoiceDetail['f092fdiscount'])
					   			->setf092ftotal((int)$NonPoInvoiceDetail['f092ftotal'])
					   			->setf092fstatus((int)$NonPoInvoiceDetail['f092fstatus'])
					   			->setF092fcreatedBy((int)$_SESSION['userId'])
	     			   			->setF092fupdatedBy((int)$_SESSION['userId']);

	     	$NonPoInvoiceDetailObj->setF092fcreatedDtTm(new \DateTime())
    			       ->setF092fupdatedDtTm(new \DateTime());	

    		$em->persist($NonPoInvoiceDetailObj);	 

    		$em->flush();
    	}
		
		return $NonPoInvoice;
	}

	public function updateStatus($nonPoInvoices) 
	{
		
		$em = $this->getEntityManager();

		foreach ($nonPoInvoices as $nonPoInvoice) 
        {

		$nonPoInvoice->setf091fstatus((int)"1")
				 ->setF091fupdatedDtTm(new \DateTime());
       
        $em->persist($nonPoInvoice);
        $em->flush();
    	}
    }
}