<?php
namespace Application\Repository;

use Application\Entity\T081ftextCode;
use Application\Entity\T013fcompany;
use Application\Entity\T059faccountCode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TextCodeRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("t.f081fid, t.f081ftype, t.f081fcode, t.f081fpercentage, t. f081fidAccountCode,a.f059fcompleteCode, a.f059fname,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, t.f081fstatus, t.f081ftaxDescription, (t.f081fcode+'-'+t.f081ftaxDescription) as taxInformation ")
            ->from('Application\Entity\T081ftextCode', 't')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 't.f081fidAccountCode = a.f059fcompleteCode')
            ->orderBy('t.f081fid','DESC')
            ->orderBy('t.f081fcode','ASC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("t.f081fid, t.f081ftype, t.f081fcode, t.f081fpercentage, t. f081fidAccountCode, a.f059fcompleteCode, a.f059fname,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, t.f081fstatus, t.f081ftaxDescription, (t.f081fcode+'-'+t.f081ftaxDescription) as taxInformation ")
            ->from('Application\Entity\T081ftextCode', 't')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 't.f081fidAccountCode = a.f059fcompleteCode')
            ->where('t.f081fid = :textCodeId')
            ->setParameter('textCodeId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $textCode = new T081ftextCode();

        $textCode->setF081ftype((int)$data['f081ftype'])
                ->setF081fcode(($data['f081fcode']))
                ->setF081fpercentage((float)$data['f081fpercentage'])
                ->setF081fidAccountCode($data['f081fidAccountCode'])
                ->setF081ftaxDescription($data['f081ftaxDescription'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081fcreatedBy((int)$_SESSION['userId'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $textCode->setF081fcreatedDtTm(new \DateTime())
                ->setF081fupdatedDtTm(new \DateTime());


        try{
        $em->persist($textCode);
        $em->flush();
        }
        catch(\Exceprion $e){
            echo $e;
        }
        return $textCode;
    }

    public function updateData($textCode, $data = []) 
    {
        $em = $this->getEntityManager();

        $textCode->setF081ftype((int)$data['f081ftype'])
                ->setF081fcode(($data['f081fcode']))
                ->setF081fpercentage((float)$data['f081fpercentage'])
                ->setF081fidAccountCode($data['f081fidAccountCode'])
                ->setF081ftaxDescription($data['f081ftaxDescription'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081fcreatedBy((int)$_SESSION['userId'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $textCode->setF081fupdatedDtTm(new \DateTime());


        
        $em->persist($textCode);
        $em->flush();

    }

     
    public function taxList($data)
    {

      $em = $this->getEntityManager();


      // $taxType = $data['taxType'];
      if($data == '1') {
    
        $query = "select * from t081ftext_code where  f081ftype= '1'"; 
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        

        }
        elseif ($data == '2') {
          
        $query = "select * from t081ftext_code where f081ftype = '2'"; 
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        }
        
        return $result;
    }

   
    public function getActiveList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("t.f081fid, t.f081ftype, t.f081fcode, t.f081fpercentage, t. f081fidAccountCode,a.f059fcompleteCode, a.f059fname,(a.f059fcompleteCode+' - ' +a.f059fname) as originalName, t.f081fstatus, t.f081ftaxDescription, (t.f081fcode+'-'+t.f081ftaxDescription) as taxInformation ")
            ->from('Application\Entity\T081ftextCode', 't')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 't.f081fidAccountCode = a.f059fcompleteCode')
            ->orderBy('t.f081fid','DESC')
            ->orderBy('t.f081fcode','ASC')
            ->where('t.f081fstatus=1');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

}
