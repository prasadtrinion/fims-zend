<?php
namespace Application\Repository;

use Application\Entity\T049fcreditCardTerminal;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CreditCardTerminalRepository extends EntityRepository 
{
    /* to retrive the data from database*/


    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select(" c.f049fid, c.f049fterminalNo, c.f049fidDepartment, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as departmentName, c.f049fbankCardType, c.f049fstatus, b.f042fbankName")
            ->from('Application\Entity\T049fcreditCardTerminal','c')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with', 'c.f049fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T042fbank','b', 'with', 'b.f042fid = c.f049fbank')
            ->orderBy('c.f049fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("c.f049fid, c.f049fterminalNo, c.f049fidDepartment, c.f049fbankCardType, c.f049fstatus, d.f015fdepartmentCode, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as departmentName, b.f042fbankName, c.f049fbank")
            ->from('Application\Entity\T049fcreditCardTerminal','c')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with', 'c.f049fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T042fbank','b', 'with', 'b.f042fid = c.f049fbank')
            ->where('c.f049fid = :creditId')
            ->setParameter('creditId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;

        
 //        try{
 //        $em->persist($result);
 //        echo $result
 //        $em->flush();
 // }
 // catch(\Exception $e){
 //    echo $e;
 // }
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $credit = new T049fcreditCardTerminal();

        $credit->setF049fterminalNo($data['f049fterminalNo'])
             ->setF049fidDepartment($data['f049fidDepartment'])
             ->setF049fbankCardType($data['f049fbankCardType'])
             ->setF049fbank((int)$data['f049fbank'])
             ->setF049fstatus((int)$data['f049fstatus'])
             ->setF049fcreatedBy((int)$_SESSION['userId'])
             ->setF049fupdatedBy((int)$_SESSION['userId']);

        $credit ->setF049fcreatedDtTm(new \DateTime())
                ->setF049fupdatedDtTm(new \DateTime());
               

        $em->persist($credit);
        
        $em->flush();


        return $credit;

    }

    /* to edit the data in database*/

    public function updateData($credit, $data = []) 
    {
        $em = $this->getEntityManager();

        $credit->setF049fterminalNo($data['f049fterminalNo'])
             ->setF049fidDepartment($data['f049fidDepartment'])
             ->setF049fbankCardType($data['f049fbankCardType'])
             ->setF049fbank((int)$data['f049fbank'])
             ->setF049fstatus((int)$data['f049fstatus'])
             ->setF049fupdatedBy((int)$_SESSION['userId']);

        $credit ->setF049fupdatedDtTm(new \DateTime());

        try{
        $em->persist($credit);
        
        $em->flush();
 }
 catch(\Exception $e){
    echo $e;
 }

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */


    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('c.f049fid, c.f049fterminalNo, c.f049fidDepartment, c.f049fbankCardType, c.f049fstatus, c.f049fbank')
            ->from('Application\Entity\T049fcreditCardTerminal','c')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with', 'c.f049fidDepartment = d.f015fid')
            ->where('c.f049fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
}

?>