<?php
namespace Application\Repository;

use Application\Entity\T062fcurrency;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CurrencyRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f062fid,c.f062fcurCode,c.f062fcurDesc, c.f062fcurDescDefaultLang, c.f062fcurSymbolPrefix, c.f062fcurSymbolSuffix, c.f062fcurDefault, c.f062fcurDecimal,c.f062fcurStatus,c.f062fcurCreatedBy,c.f062fcurUpdatedBy')
            ->from('Application\Entity\T062fcurrency','c')
            ->orderBy('c.f062fid','DESC');
            

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f062fid, c.f062fcurCode , c.f062fcurDesc, c.f062fcurDescDefaultLang, c.f062fcurSymbolPrefix, c.f062fcurSymbolSuffix, c.f062fcurDefault, c.f062fcurDecimal,c.f062fcurStatus,c.f062fcurCreatedBy,c.f062fcurUpdatedBy')
            ->from('Application\Entity\T062fcurrency','c')
            ->where('c.f062fid = :currencyId')
            ->setParameter('currencyId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $currency = new T062fcurrency();

        $currency->setF062fcurCode($data['f062fcurCode'])
             ->setF062fcurDesc($data['f062fcurDesc'])
             ->setF062fcurDescDefaultLang($data['f062fcurDescDefaultLang'])
             ->setF062fcurSymbolPrefix($data['f062fcurSymbolPrefix'])
             ->setF062fcurSymbolSuffix($data['f062fcurSymbolSuffix'])
             ->setF062fcurDefault($data['f062fcurDefault'])
             ->setF062fcurDecimal($data['f062fcurDecimal'])
             ->setF062fcurStatus((int)$data['f062fcurStatus'])
             ->setF062fcurCreatedBy((int)$_SESSION['userId'])
             ->setF062fcurUpdatedBy((int)$_SESSION['userId']);

        $currency->setF062fcurCreatedDtTm(new \DateTime())
                ->setF062fcurUpdatedDtTm(new \DateTime());

 
        $em->persist($currency);
        $em->flush();

        return $currency;

    }

    /* to edit the data in database*/

    public function updateData($currency, $data = []) 
    {
        $em = $this->getEntityManager();

        $currency->setF062fcurCode($data['f062fcurCode'])
             ->setF062fcurDesc($data['f062fcurDesc'])
             ->setF062fcurDescDefaultLang($data['f062fcurDescDefaultLang'])
             ->setF062fcurSymbolPrefix($data['f062fcurSymbolPrefix'])
             ->setF062fcurSymbolSuffix($data['f062fcurSymbolSuffix'])
             ->setF062fcurDefault($data['f062fcurDefault'])
             ->setF062fcurDecimal($data['f062fcurDecimal'])
             ->setF062fcurStatus((int)$data['f062fcurStatus'])
             ->setF062fcurCreatedBy((int)$_SESSION['userId'])
             ->setF062fcurUpdatedBy((int)$_SESSION['userId']);

        $currency->setF062fcurUpdatedDtTm(new \DateTime());

        $em->persist($currency);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f062fid,c.f062fcurCode,c.f062fcurDesc, c.f062fcurDescDefaultLang, c.f062fcurSymbolPrefix, c.f062fcurSymbolSuffix, c.f062fcurDefault, c.f062fcurDecimal,c.f062fcurStatus,c.f062fcurCreatedBy,c.f062fcurUpdatedBy')
            ->from('Application\Entity\T062fcurrency','c')
            ->where('c.f062fcurStatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
}

?>