<?php
namespace Application\Repository;

use Application\Entity\T011femployeeSalary;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class EmployeeSalaryRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('s.f011fid,s.f011femployeeId,s.f011fidSalaryComponent,s.f011famount,s.f011fcurrency,s.f011ffinancialPeriod,s.f011ffinancialYear,s.f011fstatus')
            ->from('Application\Entity\T011femployeeSalary','s');
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('s.f011fid,s.f011femployeeId,s.f011fidSalaryComponent,s.f011famount,s.f011fcurrency,s.f011ffinancialPeriod,s.f011ffinancialYear,s.f011fstatus')
            ->from('Application\Entity\T011femployeeSalary','s')
            ->where('s.f011fid = :employeeId')
            ->setParameter('employeeId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $state = new T011femployeeSalary();

        $state->setF012fidCountry((int)$data['f012fidCountry'])
              ->setf012fstateName($data['f012fstateName'])
              ->setf012fshortName($data['f012fshortName'])
              ->setF012fstatus((int)"1")
              ->setF012fupdatedBy((int)$_SESSION['userId']);

        $state->setF012fupdatedDtTm(new \DateTime());


        $em->persist($state);
        $em->flush();

        return $state ;

    }

    public function updateData($state, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $state->setF012fidCountry($data['f012fidCountry'])
              ->setf012fstateName($data['f012fstateName'])
              ->setf012fshortName($data['f012fshortName'])
              ->setF012fstatus((int)$data['f012fstatus'])
              ->setF012fupdatedBy((int)$_SESSION['userId']);

        $state->setF012fupdatedDtTm(new \DateTime());

        try{
        $em->persist($state);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('s.f012fid, s.f012fidCountry,c.f013fcountryName , s.f012fstateName, s.f012fshortName, s.f012fstatus, s.f012fupdatedBy')
            ->from('Application\Entity\T011femployeeSalary','s')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f012fidCountry = c.f013fid')
            ->orderBy('c.f013fcountryName', 'ASC')
            ->where('s.f012fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }


 }
 ?>
