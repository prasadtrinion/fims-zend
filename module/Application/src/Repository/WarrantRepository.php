<?php
namespace Application\Repository;

use Application\Entity\T143fwarrant;
use Application\Entity\T144fwarrantDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class WarrantRepository extends EntityRepository
{

    public function getList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

       $qb->select("w.f143fid, w.f143freferenceNumber, w.f143fidVendor, w.f143fdepartment, w.f143fdescription, w.f143ftotal, w.f143fstatus, (v.f030fvendorCode+'-'+v.f030fcompanyName) as vendor")
            ->from('Application\Entity\T143fwarrant', 'w')
                ->join('Application\Entity\T030fvendorRegistration', 'v', 'with', 'v.f030fid = w.f143fidVendor')
            ->orderby('w.f143fid','DESC');

        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result = array("data"=>$result);
        return $result;
    }  

    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('w.f143fid, w.f143freferenceNumber, w.f143fidVendor, w.f143fdepartment, w.f143fdescription, w.f143ftotal, w.f143fstatus, wd.f144fid, wd.f144fclass, wd.f144fidWarrant, wd.f144ffrom, wd.f144fto, wd.f144fidStaff, wd.f144fdate, wd.f144famount, wd.f144fstatus')
            ->from('Application\Entity\T143fwarrant', 'w')
                ->join('Application\Entity\T144fwarrantDetails', 'wd', 'with', 'w.f143fid = wd.f144fidWarrant')
                ->where('w.f143fid = :id')
                ->setParameter('id', (int)$id);

        try
        {
            $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        $result1 = array();
        foreach ($data as $note) {
            $date                        = $note['f144fdate'];
            $note['f144fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $note);
        }
        $result = array("data"=>$result1);

        return $result;

    }

    public function createNewData($data)
    {
        $em        = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= 'Warrant';
        $number = $configRepository->generateFIMS($numberData);

        $warrant = new T143fwarrant();
                // print_r($number);exit;


            $warrant->setF143freferenceNumber($number)
                ->setF143fidVendor((int)$data['f143fidVendor'])
                ->setF143fdepartment($data['f143fdepartment'])
                ->setF143fdescription($data['f143fdescription'])
                ->setF143ftotal((float)$data['f143ftotal'])
                ->setF143fcreatedDtTm(new \DateTime())
                ->setF143fstatus((int)$data['f143fstatus'])
                ->setF143fcreatedBy((int)$_SESSION['userId']);

           
                $em->persist($warrant);
                $em->flush();
                // print_r($warrant);exit;

                $warrants = $data['details'];

        foreach ($warrants as $details)
        {
                   

                $warrantDetails = new T144fwarrantDetails();
            
        $warrantDetails->setF144fidWarrant((int)$warrant->getF143fid())
                ->setF144fclass($details['f144fclass'])
                ->setF144ffrom($details['f144ffrom'])
                ->setF144fto($details['f144fto'])
                ->setF144fidStaff((int)$details['f144fidStaff'])
                ->setF144fdate(new \DateTime($details['f144fdate']))
                ->setF144famount((float)$details['f144famount'])
                ->setF144fstatus((int)$details['f144fstatus'])
                ->setF144fcreatedDtTm(new \DateTime())     
                ->setF144fcreatedBy((int)$_SESSION['userId']);
            
            try
            {
                $em->persist($warrantDetails);

                $em->flush();
            // print_r($warrantDetails);exit;
                
            }
            catch (\Exception $e)
            {
                echo $e;
            }
        }
        return $warrant;
    }

    public function UpdateMaster($warrant,$data)
    {
        $em      = $this->getEntityManager();
        
       $warrant->setF143fidVendor((int)$data['f143fidVendor'])
                ->setF143fdepartment($data['f143fdepartment'])
                ->setF143fdescription($data['f143fdescription'])
                ->setF143ftotal((float)$data['f143ftotal'])
                ->setF143fcreatedDtTm(new \DateTime())
                ->setF143fstatus((int)$data['f143fstatus'])
                ->setF143fupdatedBy((int)$_SESSION['userId']);
            try {

                $em->persist($warrant);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

        return $warrant;
    }
    public function createDetail($warrant,$details)
    {
        $em      = $this->getEntityManager();
        
       
                $warrantDetails = new T144fwarrantDetails();
          

            $warrantDetails->setF144fidWarrant((int)$warrant->getF143fid())
                ->setF144fclass($details['f144fclass'])
                ->setF144ffrom($details['f144ffrom'])
                ->setF144fto($details['f144fto'])
                ->setF144fidStaff((int)$details['f144fidStaff'])
                ->setF144fdate(new \DateTime($details['f144fdate']))
                ->setF144famount((float)$details['f144famount'])
                ->setF144fstatus((int)$details['f144fstatus'])
                ->setF144fupdatedDtTm(new \DateTime())     
                ->setF144fupdatedBy((int)$_SESSION['userId']);
            try {
                $em->persist($warrantDetails);
                $em->flush();
            } catch (\Exception $e) {
                echo $e;
            }

        return $warrantDetails;
    }
    public function UpdateDetail($warrantDetails,$details)
    {
        $em      = $this->getEntityManager();
        
       $warrantDetails->setF144fclass($details['f144fclass'])
                ->setF144ffrom($details['f144ffrom'])
                ->setF144fto($details['f144fto'])
                ->setF144fidStaff((int)$details['f144fidStaff'])
                ->setF144fdate(new \DateTime($details['f144fdate']))
                ->setF144famount((float)$details['f144famount'])
                ->setF144fstatus((int)$details['f144fstatus'])      
                ->setF144fcreatedDtTm(new \DateTime())
                ->setF144fupdatedBy((int)$_SESSION['userId']);
            try {
                $em->persist($warrantDetails);
                $em->flush();
            } catch (\Exception $e) {
                echo $e;
            }

        return $warrantDetails;
    }

    public function getWarrantActiveList($id)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

       $qb->select("w.f143fid, w.f143freferenceNumber, w.f143fidVendor, w.f143fdepartment, w.f143fdescription, w.f143ftotal, w.f143fstatus, (v.f030fvendorCode+'-'+v.f030fcompanyName) as vendor")
            ->from('Application\Entity\T143fwarrant', 'w')
            ->join('Application\Entity\T030fvendorRegistration', 'v', 'with', 'v.f030fid = w.f143fidVendor')
            ->where('w.f143fstatus = :id')
            ->setParameter('id', (int)$id)
            ->orderby('w.f143fid','DESC');

        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result = array("data"=>$result);

        return $result;
    }


    public function approveWarrant($data)
    {
        $status = (int)$data['status'];
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
             $query = " UPDATE t143fwarrant SET f143fstatus = $status where f143fid = $id";

        $res = $em->getConnection()->executeQuery($query);
        
        }
        return $res;
    
    }

    public function deleteWarrantDetails($data)
    {
        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $em = $this->getEntityManager();

            $query = " DELETE FROM t144fwarrant_details  where f144fid = $id";

            $res = $em->getConnection()->executeQuery($query);
        }

       return $res;
       
    }
}
