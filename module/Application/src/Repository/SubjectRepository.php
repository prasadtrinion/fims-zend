<?php
namespace Application\Repository;

use Application\Entity\T047fsubject;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SubjectRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('s.f047fid, s.f047fdescription, s.f047fcode, s.f047fstatus ')
            ->from('Application\Entity\T047fsubject', 's');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('s.f047fid, s.f047fdescription, s.f047fcode, s.f047fstatus ')
            ->from('Application\Entity\T047fsubject', 's')
            ->where('s.f047fid = :subjectId')
            ->setParameter('subjectId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $subject = new T047fsubject();

        $subject->setF047fdescription($data['f047fdescription'])
                ->setF047fcode($data['f047fcode'])
                ->setF047fstatus((int)$data['f047fstatus'])
                ->setF047fcreatedBy((int)$_SESSION['userId'])
                ->setF047fupdatedBy((int)$_SESSION['userId']);

        $subject->setF047fcreatedDtTm(new \DateTime())
                ->setF047fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($subject);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $subject;
    }

    public function updateData($subject, $data = []) 
    {
        $em = $this->getEntityManager();
    
         $subject->setF047fdescription($data['f047fdescription'])
                ->setF047fcode($data['f047fcode'])
                ->setF047fstatus((int)$data['f047fstatus'])
                ->setF047fcreatedBy((int)$_SESSION['userId'])
                ->setF047fupdatedBy((int)$_SESSION['userId']);

        $subject->setF047fcreatedDtTm(new \DateTime())
                ->setF047fupdatedDtTm(new \DateTime());



        
        $em->persist($subject);
        $em->flush();

    }

    
}