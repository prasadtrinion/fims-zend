<?php
namespace Application\Repository;
 
use Application\Entity\T082freceipt;
use Application\Entity\T083freceiptPaymentInfo;
use Application\Entity\T084freceiptNonInvoice;
use Application\Entity\T084freceiptInvoice;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ReceiptRepository extends EntityRepository
{

    public function getList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query 
        $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus')
            ->from('Application\Entity\T082freceipt', 'r')
            ->orderby('r.f082fid','DESC');
        $query   = $qb->getQuery();
        $results  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach($results as $result)
        {
            $type=$result['f082fpayeeType'];
            switch ($type) {
            case 'ROR':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus,(c.f021ffirstName+' '+c.f021flastName) as fullName,ba.f042fbankName")
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'r.f082fidCustomer = c.f021fid')
                ->leftjoin('Application\Entity\T041fbankSetupAccount', 'b', 'with', 'r.f082fpaymentBank = b.f041fid')
                ->leftjoin('Application\Entity\T042fbank', 'ba', 'with', 'ba.f042fid = b.f041fidBank')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                
                break;
            case 'RSP':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, (s.f063ffirstName +' '+s.f063flastName) as fullName,ba.f042fbankName")
               ->from('Application\Entity\T082freceipt', 'r')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T063fsponsor', 's', 'with', 'r.f082fidCustomer = s.f063fid')
                ->leftjoin('Application\Entity\T041fbankSetupAccount', 'b', 'with', 'r.f082fpaymentBank = b.f041fid')
                ->leftjoin('Application\Entity\T042fbank', 'ba', 'with', 'ba.f042fid = b.f041fidBank')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'RSTD':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, st.f060fname as fullName,ba.f042fbankName')
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T041fbankSetupAccount', 'b', 'with', 'r.f082fpaymentBank = b.f041fid')
                ->leftjoin('Application\Entity\T042fbank', 'ba', 'with', 'ba.f042fid = b.f041fidBank')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'r.f082fidCustomer = st.f060fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'RSTA':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, sm.f034fname as fullName,ba.f042fbankName')
                ->from('Application\Entity\T082freceipt', 'r')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 'r.f082fidCustomer = sm.f034fstaffId')
                ->leftjoin('Application\Entity\T041fbankSetupAccount', 'b', 'with', 'r.f082fpaymentBank = b.f041fid')
                ->leftjoin('Application\Entity\T042fbank', 'ba', 'with', 'ba.f042fid = b.f041fidBank')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            }
            if($credit)
            {    
                array_push($result1, $credit[0]);
            }
        }
       

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getReceipts($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus ')
            ->from('Application\Entity\T082freceipt', 'r');
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'r.f082fidCustomer = c.f021fid');
            $qb->where('r.f082fstatus = :status')
            ->setParameter('status', $id)
            ->orderby('r.f082fid','DESC');
        $query   = $qb->getQuery();
        $results  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
          $result1 = array();

        foreach($results as $result)
        {
            // print_r($result);exit;
            $type=$result['f082fpayeeType'];
            switch ($type)
            {
            case 'ROR':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid, r.f082freceiptNumber, r.f082fidCustomer, r.f082fpayeeType, r.f082fpaymentBank, r.f082fidInvoice, r.f082fdate, r.f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus, (c.f021ffirstName+' '+c.f021flastName) as fullName")
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'r.f082fidCustomer = c.f021fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;

            case 'RSP':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid, r.f082freceiptNumber, r.f082fidCustomer, r.f082fpayeeType, r.f082fpaymentBank, r.f082fidInvoice, r.f082fdate, r.f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus, (s.f063ffirstName+'-'+s.f063flastName) as fullName")
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T063fsponsor', 's', 'with', 'r.f082fidCustomer = s.f063fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;

            case 'RSTD':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid, r.f082freceiptNumber, r.f082fidCustomer, r.f082fpayeeType, r.f082fpaymentBank, r.f082fidInvoice, r.f082fdate, r.f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus, st.f060fname as fullName')
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'r.f082fidCustomer = st.f060fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;

            case 'RSTA':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid, r.f082freceiptNumber, r.f082fidCustomer, r.f082fpayeeType, r.f082fpaymentBank, r.f082fidInvoice, r.f082fdate, r.f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus, sm.f034fname as fullName')
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 'r.f082fidCustomer = sm.f034fstaffId')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);
                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            }
            if($credit)
            {    
                // print_r($credit);exit;
                array_push($result1, $credit[0]);
            }
        }

       

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function vendorReceipts($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus')
            ->from('Application\Entity\T082freceipt', 'r');
            $qb->where('r.f082fidCustomer = :id')
                ->andwhere("r.f082fpayeeType != 'RSTD'")
                ->setParameter('id', $id);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $receipt) {
            $date                        = $receipt['f082fdate'];
            $receipt['f082fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $receipt);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getListById($id)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus')
            ->from('Application\Entity\T082freceipt', 'r')
            ->leftjoin('Application\Entity\T083freceiptPaymentInfo', 'rp', 'with', 'rp.f083fidReceipt = r.f082fid')
            ->leftjoin('Application\Entity\T084freceiptNonInvoice', 'rn', 'with', 'rn.f084fidReceipt = r.f082fid')
            ->leftjoin('Application\Entity\T084freceiptInvoice', 'ri', 'with', 'ri.f084fidReceipt = r.f082fid');
            $qb->where('r.f082fid = :receiptId')
            ->orderby('r.f082fid','DESC')

                ->setParameter('receiptId', (int)$id);
        
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $receipt) {
            $date                        = $receipt['f082fdate'];
            $receipt['f082fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $receipt);
        }
        $result = $result1;
        $result = $result[0];
        
        $receipt_id = (int)$result['f082fid'];
        if($result){
        $qb = $em->createQueryBuilder();

             $qb->select('rp.f083fid,rp.f083fidPayment,rp.f083famount,rp.f083freferenceNumber,rp.f083fidBank,rp.f083fidTerminal,rp.f083fstatus as approvalStatus')
            ->from('Application\Entity\T083freceiptPaymentInfo', 'rp');
            $qb->where('rp.f083fidReceipt = :receiptId')
                ->setParameter('receiptId', (int)$receipt_id);
             $query   = $qb->getQuery();
            $payment_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();

            $qb->select('rn.f084fid,rn.f084fidItem,rn.f084fgstCode,rn.f084fgstValue,rn.f084fdebitFundCode,rn.f084fdebitDepartmentCode,rn.f084fdebitActivityCode,rn.f084fdebitAccountCode,rn.f084fcreditFundCode,rn.f084fcreditDepartmentCode,rn.f084fcreditActivityCode,rn.f084fcreditAccountCode,rn.f084fquantity,rn.f084fprice,rn.f084ftotal,rn.f084fidTaxCode,rn.f084ftotalExc,rn.f084ftaxAmount,rn.f084fstatus as approvalStatus')
            ->from('Application\Entity\T084freceiptNonInvoice', 'rn');
            $qb->where('rn.f084fidReceipt = :receiptId')
                ->setParameter('receiptId', (int)$receipt_id);
             $query   = $qb->getQuery();
            $non_invoice_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

             $qb = $em->createQueryBuilder();

            $qb->select('ri.f084fid,ri.f084fdebitFundCode,ri.f084fdebitDepartmentCode,ri.f084fdebitActivityCode,ri.f084fdebitAccountCode,ri.f084fcreditFundCode,ri.f084fcreditDepartmentCode,ri.f084fcreditActivityCode,ri.f084fcreditAccountCode,ri.f084ftotal,ri.f084fidInvoice,ri.f084fstatus as approvalStatus')
            ->from('Application\Entity\T084freceiptInvoice', 'ri');
            $qb->where('ri.f084fidReceipt = :receiptId')
                ->setParameter('receiptId', (int)$receipt_id);
             $query   = $qb->getQuery();
            $invoice_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result["receipt-payments"] = $payment_result;
            $result["receipt-invoice"] = $invoice_result;
            $result["receipt-non-invoice"] = $non_invoice_result;
        }
        // $result1 = array();
        // foreach ($result as $receipt) {
        //     $date                        = $receipt['f082fdate'];
        //     $receipt['f082fdate'] = date("Y-m-d", strtotime($date));
        //     array_push($result1, $receipt);
        // }
        return $result;
    }

      public function updateReceipt($receipt,$data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $receipt->setF082fpayeeType($data['f082fpayeeType'])
            ->setF082fidInvoice((int)$data['f082fidInvoice'])
            ->setF082fidCustomer((int)$data['f082fidCustomer'])
            ->setF082fpaymentBank((int) $data['f082fpaymentBank'])
            ->setF082fdate(new \DateTime($data['f082fdate']))
            ->setF082ftotalAmount((float)$data['f082ftotalAmount'])
            ->setF082fdescription($data['f082fdescription'])
            ->setF082fstatus((int)"0")
            ->setF082fupdatedBy((int)$_SESSION['userId']);

        $receipt->setF082fupdatedDtTm(new \DateTime());

        try {
            $em->persist($receipt);
            
            $em->flush();
            

        } catch (\Exception $e) {
            echo $e;
        }
        return $receipt;
    }
    public function createNewData($data)
    {


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $receiptAmount = $data['f082ftotalAmount'];
        $invoiceAmount =0 ;
        $receipt = new T082freceipt();
         $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= $data['f082fpayeeType'];
        $number = $configRepository->generateFIMS($numberData);
        $receipt->setF082freceiptNumber($number)
            ->setF082fpayeeType($data['f082fpayeeType'])
            ->setF082fidInvoice((int)$data['f082fidInvoice'])
            ->setF082fidCustomer((int)$data['f082fidCustomer'])
            ->setF082fpaymentBank((int) $data['f082fpaymentBank'])
            ->setF082fdate(new \DateTime($data['f082fdate']))
            ->setF082ftotalAmount((float)$data['f082ftotalAmount'])
            ->setF082fdescription($data['f082fdescription'])
            ->setF082fstatus((int)"0")
            ->setF082fcreatedBy((int)$_SESSION['userId'])
            ->setF082fupdatedBy((int)$_SESSION['userId']);

        $receipt->setF082fcreatedDtTm(new \DateTime())
            ->setF082fupdatedDtTm(new \DateTime());

        try {
            $em->persist($receipt);
            
            $em->flush();
            // print_r($receipt);
            // die();

        } catch (\Exception $e) {
            echo $e;
        }

        $receiptPayments = $data['receipt-payments'];

        foreach ($receiptPayments as $receiptPayment) {
if(is_null($receiptPayment)){
                continue;
            }
            // $receiptAmount = $receiptAmount + $receiptPayment['f083famount'];
            $receiptPaymentObj = new T083freceiptPaymentInfo();
            $receiptPaymentObj->setF083fidPayment($receiptPayment['f083fidPayment'])
                ->setF083famount((float)$receiptPayment['f083famount'])
                ->setF083fidTerminal((int)$receiptPayment['f083fidTerminal'])
                ->setF083freferenceNumber($receiptPayment['f083freferenceNumber'])
                ->setF083fidBank((int)$receiptPayment['f083fidBank'])
                ->setF083fstatus((int)"0")
                ->setF083fcreatedBy((int)$_SESSION['userId'])
                ->setF083fupdatedBy((int)$_SESSION['userId'])
                ->setF083fidReceipt((int)$receipt->getF082fid());

            $receiptPaymentObj->setF083fcreatedDtTm(new \DateTime())
                ->setF083fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptPaymentObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }

        if(isset($data['receipt-non-invoice'])){
        $receiptNonInvoices = $data['receipt-non-invoice'];

        foreach ($receiptNonInvoices as $receiptNonInvoice) {
if(is_null($receiptNonInvoice)){
                continue;
            }
            // $receiptAmount = $receiptAmount + $receiptNonInvoice['f084ftotal'];
            $invoiceAmount = $invoiceAmount + $receiptNonInvoice['f084ftotal'];
            $receiptNonInvoiceObj = new T084freceiptNonInvoice();
            $receiptNonInvoiceObj->setF084fidItem((int) $receiptNonInvoice['f084fidItem'])
            ->setF084fdebitFundCode( $data['f084fdebitFundCode'])
            ->setF084fdebitAccountCode( $data['f084fdebitAccountCode'])
            ->setF084fdebitActivityCode( $data['f084fdebitActivityCode'])
            ->setF084fdebitDepartmentCode( $data['f084fdebitDepartmentCode'])
            ->setF084fcreditFundCode( $receiptNonInvoice['f084fcreditFundCode'])
            ->setF084fcreditAccountCode( $receiptNonInvoice['f084fcreditAccountCode'])
            ->setF084fcreditActivityCode( $receiptNonInvoice['f084fcreditActivityCode'])
            ->setF084fcreditDepartmentCode( $receiptNonInvoice['f084fcreditDepartmentCode'])
            ->setF084fgstCode($receiptNonInvoice['f084fgstCode'])
            ->setF084fgstValue((int)$receiptNonInvoice['f084fgstValue'])
            ->setF084fquantity((int) $receiptNonInvoice['f084fquantity'])
            ->setF084fprice((float)$receiptNonInvoice['f084fprice'])
            ->setF084ftotal((float)$receiptNonInvoice['f084ftotal'])
            ->setF084fidTaxCode((int) $receiptNonInvoice['f084fidTaxCode'])
            ->setF084ftotalExc((float) $receiptNonInvoice['f084ftotalExc'])
            ->setF084ftaxAmount((float) $receiptNonInvoice['f084ftaxAmount'])
            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fcreatedBy((int)$_SESSION['userId'])
                ->setF084fupdatedBy((int)$_SESSION['userId'])
                ->setF084fidReceipt((int)$receipt->getF082fid());

            $receiptNonInvoiceObj->setF084fcreatedDtTm(new \DateTime())
                ->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptNonInvoiceObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }
        }

        $receiptInvoices = $data['receipt-invoice'];

        foreach ($receiptInvoices as $receiptInvoice) {
            if(is_null($receiptInvoice)){
                continue;
            }
            $amount = $receiptInvoice['f084ftotal'];
            $idDetails = $receiptInvoice['f072fid'];
            $idInvoice = $receiptInvoice['f084fidInvoice'];
            $query1 = "update t072finvoice_details set f072fbalance_amount = f072fbalance_amount - $amount where f072fid = $idDetails";
            $em->getConnection()->executeQuery($query1);
            $query1 = "update t071finvoice set f071ftotal_paid = f071ftotal_paid + $amount where f071fid = $idInvoice";
            $em->getConnection()->executeQuery($query1);
        $invoiceAmount = $invoiceAmount + $receiptInvoice['f084ftotal'];

            $receiptInvoiceObj = new T084freceiptInvoice();
            $receiptInvoiceObj->setF084fdebitFundCode( $data['f084fdebitFundCode'])
                            ->setF084fdebitAccountCode( $data['f084fdebitAccountCode'])
                            ->setF084fdebitActivityCode( $data['f084fdebitActivityCode'])
                            ->setF084fdebitDepartmentCode( $data['f084fdebitDepartmentCode'])
                            ->setF084fcreditFundCode( $receiptInvoice['f084fcreditFundCode'])
                            ->setF084fcreditAccountCode( $receiptInvoice['f084fcreditAccountCode'])
                            ->setF084fcreditActivityCode( $receiptInvoice['f084fcreditActivityCode'])
                            ->setF084fcreditDepartmentCode( $receiptInvoice['f084fcreditDepartmentCode'])
                            ->setF084ftotal((float)$receiptInvoice['f084ftotal'])
                            ->setF084fidInvoice((int) $receiptInvoice['f084fidInvoice'])
                            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fcreatedBy((int)$_SESSION['userId'])
                ->setF084fupdatedBy((int)$_SESSION['userId'])
                ->setF084fidReceipt((int)$receipt->getF082fid());

            $receiptInvoiceObj->setF084fcreatedDtTm(new \DateTime())
                ->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptInvoiceObj);
                $em->flush();
            } catch (\Exception $e) {
                echo $e;
            }
        }
        if($invoiceAmount < $receiptAmount){
            $postData = array();
            $postData['f020fidCustomer'] = $data['f082fidCustomer'];
            $postData['f020famount'] =  $receiptAmount - $invoiceAmount;
            if($type == 'RSTD'){
                $type = 'Student';
            }
            else{
                $type = 'Customer';
            }
            $year = date('y');
            $query  = "select * from t020fadvance_payment";
                $result = $em->getConnection()->executeQuery($query)->fetchAll();
                $count  = count($result) + 1;
                $number = "ADV00000" . $count . "/" . $year;
            // $postData['f020ftype'] = $type;
            $postData['f020fdocumentNumber'] = $number;
            $postData['f020fidNumber'] = rand(10,10000);
            $postData['f020fstatus'] = 1;
            $postData['f020fpaymentStatus'] = "NULL";
            $postData['f020fbalanceAmount'] = $receiptAmount - $invoiceAmount ;

            $advanceRepository = $em->getRepository("Application\Entity\T020fadvancePayment");
            $advanceRepository->createNewData($postData);

        }
        return $receipt;
    }

    public function createPayment($receipt,$receiptPayment){
            $em = $this->getEntityManager();

          $receiptPaymentObj = new T083freceiptPaymentInfo();
            $receiptPaymentObj->setF083fidPayment($receiptPayment['f083fidPayment'])
                ->setF083famount((float)$receiptPayment['f083famount'])
                ->setF083fidTerminal((int)$receiptPayment['f083fidTerminal'])
                ->setF083freferenceNumber($receiptPayment['f083freferenceNumber'])
                ->setF083fidBank((int)$receiptPayment['f083fidBank'])
                ->setF083fstatus((int)"0")
                ->setF083fcreatedBy((int)$_SESSION['userId'])
                ->setF083fupdatedBy((int)$_SESSION['userId'])
                ->setF083fidReceipt((int)$receipt->getF082fid());

            $receiptPaymentObj->setF083fcreatedDtTm(new \DateTime())
                ->setF083fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptPaymentObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }

     public function updatePayment($receiptPaymentObj,$receiptPayment){
            $em = $this->getEntityManager();

            $receiptPaymentObj->setF083fidPayment($receiptPayment['f083fidPayment'])
                ->setF083famount((float)$receiptPayment['f083famount'])
                ->setF083fidTerminal((int)$receiptPayment['f083fidTerminal'])
                ->setF083freferenceNumber($receiptPayment['f083freferenceNumber'])
                ->setF083fidBank((int)$receiptPayment['f083fidBank'])
                ->setF083fstatus((int)"0")
                ->setF083fupdatedBy((int)$_SESSION['userId']);

            $receiptPaymentObj->setF083fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptPaymentObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }

    public function createNonInvoice($receipt,$receiptNonInvoice){
        $em = $this->getEntityManager();

        $receiptNonInvoiceObj = new T084freceiptNonInvoice();
            $receiptNonInvoiceObj->setF084fidItem((int) $receiptNonInvoice['f084fidItem'])
            ->setF084fdebitFundCode( $receiptNonInvoice['f084fdebitFundCode'])
            ->setF084fdebitAccountCode( $receiptNonInvoice['f084fdebitAccountCode'])
            ->setF084fdebitActivityCode( $receiptNonInvoice['f084fdebitActivityCode'])
            ->setF084fdebitDepartmentCode( $receiptNonInvoice['f084fdebitDepartmentCode'])
            ->setF084fcreditFundCode( $receiptNonInvoice['f084fcreditFundCode'])
            ->setF084fcreditAccountCode( $receiptNonInvoice['f084fcreditAccountCode'])
            ->setF084fcreditActivityCode( $receiptNonInvoice['f084fcreditActivityCode'])
            ->setF084fcreditDepartmentCode( $receiptNonInvoice['f084fcreditDepartmentCode'])
            ->setF084fgstCode($receiptNonInvoice['f084fgstCode'])
            ->setF084fgstValue((int)$receiptNonInvoice['f084fgstValue'])
            ->setF084fquantity((int) $receiptNonInvoice['f084fquantity'])
            ->setF084fprice((float)$receiptNonInvoice['f084fprice'])
            ->setF084ftotal((float)$receiptNonInvoice['f084ftotal'])
            ->setF084fidTaxCode((int) $receiptNonInvoice['f084fidTaxCode'])
            ->setF084ftotalExc((float) $receiptNonInvoice['f084ftotalExc'])
            ->setF084ftaxAmount((float) $receiptNonInvoice['f084ftaxAmount'])
            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fcreatedBy((int)$_SESSION['userId'])
                ->setF084fupdatedBy((int)$_SESSION['userId'])
                ->setF084fidReceipt((int)$receipt->getF082fid());

            $receiptNonInvoiceObj->setF084fcreatedDtTm(new \DateTime())
                ->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptNonInvoiceObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }
    public function updateNonInvoice($receiptNonInvoiceObj,$receiptNonInvoice){
        $em = $this->getEntityManager();
        
            $receiptNonInvoiceObj->setF084fidItem((int) $receiptNonInvoice['f084fidItem'])
            ->setF084fdebitFundCode( $receiptNonInvoice['f084fdebitFundCode'])
            ->setF084fdebitAccountCode( $receiptNonInvoice['f084fdebitAccountCode'])
            ->setF084fdebitActivityCode( $receiptNonInvoice['f084fdebitActivityCode'])
            ->setF084fdebitDepartmentCode( $receiptNonInvoice['f084fdebitDepartmentCode'])
            ->setF084fcreditFundCode( $receiptNonInvoice['f084fcreditFundCode'])
            ->setF084fcreditAccountCode( $receiptNonInvoice['f084fcreditAccountCode'])
            ->setF084fcreditActivityCode( $receiptNonInvoice['f084fcreditActivityCode'])
            ->setF084fcreditDepartmentCode( $receiptNonInvoice['f084fcreditDepartmentCode'])
            ->setF084fgstCode($receiptNonInvoice['f084fgstCode'])
            ->setF084fgstValue((int)$receiptNonInvoice['f084fgstValue'])
            ->setF084fquantity((int) $receiptNonInvoice['f084fquantity'])
            ->setF084fprice((float)$receiptNonInvoice['f084fprice'])
            ->setF084ftotal((float)$receiptNonInvoice['f084ftotal'])
            ->setF084fidTaxCode((int) $receiptNonInvoice['f084fidTaxCode'])
            ->setF084ftotalExc((float) $receiptNonInvoice['f084ftotalExc'])
            ->setF084ftaxAmount((float) $receiptNonInvoice['f084ftaxAmount'])
            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fupdatedBy((int)$_SESSION['userId']);

            $receiptNonInvoiceObj->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptNonInvoiceObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }
    public function createInvoice($receipt,$receiptInvoice){
        $em = $this->getEntityManager();
 if(is_null($receiptInvoice)){
                return;
            }
            $amount = $receiptInvoice['f084ftotal'];
            $idDetails = $receiptInvoice['f072fid'];
            $idInvoice = $receiptInvoice['f084fidInvoice'];
            $query1 = "update t072finvoice_details set f072fbalance_amount = f072fbalance_amount - $amount where f072fid = $idDetails";
            $em->getConnection()->executeQuery($query1);
            $query1 = "update t071finvoice set f071ftotal_paid = f071ftotal_paid + $amount where f071fid = $idInvoice";
            $em->getConnection()->executeQuery($query1);
        $receiptInvoiceObj = new T084freceiptInvoice();
            $receiptInvoiceObj->setF084fdebitFundCode( $receiptInvoice['f084fdebitFundCode'])
                            ->setF084fdebitAccountCode( $receiptInvoice['f084fdebitAccountCode'])
                            ->setF084fdebitActivityCode( $receiptInvoice['f084fdebitActivityCode'])
                            ->setF084fdebitDepartmentCode( $receiptInvoice['f084fdebitDepartmentCode'])
                            ->setF084fcreditFundCode( $receiptInvoice['f084fcreditFundCode'])
                            ->setF084fcreditAccountCode( $receiptInvoice['f084fcreditAccountCode'])
                            ->setF084fcreditActivityCode( $receiptInvoice['f084fcreditActivityCode'])
                            ->setF084fcreditDepartmentCode( $receiptInvoice['f084fcreditDepartmentCode'])
                            ->setF084ftotal((float)$receiptInvoice['f084ftotal'])
                            ->setF084fidInvoice((int) $receiptInvoice['f084fidInvoice'])
                            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fcreatedBy((int)$_SESSION['userId'])
                ->setF084fupdatedBy((int)$_SESSION['userId'])
                ->setF084fidReceipt((int)$receipt->getF082fid());

            $receiptInvoiceObj->setF084fcreatedDtTm(new \DateTime())
                ->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptInvoiceObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }
    public function updateInvoice($receiptInvoiceObj,$receiptInvoice){
        $em = $this->getEntityManager();
         if(is_null($receiptInvoice)){
                return;
            }
            $amount = $receiptInvoice['f084ftotal'];
            $balance = $receiptInvoice['balance'];
            $idDetails = $receiptInvoice['f072fid'];
            $idInvoice = $receiptInvoice['f084fidInvoice'];
            $orginalBalance = $receiptInvoiceObj->getF084ftotal();
            $query1 = "update t072finvoice_details set f072fbalance_amount = $balance + $orginalBalance - $amount where f072fid = $idDetails";
            $em->getConnection()->executeQuery($query1);
            $query1 = "update t071finvoice set f071ftotal_paid = $balance + $orginalBalance - $amount where f071fid = $idInvoice";
            $em->getConnection()->executeQuery($query1);
            $receiptInvoiceObj->setF084fdebitFundCode( $receiptInvoice['f084fdebitFundCode'])
                            ->setF084fdebitAccountCode( $receiptInvoice['f084fdebitAccountCode'])
                            ->setF084fdebitActivityCode( $receiptInvoice['f084fdebitActivityCode'])
                            ->setF084fdebitDepartmentCode( $receiptInvoice['f084fdebitDepartmentCode'])
                            ->setF084fcreditFundCode( $receiptInvoice['f084fcreditFundCode'])
                            ->setF084fcreditAccountCode( $receiptInvoice['f084fcreditAccountCode'])
                            ->setF084fcreditActivityCode( $receiptInvoice['f084fcreditActivityCode'])
                            ->setF084fcreditDepartmentCode( $receiptInvoice['f084fcreditDepartmentCode'])
                            ->setF084ftotal((float)$receiptInvoice['f084ftotal'])
                            ->setF084fidInvoice((int) $receiptInvoice['f084fidInvoice'])
                            ->setF084fstatus((int) $data['f071fstatus'])
                ->setF084fupdatedBy((int)$_SESSION['userId']);

            $receiptInvoiceObj->setF084fupdatedDtTm(new \DateTime());
            try {
                $em->persist($receiptInvoiceObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
    }
    public function approve($data)
    {
        $em = $this->getEntityManager();
        foreach ($data['id'] as $id)
        {
            // print_r($id);exit;

            $query1 = "update t082freceipt set f082fstatus = 1 where f082fid = $id";
            $em->getConnection()->executeQuery($query1);
            $query2 = "update t083freceipt_payment_info set f083fstatus = 1 where f083fid_receipt = $id";
            $em->getConnection()->executeQuery($query2);
            $query3 = "update t084freceipt_non_invoice set f084fstatus = 1 where f084fid_receipt = $id";
            $em->getConnection()->executeQuery($query3);
            $query4 = "update t084freceipt_invoice set f084fstatus = 1 where f084fid_receipt = $id";
            $em->getConnection()->executeQuery($query4);

            $query4 = "select f084fid_invoice,f084ftotal from t084freceipt_invoice where f084fid_receipt = $id";
            $select = $em->getConnection()->executeQuery($query4);
            $result = $select->fetchAll();
            

            foreach ($result as $value)
            {
                // print_r($value['f084fid_invoice']);exit;
                $idReceipt = $value['f084fid_invoice'];
                $amount = $value['f084ftotal'];
                $query5 = "update t071finvoice set f071fis_paid = 1 where f071fid = $idReceipt and f071finvoice_total = $amount";
                $em->getConnection()->executeQuery($query5);
                $query5 = "update t071finvoice set f071fbalance = f071fbalance - $amount,f071ftotal_paid = f071ftotal_paid + $amount where f071fid = $idReceipt";
                $em->getConnection()->executeQuery($query5);
            }
            
        }
    }
    public function getNonApprovedReceipts()
    { 
 
         $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query 
        $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus')
            ->from('Application\Entity\T082freceipt', 'r')
            ->where('r.f082fstatus = 0')
            ->orderby('r.f082fid','DESC');

        $query   = $qb->getQuery();
        $results  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach($results as $result)
        {
            $type=$result['f082fpayeeType'];
            switch ($type) {
            case 'ROR':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus,(c.f021ffirstName+' '+c.f021flastName) as fullName")
                ->from('Application\Entity\T082freceipt', 'r')
                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'r.f082fidCustomer = c.f021fid')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                
                break;
            case 'RSP':
                $qb = $em->createQueryBuilder();
                $qb->select("r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, (s.f063ffirstName +' '+s.f063flastName) as fullName")
               ->from('Application\Entity\T082freceipt', 'r')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T063fsponsor', 's', 'with', 'r.f082fidCustomer = s.f063fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'RSTD':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, st.f060fname as fullName')
                ->from('Application\Entity\T082freceipt', 'r')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'r.f082fidCustomer = st.f060fid')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'RSTA':
                $qb = $em->createQueryBuilder();
                $qb->select('r.f082fid,r.f082freceiptNumber,r.f082fpayeeType,r.f082fidInvoice,r.f082fidCustomer,r.f082fpaymentBank,r.f082fdate,r.f082ftotalAmount,r.f082fdescription,r.f082fstatus as approvalStatus, sm.f034fname as fullName')
                ->from('Application\Entity\T082freceipt', 'r')
                // ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'r.f082fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 'r.f082fidCustomer = sm.f034fstaffId')
                ->where('r.f082fid = :id')
                ->setParameter('id',(int)$result['f082fid']);

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            }
            if($credit)
            {    
                array_push($result1, $credit[0]);
            }
        }
       

        $result = array(
            'data' => $result1,
        );

        return $result;
    }
    public function approveReceipt($postData)
    {
        $em = $this->getEntityManager();
        foreach ($postData['id'] as $id)
        {
           
            $status = $postData['status'];
            $reason = $postData['reason'];
             // print_r($reason);exit;

            $query1 = "update t082freceipt set f082fstatus = $status, f082freason = '$reason' where f082fid = $id";
            $em->getConnection()->executeQuery($query1);
        }
        

    }

    public function getreceiptStatus($id)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f082fid, r.f082freceiptNumber, r.f082fpayeeType, r.f082fidInvoice, r.f082fidCustomer, r.f082fpaymentBank, r.f082fdate, r.f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus, ri.f084fidInvoice, iv.f071finvoiceNumber')
            ->from('Application\Entity\T082freceipt', 'r')
            ->leftjoin('Application\Entity\T083freceiptPaymentInfo', 'rp', 'with', 'rp.f083fidReceipt = r.f082fid')
            ->leftjoin('Application\Entity\T084freceiptInvoice', 'ri', 'with', 'ri.f084fidReceipt = r.f082fid')
            ->leftjoin('Application\Entity\T071finvoice', 'iv', Expr\Join::LEFT_JOIN, 'ri.f084fidInvoice = iv.f071fid')
            ->where('r.f082fid = :receiptId')
            ->setParameter('receiptId', (int)$id);
        
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        // print_r($result);exit;
        foreach ($result as $receipt) {
            $date                        = $receipt['f082fdate'];
            $receipt['f082fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $receipt);
        }
        $result = $result1;
        $result = $result[0];
        
        $receipt_id = (int)$result['f082fid'];
        if($result){
        $qb = $em->createQueryBuilder();

             $qb->select('rp.f083fid,rp.f083fidPayment,rp.f083famount,rp.f083freferenceNumber,rp.f083fidBank,rp.f083fidTerminal,rp.f083fstatus as approvalStatus')
            ->from('Application\Entity\T083freceiptPaymentInfo', 'rp');
            $qb->where('rp.f083fidReceipt = :receiptId')
            ->setParameter('receiptId', (int)$receipt_id);
             $query   = $qb->getQuery();
            $payment_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();
            $result["receipt-payments"] = $payment_result;

        }

        $receiptAmount = $result['f082ftotalAmount'];
        $paidAmount = $result['receipt-payments'][0]['f083famount'];
        // print_r($receiptAmount);
        // print_r($paidAmount);exit;
        if (isset($result) && isset($result['receipt-payments']))
        {
            
        if ($receiptAmount <= $paidAmount)
        {
           $result['paidStatus'] = 'PAID';
        }
        else
        {
            $result['paidStatus'] = 'PENDING';
        }
    }
        return $result;
    }

    public function receiptByBankDate($data)
    {
        $idBank = (int)$data['idBank'];
        $date = $data['date'];
        $em = $this->getEntityManager();
        $query1 = "SELECT r.f082fid, r.f082freceipt_number as f082freceiptNumber, r.f082fpayee_type as f082fpayeeType, r.f082fid_invoice as f082fidInvoice, r.f082fid_customer as f082fidCustomer, r.f082fpayment_bank as f082fpaymentBank, r.f082fdate, r.f082ftotal_amount as f082ftotalAmount, r.f082fdescription, r.f082fstatus as approvalStatus from t082freceipt r where r.f082fpayment_bank = $idBank and r.f082fdate = '$date'";
        // exit;
        $result = $em->getConnection()->executeQuery($query1)->fetchAll();
        $result = array(
            "data"=> $result);

        return $result;
    }

}
