<?php
namespace Application\Repository;

use Application\Entity\T041fbankSetupAccount;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BankSetupAccountRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('bs.f041fid, bs.f041fidBank, b.f042fbankName as Bank, bs.f041fbankNo, bs.f041ffundCode,bs.f041fdepartmentCode,bs.f041factivityCode,bs.f041faccountCode, bs.f041faddress, bs.f041fcontactPerson, bs.f041fphone, bs.f041femail, bs.f041fdescription, bs.f041faccountNumber, bs.f041fprocessing, bs.f041fstatus, bs.f041fbranch, bs.f041faccountType, d.f011fdefinitionCode as accountType,  bs.f041fcity, bs.f041fstate, s.f012fstateName as stateName, bs.f041fcountry, c.f013fcountryName as countryName')
           ->from('Application\Entity\T041fbankSetupAccount', 'bs')
           ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
           ->leftjoin('Application\Entity\T013fcountry','c','with', 'bs.f041fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', ' bs.f041fstate = s.f012fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'bs.f041faccountType = d.f011fid')
            ->orderby('bs.f041fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('bs.f041fid, bs.f041fidBank, b.f042fbankName as Bank, bs.f041fbankNo, bs.f041ffundCode,bs.f041fdepartmentCode,bs.f041factivityCode,bs.f041faccountCode, bs.f041faddress, bs.f041fcontactPerson, bs.f041fphone, bs.f041femail, bs.f041fdescription, bs.f041faccountNumber, bs.f041fprocessing, bs.f041fstatus, bs.f041fbranch, bs.f041faccountType, d.f011fdefinitionCode as accountType,  bs.f041fcity, bs.f041fstate, s.f012fstateName as stateName, bs.f041fcountry, c.f013fcountryName as countryName')
           ->from('Application\Entity\T041fbankSetupAccount', 'bs')
           ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
           ->leftjoin('Application\Entity\T013fcountry','c','with', 'bs.f041fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', ' bs.f041fstate = s.f012fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'bs.f041faccountType = d.f011fid')
           // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'bs.f041fprocessing = d.f011fid')
            ->where('bs.f041fid = :bankSetupId')
            ->setParameter('bankSetupId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $bank = new T041fbankSetupAccount();

        $bank->setF041fidBank((int)$data['f041fidBank'])
        ->setF041ffundCode( $data['f041ffundCode'])
        ->setF041faccountCode( $data['f041faccountCode'])
        ->setF041factivityCode( $data['f041factivityCode'])
        ->setF041fdepartmentCode( $data['f041fdepartmentCode'])
                     ->setF041faccountNumber((int)$data['f041faccountNumber'])
             ->setF041faddress($data['f041faddress'])
             ->setF041fcontactPerson($data['f041fcontactPerson'])
             ->setF041fphone((int)$data['f041fphone'])
             ->setF041femail($data['f041femail'])
             ->setF041fdescription($data['f041fdescription'])
             ->setF041fprocessing((int)$data['f041fprocessing'])
             ->setF041fstatus((int)$data['f041fstatus'])
             ->setF041fbankNo($data['f041fbankNo'])
             ->setF041fbranch($data['f041fbranch'])
             ->setF041faccountType((int)$data['f041faccountType'])
             ->setF041fcity($data['f041fcity'])
             ->setF041fstate((int)$data['f041fstate'])
             ->setF041fcountry((int)$data['f041fcountry'])
             ->setF041fcreatedBy((int)$_SESSION['userId'])
             ->setF041fupdatedBy((int)$_SESSION['userId']);

        $bank->setF041fcreatedDtTm(new \DateTime())
                ->setF041fupdatedDtTm(new \DateTime());

        try{
$em->persist($bank);
        
        $em->flush();
    // print_r($bank);
    // die();
    }
    catch(\Exception $e){
        echo $e;
        
    }
return $bank;
    }

    /* to edit the data in database*/

    public function updateData($bank, $data = []) 
    {
        $em = $this->getEntityManager();

        $bank->setF041fidBank((int)$data['f041fidBank'])
        ->setF041ffundCode( $data['f041ffundCode'])
        ->setF041faccountCode( $data['f041faccountCode'])
        ->setF041factivityCode( $data['f041factivityCode'])
        ->setF041fdepartmentCode( $data['f041fdepartmentCode'])
             ->setF041faccountNumber((int)$data['f041faccountNumber'])
             ->setF041faddress($data['f041faddress'])
             ->setF041fcontactPerson($data['f041fcontactPerson'])
             ->setF041fphone((int)$data['f041fphone'])
             ->setF041femail($data['f041femail'])
             ->setF041fdescription($data['f041fdescription'])
             ->setF041fbankNo($data['f041fbankNo'])
             ->setF041fprocessing((int)$data['f041fprocessing'])
             ->setF041fstatus((int)$data['f041fstatus'])
             ->setF041fbranch($data['f041fbranch'])
             ->setF041faccountType((int)$data['f041faccountType'])
             ->setF041fcity($data['f041fcity'])
             ->setF041fstate((int)$data['f041fstate'])
             ->setF041fcountry((int)$data['f041fcountry'])
             ->setF041fupdatedBy((int)$_SESSION['userId']);
                

        $bank->setF041fupdatedDtTm(new \DateTime());
        
        $em->persist($bank);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('bs.f041fid, bs.f041fidBank, b.f042fbankName as Bank, bs.f041fbankNo, bs.f041ffundCode,bs.f041fdepartmentCode,bs.f041factivityCode,bs.f041faccountCode, bs.f041faddress, bs.f041fcontactPerson, bs.f041fphone, bs.f041femail, bs.f041fdescription,  bs.f041faccountNumber, bs.f041fprocessing, bs.f041fstatus, bs.f041fbranch, bs.f041faccountType, d.f011fdefinitionCode as accountType,  bs.f041fcity, bs.f041fstate, s.f012fstateName as stateName, bs.f041fcountry, c.f013fcountryName as countryName')
           ->from('Application\Entity\T041fbankSetupAccount', 'bs')
           ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
           ->leftjoin('Application\Entity\T013fcountry','c','with', 'bs.f041fcountry = c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with', ' bs.f041fstate = s.f012fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'bs.f041faccountType = d.f011fid')
            ->where('bs.f041fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
}

?>