<?php
namespace Application\Repository;

use Application\Entity\T019fyearlyProcess;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class YearlyProcessRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('m.f019fid, m.f019fdate, m.f019fyear, m.f019fstatus, m.f019fcreatedBy, m.f019fupdatedBy')
            ->from('Application\Entity\T019fyearlyProcess','m');

        $query = $qb->getQuery();
        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f019fdate'];
            $item['f019fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('m.f019fid, m.f019fdate, m.f019fyear, m.f019fstatus, m.f019fcreatedBy, m.f019fupdatedBy')
            ->from('Application\Entity\T019fyearlyProcess','m')
            ->where('m.f019fid = :yearId')
            ->setParameter('yearId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f019fdate'];
            $item['f019fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $year = new T019fyearlyProcess();

        $year->setF019fdate(new \DateTime($data['f019fdate']))
             ->setF019fyear((int)$data['f019fyear'])
             ->setF019fstatus((int)$data['f019fstatus'])
             ->setF019fcreatedBy((int)$_SESSION['userId'])
             ->setF019fupdatedBy((int)$_SESSION['userId']);

        $year->setF019fcreatedDtTm(new \DateTime())
                ->setF019fupdatedDtTm(new \DateTime());

 
        $em->persist($year);
        $em->flush();

        return $year;

    }

    /* to edit the data in database*/

    public function updateData($year, $data = []) 
    {
        $em = $this->getEntityManager();

        $year->setF019fdate(new \DateTime($data['f019fdate']))
             ->setF019fyear((int)$data['f019fyear'])
             ->setF019fstatus((int)$data['f019fstatus'])
             ->setF019fcreatedBy((int)$_SESSION['userId'])
             ->setF019fupdatedBy((int)$_SESSION['userId']);

                

        $year->setF019fupdatedDtTm(new \DateTime());
        
        $em->persist($year);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>