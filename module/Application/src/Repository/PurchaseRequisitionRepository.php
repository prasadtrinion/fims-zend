<?php
namespace Application\Repository;

use Application\Entity\T088fpurchaseRequisition;
use Application\Entity\T088fpurchaseReqDetails;
use Application\Entity\T015fdepartment;
use Application\Entity\T015ffinancialyear;
use Application\Entity\T014fglcode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PurchaseRequisitionRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f088fid, p.f088freferenceNumber, p.f088forderType, p.f088fdate, p.f088fidFinancialyear, f.f015fname as budgetYear, p.f088fidSupplier, v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f088fdescription, p.f088fidDepartment, d.f015fdepartmentName as costCenter, p.f088fstatus, p.f088fapprovalStatus, p.f088ftotalAmount, p.f088fupdatedBy
            ,u.f014fuserName as userName, p.f088fcreatedDtTm")
        ->from('Application\Entity\T088fpurchaseRequisition', 'p')
        ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
        ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f088fidFinancialyear = f.f015fid')
        ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f088fidSupplier = v.f030fid')
        ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f088fupdatedBy = u.f014fid')
        ->orderBy('p.f088fid','DESC');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f088fdate'];
            $item['f088fdate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f088fcreatedDtTm'];
            $item['f088fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;

    }

    

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("p.f088fid, p.f088freferenceNumber, p.f088forderType, p.f088fdate, p.f088fidFinancialyear, f.f015fname as budgetYear, p.f088fidSupplier, v.f030fcompanyName , v.f030fvendorCode ,(v.f030fcompanyName+'-'+v.f030fvendorCode) as supplier, p.f088fdescription, p.f088fidDepartment, d.f015fdepartmentName as costCenter, p.f088fstatus, p.f088ftotalAmount, pd.f088fidDetails, pd.f088fidItem,pd.f088fitemType, pd.f088fsoCode, pd.f088frequiredDate, pd.f088fquantity, pd.f088funit, pd.f088fprice, pd. f088total, pd.f088ftaxCode, pd.f088fpercentage, pd.f088ftaxAmount, pd.f088ftotalIncTax, pd.f088ffundCode, pd.f088fdepartmentCode, pd.f088factivityCode, pd.f088faccountCode, pd.f088fbudgetFundCode, pd.f088fbudgetDepartmentCode, pd.f088fbudgetActivityCode, pd.f088fbudgetAccountCode, pd.f088fidPurchase, p.f088fupdatedBy
            , u.f014fuserName as userName, p.f088fcreatedDtTm, p.f088fapprovalStatus, pd.f088fitemCategory, pd.f088fitemSubCategory, pd.f088fitemCode, pd.f088fcategoryType, pd.f088fassetCode ")
        ->from('Application\Entity\T088fpurchaseRequisition', 'p')
        ->leftjoin('Application\Entity\T088fpurchaseReqDetails', 'pd', 'with', 'p.f088fid = pd.f088fidPurchase')
        ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
        ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f088fidFinancialyear = f.f015fid')
        ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f088fidSupplier = v.f030fid')
        ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f088fupdatedBy = u.f014fid')
        ->where('p.f088fid = :purchaseId')
        ->setParameter('purchaseId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f088fdate'];
            $item['f088fdate'] = date("Y-m-d", strtotime($date));
            $item['f088fidItem'] = $item['f088fidItem'].$item['f088fitemType'];
            $date                        = $item['f088frequiredDate'];
            $item['f088frequiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f088fcreatedDtTm'];
            $item['f088fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }

        // print_r($result1);exit;
        $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

            $categoryType = $purchase['f088fcategoryType'];
            $category = (int)$purchase['f088fitemCategory'];
            $subCategory = (int)$purchase['f088fitemSubCategory'];
            $item = (int)$purchase['f088fidItem'];

            $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
            $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

            $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
            $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

            $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
            $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();



             $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
             $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;

             $result1[$i]['itemName'] = $itemResult['assetItem'];



         }
         elseif($categoryType == 'P')
         {
                // $em = $this->getEntityManager();

        // print_r("hhh");exit;


            $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
            $itemResult=$em->getConnection()->executeQuery($query)->fetch();



            $result1[$i]['itemName'] = $itemResult['procurementItem'];

        }
        $i++;

    }

    return $result1;
}

public function createPurchase($data)
{
        // print_r($data);exit;

    $em = $this->getEntityManager();
    $qb = $em->createQueryBuilder();
    if(isset($data['financialyear']))
    {
        $financialYear = $data['financialyear'];
        $query = "select f015fid from t015ffinancialyear where f015fname = '$financialYear'"; 

        $financialyear_result=$em->getConnection()->executeQuery($query)->fetch();
        $data['f088fidFinancialyear'] = $financialyear_result['f015fid'];
        
    }
        // print_r($data);
        // die();
    $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
    $numberData = array();
    $numberData['type']="PR";
    $number = $configRepository->generateFIMS($numberData);

    $purchase = new T088fpurchaseRequisition();

    $purchase->setF088forderType($data['f088forderType'])
    ->setF088fdate(new \DateTime($data['f088fdate']))
    ->setF088fidFinancialyear((int)$data['f088fidFinancialyear'])
    ->setF088freferenceNumber($number)
    ->setF088fidDepartment($data['f088fidDepartment'])
    ->setF088fidSupplier((int)$data['f088fidSupplier'])
    ->setF088fdescription($data['f088fdescription'])
    ->setF088ftotalAmount((float)$data['f088ftotalAmount'])
    ->setF088fstatus((int)$data['f088fstatus'])
    ->setF088fapprovalStatus((int)0)
    ->setF088fcreatedBy((int)$_SESSION['userId'])
    ->setF088fupdatedBy((int)$_SESSION['userId']);

    $purchase->setF088fcreatedDtTm(new \DateTime())
    ->setF088fupdatedDtTm(new \DateTime());

    try{
        $em->persist($purchase);
            // print_r($purchase);
            // die();
        $em->flush();

    }
    catch(\Exception $e){
        echo $e;
    }


    $purchaseDetails = $data['purchase-details'];

    foreach ($purchaseDetails as $purchaseDetail) {

        $purchaseDetailObj = new T088fpurchaseReqDetails();

        $purchaseDetailObj->setF088fidPurchase((int)$purchase->getF088fid())
        ->setF088fidItem((int)$purchaseDetail['f088fidItem'])
        ->setF088fitemType($purchaseDetail['f088fitemType'])
        ->setF088fsoCode($purchaseDetail['f088fsoCode'])
        ->setF088frequiredDate(new \DateTime($purchaseDetail['f088frequiredDate']))
        ->setF088fquantity((int)$purchaseDetail['f088fquantity'])
        ->setF088funit($purchaseDetail['f088funit'])
        ->setF088fprice((float)$purchaseDetail['f088fprice'])
        ->setF088total((float)$purchaseDetail['f088total'])
        ->setF088fpercentage((int) $purchaseDetail['f088fpercentage'])
        ->setF088ftaxAmount((float) $purchaseDetail['f088ftaxAmount'])
        ->setF088ftotalIncTax((float) $purchaseDetail['f088ftotalIncTax'])
        ->setF088ftaxCode((int)$purchaseDetail['f088ftaxCode'])
        ->setF088fstatus((int)$purchaseDetail['f088fstatus'])
        ->setF088ffundCode($purchaseDetail['f088ffundCode'])
        ->setF088fdepartmentCode($purchaseDetail['f088fdepartmentCode'])
        ->setF088factivityCode($purchaseDetail['f088factivityCode'])
        ->setF088faccountCode($purchaseDetail['f088faccountCode'])
        ->setF088fbudgetFundCode($purchaseDetail['f088fbudgetFundCode'])
        ->setF088fbudgetDepartmentCode($purchaseDetail['f088fbudgetDepartmentCode'])
        ->setF088fbudgetActivityCode($purchaseDetail['f088fbudgetActivityCode'])
        ->setF088fbudgetAccountCode($purchaseDetail['f088fbudgetAccountCode'])
        ->setF088fitemCategory($purchaseDetail['f088fitemCategory'])
        ->setF088fitemSubCategory($purchaseDetail['f088fitemSubCategory'])
        ->setF088fcategoryType($purchaseDetail['f088fcategoryType'])
        ->setF088fitemCode($purchaseDetail['f088fitemCode'])
        ->setF088fassetCode($purchaseDetail['f088fassetCode'])
        ->setF088fcreatedBy((int)$_SESSION['userId'])
        ->setF088fupdatedBy((int)$_SESSION['userId']);


        $purchaseDetailObj->setF088fcreatedDtTm(new \DateTime())
        ->setF088fupdatedDtTm(new \DateTime());
        try{
            $em->persist($purchaseDetailObj);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }

    }
    return $purchase;
}

public function updatePurchase($purchase, $data = [])
{
    $em = $this->getEntityManager();

    $purchase->setF088forderType($data['f088forderType'])
    ->setF088fdate(new \DateTime($data['f088fdate']))
    ->setF088fidFinancialyear((int)$data['f088fidFinancialyear'])
        // ->setF088freferenceNumber($data['f088freferenceNumber'])
    ->setF088fidDepartment($data['f088fidDepartment'])
    ->setF088fidSupplier((int)$data['f088fidSupplier'])
    ->setF088fdescription($data['f088fdescription'])
    ->setF088ftotalAmount((float)$data['f088ftotalAmount'])
    ->setF088fstatus((int)$data['f088fstatus'])
    ->setF088fupdatedBy((int)$_SESSION['userId']);

    $purchase->setF088fupdatedDtTm(new \DateTime());

    try {
        $em->persist($purchase);

        $em->flush();
    } catch (\Exception $e) {
        echo $e;
    }
    return $purchase;
}


public function updatePurchaseDetail($purchaseDetailObj, $purchaseDetail)
{

    $em = $this->getEntityManager();

    $purchaseDetailObj->setF088fidItem((int) substr($purchaseDetail['f088fidItem'], 0,-1))
    ->setF088fitemType( substr($purchaseDetail['f088fidItem'],-1))
    ->setF088fsoCode($purchaseDetail['f088fsoCode'])
    ->setF088frequiredDate(new \DateTime($purchaseDetail['f088frequiredDate']))
    ->setF088fquantity((int)$purchaseDetail['f088fquantity'])
    ->setF088funit($purchaseDetail['f088funit'])
    ->setF088fprice((float)$purchaseDetail['f088fprice'])
    ->setF088total((float)$purchaseDetail['f088total'])
    ->setF088fpercentage((int) $purchaseDetail['f088fpercentage'])
    ->setF088ftaxAmount((float) $purchaseDetail['f088ftaxAmount'])
    ->setF088ftotalIncTax((float) $purchaseDetail['f088ftotalIncTax'])
    ->setF088ftaxCode((int)$purchaseDetail['f088ftaxCode'])
    ->setF088fstatus((int)$purchaseDetail['f088fstatus'])
    ->setF088ffundCode($purchaseDetail['f088ffundCode'])
    ->setF088fdepartmentCode($purchaseDetail['f088fdepartmentCode'])
    ->setF088factivityCode($purchaseDetail['f088factivityCode'])
    ->setF088faccountCode($purchaseDetail['f088faccountCode'])
    ->setF088fbudgetFundCode($purchaseDetail['f088fbudgetFundCode'])
    ->setF088fbudgetDepartmentCode($purchaseDetail['f088fbudgetDepartmentCode'])
    ->setF088fbudgetActivityCode($purchaseDetail['f088fbudgetActivityCode'])
    ->setF088fbudgetAccountCode($purchaseDetail['f088fbudgetAccountCode'])
    ->setF088fitemCategory($purchaseDetail['f088fitemCategory'])
    ->setF088fitemSubCategory($purchaseDetail['f088fitemSubCategory'])
    ->setF088fitemCode($purchaseDetail['f088fitemCode'])
    ->setF088fcategoryType($purchaseDetail['f088fcategoryType'])
    ->setF088fassetCode($purchaseDetail['f088fassetCode'])
    ->setF088fupdatedBy((int)$_SESSION['userId']);


    $purchaseDetailObj->setF088fupdatedDtTm(new \DateTime());
    try {
        $em->persist($purchaseDetailObj);
        $em->flush();
    } catch (\Exception $e) {
        echo $e;
    }
}

public function createPurchaseDetail($purchaseObj, $purchaseDetail)
{
    $em = $this->getEntityManager();

    $purchaseDetailObj = new T088fpurchaseReqDetails();

    $purchaseDetailObj->setF088fidPurchase((int)$purchaseObj->getF088fid())
    ->setF088fidItem((int) substr($purchaseDetail['f088fidItem'], 0,-1))
    ->setF088fitemType( substr($purchaseDetail['f088fidItem'],-1))
    ->setF088fsoCode($purchaseDetail['f088fsoCode'])
    ->setF088frequiredDate(new \DateTime($purchaseDetail['f088frequiredDate']))
    ->setF088fquantity((int)$purchaseDetail['f088fquantity'])
    ->setF088funit($purchaseDetail['f088funit'])
    ->setF088fprice((float)$purchaseDetail['f088fprice'])
    ->setF088total((float)$purchaseDetail['f088total'])
    ->setF088fpercentage((int) $purchaseDetail['f088fpercentage'])
    ->setF088ftaxAmount((float) $purchaseDetail['f088ftaxAmount'])
    ->setF088ftotalIncTax((float) $purchaseDetail['f088ftotalIncTax'])
    ->setF088ftaxCode((int)$purchaseDetail['f088ftaxCode'])
    ->setF088fstatus((int)$purchaseDetail['f088fstatus'])
    ->setF088ffundCode($purchaseDetail['f088ffundCode'])
    ->setF088fdepartmentCode($purchaseDetail['f088fdepartmentCode'])
    ->setF088factivityCode($purchaseDetail['f088factivityCode'])
    ->setF088faccountCode($purchaseDetail['f088faccountCode'])
    ->setF088fbudgetFundCode($purchaseDetail['f088fbudgetFundCode'])
    ->setF088fbudgetDepartmentCode($purchaseDetail['f088fbudgetDepartmentCode'])
    ->setF088fbudgetActivityCode($purchaseDetail['f088fbudgetActivityCode'])
    ->setF088fbudgetAccountCode($purchaseDetail['f088fbudgetAccountCode'])
    ->setF088fitemCategory($purchaseDetail['f088fitemCategory'])
    ->setF088fitemSubCategory($purchaseDetail['f088fitemSubCategory'])
    ->setF088fitemCode($purchaseDetail['f088fitemCode'])
    ->setF088fcategoryType($purchaseDetail['f088fcategoryType'])
    ->setF088fassetCode($purchaseDetail['f088fassetCode'])
    ->setF088fupdatedBy((int)$_SESSION['userId']);


    $purchaseDetailObj->setF088fcreatedDtTm(new \DateTime())
    ->setF088fupdatedDtTm(new \DateTime());
    try
    {
        $em->persist($purchaseDetailObj);
        $em->flush();
    } 
    catch (\Exception $e) 
    {
        echo $e;
    }
}


    // public function purchaseApproval($purchases) 
    // {
    //     $em = $this->getEntityManager();

    //     foreach ($purchases as $purchase) 
    //     {

    //         $purchase->setF088fapprovalStatus((int)"1")
    //              ->setF088fUpdatedDtTm(new \DateTime());

    //         $em->persist($purchase);
    //         $em->flush();

    //         $em = $this->getEntityManager();

    //         $qb = $em->createQueryBuilder();

    //         $Id=$purchase->getF088fid();
    //         $id=(int)$Id;

    //         $qb->select(' p.f088ftotalAmount,p.f088fidDepartment,p.f088fidFinancialyear')
    //         ->from('Application\Entity\T088fpurchaseRequisition', 'p')
    //         ->where('p.f088fapprovalStatus=1')
    //         ->andWhere('p.f088fid = :procurementId')
    //         ->setParameter('procurementId',$id);

    //     $query = $qb->getQuery();

    //     $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    //     $amount = $result[0]['f088ftotalAmount'];
    //     $department = $result[0]['f088fidDepartment'];
    //     $idFinancial= $result[0]['f088fidFinancialyear'];
    //     $financial=(int)$idFinancial;

    //     $query = "UPDATE t108fbudget_summary set f108fbudget_commitment =0 , f108fbudget_expensed='$amount' where f108fdepartment = '$department' and f108fid_financial_year= $financial"; 
    //         $result=$em->getConnection()->executeQuery($query);
    //     }
    // }

public function getPurchaseApprovedList($approvedStatus)
{

    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();


    if($approvedStatus == 0 && isset($approvedStatus)){
      $qb->select("p.f088fid, p.f088freferenceNumber, p.f088forderType, p.f088fdate, p.f088fidFinancialyear, f.f015fname as budgetYear, p.f088fidSupplier, s.f030fcompanyName as supplier, p.f088fdescription, p.f088fidDepartment, d.f015fdepartmentName as costCenter, p.f088fstatus, p.f088fapprovalStatus, p.f088ftotalAmount, p.f088fcreatedDtTm, p.f088fupdatedBy, u.f014fuserName as userName,(s.f030fvendorCode+'-'+s.f030fcompanyName) as Supplier")
      ->from('Application\Entity\T088fpurchaseRequisition', 'p')
      ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
      ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f088fidFinancialyear = f.f015fid')
      ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f088fidSupplier = s.f030fid')
      ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f088fupdatedBy = u.f014fid')
      ->where('p.f088fapprovalStatus = 0');
  }
  elseif($approvedStatus == 1 && isset($approvedStatus)){
   $qb->select("p.f088fid, p.f088freferenceNumber, p.f088forderType,p.f088fdate, p.f088fidFinancialyear, f.f015fname as budgetYear, p.f088fidSupplier, s.f030fcompanyName as supplier, p.f088fdescription, p.f088fidDepartment, d.f015fdepartmentName as costCenter, p.f088fstatus, p.f088fapprovalStatus, p.f088ftotalAmount, p.f088fcreatedDtTm, p.f088fupdatedBy,  u.f014fuserName as userName, (s.f030fvendorCode+'-'+s.f030fcompanyName) as Supplier")
   ->from('Application\Entity\T088fpurchaseRequisition', 'p')
   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
   ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f088fidFinancialyear = f.f015fid')
   ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f088fidSupplier = s.f030fid')
   ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f088fupdatedBy = u.f014fid')
   ->where('p.f088fapprovalStatus = 1');
}
elseif ($approvedStatus == 2 && isset($approvedStatus)) {
   $qb->select("p.f088fid, p.f088freferenceNumber, p.f088forderType, p.f088fdate, p.f088fidFinancialyear, f.f015fname as budgetYear, p.f088fidSupplier, s.f030fcompanyName as supplier, p.f088fdescription, p.f088fidDepartment, d.f015fdepartmentName as costCenter, p.f088fstatus, p.f088fapprovalStatus, p.f088ftotalAmount, p.f088fcreatedDtTm, p.f088fupdatedBy,  u.f014fuserName as userName, (s.f030fvendorCode+'-'+s.f030fcompanyName) as Supplier")
   ->from('Application\Entity\T088fpurchaseRequisition', 'p')
   ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
   ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f088fidFinancialyear = f.f015fid')
   ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f088fidSupplier = s.f030fid')
   ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f088fupdatedBy = u.f014fid');
}




$query = $qb->getQuery();

$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
$result1 = array();
foreach ($result as $item)
{
    $date                        = $item['f088fdate'];
    $item['f088fdate'] = date("Y-m-d", strtotime($date));
    $date                        = $item['f088fcreatedDtTm'];
    $item['f088fcreatedDtTm'] = date("Y-m-d", strtotime($date));
    array_push($result1, $item);
}
$result = array(
    'data' => $result1
);
return $result;


}

public function purchaseApprove($purchases,$reason,$status,$purchaseOrderRepository) 
{
    $em = $this->getEntityManager();
    $user=(int)$_SESSION['userId'];
        // print_r($purchases);
        // die();

    foreach ($purchases as $purchase) 
    {
        $Id = $purchase->getF088fid();
        $id = (int)$Id;
        $balance_status = $this->checkBudget($id);
        if($balance_status == 0){
            return 0;
        }
        $query = "UPDATE t088fpurchase_requisition set f088freason='$reason',f088fapproval_status=$status, f088fapproved_by = $user where f088fid = $id"; 
        $result=$em->getConnection()->executeQuery($query);

        if ($status == 1)
        {
            $query = "select * from t088fpurchase_requisition as pr where pr.f088fid = $id and pr.f088forder_type = 'Direct Purchase' "; 
            $resultPRE=$em->getConnection()->executeQuery($query)->fetch();



                // print_r($resultPRE);exit;

            $resultPRE['f088fdate'] = date("Y-m-d", strtotime($resultPRE['f088fdate']));

                // print_r($resultPRE['f088fdate']);exit;

                // $resultPRE['f088fdate'];

                // $data = array();

            $orderType = $resultPRE['f088forder_type'];
            $date = $resultPRE['f088fdate'];
            $financialYear = $resultPRE['f088fid_financialyear'];
            $idSupplier = $resultPRE['f088fid_supplier'];
            $description = $resultPRE['f088fdescription'];
            $idDepartment= $resultPRE['f088fid_department'];
            $purchaseRequisition = $resultPRE['f088fid'];
                // $expiredDate = $resultPRE['f088forder_type'];
            $totalAmount = $resultPRE['f088ftotal_amount'];
            $status = 1;
            $approvalStatus = 1;

                // print_r($orderType);exit;

            $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
            $numberData = array();
            $numberData['type']= "NONPO";
            $number = $configRepository->generateFIMS($numberData);
                // print_r($number);exit;



            $queryInsertMaster = "INSERT INTO t034fpurchase_order (f034forder_type,f034forder_date,f034fid_financialyear,f034fid_supplier,f034fdescription,f034freference_number,f034fid_department,f034fid_purchasereq,f034fexpired_date,f034fstatus,f034fcreated_by,f034fupdated_by,f034fcreated_dt_tm,f034fupdated_dt_tm,f034fapproval_status,f034ftotal_amount,f034freason) VALUES (
            '$orderType','$date',$financialYear,$idSupplier,'$description','$number','$idDepartment',$purchaseRequisition,'',1,$user,$user,getdate(),getdate(),1,$totalAmount,'')"; 
// print_r($queryInsertMaster);exit;
            $resultPo=$em->getConnection()->executeQuery($queryInsertMaster);

                // print_r($resultPo);exit;
                // $data['purchase-details'] = $resultPRE['f088forder_type'];

            $query = "SELECT top 1 f034fid FROM t034fpurchase_order order by f034fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();

            $idPo = (int)$result['f034fid'];
                // print_r($idPo);exit;


            $query = "select f088fid_item , f088fcategory_type , f088fso_code , f088frequired_date , f088fquantity , f088funit , f088fprice , f088total , f088fpercentage , f088ftax_code , f088forder_type , f088ftax_amount , f088ftotal_inc_tax , f088ffund_code , f088fdepartment_code , f088factivity_code , f088faccount_code , f088fbudget_fund_code , f088fbudget_department_code , f088fbudget_activity_code , f088fbudget_account_code , f088fitem_category , f088fitem_sub_category , f088fasset_code from t088fpurchase_req_details as prd inner join t088fpurchase_requisition as pr on prd.f088fid_purchase = pr.f088fid  where prd.f088fid_purchase = $id "; 

            $resultPRD=$em->getConnection()->executeQuery($query)->fetchAll();


                // print_r($idPo);exit;


                   // $details = array(
                   //          'purchase-details' => $details
                   //      );
                // print_r($data);exit;
                //  $result =$purchaseOrderRepository->createPurchase($data);
                // $details = $data['purchase-details'];

            foreach ($resultPRD as $resultPRED)
            {
                     // print_r($resultPRED);



                $idItemDetail = $resultPRED['f088fid_item'];
                $itemTypeDetail = $resultPRED['f088fcategory_type'];
                $soCodeDetail = $resultPRED['f088fso_code'];
                $requiredDateDetail = $resultPRED['f088frequired_date'];
                $quantityDetail = $resultPRED['f088fquantity'];
                $unitDetail = $resultPRED['f088funit'];
                $priceDetail = $resultPRED['f088fprice'];
                $totalDetail = $resultPRED['f088total'];
                $percentageDetail = $resultPRED['f088fpercentage'];
                $taxCodeDetail = $resultPRED['f088ftax_code'];
                $statusDetail = $resultPRED['f088forder_type'];
                $taxAmountDetail = $resultPRED['f088ftax_amount'];
                $totalIncTaxDetail = $resultPRED['f088ftotal_inc_tax'];
                $fundCodeDetail = $resultPRED['f088ffund_code'];
                $departmentCodeDetail = $resultPRED['f088fdepartment_code'];
                $activityCodeDetail = $resultPRED['f088factivity_code'];
                $accountCodeDetail = $resultPRED['f088faccount_code'];
                $budgetFundCodeDetail = $resultPRED['f088fbudget_fund_code'];
                $budgetDepartmentCode = $resultPRED['f088fbudget_department_code'];
                $budgetActivityCodeDetail = $resultPRED['f088fbudget_activity_code'];
                $budgetAccountCodeDetail = $resultPRED['f088fbudget_account_code'];
                $itemCategoryDetail = $resultPRED['f088fitem_category'];
                $itemSubCategoryDetail = $resultPRED['f088fitem_sub_category'];
                $assetCodeDetail = $resultPRED['f088fasset_code'];
                // print_r("dfg");exit;

                $queryInsertMaster = "INSERT into t034fpurchase_order_details (f034fid_purchase_order,f034fid_item,f034fso_code,f034frequired_date,f034fquantity,f034funit,f034fprice,f034total,f034ftax_code,f034fpercentage,f034ftax_amount,f034ftotal_inc_tax,f034fstatus,f034fcreated_by,f034fupdated_by,f034fcreated_dt_tm,f034fupdated_dt_tm,f034ffund_code,f034fdepartment_code,f034factivity_code,f034faccount_code,f034fbudget_fund_code,f034fbudget_department_code,f034fbudget_activity_code,f034fbudget_account_code,f034fitem_type,f034fitem_category,f034fitem_sub_category,f034fasset_code,f034fbalance_quantity) VALUES 
                ($idPo,$idItemDetail,'$soCodeDetail','$requiredDateDetail',$quantityDetail,'$unitDetail',$priceDetail,$totalDetail,$taxCodeDetail,$percentageDetail,$taxAmountDetail,$totalIncTaxDetail,1,$user,$user,'2019-04-01 00:00:00','2019-04-01 00:00:00','$fundCodeDetail','$departmentCodeDetail','$activityCodeDetail','$accountCodeDetail','$budgetFundCodeDetail','$budgetDepartmentCode','$budgetActivityCodeDetail','$budgetAccountCodeDetail','$itemTypeDetail',$itemCategoryDetail,$itemSubCategoryDetail,'$assetCodeDetail',$quantityDetail)"; 
// print_r($queryInsertMaster);exit;
                // exit;
                $resultPoD = $em->getConnection()->executeQuery($queryInsertMaster);
               // print_r($resultPoD);exit;
                // array_push($data, $result);
            }



        }

        $this->updateBudgetSummary($id);
    }
    return 1;
}

public function checkBudget($id) 
{
    $em = $this->getEntityManager();
    $query = "select f088fbudget_fund_code,f088fbudget_department_code,f088fbudget_activity_code,f088fbudget_account_code,f088fid_financialyear,f088ftotal_amount from t088fpurchase_requisition inner join t088fpurchase_req_details on f088fid_purchase = f088fid where f088fid = $id"; 

    $result1=$em->getConnection()->executeQuery($query)->fetchAll();
            // print_r($result1);
            // die();
    foreach ($result1 as $result) {
        $request = array();
        $request['Department'] = $result['f088fbudget_department_code'];
        $request['FinancialYear'] = $result['f088fid_financialyear'];
        $request['Fund'] = $result['f088fbudget_fund_code'];
        $request['Activity'] = $result['f088fbudget_activity_code'];
        $request['Account'] = $result['f088fbudget_account_code'];
        $request['Amount'] = $result['f088ftotal_amount'];

        $summaryRepository = $em->getRepository("Application\Entity\T108fbudgetSummary");
        $status = $summaryRepository->getBalanceStatus($request);
        // echo $status;
        //  die();
        if($status == 0){
            return 0;
        }
    }
    return 1;
}

public function updateBudgetSummary($id) 
{

    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

        // Query

    $qb->select('pd.f088ftotalIncTax, p.f088fidFinancialyear,pd.f088fbudgetFundCode,pd.f088fbudgetDepartmentCode,pd.f088fbudgetActivityCode,pd.f088fbudgetAccountCode, pd.f088fcategoryType, pd.f088fassetCode')
    ->from('Application\Entity\T088fpurchaseRequisition', 'p')
    ->leftjoin('Application\Entity\T088fpurchaseReqDetails', 'pd', 'with', 'p.f088fid = pd.f088fidPurchase')
            // ->where('p.f088fapprovalStatus=0')
    ->where('p.f088fid = :procurementId')
    ->setParameter('procurementId',(int)$id);

    $query = $qb->getQuery();

    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         // print_r($result);
         // die();
    foreach ($result as $row) {
        $Amount = $row['f088ftotalIncTax'];
        $department = $row['f088fbudgetDepartmentCode'];
        $fund = $row['f088fbudgetFundCode'];
        $activity = $row['f088fbudgetActivityCode'];
        $account = $row['f088fbudgetAccountCode'];
        $idFinancial= $row['f088fidFinancialyear'];
        $year_query = "select f110fid from t110fbudgetyear inner join t015ffinancialyear on f110fyear = f015fname where f015fid = $idFinancial"; 
        $year_result=$em->getConnection()->executeQuery($year_query)->fetch();
        $financial=(int)$year_result['f110fid'];

        $amount = (float)$Amount;
///////////////////////////////////////////////////////////////////////////////////
        $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $financial";
        $result=$em->getConnection()->executeQuery($query)->fetch();

        // print_r($result);
        // exit();
        if(!$result){
            $account=substr($account,0,2)."000"; 
            $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $financial";
            $result=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);
        // exit();
            if(!$result){
                $account=substr($account,0,1)."0000"; 
                $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $financial";
                $result=$em->getConnection()->executeQuery($query)->fetch();
                if(!$result){
                    $status=-1;
                }
                else{

                    $balance=$result['f108fbudget_pending'];

                    if($amount<$balance)
                    {
                        $status=1;
                    }
                    else
                    {
                        $status=0;
                    }
                }
            }
            else{

                $balance=$result['f108fbudget_pending'];

                if($amount<$balance)
                {
                    $status=1;
                }
                else
                {
                    $status=0;
                }
            }
        }
        else{

            $balance=$result['f108fbudget_pending'];

            if($amount<$balance)
            {
                $status=1;
            }
            else
            {
                $status=0;
            }
        }
/////////////////////////////////////////////////////////////////////////
        if($status == 1){
            
            $query = "UPDATE t108fbudget_summary set f108fbudget_commitment = f108fbudget_commitment + $amount  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $financial"; 
            $result=$em->getConnection()->executeQuery($query);
        }
        // echo $query;

    }
// die();
}

public function getDetailsAmount($postData) 
{

    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

        // Query

    $qb->select('pd.f088ftotalIncTax, p.f088fidDepartment, p.f088fidFinancialyear, pd.f088fcategoryType, pd.f088fassetCode')
    ->from('Application\Entity\T088fpurchaseRequisition', 'p')
    ->leftjoin('Application\Entity\T088fpurchaseReqDetails', 'pd', 'with', 'p.f088fid = pd.f088fidPurchase')
    ->where('p.f088fapprovalStatus=0')
    ->andWhere('p.f088fid = :procurementId')
    ->setParameter('procurementId',(int)$id);

    $query = $qb->getQuery();

    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    print_r($result);
    exit();

    $amount = $result[0]['f088ftotalIncTax'];
    $department = $result[0]['f088fidDepartment'];
    $idFinancial= $result[0]['f088fidFinancialyear'];
    $financial=(int)$idFinancial;
    
    $query = "UPDATE t108fbudget_summary set f108fbudget_commitment = '$amount'  where f108fdepartment = '$department' and f108fid_financial_year= $financial"; 
    $result=$em->getConnection()->executeQuery($query);

}

public function getAllPurchaseGlCode($idFinancialyear) 
{
    $em = $this->getEntityManager();
    $qb = $em->createQueryBuilder();

    $qb->select('f.f015fname, b.f110fid')
    ->from('Application\Entity\T015ffinancialyear', 'f')
    ->leftjoin('Application\Entity\T110fbudgetyear', 'b', 'with','f.f015fname = b.f110fyear')
    ->where('f.f015fid = :financialyear')
    ->setParameter('financialyear',$idFinancialyear);

    $query = $qb->getQuery();
    $data = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $id = $data[0]['f110fid'];


    $em = $this->getEntityManager();
    $qb = $em->createQueryBuilder();
    $qb->select("distinct(d.f054ffundCode) as f054ffundCode, f.f057fname, f.f057fcode, (f.f057fcode+' - ' +f.f057fname) as originalName")
    ->from('Application\Entity\T054factivityDetails', 'd')
    ->leftjoin('Application\Entity\T057ffund', 'f', 'with','d.f054ffundCode = f.f057fcode')
    ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','am.f054fid = d.f054fidActivityMaster')

    ->where('am.f054fidFinancialyear = :financialyearId')

    ->setParameter('financialyearId',$id);

    $query = $qb->getQuery();
    $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
        // $result = $result[0];
    $result['Fund'] = $result1; 

    $qb = $em->createQueryBuilder();
    
    $qb->select("distinct(d.f054fdepartmentCode) as f054fdepartmentCode, de.f015fdepartmentName, de.f015fdepartmentCode, (de.f015fdepartmentCode+' - ' +de.f015fdepartmentName) as originalName")
    ->from('Application\Entity\T054factivityDetails', 'd')
    ->leftjoin('Application\Entity\T015fdepartment', 'de', 'with','d.f054fdepartmentCode = de.f015fdepartmentCode')
    ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','am.f054fid = d.f054fidActivityMaster')

    ->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')

    ->setParameter('financialyearId',$id);


    $query = $qb->getQuery();
    $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    $result['Department'] = $result1; 
    
    $qb = $em->createQueryBuilder();
    
    $qb->select(" a.f059fname, a.f059fcompleteCode, (a.f059fcompleteCode+' - ' +a.f059fname) as originalName")
    ->from('Application\Entity\T059faccountCode', 'a');


    $query = $qb->getQuery();
    $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    $result['Account'] = $result1; 
    
    $qb = $em->createQueryBuilder();
    
    $qb->select("distinct(d.f054factivityCode) as f054factivityCode, a.f058fname, a.f058fcompleteCode, (a.f058fcompleteCode+' - ' +a.f058fname) as originalName")
    ->from('Application\Entity\T054factivityDetails', 'd')
    ->leftjoin('Application\Entity\T058factivityCode', 'a', 'with','d.f054factivityCode = a.f058fcompleteCode')
    ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','am.f054fid = d.f054fidActivityMaster')
    ->where('am.f054fidFinancialyear = :financialyearId')
            // ->andWhere('am.f054fapprovalStatus = 1')

    ->setParameter('financialyearId',$id);


    $query = $qb->getQuery();
    $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    $result['Activity'] = $result1; 


    return $result;
}

public function deletePurchaseRequistionDetails($data)
{
    $em = $this->getEntityManager();

    foreach ($data['id'] as $id)
    {
        $query2 = "DELETE from t088fpurchase_req_details WHERE f088fid_details = $id";
        $res = $em->getConnection()->executeQuery($query2);
    }
    return $res;
}

}