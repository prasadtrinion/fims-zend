<?php
namespace Application\Repository;

use Application\Entity\T046fstudentDiscount;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StudentDiscountRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f046fid,s.f046fdiscountType,s.f046ffund,s.f046fdepartment,s.f046factivity,s.f046faccount,s.f046fcustomerType,s.f046fidRevenueCategory,s.f046fidRevenueCode,s.f046fdiscountRateType,s.f046fdiscountRate,s.f046fminimumOutstanding,s.f046foutstandingAs,s.f046fstatus,s.f046fdiscountGenerationType,s.f046fidCustomer,s.f046fdiscountGenerationDate,(c.f073fcode+'-'+c.f073fcategoryName) as revenueCategory,r.f085fcode as revenueCode")
            ->from('Application\Entity\T046fstudentDiscount','s')
            ->leftjoin('Application\Entity\T073fcategory', 'c', 'with','c.f073fid = s.f046fidRevenueCategory')
            ->leftjoin('Application\Entity\T085frevenueSetUp', 'r', 'with','r.f085fid = s.f046fidRevenueCode')
            // ->leftjoin('Application\Entity\T011fdefinationms','d','with','s.f046fdiscountType = d.f011fid')
            // ->leftjoin('Application\Entity\T011fdefinationms','a','with','s.f046fdiscountAmountType = a.f011fid')
            ->orderBy('s.f046fid','DESC');
            
        $query = $qb->getQuery();


        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
       foreach ($result as $item) 
        {
            $date                       = $item['f046fdiscountGenerationDate'];
            $item['f046fdiscountGenerationDate'] = date("Y-m-d", strtotime($date));

            // $date                       = $item['f046foutstandingAs'];
            // $item['f046foutstandingAs'] = date("Y-m-d", strtotime($date));

            
            array_push($result1, $item);
        }   

        return $result1;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select("s.f046fid,s.f046fdiscountType,s.f046ffund,s.f046fdepartment,s.f046factivity,s.f046faccount,s.f046fcustomerType,s.f046fidRevenueCategory,s.f046fidRevenueCode,s.f046fdiscountRateType,s.f046fdiscountRate,s.f046fminimumOutstanding,s.f046foutstandingAs,s.f046fstatus,s.f046fdiscountGenerationType,s.f046fidCustomer,s.f046fdiscountGenerationDate")
            ->from('Application\Entity\T046fstudentDiscount','s')
            ->where('s.f046fid = :discountId')
            ->setParameter('discountId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f046fdiscountGenerationDate'];
            $item['f046fdiscountGenerationDate'] = date("Y-m-d", strtotime($date));

            // $date                       = $item['f046foutstandingAs'];
            // $item['f046foutstandingAs'] = date("Y-m-d", strtotime($date));

            
            array_push($result1, $item);
        }   

        return $result1;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $discount = new T046fstudentDiscount();

            $discount->setF046fdiscountType($data['f046fdiscountType'])
                    ->setF046fcustomerType($data['f046fcustomerType'])
                    ->setF046fidRevenueCategory((int)$data['f046fidRevenueCategory'])
                    ->setF046fidRevenueCode((int)$data['f046fidRevenueCode'])
                    ->setF046fdiscountRateType((int)$data['f046fdiscountRateType'])
                    ->setF046fdiscountRate((float)$data['f046fdiscountRate'])
                    ->setF046fminimumOutstanding((float)$data['f046fminimumOutstanding'])
                    ->setF046foutstandingAs($data['f046foutstandingAs'])
                    ->setF046ffund($data['f046ffund'])
                    ->setF046fdepartment($data['f046fdepartment'])
                    ->setF046factivity($data['f046factivity'])
                    ->setF046faccount($data['f046faccount'])
                    ->setF046fdiscountGenerationDate(new \DateTime($data['f046fdiscountGenerationDate']))
                    ->setF046fdiscountGenerationType($data['f046fdiscountGenerationType'])
                    ->setF046fidCustomer((int)$data['f046fidCustomer'])
                     ->setF046fstatus((int)$data['f046fstatus'])
                     ->setF046fcreatedBy((int)$_SESSION['userId'])
                     ->setF046fupdatedBy((int)$_SESSION['userId']);

        $discount->setF046fcreatedDtTm(new \DateTime())
                ->setF046fupdatedDtTm(new \DateTime());
               
        try {
            $em->persist($discount);
        
            $em->flush();
                   
               } catch (\Exception $e) 
               {
                 echo $e;  
               }       
        


        return $discount;

    }

    /* to edit the data in database*/

    public function updateData($discount, $data = []) 
    {
        $em = $this->getEntityManager();

        $discount->setF046fdiscountType($data['f046fdiscountType'])
                    ->setF046fcustomerType($data['f046fcustomerType'])
                    ->setF046fidRevenueCategory((int)$data['f046fidRevenueCategory'])
                    ->setF046fidRevenueCode((int)$data['f046fidRevenueCode'])
                    ->setF046fdiscountRateType((int)$data['f046fdiscountRateType'])
                    ->setF046fdiscountRate((float)$data['f046fdiscountRate'])
                    ->setF046fminimumOutstanding((float)$data['f046fminimumOutstanding'])
                    ->setF046fdiscountGenerationDate(new \DateTime($data['f046fdiscountGenerationDate']))
                    ->setF046ffund($data['f046ffund'])
                    ->setF046fdepartment($data['f046fdepartment'])
                    ->setF046factivity($data['f046factivity'])
                    ->setF046faccount($data['f046faccount'])    
                    ->setF046foutstandingAs($data['f046foutstandingAs'])
                    ->setF046fdiscountGenerationType($data['f046fdiscountGenerationType'])
                    ->setF046fidCustomer((int)$data['f046fidCustomer'])
                     ->setF046fstatus((int)$data['f046fstatus'])
                     ->setF046fupdatedBy((int)$_SESSION['userId']);

        $discount->setF046fupdatedDtTm(new \DateTime());

        try{
        $em->persist($discount);
        
        $em->flush();
 }
 catch(\Exception $e){
    echo $e;
 }

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    
}

?>