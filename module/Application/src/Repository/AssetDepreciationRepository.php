<?php
namespace Application\Repository;

use Application\Entity\T088fassetDepreciation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\view\Model\JsonModel;

class AssetDepreciationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('a.f088fid, a.f088fassetNumber, a.f088fmonth, a.f088fday, a.f088fstatus')
            ->from('Application\Entity\T088fassetDepreciation','a');
        
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;    
    }

    /* to retrive the data from database using id*/
    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f088fid, a.f088fassetNumber, a.f088fmonth, a.f088fday, a.f088fstatus')
            ->from('Application\Entity\T088fassetDepreciation','a')
            ->where('a.f088fid = :assetTypeId')
            ->setParameter('assetTypeId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $assetType = new T088fassetDepreciation();

        $assetType->setF088fassetNumber($data['f088fassetNumber'])
             ->setF088fmonth((int)$data['f088fmonth'])
             ->setF088fday((int)$data['f088fday'])
             ->setF088fstatus((int)$data['f088fstatus'])
             ->setF088fcreatedBy((int)$_SESSION['userId'])
             ->setF088fupdatedBy((int)$_SESSION['userId']);

        $assetType->setF088fcreatedDtTm(new \DateTime())
                ->setF088fupdatedDtTm(new \DateTime());

        $em->persist($assetType);
        $em->flush();

        return $assetType;
    }

    public function updateData($assetType, $data = []) 
    {
        $em = $this->getEntityManager();

        $assetType->setF088fassetNumber($data['f088fassetNumber'])
             ->setF088fmonth((int)$data['f088fmonth'])
             ->setF088fday((int)$data['f088fday'])
             ->setF088fstatus((int)$data['f088fstatus'])
             ->setF088fcreatedBy((int)$_SESSION['userId'])
             ->setF088fupdatedBy((int)$_SESSION['userId']);

        $assetType->setF088fupdatedDtTm(new \DateTime());
        
        $em->persist($assetType);
        $em->flush();
    }
}