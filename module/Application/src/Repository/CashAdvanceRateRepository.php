<?php
namespace Application\Repository;

use Application\Entity\T117fcashAdvanceRate;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CashAdvanceRateRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f117fid, c.f117fstaffType, c.f117ffromPositionGrade, c.f117ftoPositionGrade, c.f117famount, c.f117fstatus, ca.f116fstaffType as StaffType')
            ->from('Application\Entity\T117fcashAdvanceRate','c')
            ->leftjoin('Application\Entity\T116fcashAdvance', 'ca', 'with', 'c.f117fstaffType = ca.f116fid')
            ->orderBy('c.f117fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f117fid, c.f117fstaffType, c.f117ffromPositionGrade, c.f117ftoPositionGrade, c.f117famount, c.f117fstatus, ca.f116fstaffType as StaffType')
            ->from('Application\Entity\T117fcashAdvanceRate','c')
            ->leftjoin('Application\Entity\T116fcashAdvance', 'ca', 'with', 'c.f117fstaffType = ca.f116fid')
            ->where('c.f117fid = :cashId')
            ->setParameter('cashId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // print_r("sd");exit;

        $cashAdvance = new T117fcashAdvanceRate();

        $cashAdvance->setF117fstaffType((int)$data['f117fstaffType'])
            ->setF117ffromPositionGrade($data['f117ffromPositionGrade'])
            ->setF117ftoPositionGrade($data['f117ftoPositionGrade'])
            ->setF117famount((float)$data['f117famount'])
            ->setF117fstatus((int)$data['f117fstatus'])
            ->setF117fcreatedBy((int)$_SESSION['userId']);

        $cashAdvance->setF117fcreatedDtTm(new \DateTime());
               
        try
        {
        $em->persist($cashAdvance);
        
        $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    /* to edit the data in database*/

    public function updateData($cashAdvance, $data = []) 
    {
        $em = $this->getEntityManager();

        $cashAdvance->setF117fstaffType((int)$data['f117fstaffType'])
            ->setF117ffromPositionGrade($data['f117ffromPositionGrade'])
            ->setF117ftoPositionGrade($data['f117ftoPositionGrade'])
            ->setF117famount((float)$data['f117famount'])
            ->setF117fstatus((int)$data['f117fstatus'])
            ->setF117fupdatedBy((int)$_SESSION['userId']);

        $cashAdvance->setF117fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($cashAdvance);
        
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('c.f117fid, c.f117fstaffType, c.f117ffromPositionGrade, c.f117ftoPositionGrade, c.f117famount, c.f117fstatus, ca.f116fstaffType as StaffType')
            ->from('Application\Entity\T117fcashAdvanceRate','c')
            ->leftjoin('Application\Entity\T116fcashAdvance', 'ca', 'with', 'c.f117fstaffType = ca.f116fid')
            ->where('c.f117fstatus = 1')
            ->orderBy('c.f117fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}

?>