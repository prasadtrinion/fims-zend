<?php
namespace Application\Repository;

use Application\Entity\T125fstudentDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StudentDetailRepository extends EntityRepository 
{

    public function getList() 
    {
    	

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('i.f125fid, i.f125fname, i.f125fsemSequence, d.f011fdefinitionCode as semesterSequence, i.f125fdescription, i.f125fstatus, i.f125fintakeYear')
            ->from('Application\Entity\T125fstudentDetails', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'i.f125fsemSequence = d.f011fid');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('i.f125fid, i.f125fname, i.f125fsemSequence, d.f011fdefinitionCode as semesterSequence, i.f125fdescription, i.f125fintakeYear, i.f125fstatus')
            ->from('Application\Entity\T125fstudentDetails', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'i.f125fsemSequence = d.f011fid')
            ->where('i.f125fid = :intakeId')
            ->setParameter('intakeId',$id);
            
       $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

       
    return $result;
    }

    public function createStudent($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $student = new T125fstudentDetails();

        $student->setF125fname($data['f125fname'])
            ->setF125fsemSequence((int)$data['f125fsemSequence'])
            ->setF125fdescription($data['f125fdescription'])
            ->setF125fintakeYear($data['f125fintakeYear'])
            ->setF125fstatus((int) $data['f125fstatus'])
            ->setF125fcreatedBy((int)$_SESSION['userId'])
            ->setF125fupdatedBy((int)$_SESSION['userId']);

        $student->setF125fcreatedDtTm(new \DateTime())
            ->setF125fupdatedDtTm(new \DateTime());

        
            $em->persist($student);

            $em->flush();
        return $student;
    }

    public function updateIntake($studentstudent, $data = [])
    {
        $em = $this->getEntityManager();

        $studentstudent->setF125fname($data['f125fname'])
            ->setF125fsemSequence($data['f125fsemSequence'])
            ->setF125fdescription($data['f125fdescription'])
            ->setF125fintakeYear($data['f125fintakeYear'])
            ->setF125fstatus((int) $data['f125fstatus'])
            ->setF125fcreatedBy((int)$_SESSION['userId'])
            ->setF125fupdatedBy((int)$_SESSION['userId']);

        $studentstudent->setF125fupdatedDtTm(new \DateTime());

        try {
            $em->persist($studentstudent);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $studentstudent;
    }



}
