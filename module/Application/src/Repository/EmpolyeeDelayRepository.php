<?php
namespace Application\Repository;

use Application\Entity\T134femployeeDelay;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class EmpolyeeDelayRepository extends EntityRepository 
{

    public function getList() 
    {

        
        $em = $this->getEntityManager();
         
        $qb = $em->createQueryBuilder(); 

        $qb->select('e.f134fid, e.f134fdate, e.f134fproject, e.f134fmodule, e.f134fscreen, e.f134ftitle, e.f134fdescription, e.f134fhours, e.f134fstatus, dn.f011fdefinitionCode, dn1.f011fdefinitionDesc')
            ->from('Application\Entity\T134femployeeDelay','e')
             ->leftjoin('Application\Entity\T011fdefinationms', 'dn', 'with', 'e.f134fproject = dn. f011fid')
             ->leftjoin('Application\Entity\T011fdefinationms', 'dn1', 'with', 'e.f134fmodule = dn1. f011fid')
            ->orderBy('e.f134fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);


         $result1 = array();
        foreach ($result as $value)
        {

          $date  = $value['f134fdate'];
          $value['f134fdate'] = date("Y-m-d", strtotime($date));

          array_push($result1, $value);
        }
        return $result1;

    }
        
    //     return $result;
    // }

    public function getListById($id) 
    {
         $em = $this->getEntityManager();
         
        $qb = $em->createQueryBuilder(); 

        $qb->select('e.f134fid, e.f134fdate, e.f134fproject, e.f134fmodule, e.f134fscreen,e.f134ftitle, e.f134fdescription, e.f134fhours,e.f134fstatus, dn.f011fdefinitionCode, dn1.f011fdefinitionDesc')
            ->from('Application\Entity\T134femployeeDelay','e')
            ->leftjoin('Application\Entity\T011fdefinationms', 'dn', 'with', 'e.f134fproject = dn. f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'dn1', 'with', 'e.f134fmodule = dn1. f011fid')
            ->where('e.f134fid = :dempID')
            ->setParameter('dempID',$id);        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $value)
        {

          $date  = $value['f134fdate'];
          $value['f134fdate'] = date("Y-m-d", strtotime($date));

          array_push($result1, $value);
        }
        
        return $result1;
    }
        
    //     return $result;
    // }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        
        $demp = new T134femployeeDelay();

        $demp->setF134fdate(new \DateTime($$data['f134fdate']))
                ->setF134fproject((int)$data['f134fproject'])
                ->setF134fmodule((int)$data['f134fmodule'])
                ->setF134fscreen((int)$data['f134fscreen'])
                ->setF134ftitle($data['f134ftitle'])
                ->setF134fdescription($data['f134fdescription'])
                ->setF134fhours((int)$data['f134fhours'])
                ->setF134fstatus((int)$data['f134fstatus']);

            $demp->setF134fcreateDtTm(new \DateTime());
        
            $em->persist($demp);
            $em->flush();
        return $demp;
    }

    public function updateData($demp, $data) 
    {
        $em = $this->getEntityManager();
        
        // $demp = new T134femployeeDelay();

        $demp->setF134fdate(new \DateTime($data['f134fdate']))
                ->setF134fproject((int)$data['f134fproject'])
                ->setF134fmodule((int)$data['f134fmodule'])
                ->setF134fscreen((int)$data['f134fscreen'])
                ->setF134ftitle($data['f134ftitle'])
                ->setF134fdescription($data['f134fdescription'])
                ->setF134fhours((int)$data['f134fhours'])
                ->setF134fstatus((int)$data['f134fstatus']);

            $demp->setF134fupdateDtTm(new \DateTime());

            $em->persist($demp);
            $em->flush();
    }



}
?>