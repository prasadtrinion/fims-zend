<?php
namespace Application\Repository;

use Application\Entity\T071fvoucher;
use Application\Entity\T072fvoucherDetails;
use Application\Entity\T017fjournal;
use Application\Entity\T018fjournalDetails;
use Application\Repository\JournalRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class VoucherRepository extends EntityRepository
{

    public function getVoucherList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fstatus,i.f071fvoucherTotal,d.f011fdefinitionCode as voucherName ')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType');
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid');
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $voucher) {
            $date                        = $voucher['f071fvoucherDate'];
            $voucher['f071fvoucherDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $voucher);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getCustomerVouchers($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fidCustomer,i.f071fstatus,i.f071fvoucherTotal,d.f011fdefinitionCode as voucherName ')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
            ->where('i.f071fidCustomer = :customerId')
            ->setParameter('customerId', $id);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $voucher) {
            $date                        = $voucher['f071fvoucherDate'];
            $voucher['f071fvoucherDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $voucher);
        }
        $result = array(
            'data' => $result1,
        );
        return $result;

    }

    public function getVoucherById($id, $approvedStatus)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if ($approvedStatus == 0 && isset($approvedStatus)) {
            $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fstatus,i.f071fvoucherTotal,d.f011fdefinitionCode as voucherName')
                ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')

                // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
                ->where('i.f071fstatus = 0');
        } elseif ($approvedStatus == 1) {

            $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fstatus,i.f071fvoucherTotal,d.f011fdefinitionCode as voucherName')
                ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')

                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')

                ->where('i.f071fstatus = 1 ');

        } elseif ($approvedStatus == 2) {
            $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fstatus,i.f071fvoucherTotal,d.f011fdefinitionCode as voucherName')
                ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ;
                // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid');
                
        } else {
            $qb->select('i.f071fid,i.f071fvoucherNumber,i.f071fvoucherType,i.f071fidCustomer,i.f071fidFinancialYear,i.f071fvoucherDate,i.f071fapprovedBy,i.f071fstatus,i.f071fvoucherTotal,id.f072fid,id.f072fidItem,id.f072fgstCode,id.f072fgstValue,id.f072fidDebitGlcode,id.f072fidCreditGlcode,id.f072fquantity,id.f072fprice,id.f072ftotal,id.f072fstatus,d.f011fdefinitionCode as voucherName')
                ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')

                ->leftjoin('Application\Entity\T072fvoucherDetails', 'id', 'with', 'i.f071fid = id.f072fidVoucher');
                // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid');
            $qb->where('i.f071fid = :voucherId')
                ->setParameter('voucherId', $id);
        }
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $voucher) {
            $date                        = $voucher['f071fvoucherDate'];
            $voucher['f071fvoucherDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $voucher);
        }
        return $result1;
    }
public function createVoucherDetail($voucherObj, $voucherDetail)
    {
        $em = $this->getEntityManager();

        $voucherDetailObj = new T072fvoucherDetails();
        $voucherDetailObj->setF072fidItem((int) $voucherDetail['f072fidItem'])
            ->setF072fidCreditGlcode((int) $voucherDetail['f072fidCreditGlcode'])
            ->setF072fidDebitGlcode((int) $voucherDetail['f072fidDebitGlcode'])
            ->setF072fgstCode($voucherDetail['f072fgstCode'])
            ->setF072fgstValue($voucherDetail['f072fgstValue'])
            ->setF072fquantity((int) $voucherDetail['f072fquantity'])
            ->setF072fprice($voucherDetail['f072fprice'])
            ->setF072ftotal($voucherDetail['f072ftotal'])
            ->setF072fstatus((int) $data['f071fstatus'])
            ->setF072fcreatedBy((int)$_SESSION['userId'])
            ->setF072fupdatedBy((int)$_SESSION['userId'])
            ->setF072fidInvoice($voucherObj);

        $voucherDetailObj->setF072fcreatedDtTm(new \DateTime())
            ->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($voucherDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    public function updateVoucher($voucher, $data = [])
    {
        $em = $this->getEntityManager();

        $voucher->setF071fvoucherNumber($data['f071fvoucherNumber'])
            ->setF071fvoucherType($data['f071fvoucherType'])
            ->setF071fvoucherDate(new \DateTime($data['f071fvoucherDate']))
            ->setF071fidFinancialYear(1)
            // ->setF071fidFinancialYear($data['f071fIdFinancialYear'])
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071fvoucherTotal($data['f071fvoucherTotal'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $voucher->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($voucher);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $voucher;
    }
    public function updateVoucherDetail($voucherDetailObj, $voucherDetail)
    {
        
        $em = $this->getEntityManager();

        $voucherDetailObj->setF072fidItem((int) $voucherDetail['f072fidItem'])
            ->setF072fidCreditGlcode((int) $voucherDetail['f072fidCreditGlcode'])
            ->setF072fidDebitGlcode((int) $voucherDetail['f072fidDebitGlcode'])
            ->setF072fgstCode($voucherDetail['f072fgstCode'])
            ->setF072fgstValue($voucherDetail['f072fgstValue'])
            ->setF072fquantity((int) $voucherDetail['f072fquantity'])
            ->setF072fprice((int) $voucherDetail['f072fprice'])
            ->setF072ftotal((int) $voucherDetail['f072ftotal'])
            ->setF072fstatus((int) $data['f071fstatus'])
            ->setF072fupdatedBy((int)$_SESSION['userId']);

        $voucherDetailObj->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($voucherDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public function createNewVoucher($data)
    {
       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $voucher = new T071fvoucher();

        $voucher->setF071fvoucherNumber($data['f071fvoucherNumber'])
            ->setF071fvoucherType($data['f071fvoucherType'])
            ->setF071fvoucherDate(new \DateTime($data['f071fvoucherDate']))
            ->setF071fidFinancialYear(1)
            // ->setF071fidFinancialYear($data['f071fIdFinancialYear'])
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071fvoucherTotal(floatval($data['f071fvoucherTotal']))
            ->setF071fcreatedBy((int)$_SESSION['userId'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $voucher->setF071fcreatedDtTm(new \DateTime())
            ->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($voucher);

            $em->flush();

        } catch (\Exception $e) {
            echo $e;
        }

        $voucherDetails = $data['voucher-details'];

        foreach ($voucherDetails as $voucherDetail) {

            $voucherDetailObj = new T072fvoucherDetails();
            $voucherDetailObj->setF072fidItem((int) $voucherDetail['f072fidItem'])
                ->setF072fidCreditGlcode((int) $voucherDetail['f072fidCreditGlcode'])
                ->setF072fidDebitGlcode((int) $voucherDetail['f072fidDebitGlcode'])
                ->setF072fgstCode($voucherDetail['f072fgstCode'])
                ->setF072fgstValue($voucherDetail['f072fgstValue'])
                ->setF072fquantity((int) $voucherDetail['f072fquantity'])
                ->setF072fprice((int) $voucherDetail['f072fprice'])
                ->setF072ftotal((int) $voucherDetail['f072ftotal'])
                ->setF072fstatus((int) $data['f071fstatus'])
                ->setF072fcreatedBy((int)$_SESSION['userId'])
                ->setF072fupdatedBy((int)$_SESSION['userId'])
                ->setF072fidVoucher($voucher);

            $voucherDetailObj->setF072fcreatedDtTm(new \DateTime())
                ->setF072fupdatedDtTm(new \DateTime());
            try {
                $em->persist($voucherDetailObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }
        return $voucher;
    }
    
    public function voucherApprove($postData,$journalRepository)
    {
        $em = $this->getEntityManager();
        foreach ($postData['id'] as $id) {

            $query1 = "update t071fvoucher set f071fstatus = 1 where f071fid = $id";
            $em->getConnection()->executeQuery($query1);
            $query2 = "update t072fvoucher_details set f072fstatus = 1 where f072fid_voucher = $id";
            $em->getConnection()->executeQuery($query2);
            $query2 = "select f071fid_financial_year,f071fvoucher_date,f071fapproved_by,f071fcreated_by,f071fcreated_dt_tm,f071fvoucher_number from t071fvoucher where f071fid = $id";
            $voucher_result = $em->getConnection()->executeQuery($query2)->fetch();
            $query2 = "select SUM(f072ftotal) as item_debit,f014faccount_id,f014ffund_id,f072fid_debit_glcode as glcode from t072fvoucher_details inner join t014fglcode on f072fid_debit_glcode = f014fid where f072fid_voucher = $id group by f014faccount_id,f014ffund_id";
            $debit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
            $query2 = "select SUM(f072ftotal) as item_credit,f014faccount_id,f014ffund_id,f072fid_credit_glcode as glcode from t072fvoucher_details inner join t014fglcode on f072fid_credit_glcode = f014fid where f072fid_voucher = $id group by f014faccount_id,f014ffund_id";
            $credit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
           
            $totalAmount = 0;
            foreach($debit_result as $item){
                $totalAmount = $totalAmount + $item['item_debit'];
            }
            $data['f017fdescription'] = "Voucher";
            $data['f017fmodule'] = "Voucher";
            $data['f017fapprover'] = $voucher_result['f071fapproved_by'];
            $data['f017ftotalAmount'] = $totalAmount;
            $data['f017fdateOfTransaction'] = $voucher_result['f071fvoucher_date'];
            $data['f017fidFinancialYear'] = $invoice_result['f071fid_financial_year'];
            $data['f017fapprovedStatus'] = "1";
            $details = array();
            foreach($debit_result as $item){
                $detail['f018fidGlcode'] = $item['glcode'];
                $detail['f018fidAccountCode'] =  $item['f014faccount_id'];
                $detail['f018fidFundCode'] =  $item['f014ffund_id'];
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $voucher_result['f071fvoucher_number'];
                $detail['f018fdrAmount'] = (float) $item['item_debit'];
                $detail['f018fcrAmount'] = 0;
                $detail['f018fcreated_by'] = $voucher_result['f071fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $voucher_result['f071fcreated_dt_tm'];
                array_push($details,$detail);
             }
             foreach($credit_result as $item){
                $detail['f018fidGlcode'] = $item['glcode'];
                $detail['f018fidAccountCode'] =  $item['f014faccount_id'];
                $detail['f018fidFundCode'] =  $item['f014ffund_id'];
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $voucher_result['f071fvoucher_number'];
                $detail['f018fdrAmount'] = 0;
                $detail['f018fcrAmount'] = (float) $item['item_credit'];
                $detail['f018fcreated_by'] = $voucher_result['f071fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $voucher_result['f071fcreated_dt_tm'];
                array_push($details,$detail);
            }
            $data['journal-details'] = $details;
            $journalRepository->createNewData($data);
            
        }
    }
    

}
