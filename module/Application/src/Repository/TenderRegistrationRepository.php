<?php
namespace Application\Repository;

use Application\Entity\T038ftenderRegDetails;
use Application\Entity\T037ftenderRegMaster;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class TenderRegistrationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('t.f037fid,t.f037fidVendor,t.f037fidTender,v.f030fcompanyName as vendorName,td.f035fquotationNumber,td.f035fstartDate,td.f035fendDate,td.f035ftitle,td.f035ftotalAmount,t.f037fshortlisted,t.f037fawarded')
            ->from('Application\Entity\T037ftenderRegMaster','t')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with','t.f037fidVendor = v.f030fid')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'td','with','t.f037fidTender = td.f035fid');
		   
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f037fid,t.f037fidVendor,t.f037fidTender,v.f030fcompanyName as vendorName,td.f035fquotationNumber,td.f035fstartDate,td.f035fendDate,td.f035ftitle,td.f035ftotalAmount,t.f037fshortlisted,t.f037fawarded')
            ->from('Application\Entity\T037ftenderRegMaster','t')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with','t.f037fidVendor = v.f030fid')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'td','with','t.f037fidTender = td.f035fid')
            ->where('t.f037fid = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        

        $qb = $em->createQueryBuilder();
                $qb->select('i.f029fitemName,ts.f038fid,ts.f038fidItem,ts.f038ftaxCode,ts.f038ffundCode,ts.f038fdepartmentCode,ts.f038factivityCode,ts.f038faccountCode,ts.f038fbudgetFundCode,ts.f038fbudgetDepartmentCode,ts.f038fbudgetActivityCode,ts.f038fbudgetAccountCode,ts.f038fquantity,ts.f038fprice,ts.f038ftotal,ts.f038ftotalIncTax,ts.f038frequiredDate,ts.f038fsoCode,ts.f038ftaxAmount,ts.f038fstatus,ts.f038funit,ts.f038fpercentage,ts.f038fquotationAmount')
            ->from('Application\Entity\T038ftenderRegDetails','ts')
             ->leftjoin('Application\Entity\T029fitemSetUp', 'i','with','ts.f038fidItem = i.f029fid')
            ->where('ts.f038fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $detail_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = $result[0];
        $result['details'] = $detail_result;

        return $result;

    }

     public function getTenderDetails($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f037fid,t.f037fidVendor,t.f037fidTender,v.f030fcompanyName as vendorName,td.f035fquotationNumber,td.f035fstartDate,td.f035fendDate,td.f035ftitle,td.f035ftotalAmount,t.f037fshortlisted,t.f037fawarded,vr.f030fvendorCode')
            ->from('Application\Entity\T037ftenderRegMaster','t')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with','t.f037fidVendor = v.f030fid')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'td','with','t.f037fidTender = td.f035fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr','with','t.f037fidVendor = vr.f030fid')
            ->where('t.f037fidTender = :tenderId')
            ->groupby('t.f037fidVendor,t.f037fidTender')
            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result, $tender);
        }
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f037fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('i.f029fitemName,ts.f038fid,ts.f038fidItem,ts.f038ftaxCode,ts.f038ffundCode,ts.f038fdepartmentCode,ts.f038factivityCode,ts.f038faccountCode,ts.f038fbudgetFundCode,ts.f038fbudgetDepartmentCode,ts.f038fbudgetActivityCode,ts.f038fbudgetAccountCode,ts.f038fquantity,ts.f038fprice,ts.f038ftotal,ts.f038ftotalIncTax,ts.f038frequiredDate,ts.f038fsoCode,ts.f038ftaxAmount,ts.f038fstatus,ts.f038funit,ts.f038fpercentage,ts.f038fquotationAmount')
            ->from('Application\Entity\T038ftenderRegDetails','ts')
            ->leftjoin('Application\Entity\T029fitemSetUp', 'i','with','ts.f038fidItem = i.f029fid')
            ->where('ts.f038fidTender = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $tmp = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f038frequiredDate'];
            $tender['f038frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($tmp, $tender);
        }
$result[$i]['details'] = $tmp;
}
        

        
        return $result;

    }

     public function shortlistedVendorsByTender($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f037fid,t.f037fidVendor,t.f037fidTender,v.f030fcompanyName as vendorName,td.f035fquotationNumber,td.f035fstartDate,td.f035fendDate,td.f035ftitle,td.f035ftotalAmount,t.f037fshortlisted,t.f037fawarded')
            ->from('Application\Entity\T037ftenderRegMaster','t')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with','t.f037fidVendor = v.f030fid')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'td','with','t.f037fidTender = td.f035fid')
            ->where('t.f037fidTender = :tenderId')
            ->AndWhere('t.f037fshortlisted = 1')
            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result, $tender);
        }
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f037fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('i.f029fitemName,ts.f038fid,ts.f038fidItem,ts.f038ftaxCode,ts.f038ffundCode,ts.f038fdepartmentCode,ts.f038factivityCode,ts.f038faccountCode,ts.f038fbudgetFundCode,ts.f038fbudgetDepartmentCode,ts.f038fbudgetActivityCode,ts.f038fbudgetAccountCode,ts.f038fquantity,ts.f038fprice,ts.f038ftotal,ts.f038ftotalIncTax,ts.f038frequiredDate,ts.f038fsoCode,ts.f038ftaxAmount,ts.f038fstatus,ts.f038funit,ts.f038fpercentage,ts.f038fquotationAmount')
            ->from('Application\Entity\T038ftenderRegDetails','ts')
            ->leftjoin('Application\Entity\T029fitemSetUp', 'i','with','ts.f038fidItem = i.f029fid')
            ->where('ts.f038fidTender = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $tmp = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f038frequiredDate'];
            $tender['f038frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($tmp, $tender);
        }
$result[$i]['details'] = $tmp;
}
        

        
        return $result;

    }

    public function awardedVendorsByTender($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f037fid,t.f037fidVendor,t.f037fidTender,v.f030fcompanyName as vendorName,td.f035fquotationNumber,td.f035fstartDate,td.f035fendDate,td.f035ftitle,td.f035ftotalAmount,t.f037fshortlisted,t.f037fawarded')
            ->from('Application\Entity\T037ftenderRegMaster','t')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with','t.f037fidVendor = v.f030fid')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'td','with','t.f037fidTender = td.f035fid')
            ->where('t.f037fidTender = :tenderId')
            ->AndWhere('t.f037fawarded = 1')
            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result, $tender);
        }
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f037fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('i.f029fitemName,ts.f038fid,ts.f038fidItem,ts.f038ftaxCode,ts.f038ffundCode,ts.f038fdepartmentCode,ts.f038factivityCode,ts.f038faccountCode,ts.f038fbudgetFundCode,ts.f038fbudgetDepartmentCode,ts.f038fbudgetActivityCode,ts.f038fbudgetAccountCode,ts.f038fquantity,ts.f038fprice,ts.f038ftotal,ts.f038ftotalIncTax,ts.f038frequiredDate,ts.f038fsoCode,ts.f038ftaxAmount,ts.f038fstatus,ts.f038funit,ts.f038fpercentage,ts.f038fquotationAmount')
            ->from('Application\Entity\T038ftenderRegDetails','ts')
            ->leftjoin('Application\Entity\T029fitemSetUp', 'i','with','ts.f038fidItem = i.f029fid')
            ->where('ts.f038fidTender = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $tmp = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f038frequiredDate'];
            $tender['f038frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($tmp, $tender);
        }
$result[$i]['details'] = $tmp;
}
        

        
        return $result;

    }   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        foreach ($data['f037fidVendor'] as $vendor) {
        $tender = new T037ftenderRegMaster();
        
        $tender->setF037fidvendor((int)$vendor)
             ->setF037fidTender((int)$data['f037fidTender'])
             ->setF037fshortlisted((int)$data['f037fshortlisted'])
             ->setF037fstatus((int)$data['f037fstatus'])
             ->setF037fcreatedBy((int)$_SESSION['userId'])
             ->setF037fupdatedBy((int)$_SESSION['userId']);

        $tender->setF037fcreatedDtTm(new \DateTime())
                ->setF037fupdatedDtTm(new \DateTime());

        try{
        $em->persist($tender);

        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        $details = $data['details'];
        foreach ($details as $detail) {
            
        $detailObj = new T038ftenderRegDetails();

        $detailObj->setF038fidItem((int) $detail['f038fidItem'])
            ->setF038ffundCode( $detail['f038ffundCode'])
            ->setF038faccountCode( $detail['f038faccountCode'])
            ->setF038factivityCode( $detail['f038factivityCode'])
            ->setF038fdepartmentCode( $detail['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $detail['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $detail['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $detail['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $detail['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($detail['f038fsoCode'])
            ->setF038funit($detail['f038funit'])
            ->setF038fquantity((int) $detail['f038fquantity'])
            ->setF038fprice((float)$detail['f038fprice'])
            ->setF038ftotal((float)$detail['f038ftotal'])
            ->setF038fquotationAmount((float)$detail['f038fquotationAmount'])
            ->setF038ftotalIncTax((float)$detail['f038ftotalIncTax'])
            ->setF038ftaxCode((int) $detail['f038ftaxCode'])
            ->setF038fpercentage((float) $detail['f038fpercentage'])
            ->setF038ftaxAmount((float) $detail['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fcreatedBy((int)$_SESSION['userId'])
            ->setF038fupdatedBy((int)$_SESSION['userId'])
            ->setF038fidTender($tender->getF037fid());

        $detailObj->setF038fcreatedDtTm(new \DateTime())
                ->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($detail['f038frequiredDate']));

        try{
        $em->persist($detailObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }
    }

      
        return $tender;

    }

    /* to edit the data in database*/

    public function updateMaster($tender, $data = []) 
    {
        $em = $this->getEntityManager();

        $tender->setF037fidvendor((int)$data['f037fidVendor'])
             ->setF037fidTender((int)$data['f037fidTender'])
             ->setF037fstatus((int)$data['f037fstatus'])
             ->setF037fupdatedBy((int)$_SESSION['userId']);

        $tender->setF037fupdatedDtTm(new \DateTime());
        
        $em->persist($tender);
        $em->flush();

        return $tender;

    }


    public function createDetail($tender, $detail = []) 
    {
        $em = $this->getEntityManager();

        $detailObj = new T038ftenderRegDetails();

        $detailObj->setF038fidItem((int) $detail['f038fidItem'])
            ->setF038ffundCode( $detail['f038ffundCode'])
            ->setF038faccountCode( $detail['f038faccountCode'])
            ->setF038factivityCode( $detail['f038factivityCode'])
            ->setF038fdepartmentCode( $detail['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $detail['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $detail['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $detail['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $detail['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($detail['f038fsoCode'])
            ->setF038funit((int)$detail['f038funit'])
            ->setF038fquantity((int) $detail['f038fquantity'])
            ->setF038fprice((float)$detail['f038fprice'])
            ->setF038ftotal((float)$detail['f038ftotal'])
            ->setF038fquotationAmount((float)$detail['f038fquotationAmount'])
            ->setF038ftotalIncTax((float)$detail['f038ftotalIncTax'])
            ->setF038ftaxCode((int) $detail['f038ftaxCode'])
            ->setF038fpercentage((float) $detail['f038fpercentage'])
            ->setF038ftaxAmount((float) $detail['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fcreatedBy((int)$_SESSION['userId'])
            ->setF038fupdatedBy((int)$_SESSION['userId'])
            ->setF038fidTender($tender->getF037fid());

        $detailObj->setF038fcreatedDtTm(new \DateTime())
                ->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($detail['f038frequiredDate']));

        try{
        $em->persist($detailObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }

    }

public function updateDetail($detailObj, $detail = []) 
    {
        $em = $this->getEntityManager();


      $detailObj->setF038fidItem((int) $detail['f038fidItem'])
            ->setF038ffundCode( $detail['f038ffundCode'])
            ->setF038faccountCode( $detail['f038faccountCode'])
            ->setF038factivityCode( $detail['f038factivityCode'])
            ->setF038fdepartmentCode( $detail['f038fdepartmentCode'])
            ->setF038fbudgetFundCode( $detail['f038fbudgetFundCode'])
            ->setF038fbudgetAccountCode( $detail['f038fbudgetAccountCode'])
            ->setF038fbudgetActivityCode( $detail['f038fbudgetActivityCode'])
            ->setF038fbudgetDepartmentCode( $detail['f038fbudgetDepartmentCode'])
            ->setF038fsoCode($detail['f038fsoCode'])
            ->setF038funit($detail['f038funit'])
            ->setF038fquantity((int) $detail['f038fquantity'])
            ->setF038fprice((float)$detail['f038fprice'])
            ->setF038ftotal((float)$detail['f038ftotal'])
            ->setF038fquotationAmount((float)$detail['f038fquotationAmount'])
            ->setF038ftotalIncTax((float)$detail['f038ftotalIncTax'])
            ->setF038ftaxCode((int) $detail['f038ftaxCode'])
            ->setF038fpercentage((float) $detail['f038fpercentage'])
            ->setF038ftaxAmount((float) $detail['f038ftaxAmount'])
            ->setF038fstatus((int) $data['f035fstatus'])
            ->setF038fupdatedBy((int)$_SESSION['userId']);

        $detailObj->setF038fupdatedDtTm(new \DateTime())
                ->setF038frequiredDate(new \DateTime($detail['f038frequiredDate']));

        try{
        $em->persist($detailObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }

    }

   
    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('t.f035fid,t.f035fquotationNumber,t.f035fidCategory,d.f015fdepartmentName,i.f027fcategoryName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fid')
		   ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fstatus=1');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        return $result1;
        
    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function shortlistVendors($data) 
    {
        $em = $this->getEntityManager();
        $tenderId = $data['tendorId'];
        $query1 = "update t037ftender_reg_master set f037fshortlisted = 0 where f037fid_tender = $tenderId";
                $em->getConnection()->executeQuery($query1);
        foreach ($data['id'] as $id) {
            try{
                $query1 = "update t037ftender_reg_master set f037fshortlisted = 1 where f037fid = $id";
                $em->getConnection()->executeQuery($query1);
               
            }
            catch(\Exception $e){
                echo $e;
            }
        }
    }

    public function awardVendors($postdata) 
    {
       
        $em = $this->getEntityManager();
        $tendorId=$postdata['tendorId'];
        $query1 = "update t037ftender_reg_master set f037fawarded = 0 where f037fid = $tendorId";
            $em->getConnection()->executeQuery($query1);
        foreach($postdata['id'] as $id)
        {
            
            try{
                $query1 = "update t037ftender_reg_master set f037fawarded = 1 where f037fid = '$id'";
                $em->getConnection()->executeQuery($query1);
               
            }
            catch(\Exception $e){
                echo $e;
            }
        }

        
    }

    public function getFinalAwardTender() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("t.f050fid, t.f050fidVendor, t.f050fvendorCode, t.f050fgrade, t.f050famount, t.f050fcompletionDuration, t.f050fdescription, t.f050fisShortlisted,t.f050fisAwarded, t.f050fidTendor, tq.f035fquotationNumber, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle, d.f015fdepartmentName, d.f015fdepartmentCode, (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as Department")
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd','with','tq.f035fidDepartment = d.f015fdepartmentCode')
            ->where('t.f050fisAwarded =1');
           
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

}

?>
