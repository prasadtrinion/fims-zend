<?php
namespace Application\Repository;

use Application\Entity\T102fkwap;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class KwapRepository extends EntityRepository 
{
 
    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     

 
        $qb->select('k.f102fid, k.f102feffectiveDate, k.f102fkwapPercentage,  k.f102fstatus')
            ->from('Application\Entity\T102fkwap', 'k')
             ->orderBy('k.f102fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f102feffectiveDate'];
            $item['f102feffectiveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('k.f102fid, k.f102feffectiveDate, k.f102fkwapPercentage,  k.f102fstatus')
            ->from('Application\Entity\T102fkwap', 'k')
            ->where('k.f102fid = :kwapId')
            ->setParameter('kwapId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f102feffectiveDate'];
            $item['f102feffectiveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $kwap = new T102fkwap();

        $kwap->setF102feffectiveDate(new \DateTime($data['f102feffectiveDate']))
                ->setF102fkwapPercentage((float)$data['f102fkwapPercentage'])
                ->setF102fstatus((int)$data['f102fstatus'])
                ->setF102fcreatedBy((int)$_SESSION['userId'])
                ->setF102fupdatedBy((int)$_SESSION['userId']);

        $kwap->setF102fcreatedDtTm(new \DateTime())
               ->setF102fupdatedDtTm(new \DateTime());
          try{
        $em->persist($kwap);
        
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $kwap;
    }

    public function updateData($kwap, $data = []) 
    {
        $em = $this->getEntityManager();
           $kwap->setF102feffectiveDate(new \DateTime($data['f102feffectiveDate']))
                ->setF102fkwapPercentage((float)$data['f102fkwapPercentage'])
                ->setF102fstatus((int)$data['f102fstatus'])
                ->setF102fcreatedBy((int)$_SESSION['userId'])
                ->setF102fupdatedBy((int)$_SESSION['userId']);

        $kwap->setF102fcreatedDtTm(new \DateTime())
               ->setF102fupdatedDtTm(new \DateTime());
        try{
        $em->persist($kwap);
        $em->flush();
        // print_r($kwap);
        // die();
        }
        catch (\Exception $e){
            echo $e;
        }
    }  
} 
?>