<?php
namespace Application\Repository;

use Application\Entity\T051fbudgetCostcenter;
use Application\Entity\T015fdepartment;
use Application\Entity\T015ffinancialyear;
use Application\Entity\T108fbudgetSummary;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetCostcenterRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f051fid, b.f051fdepartment,b.f051ffund,d.f015fdepartmentName as departmentName,fu.f057fname as fundName, d.f015funit, d.f015fdeptCode, b.f051famount, b.f051fidFinancialyear,f.f110fyear as financialYear, b.f051fstatus, b.f051fapprovalStatus')
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.f051fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','b.f051ffund = fu.f057fcode')
            ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','b.f051fidFinancialyear = f.f110fid')
            ->orderBy('b.f051fid', 'DESC');


        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
           $qb->select('b.f051fid, b.f051fdepartment,b.f051ffund,d.f015fdepartmentName as departmentName, d.f015funit, d.f015fdeptCode, b.f051famount, b.f051fidFinancialyear, f.f110fyear as financialYear, b.f051fstatus, b.f051fapprovalStatus')
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.f051fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','b.f051fidFinancialyear = f.f110fid')
            ->where('b.f051fid = :costcenterId')
            ->setParameter('costcenterId',(int)$id);
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function getCostcenterApprovalList($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
               $qb->select("b.f051fid, b.f051fdepartment,b.f051ffund,fu.f057fname as fundName,d.f015fdepartmentName as departmentName, d.f015funit, d.f015fdeptCode, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName, b.f051famount, b.f051fidFinancialyear,f.f110fyear as financialYear, b.f051fstatus, b.f051fapprovalStatus")
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','b.f051ffund = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.f051fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','b.f051fidFinancialyear = f.f110fid')
                 ->where('b.f051fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select("b.f051fid, b.f051fdepartment,b.f051ffund,fu.f057fname as fundName,d.f015fdepartmentName as departmentName, d.f015funit, d.f015fdeptCode, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName, b.f051famount, b.f051fidFinancialyear,f.f110fyear as financialYear, b.f051fstatus, b.f051fapprovalStatus")
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','b.f051ffund = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.f051fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','b.f051fidFinancialyear = f.f110fid')
                    ->where('b.f051fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
               $qb->select("b.f051fid, b.f051fdepartment,b.f051ffund,fu.f057fname as fundName,d.f015fdepartmentName as departmentName, d.f015funit, d.f015fdeptCode, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName, b.f051famount, b.f051fidFinancialyear,f.f110fyear as financialYear, b.f051fstatus, b.f051fapprovalStatus")
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->leftjoin('Application\Entity\T057ffund', 'fu', 'with','b.f051ffund = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','b.f051fdepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','b.f051fidFinancialyear = f.f110fid')
            ->where('b.f051fapprovalStatus !=2');
            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function createCostcenter($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $budgetCostcenter = new T051fbudgetCostcenter();

        $budgetCostcenter->setF051fdepartment($data['f051fdepartment'])
              ->setF051ffund($data['f051ffund'])
              ->setF051famount((float)$data['f051famount'])
              ->setF051fbalanceAmount((int)$data['f051fbalanceAmount'])
              ->setF051fidFinancialyear((int)$data['f051fidFinancialyear'])
              ->setF051fapprovalStatus((int)$data['f051fapprovalStatus'])
              ->setF051freason($data['f051freason'])
              ->setF051fstatus((int)$data['f051fstatus'])
              ->setF051fcreatedBy((int)$_SESSION['userId']);

        $budgetCostcenter->setF051fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($budgetCostcenter);
            // print_r($budgetCostcenter);
            // die();
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
       

      return $budgetCostcenter;

    }

    public function updateData($budgetCostcenter, $data = []) 
    {
        $em = $this->getEntityManager();
        // print_r($budgetCostcenter);
        // exit();

        $budgetCostcenter->setF051fdepartment($data['f051fdepartment'])
              ->setF051ffund($data['f051ffund'])
              ->setF051famount((float)$data['f051famount'])
              ->setF051fbalanceAmount((int)$data['f051fbalanceAmount'])
              ->setF051fidFinancialyear((int)$data['f051fidFinancialyear'])
              ->setF051fapprovalStatus((int)$data['f051fapprovalStatus'])
              ->setF051freason($data['f051freason'])
              ->setF051fstatus((int)$data['f051fstatus'])
              ->setF051fcreatedBy((int)$_SESSION['userId'])
              ->setF051fupdatedBy((int)$_SESSION['userId']);


        $budgetCostcenter->setF051fupdatedDtTm(new \DateTime());
 
        $em->persist($budgetCostcenter);
        $em->flush();


    }

    public function CostcenterApprovalStatus($costcenter,$status,$reason) 
    {
        $em = $this->getEntityManager();

        foreach ($costcenter as $cost) 
        {
            $Id = $cost->getF051fid();
            $id=(int)$Id;
            
           $query = "UPDATE t051fbudget_costcenter set f051freason='$reason',f051fapproval_status=$status where f051fid = $id"; 
        
            $result=$em->getConnection()->executeQuery($query);



            // if($status==1)
            // {

            // $em = $this->getEntityManager();

            // $qb = $em->createQueryBuilder();
            
            // $qb->select('b.f051fid, b.f051fdepartment,b.f051ffund, b.f051fidFinancialyear,  b.f051famount')
            // ->from('Application\Entity\T051fbudgetCostcenter','b')
            // ->where('b.f051fid = :amountId')
            // ->setParameter('amountId',$id);

            // $query = $qb->getQuery();

            // $result = $query->getSingleResult();

            // $em = $this->getEntityManager();

            // $budgetSummary = new T108fbudgetSummary();

            // $budgetSummary->setF108fdepartment($result['f051fdepartment'])
            //              ->setF108fidFinancialYear((int)$result['f051fidFinancialyear'])
            //              ->setF108fallocatedAmount((float)$result['f051famount'])
            //              ->setF108fincrementAmount(0)
            //              ->setF108fverimentFrom(0)
            //              ->setF108fverimentTo(0)
            //              ->setF108fbudgetExpensed(0)
            //              ->setF108fbudgetPending(0)
            //              ->setF108fbudgetCommitment(0)
            //              ->setF108fstatus((int)1)
            //              ->setF108fcreatedBy((int)$_SESSION['userId'])
            //              ->setF108fupdatedBy((int)$_SESSION['userId']);

            // $budgetSummary->setF108fcreatedDtTm(new \DateTime())
            //             ->setF108fupdatedDtTm(new \DateTime());

            // $em->persist($budgetSummary);
            // $em->flush();
            // }

        }
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getCostcenterAmount($idDepartment,$idFinancialyear,$Fund)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('b.f051famount')
            ->from('Application\Entity\T051fbudgetCostcenter','b')
            ->where('b.f051fdepartment = :departmentId')
            ->setParameter('departmentId',$idDepartment)
            ->andwhere('b.f051fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('b.f051ffund = :fund')
            ->setParameter('fund',$Fund);

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getCostcenter($idFinancialyear)
    {

        $em = $this->getEntityManager();

        $query = "SELECT a.*,b.* FROM t051fbudget_costcenter as a, t015fdepartment as b  where a.f051fdepartment=b.f015fdepartment_code and f051fid_financialyear= $idFinancialyear";
            

        $result= $em->getConnection()->executeQuery($query)->fetchAll();

       
        
        return $result;
    }

    public function costcenterRejection($costcenters,$reason)
    {
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();
        
        foreach ($costcenters as $costcenter) 
        {

            $Id = $costcenter->getF051fid();
            $id=(int)$Id;
            
           $query = "UPDATE t051fbudget_costcenter set f051freason='$reason',f051fapproval_status='2' where f051fid = '$id'"; 
            $result=$em->getConnection()->executeQuery($query);

            
        }

        return $result;
    }


 }
 ?>

