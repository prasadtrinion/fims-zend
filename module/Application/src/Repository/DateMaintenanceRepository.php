<?php
namespace Application\Repository;

use Application\Entity\T070fdateMaintenance;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class DateMaintenanceRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $query = "select f070fid,f070fstafftype as f070fprocessType,f070fmonth,f070fstart_date,f070fend_date,f070fstatus,f103fname from t070fdate_maintenance inner join t103fprocess_type on cast(f070fstafftype as int)=f103fid order by f070fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();

        // $qb->select('dm.f070fid, dm.f070fstafftype as f070fprocessType, dm.f070fmonth, dm.f070fstartDate, dm.f070fendDate, dm.f070fstatus')
        //     ->from('Application\Entity\T070fdateMaintenance', 'dm')
        //     ->orderBy('dm.f070fid','DESC');

        // $query = $qb->getQuery();
        // $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f070fstart_date'];
            $item['f070fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f070fend_date'];
            $item['f070fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;

    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
          $query = "select f070fid,cast(f070fstafftype as int) as f070fprocessType,f070fmonth,f070fstart_date,f070fend_date,f070fstatus from t070fdate_maintenance  where f070fid = $id";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();

         // $qb->select('dm.f070fid, dm.f070fstafftype  as f070fprocessType, dm.f070fmonth, dm.f070fstartDate, dm.f070fendDate, dm.f070fstatus')
         //    ->from('Application\Entity\T070fdateMaintenance', 'dm')
         //    ->where('dm.f070fid = :dateId')
         //    ->setParameter('dateId',$id);
            
        // $query = $qb->getQuery();
        // $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f070fstart_date'];
            $item['f070fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f070fend_date'];
            $item['f070fendDate'] = date("Y-m-d", strtotime($date));
            // $item['f070fprocessType'] = (int)$item['f070fprocessType'];
            array_push($result1, $item);
        }
       
        return $result1;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $maintenance = new T070fdateMaintenance();
        $maintenance->setF070fstafftype($data['f070fprocessType'])
                ->setF070fmonth($data['f070fmonth'])
                ->setF070fstartDate(new \DateTime($data['f070fstartDate']))
                ->setF070fendDate(new \DateTime($data['f070fendDate']))
                ->setF070fstatus((int)$data['f070fstatus'])
                ->setF070fcreatedBy((int)$_SESSION['userId'])
                ->setF070fupdatedBy((int)$_SESSION['userId']);

        $maintenance->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($maintenance);
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $maintenance;
    }
    public function updateData($maintenance, $data = []) 
    {
        $em = $this->getEntityManager();

        $maintenance->setF070fstafftype($data['f070fprocessType'])
                ->setF070fmonth($data['f070fmonth'])
                ->setF070fstartDate(new \DateTime($data['f070fstartDate']))
                ->setF070fendDate(new \DateTime($data['f070fendDate']))
                ->setF070fstatus((int)$data['f070fstatus'])
                ->setF070fcreatedBy((int)$_SESSION['userId'])
                ->setF070fupdatedBy((int)$_SESSION['userId']);

        $maintenance->setF070fcreatedDtTm(new \DateTime())
                ->setF070fupdatedDtTm(new \DateTime());

        $em->persist($maintenance);
        $em->flush();
    }
}
?>