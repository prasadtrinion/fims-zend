<?php
namespace Application\Repository;

use Application\Entity\T085fassetInformation;
use Application\Entity\T085fassetInformationNew;
use Application\Entity\T085fassetInformationHistory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetInformationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("a.f085fid, a.f085fassetCode, a.f085fassetDescription, a.f085fidDepartment, a.f085fidAssetCategory, c.f087fcategoryCode as Category, a.f085fAcquisitionDate, a.f085forderNumber, a.f085fstoreCode, a.f085faccountCode, a.f085fapproveDate, a.f085fexpireDate, a.f085fidAssetType, a.f085finstallCost, a.f085faccumDeprCost, a.f085fnetValue, a.f085fyearDeprCost, a.f085fresidualValue, a.f085fstatus,a.f085fassetOwner, a.f085fbarCode, a.f085fserialNumber, a.f085fbrandName, a.f085fcurrentBuilding, a.f085fcurrentRoom, a.f085fcurrentBlock, a.f085fcurrentCampus, a.f085fsubCategory,a.f085fpoNumber, a.f085fgrnNumber, a.f085fsoCode,a.f085freferrer, a.f085fitemName, a.f085fidItem, s.f089fsubcode as subCategory, i.f088fitemCode as itemCode, c.f087fcategoryDiscription, a.f085fid as f086fidAsset")
            ->from('Application\Entity\T085fassetInformation','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f085fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f085fidItem = i.f088fid')
            // ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'a.f085fidAssetType = t.f090fid')
            // ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'a.f085faccountCode = ac.f059fcompleteCode')
            ->where('a.f085fstatus = 1')
            ->orderBy('a.f085fid','DESC');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f085fAcquisitionDate'];
            $row['f085fAcquisitionDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("a.f085fid, a.f085fassetCode, a.f085fassetDescription, a.f085fidDepartment, a.f085fidAssetCategory, c.f087fcategoryCode as Category, a.f085fAcquisitionDate, a.f085forderNumber, a.f085fstoreCode, a.f085faccountCode, a.f085fapproveDate, a.f085fexpireDate, a.f085fidAssetType, t.f090fcode as assetType, a.f085finstallCost, a.f085faccumDeprCost, a.f085fnetValue, a.f085fyearDeprCost, a.f085fresidualValue, a.f085fstatus,a.f085fassetOwner, a.f085fbarCode, a.f085fserialNumber, a.f085fbrandName, a.f085fcurrentBuilding, a.f085fcurrentRoom,a.f085fcurrentBlock,a.f085fcurrentCampus,a.f085fsubCategory,a.f085fpoNumber,a.f085fgrnNumber,a.f085fsoCode,a.f085freferrer,a.f085fitemName,a.f085fidItem,s.f089fsubcode as subCategory,i.f088fitemCode as itemCode, c.f087fcategoryDiscription, ac.f059fdescription, (ac.f059fcompleteCode+'-'+ac.f059fname) as Account")
            ->from('Application\Entity\T085fassetInformation','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f085fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f085fidItem = i.f088fid')
            ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'a.f085fidAssetType = t.f090fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'a.f085faccountCode = ac.f059fcompleteCode')
            ->where('a.f085fid = :assetId')
            ->orderBy('a.f085fid','DESC')
            ->setParameter('assetId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f085fAcquisitionDate'];
            $row['f085fAcquisitionDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        return $result1;
    }

    public function createAsset($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $dept = $data['f085fidDepartment'];
        $subCat = $data['f085fsubCategory'];
        $Year = date('Y');

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="Asset";
        $runningNumber = $configRepository->generateFIMS($numberData);
        // if($runningNumber<10){
        //     $runningNumber = '000'.$runningNumber;
        // }
        // else if($runningNumber<100){
        //     $runningNumber = '00'.$runningNumber;
        // }
        // else if($runningNumber<1000){
        //     $runningNumber = '0'.$runningNumber;
        // }
        $query  = "select f015fdepartment_code from t015fdepartment where f015fid=$dept";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        $deptCode = $result['f015fdepartment_code'];

        $query  = "select f089fsubcode from t089fasset_sub_categories where f089fid=$subCat";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        $subCode = $result['f089fsubcode'];
        
        $finalCode = 'UUM/'.$dept.'/'.$subCode.$runningNumber.'/'.$Year;
        $asset = new T085fassetInformation();
   
        $asset->setF085fassetCode($finalCode)
             ->setF085fassetDescription($data['f085fassetDescription'])
             ->setF085fidDepartment($data['f085fidDepartment'])
             ->setF085fidAssetCategory((int)$data['f085fidAssetCategory'])
             ->setF085fAcquisitionDate(new \DateTime($data['f085fAcquisitionDate']))
             ->setF085forderNumber($data['f085forderNumber'])
             ->setF085fstoreCode($data['f085fstoreCode'])
             ->setF085faccountCode($data['f085faccountCode'])
             ->setF085fapproveDate(new \DateTime($data['f085fapproveDate']))
             ->setF085fexpireDate(new \DateTime($data['f085fexpireDate']))
             ->setF085fidAssetType((int)$data['f085fidAssetType'])
             ->setF085finstallCost((float)$data['f085finstallCost'])
             ->setF085faccumDeprCost((float)$data['f085faccumDeprCost'])
             ->setF085fnetValue((float)$data['f085fnetValue'])
             ->setF085fyearDeprCost((float)$data['f085fyearDeprCost'])
             ->setF085fresidualValue((float)$data['f085fresidualValue'])
              ->setF085fassetOwner($data['f085fassetOwner'])
             ->setF085fbarCode($data['f085fbarCode'])
             ->setF085fserialNumber($data['f085fserialNumber'])
             ->setF085fbrandName($data['f085fbrandName'])
             ->setF085fcurrentBuilding((int)$data['f085fcurrentBuilding'])
             ->setF085fcurrentRoom((int)$data['f085fcurrentRoom'])
             ->setF085fcurrentBlock((int)$data['f085fcurrentBlock'])
             ->setF085fcurrentCampus((int)$data['f085fcurrentCampus'])
             ->setF085fsubCategory((int)$data['f085fsubCategory'])
             ->setF085fpoNumber($data['f085fpoNumber'])
             ->setF085fgrnNumber((int)$data['f085fgrnNumber'])
             ->setF085fsoCode($data['f085fsoCode'])
             ->setF085freferrer($data['f085freferrer'])
             ->setF085fidItem((int)$data['f085fidItem'])
             ->setF085fitemName($data['f085fitemName'])
             ->setF085fstatus((int)$data['f085fstatus'])
             ->setF085fcreatedBy((int)$_SESSION['userId'])
             ->setF085fupdatedBy((int)$_SESSION['userId']);

        $asset->setF085fcreatedDtTm(new \DateTime())
                ->setF085fupdatedDtTm(new \DateTime());
        try{
        $em->persist($asset);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $asset;
    }
     public function updateAsset($asset,$data = []) 
    {
        $em = $this->getEntityManager();
        

        $asset->setF085fassetCode($data['f085fassetCode'])
             ->setF085fassetDescription($data['f085fassetDescription'])
             ->setF085fidDepartment($data['f085fidDepartment'])
             ->setF085fidAssetCategory((int)$data['f085fidAssetCategory'])
             ->setF085fAcquisitionDate(new \DateTime($data['f085fAcquisitionDate']))
             ->setF085forderNumber($data['f085forderNumber'])
             ->setF085fstoreCode($data['f085fstoreCode'])
             ->setF085faccountCode($data['f085faccountCode'])
             ->setF085fapproveDate(new \DateTime($data['f085fapproveDate']))
             ->setF085fexpireDate(new \DateTime($data['f085fexpireDate']))
             ->setF085fidAssetType((int)$data['f085fidAssetType'])
             ->setF085finstallCost((float)$data['f085finstallCost'])
             ->setF085faccumDeprCost((float)$data['f085faccumDeprCost'])
             ->setF085fnetValue((float)$data['f085fnetValue'])
             ->setF085fyearDeprCost((float)$data['f085fyearDeprCost'])
             ->setF085fresidualValue((float)$data['f085fresidualValue'])
              ->setF085fassetOwner($data['f085fassetOwner'])
           ->setF085fbarCode($data['f085fbarCode'])
           ->setF085fserialNumber($data['f085fserialNumber'])
           ->setF085fbrandName($data['f085fbrandName'])
           ->setF085fcurrentBuilding((int)$data['f085fcurrentBuilding'])
             ->setF085fcurrentRoom((int)$data['f085fcurrentRoom'])
             ->setF085fcurrentBlock((int)$data['f085fcurrentBlock'])
             ->setF085fcurrentCampus((int)$data['f085fcurrentCampus'])
             ->setF085fsubCategory((int)$data['f085fsubCategory'])
             ->setF085fpoNumber($data['f085fpoNumber'])
             ->setF085fgrnNumber((int)$data['f085fgrnNumber'])
             ->setF085fsoCode($data['f085fsoCode'])
             ->setF085freferrer($data['f085freferrer'])
             ->setF085fitemName($data['f085fitemName'])
             ->setF085fidItem((int)$data['f085fidItem'])
             ->setF085fstatus((int)$data['f085fstatus'])
             ->setF085fupdatedBy((int)$_SESSION['userId']);

        $asset  ->setF085fupdatedDtTm(new \DateTime());
        
        try{
        $em->persist($asset);
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
    }
        return $asset;

    }

   public function updateNewAsset($asset,$data = []) 
    {
        // print_r($asset);
        // die();
        $em = $this->getEntityManager();
        // print_r($data);exit;
        
        $asset->setF085fassetCode($data['f085fassetCode'])
             ->setF085fassetDescription($data['f085fassetDescription'])
             ->setF085fidDepartment($data['f085fidDepartment'])
             ->setF085fidAssetCategory((int)$data['f085fidAssetCategory'])
             ->setF085fAcquisitionDate(new \DateTime($data['f085fAcquisitionDate']))
             ->setF085forderNumber($data['f085forderNumber'])
             ->setF085fstoreCode($data['f085fstoreCode'])
             ->setF085faccountCode($data['f085faccountCode'])
             ->setF085fapproveDate(new \DateTime($data['f085fapproveDate']))
             ->setF085fexpireDate(new \DateTime($data['f085fexpireDate']))
             ->setF085fidAssetType((int)$data['f085fidAssetType'])
             ->setF085finstallCost((float)$data['f085finstallCost'])
             ->setF085faccumDeprCost((float)$data['f085faccumDeprCost'])
             ->setF085fnetValue((float)$data['f085fnetValue'])
             ->setF085fyearDeprCost((float)$data['f085fyearDeprCost'])
             ->setF085fresidualValue((float)$data['f085fresidualValue'])
              ->setF085fassetOwner($data['f085fassetOwner'])
           ->setF085fbarCode($data['f085fbarCode'])
           ->setF085fserialNumber($data['f085fserialNumber'])
           ->setF085fbrandName($data['f085fbrandName'])
           ->setF085fcurrentBuilding((int)$data['f085fcurrentBuilding'])
             ->setF085fcurrentRoom((int)$data['f085fcurrentRoom'])
             ->setF085fcurrentBlock((int)$data['f085fcurrentBlock'])
             ->setF085fcurrentCampus((int)$data['f085fcurrentCampus'])
             ->setF085fsubCategory((int)$data['f085fsubCategory'])
             ->setF085fpoNumber($data['f085fpoNumber'])
             ->setF085fgrnNumber((int)$data['f085fgrnNumber'])
             ->setF085fsoCode($data['f085fsoCode'])
             ->setF085freferrer($data['f085freferrer'])
             ->setF085fitemName($data['f085fitemName'])
             ->setF085fidItem((int)$data['f085fidItem'])
             ->setF085fstatus((int)$data['f085fstatus'])
             ->setF085fapprovalStatus((int)$data['f085fstatus'])
             ->setF085fdata(serialize($data))
             ->setF085fupdatedBy((int)$_SESSION['userId']);

        $asset  ->setF085fupdatedDtTm(new \DateTime());
        
          try{
        $em->persist($asset);
        $em->flush();
        }
        catch(\Exception $e){
        echo $e;
    }

        return $asset;
    }
    public function createNewAsset($data = []) 
    {
        $em = $this->getEntityManager();
        
        $asset = new T085fassetInformationNew();

        $asset->setF085fassetCode($data['f085fassetCode'])
             ->setF085fassetDescription($data['f085fassetDescription'])
             ->setF085fidDepartment($data['f085fidDepartment'])
             ->setF085fidAssetCategory((int)$data['f085fidAssetCategory'])
             ->setF085fAcquisitionDate(new \DateTime($data['f085fAcquisitionDate']))
             ->setF085forderNumber($data['f085forderNumber'])
             ->setF085fstoreCode($data['f085fstoreCode'])
             ->setF085faccountCode($data['f085faccountCode'])
             ->setF085fapproveDate(new \DateTime($data['f085fapproveDate']))
             ->setF085fexpireDate(new \DateTime($data['f085fexpireDate']))
             ->setF085fidAssetType((int)$data['f085fidAssetType'])
             ->setF085finstallCost((float)$data['f085finstallCost'])
             ->setF085faccumDeprCost((float)$data['f085faccumDeprCost'])
             ->setF085fnetValue((float)$data['f085fnetValue'])
             ->setF085fyearDeprCost((float)$data['f085fyearDeprCost'])
             ->setF085fresidualValue((float)$data['f085fresidualValue'])
              ->setF085fassetOwner($data['f085fassetOwner'])
           ->setF085fbarCode($data['f085fbarCode'])
           ->setF085fserialNumber($data['f085fserialNumber'])
           ->setF085fbrandName($data['f085fbrandName'])
           ->setF085fcurrentBuilding((int)$data['f085fcurrentBuilding'])
             ->setF085fcurrentRoom((int)$data['f085fcurrentRoom'])
             ->setF085fcurrentBlock((int)$data['f085fcurrentBlock'])
             ->setF085fcurrentCampus((int)$data['f085fcurrentCampus'])
             ->setF085fsubCategory((int)$data['f085fsubCategory'])
             ->setF085fpoNumber($data['f085fpoNumber'])
             ->setF085fgrnNumber((int)$data['f085fgrnNumber'])
             ->setF085fsoCode($data['f085fsoCode'])
             ->setF085freferrer($data['f085freferrer'])
             ->setF085fitemName($data['f085fitemName'])
             ->setF085fidItem((int)$data['f085fidItem'])
             ->setF085fstatus((int)"0")
             ->setF085fapprovalStatus((int)"0")
             ->setF085fdata(serialize($data))
             ->setF085fcreatedBy((int)$_SESSION['userId'])
             ->setF085fupdatedBy((int)$_SESSION['userId']);

        $asset->setF085fcreatedDtTm(new \DateTime())
                ->setF085fupdatedDtTm(new \DateTime());
        
        $em->persist($asset);
        $em->flush();

        return $asset;
    }


     

    public function approveAssetUpdate($data) 
    {
        $em = $this->getEntityManager();
        $status = $data['status'];
        foreach ($data['id'] as $id) 
        {

            try{
                if($status !=2){
        $query1 = "select f085fasset_code,f085fdata from t085fasset_information_new where f085fid = $id";
        $result = $em->getConnection()->executeQuery($query1)->fetch();
        // $assetCode = $result['f085fasset_code'];
        $data = unserialize($result['f085fdata']);
        $this->createAsset($data);
        }
        // $query1 = "insert into t085fasset_information_history (f085fasset_code,f085fasset_description,f085fid_department,f085fid_asset_category,f085f_acquisition_date,f085forder_number,f085fstore_code,f085faccount_code,f085fapprove_date,f085fexpire_date,f085fid_asset_type,f085finstall_cost,f085faccum_depr_cost,f085fyear_depr_cost,f085fresidual_value,f085fstatus,f085fcreated_by,f085fupdated_by,f085fcreated_dt_tm,f085fupdated_dt_tm,f085fasset_owner,f085fbar_code,f085fserial_number,f085fbrand_name,f085fcurrent_building,f085fcurrent_room,f085fcurrent_block,f085fcurrent_campus,f085fsub_category,f085fpo_number,f085fgrn_number,f085fso_code,f085freferrer,f085fitem_name) select f085fasset_code,f085fasset_description,f085fid_department,f085fid_asset_category,f085f_acquisition_date,f085forder_number,f085fstore_code,f085faccount_code,f085fapprove_date,f085fexpire_date,f085fid_asset_type,f085finstall_cost,f085faccum_depr_cost,f085fyear_depr_cost,f085fresidual_value,f085fstatus,f085fcreated_by,f085fupdated_by,f085fcreated_dt_tm,f085fupdated_dt_tm,f085fasset_owner,f085fbar_code,f085fserial_number,f085fbrand_name,f085fcurrent_building,f085fcurrent_room,f085fcurrent_block,f085fcurrent_campus,f085fsub_category,f085fpo_number,f085fgrn_number,f085fso_code,f085freferrer,f085fitem_name from t085fasset_information where f085fasset_code='$assetCode' ";
        // $em->getConnection()->executeQuery($query1);
        // print_r($data);
        // die();
          $query1 = "delete from t085fasset_information_new where f085fid = $id";
        $em->getConnection()->executeQuery($query1);

        // $query1 = "delete from t085fasset_information where f085fasset_code='$assetCode'";
        // $em->getConnection()->executeQuery($query1);
        // foreach ($data as $row) 
        // {
        }
        // }
        catch(\Exception $e){
            echo $e;
        }
        }
    }

    public function grnAsset($data) 
    {
        $em = $this->getEntityManager();
        // print_r($data);
        // die();
        foreach ($data as $row) 
        {
          
          $this->createNewAsset($row);
            // echo $assetCode;
            // die();
            try{
        // $query1 = "select f085fasset_code from t085fasset_information where f085fasset_code = '$assetCode'";
        // $result = $em->getConnection()->executeQuery($query1)->fetch();
        // if(!$result){
        //   $query1 = "insert into t085fasset_information(f085fasset_code) values('$assetCode')";
        // $em->getConnection()->executeQuery($query1);

        //   $query1 = "insert into t085fasset_information_new(f085fasset_code) values('$assetCode')";
        // $em->getConnection()->executeQuery($query1);
        
          
        }
        
        catch(\Exception $e){
            echo $e;
        }
        }
    }
    public function assetInformationDetails($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("a.f085fid, a.f085fassetCode, a.f085fassetDescription, a.f085fidDepartment, a.f085fidAssetCategory, c.f087fcategoryCode as Category, a.f085fAcquisitionDate, a.f085forderNumber, a.f085fstoreCode, a.f085faccountCode, a.f085fapproveDate, a.f085fexpireDate, a.f085fidAssetType, t.f090fcode as assetType, a.f085finstallCost, a.f085faccumDeprCost, a.f085fnetValue, a.f085fyearDeprCost, a.f085fresidualValue, a.f085fstatus,a.f085fassetOwner, a.f085fbarCode, a.f085fserialNumber, a.f085fbrandName, a.f085fcurrentBuilding, a.f085fcurrentRoom, a.f085fcurrentBlock, a.f085fcurrentCampus, a.f085fsubCategory, a.f085fpoNumber,a.f085fgrnNumber, a.f085fsoCode, a.f085freferrer, a.f085fitemName, a.f085fidItem,s.f089fsubcode as subCategory, i.f088fitemCode as itemCode, a.f085fapprovalStatus, c.f087fcategoryDiscription, ac.f059fname, ac.f059fdescription, (ac.f059fcompleteCode+'-'+ac.f059fname) as Account")
            ->from('Application\Entity\T085fassetInformationNew','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f085fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f085fidItem = i.f088fid')
            ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'a.f085fidAssetType = t.f090fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'a.f085faccountCode = ac.f059fcompleteCode')
            ->where('a.f085fapprovalStatus = :assetId')
            ->setParameter('assetId',(int)$id)
            ->orderBy('a.f085fid','DESC');
            

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f085fAcquisitionDate'];
            $row['f085fAcquisitionDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fapproveDate'];
            $row['f085fapproveDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fexpireDate'];
            $row['f085fexpireDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }
    public function assetInformationDetailsById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f085fid, a.f085fassetCode, a.f085fassetDescription, a.f085fidDepartment, a.f085fidAssetCategory, c.f087fcategoryCode as Category, a.f085fAcquisitionDate, a.f085forderNumber, a.f085fstoreCode, a.f085faccountCode, a.f085fapproveDate, a.f085fexpireDate, a.f085fidAssetType, t.f090fcode as assetType, a.f085finstallCost, a.f085faccumDeprCost, a.f085fnetValue, a.f085fyearDeprCost, a.f085fresidualValue, a.f085fstatus,a.f085fassetOwner, a.f085fbarCode, a.f085fserialNumber, a.f085fbrandName, a.f085fcurrentBuilding, a.f085fcurrentRoom,a.f085fcurrentBlock,a.f085fcurrentCampus,a.f085fsubCategory,a.f085fpoNumber,a.f085fgrnNumber,a.f085fsoCode,a.f085freferrer,a.f085fitemName,a.f085fidItem,s.f089fsubcode as subCategory,i.f088fitemCode as itemCode, a.f085fapprovalStatus')
            ->from('Application\Entity\T085fassetInformationNew','a')
            ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 's', 'with', 'a.f085fsubCategory = s.f089fid')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'a.f085fidItem = i.f088fid')
            ->leftjoin('Application\Entity\T090fassetType', 't', 'with', 'a.f085fidAssetType = t.f090fid')
            ->orderBy('a.f085fid','DESC')
            ->where('a.f085fid = :id')
            ->setParameter('id',(int)$id);
            

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f085fAcquisitionDate'];
            $row['f085fAcquisitionDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fapproveDate'];
            $row['f085fapproveDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fexpireDate'];
            $row['f085fexpireDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    public function getAssetInformation() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("a.f085fid, a.f085fassetCode, a.f085fassetDescription, a.f085fidDepartment, a.f085fidAssetCategory, a.f085fAcquisitionDate, a.f085forderNumber, a.f085fstoreCode, a.f085faccountCode, a.f085fapproveDate, a.f085fexpireDate, a.f085fidAssetType, a.f085finstallCost, a.f085faccumDeprCost, a.f085fnetValue, a.f085fyearDeprCost, a.f085fresidualValue, a.f085fstatus,a.f085fassetOwner, a.f085fbarCode, a.f085fserialNumber, a.f085fbrandName, a.f085fcurrentBuilding, a.f085fcurrentRoom, a.f085fcurrentBlock, a.f085fcurrentCampus, a.f085fsubCategory,a.f085fpoNumber, a.f085fgrnNumber, a.f085fsoCode, a.f085freferrer, a.f085fitemName, a.f085fidItem")
            ->from('Application\Entity\T085fassetInformation','a');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f085fAcquisitionDate'];
            $row['f085fAcquisitionDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fapproveDate'];
            $row['f085fapproveDate'] = date("Y-m-d", strtotime($date));

            $date                        = $row['f085fexpireDate'];
            $row['f085fexpireDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }
   
}

?>


