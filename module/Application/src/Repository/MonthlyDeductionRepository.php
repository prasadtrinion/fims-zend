<?php
namespace Application\Repository;

use Application\Entity\T082fmonthlyDeductionMaster;
use Application\Entity\T082fmonthlyDeductionDetail;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class MonthlyDeductionRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

         $qb->select('m.f082fid, m.f082fidStaff, m.f082fname, m.f082fstatus, s.f034fname as staffName')

            ->from('Application\Entity\T082fmonthlyDeductionMaster', 'm')
            ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId= m.f082fidStaff')
            ->orderBy('m.f082fid','DESC');

            
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('m.f082fid, m.f082fidStaff, m.f082fname, m.f082fstatus, s.f034fname as staffName, md.f082fidDetails, md.f082fidMaster, md.f082fdeductionCode, md.f082famount, md.f082fstartDate, md.f082fendDate, md.f082fcreatedBy, md.f082fcreatedDtTm, md.f082fupdatedBy, md.f082fupdatedDtTm, md.f082fstatus')

            ->from('Application\Entity\T082fmonthlyDeductionMaster', 'm')
            ->leftjoin('Application\Entity\T082fmonthlyDeductionDetail','md','with', 'm.f082fid= md.f082fidMaster')
            ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId= m.f082fidStaff')

            ->where('m.f082fid = :monthlyId')
            ->setParameter('monthlyId',$id);
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date = $item['f082fstartDate'];
            $item['f082fstartDate'] = date("Y-m-d", strtotime($date));

            $date = $item['f082fendDate'];
            $item['f082fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }   

        return $result1;

    }

    public function createNewData($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $monthlyDeduction = new T082fmonthlyDeductionMaster();

        $monthlyDeduction->setF082fidStaff((int)$data['f082fidStaff'])
             ->setF082fname($data['f082fname'])
             ->setF082fstatus((int)$data['f082fstatus'])
             ->setF082fcreatedBy((int)$_SESSION['userId'])
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $monthlyDeduction->setF082fcreatedDtTm(new \DateTime())
                ->setF082fupdatedDtTm(new \DateTime());
        
        $em->persist($monthlyDeduction);
        $em->flush();
        
        $monthlyDeductionDetails = $data['monthlyDeduction-details'];

        foreach ($monthlyDeductionDetails as $monthlyDeductionDetail) {

            $monthlyDetailObj = new T082fmonthlyDeductionDetail();

            $monthlyDetailObj->setF082fidMaster((int)$monthlyDeduction->getF082fid())
                       ->setF082fdeductionCode($monthlyDeductionDetail['f082fdeductionCode'])
                       ->setF082famount((float)$monthlyDeductionDetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($monthlyDeductionDetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($monthlyDeductionDetail['f082fendDate']))
                       ->setF082fstatus((int)"1")
                       ->setF082fcreatedBy((int)$_SESSION['userId'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

            $monthlyDetailObj->setF082fcreatedDtTm(new \DateTime())
                       ->setF082fupdatedDtTm(new \DateTime());
        
try{
        $em->persist($monthlyDetailObj);

        $em->flush();
}
catch(\Exception $e){
    echo $e;
}
            
        }
        return $monthlyDeduction;
    }

    public function updateData($monthlyDeduction, $data = []) 
    {
        $em = $this->getEntityManager();

         $monthlyDeduction->setF082fidStaff((int)$data['f082fidStaff'])
             ->setF082fname($data['f082fname'])
             ->setF082fstatus((int)$data['f082fstatus'])
             ->setF082fcreatedBy((int)$_SESSION['userId'])
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $monthlyDeduction->setF082fupdatedDtTm(new \DateTime());


        
        $em->persist($monthlyDeduction);
        $em->flush();

        return $monthlyDeduction;

    }

    public function updateMonthlyDetail($monthlyDetailObj, $monthlyDetail) 
    {

        $em = $this->getEntityManager();

        $monthlyDetailObj->setF082fdeductionCode($monthlyDetail['f082fdeductionCode'])
                       ->setF082famount((float)$monthlyDetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($monthlyDetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($monthlyDetail['f082fendDate']))
                       ->setF082fstatus((int)$monthlyDetail['f082fstatus'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

        $monthlyDetailObj->setF082fupdatedDtTm(new \DateTime());


        
        $em->persist($monthlyDetailObj);
        $em->flush();

        return $monthlyDetailObj;

    }

    public function createMonthlyDetail($monthlyDetailObj, $monthlydetail) 
    {
        $em = $this->getEntityManager();

        $monthlyObj = new T082fmonthlyDeductionDetail();

        $monthlyObj->setF082fidMaster((int)$monthlyDetailObj->getF082fid())
                       ->setF082fdeductionCode($monthlydetail['f082fdeductionCode'])
                       ->setF082famount($monthlydetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($monthlydetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($monthlydetail['f082fendDate']))
                       ->setF082fstatus((int)$monthlydetail['f082fstatus'])
                       ->setF082fcreatedBy((int)$_SESSION['userId'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

            $monthlyObj->setF082fcreatedDtTm(new \DateTime())
                       ->setF082fupdatedDtTm(new \DateTime());


        
        $em->persist($monthlyObj);
        $em->flush();

        return $monthlyObj;

    }

    public function deleteMonthlyDeductionDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t082fmonthly_deduction_detail WHERE f082fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    } 

}
