<?php
namespace Application\Repository;

use Application\Entity\T124fstockRegistration;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StockRegistrationRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('sr.f124fid, sr.f124fassetCategory, sr.f124fassetSubCategory, sr.f124fassetItem, sr.f124fstore, sr.f124fquantity, sr.f124fdate, sr.f124fstatus, ac.f087fcategoryDiscription, i.f088fitemDescription, ac1.f089fsubDescription, s.f097fstoreName ')
            ->from('Application\Entity\T124fstockRegistration', 'sr')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'ac.f087fid = sr.f124fassetCategory')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'ac1', 'with', 'ac1.f089fid = sr.f124fassetSubCategory')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'sr.f124fassetItem = i.f088fid')
            ->leftjoin('Application\Entity\T097fstoreSetup', 's', 'with', 's.f097fid = sr.f124fstore')
            ->orderBy('sr.f124fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f124fdate'];
            $item['f124fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('sr.f124fid, sr.f124fassetCategory, sr.f124fassetSubCategory, sr.f124fassetItem, sr.f124fstore, sr.f124fquantity, sr.f124fdate, sr.f124fstatus, ac.f087fcategoryDiscription, ac1.f089fsubDescription, i.f088fitemDescription, s.f097fstoreName ')
            ->from('Application\Entity\T124fstockRegistration', 'sr')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'ac.f087fid = sr.f124fassetCategory')
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'ac1', 'with', 'ac1.f089fid = sr.f124fassetSubCategory')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'sr.f124fassetItem = i.f088fid')
            ->leftjoin('Application\Entity\T097fstoreSetup', 's', 'with', 's.f097fid = sr.f124fstore')
            ->where('sr.f124fid = :stockId')
            ->setParameter('stockId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $item) {
            $date                       = $item['f124fdate'];
            $item['f124fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $stockregistration = new T124fstockRegistration();

        $stockregistration->setF124fassetCategory((int)$data['f124fassetCategory'])
                ->setF124fassetSubCategory((int)$data['f124fassetSubCategory'])
                ->setF124fassetItem((int)$data['f124fassetItem'])
                ->setF124fstore((int)$data['f124fstore'])
                ->setF124fquantity((int)$data['f124fquantity'])
                ->setF124fdate(new \DateTime($data['f124fdate']))
                ->setF124fstatus((int)$data['f124fstatus'])
                ->setF124fcreatedBy((int)$_SESSION['userId']);

        $stockregistration->setF124fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($stockregistration);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $stockregistration;
    }

    public function updateData($stockregistration, $data = []) 
    {
        $em = $this->getEntityManager();
        

        $stockregistration->setF124fassetCategory((int)$data['f124fassetCategory'])
                ->setF124fassetSubCategory((int)$data['f124fassetSubCategory'])
                ->setF124fassetItem((int)$data['f124fassetItem'])
                ->setF124fstore((int)$data['f124fstore'])
                ->setF124fquantity((int)$data['f124fquantity'])
                ->setF124fdate(new \DateTime($data['f124fdate']))
                ->setF124fstatus((int)$data['f124fstatus'])
                ->setF124fupdatedBy((int)$_SESSION['userId']);

        $stockregistration->setF124fupdatedDtTm(new \DateTime());

        $em->persist($stockregistration);
        $em->flush();
    }
}

