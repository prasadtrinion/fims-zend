<?php
namespace Application\Repository;

use Application\Entity\T017fjournal;
use Application\Entity\T018fjournalDetails;
use Application\Entity\T019fjournalMonthlySummary;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
// use Doctrine\ORM\DoctrineExtensions\src\Query\Mysql\Month;

class JournalMonthlySummaryRepository extends EntityRepository 
{

    public function getListById($id) 
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('j.f018fid, j.f018ffundCode,j.f018fdepartmentCode,j.f018factivityCode,j.f018faccountCode, j.f018fcrAmount, j.f018fdrAmount, j.f018fstatus, j.f018fmonth')
            ->from('Application\Entity\T019fjournalMonthlySummary','j')
            ->where('j.f018fid = :month')
            ->setParameter('month',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
    public function getList() 
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("j.f018fid, j.f018ffundCode,j.f018fdepartmentCode,j.f018factivityCode,j.f018faccountCode, j.f018fcrAmount, j.f018fdrAmount,  j.f018fstatus, j.f018fmonth,(j.f018ffundCode+'-'+f.f057fname) as Fund, (j.f018fdepartmentCode+'-'+dep.f015fdepartmentName) as Department, (j.f018faccountCode+'-'+acc.f059fname) as Account,(j.f018factivityCode+'-'+ac.f058fname) as Activity")
            ->from('Application\Entity\T019fjournalMonthlySummary','j')
            ->leftjoin('Application\Entity\T057ffund','f','with', 'j.f018ffundCode = f.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment','dep','with', 'j.f018fdepartmentCode = dep.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T059faccountCode','acc','with', 'j.f018faccountCode = acc.f059fcompleteCode')
            ->leftjoin('Application\Entity\T058factivityCode','ac','with', 'j.f018factivityCode = ac.f058fcompleteCode');
            // print_r($qb);die();
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function updateCrDrByMonth($month)

    {   
         // $months = array(  'January' => 1 ,
         //                  'February'=> 2, 
         //                  'March' => 3,
         //                  'April' => 4,
         //                  'May' => 5,
         //                  'June' => 6,
         //                  'July' => 7, 
         //                  'August' => 8,
         //                  'September' => 9,
         //                  'October' => 10,
         //                  'November' => 11,
         //                  'December' => 12);
         $idMonth = (int)$month;
        $glCodes = array();
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        

            $qb1 = $em->createQueryBuilder();
            $query = "select SUM(j.f018fcr_amount) as cr_sum,SUM(j.f018fdr_amount) as dr_sum,j.f018faccount_code  as f018faccountCode,j.f018factivity_code  as f018factivityCode,j.f018fdepartment_code as f018fdepartmentCode,j.f018ffund_code as f018ffundCode from t018fjournal_details as j where j.f018fstatus = 1 and month(j.f018fcreate_dt_tm) = $idMonth group by j.f018faccount_code,j.f018fdepartment_code,j.f018factivity_code,j.f018ffund_code";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
    // echo $query;
           // print_r($result);
           // die();
        //     $qb1->select('SUM(j.f018fcrAmount) as cr_sum,SUM(j.f018fdrAmount) as dr_sum,j.f018faccountCode,j.f018ffundCode')
        //     ->from('Application\Entity\T018fjournalDetails','j')
        //     ->Where("j.f018fstatus = 1")
        //     ->andwhere("MONTH(j.f018fcreateDtTm) = month")
        //     ->groupBy('j.f018faccountCode,j.f018ffundCode')
        //      ->setParameter('month', $idMonth);
             
        //     $query = $qb1->getQuery();
           
        // $result = $query->getResult();
    
        if($result){
            foreach($result as $summary){
        $journalMonthlySummary = new T019fjournalMonthlySummary();


        $journalMonthlySummary->setF018ffundCode( $summary['f018ffundCode'])
                     ->setF018faccountCode( $summary['f018faccountCode'])
                 ->setF018factivityCode( $summary['f018factivityCode'])
                     ->setF018fdepartmentCode( $summary['f018fdepartmentCode'])
                     ->setF018fmonth($month)
                              ->setF018fdrAmount((float)$summary['dr_sum'])
                              ->setF018fcrAmount((float)$summary['cr_sum'])
                              ->setF018fstatus((int)"1")
                              ->setF018fcreatedBy((int)$_SESSION['userId'])
                              ->setF018fupdatedBy((int)$_SESSION['userId']);
             
            $journalMonthlySummary->setF018fcreateDtTm(new \DateTime())
                             ->setF018fupdateDtTm(new \DateTime());
 
        $em->persist($journalMonthlySummary);
        $em->flush();
        }
    }
        
        $update_query = "UPDATE t018fjournal_details set f018fstatus = 2 where f018fstatus = 1";
        $em->getConnection()->executeQuery($update_query);

       
        return $journalMonthlySummary;
    
    }
    
}