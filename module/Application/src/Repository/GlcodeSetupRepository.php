<?php
namespace Application\Repository;

use Application\Entity\T140fglcodeSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class GlcodeSetupRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("g.f140fid, g.f140fidVoucherType, g.f140fcreditFund, g.f140fcreditDepartment, g.f140fcreditActivity, g.f140fcreditAccount, g.f140fdebitFund, g.f140fdebitDepartment, g.f140fdebitActivity, g.f140fdebitAccount, g.f140fstatus, d.f011fdefinitionCode")
            ->from('Application\Entity\T140fglcodeSetup', 'g')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'g.f140fidVoucherType = d.f011fid')

            ->orderBy('g.f140fid','DESC');
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array(
            'data' => $result,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("g.f140fid, g.f140fidVoucherType, g.f140fcreditFund, g.f140fcreditDepartment, g.f140fcreditActivity, g.f140fcreditAccount, g.f140fdebitFund, g.f140fdebitDepartment, g.f140fdebitActivity, g.f140fdebitAccount, g.f140fstatus, d.f011fdefinitionCode")
            ->from('Application\Entity\T140fglcodeSetup', 'g')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'g.f140fidVoucherType = d.f011fid')
            ->where('g.f140fid = :Id')
            ->setParameter('Id',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
     
       
        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $glcode = new T140fglcodeSetup();

        $glcode->setF140fidVoucherType((int)$data['f140fidVoucherType'])
                ->setF140fcreditFund($data['f140fcreditFund'])
                ->setF140fcreditActivity($data['f140fcreditActivity'])
                ->setF140fcreditDepartment($data['f140fcreditDepartment'])
                ->setF140fcreditAccount($data['f140fcreditAccount'])
                ->setF140fdebitFund($data['f140fdebitFund'])
                ->setF140fdebitActivity($data['f140fdebitActivity'])
                ->setF140fdebitDepartment($data['f140fdebitDepartment'])
                ->setF140fdebitAccount($data['f140fdebitAccount'])
                ->setF140fstatus((int)$data['f140fstatus'])
                ->setF140fcreatedBy((int)$_SESSION['userId'])
                ->setF140fupdatedBy((int)$_SESSION['userId']);

        $glcode->setF140fcreatedDtTm(new \DateTime())
                ->setF140fupdatedDtTm(new \DateTime());


        try{
            $em->persist($glcode);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $glcode;
    }

    public function updateData($glcode, $data = []) 
    {
        $em = $this->getEntityManager();

        $glcode->setF140fidVoucherType((int)$data['f140fidVoucherType'])
                ->setF140fcreditFund($data['f140fcreditFund'])
                ->setF140fcreditActivity($data['f140fcreditActivity'])
                ->setF140fcreditDepartment($data['f140fcreditDepartment'])
                ->setF140fcreditAccount($data['f140fcreditAccount'])
                ->setF140fdebitFund($data['f140fdebitFund'])
                ->setF140fdebitActivity($data['f140fdebitActivity'])
                ->setF140fdebitDepartment($data['f140fdebitDepartment'])
                ->setF140fdebitAccount($data['f140fdebitAccount'])
                ->setF140fstatus((int)$data['f140fstatus'])
                ->setF140fupdatedBy((int)$_SESSION['userId']);

        $glcode->setF140fupdatedDtTm(new \DateTime());
        
        $em->persist($glcode);
        $em->flush();

    }

}
