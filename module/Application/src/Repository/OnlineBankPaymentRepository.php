<?php
namespace Application\Repository;

use Application\Entity\T078fonlineBankPayment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class OnlineBankPaymentRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('b.f042fbankName,o.f078fidBank,o.f078ffileName,o.f078fdate,SUM(o.f078famount) as f078famount,o.f078fstatus')
            ->from('Application\Entity\T078fonlineBankPayment', 'o')
            ->leftjoin('Application\Entity\T042fbank','b','with', 'o.f078fidBank = b.f042fid')
            ->groupby('o.f078fidBank,o.f078fdate');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f078transactionDate'];
            $row['f078transactionDate'] = date("Y-m-d", strtotime($date));
            $date                        = $row['f078fdate'];
            $row['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $row);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

     public function onlineBankPaymentByApprove($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('b.f042fbankName,o.f078fidBank,o.f078ffileName,o.f078fdate,SUM(o.f078famount) as f078famount,o.f078fstatus')
            ->from('Application\Entity\T078fonlineBankPayment', 'o')
            ->leftjoin('Application\Entity\T042fbank','b','with', 'o.f078fidBank = b.f042fid')
            ->where('o.f078fstatus = :status')
            ->groupby('o.f078fidBank,o.f078fdate')
            ->setParameter('status',$id);
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f078transactionDate'];
            $row['f078transactionDate'] = date("Y-m-d", strtotime($date));
            $date                        = $row['f078fdate'];
            $row['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $row);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($date,$bank) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('o.f078fid,b.f042fbankName,o.f078ffileName,o.f078fidBank,o.f078ftype,o.f078fbillType,o.f078fmatricNumber,o.f078transactionDate,o.f078fdate,o.f078freferenceNumber,o.f078freferenceNumber1,o.f078famount,o.f078ficNumber,o.f078fstatus,s.f060fname as studentName')
            ->from('Application\Entity\T078fonlineBankPayment', 'o')
            ->leftjoin('Application\Entity\T042fbank','b','with', 'o.f078fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T060fstudent','s','with', 'o.f078fmatricNumber = s.f060fmatricNumber')

            ->where('o.f078fidBank = :bankId')
            ->andWhere('o.f078fdate = :date')
            ->setParameter('bankId',$bank)
            ->setParameter('date',new \DateTime($date));
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
     
        $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f078transactionDate'];
            $row['f078transactionDate'] = date("Y-m-d", strtotime($date));
            $date                        = $row['f078fdate'];
            $row['f078fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $row);
        }

        return $result1;
    }

    public function createNewData($postData) 
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        foreach ($postData['payments'] as $data) {
            
        $payment = new T078fonlineBankPayment();

        $payment->setF078fbillType($data['billType'])
                ->setF078ffileName($data['fileName'])
                ->setF078fmatricNumber($data['matricNumber'])
                ->setF078ftype($postData['type'])
                ->setF078fidBank((int)$postData['idBank'])
                ->setF078transactionDate(new \DateTime($data['date']))
                ->setF078famount((float)$data['amount'])
                ->setF078freferenceNumber($data['referenceNumber'])
                ->setF078freferenceNumber1($data['referenceNumber1'])
                ->setF078ficNumber($data['passportNumber'])
                ->setF078fdate(new \DateTime($postData['date']))
                ->setF078fstatus((int)"0")
                ->setF078fcreatedBy((int)$_SESSION['userId'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $payment->setF078fcreatedDtTm(new \DateTime())
                ->setF078fupdatedDtTm(new \DateTime());


        try{
            $em->persist($payment);
            
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }
        return $payment;
    }

    public function fileRead($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $idBank = $data['f078fidBank'];
        $date = $data['f078fdate'];  
        $type = $data['f078ftype'];  
        $name = $data['f078ffileName'];
        $myfile = fopen("/var/www/html/fims-zend/public/files/".$name, "w");
        fwrite($myfile, $data['fileData']);
        fclose($myfile);
        $lines = file("/var/www/html/fims-zend/public/files/".$name);
        $line_count = 0;
        $payments = array();
        $knocked_invoices = array();
        $year = date('y');
                         $query  = "select * from t078fonline_bank_payment";
                        $result = $em->getConnection()->executeQuery($query)->fetchAll();
                        $count  = count($result) + 1;
        foreach ($lines as $line) {
            if($line_count == 0){
                 if ($type == "FPX") {
                    $payment['payeeCode'] = substr($line, 0, 3);
                    $date                 = substr($line, 3, 2) . '-' . substr($line, 5, 2) . '-' . substr($line, 7, 4);
                    $payment['tmDate']    = $date;
                    $payment['totRec']    = substr($line, 11, 11);
                    $payment['totAmount'] = substr($line, 22, 10);
                }
            }
            else{
                if($type == "Bill Presentment" || $type == "Bill Payment")
                {
                    if($idBank == "CIMB Bank Berhad" || $idBank == "Bank Islam Malaysia"){
                        if($idBank == "Bank Islam Malaysia"){
                            $max_count = 4;
                        }
                        else{
                            $max_count = 20;
                        }
                        $payment['date'] = substr($line, 14,8);
                        $payment['fileName'] = $name;
                        $payment['matricNumber'] = substr($line, 7,6);
                        $payment['passportNumber'] = substr($line, 56,13);
                        $payment['name'] = 'NULL';//substr($line, 81,40);
                        $payment['amount'] = (float)substr($line, 40,15)/100;
                        $payment['billType'] = substr($line, 0,7);
                        $payment['referenceNumber'] = substr($line, 23,16);
                        
                        $number = "OLP00000" . $count++ . "/" . $year;
                         $payment['referenceNumber1'] = $number;
                        $tmp_matric = $payment['matricNumber'];
                        $tmp_amount = $payment['amount'];
                    }
                    else {
                        $max_count = 21;
                        $payment['date'] = substr($line, 0,8);
                        $payment['fileName'] = $name;
                        $payment['matricNumber'] = substr($line, 28,6);
                        $payment['passportNumber'] = substr($line, 68,13);
                        $payment['name'] = substr($line, 81,40);
                        $payment['amount'] = (float)substr($line, 121,10)/100;
                        $payment['billType'] = substr($line, 131,10);
                        $payment['referenceNumber'] = substr($line, 141,7);
                         $number = "OLP00000" . $count++ . "/" . $year;
                         $payment['referenceNumber1'] = $number;

                        $tmp_matric = $payment['matricNumber'];
                        $tmp_amount = $payment['amount'];
                    
               }
           }
           else{
                if($type == "FPX"){
                       $date = date('dd-mm-yy',strtotime(substr($line, 0,2) .'-'.substr($line, 2,2).'-'.substr($line, 4,4)));
                    $payment['details'][$line_count-1]['tmDate2'] = $date;
                    $payment['details'][$line_count-1]['billRef1'] = substr($line,8,20);
                    $payment['details'][$line_count-1]['billRef2'] = substr($line,28,20);
                    $payment['details'][$line_count-1]['billRef3'] = substr($line,48,20);
                    $payment['details'][$line_count-1]['icNumber'] = substr($line,68,13);
                    $payment['details'][$line_count-1]['name'] = substr($line,81,40);
                    $payment['details'][$line_count-1]['amount'] = substr($line,121,10);
                    $payment['details'][$line_count-1]['billType'] = substr($line,131,10);
                }
                
           }
                 
             if($type =='FPX'){
                $payments = $payment;
            }
            else{
                array_push($payments,$payment);

            }
            }
            $line_count++;
            if($line_count == $max_count){
                break;
            }

        }

        for ($i=0;$i<count($payments);$i++) {
            $day = substr($line, 0,2);
            $month = substr($line, 2,2);
            $year = substr($line, 4,4);
            $tmp_date = $day.'-'.$month.'-'.$year;
           $payments[$i]['date'] = date("Y-m-d",strtotime($tmp_date));
        }
       
        $newdata['idBank'] = $data['f078fidBank'];
           $newdata['date'] = $data['f078fdate'];
           $newdata['type'] = $data['f078ftype'];
           $newdata['fileName'] = $data['f078ffileName'];
           $newdata['payments'] = $payments;
        
        $this->createNewData($newdata);

    }

    public function updateData($payment, $data = []) 
    {
        $em = $this->getEntityManager();

        $payment->setF078fbillType($data['f078fbillType'])
        ->setF078fmatricNumber($data['f078fmatricNumber'])
        ->setF078fidBank((int)$data['f078fidBank'])
        ->setF078transactionDate(new \DateTime($data['f078ftransactionDate']))
        ->setF078famount((float)$data['f078famount'])
        ->setF078freferenceNumber($data['f078freferenceNumber'])
        ->setF078ficNumber($data['f078ficNumber'])
        ->setF078fdate(new \DateTime($data['f078fdate']))
        ->setF078fstatus((int)$data['f078fstatus'])
        ->setF078fupdatedBy((int)$_SESSION['userId']);

        $payment->setF078fupdatedDtTm(new \DateTime());


        
        $em->persist($payment);
        $em->flush();

    }
   
    public function approve($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id) {
           try{
            $date = $id['f078fdate'];
            $bank = $id['f078fidBank'];
            $query1 = "update t078fonline_bank_payment set f078fstatus = 1 where f078fdate = '$date' and f078fid_bank = $bank";
            $em->getConnection()->executeQuery($query1);
        }
        catch(\Exception $e){
            echo $e;
          
        }
        }
    }

     public function knockOnlinePayments($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $row) {
            $fileName = $row['f078ffileName'];
            $date = $row['f078fdate'];
           try{
            $query1 = "select f078fmatric_number from t078fonline_bank_payment where f078ffile_name = '$fileName' and f078fdate = '$date'";
             $payment_result = $em->getConnection()->executeQuery($query1)->fetchAll();
             
             foreach($payment_result as $row){

             $metric_number = $row['f078fmatric_number'];
           
            $query1 = "select f071fid,f071finvoice_number,f078famount,f071fid_customer,f071finvoice_total,f060fname,f060fmatric_number from t060fstudent inner join t071finvoice on f060fid = f071fid_customer inner join t078fonline_bank_payment on f060fmatric_number = f078fmatric_number where f071finvoice_type = 'STD' and f060fmatric_number = '$metric_number' ";
                
             $invoice_result = $em->getConnection()->executeQuery($query1)->fetchAll();
             // print_r($invoice_result);
             // die();
             foreach ($invoice_result as $invoice) {
                $tmp_invoice_amount = $invoice['f071finvoice_total'];
                $tmp_amount = $invoice['f078famount'];
                $balance_invoice_amount = $tmp_invoice_amount - $tmp_amount;
                $id_invoice = $invoice['f071fid'];
                $query1 = "update t071finvoice set f071fonline_payment = $tmp_amount,f071fbalance = $balance_invoice_amount where f071fid = $id_invoice";
                 $em->getConnection()->executeQuery($query1);
                 $query1 = "update t078fonline_bank_payment set f078fstatus = 2 where f078ffile_name = '$fileName' and f078fdate = '$date'";
                 $em->getConnection()->executeQuery($query1);
                 // array_push($knocked_invoices, $invoice);
                }
            }
        }
        catch(\Exception $e){
            echo $e;
          
        }
        }
    }
}