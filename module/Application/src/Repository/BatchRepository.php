<?php
namespace Application\Repository;

use Application\Entity\T050fbatchMaster;
use Application\Entity\T051fbatchDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class BatchRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f050fid,b.f050fbatchNumber,b.f050fdate,b.f050freferenceNumber,b.f050fpaymentBank,b.f050fstatus,bs.f041fdescription,ba.f042fbankName,b.f050fpaymentType')
            ->from('Application\Entity\T050fbatchMaster', 'b')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'bs', 'with', 'b.f050fpaymentBank = bs.f041fid')
            ->leftjoin('Application\Entity\T042fbank', 'ba', 'with', 'bs.f041fidBank = ba.f042fid')
            ->orderby('b.f050fid','DESC');
        
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $batch) {
            $date                        = $batch['f050fdate'];
            $batch['f050fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $batch);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('b.f050fid,b.f050fbatchNumber,b.f050fdate,b.f050freferenceNumber,b.f050fpaymentBank,b.f050fstatus,bd.f051fid,bd.f051fidBatch,bd.f051fidReceipt,b.f050fpaymentType')
            ->from('Application\Entity\T050fbatchMaster', 'b')
            ->leftjoin('Application\Entity\T051fbatchDetails', 'bd', 'with', 'bd.f051fidBatch = b.f050fid')
            ->where('b.f050fid = :batchId')
            ->setParameter('batchId',$id);
           
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $batch) {
            $date                        = $batch['f050fdate'];
            $batch['f050fdate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $batch);
        }
        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='BCH';
        $number = $configRepository->generateFIMS($numberData);
        $batchMaster = new T050fbatchMaster();
 
        $batchMaster->setF050fbatchNumber($data['f050fbatchNumber'])
            ->setF050fdate(new \DateTime($data['f050fdate']))
            ->setF050freferenceNumber($number)
            ->setF050fpaymentBank((int)$data['f050fpaymentBank'])
            ->setF050fpaymentType((int)$data['f050fpaymentType'])
            ->setF050fstatus((int)$data['f050fstatus'])
            ->setF050fcreatedBy((int)$_SESSION['userId'])
            ->setF050fupdatedBy((int)$_SESSION['userId']);

        $batchMaster->setF050fcreateDtTm(new \DateTime())
                ->setF050fupdateDtTm(new \DateTime());
            try{
                $em->persist($batchMaster);
                $em->flush();
            }
            catch(\Exception $e){
                echo $e;
            }
            $batchDetails = $data['batch-details'];
            foreach($batchDetails as $detail){
            $batchdetail = new T051fbatchDetails();

        $batchdetail->setF051fidBatch((int)$batchMaster->getF050fid())
            ->setF051fidReceipt((int)$detail['f051fidReceipt'])
            ->setF051fstatus((int) $data['f050fstatus'])
            ->setF051fcreatedBy((int)$_SESSION['userId'])
            ->setF051fupdatedBy((int)$_SESSION['userId']);

        $batchdetail->setF051fcreateDtTm(new \DateTime())
                ->setF051fupdateDtTm(new \DateTime());


        try{
            $em->persist($batchdetail);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    }
        
    return $batchMaster;

}

    public function updateData($tender, $data = []) 
    {
        $em = $this->getEntityManager();

        $tender->setF037fidTender((int)$data['f037fidTender'])
        ->setF037fapplied((int)$data['f037fapplied'])
        ->setF037fstatus((int) $data['f037fstatus'])
        ->setF037fshortlisted((int) $data['f037fshortlisted'])
        ->setF037ffinalised((int) $data['f037ffinalised'])
        ->setF037fidSupplier((int) $data['f037fidSupplier'])
        ->setF037fupdatedBy((int)$_SESSION['userId']);

    $tender->setF037fupdateDtTm(new \DateTime());


        
        $em->persist($tender);
        $em->flush();

    }

    public function getNonBatchReceipts($postData) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $date = date("Y-m-d H:i:s",strtotime($postData['date']));
        $payment_bank = $postData['paymentBank'];
           
        $query2 = "select f082fid,f082freceipt_number from t082freceipt where f082fdate='$date' and f082fpayment_bank=$payment_bank and f082fid not in (select f051fid_receipt from t051fbatch_details)  ";
        // echo $query2;
        // die();
        $result = $em->getConnection()->executeQuery($query2)->fetchAll();
      
        // $result1 = array();
        // foreach ($result as $batch) {
        //     $date                        = $batch['f050fdate'];
        //     $batch['f050fdate'] = date("Y-m-d", strtotime($date));
           
        //     array_push($result1, $batch);
        // }
        return $result;
    }

   

}
