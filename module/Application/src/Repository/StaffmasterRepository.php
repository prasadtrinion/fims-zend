<?php
namespace Application\Repository;

use Application\Entity\Staffmaster;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StaffmasterRepository extends EntityRepository 
{



    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('g.id','g.name','g.email','g.addressone','g.phone')
            ->from('Application\Entity\Staffmaster', 'g');


        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    
    }

   


}
