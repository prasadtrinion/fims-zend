<?php
namespace Application\Repository;

use Application\Entity\T053fsemester;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class SemesterRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('s.f053fid, s.f053fsemester,s.f053fgraduationType, s.f053fcode, s.f053fstartDate, s.f053fendDate, s.f053fsemSequence, a.f011fdefinitionCode as semesterSequence, s.f053fstatus, s.f053ftype, d.f011fdefinitionCode as semesterType')
          ->from('Application\Entity\T053fsemester','s')
          ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = s.f053ftype')
          ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with', 'a.f011fid = s.f053fsemSequence')
          ->orderBy('s.f053fid','DESC');

        
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f053fstartDate'];
            $item['f053fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f053fendDate'];
            $item['f053fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }
    public function getSemesters($type) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select("s.f053fid, s.f053fsemester,s.f053fgraduationType, s.f053fcode,(s.f053fcode +'-'+s.f053fsemester) as originalName, s.f053fstartDate, s.f053fendDate, s.f053fsemSequence, a.f011fdefinitionCode as semesterSequence, s.f053fstatus, s.f053ftype, d.f011fdefinitionCode as semesterType")
          ->from('Application\Entity\T053fsemester','s')
          ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = s.f053ftype')
          ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with', 'a.f011fid = s.f053fsemSequence')
          ->where("s.f053fgraduationType = '".$type."'");

        
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f053fstartDate'];
            $item['f053fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f053fendDate'];
            $item['f053fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('s.f053fid, s.f053fsemester,s.f053fgraduationType, s.f053fcode, s.f053fstartDate, s.f053fendDate, s.f053fsemSequence, a.f011fdefinitionCode as semesterSequence, s.f053fstatus, s.f053ftype, d.f011fdefinitionCode as semesterType')
          ->from('Application\Entity\T053fsemester','s')
          ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = s.f053ftype')
          ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with', 'a.f011fid = s.f053fsemSequence')
             ->where('s.f053fid = :semesterId')
            ->setParameter('semesterId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f053fstartDate'];
            $item['f053fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f053fendDate'];
            $item['f053fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $semester = new T053fsemester();

        $semester->setF053fsemester($data['f053fsemester'])
                 ->setF053fcode($data['f053fcode'])
                 ->setF053fgraduationType($data['f053fgraduationType'])
                 ->setF053fstartDate(new \DateTime($data['f053fstartDate']))
                 ->setF053fendDate(new \DateTime($data['f053fendDate']))
                 ->setF053fsemSequence((int)$data['f053fsemSequence'])
                 ->setF053ftype((int)$data['f053ftype'])
                 // ->setF053ffasa("NULL")
                 ->setF053fstatus((int)$data['f053fstatus'])
                 ->setF053fcreatedBy((int)$_SESSION['userId'])
                 ->setF053fupdatedBy((int)$_SESSION['userId']);

        $semester->setF053fcreatedDtTm(new \DateTime())
                ->setF053fupdatedDtTm(new \DateTime());

 
        $em->persist($semester);
        $em->flush();
        return $semester;

    }

    public function updateData($semester, $data = []) 
    {
        $em = $this->getEntityManager();

        // $qb = $em->createQueryBuilder();

       $semester->setF053fsemester($data['f053fsemester'])
                 ->setF053fgraduationType($data['f053fgraduationType'])
                 ->setF053fcode($data['f053fcode'])
                 ->setF053fstartDate(new \DateTime($data['f053fstartDate']))
                 ->setF053fendDate(new \DateTime($data['f053fendDate']))
                 ->setF053fsemSequence($data['f053fsemSequence'])
                 ->setF053ftype($data['f053ftype'])
                 ->setF053fstatus((int)$data['f053fstatus'])
                 ->setF053fcreatedBy((int)$_SESSION['userId'])
                 ->setF053fupdatedBy((int)$_SESSION['userId']);

        $semester->setF053fupdatedDtTm(new \DateTime());
        
        $em->persist($semester);
        $em->flush();

    }
}
