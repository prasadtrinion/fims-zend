<?php
namespace Application\Repository;

use Application\Entity\T086fpurchaseLimit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PurchaseLimitRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f086fid, f.f086ftype, o.f081forderType, f.f086fminAmount, f.f086fmaxAmount, f.f086fstatus')
            ->from('Application\Entity\T086fpurchaseLimit','f')
            ->leftjoin('Application\Entity\T081forderType', 'o', 'with', 'f.f086ftype = o.f081fid')
            ->orderBy('f.f086fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f.f086fid, f.f086ftype, o.f081forderType, f.f086fminAmount, f.f086fmaxAmount, f.f086fstatus')
            ->from('Application\Entity\T086fpurchaseLimit','f')
            ->leftjoin('Application\Entity\T081forderType', 'o', 'with', 'f.f086ftype = o.f081fid')
            ->where('f.f086fid = :purchaseId')
            ->setParameter('purchaseId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $purchase = new T086fpurchaseLimit();

        $purchase->setF086ftype((int)$data['f086ftype'])
             ->setF086fminAmount((float)$data['f086fminAmount'])
             ->setF086fmaxAmount((float)$data['f086fmaxAmount'])
             ->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fcreatedBy((int)$_SESSION['userId'])
             ->setF086fupdatedBy((int)$_SESSION['userId']);

        $purchase->setF086fcreatedDtTm(new \DateTime())
                ->setF086fupdatedDtTm(new \DateTime());
               
                try{
        $em->persist($purchase);
        
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }

        return $purchase;

    }

    /* to edit the data in database*/

    public function updateData($purchase, $data = []) 
    {
        $em = $this->getEntityManager();

        $purchase->setF086ftype((int)$data['f086ftype'])
             ->setF086fminAmount((float)$data['f086fminAmount'])
             ->setF086fmaxAmount((float)$data['f086fmaxAmount'])
             ->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fcreatedBy((int)$_SESSION['userId'])
             ->setF086fupdatedBy((int)$_SESSION['userId']);

        $purchase->setF086fcreatedDtTm(new \DateTime())
                ->setF086fupdatedDtTm(new \DateTime());

        try{
        $em->persist($purchase);
        
        $em->flush();
 }
 catch(\Exception $e){
    echo $e;
 }

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    
}

?>