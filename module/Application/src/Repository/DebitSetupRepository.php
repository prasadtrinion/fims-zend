<?php
namespace Application\Repository;

use Application\Entity\T040fdebitSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DebitSetupRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f040fid, c.f040fname, d.f011fdefinitionCode as name, c.f040fidGlcode, g.f014fglCode as GlCode, c.f040fstatus')
            ->from('Application\Entity\T040fdebitSetup','c')
            ->leftjoin('Application\Entity\T014fglcode', 'g','with','c.f040fidGlcode = g.f014fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','c.f040fname = d.f011fid')
            ->orderBy('f040fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f040fid, c.f040fname, d.f011fdefinitionCode as name, c.f040fidGlcode, g.f014fglCode as GlCode, c.f040fstatus')
            ->from('Application\Entity\T040fdebitSetup','c')
            ->leftjoin('Application\Entity\T014fglcode', 'g','with','c.f040fidGlcode = g.f014fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','c.f040fname = d.f011fid')
            ->where('c.f040fid = :debitId')
            ->setParameter('debitId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $debit = new T040fdebitSetup();

        $debit->setF040fname((int)$data['f040fname'])
             ->setF040fidGlcode((int)$data['f040fidGlcode'])
             ->setF040fstatus($data['f040fstatus'])
             ->setF040fcreatedBy((int)$_SESSION['userId'])
             ->setF040fupdatedBy((int)$_SESSION['userId']);

        $debit->setF040fcreatedDtTm(new \DateTime())
                ->setF040fupdatedDtTm(new \DateTime());
               

        $em->persist($debit);
        
        $em->flush();


        return $debit;

    }

    /* to edit the data in database*/

    public function updateData($debit, $data = []) 
    {
        $em = $this->getEntityManager();

        $debit->setF040fname((int)$data['f040fname'])
             ->setF040fidGlcode((int)$data['f040fidGlcode'])
             ->setF040fupdatedBy((int)$_SESSION['userId']);

        $debit->setF040fupdatedDtTm(new \DateTime());

        
        $em->persist($debit);
        
        $em->flush();
 

    }


    
}

?>