<?php
namespace Application\Repository;

use Application\Entity\T066ffeeItem;
use Application\Entity\T066ffeeItemDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class FeeItemRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("f.f066fid, f.f066fname, f.f066fcode, f.f066fdescription, f.f066fidFeeCategory, c.f065fname as feeCategory, f.f066fidTaxCode, t.f081fcode as GST, f.f066finsuredItem, f.f066fsoCode, f.f066frefundable, f.f066finvoiceItem, f.f066fsoa, f.f066fstatus, (f.f066fcode+'-'+f.f066fname) as Feename ")
            ->from('Application\Entity\T066ffeeItem', 'f')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'c', 'with','f.f066fidFeeCategory = c.f065fid')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with','f.f066fidTaxCode = t.f081fid')
            ->orderBy('f.f066fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('f.f066fid, f.f066fname, f.f066fcode, f.f066fdescription, f.f066fidFeeCategory, f.f066fidTaxCode, f.f066finsuredItem, f.f066fsoCode, f.f066frefundable, f.f066finvoiceItem, f.f066fsoa, f.f066fstatus, t.f081fcode as GST, c.f065fname as feeCategory, fd.f066fidDetails, fd. f066fcrFund, fd.f066fcrActivity, fd.f066fcrAccount, fd.f066fcrDepartment, fd.f066fdrFund, fd.f066fdrActivity, fd.f066fdrDepartment, fd.f066fdrAccount ')
            ->from('Application\Entity\T066ffeeItem', 'f')
            ->leftjoin('Application\Entity\T066ffeeItemDetails', 'fd', 'with','f.f066fid = fd.f066fidFeeItem')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'c', 'with','f.f066fidFeeCategory = c.f065fid')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with','f.f066fidTaxCode = t.f081fid')
            ->where('f.f066fid = :feeItemId')
            ->setParameter('feeItemId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $feeItem = new T066ffeeItem();

        $feeItem->setF066fname($data['f066fname'])
                ->setF066fcode($data['f066fcode'])
                ->setF066fdescription($data['f066fdescription'])
                ->setF066fidFeeCategory((int)$data['f066fidFeeCategory'])
                ->setF066fidTaxCode((int)$data['f066fidTaxCode'])
                ->setF066fsoCode($data['f066fsoCode'])
                ->setF066finsuredItem($data['f066finsuredItem'])
                ->setF066finvoiceItem((int)$data['f066finvoiceItem'])
                ->setF066frefundable((int)$data['f066frefundable'])
                ->setF066fsoa((int)$data['f066fsoa'])
                ->setF066fstatus((int)$data['f066fstatus'])
                ->setF066fcreatedBy((int)$_SESSION['userId'])
                ->setF066fupdatedBy((int)$_SESSION['userId']);

        $feeItem ->setF066fcreatedDtTm(new \DateTime())
                ->setF066fupdatedDtTm(new \DateTime());
	try{
        $em->persist($feeItem);
	$em->flush();
	      
}
	catch(\Exception $e){
	echo $e;
}

$feeItemDetails = $data['fee-details'];
foreach ($feeItemDetails as $detail) {
    $feeItemDetailobj = new T066ffeeItemDetails();

    $feeItemDetailobj->setF066fidFeeItem((int)$feeItem->getF066fid())
                     ->setF066fdrFund($detail['f066fdrFund'])
                     ->setF066fdrAccount($detail['f066fdrAccount'])
                     ->setF066fdrActivity($detail['f066fdrActivity'])
                     ->setF066fdrDepartment($detail['f066fdrDepartment'])
                     ->setF066fcrFund($detail['f066fcrFund'])
                     ->setF066fcrAccount($detail['f066fcrAccount'])
                     ->setF066fcrActivity($detail['f066fcrActivity'])
                     ->setF066fcrDepartment($detail['f066fcrDepartment'])
                     ->setF066fstatus((int)$detail['f066fstatus'])
                     ->setF066fcreatedBy((int)$_SESSION['userId'])
                     ->setF066fupdatedBy((int)$_SESSION['userId']);

    $feeItemDetailobj ->setF066fcreatedDtTm(new \DateTime())
                ->setF066fupdatedDtTm(new \DateTime());
    try{
        $em->persist($feeItemDetailobj);
    $em->flush();
          
}
    catch(\Exception $e){
    echo $e;
}
}
        return $feeItem;
    }

    public function updateData($feeItem, $data = []) 
    {
        $em = $this->getEntityManager();

        $feeItem->setF066fname($data['f066fname'])
                ->setF066fcode($data['f066fcode'])
                ->setF066fdescription($data['f066fdescription'])
                ->setF066fidFeeCategory((int)$data['f066fidFeeCategory'])
                ->setF066fidTaxCode((int)$data['f066fidTaxCode'])
                ->setF066fsoCode($data['f066fsoCode'])
                ->setF066finsuredItem($data['f066finsuredItem'])
                ->setF066finvoiceItem((int)$data['f066finvoiceItem'])
                ->setF066frefundable((int)$data['f066frefundable'])
                ->setF066fsoa((int)$data['f066fsoa'])
                ->setF066fstatus((int)$data['f066fstatus'])
                ->setF066fupdatedBy((int)$_SESSION['userId']);

        $feeItem->setF066fupdatedDtTm(new \DateTime());


        
        $em->persist($feeItem);
        $em->flush();
        return $feeItem;

    }

    public function updateDetail($feeItemDetailobj, $detail = []) 
    {
        $em = $this->getEntityManager();

    $feeItemDetailobj->setF066fdrFund($detail['f066fdrFund'])
                     ->setF066fdrAccount($detail['f066fdrAccount'])
                     ->setF066fdrActivity($detail['f066fdrActivity'])
                     ->setF066fdrDepartment($detail['f066fdrDepartment'])
                     ->setF066fcrFund($detail['f066fcrFund'])
                     ->setF066fcrAccount($detail['f066fcrAccount'])
                     ->setF066fcrActivity($detail['f066fcrActivity'])
                     ->setF066fcrDepartment($detail['f066fcrDepartment'])
                     ->setF066fstatus((int)$detail['f066fstatus'])
                     ->setF066fupdatedBy((int)$_SESSION['userId']);

    $feeItemDetailobj->setF066fupdatedDtTm(new \DateTime());
    try{
        $em->persist($feeItemDetailobj);
        $em->flush();
          
        }
        catch(\Exception $e)
        {
        echo $e;
        }

    }

    public function createDetail($feeItem, $detail = []) 
    {
        $em = $this->getEntityManager();

        $feeItemDetailobj = new T066ffeeItemDetails();

    $feeItemDetailobj->setF066fidFeeItem((int)$feeItem->getF066fid())
                     ->setF066fdrFund($detail['f066fdrFund'])
                     ->setF066fdrAccount($detail['f066fdrAccount'])
                     ->setF066fdrActivity($detail['f066fdrActivity'])
                     ->setF066fdrDepartment($detail['f066fdrDepartment'])
                     ->setF066fcrFund($detail['f066fcrFund'])
                     ->setF066fcrAccount($detail['f066fcrAccount'])
                     ->setF066fcrActivity($detail['f066fcrActivity'])
                     ->setF066fcrDepartment($detail['f066fcrDepartment'])
                     ->setF066fstatus((int)$detail['f066fstatus'])
                     ->setF066fcreatedBy((int)$_SESSION['userId'])
                     ->setF066fupdatedBy((int)$_SESSION['userId']);

    $feeItemDetailobj ->setF066fcreatedDtTm(new \DateTime())
                ->setF066fupdatedDtTm(new \DateTime());
        try{
        $em->persist($feeItemDetailobj);
        $em->flush();
          
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }
    public function getFeeItemByCategory($category)
    {
        $categoryId = (int)$category;
        $query = "select f.f066fid,f.f066fcode, f.f066fname,  f.f066fdescription, f.f066fid_fee_category, f.f066fid_tax_code, f.f066frefundable, f.f066fstatus, f.f066fcreated_by, f.f066finsured_item, f.f066fso_code, f.f066finvoice_item, f.f066fsoa, fc.f065fcode, fc.f065fname, fc.f065fdescription from t066ffee_item f inner join t065ffee_category fc on fc.f065fid = f.f066fid_fee_category where f.f066fid_fee_category = $categoryId ";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;
    }

    public function deleteFeeItemDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t066ffee_item_details WHERE f066fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function getCreditDebitByFee($FeeId)
    {
      // print_r($FeeId);exit;
        $FeeId = (int)$FeeId;
        $query = "select (f.f066fcr_fund+'-'+fcr.f057fname) as creditFund, f.f066fcr_fund, (f.f066fcr_activity+'-'+actcr.f058fname) creditActivity, f.f066fcr_activity, (f.f066fcr_account+'-'+acocr.f059fname) as creditAccount,  f.f066fcr_account, (f.f066fcr_department+'-'+dcr.f015fdepartment_name) as creditDepartment, f.f066fcr_department, (f.f066fdr_fund+'-'+fdr.f057fname) as debitFund, f.f066fdr_fund, (f.f066fdr_activity+'-'+actdbt.f058fname) as debitActivity, f.f066fdr_activity, (f.f066fdr_account+'-'+acodbt.f059fname) debitAccount, f.f066fdr_account, (f.f066fdr_department+'-'+ddr.f015fdepartment_name) as debitDepartment , f.f066fdr_department from t066ffee_item_details f inner join t015fdepartment dcr on dcr.f015fdepartment_code = f.f066fcr_department inner join t015fdepartment ddr on ddr.f015fdepartment_code = f.f066fdr_department inner join t057ffund fcr on fcr.f057fcode = f.f066fcr_fund inner join t057ffund fdr on fdr.f057fcode = f.f066fdr_fund inner join t058factivity_code actcr on actcr.f058fcomplete_code = f.f066fcr_activity inner join t058factivity_code actdbt on actdbt.f058fcomplete_code = f.f066fdr_activity inner join t059faccount_code acocr on acocr.f059fcomplete_code = f.f066fcr_account inner join t059faccount_code acodbt on acodbt.f059fcomplete_code = f.f066fdr_account where f.f066fid_fee_item = $FeeId ";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        // print_r($query);exit;
        
        return $result;
    }

    public function getFeeItemByStatus($id) 
    {
        $id = (int)$id;

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("f.f066fid, f.f066fname, f.f066fcode, f.f066fdescription, f.f066fidFeeCategory, c.f065fname as feeCategory, f.f066fidTaxCode, t.f081fcode as GST, f.f066finsuredItem, f.f066fsoCode, f.f066frefundable, f.f066finvoiceItem, f.f066fsoa, f.f066fstatus, (f.f066fcode+'-'+f.f066fname) as Feename ")
            ->from('Application\Entity\T066ffeeItem', 'f')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'c', 'with','f.f066fidFeeCategory = c.f065fid')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with','f.f066fidTaxCode = t.f081fid')
            ->where('f.f066fstatus = :status')
            ->setParameter('status',$id)
            ->orderBy('f.f066fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );        return $result;
    
    }
}
