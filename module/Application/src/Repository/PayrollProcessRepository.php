<?php
namespace Application\Repository;

use Application\Entity\T044fpayrollProcess;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PayrollProcessRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('pr.f044fid, pr.f044fcompany, c.f013fname as company,  pr.f044fmonth, pr.f044fstaffType, pr.f044fstaffIp, pr.f044fstatus')
            ->from('Application\Entity\T044fpayrollProcess', 'pr')
            ->leftjoin('Application\Entity\T013fcompany', 'c', 'with','pr.f044fcompany = c.f013fid')
            ->orderBy('pr.f044fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('pr.f044fid, pr.f044fcompany, c.f013fname as company,  pr.f044fmonth, pr.f044fstaffType, pr.f044fstaffIp, pr.f044fstatus')
            ->from('Application\Entity\T044fpayrollProcess', 'pr')
            ->leftjoin('Application\Entity\T013fcompany', 'c', 'with','pr.f044fcompany = c.f013fid')
            ->where('pr.f044fid = :payrollId')
            ->setParameter('payrollId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $payroll = new T044fpayrollProcess();

        $payroll->setF044fcompany((int)$data['f044fcompany'])
                ->setF044fmonth($data['f044fmonth'])
                ->setF044fstaffType($data['f044fstaffType'])
                ->setF044fstaffIp((int)$data['f044fstaffIp'])
                ->setF044fstatus((int)$data['f044fstatus'])
                ->setF044fcreatedBy((int)$_SESSION['userId'])
                ->setF044fupdatedBy((int)$_SESSION['userId']);

        $payroll->setF044fcreatedDtTm(new \DateTime())
                ->setF044fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($payroll);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $payroll;
    }

    public function updateData($payroll, $data = []) 
    {
        $em = $this->getEntityManager();
    
         $payroll->setF044fcompany((int)$data['f044fcompany'])
                ->setF044fmonth($data['f044fmonth'])
                ->setF044fstaffType($data['f044fstaffType'])
                ->setF044fstaffIp((int)$data['f044fstaffIp'])
                ->setF044fstatus((int)$data['f044fstatus'])
                ->setF044fcreatedBy((int)$_SESSION['userId'])
                ->setF044fupdatedBy((int)$_SESSION['userId']);

        $payroll->setF044fcreatedDtTm(new \DateTime())
                ->setF044fupdatedDtTm(new \DateTime());



        
        $em->persist($payroll);
        $em->flush();

    }

    
}