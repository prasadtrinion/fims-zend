<?php
namespace Application\Repository;

use Application\Entity\T044floanTypeSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class LoanTypeSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('lt.f044fid, lt.f044fidLoan, l.f020floanName as loanName,  lt.f044fempType, lt.f044fguarantee1, lt.f044fguarantee2, lt.f044fnoGuarantee, lt.f044fstatus, lt.f044fempStatus')
            ->from('Application\Entity\T044floanTypeSetup', 'lt')
             ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'lt.f044fidLoan = l.f020fid ')
             ->orderBy('lt.f044fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;  
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('lt.f044fid, lt.f044fidLoan, l.f020floanName as loanName,  lt.f044fempType, lt.f044fguarantee1, lt.f044fguarantee2, lt.f044fnoGuarantee, lt.f044fstatus, lt.f044fempStatus')
            ->from('Application\Entity\T044floanTypeSetup', 'lt')
             ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'lt.f044fidLoan = l.f020fid ')
            ->where('lt.f044fid = :LoanTypeSetupId')
            ->setParameter('LoanTypeSetupId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $LoanTypeSetup = new T044floanTypeSetup();

        $LoanTypeSetup->setF044fidLoan((int)$data['f044fidLoan'])
                ->setF044fempType($data['f044fempType'])
                ->setF044fempStatus($data['f044fempStatus'])
                ->setF044fguarantee1($data['f044fguarantee1'])
                ->setF044fguarantee2($data['f044fguarantee2'])
                ->setF044fnoGuarantee((int)$data['f044fnoGuarantee'])
                ->setF044fstatus((int)$data['f044fstatus'])
                ->setF044fcreatedBy((int)$_SESSION['userId'])
                ->setF044fupdatedBy((int)$_SESSION['userId']);

        $LoanTypeSetup->setF044fcreatedDtTm(new \DateTime())
                ->setF044fupdatedDtTm(new \DateTime());
          
        $em->persist($LoanTypeSetup);
        
        $em->flush();
        
        return $LoanTypeSetup;
    }

    public function updateData($LoanTypeSetup, $data = []) 
    {
        $em = $this->getEntityManager();

        $LoanTypeSetup->setF044fidLoan((int)$data['f044fidLoan'])
                ->setF044fempType($data['f044fempType'])
                ->setF044fempStatus($data['f044fempStatus'])
                ->setF044fguarantee1($data['f044fguarantee1'])
                ->setF044fguarantee2($data['f044fguarantee2'])
                ->setF044fnoGuarantee((int)$data['f044fnoGuarantee'])
                ->setF044fstatus((int)$data['f044fstatus'])
                ->setF044fcreatedBy((int)$_SESSION['userId'])
                ->setF044fupdatedBy((int)$_SESSION['userId']);

        $LoanTypeSetup->setF044fcreatedDtTm(new \DateTime())
                ->setF044fupdatedDtTm(new \DateTime());
        
        $em->persist($LoanTypeSetup);
        $em->flush();
    } 

    public function getListByLoanName($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('lt.f044fid, lt.f044fidLoan, lt.f044fempType, lt.f044fguarantee1, lt.f044fguarantee2, lt.f044fnoGuarantee, lt.f044fstatus, lt.f044fempStatus')
            ->from('Application\Entity\T044floanTypeSetup', 'lt')
            ->where('lt.f044fidLoan = :LoanNameId')
            ->setParameter('LoanNameId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    } 


    public function getUserRole()
    {
        $em = $this->getEntityManager();
        
        $query = " SELECT f023fid, f023fid_role as f023fidRole, f023fid_user as f023fidUser, f023fstatus as f023fstatus from t023fuser_roles order by f023fid DESC";
            
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        $result = array(
            'data'=>$result);

        return $result;  
    }

    public function getUserRoleById($id)
    {
         $em = $this->getEntityManager();
        
        $query = " SELECT f023fid, f023fid_role as f023fidRole, f023fid_user as f023fidUser, f023fstatus as f023fstatus from t023fuser_roles WHERE f023fid_role = $id";
            
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
         $result = array(
            'data'=>$result);

        return $result;  
    }
}
