<?php
namespace Application\Repository;

use Application\Entity\T097fstoreSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StoreSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();   

        $qb->select('s.f097fid, s.f097fstoreCode, d.f015fdepartmentName as department,  s.f097fstoreName, s.f097fdepartment, s.f097fstatus, s.f097fpersonInCharge, sm.f034fname as name,sm.f034fstaffId as staffId, d.f015fdepartmentCode')
            ->from('Application\Entity\T097fstoreSetup', 's')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 's.f097fdepartment = d.f015fid ')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 's.f097fpersonInCharge = sm.f034fstaffId ')
            ->orderBy('s.f097fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('s.f097fid, s.f097fstoreCode, d.f015fdepartmentName as department,  s.f097fstoreName, s.f097fdepartment, s.f097fstatus, s.f097fpersonInCharge, d.f015fdepartmentCode')
            ->from('Application\Entity\T097fstoreSetup', 's')
             ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 's.f097fdepartment = d.f015fid ')
             ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 's.f097fpersonInCharge = sm.f034fstaffId ')
            ->where('s.f097fid = :storeId')
            ->setParameter('storeId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $store = new T097fstoreSetup();

        $store->setF097fstoreCode($data['f097fstoreCode'])
                ->setF097fstoreName($data['f097fstoreName'])
                ->setF097fdepartment((int)$data['f097fdepartment'])
                ->setF097fstatus((int)$data['f097fstatus'])
                ->setF097fpersonInCharge((int)$data['f097fpersonInCharge'])
                ->setF097fcreatedBy((int)$_SESSION['userId'])
                ->setF097fupdatedBy((int)$_SESSION['userId']);

        $store->setF097fcreatedDtTm(new \DateTime())
                ->setF097fupdatedDtTm(new \DateTime());
          
        $em->persist($store);
        
        $em->flush();
        
        return $store;
    }

    public function updateData($store, $data = []) 
    {
        $em = $this->getEntityManager();

        $store->setF097fstoreCode($data['f097fstoreCode'])
                ->setF097fstoreName($data['f097fstoreName'])
                ->setF097fdepartment((int)$data['f097fdepartment'])
                ->setF097fstatus((int)$data['f097fstatus'])
                ->setF097fpersonInCharge((int)$data['f097fpersonInCharge'])
                ->setF097fupdatedBy((int)$_SESSION['userId']);

        $store->setF097fupdatedDtTm(new \DateTime());
        
        $em->persist($store);
        $em->flush();
    }

    public function getActiveStore($id) 
    {
        // print_r($id);exit;

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('s.f097fid, s.f097fstoreCode, d.f015fdepartmentName as department,  s.f097fstoreName, s.f097fdepartment, s.f097fstatus, s.f097fpersonInCharge, d.f015fdepartmentCode')
            ->from('Application\Entity\T097fstoreSetup', 's')
             ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 's.f097fdepartment = d.f015fid ')
             ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 's.f097fpersonInCharge = sm.f034fstaffId ')
            ->where('s.f097fstatus = :storeId')
            ->setParameter('storeId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
}