<?php
namespace Application\Repository;

use Application\Entity\T024fdiscount;
use Application\Entity\T025fdiscountDetails;
use Application\Entity\T071finvoice;
use Application\Entity\T015ffinancialyear;
use Application\Entity\T014fglcode;
use Application\Entity\T017fjournal;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityRepository;

class DiscountRepository extends EntityRepository
{
 	public function getList() 
	{

 		$em = $this->getEntityManager();	

		$qb = $em->createQueryBuilder();

		$qb->select('d.f024fid, d.f024fidInvoice,d.f024fidFinancialYear,  d.f024fidCustomer, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024freason, d.f024fapprovalStatus, d.f024ftype, d.f024fdiscountType')
		   ->from('Application\Entity\T024fdiscount', 'd')
           ->orderby('d.f024fid','DESC');
		   
		$query = $qb->getQuery();

        $results = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach($results as $result)
        {

        $idDiscount = (int)$result['f024fid'];
            $type=$result['f024ftype'];
            switch ($type) {
            case 'DCOR':
                $qb = $em->createQueryBuilder();
               $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype, c.f021ffirstName as firstName, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024fdiscountType, d.f024fapprovalStatus, d.f024freason,(c.f021ffirstName+' '+c.f021flastName) as fullName,d.f024fapprovalStatus, dd.f025fidInvoiceDetail")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f024fidCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T025fdiscountDetails','dd', 'with','dd.f025fidDiscount = d.f024fid')
                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                
                break;
            case 'DCSP':
                $qb = $em->createQueryBuilder();
               $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype, d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription,d.f024fapprovalStatus, d.f024fdiscountNumber, d.f024freason, (s.f063ffirstName +' '+s.f063flastName) as fullName,d.f024fapprovalStatus, dd.f025fidInvoiceDetail")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T063fsponsor', 's', 'with', 'd.f024fidCustomer = s.f063fid')
                 ->leftjoin('Application\Entity\T025fdiscountDetails','dd', 'with','dd.f025fidDiscount = d.f024fid')
                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'DCSTD':
                $qb = $em->createQueryBuilder();
                $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype, d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate,d.f024fapprovalStatus, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,st.f060fname as fullName,d.f024fapprovalStatus, dd.f025fidInvoiceDetail")
                ->leftjoin('Application\Entity\T025fdiscountDetails','dd', 'with','dd.f025fidDiscount = d.f024fid')
                ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'd.f024fidCustomer = st.f060fid')

                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'DCSTA':
                $qb = $em->createQueryBuilder();
                $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype, d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate,d.f024fapprovalStatus, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,sm.f034fname as fullName,d.f024fapprovalStatus, dd.f025fidInvoiceDetail")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 'd.f024fidCustomer = sm.f034fstaffId')
                ->leftjoin('Application\Entity\T025fdiscountDetails','dd', 'with','dd.f025fidDiscount = d.f024fid')
                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            }

            $query2 = "select SUM(f025fdiscount_amount) as totalDiscount from t025fdiscount_details where f025fid_discount = $idDiscount ";
            $credit_result = $em->getConnection()->executeQuery($query2)->fetch();

            $credit[0]['f025fdiscountAmount'] = $credit_result['totalDiscount'];


            
            if($credit)
            {    
                array_push($result1, $credit[0]);
            }
        }

        $result2 = array();
        foreach ($result1 as $item)
        {
            $date                        = $item['f024fdate'];
            $item['f024fdate'] = date("Y-m-d", strtotime($date));
            array_push($result2, $item);
        }
        $result = array(
            'data' => $result2
        );
        // print_r($result);exit;
        return $result;
        
    }

    public function getListById($id)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        
           $qb->select('d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fapprovalStatus,d.f024fidFinancialYear, d.f024fidCustomer,d.f024ftype, d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, dd.f025fid, dd.f025fidItem, dd.f025fgstValue, dd.f025fquantity, dd.f025fprice, dd.f025ftotal, dd.f025fdiscountPercentage, dd.f025fstatus, dd.f025ftaxCode, dd.f025fdebitFundCode, dd.f025fdebitDepartmentCode, dd.f025fdebitActivityCode, dd.f025fdebitAccountCode, dd.f025fcreditFundCode, dd.f025fcreditDepartmentCode, dd.f025fcreditActivityCode, dd.f025fcreditAccountCode, dd.f025ftaxAmount, dd.f025fdiscountAmount, d.f024freason, dd.f025fidDiscount, dd.f025fbalanceAmount, dd.f025fidInvoiceDetail')
		   ->from('Application\Entity\T024fdiscount', 'd')
		   ->leftjoin('Application\Entity\T025fdiscountDetails','dd', 'with','dd.f025fidDiscount = d.f024fid')
		   // ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','d.f024fidFinancialYear = f.f015fid')
		   // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', ' d.f024fidCustomer = c.f021fid')
		   ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
               ->where('d.f024fid = :discountId')
           ->orderby('d.f024fid','DESC')
               
           	->setParameter('discountId',(int)$id);
            
		$query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $discount) 
        {
            $date                        = $discount['f024fdate'];
            $discount['f024fdate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $discount);
        }

    return $result1;
		
	}

	public function getListByApprovedStatus($approvedStatus)
    {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();
        
		// echo("hi");
		// exit;
           if($approvedStatus == 0)
           {
               $qb->select('d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber,d.f024fapprovalStatus, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber,d.f024fapprovalStatus, d.f024freason')
		   		  ->from('Application\Entity\T024fdiscount', 'd')
		   		  // ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','d.f024fidFinancialYear = f.f015fid')
		   		  // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', ' d.f024fidCustomer = c.f021fid')
		   		  ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                 ->where('d.f024fapprovalStatus = 0')
           ->orderby('d.f024fid','DESC');
                 
            }

            elseif($approvedStatus == 1)
            {
               $qb->select('d.f024fid, d.f024fidInvoice, d.f024fapprovalStatus,i.f071finvoiceNumber, d.f024fidFinancialYear, d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber,d.f024fapprovalStatus , d.f024freason')
		   		  ->from('Application\Entity\T024fdiscount', 'd')
		   		  // ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','d.f024fidFinancialYear = f.f015fid')
		   		  // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', ' d.f024fidCustomer = c.f021fid')
		   		  ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                 ->where('d.f024fapprovalStatus = 1')
           ->orderby('d.f024fid','DESC');
		   		
            }

            elseif($approvedStatus == 2) 
            {
               $qb->select('d.f024fid, d.f024fidInvoice,d.f024fapprovalStatus, i.f071finvoiceNumber, d.f024fidFinancialYear, d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber,d.f024fapprovalStatus, d.f024freason')
		   		  ->from('Application\Entity\T024fdiscount', 'd')
		   		  // ->leftjoin('Application\Entity\T015ffinancialyear','f', 'with','d.f024fidFinancialYear = f.f015fid')
		   		  // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', ' d.f024fidCustomer = c.f021fid')
		   		  ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                   
           ->orderby('d.f024fid','DESC');
                     
            }
        	
		$results = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
		 $result1 = array();
        foreach($results as $result)
        {
            $idDiscount = (int)$result['f024fid'];
            $type=$result['f024ftype'];
            switch ($type) {
            case 'DCOR':
                $qb = $em->createQueryBuilder();
               $qb->select("d.f024fid, d.f024fidInvoice,d.f024fapprovalStatus, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, c.f021ffirstName as firstName, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,(c.f021ffirstName+' '+c.f021flastName) as fullName,d.f024fapprovalStatus")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'd.f024fidCustomer = c.f021fid')

                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                
                break;
            case 'DCSP':
                $qb = $em->createQueryBuilder();
               $qb->select("d.f024fid, d.f024fidInvoice,d.f024fapprovalStatus, i.f071finvoiceNumber, d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024freason, (s.f063ffirstName +' '+s.f063flastName) as fullName,d.f024fapprovalStatus")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T063fsponsor', 's', 'with', 'd.f024fidCustomer = s.f063fid')


                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'DCSTD':
                $qb = $em->createQueryBuilder();
                $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fapprovalStatus,d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,st.f060fname as fullName,d.f024fapprovalStatus")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T060fstudent', 'st', 'with', 'd.f024fidCustomer = st.f060fid')

                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            case 'DCSTA':
                $qb = $em->createQueryBuilder();
                $qb->select("d.f024fid, d.f024fidInvoice, i.f071finvoiceNumber, d.f024fapprovalStatus,d.f024fidFinancialYear,  d.f024fidCustomer,d.f024ftype,d.f024fdiscountType, d.f024ftotalAmount, d.f024fbalance, d.f024freferenceNumber, d.f024fdate, d.f024fdescription, d.f024fdiscountNumber, d.f024freason,sm.f034fname as fullName,d.f024fapprovalStatus")
                 ->from('Application\Entity\T024fdiscount', 'd')
                ->leftjoin('Application\Entity\T071finvoice', 'i', 'with', 'd.f024fidInvoice = i.f071fid')
                ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with', 'd.f024fidCustomer = sm.f034fstaffId')

                ->where('d.f024fid = :id')
                ->setParameter('id',(int)$result['f024fid'])
                 ->orderby('d.f024fid','DESC');

                $credit = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                break;
            }


            $query2 = "select SUM(f025fdiscount_amount) as totalDiscount from t025fdiscount_details where f025fid_discount = $idDiscount ";
            $credit_result = $em->getConnection()->executeQuery($query2)->fetch();

            $credit[0]['f025fdiscountAmount'] = $credit_result['totalDiscount'];


            if($credit)
            {    
                array_push($result1, $credit[0]);
            }
        }
		return $result1;
		
	}

	public function createDiscount($data)
    {
        // $invoiceId = (int)$data['f024fidInvoice'];

		$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= $data['f024ftype'];
        $number = $configRepository->generateFIMS($numberData);

        $discount = new T024fdiscount();

        $discount->setF024fidInvoice((int)$data['f024fidInvoice'])
					   ->setF024fidFinancialYear((int)"1")
			           ->setF024fidCustomer((int)$data['f024fidCustomer'])
                       ->setF024ftotalAmount((float)$data['f024ftotalAmount'])
                       ->setF024ftype($data['f024ftype'])
			    	   ->setF024fdiscountType((int)$data['f024fdiscountType'])
			    	   ->setF024fbalance((float)$data['f024ftotalAmount'])
			    	   ->setF024freferenceNumber($number)
                       ->setF024freason($data['f024freason'])
			    	   ->setF024fdate(new \DateTime($data['f024fdate']))
			    	   ->setF024fdescription($data['f024fdescription'])
			    	   ->setF024fdiscountNumber($data['f024fdiscountNumber'])
			    	   ->setF024fapprovalStatus((int)$data['f024fapprovalStatus'])
			    	   ->setF024fstatus((int)$data['f024fstatus'])
			    	   ->setF024fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF024fupdatedBy((int)$_SESSION['userId']);

		$discount->setF024fcreatedDtTm(new \DateTime())
    			       ->setF024fupdatedDtTm(new \DateTime());
        
        $em->persist($discount);
        $em->flush();
        
        $discountDetails = $data['discount-details'];

        foreach ($discountDetails as $discountDetail)
        {
            // $idItem = (int)$details['f025fidItem'];
            // $creditBalanceAmount = (float)$details['f025fbalanceAmount'];

            $discountDetailObj = new T025fdiscountDetails();

            $discountDetailObj->setF025fidDiscount((int)$discount->getF024fid())
			    	   ->setF025fidItem((int)$discountDetail['f025fidItem'])
			           ->setF025fgstValue((float)$discountDetail['f025fgstValue'])
			    	   ->setF025fquantity((int)$discountDetail['f025fquantity'])
			    	   ->setF025fprice((float)$discountDetail['f025fprice'])
			    	   ->setF025ftotal((float)$discountDetail['f025ftotal'])
			    	   ->setF025ftaxCode($discountDetail['f025ftaxCode'])
			    	   ->setF025fdebitFundCode( $discountDetail['f025fdebitFundCode'])
                        ->setF025fdebitAccountCode( $discountDetail['f025fdebitAccountCode'])
                        ->setF025fdebitActivityCode( $discountDetail['f025fdebitActivityCode'])
                        ->setF025fdebitDepartmentCode( $discountDetail['f025fdebitDepartmentCode'])
                        ->setF025fcreditFundCode( $discountDetail['f025fcreditFundCode'])
                        ->setF025fcreditAccountCode( $discountDetail['f025fcreditAccountCode'])
                        ->setF025fcreditActivityCode( $discountDetail['f025fcreditActivityCode'])
                        ->setF025fcreditDepartmentCode( $discountDetail['f025fcreditDepartmentCode'])
			    	   ->setF025fstatus((int)$discountDetail['f025fstatus'])
			    	   ->setF025ftaxAmount((float)$discountDetail['f025ftaxAmount'])
                       ->setF025fdiscountAmount((float)$discountDetail['f025fdiscountAmount'])
                       ->setF025fdiscountType((int)$discountDetail['f025fdiscountType'])
			    	   ->setF025fbalanceAmount((float)$discountDetail['f025fbalanceAmount'])
                       ->setF025fidInvoiceDetail((int)$discountDetail['f025fidInvoiceDetail'])
			    	   ->setF025fdiscountPercentage((float)$discountDetail['f025fdiscountPercentage'])
			    	   ->setF025fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF025fupdatedBy((int)$_SESSION['userId']);

			$discountDetailObj->setF025fcreatedDtTm(new \DateTime())
    			       ->setF025fupdatedDtTm(new \DateTime());

        

        $em->persist($discountDetailObj);
        $em->flush();

            
        }
        return $discount;
    }

    public function updateDiscount($discount, $data = [])
    {
        $em = $this->getEntityManager();

        $discount->setF024fidInvoice((int)$data['f024fidInvoice'])
                        ->setF024fidFinancialYear((int)"1")
                        ->setF024fidCustomer((float)$data['f024fidCustomer'])
			    	   ->setF024ftotalAmount((float)$data['f024ftotalAmount'])
                       ->setF024ftype($data['f024ftype'])
                       ->setF024fdiscountType((int)$data['f024fdiscountType'])
			    	   ->setF024fbalance((int)$data['f024fbalance'])
			    	   ->setF024freferenceNumber((int)$data['f024freferenceNumber'])
                       ->setF024freason($data['f024freason'])
			    	   ->setF024fdate(new \DateTime($data['f024fdate']))
			    	   ->setF024fdescription((int)$data['f024fdescription'])
			    	   ->setF024fdiscountNumber((int)$data['f024fdiscountNumber'])
			    	   ->setF024fapprovalStatus((int)$data['f024fapprovalStatus'])
			    	   ->setF024fstatus((int)$_SESSION['userId'])
			    	   ->setF024fupdatedBy((int)$_SESSION['userId']);

		$discount->setF024fupdatedDtTm(new \DateTime());
        
        $em->persist($discount);

        $em->flush();
        
        return $discount;
    }


    public function updateDiscountDetail($discountDetailObj, $discountDetail)
    {
        
        $em = $this->getEntityManager();

        
        $discountDetailObj->setF025fidItem((int)$discountDetail['f025fidItem'])
			           ->setF025fgstValue((float)$discountDetail['f025fgstValue'])
			    	   ->setF025fquantity((int)$discountDetail['f025fquantity'])
			    	   ->setF025fprice((float)$discountDetail['f025fprice'])
			    	   ->setF025ftotal((float)$discountDetail['f025ftotal'])
			    	   ->setF025ftaxCode($discountDetail['f025ftaxCode'])
			    	   ->setF025fdebitFundCode( $discountDetail['f025fdebitFundCode'])
                        ->setF025fdebitAccountCode( $discountDetail['f025fdebitAccountCode'])
                        ->setF025fdebitActivityCode( $discountDetail['f025fdebitActivityCode'])
                        ->setF025fdebitDepartmentCode( $discountDetail['f025fdebitDepartmentCode'])
                        ->setF025fcreditFundCode( $discountDetail['f025fcreditFundCode'])
                        ->setF025fcreditAccountCode( $discountDetail['f025fcreditAccountCode'])
                       ->setF025fdiscountType((int)$discountDetail['f025fdiscountType'])
                        ->setF025fcreditActivityCode( $discountDetail['f025fcreditActivityCode'])
                        ->setF025fcreditDepartmentCode( $discountDetail['f025fcreditDepartmentCode'])
			    	   ->setF025ftaxAmount((float)$discountDetail['f025ftaxAmount'])
                       ->setF025fbalanceAmount((float)$discountDetail['f025fbalanceAmount'])
			    	   ->setF025fdiscountPercentage((float)$discountDetail['f025fdiscountPercentage'])
			    	   ->setF025fdiscountAmount((float)$discountDetail['f025fdiscountAmount'])
			    	   ->setF025fstatus((int)$discountDetail['f025fstatus'])
	     			   ->setF025fupdatedBy((int)$_SESSION['userId']);

		$discountDetailObj->setF054fupdatedDtTm(new \DateTime());

        $em->persist($discountDetailObj);

        $em->flush();

    }
    
    public function createDiscountDetail($discountObj, $discountDetail)
    {
        $em = $this->getEntityManager();

        $discountDetailObj = new T025fdiscountDetails();

        $discountDetailObj->setF025fidDiscount((int)$discount->getF024fid())
			    	   ->setF025fidItem((int)$discountDetail['f025fidItem'])
			           ->setF025fgstValue((float)$discountDetail['f025fgstValue'])
			    	   ->setF025fquantity((int)$discountDetail['f025fquantity'])
			    	   ->setF025fprice((float)$discountDetail['f025fprice'])
			    	   ->setF025ftotal((float)$discountDetail['f025ftotal'])
			    	   ->setF025ftaxCode($discountDetail['f025ftaxCode'])
			    	   ->setF025fdebitFundCode( $discountDetail['f025fdebitFundCode'])
                        ->setF025fdebitAccountCode( $discountDetail['f025fdebitAccountCode'])
                        ->setF025fdebitActivityCode( $discountDetail['f025fdebitActivityCode'])
                       ->setF025fdiscountType((int)$discountDetail['f025fdiscountType'])
                        ->setF025fdebitDepartmentCode( $discountDetail['f025fdebitDepartmentCode'])
                        ->setF025fcreditFundCode( $discountDetail['f025fcreditFundCode'])
                        ->setF025fcreditAccountCode( $discountDetail['f025fcreditAccountCode'])
                        ->setF025fcreditActivityCode( $discountDetail['f025fcreditActivityCode'])
                        ->setF025fcreditDepartmentCode( $discountDetail['f025fcreditDepartmentCode'])
			    	   ->setF025fstatus((int)$discountDetail['f025fstatus'])
                       ->setF025fbalanceAmount((float)$discountDetail['f025fbalanceAmount'])
			    	   ->setF025ftaxAmount((float)$discountDetail['f025ftaxAmount'])
                       ->setF025fidInvoiceDetail((int)$discountDetail['f025fidInvoiceDetail'])
			    	   ->setF025fdiscountAmount((float)$discountDetail['f025fdiscountAmount'])
			    	   ->setF025fdiscountPercentage((float)$discountDetail['f025fdiscountPercentage'])
			    	   ->setF025fcreatedBy((int)$_SESSION['userId'])
	     			   ->setF025fupdatedBy((int)$_SESSION['userId']);

			$discountDetailObj->setF025fcreatedDtTm(new \DateTime())
    			       ->setF025fupdatedDtTm(new \DateTime());

        $em->persist($discountDetailObj);

        $em->flush();

        return $discountDetailObj;
        
    }
   

	public function updateApprovalStatus($postData,$journalRepository) 
	{
		$em = $this->getEntityManager();
        $ids = $postData['id'];

		foreach ($ids as $id) 
        {
            $reason = $postData['reason'];
            $status = $postData['status'];
            // print_r($reason);exit;

			$query1 = "update t024fdiscount set f024fstatus = $status,f024fapproval_status = $status, f024freason = '$reason' where f024fid = $id"; 
            $em->getConnection()->executeQuery($query1);

            $query1 = "update t025fdiscount_details set f025fstatus = $status where f025fid_discount = $id"; 
            $em->getConnection()->executeQuery($query1);

            if ($status == 1)
            {
                 $query1 = "select disd.f025fid_item, disd.f025fbalance_amount, dis.f024fid_invoice, disd.f025fdiscount_amount, disd.f025fid_invoice_detail from t025fdiscount_details as disd inner join t024fdiscount as dis on dis.f024fid = disd.f025fid_discount where disd.f025fid_discount = $id";

                $resultDiscounts = $em->getConnection()->executeQuery($query1)->fetchAll();

                foreach ($resultDiscounts as $resultDiscount)
                {
                  

                $idItem = (int)$resultDiscount['f025fid_item'];
                $creditBalanceAmount = (float)$resultDiscount['f025fdiscount_amount'];
                $invoiceId = (int)$resultDiscount['f024fid_invoice'];
                $invoiceIdDetail = (int)$resultDiscount['f025fid_invoice_detail'];



                  $query2 = "select f072fbalance_amount from t072finvoice_details where f072fid = $invoiceIdDetail and f072fid_item = $idItem";

                $resultCredit = $em->getConnection()->executeQuery($query2)->fetch();

                $totalInvoiceAmount = (float)$resultCredit['f072fbalance_amount'];

                $balanceInvoiceDetails = $totalInvoiceAmount - $creditBalanceAmount;

                $amount = (float)$balanceInvoiceDetails;

                $query3 = "update t072finvoice_details set f072fbalance_amount = $amount where f072fid = $invoiceIdDetail and f072fid_item = $idItem";

                $insertBalance = $em->getConnection()->executeQuery($query3);
            }
                
        }


            $query1 = "select f024fdescription,f024fid_invoice,f024fid_financial_year,f024fid_customer,f024ftotal_amount,f024fcreated_by,f024fupdated_by,f024freference_number,f024fcreated_dt_tm, f024freason from t024fdiscount where f024fid = $id";
			
			$result = $em->getConnection()->executeQuery($query1)->fetch();
           $idInvoice = $result['f024fid_invoice'];
            
            

            $query2 = "select SUM(f025fdiscount_amount) as item_debit,f025fdebit_account_code as account_code,f025fdebit_activity_code as activity_code,f025fdebit_department_code as department_code,f025fdebit_fund_code as fund_code from t025fdiscount_details  where f025fid_discount = $id group by f025fdebit_account_code,f025fdebit_activity_code,f025fdebit_department_code,f025fdebit_fund_code";
            $debit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
           
            $query2 = "select SUM(f025fdiscount_amount) as item_credit,f025fcredit_account_code as account_code,f025fcredit_activity_code as activity_code,f025fcredit_department_code as department_code,f025fcredit_fund_code as fund_code from t025fdiscount_details where f025fid_discount = $id group by f025fcredit_account_code,f025fcredit_activity_code,f025fcredit_department_code,f025fcredit_fund_code";
            $credit_result = $em->getConnection()->executeQuery($query2)->fetchAll();
           
            $totalAmount = 0;
            foreach($debit_result as $item){
                $totalAmount = $totalAmount + $item['item_debit'];
            }
        
            // print_r("Hi");
            // exit;
            

            $data['f017fdescription'] = "Discount Note";
            $data['f017fmodule'] = "Discount";
            $data['f017fapprover'] = $result['f024fupdated_by'];
            $data['f017fidFinancialYear'] = $result['f024fid_financial_year'];
           	$data['f017fdateOfTransaction'] = $result['f024fdate'];
           	$data['f017freferenceNumber'] = $result['f024freference_number'];
            $data['f017fapprovedStatus'] = "1";
            $data['f017ftotalAmount'] = $totalAmount;


            $details = array();
             foreach ($credit_result as $item) {

                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                $detail['f018fsoCode'] = "NULL";

                $detail['f018freferenceNumber'] = $result['f024freference_number'];
                $detail['f018fdrAmount'] = 0;
                $detail['f018fcrAmount'] = (float) $item['item_credit'];
                $detail['f018fcreated_by'] = $result['f024fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $result['f024fcreated_dt_tm'];
                array_push($details,$detail);
            }
            foreach ($debit_result as $item) {

                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $result['f024freference_number'];
                $detail['f018fdrAmount'] = (float) $item['item_debit'];
                $detail['f018fcrAmount'] = 0;
                $detail['f018fcreated_by'] = $result['f024fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $result['f024fcreated_dt_tm'];
                array_push($details,$detail);
            }
            $data['journal-details'] = $details;
            try{
           
                // $final_dr = $amount + $invoice_result['f071fdr_amount'];
                // $paid = $final_dr - $invoice_result['f071fcr_amount'] - $invoice_result['f071fdiscount'];
                // $balance = $invoice_result['f071finvoice_total'] - $paid;
            if($status == 1){
            
            $journalRepository->createNewData($data);
                $query1 = "update t071finvoice set f071fdiscount = (f071fdiscount + $totalAmount)  where f071fid = $idInvoice";
                $em->getConnection()->executeQuery($query1);

                  $query1 = "update t071finvoice set  f071fbalance = (f071finvoice_total - f071fcr_amount + f071fdr_amount - f071ftotal_paid - f071fdiscount - f071fonline_payment)  where f071fid = $idInvoice";
                $em->getConnection()->executeQuery($query1);
            }
        }
        catch(\Exception $e){
            echo $e;
        }
       
       		
       	}
	}

}
