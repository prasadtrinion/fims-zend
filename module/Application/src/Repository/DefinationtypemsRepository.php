<?php
namespace Application\Repository;

use Application\Entity\T012fdefinationtypems;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DefinationtypemsRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList(){
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f012fid, d.f012fdefTypeDesc, d.f012fstatus, d.f012fdescription, d.f012fdefinitionTypeDesc, d.f012fbahasaIndonesia, d.f012fcreatedBy, d.f012fupdatedBy')
            ->from('Application\Entity\T012fdefinationtypems', 'd');
            


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f012fid, d.f012fdefTypeDesc, d.f012fstatus, d.f012fdescription,d.f012fdefinitionTypeDesc, d.f012fbahasaIndonesia, d.f012fcreatedBy, d.f012fupdatedBy')
            ->from('Application\Entity\T012fdefinationtypems', 'd')
            ->where('d.f012fid = :DefinationtypemsId')
            ->setParameter('DefinationtypemsId',$id);

        $query = $qb->getQuery();
        $result = $query->getArrayResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $definationtypems = new T012fdefinationtypems();

       
         $definationtypems->setF012fdefTypeDesc($data['f012fdefTypeDesc'])
                        ->setF012fstatus((int)$data['f012fstatus'])  
                        ->setF012fdescription($data['f012fdescription'])
                        ->setF012fdefinitionTypeDesc($data['f012fdefinitionTypeDesc'])
                        ->setF012fbahasaIndonesia($data['f012fbahasaIndonesia'])
                        ->setF012fcreatedBy((int)$_SESSION['userId'])
                        ->setF012fupdatedBy((int)$_SESSION['userId']);

        $definationtypems->setF012fcreateDtTm(new \DateTime())
                         ->setF012fupdateDtTm(new \DateTime());


        $em->persist($definationtypems);
        $em->flush();

       
        return $definationtypems;

    }

    public function updateData($data) 
    {
        $em = $this->getEntityManager();
        $definationtypems = new T012fdefinationtypems();


        $definationtypems->setF012fdefTypeDesc($data['f012fdefTypeDesc'])
             ->setF012fstatus((int)$data['f012fstatus'])  
             ->setF012fdescription($data['f012fdescription'])
             ->setF012fdefinitionTypeDesc($data['f012fdefinitionTypeDesc'])
             ->setF012fbahasaIndonesia($data['f012fbahasaIndonesia'])
             ->setF012fcreatedBy((int)$_SESSION['userId'])
             ->setF012fupdatedBy((int)$_SESSION['userId']);

        $definationtypems->setF012fcreateDtTm(new \DateTime())
                         ->setF012fupdateDtTm(new \DateTime());


        $em->persist($definationtypems);
        $em->flush();

       
        return $definationtypems;

    }



     public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

}
