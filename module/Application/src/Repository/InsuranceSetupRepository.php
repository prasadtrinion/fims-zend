<?php
namespace Application\Repository;

use Application\Entity\T069finsuranceSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class InsuranceSetupRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f069fid, i.f069finstitutionCode ,i.f069fname, i.f069faddress, i.f069fphone, i.f069femail, i.f069fcontactPerson, i.f069feffectiveDate, i.f069fendDate, i.f069fstatus')
            ->from('Application\Entity\T069finsuranceSetup','i');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('i.f069fid, i.f069finstitutionCode , i.f069fname, i.f069faddress, i.f069fphone, i.f069femail, i.f069fcontactPerson, i.f069feffectiveDate, i.f069fendDate, i.f069fstatus')
            ->from('Application\Entity\T069finsuranceSetup','i')
            ->where('i.f069fid = :insuranceId')
            ->setParameter('insuranceId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $insurance = new T069finsuranceSetup();

        $insurance->setF069finstitutionCode($data['f069finstitutionCode'])
             ->setF069fname($data['f069fname'])
             ->setF069faddress($data['f069faddress'])
             ->setF069fphone($data['f069fphone'])
             ->setF069femail($data['f069femail'])
             ->setF069fcontactPerson($data['f069fcontactPerson'])
             ->setF069feffectiveDate(new \DateTime($data['f069feffectiveDate']))
             ->setF069fendDate(new \DateTime($data['f069fendDate']))
             ->setF069fstatus($data['f069fstatus'])
             ->setF069fcreatedBy((int)$_SESSION['userId'])
             ->setF069fupdatedBy((int)$_SESSION['userId']);

        $insurance->setF069fcreatedDtTm(new \DateTime())
                ->setF069fupdatedDtTm(new \DateTime());

 
        $em->persist($insurance);
        $em->flush();

        return $insurance;

    }

    /* to edit the data in database*/

    public function updateData($insurance, $data = []) 
    {
        $em = $this->getEntityManager();

        $insurance->setF069finstitutionCode($data['f069finstitutionCode'])
             ->setF069fname($data['f069fname'])
             ->setF069faddress($data['f069faddress'])
             ->setF069fphone($data['f069fphone'])
             ->setF069femail($data['f069femail'])
             ->setF069fcontactPerson($data['f069fcontactPerson'])
             ->setF069feffectiveDate(new \DateTime($data['f069feffectiveDate']))
             ->setF069fendDate(new \DateTime($data['f069fendDate']))
             ->setF069fstatus($data['f069fstatus'])
             ->setF069fcreatedBy((int)$_SESSION['userId'])
             ->setF069fupdatedBy((int)$_SESSION['userId']);

        $insurance->setF069fcreatedDtTm(new \DateTime())
                ->setF069fupdatedDtTm(new \DateTime());

        $em->persist($insurance);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>