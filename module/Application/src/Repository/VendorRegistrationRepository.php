<?php
namespace Application\Repository;

use Application\Entity\T030fvendorRegistration;
use Application\Entity\T031flicenseInformation;
use Application\Entity\T112fcontactDetails;
use Application\Entity\T113fnatureOfBusiness;
use Application\Entity\T115fvendorMof;
use Application\Entity\T071finvoice;
use Application\Entity\T072finvoiceDetails;
use Application\Entity\T127fpaytoProfile;
use Application\Entity\T031fvendorOwner;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;


class VendorRegistrationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, (s.f030fvendorCode+' - '+s.f030fcompanyName) as Vendor, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite,  s.f030fapprovalStatus,s.f030ffile, s.f030fnumberReceipt, s.f030fpayToProfile, s.f030fdebtorCode, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace,s.f030fuserStatus")
            ->from('Application\Entity\T030fvendorRegistration','s')
            // ->leftjoin('Application\Entity\T042fbank','b','with', 's.f030fidBank=b.f042fid')
            // ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f030fcountry=c.f013fid')
            // ->leftjoin('Application\Entity\T012fstate','st','with', 's.f030fstate=st.f012fid')
            // ->leftjoin('Application\Entity\T031flicenseInformation','lc','with', 's.f030fid=lc.f031fidSupplierReg')
            // ->leftjoin('Application\Entity\T112fcontactDetails','cd','with', 's.f030fid=cd.f112fidVendor')
            // ->leftjoin('Application\Entity\T113fnatureOfBusiness','nb','with', 's.f030fid=nb.f113fidVendor')
            // ->leftjoin('Application\Entity\T115fvendorMof','m','with', 's.f030fid=m.f115fidVendor')
            // ->where('s.f030fapprovalStatus = 1')
            ->andWhere('s.f030fid > 21193')
            ->orderBy('s.f030fid','DESC');
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f030fstartDate'];
            $row['f030fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $row['f030fendDate'];
            $row['f030fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // 
        $qb->select("s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, (s.f030fvendorCode+' - '+s.f030fcompanyName) as Vendor, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite,  s.f030fapprovalStatus,s.f030ffile, s.f030fnumberReceipt, s.f030fpayToProfile, s.f030fdebtorCode, s.f030fnumberReceipt, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace,s.f030fuserStatus")
            ->from('Application\Entity\T030fvendorRegistration','s')
            ->where('s.f030fid = :vendorId')
            ->setParameter('vendorId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        if($result)
        {
            $result1 = array();
            foreach ($result as $row) 
            {
                $date                        = $row['f030fstartDate'];
                $row['f030fstartDate'] = date("Y-m-d", strtotime($date));
                $date                        = $row['f030fendDate'];
                $row['f030fendDate'] = date("Y-m-d", strtotime($date));
                $date                        = $row['f031fexpireDate'];
                $row['f031fexpireDate'] = date("Y-m-d", strtotime($date));
                array_push($result1, $row);
            }
            $result1 = $result1[0];

            $qb = $em->createQueryBuilder();
            $qb->select('lc.f031fid, lc.f031fidLicense, lc.f031fexpireDate,lc.f031fidSupplierReg')
                ->from('Application\Entity\T031flicenseInformation','lc')
                ->where('lc.f031fidSupplierReg = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $license_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result2 = array();
            foreach ($license_result as $row) 
            {
                $date                        = $row['f031fexpireDate'];
                $row['f031fexpireDate'] = date("Y-m-d", strtotime($date));
                array_push($result2, $row);
            }
            $result1['license-details'] = $result2;

            $qb = $em->createQueryBuilder();
            $qb->select('cd.f112fid,cd.f112fidVendor, cd.f112fcontactPerson, cd.f112ficNumber, cd.f112femail, cd.f112fcontactNumber, cd.f112fstatus, cd.f112fidType')
                ->from('Application\Entity\T112fcontactDetails','cd')
                ->where('cd.f112fidVendor = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $contact_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result1['contact-details'] = $contact_result;


            $qb = $em->createQueryBuilder();
            $qb->select('nb.f113fid,nb.f113fidVendor, nb.f113fnatureOfBusiness,nb.f113fstatus, nb.f113fcategory, nb.f113fsubCategory')
                ->from('Application\Entity\T113fnatureOfBusiness','nb')
                ->where('nb.f113fidVendor = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $nature_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result1['nature-details'] = $nature_result;

            $qb = $em->createQueryBuilder();
            $qb->select('m.f115fid,m.f115fidVendor, m.f115fmodeOfCategory,m.f115fstatus')
                ->from('Application\Entity\T115fvendorMof','m')
                ->where('m.f115fidVendor = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $mof_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result1['mof-details'] = $mof_result;

            $qb = $em->createQueryBuilder();
            $qb->select('p.f127fid, p.f127fname, p.f127fbank, p.f127faccountNo, p.f127fidVendor, p.f127fstatus')
                ->from('Application\Entity\T127fpaytoProfile','p')
                ->where('p.f127fidVendor = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $pop_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result1['payto-profile'] = $pop_result;

             $qb = $em->createQueryBuilder();
            $qb->select('o.f031fid,o.f031fownerName,o.f031fownerIc,o.f031fownerDesignation,o.f031fidVendor,o.f031fstatus')
                ->from('Application\Entity\T031fvendorOwner','o')
                ->where('o.f031fidVendor = :vendorId')
                ->setParameter('vendorId',(int)$id);
            $query = $qb->getQuery();
            $own_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $result1['owner-details'] = $own_result;

        }
        return $result1;
    }

    public function createVendor($data) 
    {
        $em = $this->getEntityManager();

        // $name = gmdate("YmdHis") . ".pdf";
        // $myfile = fopen("/var/www/html/fims-zend/public/download/".$name, "w");
        // $fileData = json_decode(file_get_contents($data['file']), true);
        
        // echo $fileData;
        // die();
        // fwrite($myfile, $fileData);
        // fclose($myfile);
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="Vendor";
        $number = $configRepository->generateFIMS($numberData);
        $interval = new \DateInterval('P2Y');
        $current = new \DateTime($data['f030fstartDate']);
        $expire = new \DateTime();
        $expire->add($interval);
        $item = new T030fvendorRegistration();
        // print_r($number);exit;

        $item->setF030fcompanyName($data['f030fcompanyName'])
             ->setF030fcompanyRegistration($data['f030fcompanyRegistration'])
             ->setF030femail($data['f030femail'])
             ->setF030ffile($data['f030faccountNumber'])
             ->setF030faddress1($data['f030faddress1'])
             ->setF030faddress2($data['f030faddress2'])
             ->setF030faddress3($data['f030faddress3'])
             ->setF030faddress4($data['f030faddress4'])
             ->setF030fphone($data['f030fphone'])
             ->setF030fvendorCode($number)
             ->setF030fstartDate($current)
             ->setF030fendDate($expire)
             ->setF030fcity($data['f030fcity'])
             ->setF030fpostCode($data['f030fpostCode'])
             ->setF030fstate((int)$data['f030fstate'])
             ->setF030fcountry((int)$data['f030fcountry'])
             ->setF030fwebsite($data['f030fwebsite'])
             ->setF030fidBank((int)$data['f030fidBank'])
             ->setF030faccountNumber($data['f030faccountNumber'])
             ->setF030ffax($data['f030ffax'])
             ->setF030fpayToProfile($data['f030fpayToProfile'])
             ->setF030fdebtorCode($data['f030fdebtorCode'])
             ->setF030fapprovalStatus((int)0)
             ->setF030fcompanyRegNo($data['f030fcompanyRegNo'])
             ->setF030faccountNo($data['f030faccountNo'])
             ->setF030faccountCode($data['f030faccountCode'])
             ->setF030frace((int)$data['f030frace'])
             ->setF030fuserStatus((int)$data['f030fuserStatus'])
             ->setF030fstatus((int)$data['f030fstatus'])
             ->setF030fnumberReceipt($data['f030fnumberReceipt'])
             ->setF030fcreatedBy((int)$_SESSION['userId'])
             ->setF030fupdatedBy((int)$_SESSION['userId']);

        $item->setF030fcreatedDtTm(new \DateTime())
                ->setF030fupdatedDtTm(new \DateTime());

	try{ 
        $em->persist($item);
        $em->flush();
        // print_r($item);
        // die();
	}
	catch(\Exception $e)
    {
       
        echo $e;
    }
    
    $licenseDetails = $data['license-details'];
    foreach($licenseDetails as $licenseDetail)
    {
        $licenseDetailObj = new T031flicenseInformation();

        $licenseDetailObj->setF031fidLicense((int)$licenseDetail['f031fidLicense'])
                         ->setF031fexpireDate(new \DateTime($licenseDetail['f031fexpireDate']))
                         ->setF031fcreatedBy((int)$_SESSION['userId'])
                         ->setF031fupdatedBy((int)$_SESSION['userId'])
                         ->setF031fidSupplierReg((int)$item->getF030fid())
                         ->setF031fstatus((int)"1");
        $licenseDetailObj->setF031fcreatedDtTm(new \DateTime())
                ->setF031fupdatedDtTm(new \DateTime());
                try{ 
                    $em->persist($licenseDetailObj);
                    
                    $em->flush();
                }
                catch(\Exception $e){
            echo $e;
            }
         
    }
    // $this->sendMail($data['f030femail'],$data['f030fcompanyName']);

    $contactDetails = $data['contact-details'];
    foreach($contactDetails as $contactDetail)
    {
        $contactDetailObj = new T112fcontactDetails();

        $contactDetailObj->setF112fidVendor((int)$item->getF030fid())
                         ->setF112fcontactPerson($contactDetail['f112fcontactPerson'])
                         ->setF112ficNumber($contactDetail['f112ficNumber'])
                         ->setF112femail($contactDetail['f112femail'])
                         ->setF112fcontactNumber($contactDetail['f112fcontactNumber'])
                         ->setF112fstatus((int)$contactDetail['f112fstatus'])
                         ->setF112fidType($contactDetail['f112fidType'])
                         ->setF112fcreatedBy((int)$_SESSION['userId'])
                         ->setF112fupdatedBy((int)$_SESSION['userId']);

        $contactDetailObj->setF112fcreatedDtTm(new \DateTime())
                         ->setF112fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($contactDetailObj);
                    
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
         
    }

    $businessNatures = $data['business-nature'];

    foreach($businessNatures as $businessNature)
    {
        $businessNatureObj = new T113fnatureOfBusiness();

        $businessNatureObj->setF113fidVendor((int)$item->getF030fid())
                         ->setF113fnatureOfBusiness($businessNature['f113fnatureOfBusiness'])
                         ->setF113fstatus((int)$businessNature['f113fstatus'])
                         ->setF113fcategory($businessNature['f113fcategory'])
                         ->setF113fsubCategory($businessNature['f113fsubCategory'])
                         ->setF113fcreatedBy((int)$_SESSION['userId'])
                         ->setF113fupdatedBy((int)$_SESSION['userId']);

        $businessNatureObj->setF113fcreatedDtTm(new \DateTime())
                         ->setF113fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($businessNatureObj);
                    
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
         
    }
    $mofs = $data['mof-details'];

    foreach($mofs as $mof)
    {
        $mofObj = new T115fvendorMof();

        $mofObj->setF115fidVendor((int)$item->getF030fid())
                         ->setF115fmodeOfCategory($mof['f115fmodeOfCategory'])
                         ->setF115fstatus((int)$mof['f115fstatus'])
                         ->setF115fcreatedBy((int)$_SESSION['userId'])
                         ->setF115fupdatedBy((int)$_SESSION['userId']);

        $mofObj->setF115fcreatedDtTm(new \DateTime())
                         ->setF115fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($mofObj);
                    
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
         
    }


    $paytoprofiles = $data['payto-profile'];

    foreach($paytoprofiles as $paytoprofile)
    {
        $payto = new T127fpaytoProfile();

        $payto->setF127fidVendor((int)$item->getF030fid())
                        ->setF127fname($paytoprofile['f127fname'])
                        ->setF127fbank((int)$paytoprofile['f127fbank'])
                        ->setF127faccountNo($paytoprofile['f127faccountNo'])
                         ->setF127fstatus((int)$paytoprofile['f115fstatus'])
                         ->setF127fcreatedBy((int)$_SESSION['userId'])
                         ->setF127fupdatedBy((int)$_SESSION['userId']);

        $payto->setF127fcreatedDtTm(new \DateTime())
                         ->setF127fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($payto);
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
         
    }
    $owners = $data['owner-details'];

    foreach($owners as $owner)
    {
        $ownerObj = new T031fvendorOwner();

        $ownerObj->setF031fidVendor((int)$item->getF030fid())
                        ->setF031fownerName($owner['f031fownerName'])
                        ->setF031fownerIc($owner['f031fownerIc'])
                        ->setF031fownerDesignation($owner['f031fownerDesignation'])
                         ->setF031fstatus((int)$owner['f031fstatus'])
                         ->setF031fcreatedBy((int)$_SESSION['userId'])
                         ->setF031fupdatedBy((int)$_SESSION['userId']);

        $ownerObj->setF031fcreatedDtTm(new \DateTime())
                         ->setF031fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($ownerObj);
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
         
    }
    // $this->sendMail($data['f030femail'],$data['f030fcompanyName']);
        return $item;

    }

    public function updateVendor($item, $data = []) 
    {
        $interval = new \DateInterval('P2Y');
        $current = new \DateTime($data['f030fstartDate']);
        $expire = new \DateTime();
        $expire->add($interval);
        
        $em = $this->getEntityManager();

        $item->setF030fcompanyName($data['f030fcompanyName'])
             ->setF030fcompanyRegistration($data['f030fcompanyRegistration'])
             ->setF030femail($data['f030femail'])
             ->setF030faddress1($data['f030faddress1'])
             ->setF030faddress2($data['f030faddress2'])
             ->setF030faddress3($data['f030faddress3'])
             ->setF030faddress4($data['f030faddress4'])
             ->setF030fphone($data['f030fphone'])
             ->setF030fvendorCode($data['f030fvendorCode'])
             ->setF030fstartDate($current)
             ->setF030fendDate($expire)
             ->setF030fcity($data['f030fcity'])
             ->setF030fpostCode($data['f030fpostCode'])
             ->setF030fstate((int)$data['f030fstate'])
             ->setF030fcountry((int)$data['f030fcountry'])
             ->setF030fwebsite($data['f030fwebsite'])
             ->setF030fidBank($data['f030fidBank'])
             ->setF030faccountNumber($data['f030faccountNumber'])
             ->setF030ffax($data['f030ffax'])
             ->setF030fpayToProfile($data['f030fpayToProfile'])
             ->setF030fdebtorCode($data['f030fdebtorCode'])
             ->setF030fcompanyRegNo($data['f030fcompanyRegNo'])
             ->setF030faccountNo($data['f030faccountNo'])
             ->setF030faccountCode($data['f030faccountCode'])
             ->setF030fuserStatus((int)$data['f030fuserStatus'])
             ->setF030frace((int)$data['f030frace'])
             ->setF030fstatus((int)$data['f030fstatus'])
             ->setF030fnumberReceipt($data['f030fnumberReceipt']);

        $item->setF030fupdatedDtTm(new \DateTime());
        
        $em->persist($item);
        $em->flush();

        return $item;

    }

     public function updateLicense($licenseObj, $license)
    {
        
        $em = $this->getEntityManager();

        $licenseObj->setF031fidLicense((int)$license['f031fidLicense'])
                         ->setF031fexpireDate(new \DateTime($license['f031fexpireDate']))
                         ->setF031fcreatedBy((int)$_SESSION['userId'])
                         ->setF031fupdatedBy((int)$_SESSION['userId'])
                         ->setF031fstatus((int)$license['f031fstatus']);
        $licenseObj->setF031fcreatedDtTm(new \DateTime())
                         ->setF031fupdatedDtTm(new \DateTime());

        $em->persist($licenseObj);
        $em->flush();   
        return $licenseObj;
    }
    public function createLicense($vendorObj, $license)
    {
        $em = $this->getEntityManager();

        $licenseObj = new T031flicenseInformation();

        $licenseObj->setF031fidLicense((int)$license['f031fidLicense'])
                         ->setF031fexpireDate(new \DateTime($license['f031fexpireDate']))
                         ->setF031fcreatedBy((int)$_SESSION['userId'])
                         ->setF031fupdatedBy((int)$_SESSION['userId'])
                         ->setF031fidSupplierReg((int)$vendorObj->getF030fid())
                         ->setF031fstatus((int)"1");
        $licenseObj->setF031fcreatedDtTm(new \DateTime())
                ->setF031fupdatedDtTm(new \DateTime());

        $em->persist($licenseObj);
        $em->flush();
        return $licenseObj;   
    }
     public function updateOwner($ownerObj, $owner)
    {
        
        $em = $this->getEntityManager();

         $ownerObj->setF031fidVendor((int)$item->getF030fid())
                        ->setF031fownerName($owner['f031fownerName'])
                        ->setF031fownerIc($owner['f031fownerIc'])
                        ->setF031fownerDesignation($owner['f031fownerDesignation'])
                         ->setF031fstatus((int)$owner['f031fstatus'])
                         ->setF031fupdatedBy((int)$_SESSION['userId']);

        $ownerObj->setF031fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($ownerObj);
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $ownerObj;
    }
    public function createOwner($vendorObj, $owner)
    {
        $em = $this->getEntityManager();

        $ownerObj = new T031fvendorOwner();

        $ownerObj->setF031fidVendor((int)$vendorObj->getF030fid())
                        ->setF031fownerName($owner['f031fownerName'])
                        ->setF031fownerIc($owner['f031fownerIc'])
                        ->setF031fownerDesignation($owner['f031fownerDesignation'])
                         ->setF031fstatus((int)$owner['f031fstatus'])
                         ->setF031fcreatedBy((int)$_SESSION['userId'])
                         ->setF031fupdatedBy((int)$_SESSION['userId']);

        $ownerObj->setF031fcreatedDtTm(new \DateTime())
                         ->setF031fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($ownerObj);
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $ownerObj;
    }

    public function updateContact($contactObj, $contact)
    {
        
        $em = $this->getEntityManager();

        $contactObj->setF112fcontactPerson($contact['f112fcontactPerson'])
                    ->setF112ficNumber($contact['f112ficNumber'])
                    ->setF112femail($contact['f112femail'])
                    ->setF112fcontactNumber($contact['f112fcontactNumber'])
                    ->setF112fidType($contact['f112fidType'])
                    ->setF112fstatus((int)$contact['f112fstatus'])
                    ->setF112fupdatedBy((int)$_SESSION['userId']);

        $contactObj->setF112fupdatedDtTm(new \DateTime());

        $em->persist($contactObj);
        $em->flush();  
        return $contactObj; 
    }
    public function createContact($vendorObj, $contact)
    {
        $em = $this->getEntityManager();

        $contactObj = new T112fcontactDetails();

        $contactObj->setF112fidVendor((int)$vendorObj->getF030fid())
                         ->setF112fcontactPerson($contactDetail['f112fcontactPerson'])
                         ->setF112ficNumber($contactDetail['f112ficNumber'])
                         ->setF112femail($contactDetail['f112femail'])
                         ->setF112fcontactNumber($contactDetail['f112fcontactNumber'])
                         ->setF112fstatus((int)$contactDetail['f112fstatus'])
                         ->setF112fidType($contactDetail['f112fidType'])
                         ->setF112fcreatedBy((int)$_SESSION['userId'])
                         ->setF112fupdatedBy((int)$_SESSION['userId']);

        $contactObj->setF112fcreatedDtTm(new \DateTime())
                         ->setF112fupdatedDtTm(new \DateTime());

        $em->persist($contactObj);
        $em->flush();
        return $contactObj;   
    }

    public function updateBusiness($businessObj, $business)
    {
        
        $em = $this->getEntityManager();

        $businessObj->setF113fnatureOfBusiness($business['f113fnatureOfBusiness'])
                    ->setF113fstatus((int)$business['f113fstatus'])
                    ->setF113fcategory($data['f113fcategory'])
                    ->setF113fsubCategory($data['f113fsubCategory'])
                    ->setF113fcreatedBy((int)$_SESSION['userId'])
                    ->setF113fupdatedBy((int)$_SESSION['userId']);

        $businessObj->setF113fcreatedDtTm(new \DateTime())
                         ->setF113fupdatedDtTm(new \DateTime());

         $em->persist($businessObj);
        $em->flush();
     
    }

    public function createBusiness($vendorObj, $business)
    {
        $em = $this->getEntityManager();

        $businessObj = new T113fnatureOfBusiness();

        $businessObj->setF113fidVendor((int)$vendorObj->getF030fid())
                         ->setF113fnatureOfBusiness($business['f113fnatureOfBusiness'])
                         ->setF113fcategory($data['f113fcategory'])
                         ->setF113fsubCategory($data['f113fsubCategory'])
                         ->setF113fstatus((int)$business['f113fstatus'])
                         ->setF113fcreatedBy((int)$_SESSION['userId'])
                         ->setF113fupdatedBy((int)$_SESSION['userId']);

        $businessObj->setF113fcreatedDtTm(new \DateTime())
                         ->setF113fupdatedDtTm(new \DateTime());

        $em->persist($businessObj);
        $em->flush();
        return $businessObj;   
    }

    public function updateMof($mofObj, $mof)
    {
        
        $em = $this->getEntityManager();


        $mofObj->setF115fmodeOfCategory($mof['f115fmodeOfCategory'])
                         ->setF115fstatus((int)$mof['f115fstatus'])
                         ->setF115fupdatedBy((int)$_SESSION['userId']);

        $mofObj->setF115fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($mofObj);
                    
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
     
    }

    public function createMof($vendorObj, $mof)
    {
        $em = $this->getEntityManager();

        $mofObj = new T115fvendorMof();

        $mofObj->setF115fidVendor((int)$vendorObj->getF030fid())
                         ->setF115fmodeOfCategory($mof['f115fmodeOfCategory'])
                         ->setF115fstatus((int)$mof['f115fstatus'])
                         ->setF115fcreatedBy((int)$_SESSION['userId'])
                         ->setF115fupdatedBy((int)$_SESSION['userId']);

        $mofObj->setF115fcreatedDtTm(new \DateTime())
                         ->setF115fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($mofObj);
                    
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }  
    }


    public function createPaytoProfile($vendorObj, $ptp)
    {
        $em = $this->getEntityManager();

        $payto = new T127fpaytoProfile();

        $payto->setF127fidVendor((int)$vendorObj->getF030fid())
                        ->setF127fname($ptp['f127fname'])
                        ->setF127fbank((int)$ptp['f127fbank'])
                        ->setF127faccountNo($ptp['f127faccountNo'])
                        ->setF127fstatus((int)$ptp['f115fstatus'])
                        ->setF127fcreatedBy((int)$_SESSION['userId'])
                        ->setF127fupdatedBy((int)$_SESSION['userId']);

        $payto->setF127fcreatedDtTm(new \DateTime());
        try{ 
                $em->persist($payto);
                $em->flush();
            }   
        catch(\Exception $e)
        {
            echo $e;
        }
    }

    public function updatePaytoProfile($paytoP, $ptp)
    {
         $em = $this->getEntityManager();

         $paytoP->setF127fname($ptp['f127fname'])
                        ->setF127fbank((int)$ptp['f127fbank'])
                        ->setF127faccountNo($ptp['f127faccountNo'])
                        ->setF127fstatus((int)$ptp['f115fstatus'])
                        ->setF127fupdatedBy((int)$_SESSION['userId']);

        $paytoP->setF127fupdatedDtTm(new \DateTime());
        try{ 
                $em->persist($paytoP);
                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
    }

    public function getVendorApprovedList($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
               $qb->select('s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite, s.f030fapprovalStatus, s.f030fnumberReceipt,  b.f042fbankName, st.f012fstateName, c.f013fcountryName, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace')
            ->from('Application\Entity\T030fvendorRegistration','s')
            ->leftjoin('Application\Entity\T042fbank','b','with', 's.f030fidBank=b.f042fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f030fcountry=c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','st','with', 's.f030fstate=st.f012fid')
                 ->where('s.f030fapprovalStatus = 0')
                 ->andWhere('s.f030fid > 21198');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite, s.f030fapprovalStatus, s.f030fnumberReceipt,  b.f042fbankName, st.f012fstateName, c.f013fcountryName, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace')
            ->from('Application\Entity\T030fvendorRegistration','s')
            ->leftjoin('Application\Entity\T042fbank','b','with', 's.f030fidBank=b.f042fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f030fcountry=c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','st','with', 's.f030fstate=st.f012fid')
            ->where('s.f030fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
               $qb->select('s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite, s.f030fapprovalStatus, s.f030fnumberReceipt,  b.f042fbankName, st.f012fstateName, c.f013fcountryName, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace')
            ->from('Application\Entity\T030fvendorRegistration','s')
            ->leftjoin('Application\Entity\T042fbank','b','with', 's.f030fidBank=b.f042fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f030fcountry=c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','st','with', 's.f030fstate=st.f012fid');

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function vendorApproval($vendors,$reason,$status) 
    {
        $em = $this->getEntityManager();
        

        foreach ($vendors as $vendor) 
        {
            $Id = $vendor->getF030fid();
            $id = (int)$Id;

            $query = "UPDATE t030fvendor_registration set f030freason='$reason',f030fapproval_status=$status where f030fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);

            // $vendor->setF030fapprovalStatus((int)"1")
            //        ->setF030fpassword("e10adc3949ba59abbe56e057f20f883e")
            //        ->setF030ftoken("vendor")
            //      ->setF030fUpdatedDtTm(new \DateTime());
       
            // $em->persist($vendor);
            // $em->flush();
        }
    }


    public function createSupplierInvoice($postData, $invoiceNumber) 
    {
            
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $invoice = new T071finvoice();

        $invoice->setF071finvoiceNumber($invoiceNumber)
            ->setF071finvoiceType('DOC')
            ->setF071fbillType(0)
            ->setF071finvoiceDate(new \DateTime())
            // ->setF071fidFinancialYear($data['f071fIdFinancialYear'])
            ->setF071fidFinancialYear(1)
            ->setF071fdescription('supplier')
            ->setF071fidCustomer((int) $postData['id'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fstatus(1)
            ->setF071finvoiceTotal(30)
            ->setF071fbalance(30)
            ->setF071fcrAmount(0)
            ->setF071fdrAmount(0)
            ->setF071ftotalPaid(0)
            ->setF071fdiscount(0)
            ->setF071fisRcp(1)
            ->setF071finvoiceFrom((int)1)
            ->setF071fcreatedBy((int)$_SESSION['userId'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $invoice->setF071fcreatedDtTm(new \DateTime())
            ->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($invoice);

            $em->flush();

        } catch (\Exception $e) {
            echo $e;
        }

        $invoiceDetails = $data['invoice-details'];

        foreach ($invoiceDetails as $invoiceDetail) {

            $invoiceDetailObj = new T072finvoiceDetails();
            $invoiceDetailObj->setF072fidItem((int)1)
            ->setF072fdebitFundCode(1)
            ->setF072fdebitAccountCode(1)
            ->setF072fdebitActivityCode(1)
            ->setF072fdebitDepartmentCode(1)
            ->setF072fcreditFundCode(1)
            ->setF072fcreditAccountCode(1)
            ->setF072fcreditActivityCode(1)
            ->setF072fcreditDepartmentCode(1)
            ->setF072fgstCode(1)
            ->setF072fgstValue((int)1)
            ->setF072fquantity((int)1)
            ->setF072fprice(30)
            ->setF072ftotal(30)
            ->setF072fidTaxCode(1)
            ->setF072ftotalExc((float) 0)
            ->setF072ftaxAmount((float) 0)
            ->setF072fisRefundable((int) 1)
            ->setF072fstatus((int)1)
                ->setF072fcreatedBy((int)$_SESSION['userId'])
                ->setF072fupdatedBy((int)$_SESSION['userId'])
                ->setF072fidInvoice($invoice);

            $invoiceDetailObj->setF072fcreatedDtTm(new \DateTime())
                ->setF072fupdatedDtTm(new \DateTime());
            try {
                $em->persist($invoiceDetailObj);
                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }
        return $invoice;
    
    }

    public function getVendorList() 
    {
        $em = $this->getEntityManager();

    

        $now = date('Y-m-d');
        // print_r($now);
        // exit();
        $newDate=Date('Y-m-d', strtotime("+15 days"));


        $query = "select * from t030fvendor_registration where f030fend_date >'$newDate'";
        // $query = "select f030fend_date from t030fvendor_registration ";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

       
        return $result;
        
    }
    public function checkEmail($email,$name) 
    {
        $em = $this->getEntityManager();

        $query = "select * from t030fvendor_registration where f030femail = '$email'";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        return $result;
        
    }

    public function sendMail($to,$name){

        try{
            
        $message = new Message();
$message->addTo($to);
$message->addFrom('rajashekhar4647@gmail.com');
$message->setSubject('Registration');
$message->setBody("<p style='font-weight: 400;'><strong>Dear Mr. Sachin Gururaj</strong></p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>As we are nearing the end of the year 2018, the CIIF Management wishes to express our utmost gratitude and appreciation for your continuous support as a member of the CIIF.</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'><strong>Happy New&nbsp;Year</strong></p>
<p style='font-weight: 400;'>As 2019 will be the first blank page of another 365-page book, we would like to wish you and your family a blessed New Year full of peace, happiness, prosperity and health. We hope that you will achieve your goals for the coming years.</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'><strong>Membership Portal and CPIF</strong></p>
<p style='font-weight: 400;'>During the year 2018, we have been primarily focused on developing the CPIF modules (including piloting the programme) as well as building out new infrastructures that will enhance our services to members. Our aim for the year 2019 is to increase the value of your membership with the CIIF. One of the key outcomes of the CIIF's efforts this year is the development of the online membership portal that will be introduced in 2019, with your membership account pre-created based on your current information. Through the portal, you can update your profile and credentials, apply for the CPIF (the CIIF's new professional qualification), make payments of membership fees, RSVP to events, workshops and programmes, as well as update your CPD activities.&nbsp;</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>You may now access the portal by clicking at the following link:</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'><strong><a href='https://register.ciif-global.org/login'>https://register.ciif-global.org/login</a></strong>&nbsp;</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>The following are the information that is required for you to login:</p>
<p style='font-weight: 400;'>&nbsp;</p>
<ul style='font-weight: 400;'>
<li>Your NRIC or Passport number will be the user ID for your portal. Your user ID is&nbsp;<strong>J7793888</strong></li>
<li>Your password will be&nbsp;<strong>12345678</strong>. This password is temporary and you are required to change the password after you have successfully login.</li>
</ul>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>We urge all members to log in and update their profiles in the portal. We also welcome feedbacks from members on the said portal.</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'><strong>Continuing Professional Development (CPD)</strong></p>
<p style='font-weight: 400;'>In an ever-increasing globalised industry and competitive society, the importance of CPD cannot be overstated. The Islamic Finance industry is forever evolving, which creates exciting opportunities but also imposes new challenges along the way. CPD enables members to regularly focus on important areas of development and take appropriate actions to increase knowledge in a technologically driven environment. Equally, members should approach CPD as an opportunity to remain competitive with their peers, and as an opportunity to differentiate themselves in the work place. As more people become professionally qualified with similar qualifications, CPD becomes more important as a means of separating oneself from the pack.&nbsp;<br /><br />In this regard, we are pleased to introduce to you the CIIF CPD Standards and Guidelines. The standards and guidelines are a culmination of extensive engagement with members and industry on professional development. These documents are important as they help to ensure that further learning progresses in a structured, practical and relevant way, and to guarantee relevancy of course offerings. The CIIF CPD Standards and Guidelines allow members to focus on the specific skills and knowledge they require over a period of time in order to ensure recognisable improvement in their competency and skill sets within their work environment.&nbsp;</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>You may find further details of the CIIF CPD Standards and Guidelines by clicking at these links:</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>CIIF CPD Standards:&nbsp;<a href='https://ciif-global.org/web/wp-content/uploads/2018/12/CIIF-CPD-Standards.pdf'>https://ciif-global.org/web/wp-content/uploads/2018/12/CIIF-CPD-Standards.pdf</a></p>
<p style='font-weight: 400;'>CIIF CPD Guidelines:&nbsp;<a href='https://ciif-global.org/web/wp-content/uploads/2018/12/CIIF-CPD-Guidelines.pdf'>https://ciif-global.org/web/wp-content/uploads/2018/12/CIIF-CPD-Guidelines.pdf</a></p>
<p style='font-weight: 400;'><br />Should you need further assistance on the membership portal and CPD requirements, kindly contact our membership team:</p>
<p style='font-weight: 400;'>&nbsp;</p>
<ol style='font-weight: 400;'>
<li>Office line&nbsp;<strong>+603-2276 5279</strong></li>
<li>WhatsApp us (<strong>Click this!&nbsp;</strong><a href='http://www.wasap.my/+60192160611'><strong>http://www.wasap.my/+60192160611</strong></a>),</li>
<li>Forward this email to&nbsp;<a href='http://membership@ciif-global.org/'>membership@ciif-global.org</a></li>
</ol>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>Happy holidays to everyone and remember to follow our posts, join our tweets and have fabulous Insta time with CIIF.</p>
<p style='font-weight: 400;'>&nbsp;</p>
<p style='font-weight: 400;'>Thank you and best regards,</p>
<p style='font-weight: 400;'>&nbsp;CIIF Management Team</p>");

$transport = new SmtpTransport();
$options   = new SmtpOptions(array(
    'name'              => 'localhost',
    'host'              => '127.0.0.1',
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => 'rajashekhar4647@gmail.com',
        'password' => '8179237622',
    ),
));
$transport->setOptions($options);
// $transport->send($message);
}
catch(Exception $e){
    echo $e;
    }
}
public function sendMail1($to,$message1,$subject){


        try{
            
        $message = new Message();
        $message->addTo($to);
        $message->addFrom('admission.ciif@gmail.com','UUM');
        $message->setSubject($subject);



//         $text = new MimePart($textContent);
// $text->type = "text/plain";

$html = new MimePart($message1);
$html->type = "text/html";

// $image = new MimePart(fopen($pathToImage, 'r'));
// $image->type = "image/jpeg";

$body = new MimeMessage();
$body->setParts(array($html));
   
        $message->setBody($body);
        // $message->setType('html');

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
        'name'              => 'localhost',
        'host'              => 'smtp.gmail.com',
        'port'              => 587,
        'connection_class'  => 'login',
        'connection_config' => array(
            'ssl'      => "tls",
            'username' => "uum.utara.07@gmail.com",
            'password' => "trinionuum"
    )
));
$transport->setOptions($options);
// $transport->send($message);
}
catch(Exception $e){
    echo $e;
    }
}
public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("s.f030fid, s.f030fcompanyName, s.f030fcompanyRegistration, s.f030faccountNumber, s.f030fidBank, b.f042fbankName, st.f012fstateName, c.f013fcountryName, s.f030femail, s.f030faddress1, s.f030faddress2, s.f030faddress3, s.f030faddress4, s.f030fphone, s.f030ffax, s.f030fstatus, s.f030fvendorCode, (s.f030fvendorCode+' - '+s.f030fcompanyName) as Vendor, s.f030fstartDate, s.f030fendDate, s.f030fcity, s.f030fpostCode, s.f030fstate, s.f030fcountry, s.f030fwebsite,  s.f030fapprovalStatus,s.f030ffile, s.f030fnumberReceipt, s.f030fpayToProfile, s.f030fdebtorCode, s.f030fcompanyRegNo, s.f030faccountNo, s.f030faccountCode, s.f030frace,s.f030fuserStatus")
            ->from('Application\Entity\T030fvendorRegistration','s')
            ->leftjoin('Application\Entity\T042fbank','b','with', 's.f030fidBank=b.f042fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with', 's.f030fcountry=c.f013fid')
            ->leftjoin('Application\Entity\T012fstate','st','with', 's.f030fstate=st.f012fid')
            ->where('s.f030fapprovalStatus = 1')
            // ->leftjoin('Application\Entity\T031flicenseInformation','lc','with', 's.f030fid=lc.f031fidSupplierReg')
            // ->leftjoin('Application\Entity\T112fcontactDetails','cd','with', 's.f030fid=cd.f112fidVendor')
            // ->leftjoin('Application\Entity\T113fnatureOfBusiness','nb','with', 's.f030fid=nb.f113fidVendor')
            // ->leftjoin('Application\Entity\T115fvendorMof','m','with', 's.f030fid=m.f115fidVendor')
            ->andWhere('s.f030fid > 21193')
            ->orderBy('s.f030fid','DESC');
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $row) {
            $date                        = $row['f030fstartDate'];
            $row['f030fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $row['f030fendDate'];
            $row['f030fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $row);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    public function deleteVendorRegistration($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t030fvendor_registration WHERE f030fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteLicenseDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t031flicense_information WHERE f031fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteContactDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t112fcontact_details WHERE f112fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteNatureOfBusiness($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t113fnature_of_business WHERE f113fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deletevendorMof($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t115fvendor_mof WHERE f115fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deletePaytoProfileDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $query2 = "DELETE from t127fpayto_profile WHERE f127fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
    
}
