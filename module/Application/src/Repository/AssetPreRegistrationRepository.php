<?php
namespace Application\Repository;

use Application\Entity\T092fassetPreRegistration;
use Application\Entity\T092fpreRegistrationDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class AssetPreRegistrationRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

         $qb->select('a.f092fid, a.f092fidGrn, a.f092fgrnNumber, a.f092fgrnDate, a.f092fassetItem,  a.f092fquantity, a.f092fstatus')

            ->from('Application\Entity\T092fassetPreRegistration', 'a');
            
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('a.f092fid, a.f092fidGrn, a.f092fgrnNumber, a.f092fgrnDate, a.f092fassetItem, a.f092fquantity, a.f092fstatus, p.f092fidDetails, p.f092fpoReference, p.f092frefSeqNo, p.f092fidCategory, p.f092fidSubCategory, p.f092fidAssetType, p.f092fidAssetItem, p.f092fassetName, p.f092fsoCode, p.f092fidStaff, p.f092fstatus')

            ->from('Application\Entity\T092fassetPreRegistration', 'a')
            ->leftjoin('Application\Entity\T092fpreRegistrationDetails','p','with', 'a.f092fid= p.f092fidDetails')

            ->where('a.f092fid = :registrationId')
            ->setParameter('registrationId',$id);
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $result;
    }

    public function createNewData($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $registration = new T092fassetPreRegistration();

        $registration->setF092fidGrn((int)$data['f092fidGrn'])
             ->setF092fgrnNumber($data['f092fgrnNumber'])
             ->setF092fgrnDate(new \DateTime($data['f092fgrnDate']))
             ->setF092fassetItem($data['f092fassetItem'])
             ->setF092fquantity($data['f092fquantity'])
             ->setF092fstatus($data['f092fstatus'])
             ->setF092fcreatedBy((int)$_SESSION['userId'])
             ->setF092fupdatedBy((int)$_SESSION['userId']);

        $registration->setF092fcreatedDtTm(new \DateTime())
                ->setF092fupdatedDtTm(new \DateTime());
        
        $em->persist($registration);
        $em->flush();
        
        $registrationDetails = $data['registration-details'];

        foreach ($registrationDetails as $registrationDetail) {

            $registrationDetailObj = new T092fpreRegistrationDetails();

            $registrationDetailObj->setF092fidPre($registration->getF092fid())
                       ->setF092fpoReference($registrationDetail['f092fpoReference'])
                       ->setF092frefSeqNo($registrationDetail['f092frefSeqNo'])
                       ->setF092fidCategory($registrationDetail['f092fidCategory'])
                       ->setF092fidSubCategory($registrationDetail['f092fidSubCategory'])
                       ->setF092fidAssetType((int)$registrationDetail['f092fidAssetType'])
                       ->setF092fidAssetItem((int)$registrationDetail['f092fidAssetItem'])
                       ->setF092fassetName((int)$registrationDetail['f092fassetName'])
                       ->setF092fidStaff((int)$registrationDetail['f092fidStaff'])
                       ->setF092fstatus((int)$registrationDetail['f092fstatus'])
                       ->setF092fcreatedBy((int)$_SESSION['userId'])
                       ->setF092fupdatedBy((int)$_SESSION['userId']);

            $registrationDetailObj->setF092fcreatedDtTm(new \DateTime())
                       ->setF092fupdatedDtTm(new \DateTime());
        

        $em->persist($registrationDetailObj);

        $em->flush();

            
        }
        return $registration;
    }

    public function updateData($registration, $data = []) 
    {
        $em = $this->getEntityManager();

        $registration->setF092fidGrn((int)$data['f092fidGrn'])
             ->setF092fgrnNumber($data['f092fgrnNumber'])
             ->setF092fgrnDate(new \DateTime($data['f092fgrnDate']))
             ->setF092fassetItem($data['f092fassetItem'])
             ->setF092fquantity($data['f092fquantity'])
             ->setF092fstatus($data['f092fstatus'])
             ->setF092fupdatedBy((int)$_SESSION['userId']);

        $registration->setF092fupdatedDtTm(new \DateTime());


        
        $em->persist($registration);
        $em->flush();

        return $registration;

    }

    public function updateRegistrationDetail($assetDetailObj, $assetDetail) 
    {
        $em = $this->getEntityManager();

        $assetDetailObj ->setF092fpoReference($assetDetail['f092fpoReference'])
                       ->setF092frefSeqNo($assetDetail['f092frefSeqNo'])
                       ->setF092fidCategory($assetDetail['f092fidCategory'])
                       ->setF092fidSubCategory($assetDetail['f092fidSubCategory'])
                       ->setF092fidAssetType((int)$assetDetail['f092fidAssetType'])
                       ->setF092fidAssetItem((int)$assetDetail['f092fidAssetItem'])
                       ->setF092fassetName((int)$assetDetail['f092fassetName'])
                       ->setF092fidStaff((int)$assetDetail['f092fidStaff'])
                       ->setF092fstatus((int)$assetDetail['f092fstatus'])
                       ->setF092fupdatedBy((int)$_SESSION['userId']);

        $assetDetailObj->setF092fupdatedDtTm(new \DateTime());


        
        $em->persist($assetDetailObj);
        $em->flush();

        return $assetDetailObj;

    }

    public function createRegistartionDetail($assetDetailObj, $assetDetail) 
    {
        $em = $this->getEntityManager();

        $registrationDetailObj = new T092fpreRegistrationDetails();

        $registrationDetailObj->setF092fidPre($assetDetailObj->getF092fid())
                       ->setF092fpoReference($assetDetail['f092fpoReference'])
                       ->setF092frefSeqNo($assetDetail['f092frefSeqNo'])
                       ->setF092fidCategory($assetDetail['f092fidCategory'])
                       ->setF092fidSubCategory($assetDetail['f092fidSubCategory'])
                       ->setF092fidAssetType((int)$assetDetail['f092fidAssetType'])
                       ->setF092fidAssetItem((int)$assetDetail['f092fidAssetItem'])
                       ->setF092fassetName((int)$assetDetail['f092fassetName'])
                       ->setF092fidStaff((int)$assetDetail['f092fidStaff'])
                       ->setF092fstatus((int)$assetDetail['f092fstatus'])
                       ->setF092fupdatedBy((int)$_SESSION['userId']);

        $registrationDetailObj->setF092fupdatedDtTm(new \DateTime());


        
        $em->persist($registrationDetailObj);
        $em->flush();

        return $registrationDetailObj;

    }



}
