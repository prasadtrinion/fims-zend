<?php
namespace Application\Repository;

use Application\Entity\T093feaFormProcess;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class EaFormProcessRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('e.f093fid, e.f093fidCompany, c.f013fname as companyName, e.f093fyear, e.f093fidStaff, s.f034fname as staff, e.f093fstatus, f.f015fname as financialYear')
            ->from('Application\Entity\T093feaFormProcess', 'e')
            ->leftjoin('Application\Entity\T013fcompany', 'c', 'with','c.f013fid = e.f093fidCompany')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 's.f034fstaffId = e.f093fidStaff')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'e.f093fyear = f.f015fid')
            ->orderBy('e.f093fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('e.f093fid, e.f093fidCompany, c.f013fname as companyName, e.f093fyear, e.f093fidStaff, s.f034fname as staff, e.f093fstatus, f.f015fname as financialYear')
            ->from('Application\Entity\T093feaFormProcess', 'e')
            ->leftjoin('Application\Entity\T013fcompany', 'c', 'with','c.f013fid = e.f093fidCompany')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 's.f034fstaffId = e.f093fidStaff')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'e.f093fyear = f.f015fid')
            ->where('e.f093fid = :eaFormProcessId')
            ->setParameter('eaFormProcessId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $eaFormProcess = new T093feaFormProcess();
        $eaFormProcess->setF093fidCompany((int)$data['f093fidCompany'])
                ->setF093fyear((int)$data['f093fyear'])
                ->setF093fidStaff((int)$data['f093fidStaff'])
                ->setF093fstatus((int)$data['f093fstatus'])
                ->setF093fcreatedBy((int)$_SESSION['userId'])
                ->setF093fupdatedBy((int)$_SESSION['userId']);

        $eaFormProcess->setF093fcreatedDtTm(new \DateTime())
                ->setF093fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($eaFormProcess);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $eaFormProcess;
    }
    public function updateData($eaFormProcess, $data = []) 
    {
        $em = $this->getEntityManager();
        $eaFormProcess->setF093fidCompany((int)$data['f093fidCompany'])
                ->setF093fyear((int)$data['f093fyear'])
                ->setF093fidStaff((int)$data['f093fidStaff'])
                ->setF093fstatus((int)$data['f093fstatus'])
                ->setF093fcreatedBy((int)$_SESSION['userId'])
                ->setF093fupdatedBy((int)$_SESSION['userId']);

        $eaFormProcess->setF093fcreatedDtTm(new \DateTime())
                ->setF093fupdatedDtTm(new \DateTime());


        $em->persist($eaFormProcess);
        $em->flush();
    }
}