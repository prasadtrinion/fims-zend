<?php
namespace Application\Repository;

use Application\Entity\T103fprocessType;
use Application\Entity\T103fprocessTypeDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class ProcessTypeRepository extends EntityRepository 
{

     public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('c.f103fid,c.f103fname,c.f103fstatus')
            ->from('Application\Entity\T103fprocessType', 'c')
            ->orderBy('c.f103fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('c.f103fid,c.f103fname,c.f103fstatus, i.f103fidDetails,i.f103fidType')
            ->from('Application\Entity\T103fprocessType', 'c')
            ->leftjoin('Application\Entity\T103fprocessTypeDetails', 'i', 'with', 'i.f103fidMaster = c.f103fid')
            ->where('c.f103fid = :Id')
            ->setParameter('Id',(int)$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data)
    {


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $process = new T103fprocessType();

        $process->setF103fname($data['f103fname'])
            ->setF103fstatus((int) $data['f0103fstatus'])
            ->setF103fcreatedBy((int)$_SESSION['userId'])
            ->setF103fupdatedBy((int)$_SESSION['userId']);

        $process->setF103fcreatedDtTm(new \DateTime())
            ->setF103fupdatedDtTm(new \DateTime());

            try{
            $em->persist($process);

            $em->flush();
}
catch(\Exception $e)
{
    echo $e;
}        

        $processDetails = $data['process-details'];
        
            foreach ($processDetails as $processDetail)  {

            $processDetailObj = new T103fprocessTypeDetails();

            $processDetailObj->setF103fidMaster((int)$process->getF103fid())
                ->setF103fidType((int)$processDetail['f103fidType'])
                ->setF103fstatus((int)"1")
                ->setF103fcreatedBy((int)$_SESSION['userId'])
                ->setF103fupdatedBy((int)$_SESSION['userId']);
                

            $processDetailObj->setF103fcreatedDtTm(new \DateTime())
                ->setF103fupdatedDtTm(new \DateTime());
            try{
                $em->persist($processDetailObj);

                $em->flush();
                }
catch(\Exception $e)
{
    echo $e;
}        
            
        }
        return $process;
    }

    public function updateProcess($process, $data = [])
    {
        $em = $this->getEntityManager();

         $process->setF103fname($data['f103fname'])
            ->setF103fstatus((int) $data['f0103fstatus'])
            ->setF103fupdatedBy((int)$_SESSION['userId']);

        $process->setF103fupdatedDtTm(new \DateTime());

            try{
            $em->persist($process);

            $em->flush();
}
catch(\Exception $e)
{
    echo $e;
}        
        return $process;
    }


    public function updateDetail($processDetailObj, $processDetail)
    {
        
        $em = $this->getEntityManager();

         $processDetailObj->setF103fidType((int)$processDetail['f103fidType'])
                ->setF103fstatus((int)"1")
                ->setF103fupdatedBy((int)$_SESSION['userId']);
                

            $processDetailObj->setF103fupdatedDtTm(new \DateTime());
            try{
                $em->persist($processDetailObj);

                $em->flush();
                }
catch(\Exception $e)
{
    echo $e;
}        
    }
     
    public function createDetail($processObj, $processDetail)
    {
        $em = $this->getEntityManager();
        // print_r($cashbankObj);
        // exit();

       $processDetailObj = new T103fprocessTypeDetails();

       $processDetailObj->setF103fidMaster((int)$processObj->getF103fid())
                ->setF103fidType((int)$processDetail['f103fidType'])
                ->setF103fstatus((int)"1")
                ->setF103fcreatedBy((int)$_SESSION['userId'])
                ->setF103fupdatedBy((int)$_SESSION['userId']);
                

            $processDetailObj->setF103fcreatedDtTm(new \DateTime())
                ->setF103fupdatedDtTm(new \DateTime());
            try{
                $em->persist($processDetailObj);

                $em->flush();
                }
catch(\Exception $e)
{
    echo $e;
}        
    }
   

}

