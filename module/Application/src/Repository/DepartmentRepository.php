<?php
namespace Application\Repository;

use Application\Entity\T015fdepartment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DepartmentRepository extends EntityRepository 
{

    public function getList() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query1 = "select f015fid,f015fdepartment_name as originalname,f015fdepartment_code +' - '+ f015fdepartment_name  as f015fdepartmentName,f015fdepartment_code as f015fdepartmentCode,f015funit, f015fdept_code as f015fdeptCode, f015fstatus, f015fupdated_by as f015fupdatedBy from t015fdepartment  order by f015fdepartment_code ASC";
        $res = $em->getConnection()->executeQuery($query1)->fetchAll();

        // $qb->select("d.f015fid,d.f015fdepartmentName as originalname,d.f015fdepartmentCode +' - '+ d.f015fdepartmentName  as f015fdepartmentName,d.f015fdepartmentCode, d.f015funit, d.f015fdeptCode, d.f015fstatus, d.f015fupdatedBy")
        //     ->from('Application\Entity\T015fdepartment','d')
        //     ->where("right(d.f015fdepartmentCode,2) = '00'")
        //     ->orderBy('originalname');

        
        // $query = $qb->getQuery();

        $result = array(
            'data' => $res//$query->getArrayResult(),
        );

        return $result;
        
    }

    public function getZeroDepartments() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query1 = "select f015fid,f015fdepartment_name as originalname,f015fdepartment_code +' - '+ f015fdepartment_name  as f015fdepartmentName,f015fdepartment_code as f015fdepartmentCode,f015funit, f015fdept_code as f015fdeptCode, f015fstatus, f015fupdated_by as f015fupdatedBy from t015fdepartment where f015funit='00'  order by f015fid DESC";
        $res = $em->getConnection()->executeQuery($query1)->fetchAll();

        // $qb->select("d.f015fid,d.f015fdepartmentName as originalname,d.f015fdepartmentCode +' - '+ d.f015fdepartmentName  as f015fdepartmentName,d.f015fdepartmentCode, d.f015funit, d.f015fdeptCode, d.f015fstatus, d.f015fupdatedBy")
        //     ->from('Application\Entity\T015fdepartment','d')
        //     ->where("right(d.f015fdepartmentCode,2) = '00'")
        //     ->orderBy('originalname');

        
        // $query = $qb->getQuery();

        $result = array(
            'data' => $res//$query->getArrayResult(),
        );

        return $result;
        
    }

    public function getByUnitCode($code) 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query1 = "select f015fid,f015fdepartment_name as originalname,f015fdepartment_code +' - '+ f015fdepartment_name  as f015fdepartmentName,f015fdepartment_code as f015fdepartmentCode,f015funit, f015fdept_code as f015fdeptCode, f015fstatus, f015fupdated_by as f015fupdatedBy from t015fdepartment where f015fdept_code='$code' order by f015fid DESC";
        $res = $em->getConnection()->executeQuery($query1)->fetchAll();

        // $qb->select("d.f015fid,d.f015fdepartmentName as originalname,d.f015fdepartmentCode +' - '+ d.f015fdepartmentName  as f015fdepartmentName,d.f015fdepartmentCode, d.f015funit, d.f015fdeptCode, d.f015fstatus, d.f015fupdatedBy")
        //     ->from('Application\Entity\T015fdepartment','d')
        //     ->where("right(d.f015fdepartmentCode,2) = '00'")
        //     ->orderBy('originalname');

        
        // $query = $qb->getQuery();

        $result = array(
            'data' => $res//$query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('d.f015fid, d.f015fdepartmentName, d.f015fdepartmentCode, d.f015funit, d.f015fdeptCode, d.f015fstatus, d.f015fupdatedBy')
            ->from('Application\Entity\T015fdepartment','d')
            ->where('d.f015fid = :departmentId')
            ->setParameter('departmentId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }
    
    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $department = new T015fdepartment();

        $department->setF015fdepartmentName($data['f015fdepartmentName'])
                ->setF015funit($data['f015funit'])
                ->setF015fdeptCode($data['f015fdeptCode'])
                ->setF015fdepartmentCode($data['f015fdepartmentCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $department->setF015fupdatedDtTm(new \DateTime());


        try{
        $em->persist($department);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        
        
        return $department;
    }

    public function updateData($department, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $department->setF015fdepartmentName($data['f015fdepartmentName'])
                ->setF015funit($data['f015funit'])
                ->setF015fdeptCode($data['f015fdeptCode'])
                ->setF015fdepartmentCode($data['f015fdepartmentCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $department->setF015fupdatedDtTm(new \DateTime());
       
        $em->persist($department);
        $em->flush();
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("d.f015fid,d.f015fdepartmentName as originalname, d.f015fdepartmentCode, d.f015funit, d.f015fdeptCode, d.f015fstatus, d.f015fupdatedBy, (d.f015fdepartmentCode +' - '+ d.f015fdepartmentName)  as f015fdepartmentName")
            ->from('Application\Entity\T015fdepartment','d')
            ->where('d.f015fstatus=1')
            ->orderBy('originalname');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}

?>
