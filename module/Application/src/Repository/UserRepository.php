<?php
namespace Application\Repository;

use Application\Entity\Role;
use Application\Entity\User;
use Application\Entity\UserHasRole;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * T014fuserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class userRepository extends EntityRepository {

	/**
	 * Get users
	 *
	 * @param array $filters
	 * @param string $search
	 * @param array $sort
	 * @param number $offset
	 * @param number $limit
	 * @return NULL[]|mixed[]
	 */
	public function getUsers($filters = array(), $search = null, $sort = array(), $offset = 0, $limit = 10, $currentUserId = null) {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();

		// Query
		$qb->select('count(u.id) as total')
			->from('Application\Entity\User', 'u')
			->leftjoin('u.account', 'a')
			->leftjoin('Application\Entity\UserHasRole', 'ur', 'with', 'u.id = ur.userId')
			->leftjoin('Application\Entity\Role', 'r', 'with', 'ur.roleId = r.id')
			->where('a.deleteFlag != 1');
		if ($currentUserId) {
			$qb->andWhere('u.id != :currentUser')
				->setParameter('currentUser', $currentUserId);
		}

		// Filter
		if (isset($filters['role'])) {
			$qb->andWhere('r.id = :role')->setParameter('role', $filters['role']);
		}

		// For multiple roles
		if (isset($filters['roles'])) {
		    $qb->andWhere('r.id IN (:roles)')->setParameter('roles', $filters['roles']);
		}
		
		if (isset($filters['status'])) {
			$qb->andWhere('a.status = :status')->setParameter('status', $filters['status']);
		}

		// Search
		if (!empty($search)) {
			$expr = $qb->expr();
			$qb->andWhere($expr->orX(
				$expr->like('a.username', ':search'),
				$expr->like('u.companyName', ':search'),
				$expr->like('u.lastName', ':search'),
				$expr->like('u.firstName', ':search'),
				$expr->like('u.diverseyId', ':search')
			));

			$qb->setParameter('search', "%{$search}%");
		}

		// Get total records
		$total = $qb->getQuery()->getSingleScalarResult();

		$qb->select('u.id, u.firstName, u.lastName, u.companyName, u.diverseyId, u.country, u.phoneNumber,u.updateDtTm,
                    a.username as email, a.username, a.emailStatus, a.status,
                    r.role, ur.roleId');

		// Sort
		$column = 'updateDtTm';
		$order = 'desc';

		if (!empty($sort)) {
			$column = $sort['column'];
			$order = $sort['order'];
		}

		switch ($column) {
		case 'lastName':
			$column = 'u.lastName';
			break;
		case 'firstName':
			$column = 'u.firstName';
			break;
		case 'companyName':
			$column = 'u.companyName';
			break;
		default:
			$column = 'u.updateDtTm';
		}

		$qb->orderBy($column, $order);

		// Limit
		if ($limit > 0) {
			$qb->setFirstResult($offset)->setMaxResults($limit);
		}

		$query = $qb->getQuery();

		$result = array(
			'total' => $total,
			'data' => $query->getArrayResult(),
		);

		return $result;
	}

	/**
	 * Function to update the user info
	 *
	 * @param object $entity
	 */
	public function update($entity) {
		$em = $this->getEntityManager();
		$em->persist($entity);
		$em->flush();
	}

	/**
	 * Function to save user info
	 *
	 * @param object $accountObj
	 * @param array $data
	 */
	public function createUser($accountObj, $data) {
		$em = $this->getEntityManager();

		$user = new User();
		$user->setAccount($accountObj)
			->setName($data['name'])
			->setEmail($data['email']);
			
		// Contact information
		$user->setPhoneNumber($data['mobile']);

		$user->setCreateDtTm(new \DateTime())
    		->setUpdateDtTm(new \DateTime());
		
		$em->persist($user);
		$em->flush();

		// Assign user role
		$userHasRole = new UserHasRole();
		$userHasRole->setUserId($user->getId())
				->setRoleId($data['roleId']);
		
		$em->persist($userHasRole);
		$em->flush();
	}

	/**
	 * Function to update user info
	 *
	 * @param object $accountObj
	 * @param array $data
	 */
	public function updateUser($user, $data = []) 
	{
        $em = $this->getEntityManager();
        
        $user->setFirstName($data['firstName'])
            ->setLastName($data['lastName'])
            ->setEmail($data['username']);
        
        // Contact information
        $user->setSalesExecutive($data['salesExecutive'])
            ->setAddressLine1($data['addressLine1'])
            ->setPhoneNumber($data['phoneNumber'])
            ->setCountry($data['country'])
            ->setState($data['state'])
            ->setCity($data['city'])
            ->setZipCode($data['zipCode'])
            ->setContactType($data['contactType']);
        
        // Distributor information
        $user->setCompanyName($data['companyName'])
            ->setDistAddressLine1($data['distAddressLine1'])
            ->setDistPhoneNumber($data['distPhoneNumber'])
            ->setDistCountry($data['distCountry'])
            ->setDistState($data['distState'])
            ->setDistCity($data['distCity'])
            ->setDistZipCode($data['distZipCode']);
        
        // Set DSR type
        $dsrType = !empty($data['dsrType']) ? $data['dsrType'] : null;
        $user->setDsrType($dsrType);
        
        if (isset($data['mailingList']) && ! empty($data['mailingList'])) {
            $user->setAddressLine2($data['mailingList']);
        }
        
        if (isset($data['addressLine2']) && ! empty($data['addressLine2'])) {
            $user->setAddressLine2($data['addressLine2']);
        }
        
        if (isset($data['diverseyId']) && ! empty($data['diverseyId'])) {
            $user->setDiverseyId($data['diverseyId']);
        }
        
        $user->setUpdateDtTm(new \DateTime());
        
        $em->persist($user);
        $em->flush();
    }
	
	/**
	 * Get display names for roles
	 *
	 * @return array
	 */
	public function getRoleDisplayNames() {
		return array(
			Role::ROLE_SYSTEM_ADMIN => 'System Admin',
			Role::ROLE_CSR => 'CSR',
			Role::ROLE_DSR => 'DSR',
		);
	}

	
	/**
	 * Get display names for roles
	 *
	 * @return array
	 */
	public function getDsrTypes() 
	{
        return [
            User::DSR_STANDARD => 'Standard',
            User::DSR_PREMIERE => 'Premiere'
        ];
    }
	
	/**
	 * Function to get user role based on user id
	 * @param int $userId
	 * return $roleId
	 */
	public function getRoleByUser($userId) {
		$em = $this->getEntityManager();
		$conn = $em->getConnection();
		$query = "Select role_id from user_has_role where user_id = :userId ";

		$stmt = $conn->prepare($query);
		$stmt->bindValue(':userId', $userId);
		$stmt->execute();

		$roleId = $stmt->fetchColumn();

		return $roleId;
	}

	public function getRoleKey($userId) {
		$em = $this->getEntityManager();
		$conn = $em->getConnection();
		$query = "Select id from user_has_role where user_id = :userId ";

		$stmt = $conn->prepare($query);
		$stmt->bindValue(':userId', $userId);
		$stmt->execute();

		$Id = $stmt->fetchColumn();

		return $Id;
	}

	public function updateRole($roleKey, $roleId) {
		$em = $this->getEntityManager();
		$conn = $em->getConnection();
		$query = "UPDATE user_has_role SET role_id = :roleId WHERE id LIKE :roleKey";
		$stmt = $conn->prepare($query);
		$stmt->bindValue(':roleId', (int) $roleId);
		$stmt->bindValue(':roleKey', (int) $roleKey);
		$stmt->execute();
	}

	/**
	 * Get user details
	 */
	public function getUserDetails($userId) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('u.id, u.firstName, u.lastName, u.companyName, u.diverseyId, u.contactType,
		            u.addressLine1, u.addressLine2, u.country, u.state, u.city, u.phoneNumber, u.zipCode, 
		            u.distAddressLine1, u.distAddressLine2, u.distCountry, u.distState, u.distCity, u.distPhoneNumber, u.distZipCode,
		            u.salesExecutive, u.dsrType,
                    a.username, a.status, a.emailStatus, 
                    r.role, r.id as roleId')
			->from('Application\Entity\User', 'u')
			->leftjoin('u.account', 'a')
			->leftjoin('Application\Entity\UserHasRole', 'ur', 'with', 'u.id = ur.userId')
			->leftjoin('Application\Entity\Role', 'r', 'with', 'ur.roleId = r.id')
			->where('a.deleteFlag != 1')
			->andWhere('u.id = :userId')
			->setParameter('userId', $userId);

		$query = $qb->getQuery();
		$result = $query->getSingleResult(2);

		return $result;
	}

	public function getUserDetailsByEmail($email) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('u.id, u.firstName, u.lastName, u.companyName, u.diverseyId, u.addressLine1, u.addressLine2, u.country, u.state,
                    u.phoneNumber, u.zipCode, u.contactType,
                    a.username, a.status, a.emailStatus,
                    r.role, r.id as roleId')
			->from('Application\Entity\User', 'u')
			->leftjoin('u.account', 'a')
			->leftjoin('Application\Entity\UserHasRole', 'ur', 'with', 'u.id = ur.userId')
			->leftjoin('Application\Entity\Role', 'r', 'with', 'ur.roleId = r.id')
			->where('a.deleteFlag != 1')
			->andWhere('a.username = :email')
			->setParameter('email', $email);
		$query = $qb->getQuery();
		$result = $query->getArrayResult();

		return $result[0];
	}

	public function getUserByAccount($id) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('u')
			->from('Application\Entity\User', 'u')
			->where('u.account = :id')
			->setParameter('id', $id);
		
		$query = $qb->getQuery();
		$result = $query->getSingleResult(2);
		
		return $result;

	}
}
