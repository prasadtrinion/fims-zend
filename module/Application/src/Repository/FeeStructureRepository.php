<?php
namespace Application\Repository;

use Application\Entity\T048ffeeStructureMaster;
use Application\Entity\T048ffeeStructureDetails;
use Application\Entity\T048ffeeStructureProgram;
use Application\Entity\T048fprogramCourseFee;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class FeeStructureRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
         

        $qb->select('f.f048fid, f.f048fname, f.f048fcode, f.f048ffeeType,f.f048feffectiveDate,f.f048fisProgramAttached, f.f048fidIntake, f.f048flearningCentre, f.f048fpartner, f.f048fcitizen,  f.f048fregion,  f.f048fstudentCategory, s.f011fdefinitionCode as studentCategory, f.f048fstudyMode, m.f011fdefinitionCode as studyMode, f.f048ftotal, f.f048fstatus, f.f048fisProgram')
            ->from('Application\Entity\T048ffeeStructureMaster', 'f')
         //   ->leftjoin('Application\Entity\T067fintake','i','with', 'f.f048fidIntake = i.f067fid')
            // ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'f.f048fcitizen = d.f011fid')
          //  ->leftjoin('Application\Entity\T011fdefinationms','r','with', 'f.f048fregion = r.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','s','with', 'f.f048fstudentCategory = s.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','m','with', 'f.f048fstudyMode = m.f011fid')
            ->orderBy('f.f048fid','DESC');
             
         
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
	public function getListById($id)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();


		$qb->select('f.f048fid, f.f048fname, f.f048fcode,f.f048ffeeType, f.f048feffectiveDate, f.f048fgraduationType, f.f048fidIntake, f.f048flearningCentre, f.f048fpartner, f.f048fcitizen, f.f048fregion, f.f048fstudentCategory, f.f048fstudyMode, f.f048ftotal, f.f048fstatus, f.f048fisProgramAttached, f.f048fisProgram')
            ->from('Application\Entity\T048ffeeStructureMaster', 'f')    
			->where('f.f048fid =:feeId')
			->setParameter('feeId', $id);

		$query = $qb->getQuery();
		$result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = $result1[0]; 
        $date                        = $result1['f048feffectiveDate'];
        $result1['f048feffectiveDate'] = date("Y-m-d", strtotime($date));

        $qb = $em->createQueryBuilder();
        $qb->select("f.f048fidDetails, f.f048fidFee, f.f048fidFeeItem, f.f048fidFeeCategory, f.f048fidCurrency, f.f048ffrequencyMode, f.f048fcalType, f.f048fcrFund, f.f048fcrActivity, f.f048fcrDepartment, f.f048fcrAccount, f.f048fdrFund, f.f048fdrActivity, f.f048fdrDepartment, f.f048fdrAccount, f.f048famount, f.f048fstatus, f.f048ffeeType")
            ->from('Application\Entity\T048ffeeStructureDetails', 'f')
            // ->leftjoin('Application\Entity\T120ffeeType','ft','with','f.f048ffeeType = ft.f120fid')
            ->where('f.f048fidFee =:feeId')
            ->setParameter('feeId', $id);

        $query = $qb->getQuery();
        $result2 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1['fee-details'] = $result2;

        $qb = $em->createQueryBuilder();
        $qb->select('f.f048fidStructProgram,f.f048fidFeeProgram,f.f048fidProgramme,f.f048fstatus')
            ->from('Application\Entity\T048ffeeStructureProgram', 'f')
            ->where('f.f048fidFeeProgram =:feeId')
            ->setParameter('feeId', $id);

        $query = $qb->getQuery();
        $result3 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1['program-details'] = $result3;

        $qb = $em->createQueryBuilder();
        $qb->select('f.f048fidProgramCourse, f.f048fidCourseFee, f.f048fidProgram, f.f048fidSubject, f.f048famount, f.f048fstatus')
            ->from('Application\Entity\T048fprogramCourseFee', 'f')
            ->where('f.f048fidCourseFee =:feeId')
            ->setParameter('feeId', $id);
        $query = $qb->getQuery();
        $result3 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1['course-details'] = $result3;

		return $result1;
	}
    // public function getListById($id)
    // {
    //     $em = $this->getEntityManager();
    //     $qb = $em->createQueryBuilder();

    //     $qb->select('f.f048fid, f.f048fname, f.f048fcode, f.f048feffectiveDate, f.f048fidIntake,  f.f048flearningCentre, f.f048fpartner, f.f048fcitizen,  f.f048fregion,  f.f048fstudentCategory, f.f048ftotal, f.f048fstatus, fd.f048fidDetails, fd.f048fidFeeItem, fd.f048fidCurrency, fd.f048fcalType, fd.f048fcrFund, fd.f048fcrActivity, fd.f048fcrDepartment, fd.f048fcrAccount, fd.f048fdrFund, fd.f048fdrActivity, fd.f048fdrDepartment, fd.f048fdrAccount, fp.f048fidStructProgram, fp.f048fidProgramme, fc.f048fidProgram, fc.f048fidSubject, fc.f048famount')
    //         ->from('Application\Entity\T048ffeeStructureMaster', 'f')
    //         ->leftJoin('Application\Entity\T048ffeeStructureProgram', 'fp', Expr\Join::WITH,'f.f048fid = fp.f048fidFeeProgram')
    //         ->leftJoin('Application\Entity\T048ffeeStructureDetails', 'fd', Expr\Join::WITH,'f.f048fid = fd.f048fidFee')
    //         ->leftJoin('Application\Entity\T048fprogramCourseFee', 'fc', Expr\Join::WITH,'f.f048fid = fc.f048fidProgramCourse')
    //         // ->leftjoin('Application\Entity\T067fintake','i','with', 'f.f048fidIntake = i.f067fid')
    //         // ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'f.f048fcitizen = d.f011fid')
    //         // ->leftjoin('Application\Entity\T011fdefinationms','r','with', 'f.f048fregion = r.f011fid')
    //         // ->leftjoin('Application\Entity\T011fdefinationms','s','with', 'f.f048fstudentCategory = s.f011fid')
    //         // ->leftjoin('Application\Entity\T011fdefinationms','m','with', 'f.f048fstudyMode = m.f011fid')
    //          // ->leftjoin('Application\Entity\T068fprogramme','p','with', 'f.f048fidProgramme = p.f068fid')
    //         ->where('f.f048fid =:feeId')
    //         ->setParameter('feeId', $id);

    //     $query = $qb->getQuery();
    //     $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    //     return $result;
    // }

	public function createFee($data) 
	{

		$em = $this->getEntityManager();

        $FeeType = (int)$data['f048ffeeType'];
        $intake = $data['f048fidIntake'];

        $query = "select * from t120ffee_type WHERE f120fid = $FeeType";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        $numberData = array();
        $numberData['code'] = $result['f120fcode'];

        $query1 = "select * from t067fintake WHERE f067fname like '$intake' ";
        $result1 = $em->getConnection()->executeQuery($query1)->fetch();

        $numberData['intake'] = $result1['f067fname'];

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData['type']="FEESTRUCTURE";
        $number = $configRepository->generateFIMS($numberData);
        // print_r($number);exit;

		$fee = new T048ffeeStructureMaster();
       

		$fee->setF048fname($data['f048fname'])
			    ->setF048fcode($number)
			    ->setF048fgraduationType($data['f048fgraduationType'])
			    ->setF048feffectiveDate(new \DateTime($data['f048feffectiveDate']))
			    ->setF048fidIntake($data['f048fidIntake'])
			    ->setF048flearningCentre($data['f048flearningCentre'])
			    ->setF048fpartner($data['f048fpartner'])
			    ->setF048fcitizen($data['f048fcitizen'])
			    ->setF048fregion($data['f048fregion'])
			    ->setF048fstudentCategory((int)$data['f048fstudentCategory'])
                ->setF048fstudyMode((int)$data['f048fstudyMode'])
                ->setF048ftotal((float)$data['f048ftotal'])
                ->setF048ffeeType((int)$data['f048ffeeType'])
                ->setF048fisProgramAttached((int)$data['f048fisProgramAttached'])
                ->setF048fisProgram((int)$data['f048fisProgram'])
			    ->setF048fstatus((int)$data['f048fstatus'])
			    ->setF048fcreatedBy((int)$_SESSION['userId'])
			    ->setF048fupdatedBy((int)$_SESSION['userId']);

		$fee->setF048fcreatedDtTm(new \DateTime())
    			->setF048fupdatedDtTm(new \DateTime());


		try{
		$em->persist($fee);
		$em->flush();
		}
		catch(\Exception $e){
			echo $e;
		}
        $feePrograms = $data['fee-program']; 

        foreach ($feePrograms as $feeProgram) 
        {

            $feeProgramObj = new T048ffeeStructureProgram();

            $feeProgramObj->setF048fidFeeProgram((int)$fee->getF048fid())
                ->setF048fidProgramme($feeProgram['f048fidProgramme'])
                ->setF048fstatus((int) $feeDetail['f048fstatus'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeProgramObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            	try{
                $em->persist($feeProgramObj);

                $em->flush();
		}
		catch(\Exception $e){
			echo $e;
		}
            
        }

		$feeDetails = $data['fee-details'];

        foreach ($feeDetails as $feeDetail) 
        {

            $feeDetailObj = new T048ffeeStructureDetails();

            $feeDetailObj->setF048fidFee((int)$fee->getF048fid())
                ->setF048fidFeeItem((int)$feeDetail['f048fidFeeItem'])
                ->setF048fidCurrency((int)$feeDetail['f048fidCurrency'])
                ->setF048fcalType((int)$feeDetail['f048fcalType'])
                ->setF048fidFeeCategory((int)$feeDetail['f048fidFeeCategory'])
                ->setF048famount((float)$feeDetail['f048famount'])
                ->setF048fstatus((int) $feeDetail['f048fstatus'])
                ->setF048ffeeType((int)$feeDetail['f048ffeeType'])
                ->setF048fcrFund($feeDetail['f048fcrFund'])
                ->setF048fcrActivity($feeDetail['f048fcrActivity'])
                ->setF048fcrDepartment($feeDetail['f048fcrDepartment'])
                ->setF048fcrAccount($feeDetail['f048fcrAccount'])
                ->setF048fdrFund($feeDetail['f048fdrFund'])
                ->setF048fdrActivity($feeDetail['f048fdrActivity'])
                ->setF048fdrDepartment($feeDetail['f048fdrDepartment'])
                ->setF048fdrAccount($feeDetail['f048fdrAccount'])
                ->setF048ffrequencyMode((int)$data['f048ffrequencyMode'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeDetailObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            	try{
                $em->persist($feeDetailObj);
                $em->flush();
		}
		catch(\Exception $e)
		{
			echo $e;
		}
            
        }

        $courseFees = $data['course-fee'];

        foreach ($courseFees as $courseFee) 
        {

            $courseFeeObj = new T048fprogramCourseFee();

            $courseFeeObj->setF048fidCourseFee((int)$fee->getF048fid())
                ->setF048fidProgram((int)$courseFee['f048fidProgram'])
                ->setF048fidSubject((int)$courseFee['f048fidSubject'])
                ->setF048famount((float)$courseFee['f048famount'])
                ->setF048fstatus((int) $courseFee['f048fstatus'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $courseFeeObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            
            $em->persist($courseFeeObj);

            $em->flush();

            
        }

		return $fee;
	}

	public function updateFeeMaster($fee, $data = []) 
	{
        $em = $this->getEntityManager();
        
         $fee->setF048fname($data['f048fname'])
                ->setF048fcode($data['f048fcode'])
		->setF048fgraduationType($data['f048fgraduationType'])
                ->setF048feffectiveDate(new \DateTime($data['f048feffectiveDate']))
                ->setF048fidIntake((int)$data['f048fidIntake'])
                ->setF048flearningCentre($data['f048flearningCentre'])
                ->setF048ffeeType((int)$data['f048ffeeType'])
                ->setF048fpartner($data['f048fpartner'])
                ->setF048fcitizen($data['f048fcitizen'])
                ->setF048fregion((int)$data['f048fregion'])
                ->setF048fstudentCategory((int)$data['f048fstudentCategory'])
                ->setF048fstudyMode((int)$data['f048fstudyMode'])
                ->setF048ftotal($data['f048ftotal'])
                ->setF048fisProgramAttached((int)$data['f048fisProgramAttached'])
                ->setF048fisProgram((int)$data['f048fisProgram'])
                ->setF048fstatus((int)$data['f048fstatus'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);

        $fee->setF048fupdatedDtTm(new \DateTime());


		
		$em->persist($fee);
		$em->flush();

        return $fee;

    }

    public function updateFeeDetail($feeDetailObj, $feeDetail)
    {
        
        $em = $this->getEntityManager();

        $feeDetailObj->setF048fidFeeItem((int)$feeDetail['f048fidFeeItem'])
                ->setF048fidCurrency((int)$feeDetail['f048fidCurrency'])
                ->setF048fcalType((int)$feeDetail['f048fcalType'])
                ->setF048fidFeeCategory((int)$feeDetail['f048fidFeeCategory'])
                ->setF048famount((float)$feeDetail['f048famount'])
                ->setF048ffeeType((int)$feeDetail['f048ffeeType'])
                ->setF048fstatus((int) $feeDetail['f048fstatus'])
                ->setF048fcrFund($feeDetail['f048fcrFund'])
                ->setF048fcrActivity($feeDetail['f048fcrActivity'])
                ->setF048fcrDepartment($feeDetail['f048fcrDepartment'])
                ->setF048fcrAccount($feeDetail['f048fcrAccount'])
                ->setF048fdrFund($feeDetail['f048fdrFund'])
                ->setF048fdrActivity($feeDetail['f048fdrActivity'])
                ->setF048fdrDepartment($feeDetail['f048fdrDepartment'])
                ->setF048fdrAccount($feeDetail['f048fdrAccount'])
                ->setF048ffrequencyMode((int)$data['f048ffrequencyMode'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeDetailObj->setF048fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($feeDetailObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
        return $feeDetailObj;
    }
    
    public function createFeeDetail($fee, $feeDetail)
    {
        $em = $this->getEntityManager();

       $feeDetailObj = new T048ffeeStructureDetails();

            $feeDetailObj->setF048fidFee((int)$fee->getF048fid())
                ->setF048fidFeeItem((int)$feeDetail['f048fidFeeItem'])
                ->setF048fidCurrency((int)$feeDetail['f048fidCurrency'])
                ->setF048fcalType((int)$feeDetail['f048fcalType'])
                ->setF048fidFeeCategory((int)$feeDetail['f048fidFeeCategory'])
                ->setF048famount((float)$feeDetail['f048famount'])
                ->setF048ffeeType((int)$feeProgram['f048ffeeType'])
                ->setF048fstatus((int) $feeDetail['f048fstatus'])
                ->setF048fcrFund($feeDetail['f048fcrFund'])
                ->setF048fcrActivity($feeDetail['f048fcrActivity'])
                ->setF048fcrDepartment($feeDetail['f048fcrDepartment'])
                ->setF048fcrAccount($feeDetail['f048fcrAccount'])
                ->setF048fdrFund($feeDetail['f048fdrFund'])
                ->setF048fdrActivity($feeDetail['f048fdrActivity'])
                ->setF048fdrDepartment($feeDetail['f048fdrDepartment'])
                ->setF048fdrAccount($feeDetail['f048fdrAccount'])
                ->setF048ffrequencyMode((int)$data['f048ffrequencyMode'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeDetailObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            
                $em->persist($feeDetailObj);

                $em->flush();

        return $feeDetailObj;
    }

     public function updateFeeProgram($feeProgramObj, $feeProgram)
    {
        
        $em = $this->getEntityManager();

            $feeProgramObj->setF048fidProgramme((int)$feeProgram['f048fidProgramme'])
                ->setF048fstatus((int) $feeProgram['f048fstatus'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeProgramObj->setF048fupdatedDtTm(new \DateTime());
            
                $em->persist($feeProgramObj);

                $em->flush();

        return $feeProgramObj;
        
    }
    
    public function createFeeProgram($fee, $feeProgram)
    {
        $em = $this->getEntityManager();

       $feeProgramObj = new T048ffeeStructureProgram();

            $feeProgramObj->setF048fidFeeProgram((int)$fee->getF048fid())
                ->setF048fidProgramme((int)$feeProgram['f048fidProgramme'])
                ->setF048fstatus((int) $feeProgram['f048fstatus'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $feeProgramObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            
                $em->persist($feeProgramObj);

                $em->flush();

        return $feeProgramObj;
    }

     public function updateProgramCourse($courseFeeObj, $courseFee)
    {
        
        $em = $this->getEntityManager();

            $courseFeeObj->setF048fidProgram((int)$courseFee['f048fidProgram'])
                ->setF048fidSubject((int)$courseFee['f048fidSubject'])
                ->setF048famount((float)$courseFee['f048famount'])
                ->setF048fstatus((int) $courseFee['f048fstatus'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);

                $courseFeeObj->setF048fupdatedDtTm(new \DateTime());
            
                $em->persist($courseFeeObj);

                $em->flush();

        return $courseFeeObj;
        
    }
    
    public function createProgramCourse($fee, $courseFee)
    {
        $em = $this->getEntityManager();

       $courseFeeObj = new T048fprogramCourseFee();

            $courseFeeObj->setF048fidCourseFee((int)$fee->getF048fid())
                ->setF048fidProgram((int)$courseFee['f048fidProgram'])
                ->setF048fidSubject((int)$courseFee['f048fidSubject'])
                ->setF048famount((float)$courseFee['f048famount'])
                ->setF048fstatus((int) $courseFee['f048fstatus'])
                ->setF048fcreatedBy((int)$_SESSION['userId'])
                ->setF048fupdatedBy((int)$_SESSION['userId']);
                

            $courseFeeObj->setF048fcreatedDtTm(new \DateTime())
                ->setF048fupdatedDtTm(new \DateTime());
            
            $em->persist($courseFeeObj);

            $em->flush();
            return $courseFeeObj;
    }
    
    public function deleteFeeStructureDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t048ffee_structure_details WHERE f048fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteFeeStructureProgram($data)
    {
        // print_r($data);exit;

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t048ffee_structure_program WHERE f048fid_struct_program = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteProgramCourceFee($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t048fprogram_Course_fee WHERE f048fid_program_course = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
}
?>
