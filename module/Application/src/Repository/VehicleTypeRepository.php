<?php
namespace Application\Repository;

use Application\Entity\T042fvehicleType;   
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class VehicleTypeRepository extends EntityRepository 
{

    public function getList() 
    {
    	       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('v.f042fid, v.f042fname, v.f042fmonths, v.f042fstatus')
            ->from('Application\Entity\T042fvehicleType', 'v')
            ->orderBy('v.f042fid','DESC');  

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    public function getListById($id) 
    {
       $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('v.f042fid, v.f042fname, v.f042fmonths, v.f042fstatus')
            ->from('Application\Entity\T042fvehicleType', 'v')
            ->where('v.f042fid = :vehicleId')
            ->setParameter('vehicleId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $vehicle = new T042fvehicleType();
        $vehicle->setF042fname($data['f042fname'])
                ->setF042fmonths($data['f042fmonths'])
                ->setF042fstatus((int)$data['f042fstatus'])
                ->setF042fcreatedBy((int)$_SESSION['userId'])
                ->setF042fupdatedBy((int)$_SESSION['userId']);

        $vehicle->setF042fcreatedDtTm(new \DateTime())
                ->setF042fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($vehicle);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $vehicle;
    }

    public function updateData($vehicle, $data = []) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $vehicle->setF042fname($data['f042fname'])
                ->setF042fmonths($data['f042fmonths'])
                ->setF042fstatus((int)$data['f042fstatus'])
                ->setF042fcreatedBy((int)$_SESSION['userId'])
                ->setF042fupdatedBy((int)$_SESSION['userId']);

        $vehicle->setF042fupdatedDtTm(new \DateTime());

        $em->persist($vehicle);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
          $qb->select('v.f042fid, v.f042fname, v.f042fmonths, v.f042fstatus')
            ->from('Application\Entity\T042fvehicleType', 'v')
            ->orderBy('v.f042fname', 'ASC')
            ->where('v.f042fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}