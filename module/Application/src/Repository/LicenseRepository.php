<?php
namespace Application\Repository;

use Application\Entity\T091flicense;   
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class LicenseRepository extends EntityRepository 
{

    public function getList() 
    {
    	       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f091fid, l.f091flicense, l.f091fstatus')
            ->from('Application\Entity\T091flicense', 'l')
            ->orderBy('l.f091fid','DESC');  

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    public function getListById($id) 
    {
       $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f091fid, l.f091flicense, l.f091fstatus')
            ->from('Application\Entity\T091flicense', 'l')
            ->where('l.f091fid = :licenseId')
            ->setParameter('licenseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $license = new T091flicense();
        $license->setF091flicense($data['f091flicense'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $license->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());

        
            $em->persist($license);
            $em->flush();
        
        return $license;
    }

    public function updateData($license, $data = []) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $license->setF091flicense($data['f091flicense'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $license->setF091fupdatedDtTm(new \DateTime());

        $em->persist($license);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f091fid, l.f091flicense, l.f091fstatus')
            ->from('Application\Entity\T091flicense', 'l')
            ->orderBy('i.f091flicense')
            ->where('i.f091fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getActiveLicense($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f091fid, l.f091flicense, l.f091fstatus')
            ->from('Application\Entity\T091flicense', 'l')
            ->where('l.f091fstatus = :licenseId')
            ->setParameter('licenseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
}