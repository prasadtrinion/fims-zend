<?php
namespace Application\Repository;

use Application\Entity\T018flawyer;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class LawyerRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f018fid, l.f018flawyerName, l.f018fdateOfSurrender, l.f018fclaims, l.f018fattorneysFees, l.f018ftotal, l.f018fdateOfClaim, l.f018fdescription, l.f018fstatus, l.f018fcreatedBy, l.f018fupdatedBy')
            ->from('Application\Entity\T018flawyer','l');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f018fid, l.f018flawyerName, l.f018fdateOfSurrender, l.f018fclaims, l.f018fattorneysFees, l.f018ftotal, l.f018fdateOfClaim, l.f018fdescription, l.f018fstatus, l.f018fcreatedBy, l.f018fupdatedBy')
            ->from('Application\Entity\T018flawyer','l')
            ->where('l.f018fid = :lawyerId')
            ->setParameter('lawyerId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $lawyer = new T018flawyer();

        $lawyer->setF018fname($data['f018flawyerName'])
             ->setF018Ffstatus($data['f018fstatus'])
             ->setF018fcreatedBy((int)$_SESSION['userId'])
             ->setF018fupdatedBy((int)$_SESSION['userId']);

        $lawyer->setF018fcreateDtTm(new \DateTime())
                ->setF018fupdateDtTm(new \DateTime());

 
        $em->persist($lawyer);
        $em->flush();

        return $lawyer;

    }

    /* to edit the data in database*/

    public function updateData($lawyer, $data = []) 
    {
        $em = $this->getEntityManager();

        $lawyer->setF018fname($data['f018flawyerName'])
             ->set018Ffstatus($data['f018fstatus'])
             ->setF018fcreatedBy((int)$_SESSION['userId'])
             ->setF018fupdatedBy((int)$_SESSION['userId']);
                

        $lawyer->setF018fUpdateDtTm(new \DateTime());
        
        $em->persist($lawyer);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>