<?php
namespace Application\Repository;

use Application\Entity\T145fbatchWithdraw;
use Application\Entity\T146fbatchWithdrawDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BatchWithdrawRepository extends EntityRepository
{

    public function getList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

       $qb->select('b.f145fid, b.f145fbatchNo, b.f145finvoiceBank, b.f145fuumBank, b.f145fmaturityDate, b.f145fcertificateDate, b.f145fstatus')
            ->from('Application\Entity\T145fbatchWithdraw', 'b')
            ->orderby('b.f145fid','DESC');
        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $note)
        {
            $date                        = $note['f145fmaturityDate'];
            $note['f145fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $note['f145fcertificateDate'];
            $note['f145fcertificateDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $note);
        }
        
        $result = array("data"=>$result1);
        return $result;
    }  

    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('b.f145fid, b.f145fbatchNo, b.f145finvoiceBank, b.f145fuumBank, b.f145fmaturityDate, b.f145fcertificateDate, b.f145fstatus, bw.f146fid, bw.f146fidBatch, bw.f146famount, bw.f146fmaturityDate, bw.f146fstatus, bnk1.f042fbankName as uumBank, bnk.f042fbankName as investmentBank, ir.f064freferenceNumber as f146fapplication, ir.f064fsum, ir.f064fname as application')
            ->from('Application\Entity\T145fbatchWithdraw', 'b')
                ->leftjoin('Application\Entity\T146fbatchWithdrawDetails', 'bw', 'with', 'b.f145fid = bw.f146fidBatch')
                ->leftjoin('Application\Entity\T041fbankSetupAccount', 'bsa', 'with', 'bsa.f041fid = b.f145fuumBank')
                ->leftjoin('Application\Entity\T042fbank', 'bnk', 'with', 'bnk.f042fid = b.f145finvoiceBank')
                ->leftjoin('Application\Entity\T042fbank', 'bnk1', 'with', 'bnk1.f042fid = bsa.f041fidBank')
                ->leftjoin('Application\Entity\T064finvestmentRegistration', 'ir', 'with', 'ir.f064fid = bw.f146fapplication')
                ->where('b.f145fid = :id')
                ->setParameter('id', (int)$id);

        try
        {
            $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        $result1 = array();
        foreach ($data as $note) {
            $date                        = $note['f145fmaturityDate'];
            $note['f145fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $note['f145fcertificateDate'];
            $note['f145fcertificateDate'] = date("Y-m-d", strtotime($date));

            $date                        = $note['f146fmaturityDate'];
            $note['f146fmaturityDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $note);
        }
        $result = array("data"=>$result1);

        return $result;

    }

    public function createNewData($data)
    {
        $em        = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");

        $numberData = array();
        $numberData['type'] = 'WBATCH';
        $number = $configRepository->generateFIMS($numberData);

        $batchWithdraw = new T145fbatchWithdraw();


            $batchWithdraw->setF145fbatchNo($number)
                ->setF145finvoiceBank((int)$data['f145finvoiceBank'])
                ->setF145fmaturityDate(new \DateTime($data['f145fmaturityDate']))
                ->setF145fcertificateDate(new \DateTime($data['f145fcertificateDate']))
                ->setF145fuumBank((int)$data['f145fuumBank'])
                ->setF145fstatus((int)$data['f145fstatus'])
                ->setF145fcreatedBy((int)$_SESSION['userId'])
                ->setF145fcreatedDtTm(new \DateTime());

           
                $em->persist($batchWithdraw);
                $em->flush();
                // print_r($batchWithdraw);exit;

                $batchWithdraws = $data['details'];

        foreach ($batchWithdraws as $details)
        {
                   

                $batchWithdrawDetails = new T146fbatchWithdrawDetails();
            
                $batchWithdrawDetails->setF146fidBatch((int)$batchWithdraw->getF145fid())
                ->setF146fapplication((int)$details['f064fid'])
                ->setF146fmaturityDate(new \DateTime($details['f146fmaturityDate']))
                ->setF146famount((float)$details['f146famount'])
                ->setF146fstatus((int)$details['f146fstatus'])
                ->setF146fcreatedDtTm(new \DateTime())     
                ->setF146fcreatedBy((int)$_SESSION['userId']);
            
            try
            {
                $em->persist($batchWithdrawDetails);

                $em->flush();
            // print_r($batchWithdrawDetails);exit;
                
            }
            catch (\Exception $e)
            {
                echo $e;
            }
        }
        return $batchWithdraw;
    }

    public function UpdateMaster($batchWithdraw,$data)
    {
        $em      = $this->getEntityManager();
        
       $batchWithdraw->setF145finvoiceBank((int)$data['f145finvoiceBank'])
                ->setF145fmaturityDate(new \DateTime($data['f145fmaturityDate']))
                ->setF145fcertificateDate(new \DateTime($data['f145fcertificateDate']))
                ->setF145fuumBank((int)$data['f145fuumBank'])
                ->setF145fstatus((int)$data['f145fstatus'])
                ->setF145fupdatedDtTm(new \DateTime())
                ->setF145fstatus((int)$data['f145fstatus'])
                ->setF145fupdatedBy((int)$_SESSION['userId']);
            try {

                $em->persist($batchWithdraw);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

        return $batchWithdraw;
    }
    public function createDetail($batchWithdraw,$details)
    {
        $em      = $this->getEntityManager();
        
       
        $batchWithdrawDetails = new T146fbatchWithdrawDetails();
            
        $batchWithdrawDetails->setF146fidBatch((int)$batchWithdraw->getF145fid())
                ->setF146fapplication((int)$details['f064fid'])
                ->setF146fmaturityDate(new \DateTime($details['f146fmaturityDate']))
                ->setF146famount((float)$details['f146famount'])
                ->setF146fstatus((int)$details['f146fstatus'])
                ->setF146fcreatedDtTm(new \DateTime())     
                ->setF146fcreatedBy((int)$_SESSION['userId']);
            
            try
            {
                $em->persist($batchWithdrawDetails);

                $em->flush();
            // print_r($batchWithdrawDetails);exit;
                
            }
            catch (\Exception $e)
            {
                echo $e;
            }

        return $batchWithdrawDetails;
    }

    public function UpdateDetail($batchWithdrawDetails,$details)
    {
        $em      = $this->getEntityManager();
        
       $batchWithdrawDetails ->setF146fapplication((int)$details['f064fid'])
                ->setF146fmaturityDate(new \DateTime($details['f146fmaturityDate']))
                ->setF146famount((float)$details['f146famount'])
                ->setF146fupdatedDtTm(new \DateTime())
                ->setF146fupdatedBy((int)$_SESSION['userId']);
            try {
                $em->persist($batchWithdrawDetails);
                $em->flush();
            } catch (\Exception $e) {
                echo $e;
            }

        return $batchWithdrawDetails;
    }

    public function getBatchWithdrawActiveList($id)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('b.f145fid, b.f145fbatchNo, b.f145finvoiceBank, b.f145fuumBank, b.f145fmaturityDate, b.f145fcertificateDate, b.f145fstatus')
            ->from('Application\Entity\T145fbatchWithdraw', 'b')
            ->where('b.f145fstatus = :id')
            ->setParameter('id', (int)$id)
            ->orderby('b.f145fid','DESC');

        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        

        $result1 = array();
        foreach ($result as $note)
        {
            $date                        = $note['f145fmaturityDate'];
            $note['f145fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $note['f145fcertificateDate'];
            $note['f145fcertificateDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $note);
        }
        

        $result = array("data"=>$result1);

        return $result;
    }


    public function approveBatchWithdraw($data)
    {
        $status = (int)$data['status'];
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
             $query = " UPDATE t145fbatch_withdraw SET f145fstatus = $status where f145fid = $id";

        $res = $em->getConnection()->executeQuery($query);
        
        }
        return $res;
    }

    public function deleteBatchWithdrawDetails($data)
    {
        foreach ($data['id'] as $id)
        {
            $id = (int)$id;
            $em = $this->getEntityManager();

            $query = " DELETE FROM t146fbatch_withdraw_details  where f146fid = $id";

            $res = $em->getConnection()->executeQuery($query);
        }

       return $res;
    }

    public function getInvestmentRegNotInBatchWithdraw($data)
    {
        $em = $this->getEntityManager();


        $idBank = (int)$data['idBank'];
        $idUUMBank = (int)$data['idUUMBank'];
        $query = "SELECT distinct(ir.f064fid) as f064fid, ir.f064freference_number as f146fapplication, ir.f064fname as application, ir.*, bnk1.f042fbank_name as uumBank, bnk.f042fbank_name as investmentBank from t064finvestment_registration ir left join t041fbank_setup_account bsa on bsa.f041fid = ir.f064fuum_bank left join t042fbank bnk on bnk.f042fid = ir.f064finvestment_bank left join t042fbank bnk1 on bnk1.f042fid = bsa.f041fid_bank where f064fid not in (SELECT wd.f146fapplication from t146fbatch_withdraw_details wd) and ir.f064fuum_bank = $idUUMBank and ir.f064finvestment_bank = $idBank";

            $res = $em->getConnection()->executeQuery($query)->fetchAll();

            $res = array("data"=>$res);

            return $res;
    }
}
