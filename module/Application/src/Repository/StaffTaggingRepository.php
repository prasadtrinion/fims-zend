<?php 
namespace Application\Repository;

use Application\Entity\T043fstaffTagging;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StaffTaggingRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('v.f043fid,v.f043fdepartment,v.f043ffund,v.f043faddress1,v.f043faddress2,v.f043faddress3,v.f043fcountry,v.f043fstate,v.f043fzipCode,v.f043fbank,v.f043fidEpf,v.f043fidSocso,v.f043ftaxable,v.f043fidStaff,v.f043fcostCenter,v.f043factivity,v.f043faccountNumber,v.f043fstatus,s.f034fname as staff,c.f091fcategoryName as socsoName,e.f030fcategoryName as epfName,d.f015fdepartmentName')
            ->from('Application\Entity\T043fstaffTagging', 'v')
             ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 's.f034fstaffId = v.f043fidStaff')
            ->leftjoin('Application\Entity\T091socsoCategory', 'c', 'with','v.f043fidSocso = c.f091fid')
            ->leftjoin('Application\Entity\T030epfCategory', 'e', 'with','v.f043fidEpf = e.f030fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','v.f043fdepartment = d.f015fdepartmentCode')
             ->orderby('v.f043fid','DESC');
           // ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','v.f043fvehicleCondition = d.f011fid')
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );        
        return $result;
    
    } 

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('v.f043fid,v.f043fdepartment,v.f043ffund,v.f043faddress1,v.f043faddress2,v.f043faddress3,v.f043fcountry,v.f043fstate,v.f043fzipCode,v.f043fbank,v.f043fidEpf,v.f043fidSocso,v.f043ftaxable,v.f043fidStaff,v.f043fcostCenter,v.f043factivity,v.f043faccountNumber,v.f043fstatus')
            ->from('Application\Entity\T043fstaffTagging', 'v')
            ->where('v.f043fid = :taggingId')
            ->setParameter('taggingId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
        $tagging = new T043fstaffTagging();
        $tagging->setF043fdepartment($data['f043fdepartment'])
             ->setF043ffund($data['f043ffund'])
             ->setF043faddress1($data['f043faddress1'])
             ->setF043faddress2($data['f043faddress2'])
             ->setF043faddress3($data['f043faddress3'])  
             ->setF043fcountry((int)$data['f043fcountry'])  
             ->setF043fstate((int)$data['f043fstate'])  
             ->setF043fzipCode($data['f043fzipCode'])  
             ->setF043fbank((int)$data['f043fbank'])  
             ->setF043fidEpf((int)$data['f043fidEpf'])  
             ->setF043fidSocso((int)$data['f043fidSocso'])  
             ->setF043ftaxable((int)$data['f043ftaxable'])  
             ->setF043fidStaff((int)$data['f043fidStaff'])  
             ->setF043fcostCenter($data['f043fcostCenter'])  
             ->setF043factivity($data['f043factivity'])  
             ->setF043faccountNumber($data['f043faccountNumber'])  
             ->setF043fcreatedBy((int)$_SESSION['userId'])  
             ->setF043fupdatedBy((int)$_SESSION['userId'])  
             ->setF043fcreatedDtTm(new \DateTime())  
             ->setF043fupdatedDtTm(new \DateTime()); 


try{
  
        $em->persist($tagging);
        $em->flush();
    }
catch(\Exception $e)
{echo $e;}    
        return $tagging;
    }


    public function updateData($tagging,$data = []) 
    {
        $em = $this->getEntityManager();


             $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
        $tagging->setF043fdepartment($data['f043fdepartment'])
             ->setF043ffund($data['f043ffund'])
             ->setF043faddress1($data['f043faddress1'])
             ->setF043faddress2($data['f043faddress2'])
             ->setF043faddress3($data['f043faddress3'])  
             ->setF043fcountry((int)$data['f043fcountry'])  
             ->setF043fstate((int)$data['f043fstate'])  
             ->setF043fzipCode($data['f043fzipCode'])  
             ->setF043fbank((int)$data['f043fbank'])  
             ->setF043fidEpf((int)$data['f043fidEpf'])  
             ->setF043fidStaff((int)$data['f043fidStaff'])  
             ->setF043fcostCenter($data['f043fcostCenter'])  
             ->setF043factivity($data['f043factivity'])  
             ->setF043faccountNumber($data['f043faccountNumber'])
             ->setF043fidSocso((int)$data['f043fidSocso'])  
             ->setF043ftaxable((int)$data['f043ftaxable'])  
             ->setF043fupdatedBy((int)$_SESSION['userId'])  
             ->setF043fupdatedDtTm(new \DateTime()); 


try{
  
        $em->persist($tagging);
        $em->flush();
    }
catch(\Exception $e)
{echo $e;}    
        return $tagging;
    }

}
?>