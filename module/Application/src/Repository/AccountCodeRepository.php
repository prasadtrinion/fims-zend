<?php
namespace Application\Repository;
 
use Application\Entity\T059faccountCode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AccountCodeRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("a.f059fid, a.f059fcode,a.f059fname as originalname,a.f059fcompleteCode+ ' - '+a.f059fname as f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType, a.f059faccountActivity, a.f059foldCode")
            ->from('Application\Entity\T059faccountCode','a')
            // ->where('a.f059flevelStatus=2')
            // ->where('a.f059fstatus=1')
            ->orderBy('a.f059fname', 'ASC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(),
        );

        return $result;
        
    }

     public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode, a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType, a.f059faccountActivity, a.f059foldCode')

           ->from('Application\Entity\T059faccountCode','a')
            ->where('a.f059fid = :accountId')
            ->setParameter('accountId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    
    /* to retrive the data from database using id*/

    public function getListByRef($refCode) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType, a.f059faccountActivity, a.f059foldCode')

           ->from('Application\Entity\T059faccountCode','a')
           ->where('a.f059frefCode = :refCode')
           ->setParameter('refCode',$refCode);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }
   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $account = new T059faccountCode();

        $account->setF059fcode($data['f059fcode'])
             ->setF059fname($data['f059fname'])  
             ->setF059ftype($data['f059ftype'])
             ->setF059fdescription($data['f059fdescription'])
             ->setF059fshortCode($data['f059fshortCode'])
             ->setF059fparentCode((int)$data['f059fparentCode'])
             ->setF059frefCode((int)$data['f059frefCode'])
             ->setF059fstatus((int)$data['f059fstatus'])
             ->setF059flevelStatus((int)$data['f059flevelStatus'])
             ->setF059fcompleteCode($data['f059fcompleteCode'])
             ->setF059fallowNegative((int)$data['f059fallowNegative'])
             ->setF059fcategory($data['f059fcategory'])
             ->setF059fbalanceType($data['f059fbalanceType'])
             ->setF059faccountActivity($data['f059faccountActivity'])
             ->setF059foldCode($data['f059foldCode'])
             ->setF059fcreatedBy((int)$_SESSION['userId'])
             ->setF059fupdatedBy((int)$_SESSION['userId']);

        $account->setF059fcreatedDtTm(new \DateTime())
                ->setF059fupdatedDtTm(new \DateTime());

 
        $em->persist($account);
        $em->flush();

        if($data['f059flevelStatus'] == 0){
            $type= $data['f059ftype'];
            $code= $data['f059fcode'];
             $query1 = "UPDATE t059faccount_code set f059ftype='$type' where f059fcomplete_code like '$code%'"; 
        $result=$em->getConnection()->executeQuery($query1);
        }
        return $account;

    }

    /* to edit the data in database*/

    public function updateData($account, $data = []) 
    {
        $em = $this->getEntityManager();

        $account->setF059fcode($data['f059fcode'])
             ->setF059fname($data['f059fname'])  
             ->setF059ftype($data['f059ftype'])
             ->setF059fdescription($data['f059fdescription'])
             ->setF059fshortCode($data['f059fshortCode'])
             ->setF059fparentCode((int)$data['f059fparentCode'])
             ->setF059frefCode((int)$data['f059frefCode'])
             ->setF059fstatus((int)$data['f059fstatus'])
             ->setF059flevelStatus((int)$data['f059flevelStatus'])
             ->setF059fcompleteCode($data['f059fcompleteCode'])
             ->setF059fallowNegative((int)$data['f059fallowNegative'])
             ->setF059fcategory($data['f059fcategory'])
             ->setF059fbalanceType($data['f059fbalanceType'])
             ->setF059faccountActivity($data['f059faccountActivity'])
             ->setF059foldCode($data['f059foldCode'])
             ->setF059fbalanceType($data['f059fbalanceType'])
             ->setF059fcreatedBy((int)$_SESSION['userId'])
             ->setF059fupdatedBy((int)$_SESSION['userId']);

        $account->setF059fupdatedDtTm(new \DateTime());
        
        $em->persist($account);
        $em->flush();
        if($data['f059flevelStatus'] == 0){
            $type= $data['f059ftype'];
            $code= $data['f059fcode'];
             $query1 = "UPDATE t059faccount_code set f059ftype='$type' where f059fcomplete_code like '$code%'"; 
        $result=$em->getConnection()->executeQuery($query1);
        }
    }
    public function getListByLevel($level) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType, a.f059faccountActivity, a.f059foldCode')
           ->from('Application\Entity\T059faccountCode','a')
           ->where('a.f059flevelStatus = :level')
           ->setParameter('level',$level);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getListByParentAndLevel($parent,$level)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode, a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType, a.f059faccountActivity, a.f059foldCode')

           ->from('Application\Entity\T059faccountCode','a')
           ->where('a.f059flevelStatus = :level')
           ->setParameter('level',$level)
           ->andWhere('a.f059fparentCode = :parent')
           ->setParameter('parent',$parent)
           ->orderBy('a.f059fcode');
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }



    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function updateAccount($account) 
    {
        $em = $this->getEntityManager();

        $account->setF059fstatus((int)"0")
                 ->setF059fupdatedDtTm(new \DateTime());

        $em->persist($account);
        $em->flush();
    }

    public function accountCodeStatus($id) 
    {
        $em = $this->getEntityManager();

        
        $query1 = "UPDATE t059faccount_code set f059fstatus='1' where f059fid='$id'"; 
        $result=$em->getConnection()->executeQuery($query1);
        

    }
}

?>
