<?php
namespace Application\Repository;

use Application\Entity\T068fprogramme;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class ProgrammeRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f068fid, p.f068fname, p.f068fcode,p.f068ftype, p.f068ftotalCreditHours, p.f068fidScheme, d.f011fdefinitionCode as scheme, p.f068fstatus')
            ->from('Application\Entity\T068fprogramme', 'p')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f068fidScheme = d.f011fid')
            ->orderBy('p.f068fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }
    public function getProgrammes($type) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f068fid, p.f068fname, p.f068fcode,(p.f068fcode+'-'+p.f068fname) as originalName,p.f068ftype, p.f068ftotalCreditHours, p.f068fidScheme, d.f011fdefinitionCode as scheme, p.f068fstatus, p.f068faward")
            ->from('Application\Entity\T068fprogramme', 'p')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f068fidScheme = d.f011fid')
            ->where("p.f068ftype = '".$type."'");
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('p.f068fid, p.f068fname, p.f068fcode,p.f068ftype, p.f068ftotalCreditHours, p.f068fidScheme, d.f011fdefinitionCode as scheme, p.f068fstatus,  p.f068faward, a.f011fdefinitionCode as award')
            ->from('Application\Entity\T068fprogramme', 'p')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f068fidScheme = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with','p.f068faward = a.f011fid')
            ->where('p.f068fid = :programmeId')
            ->setParameter('programmeId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }



    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $program = new T068fprogramme();

        $program->setF068fname($data['f068fname'])
                ->setF068fcode($data['f068fcode'])
                ->setF068ftype($data['f068ftype'])
                ->setF068ftotalCreditHours($data['f068ftotalCreditHours'])
                ->setF068fidScheme((int)$data['f068fidScheme'])
                ->setF068faward((int)$data['f068faward'])
                ->setF068fstatus((int)$data['f068fstatus'])
                ->setF068fcreatedBy((int)$_SESSION['userId'])
                ->setF068fupdatedBy((int)$_SESSION['userId']);

        $program ->setF068fcreatedDtTm(new \DateTime())
                ->setF068fupdatedDtTm(new \DateTime());

        $em->persist($program);
        $em->flush();
       
        return $program;
    }

    public function updateData($program, $data = []) 
    {
        $em = $this->getEntityManager();

        $program->setF068fname($data['f068fname'])
                ->setF068ftype($data['f068ftype'])
                ->setF068fcode($data['f068fcode'])
                ->setF068ftotalCreditHours((int)$data['f068ftotalCreditHours'])
                ->setF068fidScheme((int)$data['f068fidScheme'])
                ->setF068faward((int)$data['f068faward'])
                ->setF068fstatus((int)$data['f068fstatus'])
                ->setF068fcreatedBy((int)$_SESSION['userId'])
                ->setF068fupdatedBy((int)$_SESSION['userId']);

         $program->setF068fupdatedDtTm(new \DateTime());


        
        $em->persist($program);
        $em->flush();

    }

    public function getProgrammeByStatus($id) 
    {
        $id = (int)$id;
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f068fid, p.f068fname, p.f068fcode,p.f068ftype, p.f068ftotalCreditHours, p.f068fidScheme, d.f011fdefinitionCode as scheme, p.f068fstatus')
            ->from('Application\Entity\T068fprogramme', 'p')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f068fidScheme = d.f011fid')
            ->where('p.f068fstatus = :status')
            ->setParameter('status',$id)
            ->orderBy('p.f068fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

}
