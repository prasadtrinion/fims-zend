<?php
namespace Application\Repository;

use Application\Entity\T111fblockVendor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class BlockVendorRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("bv.f111fid, bv.f111fvendor, v.f030fcompanyName, v.f030fvendorCode, bv.f111fblocklistFrom, bv.f111freason, bv.f111freleaseDate, bv.f111fstatus , (v.f030fcompanyName+ '-' +v.f030fvendorCode) as vendor")
            ->from('Application\Entity\T111fblockVendor', 'bv')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'bv.f111fvendor = v.f030fid')
            ->orderBy('bv.f111fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $blackList) {
            $effective_date                        = $blackList['f111freleaseDate'];
            $blackList['f111freleaseDate'] = date("Y-m-d", strtotime($effective_date));
            $end_date                        = $blackList['f111fblocklistFrom'];
            $blackList['f111fblocklistFrom'] = date("Y-m-d", strtotime($end_date));
            array_push($result1, $blackList);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    }

    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("bv.f111fid, bv.f111fvendor, v.f030fcompanyName, v.f030fvendorCode, bv.f111fblocklistFrom, bv.f111freason, bv.f111freleaseDate, bv.f111fstatus, (v.f030fvendorCode+ '-' +v.f030fcompanyName) as vendor")
            ->from('Application\Entity\T111fblockVendor', 'bv')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'bv.f111fvendor = v.f030fid')
            ->where('bv.f111fid = :blockId')
            ->setParameter('blockId',$id);
             
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $blackList) {
            $effective_date                        = $blackList['f111freleaseDate'];
            $blackList['f111freleaseDate'] = date("Y-m-d", strtotime($effective_date));
            $end_date                        = $blackList['f111fblocklistFrom'];
            $blackList['f111fblocklistFrom'] = date("Y-m-d", strtotime($end_date));
            array_push($result1, $blackList);
        }
        return $result1;
    }

    public function createNewData($data) 
    {
       
     	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $block_list = new T111fblockVendor();

        $block_list->setF111fvendor((int)$data['f111fvendor'])
                ->setF111fblocklistFrom(new \DateTime($data['f111fblocklistFrom']))
                ->setF111freason($data['f111freason'])
                ->setF111freleaseDate(new \DateTime($data['f111freleaseDate']))
                ->setF111fstatus((int)$data['f111fstatus'])
                ->setF111fcreatedBy((int)$_SESSION['userId']);

        $block_list->setF111fcreatedDtTm(new \DateTime());
     
            $em->persist($block_list);
            $em->flush();

            $vendor = (int)$data['f111fvendor'];

            $query  = "update t030fvendor_registration set f030fapproval_status = 2 where f030fid = $vendor";
            $em->getConnection()->executeQuery($query);

        	return $block_list;
    }

    public function updateData($block_list, $data = []) 
    {
        $em = $this->getEntityManager();

        $block_list->setF111fvendor((int)$data['f111fvendor'])
                ->setF111fblocklistFrom(new \DateTime($data['f111fblocklistFrom']))
                ->setF111freason($data['f111freason'])
                ->setF111freleaseDate(new \DateTime($data['f111freleaseDate']))
                ->setF111fstatus((int)$data['f111fstatus'])
                ->setF111fupdatedBy((int)$_SESSION['userId']);

        $block_list->setF111fupdatedDtTm(new \DateTime());

        $em->persist($block_list);
        $em->flush();
    }
}
