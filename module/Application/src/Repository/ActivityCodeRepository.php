<?php
namespace Application\Repository;

use Application\Entity\T058factivityCode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ActivityCodeRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("a.f058fid, a.f058fcode,a.f058fname as originalname,a.f058fcompleteCode+ ' - '+ a.f058fname as f058fname, a.f058ftype, a.f058fdescription, a.f058fshortCode,a.f058fparentCode, a.f058fstatus, a.f058frefCode, a.f058flevelStatus, a.f058fcompleteCode")
            ->where('a.f058fstatus = 1')
           ->from('Application\Entity\T058factivityCode','a')
           ->orderBy('a.f058fcompleteCode','ASC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f058fid,a.f058fcode, a.f058fname, a.f058ftype, a.f058fdescription, a.f058fshortCode,a.f058fparentCode, a.f058fstatus, a.f058frefCode, a.f058flevelStatus, a.f058fcompleteCode')

           ->from('Application\Entity\T058factivityCode','a')
            ->where('a.f058fid = :activityId')
            ->setParameter('activityId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }



    
    /* to retrive the data from database using id*/

    public function getListByRef($refCode) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f058fid, a.f058fcode, a.f058fname, a.f058ftype, a.f058fdescription, a.f058fshortCode,a.f058fparentCode, a.f058fstatus, a.f058frefCode, a.f058flevelStatus, a.f058fcompleteCode')

           ->from('Application\Entity\T058factivityCode','a')
           ->where('a.f058frefCode = :refCode')
           ->setParameter('refCode',$refCode);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $activity = new T058factivityCode();

        $activity->setF058fcode($data['f058fcode'])
             ->setF058fname($data['f058fname'])  
             ->setF058ftype("0")
             ->setF058fdescription($data['f058fname'])
             ->setF058fshortCode("SH")
             ->setF058fstatus((int)$data['f058fstatus'])
             ->setF058fparentCode((int)$data['f058fparentCode'])
             ->setF058frefCode((int)$data['f058frefCode'])
             ->setF058flevelStatus((int)$data['f058flevelStatus'])
             ->setF058fcompleteCode($data['f058fcompleteCode'])
             ->setF058fcreatedBy((int)$_SESSION['userId'])
             ->setF058fupdatedBy((int)$_SESSION['userId']);

        $activity->setF058fcreatedDtTm(new \DateTime())
                ->setF058fupdatedDtTm(new \DateTime());

        try{
        $em->persist($activity);
        $em->flush();
        // print_r($activity);
        // die();
    }
    catch(\Exception $e){
        echo $e;        
    }
        return $activity;

    }

    /* to edit the data in database*/

    public function updateData($activity, $data = []) 
    {
        $em = $this->getEntityManager();

        $activity->setF058fcode($data['f058fcode'])
             ->setF058fname($data['f058fname'])  
             ->setF058ftype($data['f058ftype'])
             ->setF058fdescription($data['f058fdescription'])
             ->setF058fshortCode($data['f058fshortCode'])
             ->setF058fstatus((int)$data['f058fstatus'])
             ->setF058fparentCode((int)$data['f058fparentCode'])
             ->setF058frefCode((int)$data['f058frefCode'])
             ->setF058flevelStatus((int)$data['f058flevelStatus'])
             ->setF058fcompleteCode($data['f058fcompleteCode'])
             ->setF058fcreatedBy((int)$_SESSION['userId'])
             ->setF058fupdatedBy((int)$_SESSION['userId']);

        $activity->setF058fupdatedDtTm(new \DateTime());
        
        $em->persist($activity);
        $em->flush();

    }

    public function getListByLevel($level) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f058fid,a.f058fcode, a.f058fname, a.f058ftype, a.f058fdescription, a.f058fshortCode,a.f058fparentCode, a.f058fstatus, a.f058frefCode, a.f058flevelStatus, a.f058fcompleteCode')

           ->from('Application\Entity\T058factivityCode','a')
           ->where('a.f058flevelStatus = :level')
           ->setParameter('level',$level)
           ->andWhere('a.f058fstatus=1');
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getListByParentAndLevel($parent,$level)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f058fid,a.f058fcode, a.f058fname, a.f058ftype, a.f058fdescription, a.f058fshortCode,a.f058fparentCode, a.f058fstatus, a.f058frefCode, a.f058flevelStatus, a.f058fcompleteCode')

           ->from('Application\Entity\T058factivityCode','a')
           ->where('a.f058flevelStatus = :level')
           ->setParameter('level',$level)
           ->andWhere('a.f058fparentCode = :parent')
           ->setParameter('parent',$parent);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    
    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function updateActivity($activity) 
    {
        $em = $this->getEntityManager();

        $activity->setF058fstatus((int)"0")
                 ->setF058fupdatedDtTm(new \DateTime());

        $em->persist($activity);
        $em->flush();
    }

    public function activityCodeStatus($id) 
    {
        $em = $this->getEntityManager();

        
        $query1 = "UPDATE t058factivity_code set f058fstatus='1' where f058fid='$id'"; 
        $result=$em->getConnection()->executeQuery($query1);
        

    }
}

?>
