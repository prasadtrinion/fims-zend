<?php
namespace Application\Repository;

use Application\Entity\T073frequestInvoice;
use Application\Entity\T074frequestInvoiceDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class RequestInvoiceRepository extends EntityRepository
{

    public function getRequestInvoiceList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071frevenue_type as f071frevenueType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType from t073frequest_invoice as i inner join t132frevenue_type as rt on i.f071frevenue_type = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') order by i.f071fid DESC";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
       
        $result1 = array();
        foreach ($result as $invoice) {
            $STA                        = $invoice['f034fname'];
            $STD                        = $invoice['f060fname'];
            $OR                        = $invoice['f021ffirst_name'].$invoice['f021flast_name'];

            if($STA!=null){
                $name = $STA;
            }
            if($STD!=null){
                $name = $STD;
            }
            if($OR!=null){
                $name = $OR;
            }
            $invoice['customerName'] = $name;
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getRequestInvoiceListByType($type,$user)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071frevenue_type as f071frevenueType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType from t073frequest_invoice as i inner join t132frevenue_type as rt on i.f071frevenue_type = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071finvoice_type = '$type' and i.f071fid_customer = $user";
           
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
       
        $result1 = array();
        foreach ($result as $invoice) {
            $STA                        = $invoice['f034fname'];
            $STD                        = $invoice['f060fname'];
            $OR                        = $invoice['f021ffirst_name'].$invoice['f021flast_name'];

            if($STA!=null){
                $name = $STA;
            }
            if($STD!=null){
                $name = $STD;
            }
            if($OR!=null){
                $name = $OR;
            }
            $invoice['customerName'] = $name;
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }

        $result = array(
            'data' => $result1,
        );

        return $result;

    }

    public function getCustomerRequestInvoices($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071frevenueType,i.f071fdescription,i.f071fidFinancialYear,i.f071fidCustomer,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fidCustomer,i.f071fstatus,i.f071finvoiceTotal,i.f071fisRcp,i.f071finvoiceFrom,c.f021ffirstName as firstName,c.f021flastName as lastName, i.f071freason, (rt.f132ftypeCode+'-'+rt.f132frevenueName) revenueType , (rts.f085fcode+'-'+rts.f085fname) revenueItem")
            ->from('Application\Entity\T073frequestInvoice', 'i')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
                ->leftjoin('Application\Entity\T074frequestInvoiceDetails', 'id', 'with', 'i.f071fid = id.f072fidInvoice')
            ->leftjoin('Application\Entity\T132frevenueType', 'rt', 'with', 'i.f071frevenueType = rt.f132fid')
            ->leftjoin('Application\Entity\T085frevenueSetUp', 'rts', 'with', 'id.f072fidItem = rts.f085fid')
            ->where('i.f071fidCustomer = :customerId')
            ->orderby('i.f071fid','DESC')

            ->setParameter('customerId', $id);
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $invoice) {
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        $result = array(
            'data' => $result1,
        );
        return $result;

    }

    public function getRequestInvoiceById($id, $approvedStatus)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $result1 = array();

        if ($approvedStatus == 0 && isset($approvedStatus)) {
            $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071frevenue_type as f071frevenueType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType from t073frequest_invoice as i inner join t132frevenue_type as rt on i.f071frevenue_type = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 0 and i.f071fis_rcp = $id  order by i.f071fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        } elseif ($approvedStatus == 1) {

           $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071frevenue_type as f071frevenueType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType from t073frequest_invoice as i inner join t132frevenue_type as rt on i.f071frevenue_type = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 1 and i.f071fis_rcp = $id  order by i.f071fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();


        } elseif ($approvedStatus == 2) {
            $query = "select i.f071fid, s.f034fname,st.f060fname,c.f021ffirst_name,c.f021flast_name,i.f071finvoice_number as f071finvoiceNumber,i.f071finvoice_type as f071finvoiceType,i.f071frevenue_type as f071frevenueType,i.f071fdescription,i.f071fid_financial_year as f071fidFinancialYear,i.f071fid_customer as f071fidCustomer,i.f071finvoice_date as f071finvoiceDate,i.f071fapproved_by as f071fapprovedBy,i.f071fstatus,i.f071finvoice_total as f071finvoiceTotal,i.f071fis_rcp as f071fisRcp,i.f071finvoice_from as f071finvoiceFrom, (rt.f132ftype_code+'-'+rt.f132frevenue_name) as revenueType from t073frequest_invoice as i inner join t132frevenue_type as rt on i.f071frevenue_type = rt.f132fid  left outer join vw_staff_master as s on (s.f034fstaff_id = i.f071fid_customer and i.f071finvoice_type='STA') left outer join vw_all_students as st on (st.f060fid = i.f071fid_customer and i.f071finvoice_type='STD') left outer join t021fcustomers as c on (c.f021fid = i.f071fid_customer and i.f071finvoice_type='OR') where i.f071fstatus = 2 and i.f071fis_rcp = $id order by i.f071fid DESC";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        } else {
            $qb->select(" i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,i.f071frevenueType,i.f071fdescription,i.f071fidFinancialYear,i.f071fidCustomer,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fisRcp,i.f071finvoiceFrom,id.f072fidTaxCode,id.f072ftotalExc,id.f072ftaxAmount,id.f072fid,id.f072fidItem,id.f072fgstCode,id.f072fgstValue,id.f072fdebitFundCode,id.f072fdebitDepartmentCode,id.f072fdebitActivityCode,id.f072fdebitAccountCode,id.f072fcreditFundCode,id.f072fcreditDepartmentCode,id.f072fcreditActivityCode,id.f072fcreditAccountCode,id.f072fquantity,id.f072fprice,id.f072ftotal,id.f072fstatus, i.f071freason")
                ->from('Application\Entity\T073frequestInvoice', 'i')
                ->leftjoin('Application\Entity\T074frequestInvoiceDetails', 'id', 'with', 'i.f071fid = id.f072fidInvoice');
            $qb->where('i.f071fid = :invoiceId')
            ->orderby('i.f071fid','DESC')

                ->setParameter('invoiceId', $id);
                 $query   = $qb->getQuery();
        $result1  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }

        foreach ($result as $invoice) {
            $STA                        = $invoice['f034fname'];
            $STD                        = $invoice['f060fname'];
            $OR                        = $invoice['f021ffirst_name'].$invoice['f021flast_name'];

            if($STA!=null){
                $name = $STA;
            }
            if($STD!=null){
                $name = $STD;
            }
            if($OR!=null){
                $name = $OR;
            }
            $invoice['customerName'] = $name;
            $date                        = $invoice['f071finvoiceDate'];
            $invoice['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $invoice);
        }
        return $result1;
    }

    public function createRequestInvoiceDetail($invoiceObj, $invoiceDetail)
    {
        $em = $this->getEntityManager();

        $invoiceDetailObj = new T074frequestInvoiceDetails();
        $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
            ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
            ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
            ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
            ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
            ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
            ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
            ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
            ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
            ->setF072fgstCode($invoiceDetail['f072fgstCode'])
            ->setF072fgstValue((int)$invoiceDetail['f072fgstValue'])
            ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
            ->setF072fprice((float)$invoiceDetail['f072fprice'])
            ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
            ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
            ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
            ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
            ->setF072fstatus((int) $data['f071fstatus'])
            ->setF072fcreatedBy((int)$_SESSION['userId'])
            ->setF072fupdatedBy((int)$_SESSION['userId'])
            ->setF072fidInvoice($invoiceObj);

        $invoiceDetailObj->setF072fcreatedDtTm(new \DateTime())
            ->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($invoiceDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    public function updateRequestInvoice($invoice, $data = [])
    {
        $em = $this->getEntityManager();

        $invoice->setF071finvoiceNumber($data['f071finvoiceNumber'])
            ->setF071finvoiceType($data['f071finvoiceType'])
            ->setF071frevenueType((int)$data['f071frevenueType'])
            ->setF071finvoiceDate(new \DateTime($data['f071finvoiceDate']))
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            // ->setF071fidFinancialYear(1)
            ->setF071fdescription($data['f071fdescription'])
            ->setF071fidFinancialYear((int)$data['f071fidFinancialYear'])
            ->setF071fapprovedBy((int) "1")
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071fisRcp((int) $data['f071fisRcp'])
            ->setF071finvoiceFrom((int) $data['f071finvoiceFrom'])
            ->setF071customerName((int)$data['f071fcustomerName'])
            ->setF071finvoiceTotal( $data['f071finvoiceTotal'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $invoice->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($invoice);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $invoice;
    }
    public function updateRequestInvoiceDetail($invoiceDetailObj, $invoiceDetail)
    {
        $em = $this->getEntityManager();

        $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
        ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
        ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
        ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
        ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
        ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
        ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
        ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
        ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
        ->setF072fgstCode($invoiceDetail['f072fgstCode'])
        ->setF072fgstValue((int)$invoiceDetail['f072fgstValue'])
        ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
        ->setF072fprice((float)$invoiceDetail['f072fprice'])
        ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
        ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
        ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
        ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
        ->setF072fstatus((int) $data['f071fstatus'])
            ->setF072fupdatedBy((int)$_SESSION['userId']);

        $invoiceDetailObj->setF072fupdatedDtTm(new \DateTime());
        try {
            $em->persist($invoiceDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public function createNewRequestInvoice($data)
    {
 
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']=$data['f071finvoiceType'];
        $number = $configRepository->generateFIMS($numberData);

        $invoice = new T073frequestInvoice();

        $invoice->setF071finvoiceNumber($number)
            ->setF071finvoiceType($data['f071finvoiceType'])
            ->setF071finvoiceDate(new \DateTime($data['f071finvoiceDate']))
            // ->setF071fidFinancialYear(1)
            ->setF071fidFinancialYear((int)$data['f071fidFinancialYear'])
            ->setF071fdescription($data['f071fdescription'])
            ->setF071freason($data['f071freason'])
            ->setF071fidCustomer((int) $data['f071fidCustomer'])
            ->setF071fapprovedBy((int)$_SESSION['userId'])
            ->setF071fstatus((int) $data['f071fstatus'])
            ->setF071finvoiceTotal($data['f071finvoiceTotal'])
            ->setF071frevenueType((int)$data['f071frevenueType'])
            ->setF071customerName((int)$data['f071fcustomerName'])
            ->setF071fisRcp((int) $data['f071fisRcp'])
            ->setF071finvoiceFrom((int) $data['f071finvoiceFrom'])
            ->setF071fcreatedBy((int)$_SESSION['userId'])
            ->setF071fupdatedBy((int)$_SESSION['userId']);

        $invoice->setF071fcreatedDtTm(new \DateTime())
            ->setF071fupdatedDtTm(new \DateTime());

        try {
            $em->persist($invoice);

            $em->flush();

        } catch (\Exception $e) {
            echo $e;
        }

        $invoiceDetails = $data['invoice-details'];

        foreach ($invoiceDetails as $invoiceDetail) {

            $invoiceDetailObj = new T074frequestInvoiceDetails();
            $invoiceDetailObj->setF072fidItem((int) $invoiceDetail['f072fidItem'])
            ->setF072fdebitFundCode( $invoiceDetail['f072fdebitFundCode'])
            ->setF072fdebitAccountCode( $invoiceDetail['f072fdebitAccountCode'])
            ->setF072fdebitActivityCode( $invoiceDetail['f072fdebitActivityCode'])
            ->setF072fdebitDepartmentCode( $invoiceDetail['f072fdebitDepartmentCode'])
            ->setF072fcreditFundCode( $invoiceDetail['f072fcreditFundCode'])
            ->setF072fcreditAccountCode( $invoiceDetail['f072fcreditAccountCode'])
            ->setF072fcreditActivityCode( $invoiceDetail['f072fcreditActivityCode'])
            ->setF072fcreditDepartmentCode( $invoiceDetail['f072fcreditDepartmentCode'])
            ->setF072fgstCode($invoiceDetail['f072fgstCode'])
            ->setF072fgstValue((int)$invoiceDetail['f072fgstValue'])
            ->setF072fquantity((int) $invoiceDetail['f072fquantity'])
            ->setF072fprice((float)$invoiceDetail['f072fprice'])
            ->setF072ftotal((float)$invoiceDetail['f072ftotal'])
            ->setF072fidTaxCode((int) $invoiceDetail['f072fidTaxCode'])
            ->setF072ftotalExc((float) $invoiceDetail['f072ftotalExc'])
            ->setF072ftaxAmount((float) $invoiceDetail['f072ftaxAmount'])
            ->setF072fstatus((int) $data['f071fstatus'])

                ->setF072fcreatedBy((int)$_SESSION['userId'])
                ->setF072fupdatedBy((int)$_SESSION['userId'])
                ->setF072fidInvoice($invoice);

            $invoiceDetailObj->setF072fcreatedDtTm(new \DateTime())
                ->setF072fupdatedDtTm(new \DateTime());
            try {
                $em->persist($invoiceDetailObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }
        return $invoice;
    }
    public function requestInvoiceApprove($data)
    {
        $em = $this->getEntityManager();
        $status =$data['status'];
        $reason = $data['reason'];

        foreach ($data['id'] as $id)
        {
            if ($status == 1) 
            {
            
            try{
            $query1 = "insert into t071finvoice (f071finvoice_number,f071finvoice_type,f071fbill_type,f071fdescription,f071fid_financial_year,f071finvoice_date,f071fapproved_by,f071fstatus,f071fcreated_by,f071fupdated_by,f071fcreated_dt_tm,f071fupdated_dt_tm,f071finvoice_total,f071fbalance,f071fid_customer,f071fis_rcp,f071finvoice_from,f071fprocessed,f071fcr_amount,f071fdr_amount,f071ftotal_paid,f071fdiscount,f071fonline_payment) select f071finvoice_number,'OR',f071frevenue_type,f071fdescription,f071fid_financial_year,f071finvoice_date,f071fapproved_by,f071fstatus,f071fcreated_by,f071fupdated_by,f071fcreated_dt_tm,f071fupdated_dt_tm,f071finvoice_total,f071finvoice_total,f071fid_customer,f071fis_rcp,f071finvoice_from,0,0,0,0,0,0 from t073frequest_invoice where f071fid = $id"; 
// echo $query1;
//             die();
            // $em->getConnection()->executeQuery($query1);
            }
            catch(\Exception $e){
                echo $e;
            }
            //Mysql
            // $query = "select LAST_INSERT_ID() as id";

            //Sybase
            $query = "select @@identity as id from t071finvoice";
           
            // $result = $em->getConnection()->executeQuery($query)->fetch();
            $last_id = $result['id'];
            // echo $last_id;
            // die();
            $query2 = "insert into t072finvoice_details (f072fid_invoice,f072fid_item,f072fcredit_fund_code,f072fcredit_account_code,f072fcredit_activity_code,f072fcredit_department_code,f072fdebit_fund_code,f072fdebit_account_code,f072fdebit_activity_code,f072fdebit_department_code,f072fquantity,f072fprice,f072ftotal,f072fstatus,f072fcreated_by,f072fupdated_by,f072fcreated_dt_tm,f072fupdated_dt_tm,f072fgst_code,f072fgst_value,f072fid_tax_code,f072ftotal_exc,f072ftax_amount) select $last_id,f072fid_item,f072fcredit_fund_code,f072fcredit_account_code,f072fcredit_activity_code,f072fcredit_department_code,f072fdebit_fund_code,f072fdebit_account_code,f072fdebit_activity_code,f072fdebit_department_code,f072fquantity,f072fprice,f072ftotal,f072fstatus,f072fcreated_by,f072fupdated_by,f072fcreated_dt_tm,f072fupdated_dt_tm,f072fgst_code,f072fgst_value,f072fid_tax_code,f072ftotal_exc,f072ftax_amount from t074frequest_invoice_details where f072fid_invoice = $id";
            
            // echo $query2;
            // die();
            // $em->getConnection()->executeQuery($query2);
            }

            $query1 = "update t073frequest_invoice set f071fstatus = $status, f071freason = '$reason' where f071fid = $id";
            // print_r($query1);exit;
            $em->getConnection()->executeQuery($query1);
            $query2 = "update t074frequest_invoice_details set f072fstatus = $status where f072fid_invoice = $id";
            $em->getConnection()->executeQuery($query2);
            if ($status ==1)
            {
        
            

            
            $query  = "select ( 'IVD' + right('000000' + cast( count(*) as varchar(20)),6)+'/18') as reference_number from t071finvoice where f071finvoice_from = 2 and f071finvoice_type = 'OR'";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $number = $result['reference_number'];

            $time   = date('Y-m-d H:i:s');
            $query1 = "update t071finvoice set f071finvoice_number = '$number',f071fupdated_dt_tm = '$time' where f071fid = $last_id";

            $em->getConnection()->executeQuery($query1);
            }
        }
        return;
    }

    public function deleteRequestInvoiceDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t074frequest_invoice_details WHERE f072fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }


}
