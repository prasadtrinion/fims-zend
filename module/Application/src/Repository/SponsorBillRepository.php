<?php
namespace Application\Repository;

use Application\Entity\T063fsponsorBill;
use Application\Entity\T064fsponsorBillDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SponsorBillRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f063fid, s.f063fbillNo, s.f063fidSponsor,(sb.f063ffirstName+'-'+sb.f063flastName) as sponsor, s.f063fdate, s.f063famount, s.f063fstatus")
            ->from('Application\Entity\T063fsponsorBill', 's')
            ->leftjoin('Application\Entity\T063fsponsor','sb','with', 's.f063fidSponsor = sb.f063fid')
            ->orderBy('s.f063fid','DESC');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $sponsor) {
            $date                        = $sponsor['f063fdate'];
            $sponsor['f063fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $sponsor);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }
    public function getInvoices($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f063fid, s.f063fbillNo, s.f063fidSponsor, sb.f063ffirstName +'-'+sb.f063flastName as sponsor, s.f063fdate, s.f063famount, i.f071fid as invoiceId,i.f071finvoiceNumber,i.f071finvoiceTotal,i.f071finvoiceDate")
            ->from('Application\Entity\T063fsponsorBill', 's')
            ->leftjoin('Application\Entity\T063fsponsor','sb','with', 's.f063fidSponsor = sb.f063fid')
            ->leftjoin('Application\Entity\T064fsponsorBillDetails','sbd','with', 's.f063fid = sbd.f064fidSponserBill')
            ->leftjoin('Application\Entity\T071finvoice','i','with', 'i.f071fid = sbd.f064fidInvoice')
            ->where('s.f063fidSponsor = :sponsorId')
            ->setParameter('sponsorId',$id);
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $sponsor) {
            $date                        = $sponsor['f063fdate'];
            $sponsor['f063fdate'] = date("Y-m-d", strtotime($date));
            $date                        = $sponsor['f071finvoiceDate'];
            $sponsor['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $sponsor);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }
    public function getBills($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f063fid, s.f063fbillNo, s.f063fidSponsor, (sb.f063ffirstName+'-'+sb.f063flastName) as sponsor, s.f063fdate, s.f063famount, s.f063fstatus")
            ->from('Application\Entity\T063fsponsorBill', 's')
            ->leftjoin('Application\Entity\T063fsponsor','sb','with', 's.f063fidSponsor = sb.f063fid')
            ->where('s.f063fidSponsor = :sponsorId')
            ->setParameter('sponsorId',$id);
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $sponsor) {
            $date                        = $sponsor['f063fdate'];
            $sponsor['f063fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $sponsor);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

       $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('s.f063fid, s.f063fbillNo, s.f063fidSponsor, s.f063fdate, s.f063famount, s.f063fstatus, sd.f064fid, sd.f064fidInvoice, sd.f064fidStudent, sd.f064famount, sd.f064fstatus, i.f071finvoiceNumber, i.f071finvoiceDate')
             ->from('Application\Entity\T063fsponsorBill', 's')            
             ->leftjoin('Application\Entity\T064fsponsorBillDetails','sd','with', 'sd.f064fidSponserBill = s.f063fid')
             ->leftjoin('Application\Entity\T071finvoice','i','with', 'sd.f064fidInvoice = i.f071fid')
            ->where('s.f063fid = :sponserBillId')
            ->setParameter('sponserBillId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $sponser) {
            $date                        = $sponser['f063fdate'];
            $sponser['f063fdate'] = date("Y-m-d", strtotime($date));

            $date                        = $sponser['f071finvoiceDate'];
            $sponser['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $sponser);
        }

        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $sponsorBill = new T063fsponsorBill();

        $sponsorBill->setF063fidSponsor($data['f063fidSponsor'])
                    ->setF063fbillNo($data['f063fbillNo'])
                    ->setF063fdate(new \Datetime($data['f063fdate']))
                    ->setF063famount((float)$data['f063famount'])
                    ->setF063fstatus((int) $data['f063fstatus'])
                    ->setF063fcreatedBy((int)$_SESSION['userId'])
                    ->setF063fupdatedBy((int)$_SESSION['userId']);

        $sponsorBill->setF063fcreatedDtTm(new \DateTime())
                    ->setF063fupdatedDtTm(new \DateTime());

        try {
            $em->persist($sponsorBill);

            $em->flush();

        } catch (\Exception $e) {
            echo $e;
        }

        $sponsorBillDetails = $data['details'];

        foreach ($sponsorBillDetails as $sponsorBillDetail) {

            $sponsorBillDetailObj = new T064fsponsorBillDetails();
            $sponsorBillDetailObj->setF064fidInvoice((int) $sponsorBillDetail['f064fidInvoice'])
                                ->setF064fidStudent((int) $sponsorBillDetail['f064fidStudent'])
                                ->setF064famount((int) $sponsorBillDetail['f064famount'])
                                ->setF064fstatus((int) $data['f064fstatus'])
                                ->setF064fcreatedBy((int)$_SESSION['userId'])
                                ->setF064fupdatedBy((int)$_SESSION['userId'])
                                ->setF064fidSponserBill($sponsorBill);

            $sponsorBillDetailObj->setF064fcreatedDtTm(new \DateTime())
                                ->setF064fupdatedDtTm(new \DateTime());
            try {
                $em->persist($sponsorBillDetailObj);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }
        }
        return $sponsorBill;
    }

    public function getSponsorInvoiceList($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('s.f063fid, s.f063fbillNo, s.f063fidSponsor, s.f063fdate, s.f063famount, s.f063fstatus, i.f071finvoiceNumber, i.f071finvoiceDate')
             ->from('Application\Entity\T063fsponsorBill', 's')            
             // ->leftjoin('Application\Entity\T064fsponsorBillDetails','sd','with', 'sd.f064fidSponserBill = s.f063fid')
             ->leftjoin('Application\Entity\T071finvoice','i','with', 'sd.f064fidInvoice = i.f071fid')
            ->where('s.f063fid = :sponserBillId')
            ->setParameter('sponserBillId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $sponser) {
            $date                        = $sponser['f063fdate'];
            $sponser['f063fdate'] = date("Y-m-d", strtotime($date));

            $date                        = $sponser['f071finvoiceDate'];
            $sponser['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $sponser);
        }

        return $result1;
    }

     


}
