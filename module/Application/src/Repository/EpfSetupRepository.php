<?php
namespace Application\Repository;

use Application\Entity\T030fepfSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class EpfSetupRepository extends EntityRepository 
{ 

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('es.f030fid, es.f030fcategory,  es.f030fstartAmount, es.f030fendAmount, es.f030femployerContribution, es.f030femployerDeduction, es.f030fstatus,c.f030fcategoryName')
            ->from('Application\Entity\T030fepfSetup', 'es')
            ->leftjoin('Application\Entity\T030epfCategory', 'c', 'with','es.f030fcategory = c.f030fid')
            ->orderBy('es.f030fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('es.f030fid, es.f030fcategory, es.f030fstartAmount, es.f030fendAmount, es.f030femployerContribution, es.f030femployerDeduction, es.f030fstatus,c.f030fcategoryName')
            ->from('Application\Entity\T030fepfSetup', 'es')
            ->leftjoin('Application\Entity\T030epfCategory', 'c', 'with','es.f030fcategory = c.f030fid')
            ->where('es.f030fid = :epfId')
            ->setParameter('epfId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $epf = new T030fepfSetup();

        $epf->setF030fcategory((int)$data['f030fcategory'])
                ->setF030fstartAmount((float)$data['f030fstartAmount'])
                ->setF030fendAmount((float)$data['f030fendAmount'])
                ->setF030femployerContribution((float)$data['f030femployerContribution'])
                ->setF030femployerDeduction((float)$data['f030femployerDeduction'])
                ->setF030fstatus($data['f030fstatus'])
                ->setF030fcreatedBy((int)$_SESSION['userId'])
                ->setF030fupdatedBy((int)$_SESSION['userId']);

        $epf->setF030fcreatedDtTm(new \DateTime())
                ->setF030fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($epf);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $epf;
    }

    public function updateData($epf, $data = []) 
    {
        $em = $this->getEntityManager();
    
         $epf->setF030fcategory((int)$data['f030fcategory'])
                ->setF030fstartAmount((float)$data['f030fstartAmount'])
                ->setF030fendAmount((float)$data['f030fendAmount'])
                ->setF030femployerContribution((float)$data['f030femployerContribution'])
                ->setF030femployerDeduction((float)$data['f030femployerDeduction'])
                ->setF030fstatus($data['f030fstatus'])
                ->setF030fcreatedBy((int)$_SESSION['userId'])
                ->setF030fupdatedBy((int)$_SESSION['userId']);

        $epf->setF030fcreatedDtTm(new \DateTime())
                ->setF030fupdatedDtTm(new \DateTime());



        
        $em->persist($epf);
        $em->flush();

    }

    
}