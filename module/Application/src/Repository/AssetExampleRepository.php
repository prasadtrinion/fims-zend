<?php
namespace Application\Repository;

use Application\Entity\T135fassetExample;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetExampleRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('ae.f135fid, ac.f087fcategoryDiscription, c.f089fsubDescription, d.f090ftypeDescription')
            ->from('Application\Entity\T135fassetExample', 'ae') 
            ->leftjoin('Application\Entity\T087fassetCategory','ac', 'with','ac.f087fid = ae.f135fassetCategory')
           ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'ae.f135fassetSubcategory = c.f089fid')
            ->leftjoin('Application\Entity\T090fassetType', 'd', 'with', 'ae.f135fassetType = d.f090fid')
         ->orderBy('ae.f135fid','DESC');    
        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
       $result1 = array();
		


        foreach ($result as $createitem) 
        {
            $crdate                       = $createitem['f135fcreatedDtTm'];
            $createitem['f135fcreatedDtTm'] = date("Y-m-d", strtotime($crdate));
       
        
            $update                       = $createitem['f135fupdatedDtTm'];
            $createitem['f135fupdatedDtTm'] = date("Y-m-d", strtotime($update));
           
            array_push($result1,$createitem);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }
	public function getListById($id)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();


          $qb->select('ae.f135fid, ac.f087fcategoryDiscription, c.f089fsubDescription, d.f090ftypeDescription')
            ->from('Application\Entity\T135fassetExample', 'ae') 
            ->leftjoin('Application\Entity\T087fassetCategory','ac', 'with','ac.f087fid = ae.f135fassetCategory')
           ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'ae.f135fassetSubcategory = c.f089fid')
            ->leftjoin('Application\Entity\T090fassetType', 'd', 'with', 'ae.f135fassetType = d.f090fid')
             ->where('ae.f135fid =:assetId')
		   ->setParameter('assetId', $id);

		$query = $qb->getQuery();
		$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
       $result1 = array();
		
 
       foreach ($result as $createitem) 
        {
            $crdate                       = $createitem['f135fcreatedDtTm'];
            $createitem['f135fcreatedDtTm'] = date("Y-m-d", strtotime($crdate));
       
        
            $update                       = $createitem['f135fupdatedDtTm'];
            $createitem['f135fupdatedDtTm'] = date("Y-m-d", strtotime($update));
           
            array_push($result1,$createitem);
        }   

        $result = array(
            'data' => $result1,
        );
		return $result;
    }

	public function createNewData($data) 
	{
		
		$em = $this->getEntityManager();

		$assetExamp = new T135fassetExample(); 

  		$assetExamp->setF135fassetCategory((int)$data['f135fassetCategory'])
			    ->setF135fassetSubcategory((int)$data['f135fassetSubcategory'])
			    ->setF135fassetType((int)$data['f135fassetType'])
			    ->setF135fassetCode($data['f135fassetCode'])
			    ->setF135fstatus((int)$data['f135fstatus'])
			    ->setF135fcreatedBy((int)$_SESSION['userId'])
			    ->setF135fupdatedBy((int)$_SESSION['userId']);

	
        $assetExamp->setF135fcreatedDtTm(new \DateTime());

    
		try
		{
		$em->persist($assetExamp);
		$em->flush();
		}
		catch(\Exception $e)
		{
			echo $e;
		}
		return $assetExamp;
	}

	public function updateData($assetExamp, $data = []) 
	{
        $em = $this->getEntityManager();
        
      	$assetExamp->setF135fassetCategory((int)$data['f135fassetCategory'])
			    ->setF135fassetSubcategory((int)$data['f135fassetSubcategory'])
			    ->setF135fassetType((int)$data['f135fassetType'])
			    ->setF135fassetCode($data['f135fassetCode'])
			    ->setF135fstatus((int)$data['f135fstatus'])
			    ->setF135fcreatedBy((int)$_SESSION['userId'])
			    ->setF135fupdatedBy((int)$_SESSION['userId']);

		$assetExamp->setF135fupdatedDtTm(new \DateTime());

    			
        $em->persist($assetExamp);
        $em->flush();

    }
  
}
?>
	