<?php
namespace Application\Repository;

use Application\Entity\T082ftaxEmployeeDeduction;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TaxEmployeeDeductionRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

         $qb->select('t.f082fid, t.f082fidStaff, t.f082ftaxCode, t.f082freferenceNumber, t.f082fq1, t.f082fq2, t.f082fq3, t.f082fq4')

            ->from('Application\Entity\T082ftaxEmployeeDeduction', 't');
            
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function createNewData($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $taxDeduction = new T082ftaxEmployeeDeduction();

        $taxDeduction->setF082fidStaff((int)$_SESSION['staffId'])
             ->setF082ftaxCode($data['f082ftaxCode'])
              ->setF082freferenceNumber($data['f082freferenceNumber'])
              ->setF082fq1($data['f082fq1'])
              ->setF082fq2($data['f082fq2'])
              ->setF082fq3($data['f082fq3'])
              ->setF082fq4($data['f082fq4'])
             ->setF082fstatus((int)"1")
             ->setF082fcreatedBy((int)$_SESSION['userId'])
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $taxDeduction->setF082fcreatedDtTm(new \DateTime())
                ->setF082fupdatedDtTm(new \DateTime());
        try{
        $em->persist($taxDeduction);
        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }
            
        return $taxDeduction;
    }

    public function updateMaster($taxDeduction, $data = []) 
    {
             $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $taxDeduction->setF082fidStaff((int)$_SESSION['staffId'])
             ->setF082ftaxCode($data['f082ftaxCode'])
              ->setF082freferenceNumber($data['f082freferenceNumber'])
              ->setF082fq1($data['f082fq1'])
              ->setF082fq2($data['f082fq2'])
              ->setF082fq3($data['f082fq3'])
              ->setF082fq4($data['f082fq4'])
             ->setF082fstatus((int)"1")
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $taxDeduction->setF082fupdatedDtTm(new \DateTime());
        try{
        $em->persist($taxDeduction);
        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }
            
        return $taxDeduction;
    }

}
?>