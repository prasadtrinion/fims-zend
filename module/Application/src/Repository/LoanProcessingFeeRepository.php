<?php
namespace Application\Repository;

use Application\Entity\T142floanProcessingFee;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class LoanProcessingFeeRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('lpf.f142fid, lpf.f142fidLoanType, lpf.f142ffactor, lpf.f142feffectiveDate, lpf.f142famount, lpf.f142fstatus, lpf.f142ffeeType, lpf.f142ffactorAmount, lpf.f142ffactorDetail, l.f020floanName as loanName, lpf.f142ffund,lpf.f142fdepartment,lpf.f142factivity,lpf.f142faccount')
        ->from('Application\Entity\T142floanProcessingFee', 'lpf')
        ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'lpf.f142fidLoanType = l.f020fid ')
        ->orderBy('lpf.f142fid','DESC');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $intake) 
        {
            $date                        = $intake['f142feffectiveDate'];
            $intake['f142feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $intake);
        }
        $result = array(
          'data'=>$result1);

        return $result;
        
    } 


    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('lpf.f142fid, lpf.f142fidLoanType, lpf.f142ffactor, lpf.f142feffectiveDate, lpf.f142famount, lpf.f142fstatus, lpf.f142ffeeType, lpf.f142ffactorAmount, lpf.f142ffactorDetail, l.f020floanName as loanName, lpf.f142ffund,lpf.f142fdepartment,lpf.f142factivity,lpf.f142faccount')
        ->from('Application\Entity\T142floanProcessingFee', 'lpf')
        ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'lpf.f142fidLoanType = l.f020fid ')
        ->where('lpf.f142fid = :intakeId')
        ->setParameter('intakeId',(int)$id);
        
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $intake) 
        {
            $date                        = $intake['f142feffectiveDate'];
            $intake['f142feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $intake);
        }


        return $result1;
    }

    public function createLoan($data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $loan = new T142floanProcessingFee();

        $loan->setF142fidLoanType((int)$data['f142fidLoanType'])
        ->setF142ffactor($data['f142ffactor'])
        ->setF142famount((float)$data['f142famount'])
        ->setF142feffectiveDate(new \DateTime($data['f142feffectiveDate']))
        ->setF142ffeeType((int)$data['f142ffeeType'])
        ->setF142ffactorAmount((float)$data['f142ffactorAmount'])
        ->setF142ffactorDetail((float)$data['f142ffactorDetail'])
        ->setF142ffund($data['f142ffund'])
        ->setF142fdepartment($data['f142fdepartment'])
        ->setF142factivity($data['f142factivity'])
        ->setF142faccount($data['f142faccount'])
        ->setF142fstatus((int) $data['f142fstatus'])
        ->setF142fcreatedBy((int)$_SESSION['userId'])
        ->setF142fupdatedBy((int)$_SESSION['userId']);

        $loan->setF142fcreatedDtTm(new \DateTime())
        ->setF142fupdatedDtTm(new \DateTime());

        
        $em->persist($loan);

        $em->flush();
// print_r($loan);exit;
    }

    public function updateLoan($loan, $data)
    {
    	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $loan->setF142fidLoanType((int)$data['f142fidLoanType'])
        ->setF142ffactor($data['f142ffactor'])
        ->setF142famount((float)$data['f142famount'])
        ->setF142feffectiveDate(new \DateTime($data['f142feffectiveDate']))
        ->setF142ffeeType((int)$data['f142ffeeType'])
        ->setF142ffactorAmount((float)$data['f142ffactorAmount'])
        ->setF142ffactorDetail((float)$data['f142ffactorDetail'])
        ->setF142ffund($data['f142ffund'])
        ->setF142fdepartment($data['f142fdepartment'])
        ->setF142factivity($data['f142factivity'])
        ->setF142faccount($data['f142faccount'])
        ->setF142fstatus((int) $data['f142fstatus'])
        ->setF142fcreatedBy((int)$_SESSION['userId'])
        ->setF142fupdatedBy((int)$_SESSION['userId']);

        $loan->setF142fcreatedDtTm(new \DateTime())
        ->setF142fupdatedDtTm(new \DateTime());

        
        $em->persist($loan);
        $em->flush();
    }
}