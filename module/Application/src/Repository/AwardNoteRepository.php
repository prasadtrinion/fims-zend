<?php
namespace Application\Repository;

use Application\Entity\T014fawardNote;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetTypeRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f014fid, a.f014fidTendor, a.f014ftext, a.f014famount, a.f014fstatus')
            ->from('Application\Entity\T014fawardNote','a')
            ->orderBy('a.f014fid', 'DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f014fid, a.f014fidTendor, a.f014ftext, a.f014famount, a.f014fstatus')
            ->from('Application\Entity\T014fawardNote','a')
             ->where('a.f014fid = :awardId')
            ->setParameter('awardId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $awardNote = new T014fawardNote();

        $awardNote->setFf014fidTendor((int)$data['f014fidTendor'])
             ->setF014ftext($data['f014ftext'])
             ->setF014famount((float)$data['f014famount'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fcreatedBy((int)$_SESSION['userId'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $awardNote->setF014fcreatedDtTm(new \DateTime())
                ->setF014fupdatedDtTm(new \DateTime());

 
        $em->persist($awardNote);
        $em->flush();

        return $awardNote;

    }

    public function updateData($awardNote, $data = []) 
    {
        $em = $this->getEntityManager();

        $awardNote->setFf014fidTendor((int)$data['f014fidTendor'])
             ->setF014ftext($data['f014ftext'])
             ->setF014famount((float)$data['f014famount'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $awardNote->setF014fupdatedDtTm(new \DateTime());

 
        $em->persist($awardNote);
        $em->flush();


    }

}