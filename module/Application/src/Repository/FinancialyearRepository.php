<?php
namespace Application\Repository;

use Application\Entity\T015ffinancialyear;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class FinancialyearRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f015fid,f.f015fname, f.f015fstartDate, f.f015fendDate, f.f015fcreatedBy, f.f015fupdatedBy, f.f015fstatus')
            ->from('Application\Entity\T015ffinancialyear','f')
            ->orderby('f.f015fname');

        
        $query = $qb->getQuery();
       
        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f015fstartDate'];
            $item['f015fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f015fendDate'];
            $item['f015fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('f.f015fid,f.f015fname, f.f015fstartDate, f.f015fendDate, f.f015fcreatedBy, f.f015fupdatedBy, f.f015fstatus')
            ->from('Application\Entity\T015ffinancialyear','f')
            ->where('f.f015fid = :financialId')
            ->setParameter('financialId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f015fstartDate'];
            $item['f015fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f015fendDate'];
            $item['f015fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        
        return $result1;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        if($data['f015fstatus'] == 1)
        {
            $query1 = "UPDATE t015ffinancialyear set f015fstatus=0"; 
            $result=$em->getConnection()->executeQuery($query1);
        }

        $financial = new T015ffinancialyear();

        $financial->setF015fname($data['f015fname'])
              ->setF015fstartDate(new \DateTime($data['f015fstartDate']))
              ->setF015fendDate(new \DateTime($data['f015fendDate']))
              ->setF015fstatus((int)$data['f015fstatus'])
              ->setF015fcreatedBy((int)$_SESSION['userId'])
              ->setF015fupdatedBy((int)$_SESSION['userId']);


        $financial->setF015fcreateDtTm(new \DateTime())
                ->setF015fupdateDtTm(new \DateTime());

 
        $em->persist($financial);
        $em->flush();

        return $financial;

    }

    public function updateData($financial, $data = []) 
    {
        $em = $this->getEntityManager();

        

        $id = $financial->getF015fid();
        if($data['f015fstatus'] == 1)

        {
            $query1 = "UPDATE t015ffinancialyear set f015fstatus=0 where f015fid != $id"; 
            $result=$em->getConnection()->executeQuery($query1);
        }
        

        $financial->setF015fname($data['f015fname'])
              ->setF015fstartDate(new \DateTime($data['f015fstartDate']))
              ->setF015fendDate(new \DateTime($data['f015fendDate']))
              ->setF015fstatus((int)$data['f015fstatus'])
              ->setF015fcreatedBy((int)$_SESSION['userId'])
              ->setF015fupdatedBy((int)$_SESSION['userId']);
        
        $financial->setF015fupdateDtTm(new \DateTime());



        try
        {
        $em->persist($financial);
        
        $em->flush();
        

        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $financial;


    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f015fid,f.f015fname, f.f015fstartDate, f.f015fendDate, f.f015fcreatedBy, f.f015fupdatedBy, f.f015fstatus')
            ->from('Application\Entity\T015ffinancialyear','f')
            ->where('f.f015fstatus=1');

        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f015fstartDate'];
            $item['f015fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f015fendDate'];
            $item['f015fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
       

        return $result1;
        
    }

    public function getActiveFinancialyear() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('f.f015fid,f.f015fname, f.f015fstartDate, f.f015fendDate, f.f015fcreatedBy, f.f015fupdatedBy, f.f015fstatus')
            ->from('Application\Entity\T015ffinancialyear','f')
            ->where('f.f015fstatus=1');

        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f015fstartDate'];
            $item['f015fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f015fendDate'];
            $item['f015fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
       

        return $result1;
        
    }

 }
 ?>

