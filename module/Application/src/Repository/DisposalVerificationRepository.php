<?php
namespace Application\Repository;

use Application\Entity\T087fdisposalVerification;
use Application\Entity\T087fdisposalVerDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DisposalVerificationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('d.f086fid, d.f086fbatchId, d.f086fdescription, d.f086fverificationStatus,d.f086fstatus,d.f086frequestBy,d.f086fisEndorsement,u.f014fuserName')
            ->from('Application\Entity\T086fdisposalRequisition','d')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','d.f086fcreatedBy = u.f014fid')
            ->where('d.f086fisEndorsement = 0')
            ->orderBy('d.f086fid','DESC');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f087fid, d.f087fmeetingNumber, d.f087ftype, d.f087fremarks, d.f087freferenceNumber, d.f087fstatus, ad.f087fidDetails, ad.f087fassetCode, ad.f087fidAsset , ad.f087fdescription, ad.f087fnetValue, ad.f087ftype, ad.f087freason, u.f014fuserName as endorsedBy, ad.f087fupdatedDtTm, d.f087fidRequisition, dr.f086fbatchId, dr.f086fdescription, dr.f086fisEndorsement')
            ->from('Application\Entity\T087fdisposalVerification','d')
            ->leftjoin('Application\Entity\T087fdisposalVerDetails', 'ad', 'with','d.f087fid = ad.f087fidDisposal')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','u.f014fid = ad.f087fupdatedBy')
            ->leftjoin('Application\Entity\T086fdisposalRequisition', 'dr', 'with','d.f087fidRequisition = dr.f086fid')
            ->where('d.f087fidRequisition = :disposalId')
            ->setParameter('disposalId',$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $note) {
            $date                        = $note['f087fupdatedDtTm'];
            $note['f087fupdatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $note);
        }
        return $result1;
    }

    public function createDisposal($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="DV";
        $number = $configRepository->generateFIMS($numberData);

        $disposal = new T087fdisposalVerification();

        $disposal->setF087fmeetingNumber($data['f087fmeetingNumber'])
             ->setF087ftype($data['f087ftype'])
             ->setF087freferenceNumber($number)
             ->setF087fremarks($data['f087fremarks'])
             ->setF087fstatus($data['f087fstatus'])
             ->setF087fidRequisition($data['f087fidRequisition'])
             ->setF087fcreatedBy((int)$_SESSION['userId'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $disposal->setF087fcreatedDtTm(new \DateTime())
                ->setF087fupdatedDtTm(new \DateTime());
        
        $em->persist($disposal);
        $em->flush();
        $verificationDetails = $data['verification-details'];
        foreach ($verificationDetails as $verificationDeatail) {

                $disposalDetailsObj = new T087fdisposalVerDetails();

            $disposalDetailsObj->setF087fidDisposal((int)$disposal->getF087fid())
                       ->setF087fassetCode($verificationDeatail['f087fassetCode'])
                       ->setF087fdescription($verificationDeatail['f087fdescription'])
                       ->setF087fnetValue((float)$verificationDeatail['f087fnetValue'])
                       ->setF087ftype((int)$verificationDeatail['f087ftype'])
                       ->setF087fstatus((int)$verificationDeatail['f087fstatus'])
                       ->setF087fidAsset((int)$verificationDeatail['f087fidAsset'])
                       ->setF087fcreatedBy((int)$_SESSION['userId'])
                       ->setF087fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailsObj->setF087fcreatedDtTm(new \DateTime())
                       ->setF087fupdatedDtTm(new \DateTime());
        

                $em->persist($disposalDetailsObj);

                $em->flush();
                // print_r($disposalDetailsObj);exit;
        }
            
        return $disposal;
    }

   public function updateDisposal($disposal, $data = []) 
    {
        $em = $this->getEntityManager();

         $disposal->setF087fmeetingNumber($data['f087fmeetingNumber'])
             ->setF087ftype($data['f087ftype'])
             ->setF087fremarks($data['f087fremarks'])
             ->setF087fstatus($data['f087fstatus'])
             ->setF087fidRequisition($data['f087fidRequisition'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $disposal->setF087fupdatedDtTm(new \DateTime());
        
        $em->persist($disposal);
        $em->flush();

        return $disposal;
    }


    public function updateDisposalDetail($disposalDetailObj, $disposalDetail)
    {

        $em = $this->getEntityManager();


        $disposalDetailObj->setF087fassetCode($disposalDetail['f087fassetCode'])
                       ->setF087fdescription($disposalDetail['f087fdescription'])
                       ->setF087fnetValue((float)$disposalDetail['f087fnetValue'])
                       ->setF087fidAsset((int)$disposalDetail['f087fidAsset'])
                       ->setF087ftype((int)$disposalDetail['f087ftype'])
                       ->setF087fstatus((int)$data['f087fstatus']);
                       // ->setF087fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailObj->setF087fupdatedDtTm(new \DateTime());

        $em->persist($disposalDetailObj);

        $em->flush();

            
        
    }
    
    public function createDisposalDetail($disposalObj, $disposalDetail)
    {
        $em = $this->getEntityManager();

        $disposalDetailObj = new T087fdisposalVerDetails();

        $disposalDetailObj->setF087fidDisposal((int)$disposalObj->getF087fid())
                       ->setF087fassetCode($disposalDetail['f087fassetCode'])
                       ->setF087fdescription($disposalDetail['f087fdescription'])
                       ->setF087fnetValue((float)$disposalDetail['f087fnetValue'])
                       ->setF087ftype((int)$disposalDetail['f087ftype'])
                       ->setF087fidAsset((int)$disposalDetail['f087fidAsset'])
                       ->setF087fstatus((int)$disposalDetail['f087fstatus'])
                       ->setF087fcreatedBy((int)$_SESSION['userId'])
                       ->setF087fupdatedBy((int)$_SESSION['userId']);

            $disposalDetailObj->setF087fcreatedDtTm(new \DateTime())
                       ->setF087fupdatedDtTm(new \DateTime());

        try
        {
        $em->persist($disposalDetailsObj);
        $em->flush();
        }
        catch(\Exception $e)
            {
                echo $e;
            }
        return $disposalDetailsObj;
        
    }

    public function getListByVerificationStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select(' ad.f087fidDetails, ad.f087fassetCode,ad.f087fidAsset, ad.f087fdescription,ad.f087fnetValue,ad.f087ftype,ad.f087freason,u.f014fuserName as endorsedBy,ad.f087fupdatedDtTm as edorsedDate,u1.f014fuserName as approvedBy,ad.f087fcreatedDtTm as approvedDate')
            ->from('Application\Entity\T087fdisposalVerDetails', 'ad')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','u.f014fid = ad.f087fupdatedBy')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with','u.f014fid = ad.f087fcreatedBy')

            ->where('ad.f087fstatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
            $qb->select(' ad.f087fidDetails, ad.f087fassetCode,ad.f087fidAsset ,ad.f087fdescription,ad.f087fnetValue,ad.f087ftype,ad.f087freason,u.f014fuserName as endorsedBy,ad.f087fupdatedDtTm as edorsedDate,u1.f014fuserName as approvedBy,ad.f087fcreatedDtTm as approvedDate')
            ->from('Application\Entity\T087fdisposalVerDetails', 'ad')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','u.f014fid = ad.f087fupdatedBy')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with','u.f014fid = ad.f087fcreatedBy')


                 ->where('ad.f087fstatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
            $qb->select(' ad.f087fidDetails, ad.f087fassetCode,ad.f087fidAsset ,ad.f087fdescription,ad.f087fnetValue,ad.f087ftype,ad.f087freason,u.f014fuserName as endorsedBy,ad.f087fupdatedDtTm as edorsedDate,u1.f014fuserName as approvedBy,ad.f087fcreatedDtTm as approvedDate')
            ->from('Application\Entity\T087fdisposalVerDetails', 'ad')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','u.f014fid = ad.f087fupdatedBy')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with','u.f014fid = ad.f087fcreatedBy')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with','u.f014fid = ad.f087fcreatedBy')
            ;
                
            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
         $result1 = array();
        foreach ($data as $note) {
            $date                        = $note['f087fupdatedDtTm'];
            $note['f087fupdatedDtTm'] = date("Y-m-d", strtotime($date));
            $date                        = $note['f087fcreatedDtTm'];
            $note['f087fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $note);
        }
        return $result1;
        
    }

    public function approveDisposal($data) 
    {
        $em = $this->getEntityManager();
        $reason = $data['reason'];
        $status = $data['status'];
        foreach ($data['id'] as $id) 
        {


            $user = $_SESSION['userId'];
            $date = date('Y-m-d h:m:s');

        $getIdAsset = "select f087fid_asset,f087fid_disposal from t087fdisposal_ver_details where f087fid_details = $id";
        $select = $em->getConnection()->executeQuery($getIdAsset);
        $getIdAsset = $select->fetch();
        // print_r($getIdAsset);exit;

        $idAsset = $getIdAsset['f087fid_asset'];
        $idDisposal = $getIdAsset['f087fid_disposal'];


        // $query1 = "update t086fdisposal_requisition set f086fis_endorsement = 1,f086fupdated_by = $user,f086fupdated_dt_tm='$date' where f086fid = $id";
        // $reqUpdate = $em->getConnection()->executeQuery($query1);

        // $query2 = "update t086fdisposal_req_details set f086fstatus = 1,f086fupdated_by = $user,f086fupdated_dt_tm='$date' where f086fid_disposal = $id";
        // $reqDetailUpdate = $em->getConnection()->executeQuery($query2);

        $query3 = "update t087fdisposal_verification set f087fstatus = 1,f087fupdated_by = $user,f087fupdated_dt_tm='$date' where f087fid = $idDisposal";
        $verUpdate = $em->getConnection()->executeQuery($query3);

         $query4 = "update t087fdisposal_ver_details set f087fstatus = 1,f087fupdated_by = $user,f087fupdated_dt_tm='$date', f087freason = '$reason' where f087fid_details = $id";
        $verDetailUpdate = $em->getConnection()->executeQuery($query4);

        $query5 = "update t085fasset_information set f085fstatus = 2,f085fupdated_by = $user,f085fupdated_dt_tm='$date' where f085fid = $idAsset";
        $assetInfoUpdate = $em->getConnection()->executeQuery($query5);



        //INSERTING INTO VERIFICATION DETAILS


        // $query3 = "select * from t086fdisposal_requisition where f086fid = $id";
        // $select = $em->getConnection()->executeQuery($query3);
        // $reqResult = $select->fetch();


        // $query4 = " select f086fid_details, f086fid_disposal, f086fasset_code, f086fdescription, f086fnet_value, f086ftype, f086fstatus, f086fcreated_by, f086fupdated_by, f086fcreated_dt_tm, f086fupdated_dt_tm, f086fid_asset from t086fdisposal_req_details where f086fid_disposal = $id";
       
        // $select = $em->getConnection()->executeQuery($query4);
        // $reqDetailresult = $select->fetch();



        // if (isset($reqDetailresult))
        // {
        

        // $disposalDetailObj = new T087fdisposalVerDetails();

        // $disposalDetailObj->setF087fidDisposal($reqDetailresult['f086fid_disposal'])
        //                ->setF087fassetCode($reqDetailresult['f086fasset_code'])
        //                ->setF087fdescription($reqDetailresult['f086fdescription'])
        //                ->setF087fnetValue($reqDetailresult['f086fnet_value'])
        //                ->setF087ftype($reqDetailresult['f086ftype'])
        //                ->setF087fstatus((int)$reqDetailresult['f086fstatus'])
        //                ->setF087fidAsset($['f086fid_asset'])
        //                ->setF087fcreatedBy((int)$_SESSION['userId'])
        //                ->setF087fupdatedBy((int)$_SESSION['userId']);

        //     $disposalDetailObj->setF087fcreatedDtTm(new \DateTime())
        //                ->setF087fupdatedDtTm(new \DateTime());

        //     try
        //     {
        //         $em->persist($disposalDetailObj);
        //         $em->flush();
        //     }
        //      catch(\Exception $e)
        //     {
        //         echo $e;
        //     }
        // }


        //  if (isset($reqResult))
        //  {
            
        //     $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        //     $numberData = array();
        //     $numberData['type']="DV";
        //     $number = $configRepository->generateFIMS($numberData);
        
        //     $disposal = new T087fdisposalVerification();

        //     $disposal->setF087fmeetingNumber($data['f087fmeetingNumber'])
        //      ->setF087ftype($data['f087ftype'])
        //      ->setF087freferenceNumber($number)
        //      ->setF087fremarks($data['f087fremarks'])
        //      ->setF087fstatus($data['f087fstatus'])
        //      ->setF087fcreatedBy((int)$_SESSION['userId'])
        //      ->setF087fupdatedBy((int)$_SESSION['userId']);

        //     $disposal->setF087fcreatedDtTm(new \DateTime())
        //         ->setF087fupdatedDtTm(new \DateTime());
        //     try
        //     {
        //         $em->persist($disposal);
        //         $em->flush();
        //     }
        //      catch(\Exception $e)
        //     {
        //         echo $e;
        //     }
        //  }


        }
    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

     public function getAllVerifiedDetails($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
// print_r($approvedStatus);exit;

        // Query
        if ($approvedStatus == 0 && isset($approvedStatus))
        {

        $qb->select('d.f087fidDetails, d.f087fidDisposal, d.f087fassetCode, d.f087fdescription, d.f087fnetValue, d.f087fstatus, ai.f085fassetCode, ai.f085fassetDescription')
            ->from('Application\Entity\T087fdisposalVerDetails','d')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with','ai.f085fid = d.f087fidAsset')
            ->where('d.f087fstatus = :disposalId')
            ->setParameter('disposalId',$approvedStatus)
            ->orderBy('d.f087fidDetails','DESC');
        }
        elseif ($approvedStatus == 1 && isset($approvedStatus))
        {
            
        $qb->select('d.f087fidDetails, d.f087fidDisposal, d.f087fassetCode, d.f087fdescription, d.f087fnetValue, d.f087fstatus, ai.f085fassetCode, ai.f085fassetDescription')
            ->from('Application\Entity\T087fdisposalVerDetails','d')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with','ai.f085fid = d.f087fidAsset')
            ->where('d.f087fstatus = :disposalId')
            ->setParameter('disposalId',$approvedStatus)
            ->orderBy('d.f087fidDetails','DESC');
        }

        elseif ($approvedStatus == 2 && isset($approvedStatus))
        {
        $qb->select('d.f087fidDetails, d.f087fidDisposal, d.f087fassetCode, d.f087fdescription, d.f087fnetValue, d.f087fstatus, ai.f085fassetCode, ai.f085fassetDescription')
            ->from('Application\Entity\T087fdisposalVerDetails','d')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with','ai.f085fid = d.f087fidAsset')
            ->where('d.f087fstatus = :disposalId')
            ->setParameter('disposalId',$approvedStatus)
            ->orderBy('d.f087fidDetails','DESC');
        }

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function deleteDisposalVerDetail($id)
    {
        $em = $this->getEntityManager();
        $query = "delete from t087fdisposal_ver_details where f087fid_details = $id";
        $em->getConnection()->executeQuery($query);
        return ;
    }

    public function getEndorsmentList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

            $qb->select('d.f087fmeetingNumber, d.f087fremarks, d.f087freferenceNumber, ad.f087fdescription, ad.f087fassetCode, ad.f087fnetValue, ad.f087fidDetails, ai.f085fassetCode, ai.f085fassetDescription ')
            ->from('Application\Entity\T087fdisposalVerification','d')
            ->leftjoin('Application\Entity\T087fdisposalVerDetails', 'ad', 'with','d.f087fid = ad.f087fidDisposal')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with','ai.f085fid = ad.f087fidAsset')
            ->where('d.f087fstatus = 0');
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
    }
}

?>



