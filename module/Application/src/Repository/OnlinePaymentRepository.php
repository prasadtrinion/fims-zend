<?php
namespace Application\Repository;

use Application\Entity\T018fonlinePayment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class OnlinePaymentRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('o.f018fid, o.f018fid, o.f018fname,o.f018fcode, o.f018fstatus')
            ->from('Application\Entity\T018fonlinePayment','o')
            ->orderBy('o.f018fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('o.f018fid, o.f018fname,o.f018fcode, o.f018fstatus')
            ->from('Application\Entity\T018fonlinePayment','o')
            ->where('o.f018fid = :paymentId')
            ->setParameter('paymentId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $online = new T018fonlinePayment();

        $online->setF018fname($data['f018fname'])
             ->setF018fcode($data['f018fcode'])
             ->setF018fstatus($data['f018fstatus'])
             ->setF018fcreatedBy((int)$_SESSION['userId'])
             ->setF018fupdatedBy((int)$_SESSION['userId']);

        $online->setF018fcreatedDtTm(new \DateTime())
                ->setF018fupdatedDtTm(new \DateTime());
               

        $em->persist($online);
        
        $em->flush();


        return $online;

    }

    /* to edit the data in database*/

    public function updateData($online, $data = []) 
    {
        $em = $this->getEntityManager();

       $online->setF018fname($data['f018fname'])
               ->setF018fcode($data['f018fcode'])
             ->setF018fstatus($data['f018fstatus'])
             ->setF018fupdatedBy((int)$_SESSION['userId']);

        $online->setF018fupdatedDtTm(new \DateTime());

        
        $em->persist($online);
        
        $em->flush();
 

    }


    
}

?>