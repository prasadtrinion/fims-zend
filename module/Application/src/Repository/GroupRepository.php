<?php
namespace Application\Repository;

use Application\Entity\T012fgroup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class GroupRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f012fid as f012fid,b.f012fgroupName as f012fgroupName,a.f012fid as f012fidParent, a.f012fgroupName as parent_name, b.f012forder, a.f012fstatus')
           ->from('Application\Entity\T012fgroup', 'a')
           ->leftjoin('Application\Entity\T012fgroup', 'b','with', 'a.f012fid=b.f012fidParent');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }
 
    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('b.f012fid as f012fid,b.f012fgroupName as f012fgroupName,a.f012fid as f012fidParent, a.f012fgroupName as parent_name, b.f012forder,a.f012fstatus')
           ->from('Application\Entity\T012fgroup', 'a')
           ->leftjoin('Application\Entity\T012fgroup', 'b','with', 'a.f012fid=b.f012fidParent')
            ->where('b.f012fid = :GroupId')
            ->setParameter('GroupId',$id);

        
        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
        
    }

     /* to insert the data to database*/

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $group = new T012fgroup();

        $group->setF012fgroupName($data['f012fgroupName'])
             ->setF012fidParent($data['f012fidParent'])  
             ->setF012forder($data['f012forder'])
             ->setF012fstatus($data['f012fstatus'])
             ->setF012fcreatedBy((int)$_SESSION['userId'])
             ->setF012fupdatedBy((int)$_SESSION['userId']);

        $group->setF012fcreateDtTm(new \DateTime())
                ->setF012fupdateDtTm(new \DateTime());


        $em->persist($group);
        $em->flush();

       
        return $group;

    }

    /* to edit the data in database*/

    public function updateData($group, $data = []) 
    {
        $em = $this->getEntityManager();

        $group->setF012fgroupName($data['f012fgroupName'])
             ->setF012fidParent($data['f012fidParent'])  
             ->setF012forder($data['f012forder'])
             ->setF012fstatus($data['f012fstatus'])
             ->setF012fcreatedBy((int)$_SESSION['userId'])
             ->setF012fupdatedBy((int)$_SESSION['userId']);
                

        $group->setF012fUpdateDtTm(new \DateTime());
        
        $em->persist($group);
        try{
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }


    }

    public function getParentId()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();


        $qb->select('a.f012fid as parent_id, a.f012fgroupName as parent_name,b.f012fid as group_id, b.f012fgroupName as group_name')
           ->from('Application\Entity\T012fgroup', 'a')
           ->leftjoin('Application\Entity\T012fgroup', 'b','with', 'a.f012fid=b.f012fidParent');
        

       $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to update the data in database*/

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>