<?php
namespace Application\Repository;

use Application\Entity\T061finvestmentInstitution;
use Application\Entity\T061finstituteDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class InvestmentInstitutionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f061fid, i.f061finstitutionCode ,i.f061fname, i.f061faddress, i.f061fcontactNo, i.f061fcontactPerson, i.f061fstatus, i.f061fbranch, i.f061fidBank, b.f042fbankName, i.f061fcountry, i.f061fstate, i.f061fcity, i.f061fpostCode, s.f012fstateName, c.f013fcountryName, b.f042fbranchName')
            ->from('Application\Entity\T061finvestmentInstitution','i')
            ->leftjoin('Application\Entity\T042fbank', 'b','with','i.f061fidBank= b.f042fid')
            ->leftjoin('Application\Entity\T013fcountry', 'c','with','i.f061fcountry= c.f013fid')
            ->leftjoin('Application\Entity\T012fstate', 's','with','s.f012fid= i.f061fstate')
            ->orderBy('i.f061fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('i.f061fid, i.f061finstitutionCode, i.f061fname, i.f061faddress, i.f061fcontactNo, i.f061fcontactPerson, i.f061fstatus, i.f061fidBank, b.f042fbankName, i.f061fbranch, id.f061fidDetails, id.f061fidInstitute, id.f061fidInvestmentType,  i.f061fcountry, i.f061fstate, i.f061fcity, i.f061fpostCode,  s.f012fstateName, c.f013fcountryName, ti.f038ftype, b.f042fbranchName')
            ->from('Application\Entity\T061finstituteDetails','id')
            ->leftjoin('Application\Entity\T061finvestmentInstitution', 'i','with','id.f061fidInstitute= i.f061fid')
            ->leftjoin('Application\Entity\T013fcountry', 'c','with','i.f061fcountry= c.f013fid')
            ->leftjoin('Application\Entity\T012fstate', 's','with','s.f012fid= i.f061fstate')
            ->leftjoin('Application\Entity\T042fbank', 'b','with','i.f061fidBank= b.f042fid')
            ->leftjoin('Application\Entity\T038finvestmentType', 'ti','with','ti.f038fid= id.f061fidInvestmentType')
            ->where('i.f061fid = :instituteId')
            ->setParameter('instituteId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result; 
    }

     public function getListByInvestmentType($institution) 
    {
        $em = $this->getEntityManager();


        $query = "select distinct(p.f038ftype), id.f061fid_institute, id.f061fid_investment_type, p.f038fid FROM t061finstitute_details as id inner join t038finvestment_type as p on id.f061fid_investment_type = p.f038fid where id.f061fid_institute = $institution";

        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result; 
    }


    public function createInstitution($data) 
    {
       
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $institute = new T061finvestmentInstitution();

        $institute->setF061finstitutionCode($data['f061finstitutionCode'])
             ->setF061fname($data['f061fname'])
             ->setF061faddress($data['f061faddress'])
             ->setF061fcontactNo($data['f061fcontactNo'])
             ->setF061fidBank((int)$data['f061fidBank'])
             ->setF061fbranch($data['f061fbranch'])
             ->setF061fcountry((int)$data['f061fcountry'])
             ->setF061fstate((int)$data['f061fstate'])
             ->setF061fcity($data['f061fcity'])
             ->setF061fpostCode($data['f061fpostCode'])
             ->setF061fcontactPerson($data['f061fcontactPerson'])
             ->setF061fstatus((int)$data['f061fstatus'])
             ->setF061fcreatedBy((int)$_SESSION['userId'])
             ->setF061fupdatedBy((int)$_SESSION['userId']);

        $institute->setF061fcreatedDtTm(new \DateTime())
                ->setF061fupdatedDtTm(new \DateTime());

 
        $em->persist($institute);
        $em->flush();


        $instituteDetails = $data['institute-details'];

        foreach ($instituteDetails as $instituteDetail) 
        {

            $instituteDetailObj = new T061finstituteDetails();

            $instituteDetailObj->setF061fidInstitute((int)$institute->getF061fid())
                                 ->setF061fidInvestmentType((int)$instituteDetail['f061fidInvestmentType'])
                                 ->setF061fstatus((int)$instituteDetail['f061fstatus'])
                                 ->setF061fcreatedBy((int)$_SESSION['userId'])
                                 ->setF061fupdatedBy((int)$_SESSION['userId']);

            $instituteDetailObj->setF061fcreatedDtTm(new \DateTime())
                                 ->setF061fupdatedDtTm(new \DateTime());

        
        try{
                $em->persist($instituteDetailObj);

                $em->flush();
            }
        catch(\Exception $e)
        {
            echo $e;
        }
            
        }
        return $institute;
    }

    public function updateInstitution($institute, $data = [])
    {
        $em = $this->getEntityManager();

       $em = $this->getEntityManager();

        $institute->setF061finstitutionCode($data['f061finstitutionCode'])
             ->setF061fname($data['f061fname'])
             ->setF061faddress($data['f061faddress'])
             ->setF061fcontactNo($data['f061fcontactNo'])
             ->setF061fcontactPerson($data['f061fcontactPerson'])
             ->setF061fidBank((int)$data['f061fidBank'])
             ->setF061fbranch($data['f061fbranch'])
             ->setF061fcountry((int)$data['f061fcountry'])
             ->setF061fstate((int)$data['f061fstate'])
             ->setF061fcity($data['f061fcity'])
             ->setF061fpostCode($data['f061fpostCode'])
             ->setF061fstatus((int)$data['f061fstatus'])
             ->setF061fcreatedBy((int)$_SESSION['userId'])
             ->setF061fupdatedBy((int)$_SESSION['userId']);

        $institute->setF061fcreatedDtTm(new \DateTime())
                ->setF061fupdatedDtTm(new \DateTime());

        $em->persist($institute);
        $em->flush();

        return $institute;
       
    }


    public function updateInstitutionDetail($instituteDetailObj, $instituteDetail)
    {
        
        $em = $this->getEntityManager();

        $instituteDetailObj->setF061fidInvestmentType((int)$instituteDetail['f061fidInvestmentType'])
                                 ->setF061fstatus((int)$instituteDetail['f061fstatus'])
                                 ->setF061fupdatedBy((int)$_SESSION['userId']);

        $instituteDetailObj->setF061fupdatedDtTm(new \DateTime());

        

        $em->persist($instituteDetailObj);

        $em->flush();

        
    }
    
    public function createInstitutionDetail($instituteObj, $instituteDetail)
    {
        $em = $this->getEntityManager();
      

        $instituteDetailObj = new T061finstituteDetails();

         $instituteDetailObj->setF061fidInstitute((int)$instituteObj->getF061fid())
                                 ->setF061fidInvestmentType((int)$instituteDetail['f061fidInvestmentType'])
                                 ->setF061fstatus((int)$instituteDetail['f061fstatus'])
                                 ->setF061fcreatedBy((int)$_SESSION['userId'])
                                 ->setF061fupdatedBy((int)$_SESSION['userId']);

        $instituteDetailObj->setF061fcreatedDtTm(new \DateTime())
                            ->setF061fupdatedDtTm(new \DateTime());

        

            $em->persist($instituteDetailObj);

            $em->flush();

        // return $incrementDetailObj;
        

        return $instituteDetailObj;

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f061fid, i.f061finstitutionCode ,i.f061fname, i.f061faddress, i.f061fcontactNo, i.f061fcontactPerson, i.f061fstatus, i.f061fbranch, i.f061fidBank, b.f042fbankName, i.f061fcountry, i.f061fstate, i.f061fcity, i.f061fpostCode')
            ->from('Application\Entity\T061finvestmentInstitution','i')
            ->leftjoin('Application\Entity\T042fbank', 'b','with','i.f061fidBank= b.f042fid')
            ->where("i.f061fstatus = 1")
            ->orderBy('i.f061fid','DESC');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function deleteInvestmentInstitution($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $query2 = "DELETE from t061finstitute_details WHERE f061fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

}

?>
