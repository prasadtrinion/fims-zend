<?php
namespace Application\Repository;

use Application\Entity\T087fassetMovement;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class AssetMovementRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query


         $qb->select("a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName,(d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as department, a.f087fstatus, a.f087fapproval1Status, a.f087fapproval2Status,a.f087fapproved1By,a.f087fapproved2By")
            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
             ->orderBy('a.f087fid','DESC');
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        for($i=0;$i<count($result);$i++){
            $date                        = $result[$i]['f087fstartDate'];
            $result[$i]['f087fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $result[$i]['f087ffromDate'];
            $result[$i]['f087ffromDate'] = date("Y-m-d", strtotime($date));
            $date                        = $result[$i]['f087ftoDate'];
            $result[$i]['f087ftoDate'] = date("Y-m-d", strtotime($date));
        $qb = $em->createQueryBuilder();
        
        $qb->select("u1.f014fuserName as ApprovedUser1")
            ->from('Application\Entity\T087fassetMovement', 'a')
             ->leftjoin('Application\Entity\T014fuser','u1','with', 'u1.f014fid= a.f087fapproved1By');
        $approved1_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $approved1_result = $approved1_result[0];
        $result[$i]['ApprovedUser1'] = $approved1_result['ApprovedUser1'];
         $qb = $em->createQueryBuilder();
        
        $qb->select("u2.f014fuserName as ApprovedUser2")
            ->from('Application\Entity\T087fassetMovement', 'a')
             ->leftjoin('Application\Entity\T014fuser','u2','with', 'u2.f014fid= a.f087fapproved2By');
        $approved2_result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $approved2_result = $approved2_result[0];
        $result[$i]['ApprovedUser2'] = $approved2_result['ApprovedUser2'];
        }

        $result = array(
            'data' => $result,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus, a.f087fapproval1Status, a.f087fapproval2Status, a.f087fapproved1By, a.f087fapproved2By')
            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
            ->where('a.f087fid = :assetId')
            ->setParameter('assetId',$id);
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $asset) {
            $date                        = $asset['f087fstartDate'];
            $asset['f087fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $asset['f087ffromDate'];
            $asset['f087ffromDate'] = date("Y-m-d", strtotime($date));
            $date                        = $asset['f087ftoDate'];
            $asset['f087ftoDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $asset);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $asset = new T087fassetMovement();

        $asset->setF087fidType((int)$data['f087fidType'])
                ->setF087fassetCode((int)$data['f087fassetCode'])
                ->setF087fidReceiver((int)$data['f087fidReceiver'])
                ->setF087fapproved1By((int)$data['f087fapproved1By'])
                ->setF087fapproved2By((int)$data['f087fapproved2By'])
                ->setF087idDepartment($data['f087idDepartment'])
                ->setF087fstartDate(new \DateTime($data['f087fstartDate']))
                ->setF087ffromDate(new \DateTime($data['f087ffromDate']))
                ->setF087ftoDate(new \DateTime($data['f087ftoDate']))
                ->setF087freason($data['f087freason'])
                ->setF087fapproval1Status((int)"0")
                ->setF087fapproval2Status((int)"0")
                ->setF087fstatus((int)$data['f087fstatus'])
                ->setF087fcreatedBy((int)$_SESSION['userId'])
                ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fcreatedDtTm(new \DateTime())
                ->setF087fupdatedDtTm(new \DateTime());


        try{
            $em->persist($asset);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $asset;
    }

    public function updateData($asset, $data = []) 
    {
        $em = $this->getEntityManager();

        $asset->setF087fidType((int)$data['f087fidType'])
                ->setF087fassetCode((int)$data['f087fassetCode'])
                ->setF087fidReceiver((int)$data['f087fidReceiver'])
                ->setF087fapproved1By((int)$data['f087fapproved1By'])
                ->setF087fapproved2By((int)$data['f087fapproved2By'])
                ->setF087idDepartment($data['f087idDepartment'])
                ->setF087fstartDate(new \DateTime($data['f087fstartDate']))
                ->setF087ffromDate(new \DateTime($data['f087ffromDate']))
                ->setF087ftoDate(new \DateTime($data['f087ftoDate']))
                ->setF087freason($data['f087freason'])
                ->setF087fstatus((int)$data['f087fstatus'])
                ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fupdatedDtTm(new \DateTime());


        
        $em->persist($asset);
        $em->flush();

    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
                ->where('a.f087fapproval1Status = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
                    ->where('a.f087fapproval1Status = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid');

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function assetMovementApproval($assets,$reason,$status) 
    {
        $em = $this->getEntityManager();
        $user=(int)$_SESSION['userId'];

        foreach ($assets as $asset) 
        {


            $Id = $asset->getF087fid();
            // print_r($reason);exit;
            $id = (int)$Id;
            if(isset($reason) && isset($status)){
            $query = "UPDATE t087fasset_movement set f087freasons ='$reason',f087fapproval1_status =$status, f087fapproved1_by = $user where f087fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);
        }
        }
    }


    public function getListByApproved2Status($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
                ->where('a.f087fapproval2Status = 0')
                ->andWhere('a.f087fapproval1Status = 1');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid')
                    ->where('a.f087fapproval2Status = 1')
                    ->andWhere('a.f087fapproval1Status = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
               $qb->select('a.f087fid, a.f087fassetCode, a.f087fidType, ai.f085fassetCode, a.f087fidReceiver, a.f087idDepartment, a.f087fstartDate, a.f087ffromDate, a.f087ftoDate, a.f087freason, c.f034fname as customerName, d.f015fdepartmentName, a.f087fstatus,a.f087fapproval1Status,a.f087fapproval2Status')

            ->from('Application\Entity\T087fassetMovement', 'a')
            ->leftjoin('Application\Entity\T015fdepartment','d','with', 'a.f087idDepartment= d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fstaffMaster','c','with', 'a.f087fidReceiver= c.f034fstaffId')
            ->leftjoin('Application\Entity\T085fassetInformation','ai','with', 'a.f087fassetCode= ai.f085fid');

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function assetMovementApproval2($assets,$reason,$status) 
    {
        $em = $this->getEntityManager();
        $user=(int)$_SESSION['userId'];

        foreach ($assets as $asset) 
        {

            $Id = $asset->getF087fid();
            $id = (int)$Id;
            if(isset($reason) && isset($status))
            {
            $query = "UPDATE t087fasset_movement set f087freasons='$reason',f087fapproval2_status=$status, f087fapproved2_by = $user where f087fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);
            }
        }
    }

    public function assetNotInMovement()
    {

        $em = $this->getEntityManager();

        $query = "SELECT *,f085fasset_code as f085fassetCode from t085fasset_information where f085fid not in (select f087fasset_code from t087fasset_movement) and f085fstatus = 1";
        
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }


}
