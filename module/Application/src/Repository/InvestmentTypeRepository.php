<?php
namespace Application\Repository;

use Application\Entity\T038finvestmentType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class InvestmentTypeRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("distinct(p.f038fid) as f038fid, p.f038ftype, p.f038fstatus, p.f038faccountCode, ac.f059fcompleteCode+ ' - ' + ac.f059fname as accountName, p.f038fpersonName, p.f038fcontactNumber, (p.f038ffundCode+'-'+f.f057fname) as  fundName, (p.f038fdepartmentCode+'-'+d.f015fdepartmentName) as departmentName, (p.f038factivityCode+'-'+t.f058fname) as activityName,  p.f038ffundCode, p.f038fdepartmentCode, p.f038factivityCode") 
            ->from('Application\Entity\T038finvestmentType','p')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'p.f038faccountCode = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'p.f038ffundCode = f.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'd.f015fdepartmentCode = p.f038fdepartmentCode')
            ->leftjoin('Application\Entity\T058factivityCode', 't', 'with', 't.f058fcompleteCode = p.f038factivityCode')
            ->orderBy('p.f038fid','DESC');
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("p.f038fid, p.f038ftype, p.f038fstatus, p.f038faccountCode, p.f038fpersonName, p.f038fcontactNumber, (p.f038ffundCode+'-'+f.f057fname) as  fundName, (p.f038fdepartmentCode+'-'+d.f015fdepartmentName) as departmentName, (p.f038factivityCode+'-'+t.f058fname) as activityName, p.f038ffundCode, p.f038fdepartmentCode, p.f038factivityCode")
            ->from('Application\Entity\T038finvestmentType','p')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'p.f038faccountCode = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'p.f038ffundCode = f.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'd.f015fdepartmentCode = p.f038fdepartmentCode')
            ->leftjoin('Application\Entity\T058factivityCode', 't', 'with', 't.f058fcompleteCode = p.f038factivityCode')
            ->where('p.f038fid = :premiseId')
            ->setParameter('premiseId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $type = new T038finvestmentType();

        $type->setF038ftype($data['f038ftype'])
             ->setF038faccountCode($data['f038faccountCode'])
             ->setF038fpersonName($data['f038fpersonName'])
             ->setF038ffundCode($data['f038ffundCode'])
             ->setF038fdepartmentCode($data['f038fdepartmentCode'])
             ->setF038factivityCode($data['f038factivityCode'])
             ->setF038fcontactNumber($data['f038fcontactNumber'])
             ->setF038fstatus($data['f038fstatus'])
             ->setF038fcreatedBy((int)$_SESSION['userId'])
             ->setF038fupdatedBy((int)$_SESSION['userId']);

        $type->setF038fcreatedDtTm(new \DateTime())
                ->setF038fupdatedDtTm(new \DateTime());
               

        $em->persist($type);
        
        $em->flush();


        return $type;

    }

    /* to edit the data in database*/

    public function updateData($type, $data = []) 
    {
        $em = $this->getEntityManager();

        $type->setF038ftype($data['f038ftype'])
             ->setF038faccountCode($data['f038faccountCode'])
             ->setF038ffundCode($data['f038ffundCode'])
             ->setF038fdepartmentCode($data['f038fdepartmentCode'])
             ->setF038factivityCode($data['f038factivityCode'])
             ->setF038fpersonName($data['f038fpersonName'])
             ->setF038fcontactNumber($data['f038fcontactNumber'])
             ->setF038fstatus($data['f038fstatus'])
             ->setF038fupdatedBy((int)$_SESSION['userId']);

        $type->setF038fupdatedDtTm(new \DateTime());

        
        $em->persist($type);
        
        $em->flush();
 

    }


    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("p.f038fid, p.f038ftype, p.f038fstatus, p.f038faccountCode, ac.f059fcompleteCode+ ' - ' + ac.f059fname as accountName, p.f038fpersonName, p.f038fcontactNumber, p.f038ffundCode, p.f038fdepartmentCode, p.f038factivityCode, (p.f038ffundCode+'-'+f.f057fname) as  fundName, (p.f038fdepartmentCode+'-'+d.f015fdepartmentName) as departmentName, (p.f038factivityCode+'-'+t.f058fname) as activityName") 
            ->from('Application\Entity\T038finvestmentType','p')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'p.f038faccountCode = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'p.f038ffundCode = f.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'd.f015fdepartmentCode = p.f038fdepartmentCode')
            ->leftjoin('Application\Entity\T058factivityCode', 't', 'with', 't.f058fcompleteCode = p.f038factivityCode')
            ->where ('p.f038fstatus=1')
            ->orderBy('accountName','ASC'); 
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }


    
}

?>