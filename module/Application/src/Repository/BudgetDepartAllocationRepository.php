<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetDepartAllocationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();
        
        // Query
    
        $qb = $em->createQueryBuilder();

        $qb->select('b.f011fid as f011fid,b.f011fmenuName as f011fmenuName,a.f011fid as Parent_id, a.f011fmenuName as f011fidParent, b.f011forder,a.f011fstatus,a.f011flink')
           ->from('Application\Entity\T011fmenu', 'a')
           ->leftjoin('Application\Entity\T011fmenu', 'b','with', 'a.f011fid=b.f011fidParent');

        $query = $qb->getQuery();


        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;

        
    }
    
    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('b.f011fid as f011fid,b.f011fmenuName as f011fmenuName,a.f011fid as f011fidParent, a.f011fmenuName as parent_name, b.f011forder,a.f011fstatus,a.f011flink')
           ->from('Application\Entity\T011fmenu', 'a')
           ->leftjoin('Application\Entity\T011fmenu', 'b','with', 'a.f011fid=b.f011fidParent')
           ->where('b.f011fid = :menuId')
           ->setParameter('menuId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $menu = new T011fmenu();

        $menu->setF011fmenuName($data['f011fmenuName'])
             ->setF011fidParent($data['f011fidParent'])  
             ->setF011forder($data['f011forder'])
             ->setF011fstatus($data['f011fstatus'])
             ->setF011flink($data['f011flink'])
             ->setF011fcreatedBy((int)$_SESSION['userId'])
             ->setF011fupdatedBy((int)$_SESSION['userId']);

        $menu->setF011fcreateDtTm(new \DateTime())
                ->setF011fupdateDtTm(new \DateTime());

 
        $em->persist($menu);
        $em->flush();

        return $menu;

    }

    /* to edit the data in database*/

    public function updateData($menu, $data = []) 
    {
        $em = $this->getEntityManager();

        $menu->setF011fmenuName($data['f011fmenuName'])
             ->setF011fidParent($data['f011fidParent'])  
             ->setF011forder($data['f011forder'])
             ->setF011fstatus($data['f011fstatus'])
             ->setF011flink($data['f011flink'])
             ->setF011fcreatedBy((int)$_SESSION['userId'])
             ->setF011fupdatedBy((int)$_SESSION['userId']);
                

        $menu->setF011fUpdateDtTm(new \DateTime());
        
        $em->persist($menu);
        $em->flush();

    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>
