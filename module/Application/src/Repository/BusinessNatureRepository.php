<?php
namespace Application\Repository;

use Application\Entity\T059fbusinessNature;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BusinessNatureRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType')
            ->from('Application\Entity\T059fbusinessNature','a')
            ->where('a.f059flevelStatus=2')
            ->orderBy('a.f059fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

     public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode, a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType')

           ->from('Application\Entity\T059fbusinessNature','a')
            ->where('a.f059fid = :businessId')
            ->setParameter('businessId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    
    /* to retrive the data from database using id*/

    public function getListByRef($refCode) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType')

           ->from('Application\Entity\T059fbusinessNature','a')
           ->where('a.f059frefCode = :refCode')
           ->setParameter('refCode',$refCode);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }
   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $business = new T059fbusinessNature();

        $business->setF059fcode($data['f059fcode'])
             ->setF059fname($data['f059fname'])  
             ->setF059ftype($data['f059ftype'])
             ->setF059fdescription($data['f059fdescription'])
             ->setF059fshortCode($data['f059fshortCode'])
             ->setF059fparentCode($data['f059fparentCode'])
             ->setF059frefCode($data['f059frefCode'])
             ->setF059fstatus($data['f059fstatus'])
             ->setF059flevelStatus($data['f059flevelStatus'])
             ->setF059fcompleteCode($data['f059fcompleteCode'])
             ->setF059fallowNegative($data['f059fallowNegative'])
             ->setF059fcategory($data['f059fcategory'])
             ->setF059fbalanceType($data['f059fbalanceType'])
             ->setF059fcreatedBy((int)$_SESSION['userId'])
             ->setF059fupdatedBy((int)$_SESSION['userId']);

        $business->setF059fcreatedDtTm(new \DateTime())
                ->setF059fupdatedDtTm(new \DateTime());

 
        $em->persist($business);
        $em->flush();

        return $business;

    }

    /* to edit the data in database*/

    public function updateData($business, $data = []) 
    {
        $em = $this->getEntityManager();

        $business->setF059fcode($data['f059fcode'])
             ->setF059fname($data['f059fname'])  
             ->setF059ftype($data['f059ftype'])
             ->setF059fdescription($data['f059fdescription'])
             ->setF059fshortCode($data['f059fshortCode'])
             ->setF059fparentCode($data['f059fparentCode'])
             ->setF059frefCode($data['f059frefCode'])
             ->setF059fstatus($data['f059fstatus'])
             ->setF059flevelStatus($data['f059flevelStatus'])
             ->setF059fcompleteCode($data['f059fcompleteCode'])
             ->setF059fallowNegative((int)$data['f059fallowNegative'])
             ->setF059fcategory($data['f059fcategory'])
             ->setF059fbalanceType($data['f059fbalanceType'])
             ->setF059fcreatedBy((int)$_SESSION['userId'])
             ->setF059fupdatedBy((int)$_SESSION['userId']);

        $business->setF059fupdatedDtTm(new \DateTime());
        
        $em->persist($business);
        $em->flush();

    }
    public function getListByLevel($level) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode,a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType')
           ->from('Application\Entity\T059fbusinessNature','a')
           ->where('a.f059flevelStatus = :level')
           ->setParameter('level',$level);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getListByParentAndLevel($parent,$level)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f059fid, a.f059fcode, a.f059fname, a.f059ftype, a.f059fdescription, a.f059fshortCode, a.f059fparentCode, a.f059fstatus, a.f059frefCode, a.f059flevelStatus, a.f059fcompleteCode, a.f059fallowNegative, a.f059fcategory, a.f059fbalanceType')

           ->from('Application\Entity\T059fbusinessNature','a')
           ->where('a.f059flevelStatus = :level')
           ->setParameter('level',$level)
           ->andWhere('a.f059fparentCode = :parent')
           ->setParameter('parent',$parent);
            

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }



    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function updateAccount($business) 
    {
        $em = $this->getEntityManager();

        $business->setF059fstatus((int)"0")
                 ->setF059fupdatedDtTm(new \DateTime());

        $em->persist($business);
        $em->flush();
    }

    public function accountCodeStatus($id) 
    {
        $em = $this->getEntityManager();

        
        $query1 = "UPDATE t059fbusiness_nature set f059fstatus='1' where f059fid='$id'"; 
        $result=$em->getConnection()->executeQuery($query1);
        

    }
}

?>