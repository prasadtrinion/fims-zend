<?php
namespace Application\Repository;

use Application\Entity\T120ffeeType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class FeeTypeRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f.f120fid, f.f120fname, f.f120fcode, f.f120fidCategory, f.f120fstatus, fc.f065fname, fc.f065fcode, fc.f065fdescription')
            ->from('Application\Entity\T120ffeeType', 'f')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'fc', 'with','fc.f065fid = f.f120fidCategory')
            ->orderBy('f.f120fid','DESC');
            // print_r("dsf");exit;
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }
    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f.f120fid, f.f120fname, f.f120fcode, f.f120fidCategory, f.f120fstatus, fc.f065fname, fc.f065fcode, fc.f065fdescription')
            ->from('Application\Entity\T120ffeeType', 'f')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'fc', 'with','fc.f065fid = f.f120fidCategory')
            ->where('f.f120fid = :feeTypeId')
            ->setParameter('feeTypeId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $feeType = new T120ffeeType();

        $feeType->setF120fname($data['f120fname'])
                ->setF120fcode($data['f120fcode'])
                ->setF120fidCategory((int)$data['f120fidCategory'])
                ->setF120fstatus((int)$data['f120fstatus'])
                ->setF120fcreatedBy((int)$_SESSION['userId']);

        $feeType->setF120fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($feeType);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $feeType;
    }
    public function updateData($feeType, $data = []) 
    {
        $em = $this->getEntityManager();

        $feeType->setF120fname($data['f120fname'])
                ->setF120fcode($data['f120fcode'])
                ->setF120fidCategory((int)$data['f120fidCategory'])
                ->setF120fstatus((int)$data['f120fstatus'])
                ->setF120fupdatedBy((int)$_SESSION['userId']);

        $feeType->setF120fupdatedDtTm(new \DateTime());


        $em->persist($feeType);
        $em->flush();
    }

    public function getFeeTypeByStatus($id) 
    {
        $id = (int)$id;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f.f120fid, f.f120fname, f.f120fcode, f.f120fidCategory, f.f120fstatus, fc.f065fname, fc.f065fcode, fc.f065fdescription')
            ->from('Application\Entity\T120ffeeType', 'f')
            ->leftjoin('Application\Entity\T065ffeeCategory', 'fc', 'with','fc.f065fid = f.f120fidCategory')
            ->where('f.f120fstatus = :status')
            ->setParameter('status',$id)
            ->orderBy('f.f120fid','DESC');
            // print_r("dsf");exit;
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }
}






