<?php
namespace Application\Repository;

use Application\Entity\T092feaForm;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class EaformRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('ea.f092fid, ea.f092fyear, f.f015fname as financialYear, ea.f092freleasingDate, ea.f092fstatus ')
            ->from('Application\Entity\T092feaForm', 'ea')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'ea.f092fyear = f.f015fid')
            ->orderBy('ea.f092fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f092freleasingDate'];
            $item['f092freleasingDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('ea.f092fid, ea.f092fyear, f.f015fname as financialYear, ea.f092freleasingDate, ea.f092fstatus ')
            ->from('Application\Entity\T092feaForm', 'ea')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'ea.f092fyear = f.f015fid')
            ->where('ea.f092fid = :eaId')
            ->setParameter('eaId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $item) {
            $date                       = $item['f092freleasingDate'];
            $item['f092freleasingDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;

       
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $eaFrom = new T092feaForm();
        $eaFrom->setF092fyear($data['f092fyear'])
                ->setF092freleasingDate(new \DateTime($data['f092freleasingDate']))
                ->setF092fstatus((int)$data['f092fstatus'])
                ->setF092fcreatedBy((int)$_SESSION['userId'])
                ->setF092fupdatedBy((int)$_SESSION['userId']);

        $eaFrom->setF092fcreatedDtTm(new \DateTime())
                ->setF092fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($eaFrom);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $eaFrom;
    }

    public function updateData($eaFrom, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $eaFrom->setF092fyear($data['f092fyear'])
                ->setF092freleasingDate(new \DateTime($data['f092freleasingDate']))
                ->setF092fstatus((int)$data['f092fstatus'])
                ->setF092fcreatedBy((int)$_SESSION['userId'])
                ->setF092fupdatedBy((int)$_SESSION['userId']);

        $eaFrom->setF092fcreatedDtTm(new \DateTime())
                ->setF092fupdatedDtTm(new \DateTime());

        $em->persist($eaFrom);
        $em->flush();
    }
}