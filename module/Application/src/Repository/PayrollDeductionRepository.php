<?php
namespace Application\Repository;

use Application\Entity\T064fpayrollDeduction;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PayrollDeductionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f064fid, d.f064freferenceNumber, d.f064fdescription, d.f064flimit, d.f064fcombineLimit, d.f064fstatus')
            ->from('Application\Entity\T064fpayrollDeduction','d');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('d.f064fid, d.f064freferenceNumber, d.f064fdescription, d. f064flimit, d.f064fcombineLimit, d.f064fstatus')
            ->from('Application\Entity\T064fpayrollDeduction','d')
            ->where('d.f064fid = :deductionId')
            ->setParameter('deductionId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $deduction = new T064fpayrollDeduction();

        $deduction->setF064freferenceNumber($data['f064freferenceNumber'])
             ->setF064fdescription($data['f064fdescription'])
             ->setF064flimit($data['f064flimit'])
             ->setF064fcombineLimit($data['f064fcombineLimit'])
             ->setF064fstatus((int)$data['f064fstatus'])
             ->setF064fcreatedBy((int)$_SESSION['userId'])
             ->setF064fupdatedBy((int)$_SESSION['userId']);

        $deduction->setF064fcreatedDtTm(new \DateTime())
              ->setF064fupdatedDtTm(new \DateTime());

        $em->persist($deduction);
        $em->flush();
        
        return $deduction;

    }

    /* to edit the data in database*/

    public function updateData($deduction, $data = []) 
    {
        $em = $this->getEntityManager();

        $deduction->setF064freferenceNumber($data['f064freferenceNumber'])
             ->setF064fdescription($data['f064fdescription'])
             ->setF064flimit($data['f064flimit'])
             ->setF064fcombineLimit($data['f064fcombineLimit'])
             ->setF064fstatus((int)$data['f064fstatus'])
             ->setF064fcreatedBy((int)$_SESSION['userId'])
             ->setF064fupdatedBy((int)$_SESSION['userId']);

             $deduction->setF064fupdatedDtTm(new \DateTime());
 
        $em->persist($deduction);
        $em->flush();
        
        return $deduction;

    }


}

?>