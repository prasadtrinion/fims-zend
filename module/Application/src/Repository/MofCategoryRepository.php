<?php
namespace Application\Repository;

use Application\Entity\T114fmofCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class MofCategoryRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select('se.f114fid, se.f114fname, se.f114fdescription, se.f114fstatus')
            ->from('Application\Entity\T114fmofCategory', 'se');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('se.f114fid, se.f114fname, se.f114fdescription, se.f114fstatus')
            ->from('Application\Entity\T114fmofCategory', 'se')
            ->where('se.f114fid = :mofId')
            ->setParameter('mofId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $mofCategory = new T114fmofCategory();

        $mofCategory->setF114fname($data['f114fname'])
                ->setF114fdescription($data['f114fdescription'])
                ->setF114fstatus((int)$data['f114fstatus'])
                ->setF114fcreatedBy((int)$_SESSION['userId'])
                ->setF114fupdatedBy((int)$_SESSION['userId']);

        $mofCategory->setF114fcreatedDtTm(new \DateTime())
                ->setF114fupdatedDtTm(new \DateTime());
          
        $em->persist($mofCategory);
        
        $em->flush();
        
        return $mofCategory;
    }

    public function updateData($mofCategory, $data = []) 
    {
        $em = $this->getEntityManager();

        $mofCategory->setF114fname($data['f114fname'])
                ->setF114fdescription($data['f114fdescription'])
                ->setF114fstatus((int)$data['f114fstatus'])
                ->setF114fcreatedBy((int)$_SESSION['userId'])
                ->setF114fupdatedBy((int)$_SESSION['userId']);

        $mofCategory->setF114fcreatedDtTm(new \DateTime())
                ->setF114fupdatedDtTm(new \DateTime());
        
        $em->persist($mofCategory);
        $em->flush();
    }  
}