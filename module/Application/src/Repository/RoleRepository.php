<?php
namespace Application\Repository;

use Application\Entity\T021frole;
use Application\Entity\T011fmenu;
use Application\Entity\T014fuser;
use Application\Entity\T022froleDetails;
use Application\Entity\T023fuserRoles;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class RoleRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f021fid, r.f021froleName, r.f021fstatus')
            ->from('Application\Entity\T021frole','r')
            ->orderby('r.f021fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/
    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f021fid,r.f021froleName, r.f021fstatus')
            ->from('Application\Entity\T021frole','r')
             ->where('r.f021fid = :roleId')
            ->setParameter('roleId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $role = new T021frole();

        $role->setF021froleName($data['f021froleName'])
             ->setF021fstatus($data['f021fstatus'])
             ->setF021fcreatedBy((int)$_SESSION['userId'])
             ->setF021fupdatedBy((int)$_SESSION['userId']);

        $role->setF021fcreatedDtTm(new \DateTime())
                ->setF021fupdatedDtTm(new \DateTime());

 
        $em->persist($role);
        $em->flush();

        return $role;

    }

     public function createUser($data) 
    {
        $em = $this->getEntityManager();
        $token = md5(uniqid(time()));
        $email = explode("@", $data['f014femail']);
        $user = new T014fuser();

        $user->setF014fuserName($data['f014fuserName'])
             ->setF014femail($data['f014femail'])
             ->setF014ftoken($token)
             ->setF014fpassword(md5($data['f014fpassword']))
             ->setF014fidStaff((int)$data['f014fidStaff'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fcreatedBy((int)$_SESSION['userId'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $user->setF014fcreatedDtTm(new \DateTime())
                ->setF014fupdatedDtTm(new \DateTime());
try{
    $em->persist($user);
        $em->flush();
}
catch(\Exception $e){
    echo $e;
}
$userId = $user->getF014fid();
        $userRoles = $data['f014fidRole'];
        foreach ($userRoles as $userRole) {
       
        $userRoleObj = new T023fuserRoles();

            $userRoleObj->setF023fidRole((int)$userRole)
             ->setF023fidUser((int)$userId)
             ->setF023fstatus((int)"0")
             ->setF023fcreatedBy((int)$_SESSION['userId'])
             ->setF023fupdatedBy((int)$_SESSION['userId']);

        $userRoleObj->setF023fcreatedDtTm(new \DateTime())
                ->setF023fupdatedDtTm(new \DateTime());     
                try{
    $em->persist($userRoleObj);
        $em->flush();
}
catch(\Exception $e){
    echo $e;
}       
        }
 
        

        return $user;

    }
    public function updateUser($user,$data) 
    {
        $em = $this->getEntityManager();
      
        $user->setF014fuserName($data['f014fuserName'])
             ->setF014femail($data['f014femail'])
             ->setF014ftoken($token)
            //  ->setF014fpassword(md5($data['f014fpassword']))
             ->setF014fidStaff((int)$data['f014fidStaff'])
             ->setF014fstatus((int)$data['f014fstatus'])
             ->setF014fupdatedBy((int)$_SESSION['userId']);

        $user->setF014fupdatedDtTm(new \DateTime());
try{
    $em->persist($user);
        $em->flush();
}
catch(\Exception $e){
    echo $e;
}
$userId = $user->getF014fid();
        
        $userRoles = $data['f014fidRole'];
        $update_query = "delete from t023fuser_roles where f023fid_user = $userId";
        $em->getConnection()->executeQuery($update_query);
        foreach ($userRoles as $userRole) {
       
        $userRoleObj = new T023fuserRoles();

            $userRoleObj->setF023fidRole((int)$userRole)
             ->setF023fidUser((int)$userId)
             ->setF023fstatus((int)"0")
             ->setF023fcreatedBy((int)$_SESSION['userId'])
             ->setF023fupdatedBy((int)$_SESSION['userId']);

        $userRoleObj->setF023fcreatedDtTm(new \DateTime())
                ->setF023fupdatedDtTm(new \DateTime());     
                try{
    $em->persist($userRoleObj);
        $em->flush();
        // print_r($userRoleObj);
        // die();
    
    }
catch(\Exception $e){
    echo $e;
}       
        }
 
        

        return $user;

    }

    public function updateData($role, $data = []) 
    {
        $em = $this->getEntityManager();

        $role->setF021froleName($data['f021froleName'])
             ->setF021fstatus($data['f021fstatus'])
             ->setF021fcreatedBy((int)$_SESSION['userId'])
             ->setF021fupdatedBy((int)$_SESSION['userId']);

        $role->setF021fupdatedDtTm(new \DateTime());
        
        $em->persist($role);
        $em->flush();

    }
    public function getMenuById($token,$module)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select('DISTINCT(m.f011fmenuName) as f011fmenuName,m.f011flink,m.f011forder')
            ->from('Application\Entity\T011fmenu','m')
            ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
            ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
            ->join('Application\Entity\T023fuserRoles','ur','with', 'ur.f023fidRole = r.f021fid')
            ->join('Application\Entity\T014fuser','u','with', 'ur.f023fidUser = u.f014fid')
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->AndWhere('m.f011fmodule= :module')
            ->AndWhere('m.f011ftype= 1')
            ->orderby('m.f011forder')
            ->setParameter('module', $module)
            ->setParameter('token', $token);
        $query = $qb->getQuery();

        $setup_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();
        $qb->select('DISTINCT(m.f011fmenuName) as f011fmenuName,m.f011flink,m.f011forder')
            ->from('Application\Entity\T011fmenu','m')
            ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
            ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
            ->join('Application\Entity\T023fuserRoles','ur','with', 'ur.f023fidRole = r.f021fid')
            ->join('Application\Entity\T014fuser','u','with', 'ur.f023fidUser = u.f014fid')
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->AndWhere('m.f011fmodule= :module')
            ->AndWhere('m.f011ftype= 2')
            ->orderby('m.f011forder')
            ->setParameter('module', $module)
            ->setParameter('token', $token);
        $query = $qb->getQuery();

        $transaction_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();
        $qb->select('DISTINCT(m.f011fmenuName) as f011fmenuName,m.f011flink,m.f011forder')
            ->from('Application\Entity\T011fmenu','m')
            ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
            ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
            ->join('Application\Entity\T023fuserRoles','ur','with', 'ur.f023fidRole = r.f021fid')
            ->join('Application\Entity\T014fuser','u','with', 'ur.f023fidUser = u.f014fid')
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->AndWhere('m.f011fmodule= :module')
            ->AndWhere('m.f011ftype= 3')
            ->orderby('m.f011forder')
            ->setParameter('module', $module)
            ->setParameter('token', $token);
        $query = $qb->getQuery();

        $report_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $return_result = array();
        $return_result['setup'] = $setup_result;
        $return_result['transaction'] = $transaction_result;
        $return_result['report'] = $report_result;
        
        return $return_result;
    }
    public function getMenu1ById($token,$module)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select('m.f011fmenuName as mainmenu,m.f011flink as url,m.f011fid as menuid,m.f011forder')
            ->from('Application\Entity\T011fmenu','m')
            ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
            ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
            ->join('Application\Entity\T014fuser','u','with', 'u.f014fidRole = r.f021fid')
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->AndWhere('m.f011fmodule= :module')
            ->AndWhere('m.f011fidParent = 0')
            ->orderby('m.f011forder')
            ->setParameter('module', $module)
            ->setParameter('token', $token);
        
        $query = $qb->getQuery();
        $result = $query->getArrayResult();
        for($i=0;$i<count($result);$i++){
        $qb = $em->createQueryBuilder();
        $qb->select('m.f011fmenuName as name,m.f011flink as url')
            ->from('Application\Entity\T011fmenu','m')
            ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
            ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
            ->join('Application\Entity\T014fuser','u','with', 'u.f014fidRole = r.f021fid')
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->AndWhere('m.f011fmodule= :module')
            ->AndWhere('m.f011fidParent = :parent')
            ->orderby('m.f011forder')
            ->setParameter('module', $module)
            ->setParameter('parent', $result[$i]['menuid'])
            ->setParameter('token', $token);
        // echo$qb;
        // die();
        $query = $qb->getQuery();

        $sub_result = $query->getArrayResult();
        $result[$i]['submenu'] = $sub_result;
    }

        return $result;
    }
    public function getModuleById($token)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
//m.f011fidm.f011fmenuName,m.f011flink
        $qb->select('DISTINCT(m.f011fmodule) as f011fmodule')
        ->from('Application\Entity\T011fmenu','m')
        ->join('Application\Entity\T022froleDetails','d','with', 'm.f011fid = d.f022fidMenu')
        ->join('Application\Entity\T021frole','r','with', 'd.f022fidRole = r.f021fid')
        ->join('Application\Entity\T023fuserRoles','ur','with', 'ur.f023fidRole = r.f021fid')
        ->join('Application\Entity\T014fuser','u','with', 'ur.f023fidUser = u.f014fid')
        
            // ->from('Application\Entity\T014fuser','u')
            // 
            //  ->join('Application\Entity\T022froleDetails','d','with', 'r.f021fid = d.f022fidRole')
            //  ->join(,'with', 'd.f011fidMenu = m.f011fid');
            
            ->where('u.f014ftoken= :token')
            ->Andwhere('m.f011fstatus= 1')
            ->setParameter('token', $token);
        // echo$qb;
        // die();
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getUserById($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('u.f014fuserName')
            ->from('Application\Entity\T014fuser','u')
            ->join('Application\Entity\T023fuserRoles','ur','with', 'u.f014fid = ur.f023fidUser')
            ->where('ur.f023fidUser= :userId')
            ->setParameter('userId', $id);
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f021fid, r.f021froleName, r.f021fstatus')
            ->from('Application\Entity\T021frole','r')
            ->orderBy('r.f021froleName', 'ASC')
            ->where('r.f021fstatus= 1');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }




    
    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }


    public function createUserRole($data)
    {
        // print_r($data);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        foreach ($data['idUser'] as $userId)
        {
            // print_r($userId);exit;
       
            $userRoleObj = new T023fuserRoles();

            $userRoleObj->setF023fidRole((int)$data['idRole'])
             ->setF023fidUser((int)$userId)
             ->setF023fstatus((int)1)
             ->setF023fcreatedBy((int)$_SESSION['userId'])
             ->setF023fupdatedBy((int)$_SESSION['userId']);

            $userRoleObj->setF023fcreatedDtTm(new \DateTime())
                ->setF023fupdatedDtTm(new \DateTime());     

                $em->persist($userRoleObj);
                $em->flush();
        }    
    }
}

?>