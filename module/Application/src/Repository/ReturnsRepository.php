<?php
namespace Application\Repository;

use Application\Entity\T118freturns;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class ReturnsRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select("r.f118fid, r.f118famount, r.f118famountNumber,  r.f118freceiptNumber, r.f118fstudent,s.f060fname ,(s.f060ficNumber+ '-' +s.f060fname) as student,
         r.f118fapprovalStatus, r.f118fstatus")
            ->from('Application\Entity\T118freturns', 'r')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'r.f118fstudent = s.f060fid  ')
            ->orderBy('r.f118fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f118fid, r.f118famount, r.f118famountNumber,  r.f118freceiptNumber, r.f118fstudent,s.f060fname as student, r.f118fapprovalStatus, r.f118fstatus')
            ->from('Application\Entity\T118freturns', 'r')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 's.f060fid = r.f118fstudent')
            ->where('r.f118fid = :returnsId')
            ->setParameter('returnsId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $returns = new T118freturns();

        $returns->setF118famount((float)$data['f118famount'])
                ->setF118famountNumber($data['f118famountNumber'])
                ->setF118freceiptNumber($data['f118freceiptNumber'])
                ->setF118fstudent((int)$data['f118fstudent'])
                ->setF118fapprovalStatus((int)0)
                ->setF118fstatus((int)$data['f118fstatus'])

                ->setF118fcreatedBy((int)$_SESSION['userId'])
                ->setF118fupdatedBy((int)$_SESSION['userId']);

        $returns->setF118fcreatedDtTm(new \DateTime())
                ->setF118fupdatedDtTm(new \DateTime());
          

        
        try{
            $em->persist($returns);
           $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
       
        return $returns;
    }

    public function updateData($returns, $data = []) 
    {
        $em = $this->getEntityManager();

        $returns->setF118famount((float)$data['f118famount'])
                ->setF118famountNumber($data['f118famountNumber'])
                ->setF118freceiptNumber($data['f118freceiptNumber'])
                ->setF118fstudent((int)$data['f118fstudent'])
                ->setF118fapprovalStatus((int)0)
                ->setF118fstatus((int)$data['f118fstatus'])

                ->setF118fcreatedBy((int)$_SESSION['userId'])
                ->setF118fupdatedBy((int)$_SESSION['userId']);

        $returns->setF118fcreatedDtTm(new \DateTime())
                ->setF118fupdatedDtTm(new \DateTime());
        
        $em->persist($returns);
        $em->flush();
    } 

    public function returnsByApproved($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select("r.f118fid, r.f118famount, r.f118famountNumber,  r.f118freceiptNumber, r.f118fstudent,s.f060fname as student, r.f118fapprovalStatus, r.f118fstatus, (s.f060ficNumber+'-'+s.f060fname) as ICstudent")
            ->from('Application\Entity\T118freturns', 'r')
             ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'r.f118fstudent = s.f060fid  ')
             ->where('r.f118fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select("r.f118fid, r.f118famount, r.f118famountNumber,  r.f118freceiptNumber, r.f118fstudent,s.f060fname as student, r.f118fapprovalStatus, r.f118fstatus, (s.f060ficNumber+'-'+s.f060fname) as ICstudent")
            ->from('Application\Entity\T118freturns', 'r')
             ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'r.f118fstudent = s.f060fid  ')
            ->where('r.f118fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) 
            {
              $qb->select("r.f118fid, r.f118famount, r.f118famountNumber,  r.f118freceiptNumber, r.f118fstudent,s.f060fname as student, r.f118fapprovalStatus, r.f118fstatus, (s.f060ficNumber+'-'+s.f060fname) as ICstudent")
            ->from('Application\Entity\T118freturns', 'r')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'r.f118fstudent = s.f060fid  ');

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }
    public function approveReturns($postData)
    {
        $em = $this->getEntityManager();
        // print_r($postData);exit;
        foreach ($postData['id'] as $id)
        {
            $status = $postData['status'];
            $user = (int)$_SESSION['userId'];

            $query = "update t118freturns set f118fapproval_status = $status, f118fupdated_by = $user where f118fid = $id";

            $em->getConnection()->executeQuery($query);
            
        }
        
    }

}