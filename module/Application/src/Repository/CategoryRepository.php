<?php
namespace Application\Repository;

use Application\Entity\T073fcategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CategoryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("c.f073fid, c.f073fcategoryName, c.f073fcode, c.f073fstatus, c.f073fcreatedBy, c.f073fupdatedBy, c.f073frevenueCode, c.f073ftaxCode, c.f073fprefixCode,(tc.f059fcompleteCode+'-'+tc.f059fname) as Tax, (c.f073fcode+'-'+c.f073fcategoryName) as revenueName ")
            ->from('Application\Entity\T073fcategory','c')
                ->leftjoin('Application\Entity\T059faccountCode', 'tc', 'with','tc.f059fid = c.f073ftaxCode')
            ->orderBy('c.f073fid','DESC');
// echo $qb;
// die();
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f073fid, c.f073fcategoryName, c.f073fcode, c.f073fstatus, c.f073fcreatedBy, c.f073fupdatedBy, c.f073frevenueCode, c.f073ftaxCode, c.f073fprefixCode')
            ->from('Application\Entity\T073fcategory','c')
            ->where('c.f073fid = :categoryId')
            ->setParameter('categoryId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="REVENUE";
        $revenueCode = $configRepository->generateFIMS($numberData);

        $category = new T073fcategory();

        $category->setF073fcategoryName($data['f073fcategoryName'])
             ->setF073fcode($data['f073fcode'])
             ->setF073frevenueCode($revenueCode)
             ->setF073ftaxCode((int)$data['f073ftaxCode'])
             ->setF073fprefixCode($data['f073fprefixCode'])
             ->setF073fstatus((int)$data['f073fstatus'])
             ->setF073fcreatedBy((int)$_SESSION['userId'])
             ->setF073fupdatedBy((int)$_SESSION['userId']);

        $category->setF073fcreatedDtTm(new \DateTime())
                ->setF073fupdatedDtTm(new \DateTime());

 try{
        $em->persist($category);
        $em->flush();
        // print_r($category);
        // die();
        
}
catch(\Exception $e){
    echo $e;
}
        return $category;

    }

    /* to edit the data in database*/

    public function updateData($category, $data = []) 
    {
        $em = $this->getEntityManager();

        $category->setF073fcategoryName($data['f073fcategoryName'])
             ->setF073fcode($data['f073fcode'])
             ->setF073ftaxCode((int)$data['f073ftaxCode'])
             ->setF073fprefixCode($data['f073fprefixCode'])
             ->setF073fstatus((int)$data['f073fstatus'])
             ->setF073fupdatedBy((int)$_SESSION['userId']);

        $category->setF073fupdatedDtTm(new \DateTime());

        $em->persist($category);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
        public function getActiveList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select("c.f073fid, c.f073fcategoryName, c.f073fcode, c.f073fstatus, c.f073fcreatedBy, c.f073fupdatedBy,c.f073frevenueCode, c.f073ftaxCode, c.f073fprefixCode, (c.f073fcode+' - '+c.f073fcategoryName) as revenueName ")
            ->from('Application\Entity\T073fcategory','c')
            ->orderBy('c.f073fid','DESC')
            ->where('c.f073fstatus = 1');


        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    }
}

?>