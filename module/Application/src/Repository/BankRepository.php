<?php
namespace Application\Repository;

use Application\Entity\T042fbank;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BankRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f042fid, b.f042fbankName, b.f042fbranchName, b.f042faccountNumber, b.f042faccountType, b.f042fswiftCode, b.f042fbrsCode, b.f042fstatus, b.f042fcreatedBy, b.f042fupdatedBy')
            ->from('Application\Entity\T042fbank','b')
            ->orderBy('b.f042fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('b.f042fid, b.f042fbankName, b.f042fbranchName, b.f042faccountNumber, b.f042faccountType, b.f042fswiftCode, b.f042fbrsCode, b.f042fstatus, b.f042fcreatedBy, b.f042fupdatedBy')
            ->from('Application\Entity\T042fbank','b')
            ->where('b.f042fid = :bankId')
            ->setParameter('bankId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $data['f042fbranchName']  = "Branch";
        $data['f042faccountNumber'] = "1231323";
        $data['f042faccountType'] = "ACT";
        // $data['f042fswiftCode'] = "SWFT";
        $data['f042fbrsCode'] = "BRS";
        $em = $this->getEntityManager();

        $bank = new T042fbank();

        $bank->setF042fbankName($data['f042fbankName'])
             ->setF042fbranchName($data['f042fbranchName'])
             ->setF042faccountNumber($data['f042faccountNumber'])
             ->setF042faccountType($data['f042faccountType'])
             ->setF042fswiftCode($data['f042fswiftCode'])
             ->setF042fbrsCode($data['f042fbrsCode'])
             ->setF042fstatus((int)$data['f042fstatus'])
             ->setF042fcreatedBy((int)$_SESSION['userId'])
             ->setF042fupdatedBy((int)$_SESSION['userId']);

        $bank->setF042fcreatedDtTm(new \DateTime())
                ->setF042fupdatedDtTm(new \DateTime());

        
        $em->persist($bank);
        $em->flush();
        
        return $bank;

    }

    /* to edit the data in database*/

    public function updateData($bank, $data = []) 
    {
        $em = $this->getEntityManager();

        $bank->setF042fbankName($data['f042fbankName'])
             ->setF042fbranchName($data['f042fbranchName'])
             ->setF042faccountNumber($data['f042faccountNumber'])
             ->setF042faccountType($data['f042faccountType'])
             ->setF042fswiftCode($data['f042fswiftCode'])
             ->setF042fbrsCode($data['f042fbrsCode'])
             ->setF042fstatus((int)$data['f042fstatus'])
             ->setF042fcreatedBy((int)$_SESSION['userId'])
             ->setF042fupdatedBy((int)$_SESSION['userId']);
                

        $bank->setF042fupdatedDtTm(new \DateTime());
        
        $em->persist($bank);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('b.f042fid, b.f042fbankName, b.f042fbranchName, b.f042faccountNumber, b.f042faccountType, b.f042fswiftCode, b.f042fbrsCode, b.f042fstatus, b.f042fcreatedBy, b.f042fupdatedBy')
            ->from('Application\Entity\T042fbank','b')
            ->orderBy('b.f042fbankName', 'ASC')
            ->where('b.f042fstatus=1');


        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>