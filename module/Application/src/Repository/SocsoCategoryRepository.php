<?php
namespace Application\Repository;

use Application\Entity\T091socsoCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SocsoCategoryRepository extends EntityRepository 
{

    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     


        $qb->select('s.f091fid, s.f091fcategoryName, s.f091fdescription,  s.f091fstatus')
            ->from('Application\Entity\T091socsoCategory', 's')
             ->orderBy('s.f091fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('s.f091fid, s.f091fcategoryName, s.f091fdescription,  s.f091fstatus')
            ->from('Application\Entity\T091socsoCategory', 's')
            ->where('s.f091fid = :socsoId')
            ->setParameter('socsoId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $socso = new T091socsoCategory();

        $socso->setF091fcategoryName($data['f091fcategoryName'])
                ->setF091fdescription($data['f091fdescription'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $socso->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());
          
        $em->persist($socso);
        
        $em->flush();
        
        return $socso;
    }

    public function updateData($socso, $data = []) 
    {
        $em = $this->getEntityManager();

        $socso->setF091fcategoryName($data['f091fcategoryName'])
                ->setF091fdescription($data['f091fdescription'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $socso->setF091fupdatedDtTm(new \DateTime());
        
        $em->persist($socso);
        $em->flush();
    }  
}