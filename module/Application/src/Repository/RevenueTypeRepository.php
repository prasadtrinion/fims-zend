<?php
namespace Application\Repository;

use Application\Entity\T132frevenueType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class RevenueTypeRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("rt.f132fid, rt.f132frevenueName, rt.f132ftypeCode, rt.f132fidCategory, rt.f132fstatus, (c.f073fcode+'-'+c.f073fcategoryName) as revenueName")
            ->from('Application\Entity\T132frevenueType', 'rt')
            ->leftjoin('Application\Entity\T073fcategory','c','with','rt.f132fidCategory = c.f073fid')
            ->orderBy('rt.f132fid','DESC');
            

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("rt.f132fid, rt.f132frevenueName, rt.f132ftypeCode, rt.f132fidCategory, rt.f132fstatus, (c.f073fcode+'-'+c.f073fcategoryName) as revenueName")
            ->from('Application\Entity\T132frevenueType', 'rt')
            ->leftjoin('Application\Entity\T073fcategory','c','with','rt.f132fidCategory = c.f073fid')
            ->where('rt.f132fid = :blockId')
            ->setParameter('blockId',$id);
             
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
       
     	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $revenueType = new T132frevenueType();

        $revenueType->setF132frevenueName($data['f132frevenueName'])
        ->setF132ftypeCode($data['f132ftypeCode'])
        ->setF132fidCategory((int)$data['f132fidCategory'])
        ->setF132fstatus((int)$data['f132fstatus'])
        ->setF132fcreatedBy((int)$_SESSION['userId']);


        $revenueType->setF132fcreatedDtTm(new \DateTime());
     
            $em->persist($revenueType);
            $em->flush();


        	return $revenueType;
    }

    public function updateData($revenueType, $data = []) 
    {
        $em = $this->getEntityManager();

        $revenueType->setF132frevenueName($data['f132frevenueName'])
        ->setF132ftypeCode($data['f132ftypeCode'])
        ->setF132fidCategory((int)$data['f132fidCategory'])
        ->setF132fstatus((int)$data['f132fstatus'])
        ->setF132fupdatedBy((int)$_SESSION['userId']);

        $revenueType->setF132fupdatedDtTm(new \DateTime());

        $em->persist($revenueType);
        $em->flush();
    }

    public function getRevenueTypeActive($id)
    {

        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
         $qb->select("rt.f132fid, rt.f132frevenueName, rt.f132ftypeCode, rt.f132fidCategory, rt.f132fstatus, (c.f073fcode+'-'+c.f073fcategoryName) as revenueName")
            ->from('Application\Entity\T132frevenueType', 'rt')
            ->leftjoin('Application\Entity\T073fcategory','c','with','rt.f132fidCategory = c.f073fid')
            ->where('rt.f132fstatus = :blockId')
            ->setParameter('blockId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
}
