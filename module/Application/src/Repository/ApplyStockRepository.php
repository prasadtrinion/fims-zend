<?php
namespace Application\Repository;

use Application\Entity\T099fapplyStock;
use Application\Entity\T099fapplyStockDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ApplyStockRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("a.f099fid, a.f099fapplicantName, a.f099fposition, a.f099fdepartment, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as departmentName, a.f099frequiredDate, a.f099fstatus, a.f099fapprovalStatus, s.f034fname as applicantName")
            ->from('Application\Entity\T099fapplyStock','a')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','a.f099fdepartment = d.f015fdepartmentCode')
             ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','a.f099fapplicantName = s.f034fstaffId')
            ->orderBy('a.f099fid','DESC');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
                $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f099frequiredDate'];
            $item['f099frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("a.f099fid, a.f099fapplicantName, a.f099fposition, a.f099fdepartment, a.f099frequiredDate, a.f099fstatus, ad.f099fidDetails,a.f099fapprovalStatus, ad.f099fkodStock, ad.f099fidCategory, ad.f099fidSubCategory, ad.f099fidAssetType, ad.f099fidItem, ad.f099fcurrentStock, ad.f099frequestedQty, ad.f099fstatus, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as departmentName")
            ->from('Application\Entity\T099fapplyStock','a')
            ->leftjoin('Application\Entity\T099fapplyStockDetails', 'ad', 'with','a.f099fid = ad.f099fidApplyStock')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','a.f099fdepartment = d.f015fdepartmentCode')
            ->where('a.f099fid = :stockId')
            ->setParameter('stockId',$id);
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f099frequiredDate'];
            $item['f099frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );

        return $result1;
    }

    public function createStock($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $stock = new T099fapplyStock();

        $stock->setF099fapplicantName((int)$data['f099fapplicantName'])
             ->setF099fposition($data['f099fposition'])
             ->setF099fdepartment($data['f099fdepartment'])
             ->setF099frequiredDate(new \DateTime($data['f099frequiredDate']))
             ->setF099fstatus((int)$data['f099fstatus'])
             ->setF099fapprovalStatus((int)0)
             ->setF099freason('NULL')
             ->setF099fcreatedBy((int)$_SESSION['userId'])
             ->setF099fupdatedBy((int)$_SESSION['userId']);

        $stock->setF099fcreatedDtTm(new \DateTime())
                ->setF099fupdatedDtTm(new \DateTime());
       
        $em->persist($stock);

        $em->flush();

        
        $stockDetails = $data['stock-details'];

        foreach ($stockDetails as $stockDetail) {

            $stockDetailObj = new T099fapplyStockDetails();

            $stockDetailObj->setF099fidApplyStock((int)$stock->getF099fid())
                       ->setF099fkodStock($stockDetail['f099fkodStock'])
                       ->setF099fidCategory((int)$stockDetail['f099fidCategory'])
                       ->setF099fidSubCategory((int)$stockDetail['f099fidSubCategory'])
                       ->setF099fidAssetType((int)$stockDetail['f099fidAssetType'])
                       ->setF099fidItem((int)$stockDetail['f099fidItem'])
                       ->setF099fcurrentStock($stockDetail['f099fcurrentStock'])
                       ->setF099frequestedQty($stockDetail['f099frequestedQty'])
                       ->setF099fstatus((int)0)
                       ->setF099fcreatedBy((int)$_SESSION['userId'])
                       ->setF099fupdatedBy((int)$_SESSION['userId']);

            $stockDetailObj->setF099fcreatedDtTm(new \DateTime())
                       ->setF099fupdatedDtTm(new \DateTime());
        

        $em->persist($stockDetailObj);

        $em->flush();
        // print_r($stockDetailObj);exit;

            
        }
        return $stock;
    }

   public function updateStock($applyStock, $data) 
    {
        $em = $this->getEntityManager();

        $applyStock->setF099fapplicantName((int)$data['f099fapplicantName'])
             ->setF099fposition($data['f099fposition'])
             ->setF099fdepartment($data['f099fdepartment'])
             ->setF099frequiredDate(new \DateTime($data['f099frequiredDate']))
             ->setF099fstatus((int)$data['f099fstatus'])
             ->setF099fcreatedBy((int)$_SESSION['userId'])
             ->setF099fupdatedBy((int)$_SESSION['userId']);

        $applyStock->setF099fupdatedDtTm(new \DateTime());
        
        $em->persist($applyStock);
        $em->flush();

        return $applyStock;
    }


    public function updateStockDetail($stockDetailObj, $stockDetail)
    {

        $em = $this->getEntityManager();

        $stockDetailObj->setF099fkodStock($stockDetail['f099fkodStock'])
                       ->setF099fidCategory((int)$stockDetail['f099fidCategory'])
                       ->setF099fidSubCategory((int)$stockDetail['f099fidSubCategory'])
                       ->setF099fidAssetType((int)$stockDetail['f099fidAssetType'])
                       ->setF099fidItem((int)$stockDetail['f099fidItem'])
                       ->setF099fcurrentStock($stockDetail['f099fcurrentStock'])
                       ->setF099frequestedQty($stockDetail['f099frequestedQty'])
                       ->setF099fstatus((int)$stockDetail['f099fstatus'])
                       ->setF099fupdatedBy((int)$_SESSION['userId']);

        $stockDetailObj->setF099fupdatedDtTm(new \DateTime());

        $em->persist($stockDetailObj);

        $em->flush();
    }
    
    public function createStockDetail($applyStockObj, $stockDetail)
    {
        $em = $this->getEntityManager();

        $stockDetailObj = new T099fapplyStockDetails();

        $stockDetailObj->setF099fidApplyStock($applyStockObj->getF099fid())
                       ->setF099fkodStock($stockDetail['f099fkodStock'])
                       ->setF099fidCategory($stockDetail['f099fidCategory'])
                       ->setF099fidSubCategory($stockDetail['f099fidSubCategory'])
                       ->setF099fidAssetType($stockDetail['f099fidAssetType'])
                       ->setF099fidItem($stockDetail['f099fidItem'])
                       ->setF099fcurrentStock($stockDetail['f099fcurrentStock'])
                       ->setF099frequestedQty($stockDetail['f099frequestedQty'])
                       ->setF099fstatus($stockDetail['f099fstatus'])
                       ->setF099fcreatedBy((int)$_SESSION['userId'])
                       ->setF099fupdatedBy((int)$_SESSION['userId']);

        $stockDetailObj->setF099fcreatedDtTm(new \DateTime())
                       ->setF099fupdatedDtTm(new \DateTime());

        $em->persist($stockDetailObj);

        $em->flush();

        return $stockDetailObj;
        
    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
         $qb->select("a.f099fid, a.f099fapplicantName, a.f099fposition, a.f099fdepartment, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as departmentName, a.f099frequiredDate, a.f099fstatus, a.f099fapprovalStatus, s.f034fname as applicantName")
            ->from('Application\Entity\T099fapplyStock','a')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','a.f099fdepartment = d.f015fdepartmentCode')
             ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','a.f099fapplicantName = s.f034fstaffId');

        if($approvedStatus == 0)
        {
            $qb->where('a.f099fapprovalStatus = 0');
        }
        elseif($approvedStatus == 1)
        {
            $qb->where('a.f099fapprovalStatus = 1');
        }
        elseif ($approvedStatus == 2) 
        {}
        
        $qb->orderBy('a.f099fid','DESC');
        
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function applyStockApproval($stocks, $status, $reason) 
    {
        $em = $this->getEntityManager();

        foreach ($stocks as $stock) 
        {

          $Id = $stock->getF099fid();
          $id=(int)$Id;

          $query = "UPDATE t099fapply_stock set f099freason='$reason',f099fapproval_status=$status where f099fid = $id"; 
        
          $result=$em->getConnection()->executeQuery($query);
            
        }
    }

    public function stockDistribution($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f099fid, a.f099fapplicantName, a.f099fposition, a.f099fdepartment, a.f099frequiredDate, a.f099fstatus, ad.f099fidDetails, ad.f099fkodStock, ad.f099fidCategory, ad.f099fidSubCategory, ad.f099fidAssetType, ad.f099fidItem, ad.f099fcurrentStock, ad.f099frequestedQty, ad.f099fstatus')
            ->from('Application\Entity\T099fapplyStock','a')
            ->leftjoin('Application\Entity\T099fapplyStockDetails', 'ad', 'with','a.f099fid = ad.f099fidApplyStock')
            ->where('a.f099fid = :stockId')
            ->setParameter('stockId',$id);
        $query = $qb->getQuery();
        $results = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        foreach($results as $result)
        {
          // print_r($result);

          $Id = $result['f099fid'];
          $id = (int)$Id;

          $DetailId = $result['f099fidDetails'];
          $detailId = (int)$DetailId;

          $currentStock = $result['f099fcurrentStock'];
          $requestedStock = $result['f099frequestedQty'];
          
          $stock = $currentStock - $requestedStock;
          

          $query1 = "UPDATE t099fapply_stock_details set f099fcurrent_stock='$stock', f099fstatus=1 where f099fid_details = $detailId"; 
          $result1=$em->getConnection()->executeQuery($query1);

          $query2 = "UPDATE t099fapply_stock set f099fapproval_status=3 where f099fid = $id"; 
          $result=$em->getConnection()->executeQuery($query2);


        }
  
    }

    
    public function deleteApplyStockDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t099fapply_stock_details WHERE f099fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
    
}

?> 
