<?php
namespace Application\Repository;

use Application\Entity\T045fapplyLoan;
use Application\Entity\T045fapplyLoanDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ApplyLoanRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('l.f045fid, l.f045fidName, ln.f020floanName as name, l.f045fdescription, l.f045ffile, l.f045fnoOfMonths, l.f045fidStaff, s.f034fname as staff, l.f045fguarantee, l.f045fstatus')
            ->from('Application\Entity\T045fapplyLoan','l')
            ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','l.f045fidName = ln.f020fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f045fidStaff = s.f034fid')
            ->orderBy('l.f045fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('l.f045fid, l.f045fidName, ln.f020floanName, l.f045fdescription, l.f045ffile, l.f045fnoOfMonths, l.f045fidStaff, s.f034fname as staff, l.f045fguarantee, l.f045fstatus, ld.f045fidDetails, ld.f045fname, ld.f045faccountCode, ld.f045fdeduction, ld.f045fpercentage')
            ->from('Application\Entity\T045fapplyLoan','l')
            ->leftjoin('Application\Entity\T045fapplyLoanDetails', 'ld', 'with','l.f045fid = ld.f045fidApplyLoan')
            ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','l.f045fidName = ln.f020fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f045fidStaff = s.f034fid')
            ->where('l.f045fid = :loanId')
            ->setParameter('loanId',$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createLoan($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $loan = new T045fapplyLoan();

        $loan->setF045fidName((int)$data['f045fidName'])
             ->setF045fdescription($data['f045fdescription'])
             ->setF045ffile($data['f045ffile'])
             ->setF045fnoOfMonths((int)$data['f045fnoOfMonths'])
             ->setF045fidStaff((int)$data['f045fidStaff'])
             ->setF045fguarantee((int)$data['f045fguarantee'])
             ->setF045fstatus((int)$data['f045fstatus'])
             ->setF045fapprovalStatus((int)"0")
             ->setF045fcreatedBy((int)$_SESSION['userId'])
             ->setF045fupdatedBy((int)$_SESSION['userId']);

        $loan->setF045fcreatedDtTm(new \DateTime())
                ->setF045fupdatedDtTm(new \DateTime());
        
        $em->persist($loan);
        $em->flush();
        
        $loanDetails = $data['loan-details'];

        foreach ($loanDetails as $loanDetail) {

            $loanDetailObj = new T045fapplyLoanDetails();

            $loanDetailObj->setF045fidApplyLoan((int)$loan->getF045fid())
                       ->setF045fname($loanDetail['f045fname'])
                       ->setF045faccountCode($loanDetail['f045faccountCode'])
                       ->setF045fdeduction((float)$loanDetail['f045fdeduction'])
                       ->setF045fpercentage((float)$loanDetail['f045fpercentage'])
                       ->setF045fstatus((int)$loanDetail['f045fstatus'])
                       ->setF045fcreatedBy((int)$_SESSION['userId'])
                       ->setF045fupdatedBy((int)$_SESSION['userId']);

            $loanDetailObj->setF045fcreatedDtTm(new \DateTime())
                       ->setF045fupdatedDtTm(new \DateTime());
        

        $em->persist($loanDetailObj);

        $em->flush();

            
        }
        return $loan;
    }

   public function updateLoan($loan, $data = []) 
    {
        $em = $this->getEntityManager();

        $loan->setF045fidName((int)$data['f045fidName'])
             ->setF045fdescription($data['f045fdescription'])
             ->setF045ffile($data['f045ffile'])
             ->setF045fnoOfMonths((int)$data['f045fnoOfMonths'])
             ->setF045fidStaff((int)$data['f045fidStaff'])
             ->setF045fguarantee((int)$data['f045fguarantee'])
             ->setF045fstatus((int)$data['f045fstatus'])
             ->setF045fapprovalStatus((int)"0")
             ->setF045fupdatedBy((int)$_SESSION['userId']);

        $loan->setF045fupdatedDtTm(new \DateTime());
        
        $em->persist($loan);
        $em->flush();

        return $loan;
    }


    public function updateLoanDetail($loanDetailObj, $loanDetail)
    {

        $em = $this->getEntityManager();


        $loanDetailObj->setF045fname($loanDetail['f045fname'])
                       ->setF045faccountCode($loanDetail['f045faccountCode'])
                       ->setF045fdeduction((float)$loanDetail['f045fdeduction'])
                       ->setF045fpercentage((float)$loanDetail['f045fpercentage'])
                       ->setF045fstatus((int)$loanDetail['f045fstatus'])
                       ->setF045fupdatedBy((int)$_SESSION['userId']);


        $loanDetailObj->setF045fupdatedDtTm(new \DateTime());

        $em->persist($loanDetailObj);

        $em->flush();
    }
    
    public function createLoanDetail($loanObj, $loanDetail)
    {
        $em = $this->getEntityManager();

        $loanDetailsObj = new T045fapplyLoanDetails();

        $loanDetailsObj->setF045fidApplyLoan((int)$loanObj->getF045fid())
                       ->setF045fname($loanDetail['f045fname'])
                       ->setF045faccountCode($loanDetail['f045faccountCode'])
                       ->setF045fdeduction((float)$loanDetail['f045fdeduction'])
                       ->setF045fpercentage((float)$loanDetail['f045fpercentage'])
                       ->setF045fstatus((int)$loanDetail['f045fstatus'])
                       ->setF045fcreatedBy((int)$_SESSION['userId'])
                       ->setF045fupdatedBy((int)$_SESSION['userId']);

        $loanDetailsObj->setF045fcreatedDtTm(new \DateTime())
                       ->setF045fupdatedDtTm(new \DateTime());

        $em->persist($loanDetailsObj);

        $em->flush();

        return $loanDetailsObj;
        
    }

    public function getLoanApprovedList($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select('l.f045fid, l.f045fidName, c.f021ffirstName as name, l.f045fdescription, l.f045ffile, l.f045fnoOfMonths, l.f045fidStaff, s.f034fname as staff, l.f045fguarantee, l.f045fstatus')
            ->from('Application\Entity\T045fapplyLoan','l')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','l.f045fidName = c.f021fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f045fidStaff = s.f034fid')
            ->where('l.f045fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
            $qb->select('l.f045fid, l.f045fidName, c.f021ffirstName as name, l.f045fdescription, l.f045ffile, l.f045fnoOfMonths, l.f045fidStaff, s.f034fname as staff, l.f045fguarantee, l.f045fstatus')
            ->from('Application\Entity\T045fapplyLoan','l')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','l.f045fidName = c.f021fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f045fidStaff = s.f034fid')
            ->where('l.f045fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
            $qb->select('l.f045fid, l.f045fidName, c.f021ffirstName as name, l.f045fdescription, l.f045ffile, l.f045fnoOfMonths, l.f045fidStaff, s.f034fname as staff, l.f045fguarantee, l.f045fstatus')
            ->from('Application\Entity\T045fapplyLoan','l')
            ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with','l.f045fidName = c.f021fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','l.f045fidStaff = s.f034fid');
                
            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function loanApproval($loans) 
    {
        $em = $this->getEntityManager();

        foreach ($loans as $loan) 
        {

            $loan->setF045fapprovalStatus((int)"1")
                 ->setF045fUpdatedDtTm(new \DateTime());
       
            $em->persist($loan);
            $em->flush();
        }
    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>