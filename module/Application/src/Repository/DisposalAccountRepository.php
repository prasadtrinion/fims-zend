<?php
namespace Application\Repository;

use Application\Entity\T033fdisposalAccount;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DisposalAccountRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("d.f033fid, d.f033fcode, d.f033faccountCode,   d.f033ftype, sd.f086fdisposalType as type,  d.f033fstatus, (a.f059fcompleteCode+'-'+a.f059fname) as accountCode")
            ->from('Application\Entity\T033fdisposalAccount','d')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'd.f033faccountCode = a.f059fcompleteCode')
            ->leftjoin('Application\Entity\T086fsetupDisposal', 'sd', 'with', 'sd.f086fid = d.f033ftype')
            ->orderBy('d.f033fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f033fid, d.f033fcode, d.f033faccountCode, a.f059fname as accountCode,  d.f033ftype, sd.f086fdisposalType as type,  d.f033fstatus')
            ->from('Application\Entity\T033fdisposalAccount','d')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'd.f033faccountCode = a.f059fcompleteCode')
            ->leftjoin('Application\Entity\T086fsetupDisposal', 'sd', 'with', 'sd.f086fid = d.f033ftype')
             ->where('d.f033fid = :disposalId')
            ->setParameter('disposalId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $disposalAccount = new T033fdisposalAccount();

        $disposalAccount->setF033fcode($data['f033fcode'])
             ->setF033faccountCode($data['f033faccountCode'])
             ->setF033ftype((int)$data['f033ftype'])
             ->setF033fstatus((int)$data['f033fstatus'])
             ->setF033fcreatedBy((int)$_SESSION['userId'])
             ->setF033fupdatedBy((int)$_SESSION['userId']);

        $disposalAccount->setF033fcreatedDtTm(new \DateTime())
                ->setF033fupdatedDtTm(new \DateTime());

        try{
        $em->persist($disposalAccount);
        $em->flush();
        
    }
    catch(\Exception $e){
        echo $e;
    }
        return $disposalAccount;

    }

    public function updateData($disposalAccount, $data = []) 
    {
        $em = $this->getEntityManager();

       $disposalAccount->setF033fcode($data['f033fcode'])
             ->setF033faccountCode((int)$data['f033faccountCode'])
             ->setF033ftype((int)$data['f033ftype'])
             ->setF033fstatus((int)$data['f033fstatus'])
             ->setF033fupdatedBy((int)$_SESSION['userId']);

        $disposalAccount->setF033fupdatedDtTm(new \DateTime());
        
        $em->persist($disposalAccount);
        $em->flush();

    }

}