<?php
namespace Application\Repository;

use Application\Entity\T016faccountSetting;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AccountSettingRepository extends EntityRepository 
{

    public function getList() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f016fid, a.f016fsubAccUnderLedger, a.f016fenableTds, a.f016fallowChequePrinting, a.f016fbudgetYn, a.f016fmqyBudgeting, a.f016fstatus, IDENTITY(a.f016fupdatedBy)')
            ->from('Application\Entity\T016faccountSetting', 'a');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a.f016fid, a.f016fsubAccUnderLedger, a.f016fenableTds, a.f016fallowChequePrinting, a.f016fbudgetYn, a.f016fmqyBudgeting, a.f016fstatus, IDENTITY(a.f016fupdatedBy)')
            ->from('Application\Entity\T016faccountSetting', 'a')
            ->where('a.f016fid = :accountSettingId')
            ->setParameter('accountSettingId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $accountSetting = new T016faccountSetting();

        $accountSetting->setF016fsubAccUnderLedger($data['f016fsubAccUnderLedger'])
                       ->setF016fenableTds($data['f016fenableTds'])
                       ->setF016fallowChequePrinting($data['f016fallowChequePrinting'])
                       ->setF016fbudgetYn($data['f016fbudgetYn'])
                       ->setF016fmqyBudgeting($data['f016fmqyBudgeting'])
                       ->setF016fstatus($data['f016fstatus'])
                       ->setF016fupdatedBy((int)$_SESSION['userId']);

        $accountSetting->setF016fupdatedDtTm(new \DateTime());


        
        $em->persist($accountSetting);
        $em->flush();

        return $accountSetting;
    }

    public function updateData($accountSetting, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $accountSetting->setF016fsubAccUnderLedger($data['f016fsubAccUnderLedger'])
                       ->setF016fenableTds($data['f016fenableTds'])
                       ->setF016fallowChequePrinting($data['f016fallowChequePrinting'])
                       ->setF016fbudgetYn($data['f016fbudgetYn'])
                       ->setF016fmqyBudgeting($data['f016fmqyBudgeting'])
                       ->setF016fstatus($data['f016fstatus'])
                       ->setF016fupdatedBy((int)$_SESSION['userId']);
        $accountSetting->setF016fupdatedDtTm(new \DateTime());


        
        $em->persist($accountSetting);
        $em->flush();

    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

}