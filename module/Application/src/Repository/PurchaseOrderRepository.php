<?php
namespace Application\Repository;

use Application\Entity\T034fpurchaseOrder;
use Application\Entity\T034fpurchaseOrderDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PurchaseOrderRepository extends EntityRepository  
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier ,v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm, p.f034fprintDate, p.f034frePrintDate")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f034fidSupplier = v.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where("p.f034forderType != 'Warrant'")
            ->andWhere("p.f034forderType = 'PO'")
            ->orderBy('p.f034fid','DESC');

        $query = $qb->getQuery();
	$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	$result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
	        $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fprintDate'];
            $item['f034fprintDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frePrintDate'];
            $item['f034frePrintDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function getWarrantyPos() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier ,v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm,p.f034fapprovalStatus")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f034fidSupplier = v.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where("p.f034forderType = 'Warrant'")
            ->orderBy('p.f034fid','DESC');

        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function purchaseOrderWarrantiesApproval() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier ,v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm,p.f034fapprovalStatus")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f034fidSupplier = v.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where('p.f034fapprovalStatus=1')
            ->orderBy('p.f034fid','DESC');

        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }



    public function getListNotIn()
    {
        $query = "select  f088fid, f088freference_number as f088freferenceNumber from t088fpurchase_requisition  where f088fapproval_status = 1 and f088forder_type !='Warrant' and f088forder_type = 'PO' and (f088fid not in (select f034fid_purchasereq from t034fpurchase_order where f034fapproval_status = 1 or f034fapproval_status = 0)) ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    public function getWarrantListNotIn()
    {
        $query = "select  f088fid, f088freference_number as f088freferenceNumber from t088fpurchase_requisition  where f088fapproval_status = 1 and f088forder_type ='Warrant' and (f088fid not in (select f034fid_purchasereq from t034fpurchase_order where f034fapproval_status = 1 or f034fapproval_status = 0)) ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    public function getPOListNotIn($data)
    {
        $type = $data['orderType'];
        // print_r($type);exit;
        $query = "select  f088fid, f088freference_number as f088freferenceNumber from t088fpurchase_requisition  where f088fapproval_status = 1 and f088forder_type ='$type' and (f088fid not in (select f034fid_purchasereq from t034fpurchase_order where f034fapproval_status = 1 or f034fapproval_status = 0)) ";

        $em = $this->getEntityManager();
        // print_r($query);exit;
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    public function supplierPurchaseOrders($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->orderBy('p.f034fid','DESC')
            ->where('p.f034fidSupplier = :supplier')
            ->setParameter('supplier',$id);

        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
        $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function getListById($id)  
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select("p.f034fid, p.f034forderType,p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount, pd.f034fidDetails, pd.f034fidItem, pd.f034fsoCode, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, p.f034fupdatedBy, u.f014fuserName as userName, p.f034fcreatedDtTm, pd.f034fitemType, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrderDetails', 'pd', 'with', 'p.f034fid = pd.f034fidPurchaseOrder')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f034fidSupplier = v.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where('p.f034fid = :purchaseId')
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // print_r($result);
        // die();
	$result1 = array();
        foreach ($result as $item) {
            $item['f034fidItem'] = $item['f034fidItem'].$item['f034fitemType'];
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }

         $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];



                 $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
                $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];

            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $id"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
       
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();


                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;
        }

        return $result1;
    }

    public function createPurchase($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $purchase = new T034fpurchaseOrder();

        $purchase->setF034forderType($data['f034forderType'])
            ->setF034forderDate(new \DateTime($data['f034forderDate']))
            ->setF034fidFinancialyear((int)$data['f034fidFinancialyear'])
            ->setF034fidSupplier((int)$data['f034fidSupplier'])
            ->setF034fdescription($data['f034fdescription'])
            ->setF034freferenceNumber($data['f034freferenceNumber'])
            ->setF034fidDepartment($data['f034fidDepartment'])
            ->setf034fidPurchasereq((int)$data['f034fidPurchaseRequisition'])
            ->setF034fexpiredDate(new \DateTime($data['f034fexpiredDate']))
            ->setF034ftotalAmount((float)$data['f034ftotalAmount'])
            ->setF034fstatus((int)$data['f034fstatus'])
            ->setF034fapprovalStatus((int)$data['f034fapprovalStatus'])
            ->setF034fcreatedBy((int)$_SESSION['userId'])
            ->setF034fupdatedBy((int)$_SESSION['userId']);

        $purchase->setF034fcreatedDtTm(new \DateTime())
            ->setF034fupdatedDtTm(new \DateTime());

   	    try{     
            $em->persist($purchase);

            $em->flush();
	}
	catch(\Exception $e){
	echo $e;
	}
        

        $purchaseDetails = $data['purchase-details'];

        foreach ($purchaseDetails as $purchaseDetail) {

            $purchaseDetailObj = new T034fpurchaseOrderDetails();

            $purchaseDetailObj->setF034fidPurchase((int)$purchase->getF034fid())
                ->setF034fidItem((int)$purchaseDetail['f034fidItem'])
                ->setF034fitemType($purchaseDetail['f034fitemType'])
                ->setF034fsoCode($purchaseDetail['f034fsoCode'])
                ->setF034frequiredDate(new \DateTime($purchaseDetail['f034frequiredDate']))
                ->setF034fquantity((int)$purchaseDetail['f034fquantity'])
                ->setF034funit($purchaseDetail['f034funit'])
                ->setF034fprice((float)$purchaseDetail['f034fprice'])
                ->setF034total((float)$purchaseDetail['f034total'])
                ->setF034fpercentage((float) $purchaseDetail['f034fpercentage'])
		        ->setF034ftaxCode((int)$purchaseDetail['f034ftaxCode'])
           	    ->setF034fstatus((int)$purchaseDetail['f034fstatus'])
	            ->setF034ftaxAmount((float) $purchaseDetail['f034ftaxAmount'])
                ->setF034ftotalIncTax((float)$purchaseDetail['f034ftotalIncTax'])
                ->setF034ffundCode($purchaseDetail['f034ffundCode'])
                ->setF034fdepartmentCode($purchaseDetail['f034fdepartmentCode'])
                ->setF034factivityCode($purchaseDetail['f034factivityCode'])
                ->setF034faccountCode($purchaseDetail['f034faccountCode'])
                ->setF034fbudgetFundCode($purchaseDetail['f034fbudgetFundCode'])
                ->setF034fbudgetDepartmentCode($purchaseDetail['f034fbudgetDepartmentCode'])
                ->setF034fbudgetActivityCode($purchaseDetail['f034fbudgetActivityCode'])
                ->setF034fbudgetAccountCode($purchaseDetail['f034fbudgetAccountCode'])
                ->setF034fitemCategory((int)$purchaseDetail['f034fitemCategory'])
                ->setF034fitemSubCategory((int)$purchaseDetail['f034fitemSubCategory'])
                ->setF034fbalanceQuantity((int)$purchaseDetail['f034fquantity'])
                ->setF034fassetCode($purchaseDetail['f034fassetCode'])
                ->setF034fcreatedBy((int)$_SESSION['userId'])
                ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $purchaseDetailObj->setF034fcreatedDtTm(new \DateTime())
                ->setF034fupdatedDtTm(new \DateTime());
         try{   
                $em->persist($purchaseDetailObj);
		
                $em->flush();
			
}
	catch(\Exception $e){
	echo $e;
	}
            
        }
        return $purchase;
    }

    public function updatePurchase($purchase, $data = [])
    {
        $em = $this->getEntityManager();
        

        $purchase->setF034forderType($data['f034forderType'])
                ->setF034forderDate(new \DateTime($data['f034forderDate']))
                ->setF034fidFinancialyear((int)$data['f034fidFinancialyear'])
                ->setF034fidSupplier((int)$data['f034fidSupplier'])
                ->setF034fdescription($data['f034fdescription'])
                ->setF034freferenceNumber($data['f034freferenceNumber'])
                ->setF034fidDepartment($data['f034fidDepartment'])
                ->setf034fidPurchasereq((int)$data['f034fidPurchaseRequisition'])
                ->setF034fexpiredDate(new \DateTime($data['f034fexpiredDate']))
                ->setF034ftotalAmount($data['f034ftotalAmount'])
                ->setF034fstatus((int)$data['f034fstatus'])
                ->setF034fupdatedBy((int)$_SESSION['userId']);

        $purchase->setF034fupdatedDtTm(new \DateTime());
            


        try {
            $em->persist($purchase);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $purchase;
    }


    public function updatePurchaseDetail($purchaseDetailObj, $purchaseDetail)
    {
        
        $em = $this->getEntityManager();
        $purchaseDetailObj->setF034fidItem((int)$purchaseDetail['f034fidItem'])
                ->setF034fitemType($purchaseDetail['f034fitemType'])
                ->setF034fsoCode($purchaseDetail['f034fsoCode'])
                ->setF034frequiredDate(new \DateTime($purchaseDetail['f034frequiredDate']))
                ->setF034fquantity((int)$purchaseDetail['f034fquantity'])
                ->setF034funit($purchaseDetail['f034funit'])
                ->setF034fprice((float)$purchaseDetail['f034fprice'])
                ->setF034total((float)$purchaseDetail['f034total'])
                ->setF034fpercentage((int) $purchaseDetail['f034fpercentage'])
                ->setF034ftaxCode((int)$purchaseDetail['f034ftaxCode'])
                ->setF034fstatus((int)$purchaseDetail['f034fstatus'])
                ->setF034ffundCode($purchaseDetail['f034ffundCode'])
                ->setF034fdepartmentCode($purchaseDetail['f034fdepartmentCode'])
                ->setF034factivityCode($purchaseDetail['f034factivityCode'])
                ->setF034faccountCode($purchaseDetail['f034faccountCode'])
                ->setF034fbudgetFundCode($purchaseDetail['f034fbudgetFundCode'])
                ->setF034fbudgetDepartmentCode($purchaseDetail['f034fbudgetDepartmentCode'])
                ->setF034fbudgetActivityCode($purchaseDetail['f034fbudgetActivityCode'])
                ->setF034fbudgetAccountCode($purchaseDetail['f034fbudgetAccountCode'])
                ->setF034ftaxAmount( $purchaseDetail['f034ftaxAmount'])
                ->setF034ftotalIncTax($purchaseDetail['f034ftotalIncTax'])
                ->setF034fitemCategory((int)$purchaseDetail['f034fitemCategory'])
                ->setF034fitemSubCategory((int)$purchaseDetail['f034fitemSubCategory'])
                ->setF034fassetCode($purchaseDetail['f034fassetCode'])
                ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $purchaseDetailObj->setF034fupdatedDtTm(new \DateTime());
            try {
                $em->persist($purchaseDetailObj);
    
                $em->flush();
            } catch (\Exception $e) {
                echo $e;
            }
    }
    
    public function createPurchaseDetail($purchase, $purchaseDetail)
    {
        $em = $this->getEntityManager();

       $purchaseDetailObj = new T034fpurchaseOrderDetails();

       $purchaseDetailObj->setF034fidPurchase((int)$purchase->getF034fid())
                ->setF034fidItem((int)$purchaseDetail['f034fidItem'])
                ->setF034fitemType($purchaseDetail['f034fitemType'])
                ->setF034fsoCode($purchaseDetail['f034fsoCode'])
                ->setF034frequiredDate(new \DateTime($purchaseDetail['f034frequiredDate']))
                ->setF034fquantity((int)$purchaseDetail['f034fquantity'])
                ->setF034funit($purchaseDetail['f034funit'])
                ->setF034fprice((float)$purchaseDetail['f034fprice'])
                ->setF034total((float)$purchaseDetail['f034total'])
                ->setF034fpercentage((int) $purchaseDetail['f034fpercentage'])
                ->setF034ftaxCode((int)$purchaseDetail['f034ftaxCode'])
                ->setF034fstatus((int)$purchaseDetail['f034fstatus'])
                ->setF034ftaxAmount((float) $purchaseDetail['f034ftaxAmount'])
                ->setF034ftotalIncTax((float) $purchaseDetail['f034ftotalIncTax'])
                ->setF034ffundCode($purchaseDetail['f034ffundCode'])
                ->setF034fdepartmentCode($purchaseDetail['f034fdepartmentCode'])
                ->setF034factivityCode($purchaseDetail['f034factivityCode'])
                ->setF034faccountCode($purchaseDetail['f034faccountCode'])
                ->setF034fbudgetFundCode($purchaseDetail['f034fbudgetFundCode'])
                ->setF034fbudgetDepartmentCode($purchaseDetail['f034fbudgetDepartmentCode'])
                ->setF034fbudgetActivityCode($purchaseDetail['f034fbudgetActivityCode'])
                ->setF034fbudgetAccountCode($purchaseDetail['f034fbudgetAccountCode'])
                ->setF034fbalanceQuantity((int)$purchaseDetail['f034fquantity'])
                ->setF034fitemCategory((int)$purchaseDetail['f034fitemCategory'])
                ->setF034fitemSubCategory((int)$purchaseDetail['f034fitemSubCategory'])
                ->setF034fassetCode($purchaseDetail['f034fassetCode'])
                ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $purchaseDetailObj->setF034fupdatedDtTm(new \DateTime());
        try
        {
            $em->persist($purchaseDetailObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }

    public function purchaseOrderApprove($purchases,$reason,$status)
    {
        $em = $this->getEntityManager();

        foreach ($purchases as $purchase) 
        {
            $Id = $purchase->getF034fid();
            $id = (int)$Id;
            // print_r($id);exit;

            $query = "UPDATE t034fpurchase_order set f034freason='$reason',f034fapproval_status=$status where f034fid = $id"; 

            $result=$em->getConnection()->executeQuery($query);


            if ($status == 2)
            {
                $querySelect = "select f034fid_purchasereq from t034fpurchase_order where f034fid = $id"; 

            $result =$em->getConnection()->executeQuery($querySelect)->fetch();

            $idReq = (int)$result['f034fid_purchasereq'];



            $queryPend = "UPDATE t088fpurchase_requisition set f088fapproval_status = 0 where f088fid = $idReq"; 

            $result=$em->getConnection()->executeQuery($queryPend);

            }

            $qb = $em->createQueryBuilder();
            $qb->select("p.f034fid,pd.f034total,pd.f034ftotalIncTax, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode,p.f034fidFinancialyear")
                ->from('Application\Entity\T034fpurchaseOrder', 'p')
                ->leftjoin('Application\Entity\T034fpurchaseOrderDetails', 'pd', 'with', 'p.f034fid = pd.f034fidPurchaseOrder')
                ->where('p.f034fid = :purchaseId')
                ->setParameter('purchaseId',$id);
            $datas = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            
            foreach($datas as $data)
            {
                  
                $fund=$data['f034fbudgetFundCode'];
                $activity=$data['f034fbudgetActivityCode'];
                $account=$data['f034fbudgetAccountCode'];
                $department=$data['f034fbudgetDepartmentCode'];
                $financialyear=(int)$data['f034fidFinancialyear'];
                 $year_query = "select f110fid from t110fbudgetyear inner join t015ffinancialyear on f110fyear = f015fname where f015fid = $financialyear"; 
            $year_result=$em->getConnection()->executeQuery($year_query)->fetch();

            $year = (int)$year_result['f110fid'];

        

                $total=$data['f034ftotalIncTax'];
                // print_r($total);exit;
                ///////////////////////////////////////////////////////////////////////////////////
        $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $year";
        $result=$em->getConnection()->executeQuery($query)->fetch();

        // print_r($result);
        // exit();
        if(!$result){
            $account=substr($account,0,2)."000"; 
            $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $year";
            $result=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);
        // exit();
            if(!$result){
                $account=substr($account,0,1)."0000"; 
                $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $year";
                $result=$em->getConnection()->executeQuery($query)->fetch();
                if(!$result){
                    $status=-1;
                }
                else{

                    $balance=$result['f108fbudget_pending'];

                    if($amount<$balance)
                    {
                        $status=1;
                    }
                    else
                    {
                        $status=0;
                    }
                }
            }
            else{

                $balance=$result['f108fbudget_pending'];

                if($amount<$balance)
                {
                    $status=1;
                }
                else
                {
                    $status=0;
                }
            }
        }
        else{

            $balance=$result['f108fbudget_pending'];

            if($amount<$balance)
            {
                $status=1;
            }
            else
            {
                $status=0;
            }
        }
/////////////////////////////////////////////////////////////////////////
        if($status == 1){
            
                $query = "UPDATE t108fbudget_summary set f108fbudget_commitment= f108fbudget_commitment + $total where f108fdepartment='$department'and f108ffund='$fund' and f108factivity='$activity' and f108faccount='$account' and f108fid_financial_year = $year"; 
                    // print_r($query);exit;
                $value=$em->getConnection()->executeQuery($query);
        }
            }
            
        }
    }

    public function purchaseOrderByApproved($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p.f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy,u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode as Supplier')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
                ->where('p.f034fapprovalStatus = 0')
                ->andWhere("p.f034forderType != 'Warrant'");
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode as Supplier')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
                ->where('p.f034fapprovalStatus = 1')
                ->andWhere("p.f034forderType != 'Warrant'");
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) 
            {
               $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode as Supplier')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where("p.f034forderType != 'Warrant'");

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function getListByOrderType($orderType) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where('p.f034fapprovalStatus=1')
            ->andWhere('p.f034forderType = :orderType')
            ->setParameter('orderType',$orderType);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
        $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

     public function purchaseOrderWarrantiesByApproved($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p.f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy,u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
                ->where("p.f034forderType = 'Warrant'")
                ->orWhere("p.f034forderType = 'Direct Purchase'")
                ->andWhere('p.f034fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
               $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy, u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode ')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
                ->where("p.f034forderType = 'Warrant'")
                ->orWhere("p.f034forderType = 'Direct Purchase'")
                ->andWhere('p.f034fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) 
            {
               $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fupdatedBy, u.f014fuserName as userName, p.f034fcreatedDtTm, s.f030fvendorCode')
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->where("p.f034forderType = 'Warrant'")
                ->orWhere("p.f034forderType = 'Direct Purchase'");

            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function PONotInGrn($data)
    {

        $em = $this->getEntityManager();

        $type = $data['orderType'];
        $query = "SELECT distinct(po.f034fid), po.f034fid_purchasereq as f034fidPurchasereq, po.f034freference_number as f034freferenceNumber from t034fpurchase_order as po  inner join t034fpurchase_order_details as pod on pod.f034fid_purchase_order = po.f034fid  where pod.f034fbalance_quantity != 0 and po.f034forder_type =  '$type' and po.f034fapproval_status = 1";
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();

        // print_r($result);exit;
        

        // $result1 = array();
        // foreach ($result as $item) {
        //     $date                        = $item['f034forder_date'];
        //     $item['f034forder_date'] = date("Y-m-d", strtotime($date));
        //     $date                        = $item['f034fcreated_dt_tm'];
        //     $item['f034fcreated_dt_tm'] = date("Y-m-d", strtotime($date));
        //     $date                        = $item['f034fupdated_dt_tm'];
        //     $item['f034fupdated_dt_tm'] = date("Y-m-d", strtotime($date));
        //     array_push($result1, $item);
        // }
        // print_r($result1);exit;

        return $result;
    }

    public function getPurchaseOrderWarrant() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier ,v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fupdatedBy
            ,u.f014fuserName as userName, p.f034fcreatedDtTm")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T088fpurchaseRequisition', 'pr', 'with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'p.f034fidSupplier = v.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->orderBy('p.f034fid','DESC');

        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }


    public function deletePurchaseOrderDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t034fpurchase_order_details WHERE f034fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

}
