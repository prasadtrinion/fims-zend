<?php
namespace Application\Repository;

use Application\Entity\T088fitem;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class ItemRepository extends EntityRepository 
{
     public function allItems() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $query = "select cast(f088fid as varchar(10))+'A' as item_id,f088fitem_description as item_name from t088fitem union select cast(f029fid as varchar(10))+'P' as item_id,f029fitem_name as item_name from t029fitem_set_up"; 

            $result1=$em->getConnection()->executeQuery($query)->fetchAll();
     $result = array(
            'data' => $result1
        );
    return $result;
    
    }
     public function allCategories() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $query = "select f027fid as category_id,cast(f027fid as varchar(30))+'P' as category_code,f027fcategory_name as category_name,f027fmaintenance as maintenanceCode from t027fitem_category"; 

            $result1=$em->getConnection()->executeQuery($query)->fetchAll();
     $result = array(
            'data' => $result1
        );
    return $result;
    
    }

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // $qb->select('i.f088fid, i.f088fidItemGroup, i.f088fidCategory, i.f088fidSubCategory, i.f088fidType, i.f088fitemCode, i.f088fitemDescription, i.f088fstatus, s.f028fsubCategoryName as itemGroup, c.f089fsubcode as assetSubCategory, 
        //     ac.f087fcategoryCode as CategoryName')
        //     ->from('Application\Entity\T088fitem', 'i')
        //     ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'i.f088fidItemGroup = s.f028fid')
        //     ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'i.f088fidCategory = ac.f087fid')
        //     ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'i.f088fidSubCategory = c.f089fid');

        $qb->select('i.f088fid, i.f088fidItemGroup, i.f088fidCategory, i.f088fidSubCategory, i.f088fidType, i.f088fitemCode, i.f088fitemDescription, i.f088fstatus,i.f088famount, s.f028fsubCategoryName as itemGroup, c.f089fsubDescription,ac.f087fcategoryCode, c.f089fsubcode as assetSubCategory, at.f090ftypeCode as assetType,at.f090ftypeDescription, ac.f087fcategoryDiscription as categoryName')
            ->from('Application\Entity\T088fitem', 'i')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'i.f088fidItemGroup = s.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'i.f088fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories','c', 'with', 'i.f088fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T090fassetType','at', 'with', 'i.f088fidType = at.f090fid')
            ->orderBy('i.f088fid','DESC');

            // ->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'r.f088fidGlcode = g.f014fid');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
          $qb->select('i.f088fid, i.f088fidItemGroup, i.f088fidCategory, i.f088fidSubCategory, i.f088fidType, i.f088fitemCode, i.f088fitemDescription, i.f088fstatus, s.f028fsubCategoryName as itemGroup, ac.f087fcategoryCode, c.f089fsubcode as assetSubCategory, at.f090ftypeCode as assetType, ac.f087fcategoryDiscription as categoryName,i.f088famount')
            ->from('Application\Entity\T088fitem', 'i')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'i.f088fidItemGroup = s.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'i.f088fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories','c', 'with', 'i.f088fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T090fassetType','at', 'with', 'i.f088fidType = at.f090fid')
            // ->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'r.f088fidGlcode = g.f014fid')
            ->where('i.f088fid = :itemId')
            ->setParameter('itemId',(int)$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $item = new T088fitem();

        $item->setF088fidItemGroup((int)$data['f088fidItemGroup'])
                ->setF088fidCategory((int)$data['f088fidCategory'])
                ->setF088fidSubCategory((int)$data['f088fidSubCategory'])
                ->setF088fidType((int)$data['f088fidType'])
                ->setF088famount((float)$data['f088famount'])
                ->setF088fitemCode($data['f088fitemCode'])
                ->setF088fitemDescription($data['f088fitemDescription'])
                ->setF088fstatus((int)$data['f088fstatus'])
                ->setF088fcreatedBy((int)$_SESSION['userId'])
                ->setF088fupdatedBy((int)$_SESSION['userId']);

        $item->setF088fcreatedDtTm(new \DateTime())
                ->setF088fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($item);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $item;
    }

    public function updateData($item, $data = []) 
    {
        $em = $this->getEntityManager();

       $item->setF088fidItemGroup((int)$data['f088fidItemGroup'])
                ->setF088fidCategory((int)$data['f088fidCategory'])
                ->setF088fidSubCategory((int)$data['f088fidSubCategory'])
                ->setF088fidType((int)$data['f088fidType'])
                ->setF088famount((float)$data['f088famount'])
                ->setF088fitemCode($data['f088fitemCode'])
                ->setF088fitemDescription($data['f088fitemDescription'])
                ->setF088fstatus((int)$data['f088fstatus'])
                ->setF088fupdatedBy((int)$_SESSION['userId']);

        $item->setF088fupdatedDtTm(new \DateTime());


        
        $em->persist($item);
        $em->flush();

    }

    public function getActiveList()
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

    

        $qb->select('i.f088fid, i.f088fidItemGroup, i.f088fidCategory, i.f088fidSubCategory, i.f088fidType, i.f088fitemCode, i.f088fitemDescription, i.f088fstatus,i.f088famount, s.f028fsubCategoryName as itemGroup, ac.f087fcategoryCode as categoryName, c.f089fsubcode as assetSubCategory, at.f090ftypeCode as assetType')
            ->from('Application\Entity\T088fitem', 'i')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 's', 'with', 'i.f088fidItemGroup = s.f028fid')
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'i.f088fidCategory = ac.f087fid')
            ->leftjoin('Application\Entity\T089fassetSubCategories','c', 'with', 'i.f088fidSubCategory = c.f089fid')
            ->leftjoin('Application\Entity\T090fassetType','at', 'with', 'i.f088fidType = at.f090fid')
            ->where('i.f088fstatus=1')
            ->orderBy('i.f088fitemCode','ASC');

            // ->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'r.f088fidGlcode = g.f014fid');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }
     public function assetAmountIncrease($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query = "select f034fid_item,f034ftotal_inc_tax from t034fgrn_details where f034fid_details = $id"; 
            $item_result=$em->getConnection()->executeQuery($query)->fetch();
            $amount = $item_result['f034ftotal_inc_tax'];
            $id_item = $item_result['f034fid_item'];
            $query = "UPDATE t088fitem set f088famount=f088famount+$amount where f088fid = $id_item"; 
            $result=$em->getConnection()->executeQuery($query);
        }
        return $res;
    }
     public function assetLifeIncrease($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
             $query = "select f034fid_item,f088fid_sub_category from t034fgrn_details inner join  t088fitem on f088fid = f034fid_item  where f034fid_details = $id"; 
            $item_result=$em->getConnection()->executeQuery($query)->fetch();
            $amount = $item_result['f034ftotal_inc_tax'];
            $sub_category = $item_result['f088fid_sub_category'];
            $query = "UPDATE t089fasset_sub_categories set f089flife_expectancy=f089flife_expectancy+1 where f089fid = $sub_category"; 
            $result=$em->getConnection()->executeQuery($query);
        }
        return $res;
    }
    
}
