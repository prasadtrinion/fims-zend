<?php
namespace Application\Repository;

use Application\Entity\T148floanPaymentHistory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Application\Entity\T082fmonthlyDeductionMaster;
use Application\Entity\T082fmonthlyDeductionDetail;
class LoanPaymentHistoryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("l.f148fid,l.f148fidLoan,l.f148fstaff,l.f148fdeductDate,l.f148famount,l.f148fstatus,ln.f020floanName, s.f034fname,lo.f043freferenceNumber")
        ->from('Application\Entity\T148floanPaymentHistory','l')
        ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId = l.f148fstaff')
        ->leftjoin('Application\Entity\T043fvehicleLoan','lo','with', 'lo.f043fid = l.f148fidLoan')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','lo.f043fidLoanName = ln.f020fid')

        ->orderBy('l.f148fid','DESC');

        $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result = array();
        foreach ($result1 as $row) {
            $row['f148fdeductDate'] = date("d/m/Y",strtotime($row['f148fdeductDate']));
            array_push($result, $row);
        }
        $result = array(
            'data' => $result
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("l.f148fid,l.f148fidLoan,l.f148fstaff,l.f148fdeductDate,l.f148famount,l.f148fstatus,ln.f020floanName, s.f034fname")
        ->from('Application\Entity\T148floanPaymentHistory','l')
        ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId = l.f148fstaff')
        ->leftjoin('Application\Entity\T043fvehicleLoan','lo','with', 'lo.f043fid = l.f148fidLoan')
        ->leftjoin('Application\Entity\T020floanName', 'ln', 'with','lo.f043fidLoanName = ln.f020fid')
        ->where('l.f148fid = :fundId')
        ->setParameter('fundId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }



    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $loanPaymentHistory = new T148floanPaymentHistory();

        $loanPaymentHistory->setF148fidLoan((int)$data['f148fidLoan'])
        ->setF148fstaff((int)$data['f148fstaff'])
        ->setF148famount((float)$data['f148famount'])
        ->setF148fdeductDate(new \DateTime())
        ->setF148fstatus((int)1)
        ->setF148fcreatedBy((int)$_SESSION['userId'])
        ->setF148fupdatedBy((int)$_SESSION['userId']);

        $loanPaymentHistory->setF148fcreatedDtTm(new \DateTime())
        ->setF148fupdatedDtTm(new \DateTime());

        try{
            $amount = $data['f148famount'];
            $idLoan = $data['f148fidLoan'];
            $query = "update t043fvehicle_loan set f043fpaid = f043fpaid + $amount,f043fbalance = f043fbalance - $amount where f043fid = $idLoan";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $em->persist($loanPaymentHistory);
            $this->processLoan($idLoan,$amount);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }


        



        return $loanPaymentHistory;

    }
    public function processLoan($id,$amount) 
{
    $em = $this->getEntityManager();

        $query = "update t043fvehicle_loan set f043floan_posting=1 where f043fid = $id";

        $em->getConnection()->executeQuery($query);

        // $query = "select f043floan_amount,f043fmonthly_installment from t043fvehicle_loan where f043fid = $id";
        // $loans = $em->getConnection()->executeQuery($query)->fetch();

        // $balance_amount = $loans['f043floan_amount'] - $loans['f043fmonthly_installment'];
        // $query = "update t043fvehicle_loan set f043floan_amount=$balance_amount where f043fid = $id";
        // $em->getConnection()->executeQuery($query);

        // print_r($loans);
        // print_r($balance_amount);
        // exit();
        $query = "select f043fid_vendor, f043femp_name, f043fmonthly_installment, f043fstart_date, f043fend_date from t043fvehicle_loan where f043fid = $id";
        $loan_result = $em->getConnection()->executeQuery($query)->fetch();
        // print_r($loan_result);
        // // print_r($balance_amount);
        // exit();

        if($loan_result){
         $monthlyDeduction = new T082fmonthlyDeductionMaster();

         $monthlyDeduction->setF082fidStaff((int)$loan_result['f043femp_name'])
         ->setF082fname($loan_result['f043femp_name'])
         ->setF082fstatus((int)"0")
         ->setF082fcreatedBy((int)$_SESSION['userId'])
         ->setF082fupdatedBy((int)$_SESSION['userId']);

         $monthlyDeduction->setF082fcreatedDtTm(new \DateTime())
         ->setF082fupdatedDtTm(new \DateTime());

         $em->persist($monthlyDeduction);
         $em->flush();


         $monthlyDetailObj = new T082fmonthlyDeductionDetail();

         $monthlyDetailObj->setF082fidMaster((int)$monthlyDeduction->getF082fid())
         ->setF082fdeductionCode("LOAN")
         ->setF082famount((float)$amount)
         ->setF082fstartDate(new \DateTime($loan_result['f043fstart_date']))
         ->setF082fendDate(new \DateTime($loan_result['f043fend_date']))
         ->setF082fstatus((int)"0")
         ->setF082fcreatedBy((int)$_SESSION['userId'])
         ->setF082fupdatedBy((int)$_SESSION['userId']);

         $monthlyDetailObj->setF082fcreatedDtTm(new \DateTime())
         ->setF082fupdatedDtTm(new \DateTime());


         $em->persist($monthlyDetailObj);

         $em->flush();

     }
}

    /* to edit the data in database*/

    public function updateData($loanPaymentHistory, $data = []) 
    {
        $em = $this->getEntityManager();

        $loanPaymentHistory->setF148fidLoan((int)$data['f148fidLoan'])
        ->setF148fstaff((int)$data['f148fstaff'])
        ->setF148famount((float)$data['f148famount'])
        // ->setF148fdeductDate(new \DateTime($data['f148fdeductDate']))
        ->setF148fstatus((int)1)
        ->setF148fupdatedBy((int)$_SESSION['userId']);

        $loanPaymentHistory->setF148fupdatedDtTm(new \DateTime());


        $em->persist($loanPaymentHistory);
        
        $em->flush();



    }    
}

?>
