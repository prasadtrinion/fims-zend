<?php
namespace Application\Repository;

use Application\Entity\T057ffund;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class FundRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("f.f057fid, f.f057fname as originalname, (f.f057fcode+' - ' +f.f057fname) as f057fname,f.f057fcode, f.f057fstatus, f.f057fcreatedBy, f.f057fupdatedBy,f.f057foldCode")
            ->from('Application\Entity\T057ffund','f')
            ->orderBy('f.f057fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f.f057fid, f.f057fname, f.f057fcode, f.f057fstatus, f.f057fcreatedBy, f.f057fupdatedBy')
            ->from('Application\Entity\T057ffund','f')
            ->where('f.f057fid = :fundId')
            ->setParameter('fundId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $fund = new T057ffund();

        $fund->setF057fname($data['f057fname'])
             ->setF057fcode($data['f057fcode'])
             ->setF057fstatus((int)$data['f057fstatus'])
             ->setF057fcreatedBy((int)$_SESSION['userId'])
             ->setF057fupdatedBy((int)$_SESSION['userId']);

        $fund->setF057fcreatedDtTm(new \DateTime())
                ->setF057fupdatedDtTm(new \DateTime());
               
         

        $em->persist($fund);
        
        $em->flush();
        
           
    

        return $fund;

    }

    /* to edit the data in database*/

    public function updateData($fund, $data = []) 
    {
        $em = $this->getEntityManager();

        $fund->setF057fname($data['f057fname'])
             ->setF057fcode($data['f057fcode'])
             ->setF057fstatus((int)$data['f057fstatus'])
             ->setF057fcreatedBy((int)$_SESSION['userId'])
             ->setF057fupdatedBy((int)$_SESSION['userId']);

                 $fund->setF057fupdatedDtTm(new \DateTime());

      
        $em->persist($fund);
        
        $em->flush();



    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("f.f057fid,f.f057fname as originalname, f.f057fcode,f.f057fcode+' - ' +f.f057fname as f057fname, f.f057fstatus, f.f057fcreatedBy, f.f057fupdatedBy")
            ->from('Application\Entity\T057ffund','f')
            ->where('f.f057fstatus=1')
	    ->orderBy('f057fname');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
}

?>
