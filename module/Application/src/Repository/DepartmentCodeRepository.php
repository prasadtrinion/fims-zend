<?php
namespace Application\Repository;

use Application\Entity\T015fdepartmentCode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DepartmentCodeRepository extends EntityRepository 
{

    public function getList() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015fid, d.f015fdepartmentName, d.f015fdepartmentCode, d.f015fstatus')
            ->from('Application\Entity\T015fdepartmentCode','d')
            ->orderBy('d.f015fid','DESC');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('d.f015fid, d.f015fdepartmentName, d.f015fdepartmentCode, d.f015fstatus')
            ->from('Application\Entity\T015fdepartmentCode','d')
            ->where('d.f015fid = :departmentId')
            ->setParameter('departmentId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }
    
    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $department = new T015fdepartmentCode();

        $department->setF015fdepartmentName($data['f015fdepartmentName'])
                ->setF015fdepartmentCode($data['f015fdepartmentCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fcreatedBy((int)$_SESSION['userId'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $department->setF015fupdatedDtTm(new \DateTime())
                    ->setF015fcreatedDtTm(new \DateTime());


        
        $em->persist($department);
        $em->flush();
       
        
        
        return $department;
    }

    public function updateData($department, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $department->setF015fdepartmentName($data['f015fdepartmentName'])
                ->setF015fdepartmentCode($data['f015fdepartmentCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $department->setF015fupdatedDtTm(new \DateTime());
       
        $em->persist($department);
        $em->flush();
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015fid, d.f015fdepartmentName, d.f015fdepartmentCode, d.f015fstatus')
            ->from('Application\Entity\T015fdepartmentCode','d')
            ->where('d.f015fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}

?>