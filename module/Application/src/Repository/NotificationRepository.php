<?php
namespace Application\Repository;

use Application\Entity\T135fnotification;
use Application\Entity\T136fnotificationEmail;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class NotificationRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
         $qb->select("n.f135fid, n.f135fdays, n.f135fmaturityDate, n.f135fstatus")
            ->from('Application\Entity\T135fnotification', 'n')
            // ->leftjoin('Application\Entity\T073fcategory','c','with','n.f135fidCategory = c.f073fid')
            ->orderBy('n.f135fid','DESC');
            

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("n.f135fid, n.f135fdays, n.f135fmaturityDate, n.f135fstatus, ne.f136femail, ne.f136fid")
            ->from('Application\Entity\T135fnotification', 'n')
            ->leftjoin('Application\Entity\T136fnotificationEmail','ne','with','n.f135fid = ne.f136fidNotification')
            ->where('n.f135fid = :blockId')
            ->setParameter('blockId',$id);
             
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
       
     	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $notification = new T135fnotification();

         $notification->setF135fdays((int)$data['f135fdays'])
        ->setF135fmaturityDate(new \DateTime($data['f135fmaturityDate']))
        ->setF135fstatus((int)$data['f135fstatus'])
        ->setF135fcreatedBy((int)$_SESSION['userId']);


        $notification->setF135fcreatedDtTm(new \DateTime());
     
            $em->persist($notification);
            $em->flush();


            $notifications = $data['notification-email'];
            foreach ($notifications as $emailData)
            {
            
                $notificationEmail = new T136fnotificationEmail();

                $notificationEmail->setF136fidNotification((int)$notification->getF135fid())
                ->setF136femail($emailData['f136femail'])
                ->setF136fstatus((int)$emailData['f136fstatus'])
                ->setF136fcreatedBy((int)$_SESSION['userId']);
                
                $notificationEmail->setF136fcreatedDtTm($f136fcreatedDtTm);

                $em->persist($notificationEmail);
                $em->flush();
            } 


        	return $notification;
    }

    public function updateData($notification, $data = []) 
    {
        $em = $this->getEntityManager();

        $notification->setF135fdays((int)$data['f135fdays'])
        ->setF135fmaturityDate(new \DateTime($data['f135fmaturityDate']))
        ->setF135fstatus((int)$data['f135fstatus'])
        ->setF135fupdatedBy((int)$_SESSION['userId']);

        $notification->setF135fupdatedDtTm(new \DateTime());

        $em->persist($notification);
        $em->flush();
        return $notification;
    }


    public function updateNotificationEmailData($notificationEmailObj, $emailData)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $notificationEmailObj->setF136femail($emailData['f136femail'])
                ->setF136fstatus((int)$emailData['f136fstatus'])
                ->setF136fupdatedBy((int)$_SESSION['userId']);

        $notificationEmailObj->setF136fupdatedDtTm(new \DateTime());

            $em->persist($notificationEmailObj);
            $em->flush();

        return $notificationEmailObj;
    }

    public function createNotificationEmailData($notification, $emailData)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $notificationEmail = new T136fnotificationEmail();

                $notificationEmail->setF136fidNotification((int)$notification->getF135fid())
                ->setF136femail($emailData['f136femail'])
                ->setF136fstatus((int)$emailData['f136fstatus'])
                ->setF136fcreatedBy((int)$_SESSION['userId']);
                
                $notificationEmail->setF136fcreatedDtTm($f136fcreatedDtTm);

                $em->persist($notificationEmail);
                $em->flush();

                return $notificationEmail;
    }

    public function notificationActiveList($id)
    {

        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
         $qb->select("n.f135fid, n.f135fdays, n.f135fmaturityDate, n.f135fstatus")
            ->from('Application\Entity\T135fnotification', 'n')
            // ->leftjoin('Application\Entity\T073fcategory','c','with','n.f135fidCategory = c.f073fid')
            ->where('n.f135fstatus = :blockId')
            ->setParameter('blockId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
}
