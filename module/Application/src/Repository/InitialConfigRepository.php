<?php
namespace Application\Repository;

use Application\Entity\T011finitialConfig;
use Doctrine\ORM\EntityRepository;

class InitialConfigRepository extends EntityRepository
{
    /* to retrive the data from database*/
    public function getList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f011fid, i.f011ffyStarting, i.f011ffyEnding, i.f011fgstNo, i.f011fcentralExcise, i.f011faddress1, i.f011faddress2, i.f011fcity, i.f011fcontactName, i.f011fphoneNo, i.f011femail, i.f011fstatus, i.f011fupdatedBy, i.f011fstate, i.f011fcountry, i.f011fdefaultState, i.f011fdefaultCountry,i.f011fidFinancialYear, i.f011fbudgetSplit, i.f011fcreditNoteNarration, i.f011fdebitNoteNarration, i.f011finvoiceNarration, i.f011fnoninvoiceNarration, i.f011fcreditorNarration, i.f011fdebitorNarration')
        ->from('Application\Entity\T011finitialConfig', 'i');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f011ffyStarting'];
            $item['f011ffyStarting'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f011ffyEnding'];
            $item['f011ffyEnding'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;

    }

    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('i.f011fid, i.f011ffyStarting, i.f011ffyEnding, i.f011fgstNo, i.f011fcentralExcise, i.f011faddress1, i.f011faddress2, i.f011fcity, i.f011fcontactName, i.f011fphoneNo, i.f011femail, i.f011fstatus, i.f011fupdatedBy, i.f011fstate, i.f011fcountry, i.f011fdefaultState, i.f011fdefaultCountry, i.f011fidFinancialYear, i.f011fbudgetSplit, i.f011fcreditNoteNarration, i.f011fdebitNoteNarration, i.f011finvoiceNarration, i.f011fnoninvoiceNarration, i.f011fcreditorNarration, i.f011fdebitorNarration')
        ->from('Application\Entity\T011finitialConfig', 'i')
        ->where('i.f011fid = :initialconfigId')
        ->setParameter('initialconfigId', $id);

        $query  = $qb->getQuery();
        $result = $query->getSingleResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f011ffyStarting'];
            $item['f011ffyStarting'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f011ffyEnding'];
            $item['f011ffyEnding'] = date("Y-m-d", strtotime($date));
            array_push($result1, $journal);
        }
        return $result1;
    }

    public function createNewData($data)
    {
        $em = $this->getEntityManager();

        $initialConfig = new T011finitialConfig();

        $initialConfig->setF011ffyStarting(new \DateTime($data['f011ffyStarting']))
        ->setF011ffyEnding(new \DateTime($data['f011ffyEnding']))
        ->setF011fgstNo($data['f011fgstNo'])
        ->setF011fidFinancialYear((int)$data['f011fidFinancialYear'])
        ->setF011fcentralExcise($data['f011fcentralExcise'])
        ->setF011faddress1($data['f011faddress1'])
        ->setF011faddress2($data['f011faddress2'])
        ->setF011fcity($data['f011fcity'])
        ->setF011fcontactName($data['f011fcontactName'])
        ->setF011fphoneNo($data['f011fphoneNo'])
        ->setF011femail($data['f011femail'])
        ->setF011fstatus((int)$data['f011fstatus'])
        ->setF011fstate((int)$data['f011fstate'])
        ->setF011fcountry((int)$data['f011fcountry'])
        ->setF011fdefaultstate((int)$data['f011fdefaultState'])
        ->setF011fdefaultCountry((int)$data['f011fdefaultCountry'])
        ->setF011fbudgetDepartmentActivitySplit((int)$data['f011fbudgetSplit'])
        ->setF011fcreditNoteNarration($data['f011fcreditNoteNarration'])
        ->setF011fdebitNoteNarration($data['f011fdebitNoteNarration'])
        ->setF011finvoiceNarration($data['f011finvoiceNarration'])
        ->setF011fnoninvoiceNarration($data['f011fnoninvoiceNarration'])
        ->setF011fcreditorNarration($data['f011fcreditorNarration'])
        ->setF011fdebitorNarration($data['f011fdebitorNarration'])
        ->setF011fcreatedBy((int)$_SESSION['userId'])
        ->setF011fupdatedBy((int)$_SESSION['userId']);

        $initialConfig->setF011fcreatedDtTm(new \DateTime())
        ->setF011fupdatedDtTm(new \DateTime());

        $em->persist($initialConfig);
        $em->flush();

        return $initialConfig;

    }

    public function updateData($initialConfig, $data = [])
    {

        $em = $this->getEntityManager();

        $initialConfig->setF011ffyStarting(new \DateTime($data['f011ffyStarting']))
        ->setF011ffyEnding(new \DateTime($data['f011ffyEnding']))
        ->setF011fgstNo($data['f011fgstNo'])
        ->setF011fidFinancialYear((int)$data['f011fidFinancialYear'])
        ->setF011fcentralExcise($data['f011fcentralExcise'])
        ->setF011faddress1($data['f011faddress1'])
        ->setF011faddress2($data['f011faddress2'])
        ->setF011fcity($data['f011fcity'])
        ->setF011fcontactName($data['f011fcontactName'])
        ->setF011fphoneNo($data['f011fphoneNo'])
        ->setF011femail($data['f011femail'])
        ->setF011fstatus((int)$data['f011fstatus'])
        ->setF011fstate((int)$data['f011fstate'])
        ->setF011fcountry((int)$data['f011fcountry'])
        ->setF011fdefaultstate((int)$data['f011fdefaultState'])
        ->setF011fdefaultCountry((int)$data['f011fdefaultCountry'])
        ->setF011fbudgetDepartmentActivitySplit((int)$data['f011fbudgetSplit'])
        ->setF011fcreditNoteNarration($data['f011fcreditNoteNarration'])
        ->setF011fdebitNoteNarration($data['f011fdebitNoteNarration'])
        ->setF011finvoiceNarration($data['f011finvoiceNarration'])
        ->setF011fnoninvoiceNarration($data['f011fnoninvoiceNarration'])
        ->setF011fcreditorNarration($data['f011fcreditorNarration'])
        ->setF011fdebitorNarration($data['f011fdebitorNarration'])
        ->setF011fcreatedBy((int)$_SESSION['userId'])
        ->setF011fupdatedBy((int)$_SESSION['userId']);

        $initialConfig->setF011fupdatedDtTm(new \DateTime());

        $em->persist($initialConfig);
        $em->flush();

    }

    public function update($entity)
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    public function generateFIMS($data)
    {

        $em   = $this->getEntityManager();
        $year = date('y');
        $Year = date('Y');
        $type = $data['type'];
        // print_r($type);exit;

        switch ($type) {
            // case 'CN':
            //     $query  = "select * from t031fcredit_note inner join t071finvoice on f031fid_invoice = f071fid where f071finvoice_from = 2";
            //     $result = $em->getConnection()->executeQuery($query)->fetchAll();
            //     $count  = count($result) + 1;
            //     $number = "CND00000" . $count . "/" . /$year;
            //     break;
            case 'C':
            $query  = "select top 1 f030fvendor_code from t030fvendor_registration order by f030fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f030fvendor_code'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "C" .(sprintf("%'06d", $count)). "/" . $year;
            break;
            // case 'DN':
            //     $query  = "select * from t031fdebit_note inner join t071finvoice on f031fid_invoice = f071fid where f071finvoice_from = 2";
            //     $result = $em->getConnection()->executeQuery($query)->fetchAll();
            //     $count  = count($result) + 1;
            //     $number = "DND00000" . $count . "/" . /$year;
            //     break;
            case 'OR':

            case 'IV':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071finvoice_type = 'OR' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IVD" .(sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'STD':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071finvoice_type = 'STD' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IVA" .(sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'STA':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071finvoice_type = 'STA' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IVB" .(sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'SP':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071finvoice_type = 'SP' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IVE" .(sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'DOC':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071fis_rcp = 1 and f071finvoice_from = 1 and f071finvoice_type = 'DOC' order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IVF" .(sprintf("%'06d", $count)). "/" . $year;
            break;


            case 'COR':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'COR' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CND" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CSTD':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CSTD' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNA" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CSTA':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CSTA' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNB" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CSP':
            $query  = "select top 1 f061freference_number from t061fsponsor_cn where f061ftype = 'CSP' order by f061fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f061freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNE" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CDOC':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CDOC' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CND" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DOR':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DOR' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DND" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DSTD':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DSTD' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNA" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DSTA':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DSTA' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNB" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DSP':
            $query  = "select top 1 f063freference_number from t063fsponsor_dn where f063ftype = 'DSP' order by f063fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f063freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNE" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            // case 'DDOC':
            //     $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DDOC'";
            //     $result = $em->getConnection()->executeQuery($query)->fetch();
            //     $data=$result['f031freference_number'];
            //     $value=substr($data, -9);
            //     $value=substr($value, 0,6);
            //     $count=$value + 1;
            //     $number = "DND" .(sprintf("%'06d", $count)) . "/" . $year;
            //     break;

            case 'DCOR':
            $query  = "select top 1 f024freference_number from t024fdiscount where f024ftype = 'DCOR' order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DCD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DCSTD':
            $query  = "select top 1 f024freference_number from t024fdiscount where f024ftype = 'DCSTD' order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DCA" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DCSTA':
            $query  = "select top 1 f024freference_number from t024fdiscount where f024ftype = 'DCSTA' order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
                // print_r($data);exit;
            $value=substr($data, 3,6);
            $count=$value + 1;
            $number = "DCB" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DCSP':
            $query  = "select top 1 f024freference_number from t024fdiscount where f024ftype = 'DCSP' order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DCE" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DCDOC':
            $query  = "select top 1 f024freference_number from t024fdiscount where f024ftype = 'DCDOC' order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DCDO" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'ROR':
            $query  = "select top 1 f082freceipt_number from t082freceipt where f082fpayee_type = 'ROR' order by f082fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f082freceipt_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RSTD':
            $query  = "select top 1 f082freceipt_number from t082freceipt where f082fpayee_type = 'RSTD' order by f082fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f082freceipt_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCA" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RSTA':
            $query  = "select top 1 f082freceipt_number from t082freceipt where f082fpayee_type = 'RSTA' order by f082fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f082freceipt_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCB" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RSP':
            $query  = "select top 1 f082freceipt_number from t082freceipt where f082fpayee_type = 'RSP' order by f082fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f082freceipt_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RDOC':
            $query  = "select top 1 f082freceipt_number from t082freceipt where f082fpayee_type = 'RDOC' order by f082fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f082freceipt_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RCP':
            $query  = "select top 1 f071finvoice_number from t071finvoice where f071fis_rcp = 2 and f071finvoice_from = 2 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "RCPD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'KI':
            $query = "SELECT top 1 f071finvoice_number FROM t073frequest_invoice order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "KI" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CI':
            $query  = "select top 1 f071finvoice_number from t073frequest_invoice where f071finvoice_type = 'CI' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $number = $result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CI" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'PI':
            $query  = "select top 1 f071finvoice_number from t073frequest_invoice where f071finvoice_type = 'PI' and f071finvoice_from = 2 and f071fis_rcp = 1 order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $number = $result['f071finvoice_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "PI" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'FD':
                // $query = "SELECT f064freference_number FROM t064finvestment_registration order by f064fid desc";
                // $result = $em->getConnection()->executeQuery($query)->fetch();

                // $data=$result['f064freference_number'];
                // $value=substr($data, 3,6);

                // $count=$value + 1;
                // $number = "FD" . (sprintf("%'06d", $count)) . "/" . /$year;
            $query = "SELECT top 1 f064freference_number FROM t064finvestment_registration order by f064fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f064freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "FD" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'PV':
            $query  = "select top 1 f093fvoucher_no from t093fpayment_voucher order by f093fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f093fvoucher_no'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "PV" . (sprintf("%'06d", $count)) . "/" . $year;
                // print_r($number);
                // die();
            break;

            case 'VC':
            $query  = "SELECT top 1 f071fvoucher_number from t071fvoucher order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071fvoucher_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "VC" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DS':
            $query  = "SELECT top 1 f024freference_number from t024fdiscount order by f024fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f024freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DSD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'Student':
            $query  = "SELECT top 1 f071fvoucher_number from t071fvoucher inner join t011fdefinationms on f071fvoucher_type = f011fid where f011fdefinition_code = 'Student' order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071fvoucher_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "VCA00000" . (sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'Sponsor':
            $query  = "SELECT top 1 f071fvoucher_number from t071fvoucher inner join t011fdefinationms on f071fvoucher_type = f011fid where f011fdefinition_code = 'Sponsor' order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071fvoucher_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "VCE" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'Other Receivables':
            $query  = "SELECT top 1 f071fvoucher_number from t071fvoucher inner join t011fdefinationms on f071fvoucher_type = f011fid where f011fdefinition_code = 'Other Receivables' order by f071fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f071fvoucher_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "VCD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'PR':
            $query = "SELECT top 1 f088freference_number FROM t088fpurchase_requisition order by f088fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f088freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "PR" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

                // $query  = "SELECT * from t088fpurchase_requisition";
                // $result = $em->getConnection()->executeQuery($query)->fetchAll();
                // $count  = count($result) + 1;
                // $number = "PR00000" . $count . "/" . $year;
                // break;

            case 'GL':
            $query  = "SELECT top 1 f017freference_number from t017fjournal order by f017fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f017freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "GL" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CSTD':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CSTD' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNA" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CSTA':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CSTA' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNB" . (sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'CDOC':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'CDOC' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CNF" . (sprintf("%'06d", $count)) . "/" . $year;
            break;
            case 'COR':
            $query  = "select top 1 f031freference_number from t031fcredit_note where f031ftype = 'COR' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CND" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DSTD':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DSTD' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNA" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DSTA':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DSTA' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNB" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DDOC':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DDOC' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f031freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DNF" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'DOR':
            $query  = "select top 1 f031freference_number from t031fdebit_note where f031ftype = 'DOR' order by f031fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $count  = count($result) + 1;
            $number = "DND" . (sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'BCH':
            $query  = "select top 1 f050freference_number from t050fbatch_master order by f050fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f050freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "BCH" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'SPBILL':
            $query  = "select top 1 f063fbill_no from t063fsponsor_bill order by f063fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f063fbill_no'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "SPB" . (sprintf("%'06d", $count))  . "/" . $year;
            break;

            case 'Vendor':
            $query  = "select top 1 right(f030fvendor_code,len(f030fvendor_code)-1) as f030fvendor_code from t030fvendor_registration order by cast(right(f030fvendor_code,len(f030fvendor_code)-1) as int)  desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f030fvendor_code'];
            // print_r($data);exit;
            // $value=substr($data, 1);
            // print_r($value);exit;
            $count=$data + 1;
            $number = "C" .  $count ;
            break;


            case 'BIL':
            $query = "SELECT top 1 f091freference_number FROM t091fbill_registration order by f091fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f091freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "BIL" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'Cash':
            $query  = "select top 1 f052fcash_advance from t052fcash_application order by f052fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f052fcash_advance'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "CAD" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'LOAN':
            $query = "SELECT top 1 f043freference_number from t043fvehicle_loan order by f043fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f043freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "LOAN" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'PO':
            $query  = "select top 1 f034freference_number from t034fpurchase_order order by f034fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f034freference_number'];
                // print_r($data);
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "PO" .(sprintf("%'06d", $count)) . "/" . $year;
                // print_r($number);
                // die();
            break;

            case 'Warrant':
            $query  = "select top 1 f143freference_number from t143fwarrant order by f143fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f143freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "WP" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'NONPO':
            $query  = "select top 1 f034freference_number from t034fpurchase_order where f034forder_type='NONPO' order by f034fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f034freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "NP" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'Tender':
            $query  = "select top 1 f035fquotation_number from t035ftender_quotation where f035fid_category = 'Tender' order by f035fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f035fquotation_number'];
            $value=substr($data, -6);
            $value=substr($value, 0,3);
            $count=$value + 1;
            $number = "UUM/BEN/T" .(sprintf("%'03d", $count)) . "/" . $year;
            break;

            case 'Quotation':
            $query  = "select top 1 f035fquotation_number from t035ftender_quotation where f035fid_category = 'Quotation' order by f035fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f035fquotation_number'];
            $value=substr($data, -6);
            $value=substr($value, 0,3);
            $count=$value + 1;
            $number = "UUM/BEN/S" . (sprintf("%'03d", $count)) . "/" . $year;

            break;

            case 'GR':
            $query = "SELECT top 1 f034freference_number FROM t034fgrn order by f034fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f034freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "GR" .(sprintf("%'06d", $count)) . "/" . $year;
            break;

            // case 'ASC':
                // $query  = "select * from t089fasset_sub_categories";
                // $result = $em->getConnection()->executeQuery($query)->fetchAll();
                // $count  = count($result) + 1;
                // $number = $count ;
                // break;

            case 'ASC':
            $query  = "select top 1 f089faccount_number from t089fasset_sub_categories order by f089fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f089faccount_number'];
            $number  = $data + 1;
                // echo $number;
                // die();
            break;

            case 'AssetDisposal':
            $query  = "select top 1 f086fbatch_id from t086fdisposal_requisition order by f086fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f086fbatch_id'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "AD" . (sprintf("%'06d", $count)) . "/" . $year;
                // print_r($number);
                // die();
            break;

            case 'Asset':
            $query  = "select top 1 f085fasset_code from t085fasset_information order by f085fid desc"; 
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f085fasset_code'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = (sprintf("%'06d", $count));
            break;

            case 'DV':
            $query  = "SELECT top 1 f087freference_number from t087fdisposal_verification order by f087fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f087freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "DV" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'CAT':
            $query  = "select top 1 f051fcash_id from t051fcash_advance_type order by f051fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $count  = $result['f051fcash_id'] + 1;
            $number = $count ;
            break;

            case 'IBN':

                // $query = "SELECT f063fapplication_id FROM t063fapplication_master order by f063fid desc";
                // $result = $em->getConnection()->executeQuery($query)->fetch();

                // $data=$result['f063fapplication_id'];
                // $value=substr($data, 3,6);

                // $count=$value + 1;
                // $number = "IBN" . (sprintf("%'06d", $count)) . "/" . /$year;

            $query  = "SELECT top 1 f063fapplication_id from t063fapplication_master order by f063fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f063fapplication_id'];
                // print_r($data);exit;
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "IBN" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'RECSETUP':
            $query  = "SELECT top 1 f077freference_number from t077frecurring_set_up  order by f077fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f077freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "REC" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'REVENUE':
            $query  = "SELECT top 1 f073frevenue_code from t073fcategory order by f073fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
                // $count  = count($result) + 1;                
            $number = (sprintf("%'05d",$result['f073frevenue_code']+1));
            break;

            case 'SPC':
            $query  = "select top 1 * from t064ftagging_sponsors order by f064fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f064fsponsor_code'];
            $value=substr($data, -9);
            $value=substr($value, 2,6);
                // print_r($value);exit;
            $count=$value + 1;
            $number = "SP" . (sprintf("%'06d", $count));
            break;

            case 'GRND':
            $query = "SELECT top 1 f121freference_number FROM t121fgrn_donation order by f121fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f121freference_number'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "GRND" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'AG':
            $query = "SELECT top 1 f126fregister_no FROM t126ftender_aggrement order by f126fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f126fregister_no'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "AG" . (sprintf("%'06d", $count)) . "/" . $year;
            break;


            case 'SPONS':
            $query = "SELECT top 1 f063fcode FROM t063fsponsor order by f063fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f063fcode'];
            $value=substr($data, -9);
            $value=substr($value, 0,6);
            $count=$value + 1;
            $number = "SP" . (sprintf("%'06d", $count));
            break;

            case 'FEESTRUCTURE':
            $Fee = $data['code'];
            $intake = $data['intake'];
            $query = "SELECT top 1 f048fcode FROM t048ffee_structure_master order by f048fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f048fcode'];
            $value=substr($data, -9);
            $value=substr($value, 6,9);
            $count=$value + 1;
            $number = $Fee . $intake . (sprintf("%'06d", $count));
            break;

            case 'PATTYCASHALLOCATION':
            $department = $data['department'];
            $query = "SELECT top 1 f122freference_no FROM T122fpatty_cash_allocation order by f122fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f122freference_no'];
            $value=substr($data, -6);
                // print_r($data);exit;

            $value=substr($value, 2,2);
                // print_r($value);exit;

            $count=$value + 1;
            $number = "PC" . $department . - (sprintf("%'06d", $count)). "/" . $year;
            break;

            case 'Customer':
            $query = "SELECT top 1 f021fdebtor_code FROM t021fcustomers order by f021fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f021fdebtor_code'];
                // print_r($data);exit;
            $value=substr($data, 1);

                // $value=substr($value, 0,1);
                   // print_r($value);exit;

            $count=$value + 1;
            $number = "D" . $count;
            break;

            case 'REVENUESETUP':
            $preFix = $data['prefixCode'];
                // print_r($data);exit;
            $revenueId = $data['revenueId'];
            $query = "SELECT top 1 f085fcode FROM t085frevenue_set_up where f085fid_revenue_category = $revenueId order by f085fid desc";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f085fcode'];
                // print_r($preFix);exit;
            if (!$data)
            {
                $number = $preFix. "101";

            }
            else
            {
                $value=substr($data, -6);
                    // print_r($data);exit;
                $value=substr($value, 3,6);
                $count=$value + 1;
                    // print_r($count);exit;
                $number = $preFix. (sprintf("%'02d", $count));
            }   
            break;

            case 'Premise':
                // print_r("fgh");exit;
            $preFix = $data['premise'];
            $revenueId = $data['revenueId'];
            $query = "SELECT top 1 f076fpremise_code FROM t076fpremise where f076fid_premise_type = $revenueId order by f076fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f076fpremise_code'];
            $value=substr($data, -3);
            $count=$value + 1;
            $number = $preFix . "F" . (sprintf("%'03d", $count));
            break;

            case 'ETF':
                // print_r("fgh");exit;
                // $preFix = $data['premise'];
                // $revenueId = $data['revenueId'];
            $query = "SELECT top 1 f136fbatch_id FROM t136fetf order by f136fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f136fbatch_id'];
            $value=substr($data, -3);
            $count=$value + 1;
            $number = "EFT" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'ETFGroup':
                // print_r("fgh");exit;
                // $preFix = $data['premise'];
                // $revenueId = $data['revenueId'];
            $query = "SELECT top 1 f138fbatch_id FROM t138fetf_grouping order by f138fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f138fbatch_id'];
            $value=substr($data, -3);
            $count=$value + 1;
            $number = "EFTBATCH" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            case 'WBATCH':
            $query = "SELECT top 1 f145fbatch_no FROM t145fbatch_withdraw order by f145fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f145fbatch_no'];
            $value=substr($data,6,6);
            $count=$value + 1;
            // print_r($count);exit;
            $number = "WBATCH" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

             case 'FDW':
            $query = "SELECT top 1 f064freference_number FROM t147finvestment_registration_withdraw order by f064fid desc ";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $data=$result['f064freference_number'];
            $value=substr($data,3,6);
            $count=$value + 1;
            $number = "FDW" . (sprintf("%'06d", $count)) . "/" . $year;
            break;

            

        }
                // print_r($number);exit;

        return $number;
    }
    public function generateSF($data)
    {

        $em   = $this->getEntityManager();
        $year = date('y');
        $type = $data['type'];
        switch ($type) {
            case 'CN':
            $query  = "select * from t031fcredit_note inner join t071finvoice on f031fid_invoice = f071fid where f071finvoice_from = 1";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $count  = count($result) + 1;
            $number = "CNA00000" . $count . "/" . $year;
            break;
            case 'DN':
            $query  = "select * from t031fdebit_note inner join t071finvoice on f031fid_invoice = f071fid where f071finvoice_from = 1";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $count  = count($result) + 1;
            $number = "DNA00000" . $count . "/" . $year;
            break;
            case 'IV':
            $query  = "select * from t071finvoice where f071fis_rcp = 1 and f071finvoice_from = 1";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $count  = count($result) + 1;
            $number = "IVA00000" . $count . "/" . $year;
            break;
            case 'RCP':
            $query  = "select * from t071finvoice where f071fis_rcp = 2 and f071finvoice_from = 1";
            $result = $em->getConnection()->executeQuery($query)->fetchAll();
            $count  = count($result) + 1;
            $number = "RCPA00000" . $count . "/" . $year;
            break;


        }
        return $number;
    }

    public function setUserId($token)
    {
        $em   = $this->getEntityManager();
        $query  = "select f014fid,f014fid_staff from t014fuser where f014ftoken = $token ";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        $_SESSION['userId'] = $result['f014fid']; 
        $_SESSION['staffId'] = $result['f014fid_staff']; 
        return;
    }
    public function getTables()
    {
        $em   = $this->getEntityManager();
        $query  = "select ob.name from sysobjects as ob where ob.type='U' order by ob.name";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }

    public function getColumns($table)
    {
        $em   = $this->getEntityManager();
        $query  = "SELECT sc.name FROM syscolumns sc INNER JOIN sysobjects so ON sc.id = so.id WHERE so.name= '".$table."'";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }

    public function delete($data)
    {
        $em   = $this->getEntityManager();
        foreach($data['id'] as $id)
        {
            $type = $data['type'];
            switch ($type)
            {

                case 'payrollContribution':
                $query  = "delete from t061fpayroll_contribution where f061fid = $id";
                $em->getConnection()->executeQuery($query);
                break;

                case 'payrollSchedule':
                $query  = "delete from t062fpayroll_schedule where f062fid = $id";
                $em->getConnection()->executeQuery($query);
                break;

                case 'payrollExcemption':
                $query  = "delete from t063fpayroll_excemption where f063fid = $id";
                $em->getConnection()->executeQuery($query);
                break;

                case 'payrollDeduction':
                $query  = "delete from t064fpayroll_deduction where f064fid = $id";
                $em->getConnection()->executeQuery($query);
                break;
            }   
        }      
        return;
    }
    public function checkDuplication($data){
        $em   = $this->getEntityManager();
        $key = $data['key'];
        switch ($key) {
            case 'Loan Name':
            $loanName = trim(strtolower($data['loanName']));
            $query  = "select f020floan_name from t020floan_name where LOWER(f020floan_name) = '$loanName'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f020fid != $id";
            }
            break;

            case 'Loan Insurance Company':
            $companyName = trim(strtolower($data['companyName']));
            $query  = "select f101fproduct_name from t101finsurance_company where LOWER(f101fproduct_name) = '$companyName'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f101fid != $id";
            }
            break;
            case 'Budget Year':
            $budgetYear = trim($data['budgetYear']);
            $query  = "select f110fyear from t110fbudgetyear where f110fyear = '$budgetYear'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f110fid != $id";
            }
            break;
            case 'Revenue Category':
            $revenueCategory = trim(strtolower($data['revenueCategory']));
            $query  = "select f073fcategory_name from t073fcategory where LOWER(f073fcategory_name) = '$revenueCategory'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f073fid != $id";
            }
            break;
            case 'Revenue Prefix':
            $revenuePrefix = trim(strtolower($data['revenuePrefix']));
            $query  = "select f073fcode from t073fcategory where LOWER(f073fcode) = '$revenuePrefix'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f073fid != $id";
            }
            break;
            case 'Revenue Setup':
            $revenueSetup = trim(strtolower($data['revenueSetup']));
            $revenueCategory = $data['revenueCategory'];
            $query  = "select f085fname from t085frevenue_set_up where LOWER(f085fname) = '$revenueSetup' and f085fid_revenue_category = $revenueCategory";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f085fid != $id";
            }
            break;
            case 'Premise Category':
            $premiseCode = trim(strtolower($data['premiseCode']));
            $query  = "select f037fcode from t037fpremise_type_setup where LOWER(f037fcode) = '$premiseCode'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f037fid != $id";
            }
            break;
            case 'Premise Setup':
            $premiseName = trim(strtolower($data['premiseName']));
            $premiseType = trim(strtolower($data['premiseType']));
            $query  = "select f076fname from t076fpremise where LOWER(f076fname) = '$premiseName' and f076fid_premise_type = $premiseType";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f076fid != $id";
            }
            break;
            case 'Debtor Email':
            $email = trim(strtolower($data['email']));
            $query  = "select f021femail_id from t021fcustomers where LOWER(f021femail_id) = '$email'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f021fid != $id";
            }
            break;
            case 'Debtor IC':
            $ic = trim(strtolower($data['ic']));
            $query  = "select f021femail_id from t021fcustomers where LOWER(f021flast_name) = '$ic'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f021fid != $id";
            }
            break;
            case 'Utility':
            $meter = trim(strtolower($data['meter']));
            $query  = "select f078fdescription from t078futility where LOWER(f078fdescription) = '$meter'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f078fid != $id";
            }
            break;
             case 'Student Discount':
            $discountType = trim(strtolower($data['discountType']));
            $query  = "select f046fdiscount_type from t046fstudent_discount where LOWER(f046fdiscount_type) = '$discountType'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f046fid != $id";
            }
            break;
            case 'Revenue Type':
            $revenueType = trim(strtolower($data['revenueType']));
            $query  = "select f132frevenue_name from t132frevenue_type where LOWER(f132frevenue_name) = '$revenueType'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f132fid != $id";
            }
            break;
            case 'Revenue Code':
            $revenueCode = trim(strtolower($data['revenueCode']));
            $query  = "select f132ftype_code from t132frevenue_type where LOWER(f132ftype_code)='$revenueCode'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f132fid != $id";
            }
            break;
            case 'Debtor Category':
            $debtorCategory = trim(strtolower($data['debtorCategory']));
            $query  = "select f130fdebtor_category_name from t130fdebtor_category where LOWER(f130fdebtor_category_name) = '$debtorCategory'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f130fid != $id";
            }
            break;
            case 'Payment Type':
            $paymentType = trim(strtolower($data['paymentType']));
            $query  = "select f018fcode from t018fonline_payment where LOWER(f018fcode) = '$paymentType'";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f018fid != $id";
            }
            break;
            case 'Loan Parameters':
            $empType = trim(strtolower($data['empType']));
            $empStatus = trim(strtolower($data['empStatus']));
            $loanType = $data['loanType'];
            $query  = "select f044fid_loan from t044floan_type_setup where LOWER(f044femp_type) = '$empType' and LOWER(f044femp_status) = '$empStatus' and f044fid_loan = $loanType";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f044fid != $id";
            }
            break;
            case 'Apply Loan':
            $loanType = $data['loanType'];
            $staff = $data['staff'];
            $query  = "select f043fid_loan_name from t043fvehicle_loan where f043femp_name = $staff and f043fid_loan_name = $loanType";
            if(isset($data['id'])){
                $id = $data['id'];
                $query = $query." and f043fid != $id";
            }
            break;

        }   
       // echo $query;
       // die();
        $result = $em->getConnection()->executeQuery($query)->fetch();
        if($result){
            return 1;
        }
        else{
            return 0;
        }
    }
}
