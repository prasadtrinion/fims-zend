<?php
namespace Application\Repository;

use Application\Entity\T074fsetUpItem;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class SetUpItemRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('s.f074fid, s.f074frevenueTypeSetUpId, s.f074fgstApplicable , s.f074fgstCode, s.f074fstatus, s.f074fcreatedBy, s.f074fupdatedBy')
            ->from('Application\Entity\T074fsetUpItem','s');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('s.f074fid, s.f074frevenueTypeSetUpId, s.f074fgstApplicable , s.f074fgstCode, s.f074fstatus, s.f074fcreatedBy, s.f074fupdatedBy')
            ->from('Application\Entity\T074fsetUpItem','s')
            ->where('s.f074fid = :setUpId')
            ->setParameter('setUpId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $setup = new T074fsetUpItem();

        $setup->setF074frevenueTypeSetUpId($data['f074frevenueTypeSetUpId'])
             ->setF074fgstApplicable($data['f074fgstApplicable'])
             ->setF074fgstCode($data['f074fgstCode'])
             ->setF074fstatus($data['f074fstatus'])
             ->setF074fcreatedBy((int)$_SESSION['userId'])
             ->setF074fupdatedBy((int)$_SESSION['userId']);

        $setup->setF074fcreatedDtTm(new \DateTime())
                ->setF074fupdatedDtTm(new \DateTime());

 
        $em->persist($setup);
        $em->flush();

        return $setup;

    }

    /* to edit the data in database*/

    public function updateData($setup, $data = []) 
    {
        $em = $this->getEntityManager();

        $setup->setF074frevenueTypeSetUpId($data['f074frevenueTypeSetUpId'])
             ->setF074fgstApplicable($data['f074fgstApplicable'])
             ->setF074fgstCode($data['f074fgstCode'])
             ->setF074fstatus($data['f074fstatus'])
             ->setF074fcreatedBy((int)$_SESSION['userId'])
             ->setF074fupdatedBy((int)$_SESSION['userId']);

        $setup->setF074fupdatedDtTm(new \DateTime());

        $em->persist($setup);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>