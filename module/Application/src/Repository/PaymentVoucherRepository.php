<?php
namespace Application\Repository;

use Application\Entity\T093fpaymentVoucher;
use Application\Entity\T094fpaymentVoucherDetails;
use Application\Entity\T096fcashBank;
use Application\Entity\T097fcashBankDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PaymentVoucherRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f093fid, p.f093fvoucherNo, p.f093fpaymentType, d.f011fdefinitionCode as paymentType, p.f093fdescription, p.f093fpaymentMode, m.f011fdefinitionCode as PaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidBank, p.f093fidCompanyBank, p.f093fsource, p.f093fidSupplier, b.f042fbankName as bankName, b.f042fbranchName as bankBranch, p.f093ftotalAmount')
            ->from('Application\Entity\T093fpaymentVoucher', 'p')
            // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', 'p.f093idCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T094fpaymentVoucherDetails', 'pv', 'with','pv.f094fidPayment = p.f093fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f093fpaymentType = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'm', 'with','p.f093fpaymentMode = m.f011fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','p.f093fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'cb', 'with','p.f093fidCompanyBank = cb.f041fid')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
            ->orderBy('p.f093fid','DESC');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 =  array();
        foreach ($result as $item)
        {
            $payment_id = $item['f093fid'];
             $query1 = "select sum(f094ftotal_amount) as totalAmount from t094fpayment_voucher_details where f094fid_payment = $payment_id";
            $res = $em->getConnection()->executeQuery($query1)->fetch();
            $item['totalAmount'] = $res['totalAmount'];
            array_push($result1, $item);

        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('p.f093fid, p.f093fvoucherNo, p.f093fpaymentType, d.f011fdefinitionCode as paymentType, p.f093fdescription, p.f093fpaymentMode, m.f011fdefinitionCode as PaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidBank, b.f042fbankName as customerBank, p.f093fidCompanyBank, cb.f041fidBank, a.f042fbankName as CompanyBank, p.f093fsource,  p.f093fidSupplier, b.f042fbankName as bankName, b.f042fbranchName as bankBranch, b.f042faccountNumber as bankAccountNo, b.f042fswiftCode, cb.f041faccountNumber as companyBankAccountNo, cb.f041fbranch, p.f093ftotalAmount')
            ->from('Application\Entity\T093fpaymentVoucher', 'p')
            // ->leftjoin('Application\Entity\T021fcustomers','c', 'with', 'p.f093idCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f093fpaymentType = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'm', 'with','p.f093fpaymentMode = m.f011fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','p.f093fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'cb', 'with','p.f093fidCompanyBank = cb.f041fid')
            ->leftjoin('Application\Entity\T042fbank', 'a', 'with','cb.f041fidBank = a.f042fid')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
            ->where('p.f093fid = :paymentId')
            ->setParameter('paymentId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result =  $result[0];
        // print_r($result);exit;
        $qb = $em->createQueryBuilder();

        $qb->select('p.f094fid,p.f094fidPayment,p.f094idBillRegistration,b.f091freferenceNumber as billReferenceNumber,p.f094fvoucherDate as f091fcreatedDate,p.f094ftotalAmount as f091ftotalAmount,p.f094fpaidAmount,p.f094fcustomerName as f093idCustomer')
            ->from('Application\Entity\T094fpaymentVoucherDetails', 'p')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
            ->leftjoin('Application\Entity\T091fbillRegistration', 'b', 'with','b.f091fid = p.f094idBillRegistration')

          ->where('p.f094fidPayment = :paymentId')
            ->setParameter('paymentId',$id);
            
        $query = $qb->getQuery();
        $detail_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
          $result1 = array();
        foreach ($detail_result as $note) {
            $date                        = $note['f091fcreatedDate'];
            $note['f091fcreatedDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $note);
        }
        $result['details'] = $result1;
        return $result;
    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
        // echo("hi");
        // exit;
           if($approvedStatus == 0)
           {
               $qb->select('p.f093fid, p.f093fvoucherNo, p.f093fpaymentType, d.f011fdefinitionCode as paymentType, p.f093fdescription, p.f093fpaymentMode, m.f011fdefinitionCode as PaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidBank, b.f042fbankName as customerBank, p.f093fidCompanyBank, cb.f041fidBank,b.f042fbankName as CompanyBank, p.f093fsource, p.f093fidSupplier, b.f042fbankName as bankName, b.f042fbranchName as bankBranch, b.f042faccountNumber as bankAccountNo, b.f042fswiftCode, cb.f041faccountNumber as companyBankAccountNo, cb.f041fbranch, p.f093ftotalAmount')
            ->from('Application\Entity\T093fpaymentVoucher', 'p')
            //->leftjoin('Application\Entity\T021fcustomers','c', 'with', 'p.f093idCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f093fpaymentType = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'm', 'with','p.f093fpaymentMode = m.f011fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','p.f093fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'cb', 'with','p.f093fidCompanyBank = cb.f041fid')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
            ->where(' p.f093fapprovalStatus = 0')
            ->orderBy('p.f093fid','DESC');
                 
            }

            elseif($approvedStatus == 1)
            {
               $qb->select('p.f093fid, p.f093fvoucherNo, p.f093fpaymentType, d.f011fdefinitionCode as paymentType, p.f093fdescription, p.f093fpaymentMode, m.f011fdefinitionCode as PaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidBank, b.f042fbankName as customerBank, p.f093fidCompanyBank, cb.f041fidBank,b.f042fbankName as CompanyBank, p.f093fsource, p.f093fidSupplier, b.f042fbankName as bankName, b.f042fbranchName as bankBranch, b.f042faccountNumber as bankAccountNo, b.f042fswiftCode, cb.f041faccountNumber as companyBankAccountNo, cb.f041fbranch, p.f093ftotalAmount')
            ->from('Application\Entity\T093fpaymentVoucher', 'p')
            //->leftjoin('Application\Entity\T021fcustomers','c', 'with', 'p.f093idCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f093fpaymentType = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'm', 'with','p.f093fpaymentMode = m.f011fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','p.f093fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'cb', 'with','p.f093fidCompanyBank = cb.f041fid')
            
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
                ->where(' p.f093fapprovalStatus = 1')
                ->orderBy('p.f093fid','DESC');;
                
            }

            elseif($approvedStatus == 2) 
            {
               $qb->select('p.f093fid, p.f093fvoucherNo, p.f093fpaymentType, d.f011fdefinitionCode as paymentType, p.f093fdescription, p.f093fpaymentMode, m.f011fdefinitionCode as PaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidBank, b.f042fbankName as customerBank, p.f093fidCompanyBank, cb.f041fidBank, b.f042fbankName as CompanyBank, p.f093fsource, p.f093fidSupplier, b.f042fbankName as bankName, b.f042fbranchName as bankBranch, b.f042faccountNumber as bankAccountNo, b.f042fswiftCode, cb.f041faccountNumber as companyBankAccountNo, cb.f041fbranch, p.f093ftotalAmount')
            ->from('Application\Entity\T093fpaymentVoucher', 'p')
            //->leftjoin('Application\Entity\T021fcustomers','c', 'with', 'p.f093idCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','p.f093fpaymentType = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'm', 'with','p.f093fpaymentMode = m.f011fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','p.f093fidBank = b.f042fid')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'cb', 'with','p.f093fidCompanyBank = cb.f041fid')
            // ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with','p.f093fidSupplier = vr.f030fid')
            ->orderBy('p.f093fid','DESC');
                
                
            }
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 =  array();
        foreach ($result as $item)
        {
            $payment_id = $item['f093fid'];
             $query1 = "select sum(f094ftotal_amount) as totalAmount from t094fpayment_voucher_details where f094fid_payment = $payment_id";
            $res = $em->getConnection()->executeQuery($query1)->fetch();
            $item['totalAmount'] = $res['totalAmount'];
            array_push($result1, $item);

        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    // public function updateApprovalStatus($paymentVouchers,$status,$reason) 
    // {
    //     $em = $this->getEntityManager(); 

    //     foreach ($paymentVouchers as $paymentVoucher) 
    //     {
    //         // print_r($paymentVoucher);exit;
    //     $paymentVoucher->setF093fapprovalStatus((int)$status), vr.f030fcompanyName vendorCompany, vr.f030femail as vendorEmail, vr.f030fvendorCode
    //                     ->setF093freason($reason)
    //                     ->setF093fUpdatedDtTm(new \DateTime());
       
    //     $em->persist($paymentVoucher);
    //     $em->flush();


    //     $voucher=$paymentVoucher->getF093fid();

	   // // print_r($voucher);exit;

    //     $qb = $em->createQueryBuilder();

    //     $qb->select('p.f093fvoucherNo, p.f093fpaymentType, p.f093fdescription, p.f093fpaymentMode, p.f093fpaymentRefNo, p.f093fstatus, p.f093fapprovalStatus, p.f093fidCompanyBank, pd.f094fpaidAmount, p.f093fupdatedDtTm')
    //         ->from('Application\Entity\T093fpaymentVoucher', 'p')
    //         ->leftjoin('Application\Entity\T094fpaymentVoucherDetails','pd', 'with', 'pd.f094fidPayment = p.f093fid')
    //         ->where('p.f093fid = :voucherId')
    //         ->setParameter('voucherId',(int)$voucher);

    //     $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        
        

    //     $qb1 = $em->createQueryBuilder();

    //     $qb1->select('pd.f094fpaidAmount, p.f093fstatus')
    //         ->from('Application\Entity\T093fpaymentVoucher', 'p')
    //         ->leftjoin('Application\Entity\T094fpaymentVoucherDetails','pd', 'with', 'p.f093fid = pd.f094fidPayment')
    //         //->leftjoin('Application\Entity\T071fvoucher','v', 'with', 'v.f071fid = pd.f094idVoucher')
    //         //->leftjoin('Application\Entity\T072fvoucherDetails','vd', 'with', 'vd.f072fidVoucher = v.f071fid')
    //         ->where('p.f093fid = :paymentId')
    //         ->setParameter('paymentId',(int)$voucher);

    //     $results = $qb1->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    //     $qb1 = $em->createQueryBuilder();

    //      $qb1->select('pd.f094fpaidAmount,pd.f093ftotalAmount,pd.f094idBillRegistration')
    //         ->from('Application\Entity\T094fpaymentVoucherDetails', 'pd')
    //         //->leftjoin('Application\Entity\T071fvoucher','v', 'with', 'v.f071fid = pd.f094idVoucher')
    //         //->leftjoin('Application\Entity\T072fvoucherDetails','vd', 'with', 'vd.f072fidVoucher = v.f071fid')
    //         ->where('pd.f094fidPayment = :paymentId')
    //         ->setParameter('paymentId',(int)$voucher);

    //     $bill_registration_result = $qb1->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	
    //     foreach ($bill_registration_result as $row) {
    //         if($row['f094fpaidAmount'] >= $row['f093ftotalAmount']){
    //             $id_bill = $row['f094idBillRegistration'];
    //              $query  = "update t091fbill_registration set f091fpayment_status = 1 where f091fid=$id_bill";
    //             $em->getConnection()->executeQuery($query);
    //         }
    //     }
    //     $totalAmount = 0;
    //     foreach($data as $item)
    //     {
    //         $totalAmount = $totalAmount + $item['f094fpaidAmount'];
                
    //     }
	
	   //  $em = $this->getEntityManager();
    //     $qb = $em->createQueryBuilder();

    //     $cashbank = new T096fcashBank();

    //     $cashbank->setF096fidBank((int)$data[0]['f093fidCompanyBank'])
    //         ->setF096fidPayment((int)$data[0]['f093fpaymentType'])
    //         ->setF096ftype("Voucher")
    //         ->setF096famount((float)$totalAmount)
    //         ->setF096ftransactionDate(new \DateTime($data[0]['f093fupdatedDtTm']))
    //         ->setF096fstatus((int) $data['f093fstatus'])
    //         ->setF096fapprovalStatus((int)0)
    //         ->setF096fcreatedBy((int)$_SESSION['userId'])
    //         ->setF096fupdatedBy((int)$_SESSION['userId']);

    //     $cashbank->setF096fcreatedDtTm(new \DateTime())
    //             ->setF096fupdatedDtM(new \DateTime());

    //     $em->persist($cashbank);

    //     $em->flush();
       


    //     foreach($results as $result)
    //     {
    //             $cashbankDetail = new T097fcashBankDetails();
    //             $cashbankDetail->setF097fidCashBank((int)$cashbank->getF096fid())
    //                            ->setF097fcrGlcodeId((int)"0")
    //                            ->setF097fdbGlcodeId((int)"0")
    //                            ->setF097fcreditAmount((float)"0")
    //                            ->setF097fdebitAmount((float)$result['f094fpaidAmount'])
    //                            ->setF097fstatus((int) $result['f093fstatus'])
    //                            ->setF097fcreatedBy((int)$_SESSION['userId'])
    //                            ->setF097fupdatedBy((int)$_SESSION['userId']);
                

    //             $cashbankDetail->setF097fcreatedDtTm(new \DateTime())
    //                               ->setF097fupdatedDtTm(new \DateTime());
            
    //             $em->persist($cashbankDetail);

    //             $em->flush();
    //     }
 
    //     }
    // }

    public function createPaymentVoucher($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='PV';
        $number = $configRepository->generateFIMS($numberData);
        $paymentVoucher = new T093fpaymentVoucher();

        $paymentVoucher->setF093fvoucherNo($number)
            ->setF093fpaymentType((int)$data['f093fpaymentType'])
            ->setF093fdescription($data['f093fdescription'])
            ->setF093fpaymentMode((int) $data['f093fpaymentMode'])
            ->setF093fpaymentRefNo($data['f093fpaymentRefNo'])
            ->setF093fidBank((int)$data['f093fidBank'])
            ->setF093fidCompanyBank((int)$data['f093fidCompanyBank'])
            ->setF093fsource($data['f093fsource'])
            ->setF093fapprovalStatus((int) $data['f093fapprovalStatus'])
            ->setF093ftotalAmount((float)$data['f093ftotalAmount'])
            ->setF093fidSupplier((int)$data['f093fidSupplier'])
            ->setF093fstatus((int)$data['f093fstatus'])
            ->setF093fcreatedBy((int)$_SESSION['userId']);

        $paymentVoucher->setF093fcreatedDtTm(new \DateTime());

        
            $em->persist($paymentVoucher);

            $em->flush();

        

        $paymentVoucherDetails = $data['paymentVoucher-details'];

        foreach ($paymentVoucherDetails as $paymentVoucherDetail) {
if((float)$paymentVoucherDetail['f094fpaidAmount'] > 0)
{
            $paymentVoucherDetailObj = new T094fpaymentVoucherDetails();

            
            $paymentVoucherDetailObj->setF094fidPayment((int)$paymentVoucher->getF093fid())
                ->setF094idBillRegistration((int) $paymentVoucherDetail['f094idBillRegistration'])
                ->setF094fcustomerName($paymentVoucherDetail['f094fcustomerName'])
                ->setF094fvoucherDate(new \DateTime($paymentVoucherDe['f094fvoucherDate']))
                ->setF094ftotalAmount((float)$paymentVoucherDetail['f093ftotalAmount'])
                ->setF094fpaidAmount((float) $paymentVoucherDetail['f094fpaidAmount'])
                ->setF094fstatus((int) $paymentVoucherDetail['f094fstatus'])
                ->setF094fcreatedBy((int)$_SESSION['userId']);
                

            $paymentVoucherDetailObj->setF094fcreatedDtTm(new \DateTime());
            
                $em->persist($paymentVoucherDetailObj);

                $em->flush();
                 $query2 = "update t091fbill_registration set f091fpayment_status = 1 where f091fid = ".$paymentVoucherDetail['f094idBillRegistration'];
            $em->getConnection()->executeQuery($query2);
            
        }
    }
        return $paymentVoucher;
    }

    public function updatePaymentVoucher($paymentVoucher, $data = [])
    {
        $em = $this->getEntityManager();
 

        $paymentVoucher->setF093fpaymentType((int)$data['f093fpaymentType'])
            ->setF093fdescription($data['f093fdescription'])
            ->setF093fpaymentMode((int) $data['f093fpaymentMode'])
            ->setF093fpaymentRefNo($data['f093fpaymentRefNo'])
            ->setF093fidBank((int)$data['f093fidBank'])
            ->setF093fidCompanyBank((int)$data['f093fidCompanyBank'])
            ->setF093fapprovalStatus((int) $data['f093fapprovalStatus'])
            ->setF093fsource($data['f093fsource'])
            ->setF093fidSupplier((int)$data['f093fidSupplier'])
            ->setF093ftotalAmount((float)$data['f093ftotalAmount'])
            ->setF093fstatus((int)$data['f093fstatus'])
            ->setF093fupdatedBy((int)$_SESSION['userId']);




        $paymentVoucher->setF093fupdatedDtTm(new \DateTime());

        
            $em->persist($paymentVoucher);

            $em->flush();

        
        return $paymentVoucher;
    }


    public function updatePaymentVoucherDetail($paymentVoucherDetailObj,$paymentVoucherDetail)
    {
        
        $em = $this->getEntityManager();

// print_r("hi");
// exit();
        
        // ->setF094idBillRegistration((int) $paymentVoucherDetail['f094idBillRegistration'])
                // ->setF094fcustomerName($paymentVoucherDetail['f094fcustomerName'])
                // ->setF094fvoucherDate(new \DateTime($paymentVoucherDetail['f094fvoucherDate']))
                // ->setF094ftotalAmount((float)$paymentVoucherDetail['f093ftotalAmount'])
        $paymentVoucherDetailObj->setF094fpaidAmount((float) $paymentVoucherDetail['f094fpaidAmount'])
                // ->setF094fstatus((int) $paymentVoucherDetail['f094fstatus'])
                ->setF094fupdatedBy((int)$_SESSION['userId']);
      
       $paymentVoucherDetailObj->setF094fupdatedDtTm(new \DateTime());
        
            $em->persist($paymentVoucherDetailObj);
            $em->flush();
//             print_r($paymentVoucherDetailObj);
// exit();

        
    }
    
    public function createPaymentVoucherDetail($paymentVoucherObj, $paymentVoucherDetail)
    {
        $em = $this->getEntityManager();

       $paymentVoucherDetailObj = new T094fpaymentVoucherDetails();

        $paymentVoucherDetailObj->setF094fidPayment($paymentVoucherObj->getF093fid())
                ->setF094idBillRegistration((int) $paymentVoucherDetail['f094idBillRegistration'])
                ->setF094fcustomerName($paymentVoucherDetail['f094fcustomerName'])
                ->setF094fvoucherDate(new \DateTime($paymentVoucherDetail['f094fvoucherDate']))
                ->setF094ftotalAmount((float)$paymentVoucherDetail['f093ftotalAmount'])
                ->setF094fpaidAmount((float) $paymentVoucherDetail['f094fpaidAmount'])
                ->setF094fstatus((int) $paymentVoucherDetail['f094fstatus'])
                ->setF094fcreatedBy((int)$_SESSION['userId'])
                ->setF094fupdatedBy((int)$_SESSION['userId']);
                

        $paymentVoucherDetailObj->setF094fcreatedDtTm(new \DateTime())
                ->setF094fupdatedDtTm(new \DateTime());
       
            $em->persist($paymentVoucherDetailObj);
            $em->flush();
        
    }

    public function getClaimcustomerVouchers($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fid, i.f071fvoucherNumber, i.f071fvoucherType, d.f011fdefinitionCode')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ->leftjoin('Application\Entity\T022fclaimCustomers', 'c', 'with', 'i.f071fidCustomer = c.f022fid')
            ->where('i.f071fidCustomer = :customerId')
            ->setParameter('customerId', (int)$id);
           
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;

    }

    public function getClaimcustomerType($id)
    {

        $em = $this->getEntityManager();
         
        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fidCustomer, c.f022ffirstName, c.f022flastName, d.f011fdefinitionCode, i.f071fvoucherNumber, i.f071fvoucherType')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ->leftjoin('Application\Entity\T022fclaimCustomers', 'c', 'with', 'i.f071fidCustomer = c.f022fid')
            ->groupBy('i.f071fidCustomer')
            ->where('i.f071fidCustomer = :typeId')
            ->setParameter('typeId', (int)$id);
            // ->setParameter('paymentTypeId',$data);
           
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;

    }

    public function getStudentVouchers($id)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fid, i.f071fvoucherNumber, i.f071fvoucherType, d.f011fdefinitionCode')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fidCustomer = :customerId')
            ->setParameter('customerId', (int)$id);
           
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;

    }

    public function getStudentType($id)
    {

        $em = $this->getEntityManager();
         
        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f071fidCustomer, s.f060fname, d.f011fdefinitionCode, i.f071fvoucherNumber, i.f071fvoucherType')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->groupBy('i.f071fidCustomer')
            ->where('i.f071fvoucherType = :typeId')
            ->setParameter('typeId', (int)$id);
            // ->setParameter('paymentTypeId',$data);
          
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;

    }

    public function getVoucherDetails($id)
    {

        $em = $this->getEntityManager();
         
        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('i.f071fid, i.f071fvoucherNumber, i.f071fvoucherType, d.f011fdefinitionCode')
            ->from('Application\Entity\T071fvoucher', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'd.f011fid = i.f071fvoucherType')
            ->where('i.f071fvoucherType = :typeId')
            ->setParameter('typeId', (int)$id);
            // ->setParameter('paymentTypeId',$data);
           
        $query   = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;

    }

    public function updateApprovalStatus($paymentVouchers,$status,$reason) 
    {
        $em = $this->getEntityManager(); 

        foreach ($paymentVouchers as $paymentVoucher) 
        {
            // print_r($paymentVoucher);exit;
            $paymentVoucher->setF093fapprovalStatus((int)$status)
                        ->setF093freason($reason)
                        ->setF093fUpdatedDtTm(new \DateTime());
       
            $em->persist($paymentVoucher);
            $em->flush();


            $Voucher=$paymentVoucher->getF093fid();
            $voucher = (int)$Voucher;
            // print_r($voucher);
            // exit();

            $query1 = "select * from t093fpayment_voucher where f093fid = $voucher";
            $voucher_result = $em->getConnection()->executeQuery($query1)->fetch();
            

            $query2 = "select *,SUM(f094fpaid_amount) as paid_amount, SUM(f094ftotal_amount) as total_amount from t094fpayment_voucher_details where f094fid_payment = $voucher group by f094fid_payment";
            $details_result = $em->getConnection()->executeQuery($query2)->fetchAll();
// print_r($details_result);
// die();
            $total = $details_result[0]['total_amount'];
            $paid = $details_result[0]['paid_amount'];
            
            $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();

            $cashbank = new T096fcashBank();

            $cashbank->setF096fidBank((int)$voucher_result['f093fid_company_bank'])
                    ->setF096fidPayment((int)$voucher_result['f093fpayment_type'])
                    ->setF096ftype("Voucher")
                    ->setF096famount((float)$total)
                    ->setF096ftransactionDate(new \DateTime($voucher_result['f093fupdated_dt_tm']))
                    ->setF096fstatus((int)$voucher_result['f093fstatus'])
                    ->setF096fapprovalStatus((int)0)
                    ->setF096fcreatedBy((int)$_SESSION['userId'])
                    ->setF096fupdatedBy((int)$_SESSION['userId']);

            $cashbank->setF096fcreatedDtTm(new \DateTime())
                    ->setF096fupdatedDtM(new \DateTime());

            $em->persist($cashbank);

            $em->flush();
       // print_r($cashbank);
       // die();
       
            foreach($details_result as $detail)
            {
                $cashbankDetail = new T097fcashBankDetails();

                $cashbankDetail->setF097fidCashBank((int)$cashbank->getF096fid())

                               ->setF097fdebitFundCode("NULL")
                               ->setF097fdebitDepartmentCode("NULL")
                               ->setF097fdebitActivityCode("NULL")
                               ->setF097fdebitAccountCode("NULL")

                                ->setF097fcreditFundCode("NULL")
                                ->setF097fcreditDepartmentCode("NULL")
                                ->setF097fcreditActivityCode("NULL")
                                ->setF097fcreditAccountCode("NULL")

                               ->setF097fcreditAmount((float)"0")
                               ->setF097fdebitAmount((float)$paid)
                               ->setF097fstatus((int) $detail['f093fstatus'])
                               ->setF097fcreatedBy((int)$_SESSION['userId'])
                               ->setF097fupdatedBy((int)$_SESSION['userId']);
                

                $cashbankDetail->setF097fcreatedDtTm(new \DateTime())
                               ->setF097fupdatedDtTm(new \DateTime());
            
                $em->persist($cashbankDetail);

                $em->flush();
                // print_r($cashbankDetail);
                // die();   
            }
 
        }

    }

    public function postGl($postData)
    {
        $em = $this->getEntityManager(); 

        $user = (int)$_SESSION['userId'];

        foreach ($postData['id'] as $id) 
        {

             $query1 = "select f093fid_company_bank, f093fpayment_type, f093ftotal_amount, f093fvoucher_no from t093fpayment_voucher where f093fid = $id";
            
            $voucher_result = $em->getConnection()->executeQuery($query1)->fetch();


            $uum = (int)$voucher_result['f093fid_company_bank'];
            $paymentType = (int)$voucher_result['f093fpayment_type'];
            $paymentAmount = (float)$voucher_result['f093ftotal_amount'];
            $voucherNo = (float)$voucher_result['f093fvoucher_no'];



            $query2 = "select f041ffund_code, f041fdepartment_code, f041factivity_code, f041faccount_code from t041fbank_setup_account where f041fid = $uum";
            $bankResult = $em->getConnection()->executeQuery($query2)->fetch();


            $JournalFund = $bankResult['f041ffund_code'];
            $JournalCode = $bankResult['f041fdepartment_code'];
            $JournalActivity = $bankResult['f041factivity_code'];
            $JournalAccount = $bankResult['f041faccount_code'];


            $query3 = "select f015fid from t015ffinancialyear where f015fstatus = 1";
            $yearResult = $em->getConnection()->executeQuery($query3)->fetch();

            $year = (int)$yearResult['f015fid'];




            $query3 = "select * from t140fglcode_setup where f140id_voucher_type = $paymentType";
            $glResult = $em->getConnection()->executeQuery($query3)->fetch();
            // print_r($glResult);exit;

            $Crfund = $glResult['f140fcredit_fund'];
            $Crdepartment = $glResult['f140fcredit_department'];
            $Cractivity = $glResult['f140fcredit_activity'];
            $Craccount = $glResult['f140fcredit_account'];
            $Dbtfund = $glResult['f140fdebit_fund'];
            $Dbtdepartment = $glResult['f140fdebit_department'];
            $Dbtactivity = $glResult['f140fdebit_activity'];
            $Dbtaccount = $glResult['f140fdebit_account'];
            $voucharType = $glResult['f140id_voucher_type'];
            // $paymentAmount = $glResult['f140fdebit_account'];

            switch ($voucharType)
            {
                case '136':
                   $module = 'GRN';
                    break;
                
                default:
                    # code...
                    break;
            }

                 $journalQuery = "INSERT INTO t017fjournal (f017fdescription,f017fmodule,f017ftotal_amount,f017fcreated_by,f017fupdated_by,f017fcreate_dt_tm,f017fupdate_dt_tm,f017fapprover,f017fapproved_status,f017fid_financial_year,f017freference_number) VALUES (
'$module','$module',$paymentAmount,$user,$user,getdate(),getdate(),0,0,$year,'$voucherNo')";
                // exit;

            $glResult = $em->getConnection()->executeQuery($journalQuery)->fetch();


             $journalGetQuery = " select top 1 f017fid from t017fjournal order by f017fid desc ";
            $glIdResult = $em->getConnection()->executeQuery($journalGetQuery)->fetch();

            $idGrn = (int)$glIdResult['f017fid'];


   $journValDetailQuery = "INSERT into t018fjournal_details (f018fid_journal,f018fso_code,f018fdr_amount,f018fcr_amount,f018fstatus,f018fcreated_by,f018fupdated_by,f018fcreate_dt_tm,f018fupdate_dt_tm,f018fdate_of_transaction,f018freference_number,f018ffund_code,f018fdepartment_code,f018factivity_code,f018faccount_code) VALUES ($idGrn,'NULL',$paymentAmount,0.00,0,1,0,getdate(),getdate(),getdate(),'$voucherNo','$Dbtfund','$Dbtdepartment','$Dbtactivity','$Dbtaccount')";

 
            $glResult = $em->getConnection()->executeQuery($journValDetailQuery)->fetch();


               $journalDetailQuery1 = "INSERT INTO t018fjournal_details (f018fid_journal,f018fso_code,f018fdr_amount,f018fcr_amount,f018fstatus,f018fcreated_by,f018fupdated_by,f018fcreate_dt_tm,f018fupdate_dt_tm,f018fdate_of_transaction,f018freference_number,f018ffund_code,f018fdepartment_code,f018factivity_code,f018faccount_code) VALUES (
$idGrn,'NULL',0.00,$paymentAmount,1,1,1,getdate(),getdate(),getdate(),'$voucherNo','$JournalFund','$JournalCode','$JournalActivity','$JournalAccount')";
            $glResult = $em->getConnection()->executeQuery($journalDetailQuery1)->fetch();

        }

    }

    public function getPaymentVoucherByVendor($data) 
    {
        // print_r($data);exit;
        $idBank = $data['idBank'];
        $idCompanyBank = $data['idCompanyBank'];
        $idSupplier = $data['idSupplier'];


        $em = $this->getEntityManager();

        // Query
        if (isset($idBank))
        {
            $idBank = (int)$idBank;
            $queryBank = " where f093fid_bank = $idBank";

        }
        if (isset($idCompanyBank))
        {

            $idCompanyBank = (int)$idCompanyBank;
            $queryCompanyBank = "and f093fid_company_bank = $idCompanyBank";

        }
        if (isset($idSupplier))
        {
            $idSupplier = (int)$idSupplier;
            $queryVendor = "and f093fid_supplier = $idSupplier";
        }

       $query = " SELECT p.f093fid, p.f093fvoucher_no as f093fvoucherNo, p.f093fpayment_type as f093fpayment_type, p.f093fdescription, p.f093fpayment_mode as f093fpaymentMode,  p.f093fpayment_ref_no as f093fpaymentRefNo, p.f093fstatus, p.f093fapproval_status as f093fapprovalStatus, p.f093fid_bank as f093fidBank, p.f093fid_company_bank as f093fidCompanyBank, p.f093fsource, p.f093fid_supplier as f093fidSupplier, p.f093ftotal_amount as f093ftotalAmount from t093fpayment_voucher as p $queryBank $queryCompanyBank $queryVendor";

        

            $result = $em->getConnection()->executeQuery($query)->fetchAll();


        $result1 =  array();
        foreach ($result as $item)
        {
            $payment_id = $item['f093fid'];
             $query1 = "select sum(f094ftotal_amount) as totalAmount from t094fpayment_voucher_details where f094fid_payment = $payment_id";
            $res = $em->getConnection()->executeQuery($query1)->fetch();
            $item['totalAmount'] = $res['totalAmount'];
            array_push($result1, $item);

        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }
}
