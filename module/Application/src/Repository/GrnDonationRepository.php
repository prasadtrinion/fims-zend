<?php
namespace Application\Repository;

use Application\Entity\T121fgrnDonation;
use Application\Entity\T121fgrnDonationDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class GrnDonationRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('g.f121fid, g.f121forderType, g.f121freferenceNumber, g.f121fapproveDate, g.f121fdonor, g.f121fvalue, g.f121fstatus')
            ->from('Application\Entity\T121fgrnDonation','g')
            // ->leftjoin('Application\Entity\T014fuser', 'u', 'with','d.f086fcreatedBy = u.f014fid')
            ->orderBy('g.f121fid','DESC');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f121fapproveDate'];
            $item['f121fapproveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('g.f121fid, g.f121forderType, g.f121freferenceNumber, g.f121fapproveDate, g.f121fdonor, g.f121fvalue, g.f121fstatus, gd.f121fidDetails, gd.f121fassetType, gd.f121ffundCode, gd.f121factivityCode, gd.f121fdepartmentCode, gd.f121faccountCode, gd.f121fquantity, gd.f121fprice, gd.f121ftotal, gd.f121fstatus as DetailStatus')
            ->from('Application\Entity\T121fgrnDonation','g')
            ->leftjoin('Application\Entity\T121fgrnDonationDetails', 'gd', 'with','g.f121fid = gd.f121fidDonation')
            // ->leftjoin('Application\Entity\T014fuser', 'u', 'with','d.f086fcreatedBy = u.f014fid')
            ->where('g.f121fid = :grnId')
            ->setParameter('grnId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f121fapproveDate'];
            $item['f121fapproveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function createGrnDonation($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="GRND";
        $number = $configRepository->generateFIMS($numberData);
// print_r($number);exit;

        $grnDonation = new T121fgrnDonation();


        $grnDonation->setF121forderType($data['f121forderType'])
                    ->setF121freferenceNumber($number)
                    ->setF121fapproveDate(new \DateTime($data['f121fapproveDate']))
                    ->setF121fdonor($data['f121fdonor'])
                    ->setF121fvalue((float)$data['f121fvalue'])
                    ->setF121fstatus((int)$data['f121fstatus'])
                    ->setF121fcreatedBy((int)$_SESSION['userId']);
                    

        $grnDonation->setF121fcreatedDtTm(new \DateTime());

        
        
        try
        { 

            $em->persist($grnDonation);
            // print_r($grnDonation);exit;

            $em->flush();

            
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        
        $grnDonationDetails = $data['grnDonation-details'];

        foreach ($grnDonationDetails as $grnDonationDetail)
        {


            $grnDonationDetailsObj = new T121fgrnDonationDetails();

            $grnDonationDetailsObj->setF121fidDonation((int)$grnDonation->getf121fid())
                                ->setF121fassetType($grnDonationDetail['f121fassetType'])
                                ->setF121ffundCode($grnDonationDetail['f121ffundCode'])
                                ->setF121factivityCode($grnDonationDetail['f121factivityCode'])
                                ->setF121fdepartmentCode($grnDonationDetail['f121fdepartmentCode'])
                                ->setF121faccountCode($grnDonationDetail['f121faccountCode'])
                                ->setF121fquantity((float)$grnDonationDetail['f121fquantity'])
                                ->setF121fprice((float)$grnDonationDetail['f121fprice'])
                                ->setF121ftotal((float)$grnDonationDetail['f121ftotal'])
                                ->setF121fstatus((int)$grnDonationDetail['f121fstatus'])
                                ->setF121fcreatedBy((int)$_SESSION['userId']);
                                

            $grnDonationDetailsObj->setF121fcreatedDtTm(new \DateTime());
        

        $em->persist($grnDonationDetailsObj);
        

        $em->flush();
        

            
        }
        return $grnDonation;
    }



    public function updateGrnDonation($grnDonation, $data = []) 
    {
        $em = $this->getEntityManager();

         $grnDonation->setF121forderType($data['f121forderType'])
                    ->setF121fapproveDate(new \DateTime($data['f121fapproveDate']))
                    ->setF121fdonor($data['f121fdonor'])
                    ->setF121fvalue((float)$data['f121fvalue'])
                    ->setF121fstatus((int)$data['f121fstatus'])
                    ->setF121fupdatedBy((int)$_SESSION['userId']);

        $grnDonation->setF121fupdatedDtTm(new \DateTime());
        
        $em->persist($grnDonation);
        $em->flush();
        // print_r($grnDonation);exit;

        return $grnDonation;
    }

    public function updateGrnDonationDetail($grnDonationDetailObj, $grnDonationDetail)
    {

        // print_r($grnDonationDetailObj);
        // print_r($grnDonationDetail); exit;
        $em = $this->getEntityManager();


         $grnDonationDetailObj->setF121fassetType($grnDonationDetail['f121fassetType'])
                                ->setF121ffundCode($grnDonationDetail['f121ffundCode'])
                                ->setF121factivityCode($grnDonationDetail['f121factivityCode'])
                                ->setF121fdepartmentCode($grnDonationDetail['f121fdepartmentCode'])
                                ->setF121faccountCode($grnDonationDetail['f121faccountCode'])
                                ->setF121fquantity((float)$grnDonationDetail['f121fquantity'])
                                ->setF121fprice((float)$grnDonationDetail['f121fprice'])
                                ->setF121ftotal((float)$grnDonationDetail['f121ftotal'])
                                ->setF121fstatus((int)$grnDonationDetail['f121fstatus'])
                                ->setF121fupdatedBy((int)$_SESSION['userId']);
                                

            $grnDonationDetailObj->setF121fupdatedDtTm(new \DateTime());

        $em->persist($grnDonationDetailObj);


        $em->flush();

            
        
    }
    
    public function createGrnDonationDetail($grnDonationObj, $grnDonationDetail)
    {
        $em = $this->getEntityManager();
        // print_r($grnDonationObj);exit;

        $grnDonationDetailsObj = new T121fgrnDonationDetails();

            $grnDonationDetailsObj->setF121fidDonation((int)$grnDonationObj->getF121fid())
                                ->setF121fassetType($grnDonationDetail['f121fassetType'])
                                ->setF121ffundCode($grnDonationDetail['f121ffundCode'])
                                ->setF121factivityCode($grnDonationDetail['f121factivityCode'])
                                ->setF121fdepartmentCode($grnDonationDetail['f121fdepartmentCode'])
                                ->setF121faccountCode($grnDonationDetail['f121faccountCode'])
                                ->setF121fquantity((int)$grnDonationDetail['f121fquantity'])
                                ->setF121fprice((float)$grnDonationDetail['f121fprice'])
                                ->setF121ftotal((float)$grnDonationDetail['f121ftotal'])
                                ->setF121fstatus((int)$grnDonationDetail['f121fstatus'])
                                ->setF121fcreatedBy((int)$_SESSION['userId']);
                                

            $grnDonationDetailsObj->setF121fcreatedDtTm(new \DateTime());
        

        $em->persist($grnDonationDetailsObj);

        $em->flush();

        return $grnDonationDetailsObj;
        
    }

    public function deleteGrnDonationDetail($id)
    {
        $em = $this->getEntityManager();
        $query = "delete from t121fgrn_donation_details where f121fid_details = $id";
        $em->getConnection()->executeQuery($query);
        return ;
    }
    
}

?>