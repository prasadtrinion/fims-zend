<?php
namespace Application\Repository;

use Application\Entity\T050fgenerateInvoice;
use Application\Entity\T067fintake;
use Application\Entity\T011fdefinationms;
use Application\Entity\T068fprogramme;
use Application\Entity\T060fstudent;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;


class GenerateInvoiceRepository extends EntityRepository 
{

	public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
         

        $qb->select('g.f050fcitizen, d.f011fdefinitionCode as citizen, g.f050fidIntake, i.f067fname as intake,  g.f050fmodeOfStudy, g.f050fidProgramme, p.f068fname as program, m.f011fdefinitionCode as studyMode, g.f050fidStudent, s.f060fname, s.f060femail, s.f060fphoneNumber, g.f050fstatus')
            ->from('Application\Entity\T050fgenerateInvoice', 'g')
            ->leftjoin('Application\Entity\T067fintake','i','with', 'g.f050fidIntake = i.f067fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'g.f050fcitizen = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','m','with', 'g.f050fmodeOfStudy = m.f011fid')
             ->leftjoin('Application\Entity\T068fprogramme','p','with', 'g.f050fidProgramme = p.f068fid')
             ->leftjoin('Application\Entity\T060fstudent','s','with', 'g.f050fidStudent = s.f060fid');
         
        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
	public function getListById($id)
	{
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();

		$qb->select('g.f050fcitizen, d.f011fdefinitionCode as citizen, g.f050fidIntake, i.f067fname as intake,  g.f050fmodeOfStudy, g.f050fidProgramme, p.f068fname as program, m.f011fdefinitionCode as studyMode, g.f050fidStudent, s.f060fname, s.f060femail, s.f060fphoneNumber, g.f050fstatus')
            ->from('Application\Entity\T050fgenerateInvoice', 'g')
            ->leftjoin('Application\Entity\T067fintake','i','with', 'g.f050fidIntake = i.f067fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with', 'g.f050fcitizen = d.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms','m','with', 'g.f050fmodeOfStudy = m.f011fid')
             ->leftjoin('Application\Entity\T068fprogramme','p','with', 'g.f050fidProgramme = p.f068fid')
             ->leftjoin('Application\Entity\T060fstudent','s','with', 'g.f050fidStudent = s.f060fid')
			->where('g.f050fid =:invoiceId')
			->setParameter('invoiceId', $id);

		$query = $qb->getQuery();
		$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

		return $result;
	}

	public function createGenerateInvoice($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $generate = new T050fgenerateInvoice();

        $generate->setF050fcitizen((int)$data['f050fcitizen'])
                ->setF050fidIntake((int)$data['f050fidIntake'])
                ->setF050fmodeOfStudy((int)$data['f050fmodeOfStudy'])
                ->setF050fidProgramme((int)$data['f050fidProgramme'])
                ->setF050fidStudent((int)$data['f050fidStudent'])
                ->setF050fstatus((int)$data['f050fstatus'])
                ->setF050fcreatedBy((int)$_SESSION['userId'])
                ->setF050fupdatedBy((int)$_SESSION['userId']);

        $generate->setF050fcreatedDtTm(new \DateTime())
                ->setF050fupdatedDtTm(new \DateTime());


        try{
            $em->persist($generate);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $generate;
    }

    public function updateGenerateInvoice($generate, $data = []) 
    {
        $em = $this->getEntityManager();

        $generate->setF050fcitizen((int)$data['f050fcitizen'])
                ->setF050fidIntake((int)$data['f050fidIntake'])
                ->setF050fmodeOfStudy((int)$data['f050fmodeOfStudy'])
                ->setF050fidProgramme((int)$data['f050fidProgramme'])
                ->setF050fidStudent((int)$data['f050fidStudent'])
                ->setF050fstatus((int)$data['f050fstatus'])
                ->setF050fcreatedBy((int)$_SESSION['userId'])
                ->setF050fupdatedBy((int)$_SESSION['userId']);

        $generate->setF050fupdatedDtTm(new \DateTime());


        
        $em->persist($generate);
        $em->flush();

    }
    
}
?>