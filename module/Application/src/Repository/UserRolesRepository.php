<?php
namespace Application\Repository;

use Application\Entity\T023fuserRoles;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class UserRolesRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('ur.f023fid,u.f014fid as f023fuserId,u.f014fuserName as f023fuserName, r.f021fid as f023froleId, r.f021froleName as f023froleName,ur.f023fstatus')
            ->from('Application\Entity\T023fuserRoles','ur')
            ->leftjoin('Application\Entity\T014fuser', 'u','with', 'u.f014fid=ur.f023fidUser')
            ->leftjoin('Application\Entity\T021frole', 'r','with', 'r.f021fid = ur.f023fidRole');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('ur.f023fid,u.f014fid as f023fuserId,u.f014fuserName as f023fuserName, r.f021fid as f023froleId, r.f021froleName as f023froleName,ur.f023fstatus')
            ->from('Application\Entity\T023fuserRoles','ur')
            ->leftjoin('Application\Entity\T014fuser', 'u','with', 'u.f014fid=ur.f023fidUser')
            ->leftjoin('Application\Entity\T021frole', 'r','with', 'r.f021fid = ur.f023fidRole')
            ->where('ur.f023fid = :userRolesId')
            ->setParameter('userRolesId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $userRoles = new T023fuserRoles();

        $userRoles->setF023fidRole($data['f023fidRole'])
             ->setF023fidUser($data['f023fidUser'])
             ->setF023fstatus($data['f023fstatus'])
             ->setF023fcreatedBy((int)$_SESSION['userId'])
             ->setF023fupdatedBy((int)$_SESSION['userId']);

        $userRoles->setF023fcreatedDtTm(new \DateTime())
                ->setF023fupdatedDtTm(new \DateTime());

 
        $em->persist($userRoles);
        $em->flush();

        return $userRoles;

    }

    public function updateData($userRoles, $data = []) 
    {
        $em = $this->getEntityManager();

        $userRoles->setF023fidRole($data['f023fidRole'])
             ->setF023fidUser($data['f023fidUser'])
             ->setF023fstatus($data['f023fstatus'])
             ->setF023fcreatedBy((int)$_SESSION['userId'])
             ->setF023fupdatedBy((int)$_SESSION['userId']);

        $userRoles->setF023fupdatedDtTm(new \DateTime());
        
        $em->persist($userRoles);
        $em->flush();

    }



    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>