<?php
namespace Application\Repository;

use Application\Entity\T055fincrementMaster;
use Application\Entity\T055fincrementDetails;
use Application\Entity\T015fdepartment;
use Application\Entity\T015ffinancialyear;
use Application\Entity\T014fglcode;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityRepository;

class BudgetIncrementRepository extends EntityRepository
{
  public function getIncrement() 
  {

    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();

    $qb->select('am.f055fid, am.f055fdepartment,am.f055ffund,fu.f057fname as fundName, am.f055fidFinancialYear, am.f055ftotalAmount, am.f055fapprovalStatus, am.f055fstatus,  d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear')
       ->from('Application\Entity\T055fincrementMaster', 'am')
       ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f055fdepartment = d.f015fdepartmentCode')
       ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f055fidFinancialYear = f.f110fid')
       ->leftjoin('Application\Entity\T057ffund','fu', 'with','am.f055ffund = fu.f057fcode')
       ->orderBy('am.f055fid', 'DESC');
       // ->groupBy('bc.f055fidFinancialyear');
       // ->groupBy('bc.f055fidDepartment');

    $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
    public function getListById($id)
    {
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();
        
           $qb->select('am.f055fid, am.f055fdepartment,am.f055ffund, am.f055fidFinancialYear, am.f055ftotalAmount, am.f055fapprovalStatus, am.f055fstatus,  ad.f055fadjustmentType, d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear, ad.f055fidDetails, ad.f055famount, ad.f055fq1, ad.f055fq2, ad.f055fq3, ad.f055fq4, ad.f055ffundCode, ad.f055faccountCode, ad.f055factivityCode, ad.f055fdepartmentCode, ad.f055fverimentAmount')
       ->from('Application\Entity\T055fincrementMaster', 'am')
        ->leftjoin('Application\Entity\T055fincrementDetails', 'ad', 'with','am.f055fid = ad.f055fidMaster')
       ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f055fdepartment = d.f015fdepartmentCode')
       ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f055fidFinancialYear = f.f110fid')
        ->where('am.f055fid = :budgetIncrementId')
            ->setParameter('budgetIncrementId',$id)
       ->andwhere('ad.f055fstatus != 2');
            
    $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
    
  }

  public function getListByApprovedStatus($approvedStatus)
    {
    $em = $this->getEntityManager();

    $qb = $em->createQueryBuilder();
        
    // echo("hi");
    // exit;
           if($approvedStatus == 0)
           {
               $qb->select("am.f055fid, am.f055fdepartment,am.f055ffund,fu.f057fname as fundName, am.f055fidFinancialYear, am.f055ftotalAmount, am.f055fapprovalStatus, am.f055fstatus,  d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode,(d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName, f.f110fyear")
       ->from('Application\Entity\T055fincrementMaster', 'am')
       ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f055fdepartment = d.f015fdepartmentCode')
       ->leftjoin('Application\Entity\T057ffund','fu', 'with','am.f055ffund = fu.f057fcode')
       ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f055fidFinancialYear = f.f110fid')
                 ->where('am.f055fapprovalStatus = 0');
                 // ->groupBy('bc.f055fidDepartment,bc.f055fidFinancialyear');
            }

            elseif($approvedStatus == 1)
            {
               $qb->select("am.f055fid, am.f055fdepartment,am.f055ffund,fu.f057fname as fundName, am.f055fidFinancialYear, am.f055ftotalAmount, am.f055fapprovalStatus, am.f055fstatus,  d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, f.f110fyear, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName")
       ->from('Application\Entity\T055fincrementMaster', 'am')
       ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f055fdepartment = d.f015fdepartmentCode')
       ->leftjoin('Application\Entity\T057ffund','fu', 'with','am.f055ffund = fu.f057fcode')
       ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f055fidFinancialYear = f.f110fid')
          ->where('am.f055fapprovalStatus = 1');
          // ->groupBy('bc.f055fidDepartment,bc.f055fidFinancialyear');
            }

            elseif($approvedStatus == 2) 
            {
               $qb->select("am.f055fid, am.f055fdepartment,am.f055ffund, fu.f057fname as fundName,am.f055fidFinancialYear, am.f055ftotalAmount, am.f055fapprovalStatus, am.f055fstatus,  d.f015fdepartmentName, d.f015funit, d.f015fdeptCode, d.f015fdepartmentCode, (d.f015fdepartmentCode+' - '+d.f015fdepartmentName) as originalName, f.f110fyear")
       ->from('Application\Entity\T055fincrementMaster', 'am')
       ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with','am.f055fdepartment = d.f015fdepartmentCode')
       ->leftjoin('Application\Entity\T057ffund','fu', 'with','am.f055ffund = fu.f057fcode')
       ->leftjoin('Application\Entity\T110fbudgetyear','f', 'with','am.f055fidFinancialYear = f.f110fid');
       // ->groupBy('bc.f055fidDepartment,bc.f055fidFinancialyear');
          
            }
          
    $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
    return $data;
    
  }

  public function createIncrement($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $increment = new T055fincrementMaster();

        $increment->setF055fdepartment($data['f055fdepartment'])
                  ->setF055fidFinancialYear((int)$data['f055fidFinancialyear'])
                 ->setF055ffund($data['f055ffund'])
                 ->setF055ftotalAmount((float)$data['f055ftotalAmount'])
               ->setF055fapprovalStatus((int)$data['f055fapprovalStatus'])
               ->setF055fstatus((int)$data['f055fstatus'])
               ->setF055fcreatedBy((int)$_SESSION['userId'])
               ->setF055fupdatedBy((int)$_SESSION['userId']);

    $increment->setF055fcreatedDtTm(new \DateTime())
                 ->setF055fupdatedDtTm(new \DateTime());
        
        $em->persist($increment);
        $em->flush();

        $incrementDetails = $data['increment-details'];

        foreach ($incrementDetails as $incrementDetail) {

            $incrementDetailObj = new T055fincrementDetails();
 
            $incrementDetailObj->setF055fidMaster((int)$increment->getF055fid())
                 ->setF055ffundCode( $incrementDetail['f054ffundCode'])
                  ->setF055faccountCode( $incrementDetail['f054faccountCode'])
                  ->setF055factivityCode( $incrementDetail['f054factivityCode'])
                  ->setF055fdepartmentCode( $incrementDetail['f054fdepartmentCode'])
                 ->setF055fverimentAmount((float)$incrementDetail['f055fverimentAmount'])
                 ->setF055fadjustmentType($incrementDetail['f055fadjustmentType'])
                 ->setF055famount((float)$incrementDetail['f054famount'])
               ->setF055fq1((float)$incrementDetail['f054fq1'])
               ->setF055fq2((float)$incrementDetail['f054fq2'])
               ->setF055fq3((float)$incrementDetail['f054fq3'])
               ->setF055fq4((float)$incrementDetail['f054fq4'])
               ->setF055fstatus((int)$incrementDetail['f055fstatus'])
               ->setF055fcreatedBy((int)$_SESSION['userId'])
               ->setF055fupdatedBy((int)$_SESSION['userId']);

      $incrementDetailObj->setF055fcreatedDtTm(new \DateTime())
                 ->setF055fupdatedDtTm(new \DateTime());



        $em->persist($incrementDetailObj);

        $em->flush();

         
        }
        return $increment;
    }

    public function updateIncrement($increment, $data = [])
    {
        $em = $this->getEntityManager();

       $increment->setF055fdepartment($data['f055fdepartment'])
                 ->setF055ffund($data['f055ffund'])
                 ->setF055fidFinancialYear((int)$data['f055fidFinancialyear'])
                 ->setF055ftotalAmount((float)$data['f055ftotalAmount'])
               ->setF055fapprovalStatus((int)$data['f055fapprovalStatus'])
               ->setF055fstatus((int)$data['f055fstatus'])
               ->setF055fupdatedBy((int)$_SESSION['userId']);

    $increment->setF055fupdatedDtTm(new \DateTime());
        
        $em->persist($increment);
        $em->flush();

        return $increment;
       
    }


    public function updateIncrementDetail($incrementDetailObj, $incrementDetail)
    {
        
        $em = $this->getEntityManager();

        $incrementDetailObj->setF055ffundCode( $incrementDetail['f054ffundCode'])
                  ->setF055faccountCode( $incrementDetail['f054faccountCode'])
                  ->setF055factivityCode( $incrementDetail['f054factivityCode'])
                  ->setF055fdepartmentCode( $incrementDetail['f054fdepartmentCode'])
                 ->setF055fadjustmentType($incrementDetail['f055fadjustmentType'])
                 ->setF055fverimentAmount((float)$incrementDetail['f055fverimentAmount'])
                 ->setF055famount((float)$incrementDetail['f054famount'])
               ->setF055fq1((float)$incrementDetail['f054fq1'])
               ->setF055fq2((float)$incrementDetail['f054fq2'])
               ->setF055fq3((float)$incrementDetail['f054fq3'])
               ->setF055fq4((float)$incrementDetail['f054fq4'])
               ->setF055fstatus((int)$incrementDetail['f055fstatus'])
               ->setF055fupdatedBy((int)$_SESSION['userId']);

      $incrementDetailObj->setF055fupdatedDtTm(new \DateTime());

        

        $em->persist($incrementDetailObj);

        $em->flush();

    }
    
    public function createIncrementDetail($incrementObj, $incrementDetail)
    {
        $em = $this->getEntityManager();
        // print_r($incrementObj);
        // exit();

         $incrementDetailObj = new T055fincrementDetails();

        $incrementDetailObj->setF055fidMaster($incrementObj->getF055fid())
               ->setF055ffundCode( $incrementDetail['f054ffundCode'])
                  ->setF055faccountCode( $incrementDetail['f054faccountCode'])
                  ->setF055factivityCode( $incrementDetail['f054factivityCode'])
                  ->setF055fdepartmentCode( $incrementDetail['f054fdepartmentCode'])
                 ->setF055famount((float)$incrementDetail['f054famount'])
                 ->setF055fadjustmentType($incrementDetail['f055fadjustmentType'])
                 ->setF055fverimentAmount((float)$incrementDetail['f055fverimentAmount'])
               ->setF055fq1((float)$incrementDetail['f054fq1'])
               ->setF055fq2((float)$incrementDetail['f054fq2'])
               ->setF055fq3((float)$incrementDetail['f054fq3'])
               ->setF055fq4((float)$incrementDetail['f054fq4'])
               ->setF055fstatus((int)$incrementDetail['f054fstatus'])
               ->setF055fcreatedBy((int)$_SESSION['userId'])
               ->setF055fupdatedBy((int)$_SESSION['userId']);

      $incrementDetailObj->setF055fcreatedDtTm(new \DateTime())
                 ->setF055fupdatedDtTm(new \DateTime());

        

        $em->persist($incrementDetailObj);

        $em->flush();

        // return $incrementDetailObj;
        
    }
   

  public function updateApprovalStatus($increments,$status,$reason) 
  {
    $em = $this->getEntityManager();

    foreach ($increments as $increment)
        {

          $Id = $increment->getF055fid();
          $id = (int)$Id;

          $query = "UPDATE t055fincrement_master set f055freason='$reason',f055fapproval_status=$status where f055fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);

          if($status==1)
          {
          $em = $this->getEntityManager();

          $qb = $em->createQueryBuilder();

          $qb->select('am.f055fid, ad.f055fdepartmentCode,ad.f055fadjustmentType,ad.f055ffundCode,ad.f055factivityCode,ad.f055faccountCode,ad.f055fverimentAmount, am.f055fidFinancialYear, ad.f055famount')
            ->from('Application\Entity\T055fincrementMaster', 'am')
        ->leftjoin('Application\Entity\T055fincrementDetails', 'ad', 'with','am.f055fid = ad.f055fidMaster')
        ->where('am.f055fid = :amountId')
            ->setParameter('amountId',$id);

          $query = $qb->getQuery();

          $result1 = $query->getResult();
            // print_r($result);
            // die();
            foreach($result1 as $result){
          $department = $result['f055fdepartmentCode'];
          $fund = $result['f055ffundCode'];
          $activity = $result['f055factivityCode'];
          $account = $result['f055faccountCode'];
          $idFinancialyear = $result['f055fidFinancialYear'];
          $amount=$result['f055fverimentAmount'];
          $type=$result['f055fadjustmentType'];
          $financialYear = (int)$idFinancialyear;
          $totalamount=(float)$amount;
          
          
            if($type == "Increment"){
               $tmp_query =  "f108fincrement_amount = f108fincrement_amount + $totalamount";
            }
            else{
                $tmp_query = "f108fdecrement_amount = f108fdecrement_amount + $totalamount";
            }
          $query = "update t108fbudget_summary set $tmp_query where f108fdepartment='$department' and f108ffund='$fund' and f108factivity='$activity' and f108faccount='$account' and f108fid_financial_year=$financialYear";
            // echo $query;
            // die();
          $em->getConnection()->executeQuery($query);

           $query = "update t108fbudget_summary set f108fbudget_pending = f108fallocated_amount + f108fincrement_amount - f108fveriment_from + f108fveriment_to -f108fbudget_expensed - f108fbudget_commitment - f108fdecrement_amount where f108fdepartment='$department' and f108ffund='$fund' and f108factivity='$activity' and f108faccount='$account' and f108fid_financial_year=$financialYear";
          $em->getConnection()->executeQuery($query);
        }
    }
        }
    }

    public function deleteIncrementStatus($increments) 
    {
        $em = $this->getEntityManager();

        foreach ($increments as $increment)
        {

            $increment->setF055fstatus((int)"0")
                      ->setF055fUpdatedDtTm(new \DateTime());
       
            $em->persist($increment);
            $em->flush();
        }
    }

     public function getIncrementAmount($idFinancialyear,$idDepartment)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('i.f055ftotalAmount')
            ->from('Application\Entity\T055fincrementMaster','i')
            ->where('i.department = :departmentId')
            ->setParameter('departmentId',$idDepartment)
            ->andwhere('i.f055fidFinancialYear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear);

        $query = $qb->getQuery();
        
        $result= array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

     public function updateIncrementDetailStatus($deletedIds) 
  {
    $em = $this->getEntityManager();

    foreach ($deletedIds as $deletedId) 
    {
            
        $query = "UPDATE t055fincrement_details set f055fstatus = 2 where f055fid_details = $deletedId"; 
        $result=$em->getConnection()->executeQuery($query);
    }
    return;
        // exit();
    }
   
    // public function updateApprovalStatus($increments) 
    // {
    //     $em = $this->getEntityManager();
        
    //     foreach ($increments as $increment) 
    //     {

    //     $increment->setF055fapprovalStatus((int)"1")
    //              ->setF055fUpdatedDtTm(new \DateTime());
       
    //     $em->persist($increment);
    //     $em->flush();

    //     $id=$increment->getF055fid();
    //     $financialyear=$increment->getF055fidFinancialYear();
    //     $department=$increment->getF055fidDepartment();
    //     $amount=$increment->getF055famount();

    //     $qb = $em->createQueryBuilder();
    //     $qb->select('b.f051famount')
    //         ->from('Application\Entity\T051fbudgetCostcenter','b')
    //         ->where('b.f051fidDepartment = :departmentId')
    //         ->andwhere('b.f051fidFinancialyear = :financialyearId')
    //         ->andwhere('b.f051fid = :id')
    //         ->setParameter('departmentId',$department)
    //         ->setParameter('financialyearId',$financialyear)
    //         ->setParameter('id',$id);
        
    //     $result = $qb->getQuery()->getResult();
        
    //     $finalAmount = $amount + $result['f051famount'];
        
    //     $update_query = "UPDATE t051fbudget_costcenter set f051famount = $finalAmount where f051fid_department=$department and f051fid_financialyear=$financialyear";
    //     $em->getConnection()->executeQuery($update_query);
        
        
    //     }
        
    // }


    public function deleteBudgetIncrementDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $query2 = "DELETE from t055fincrement_details WHERE f055fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
}

?>
