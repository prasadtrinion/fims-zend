<?php
namespace Application\Repository;

use Application\Entity\T064fcurrencyRateSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class CurrencyRateSetupRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('c.f064fid, c.f064fidCurrency, t.f062fcurCode as currencyCode, t.f062fcurDesc as currencyName, c.f064fexchangeRate, c.f064fminRate, c.f064fmaxRate, c.f064feffectiveDate, c.f064fstatus ')
            ->from('Application\Entity\T064fcurrencyRateSetup', 'c')
            ->leftjoin('Application\Entity\T062fcurrency','t', 'with', 'c.f064fidCurrency = t.f062fid')
            ->orderBy('c.f064fid','DESC');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $currency) {
            $date                        = $currency['f064feffectiveDate'];
            $currency['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $currency);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c.f064fid, c.f064fidCurrency, t.f062fcurCode as currencyCode, t.f062fcurDesc as currencyName, c.f064fexchangeRate, c.f064fminRate, c.f064fmaxRate, c.f064feffectiveDate, c.f064fstatus ')
            ->from('Application\Entity\T064fcurrencyRateSetup', 'c')
            ->leftjoin('Application\Entity\T062fcurrency', 't', 'with', 'c.f064fidCurrency = t.f062fid')
            ->where('c.f064fid = :currencyRateId')
            ->setParameter('currencyRateId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $currency) {
            $date                        = $currency['f064feffectiveDate'];
            $currency['f064feffectiveDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $currency);
        }

    return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $currencyRate = new T064fcurrencyRateSetup();

        $currencyRate->setF064fidCurrency((int)$data['f064fidCurrency'])
                ->setF064fexchangeRate((float)$data['f064fexchangeRate'])
                ->setF064fminRate((float)$data['f064fminRate'])
                ->setF064fmaxRate((float)$data['f064fmaxRate'])
                ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                ->setF064fstatus((int)$data['f064fstatus'])
                ->setF064fcreatedBy((int)$_SESSION['userId'])
                ->setF064fupdatedBy((int)$_SESSION['userId']);

        $currencyRate->setF064fcreateDtTm(new \DateTime())
                ->setF064fupdateDtTm(new \DateTime());


        $em->persist($currencyRate);
        $em->flush();
       
        return $currencyRate;
    }

    public function updateData($currencyRate, $data = []) 
    {
        $em = $this->getEntityManager();

        $currencyRate->setF064fidCurrency((int)$data['f064fidCurrency'])
                ->setF064fexchangeRate((float)$data['f064fexchangeRate'])
                ->setF064fminRate((float)$data['f064fminRate'])
                ->setF064fmaxRate((float)$data['f064fmaxRate'])
                ->setF064feffectiveDate(new \DateTime($data['f064feffectiveDate']))
                ->setF064fstatus((int)$data['f064fstatus'])
                ->setF064fcreatedBy((int)$_SESSION['userId'])
                ->setF064fupdatedBy((int)$_SESSION['userId']);

        $currencyRate->setF064fupdateDtTm(new \DateTime());
        
        $em->persist($currencyRate);
        $em->flush();

    }

}
