<?php
namespace Application\Repository;
 
use Application\Entity\T017fjournal;
use Application\Entity\T018fjournalDetails;
use Application\Entity\T018fcashBook;
use Application\Entity\T054fbudgetDepartmentActivityAllocation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;
use Zend\ServiceManager\ServiceManager;


class JournalRepository extends EntityRepository 
{
   
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fidFinancialYear, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus")
            ->from('Application\Entity\T017fjournal', 'j')
            // ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','j.f017fapprover = s.f034fstaffId')
            ->orderBy('j.f017fid','DESC');


        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        $result = array(
            'data' => $result,
        );
        

        return $result;
    
    }

    public function getListById($id) 
    {
        // print_r($id);exit();
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

                $qb->select("jd.f018fid, jd.f018ffundCode, jd.f018fdepartmentCode, jd.f018factivityCode, jd.f018faccountCode, j.f017fidFinancialYear, jd.f018fsoCode, jd.f018fdrAmount, jd.f018fcrAmount, jd.f018fstatus, j.f017fid, jd.f018fdateOfTransaction, j.f017fdescription, j.f017freferenceNumber, jd.f018freferenceNumber, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus,
                    s.f034fstaffId, s.f034fname")
            ->from('Application\Entity\T017fjournal','j')
            ->leftjoin('Application\Entity\T018fjournalDetails','jd', 'with', 'j.f017fid = jd.f018fidJournal')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with','j.f017fapprover = u.f014fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','u.f014fidStaff = s.f034fstaffId')
           ->where('j.f017fid = :journalId')
           ->orderby('j.f017fid','DESC')

            ->setParameter('journalId',(int)$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $journal) {
            $date                        = $journal['f018fdateOfTransaction'];
            $journal['f018fdateOfTransaction'] = date("Y-m-d", strtotime($date));
            array_push($result1, $journal);
        }

        return $result1;
    }
    public function getApprovedJournals($approvedStatus) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
            if($approvedStatus == 0 && isset($approvedStatus)){
                $qb->select('j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fidFinancialYear, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus, s.f034fname as originalName, s.f034fstaffId')
                ->from('Application\Entity\T017fjournal','j')
                 ->leftjoin('Application\Entity\T014fuser', 'u', 'with','j.f017fapprover = u.f014fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','u.f014fidStaff = s.f034fstaffId')
               ->where('j.f017fapprovedStatus = 0')
               ->orderby('j.f017fid','DESC')
               ;
            }
            elseif($approvedStatus == 1 ){
               $qb->select('j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fidFinancialYear, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus,s.f034fstaffId, s.f034fname as originalName')
                ->from('Application\Entity\T017fjournal','j')
                ->leftjoin('Application\Entity\T014fuser', 'u', 'with','j.f017fapprover = u.f014fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','u.f014fidStaff = s.f034fstaffId')
                ->where('j.f017fapprovedStatus = 1')
                ->orderby('j.f017fid','DESC')
                ;
            }
            elseif ($approvedStatus == 2 ) {
               $qb->select('j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fidFinancialYear, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus, s.f034fstaffId, s.f034fname as originalName')
                ->from('Application\Entity\T017fjournal','j')
                ->leftjoin('Application\Entity\T014fuser', 'u', 'with','j.f017fapprover = u.f014fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','u.f014fidStaff = s.f034fstaffId')
                ->where('j.f017fapprovedStatus = 2')
                ->orderby('j.f017fid','DESC')
                ;
            }
            elseif ($approvedStatus == 3 ) {
               $qb->select('j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fidFinancialYear, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount, j.f017fapprovedStatus, s.f034fstaffId, s.f034fname as originalName')
                ->from('Application\Entity\T017fjournal','j')
                 ->leftjoin('Application\Entity\T014fuser', 'u', 'with','j.f017fapprover = u.f014fid')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with','u.f014fidStaff = s.f034fstaffId')
             ->orderby('j.f017fid','DESC')
             ; 
            }
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;
    }

    public function createNewData($data) 
    {

        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       
        $journal = new T017fjournal();

        $journal->setF017fdescription($data['f017fdescription'])
                ->setF017fmodule($data['f017fmodule'])
                ->setF017fapprover((int)$data['f017fapprover'])
                ->setF017ftotalAmount((float)$data['f017ftotalAmount'])
                ->setF017fapprovedStatus((int)$data['f017fapprovedStatus'])
                ->setF017freferenceNumber($data['f017freferenceNumber'])
                // ->setF017fidFinancialYear((int)"1")
                ->setF017fidFinancialYear((int)$data['f017fidFinancialYear'])
                ->setF017fcreatedBy((int)$_SESSION['userId'])
                ->setF017fupdatedBy((int)$_SESSION['userId']);

        $journal->setF017fcreateDtTm(new \DateTime())
                ->setF017fupdateDtTm(new \DateTime());


        try{
            $em->persist($journal);

            $em->flush();

        }
        catch(\Exception $e){
            echo $e;
        }

        $journalDetails = $data['journal-details'];

        foreach ($journalDetails as $journalDetail) {
        try{
            $glcode=(int)$journalDetail['f018fidGlcode'];
//INVOICE CRON
//             $query1 = "select * from t041fbank_setup_account where f041fid_glcode = $glcode "; 
//             $result=$em->getConnection()->executeQuery($query1)->fetch();
//             if($result){
//                 $cashBook = new T018fcashBook();
//                 $cashBook ->setF018ffundCode( $journalDetail['f018ffundCode'])
//                 ->setF018faccountCode( $journalDetail['f018faccountCode'])
//                 ->setF018factivityCode( $journalDetail['f018factivityCode'])
//                 ->setF018fdepartmentCode( $journalDetail['f018fdepartmentCode'])
//                 ->setF018fdrAmount((float)$journalDetail['f018fdrAmount'])
//                     ->setF018freferenceNumber($journalDetail['f018freferenceNumber'])
//                   ->setF018fcrAmount((float)$journalDetail['f018fcrAmount'])
//                   ->setF018fstatus((int)$data['f017fapprovedStatus'])
//                   ->setF018fcreatedBy((int)"1")
//                   ->setF018fupdatedBy((int)"1");
 
// $cashBook->setF018fcreateDtTm(new \DateTime())
//                  ->setF018fupdateDtTm(new \DateTime())
//                 ->setF018fdateOfTransaction(new \DateTime($data['f018fdateOfTransaction']));
//                 $em->persist($cashBook);
            
//                 $em->flush();
//             }
//INVOICE CRON END
           
        // $glcode = $journalDetail['f018fidGlcode'];
        // $financialYear = $data['financialYear'];
        // $qb = $em->createQueryBuilder();
        // $qb->select('ac.f059fallowNegative,ba.f054famount,ba.f054fid,ac.f059fparentCode')
        //     ->from('Application\Entity\T059faccountCode', 'ac')
        //     ->leftjoin('Application\Entity\T014fglcode', 'gl','with', 'ac.f059fid = gl.f014faccountId')
        //     ->leftjoin('Application\Entity\T054fbudgetActivity', 'ba','with', 'ba.f054fidDepartment = gl.f014fdepartmentId')
        //     ->where('gl.f014fid = :glcode')
        //     ->andWhere('ba.f054fidFinancialyear = :financialYear')
        //     ->setParameter('glcode',(int)$glcode)
        //     ->setParameter('financialYear',(int)$financialYear);
        // $query = $qb->getQuery();
        // $result = $query->getOneorNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $allowNegative = (int)$result['f059fallowNegative'];
        // $budgetActivityId = (int)$result['f054fid'];
        // $amount = (float)$result['f054famount'];
        // $parentCode = (int)$result['f059fparentCode'];
        // $query1 = "select f059ftype from t059faccount_code where f059fid=(select f059fparent_code from t059faccount_code where f059fid = $parentCode ) "; 
        // $result1 = $em->getConnection()->executeQuery($query1)->fetch();
        // $type = $result1['f059ftype']; 
        // if($type == 'DR'){
        //     if($allowNegative == 0 ){
        //         if(($amount - $journalDetail['f018fdrAmount'] >= 0)){
        //             $query2 = "select f054fdr_amount, f054fbalance_amount from t054fbudget_depart_allocation where f054fid_budget_activity=$budgetActivityId order by f054fcreated_dt_tm DESC"; 
        //             $result2 = $em->getConnection()->executeQuery($query2)->fetch();
        //             $initialBalance = (float)$result2['f054fbalance_amount'];
        //             $initialDrAmount = (float)$result2['f054fdr_amount'];
        //             $finalBalance = $initialBalance - (float)$journalDetail['f018fdrAmount'];
        //             $finalDrAmount = $initialDrAmount + (float)$journalDetail['f018fdrAmount'];
        //             $query2 = "update t054fbudget_depart_allocation set f054fdr_amount =$finalDrAmount, f054fbalance_amount = $finalBalance where f054fid_budget_activity=$budgetActivityId "; 
        //             $em->getConnection()->executeQuery($query2);
                    

        //         }
        //         else
        //         {
        //             return 0;
        //         }
        //     }
        //     else {
        //         $query2 = "select f054fdr_amount, f054fbalance_amount from t054fbudget_depart_allocation where f054fid_budget_activity=$budgetActivityId order by f054fcreated_dt_tm DESC"; 
        //             $result2 = $em->getConnection()->executeQuery($query2)->fetch();
        //             $initialBalance = (float)$result2['f054fbalance_amount'];
        //             $initialDrAmount = (float)$result2['f054fdr_amount'];
        //             $finalBalance = $initialBalance - (float)$journalDetail['f018fdrAmount'];
        //             $finalDrAmount = $initialDrAmount + (float)$journalDetail['f018fdrAmount'];
        //             $query2 = "update t054fbudget_depart_allocation set f054fdr_amount =$finalDrAmount, f054fbalance_amount = $finalBalance where f054fid_budget_activity=$budgetActivityId "; 
        //             $em->getConnection()->executeQuery($query2);
        //     }
        // }
        // else
        // {
        
        //     $query2 = "select f054fcr_amount, f054fbalance_amount from t054fbudget_depart_allocation where f054fid_budget_activity=$budgetActivityId order by f054fcreated_dt_tm DESC"; 
        //             $result2 = $em->getConnection()->executeQuery($query2)->fetch();
        //             $initialBalance = $result2['f054fbalance_amount'];
        //             $initialCrAmount = $result2['f054fcr_amount'];
        //             $finalBalance = $initialBalance + $journalDetail['f018fcrAmount'];
        //             $finalCrAmount = $initialCrAmount + $journalDetail['f018fcrAmount'];
                    
        //             $query2 = "update t054fbudget_depart_allocation set f054fcr_amount =$finalCrAmount, f054fbalance_amount = $finalBalance where f054fid_budget_activity=$budgetActivityId "; 
        //             $em->getConnection()->executeQuery($query2);
        
        // }
            
            $qb = $em->createQueryBuilder();

            $qb->select('g.f014ffundId, g.f014faccountId')
            ->from('Application\Entity\T014fglcode','g')
            ->where('g.f014fid = :glcodeId')
            ->setParameter('glcodeId',(int)$glcode);
            
            $query = $qb->getQuery();

            $result = $query->getResult();
    
            
            $journalDetailObj = new T018fjournalDetails();
            $journalDetailObj->setF018ffundCode( $journalDetail['f018ffundCode'])
            ->setF018faccountCode( $journalDetail['f018faccountCode'])
            ->setF018factivityCode( $journalDetail['f018factivityCode'])
            ->setF018fdepartmentCode( $journalDetail['f018fdepartmentCode'])
                              ->setF018fsoCode($journalDetail['f018fsoCode'])
                              ->setF018fdrAmount((float)$journalDetail['f018fdrAmount'])
                            ->setF018freferenceNumber($journalDetail['f018freferenceNumber'])
                              ->setF018fcrAmount((float)$journalDetail['f018fcrAmount'])
                              ->setF018fstatus((int)$data['f017fapprovedStatus'])
                              ->setF018fcreatedBy((int)$_SESSION['userId'])
                              ->setF018fupdatedBy((int)$_SESSION['userId'])
                              ->setF018fidJournal($journal);
             
            $journalDetailObj->setF018fcreateDtTm(new \DateTime())
                             ->setF018fupdateDtTm(new \DateTime())
                            ->setF018fdateOfTransaction(new \DateTime($journalDetail['f018fdateOfTransaction']));
                
            $em->persist($journalDetailObj);
            
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }
        return $journal;
    }

    public function updateJournal($journal, $data = []) 
    {
        $em = $this->getEntityManager();

        $journal->setF017fdescription($data['f017fdescription'])
                ->setF017fmodule($data['f017fmodule'])
                ->setF017fapprover((int)$data['f017fapprover'])
                ->setF017ftotalAmount((float)$data['f017ftotalAmount'])
                ->setF017fapprovedStatus((int)$data['f017fapprovedStatus'])
                // ->setF017fidFinancialYear((int)"1")
                ->setF017fupdatedBy((int)$_SESSION['userId']);

        $journal->setF017fupdateDtTm(new \DateTime());


        try{
            $em->persist($journal);

            $em->flush();

        }
        catch(\Exception $e){
            echo $e;
        }

    }
    public function createDetail($journal,$journalDetail){

        $em = $this->getEntityManager();

         $journalDetailObj = new T018fjournalDetails();
            $journalDetailObj->setF018ffundCode( $journalDetail['f018ffundCode'])
                             ->setF018faccountCode( $journalDetail['f018faccountCode'])
                             ->setF018factivityCode( $journalDetail['f018factivityCode'])
                             ->setF018fdepartmentCode( $journalDetail['f018fdepartmentCode'])
                             ->setF018fsoCode($journalDetail['f018fsoCode'])
                             ->setF018fdrAmount((float)$journalDetail['f018fdrAmount'])
                             ->setF018freferenceNumber($journalDetail['f018freferenceNumber'])
                             ->setF018fcrAmount((float)$journalDetail['f018fcrAmount'])
                             ->setF018fstatus((int)$data['f017fapprovedStatus'])
                             ->setF018fcreatedBy((int)$_SESSION['userId'])
                             ->setF018fupdatedBy((int)$_SESSION['userId'])
                             ->setF018fidJournal($journal);
             
            $journalDetailObj->setF018fcreateDtTm(new \DateTime())
                             ->setF018fupdateDtTm(new \DateTime())
                            ->setF018fdateOfTransaction(new \DateTime($journalDetail['f018fdateOfTransaction']));
                
            $em->persist($journalDetailObj);
            
            $em->flush();
    }

    public function updateDetail($journalDetailObj,$journalDetail)
    {

        $em = $this->getEntityManager();
        
            $journalDetailObj->setF018ffundCode( $journalDetail['f018ffundCode'])
                             ->setF018faccountCode( $journalDetail['f018faccountCode'])
                             ->setF018factivityCode( $journalDetail['f018factivityCode'])
                             ->setF018fdepartmentCode( $journalDetail['f018fdepartmentCode'])
                             ->setF018fsoCode($journalDetail['f018fsoCode'])
                             ->setF018fdrAmount((float)$journalDetail['f018fdrAmount'])
                             ->setF018freferenceNumber($journalDetail['f018freferenceNumber'])
                             ->setF018fcrAmount((float)$journalDetail['f018fcrAmount'])
                             ->setF018fstatus((int)$data['f017fapprovedStatus'])
                             ->setF018fupdatedBy((int)$_SESSION['userId']);
             
            $journalDetailObj->setF018fupdateDtTm(new \DateTime())
                             ->setF018fdateOfTransaction(new \DateTime($journalDetail['f018fdateOfTransaction']));
            try
            {  
                $em->persist($journalDetailObj);
                $em->flush();
            }
            catch(\Exception $e)
            {
                echo $e;
            }
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    public function approveJournals($journals)
    {
        $em = $this->getEntityManager();
        // $id = $data['']
        foreach ($journals as $id) {
            $query1 = "update t017fjournal set f017fapproved_status = 1 where f017fid = $id"; 
            $em->getConnection()->executeQuery($query1);
            $query1 = "update t018fjournal_details set f018fstatus = 1 where f018fid_journal = $id"; 
            $em->getConnection()->executeQuery($query1);

            // $query = "select SUM(f018fcr_amount) as f018fcr_amount,SUM(f018fdr_amount) as f018fdr_amount,f018faccount_code,f018ffund_code from t018fjournal_details where f018fid_journal = $id and f018fstatus = 1 group by f018faccount_code,f018ffund_code"; 
            // $result = $em->getConnection()->executeQuery($query)->fetchAll();
            
            // foreach ($result as $row) {
                
            //     $idGlcode = (int)$row['f018fid_glcode'];
            //     $query = "select f016fbalance_cr_amount,f016fbalance_dr_amount from t016fopeningbalance where f016fid_glcode = $idGlcode"; 
            //     $result1 = $em->getConnection()->executeQuery($query)->fetch();
            //     $balance_cr_amount = (float)$result1['f016fbalance_cr_amount'] + (float)$row['f018fcr_amount'];
            //     $balance_dr_amount = (float)$result1['f016fbalance_dr_amount'] - (float)$row['f018fdr_amount'];
            //     $query = "update t016fopeningbalance set f016fbalance_cr_amount = $balance_cr_amount, f016fbalance_dr_amount = $balance_dr_amount  where f016fid_glcode = $idGlcode"; 
            //     $em->getConnection()->executeQuery($query);
            // }
        }

    }

    public function journalDescription($journals,$description)
    {
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();

        
        $journals = $journals[0];

        foreach ($journals as $journal) 
        {

            $id = $journal->getF017fid();
            
            $query1 = "UPDATE t017fjournal set f017fdescription='$description' where f017fid = $id"; 
            $result=$em->getConnection()->executeQuery($query1);


        }
    }

    public function createJournal($journalIds,$description,$reference)
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        

        foreach ($journalIds as $journalId) {
            
            $query = "UPDATE t017fjournal set f017fapproved_status=3 where f017fid = $journalId"; 
            $result=$em->getConnection()->executeQuery($query);
        $qb = $em->createQueryBuilder();

        $qb->select('j.f017fid, j.f017freferenceNumber, j.f017fdescription, j.f017fmodule, j.f017fapprover, j.f017ftotalAmount,j.f017fapprovedStatus')
            ->from('Application\Entity\T017fjournal', 'j')
            ->where('j.f017fid = :id')
            ->setParameter('id',(int)$journalId);


        $query = $qb->getQuery();
        $results  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        

        
        foreach ($results as $journals) 
        {
           
            $id=$journals['f017fid'];
            
           
            
            $journal = new T017fjournal();

            $journal->setF017fdescription($description)
                ->setF017fmodule($journals['f017fmodule'])
                ->setF017fapprover((int)$journals['f017fapprover'])
                ->setF017fidFinancialYear(1)
                // ->setF017fidFinancialYear((int)$data['f017fidFinancialYear'])
                ->setF017ftotalAmount((float)$journals['f017ftotalAmount'])
                ->setF017freferenceNumber($reference)
                ->setF017fapprovedStatus((int)'0')
                ->setF017fcreatedBy((int)$_SESSION['userId'])
                ->setF017fupdatedBy((int)$_SESSION['userId']);

            $journal->setF017fcreateDtTm(new \DateTime())
                ->setF017fupdateDtTm(new \DateTime());
            
            $em->persist($journal);

            $em->flush();

            $em = $this->getEntityManager();
            $qb1 = $em->createQueryBuilder();

            $qb1->select('jd.f018fid, jd.f018ffundCode,jd.f018fdepartmentCode,jd.f018factivityCode,jd.f018faccountCode, jd.f018fsoCode, jd.f018fdrAmount, jd.f018fcrAmount, jd.f018fstatus, jd.f018fdateOfTransaction, jd.f018freferenceNumber')
            ->from('Application\Entity\T018fjournalDetails','jd')
            // ->where('jd.f018fidJournal=$id');
            ->where('jd.f018fidJournal = :journalId')
            ->setParameter('journalId',(int)$id);

            $query1 = $qb1->getQuery();
            $details  = $query1->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

            foreach ($details as $detail) 
            {
        
                $journalDetail = new T018fjournalDetails();
               $journalDetail->setF018ffundCode( $detail['f018ffundCode'])
            ->setF018faccountCode( $detail['f018faccountCode'])
            ->setF018factivityCode( $detail['f018factivityCode'])
            ->setF018fdepartmentCode( $detail['f018fdepartmentCode'])
                              ->setF018fsoCode($detail['f018fsoCode'])
                              ->setF018fdrAmount((float)$detail['f018fcrAmount'])
                              ->setF018freferenceNumber($detail['f018freferenceNumber'])
                              ->setF018fcrAmount((float)$detail['f018fdrAmount'])
                              ->setF018fstatus((int)"0")
                              ->setF018fcreatedBy((int)$_SESSION['userId'])
                              ->setF018fupdatedBy((int)$_SESSION['userId'])
                              ->setF018fidJournal($journal);
             
            $journalDetail->setF018fcreateDtTm(new \DateTime())
                             ->setF018fupdateDtTm(new \DateTime())
                            ->setF018fdateOfTransaction(new \DateTime($detail['f018fdateOfTransaction']));

      
                $em->persist($journalDetail);
            
                $em->flush();
            }
        }
        
        }
        return $journal;
    }

    public function getCashBook() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f018fid,c.f018fdateOfTransaction,c.f018freferenceNumber,c.f018fdrAmount,c.f018fcrAmount,c.f018fstatus,c.f018fidGlcode')
            ->from('Application\Entity\T018fcashBook', 'c');


        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        $result = array(
            'data' => $result,
        );
        

        return $result;
    
    }
    
}
