<?php
namespace Application\Repository;

use Application\Entity\T086fassetDisposal;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetDisposalRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        // Query
       $qb->select('ad.f086fid, ad.f086fdisposalName, ad.f086fdescription, ad.f086fverificationStatus, ad.f086fstatus')
            ->from('Application\Entity\T086fassetDisposal','ad')
            // ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->orderBy('ad.f086fid','DESC');


        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
        
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       	$qb->select('ad.f086fid, ad.f086fdisposalName, ad.f086fdescription, ad.f086fverificationStatus, ad.f086fstatus')
            ->from('Application\Entity\T086fassetDisposal','ad')
            // ->leftjoin('Application\Entity\T087fassetCategory', 'c', 'with', 'a.f085fidAssetCategory = c.f087fid')
            ->where('ad.f086fid = :assetId')
            ->setParameter('assetId',(int)$id);



        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createAsset($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $asset = new T086fassetDisposal();
   
        $asset->setF086fdisposalName($data['f086fdisposalName'])
			->setF086fdescription($data['f086fdescription'])
			->setF086fidDepartment((int)$data['f086fverificationStatus'])
			->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fcreatedBy((int)$_SESSION['userId']);

        $asset->setF086fupdatedDtTm(new \DateTime());

        try{
        $em->persist($asset);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $asset;
    }

    public function updateAsset($asset,$data = []) 
    {
        $em = $this->getEntityManager();
        

        $asset->setF086fdisposalName($data['f086fdisposalName'])
			->setF086fdescription($data['f086fdescription'])
			->setF086fidDepartment((int)$data['f086fverificationStatus'])
			->setF086fstatus((int)$data['f086fstatus'])
             ->setF086fcreatedBy((int)$_SESSION['userId']);

        $asset->setF086fupdatedDtTm(new \DateTime());
        
        try
        {
        	$em->persist($asset);
        	$em->flush();
    	}
    	catch(\Exception $e)
    	{
        	echo $e;
    	}
        return $asset;

    }

   

    public function approveAssetUpdate($approvedStatus) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus))
           {
            $qb->select('ad.f086fid, ad.f086fdisposalName, ad.f086fdescription, ad.f086fverificationStatus, ad.f086fstatus')
            ->from('Application\Entity\T086fassetDisposal','ad')
            ->orderBy('ad.f086fid','DESC')
            ->where('ad.f086fverificationStatus =:assetId')
            ->setParameter('assetId',(int)$approvedStatus);
            
            }

            elseif($approvedStatus == 1 && isset($approvedStatus))
            {

            	$qb->select('ad.f086fid, ad.f086fdisposalName, ad.f086fdescription, ad.f086fverificationStatus, ad.f086fstatus')
            ->from('Application\Entity\T086fassetDisposal','ad')
            ->orderBy('ad.f086fid','DESC')
            ->where('ad.f086fverificationStatus =:assetId')
            ->setParameter('assetStatus',(int)$approvedStatus);
                 
            }

            elseif ($approvedStatus == 2 && isset($approvedStatus))
            {

            $qb->select('ad.f086fid, ad.f086fdisposalName, ad.f086fdescription, ad.f086fverificationStatus, ad.f086fstatus')
            ->from('Application\Entity\T086fassetDisposal','ad')
            ->orderBy('ad.f086fid','DESC');
                
            }
            
            
       $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

		
        
        return $result;

    }
   
}


