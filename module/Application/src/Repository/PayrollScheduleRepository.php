<?php
namespace Application\Repository;

use Application\Entity\T062fpayrollSchedule;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class PayrollScheduleRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f062fid, c.f062fpStart, c.f062fpEnd, c.f062fm, c.f062fr, c.f062fc1, c.f062fc2, c.f062fc3, c.f062fstatus')
            ->from('Application\Entity\T062fpayrollSchedule','c');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('c.f062fid, c.f062fpStart, c.f062fpEnd, c.f062fm, c.f062fr, c.f062fc1, c.f062fc2, c.f062fc3, c.f062fstatus')
            ->from('Application\Entity\T062fpayrollSchedule','c')
            ->where('c.f062fid = :scheduleId')
            ->setParameter('scheduleId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $schedule = new T062fpayrollSchedule();

        $schedule->setF062fpStart($data['f062fpStart'])
                ->setF062fpEnd($data['f062fpEnd'])
                ->setF062fm($data['f062fm'])
               ->setF062fr($data['f062fr'])
               ->setF062fc1($data['f062fc1'])
               ->setF062fc2($data['f062fc2'])
               ->setF062fc3($data['f062fc3'])
             ->setF062fstatus((int)$data['f062fstatus'])
             ->setF062fcreatedBy((int)$_SESSION['userId'])
             ->setF062fupdatedBy((int)$_SESSION['userId']);

        $schedule->setF062fcreatedDtTm(new \DateTime())
                ->setF062fupdatedDtTm(new \DateTime());

 
        $em->persist($schedule);
        $em->flush();
        
        return $schedule;

    }

    /* to edit the data in database*/

    public function updateData($schedule, $data = []) 
    {
        $em = $this->getEntityManager();

        $schedule->setF062fpStart($data['f062fpStart'])
                ->setF062fpEnd($data['f062fpEnd'])
                ->setF062fm($data['f062fm'])
               ->setF062fr($data['f062fr'])
               ->setF062fc1($data['f062fc1'])
               ->setF062fc2($data['f062fc2'])
               ->setF062fc3($data['f062fc3'])
             ->setF062fstatus((int)$data['f062fstatus'])
             ->setF062fcreatedBy((int)$_SESSION['userId'])
             ->setF062fupdatedBy((int)$_SESSION['userId']);

        $schedule->setF062fcreatedDtTm(new \DateTime())
                ->setF062fupdatedDtTm(new \DateTime());

 
        $em->persist($schedule);
        $em->flush();
        
        return $schedule;

    }

    public function getEmployeeDetails() {
        $query = "Select * from t106fmonthly_payroll";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    /*
    * Input EmpId
    *  Output - Total Amount Paid to Employee
    */
    public function totalRenumerationForEmployee($empId) {

        $query = "Select sum(a.MPD_PAID_AMT) as totalPaid, a.*,b.* from t109fmonthly_payroll_details_history as a, t081fsalary as b where a.MPD_INCOME_CODE=b.f081fcode and a.MPD_STAFF_ID='$empId' and b.f081ftax_deductable='1' and b.f081ftype_of_component='1'";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    /*
    * Input EmpId
    *  Output - Total Amount Paid to Employee
    */
    public function accumulatedEPFAndOtherFund($empId) {

        $query = "Select sum(a.MPD_PAID_AMT) as totalPaid, a.*,b.* from t109fmonthly_payroll_details_history as a, t081fsalary as b where a.MPD_INCOME_CODE=b.f081fcode and a.MPD_STAFF_ID='$empId'  and b.f081ftype_of_component='3'";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    public function accumulatedZakat(){
        $query = "Select sum(a.MPD_PAID_AMT) as totalPaid, a.*,b.* from t109fmonthly_payroll_details_history as a, t081fsalary as b where a.MPD_INCOME_CODE=b.f081fcode and a.MPD_STAFF_ID='$empId'  and b.f081fname='zakat'";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }


    public function accumulatedMTD(){
        $query = "Select sum(a.MP_ADD_TAXABLE_AMT) as totalPaid  from t108fmonthly_payroll_history as a";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
}

?>