<?php
namespace Application\Repository;

use Application\Entity\T134fstockRegistration;
use Application\Entity\T124fstockRegistration;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StockRegistrationMirrorRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('sr.f134fid, sr.f134fassetCategory, sr.f134fassetSubCategory, sr.f134fassetItem, sr.f134fstore, sr.f134fquantity, sr.f134fdate, sr.f134fstatus, i.f088fitemDescription, s.f097fstoreName, ic.f027fcategoryName , isc.f028fsubCategoryName')
            ->from('Application\Entity\T134fstockRegistration', 'sr')
            ->leftjoin('Application\Entity\T027fitemCategory', 'ic', 'with', 'ic.f027fid = sr.f134fassetCategory')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'isc.f028fid = sr.f134fassetSubCategory')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'sr.f134fassetItem = i.f088fid')
            ->leftjoin('Application\Entity\T097fstoreSetup', 's', 'with', 's.f097fid = sr.f134fstore')
            ->orderBy('sr.f134fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f134fdate'];
            $item['f134fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('sr.f134fid, sr.f134fassetCategory, sr.f134fassetSubCategory, sr.f134fassetItem, sr.f134fstore, sr.f134fquantity, sr.f134fdate, sr.f134fstatus, i.f088fitemDescription, s.f097fstoreName, ic.f027fcategoryName , isc.f028fsubCategoryName  ')
            ->from('Application\Entity\T134fstockRegistration', 'sr')
            ->leftjoin('Application\Entity\T027fitemCategory', 'ic', 'with', 'ic.f027fid = sr.f134fassetCategory')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'isc.f028fid = sr.f134fassetSubCategory')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'sr.f134fassetItem = i.f088fid')
            ->leftjoin('Application\Entity\T097fstoreSetup', 's', 'with', 's.f097fid = sr.f134fstore')
            ->where('sr.f134fid = :stockId')
            ->setParameter('stockId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $item) {
            $date                       = $item['f134fdate'];
            $item['f134fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $stockregistration = new T134fstockRegistration();

        $stockregistration->setF134fassetCategory((int)$data['f134fassetCategory'])
                ->setF134fassetSubCategory((int)$data['f134fassetSubCategory'])
                ->setF134fassetItem((int)$data['f134fassetItem'])
                ->setF134fstore((int)$data['f134fstore'])
                ->setF134fquantity((int)$data['f134fquantity'])
                ->setF134fdate(new \DateTime($data['f134fdate']))
                ->setF134fstatus((int)$data['f134fstatus']);
                // ->setF134fcreatedBy((int)$_SESSION['userId']);

        // $stockregistration->setF134fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($stockregistration);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $stockregistration;
    }

    public function updateData($stockregistration, $data = []) 
    {
        $em = $this->getEntityManager();
        

        $stockregistration->setF134fassetCategory((int)$data['f134fassetCategory'])
                ->setF134fassetSubCategory((int)$data['f134fassetSubCategory'])
                ->setF134fassetItem((int)$data['f134fassetItem'])
                ->setF134fstore((int)$data['f134fstore'])
                ->setF134fquantity((int)$data['f134fquantity'])
                ->setF134fdate(new \DateTime($data['f134fdate']))
                ->setF134fstatus((int)$data['f134fstatus'])
                ->setF134fupdatedBy((int)$_SESSION['userId']);

        $stockregistration->setF134fupdatedDtTm(new \DateTime());

        $em->persist($stockregistration);
        $em->flush();
    }

    public function getMirrorStockRegistrationActiveList($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('sr.f134fid, sr.f134fassetCategory, sr.f134fassetSubCategory, sr.f134fassetItem, sr.f134fstore, sr.f134fquantity, sr.f134fdate, sr.f134fstatus,  i.f088fitemDescription, s.f097fstoreName, ic.f027fcategoryName , isc.f028fsubCategoryName')
            ->from('Application\Entity\T134fstockRegistration', 'sr')
            ->leftjoin('Application\Entity\T027fitemCategory', 'ic', 'with', 'ic.f027fid = sr.f134fassetCategory')
            ->leftjoin('Application\Entity\T028fitemSubCategory', 'isc', 'with', 'isc.f028fid = sr.f134fassetSubCategory')
            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'sr.f134fassetItem = i.f088fid')
            ->leftjoin('Application\Entity\T097fstoreSetup', 's', 'with', 's.f097fid = sr.f134fstore')
            ->where('sr.f134fstatus = :stockId')
            ->setParameter('stockId',$id)
            ->orderBy('sr.f134fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f134fdate'];
            $item['f134fdate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function approveStockRegistrationMirrorData($data)
    {
    	// print_r($data);exit;
    	 $em = $this->getEntityManager();
        foreach ($data['stockId'] as $id) 
        {
        	// print_r($id);exit;
        	$id = (int)$id;

        	$query1 = "update t134fstock_registration set f134fstatus = 2, f134fupdated_dt_tm = getdate() where f134fid = $id";
        	$em->getConnection()->executeQuery($query1);





        	$queryAsset = " select * from t134fstock_registration where f134fid = $id";
        	$result = $em->getConnection()->executeQuery($queryAsset)->fetch();


        	$idAsset  = (int)$result['f134fasset_item'];
        	$qty  = (int)$result['f134fquantity'];

// print_r($result);exit;


        	$queryStock = " select * from t124fstock_registration where f124fasset_item = $idAsset";

        	$resultStock = $em->getConnection()->executeQuery($queryStock)->fetch();


        	// var_dump($resultStock);exit;

        	if ($resultStock)
        	{

        	$currentQty  = (int)$resultStock['f124fquantity'];
        	$idStock  = (int)$resultStock['f124fid'];

        		$totalQty = $currentQty + $qty;

        		$query1 = "update t124fstock_registration set f124fquantity = $totalQty, f124fupdated_dt_tm = getdate() where f124fid = $idStock";
        	$em->getConnection()->executeQuery($query1);


        	}
        	else
        	{

        	$dataForNewStock['f124fassetCategory'] = $result['f134fasset_category'];
        	$dataForNewStock['f124fassetSubCategory'] = $result['f134fasset_sub_category'];
        	$dataForNewStock['f124fassetItem'] = $result['f134fasset_item'];
        	$dataForNewStock['f124fstore'] = $result['f134fstore'];
        	$dataForNewStock['f124fquantity'] = $result['f134fquantity'];
        	$dataForNewStock['f124fdate'] = $result['f134fdate'];
        	// print_r($dataForNewStock);exit;

        	// $this->createNewData($dataForNewStock);


        $stockregistration = new T124fstockRegistration();

        $stockregistration->setF124fassetCategory((int)$dataForNewStock['f124fassetCategory'])
                ->setF124fassetSubCategory((int)$dataForNewStock['f124fassetSubCategory'])
                ->setF124fassetItem((int)$dataForNewStock['f124fassetItem'])
                ->setF124fstore((int)$dataForNewStock['f124fstore'])
                ->setF124fquantity((int)$dataForNewStock['f124fquantity'])
                ->setF124fdate(new \DateTime($dataForNewStock['f124fdate']))
                ->setF124fstatus((int)$dataForNewStock['f124fstatus'])
                ->setF124fcreatedBy((int)$_SESSION['userId']);

        $stockregistration->setF124fcreatedDtTm(new \DateTime());

       
            $em->persist($stockregistration);
            $em->flush();

        }
    }
        return 1;
        
    }
}

