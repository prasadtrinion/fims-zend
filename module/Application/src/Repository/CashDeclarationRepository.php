<?php
namespace Application\Repository;

use Application\Entity\T052fcashDeclaration;
use Application\Entity\T052fcashDeclarationDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class cashDeclarationRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('c.f052fidDeclare, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as budgetYear, c.f052fstatus, c.f052ftotal, c.f052freceiptNo, c.f052freceiptAmount, c.f052fidCash, ca.f052fcashAdvance as cashApplication')
            ->from('Application\Entity\T052fcashDeclaration','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->leftjoin('Application\Entity\T052fcashApplication', 'ca', 'with', 'c.f052fidCash = ca.f052fid')
            ->orderBy('c.f052fidDeclare','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    public function getApprovalList($status) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('c.f052fidDeclare, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as budgetYear, c.f052fstatus, c.f052ftotal, c.f052freceiptNo, c.f052freceiptAmount,c.f052fapprovalStatus, c.f052fidCash, ca.f052fcashAdvance as cashApplication')
            ->from('Application\Entity\T052fcashDeclaration','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->leftjoin('Application\Entity\T052fcashApplication', 'ca', 'with', 'c.f052fidCash = ca.f052fid')
            ->where('c.f052fapprovalStatus = :status')
            ->setParameter('status',(int)$status)
            ->orderBy('c.f052fidDeclare','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    // public function getListById($id) 
    // {
    //     $em = $this->getEntityManager();
    //     $qb = $em->createQueryBuilder();
    //     $qb->select('c.f052fid, c.f052fidCash, c.f052fcashAdvance, c.f052fidStaff, c.f052fidDepartment, c.f052fidFinancialYear, c.f052fstatus, cd.f052fidDetails, cd.f052fidCashType, cd.f052fitem, cd.f052ffund, cd.f052factivity, cd.f052fdepartment, cd.f052faccount, cd.f052fsoCode, cd.f052famount, cd.f052fquantity, cd. f052ftotalAmount, c.f052ftotal, c.f052freceiptAmount, c.f052freceiptNo')
    //         ->from('Application\Entity\T052fcashDeclaration','c')
    //         ->leftjoin('Application\Entity\T052fcashDeclarationDetails', 'cd', 'with', 'c.f052fid = cd.f052fidcashDeclaration')
    //         ->where('c.f052fidCash = :cashDeclarationId')
    //         ->setParameter('cashDeclarationId',$id);

    //     $query = $qb->getQuery();
    //     $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    //     return $result;
    // }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fidDeclare, c.f052fidCash, c.f052fcashAdvance, c.f052fidStaff, c.f052fidDepartment, c.f052fidFinancialYear, c.f052fstatus, cd.f052fidDeclareDetails, cd.f052fidCashType, cd.f052fitem, cd.f052ffund, cd.f052factivity, cd.f052fdepartment, cd.f052faccount, cd.f052fsoCode, cd.f052famount, cd.f052fquantity, cd. f052ftotalAmount,cd.f052frcpNo, c.f052ftotal, c.f052freceiptAmount, c.f052freceiptNo')
            ->from('Application\Entity\T052fcashDeclaration','c')
            ->leftjoin('Application\Entity\T052fcashDeclarationDetails', 'cd', 'with', 'c.f052fidDeclare = cd.f052fidCashDeclaration')
            ->where('c.f052fidCash = :cashDeclarationId')
            ->setParameter('cashDeclarationId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
   

    public function createCash($data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $cash = new T052fcashDeclaration();

        $cash->setF052fcashAdvance($data['f052fcashAdvance'])
             ->setF052fidStaff((int)$data['f052fidStaff'])
             ->setF052fidCash((int)$data['f052fidCash'])
             ->setF052fidDepartment($data['f052fidDepartment'])
             ->setF052fidFinancialYear((int)$data['f052fidFinancialYear'])
             ->setF052fstatus((int)$data['f052fstatus'])
             ->setF052fapprovalStatus((int)$data['f052fapprovalStatus'])
             ->setF052freceiptAmount((float)$data['f052freceiptAmount'])
             ->setF052freceiptNo($data['f052freceiptNo'])
             ->setF052ftotal((float)$data['f052ftotal'])
             ->setF052fcreatedBy((int)$_SESSION['userId'])
             ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cash->setF052fcreatedDtTm(new \DateTime())
                ->setF052fupdatedDtTm(new \DateTime());
        
        $em->persist($cash);
        $em->flush();

        return $cash;

    }
   public function updateCash($cash, $data = []) 
    {

        $em = $this->getEntityManager();

         $cash->setF052fcashAdvance($data['f052fcashAdvance'])
             ->setF052fidStaff((int)$data['f052fidStaff'])
             ->setF052fidCash((int)$data['f052fidCash'])
             ->setF052fidDepartment($data['f052fidDepartment'])
             ->setF052fidFinancialYear((int)$data['f052fidFinancialYear'])
             ->setF052fapprovalStatus((int)$data['f052fapprovalStatus'])
             ->setF052fstatus((int)$data['f052fstatus'])
             ->setF052freceiptAmount($data['f052freceiptAmount'])
             ->setF052freceiptNo($data['f052freceiptNo'])
             ->setF052ftotal($data['f052ftotal'])
             ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cash->setF052fupdatedDtTm(new \DateTime());
        try{
        $em->persist($cash);
        $em->flush();
        }
        catch(\Exception $e){
        	echo $e;
        }

        return $cash;
    }


    public function updateCashDetail($cashDetailObj, $cashDetail)
    {
        
        $em = $this->getEntityManager();

        $cashDetailObj->setF052fidCashType((int)$cashDetail['f052fidCashType'])
                       ->setF052fitem($cashDetail['f052fitem'])
                       ->setF052ffund($cashDetail['f052ffund'])
                       ->setF052factivity($cashDetail['f052factivity'])
                       ->setF052fdepartment($cashDetail['f052fdepartment'])
                       ->setF052faccount($cashDetail['f052faccount'])
                       ->setF052fsoCode((int)$cashDetail['f052fsoCode'])
                       ->setF052famount($cashDetail['f052famount'])
                       ->setF052fquantity((int)$cashDetail['f052fquantity'])
                       ->setF052ftotalAmount($cashDetail['f052ftotalAmount'])
                       ->setF052frcpNo($cashDetail['f052frcpNo'])
                       ->setF052fstatus((int)$cashDetail['f052fstatus'])
                       ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cashDetailObj->setF052fupdatedDtTm(new \DateTime());

        $em->persist($cashDetailObj);
        $em->flush();   
    }
    public function createCashDetail($cashObj, $cashDetail)
    {
        $em = $this->getEntityManager();

        $cashDetailObj = new T052fcashDeclarationDetails();

        $cashDetailObj->setF052fidcashDeclaration((int)$cashObj->getF052fidDeclare())
                       ->setF052fidCashType((int)$cashDetail['f052fidCashType'])
                       ->setF052fitem($cashDetail['f052fitem'])
                       ->setF052ffund($cashDetail['f052ffund'])
                       ->setF052factivity($cashDetail['f052factivity'])
                       ->setF052fdepartment($cashDetail['f052fdepartment'])
                       ->setF052faccount($cashDetail['f052faccount'])
                       ->setF052fsoCode((int)$cashDetail['f052fsoCode'])
                       ->setF052famount($cashDetail['f052famount'])
                       ->setF052fquantity((int)$cashDetail['f052fquantity'])
                       ->setF052ftotalAmount($cashDetail['f052ftotalAmount'])
                       ->setF052fstatus((int)$cashDetail['f052fstatus'])
                       ->setF052fcreatedBy((int)$_SESSION['userId'])
                       ->setF052fupdatedBy((int)$_SESSION['userId'])
                       ->setF052frcpNo($cashDetail['f052frcpNo']);

        $cashDetailObj->setF052fcreatedDtTm(new \DateTime())
                       ->setF052fupdatedDtTm(new \DateTime());
        $em->persist($cashDetailObj);
        $em->flush();

        return $cashDetailObj;   
    }


public function approve($data){
        $em = $this->getEntityManager();
        $status = $data['status'];
    foreach ($data['id'] as $id) {
        $query  = "update t052fcash_declaration set f052fapproval_status = $status where f052fid_declare = $id";
        $em->getConnection()->executeQuery($query);
    }
}
}
?>
