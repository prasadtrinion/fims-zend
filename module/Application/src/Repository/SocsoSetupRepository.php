<?php
namespace Application\Repository;

use Application\Entity\T091fsocsoSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SocsoSetupRepository extends EntityRepository 
{

    public function getList() 
    {
         

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder(); 


        $qb->select('s.f091fid, s.f091fcode, s.f091fcategory, s.f091fstartAmount, s.f091fendAmount, s.f091femployerContribution, s.f091femployerDeduction, s.f091fstatus,c.f091fcategoryName')
            ->from('Application\Entity\T091fsocsoSetup', 's')
            ->leftjoin('Application\Entity\T091socsoCategory', 'c', 'with','s.f091fcategory = c.f091fid')
            ->orderBy('s.f091fid','DESC');
       
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('s.f091fid, s.f091fcode, s.f091fcategory, s.f091fstartAmount, s.f091fendAmount, s.f091femployerContribution, s.f091femployerDeduction, s.f091fstatus')
            ->from('Application\Entity\T091fsocsoSetup', 's')
            ->where('s.f091fid = :socsoId')
            ->setParameter('socsoId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $socso = new T091fsocsoSetup();

        $socso->setF091fcode($data['f091fcode'])
                ->setF091fcategory((int)$data['f091fcategory'])
                ->setF091fstartAmount((float)$data['f091fstartAmount'])
                ->setF091fendAmount((float)$data['f091fendAmount'])
                ->setF091femployerContribution((float)$data['f091femployerContribution'])
                ->setF091femployerDeduction((float)$data['f091femployerDeduction'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $socso->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());


        try
        {
            $em->persist($socso);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $socso;
    }

    public function updateData($socso, $data = []) 
    {
        $em = $this->getEntityManager();

        $socso->setF091fcode($data['f091fcode'])
                ->setF091fcategory((int)$data['f091fcategory'])
                ->setF091fstartAmount((float)$data['f091fstartAmount'])
                ->setF091fendAmount((float)$data['f091fendAmount'])
                ->setF091femployerContribution((float)$data['f091femployerContribution'])
                ->setF091femployerDeduction((float)$data['f091femployerDeduction'])
                ->setF091fstatus((int)$data['f091fstatus'])
                ->setF091fcreatedBy((int)$_SESSION['userId'])
                ->setF091fupdatedBy((int)$_SESSION['userId']);

        $socso->setF091fcreatedDtTm(new \DateTime())
                ->setF091fupdatedDtTm(new \DateTime());

        
        $em->persist($socso);
        $em->flush();

    }

    
}