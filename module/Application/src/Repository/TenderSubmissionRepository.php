<?php
namespace Application\Repository;

use Application\Entity\T050ftenderSubmission;
use Application\Entity\T051ftenderCommitee;
use Application\Entity\T052ftenderStaff;
use Application\Entity\T053ftenderRemarks;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class TenderSubmissionRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList()  
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("distinct(tq.f035fid) as f035fid, tq.f035fquotationNumber, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle, tq.f035fsubmitted, tq.f035fshortlisted, tq.f035fawarded , (d.f015fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName ")
            ->from('Application\Entity\T035ftenderQuotation','tq')
            ->leftjoin('Application\Entity\T050ftenderSubmission', 'ts','with','ts.f050fidTendor = tq.f035fid')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','tq.f035fidDepartment = d.f015fdepartmentCode')
            ->orderBy('tq.f035fid','DESC');

        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor,tq.f035fquotationNumber,tq.f035fstartDate,tq.f035fopeningDate,tq.f035fendDate,tq.f035ftitle')
             ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            // ->leftjoin('Application\Entity\T051ftenderCommitee', 'tc','with','tc.f051fidSubmission = t.f050fid')
            // ->leftjoin('Application\Entity\T053ftenderRemarks', 'tr','with','tr.f053fidSubmission = t.f050fid')
            ->where('t.f050fidTendor = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $result = array();
        // foreach ($result1 as $tender) {
        //     $date                        = $tender['f035fstartDate'];
        //     $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
        //     $date                        = $tender['f035fendDate'];
        //     $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
        //     array_push($result, $tender);
        // }
        // print_r($result);
        // die();
        if($result){
        $qb = $em->createQueryBuilder();
                $qb->select('tc.f051fid,tc.f051fidTender,tc.f051fidStaff,tc.f051fdescription,tc.f051fstatus')
            ->from('Application\Entity\T051ftenderCommitee','tc')
            ->where('tc.f051fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $commitee_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR); 

        $qb = $em->createQueryBuilder();
                $qb->select('tr.f053fid, tr.f053fidSubmission, tr.f053fremarks,tr.f053fstatus')
            ->from('Application\Entity\T053ftenderRemarks','tr')
            ->where('tr.f053fidTendor = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $remark_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
         $qb = $em->createQueryBuilder();
        $qb->select('t.f035fid,t.f035fquotationNumber,d.f015fdepartmentName,t.f035fstartDate,t.f035fendDate,t.f035ftitle,t.f035fidDepartment,t.f035fidFinancialYear,t.f035ftotalAmount,t.f035ftenderStatus,t.f035ftype,t.f035fstatus, t.f035fopeningDate,t.f035fidCategory, t.f035fsubmitted, t.f035fshortlisted, t.f035fawarded ')
            ->from('Application\Entity\T035ftenderQuotation','t')
            ->leftjoin('Application\Entity\T015fdepartment','d', 'with','t.f035fidDepartment = d.f015fdepartmentCode')
           // ->leftjoin('Application\Entity\T027fitemCategory','i', 'with','t.f035fidCategory = i.f027fid')
            ->where('t.f035fid = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $tender_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }
        $final_result['vendor-details'] = $result;
        $final_result['commitee-details'] = $commitee_result;
        $final_result['remarks'] = $remark_result;
        $final_result['tender-details'] = $tender_result[0];
        // print_r($final_result);
        // die();
        return $final_result;

    }

     public function getTenderDetails($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor,tq.f035fquotationNumber,tq.f035fstartDate,tq.f035fendDate,tq.f035ftitle')
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            ->where('t.f050fidTendor = :tender')
            ->setParameter('tender',$id);
           
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;

    }

    public function getNewTenders() 
    {
        $em = $this->getEntityManager();

        $query1="select f035fid,f035fquotation_number as f035fquotationNumber from t035ftender_quotation where f035fid NOT IN (select f050fid_tendor from t050ftender_submission)";

                
                $result=$em->getConnection()->executeQuery($query1)->fetchAll();
        return $result;

    }

     public function shortlistedVendorsByTender($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor,tq.f035fquotationNumber,tq.f035fstartDate,tq.f035fopeningDate,tq.f035fendDate,tq.f035ftitle')
             ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            // ->leftjoin('Application\Entity\T051ftenderCommitee', 'tc','with','tc.f051fidSubmission = t.f050fid')
            // ->leftjoin('Application\Entity\T053ftenderRemarks', 'tr','with','tr.f053fidSubmission = t.f050fid')
            ->where('t.f050fidTendor = :tenderId')
            ->AndWhere('t.f050fisShortlisted = 1')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $result = array();
        // foreach ($result1 as $tender) {
        //     $date                        = $tender['f035fstartDate'];
        //     $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
        //     $date                        = $tender['f035fendDate'];
        //     $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
        //     array_push($result, $tender);
        // }
        // print_r($result);
        // die();
        if($result){
        $qb = $em->createQueryBuilder();
                $qb->select('tc.f051fid,tc.f051fidTender,tc.f051fidStaff,tc.f051fdescription,tc.f051fstatus')
            ->from('Application\Entity\T051ftenderCommitee','tc')
            ->where('tc.f051fidTender = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $commitee_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $qb = $em->createQueryBuilder();
                $qb->select('tr.f053fid, tr.f053fidSubmission, tr.f053fremarks,tr.f053fstatus')
            ->from('Application\Entity\T053ftenderRemarks','tr')
            ->where('tr.f053fidTendor = :tenderId')
            ->setParameter('tenderId',(int)$id);

        $query = $qb->getQuery();
        $remark_result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        }
        $final_result['vendor-details'] = $result;
        $final_result['commitee-details'] = $commitee_result;
        $final_result['remarks'] = $remark_result;
        // print_r($final_result);
        // die();
        return $final_result;

    }

    public function awardedVendorsByTender($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor,tq.f035fquotationNumber,tq.f035fstartDate,tq.f035fendDate,tq.f035ftitle')
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            ->where('t.f050fidTendor = :tenderId')
            ->AndWhere('t.f050fisAwarded = 1')
            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result, $tender);
        }
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f050fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('tc.f051fid,tc.f051fidTender,tc.f051fdescription,tc.f051fstatus')
            ->from('Application\Entity\T051ftenderCommitee','tc')
            ->where('tc.f051fidSubmission = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result[$i]['commitee-details'] = $result1;
}
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f050fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('ts.f052fid,ts.f052fidTender,ts.f052fidStaff,ts.f052fstatus')
            ->from('Application\Entity\T052ftenderStaff','ts')
            ->where('ts.f052fidSubmission = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result[$i]['staff-details'] = $result1;
}
        

        
        return $result;

    }   


    public function awardedTender() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor,tq.f035fquotationNumber,tq.f035fstartDate,tq.f035fendDate,tq.f035ftitle')
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq','with','t.f050fidTendor = tq.f035fid')
            ->AndWhere('t.f050fisAwarded = 1');
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result = array();
        foreach ($result1 as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result, $tender);
        }
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f050fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('tc.f051fid,tc.f051fidTender,tc.f051fdescription,tc.f051fstatus')
            ->from('Application\Entity\T051ftenderCommitee','tc')
            ->where('tc.f051fidSubmission = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result[$i]['commitee-details'] = $result1;
}
for ($i=0; $i <count($result) ; $i++) { 
    $id = $result[$i]['f050fid'];
    $qb = $em->createQueryBuilder();
                $qb->select('ts.f052fid,ts.f052fidTender,ts.f052fidStaff,ts.f052fstatus')
            ->from('Application\Entity\T052ftenderStaff','ts')
            ->where('ts.f052fidSubmission = :tenderId')

            ->setParameter('tenderId',(int)$id);
            $query = $qb->getQuery();
        $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result[$i]['staff-details'] = $result1;
}
        

        
        return $result;

    }   

    // public function createNewData($olddata) 
    // {

    //     $em = $this->getEntityManager();

    //     $data['f050fvendorCode'] = $olddata['vendor-details'][0]['f050fvendorCode'];
    //     $data['f050fgrade'] = $olddata['vendor-details'][0]['f050fgrade'];
    //     $data['f050famount'] = $olddata['vendor-details'][0]['f050famount'];
    //     $data['f050fstatus'] = 1;
    //     $data['f050fcompletionDuration'] = $olddata['vendor-details'][0]['f050fcompletion'];
    //     $data['f050fdescription'] = "NULL";
    //     $data['f050fidVendor'] = $olddata['vendor-details'][0]['f050fidVendor'];

    //     $data['commitee-details'][0]['f051fidTender'] = $olddata['f050fidTendor'];
    //     $data['commitee-details'][0]['f051fdescription'] = "NULL";
    //      for($i=0;$i<count($olddata['vendor-details']);$i++){
    //          $data['staff-details'][$i]['f052fidStaff'] = $olddata['commitee-details'][$i]['f038fidStaff'];
    //         $data['staff-details'][$i]['f052fidTender'] =  $olddata['f050fidTendor'];

    //     }
       

    //     $tenderSubmission = new T050ftenderSubmission();
                
    //     $tenderSubmission->setF050fidVendor((int)$data['f050fidVendor'])
    //          ->setF050fvendorCode($data['f050fvendorCode'])
    //          ->setF050fvendorName($data['f050fvendorName'])
    //          ->setF050fgrade($data['f050fgrade'])
    //          ->setF050famount((float)$data['f050famount'])
    //          ->setF050fcompletionDuration($data['f050fcompletionDuration'])
    //          ->setF050fdescription($data['f050fdescription'])
    //          ->setF050fisShortlisted((int)"0")
    //          ->setF050fisAwarded((int)"0")
    //          ->setF050fidTendor((int)$olddata['f050fidTendor'])
    //          ->setF050fstatus((int)$data['f050fstatus'])
    //          ->setF050fcreatedBy((int)$_SESSION['userId'])
    //          ->setF050fupdatedBy((int)$_SESSION['userId']);

    //     $tenderSubmission->setF050fcreatedDtTm(new \DateTime())
    //             ->setF050fupdatedDtTm(new \DateTime());

    //     try{
    //     $em->persist($tenderSubmission);

    //     $em->flush();
    //     }
    //     catch(\Exception $e){
    //         echo $e;
    //     }
    //     $commiteeDetails = $data['commitee-details'];
    //     foreach ($commiteeDetails as $detail) {
            
    //     $commiteeDetailObj = new T051ftenderCommitee();

    //     $commiteeDetailObj->setF051fidTender((int) $detail['f051fidTender'])
    //         ->setF051fdescription( $detail['f051fdescription'])
    //          ->setF051fstatus((int) $data['f050fstatus'])
    //         ->setF051fcreatedBy((int)$_SESSION['userId'])
    //         ->setF051fupdatedBy((int)$_SESSION['userId'])
    //         ->setF051fidSubmission($tenderSubmission);

    //     $commiteeDetailObj->setF051fcreatedDtTm(new \DateTime())
    //             ->setF051fupdatedDtTm(new \DateTime());

    //     try{
    //     $em->persist($commiteeDetailObj);
    //     $em->flush();
    //     }
    //     catch(\Exception $e){
    //         echo $e;
    //     }
    // }
    // $staffDetails = $data['staff-details'];
    //     foreach ($staffDetails as $detail) {
            
    //     $staffDetailObj = new T052ftenderStaff();

    //     $staffDetailObj->setF052fidTender((int) $detail['f052fidTender'])
    //         ->setF052fidStaff((int) $detail['f052fidStaff'])
    //          ->setF052fstatus((int) $data['f050fstatus'])
    //         ->setF052fcreatedBy((int)$_SESSION['userId'])
    //         ->setF052fupdatedBy((int)$_SESSION['userId'])
    //         ->setF052fidSubmission($tenderSubmission);

    //     $staffDetailObj->setF052fcreatedDtTm(new \DateTime())
    //             ->setF052fupdatedDtTm(new \DateTime());

    //     try{
    //     $em->persist($staffDetailObj);
    //     $em->flush();
    //     }
    //     catch(\Exception $e){
    //         echo $e;
    //     }
    // }
    // $remarks = $data['remarks'];
            
    //     $remarkDetailObj = new T053ftenderRemarks();

    //     $remarkDetailObj->setF053fremarks((int) $$remarks)
    //          ->setF053fstatus((int) $data['f050fstatus'])
    //         ->setF053fcreatedBy((int)$_SESSION['userId'])
    //         ->setF053fupdatedBy((int)$_SESSION['userId'])
    //         ->setF053fidSubmission($tenderSubmission);

    //     $remarkDetailObj->setF053fcreatedDtTm(new \DateTime())
    //             ->setF053fupdatedDtTm(new \DateTime());

    //     try{
    //     $em->persist($remarkDetailObj);
    //     $em->flush();
    //     }
    //     catch(\Exception $e){
    //         echo $e;
    //     }
    

      
    //     return $tenderSubmission;

    // }

       public function shortlistVendors($data) 
    {
        $em = $this->getEntityManager();
        $tenderId = $data['tendorId'];
        $query1 = "update t050ftender_submission set f050fis_shortlisted = 0 where f050fid_tendor = $tenderId";
        $em->getConnection()->executeQuery($query1);


        $query1 = "update t035ftender_quotation set f035fshortlisted = 1 WHERE f035fid = $tenderId";
        $em->getConnection()->executeQuery($query1);


        
        foreach ($data['id'] as $id) {
            try{
                $query1 = "update t050ftender_submission set f050fis_shortlisted = 1 where f050fid = $id";
                $em->getConnection()->executeQuery($query1);
               
            }
            catch(\Exception $e){
                echo $e;
            }
        }
    }

    public function awardVendors($postdata) 
    {
        $em = $this->getEntityManager();
        $tenderId = $postdata['idTendor'];
        $query1 = "update t035ftender_quotation set f035fawarded = 1 WHERE f035fid = $tenderId";
                $em->getConnection()->executeQuery($query1);
        $vendor = '';
        foreach ($postdata['id'] as $id)
        {
            
                $query1 = "update t050ftender_submission set f050fis_awarded = 1 where f050fid = $id";
                $em->getConnection()->executeQuery($query1);

                $query2 = "select f050fid_vendor from t050ftender_submission where f050fid = $id";
                $result = $em->getConnection()->executeQuery($query2)->fetch();
                $vendor = $result['f050fid_vendor'];
                // array_push($vendors,$result['f050fid_vendor']);
        }
        $tendor=$postdata['idTendor'];

        $agreementRepository = $em->getRepository("Application\Entity\T126ftenderAggrement");
        $agreementData['f126fidTender'] = $tendor;
        $agreementData['f126fidVender'] = $vendor;
        $agreementData['f126fstatus'] = 1;
        $agreementRepository->createNewData($agreementData);

        foreach($postdata['textdata'] as $data)
        {
                $text=$data['text'];
                $amount=$data['amount'];

                $query1="INSERT INTO t014faward_note(f014fid_tendor, f014ftext, f014famount)  
                    VALUES ($tendor, '$text', $amount)";

                
                $result=$em->getConnection()->executeQuery($query1);
        }

       
        return $result;
    }
        
    

    

     public function createNewData($olddata) 
    {

        $em = $this->getEntityManager();

        
        foreach($olddata['vendor-details'] as $data)
        {

            $tenderSubmission = new T050ftenderSubmission();
                
            $tenderSubmission->setF050fidVendor((int)$data['f050fidVendor'])
             ->setF050fvendorCode($data['f050fvendorCode'])
             ->setF050fvendorName($data['f050fvendorName'])
             ->setF050fgrade($data['f050fgrade'])
             ->setF050famount((float)$data['f050famount'])
             ->setF050fcompletionDuration($data['f050fcompletion'])
             ->setF050fdescription( $data['f050fdescription'])
             ->setF050fisShortlisted((int)"0")
             ->setF050fisAwarded((int)"0")
             ->setF050fidTendor((int)$olddata['f050fidTendor'])
             ->setF050fstatus((int)$data['f050fstatus'])
             ->setF050fcreatedBy((int)$_SESSION['userId'])
             ->setF050fupdatedBy((int)$_SESSION['userId']);

            $tenderSubmission->setF050fcreatedDtTm(new \DateTime())
                ->setF050fupdatedDtTm(new \DateTime());

            try{
            $em->persist($tenderSubmission);

            $em->flush();
            }
            catch(\Exception $e){
            echo $e;
            }
        }
        $commiteeDetails = $olddata['commitee-details'];
        foreach ($commiteeDetails as $detail)
        {
            
            $commiteeDetailObj = new T051ftenderCommitee();

            $commiteeDetailObj->setF051fidTender((int) $olddata['f050fidTendor'])
            ->setF051fdescription( $detail['f051fdescription'])
            ->setF051fidStaff((int) $detail['f038fidStaff'])
             ->setF051fstatus((int) $data['f050fstatus'])
            ->setF051fcreatedBy((int)$_SESSION['userId'])
            ->setF051fupdatedBy((int)$_SESSION['userId'])
            ->setF051fidSubmission((int)$tenderSubmission->getF051fid());

            $commiteeDetailObj->setF051fcreatedDtTm(new \DateTime())
                ->setF051fupdatedDtTm(new \DateTime());

            try{
            $em->persist($commiteeDetailObj);
            $em->flush();
            }
            catch(\Exception $e){
            echo $e;
            }
        }
    
    // $remarks = $data['remarks'];
            
        $remarkDetailObj = new T053ftenderRemarks();

        $remarkDetailObj->setF053fremarks($olddata['remarks'])
             ->setF053fstatus((int) $olddata['f050fstatus'])
            ->setF053fcreatedBy((int)$_SESSION['userId'])
            ->setF053fupdatedBy((int)$_SESSION['userId'])
            ->setF053fidSubmission((int)$tenderSubmission->getF051fid());

        $remarkDetailObj->setF053fcreatedDtTm(new \DateTime())
                ->setF053fupdatedDtTm(new \DateTime());

        try{
        $em->persist($remarkDetailObj);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
    

        return $tenderSubmission;

    }

    public function createSubmission($data)
    {
        $em = $this->getEntityManager();

        // print_r($data['f050fidTendor']);exit;

        $submissionObj = new T050ftenderSubmission();

        $submissionObj->setF050fidVendor((int)$data['f050fidVendor'])
             ->setF050fvendorCode($data['f050fvendorCode'])
             ->setF050fvendorName($data['f050fvendorName'])
             ->setF050fgrade($data['f050fgrade'])
             ->setF050famount((float)$data['f050famount'])
             ->setF050fcompletionDuration($data['f050fcompletion'])
             ->setF050fdescription($data['f050fdescription'])
             ->setF050fisShortlisted((int)"0")
             ->setF050fisAwarded((int)"0")
             ->setF050fidTendor((int)$data['f050fidTendor'])
             ->setF050fstatus((int)$data['f050fstatus'])
             ->setF050fcreatedBy((int)$_SESSION['userId'])
             ->setF050fupdatedBy((int)$_SESSION['userId']);

        $submissionObj->setF050fcreatedDtTm(new \DateTime())
                ->setF050fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($submissionObj);
            
            $em->flush();
           
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }


        $tender = (int)$data['f050fidTendor'];

        $query2 = "update t035ftender_quotation set f035fsubmitted = 1 WHERE f035fid = $tender";
        $res = $em->getConnection()->executeQuery($query2);


        return $submissionObj;
    }

    public function createCommitee($submissionObj, $detail)
    {

        
        $em = $this->getEntityManager();

        $commiteeObj = new T051ftenderCommitee();

        $commiteeObj->setF051fidTender((int) $detail['f051fidTender'])
            ->setF051fdescription( $detail['f051fdescription'])
            ->setF051fidStaff((int) $detail['f051fidStaff'])
             ->setF051fstatus((int) $detail['f050fstatus'])
            ->setF051fcreatedBy((int)$_SESSION['userId'])
            ->setF051fupdatedBy((int)$_SESSION['userId'])
            ->setF051fidSubmission((int)$submissionObj->getF050fid());

        $commiteeObj->setF051fcreatedDtTm(new \DateTime())
                ->setF051fupdatedDtTm(new \DateTime());

        try 
        {
            $em->persist($commiteeObj);

            $em->flush();

        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
        return $commiteeObj;
    }

    public function updateSubmission($submissionObj, $postData)
    {
        
        $em = $this->getEntityManager();

        $submissionObj->setF050fidVendor((int)$postData['f050fidVendor'])
             ->setF050fvendorCode($postData['f050fvendorCode'])
             ->setF050fvendorName($postData['f050fvendorName'])
             ->setF050fgrade($postData['f050fgrade'])
             ->setF050famount((float)$postData['f050famount'])
             ->setF050fcompletionDuration($postData['f050fcompletion'])
             ->setF050fdescription($postData['f050fdescription'])
             ->setF050fisShortlisted($postData['f050fisShortlisted'])
             ->setF050fisAwarded($postData['f050fisAwarded'])
             ->setF050fidTendor($postData['f050fidTendor'])
             ->setF050fstatus($postData['f050fstatus'])
             ->setF050fupdatedBy((int)$_SESSION['userId']);

        $submissionObj->setF050fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($submissionObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }

        return $submissionObj;
    }
    
   

    public function updateCommitee($commiteeObj, $detail)
    {

        $em = $this->getEntityManager();

        $commiteeObj->setF051fidTender((int) $detail['f051fidTender'])
            ->setF051fdescription( $detail['f051fdescription'])
            ->setF051fidStaff((int) $detail['f051fidStaff'])
             ->setF051fstatus((int) $detail['f050fstatus'])
            ->setF051fupdatedBy((int)$_SESSION['userId']);
        

        $commiteeObj->setF051fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($commiteeObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
        return $commiteeObj;
    }

    public function createRemarks($submissionObj,$postData)
    {
        // print_r($submissionObj);exit;
        
        $em = $this->getEntityManager();


        $remarksObj = new T053ftenderRemarks();

        $remarksObj->setF053fremarks($postData['f053fremarks'])
             ->setF053fstatus((int) $postData['f050fstatus'])
             ->setF053fidSubmission((int)$submissionObj->getF050fid())
             ->setF053fidTendor((int)$postData['f050fidTendor'])
             ->setF053fcreatedBy((int)$_SESSION['userId'])
            ->setF053fupdatedBy((int)$_SESSION['userId']);
           

        $remarksObj->setF053fcreatedDtTm(new \DateTime())
                    ->setF053fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($remarksObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
        return $remarksObj;
    }

    public function updateRemarks($remarksObj, $postData)
    {
        
        $em = $this->getEntityManager();

        $remarksObj->setF053fremarks($postData['f053fremarks'])
             ->setF053fstatus((int) $postData['f050fstatus'])
            ->setF053fupdatedBy((int)$_SESSION['userId']);
           

        $remarksObj->setF053fupdatedDtTm(new \DateTime());
        try 
        {
            $em->persist($remarksObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
        return $remarksObj;
    }
    

    


    public function getTendorShortlistedList($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor')
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->where('t.f050fisShortlisted=1')
            ->andWhere('t.f050fidTendor = :tenderId')
            ->setParameter('tenderId',(int)$id);
           
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    public function getTendorAwardList($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('t.f050fid,t.f050fidVendor,t.f050fvendorCode,t.f050fgrade,t.f050famount,t.f050fcompletionDuration,t.f050fdescription,t.f050fisShortlisted,t.f050fisAwarded,t.f050fidTendor')
            ->from('Application\Entity\T050ftenderSubmission','t')
            ->where('t.f050fisAwarded=1')
            ->andWhere('t.f050fidTendor = :tenderId')
            ->setParameter('tenderId',(int)$id);
           
        $query = $qb->getQuery();

        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $tender) {
            $date                        = $tender['f035fstartDate'];
            $tender['f035fstartDate'] = date("Y-m-d", strtotime($date));
            $date                        = $tender['f035fendDate'];
            $tender['f035fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $tender);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
        
    }

    public function deleteTenderComitee($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t051ftender_commitee WHERE f051fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteTenderRemarks($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t053ftender_remarks WHERE f053fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function deleteTenderSubmission($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t050ftender_submission WHERE f050fid = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

}

?>