<?php
namespace Application\Repository;


use Application\Entity\T019fsabCategories;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class SabCategoriesRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f019fid, c.f019fdescription, c.f019fstatus')
            ->from('Application\Entity\T019fsabCategories', 'c');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
// print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('c.f019fid, c.f019fdescription, c.f019fstatus')
            ->from('Application\Entity\T019fsabCategories', 'c')
            ->where('c.f019fid = :sabCategoriesId')
            ->setParameter('sabCategoriesId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $category = new T019fsabCategories();

        $category->setF019fdescription($data['f019fdescription'])
             ->setF019fstatus((int)$data['f019fstatus'])  
             ->setF019fcreatedby((int)$_SESSION['userId'])
             ->setF019fupdatedby((int)$_SESSION['userId']);

        $category->setF019fcreateddttm(new \DateTime())
                ->setF019fupdateddttm(new \DateTime());

 
        $em->persist($category);
        $em->flush();

        return $category;

    }

    /* to edit the data in database*/

    public function updateData($category, $data = []) 
    {
        $em = $this->getEntityManager();

        $category->setF019fdescription($data['f019fdescription'])
             ->setF019fstatus($data['f019fstatus'])
             ->setF019fcreatedby((int)$_SESSION['userId'])  
             ->setF019fupdatedby((int)$_SESSION['userId'])
             ->setF019fupdateddttm(new \DateTime());
                
      
        $em->persist($category);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
  
}

?>