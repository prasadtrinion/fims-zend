<?php
namespace Application\Repository;

use Application\Entity\Student;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StudentRepository extends EntityRepository {

	/**
	 * Get users
	 *
	 * @param array $filters
	 * @param string $search
	 * @param array $sort
	 * @param number $offset
	 * @param number $limit
	 * @return NULL[]|mixed[]
	 */
	public function getStudents($filters = array(), $search = null, $sort = array(), $offset = 0, $limit = 10, $currentUserId = null) {
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();

		// Query
		$qb->select('count(s.id) as total')
			->from('Application\Entity\Student', 's')
			->where('s.deleteFlag != 1');

		// Search
		if (!empty($search)) {
			$expr = $qb->expr();
			$qb->andWhere($expr->orX(
				$expr->like('s.name', ':search'),
				$expr->like('s.city', ':search'),
				$expr->like('s.address',':search'),
				$expr->like('s.state', ':search'),
				$expr->like('s.gender', ':search')
			));

			$qb->setParameter('search', "%{$search}%");
		}

		// Get total records
		$total = $qb->getQuery()->getSingleScalarResult();

		$qb->select('s.id, s.name,s.phoneNumber,s.email,s.gender,s.address,s.city,s.state');


		switch ($column) {
		case 'name':
			$column = 's.name';
			break;
		case 'city':
			$column = 's.city';
			break;
		case 'gender':
			$column = 's.gender';
			break;
		default:
			$column = 's.updateDtTm';
		}


		// Limit
		if ($limit > 0) {
			$qb->setFirstResult($offset)->setMaxResults($limit);
		}

		$query = $qb->getQuery();
		//echo $query->getSql();exit;
		$result = array(
			'total' => $total,
			'data' => $query->getArrayResult(),
		);

		return $result;
	}

	/**
	 * Function to update the user info
	 *
	 * @param object $entity
	 */
	public function update($entity) {
		$em = $this->getEntityManager();
		$em->persist($entity);
		$em->flush();
	}

	/**
	 * Function to save user info
	 *
	 * @param object $accountObj
	 * @param array $data
	 */
	public function createStudent($data) {
		$em = $this->getEntityManager();

		$student = new Student();
		$student->setName($data['name'])
			->setEmail($data['email'])
			->setAddress($data['address'])
			->setGender($data['gender'])
			->setCity($data['city'])
			->setState($data['state']);
			
		// Contact information
		$student->setPhoneNumber($data['phone']);

		$student->setCreateDtTm(new \DateTime())
    		->setUpdateDtTm(new \DateTime());
		
		$em->persist($student);
		$em->flush();

		return $student;

	}

	/**
	 * Function to update user info
	 *
	 * @param object $accountObj
	 * @param array $data
	 */
	public function updateStudent($student, $data = []) 
	{
        $em = $this->getEntityManager();
        
        $student->setName($data['name'])
			->setEmail($data['email'])
			->setAddress($data['address'])
			->setGender($data['gender'])
			->setCity($data['city'])
			->setState($data['state']);
        
		$student->setPhoneNumber($data['phone']);
        
        $student->setUpdateDtTm(new \DateTime());
        
        $em->persist($student);
        $em->flush();
    }
	
	/**
	 * Get user details
	 */
	public function getStudentDetails($studentId) {
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('u.id, u.name,u.address, u.state, u.city, u.phoneNumber,u.gender, u.email')
			->from('Application\Entity\Student', 'u')
			->where('u.deleteFlag != 1')
			->andWhere('u.id = :studentId')
			->setParameter('studentId', $studentId);

		$query = $qb->getQuery();
		$result = $query->getSingleResult(2);

		return $result;
	}
}
