<?php
namespace Application\Repository;

use Application\Entity\T027fitemCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ItemCategoryRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("c.f027fid, c.f027fcode +'-'+c.f027fcategoryName as originalname,c.f027fcategoryName, c.f027fcode, c.f027fmaintenance, c.f027fstatus, c.f027fmaintenance")
            ->from('Application\Entity\T027fitemCategory','c')
            ->orderBy('c.f027fid','DESC');

        $query = $qb->getQuery();

        $result = array( 
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
        public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("c.f027fid, c.f027fcode +'-'+c.f027fcategoryName as originalname, c.f027fmaintenance, c.f027fmaintenance, c.f027fstatus, c.f027fcategoryName, c.f027fcode")
            ->from('Application\Entity\T027fitemCategory','c')
            ->orderBy('c.f027fid','DESC')
            ->where('c.f027fstatus = 1');;

        $query = $qb->getQuery();

        $result = array( 
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f027fid, c.f027fcategoryName,c.f027fcode, c.f027fstatus, c.f027fmaintenance')
            ->from('Application\Entity\T027fitemCategory','c')
             ->where('c.f027fid = :categoryId')
            ->setParameter('categoryId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $category = new T027fitemCategory();

        $category->setF027fcategoryName($data['f027fcategoryName'])
            ->setF027fcode($data['f027fcode'])
             ->setF027fstatus((int)$data['f027fstatus'])
             ->setF027fmaintenance((int)$data['f027fmaintenance'])
             ->setF027fcreatedBy((int)$_SESSION['userId'])
             ->setF027fupdatedBy((int)$_SESSION['userId']);

        $category->setF027fcreatedDtTm(new \DateTime())
                ->setF027fupdatedDtTm(new \DateTime());

 
        $em->persist($category);
        $em->flush();

        return $category;

    }

    public function updateData($category, $data = []) 
    {
        $em = $this->getEntityManager();

        $category->setF027fcategoryName($data['f027fcategoryName'])
            ->setF027fcode($data['f027fcode'])
            ->setF027fmaintenance((int)$data['f027fmaintenance'])
                ->setF027fstatus((int)$data['f027fstatus'])
             ->setF027fcreatedBy((int)$_SESSION['userId'])
             ->setF027fupdatedBy((int)$_SESSION['userId']);

        $category->setF027fupdatedDtTm(new \DateTime());
        
        $em->persist($category);
        $em->flush();

    }

}