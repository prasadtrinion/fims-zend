<?php
namespace Application\Repository;

use Application\Entity\T063fapplicationMaster;
use Application\Entity\T063fapplicationDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class InvestmentApplicationRepository extends EntityRepository 
{

	public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('distinct(a.f063fid) as f063fid, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, a.f063ffileUpload, a.f063fapplicationId, a.f063fcreatedDtTm, a.f063fcreatedBy, u.f014fuserName, a.f063fuumBank')
          ->from('Application\Entity\T063fapplicationMaster','a')
          ->leftjoin('Application\Entity\T014fuser','u','with','u.f014fid= a.f063fcreatedBy')
          ->leftjoin('Application\Entity\T063fapplicationDetails','d','with','d.f063fidMaster= a.f063fid')
          ->orderBy('a.f063fid','DESC');

          // ->leftjoin('Application\Entity\T061finvestmentInstitution', 'it','with','r.f063finstitutionId= it.f061fid');


        
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

          $result1 = array();
        foreach ($result as $item) {

            $date                        = $item['f063fcreatedDtTm'];
            $item['f063fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        
        return $result;
        
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $qb->select('a.f063fid,a.f063fapplicationId, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, d.f063fduration,d.f063fdurationType, d.f063fidDetails, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType,d.f063fbankId, d.f063fbankBatch, a.f063ffileUpload, a.f063fcreatedDtTm, a.f063fcreatedBy, u.f014fuserName, a.f063fuumBank')
          ->from('Application\Entity\T063fapplicationMaster','a')
          ->leftjoin('Application\Entity\T063fapplicationDetails','d','with','d.f063fidMaster= a.f063fid')
          ->leftjoin('Application\Entity\T014fuser','u','with','u.f014fid= a.f063fcreatedBy')
             ->where('a.f063fid = :applicationId')
            ->setParameter('applicationId',(int)$id);

        $query = $qb->getQuery();
        
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f063fmaturityDate'];
            $item['f063fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f063fcreatedDtTm'];
            $item['f063fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        return $result;
    }

    public function createApplication($data)
    {
        // print_r($data);
        // exit();
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="IBN";
        $number = $configRepository->generateFIMS($numberData);
        // print_r($number);exit;

        $application = new T063fapplicationMaster();

        $application->setF063fdescription($data['f063fdescription'])
                 ->setF063fapplicationId($number)
                 ->setF063finvestmentAmount((float)$data['f063finvestmentAmount'])
                 ->setF063fstatus((int)$data['f063fstatus'])
                 ->setF063ffileUpload($data['f063ffileUpload'])
                 ->setF063fuumBank((int)$data['f063fuumBank'])
                 ->setF063fisOnline((int)0)
                 ->setF063fcreatedBy((int)$_SESSION['userId'])
                 ->setF063fupdatedBy((int)$_SESSION['userId']);
        
        $application->setF063fcreatedDtTm(new \DateTime())
                ->setF063fupdatedDtTm(new \DateTime());

         
        try {
                    
                
        $em->persist($application);


        $em->flush();
        // print_r($application);
      // die();

       
    }
    catch (\Exception $e) {
                    echo $e;
                }        
      // print_r($application);
      // die();
                
        $applicationDetails = $data['application-details'];

        foreach ($applicationDetails as $applicationDetail) 
        {

            $applicationDetailObj = new T063fapplicationDetails();

            $applicationDetailObj->setF063fidMaster((int)$application->getF063fid())
                                 ->setF063fduration($applicationDetail['f063fduration'])
                                 ->setF063fdurationType($applicationDetail['f063fdurationType'])
                                 ->setF063fbranch($applicationDetail['f063fbranch'])
                                 ->setF063famount((float)$applicationDetail['f063famount'])
                                 ->setF063finstitutionId((int)$applicationDetail['f063finstitutionId'])
                                 ->setF063fidInvestmentType((int)$applicationDetail['f063fidInvestmentType'])
                                 ->setF063fprofitRate((float)$applicationDetail['f063fprofitRate'])
                                 ->setF063fstatus((int)$applicationDetail['f063fstatus'])
                                 ->setF063fmaturityDate(new \DateTime($data['f063fmaturityDate']))
                                 ->setF063fbankId((int)$applicationDetail['f063fbankId'])
                                 ->setF063fbankBatch($applicationDetail['f063fbankBatch'])
                                 ->setF063fapproved1Status((int)0)
                                 ->setF063fapproved2Status((int)0)
                                 ->setF063fapproved3Status((int)0)
                                 ->setF063fapproved1By((int)0)
                                 ->setF063fapproved2By((int)0)
                                 ->setF063fapproved3By((int)0)
                                 ->setF063flevel1((int)1)
                                 ->setF063flevel2((int)2)
                                 ->setF063flevel3((int)3)
                                 ->setF063frenewal1((int)0)
                                 ->setF063frenewal2((int)0)
                                 ->setF063frenewal3((int)0)
                                 ->setF063ffinalStatus((int)0)
                                 ->setF063fcreatedBy((int)$_SESSION['userId'])
                                 ->setF063fupdatedBy((int)$_SESSION['userId']);

            $applicationDetailObj->setF063fcreatedDtTm(new \DateTime())
                                 ->setF063fupdatedDtTm(new \DateTime());

        

            $em->persist($applicationDetailObj);

            $em->flush();

            
        }
        return $investmentApplication;
    }

    public function updateApplication($application, $data = [])
    {
        $em = $this->getEntityManager();

       $application->setF063fdescription($data['f063fdescription'])
                 ->setF063fapplicationId($data['f063fapplicationId'])
                 ->setF063finvestmentAmount((float)$data['f063finvestmentAmount'])
                 ->setF063ffileUpload($data['f063ffileUpload'])
                 ->setF063fuumBank((int)$data['f063fuumBank'])
                 ->setF063fstatus((int)$data['f063fstatus'])
                 ->setF063fupdatedBy((int)$_SESSION['userId']);

      $application->setF063fupdatedDtTm(new \DateTime());
        
        $em->persist($application);
        $em->flush();

        return $application;
       
    }


    public function updateApplicationDetail($applicationDetailObj, $applicationDetail)
    {
        
        $em = $this->getEntityManager();

        $applicationDetailObj->setF063fduration($applicationDetail['f063fduration'])
                                 ->setF063fbranch($applicationDetail['f063fbranch'])
                                 ->setF063fdurationType($applicationDetail['f063fdurationType'])
                                 ->setF063famount((float)$applicationDetail['f063famount'])
                                 ->setF063finstitutionId((int)$applicationDetail['f063finstitutionId'])
                                 ->setF063fprofitRate((float)$applicationDetail['f063fprofitRate'])
                                 ->setF063fstatus((int)$applicationDetail['f063fstatus'])
                                 ->setF063fmaturityDate(new \DateTime($data['f063fmaturityDate']))
                                 ->setF063fidInvestmentType((int)$applicationDetail['f063fidInvestmentType'])
                                 ->setF063fbankId((int)$applicationDetail['f063fbankId'])
                                 ->setF063fbankBatch($applicationDetail['f063fbankBatch'])
                                 // ->setF063fapproved1Status((int)0)
                                 // ->setF063fapproved2Status((int)0)
                                 // ->setF063fapproved3Status((int)0)
                                 // ->setF063fapproved1By((int)0)
                                 // ->setF063fapproved2By((int)0)
                                 // ->setF063fapproved3By((int)0)
                                 // ->setF063flevel1((int)1)
                                 // ->setF063flevel2((int)2)
                                 // ->setF063flevel3((int)3)
                                 // ->setF063frenewal1((int)0)
                                 // ->setF063frenewal2((int)0)
                                 // ->setF063frenewal3((int)0)
                                 // ->setF063ffileUpload($applicationDetail['f063ffileUpload'])
                                 ->setF063fcreatedBy((int)$_SESSION['userId'])
                                 ->setF063fupdatedBy((int)$_SESSION['userId']);

        $applicationDetailObj->setF063fupdatedDtTm(new \DateTime());

        

        $em->persist($applicationDetailObj);

        $em->flush();

    }
    
    public function createApplicationDetail($applicationObj, $applicationDetail)
    {
        $em = $this->getEntityManager();
      

         $applicationDetailObj = new T063fapplicationDetails();

            $applicationDetailObj->setF063fidMaster((int)$applicationObj->getF063fid())
                                 ->setF063fduration($applicationDetail['f063fduration'])
                                 ->setF063fdurationType($applicationDetail['f063fdurationType'])
                                 ->setF063fbranch($applicationDetail['f063fbranch'])
                                 ->setF063famount((float)$applicationDetail['f063famount'])
                                 ->setF063finstitutionId((int)$applicationDetail['f063finstitutionId'])
                                 ->setF063fidInvestmentType((int)$applicationDetail['f063fidInvestmentType'])
                                 ->setF063fprofitRate((float)$applicationDetail['f063fprofitRate'])
                                 ->setF063fmaturityDate(new \DateTime($data['f063fmaturityDate']))
                                 ->setF063fstatus((int)$applicationDetail['f063fstatus'])
                                 ->setF063fbankId((int)$applicationDetail['f063fbankId'])
                                 ->setF063fbankBatch($applicationDetail['f063fbankBatch'])
                                  ->setF063fapproved1Status((int)0)
                                 ->setF063fapproved2Status((int)0)
                                 ->setF063fapproved3Status((int)0)
                                 ->setF063fapproved1By((int)0)
                                 ->setF063fapproved2By((int)0)
                                 ->setF063fapproved3By((int)0)
                                 ->setF063flevel1((int)1)
                                 ->setF063flevel2((int)2)
                                 ->setF063flevel3((int)3)
                                 ->setF063frenewal1((int)0)
                                 ->setF063frenewal2((int)0)
                                 ->setF063frenewal3((int)0)
                                 ->setF063ffinalStatus((int)0)
                                 // ->setF063ffileUpload($applicationDetail['f063ffileUpload'])
                                 ->setF063fcreatedBy((int)$_SESSION['userId'])
                                 ->setF063fupdatedBy((int)$_SESSION['userId']);

            $applicationDetailObj->setF063fcreatedDtTm(new \DateTime())
                                 ->setF063fupdatedDtTm(new \DateTime());

        

            $em->persist($applicationDetailObj);

            $em->flush();

        // return $incrementDetailObj;
        
    }

    public function getListApproved() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('a.f063fid,a.f063fapplicationId, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, a.f063ffileUpload, a.f063fuumBank')
          ->from('Application\Entity\T063fapplicationMaster','a')
          // ->leftjoin('Application\Entity\T061finvestmentInstitution', 'it','with','r.f063finstitutionId= it.f061fid')
          ->where('a.f063fapproved1Status=1')
          ->andWhere('a.f063fapproved2Status=1')
          ->andWhere('a.f063fapproved3Status=1');
          // ->andwhere('a.f063fisOnline!=1');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        
        return $result;
        
    }
  
    public function detailApproval($postData)
    {
        // print_r($postData);exit;

        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();
        $applications=$postData['id'];

        $renewals=$postData['autoRenewal'];
        foreach ($applications as $application) 
        {
        
        // $id=(int)$application['id'];
        $level=(int)$postData['level'];
        $status=(int)$postData['status'];
        $reason=$postData['reason'];
        $user=(int)$_SESSION['userId'];

            switch ($level) 
            {
                case '1':
                    $query = "update t063fapplication_details set f063fapproved1_status = $status, f063fapproved1_by=$user, f063freason1='$reason' where f063fid_details = $application";
                    $result =$em->getConnection()->executeQuery($query);

                    $query1 = "select * from t063fapplication_details where f063fid_details = $application and (f063fapproved1_status = 2 or f063fapproved2_status = 2 or f063fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();


                    $queryAllApprove = "select * from t063fapplication_details where f063fid_details = $application and f063fapproved1_status = 1 and f063fapproved2_status = 1 and f063fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {

                      $query2 = "update t063fapplication_details set f063ffinal_status = 2, f063ffinal_reason = '$reason' where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t063fapplication_details set f063ffinal_status = 1 where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }
                    break;

                case '2':
                    $query = "update t063fapplication_details set f063fapproved2_status = $status, f063fapproved2_by=$user, f063freason2='$reason' where f063fid_details = $application";
                    $result =$em->getConnection()->executeQuery($query);
                    

                    $query1 = "select * from t063fapplication_details where f063fid_details = $application and (f063fapproved1_status = 2 or f063fapproved2_status = 2 or f063fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();


                    $queryAllApprove = "select * from t063fapplication_details where f063fid_details = $application and f063fapproved1_status = 1 and f063fapproved2_status = 1 and f063fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {

                      $query2 = "update t063fapplication_details set f063ffinal_status = 2, f063ffinal_reason = '$reason' where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t063fapplication_details set f063ffinal_status = 1 where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    break;

                case '3':
                    $query = "update t063fapplication_details set f063fapproved3_status = $status, f063fapproved3_by=$user, f063freason3='$reason', f063fall_approved=1 where f063fid_details = $application";
                    $result =$em->getConnection()->executeQuery($query);

                    $query1 = "select * from t063fapplication_details where f063fid_details = $application and (f063fapproved1_status = 2 or f063fapproved2_status = 2 or f063fapproved3_status = 2)";
                    $resultStatus =$em->getConnection()->executeQuery($query1)->fetch();


                    $queryAllApprove = "select * from t063fapplication_details where f063fid_details = $application and f063fapproved1_status = 1 and f063fapproved2_status = 1 and f063fapproved3_status = 1";
                    $resultAllApprove =$em->getConnection()->executeQuery($queryAllApprove)->fetch();

                    if ($resultStatus)
                    {

                      $query2 = "update t063fapplication_details set f063ffinal_status = 2, f063ffinal_reason = '$reason' where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }

                    if($resultAllApprove)
                    {
                       $query2 = "update t063fapplication_details set f063ffinal_status = 1 where f063fid_details = $application";
                      $resultFinal =$em->getConnection()->executeQuery($query2);
                    }


                    break;
            
            }
        }
        
        $renewals=$postData['autoRenewal'];
        $level=(int)$postData['level'];
        foreach ($renewals as $renewal) 
        {
        
        $id=(int)$renewal;
        

            switch ($level) 
            {
                case '1':
                    $query = "update t063fapplication_details set f063frenewal1 = 1 where f063fid_details = $id";
                    $result =$em->getConnection()->executeQuery($query);
                    break;
                case '2':
                    $query = "update t063fapplication_details set f063frenewal2 = 1 where f063fid_details = $id";
                    $result =$em->getConnection()->executeQuery($query);
                    break;
                case '3':
                    $query = "update t063fapplication_details set f063frenewal3 = 1 where f063fid_details = $id";
                    $result =$em->getConnection()->executeQuery($query);
                    break;
            
            }
        }
        $query1 = "select * from t063fapplication_details where f063fid_details = $application";
        $result = $em->getConnection()->executeQuery($query1)->fetchAll();

        $master=$result[0]['f063fid_master'];

        $query2 = "select * from t063fapplication_details where f063fid_master = $master and f063fall_approved = 0";
        
        $application_detail = $em->getConnection()->executeQuery($query2)->fetchAll();

        if(!$application_detail)
            {
                $query3 = "update t063fapplication_master set f063fis_online = 1 where f063fid = $master";
                $result3 = $em->getConnection()->executeQuery($query3)->fetchAll();

            }
    }

    public function getInvestmentApplicationApprovalList($level,$status)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a.f063fid, a.f063fapplicationId, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, d.f063fduration, d.f063fdurationType, d.f063fidDetails, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType, d.f063fbankId, d.f063fbankBatch,b.f042fbankName, a.f063ffileUpload, d.f063fapproved1Status, invt.f038ftype as investmentType, d.f063fapproved2Status, d.f063fapproved3Status, a.f063fuumBank, d.f063ffinalStatus')
          ->from('Application\Entity\T063fapplicationMaster','a')
          ->leftjoin('Application\Entity\T063fapplicationDetails','d','with','d.f063fidMaster= a.f063fid')
          ->leftjoin('Application\Entity\T042fbank','b','with','d.f063fbankId= b.f042fid')
          ->leftjoin('Application\Entity\T038finvestmentType','invt','with','invt.f038fid = d.f063fidInvestmentType')
          ->orderBy('a.f063fid','DESC');

            
    // echo $level;
    // echo $status;
    // die();
        switch ($level) 
        {
            case 1:
                $qb->where('d.f063flevel1 = :level1')
                   ->setParameter('level1',$level)
                   ->andWhere('d.f063ffinalStatus != 2');
                   // ->andWhere('d.f063fapproved1Status = :status1')
                   // ->setParameter('status1',$status);
                break;
            case 2:

               $qb->where('d.f063flevel2 = :level2')
                   ->setParameter('level2',$level)
                   ->andWhere('d.f063fapproved2Status = :status2')
                   ->orWhere('d.f063fapproved3Status = 0')
                   ->setParameter('status2',$status)
                   ->andWhere('d.f063fapproved1Status = 1')
                   ->andWhere('d.f063ffinalStatus != 2');
                break;
            case 3:
            $qb->where('d.f063flevel3 = :level3')
                   ->setParameter('level3',$level)
                   ->andWhere('d.f063fapproved3Status = :status3')
                   ->orWhere('d.f063fapproved2Status = 0')
                   ->setParameter('status3',$status)
                   ->andWhere('d.f063fapproved1Status = 1')
                   ->andWhere('d.f063ffinalStatus != 2');
                break;
            case 4:
                $qb->andWhere('d.f063fapproved1Status=1')
                   ->andWhere('d.f063fapproved2Status=1')
                   ->andWhere('d.f063fapproved3Status=1')
                   ->andWhere('d.f063ffinalStatus != 2');
                   // ->andWhere('d.f063fallApproved = 1');
                   // ->groupBy('a.f063fapplicationId');
                break;
        }
        

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
    
    public function getInvestmentAccountCodes()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select("c.f095fid,c.f095fsetupId,c.f095fdescription,d.f011fdefinitionCode as setup,c.f095faccountCode,c.f095ftype,a.f059fcompleteCode+ ' - '+a.f059fname as accountName")
          ->from('Application\Entity\T095fcreditorSetup','c')
          ->leftjoin('Application\Entity\T059faccountCode','a','with','a.f059fcompleteCode = c.f095faccountCode')
          ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','c.f095fsetupId = d.f011fid')
          ->where("c.f095ftype = 'Debit'")
          ->andWhere("d.f011fdefinitionCode = 'Investment'")
          ->orderby('c.f095faccountCode');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
    public function updateApplicationDetailStatus($deletedIds) 
    {
        $em = $this->getEntityManager();

        foreach ($deletedIds as $deletedId) 
        {
            

           $query = "UPDATE t063fapplication_details set f054fstatus = 2 where f063fid_details = $deletedId"; 
            $result=$em->getConnection()->executeQuery($query);
           
            

        }
        return;
        // exit();
    }

    public function getApplicationList()
    {
        $em = $this->getEntityManager();

        $query= "select distinct(am.f063fid) as f063fid, am.* from t063fapplication_master am inner join t063fapplication_details ad on ad.f063fid_master = am.f063fid where ad.f063fapproved1_status = 1 and ad.f063fapproved2_status = 1 and ad.f063fapproved3_status = 1 and ad.f063ffinal_status = 1 and am.f063fid not in (select ir.f064fid_application from  t064finvestment_registration ir)";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();

        return $application;
       
    }

    public function deleteInvestmentApplicationDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $query2 = "DELETE from t063fapplication_details WHERE f063fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function getTypeOfInvestment($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('d.f063fdurationType, d.f063fduration, d.f063fidDetails, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType ,d.f063fbankId, d.f063fbankBatch')
          ->from('Application\Entity\T063fapplicationDetails','d')
          ->where('d.f063finstitutionId = :instituteId')
          ->setParameter('instituteId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f063fmaturityDate'];
            $item['f063fmaturityDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        return $result;

    }
    

    public function getInvestmentBankListByMasterId($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("d.f063fidDetails, d.f063fdurationType, d.f063fduration, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType ,d.f063fbankId, d.f063fbankBatch,(b.f042fswiftCode+'-'+b.f042fbankName) as bankName, b.f042fbranchName, b.f042faccountNumber, b.f042faccountType, b.f042fbrsCode")
          ->from('Application\Entity\T063fapplicationDetails','d')
          ->leftjoin('Application\Entity\T042fbank', 'b', 'with','b.f042fid = d.f063fbankId')
          ->where('d.f063fidMaster = :instituteId')
          ->andWhere('d.f063fapproved1Status = 1')
          ->andWhere('d.f063fapproved2Status = 1')
          ->andWhere('d.f063fapproved3Status = 1')
          ->setParameter('instituteId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f063fmaturityDate'];
            $item['f063fmaturityDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        return $result;
    }

    public function investmentGetByApprovalDetails()
    {
        $em = $this->getEntityManager();


        $queryZero = "select ind.f063fid from t063fapplication_master as ind order by ind.f063fid desc";
        
        $resultZero = $em->getConnection()->executeQuery($queryZero)->fetchAll();

        $investment = array();


        foreach ($resultZero as $value)
        {
            $id = (int)$value['f063fid'];
        // print_r($id);exit;

            $query= "select distinct(indd.f063fid_master) as f063fid_master from t063fapplication_details as indd where indd.f063fapproved1_status = 0 and indd.f063fid_master = $id  ";
        
            $application = $em->getConnection()->executeQuery($query)->fetch();


            if (!$application)
            {
            // print_r($id);exit;

                 $queryGet = "select distinct(ind.f063fid) as f063fid, ind.f063fapplication_id, ind.f063fdescription, ind.f063finvestment_amount, ind.f063fis_online, ind.f063ffile_upload from t063fapplication_master as ind inner join t063fapplication_details as indd on indd.f063fid_master = ind.f063fid  where (indd.f063ffinal_status != 1 and indd.f063ffinal_status != 2) and ind.f063fid = $id  ";
        
                $resultGet = $em->getConnection()->executeQuery($queryGet)->fetch();
                if($resultGet){
                array_push($investment, $resultGet);
                }
            }
        }
        return $investment;

    }

    public function investmentToPaymentVouchar($id,$initialRepository)
    {
        $id = (int)$id;
        $user = (int)$_SESSION['userId'];

         $em = $this->getEntityManager();

         $query= "SELECT inm.f063fapplication_id, inm.f063finvestment_amount, inm.f063fuum_bank, inm.f063fdescription, ind.f063famount, inm.f063ffile_upload, ind.f063fbank_id from t063fapplication_master as inm inner join t063fapplication_details as ind on ind.f063fid_master = inm.f063fid WHERE ind.f063fid_details  = $id";
        // exit;
        
        $application = $em->getConnection()->executeQuery($query)->fetch();

        // print_r($application);exit;

        $voucherNo = $application['f063fapplication_id'];
        $investmentAmount = $application['f063finvestment_amount'];
        $uumBank = (int)$application['f063fuum_bank'];
        $description = $application['f063fdescription'];
        $paymentAmount = $application['f063famount'];
        $bankId = (int)$application['f063fbank_id'];


         $query2 = "select f041ffund_code, f041fdepartment_code, f041factivity_code, f041faccount_code from t041fbank_setup_account where f041fid = $uumBank";
        // exit;
            $bankResult = $em->getConnection()->executeQuery($query2)->fetch();
            // print_r($paymentAmount);exit;


            $JournalFund = $bankResult['f041ffund_code'];
            $JournalCode = $bankResult['f041fdepartment_code'];
            $JournalActivity = $bankResult['f041factivity_code'];
            $JournalAccount = $bankResult['f041faccount_code'];


            $query3 = "select f015fid from t015ffinancialyear where f015fstatus = 1";
            $yearResult = $em->getConnection()->executeQuery($query3)->fetch();

            // print_r($yearResult);exit;


            $year = (int)$yearResult['f015fid'];

             $query4 = "select gl.*, dms.f011fdefinition_code from t140fglcode_setup as gl inner join t011fdefinationms as dms on dms.f011fid = gl.f140id_voucher_type where f011fdefinition_code = 'Investment'";
            $glResult = $em->getConnection()->executeQuery($query4)->fetch();
            // print_r($glResult);exit;

            $Crfund = $glResult['f140fcredit_fund'];
            $Crdepartment = $glResult['f140fcredit_department'];
            $Cractivity = $glResult['f140fcredit_activity'];
            $Craccount = $glResult['f140fcredit_account'];
            $Dbtfund = $glResult['f140fdebit_fund'];
            $Dbtdepartment = $glResult['f140fdebit_department'];
            $Dbtactivity = $glResult['f140fdebit_activity'];
            $Dbtaccount = $glResult['f140fdebit_account'];
            $voucharType = (int)$glResult['f140id_voucher_type'];


              switch ($voucharType)
            {
                case '112':
                   $module = 'Investment';
                    break;
                
                default:
                    # code...
                    break;
            }


            $data=array();
            $data['type']='GL';
            $generateNumber = $initialRepository->generateFIMS($data);


             $journalQuery = "INSERT INTO t017fjournal (f017fdescription,f017fmodule,f017ftotal_amount,f017fcreated_by,f017fupdated_by,f017fcreate_dt_tm,f017fupdate_dt_tm,f017fapprover,f017fapproved_status,f017fid_financial_year,f017freference_number) VALUES (
'$module','$module',$paymentAmount,$user,$user,getdate(),getdate(),$user,1,$year,'$generateNumber')";
                // exit;

            $glResult = $em->getConnection()->executeQuery($journalQuery)->fetch();


             $journalGetQuery = " select top 1 f017fid from t017fjournal order by f017fid desc ";
            $glIdResult = $em->getConnection()->executeQuery($journalGetQuery)->fetch();

            $idGrn = (int)$glIdResult['f017fid'];


   $journValDetailQuery = "INSERT into t018fjournal_details (f018fid_journal,f018fso_code,f018fdr_amount,f018fcr_amount,f018fstatus,f018fcreated_by,f018fupdated_by,f018fcreate_dt_tm,f018fupdate_dt_tm,f018fdate_of_transaction,f018freference_number,f018ffund_code,f018fdepartment_code,f018factivity_code,f018faccount_code) VALUES ($idGrn,'NULL',$paymentAmount,0.00,1,$user,0,getdate(),getdate(),getdate(),'$voucherNo','$Dbtfund','$Dbtdepartment','$Dbtactivity','$Dbtaccount')";

 
            $glResult = $em->getConnection()->executeQuery($journValDetailQuery)->fetch();


               $journalDetailQuery1 = "INSERT INTO t018fjournal_details (f018fid_journal,f018fso_code,f018fdr_amount,f018fcr_amount,f018fstatus,f018fcreated_by,f018fupdated_by,f018fcreate_dt_tm,f018fupdate_dt_tm,f018fdate_of_transaction,f018freference_number,f018ffund_code,f018fdepartment_code,f018factivity_code,f018faccount_code) VALUES (
$idGrn,'NULL',0.00,$paymentAmount,1,1,1,getdate(),getdate(),getdate(),'$voucherNo','$JournalFund','$JournalCode','$JournalActivity','$JournalAccount')";
            $glResult = $em->getConnection()->executeQuery($journalDetailQuery1)->fetch();


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']='PV';
        $number = $configRepository->generateFIMS($numberData);

        // print_r($number);exit;


        $paymentInsertQuery = "INSERT INTO t093fpayment_voucher (f093fvoucher_no,f093fpayment_type,f093fdescription,f093fpayment_mode,f093fpayment_ref_no,f093fapproval_status,f093fstatus,f093fcreated_by,f093fupdated_by,f093fcreated_dt_tm,f093fupdated_dt_tm,f093fid_bank,f093fid_company_bank,f093freason,f093fsource,f093fid_supplier,f093ftotal_amount) VALUES (
'$number',$voucharType,'$module',18,'$voucherNo',1,1,$user,$user,getdate(),getdate(),$bankId,$uumBank,NULL,'$module',0,$paymentAmount)";
            $paymentInsertResult = $em->getConnection()->executeQuery($paymentInsertQuery)->fetch();

            $journalGetQuery = " select top 1 f093fid from t093fpayment_voucher order by f093fid desc ";
            $glIdResult = $em->getConnection()->executeQuery($journalGetQuery)->fetch();

            $idPay = (int)$glIdResult['f093fid'];


            $paymentDetailInsertQuery = "INSERT INTO t094fpayment_voucher_details (f094fid_payment,f094id_bill_registration,f094fvoucher_date,f094ftotal_amount,f094fpaid_amount,f094fstatus,f094fcreated_by,f094fupdated_by,f094fcreated_dt_tm,f094fupdated_dt_tm,f094fcustomer_name) VALUES (
$idPay,24,getdate(),$paymentAmount,$paymentAmount,1,$user,$user,getdate(),getdate(),'1')";

            $paymentDetailInsertResult = $em->getConnection()->executeQuery($paymentDetailInsertQuery)->fetch();




        return $application;
    }


    public function getInvestmentSummary()
    {
        $em = $this->getEntityManager();
        
       // $qb->select('a.f063fid,a.f063fapplicationId, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, d.f063fduration,d.f063fdurationType, d.f063fidDetails, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType,d.f063fbankId, d.f063fbankBatch, a.f063ffileUpload, a.f063fcreatedDtTm, a.f063fcreatedBy, u.f014fuserName, a.f063fuumBank')
       //    ->from('Application\Entity\T063fapplicationMaster','a')
       //    ->leftjoin('Application\Entity\T063fapplicationDetails','d','with','d.f063fidMaster= a.f063fid')
       //    ->leftjoin('Application\Entity\T014fuser','u','with','u.f014fid= a.f063fcreatedBy')
       //    ->where('d.f063fapproved1Status = 1')
       //    ->andWhere('d.f063fapproved2Status = 1')
       //    ->andWhere('d.f063fapproved3Status = 1');

      $summaryQuery = "SELECT DISTINCT(a.f063fid) as f063fid, a.f063fapplication_id as f063fapplicationId, a.f063fdescription, a.f063finvestment_amount as f063finvestmentAmount, a.f063fstatus, a.f063fis_online asf063fisOnline, d.f063fduration, a.f063ffile_upload as f063ffileUpload, a.f063fcreated_dt_tm as f063fcreatedDtTm, a.f063fcreated_by as f063fcreatedBy, u.f014fuser_name as f014fuserName, a.f063fuum_bank as f063fuumBank 
                from t063fapplication_master a 
             join t063fapplication_details d on d.f063fid_master = a.f063fid 
             join t014fuser u on u.f014fid = a.f063fcreated_by 
             where d.f063fapproved1_status = 1 
             and d.f063fapproved2_status = 1 
             and d.f063fapproved3_status = 1 "; 
        $result = $em->getConnection()->executeQuery($summaryQuery)->fetchAll();


        // $query = $qb->getQuery();
        
        // $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f063fmaturityDate'];
            $item['f063fmaturityDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f063fcreatedDtTm'];
            $item['f063fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );

        return $result;
    }

     public function investmentApplicationDetails()
    {
        $em = $this->getEntityManager();

        // Slect from master where masterid not in (select fo36fidMaster from detailstabol where f063fapproved1_status=1)

         $query= "select distinct(ind.f063fid) as f063fid, ind.f063fapplication_id, ind.f063fdescription, ind.f063finvestment_amount, ind.f063fis_online, ind.f063ffile_upload from t063fapplication_master as ind inner join t063fapplication_details indd on indd.f063fid_master = ind.f063fid where indd.f063fapproved1_status = 0 and indd.f063fapproved2_status = 0 and indd.f063fapproved3_status = 0 and indd.f063ffinal_status = 0 order by ind.f063fid desc";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();
        return $application;


    }

    public function investmentApplicationDetailsApproved()
    {
        $em = $this->getEntityManager();

        $query= "select distinct(ind.f063fid) as f063fid, ind.f063fapplication_id, ind.f063fdescription, ind.f063finvestment_amount, ind.f063fis_online, ind.f063ffile_upload from t063fapplication_master as ind  where ind.f063fid not in (select f063fid_master from t063fapplication_details indd where indd.f063fapproved1_status=1 and indd.f063fapproved2_status = 1 and indd.f063fapproved3_status = 1) order by ind.f063fid desc ";
        
        $application = $em->getConnection()->executeQuery($query)->fetchAll();
        return $application;

    }
}
?>