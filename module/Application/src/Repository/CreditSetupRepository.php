<?php
namespace Application\Repository;

use Application\Entity\T039fcreditSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CreditSetupRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f039fid, c.f039fname, d.f011fdefinitionCode as name, c.f039fidGlcode, g.f014fglCode as GlCode, c.f039fstatus')
            ->from('Application\Entity\T039fcreditSetup','c')
            ->leftjoin('Application\Entity\T014fglcode', 'g','with','c.f039fidGlcode = g.f014fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','c.f039fname = d.f011fid')
            ->orderBy('c.f039fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f039fid, c.f039fname, d.f011fdefinitionCode as name, c.f039fidGlcode, g.f014fglCode as GlCode, c.f039fstatus')
            ->from('Application\Entity\T039fcreditSetup','c')
            ->leftjoin('Application\Entity\T014fglcode', 'g','with','c.f039fidGlcode = g.f014fid')
            ->leftjoin('Application\Entity\T011fdefinationms','d','with','c.f039fname = d.f011fid')
            ->where('c.f039fid = :creditId')
            ->setParameter('creditId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        

        $credit = new T039fcreditSetup();

        $credit->setF039fname((int)$data['f039fname'])
             ->setF039fidGlcode((int)$data['f039fidGlcode'])
             ->setF039fstatus($data['f039fstatus'])
             ->setF039fcreatedBy((int)$_SESSION['userId'])
             ->setF039fupdatedBy((int)$_SESSION['userId']);

        $credit->setF039fcreatedDtTm(new \DateTime())
                ->setF039fupdatedDtTm(new \DateTime());
               

        $em->persist($credit);
        
        $em->flush();


        return $credit;

    }

    /* to edit the data in database*/

    public function updateData($credit, $data = []) 
    {
        $em = $this->getEntityManager();

        $credit->setF039fname((int)$data['f039fname'])
             ->setF039fidGlcode((int)$data['f039fidGlcode'])
             ->setF039fupdatedBy((int)$_SESSION['userId']);

        $credit->setF039fupdatedDtTm(new \DateTime());

        
        $em->persist($credit);
        
        $em->flush();
 

    }


    
}

?>