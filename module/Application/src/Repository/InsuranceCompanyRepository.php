<?php
namespace Application\Repository;

use Application\Entity\T101finsuranceCompany;
use Application\Entity\T101finsuranceCompanyDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class InsuranceCompanyRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("ic.f101fid, ic.f101fproductName, ic.f101fidVendor, ic.f101fstatus,v.f030fcompanyName")
        ->from('Application\Entity\T101finsuranceCompany', 'ic')
        ->leftjoin('Application\Entity\T030fvendorRegistration', 'v', 'with', 'ic.f101fidVendor = v.f030fid')
        ->orderBy('ic.f101fid','DESC');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("ic.f101fid,icd.f101fidDetails, ic.f101fproductName, ic.f101fidVendor, ic.f101fstatus, icd.f101fage, icd.f101fyear1, icd.f101fyear2, icd.f101fyear3, icd.f101fyear4, icd.f101fyear5, icd.f101fyear6, icd.f101fyear7, icd.f101fyear8, icd.f101fyear9, icd.f101fyear10")
        ->from('Application\Entity\T101finsuranceCompany', 'ic')
        ->leftjoin('Application\Entity\T101finsuranceCompanyDetails', 'icd', 'with', 'icd.f101fidMaster = ic.f101fid')
        ->where('ic.f101fid = :insId')
        ->setParameter('insId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function createNewData($data) 
    {
      $em = $this->getEntityManager();
      $qb = $em->createQueryBuilder();

      $insure = new T101finsuranceCompany();

      $insure->setF101fproductName($data['f101fproductName'])
      ->setF101fidVendor((int)$data['f101fidVendor'])
      ->setF101fstatus((int)$data['f101fstatus'])
      ->setF101fcreatedBy((int)$_SESSION['userId']);

      $insure->setF101fcreatedDtTm(new \DateTime());

      $em->persist($insure);
      $em->flush();
      $insuranceDetails = $data['insurance-details'];
      foreach ($insuranceDetails as $detail) {
        $insureDetail = new T101finsuranceCompanyDetails();

        $insureDetail->setF101fidMaster($insure->getF101fid())
        ->setF101fage($detail['f101fage'])
        ->setF101fyear1($detail['f101fyear1'])
        ->setF101fyear2($detail['f101fyear2'])
        ->setF101fyear3($detail['f101fyear3'])
        ->setF101fyear4($detail['f101fyear4'])
        ->setF101fyear5($detail['f101fyear5'])
        ->setF101fyear6($detail['f101fyear6'])
        ->setF101fyear7($detail['f101fyear7'])
        ->setF101fyear8($detail['f101fyear8'])
        ->setF101fyear9($detail['f101fyear9'])
        ->setF101fyear10($detail['f101fyear10'])
        ->setF101fstatus((int)$detail['f101fstatus'])
        ->setF101fcreatedBy((int)$_SESSION['userId']);

        $insureDetail->setF101fcreatedDtTm(new \DateTime());
        
        $em->persist($insureDetail);
        $em->flush();
    }
    return $insure;
}
public function updateData($insure, $data = []) 
{
    $em = $this->getEntityManager();

    $insure->setF101fproductName($data['f101fproductName'])
    ->setF101fidVendor((int)$data['f101fidVendor'])
    ->setF101fstatus((int)$data['f101fstatus'])
    ->setF101fupdatedBy((int)$_SESSION['userId']);

    $insure->setF101fupdatedDtTm(new \DateTime());

    $em->persist($insure);
    $em->flush();
    return $insure;
}

public function updateInsuranceDetail($insuranceDetailObj, $data = [])
{
    $em = $this->getEntityManager();


    $insuranceDetailObj->setF101fage($data['f101fage'])
    ->setF101fyear1($data['f101fyear1'])
    ->setF101fyear2($data['f101fyear2'])
    ->setF101fyear3($data['f101fyear3'])
    ->setF101fyear4($data['f101fyear4'])
    ->setF101fyear5($data['f101fyear5'])
    ->setF101fyear6($data['f101fyear6'])
    ->setF101fyear7($data['f101fyear7'])
    ->setF101fyear8($data['f101fyear8'])
    ->setF101fyear9($data['f101fyear9'])
    ->setF101fyear10($data['f101fyear10'])

    ->setF101fstatus((int)$data['f101fstatus'])
    ->setF101fupdatedBy((int)$_SESSION['userId']);

    $insuranceDetailObj->setF101fupdatedDtTm(new \DateTime());

    $em->persist($insuranceDetailObj);
    $em->flush();

    return $insuranceDetailObj;

}
public function createInsuranceDetail($insuranceObj, $data = [])
{
  $em = $this->getEntityManager();
  $qb = $em->createQueryBuilder();

  $insure = new T101finsuranceCompanyDetails();

  $insure->setF101fidMaster($insuranceObj->getF101fid())
  ->setF101fage($data['f101fage'])
  ->setF101fyear1($data['f101fyear1'])
  ->setF101fyear2($data['f101fyear2'])
  ->setF101fyear3($data['f101fyear3'])
  ->setF101fyear4($data['f101fyear4'])
  ->setF101fyear5($data['f101fyear5'])
  ->setF101fyear6($data['f101fyear6'])
  ->setF101fyear7($data['f101fyear7'])
  ->setF101fyear8($data['f101fyear8'])
  ->setF101fyear9($data['f101fyear9'])
  ->setF101fyear10($data['f101fyear10'])
  ->setF101fstatus((int)$data['f101fstatus'])
  ->setF101fcreatedBy((int)$_SESSION['userId']);

  $insure->setF101fcreatedDtTm(new \DateTime());

  $em->persist($insure);
  $em->flush();

  return $insure;
}

public function vendorNotInInsuranceCompany()
{
 $em = $this->getEntityManager();
 $query = "select f030fcompany_name as f030fcompanyName,f030fid from t030fvendor_registration where f030fid not in (select f101fid_vendor from t101finsurance_company)"; 
 $result=$em->getConnection()->executeQuery($query)->fetchAll();
 return $result;
}   
}
