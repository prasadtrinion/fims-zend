<?php
namespace Application\Repository;

use Application\Entity\T020fadvancePayment;
use Application\Entity\T021fadvancePaymentDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class AdvancePaymentRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('a.f020fid,a.f020fidCustomer,a.f020fdocumentNumber,(a.f020famount - SUM(ad.f021famount)) as f020famount,a.f020fidNumber,a.f020ftype,a.f020fbalanceAmount,s.f060fname,a.f020fpaymentStatus')

            ->from('Application\Entity\T020fadvancePayment', 'a')
            ->leftjoin('Application\Entity\T060fstudent','s','with', 'a.f020fidCustomer = s.f060fid')
            ->leftjoin('Application\Entity\T021fadvancePaymentDetails','ad','with', 'ad.f021fidAdvance = a.f020fid');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       

        $result = array(
            'data' => $result,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('a.f020fid,a.f020fidCustomer,a.f020fdocumentNumber,a.f020famount,a.f020fidNumber,a.f020ftype,a.f020fbalanceAmount,s.f060fname,a.f020fpaymentStatus')

             ->from('Application\Entity\T020fadvancePayment', 'a')
            ->leftjoin('Application\Entity\T060fstudent','s','with', 'a.f020fidCustomer = s.f060fid')

            ->where('a.f020fid = :advanceId')
            ->setParameter('advanceId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
     
       

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $advance = new T020fadvancePayment();

        $advance->setF020fidNumber($data['f020fidNumber'])
                ->setF020fidCustomer($data['f020fidCustomer'])
                ->setF020fdocumentNumber($data['f020fdocumentNumber'])
                ->setF020famount((float)$data['f020famount'])
                ->setF020ftype($data['f020ftype'])
                ->setF020fpaymentStatus($data['f020fpaymentStatus'])
                ->setF020fbalanceAmount((float)$data['f020fbalanceAmount'])
                ->setF020fstatus((int)$data['f020fstatus'])
                ->setF020fcreatedBy((int)$_SESSION['userId'])
                ->setF020fupdatedBy((int)$_SESSION['userId']);

        $advance->setF020fcreatedDtTm(new \DateTime())
                ->setF020fupdatedDtTm(new \DateTime());


        try{
            $em->persist($advance);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $advance;
    }
 
    public function updateData($utility, $data = []) 
    {
        $em = $this->getEntityManager();

       $advance->setF020fidNumber($data['f020fidNumber'])
                ->setF020fidCustomer($data['f020fidCustomer'])
                ->setF020fdocumentNumber($data['f020fdocumentNumber'])
                ->setF020famount((float)$data['f020famount'])
                ->setF020ftype($data['f020ftype'])
                ->setF020fpaymentStatus($data['f020fpaymentStatus'])
                ->setF020fbalanceAmount((float)$data['f020fbalanceAmount'])
                ->setF020fstatus((int)$data['f020fstatus'])
                ->setF020fupdatedBy((int)$_SESSION['userId']);

        $advance->setF020fupdatedDtTm(new \DateTime());


        
        $em->persist($advance);
        $em->flush();

    }

    public function advancePaymentInvoiceUpdate($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $paymentIds = $data['paymentId'];
        $invoiceIds = $data['invoiceId'];
        $totalAdvanceAmount = 0;
        foreach ($paymentIds as $payment) {
            $query1 = "select f020famount from t020fadvance_payment where f020fid = $payment";
             $advance_result = $em->getConnection()->executeQuery($query1)->fetch();
             $totalAdvanceAmount = $totalAdvanceAmount + $advance_result['f020famount'];
        }
                 
             
        foreach ($invoiceIds as $invoice) {
             $query1 = "select f071finvoice_total,f071fbalance from t071finvoice where f071fid = $invoice";
             $invoice_result = $em->getConnection()->executeQuery($query1)->fetch();
             $invoice_amount = $invoice_result['f071finvoice_total'];
             $invoice_balace = $invoice_result['f071fbalance'];
             
             if($totalAdvanceAmount <=0){
                break;
             }
             else if($totalAdvanceAmount >= $invoice_amount ){
                $totalAdvanceAmount = $totalAdvanceAmount - $invoice_amount;
                $invoice_amount = 0;
            }
            else {
                $invoice_amount = $invoice_amount - $totalAdvanceAmount  ;   

                $totalAdvanceAmount =  0 ;
            }
             $query1 = "update t071finvoice set f071finvoice_total = $invoice_amount where f071fid = $invoice";
             $em->getConnection()->executeQuery($query1);
        }
        return;
        
    }

    public function knockOff($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $rows = $data['rows'];
        foreach ($rows as $row) {
           
        $advance = new T021fadvancePaymentDetails();

        $advance->setF021fidAdvance($row['id'])
                ->setF021famount($row['amount'])
                ->setF021fstatus((int)"1")
                ->setF021fcreatedBy((int)$_SESSION['userId'])
                ->setF021fupdatedBy((int)$_SESSION['userId']);

        $advance->setF021fcreatedDtTm(new \DateTime())
                ->setF021fupdatedDtTm(new \DateTime());


        try{
            $em->persist($advance);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        }
                 
        return;
        
    }
    
   

}
