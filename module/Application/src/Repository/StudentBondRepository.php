<?php
namespace Application\Repository;

use Application\Entity\T065fstudentBondInformation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StudentBondRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f065famount, s.f065fid, s.f065fidStudent, f.f066fname, s.f065fidInvoice, s.f065freturnDate, s.f065fidInvoiceDetails, s.f065fstatus, (st.f060fmatricNumber+'-'+st.f060fname) as f060fname, iv.f071finvoiceNumber, iv.f071finvoiceDate")

            ->from('Application\Entity\T065fstudentBondInformation', 's')
          ->leftjoin('Application\Entity\T060fstudent', 'st','with','s.f065fidStudent = st.f060fid')
            ->leftjoin('Application\Entity\T071finvoice', 'iv', 'with','s.f065fidInvoice = iv.f071fid')
            ->leftjoin('Application\Entity\T072finvoiceDetails', 'ivd', 'with','s.f065fidInvoiceDetails = ivd.f072fid')
            ->leftjoin('Application\Entity\T066ffeeItem', 'f', 'with','f.f066fid = ivd.f072fidItem')
            ->orderBy('s.f065fid','DESC');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $studentBond) {
            $date                        = $studentBond['f065freturnDate'];
            $studentBond['f065freturnDate'] = date("Y-m-d", strtotime($date));
            $date                        = $studentBond['f071finvoiceDate'];
            $studentBond['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $studentBond);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }
    public function getByApprovedStatus($id) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f065famount, s.f065fid, s.f065fidStudent, f.f066fname, s.f065fidInvoice, s.f065freturnDate, s.f065fidInvoiceDetails, s.f065fstatus,(st.f060fmatricNumber+'-'+st.f060fname) as f060fname, iv.f071finvoiceNumber, iv.f071finvoiceDate")

            ->from('Application\Entity\T065fstudentBondInformation', 's')
          ->leftjoin('Application\Entity\T060fstudent', 'st','with','s.f065fidStudent = st.f060fid')
            ->leftjoin('Application\Entity\T071finvoice', 'iv', 'with','s.f065fidInvoice = iv.f071fid')
            ->leftjoin('Application\Entity\T072finvoiceDetails', 'ivd', 'with','s.f065fidInvoiceDetails = ivd.f072fid')
            ->leftjoin('Application\Entity\T066ffeeItem', 'f', 'with','f.f066fid = ivd.f072fidItem')
            ->where('s.f065fstatus = :status')
            ->setParameter('status',$id);
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $studentBond) {
            $date                        = $studentBond['f065freturnDate'];
            $studentBond['f065freturnDate'] = date("Y-m-d", strtotime($date));
            $date                        = $studentBond['f071finvoiceDate'];
            $studentBond['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
           
            array_push($result1, $studentBond);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('s.f065famount,s.f065fid,s.f065fidStudent,s.f065fidInvoice,s.f065freturnDate,s.f065fidInvoiceDetails,s.f065fstatus,st.f060fname,iv.f071finvoiceNumber,iv.f071finvoiceDate')
        ->from('Application\Entity\T065fstudentBondInformation', 's')
      ->leftjoin('Application\Entity\T060fstudent', 'st','with','s.f065fidStudent = st.f060fid')
        ->leftjoin('Application\Entity\T071finvoice', 'iv', 'with','s.f065fidInvoice = iv.f071fid')
            ->where('s.f065fid = :studentId')
            ->setParameter('studentId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $studentBond) {
            $date                        = $studentBond['f065freturnDate'];
            $studentBond['f065freturnDate'] = date("Y-m-d", strtotime($date));
            $date                        = $studentBond['f071finvoiceDate'];
            $studentBond['f071finvoiceDate'] = date("Y-m-d", strtotime($date));
           
           
            array_push($result1, $studentBond);
        }

        return $result1;
    }

    public function createNewData($data) 
    {
        $datas = $data['id'];
       

 $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // $sql = "Select f072ftotal from  t072finvoice_details where 
        //         f072fid=(int)$invoiceDetailsId)";
        
        foreach($datas as $data)
        {
            $invoiceDetailsId = $data['f072fid'];


        // $query1 = "Select f072ftotal from  t072finvoice_details where 
        //         f072fid=$invoiceDetailsId";
       
      $qb->select('s.f072ftotal')
      ->from('Application\Entity\T072finvoiceDetails', 's')
      ->where('s.f072fid = :invoiceDetailsId')
      ->setParameter('invoiceDetailsId',$invoiceDetailsId);

      $query = $qb->getQuery();
      $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
 
      $invoiceDetailsAmount = $result[0]['f072ftotal'];
         // print_r($invoiceDetailsAmount);exit;



        $qb = $em->createQueryBuilder();
        
        $studentBond = new T065fstudentBondInformation();
        

        $studentBond->setF065fidStudent((int)$data['f060fid'])
                    ->setF065fidInvoice((int)$data['f071fid'])
                    ->setF065freturnDate(new \DateTime($data['returnDate']))
                    ->setF065fidInvoiceDetails((int)$data['f072fid'])
                    ->setF065fstatus((int)"0")
                    ->setF065famount((float)$invoiceDetailsAmount)
                    ->setF065fcreatedBy((int)$_SESSION['userId']);

        $studentBond->setF065fcreatedDtTm(new \DateTime());


        try{
            $em->persist($studentBond);
            $em->flush();
            // print_r($studentBond);exit;
        }
        catch(\Exception $e){
            echo $e;
        }
    }
        return $studentBond;
    }

    public function updateData($utility, $data = []) 
    {
        $em = $this->getEntityManager();

        $utility->setF078fpremise((int)$data['f078fpremise'])
                ->setF078finitialReading((int)$data['f078finitialReading'])
                ->setF078freadingDate(new \DateTime($data['f078freadingDate']))
                ->setF078flastUpdate(new \DateTime($data['f078flastUpdate']))
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fupdatedDtTm(new \DateTime());


        
        $em->persist($utility);
        $em->flush();

    }
    
    public function approve($data)
    {
        $em = $this->getEntityManager();
        $status = (int)$data['status'];
        // print_r($data);exit;
        foreach ($data['id'] as $id) {
            try{
            $query1 = "update t065fstudent_bond_information set f065fstatus = $status where f065fid = $id"; 

            $em->getConnection()->executeQuery($query1);
        }
        catch(\Exception $e){
            echo $e;
        }
    }
}

}
