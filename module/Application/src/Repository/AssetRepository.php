<?php
namespace Application\Repository;

use Application\Entity\T087fasset;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class AssetRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f087fid, a.f087fname, a.f087fstatus')
            ->from('Application\Entity\T087fasset','a')
            ->orderBy('a.f087fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f087fid,r.f087fname, r.f087fstatus')
            ->from('Application\Entity\T087fasset','r')
             ->where('r.f087fid = :assetId')
            ->setParameter('assetId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $asset = new T087fasset();

        $asset->setF087fname($data['f087fname'])
             ->setF087fstatus($data['f087fstatus'])
             ->setF087fcreatedBy((int)$_SESSION['userId'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fcreatedDtTm(new \DateTime())
                ->setF087fupdatedDtTm(new \DateTime());

 
        $em->persist($asset);
        $em->flush();

        return $asset;

    }

    public function updateData($asset, $data = []) 
    {
        $em = $this->getEntityManager();

        $asset->setF087fname($data['f087fname'])
             ->setF087fstatus($data['f087fstatus'])
             ->setF087fcreatedBy((int)$_SESSION['userId'])
             ->setF087fupdatedBy((int)$_SESSION['userId']);

        $asset->setF087fupdatedDtTm(new \DateTime());
        
        $em->persist($asset);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('a.f087fid, a.f087fname, a.f087fstatus')
            ->from('Application\Entity\T087fasset','a')
            ->orderBy('a.f087fname')
            ->where('a.f087fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>