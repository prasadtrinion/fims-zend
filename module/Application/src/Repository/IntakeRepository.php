<?php
namespace Application\Repository;

use Application\Entity\T067fintake;
use Application\Entity\T067fintakeDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class IntakeRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('i.f067fid, i.f067fname,i.f067ftype, i.f067fsemSequence, d.f011fdefinitionCode as semesterSequence, i.f067fdescription, i.f067fstatus, i.f067fintakeYear')
            ->from('Application\Entity\T067fintake', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'i.f067fsemSequence = d.f011fid')
            ->orderBy('i.f067fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }


    public function getIntakes($type) 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('i.f067fid, i.f067fname,i.f067ftype, i.f067fsemSequence, d.f011fdefinitionCode as semesterSequence, i.f067fdescription, i.f067fstatus, i.f067fintakeYear,i.f067ftype')
            ->from('Application\Entity\T067fintake', 'i')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'i.f067fsemSequence = d.f011fid')
            ->where("i.f067ftype ='".$type."'");
// echo $qb;
// die();
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('i.f067fid, i.f067fname,i.f067ftype, i.f067fsemSequence, d.f011fdefinitionCode as semesterSequence, i.f067fdescription, i.f067fintakeYear, i.f067fstatus, id.f067fidDetails, id.f067fidIntake, id.f067fidProgramme, id.f067fstartDate, id.f067fendDate, id.f067fidCategory')
            ->from('Application\Entity\T067fintake', 'i')
            ->leftjoin('Application\Entity\T067fintakeDetails', 'id', 'with', 'i.f067fid = id.f067fidIntake')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with', 'i.f067fsemSequence = d.f011fid')
            ->where('i.f067fid = :intakeId')
            ->setParameter('intakeId',(int)$id);
            
       $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $intake) 
        {
            $date                        = $intake['f067fstartDate'];
            $intake['f067fstartDate'] = date("Y-m-d", strtotime($date));

            $date                        = $intake['f067fendDate'];
            $intake['f067fendDate'] = date("Y-m-d", strtotime($date));
            
            array_push($result1, $intake);
        }


    return $result1;
    }

    public function createIntake($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $intake = new T067fintake();

        $intake->setF067fname($data['f067fname'])
            ->setF067fsemSequence((int)$data['f067fsemSequence'])
            ->setF067fdescription($data['f067fdescription'])
            ->setF067fintakeYear($data['f067fintakeYear'])
            ->setF067ftype($data['f067ftype'])
            ->setF067fstatus((int) $data['f067fstatus'])
            ->setF067fcreatedBy((int)$_SESSION['userId'])
            ->setF067fupdatedBy((int)$_SESSION['userId']);

        $intake->setF067fcreatedDtTm(new \DateTime())
            ->setF067fupdatedDtTm(new \DateTime());

        
            $em->persist($intake);
            $em->flush();
// print_r($intake);
// die();

        

        $intakeDetails = $data['intake-details'];

        foreach ($intakeDetails as $intakeDetail) {

            $intakeDetailObj = new T067fintakeDetails();

            $intakeDetailObj->setF067fidIntake($intake->getF067fid())
                ->setF067fidProgramme((int) $intakeDetail['f067fidProgramme'])
                ->setF067fstartDate(new \DateTime($intakeDetail['f067fstartDate']))
                ->setF067fendDate(new \DateTime($intakeDetail['f067fendDate']))
                ->setF067fidCategory((int)$intakeDetail['f067fidCategory'])
                ->setF067fstatus((int) $intakeDetail['f067fstatus'])
                ->setF067fcreatedBy((int)$_SESSION['userId'])
                ->setF067fupdatedBy((int)$_SESSION['userId']);
                

            $intakeDetailObj->setF067fcreatedDtTm(new \DateTime())
                ->setF067fupdatedDtTm(new \DateTime());
            
                $em->persist($intakeDetailObj);

                $em->flush();
                // print_r($intakeDetailObj);exit;
            
        }
        return $intake;
    }

    public function updateIntake($intake, $data = [])
    {
        $em = $this->getEntityManager();

        $intake->setF067fname($data['f067fname'])
            ->setF067fsemSequence((int)$data['f067fsemSequence'])
            ->setF067fdescription($data['f067fdescription'])
            ->setF067ftype($data['f067ftype'])
            ->setF067fintakeYear($data['f067fintakeYear'])
            ->setF067fstatus((int) $data['f067fstatus'])
            ->setF067fcreatedBy((int)$_SESSION['userId'])
            ->setF067fupdatedBy((int)$_SESSION['userId']);

        $intake->setF067fupdatedDtTm(new \DateTime());

        try {
            $em->persist($intake);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $intake;
    }


    public function updateIntakeDetail($intakeDetailObj, $intakeDetail)
    {
        
        $em = $this->getEntityManager();

        $intakeDetailObj->setF067fidProgramme((int) $intakeDetail['f067fidProgramme'])
                ->setF067fstartDate(new \DateTime($intakeDetail['f067fstartDate']))
                ->setF067fendDate(new \DateTime($intakeDetail['f067fendDate']))
                ->setF067fidCategory((int)$intakeDetail['f067fidCategory'])
                ->setF067fstatus((int) $data['f067fstatus'])
                ->setF067fcreatedBy((int)$_SESSION['userId'])
                ->setF067fupdatedBy((int)$_SESSION['userId']);

        $intakeDetailObj->setF067fupdatedDtTm(new \DateTime());
        try {
            $em->persist($intakeDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    
    public function createIntakeDetail($intakeObj, $intakeDetail)
    {
        $em = $this->getEntityManager();

       $intakeDetailObj = new T067fintakeDetails();

        $intakeDetailObj->setF067fidIntake($intakeObj->getF067fid())
                ->setF067fidProgramme((int) $intakeDetail['f067fidProgramme'])
                ->setF067fstartDate(new \DateTime($intakeDetail['f067fstartDate']))
                ->setF067fendDate(new \DateTime($intakeDetail['f067fendDate']))
                ->setF067fidCategory((int)$intakeDetail['f067fidCategory'])
                ->setF067fstatus((int) $data['f067fstatus'])
                ->setF067fcreatedBy((int)$_SESSION['userId'])
                ->setF067fupdatedBy((int)$_SESSION['userId']);

       $intakeDetailObj->setF067fcreatedDtTm(new \DateTime())
            ->setF067fupdatedDtTm(new \DateTime());
        try
        {
            $em->persist($intakeDetailObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }

    public function deleteIntakeDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t067fintake_details WHERE f067fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

}
