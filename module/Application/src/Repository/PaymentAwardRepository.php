<?php
namespace Application\Repository;

use Application\Entity\T128fpaymentAward;
use Application\Entity\T129fpaymentAwardDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PaymentAwardRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f128fid, p.f128faward, p.f128fstatus')
            ->from('Application\Entity\T128fpaymentAward', 'p')
            // ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f088fidDepartment = d.f015fdepartmentCode')
            ->orderBy('p.f128fid','DESC');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
  //       $result1 = array();
  //       foreach ($result as $item) {
  //           $date                        = $item['f088fdate'];
  //           $item['f088fdate'] = date("Y-m-d", strtotime($date));
  //           $date                        = $item['f088fcreatedDtTm'];
  //           $item['f088fcreatedDtTm'] = date("Y-m-d", strtotime($date));
  //           array_push($result1, $item);
  //       }
	 // $result = array(
  //           'data' => $result1
  //       );
	return $result;
    
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('p.f128fid, p.f128faward, p.f128fstatus, pd.f129fid, pd.f129fcalender, pd.f129famount')
            ->from('Application\Entity\T128fpaymentAward', 'p')
            ->leftjoin('Application\Entity\T129fpaymentAwardDetails', 'pd', 'with', 'p.f128fid = pd.f129fidAward')
            ->where('p.f128fid = :purchaseId')
            ->setParameter('purchaseId',(int)$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	   // $result1 = array();
    //     foreach ($result as $item) {
    //         $date                        = $item['f088fdate'];
    //         $item['f088fdate'] = date("Y-m-d", strtotime($date));
	   //  $date                        = $item['f088frequiredDate'];
    //         $item['f088frequiredDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f088fcreatedDtTm'];
    //         $item['f088fcreatedDtTm'] = date("Y-m-d", strtotime($date));
    //         array_push($result1, $item);
    //     }

        return $result;
    }

    public function createPurchase($data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
      
      
        // $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        // $numberData = array();
        // $numberData['type']="PR";
        // $number = $configRepository->generateFIMS($numberData);

        $award = new T128fpaymentAward();

        $award->setF128faward($data['f128faward'])
        	->setF128fstatus($data['f128fstatus'])
        	->setF128fcreatedBy((int)$_SESSION['userId'])
            ->setF128fupdatedBy((int)$_SESSION['userId']);

        $award->setF128fcreatedDtTm(new \DateTime())
            ->setF128fupdatedDtTm(new \DateTime());

        try{
            $em->persist($award);
            // print_r($purchase);
            // die();
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        

        $awardDetails = $data['award-details'];

        foreach ($awardDetails as $awardDetail)
        {

            $awardDetailtab = new T129fpaymentAwardDetails();

            $awardDetailtab->setF129fidAward((int)$award->getF128fid())
            ->setF129fcalender(new \DateTime($awardDetail['f129fcalender']))
            ->setF129famount((float)$awardDetail['f129famount'])
            ->setF129fstatus((int)$awardDetail['f129fstatus'])
            ->setF129fcreatedBy((int)$_SESSION['userId']);
                

            $awardDetailtab->setF129fcreatedDtTm(new \DateTime());
            try{
                $em->persist($awardDetailtab);
                $em->flush();
				}
			catch(\Exception $e)
			{
				echo $e;
			}
            
        }
        return $purchase;
    }

    public function updateAwardMaster($award, $data = [])
    {
        $em = $this->getEntityManager();

        $award->setF128faward((int)$data['f128faward'])
        	->setF128fstatus((int)$data['f128fstatus'])
            ->setF129fupdatedBy((int)$_SESSION['userId']);

        $award->setF088fupdatedDtTm(new \DateTime());

        try {
            $em->persist($award);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $award;
    }


    public function updateAwardDetail($purchaseDetailObj, $purchaseDetail)
    {
        
        $em = $this->getEntityManager();

        $awardDetailtab->setF129fcalender(new \DateTime($awardDetail['f129fcalender']))
            ->setF129famount((float)$awardDetail['f129famount'])
            ->setF129fstatus((int)$awardDetail['f129fstatus'])
            ->setF129fupdatedBy((int)$_SESSION['userId']);
                

            $awardDetailtab->setF129fupdatedDtTm(new \DateTime());
        try {
            $em->persist($purchaseDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }


    public function createAwardDetail($awardObj, $data)
    {
    	$em = $this->getEntityManager();
    	$awardDetailtab = new T129fpaymentAwardDetails();

            $awardDetailtab->setF129fidAward((int)$awardObj->getF128fid())
            ->setF129fcalender(new \DateTime($data['f129fcalender']))
            ->setF129famount((float)$data['f129famount'])
            ->setF129fstatus((int)$data['f129fstatus'])
            ->setF129fcreatedBy((int)$_SESSION['userId']);
                

            $awardDetailtab->setF129fcreatedDtTm(new \DateTime());
            try{
                $em->persist($awardDetailtab);
                $em->flush();
            }
			catch(\Exception $e)
			{
				echo $e;
			}
        return $purchase;
    }
}