<?php
namespace Application\Repository;

use Application\Entity\T126ftenderAggrement;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TenderAggrementRepository extends EntityRepository 
{
    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select("ta.f126fid, ta.f126fregisterNo, ta.f126fidTender, ta.f126fidVender, ta.f126fstatus, tq.f035fquotationNumber, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle, tq.f035fidDepartment,  vr.f030fcompanyName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName ")
            ->from('Application\Entity\T126ftenderAggrement', 'ta')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq', 'with', 'ta.f126fidTender = tq.f035fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'ta.f126fidVender = vr.f030fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with', 'tq.f035fidDepartment = dep.f015fdepartmentCode')
            ->orderBy('ta.f126fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f035fstartDate'];
            $item['f035fstartDate'] = date("Y-m-d", strtotime($date));

            $date                       = $item['f035fendDate'];
            $item['f035fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select("ta.f126fid, ta.f126fregisterNo, ta.f126fidTender, ta.f126fidVender, ta.f126fstatus, tq.f035fquotationNumber, tq.f035fstartDate, tq.f035fendDate, tq.f035ftitle, tq.f035fidDepartment,  vr.f030fcompanyName, (dep.f015fdepartmentCode+'-'+dep.f015fdepartmentName) as DepartmentName ")
            ->from('Application\Entity\T126ftenderAggrement', 'ta')
            ->leftjoin('Application\Entity\T035ftenderQuotation', 'tq', 'with', 'ta.f126fidTender = tq.f035fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'vr', 'with', 'ta.f126fidVender = vr.f030fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with', 'tq.f035fidDepartment = dep.f015fdepartmentCode')
            ->where('ta.f126fid = :tenderID')
            ->setParameter('tenderID',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f035fstartDate'];
            $item['f035fstartDate'] = date("Y-m-d", strtotime($date));

            $date                       = $item['f035fendDate'];
            $item['f035fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="AG";
        $number = $configRepository->generateFIMS($numberData);
        // echo $number;
        // die();
        $tender = new T126ftenderAggrement();

        $tender->setF126fregisterNo($number)
        		->setF126fidTender((int)$data['f126fidTender'])
        		->setF126fidVender((int)$data['f126fidVender'])
                ->setF126fstatus((int)$data['f126fstatus'])
                ->setF126fcreatedBy((int)$_SESSION['userId'])
                ->setF126fupdatedBy((int)$_SESSION['userId']);

        $tender->setF126fcreatedDtTm(new \DateTime());

        try
        {
            $em->persist($tender);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $eaFrom;
    }

    public function updateData($tender, $data = []) 
    {
        $em = $this->getEntityManager();
    
        $tender->setF126fregisterNo($number)
        		->setF126fidTender((int)$data['f126fidTender'])
        		->setF126fidVender((int)$data['f126fidVender'])
                ->setF126fstatus((int)$data['f126fstatus'])
                ->setF126fcreatedBy((int)$_SESSION['userId'])
                ->setF126fupdatedBy((int)$_SESSION['userId']);

        $tender->setF126fupdatedDtTm(new \DateTime());

        $em->persist($tender);
        $em->flush();
    }
}