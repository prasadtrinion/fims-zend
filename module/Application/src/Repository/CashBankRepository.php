<?php
namespace Application\Repository;

use Application\Entity\T096fcashBank;
use Application\Entity\T097fcashBankDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class CashBankRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select(' b.f042fid, b.f042fbankName')
            ->from('Application\Entity\T096fcashBank', 'c')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'bs', 'with','c.f096fidBank = bs.f041fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
            ->groupBy(' b.f042fid')
            ->orderBy('b.f042fbankName','ASC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('c.f096fid, c.f096fidBank, b.f042fbankName, c.f096fidPayment, c.f096ftype, c.f096famount')
           ->from('Application\Entity\T096fcashBank', 'c')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'bs', 'with','c.f096fidBank = bs.f041fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
            ->where('c.f096fid = :cashbankId')
            ->setParameter('cashbankId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createCashbank($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $cashbank = new T096fcashBank();

        $cashbank->setF096fidBank((int)$data['f096fidBank'])
            ->setF096fidPayment((int)$data['f096fidPayment'])
            ->setF096ftype((int)$data['f096ftype'])
            ->setF096famount((float)$data['f096famount'])
            ->setF096fstatus((int) $data['f096fstatus'])
            ->setF096ftransactionDate(new \DateTime($data['f096ftransactionDate']))
            ->setF096fcreatedBy((int)$_SESSION['userId'])
            ->setF096fupdatedBy((int)$_SESSION['userId']);

        $cashbank->setF096fcreatedDtTm(new \DateTime())
            ->setF096fupdatedDtM(new \DateTime());

        
            $em->persist($cashbank);

            $em->flush();

        

        $cashbankDetails = $data['cashbank-details'];

        foreach ($cashbankDetails as $cashbankDetail) {

            $cashbankDetailObj = new T097fcashBankDetails();

            $cashbankDetailObj->setF097fidCashBank($cashbank->getF096fid())
                ->setF097fdebitFundCode($cashbankDetail['f097fdebitFundCode'])
                ->setF097fdebitDepartmentCode($cashbankDetail['f097fdebitDepartmentCode'])
                ->setF097fdebitActivityCode($cashbankDetail['f097fdebitActivityCode'])
                ->setF097fdebitAccountCode($cashbankDetail['f097fdebitAccountCode'])

                ->setF097fcreditFundCode($cashbankDetail['f097fcreditFundCode'])
                ->setF097fcreditDepartmentCode($cashbankDetail['f097fcreditDepartmentCode'])
                ->setF097fcreditActivityCode($cashbankDetail['f097fcreditActivityCode'])
                ->setF097fcreditAccountCode($cashbankDetail['f097fcreditAccountCode'])

                ->setF097fcreditAmount((float)$cashbankDetail['f097fcreditAmount'])
                ->setF097fdebitAmount((float)$cashbankDetail['f097fdebitAmount'])
                ->setF097fstatus((int) $cashbankDetail['f097fstatus'])
                
                ->setF097fcreatedBy((int)$_SESSION['userId'])
                ->setF097fupdatedBy((int)$_SESSION['userId']);
                

            $cashbankDetailObj->setF097fcreatedDtTm(new \DateTime())
                ->setF097fupdatedDtTm(new \DateTime());
            
                $em->persist($cashbankDetailObj);

                $em->flush();

            
        }
        return $cashbank;
    }

    public function updateCashbank($cashbank, $data = [])
    {
        $em = $this->getEntityManager();

        $cashbank->setF096fidBank((int)$data['f096fidBank'])
            ->setF096fidPayment((int)$data['f096fidPayment'])
            ->setF096ftype((int)$data['f096ftype'])
            ->setF096famount((float)$data['f096famount'])
            ->setF096fstatus((int) $data['f096fstatus'])
            ->setF096ftransactionDate(new \DateTime($data['f096ftransactionDate']))
            ->setF096fcreatedBy((int)$_SESSION['userId'])
            ->setF096fupdatedBy((int)$_SESSION['userId']);

        $cashbank->setF096fupdatedDtM(new \DateTime());
        try {
            $em->persist($cashbank);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $cashbank;
    }


    public function updateCashbankDetail($cashbankDetailObj, $cashbankDetail)
    {
        
        $em = $this->getEntityManager();

        $cashbankDetailObj->setF097fidCashBank($cashbankDetail)
                ->setF097fcrGlcodeId((int) $cashbankDetail['f097fcrGlcodeId'])
                ->setF097fdbGlcodeId((int)$cashbankDetail['f097fdbGlcodeId'])
                ->setF097fcreditAmount((float)$cashbankDetail['f097fcreditAmount'])
                ->setF097fdebitAmount((float)$cashbankDetail['f097fdebitAmount'])
                ->setF097fstatus((int) $cashbankDetail['f097fstatus'])
                ->setF097fcreatedBy((int)$_SESSION['userId'])
                ->setF097fupdatedBy((int)$_SESSION['userId']);

        $cashbankDetailObj->setF097fupdatedDtTm(new \DateTime());
        try {
            $em->persist($cashbankDetailObj);
            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
    }
    
    public function createCashbankDetail($cashbankObj, $cashbankDetail)
    {
        $em = $this->getEntityManager();
        // print_r($cashbankObj);
        // exit();

       $cashbankDetailObj = new T097fcashBankDetails();

        $cashbankDetailObj->setF097fidCashBank($cashbankObj->getF096fid())
                ->setF097fcrGlcodeId((int) $cashbankDetail['f097fcrGlcodeId'])
                ->setF097fdbGlcodeId((int)$cashbankDetail['f097fdbGlcodeId'])
                ->setF097fcreditAmount((float)$cashbankDetail['f097fcreditAmount'])
                ->setF097fdebitAmount((float)$cashbankDetail['f097fdebitAmount'])
                ->setF097fstatus((int) $cashbankDetail['f097fstatus'])
                ->setF097fcreatedBy((int)$_SESSION['userId'])
                ->setF097fupdatedBy((int)$_SESSION['userId']);

       $cashbankDetailObj->setF097fcreatedDtTm(new \DateTime())
            ->setF097fupdatedDtTm(new \DateTime());
        try
        {
            $em->persist($cashbankDetailObj);
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }
    public function getCashBankDetail($postData) 
    {

        $em = $this->getEntityManager();

        $bank = $postData['bank'];
        $qb = $em->createQueryBuilder();

         $qb->select(' b.f042fbankName')
            ->from('Application\Entity\T096fcashBank', 'c')
            ->leftjoin('Application\Entity\T041fbankSetupAccount', 'bs', 'with','c.f096fidBank = bs.f041fid')
            ->leftjoin('Application\Entity\T042fbank', 'b', 'with','bs.f041fidBank = b.f042fid')
            ->where('c.f096fidBank = :bankId')
            ->setParameter('bankId',$bank)
            ->groupBy(' b.f042fid');
            
        $query = $qb->getQuery();
        $value = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $bank_val = $value[0]['f042fbankName'];

        // print_r($value);
        // die();
        $name = $postData['file'];
        // $name = "Loans.csv";
        $data= \file('/var/www/html/fims-zend/public/files/'.$name);
        for($i=0;$i<count($data);$i++)
        {
         $row = explode(",",$data[$i]);
         $document = trim($row[0], '"');
         $resource = trim($row[1], '"');
         $transaction_date = trim($row[2], '"');
         $debit_amount= trim($row[3], '"');
         $credit_amount = trim($row[4], '"');
         $bank_name = trim($row[5], '"');
         $bank_name = str_replace("\n", "", $bank_name);

         // print_r($bank_name);
         // exit();
            
         $insert_query = "insert into cash_bank_file(document_no, resource, transaction_date, debit_amount, credit_amount, bank) values ('$document','$resource','$transaction_date','$debit_amount','$credit_amount', '$bank_name')";
           
            $em->getConnection()->executeQuery($insert_query);

        }
        
        $date = $postData['fromDate'];
        $postData['fromDate'] = date("Y-m-d", strtotime($date));

        $date = $postData['endDate'];
        $postData['endDate'] = date("Y-m-d", strtotime($date));

        $from_date = $postData['fromDate'];
        $to_date = $postData['endDate'];

        $query = "SELECT t096fcash_bank.f096fid, t096fcash_bank.f096fid_bank, t096fcash_bank.f096fid_payment, t096fcash_bank.f096ftype, t096fcash_bank.f096ftransaction_date,t097fcash_bank_details.f097fcredit_amount, t097fcash_bank_details.f097fdebit_amount
            FROM t096fcash_bank inner join t097fcash_bank_details on t097fcash_bank_details.f097fid_cash_bank = t096fcash_bank.f096fid WHERE t096fcash_bank.f096ftransaction_date BETWEEN '" . $from_date . "' AND  '" . $to_date . "' AND t096fcash_bank.f096fid_bank = $bank AND t096fcash_bank.f096fapproval_status=0";
        $data =$em->getConnection()->executeQuery($query)->fetchAll();
       
        
        $query2 = "SELECT * FROM cash_bank_file WHERE transaction_date BETWEEN '" . $from_date . "' AND  '" . $to_date . "' AND  bank = '$bank_val'";
        $file_data =$em->getConnection()->executeQuery($query2)->fetchAll();
//         echo $query2;
// print_r($file_data);
// die();
        $result = $result[0];
       
        $result['result'] = $data;
        $result['file_data'] = $file_data;
        
        return $result;
    
    }

    public function cashBankApproval($details,$status,$reason)
    {
        $em = $this->getEntityManager();

        foreach ($details as $detail) 
        {
            $Id = $detail->getF096fid();
            $id = (int)$Id;

            $query = "UPDATE t096fcash_bank set f096freason='$reason', f096fapproval_status=$status where f096fid = $id"; 

            $result=$em->getConnection()->executeQuery($query);
        }
    }
    public function getManualCashBankDetail($postData) 
    {

        $em = $this->getEntityManager();

        $bank = $postData['bank'];
        
        $date = $postData['fromDate'];
        $postData['fromDate'] = date("Y-m-d", strtotime($date));

        $date = $postData['endDate'];
        $postData['endDate'] = date("Y-m-d", strtotime($date));

        $from_date = $postData['fromDate'];
        $to_date = $postData['endDate'];

        $query = "SELECT t096fcash_bank.f096fid, t096fcash_bank.f096fid_bank, t096fcash_bank.f096fid_payment, t096fcash_bank.f096ftype, t096fcash_bank.f096ftransaction_date,t097fcash_bank_details.f097fcredit_amount, t097fcash_bank_details.f097fdebit_amount
            FROM t096fcash_bank inner join t097fcash_bank_details on t097fcash_bank_details.f097fid_cash_bank = t096fcash_bank.f096fid WHERE t096fcash_bank.f096ftransaction_date BETWEEN '" . $from_date . "' AND  '" . $to_date . "' AND t096fcash_bank.f096fid_bank = $bank AND t096fcash_bank.f096fapproval_status=0";
        $data =$em->getConnection()->executeQuery($query)->fetchAll();
        
        return $data;
    
    }

}
