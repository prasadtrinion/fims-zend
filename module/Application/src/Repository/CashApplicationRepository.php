<?php
namespace Application\Repository;

use Application\Entity\T052fcashApplication;
use Application\Entity\T052fcashApplicationDetails;
use Application\Entity\T091fbillRegistration;
use Application\Entity\T091fbillRegistrationDetails;
use Application\Entity\T011finitialConfig;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CashApplicationRepository extends EntityRepository 
{
    /* to retrive the data from database*/
    public function getList() 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        // Query
        $qb->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as financialYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->orderBy('c.f052fid','DESC');

        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    }

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, c.f052fidDepartment, c.f052fidFinancialYear, c.f052fstatus, cd.f052fidDetails, cd.f052fidCashType, cd.f052fitem, cd.f052ffund, cd.f052factivity, cd.f052fdepartment, cd.f052faccount, cd.f052fsoCode, cd.f052famount, cd.f052fquantity, cd. f052ftotalAmount, c.f052ftotal, c.f052fapprovalStatus, cd.f052ftaxCode, cd.f052funit, cd.f052fbudgetFund, cd.f052fbudgetActivity, cd.f052fbudgetDepartment, cd.f052fbudgetAccount')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T052fcashApplicationDetails', 'cd', 'with', 'c.f052fid = cd.f052fidCashApplication')
            ->where('c.f052fid = :cashApplicationId')
            ->setParameter('cashApplicationId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
    public function createCash($data)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $cash = new T052fcashApplication();

        $cash->setF052fcashAdvance($data['f052fcashAdvance'])
             ->setF052fidStaff((int)$data['f052fidStaff'])
             ->setF052fidDepartment($data['f052fidDepartment'])
             ->setF052fidFinancialYear((int)$data['f052fidFinancialYear'])
             ->setF052fstatus((int)$data['f052fstatus'])
             ->setF052ftotal((float)$data['f052ftotal'])
             ->setF052fapprovalStatus(0)
             ->setF052fisProcessed(0)
             ->setF052fcreatedBy((int)$_SESSION['userId'])
             ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cash->setF052fcreatedDtTm(new \DateTime())
                ->setF052fupdatedDtTm(new \DateTime());
        try{
        $em->persist($cash);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        $cashDetails = $data['cash-details'];

        foreach ($cashDetails as $cashDetail)
        {

            $cashDetailObj = new T052fcashApplicationDetails();

            $cashDetailObj->setF052fidCashApplication((int)$cash->getF052fid())
                       ->setF052fidCashType((int)$cashDetail['f052fidCashType'])
                       ->setF052fitem($cashDetail['f052fitem'])
                       ->setF052ffund($cashDetail['f052ffund'])
                       ->setF052factivity($cashDetail['f052factivity'])
                       ->setF052fdepartment($cashDetail['f052fdepartment'])
                       ->setF052faccount($cashDetail['f052faccount'])
                       ->setF052fsoCode((int)$cashDetail['f052fsoCode'])
                       ->setF052famount((float)$cashDetail['f052famount'])
                       ->setF052fquantity((int)$cashDetail['f052fquantity'])
                       ->setF052ftotalAmount((float)$cashDetail['f052ftotalAmount'])
                       ->setF052fstatus((int)$cashDetail['f052fstatus'])
                       ->setF052ftaxCode($cashDetail['f052ftaxCode'])
                       ->setF052funit($cashDetail['f052funit'])
                       ->setF052fbudgetFund($cashDetail['f052fbudgetFund'])
                       ->setF052fbudgetActivity($cashDetail['f052fbudgetActivity'])
                       ->setF052fbudgetDepartment($cashDetail['f052fbudgetDepartment'])
                       ->setF052fbudgetAccount($cashDetail['f052fbudgetAccount'])
                       ->setF052fcreatedBy((int)$_SESSION['userId'])
                       ->setF052fupdatedBy((int)$_SESSION['userId']);

            $cashDetailObj->setF052fcreatedDtTm(new \DateTime())
                       ->setF052fupdatedDtTm(new \DateTime());
            
            $em->persist($cashDetailObj);
            $em->flush();            
            
        
        }
        return $cash;
    }
   public function updateCash($cash, $data = []) 
    {
        $em = $this->getEntityManager();

         $cash->setF052fidStaff((int)$data['f052fidStaff'])
             ->setF052fidDepartment($data['f052fidDepartment'])
             ->setF052fidFinancialYear((int)$data['f052fidFinancialYear'])
             ->setF052fstatus((int)$data['f052fstatus'])
             ->setF052ftotal((float)$data['f052ftotal'])
             ->setF052fcreatedBy((int)$_SESSION['userId'])
             ->setF052fapprovalStatus(0)
             ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cash->setF052fupdatedDtTm(new \DateTime());
        
        $em->persist($cash);
        $em->flush();
        

        return $cash;
    }


    public function updateCashDetail($cashDetailObj, $cashDetail)
    {
        
        $em = $this->getEntityManager();

        $cashDetailObj->setF052fidCashType((int)$cashDetail['f052fidCashType'])
                       ->setF052fitem($cashDetail['f052fitem'])
                       ->setF052ffund($cashDetail['f052ffund'])
                       ->setF052factivity($cashDetail['f052factivity'])
                       ->setF052fdepartment($cashDetail['f052fdepartment'])
                       ->setF052faccount($cashDetail['f052faccount'])
                       ->setF052fsoCode((int)$cashDetail['f052fsoCode'])
                       ->setF052famount((float)$cashDetail['f052famount'])
                       ->setF052fquantity((int)$cashDetail['f052fquantity'])
                       ->setF052ftotalAmount((float)$cashDetail['f052ftotalAmount'])
                       ->setF052fstatus((int)$cashDetail['f052fstatus'])
                       ->setF052ftaxCode($cashDetail['f052ftaxCode'])
                       ->setF052funit($cashDetail['f052funit'])
                       ->setF052fbudgetFund($cashDetail['f052fbudgetFund'])
                       ->setF052fbudgetActivity($cashDetail['f052fbudgetActivity'])
                       ->setF052fbudgetDepartment($cashDetail['f052fbudgetDepartment'])
                       ->setF052fbudgetAccount($cashDetail['f052fbudgetAccount'])
                       ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cashDetailObj->setF052fupdatedDtTm(new \DateTime());

        $em->persist($cashDetailObj);
        $em->flush();   
    }
    public function createCashDetail($cashObj, $cashDetail)
    {
        $em = $this->getEntityManager();

        $cashDetailObj = new T052fcashApplicationDetails();

        $cashDetailObj->setF052fidCashApplication((int)$cashObj->getF052fid())
                       ->setF052fidCashType((int)$cashDetail['f052fidCashType'])
                       ->setF052fitem($cashDetail['f052fitem'])
                       ->setF052ffund($cashDetail['f052ffund'])
                       ->setF052factivity($cashDetail['f052factivity'])
                       ->setF052fdepartment($cashDetail['f052fdepartment'])
                       ->setF052faccount($cashDetail['f052faccount'])
                       ->setF052fsoCode((int)$cashDetail['f052fsoCode'])
                       ->setF052famount((float)$cashDetail['f052famount'])
                       ->setF052fquantity((int)$cashDetail['f052fquantity'])
                       ->setF052ftotalAmount((float)$cashDetail['f052ftotalAmount'])
                       ->setF052fstatus((int)$cashDetail['f052fstatus'])
                       ->setF052ftaxCode($cashDetail['f052ftaxCode'])
                       ->setF052funit($cashDetail['f052funit'])
                       ->setF052fbudgetFund($cashDetail['f052fbudgetFund'])
                       ->setF052fbudgetActivity($cashDetail['f052fbudgetActivity'])
                       ->setF052fbudgetDepartment($cashDetail['f052fbudgetDepartment'])
                       ->setF052fbudgetAccount($cashDetail['f052fbudgetAccount'])
                       ->setF052fcreatedBy((int)$_SESSION['userId'])
                       ->setF052fupdatedBy((int)$_SESSION['userId']);

        $cashDetailObj->setF052fcreatedDtTm(new \DateTime())
                       ->setF052fupdatedDtTm(new \DateTime());
        $em->persist($cashDetailObj);
        $em->flush();

        return $cashDetailObj;   
    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        

           if($approvedStatus == 0 && isset($approvedStatus)){
            $qb->select("c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as financialYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal, (c.f052fidDepartment+'-'+dep.f015fdepartmentName) as Department")
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with', 'dep.f015fdepartmentCode = c.f052fidDepartment')
            ->where('c.f052fapprovalStatus = 0')
            ->orderBy('c.f052fid','DESC');
            }
            elseif($approvedStatus == 1 && isset($approvedStatus)){
            $qb->select("c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as financialYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal, (c.f052fidDepartment+'-'+dep.f015fdepartmentName) as Department")
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with', 'dep.f015fdepartmentCode = c.f052fidDepartment')
                 ->where('c.f052fapprovalStatus = 1')
                 ->orderBy('c.f052fid','DESC');
            }
            elseif ($approvedStatus == 2 && isset($approvedStatus)) {
            $qb->select("c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as financialYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal, (c.f052fidDepartment+'-'+dep.f015fdepartmentName) as Department")
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'dep', 'with', 'dep.f015fdepartmentCode = c.f052fidDepartment')
            ->orderBy('c.f052fid','DESC');
                
            }
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function cashApplicationApproval($cashes) 
    {
        $em = $this->getEntityManager();

        foreach ($cashes as $cash) 
        {

            $cash->setF052fapprovalStatus((int)"1")
                 ->setF052fUpdatedDtTm(new \DateTime());
       
            $em->persist($cash);
            $em->flush();

            $Id=$cash->getF052fid();
            $id=(int)$Id;
             $year = date('y');
            $Year = date('Y');

            $data=array();
            $data['type']='BIL';
            $initialRepository = $em->getRepository("Application\Entity\T011finitialConfig");
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f091freferenceNumber'] = $generateNumber;
	    
	         
	       
           //  $query = "SELECT f091freference_number FROM t091fbill_registration order by f091fid desc";
           //  $result = $em->getConnection()->executeQuery($query)->fetch();
           
           //  $data=$result['f091freference_number'];
           //  $value=substr($data, 3,6);

           //  $count=$value + 1;
           //  $number = "BIL" . (sprintf("%'06d", $count)) . "/" . $year;
            


            $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();
        // Query
           $qb->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment, c.f052fidFinancialYear, c.f052fstatus, cd.f052fidDetails, cd.f052fidCashType, cd.f052fitem, cd.f052ffund, cd.f052factivity, cd.f052fdepartment, cd.f052faccount, cd.f052fsoCode, cd.f052famount, cd.f052fquantity, cd. f052ftotalAmount, c.f052ftotal, c.f052fapprovalStatus, cd.f052ftaxCode, cd.f052funit,cd.f052fitem, cd.f052fbudgetFund, cd.f052fbudgetActivity, cd.f052fbudgetDepartment, cd.f052fbudgetAccount')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T052fcashApplicationDetails', 'cd', 'with', 'c.f052fid = cd.f052fidCashApplication')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->where('cd.f052fidCashApplication=:cashId')
             ->setParameter('cashId',$id);

        $query = $qb->getQuery();
        $results = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	
	    
    
            $staff= $results[0]['staffName'];
            $total = $results[0]['f052ftotal'];
            
            

            $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();

     //        $bill = new T091fbillRegistration();

     //        $bill->setF091freferenceNumber($postData['f091freferenceNumber'])
     //        ->setF091fcreatedDate($cash->getF052fcreatedDtTm())
     //        ->setF091ftotalAmount((float)$cash->getF052ftotal())
     //        ->setF091fapprovedStatus((int)1)
     //        ->setF091ftype('Cash')
     //        ->setF091fpaymentStatus((int)0)
     //        ->setF091fcontactPerson1('NULL')
     //        ->setF091fcontactPerson2('NULL')
     //        ->setF091fcontactPerson3($staff)
     //        ->setF091fstatus((int)1)
     //        ->setF091fcreatedBy((int)$_SESSION['userId'])
     //        ->setF091fupdatedBy((int)$_SESSION['userId']);

     //        $bill->setF091fcreatedDtTm(new \DateTime())
     //        ->setF091fupdatedDtTm(new \DateTime());
        
     //        $em->persist($bill);
     //        $em->flush();
     //        // print_r($bill);
     //        // exit();
            
	    // $em = $this->getEntityManager();
     //        $qb = $em->createQueryBuilder();

     //        foreach ($results as $result) 
     //        {

          

     //        $billDetailObj = new T091fbillRegistrationDetails();

     //        $billDetailObj->setF091fidBill((int)$bill->getF091fid())
     //            ->setF091freferenceNumber($cash->getF052fcashAdvance())
     //            ->setF091fidItem((int)$result['f052fitem'])
     //            ->setF091ftype('NULL')
     //            ->setF091fsoCode($result['f052fsoCode'])
     //            ->setF091fquantity((int)$result['f052fquantity'])
     //            ->setF091funit($result['f052funit'])
     //            ->setF091fprice((float)$result['f052famount'])
     //            ->setF091total((float)$result['f052ftotalAmount'])
     //            ->setF091fpercentage((float)0)
     //            ->setF091ftaxCode((int)0)
     //            ->setF091fstatus((int)1)
     //            ->setF091ftaxAmount((float)0)
     //            ->setF091ftotalIncTax((float)$result['f052ftotalAmount'])
     //            ->setF091ffundCode($result['f052ffund'])
     //            ->setF091fdepartmentCode($result['f052fdepartment'])
     //            ->setF091factivityCode($result['f052factivity'])
     //            ->setF091faccountCode($result['f052faccount'])
     //            ->setF091fbudgetFundCode($result['f052fbudgetFund'])
     //            ->setF091fbudgetDepartmentCode($result['f052fbudgetDepartment'])
     //            ->setF091fbudgetActivityCode($result['f052fbudgetActivity'])
     //            ->setF091fbudgetAccountCode($result['f052fbudgetAccount'])
     //            ->setF091fcreatedBy((int)$_SESSION['userId'])
     //            ->setF091fupdatedBy((int)$_SESSION['userId']);


                

     //        $billDetailObj->setF091fcreatedDtTm(new \DateTime())
     //            ->setF091fupdatedDtTm(new \DateTime());

     //            // print_r($billDetailObj);
     //            // exit();
     //        try{   
     //                $em->persist($billDetailObj);
        
     //                $em->flush();
            
     //            }
     //            catch(\Exception $e){
     //                echo $e;
     //            }
            //print_r($billDetailObj);
//exit();
            // }
        }
        return $bill;
    }

    public function getCashApplicationStaffList($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fid, c.f052fcashAdvance,  c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,  c.f052fidFinancialYear, f.f015fname as financialYear,  c.f052fstatus, cd.f052fidDetails, cd.f052fidCashType, cd.f052fitem, cd.f052ffund, cd.f052factivity, cd.f052fdepartment, cd.f052faccount, cd.f052fsoCode, cd.f052famount, cd.f052fquantity, cd.f052ftotalAmount, c.f052ftotal, cd.f052ftaxCode, cd.f052funit, cd.f052fbudgetFund, cd.f052fbudgetActivity, cd.f052fbudgetDepartment, cd.f052fbudgetAccount')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T052fcashApplicationDetails', 'cd', 'with', 'c.f052fid = cd.f052fidCashApplication')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->where('c.f052fidStaff = :staffId')
            ->setParameter('staffId',$id)
            ->orderBy('c.f052fid','DESC');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getNonProcessedList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fid, c.f052fcashAdvance,  c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,  c.f052fidFinancialYear, f.f015fname as financialYear,  c.f052fstatus, c.f052ftotal, c.f052fisProcessed, c.f052fapprovalStatus')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->where('c.f052fapprovalStatus =1')
            ->andWhere('c.f052fisProcessed = 0')
            ->orderBy('c.f052fid','DESC');

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getProcessedList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fid, c.f052fcashAdvance,  c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,  c.f052fidFinancialYear, f.f015fname as financialYear,  c.f052fstatus, c.f052ftotal, c.f052fisProcessed, c.f052fapprovalStatus')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid')
            ->where('c.f052fapprovalStatus =1')
            ->andWhere('c.f052fisProcessed = 1')
            ->orderBy('c.f052fid','DESC');
            

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function cashApplicationProcess($cashes) 
    {
        $em = $this->getEntityManager();

        foreach ($cashes as $cash) 
        {

            $cash->setF052fisProcessed((int)"1")
                 ->setF052fUpdatedDtTm(new \DateTime());
       
            $em->persist($cash);
            $em->flush();
        }
        
    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getStaffCashAdvances($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, s.f034fname as staffName, c.f052fidDepartment,c.f052fidFinancialYear, f.f015fname as financialYear, c.f052fstatus, c.f052fapprovalStatus, c.f052ftotal')
            ->from('Application\Entity\T052fcashApplication','c')
            ->leftjoin('Application\Entity\T034fstaffMaster', 's', 'with', 'c.f052fidStaff = s.f034fstaffId')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'c.f052fidFinancialYear = f.f015fid');
            // ->where("c.f052fidStaff = '".$i."'");
            // ->setParameter('staffId',$id);
            

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function rejectReason($applications,$reason)
    {
        $em = $this->getEntityManager();
        // $qb = $em->createQueryBuilder();
        
        foreach ($applications as $application) 
        {

            // $id = $application->getF052fid();
           

            $application->setF052fapprovalStatus((int)"2")
                 ->setF052freason($reason);
       
            $em->persist($application);
            $em->flush();
            
            // $query1 = "UPDATE t052fcash_application set f052fapproval_status='$',f052freason='$reason' where f052fid = '$id'"; 
            // $result=$em->getConnection()->executeQuery($query1);

            
        }

        return $result;
    }

    public function staffConditionCheck($idStaff, $cashId) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('cl.f119fid, cl.f119fstaffId, cl.f119fcashAdvance, cl.f119famount, cl.f119fstatus')
            ->from('Application\Entity\T119fstaffCashAdvanceLimit','cl')
            ->where('cl.f119fcashAdvance = :cashId')
            ->andWhere('cl.f119fstaffId = :staffId')
            ->setParameter('staffId',(int)$idStaff)
            ->setParameter('cashId',$cashId);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function deleteCashApplicationDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query = "DELETE from t052fcash_application_details WHERE f052fid_details = $id";
            $res = $em->getConnection()->executeQuery($query);
        }
        return $res;
    }

}

?>
