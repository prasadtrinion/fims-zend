<?php
namespace Application\Repository;

use Application\Entity\T077frecurringSetUp;
use Application\Entity\T077frecurringSetupDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class RecurringSetUpRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select("r.f077fid, r.f077fcustomer as f077fidCustomer, r.f077fcontractNumber, r.f077freferenceNumber, r.f077fdateInvoice, r.f077faddress, r.f077fendDate, r.f077fdiscountEntitlement, r.f077fperiod, r.f077feffectiveDate, r.f077fpayment, r.f077fpremise, r.f077fcategory, r.f077fstatus, r.f077fdebitorType, c.f021ffirstName as firstName, c.f021flastName as lastName, p.f076fname as premiseName, r.f077fidPremiseCategory, r.f077fpaymentMethod, r.f077fidPremiseCategory, r.f077fpaymentMethod, (pts.f037fcode+'-'+pts.f037fprefixCode) as premiseTypeSetup")
            ->from('Application\Entity\T077frecurringSetUp', 'r')
            ->leftjoin('Application\Entity\T021fcustomers','c','with', 'r.f077fcustomer = c.f021fid')
            ->leftjoin('Application\Entity\T076fpremise', 'p', 'with', 'r.f077fpremise = p.f076fid')
            // ->leftjoin('Application\Entity\T076fpremise','p','with', 'r.f077fpremise = p.f076fid')
            // ->leftjoin('Application\Entity\T073fcategory','ca','with', 'r.f077fcategory = ca.f073fid')
            ->leftjoin('Application\Entity\T037fpremiseTypeSetup','pts','with', 'r.f077fidPremiseCategory = pts.f037fid')
            ->orderBy('r.f077fid','DESC');
            
        $query = $qb->getQuery();
        $result =$query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f077fendDate'];
            $item['f077fendDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f077feffectiveDate'];
            $item['f077feffectiveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function getListById($id) 
    {
        // print_r($id);exit;

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("r.f077fid, r.f077fcustomer as f077fidCustomer, r.f077fcontractNumber, r.f077freferenceNumber, r.f077fdateInvoice, r.f077faddress, r.f077fendDate, r.f077fdiscountEntitlement, r.f077fperiod,r.f077feffectiveDate,r.f077fpayment,r.f077fpremise,r.f077fcategory,r.f077fstatus, r.f077fdebitorType,c.f021ffirstName as firstName,c.f021flastName as lastName,p.f076fname as premiseName,     rd.f077ffeeSetup, rd.f077fcrFund, rd.f077fcrActivity, rd.f077fcrDepartment, rd.f077fcrAccount, rd.f077fdrFund, rd.f077fdrActivity, rd.f077fdrDepartment, rd.f077fdrAccount, rd.f077famount, rd.f077fidDetails, rd.f077fgstCode, rd.f077fgstPercentage, rd.f077ftaxCode, r.f077fidPremiseCategory, r.f077fpaymentMethod, (pts.f037fcode+'-'+pts.f037fprefixCode) as premiseTypeSetup")
            ->from('Application\Entity\T077frecurringSetUp', 'r')
            ->leftjoin('Application\Entity\T077frecurringSetupDetails','rd','with', 'r.f077fid = rd.f077fidRecurringSetup')
            ->leftjoin('Application\Entity\T021fcustomers','c','with', 'r.f077fcustomer = c.f021fid')
            ->leftjoin('Application\Entity\T076fpremise', 'p', 'with', 'r.f077fpremise = p.f076fid')
            // ->leftjoin('Application\Entity\T076fpremise','p','with', 'r.f077fpremise = p.f076fid')
            // ->leftjoin('Application\Entity\T073fcategory','ca','with', 'r.f077fcategory = ca.f073fid') 
            ->leftjoin('Application\Entity\T037fpremiseTypeSetup','pts','with', 'r.f077fidPremiseCategory = pts.f037fid')
            ->where('r.f077fid = :recurringId')
            ->setParameter('recurringId',(int)$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f077fendDate'];
            $item['f077fendDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f077feffectiveDate'];
            $item['f077feffectiveDate'] = date("Y-m-d", strtotime($date));
            $debtorType = $item['f077fdebitorType'];
            $customer = (int)$item['f077fcustomer'];

            switch ($debtorType)
            {
            case 'Staff':
                $query  = "select f034fstaff_id as Id, f034fname as userName from vw_staff_master where f034fstaff_id = $customer";
                $resultUser = $em->getConnection()->executeQuery($query)->fetch();
                break;

            case 'Others':
               $query  = "select f021ffirst_name as userName, f021flast_name as Id from t021fcustomers where f021fid = $customer";
                $resultUser = $em->getConnection()->executeQuery($query)->fetch();
                break;

            case 'Student':
               $query  = "select f060fname as userName, f060fic_number as Id from t060fstudent where f060fid = $customer";
                $resultUser = $em->getConnection()->executeQuery($query)->fetch();
                break;
                
            }

            $item['userName'] = $resultUser['userName'];
            $item['Id'] = $resultUser['Id'];

            array_push($result1, $item);
            
        }
        // print_r($result);exit;


        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="RECSETUP";
        $referenceNumber = $configRepository->generateFIMS($numberData);
        
        $recurring = new T077frecurringSetUp();

        $recurring->setF077fcustomer((int)$data['f077fidCustomer'])
                ->setF077fcontractNumber($data['f077fcontractNumber'])
                ->setF077freferenceNumber($referenceNumber)
                ->setF077fdateInvoice($data['f077fdateInvoice'])
                ->setF077fdebitorType($data['f077fdebitorType'])
                ->setF077faddress($data['f077faddress'])
                ->setF077fdiscountEntitlement($data['f077fdiscountEntitlement'])
                ->setF077fperiod((int)$data['f077fperiod'])
                ->setF077fendDate(new \DateTime($data['f077fendDate']))
                ->setF077feffectiveDate(new \DateTime($data['f077feffectiveDate']))
                ->setF077fpayment((float)$data['f077fpayment'])
                ->setF077fpremise((int)$data['f077fpremise'])
                ->setF077fcategory((int)$data['f077fcategory'])
                ->setF077fstatus((int)$data['f077fstatus'])
                ->setF077fidPremiseCategory((int)$data['f077fidPremiseCategory'])
                ->setF077fpaymentMethod((int)$data['f077fpaymentMethod'])
                ->setF077fcreatedBy((int)$_SESSION['userId'])
                ->setF077fupdatedBy((int)$_SESSION['userId']);

        $recurring->setF077fcreatedDtTm(new \DateTime())
                ->setF077fupdatedDtTm(new \DateTime());


        try{
            $em->persist($recurring);
            $em->flush();

        }
        catch(\Exception $e){
            echo $e;
        }
        $recurringDetails = $data['recurring-details'];

        foreach ($recurringDetails as $recurringDetail) {
        try{

                $recurringDetailObj = new T077frecurringSetupDetails();
                $recurringDetailObj->setF077fidRecurringSetup((int)$recurring->getF077fid())
                                ->setF077ffeeSetup((int)$recurringDetail['f077ffeeSetup'])
                                ->setF077fgstCode($recurringDetail['f077fgstCode'])
                                ->setF077fgstPercentage((float)$recurringDetail['f077fgstPercentage'])
                                ->setF077fcrFund($recurringDetail['f077fcrFund'])
                                ->setF077fcrActivity($recurringDetail['f077fcrActivity'])
                                ->setF077fcrDepartment($recurringDetail['f077fcrDepartment'])
                                ->setF077fcrAccount($recurringDetail['f077fcrAccount'])
                                ->setF077fdrFund($recurringDetail['f077fdrFund'])
                                ->setF077fdrActivity($recurringDetail['f077fdrActivity'])
                                ->setF077fdrDepartment($recurringDetail['f077fdrDepartment'])
                                ->setF077fdrAccount($recurringDetail['f077fdrAccount'])
                                ->setF077ftaxCode((int)$recurringDetail['f077ftaxCode'])
                                ->setF077famount((float)$recurringDetail['f077famount'])
                                ->setF077fstatus((int)$recurringDetail['f077fstatus'])
                                ->setF077fcreatedBy((int)$_SESSION['userId'])
                                ->setF077fupdatedBy((int)$_SESSION['userId']);
 
                $recurringDetailObj->setF077fcreateDtTm(new \DateTime())
                                 ->setF077fupdateDtTm(new \DateTime());
                 try{
                $em->persist($recurringDetailObj);
                $em->flush();
                // print_r($recurringDetailObj);exit;
                 }
                 catch(\Exception $e){
                     echo $e;
                 } 
        }
        catch(\Exception $e){
            echo $e;
        }
    }
    return $recurring;

}

    public function updateData($recurring, $data = []) 
    {
        $em = $this->getEntityManager();

        $recurring->setF077fcustomer((int)$data['f077fidCustomer'])
                ->setF077fcontractNumber($data['f077fcontractNumber'])
                ->setF077freferenceNumber($data['f077freferenceNumber'])
                ->setF077fdateInvoice($data['f077fdateInvoice'])
                ->setF077fdebitorType($data['f077fdebitorType'])
                ->setF077faddress($data['f077faddress'])
                ->setF077fdiscountEntitlement($data['f077fdiscountEntitlement'])
                ->setF077fperiod((int)$data['f077fperiod'])
                ->setF077fendDate(new \DateTime($data['f077fendDate']))
                ->setF077feffectiveDate(new \DateTime($data['f077feffectiveDate']))
                ->setF077fpayment((float)$data['f077fpayment'])
                ->setF077fpremise((int)$data['f077fpremise'])
                ->setF077fcategory((int)$data['f077fcategory'])
                ->setF077fidPremiseCategory((int)$data['f077fidPremiseCategory'])
                ->setF077fpaymentMethod((int)$data['f077fpaymentMethod'])
                ->setF077fstatus((int)$data['f077fstatus'])
                ->setF077fupdatedBy((int)$_SESSION['userId']);


        $recurring->setF077fupdatedDtTm(new \DateTime());


        
        $em->persist($recurring);
        $em->flush();

        return $recurring;

    }
    public function updateRecurringDetail($recurringDetailObj, $recurringDetail)
    {
        
        $em = $this->getEntityManager();

        $recurringDetailObj->setF077ffeeSetup((int)$recurringDetail['f077ffeeSetup'])
                                ->setF077fgstCode($recurringDetail['f077fgstCode'])
                                ->setF077fgstPercentage((float)$recurringDetail['f077fgstPercentage'])
                                ->setF077fcrFund($recurringDetail['f077fcrFund'])
                                ->setF077fcrActivity($recurringDetail['f077fcrActivity'])
                                ->setF077fcrDepartment($recurringDetail['f077fcrDepartment'])
                                ->setF077fcrAccount($recurringDetail['f077fcrAccount'])
                                ->setF077ftaxCode($recurringDetail['f077ftaxCode'])
                                ->setF077fdrFund($recurringDetail['f077fdrFund'])
                                ->setF077fdrActivity($recurringDetail['f077fdrActivity'])
                                ->setF077fdrDepartment($recurringDetail['f077fdrDepartment'])
                                ->setF077fdrAccount($recurringDetail['f077fdrAccount'])
                                ->setF077famount((float)$recurringDetail['f077famount'])
                                ->setF077fstatus((int)$recurringDetail['f077fstatus'])
                                ->setF077fupdatedBy((int)$_SESSION['userId']);
 
        $recurringDetailObj->setF077fupdateDtTm(new \DateTime());

        $em->persist($recurringDetailObj);

        $em->flush();

            
        
    }
    
    public function createRecurringDetail($recurringObj, $recurringDetail)
    {
        $em = $this->getEntityManager();

        $recurringDetailObj = new T077frecurringSetupDetails();
       

        $recurringDetailObj->setF077fidRecurringSetup($recurringObj->getF077fid())
                                ->setF077ffeeSetup((int)$recurringDetail['f077ffeeSetup'])
                                ->setF077fgstCode($recurringDetail['f077fgstCode'])
                                ->setF077fgstPercentage((float)$recurringDetail['f077fgstPercentage'])
                                ->setF077fcrFund($recurringDetail['f077fcrFund'])
                                ->setF077fcrActivity($recurringDetail['f077fcrActivity'])
                                ->setF077fcrDepartment($recurringDetail['f077fcrDepartment'])
                                ->setF077fcrAccount($recurringDetail['f077fcrAccount'])
                                ->setF077ftaxCode($recurringDetail['f077ftaxCode'])
                                ->setF077fdrFund($recurringDetail['f077fdrFund'])
                                ->setF077fdrActivity($recurringDetail['f077fdrActivity'])
                                ->setF077fdrDepartment($recurringDetail['f077fdrDepartment'])
                                ->setF077fdrAccount($recurringDetail['f077fdrAccount'])
                                ->setF077famount((float)$recurringDetail['f077famount'])
                                ->setF077fstatus((int)$recurringDetail['f077fstatus'])
                                ->setF077fcreatedBy((int)$_SESSION['userId'])
                                ->setF077fupdatedBy((int)$_SESSION['userId']);
 
                $recurringDetailObj->setF077fcreateDtTm(new \DateTime())
                                 ->setF077fupdateDtTm(new \DateTime());

            try
            {
                $em->persist($recurringDetailObj);
            
                $em->flush();
            }
            catch(\Exception $e)
            {
                echo $e;
                 
            } 
        
    

    return $recurringDetailObj;
        
    }

    public function deleteRecurringSetupDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t077frecurring_setup_details WHERE f077fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
}