<?php
namespace Application\Repository;

use Application\Entity\T078futilityBill;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class UtilityBillRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("u.f078finitialReading, u.f078flastReading, u.f078fdescription, u.f078fidNumber, ur.f078fid, ur.f078fidUtility, ur.f078ffromDate, ur.f078ftoDate, ur.f078freadingDate, ur.f078funits, ur.f078fstatus, ur.f078fcalculatedAmount, ur.f078fidPremise, ur.f078fcurrentReadingValue, ur.f078ftotalUsage, ur.f078famount, ur.f078fgeneratedBill, (p.f076fname+'-'+r.f085fname+'-' +u.f078fdescription) as meterId, (p.f076fname+'-'+p.f076fpremiseCode) as premiseName")
            ->from('Application\Entity\T078futilityBill', 'ur')
            ->leftjoin('Application\Entity\T078futility','u','with', 'u.f078fid=ur.f078fidUtility')
            ->leftJoin('Application\Entity\T076fpremise', 'p', Expr\Join::WITH,'ur.f078fidPremise = p.f076fid')
            ->leftjoin('Application\Entity\T085frevenueSetUp','r','with', 'u.f078fidRevenue = r.f085fid')
            ->orderBy('ur.f078fid','DESC');





        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078ffromDate'];
            $utility['f078ffromDate'] = date("Y-m-d", strtotime($date));
            $date                        = $utility['f078ftoDate'];
            $utility['f078ftoDate'] = date("Y-m-d", strtotime($date));
            $date                        = $utility['f078freadingDate'];
            $utility['f078ftoDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $utility);
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('u.f078finitialReading, u.f078flastReading, u.f078fdescription, u.f078fidNumber, ur.f078fid, ur.f078fidUtility, ur.f078ffromDate, ur.f078ftoDate, ur.f078freadingDate, ur.f078funits, ur.f078fstatus, ur.f078fcalculatedAmount, ur.f078fidPremise, p.f076fname as premiseName, ur.f078fcurrentReadingValue, ur.f078ftotalUsage, ur.f078famount, ur.f078fgeneratedBill')

            ->from('Application\Entity\T078futilityBill', 'ur')
            ->leftjoin('Application\Entity\T078futility','u','with', 'u.f078fid=ur.f078fidUtility')
            ->leftjoin('Application\Entity\T076fpremise','p','with', 'ur.f078fidPremise =p.f076fid')

            ->where('ur.f078fid = :utilityId')
            ->setParameter('utilityId',$id);
            
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $utility) {
            $date                        = $utility['f078ffromDate'];
            $utility['f078ffromDate'] = date("Y-m-d", strtotime($date));
            $date                        = $utility['f078ftoDate'];
            $utility['f078ftoDate'] = date("Y-m-d", strtotime($date));
            $date                        = $utility['f078freadingDate'];
            $utility['f078ftoDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $utility);
        }

        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $utility = new T078futilityBill();

        $utility->setF078fidUtility((int)$data['f078fidUtility'])
                ->setF078fidPremise((int)$data['f078fidPremise'])
                ->setF078ffromDate(new \DateTime($data['f078ffromDate']))
                ->setF078ftoDate(new \DateTime($data['f078ftoDate']))
                ->setF078freadingDate(new \DateTime($data['f078freadingDate']))
                ->setF078funits((int)$data['f078funits'])
                ->setF078fgeneratedBill((int)$data['f078fgeneratedBill'])
                ->setF078fcalculatedAmount((int)$data['f078fcalculatedAmount'])
                ->setF078fcurrentReadingValue($data['f078fcurrentReadingValue'])
                ->setF078ftotalUsage((int)$data['f078ftotalUsage'])
                ->setF078famount((int)$data['f078famount'])
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fcreatedBy((int)$_SESSION['userId'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fcreatedDtTm(new \DateTime())
                ->setF078fupdatedDtTm(new \DateTime());


    
        $em->persist($utility);
        $em->flush();
        

        $current = $data['f078fcurrentReadingValue'];
        $id = $data['f078fidUtility'];
        $date = $data['f078ftoDate'];
        // $lastDate= $date[];



        $query1 = "UPDATE t078futility set f078flast_reading= $current, f078fdate='$date' where f078fid=$id"; 
        $em->getConnection()->executeQuery($query1);

        return $utility;


    }

    public function updateData($utility, $data = []) 
    {
        $em = $this->getEntityManager();

        $utility->setF078fidUtility((int)$data['f078fidUtility'])
                ->setF078fidPremise((int)$data['f078fidPremise'])
                ->setF078ffromDate(new \DateTime($data['f078ffromDate']))
                ->setF078ftoDate(new \DateTime($data['f078ftoDate']))
                ->setF078freadingDate(new \DateTime($data['f078freadingDate']))
                ->setF078fgeneratedBill($data['f078fgeneratedBill'])
                ->setF078fcalculatedAmount((float)$data['f078fcalculatedAmount'])
                ->setF078funits((int)$data['f078funits'])
                ->setF078fstatus((int)$data['f078fstatus'])
                ->setF078fcurrentReadingValue((int)$data['f078fcurrentReadingValue'])
                ->setF078ftotalUsage((int)$data['f078ftotalUsage'])
                ->setF078famount((float)$data['f078famount'])
                ->setF078fupdatedBy((int)$_SESSION['userId']);

        $utility->setF078fupdatedDtTm(new \DateTime());


        
        $em->persist($utility);
        $em->flush();

        $current = $data['f078fcurrentReadingValue'];
        $id = $data['f078fidUtility'];
        $lastDate = new \DateTime($data['f078ftoDate']);
        // print_r($lastDate);
        // exit();
        
        $query1 = "update t078futility set f078flast_reading=$current, f078fdate=$lastDate where f078fid=$id"; 
        $em->getConnection()->executeQuery($query1);

    }

}
