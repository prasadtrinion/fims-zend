<?php
namespace Application\Repository;

use Application\Entity\T022fclaimCustomers;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ClaimCustomerRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f022fid, c.f022ffirstName, c.f022flastName, c.f022femailId, c.f022fpassword, c.f022faddress1, c.f022faddress2, c.f022fcountry, c.f022fcity, c.f022fstate, c.f022fzipCode, c.f022fcontact,c.f022fsupplier, c.f022fidSupplier, c.f022ftaxFree, c.f022fidTaxFree, c.f022fstatus,c.f022fcustomerDebit, c.f022fcustomerCrebit, c.f022fcustomerBalance, co.f013fcountryName, s.f012fstateName')
            ->from('Application\Entity\T022fclaimCustomers','c')
            ->leftjoin('Application\Entity\T013fcountry','co','with','c.f022fcountry = co.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with','c.f022fstate = s.f012fid')
            ->orderBy('c.f022fid', 'DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f022fid, c.f022ffirstName, c.f022flastName, c.f022femailId, c.f022fpassword, c.f022faddress1, c.f022faddress2, c.f022fcountry, c.f022fcity, c.f022fstate, c.f022fzipCode, c.f022fcontact, c.f022fsupplier, c.f022fidSupplier, c.f022ftaxFree, c.f022fidTaxFree, c.f022fstatus, c.f022fcustomerDebit, c.f022fcustomerCrebit, c.f022fcustomerBalance, co.f013fcountryName, s.f012fstateName')
            ->from('Application\Entity\T022fclaimCustomers','c')
            ->leftjoin('Application\Entity\T013fcountry','co','with','c.f022fcountry = co.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with','c.f022fstate = s.f012fid')
            ->where('c.f022fid = :customerId')
            ->setParameter('customerId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult();

        return $result;
    }

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $customer = new T022fclaimCustomers();

        $customer->setF022ffirstName($data['f022ffirstName'])
             ->setF022flastName($data['f022flastName'])
             ->setF022femailId($data['f022femailId'])
             ->setF022fpassword($data['f022fpassword'])
             ->setF022faddress1($data['f022faddress1'])
             ->setF022faddress2($data['f022faddress2'])
             ->setF022fcountry((int)$data['f022fcountry'])
             ->setF022fcity($data['f022fcity'])
             ->setF022fstate((int)$data['f022fstate'])
             ->setF022fcustomerDebit((int)0)
             ->setF022fcustomerCrebit((int)0)
             ->setF022fcustomerBalance((int)0)
             ->setF022fzipCode($data['f022fzipCode'])
             ->setF022fcontact($data['f022fcontact'])
             ->setF022fsupplier((int)$data['f022fsupplier'])
             ->setF022fidSupplier((int)$data['f022fidSupplier'])
             ->setF022ftaxFree((int)$data['f022ftaxFree'])
             ->setF022fidTaxFree((int)$data['f022fidTaxFree'])
             ->setF022fstatus((int)$data['f022fstatus'])
             ->setF022fcreatedBy((int)$_SESSION['userId'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $customer->setF022fcreatedDtTm(new \DateTime())
                ->setF022fupdatedDtTm(new \DateTime());

    try{
        $em->persist($customer);
        // print_r($customer);
        // die();
        $em->flush();
    }
    catch(\Exception $e){
        echo $e;
        return $customer;
    }

    }

    /* to edit the data in database*/

    public function updateCustomer($customer, $data = []) 
    {
        $em = $this->getEntityManager();

        $customer->setF022ffirstName($data['f022ffirstName'])
             ->setF022flastName($data['f022flastName'])
             ->setF022femailId($data['f022femailId'])
             ->setF022fpassword($data['f022fpassword'])
             ->setF022faddress1($data['f022faddress1'])
             ->setF022faddress2($data['f022faddress2'])
             ->setF022fcountry((int)$data['f022fcountry'])
             ->setF022fcity($data['f022fcity'])
             ->setF022fstate((int)$data['f022fstate'])
             ->setF022fcustomerDebit((int)0)
             ->setF022fcustomerCrebit((int)0)
             ->setF022fcustomerBalance((int)0)
             ->setF022fzipCode($data['f022fzipCode'])
             ->setF022fcontact($data['f022fcontact'])
             ->setF022fsupplier((int)$data['f022fsupplier'])
             ->setF022fidSupplier((int)$data['f022fidSupplier'])
             ->setF022ftaxFree((int)$data['f022ftaxFree'])
             ->setF022fidTaxFree((int)$data['f022fidTaxFree'])
             ->setF022fstatus((int)$data['f022fstatus'])
             ->setF022fcreatedBy((int)$_SESSION['userId'])
             ->setF022fupdatedBy((int)$_SESSION['userId']);

        $customer->setF022fupdatedDtTm(new \DateTime());
        
        $em->persist($customer);
        $em->flush();

    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('c.f022fid, c.f022ffirstName, c.f022flastName, c.f022femailId, c.f022fpassword, c.f022faddress1, c.f022faddress2, c.f022fcountry, c.f022fcity, c.f022fstate, c.f022fzipCode, c.f022fcontact,c.f022fsupplier, c.f022fidSupplier, c.f022ftaxFree, c.f022fidTaxFree, c.f022fstatus,c.f022fcustomerDebit, c.f022fcustomerCrebit, c.f022fcustomerBalance, co.f013fcountryName, s.f012fstateName')
            ->from('Application\Entity\T022fclaimCustomers','c')
            ->leftjoin('Application\Entity\T013fcountry','co','with','c.f022fcountry = co.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with','c.f022fstate = s.f012fid')
            ->where('c.f022fstatus');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
    
    
}

?>