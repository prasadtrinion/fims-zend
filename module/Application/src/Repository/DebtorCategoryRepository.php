<?php
namespace Application\Repository;

use Application\Entity\T130fdebtorCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class DebtorCategoryRepository extends EntityRepository 
{

    public function getList() 
    {
        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("dp.f130fid, dp.f130fdebtorCategoryName, dp.f130fstatus")
            ->from('Application\Entity\T130fdebtorCategory', 'dp')
            ->orderBy('dp.f130fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function getListById($id) 
    {
        // print_r($id);exit;
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select("dp.f130fid, dp.f130fdebtorCategoryName, dp.f130fstatus")
            ->from('Application\Entity\T130fdebtorCategory', 'dp')
            ->where('dp.f130fid = :blockId')
            ->setParameter('blockId',$id);
             
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }

    public function createNewData($data) 
    {
       
     	$em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $debtorCategory = new T130fdebtorCategory();

        $debtorCategory->setF130fdebtorCategoryName($data['f130fdebtorCategoryName'])
        ->setF130fstatus((int)$data['f130fstatus'])
        ->setF130fcreatedBy((int)$_SESSION['userId']);


        $debtorCategory->setF130fcreatedDtTm(new \DateTime());
     
            $em->persist($debtorCategory);
            $em->flush();


        	return $debtorCategory;
    }

    public function updateData($debtorCategory, $data = []) 
    {
        $em = $this->getEntityManager();

        $debtorCategory->setF130fdebtorCategoryName($data['f130fdebtorCategoryName'])
        ->setF130fstatus((int)$data['f130fstatus'])
        ->setF130fupdatedBy((int)$_SESSION['userId']);

        $debtorCategory->setF130fupdatedDtTm(new \DateTime());

        $em->persist($debtorCategory);
        $em->flush();
    }

    public function getDebtorCategoryActive($id)
    {

        $em = $this->getEntityManager(); 
        $qb = $em->createQueryBuilder();
        $qb->select("dp.f130fid, dp.f130fdebtorCategoryName, dp.f130fstatus")
            ->from('Application\Entity\T130fdebtorCategory', 'dp')
            ->where('dp.f130fstatus = :status')
            ->setParameter('status',(int)$id)
            ->orderBy('dp.f130fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }
}
