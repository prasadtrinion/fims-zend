<?php
namespace Application\Repository;

use Application\Entity\T021fcustomers;
use Application\Entity\T022fcustomerHasPayment;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class CustomerRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $query = "select top 50 c.f021fid, c.f021ffirst_name as f021ffirstName, c.f021flast_name as f021flastName, c.f021femail_id as f021femailId, c.f021fpassword, c.f021faddress1, c.f021faddress2, c.f021fcountry, c.f021fcity, c.f021fstate, c.f021fzip_code as f021fzipCode, c.f021fcontact, c.f021fsupplier, c.f021fid_supplier as f021fidSupplier, c.f021ftax_free as f021ftaxFree, c.f021fid_tax_free as f021fidTaxFree, c.f021fstatus, c.f021fcustomer_debit as f021fcustomerDebit, c.f021fcustomer_crebit as f021fcustomerCrebit, c.f021fcustomer_balance as f021fcustomerBalance, c.f021fcontact_person as f021fcontactPerson, c.f021fdebtor_code as f021fdebtorCode, c.f021fbank, c.f021fbranch, c.f021fdebtor_category as f021fdebtorCategory, c.f021faccount_number as f021faccountNumber, (c.f021ffirst_name+' ' +c.f021flast_name) as Debtor from t021fcustomers as c order by c.f021fid desc";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        // Query
        // $qb->select("c.f021fid, c.f021ffirstName, c.f021flastName, c.f021femailId, c.f021fpassword, c.f021faddress1, c.f021faddress2, c.f021fcountry, c.f021fcity, c.f021fstate, c.f021fzipCode, c.f021fcontact, c.f021fsupplier, c.f021fidSupplier, c.f021ftaxFree, c.f021fidTaxFree, c.f021fstatus, c.f021fcustomerDebit, c.f021fcustomerCrebit, c.f021fcustomerBalance, c.f021fcontactPerson, c.f021fdebtorCode, c.f021fbank, c.f021fbranch, c.f021faccountNumber, (c.f021ffirstName+' ' +c.f021flastName) as Debtor")
        //     ->from('Application\Entity\T021fcustomers','c')
        //     // ->leftjoin('Application\Entity\T042fbank','b','with','c.f021fbank = b.f042fid')
        //     ->orderBy('c.f021fid','DESC');

        // $query = $qb->getQuery();

        $result = array(
            'data' => $result,
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f021fid, c.f021ffirstName, c.f021flastName, c.f021femailId, c.f021fpassword, c.f021faddress1,c.f021faddress2,c.f021fcountry, c.f021fcity, c.f021fstate, c.f021fzipCode, c.f021fcontact, c.f021fsupplier, c.f021fidSupplier, c.f021ftaxFree, c.f021fidTaxFree, c.f021fstatus,c.f021fcustomerDebit,c.f021fcustomerCrebit,c.f021fcustomerBalance, c.f021fcontactPerson, c.f021fdebtorCode, c.f021fbank, c.f021fbranch, c.f021faccountNumber, co.f013fcountryName,s.f012fstateName, c.f021fdebtorCategory')
            ->from('Application\Entity\T021fcustomers','c')
            ->leftjoin('Application\Entity\T013fcountry','co','with','c.f021fcountry = co.f013fid')
            ->leftjoin('Application\Entity\T012fstate','s','with','c.f021fstate = s.f012fid')
            ->where('c.f021fid = :customerId')
            ->setParameter('customerId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

    public function getPaymentData($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
            
        $qb->select('cp.f022fid,cp.f022famount,cp.f022ftype,cp.f022fdateOfPayment,cp.f022fstatus')
            ->from('Application\Entity\T022fcustomerHasPayment','cp')
            ->where('cp.f022fidCustomer = :customerId')
            ->setParameter('customerId',(int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function getCustomerDetails($email,$password) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select('c.f021fid, c.f021ffirstName, c.f021flastName, c.f021femailId, c.f021fpassword, c.f021faddress, c.f021fcity, c.f021fstate, c.f021fzipCode,    c.f021fcontact, c.f021fsupplier, c.f021fidSupplier, c.f021ftaxFree, c.f021fidTaxFree, c.f021fstatus,c.f021fcustomerDebit,c.f021fcustomerCrebit,c.f021fcustomerBalance, c.f021fdebtorCategory')
            ->from('Application\Entity\T021fcustomers','c')
            ->where('c.f021femailId = :emailId')
            ->setParameter('emailId',$email)
            ->andWhere('c.f021fpassword = :password')
            ->setParameter('password',$password);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result; 
    }



   

    public function createNewData($data) 
    {
        // print_r($data);exit;
        $em = $this->getEntityManager();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="Customer";
        $number = $configRepository->generateFIMS($numberData);

        $vendorRepository = $em->getRepository("Application\Entity\T030fvendorRegistration");

        $customer = new T021fcustomers();
 
        $customer->setF021ffirstName($data['f021ffirstName'])
             ->setF021flastName($data['f021flastName'])
             ->setF021femailId($data['f021femailId'])
             ->setF021fpassword($data['f021fpassword'])
             ->setF021faddress1($data['f021faddress1'])
             ->setF021faddress2($data['f021faddress2'])
             ->setF021fcountry((int)$data['f021fcountry'])
             ->setF021fcity($data['f021fcity'])
             ->setF021fstate((int)$data['f021fstate'])
             ->setF021fcustomerDebit((int)0)
             ->setF021fcustomerCrebit((int)0)
             ->setF021fcustomerBalance((int)0)
             ->setF021fzipCode($data['f021fzipCode'])
             ->setF021fcontact((int)$data['f021fcontact'])
             ->setF021fsupplier((int)$data['f021fsupplier'])
             ->setF021fidSupplier((int)$data['f021fidSupplier'])
             ->setF021ftaxFree($data['f021ftaxFree'])
             ->setF021fidTaxFree((int)$data['f021fidTaxFree'])
             ->setF021fstatus((int)$data['f021fstatus'])
             ->setF021faccountNumber($data['f021faccountNumber'])
             ->setF021fbranch($data['f021fbranch'])
             ->setF021fbank((int)$data['f021fbank'])
             ->setF021fdebtorCode($number)
             ->setF021fdebtorCategory((int)$data['f021fdebtorCategory'])
             ->setF021fcontactPerson($data['f021fcontactPerson'])
             ->setF021fcreatedBy((int)$_SESSION['userId']);

        $customer->setF021fcreatedDtTm(new \DateTime());

    try{
        $em->persist($customer);
        

        $username = $data['f021femailId'];
        $password = $data['temp_password'];
        $message = "<p>Your Registration has been successfull. <br> Username: $username <br> Password: $password</p>";
        $subject = "Debtor Registration";
        $vendorRepository->sendMail1($username,$message,$subject);
        $em->flush();
        // print_r($customer);exit;
    }
    catch(\Exception $e){
        echo $e;
        
    }
    return $customer;
 
    }

    /* to edit the data in database*/

    public function updateCustomer($customer, $data = []) 
    {
        $em = $this->getEntityManager();

        $customer->setF021ffirstName($data['f021ffirstName'])
             ->setF021flastName($data['f021flastName'])
             ->setF021femailId($data['f021femailId'])
             // ->setF021fpassword($data['f021fpassword'])
             ->setF021faddress1($data['f021faddress1'])
             ->setF021faddress2($data['f021faddress2'])
             ->setF021fcountry((int)$data['f021fcountry'])
             ->setF021fcity($data['f021fcity'])
             ->setF021fstate((int)$data['f021fstate'])
             ->setF021fcustomerDebit((int)0)
             ->setF021fcustomerCrebit((int)0)
             ->setF021fcustomerBalance((int)0)
             ->setF021fzipCode($data['f021fzipCode'])
             ->setF021fcontact((int)$data['f021fcontact'])
             ->setF021fsupplier((int)$data['f021fsupplier'])
             ->setF021fidSupplier((int)$data['f021fidSupplier'])
             ->setF021ftaxFree((int)$data['f021ftaxFree'])
             ->setF021fidTaxFree((int)$data['f021fidTaxFree'])
             ->setF021fstatus((int)$data['f021fstatus'])
             ->setF021faccountNumber($data['f021faccountNumber'])
             ->setF021fbranch($data['f021fbranch'])
             ->setF021fbank((int)$data['f021fbank'])
             ->setF021fdebtorCategory((int)$data['f021fdebtorCategory'])
             ->setF021fdebtorCode($data['f021fdebtorCode'])
             ->setF021fcontactPerson($data['f021fcontactPerson'])
             ->setF021fupdatedBy((int)$_SESSION['userId']);
                

        $customer->setF021fupdatedDtTm(new \DateTime());
        
        $em->persist($customer);
        $em->flush();

    }


    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    public function createNewPayment($customer,$data) 
    {
        $creditAmount = $customer->getF021fcustomerCrebit();
        $debitAmount = $customer->getF021fcustomerDebit();
        $balanceAmount = $customer->getF021fcustomerBalance();
        if($data['f022ftype'] == 'CR'){
            $creditAmount = $creditAmount + $data['f022famount'];
            $balanceAmount = $balanceAmount + $data['f022famount'];
            $customer->setF021fcustomerCrebit($creditAmount);
            $customer->setF021fcustomerBalance($balanceAmount);
        }
        else{
            $debitAmount = $debitAmount + $data['f022famount'];
            $balanceAmount = $balanceAmount - $data['f022famount'];
            $customer->setF021fcustomerDebit($debitAmount);
            $customer->setF021fcustomerBalance($balanceAmount);
        }
        $em = $this->getEntityManager();

        $customerPayment = new T022fcustomerHasPayment();

        $customerPayment->setF022fidCustomer((int)$data['f022fcustomer'])
                        ->setF022famount((float)$data['f022famount'])
                        ->setF022ftype($data['f022ftype'])
                        ->setF022fdateOfPayment(new \DateTime($data['f022fdateOfPayment']))
                        ->setF022fstatus((int)$data['f022fstatus'])
                        ->setF022fcreatedBy((int)$_SESSION['userId'])
                        ->setF022fupdatedBy((int)$_SESSION['userId']);

        $customerPayment->setF022fcreatedDtTm(new \DateTime())
                ->setF022fupdatedDtTm(new \DateTime());

        $em->persist($customer);
        $em->flush();

        $em->persist($customerPayment);
        $em->flush();

        return $customerPayment;

    } 

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $query = "select top 50 c.f021fid, c.f021ffirst_name as f021ffirstName, c.f021flast_name as f021flastName, c.f021femail_id as f021femailId, c.f021fpassword, c.f021faddress1, c.f021faddress2, c.f021fcountry, c.f021fcity, c.f021fstate, c.f021fzip_code as f021fzipCode, c.f021fcontact, c.f021fsupplier, c.f021fid_supplier as f021fidSupplier, c.f021ftax_free as f021ftaxFree, c.f021fid_tax_free as f021fidTaxFree, c.f021fstatus, c.f021fcustomer_debit as f021fcustomerDebit, c.f021fcustomer_crebit as f021fcustomerCrebit, c.f021fcustomer_balance as f021fcustomerBalance, c.f021fcontact_person as f021fcontactPerson, c.f021fdebtor_code as f021fdebtorCode, c.f021fbank, c.f021fbranch, c.f021fdebtor_category as f021fdebtorCategory, c.f021faccount_number as f021faccountNumber, (c.f021ffirst_name+' ' +c.f021flast_name) as Debtor from t021fcustomers as c where f021fstatus = 1 order by c.f021fid";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        $result = array(
            'data' => $result
        );

        return $result;
        
    }
}

?>