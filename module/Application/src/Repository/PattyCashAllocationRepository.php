<?php
namespace Application\Repository;

use Application\Entity\T122fpattyCashAllocation;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PattyCashAllocationRepository extends EntityRepository 
{
 
    public function getList()   
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();     

 
        $qb->select("p.f122fid, p.f122fapprover, p.f122fdepartmentCode, p.f122fallocationYear, p.f122fminAmount, p.f122fmaxAmount, p.f122fpaymentMode, p.f122fminReimbursement, p.f122fmaxReimbursement, p.f122fstatus, p.f122famount, p.f122freferenceNo, d.f015fdepartmentName, p.f122fminReimbursementPercentage, p.f122fmaxReimbursementPercentage, p.f122fidPettyCash, ( p.f122fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, dms.f011fdefinitionCode, fy.f015fname, sm.f034fname, u.f014fuserName")
            ->from('Application\Entity\T122fpattyCashAllocation', 'p')
             ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f122fdepartmentCode')
              ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f122fpaymentMode')
              ->leftjoin('Application\Entity\T015ffinancialyear', 'fy','with','fy.f015fid= p.f122fallocationYear')
              ->leftjoin('Application\Entity\T014fuser', 'u', 'with','p.f122fapprover = u.f014fid')
              ->leftjoin('Application\Entity\T034fstaffMaster', 'sm','with','sm.f034fstaffId = u.f014fidStaff')
             ->orderBy('p.f122fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $resultpatty = array();
        // print_r($result);
        // die();
        // foreach ($result as $results)
        // {
        //     $id = $results['f122fapprover'];
        //     // print_r($id);exit;
        //     $query = "select f034fname from t034fstaff_master where f034fstaff_id = '$id' ";
        //     // print_r($query);exit;

        //     $select = $em->getConnection()->executeQuery($query);
        //     $result1 = $select->fetch();
            
        //     $results['f034fname'] = $result1['f034fname'];
        //     // print_r($result1);exit;
        //      array_push($resultpatty, $results);
            
        // }   
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("p.f122fid, p.f122fapprover, p.f122fdepartmentCode, p.f122fallocationYear, p.f122fminAmount, p.f122fmaxAmount, p.f122fpaymentMode, p.f122fminReimbursement, p.f122fmaxReimbursement, p.f122fstatus, d.f015fdepartmentName, p.f122famount, p.f122freferenceNo, p.f122fminReimbursementPercentage, p.f122fmaxReimbursementPercentage, p.f122fidPettyCash, ( p.f122fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, dms.f011fdefinitionCode, fy.f015fname, sm.f034fname, u.f014fuserName")
            ->from('Application\Entity\T122fpattyCashAllocation', 'p')
             ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f122fdepartmentCode')
              ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f122fpaymentMode')
              ->leftjoin('Application\Entity\T015ffinancialyear', 'fy','with','fy.f015fid= p.f122fallocationYear')
              ->leftjoin('Application\Entity\T014fuser', 'u', 'with','p.f122fapprover = u.f014fid')
              ->leftjoin('Application\Entity\T034fstaffMaster', 'sm','with','sm.f034fstaffId = u.f014fidStaff')
            ->where('p.f122fid = :pattyCashId')
            ->setParameter('pattyCashId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // $result1 = array();
        // foreach ($result as $item) {
        //     $date                        = $item['f102feffectiveDate'];
        //     $item['f102feffectiveDate'] = date("Y-m-d", strtotime($date));
        //     array_push($result1, $item);
        // }
        //         foreach ($result as $results)
        // {
        //     $id = $results['f122fapprover'];
        //     $query = "select f034fname from t034fstaff_master where f034fstaff_id = '$id' ";
        //     // print_r($query);exit;

        //     $select = $em->getConnection()->executeQuery($query);
        //     $result1 = $select->fetch();
            
        //     $results['f034fname'] = $result1['f034fname'];
        //     // print_r($results);exit;
            
        // }
        // $id = $result[0]['f014fidStaff'];
        // print_r($id);exit;

        


        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();



        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']="PATTYCASHALLOCATION";
        $numberData['department']= $data['f122fdepartmentCode'];
        $number = $configRepository->generateFIMS($numberData);



        $qb = $em->createQueryBuilder();
     

        $pattyCash = new T122fpattyCashAllocation();

        $pattyCash->setF122fapprover((int)$data['f122fapprover'])
                ->setF122fdepartmentCode($data['f122fdepartmentCode'])
                ->setF122fallocationYear((int)$data['f122fallocationYear'])
                ->setF122fminAmount((float)$data['f122fminAmount'])
                ->setF122fmaxAmount((float)$data['f122fmaxAmount'])
                ->setF122fpaymentMode((int)$data['f122fpaymentMode'])
                ->setF122fminReimbursement((float)$data['f122fminReimbursement'])
                ->setF122fmaxReimbursement((float)$data['f122fmaxReimbursement'])
                ->setF122famount((float)$data['f122famount'])
                ->setF122freferenceNo($number)
                ->setF122fminReimbursementPercentage((float)$data['f122fminReimbursementPercentage'])
                ->setF122fmaxReimbursementPercentage((float)$data['f122fmaxReimbursementPercentage'])
                ->setF122fidPettyCash((int)$data['f122fidPettyCash'])
                ->setF122fstatus((int)$data['f122fstatus'])
                ->setF122fcreatedBy((int)$_SESSION['userId'])
                ->setF122fupdatedBy((int)$_SESSION['userId']);

        $pattyCash->setF122fcreatedDtTm(new \DateTime())
               ->setF122fupdatedDtTm(new \DateTime());
         
          try{
                $em->persist($pattyCash);
        // print_r($pattyCash);exit;

                $em->flush();
        }

        catch(\Exception $e)
        {
            echo $e;
        }
        return $pattyCash;
    }

    public function updateData($pattyCash, $data = []) 
    {
        $em = $this->getEntityManager();
                $pattyCash->setF122fapprover((int)$data['f122fapprover'])
                ->setF122fdepartmentCode($data['f122fdepartmentCode'])
                ->setF122fallocationYear((int)$data['f122fallocationYear'])
                ->setF122fminAmount((float)$data['f122fminAmount'])
                ->setF122fmaxAmount((float)$data['f122fmaxAmount'])
                ->setF122fpaymentMode((int)$data['f122fpaymentMode'])
                ->setF122fminReimbursement((float)$data['f122fminReimbursement'])
                ->setF122fmaxReimbursement((float)$data['f122fmaxReimbursement'])
                ->setF122famount((float)$data['f122famount'])
                ->setF122freferenceNo($data['f122freferenceNo'])
                ->setF122fidPettyCash((int)$data['f122fidPettyCash'])
                ->setF122fminReimbursementPercentage((float)$data['f122fminReimbursementPercentage'])
                ->setF122fmaxReimbursementPercentage((float)$data['f122fmaxReimbursementPercentage'])
                ->setF122fstatus((int)$data['f122fstatus'])
                ->setF122fupdatedBy((int)$_SESSION['userId']);

        $pattyCash->setF122fupdatedDtTm(new \DateTime());

        try{
        $em->persist($pattyCash);
        // print_r($pattyCash);exit;
        $em->flush();
        }
        catch (\Exception $e){
            echo $e;
        }
    } 
    public function getApprovedData($postData)
    {

        $status = (int)$postData['status'];


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select("p.f122fid, p.f122fapprover, p.f122fdepartmentCode, p.f122fallocationYear, p.f122fminAmount, p.f122fmaxAmount, p.f122fpaymentMode, p.f122fminReimbursement, p.f122fmaxReimbursement, p.f122fstatus, d.f015fdepartmentName, p.f122famount, p.f122freferenceNo, p.f122fminReimbursementPercentage, p.f122fmaxReimbursementPercentage, p.f122fidPettyCash, ( p.f122fdepartmentCode+'-'+d.f015fdepartmentName) as DepartmentName, dms.f011fdefinitionCode, fy.f015fname, sm.f034fname")
            ->from('Application\Entity\T122fpattyCashAllocation', 'p')
             ->leftjoin('Application\Entity\T015fdepartment', 'd','with','d.f015fdepartmentCode= p.f122fdepartmentCode')
              ->leftjoin('Application\Entity\T011fdefinationms', 'dms','with','dms.f011fid= p.f122fpaymentMode')
              ->leftjoin('Application\Entity\T015ffinancialyear', 'fy','with','fy.f015fid= p.f122fallocationYear')
             ->leftjoin('Application\Entity\T014fuser', 'u', 'with','p.f122fapprover = u.f014fid')
              ->leftjoin('Application\Entity\T034fstaffMaster', 'sm','with','sm.f034fstaffId = u.f014fidStaff')
            ->where('p.f122fstatus = :pattyCashId')
            ->setParameter('pattyCashId',$status);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $resultpatty = array();
        // print_r($result);exit;
        
        foreach ($result as $results)
        {
            $id = (int)$results['f122fapprover'];
            $query = "select f034fname from  t014fuser inner join t034fstaff_master on f014fstaff_id = f034ftstaff_id where f014fid = $id ";
            // print_r($query);exit;

            $select = $em->getConnection()->executeQuery($query);
            $result1 = $select->fetch();
            
            $results['f034fname'] = $result1['f034fname'];
            // print_r($results);exit;
            array_push($resultpatty, $results);
            
        }
        return $resultpatty;
    }
    public function approvePettyCashAllocation($data)
    {
        $em = $this->getEntityManager();
        $user_id = $_SESSION['userId'];
        foreach ($data['id'] as $id)
        {
            try
            {
            $status = $data['status'];
            // print_r($status);exit;
            $query1 = "update T122fpatty_cash_allocation set f122fstatus = $status, f122fupdated_by = $user_id where f122fid = $id";
            // print_r($query1);exit;
            $em->getConnection()->executeQuery($query1);
            }
            catch(\Exception $e)
            {
                echo $e;
            }
        }
        
            
    }
} 
?>