<?php
namespace Application\Repository;

use Application\Entity\T081forderType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class OrderTypeRepository extends EntityRepository 
{

    public function getList() 
    {
        
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('ot.f081fid, ot.f081forderType, ot.f081fdescription, ot.f081faccountCode, a.f059fname, ot.f081fstatus')
            ->from('Application\Entity\T081forderType', 'ot')
            ->leftjoin('Application\Entity\T059faccountCode', 'a', 'with', 'ot.f081faccountCode = a.f059fcompleteCode')
            ->orderBy('ot.f081fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $qb->select('ot.f081fid, ot.f081forderType, ot.f081fdescription, ot.f081faccountCode, ot.f081fstatus ')
            ->from('Application\Entity\T081forderType', 'ot')
            ->where('ot.f081fid = :orderTypeId')
            ->setParameter('orderTypeId',$id);
            
        $query = $qb->getQuery();
        $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $result;
        
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $orderType = new T081forderType();

        $orderType->setF081forderType($data['f081forderType'])
                ->setF081fdescription($data['f081fdescription'])
                ->setF081faccountCode($data['f081faccountCode'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081fcreatedBy((int)$_SESSION['userId'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $orderType->setF081fcreatedDtTm(new \DateTime())
                  ->setF081fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($orderType);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $orderType;
    }

    public function updateData($orderType, $data = []) 
    {
        $em = $this->getEntityManager();
    
         $orderType->setF081forderType($data['f081forderType'])
                ->setF081fdescription($data['f081fdescription'])
                ->setF081faccountCode($data['f081faccountCode'])
                ->setF081fstatus((int)$data['f081fstatus'])
                ->setF081fupdatedBy((int)$_SESSION['userId']);

        $orderType->setF081fupdatedDtTm(new \DateTime());
        
        $em->persist($orderType);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('ot.f081fid, ot.f081forderType, ot.f081fdescription, ot.f081faccountCode, ot.f081fstatus ')
            ->from('Application\Entity\T081forderType', 'ot')
            ->orderBy('ot.f081forderType')
            ->where('ot.f081fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    
}