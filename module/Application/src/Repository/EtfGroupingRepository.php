<?php
namespace Application\Repository;

use Application\Entity\T138fetfGrouping;
use Application\Entity\T139fetfGroupingDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class EtfGroupingRepository extends EntityRepository
{

    public function createNewData($data)
    {

        $em        = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= "ETFGroup";
        $number = $configRepository->generateFIMS($numberData);
      
        $etfGrouping = new T138fetfGrouping();
        
    $etfGrouping->setF138fbatchId($number)
            ->setF138freferenceNumber($data['f138freferenceNumber'])
            ->setF138fdescription($data['f138fdescription'])
            ->setF138famount((float)$data['f138famount'])
            ->setF138fupdatedDtTm(new \DateTime())
            ->setF138fcreatedDtTm(new \DateTime())
            ->setF138fstatus((int)1)
            ->setF138fupdatedBy((int)$_SESSION['userId'])
            ->setF138fcreatedBy((int)$_SESSION['userId']);

                $em->persist($etfGrouping);
                $em->flush();

                $etfGroupings = $data['details'];

                foreach ($etfGroupings as $details)
                {

                $etfGroupingDetails = new T139fetfGroupingDetails();

     $etfGroupingDetails->setF139fidMaster((int)$etfGrouping->getF138fid())
                ->setF139fidEtf((int)$details['f139fidEtf'])
                ->setF139famount((float)$details['f139famount'])
                ->setF139fcreatedBy((int)$_SESSION['userId'])
                ->setF139fupdatedBy((int)$_SESSION['userId'])
                ->setF139fstatus((int)1);

            $etfGroupingDetails->setF139fcreatedDtTm(new \DateTime())
                ->setF139fupdatedDtTm(new \DateTime());

        

                $em->persist($etfGroupingDetails);
                $em->flush();
            }

           
        

        return $etfGrouping;
    }

    public function UpdateMaster($etfGrouping,$data)
    {
        $em      = $this->getEntityManager();
        
       $etfGrouping->setF138freferenceNumber($data['f138freferenceNumber'])
            ->setF138fdescription($data['f138fdescription'])
            ->setF138famount((float)$data['f138famount'])
            ->setF138fupdatedDtTm(new \DateTime())
            ->setF138fstatus((int)$data['f138fstatus'])
            ->setF138fupdatedBy((int)$_SESSION['userId']);
            try {

                $em->persist($etfGrouping);

                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

        return $etfGrouping;
    }

    public function createDetail($etfGrouping,$details)
    {
        $em      = $this->getEntityManager();
        
            try {
                $etfGroupingDetails = new T139fetfGroupingDetails();
            } catch (\Exception $e) {
                echo $e;
            }

           $etfGroupingDetails->setF139fidMaster((int)$etfGrouping->getF138fid())
                ->setF139fidEtf((int)$details['f139fidEtf'])
                ->setF139famount((float)$details['f139famount'])
                ->setF139fcreatedBy((int)$_SESSION['userId'])
                ->setF139fupdatedBy((int)$_SESSION['userId'])
                ->setF139fstatus((int) $data['f138fstatus']);

            $etfGroupingDetails->setF139fcreatedDtTm(new \DateTime())
                ->setF139fupdatedDtTm(new \DateTime());

            try {

                $em->persist($etfGroupingDetails);
                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

        return $etfGroupingDetails;
    }

    public function UpdateDetail($etfGroupingDetails,$details)
    {
        $em      = $this->getEntityManager();
        
          

           $etfGroupingDetails->setF139fidEtf((int)$details['f139fidEtf'])
                ->setF139famount((float)$details['f139famount'])
                ->setF139fupdatedBy((int)$_SESSION['userId'])
                ->setF139fstatus((int) $data['f138fstatus']);

            $etfGroupingDetails->setF139fupdatedDtTm(new \DateTime());

            try {


                $em->persist($etfGroupingDetails);
                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

            try {
                $em->persist($etfGroupingDetails);
                $em->flush();

            } catch (\Exception $e) {
                echo $e;
            }

        return $etfGroupingDetails;
    }
    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('e.f138fid, e.f138fbatchId, e.f138freferenceNumber, e.f138fdescription, e.f138famount, e.f138fstatus, ed.f139fid, ed.f139fidMaster, ed.f139fidEtf, ed.f139famount, ed.f139fstatus')
            ->from('Application\Entity\T138fetfGrouping', 'e')
            ->leftjoin('Application\Entity\T139fetfGroupingDetails', 'ed', 'with', 'ed.f139fidMaster = e.f138fid');
        $qb->where('e.f138fid = :id')
            ->setParameter('id', $id);
        try {
            $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        } catch (\Exception $e) {
            echo $e;
        }
        return $data;

    }
    public function approvedEtfGroupings($status)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('e.f138fid,e.f138fbatchId,e.f138freferenceNumber,e.f138fdescription,e.f138famount,e.f138fstatus')
            ->from('Application\Entity\T138fetfGrouping', 'e');
         $qb->where('e.f138fstatus = :status')
                ->orderby('e.f138fid','DESC')

            ->setParameter('status', (int)$status);


        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result1;
    }
  
    public function approve($postData)
    {
        $em = $this->getEntityManager();
        $user_id = $_SESSION['userId'];

        foreach ($postData['id'] as $id)
        {
            $reason = $postData['reason'];
            $status = $postData['status'];
        	$query1 = "update t138fetf_grouping set f138fstatus = $status,f138fapproved_by = $user_id, f138freason = '$reason'  where f138fid = $id"; 
            $em->getConnection()->executeQuery($query1);

            $query1 = "update t138fetf_grouping_details set f139fstatus = $status where f139fid_master = $id"; 
            $em->getConnection()->executeQuery($query1);
        }
    }  
    
    public function getList()
    {
        // print_r("asd");exit;

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('e.f138fid, e.f138fbatchId, e.f138freferenceNumber, e.f138fdescription, e.f138famount, e.f138fstatus')
            ->from('Application\Entity\T138fetfGrouping', 'e')
                ->orderby('e.f138fid','DESC');

        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
    }  

}
