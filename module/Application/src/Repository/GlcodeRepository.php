<?php
namespace Application\Repository;

use Application\Entity\T014fglcode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class GlcodeRepository extends EntityRepository
{

    public function getGlcodeList($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015funit,d.f015fdeptCode,g.f014fid,  g.f014fglCode, g.f014ffundId, g.f014factivityId, g.f014fdepartmentId, g.f014faccountId, g.f014fstatus, g.f014fcreatedBy, g.f014fupdatedBy, f.f057fname as fundName, d.f015fdepartmentName as departmentName,at.f058fcompleteCode as activityCode, ac.f059fcompleteCode as accountCode,at.f058fname as activityName, ac.f059fname as accountName')
            ->from('Application\Entity\T014fglcode', 'g')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f014ffundId = f.f057fid')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f014factivityId = at.f058fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f014fdepartmentId = d.f015fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f014faccountId = ac.f059fid');
        if ($id == 0 && isset($id)) {

            $qb->where('g.f014fstatus = 0');
        }
        if ($id == 1) {
            $qb->where('g.f014fstatus = 1');
        }

        $query  = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;

    }
    public function getList()
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015funit,d.f015fdeptCode,g.f014fid,  g.f014fglCode, g.f014ffundId, g.f014factivityId, g.f014fdepartmentId, g.f014faccountId, g.f014fstatus, g.f014fcreatedBy, g.f014fupdatedBy, f.f057fname as fundName, d.f015fdepartmentName as departmentName,at.f058fcompleteCode as activityCode, ac.f059fcompleteCode as accountCode,at.f058fname as activityName, ac.f059fname as accountName')
            ->from('Application\Entity\T014fglcode', 'g')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f014ffundId = f.f057fid')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f014factivityId = at.f058fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f014fdepartmentId = d.f015fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f014faccountId = ac.f059fid');
            // $qb->where('g.f014fstatus = 1');
        
        $query  = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;

    }

    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('d.f015funit,d.f015fdeptCode,g.f014fid,  g.f014fglCode, g.f014ffundId, g.f014factivityId, g.f014fdepartmentId, g.f014faccountId, g.f014fstatus, g.f014fcreatedBy, g.f014fupdatedBy, f.f057fname as fundName, d.f015fdepartmentName as departmentName, at.f058fcode as activityCode, ac.f059fcode as accountCode')
            ->from('Application\Entity\T014fglcode', 'g')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f014ffundId = f.f057fid')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f014factivityId = at.f058fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f014fdepartmentId = d.f015fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f014faccountId = ac.f059fid')
            ->where('g.f014fid = :glcodeId')
            ->setParameter('glcodeId', $id);

        $query  = $qb->getQuery();
        $result = $query->getOneorNullResult();

        return $result;
    }

    public function createNewData($data)
    {

        $em     = $this->getEntityManager();
        $glcode = new T014fglcode();
        $glcode->setF014fglCode($data['f014fglCode'])
            ->setF014fstatus((int) $data['f014fstatus'])
            ->setF014ffundId((int) $data['f014ffundId'])
            ->setF014fdepartmentId((int) $data['f014fdepartmentId'])
            ->setF014factivityId((int) $data['f014factivityId'])
            ->setF014faccountId((int) $data['f014faccountId'])
            ->setF014fcreatedBy((int)$_SESSION['userId'])
            ->setF014fupdatedBy((int)$_SESSION['userId']);

        $glcode->setF014fcreateDtTm(new \DateTime())
            ->setF014fupdateDtTm(new \DateTime());

        // try
        // {
            $em->persist($glcode);
            $em->flush();

        // } catch (Exception $e) {
        //     if ($e->getErrorCode() == 1062) {
        //         return new JsonModel([
        //             'status'  => 200,
        //             'message' => 'Already exist.',
        //         ]);
        //     }
        // }

        return $glcode;
    }

    public function updateData($glcode, $data = [])
    {
        $em = $this->getEntityManager();

        $glcode->setF014fglCode($data['f014fglCode'])
            ->setF014ffundId($data['f014ffundId'])
            ->setF014fdepartmentId($data['f014fdepartmentId'])
            ->setF014factivityId($data['f014factivityId'])
            ->setF014faccountId($data['f014faccountId'])
            ->setF014fcreatedBy((int)$_SESSION['userId'])
            ->setF014fupdatedBy((int)$_SESSION['userId']);

        $glcode->setF014fupdateDtTm(new \DateTime());

        $em->persist($glcode);
        
        $em->flush();

    }

    public function update($entity)
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
    public function getAllFundCodes()
    {
        $em     = $this->getEntityManager();
        $query  = "select f057fid as id,f057fcode as code from t057ffund";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }
    public function getAllDepartmentCodes()
    {
        $em     = $this->getEntityManager();
        $query  = "select f015fid as id,f015fdepartment_code as code from t015fdepartment";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }
    public function getAllActivityCodes()
    {
        $em     = $this->getEntityManager();
        $query  = "select f058fid as id,f058fcomplete_code as code from t058factivity_code where f058flevel_status = 2";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }
    public function getAllAccountCodes()
    {
        $em     = $this->getEntityManager();
        $query  = "select f059fid as id,f059fcomplete_code as code from t059faccount_code where f059flevel_status =2";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
        return $result;
    }
    public function getFundCode($id)
    {
        $em     = $this->getEntityManager();
        $query  = "select f057fid as id,f057fcode as code from t057ffund where f057fid = $id";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        return $result;
    }
    public function getDepartmentCode($id)
    {
        $em     = $this->getEntityManager();
        $query  = "select f015fid as id,f015fdepartment_code as code from t015fdepartment where f015fid = $id";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        return $result;
    }
    public function getActivityCode($id)
    {
        $em     = $this->getEntityManager();
        $query  = "select f058fid as id,f058fcomplete_code as code from t058factivity_code where f058flevel_status = 2 and f058fid = $id";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        return $result;
    }
    public function getAccountCode($id)
    {
        $em     = $this->getEntityManager();
        $query  = "select f059fid as id,f059fcomplete_code as code from t059faccount_code where f059flevel_status =2 and f059fid = $id";
        $result = $em->getConnection()->executeQuery($query)->fetch();
        return $result;
    }
    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select('g.f014fid,  g.f014fglCode, g.f014ffundId, g.f014factivityId, g.f014fdepartmentId, g.f014faccountId, g.f014fstatus, g.f014fcreatedBy, g.f014fupdatedBy')
            ->from('Application\Entity\T014fglcode', 'g')
            ->where('g.f014fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }
}
