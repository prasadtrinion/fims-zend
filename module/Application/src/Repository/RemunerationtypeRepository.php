<?php
namespace Application\Repository;


use Application\Entity\T019fremunerationType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class RemunerationtypeRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f019fid, r.f019ftype, r.f019fstatus, r.f019fupdatedBy, r.f019fupdatedDtTm, r.f019fcreatedBy, r.f019fcreatedDtTm')
          ->from('Application\Entity\T019fremunerationType', 'r');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f019fid, r.f019ftype, r.f019fstatus, r.f019fupdatedBy, r.f019fupdatedDtTm, r.f019fcreatedBy, r.f019fcreatedDtTm')
            ->from('Application\Entity\T019fremunerationType', 'r')
            ->where('r.f019fid = :remunerationTypeId')
            ->setParameter('remunerationTypeId',$id);

        $query = $qb->getQuery();
        
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $remuneration = new T019fremunerationType();

        $remuneration->setF019ftype($data['f019ftype'])
             ->setF019fstatus($data['f019fstatus'])  
             ->setF019fupdatedBy((int)$_SESSION['userId'])
             ->setF019fcreatedBy((int)$_SESSION['userId']);

        $remuneration->setF019fcreatedDtTm(new \DateTime())
                ->setF019fupdatedDtTm(new \DateTime());

 
        $em->persist($remuneration);
        $em->flush();

        return $remuneration;

    }

    /* to edit the data in database*/

    public function updateData($remuneration, $data = []) 
    {
        $em = $this->getEntityManager();

        $remuneration->setF019ftype($data['f019ftype'])
             ->setF019fstatus($data['f019fstatus'])  
             ->setF019fupdatedBy((int)$_SESSION['userId'])
             ->setF019fupdatedDtTm(new \DateTime())
             ->setF019fcreatedBy((int)$_SESSION['userId']);
                
      
        $em->persist($remuneration);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

}

?>