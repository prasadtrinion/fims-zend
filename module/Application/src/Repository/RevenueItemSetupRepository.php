<?php
namespace Application\Repository;

use Application\Entity\T036frevenueItemSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class RevenueItemSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('r.f036fid, r.f036fname, r.f036fidGlcode, r.f036fstatus')
            ->from('Application\Entity\T036frevenueItemSetup', 'r');

        $qb->select('r.f036fid, r.f036fname, r.f036fidGlcode, r.f036fstatus,g.f014fglCode as glCode')
            ->from('Application\Entity\T036frevenueItemSetup', 'r')
            ->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'r.f036fidGlcode = g.f014fid');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('r.f036fid, r.f036fname, r.f036fidGlcode, r.f036fstatus,g.f014fglCode as glCode')
            ->from('Application\Entity\T036frevenueItemSetup', 'r')
            ->leftjoin('Application\Entity\T014fglcode', 'g', 'with', 'r.f036fidGlcode = g.f014fid')
            ->where('r.f036fid = :revenueId')
            ->setParameter('revenueId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $revenue = new T036frevenueItemSetup();

        $revenue->setF036fname($data['f036fname'])
                ->setF036fidGlcode((int)$data['f036fidGlcode'])
                ->setF036fstatus((int)$data['f036fstatus'])
                ->setF036fcreatedBy((int)$_SESSION['userId'])
                ->setF036fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF036fcreatedDtTm(new \DateTime())
                ->setF036fupdatedDtTm(new \DateTime());


        try
        {
            $em->persist($revenue);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $revenue;
    }

    public function updateData($revenue, $data = []) 
    {
        $em = $this->getEntityManager();

        $revenue->setF036fname($data['f036fname'])
                ->setF036fidGlcode((int)$data['f036fidGlcode'])
                ->setF036fstatus((int)$data['f036fstatus'])
                ->setF036fcreatedBy((int)$_SESSION['userId'])
                ->setF036fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF036fcreatedDtTm(new \DateTime())
                ->setF036fupdatedDtTm(new \DateTime());


        
        $em->persist($revenue);
        $em->flush();

    }

    
}