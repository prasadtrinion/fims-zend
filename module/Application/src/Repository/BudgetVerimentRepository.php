<?php
namespace Application\Repository;

use Application\Entity\T056fverimentMaster;
use Application\Entity\T056fverimentDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class BudgetVerimentRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        // print_r("hello");
        // exit();
        $qb->select("v.f056fid, v.f056fidFinancialYear,v.f056fverimentType, f.f110fyear as financialYear,(fu.f057fcode+'-'+fu.f057fname) as fundName, v.f056ffromFundCode, v.f056ffromAccountCode, v.f056ffromActivityCode, v.f056ffromDepartmentCode, v.f056ffromDepartment, (fd.f015fdepartmentCode+'-'+fd.f015fdepartmentName) as department, fd.f015funit, fd.f015fdeptCode, fd.f015fdepartmentCode, v.f056ftotalAmount, v.f056fapproval, v.f056fstatus")
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T110fbudgetyear','f','with', 'v.f056fidFinancialYear = f.f110fid')
            ->leftjoin('Application\Entity\T015fdepartment','fd','with', 'v.f056ffromDepartment = fd.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T057ffund','fu','with', 'v.f056ffromFundCode = fu.f057fcode')
            ->orderBy('v.f056fid', 'DESC');
         

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

            $qb->select('v.f056fid, v.f056fidFinancialYear,v.f056fverimentType, v.f056ffromFundCode, v.f056ffromAccountCode, v.f056ffromActivityCode, v.f056ffromDepartmentCode, v.f056ffromDepartment, v.f056freason, v.f056ftotalAmount, v.f056fapproval, v.f056fstatus, vd.f056fidDetails, vd.f056ftoIdFinancialYear, vd.f056ftoDepartment,vd.f056fdescription, vd.f056ftoFundCode, vd.f056ftoDepartmentCode, vd.f056ftoActivityCode, vd.f056ftoAccountCode, vd.f056famount')
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T056fverimentDetails','vd','with', 'v.f056fid = vd.f056fidVeriment')
            ->where('v.f056fid = :verimentId')
            ->setParameter('verimentId',(int)$id)
            ->andwhere('vd.f056fstatus != 2');
        
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $data;
    }

     public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if($approvedStatus == 0 && isset($approvedStatus))
        {
            $qb->select("v.f056fid, v.f056fidFinancialYear,v.f056fverimentType, fu.f057fname as fundName,f.f110fyear as financialYear, v.f056ffromFundCode, v.f056ffromAccountCode, v.f056ffromActivityCode, v.f056ffromDepartmentCode, v.f056ffromDepartment, fd.f015fdepartmentName as departmentCode, fd.f015funit, fd.f015fdeptCode, fd.f015fdepartmentCode, v.f056ftotalAmount, v.f056fapproval, v.f056fstatus,(fd.f015fdepartmentCode+' - '+fd.f015fdepartmentName) as originalName")
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T110fbudgetyear','f','with', 'v.f056fidFinancialYear = f.f110fid')
            ->leftjoin('Application\Entity\T057ffund','fu','with', 'v.f056ffromFundCode = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment','fd','with', 'v.f056ffromDepartment = fd.f015fdepartmentCode')
            ->where('v.f056fapproval = 0');
        }
        elseif($approvedStatus == 1 && isset($approvedStatus))
        {
            $qb->select("v.f056fid, v.f056fidFinancialYear,v.f056fverimentType, fu.f057fname as fundName, f.f110fyear as financialYear, v.f056ffromFundCode, v.f056ffromAccountCode, v.f056ffromActivityCode, v.f056ffromDepartmentCode, v.f056ffromDepartment, fd.f015fdepartmentName as departmentCode, fd.f015funit, fd.f015fdeptCode, fd.f015fdepartmentCode, v.f056ftotalAmount, v.f056fapproval, v.f056fstatus,(fd.f015fdepartmentCode+' - '+fd.f015fdepartmentName) as originalName")
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T110fbudgetyear','f','with', 'v.f056fidFinancialYear = f.f110fid')
            ->leftjoin('Application\Entity\T057ffund','fu','with', 'v.f056ffromFundCode = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment','fd','with', 'v.f056ffromDepartment = fd.f015fdepartmentCode')
            ->where('v.f056fapproval = 1');
        }
        elseif ($approvedStatus == 2 && isset($approvedStatus)) 
        {
            $qb->select("v.f056fid, v.f056fidFinancialYear,v.f056fverimentType, fu.f057fname as fundName, f.f110fyear as financialYear, v.f056ffromFundCode, v.f056ffromAccountCode, v.f056ffromActivityCode, v.f056ffromDepartmentCode, v.f056ffromDepartment, fd.f015fdepartmentName as departmentCode, fd.f015funit, fd.f015fdeptCode, fd.f015fdepartmentCode, v.f056ftotalAmount, v.f056fapproval, v.f056fstatus,(fd.f015fdepartmentCode+' - '+fd.f015fdepartmentName) as originalName")
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T110fbudgetyear','f','with', 'v.f056fidFinancialYear = f.f110fid')
            ->leftjoin('Application\Entity\T057ffund','fu','with', 'v.f056ffromFundCode = fu.f057fcode')
            ->leftjoin('Application\Entity\T015fdepartment','fd','with', 'v.f056ffromDepartment = fd.f015fdepartmentCode');
        }
        
        
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $data;
    }

    public function createVeriment($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $veriment = new T056fverimentMaster();

        $veriment->setF056fidFinancialYear((int)$data['f056fidFinancialYear'])
                 ->setF056ffromDepartment($data['f056ffromDepartment'])
                 ->setF056ffromFundCode($data['f056ffromFundCode'])
                  ->setF056fverimentType( $data['f056fverimentType'])
                  ->setF056ffromAccountCode( $data['f056ffromAccountCode'])
                  ->setF056freason( $data['f056freason'])
                  ->setF056ffromActivityCode( $data['f056ffromActivityCode'])
                  ->setF056ffromDepartmentCode( $data['f056ffromDepartmentCode'])
                   ->setF056ftotalAmount((float)$data['f056ftotalAmount'])
             ->setF056fapproval((int)0)
             ->setF056fstatus((int)$data['f056fstatus'])
             ->setF056fcreatedBy((int)$_SESSION['userId'])
             ->setF056fupdatedBy((int)$_SESSION['userId']);

        $veriment->setF056fcreatedDtTm(new \DateTime())
                ->setF056fupdatedDtTm(new \DateTime());
        try{
        $em->persist($veriment);
        $em->flush();
        }
catch(\Exception $e){
echo $e;
}
        $verimentDetails = $data['veriment-details'];

        foreach ($verimentDetails as $verimentDetail) {

            $verimentDetailObj = new T056fverimentDetails();

            $verimentDetailObj->setf056fidVeriment($veriment->getF056fid())
             ->setF056ftoIdFinancialYear((int)$verimentDetail['f056ftoIdFinancialYear'])
             ->setF056ftoDepartment($verimentDetail['f056ftoDepartment'])
             ->setF056fdescription($verimentDetail['f056fdescription'])
            ->setF056ftoFundCode( $verimentDetail['f056ftoFundCode'])
                  ->setF056ftoAccountCode( $verimentDetail['f056ftoAccountCode'])
                  ->setF056ftoActivityCode( $verimentDetail['f056ftoActivityCode'])
                  ->setF056ftoDepartmentCode( $verimentDetail['f056ftoDepartmentCode'])
             ->setF056famount((float)$verimentDetail['f056famount'])
             ->setF056fstatus((int)$verimentDetail['f056fstatus'])
             ->setF056fcreatedBy((int)$_SESSION['userId'])
             ->setF056fupdatedBy((int)$_SESSION['userId']);

        $verimentDetailObj->setF056fcreatedDtTm(new \DateTime())
                    ->setF056fupdatedDtTm(new \DateTime());
 
        try{

	$em->persist($verimentDetailObj);
	
        $em->flush();
 }
	catch(\Exception $e){
	echo $e;
	}
            
        }
        return $veriment;
    }

    public function updateVeriment($veriment, $data = [])
    {
        $em = $this->getEntityManager();

        $veriment->setF056fidFinancialYear((int)$data['f056fidFinancialYear'])
                 ->setF056ffromDepartment($data['f056ffromDepartment'])
                  ->setF056fverimentType( $data['f056fverimentType'])
                  ->setF056freason( $data['f056freason'])
                  ->setF056ffromFundCode($data['f056ffromFundCode'])
                  ->setF056ffromAccountCode( $data['f056ffromAccountCode'])
                  ->setF056ffromActivityCode( $data['f056ffromActivityCode'])
                  ->setF056ffromDepartmentCode( $data['f056ffromDepartmentCode'])
                   ->setF056ftotalAmount((float)$data['f056ftotalAmount'])
             ->setF056fstatus((int)$data['f056fstatus'])
             ->setF056fupdatedBy((int)$_SESSION['userId']);

        $veriment->setF056fupdatedDtTm(new \DateTime());
        
        $em->persist($veriment);

        $em->flush();
        
        return $veriment;
    }


    public function updateVerimentDetail($verimentDetailObj, $verimentDetail)
    {
        
        $em = $this->getEntityManager();

        $verimentDetailObj->setF056ftoIdFinancialYear((int)$verimentDetail['f056ftoIdFinancialYear'])
             ->setF056ftoDepartment($verimentDetail['f056ftoDepartment'])
            ->setF056ftoFundCode( $verimentDetail['f056ftoFundCode'])
                  ->setF056ftoAccountCode( $verimentDetail['f056ftoAccountCode'])
             ->setF056fdescription($verimentDetail['f056fdescription'])
             ->setF056ftoActivityCode( $verimentDetail['f056ftoActivityCode'])
                  ->setF056ftoDepartmentCode( $verimentDetail['f056ftoDepartmentCode'])
             ->setF056famount((float)$verimentDetail['f056famount'])
             ->setF056fstatus((int)$verimentDetail['f056fstatus'])
             ->setF056fupdatedBy((int)$_SESSION['userId']);

        $verimentDetailObj->setF056fupdatedDtTm(new \DateTime());

        $em->persist($verimentDetailObj);

        $em->flush();

            
        
    }
    
    public function createVerimentDetail($verimentObj, $verimentDetail)
    {
        $em = $this->getEntityManager();

        $verimentDetailObj = new T056fverimentDetails();

        $verimentDetailObj->setf056fidVeriment($verimentObj->getF056fid())
             ->setF056ftoIdFinancialYear((int)$verimentDetail['f056ftoIdFinancialYear'])
             ->setF056ftoDepartment($verimentDetail['f056ftoDepartment'])
            ->setF056ftoFundCode( $verimentDetail['f056ftoFundCode'])
             ->setF056fdescription($verimentDetail['f056fdescription'])
             ->setF056ftoAccountCode( $verimentDetail['f056ftoAccountCode'])
                  ->setF056ftoActivityCode( $verimentDetail['f056ftoActivityCode'])
                  ->setF056ftoDepartmentCode( $verimentDetail['f056ftoDepartmentCode'])
             ->setF056famount((float)$verimentDetail['f056famount'])
             ->setF056fstatus((int)$verimentDetail['f056fstatus'])
             ->setF056fcreatedBy((int)$_SESSION['userId'])
             ->setF056fupdatedBy((int)$_SESSION['userId']);

        $verimentDetailObj->setF056fcreatedDtTm(new \DateTime())
                    ->setF056fupdatedDtTm(new \DateTime());
        
            $em->persist($verimentDetailObj);
            $em->flush();

        return $verimentDetailObj;
        
    }
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

     public function updateVerimentDetailStatus($deletedIds) 
  {
    $em = $this->getEntityManager();

    foreach ($deletedIds as $deletedId) 
    {
            
        $query = "UPDATE t056fveriment_details set f056fstatus = 2 where f056fid_details = $deletedId"; 
        $result=$em->getConnection()->executeQuery($query);
    }
    return;
        // exit();
    }

    public function VerimentApprovalStatus($veriments,$status,$reason) 
    {
        $em = $this->getEntityManager();

        foreach ($veriments as $veriment) 
        {
            $Id = $veriment->getF056fid();
          $id = (int)$Id;

          $query = "UPDATE t056fveriment_master set f056freason='$reason', f056fapproval=$status where f056fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);
        if($status==1)
        {

        $em = $this->getEntityManager();

          $qb = $em->createQueryBuilder();

          $qb->select('v.f056fid, v.f056fidFinancialYear, v.f056ffromDepartment,v.f056fverimentType,v.f056ffromFundCode,v.f056ffromAccountCode,v.f056ffromActivityCode, v.f056ftotalAmount,  vd.f056ftoIdFinancialYear, vd.f056ftoDepartmentCode,vd.f056ftoFundCode,vd.f056ftoAccountCode,vd.f056ftoActivityCode, vd.f056famount')
            ->from('Application\Entity\T056fverimentMaster','v')
            ->leftjoin('Application\Entity\T056fverimentDetails','vd','with', 'v.f056fid = vd.f056fidVeriment')
            ->where('v.f056fid = :amountId')
            ->setParameter('amountId',$id);

          $query = $qb->getQuery();

          $result1 = $query->getArrayResult();

          foreach($result1 as $result){

          $fromDepartment = $result['f056ffromDepartment'];
          $fromAccount = $result['f056ffromAccountCode'];
          $fromActivity = $result['f056ffromActivityCode'];
          $fromFund = $result['f056ffromFundCode'];
          $idFinancialyear = $result['f056fidFinancialYear'];
          $amount=$result['f056ftotalAmount'];
          $type=$result['f056fverimentType'];
          $financialYear = (int)$idFinancialyear;
          $fromAmount=(float)$amount;


          $toDepartment = $result['f056ftoDepartmentCode'];
          $toFund = $result['f056ftoFundCode'];
          $toAccount = $result['f056ftoAccountCode'];
          $toActivity = $result['f056ftoActivityCode'];
          $toFinancialyear = $result['f056fidFinancialYear'];
          $totalAmount=$result['f056famount'];
          $toFinancialYear = (int)$toFinancialyear;
          $toAmount=(float)$totalAmount;

          
 
            if($type != 1){
             
      $query = "update t108fbudget_summary set  f108fveriment_to = f108fveriment_to + $fromAmount  where f108fdepartment='$fromDepartment' and f108ffund='$fromFund' and f108faccount='$fromAccount' and f108factivity='$fromActivity' and f108fid_financial_year=$financialYear";
          
                $query1 = "update t108fbudget_summary set  f108fveriment_from = f108fveriment_from + $toAmount where f108fdepartment='$toDepartment' and f108ffund='$toFund' and f108faccount='$toAccount' and f108factivity='$toActivity' and f108fid_financial_year=$toFinancialyear";
            }
            else{
               $query = "update t108fbudget_summary set  f108fveriment_to = f108fveriment_to + $toAmount  where f108fdepartment='$toDepartment' and f108ffund='$toFund' and f108faccount='$toAccount' and f108factivity='$toActivity' and f108fid_financial_year=$toFinancialyear";
          
          $query1 = "update t108fbudget_summary set  f108fveriment_from = f108fveriment_from + $fromAmount where f108fdepartment='$fromDepartment' and f108ffund='$fromFund' and f108faccount='$fromAccount' and f108factivity='$fromActivity' and f108fid_financial_year=$financialYear";
           
              

      
            }
            echo $query."\n";
            echo $query1;
           
            die();
          $em->getConnection()->executeQuery($query);
          $em->getConnection()->executeQuery($query1);
           $query = "update t108fbudget_summary set f108fbudget_pending = f108fallocated_amount + f108fincrement_amount - f108fveriment_from + f108fveriment_to -f108fbudget_expensed - f108fbudget_commitment - f108fdecrement_amount where f108fdepartment='$toDepartment' and f108ffund='$toFund' and f108factivity='$toActivity' and f108faccount='$toAccount' and f108fid_financial_year=$financialYear";
          $em->getConnection()->executeQuery($query);
           $query = "update t108fbudget_summary set f108fbudget_pending = f108fallocated_amount + f108fincrement_amount - f108fveriment_from + f108fveriment_to -f108fbudget_expensed - f108fbudget_commitment - f108fdecrement_amount where f108fdepartment='$fromDepartment' and f108ffund='$fromFund' and f108factivity='$fromActivity' and f108faccount='$fromAccount' and f108fid_financial_year=$financialYear";
          $em->getConnection()->executeQuery($query);
          

        //   $query = "update t108fbudget_summary set f108fveriment_to = '$toAmount' where f108fdepartment='$toDepartment' and f108ffund='$toFund' and f108faccount='$toAccount' and f108factivity='$toActivity' and f108fid_financial_year=$toFinancialyear";
        //   $em->getConnection()->executeQuery($query);

        //   $query = "select f051fbalance_amount from t051fbudget_costcenter where f051fdepartment='$fromDepartment' and f051fid_financialyear=$financialYear";
        //   $result=$em->getConnection()->executeQuery($query)->fetchAll();

        //   $verimentFrom=$result[0]['f051fbalance_amount']-$fromAmount;
        //   $total=(float)$verimentFrom;

        //   $query = "update t051fbudget_costcenter set f051fbalance_amount = '$total' where f051fdepartment='$fromDepartment' and f051fid_financialyear=$financialYear";
        //   $em->getConnection()->executeQuery($query);

        //   $query = "select f051fbalance_amount from t051fbudget_costcenter where f051fdepartment='$toDepartment' and f051fid_financialyear=$toFinancialYear";
        //   $data=$em->getConnection()->executeQuery($query)->fetchAll();

        //   $verimentTo=$data[0]['f051fbalance_amount']+$toAmount;
        //   $totalAdd=(float)$verimentTo;

        //   $query = "update t051fbudget_costcenter set f051fbalance_amount = '$totalAdd' where f051fdepartment='$toDepartment' and f051fid_financialyear=$toFinancialYear";
        //   $em->getConnection()->executeQuery($query);
        
          }
        }
        }

    }

    public function getVerimentAmount($idFinancialyear,$idDepartment,$idGlcode)
    {

        $em = $this->getEntityManager();

        //Get the amount of veriment master 

        $qb = $em->createQueryBuilder();

        $qb->select('b.f056fid,b.f056ftotalAmount')
            ->from('Application\Entity\T056fverimentMaster','b')
            ->where('b.f056fdepartmentCode = :departmentId')
            ->setParameter('departmentId',$idDepartment)
            ->andwhere('b.f056fidFinancialYear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('b.f056ffromGlcode = :glcodeId')
            ->setParameter('glcodeId',$idGlcode);

        $query = $qb->getQuery();
        
        $results= $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $masterAmount=0;
        foreach ($results as $result) 
        {
            $masterAmount=$masterAmount + $result['f056ftotalAmount'];
        }

        //Get the amount of veriment details
        
        $q1 = $em->createQueryBuilder();
        $q1 ->select('d.f056fidDetails, d.f056famount')
            ->from('Application\Entity\T056fverimentDetails','d')
            ->where('d.f056ftoDepartment = :departmentId')
            ->setParameter('departmentId',$idDepartment)
            ->andwhere('d.f056fidFinancialYear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('d.f056ftoGlcode = :glcodeId')
            ->setParameter('glcodeId',$idGlcode);

        $query1 = $q1->getQuery();
        
        $results1= $query1->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);  

        $detailAmount=0;
        foreach ($results1 as $result1) 
        {
            $detailAmount= $detailAmount + $result1['f056famount'];
        }

        //Get the amount of budget activity

        $q2 = $em->createQueryBuilder();
        $q2->select('ad.f054fidDetails, ad.f054famount')
            ->from('Application\Entity\T054factivityDetails','ad')
            ->leftjoin('Application\Entity\T054factivityMaster', 'am', 'with','am.f054fid = ad.f054fidActivityMaster')
            ->where('am.f054fidDepartment = :departmentId')
            ->setParameter('departmentId',$idDepartment)
            ->andwhere('am.f054fidFinancialyear = :financialyearId')
            ->setParameter('financialyearId',$idFinancialyear)
            ->andwhere('ad.f054fidGlcode = :glcodeId')
            ->setParameter('glcodeId',$idGlcode);

        $query2 = $q2->getQuery();
        
        $results2= $query2->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);  

        $activityAmount=0;
        
        foreach ($results2 as $result2) 
        {
            $activityAmount= $activityAmount + $result2['f054famount'];
        }

        $finalAmount = $activityAmount+$detailAmount-$masterAmount; 

        print_r($finalAmount);
        exit();
        return $finalAmount;
    }


    public function deleteBudgetVerimentDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $query2 = "DELETE from t056fveriment_details WHERE f056fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }
   
}

?>
       
       
