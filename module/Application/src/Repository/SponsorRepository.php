<?php
namespace Application\Repository;

use Application\Entity\T063fsponsor;
use Application\Entity\T063fsponsorDetails;
use Application\Entity\T011fdefinationms;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class SponsorRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("s.f063fid, s.f063ffirstName, s.f063flastName, (s.f063ffirstName+' '+s.f063flastName) as Name,s.f063femail, s.f063ffaxNumber, s.f063fcontactNo, s.f063faddress1, s.f063faddress2, s.f063fpostCode, s.f063fcountry, co.f013fcountryName as countryName, s.f063fstate, st.f012fstateName as stateName, s.f063finchargeOfficer, s.f063fsponsorshipType, a.f011fdefinitionCode as sponsorshipType, s.f063fpaymentStatus, b.f011fdefinitionCode as paymentStatus, s.f063fjobType, c.f011fdefinitionCode as jobType, s.f063fcode, s.f063fsponsorType, s.f063freceivableStatus, e.f011fdefinitionCode as receivableStatus, s.f063fstatus, s.f063ffund, s.f063fdepartment, s.f063factivity, s.f063faccount, (s.f063fcode+'-'+s.f063ffirstName+' '+s.f063flastName) as SponsorName")
            ->from('Application\Entity\T063fsponsor', 's')
            ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with', 's.f063fsponsorshipType = a.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'b', 'with', 's.f063fpaymentStatus= b.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'c', 'with', 's.f063fjobType = c.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'e', 'with', 's.f063freceivableStatus = e.f011fid')
            ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f063fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','co','with','s.f063fcountry = co.f013fid')
            ->orderBy('s.f063fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("s.f063fid,s.f063ffirstName,s.f063flastName, (s.f063ffirstName+'-'+s.f063flastName) as Name,s.f063femail,s.f063ffaxNumber, s.f063fcontactNo, s.f063fcode, s.f063faddress1, s.f063faddress2, s.f063fpostCode, s.f063fcountry, co.f013fcountryName as countryName, s.f063fstate, st.f012fstateName as stateName, s.f063finchargeOfficer, s.f063fcontactNo, s.f063fsponsorshipType, a.f011fdefinitionCode as sponsorshipType, s.f063fpaymentStatus, b.f011fdefinitionCode as paymentStatus, s.f063fjobType, c.f011fdefinitionCode as jobType, s.f063fsponsorType , s.f063freceivableStatus, e.f011fdefinitionCode as receivableStatus, s.f063fstatus,  s.f063ffund, s.f063fdepartment, s.f063factivity, s.f063faccount, sd.f063fidDetails, sd.f063fidFeeType, sd.f063ffeeCategory, sd.f063fsponsorCode, sd.f063fallowSecond, sd.f063fstatus")
            ->from('Application\Entity\T063fsponsor', 's')
            ->leftjoin('Application\Entity\T063fsponsorDetails', 'sd', 'with','s.f063fid = sd.f063fidSponsor')
            ->leftjoin('Application\Entity\T011fdefinationms', 'a', 'with', 's.f063fsponsorshipType = a.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'b', 'with', 's.f063fpaymentStatus= b.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'c', 'with', 's.f063fjobType = c.f011fid')
            ->leftjoin('Application\Entity\T011fdefinationms', 'e', 'with', 's.f063freceivableStatus = e.f011fid')
             ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f063fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','co','with','s.f063fcountry = co.f013fid')
            ->where('s.f063fid = :sponsorId')
            ->setParameter('sponsorId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createSponsor($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();


        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type']= 'SPONS';
        $number = $configRepository->generateFIMS($numberData);

        
        $sponsor = new T063fsponsor();

        $sponsor->setF063ffirstName($data['f063ffirstName'])
                ->setF063ffaxNumber($data['f063ffaxNumber'])
                ->setF063fcountry((int)$data['f063fcountry'])
                ->setF063fstate((int)$data['f063fstate'])
                ->setF063femail($data['f063femail'])
                ->setF063finchargeOfficer($data['f063finchargeOfficer'])
                ->setF063fpostCode($data['f063fpostCode']   )
                ->setF063flastName($data['f063flastName'])
                ->setF063faddress1($data['f063faddress1'])
                ->setF063faddress2($data['f063faddress2'])
                ->setF063fcontactNo($data['f063fcontactNo'])
                ->setF063ffund($data['f063ffund'])
                ->setF063fdepartment($data['f063fdepartment'])
                ->setF063factivity($data['f063factivity'])
                ->setF063faccount($data['f063faccount'])
                ->setF063fsponsorshipType((int)$data['f063fsponsorshipType'])
                ->setF063fpaymentStatus((int)$data['f063fpaymentStatus'])
                ->setF063fjobType((int)($data['f063fjobType']))
                ->setF063fsponsorType(($data['f063fsponsorType']))
                ->setF063freceivableStatus((int)$data['f063freceivableStatus'])
                ->setF063fcode($number)
                ->setF063fstatus((int)$data['f063fstatus'])
                ->setF063fcreatedBy((int)$_SESSION['userId']);

        $sponsor->setF063fcreatedDtTm(new \DateTime());


        try{
            $em->persist($sponsor);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        
        $sponsorDetails = $data['sponsor-details'];

        foreach ($sponsorDetails as $sponsorDetail) {

            $sponsorDetailsObj = new T063fsponsorDetails();

            $sponsorDetailsObj->setF063fidSponsor((int)$sponsor->getF063fid())
                       ->setF063fidFeeType((int)$sponsorDetail['f063fidFeeType'])
                       ->setF063ffeeCategory((int)$sponsorDetail['f063ffeeCategory'])
                       ->setF063fsponsorCode($sponsorDetail['f063fsponsorCode'])
                       ->setF063fallowSecond((int)$sponsorDetail['f063fallowSecond'])
                       ->setF063fstatus((int)$sponsorDetail['f063fstatus'])
                       ->setF063fcreatedBy((int)$_SESSION['userId'])
                       ->setF063fupdatedBy((int)$_SESSION['userId']);

            $sponsorDetailsObj->setF063fcreatedDtTm(new \DateTime())
                       ->setF063fupdatedDtTm(new \DateTime());
        

        $em->persist($sponsorDetailsObj);

        $em->flush();

            
        }
        return $sponsor;
    }

   public function updateSponsor($sponsor, $data = []) 
    {
        $em = $this->getEntityManager();

        $sponsor->setF063ffirstName($data['f063ffirstName'])
                ->setF063fcontactNo($data['f063fcontactNo'])
                ->setF063ffaxNumber($data['f063ffaxNumber'])
                ->setF063fcountry((int)$data['f063fcountry'])
                ->setF063fstate((int)$data['f063fstate'])
                ->setF063femail($data['f063femail'])
                ->setF063finchargeOfficer($data['f063finchargeOfficer'])
                ->setF063fpostCode($data['f063fpostCode'])
                ->setF063flastName($data['f063flastName'])
                ->setF063faddress1($data['f063faddress1'])
                ->setF063faddress2($data['f063faddress2'])
                ->setF063fcontactNo($data['f063fcontactNo'])
                ->setF063ffund($data['f063ffund'])
                ->setF063fdepartment($data['f063fdepartment'])
                ->setF063factivity($data['f063factivity'])
                ->setF063faccount($data['f063faccount'])
                ->setF063fsponsorshipType((int)($data['f063fsponsorshipType']))
                ->setF063fpaymentStatus((int)($data['f063fpaymentStatus']))
                ->setF063fjobType((int)($data['f063fjobType']))
                ->setF063fsponsorType(($data['f063fsponsorType']))
                ->setF063freceivableStatus((int)($data['f063freceivableStatus']))
                ->setF063fstatus((int)$data['f063fstatus'])
                ->setF063fupdatedBy((int)$_SESSION['userId']);


        $sponsor->setF063fupdatedDtTm(new \DateTime());


        
        $em->persist($sponsor);
        $em->flush();

        return $sponsor;
    }


    public function updateSponsorDetail($sponsorDetailObj, $sponsorDetail)
    {
        
        $em = $this->getEntityManager();
// print_r($sponsorDetail);exit;
            $sponsorDetailObj->setF063fidFeeType((int)$sponsorDetail['f063fidFeeType'])
                       ->setF063ffeeCategory((int)$sponsorDetail['f063ffeeCategory'])
                       ->setF063fsponsorCode($sponsorDetail['f063fsponsorCode'])
                       ->setF063fallowSecond((int)$sponsorDetail['f063fallowSecond'])
                       ->setF063fstatus((int)$sponsorDetail['f063fstatus'])
                       ->setF063fupdatedBy((int)$_SESSION['userId']);

            $sponsorDetailObj->setF063fupdatedDtTm(new \DateTime());

        $em->persist($sponsorDetailObj);

        $em->flush();

            
        
    }
    
    public function createSponsorDetail($sponsorObj, $sponsorDetail)
    {
        $em = $this->getEntityManager();

        $sponsorDetailsObj = new T063fSponsorDetails();

         $sponsorDetailsObj->setF063fidSponsor((int)$sponsorObj->getF063fid())
                       ->setF063fidFeeType((int)$sponsorDetail['f063fidFeeType'])
                       ->setF063ffeeCategory((int)$sponsorDetail['f063ffeeCategory'])
                       ->setF063fsponsorCode($sponsorDetail['f063fsponsorCode'])
                       ->setF063fallowSecond((int)$sponsorDetail['f063fallowSecond'])
                       ->setF063fstatus((int)$sponsorDetail['f063fstatus'])
                       ->setF063fcreatedBy((int)$_SESSION['userId'])
                       ->setF063fupdatedBy((int)$_SESSION['userId']);

            $sponsorDetailsObj->setF063fcreatedDtTm(new \DateTime())
                       ->setF063fupdatedDtTm(new \DateTime());
        $em->persist($sponsorDetailsObj);

        $em->flush();

        return $sponsorDetailsObj;
        
    }

    public function sponsorStudentInvoices($id) 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
       $qb->select('ts.f064fidSponsor,ts.f064fidStudent,i.f071finvoiceNumber,i.f071fid,i.f071finvoiceTotal, i.f071finvoiceType')
          ->from('Application\Entity\T064ftaggingSponsors','ts')
          ->leftjoin('Application\Entity\T071finvoice', 'i','with','i.f071fidCustomer = ts.f064fidStudent')
          ->leftjoin('Application\Entity\T060fstudent', 's','with','s.f060fid = i.f071fidCustomer')
          ->where('ts.f064fidSponsor = :idSponsor')
          ->andWhere("i.f071finvoiceType = 'STD' ")
          // ->groupby('ts.f064fidSponsor,ts.f064fidStudent')
          ->setParameter('idSponsor',$id);

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );
        
        return $result;
        
    }

    public function deleteSponsorDetails($data)
    {

        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
          // print_r($id);exit;
            $id = (int)$id;
            $query2 = "DELETE from t063fsponsor_details WHERE f063fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }    

}
