<?php
namespace Application\Repository;

use Application\Entity\T075fitemCodeSetUp;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ItemCodeSetUpRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('i.f075fid, i.f075fitemCode, i.f075fglcodeId , i.f075fitemDescription, i.f075fgst, i.f075fpercentage, i.f075fstatus, i.f075fcreatedBy, i.f075fupdatedBy')
            ->from('Application\Entity\T075fitemCodeSetUp','i')
            ->leftjoin('Application\Entity\T014fglcode','g','with', 'i.f075fglcodeId = g.f014fid')
            ->orderBy('i.f075fid','DESC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('i.f075fid, i.f075fitemCode, i.f075fglcodeId , i.f075fitemDescription, i.f075fgst, i.f075fpercentage, i.f075fstatus, i.f075fcreatedBy, i.f075fupdatedBy')
            ->from('Application\Entity\T075fitemCodeSetUp','i')
            ->leftjoin('Application\Entity\T014fglcode','g','with', 'i.f075fglcodeId = g.f014fid')
            ->where('i.f075fid = :itemCodeId')
            ->setParameter('itemCodeId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $item = new T075fitemCodeSetUp();

        $item->setF075fitemCode($data['f075fitemCode'])
             ->setF075fglcodeId($data['f075fglcodeId'])
             ->setF075fitemDescription($data['f075fitemDescription'])
             ->setF075fgst($data['f075fgst'])
             ->setF075fpercentage($data['f075fpercentage'])
             ->setF075fstatus($data['f075fstatus'])
             ->setF075fcreatedBy((int)$_SESSION['userId'])
             ->setF075fupdatedBy((int)$_SESSION['userId']);

        $item->setF075fcreatedDtTm(new \DateTime())
                ->setF075fupdatedDtTm(new \DateTime());

 
        $em->persist($item);
        $em->flush();

        return $item;

    }

    /* to edit the data in database*/

    public function updateData($item, $data = []) 
    {
        $em = $this->getEntityManager();

        $item->setF075fitemCode($data['f075fitemCode'])
             ->setF075fglcodeId($data['f075fglcodeId'])
             ->setF075fitemDescription($data['f075fitemDescription'])
             ->setF075fgst($data['f075fgst'])
             ->setF075fpercentage($data['f075fpercentage'])
             ->setF075fstatus($data['f075fstatus'])
             ->setF075fcreatedBy((int)$_SESSION['userId'])
             ->setF075fupdatedBy((int)$_SESSION['userId']);

        $item->setF075fupdatedDtTm(new \DateTime());

        $em->persist($item);
        $em->flush();

    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>