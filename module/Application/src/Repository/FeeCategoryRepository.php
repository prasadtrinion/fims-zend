<?php
namespace Application\Repository;

use Application\Entity\T065ffeeCategory;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class FeeCategoryRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('f.f065fid, f.f065fname, f.f065fcode, f.f065fdescription,f.f065fstatus ')
            ->from('Application\Entity\T065ffeeCategory', 'f')
            ->orderBy('f.f065fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('f.f065fid, f.f065fname, f.f065fcode, f.f065fdescription,f.f065fstatus ')
            ->from('Application\Entity\T065ffeeCategory', 'f')
            ->where('f.f065fid = :feeCategoryId')
            ->setParameter('feeCategoryId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $feeCategory = new T065ffeeCategory();

        $feeCategory->setF065fname($data['f065fname'])
                ->setF065fcode($data['f065fcode'])
                ->setF065fdescription($data['f065fdescription'])
                ->setF065fstatus((int)$data['f065fstatus'])
                ->setF065fcreatedBy((int)$_SESSION['userId'])
                ->setF065fupdatedBy((int)$_SESSION['userId']);

        $feeCategory->setF065fcreatedDtTm(new \DateTime())
                ->setF065fupdatedDtTm(new \DateTime());

        $em->persist($feeCategory);
        $em->flush();
       
        return $feeCategory;
    }

    public function updateData($feeCategory, $data = []) 
    {
        $em = $this->getEntityManager();

        $feeCategory->setF065fname($data['f065fname'])
                ->setF065fcode($data['f065fcode'])
                ->setF065fdescription($data['f065fdescription'])
                ->setF065fstatus((int)$data['f065fstatus'])
                ->setF065fcreatedBy((int)$_SESSION['userId'])
                ->setF065fupdatedBy((int)$_SESSION['userId']);

        $feeCategory->setF065fupdatedDtTm(new \DateTime());


        
        $em->persist($feeCategory);
        $em->flush();

    }
    public function getFeeCategory($status)
    {
                $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('f.f065fid, f.f065fname, f.f065fcode, f.f065fdescription,f.f065fstatus ')
            ->from('Application\Entity\T065ffeeCategory', 'f')
            ->where('f.f065fstatus = :feeCategoryId')
            ->setParameter('feeCategoryId',$status);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

}
