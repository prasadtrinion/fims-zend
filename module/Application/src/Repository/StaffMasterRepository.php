<?php
namespace Application\Repository;

use Application\Entity\T034fstaffMaster;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StaffMasterRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
  

 $query = "select s.f034fstaff_id as f034fstaffId,cast(s.f034fstaff_id as varchar(20)) +'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo from vw_staff_master as s order by s.f034fstaff_id ASC";


//        $qb->select("s.f034fid, s.f034fstaffId,s.f034fname as originalname,s.f034fstaffId+' - '+s.f034fname as f034fname,s.f034fidDepartment, d.f015fdepartmentName as departmentName,  s.f034fstatus,s.f034fgroupId,s.f034freportingTo,s.f034fgradeId")
//            ->from('Application\Entity\T034fstaffMaster', 's')
//            ->leftjoin('Application\Entity\T015fdepartment', 'd','with', 'd.f015fid = s.f034fidDepartment')
//            ->orderBy('s.f034fid','DESC');
// // echo $qb;
// die();
            // $query = $qb->getQuery();
        
        // $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = $em->getConnection()->executeQuery($query)->fetchAll();

        $result= array(
            'data' => $result1
        );

        return $result;

        
    }
    public function getEmployeeEmail() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
  

        $query = "select employeeemail from vw_employee where employeeid = $id";
        $result = $em->getConnection()->executeQuery($query)->fetch();

        return $result;
    }

      public function getFullList() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        // Queryf015fdepartmentName
        // $query = "select s.f034fid,s.f034fstaff_id as f034fstaffId,s.f034fstaff_id +'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,d.f015fdepartment_name as departmentName,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo,e.*,st.* from t034fstaff_master as s inner join vw_employee as e on cast(s.f034fstaff_id as int)= e.employeeid inner join vw_t630staf as st on cast(s.f034fstaff_id as int)= st.f630nopkj inner join t015fdepartment as d on d.f015fid = s.f034fid_department";

 $query = "select s.f034fstaff_id as f034fstaffId, s.f034fstaff_id, s.f034fname , s.f034fname as f034freportingName, s.f034fid_department as f034fidDepartment, s.f034fgrade_id as f034fgradeId, s.f034fgroup_id as f034fgroupId, s.f034fname as originalname, d.departmentname as departmentName,d.hod,d.hod_name
 ,e.*, st.*
from vw_staff_master as s 
join vw_department as d on d.departmentcode = s.f034fid_department
inner join vw_employee as e on s.f034fstaff_id = e.employeeid
inner join vw_t630staf as st on s.f034fstaff_id= st.f630nopkj";

//        $qb->select("s.f034fid, s.f034fstaffId,s.f034fname as originalname,s.f034fstaffId+' - '+s.f034fname as f034fname,s.f034fidDepartment, d.f015fdepartmentName as departmentName,  s.f034fstatus,s.f034fgroupId,s.f034freportingTo,s.f034fgradeId")
//            ->from('Application\Entity\T034fstaffMaster', 's')
//            ->leftjoin('Application\Entity\T015fdepartment', 'd','with', 'd.f015fid = s.f034fidDepartment')
//            ->orderBy('s.f034fid','DESC');
// // echo $qb;
// die();
            // $query = $qb->getQuery();
        // print_r($query);exit;
        // $result1 = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = $em->getConnection()->executeQuery($query)->fetchAll();

        $result= array(
            'data' => $result1
        );

        return $result;

        
    }

    public function getNewStaff() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $query = "select s.f034fstaff_id as f034fstaffId,cast(s.f034fstaff_id as varchar(30))+'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,d.f015fdepartment_name as departmentName,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo,e.*,st.* from vw_staff_master as s inner join vw_employee as e on s.f034fstaff_id= e.employeeid inner join vw_t630staf as st on s.f034fstaff_id= st.f630nopkj  inner join t015fdepartment as d on d.f015fdepartment_code = s.f034fid_department where s.f034fstaff_id not in (select f014fid_staff from t014fuser) order by s.f034fstaff_id";

        $result1 = $em->getConnection()->executeQuery($query)->fetchAll();

        $result= array(
            'data' => $result1
        );

        return $result;

        
    }
     public function loanStaff() 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $query = "select s.f034fstaff_id as f034fstaffId,cast(s.f034fstaff_id as varchar(30))+'-'+s.f034fname as f034fname from vw_staff_master as s inner join t043fvehicle_loan as v on s.f034fstaff_id= v.f043femp_name order by s.f034fstaff_id";

        $result1 = $em->getConnection()->executeQuery($query)->fetchAll();

        $result= array(
            'data' => $result1
        );

        return $result;

        
    }

     public function getGroupList() 
    {
        $em = $this->getEntityManager();
        
        // Queryf015fdepartmentName
    
        $qb = $em->createQueryBuilder();

        // $qb->select('DISTINCT(s.f034fgradeId) as groupId')
        //    ->from('Application\Entity\T034fstaffMaster', 's');
          $query2 = "select f015fgrade as groupId from t015fgrade";
            $result1 = $em->getConnection()->executeQuery($query2)->fetchAll();

        // $query = $qb->getQuery();


        $result= array(
            'data' => $result1,
        );

        return $result;

        
    }

     public function nonBlackList() 
    {
        $em = $this->getEntityManager();
        
        // Queryf015fdepartmentName
    
        $qb = $em->createQueryBuilder();

        $query2 = "select cast(f034fstaff_id as varchar(30))+'-'+f034fname as f034fname,f034fstaff_id as f034fstaffId from vw_staff_master where f034fstaff_id not in (select f018fid_staff from t018fblacklist_emp)";
        // print_r($query2);exit;
            $result1 = $em->getConnection()->executeQuery($query2)->fetchAll();

        $result= array(
            'data' => $result1
        );

        return $result;

        
    }
    
    /* to retrive the data from database using id*/

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
       $query = "select s.f034fstaff_id as f034fstaffId,cast(s.f034fstaff_id as varchar(30))+'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,d.f015fdepartment_name as departmentName,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo,e.*,st.* from vw_staff_master as s inner join vw_employee as e on s.f034fstaff_id= e.employeeid inner join vw_t630staf as st on s.f034fstaff_id= st.f630nopkj  inner join t015fdepartment as d on d.f015fdepartment_code = s.f034fid_department where s.f034fstaff_id=$id";

        $result = $em->getConnection()->executeQuery($query)->fetchAll();
          
        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $staff = new T034fstaffMaster();

        $staff->setF034fstaffId((int)$data['f034fstaffId'])
             ->setF034fidDepartment($data['f034fidDepartment'])
             ->setF034fname($data['f034fname'])  
             ->setF034fgroupId($data['f034fgroupId'])
             ->setF034freportingTo((int)$data['f034freportingTo'])
             ->setF034fgradeId($data['f034fgradeId'])
             ->setF034fstatus((int)$data['f034fstatus'])
             ->setF034fcreatedBy((int)$_SESSION['userId'])
             ->setF034fupdatedBy((int)$_SESSION['userId']);

        $staff->setF034fcreatedDtTm(new \DateTime())
                ->setF034fupdatedDtTm(new \DateTime());

 
        $em->persist($staff);
        $em->flush();

        return $staff;

    }

    /* to edit the data in database*/

    public function updateData($staff, $data = []) 
    {
        $em = $this->getEntityManager();

        $staff->setF034fstaffId((int)$data['f034fstaffId'])
             ->setF034fidDepartment($data['f034fidDepartment'])
             ->setF034fname($data['f034fname']) 
              ->setF034fgroupId($data['f034fgroupId'])
             ->setF034freportingTo((int)$data['f034freportingTo']) 
             ->setF034fgradeId($data['f034fgradeId'])
             ->setF034fstatus((int)$data['f034fstatus'])
             ->setF034fupdatedBy((int)$_SESSION['userId']);

        $staff->setF034fupdateDtTm(new \DateTime());
                
        
        $em->persist($staff);
        $em->flush();

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
         $qb->select("s.f034fstaffId, s.f034fname as f034fname, s.f034fidDepartment,   s.f034fstatus, s.f034fgroupId, s.f034freportingTo, s.f034fgradeId")
           ->from('Application\Entity\T034fstaffMaster', 's')
           ->where('s.f034fstatus=1')
           ->orderBy('s.f034fstaffId','ASC');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    //public function staffListBasedOnDepartment($deptId)
    //{
        //$em = $this->getEntityManager();
        //$qb = $em->createQueryBuilder();
       //$query = "select s.f034fid,s.f034fstaff_id as f034fstaffId,s.f034fstaff_id +'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,d.f015fdepartment_name as departmentName,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo,e.*,st.* from t034fstaff_master as s inner join vw_employee as e on cast(s.f034fstaff_id as int)= e.employeeid inner join vw_t630staf as st on cast(s.f034fstaff_id as int)= st.f630nopkj  inner join t015fdepartment as d on d.f015fdepartment_code = s.f034fid_department where s.f034fid_department='$deptId'";

         //$query = $qb->getQuery();
        
        //$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        //$result = $em->getConnection()->executeQuery($query)->fetchAll();
          
        //return $result;
    //}

    public function getStaffList($deptId)
    {
        $em = $this->getEntityManager();

        //$qb = $em->createQueryBuilder();

       
         $query = "select s.f034fstaff_id as f034fstaffId,cast(s.f034fstaff_id as varchar(30))+'-'+s.f034fname as f034fname,s.f034fid_department as f034fidDepartment,d.f015fdepartment_name as departmentName,s.f034fname as originalname,s.f034fgroup_id as f034fgroupId,s.f034fgrade_id as f034fgradeId,s.f034freporting_to as f034freportingTo,e.*,st.* from vw_staff_master as s inner join vw_employee as e on cast(s.f034fstaff_id as int)= e.employeeid inner join vw_t630staf as st on cast(s.f034fstaff_id as int)= st.f630nopkj  inner join t015fdepartment as d on d.f015fdepartment_code = s.f034fid_department where s.f034fid_department='$deptId'";

        //$query = $qb->getQuery();
         // print_r($query);exit;
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        return $result;
        
    }



}

?>
