<?php
namespace Application\Repository;

use Application\Entity\T041fledger;
use Application\Entity\T042fbankDetails;
use Application\Entity\T043faddressDetails;
use Application\Entity\T044fregistration;
use Application\Entity\T014fuser;
use Application\Repository\T014fuserRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\EntityRepository;

/**
 * LedgerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LedgerRepository extends EntityRepository {
	
	public function getLedgers(){

		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();

		// Query
		$qb->select('l.f041fid,l.f041fname,l.f041fopeningBalance')
			->from('Application\Entity\T041fledger', 'l');
			//->leftjoin('Application\Entity\T042fbankDetails', 'b', 'with', 'b.f042fidLedger = l.f041fid')
			//->leftjoin('Application\Entity\T043faddressDetails', 'a', 'with', 'a.f043fidLedger = l.f041fid')
			///->leftjoin('Application\Entity\T044fregistration', 'r', 'with', 'r.f044fidLedger = l.f041fid');

		$data = $qb->getQuery()->getArrayResult();

		$qb1 = $em->createQueryBuilder();
		
		$qb1->select('count(l.f041fid)')
			->from('Application\Entity\T041fledger', 'l')
			->leftjoin('Application\Entity\T042fbankDetails', 'b', 'with', 'b.f042fidLedger = l.f041fid')
			->leftjoin('Application\Entity\T043faddressDetails', 'a', 'with', 'a.f043fidLedger = l.f041fid')
			->leftjoin('Application\Entity\T044fregistration', 'r', 'with', 'r.f044fidLedger = l.f041fid');

		$total = $qb1->getQuery()->getSingleScalarResult();
		$result = array(
			'total' => $total,
			'data' => $data,
		);
		return $result;
	}	
	
	public function createLedger($data) {

		
		$em = $this->getEntityManager();
		
		$ledger = new T041fledger();
		$ledger->setF041fname($data['f041fname'])
				->setF041faltName($data['f041falt_name'])
				->setF041fglCode($data['f041fglcode'])
				->setF041fgstNumber($data['f041fgst_number'])
				->setF041fgroupName($data['f041fgroup_name'])
				->setF041fopeningBalance((int)$data['f041fopening_balance'])
				->setF041fisSubLedger((int)$data['f041fis_sub_ledger'])
				->setF041fallowSubAccount((int)$data['f041fallow_sub_account'])
				->setF041fapplyBillwise((int)$data['f041fapply_billwise'])
				->setF041factive((int)$data['f041factive'])
				->setF041fcreatedBy((int)$_SESSION['userId'])
				->setF041fupdatedBy((int)$_SESSION['userId']);

		$ledger->setF041fcreatedDtTm(new \DateTime())
				->setF041fupdatedDtTm(new \DateTime());
		try{
		$em->persist($ledger);
		$em->flush();
		}
		catch(\Exception $e){
			echo $e;
		}
		if($data['bank-details']){
		$bankDetails = $data['bank-details'];
		
	
		$bank = new T042fbankDetails();	
		$bank->setF042fbankName($bankDetails['f042fbank_name'])
				->setF042fbranchName($bankDetails['f042fbranch_name'])
				->setF042faccNo($bankDetails['f042facc_no'])
				->setF042faccType($bankDetails['f042facc_type'])
				->setF042fswiftCode($bankDetails['f042fswift_code'])
				->setF042fbrsCode($bankDetails['f042fbrs_code'])
				->setF042fstatus((int)$bankDetails['f042fstatus'])
				->setF042fidLedger((int)$ledger->getF041fid())
				->setF042fcreatedBy((int)$_SESSION['userId'])
				->setF042fupdatedBy((int)$_SESSION['userId']);

		$bank->setF042fcreatedDtTm(new \DateTime())
				->setF042fupdatedDtTm(new \DateTime());
		$em->persist($bank);
		
		}
		if($data['address-details']){
		
		$addressDetails = $data['address-details'];

		$address = new T043faddressDetails();
		$address->setF043fprintName($addressDetails['f043print_name'])
				->setF043fdob($addressDetails['f043fdob'])
				->setF043fmobile($addressDetails['f043fmobile'])
				->setF043femail($addressDetails['f043femail'])
				->setF043fcountry($addressDetails['f043fcountry'])
				->setF043faddress1($addressDetails['f043faddress1'])
				->setF043faddress2($addressDetails['f043faddress2'])
				->setF043fpostCode($addressDetails['f043fpost_code'])
				->setF043fcity($addressDetails['f043fcity'])
				->setF043fstate($addressDetails['f043fstate'])
				->setF043fstatus((int)$addressDetails['f043fstatus'])
				->setF043fidLedger((int)$ledger->getF041fid())
				->setF043fcreatedBy((int)$_SESSION['userId'])
				->setF043fupdatedBy((int)$_SESSION['userId']);

		$address->setF043fcreatedDtTm(new \DateTime())
				->setF043fupdatedDtTm(new \DateTime());
		$em->persist($address);
		
		}
		if($data['registration-details']){
		
		$registrationDetails = $data['registration-details'];

		$registration = new T044fregistration();
		$registration->setF044fsupplierType($registrationDetails['f044fsupplier_type'])
				->setF044ftaxDeductionNumber($registrationDetails['f044ftax_deduction_number'])
				->setF044fgstNumber($data['f041fgst_number'])
				->setF044fnricNumber($registrationDetails['f044fnic_number'])
				->setF044fcin($registrationDetails['f044fcin'])
				->setF044fstatus((int)$registrationDetails['f044fstatus'])
				->setF044fidLedger((int)$ledger->getF041fid())
				->setF044fcreatedBy((int)$_SESSION['userId'])
				->setF044fupdatedBy((int)$_SESSION['userId']);

		$registration->setF044fcreatedDtTm(new \DateTime())
				->setF044fupdatedDtTm(new \DateTime());
				$em->persist($registration);
		
			}
		try{


		
		
		$em->flush();	
		
		}
		catch(\Exception $e){
				
			echo $e;
		}
		return $ledger;
	}

	public function getListById($id){
		
		$em = $this->getEntityManager();

		$qb = $em->createQueryBuilder();

		// Query
		$qb->select('l.f041fid,l.f041fname,l.f041fopeningBalance')
			->from('Application\Entity\T041fledger', 'l')
			->leftjoin('Application\Entity\T042fbankDetails', 'b', 'with', 'b.f042fidLedger = l.f041fid')
			->leftjoin('Application\Entity\T043faddressDetails', 'a', 'with', 'a.f043fidLedger = l.f041fid')
			->leftjoin('Application\Entity\T044fregistration', 'r', 'with', 'r.f044fidLedger = l.f041fid')
			->where('l.f041fid = :id')
			->setParameter('id',$id);
		try{
		$data = $qb->getQuery()->getSingleResult();
		}
		catch(\Exception $e){
			echo $e;
		}
		return $data;
	}

}