<?php
namespace Application\Repository;

use Application\Entity\T098fpreOrderLevelSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class PreOrderLevelSetupRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();   


        $qb->select('p.f098fid, p.f098fkodStock, p.f098fidCategory, ac.f087fcategoryDiscription as categoryCode, p.f098fidSubCategory, c.f089fsubDescription as subcode, p.f098fidAssetType, a.f090ftypeCode as typeCode, p.f098fidItem, 
            i.f088fitemDescription as itemCode, 
            p.f098fpreOrderLevel, p.f098fstatus')
            ->from('Application\Entity\T098fpreOrderLevelSetup', 'p')
            
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'p.f098fidCategory = ac.f087fid')
            
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'p.f098fidSubCategory = c.f089fid')

            ->leftjoin('Application\Entity\T090fassetType', 'a', 'with', 'p.f098fidAssetType = a.f090fid')

            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'p.f098fidItem = i.f088fid')
            ->orderBy('p.f098fid','DESC');
            
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('p.f098fid, p.f098fkodStock,  p.f098fidCategory, ac.f087fcategoryCode as categoryCode, p.f098fidSubCategory, c.f089fsubcode as subcode, p.f098fidAssetType, a.f090ftypeCode as typeCode, p.f098fidItem, i.f088fitemCode as itemCode, p.f098fpreOrderLevel, p.f098fstatus')
            ->from('Application\Entity\T098fpreOrderLevelSetup', 'p')
            
            ->leftjoin('Application\Entity\T087fassetCategory', 'ac', 'with', 'p.f098fidCategory = ac.f087fid')
            
            ->leftjoin('Application\Entity\T089fassetSubCategories', 'c', 'with', 'p.f098fidSubCategory = c.f089fid')

            ->leftjoin('Application\Entity\T090fassetType', 'a', 'with', 'p.f098fidAssetType = a.f090fid')

            ->leftjoin('Application\Entity\T088fitem', 'i', 'with', 'p.f098fidItem = i.f088fid')

            ->where('p.f098fid = :preOrderId')
            ->setParameter('preOrderId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $preOrder = new T098fpreOrderLevelSetup();

        $preOrder->setF098fkodStock($data['f098fkodStock'])
                ->setF098fidCategory((int)$data['f098fidCategory'])
                ->setF098fidSubCategory((int)$data['f098fidSubCategory'])
                ->setF098fidAssetType((int)$data['f098fidAssetType'])
                ->setF098fidItem((int)$data['f098fidItem'])
                ->setF098fpreOrderLevel((int)$data['f098fpreOrderLevel'])
                ->setF098fstatus((int)$data['f098fstatus'])
                ->setF098fcreatedBy((int)$_SESSION['userId'])
                ->setF098fupdatedBy((int)$_SESSION['userId']);

        $preOrder->setF098fcreatedDtTm(new \DateTime())
                ->setF098fupdatedDtTm(new \DateTime());
          
        $em->persist($preOrder);
        
        $em->flush();
        
        return $preOrder;
    }

    public function updateData($preOrder, $data = []) 
    {
        $em = $this->getEntityManager();

        $preOrder->setF098fkodStock($data['f098fkodStock'])
                ->setF098fidCategory((int)$data['f098fidCategory'])
                ->setF098fidSubCategory((int)$data['f098fidSubCategory'])
                ->setF098fidAssetType((int)$data['f098fidAssetType'])
                ->setF098fidItem((int)$data['f098fidItem'])
                ->setF098fpreOrderLevel((int)$data['f098fpreOrderLevel'])
                ->setF098fstatus((int)$data['f098fstatus'])
                ->setF098fupdatedBy((int)$_SESSION['userId']);

        $preOrder->setF098fupdatedDtTm(new \DateTime());
        
        $em->persist($preOrder);
        $em->flush();
    }  
}