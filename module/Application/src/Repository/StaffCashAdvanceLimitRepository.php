<?php
namespace Application\Repository;

use Application\Entity\T119fstaffCashAdvanceLimit;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class StaffCashAdvanceLimitRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('cl.f119fid, cl.f119fstaffId, cl.f119fcashAdvance, cl.f119famount, cl.f119fstatus, sm.f034fname as StaffName, cat.f051fdescription CashType')
            ->from('Application\Entity\T119fstaffCashAdvanceLimit','cl')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'sm','with','sm.f034fstaffId = cl.f119fstaffId')
            ->leftjoin('Application\Entity\T051fcashAdvanceType', 'cat','with','cat.f051fid = cl.f119fcashAdvance')
            ->orderBy('cl.f119fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('cl.f119fid, cl.f119fstaffId, cl.f119fcashAdvance, cl.f119famount, cl.f119fstatus, sm.f034fname as StaffName, cat.f051fdescription CashType')
            ->from('Application\Entity\T119fstaffCashAdvanceLimit','cl')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'sm','with','sm.f034fstaffId = cl.f119fstaffId')
            ->leftjoin('Application\Entity\T051fcashAdvanceType', 'cat','with','cat.f051fid = cl.f119fcashAdvance')
            ->where('cl.f119fid = :cashId')
            ->setParameter('cashId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

   

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        // print_r("sd");exit;

        $cashAdvance = new T119fstaffCashAdvanceLimit();

        $cashAdvance->setF119fstaffId((int)$data['f119fstaffId'])
					->setF119fcashAdvance((int)$data['f119fcashAdvance'])
					->setF119famount((float)$data['f119famount'])
					->setF119fstatus((int)$data['f119fstatus'])
					->setF119fcreatedBy((int)$_SESSION['userId']);
					

        $cashAdvance->setF119fcreatedDtTm(new \DateTime());
               
        try
        {
        $em->persist($cashAdvance);
        
        $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    /* to edit the data in database*/

    public function updateData($cashAdvance, $data = []) 
    {
        $em = $this->getEntityManager();

        $cashAdvance->setF119fstaffId((int)$data['f119fstaffId'])
					->setF119fcashAdvance((int)$data['f119fcashAdvance'])
					->setF119famount((float)$data['f119famount'])
					->setF119fstatus((int)$data['f119fstatus'])
             		->setF119fupdatedBy((int)$_SESSION['userId']);

        $cashAdvance->setF119fupdatedDtTm(new \DateTime());

        try
        {
            $em->persist($cashAdvance);
            $em->flush();
        }
        catch(\Exception $e)
        {
            echo $e;
        }

    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb->select('cl.f119fid, cl.f119fstaffId, cl.f119fcashAdvance, cl.f119famount, cl.f119fstatus')
            ->from('Application\Entity\T119fstaffCashAdvanceLimit','cl')
            ->where('cl.f119fstatus = 1')
            ->orderBy('cl.f119fid','DESC');
            
        $query = $qb->getQuery();


        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function staffConditionCheck($staff) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('c.f052fid, c.f052fcashAdvance, c.f052fidStaff, c.f052fidDepartment, c.f052fidFinancialYear, c.f052fstatus, c.f052ftotal, c.f052fapprovalStatus')
            ->from('Application\Entity\T052fcashApplication','c')
            ->where('c.f052fidStaff = :staffId')
            ->setParameter('staffId',$staff);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        return $result;
        
    }

}

?>