<?php
namespace Application\Repository;

use Application\Entity\T091fupdateAsset;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class UpdateAssetRepository extends EntityRepository 
{

    public function getList() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('u.f091fassetCode, u.f091fassetDescription, u.f091fidCategory, u.f091fidSubcategory, u.f091fidType,u.f091facquisitionDate','u.f091fapprovalStatus')
            ->from('Application\Entity\T091fupdateAsset','u');

        
        $query = $qb->getQuery();

        
 $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
        
    }

    public function getListById($id) 
    {
        
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('u.f091fassetCode, u.f091fassetDescription, u.f091fidCategory, u.f091fidSubcategory, u.f091fidType,u.f091facquisitionDate')
            ->from('Application\Entity\T091fupdateAsset','u')
            ->where('u.f091fid = :id')
            ->setParameter('id',$id);

        
        $query = $qb->getQuery();

        
 $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
       
        return $result;
        
    }
    
    public function createNewData($data) 
    {


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $unit = new T091fupdateAsset();

        $unit->setF091fassetCode($data['f091fassetCode'])
->setF091fassetDescription($data['f091fassetDescription'])
->setF091fidCategory((int)$data['f091fidCategory'])
->setF091fidSubcategory((int)$data['f091fidSubcategory'])
->setF091fidType((int)$data['f091fidType'])
->setF091fidDepartment((int)$data['f091fidDepartment'])
->setF091facquisitionDate(new \DateTime($data['f091facquisitionDate']))
->setF091fregistrationDate(new \DateTime($data['f091fregistrationDate']))
->setF091fapprovalStatus(0)
->setF091fstatus($data['f091fstatus'])
->setF091fcreatedBy((int)$_SESSION['userId'])
->setF091fupdatedBy((int)$_SESSION['userId']);

        $unit->setF091fupdatedDt(new \DateTime())
                    ->setF091fcreatedDt(new \DateTime());


        
        $em->persist($unit);
        $em->flush();
       
        
        
        return $unit;
    }

    public function updateData($unit, $data = []) 
    {
          $em = $this->getEntityManager();


        $unit->setF091fassetCode($data['f091fassetCode'])
        ->setF091fassetDescription($data['f091fassetDescription'])
        ->setF091fidCategory((int)$data['f091fidCategory'])
        ->setF091fidSubcategory((int)$data['f091fidSubcategory'])
        ->setF091fidType((int)$data['f091fidType'])
        ->setF091fidDepartment((int)$data['f091fidDepartment'])
        ->setF091facquisitionDate(new \DateTime($data['f091facquisitionDate']))
        ->setF091fregistrationDate(new \DateTime($data['f091fregistrationDate']))
        ->setF091fapprovalStatus($data['f091fapprovalStatus'])
        ->setF091fstatus($data['f091fstatus'])
        ->setF091fcreatedBy((int)$_SESSION['userId'])
        ->setF091fupdatedBy((int)$_SESSION['userId']);

        $unit->setF091fupdatedDt(new \DateTime())
                    ->setF091fcreatedDt(new \DateTime());

        $em->persist($unit);
        $em->flush();
       
        
        
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('u.f015fid, u.f015funitName, u.f015funitCode, u.f015fstatus')
            ->from('Application\Entity\T015funitCode','u')
            ->where('u.f015fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

    public function assetMovementApproval($assets) 
    {
        $em = $this->getEntityManager();

        foreach ($assets as $asset) 
        {

            $asset->setF091fapprovalStatus((int)"1")
                 ->setF091fUpdatedDt(new \DateTime());
       
            $em->persist($asset);
            $em->flush();
        }
    }

      public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
 $qb->select('u.f091fassetCode, u.f091fassetDescription, u.f091fidCategory, u.f091fidSubcategory, u.f091fidType,u.f091facquisitionDate')
            ->from('Application\Entity\T091fupdateAsset','u');

           if($approvedStatus == 0){
              
                 $qb->where('u.f091fapprovalStatus = 0');
            }
            elseif($approvedStatus == 1){
              
                 $qb->where('u.f091fapprovalStatus = 1');
            }
            elseif ($approvedStatus == 2) {

            }
            $qb->orderBy('u.f091fid','DESC');
            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }


}

?>