<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ViewsRepository extends EntityRepository 
{

    public function getDepartmentCodes() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015fid, d.f015fdepartmentName, d.f015fdepartmentCode, d.f015fstatus')
            ->from('Application\Entity\T015ftemporaryDepartmentCode','d');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getUnitCodes() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f015fid, d.f015funitName, d.f015funitCode, d.f015fstatus')
            ->from('Application\Entity\T015ftemporaryUnitCode','d');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }
	 public function getViewData($name) 
    {


        $em = $this->getEntityManager();
        switch($name){
        case 'employeeSalary':
            $query = "select  * from vw_employee_salary";
            break;
             case 'ugSemester':
            $query = "select  * from vw_ug_t005term";
            break;
            case 'pgSemester':
            $query = "select  * from vw_pg_t005term";
            break;
           case 'ugRegion':
            $query = "select  * from vw_ug_t016warga";
            break;
            case 'pgRegion':
            $query = "select  * from vw_pg_t016warga";
            break;
             case 'ugStudent':
            $query = "select top 100 (cast(f210matrik as varchar(30))+'-'+f210nama) as f210nama,f210matrik from vw_ug_t210student";
            break;
            case 'pgStudent':
            $query = "select  top 100 (cast(f210matrik as varchar(30))+'-'+f210nama) as f210nama,f210matrik from vw_pg_t801studmas";
            break;
            case 'ugProgram':
            $query = "select  f006kdprogram,(f006kdprogram+'-'+f006description) as program_name from vw_ug_t006program";
            break;
             case 'pgProgram':
            $query = "select  f006kdprogram,(f006kdprogram+'-'+f006description) as program_name from vw_pg_t006program";
            break;
            
            case 'ugIntake':
            $query = "select top 100 (term+'-'+sem_bi+'-'+sesi) as intake_name,term from vw_ug_t005term";
            break;
            case 'pgIntake':
            $query = "select top 100 (term+'-'+sem_bi+'-'+sesi) as intake_name,term from vw_pg_t005term";
            break;
            case 'employeeData':
            $query = "select  * from vw_employee";
            break;
            case 'staffData':
            $query = "select  * from vw_t630staf";
            break;
            case 'employeeDetails':
            $query = "select  * from t104femployee_details";
            break;
            case 'ugLearningCenter':
            $query = "select top 100  (kodpusat+'-'+keterangan),kodpusat as learning_center from vw_ug_t027pusat_pjj";
            break;
            case 'pgLearningCenter':
            $query = "select top 100 (kod+'-'+keterangan) as learning_center,kod as kodpusat from vw_pg_t002lokasi";
            break;
            case 'ugCitizen':
            $query = "select  * from vw_ug_t016warga";
            break;
            case 'pgCitizen':
            $query = "select  * from vw_pg_t002lokasi";
            break;
            case 'socode':
            $query = "select  (kod_so+'-'+ktrgn_vot) as socode, tahun as year, thpinda as date, rujukan as reference, kod_vot as codeVot, ktrgn_vot as votDescription, amaun_asal as initialAmount, amaun_kini as currentAmount, kod_so as so from vw_research_budget GROUP BY kod_so";
            break;

            case 'employeeGrade':
            $query = "select distinct(employeegradeid)as gradeId, employeegradename as gradeName, (employeegradeid+'-'+employeegradename) as Grade from vw_employee";
            break;
        }
        $result = $em->getConnection()->executeQuery($query)->fetchAll();
// echo json_encode($result);
//         die();
        return $result;
        
    }
}

?>

