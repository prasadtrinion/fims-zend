<?php
namespace Application\Repository;

use Application\Entity\T015funitCode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class UnitCodeRepository extends EntityRepository 
{

    public function getList() 
    {


        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('u.f015fid, u.f015funitName, u.f015funitCode, u.f015fstatus')
            ->from('Application\Entity\T015funitCode','u');

        
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    public function getListById($id) 
    {
        
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('u.f015fid, u.f015funitName, u.f015funitCode, u.f015fstatus')
            ->from('Application\Entity\T015funitCode','u')
            ->where('u.f015fid = :unitId')
            ->setParameter('unitId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }
    
    public function createNewData($data) 
    {

        $em = $this->getEntityManager();

        $unit = new T015funitCode();

        $unit->setF015funitName($data['f015funitName'])
                ->setF015funitCode($data['f015funitCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fcreatedBy((int)$_SESSION['userId'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $unit->setF015fupdatedDtTm(new \DateTime())
                    ->setF015fcreatedDtTm(new \DateTime());


        
        $em->persist($unit);
        $em->flush();
       
        
        
        return $unit;
    }

    public function updateData($unit, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $unit->setF015funitName($data['f015funitName'])
                ->setF015funitCode($data['f015funitCode'])
                ->setF015fstatus((int)$data['f015fstatus'])
                ->setF015fupdatedBy((int)$_SESSION['userId']);

        $unit->setF015fupdatedDtTm(new \DateTime());
       
        $em->persist($unit);
        $em->flush();
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    public function getActiveList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('u.f015fid, u.f015funitName, u.f015funitCode, u.f015fstatus')
            ->from('Application\Entity\T015funitCode','u')
            ->where('u.f015fstatus=1');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;
        
    }

}

?>