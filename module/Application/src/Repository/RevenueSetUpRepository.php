<?php
namespace Application\Repository;

use Application\Entity\T085frevenueSetUp;
use Application\Entity\T085frevenueSetupDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class RevenueSetUpRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
//, f.f057fname as fundName,f.f057fcode as fundCode, d.f015fdepartmentName as departmentName,d.f015fdepartmentCode as departmentCode, at.f058fname as activityName,at.f058fcompleteCode as activityCode
        $qb->select("g.f085fid, g.f085faccountId, g.f085fstatus,g.f085fidRevenueCategory, g.f085fname,i.f073fcategoryName as CategoryName, g.f085fidTaxCode, (g.f085ffund+'-'+f.f057fname) as Fund, (g.f085fdepartment+'-'+d.f015fdepartmentName) as Department, (g.f085factivity+'-'+at.f058fname) as Activity, t.f081fcode as TaxCode,g.f085fcode,(g.f085fcode+'-'+g.f085fname) as originalname,(ac.f059fcompleteCode +' - '+ ac.f059fname) as  accountName")
            ->from('Application\Entity\T085frevenueSetUp', 'g')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with', 'g.f085fidTaxCode = t.f081fid')
            ->leftjoin('Application\Entity\T073fcategory', 'i', 'with', 'g.f085fidRevenueCategory = i.f073fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with','g.f085faccountId = ac.f059fcompleteCode')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f085ffund = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f085factivity = at.f058fcompleteCode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f085fdepartment = d.f015fdepartmentCode')
            ->orderBy('g.f085fid','DESC');


        // $qb->select('g.f085fid, g.f085ffundId, g.f085factivityId, g.f085fdepartmentId, g.f085faccountId, g.f085fstatus, g.f085ffeeItem, f.f057fname as fundName')
        //     ->from('Application\Entity\T085frevenueSetUp', 'g')
            // ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with', 'g.f085faccountId = ac.f059fid')
            // ->leftjoin('Application\Entity\T066ffeeItem', 'f', 'with', 'g.f085ffeeItem = f.f066fid');
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("g.f085fid, g.f085faccountId,g.f085fcode, g.f085fstatus,g.f085fidRevenueCategory, g.f085fname,i.f073fcategoryName as CategoryName, g.f085fidTaxCode, t.f081fcode as TaxCode, g.f085fcode,(ac.f059fcompleteCode +' - '+ ac.f059fname) as  accountName, g.f085ffund, g.f085fdepartment, g.f085factivity")
            ->from('Application\Entity\T085frevenueSetUp', 'g')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with', 'g.f085fidTaxCode = t.f081fid')
            ->leftjoin('Application\Entity\T073fcategory', 'i', 'with', 'g.f085fidRevenueCategory = i.f073fid')
            ->leftjoin('Application\Entity\T059faccountCode', 'ac', 'with','g.f085faccountId = ac.f059fcompleteCode')
            ->where('g.f085fid = :revenueId')
            ->setParameter('revenueId',$id);
            // echo $qb;
            // die();
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $revenueId = (int)$data['f085fidRevenueCategory'];

        $query = "select * from t073fcategory where f073fid = $revenueId"; 
            $revenueResult=$em->getConnection()->executeQuery($query)->fetch();

            // print_r($revenueResult['f073fcode']);exit;

        $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $numberData = array();
        $numberData['type'] = "REVENUESETUP";
        $numberData['revenueId'] = $revenueId;
        $numberData['prefixCode'] = $revenueResult['f073fcode'];
        // print_r($numberData);exit;
        $number = $configRepository->generateFIMS($numberData);

        // print_r($number);exit;
        
        $revenue = new T085frevenueSetUp();

        $revenue->setF085fname($data['f085fname'])
            ->setF085fcode($number)
            ->setF085fstatus((int) $data['f085fstatus'])
            ->setF085faccountId($data['f085faccountId'])
            ->setF085fidRevenueCategory((int) $data['f085fidRevenueCategory'])
            ->setF085fidTaxCode((int) $data['f085fidTaxCode'])
            ->setF085ffund($data['f085ffund'])
            ->setF085fdepartment($data['f085fdepartment'])
            ->setF085factivity($data['f085factivity'])
            ->setF085fcreatedBy((int)$_SESSION['userId'])
            ->setF085fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF085fcreateDtTm(new \DateTime())
            ->setF085fupdateDtTm(new \DateTime());


        try{
            $em->persist($revenue);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $revenue;

}
//         $revenueDetails = $data['revenue-details'];

//         foreach ($revenueDetails as $revenueDetail) {
//         try{

//                 $revenueDetailObj = new T085frevenueSetupDetails();
//                 $revenueDetailObj->setF085fidRevenueSetup((int)$revenue->getF085fid())
//                                 ->setF085fcrFundId((int)$revenueDetail['f085fcrFundId'])
//                                 ->setF085fcrActivityId((int)$revenueDetail['f085fcrActivityId'])
//                                 ->setF085fcrDepartmentId((int)$revenueDetail['f085fcrDepartmentId'])
//                                 ->setF085fcrAccountId((int)$revenueDetail['f085fcrAccountId'])
//                                 ->setF085fdrFundId((int)$revenueDetail['f085fdrFundId'])
//                                 ->setF085fdrActivityId((int)$revenueDetail['f085fdrActivityId'])
//                                 ->setF085fdrDepartmentId((int)$revenueDetail['f085fdrDepartmentId'])
//                                 ->setF085fdrAccountId((int)$revenueDetail['f085fdrAccountId'])
//                                 ->setF085famount((float)$revenueDetail['f085famount'])
//                                 ->setF085fstatus((int)$data['f085fstatus'])
//                                 ->setF085fcreatedBy((int)$_SESSION['userId'])
//                                 ->setF085fupdatedBy((int)$_SESSION['userId']);
 
// $revenueDetailObj->setF085fcreateDtTm(new \DateTime())
//                  ->setF085fupdateDtTm(new \DateTime());
//                  try{
//                 $em->persist($revenueDetailObj);
            
//                 $em->flush();
//                  }
//                  catch(\Exception $e){
//                      echo $e;
//                  } 
//         }
//         catch(\Exception $e){
//             echo $e;
//         }
//     }
    

    public function updateData($revenue, $data = []) 
    {
        $em = $this->getEntityManager();

        $revenue->setF085fname($data['f085fname'])
            ->setF085fstatus((int) $data['f085fstatus'])
            ->setF085faccountId($data['f085faccountId'])
            ->setF085fidRevenueCategory((int) $data['f085fidRevenueCategory'])
            ->setF085fidTaxCode((int) $data['f085fidTaxCode'])
            ->setF085ffund($data['f085ffund'])
            ->setF085fdepartment($data['f085fdepartment'])
            ->setF085factivity($data['f085factivity'])
            ->setF085fcreatedBy((int)$_SESSION['userId'])
            ->setF085fupdatedBy((int)$_SESSION['userId']);

        $revenue->setF085fupdateDtTm(new \DateTime());


        
        $em->persist($revenue);
        $em->flush();

    }

    public function getActiveList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $qb->select(" g.f085fid, g.f085faccountId,g.f085fcode, g.f085fstatus,g.f085fidRevenueCategory, g.f085fname,i.f073fcategoryName as CategoryName, g.f085fidTaxCode, t.f081fcode as TaxCode, (g.f085ffund+'-'+f.f057fname) as Fund, (g.f085fdepartment+'-'+d.f015fdepartmentName) as Department, (g.f085factivity+'-'+at.f058fname) as Activity ")
            ->from('Application\Entity\T085frevenueSetUp', 'g')
            ->leftjoin('Application\Entity\T081ftextCode', 't', 'with', 'g.f085fidTaxCode = t.f081fid')
            ->leftjoin('Application\Entity\T073fcategory', 'i', 'with', 'g.f085fidRevenueCategory = i.f073fid')
            ->leftjoin('Application\Entity\T057ffund', 'f', 'with', 'g.f085ffund = f.f057fcode')
            ->leftjoin('Application\Entity\T058factivityCode', 'at', 'with', 'g.f085factivity = at.f058fcompleteCode')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'g.f085fdepartment = d.f015fdepartmentCode')
            ->orderBy('g.f085fid','DESC')
            ->where('g.f085fstatus=1');

        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    }

   public function getRevenueByCategory($id)
   {
    $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select("g.f085fid, g.f085faccountId,g.f085fcode, g.f085fstatus,g.f085fidRevenueCategory, g.f085fname, g.f085fidTaxCode,g.f085fcode")
            ->from('Application\Entity\T085frevenueSetUp', 'g')
            ->where("g.f085fidRevenueCategory = $id")
            ->orderBy('g.f085fid','DESC');
            // echo $qb;
            // die();
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
   }

}
