<?php
namespace Application\Repository;

use Application\Entity\T095fcreditorSetup;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class CreditorSetupRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('c.f095fid, c.f095fsetupId, d.f011fdefinitionCode as setup, c.f095fdescription,c.f095ftype, c.f095faccountCode, a.f059fname as accountCode ,c.f095fstatus')
            ->from('Application\Entity\T095fcreditorSetup', 'c')
            ->leftjoin('Application\Entity\T059faccountCode','a', 'with', 'c.f095faccountCode = a.f059fcompleteCode')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','c.f095fsetupId = d.f011fid')
            ->orderBy('c.f095fid','DESC');
        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
         $qb->select('c.f095fid, c.f095fsetupId, d.f011fdefinitionCode as setup, c.f095fdescription,c.f095ftype, c.f095faccountCode, a.f059fname as accountCode ,c.f095fstatus')
            ->from('Application\Entity\T095fcreditorSetup', 'c')
            ->leftjoin('Application\Entity\T059faccountCode','a', 'with', 'c.f095faccountCode = a.f059fcompleteCode')
            ->leftjoin('Application\Entity\T011fdefinationms', 'd', 'with','c.f095fsetupId = d.f011fid')
            ->where('c.f095fid = :creditorSetupId')
            ->setParameter('creditorSetupId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $creditor = new T095fcreditorSetup();

        $creditor->setF095fsetupId((int)$data['f095fsetupId'])
                ->setF095fdescription($data['f095fdescription'])
                ->setF095ftype($data['f095ftype'])
                ->setF095faccountCode($data['f095faccountCode'])
                ->setF095fstatus((int)$data['f095fstatus'])
                ->setF095fcreatedBy((int)$_SESSION['userId'])
                ->setF095fupdatedBy((int)$_SESSION['userId']);

        $creditor->setF095fcreatedDtTm(new \DateTime())
                ->setF095fupdatedDtTm(new \DateTime());


    
            $em->persist($creditor);
            $em->flush();
       
        
        return $creditor;
    }

    public function updateData($creditor, $data = []) 
    {
        $em = $this->getEntityManager();

        $creditor->setF095fsetupId((int)$data['f095fsetupId'])
                ->setF095ftype($data['f095ftype'])
                ->setF095fdescription($data['f095fdescription'])
                ->setF095faccountCode($data['f095faccountCode'])
                ->setF095fstatus((int)$data['f095fstatus'])
                ->setF095fupdatedBy((int)$_SESSION['userId']);

        $creditor->setF095fupdatedDtTm(new \DateTime());


        
        $em->persist($creditor);
        $em->flush();

    }

   

}
