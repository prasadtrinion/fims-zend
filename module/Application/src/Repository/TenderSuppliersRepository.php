<?php
namespace Application\Repository;

use Application\Entity\T037ftenderSuppliers;
use Application\Entity\T037ftenderMaster;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TenderSuppliersRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('t.f037fid,t.f037ftenderNumber,t.f037famount,t.f037ftitle,t.f037fperiod,t.f037fstatus')
            ->from('Application\Entity\T037ftenderMaster', 't');
        
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        ;
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('tm.f037fid,tm.f037ftenderNumber,tm.f037famount,tm.f037ftitle,tm.f037fperiod,tm.f037fstatus,t.f037fid as f037fid_details,t.f037fidTender,t.f037fapplied,t.f037fshortlisted,t.f037ffinalised,t.f037fstatus,t.f037fidSupplier,s.f030fcompanyName as supplierName')
            ->from('Application\Entity\T037ftenderMaster', 'tm')
            ->leftjoin('Application\Entity\T037ftenderSuppliers', 't', 'with', 't.f037fidTender = tm.f037fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 't.f037fidSupplier = s.f030fid')

            ->where('tm.f037fid = :tenderId')
            ->setParameter('tenderId',$id);
           
        $query = $qb->getQuery();
        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $tenderMaster = new T037ftenderMaster();

        $tenderMaster->setF037ftenderNumber($data['f037ftenderNumber'])
            ->setF037famount((float)$data['f037famount'])
            ->setF037fstatus((int) $data['f037fstatus'])
            ->setF037ftitle($data['f037ftitle'])
            ->setF037fperiod($data['f037fperiod'])
            ->setF037fcreatedBy((int)$_SESSION['userId'])
            ->setF037fupdatedBy((int)$_SESSION['userId']);

        $tenderMaster->setF037fcreateDtTm(new \DateTime())
            ->setF037fupdateDtTm(new \DateTime());
            try{
                $em->persist($tenderMaster);
                $em->flush();
            }
            catch(\Exception $e){
                echo $e;
            }
            $tenderDetails = $data['tender-details'];
            foreach($tenderDetails as $detail){
            $tender = new T037ftenderSuppliers();

        $tender->setF037fidTender((int)$tenderMaster->getF037fid())
            ->setF037fapplied((int)$detail['f037fapplied'])
            ->setF037fstatus((int) $data['f037fstatus'])
            ->setF037fshortlisted((int) $detail['f037fshortlisted'])
            ->setF037ffinalised((int) $detail['f037ffinalised'])
            ->setF037fidSupplier((int) $detail['f037fidSupplier'])
            ->setF037fcreatedBy((int)$_SESSION['userId'])
            ->setF037fupdatedBy((int)$_SESSION['userId']);

        $tender->setF037fcreateDtTm(new \DateTime())
            ->setF037fupdateDtTm(new \DateTime());


        try
        {
            $em->persist($tender);
            $em->flush();
            
        }
        catch(\Exception $e)
        {
            echo $e;
        }
    }
        
    return $tenderMaster;

    }

    public function updateData($tender, $data = []) 
    {
        $em = $this->getEntityManager();

        $tender->setF037fidTender((int)$data['f037fidTender'])
        ->setF037fapplied((int)$data['f037fapplied'])
        ->setF037fstatus((int) $data['f037fstatus'])
        ->setF037fshortlisted((int) $data['f037fshortlisted'])
        ->setF037ffinalised((int) $data['f037ffinalised'])
        ->setF037fidSupplier((int) $data['f037fidSupplier'])
        ->setF037fupdatedBy((int)$_SESSION['userId']);

    $tender->setF037fupdateDtTm(new \DateTime());


        
        $em->persist($tender);
        $em->flush();

    }

}
