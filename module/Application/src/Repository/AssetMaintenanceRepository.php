<?php
namespace Application\Repository;

use Application\Entity\T089fassetMaintenance;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class AssetMaintenanceRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('a.f089fid, a.f089fidAssets, a.f089fdateOfMaintenance, a.f089fdescription, a.f089fstatus, a.f089fapprovalStatus, ai.f085fassetCode')
            ->from('Application\Entity\T089fassetMaintenance', 'a')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with', 'ai.f085fid = a.f089fidAssets')
            ->orderBy('a.f089fid','DESC');
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f089fdateOfMaintenance'];
            $item['f089fdateOfMaintenance'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a.f089fid, a.f089fidAssets, a.f089fdateOfMaintenance, a.f089fdescription, a.f089fapprovalStatus, a.f089fstatus')
            ->from('Application\Entity\T089fassetMaintenance', 'a')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with', 'ai.f085fid = a.f089fidAssets')
            ->where('a.f089fid =:maintenanceId')
            ->setParameter('maintenanceId', $id);
            
        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                       = $item['f089fdateOfMaintenance'];
            $item['f089fdateOfMaintenance'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }   

        $result = array(
            'data' => $result1,
        );
        return $result;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $assetMaintenance = new T089fassetMaintenance();

        $assetMaintenance->setF089fidAssets((int)$data['f089fidAssets'])
                    ->setF089fdateOfMaintenance(new \DateTime($data['f089fdateOfMaintenance']))
                    ->setF089fdescription($data['f089fdescription'])
                    ->setF089fapprovalStatus((int)$data['f089fapprovalStatus'])
                    ->setF089fstatus((int)$data['f089fstatus'])
                    ->setF089fcreatedBy((int)$_SESSION['userId'])
                    ->setF089fupdatedBy((int)$_SESSION['userId']);

        $assetMaintenance->setF089fcreatedDtTm(new \DateTime())
                    ->setF089fupdatedDtTm(new \DateTime());


        try
        {
            $em->persist($assetMaintenance);
            $em->flush();
        }
        catch(Exception $e)
        {
            echo $e;
        }
        return $assetMaintenance;
    }

    public function updateData($assetMaintenance, $data = []) 
    {
        $em = $this->getEntityManager();

    $assetMaintenance->setF089fidAssets((int)$data['f089fidAssets'])
                    ->setF089fdateOfMaintenance(new \DateTime($data['f089fdateOfMaintenance']))
                    ->setF089fdescription($data['f089fdescription'])
                    ->setF089fapprovalStatus((int)$data['f089fapprovalStatus'])
                    ->setF089fstatus((int)$data['f089fstatus'])
                    ->setF089fcreatedBy((int)$_SESSION['userId'])
                    ->setF089fupdatedBy((int)$_SESSION['userId']);

        $assetMaintenance->setF089fcreatedDtTm(new \DateTime())
                    ->setF089fupdatedDtTm(new \DateTime());



        
        $em->persist($assetMaintenance);
        $em->flush();

    }

    public function getMaintenanceApprovedList($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        // 
        

        if ($approvedStatus == 2 && isset($approvedStatus))
            {
               $qb->select('a.f089fid, a.f089fidAssets, a.f089fdateOfMaintenance, a.f089fdescription, a.f089fstatus, a.f089fapprovalStatus, ai.f085fassetCode')
            ->from('Application\Entity\T089fassetMaintenance', 'a')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with', 'ai.f085fid = a.f089fidAssets')
            ->orderBy('a.f089fid','DESC');
            }

           elseif(isset($approvedStatus))
           {
            // print_r($approvedStatus);exit;
               $qb->select('a.f089fid, a.f089fidAssets, a.f089fdateOfMaintenance, a.f089fdescription, a.f089fstatus, a.f089fapprovalStatus, ai.f085fassetCode')
            ->from('Application\Entity\T089fassetMaintenance', 'a')
            ->leftjoin('Application\Entity\T085fassetInformation', 'ai', 'with', 'ai.f085fid = a.f089fidAssets')
                 ->where('a.f089fapprovalStatus = :maintenanceId')
                 ->setParameter('maintenanceId', $approvedStatus)
                 ->orderBy('a.f089fid','DESC');
            }

            
            
        $data = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        return $data;
        
    }

    public function assetMaintenanceApproval($assets,$reason,$status) 
    {
        $em = $this->getEntityManager();
        $user=(int)$_SESSION['userId'];


        foreach ($assets as $asset) 
        {

            $Id = $asset->getF089fid();
            // print_r($reason);exit;
            $id = (int)$Id;
            if(isset($reason) && isset($status))
            {
            $query = "UPDATE t089fasset_maintenance set f089freason='$reason',f089fapproval_status=$status, f089fupdated_by = $user where f089fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);
            }
        }
    }

    public function assetNotInMaintenance()
    {

        $em = $this->getEntityManager();

        $query = "SELECT *, f085fasset_code as f085fassetCode from t085fasset_information where f085fid not in (select f089fid_assets from t089fasset_maintenance) and f085fstatus = 1";

        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    
}