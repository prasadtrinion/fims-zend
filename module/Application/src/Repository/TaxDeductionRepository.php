<?php
namespace Application\Repository;

use Application\Entity\T082ftaxDeductionMaster;
use Application\Entity\T082ftaxDeductionDetail;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class TaxDeductionRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

         $qb->select('m.f082fid, m.f082fidStaff, m.f082fname, m.f082fstatus, s.f034fname as staffName')

            ->from('Application\Entity\T082ftaxDeductionMaster', 'm')
            ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId= m.f082fidStaff')
            ->orderBy('m.f082fid','DESC');

            
        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('m.f082fid, m.f082fidStaff, m.f082fname, m.f082fstatus, s.f034fname as staffName, md.f082fidDetails, md.f082fidMaster, md.f082fdeductionCode, md.f082famount, md.f082fstartDate, md.f082fendDate, md.f082fcreatedBy, md.f082fcreatedDtTm, md.f082fupdatedBy, md.f082fupdatedDtTm, md.f082fstatus, md.f082ftype')

            ->from('Application\Entity\T082ftaxDeductionMaster', 'm')
            ->leftjoin('Application\Entity\T082ftaxDeductionDetail','md','with', 'm.f082fid= md.f082fidDetails')
            ->leftjoin('Application\Entity\T034fstaffMaster','s','with', 's.f034fstaffId= m.f082fidStaff')

            ->where('m.f082fid = :taxId')
            ->setParameter('taxId',$id);
            
        $query = $qb->getQuery();

        $result  = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        
        $result1 = array();
        foreach ($result as $item) 
        {
            $date = $item['f082fstartDate'];
            $item['f082fstartDate'] = date("Y-m-d", strtotime($date));

            $date = $item['f082fendDate'];
            $item['f082fendDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }   

        return $result1;

    }

    public function createNewData($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $taxDeduction = new T082ftaxDeductionMaster();

        $taxDeduction->setF082fidStaff((int)$data['f082fidStaff'])
             ->setF082fname($data['f082fname'])
             ->setF082fstatus((int)$data['f082fstatus'])
             ->setF082fcreatedBy((int)$_SESSION['userId'])
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $taxDeduction->setF082fcreatedDtTm(new \DateTime())
                ->setF082fupdatedDtTm(new \DateTime());
        try{
        $em->persist($taxDeduction);
        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }
        $taxDeductionDetails = $data['taxDeduction-details'];

        foreach ($taxDeductionDetails as $taxDeductionDetail) {

            $taxDetailObj = new T082ftaxDeductionDetail();

            $taxDetailObj->setF082fidMaster((int)$taxDeduction->getF082fid())
                       ->setF082fdeductionCode((int)$taxDeductionDetail['f082fdeductionCode'])
                       ->setF082famount((float)$taxDeductionDetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($taxDeductionDetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($taxDeductionDetail['f082fendDate']))
                       ->setF082fstatus((int)$taxDeductionDetail['f082fstatus'])
                       ->setF082ftype($taxDeductionDetail['f082ftype'])
                       ->setF082fcreatedBy((int)$_SESSION['userId'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

            $taxDetailObj->setF082fcreatedDtTm(new \DateTime())
                       ->setF082fupdatedDtTm(new \DateTime());
        
       try{
        $em->persist($taxDetailObj);

        $em->flush();
        // print_r($taxDetailObj);exit;
        }
        catch(\Exception $e){
          echo $e;
        }

            
        }
        return $taxDeduction;
    }

    public function updateMaster($taxDeduction, $data = []) 
    {
        $em = $this->getEntityManager();

           $taxDeduction->setF082fidStaff((int)$data['f082fidStaff'])
             ->setF082fname($data['f082fname'])
             ->setF082fstatus((int)$data['f082fstatus'])
             ->setF082fcreatedBy((int)$_SESSION['userId'])
             ->setF082fupdatedBy((int)$_SESSION['userId']);

        $taxDeduction->setF082fcreatedDtTm(new \DateTime())
                ->setF082fupdatedDtTm(new \DateTime());
        try{
        $em->persist($taxDeduction);
        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }

        return $taxDeduction;
 
    }

    public function updateDetail($taxDetailObj, $taxDeductionDetail) 
    {

        $em = $this->getEntityManager();

            $taxDetailObj->setF082fdeductionCode((int)$taxDeductionDetail['f082fdeductionCode'])
                       ->setF082famount($taxDeductionDetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($taxDeductionDetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($taxDeductionDetail['f082fendDate']))
                       ->setF082fstatus((int)$taxDeductionDetail['f082fstatus'])
                       ->setF082ftype($taxDeductionDetail['f082ftype'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

            $taxDetailObj->setF082fupdatedDtTm(new \DateTime());
        
       try{
        $em->persist($taxDetailObj);

        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }
        return $taxDetailObj;

    }

    public function createDetail($taxDetailObj, $taxDeductionDetail) 
    {
        $em = $this->getEntityManager();

        $taxObj = new T082ftaxDeductionDetail();

            $taxObj->setF082fidMaster((int)$taxDetailObj->getF082fid())
                       ->setF082fdeductionCode((int)$taxDeductionDetail['f082fdeductionCode'])
                       ->setF082famount($taxDeductionDetail['f082famount'])
                       ->setF082fstartDate(new \DateTime($taxDeductionDetail['f082fstartDate']))
                       ->setF082fendDate(new \DateTime($taxDeductionDetail['f082fendDate']))
                       ->setF082fstatus((int)$taxDeductionDetail['f082fstatus'])
                       ->setF082ftype($taxDeductionDetail['f082ftype'])
                       ->setF082fcreatedBy((int)$_SESSION['userId'])
                       ->setF082fupdatedBy((int)$_SESSION['userId']);

            $taxObj->setF082fcreatedDtTm(new \DateTime())
                       ->setF082fupdatedDtTm(new \DateTime());
        
       try{
        $em->persist($taxObj);

        $em->flush();
        }
        catch(\Exception $e){
          echo $e;
        }return $taxObj;

    }



}
