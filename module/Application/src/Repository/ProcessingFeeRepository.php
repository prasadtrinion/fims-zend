<?php
namespace Application\Repository;

use Application\Entity\T021fprocessingFee;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class ProcessingFeeRepository extends EntityRepository 
{
    /* to retrive the data from database*/

    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('p.f021fid, p.f021fidLoanName,p.f021feffectiveDate,p.f021famount, p.f021fstatus, p.f021fcreatedBy, p.f021fupdatedBy,l.f020floanName as loanName, p.f021ffund,p.f021fdepartment,p.f021factivity,p.f021faccount')
            ->from('Application\Entity\T021fprocessingFee','p')
            ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'p.f021fidLoanName = l.f020fid')
            ->orderBy('p.f021fid','DESC');

        $query = $qb->getQuery();

        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $fee) {
            $date                        = $fee['f021feffectiveDate'];
            $fee['f021feffectiveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $fee);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }

    public function loanNameNotInProcessingFee() 
    {
        $em = $this->getEntityManager();
         $query = "select f020floan_name as f020floanName,f020fid from t020floan_name where f020fid not in (select f021fid_loan_name from t021fprocessing_fee)"; 
                $result=$em->getConnection()->executeQuery($query)->fetchAll();
                return $result;
    }

     public function loanNameNotInLoanGrade() 
    {
        $em = $this->getEntityManager();
         $query = "select f020floan_name as f020floanName,f020fid,f020ftype_of_loan,f011fdefinition_code as f020ftypeOfLoan from t020floan_name inner join t011fdefinationms on f011fid = f020ftype_of_loan where f020fid not in (select f046floan_type from t046fgrade_setup)"; 
                $result=$em->getConnection()->executeQuery($query)->fetchAll();
                return $result;
    }

     public function loanNameNotInLoanParameters() 
    {
        $em = $this->getEntityManager();
         $query = "select f020floan_name as f020floanName,f020fid from t020floan_name where f020fid not in (select f044fid_loan from t044floan_type_setup)"; 
                $result=$em->getConnection()->executeQuery($query)->fetchAll();
                return $result;
    }
    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('p.f021fid, p.f021fidLoanName,p.f021feffectiveDate,p.f021famount, p.f021fstatus, p.f021fcreatedBy, p.f021fupdatedBy,l.f020floanName as loanName, p.f021ffund,p.f021fdepartment,p.f021factivity,p.f021faccount')
            ->from('Application\Entity\T021fprocessingFee','p')
            ->leftjoin('Application\Entity\T020floanName', 'l', 'with', 'p.f021fidLoanName = l.f020fid')
            ->where('p.f021fid = :feeId')
            ->setParameter('feeId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $fee) {
            $date                        = $fee['f021feffectiveDate'];
            $fee['f021feffectiveDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $fee);
        }
        return $result1;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $fee = new T021fprocessingFee();

        $fee->setF021fidLoanName((int)$data['f021fidLoanName'])
             ->setF021feffectiveDate(new \DateTime($data['f021feffectiveDate']))
             ->setF021famount((float)$data['f021famount'])
             ->setF021ffund($data['f021ffund'])
             ->setF021fdepartment($data['f021fdepartment'])
             ->setF021factivity($data['f021factivity'])
             ->setF021faccount($data['f021faccount'])
             ->setF021fstatus((int)$data['f021fstatus'])
             ->setF021fcreatedBy((int)$_SESSION['userId'])
             ->setF021fupdatedBy((int)$_SESSION['userId']);

        $fee->setF021fcreatedDtTm(new \DateTime())
                ->setF021fupdatedDtTm(new \DateTime());

 try{
        $em->persist($fee);
        $em->flush();
        // print_r($fee);
        // die();
        
}
catch(\Exception $e){
    echo $e;
}
        return $loan;

    }

    /* to edit the data in database*/

    public function updateData($fee, $data = []) 
    {
        $em = $this->getEntityManager();

        $fee->setF021fidLoanName((int)$data['f021fidLoanName'])
             ->setF021feffectiveDate(new \DateTime($data['f021feffectiveDate']))
             ->setF021ffund($data['f021ffund'])
             ->setF021fdepartment($data['f021fdepartment'])
             ->setF021factivity($data['f021factivity'])
             ->setF021faccount($data['f021faccount'])
             ->setF021famount((float)$data['f021famount'])
             ->setF021fstatus((int)$data['f021fstatus'])
             ->setF021fupdatedBy((int)$_SESSION['userId']);

        $fee->setF021fupdatedDtTm(new \DateTime());
try{
        $em->persist($fee);
        $em->flush();
}
catch(\Exception $e){
    echo $e;
}
    }

    /* to update the data in database*/
    /**
     * 
     *
     * @param object $entity
     */
    
    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}

?>
