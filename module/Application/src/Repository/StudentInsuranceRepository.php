<?php
namespace Application\Repository;

use Application\Entity\T099fstudentInsurance;
use Application\Entity\T011fdefinationms;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class StudentInsuranceRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('s.f099fid, s.f099fname, s.f099finsCode, s.f099fcontactPerson, s.f099fphone, s.f099femail, s.f099ffax, s.f099fendDate , s.f099faddress, s.f099fcity, s.f099fstate, st.f012fstateName, s.f099fcountry, c.f013fcountryName, s.f099fstatus')
            ->from('Application\Entity\T099fstudentInsurance','s')
            ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f099fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with','s.f099fcountry = c.f013fid')
            ->orderBy('s.f099fid','DESC');
            
        $query = $qb->getQuery();

         $result =  $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f099fendDate'];
            $item['f099fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;

    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

         $qb->select('s.f099fid, s.f099fname, s.f099finsCode, s.f099fcontactPerson, s.f099fphone, s.f099femail, s.f099ffax, s.f099fendDate , s.f099faddress, s.f099fcity, s.f099fstate, st.f012fstateName, s.f099fcountry, c.f013fcountryName, s.f099fstatus, st.f012fid')
            ->from('Application\Entity\T099fstudentInsurance','s')
            ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f099fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with','s.f099fcountry = c.f013fid')
            ->where('s.f099fid = :insuranceId')
            ->setParameter('insuranceId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        $result1 = array();
        foreach ($result as $item) 
        {
            $date                        = $item['f099fendDate'];
            $item['f099fendDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        
        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $student = new T099fstudentInsurance();

        $student->setF099fname($data['f099fname'])
                ->setF099finsCode($data['f099finsCode'])
                ->setF099fcontactPerson($data['f099fcontactPerson'])
                ->setF099fphone($data['f099fphone'])
                ->setF099femail($data['f099femail'])
                ->setF099ffax($data['f099ffax'])
                ->setF099faddress($data['f099faddress'])
                ->setF099fcity($data['f099fcity'])
                ->setF099fstate((int)$data['f099fstate'])
                ->setF099fcountry((int)$data['f099fcountry'])
                ->setF099fendDate(new \DateTime($data['f099fendDate']))
                ->setf099fstatus((int)($data['f099fstatus']))
                ->setF099fcreatedBy((int)$_SESSION['userId'])
                ->setF099fupdatedBy((int)$_SESSION['userId']);

        $student->setF099fcreatedDtTm(new \DateTime())
                ->setF099fupdatedDtTm(new \DateTime());


        try{
            $em->persist($student);
            $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $student;
    }


    public function updateData($student, $data = []) 
    {
        $em = $this->getEntityManager();

        $student->setF099fname($data['f099fname'])
                ->setF099finsCode($data['f099finsCode'])
                ->setF099fcontactPerson($data['f099fcontactPerson'])
                ->setF099fphone($data['f099fphone'])
                ->setF099femail($data['f099femail'])
                ->setF099ffax($data['f099ffax'])
                ->setF099faddress($data['f099faddress'])
                ->setF099fcity($data['f099fcity'])
                ->setF099fstate((int)$data['f099fstate'])
                ->setF099fcountry((int)$data['f099fcountry'])
                ->setF099fendDate(new \DateTime($data['f099fendDate']))
                ->setf099fstatus((int)($data['f099fstatus']))
                ->setF099fupdatedBy((int)$_SESSION['userId']);

        $student->setF099fupdatedDtTm(new \DateTime());


        
        $em->persist($student);
        $em->flush();

    }

    public function getActiveList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('s.f099fid, s.f099fname, s.f099finsCode, s.f099fcontactPerson, s.f099fphone, s.f099femail, s.f099ffax, s.f099fendDate , s.f099faddress, s.f099fcity, s.f099fstate, st.f012fstateName, s.f099fcountry, c.f013fcountryName, s.f099fstatus')
            ->from('Application\Entity\T099fstudentInsurance','s')
            ->leftjoin('Application\Entity\T012fstate', 'st','with','s.f099fstate = st.f012fid')
            ->leftjoin('Application\Entity\T013fcountry','c','with','s.f099fcountry = c.f013fid')
            ->where('s.f099fstatus = 1')
            ->orderBy('s.f099fid','DESC');
            
         $query = $qb->getQuery();

        $result = array(
            'data' => $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR),
        );

        return $result;

    
    }

}
