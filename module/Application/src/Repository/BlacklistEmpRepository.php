<?php
namespace Application\Repository;

use Application\Entity\T018fblacklistEmp;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class BlacklistEmpRepository extends EntityRepository 
{

    public function getList() 
    {
        

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        $currentDate = date('Y-m-d H:m:s');
        $qb->select('bl.f018fid, bl.f018fidStaff, sm.f034fname as staffName, bl.f018feffectiveDate, bl.f018freason, bl.f018fstatus,  bl.f018fendDate, bl.f018freleaseDate')
            ->from('Application\Entity\T018fblacklistEmp', 'bl')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with','sm.f034fstaffId = bl.f018fidStaff')
            ->orderBy('bl.f018fid','DESC');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        
        $result1 = array();
        foreach ($result as $blackList) {
            $effective_date                        = $blackList['f018feffectiveDate'];
            $blackList['f018feffectiveDate'] = date("Y-m-d", strtotime($effective_date));
            $end_date                        = $blackList['f018fendDate'];
            $blackList['f018fendDate'] = date("Y-m-d", strtotime($end_date));
            $release_date                        = $blackList['f018freleaseDate'];
            $blackList['f018freleaseDate'] = date("Y-m-d", strtotime($release_date));
            if($currentDate >= $effective_date && ($currentDate <= $release_date || $release_date == '')){
            $blackList['f018fstatus'] = 1; 
        }
        else{
            $blackList['f018fstatus'] = 0; 
        }
            array_push($result1, $blackList);
	    
        }

        $result = array(
            'data' => $result1,
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('bl.f018fid, bl.f018fidStaff, sm.f034fname as staffName, bl.f018feffectiveDate, bl.f018freason, bl.f018fstatus, sm.f034fname, bl.f018fendDate,bl.f018freleaseDate')
            ->from('Application\Entity\T018fblacklistEmp', 'bl')
            ->leftjoin('Application\Entity\T034fstaffMaster', 'sm', 'with','sm.f034fstaffId = bl.f018fidStaff')
            ->where('bl.f018fid = :blacklistId')
            ->setParameter('blacklistId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        $result1 = array();
        foreach ($result as $blackList) {
            $date                        = $blackList['f018feffectiveDate'];
            $blackList['f018feffectiveDate'] = date("Y-m-d", strtotime($date));
            $date                        = $blackList['f018fendDate'];
            $blackList['f018fendDate'] = date("Y-m-d", strtotime($date));
            $date                        = $blackList['f018freleaseDate'];
            $blackList['f018freleaseDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $blackList);
        }

        return $result1;
    }

    public function createNewData($data) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $release_date = NULL;
        if($data['f018freleaseDate'] != ''){
        $release_date = new \DateTime($data['f018freleaseDate']);
        }
        $blacklist = new T018fblacklistEmp();


        $blacklist->setF018fidStaff((int)$data['f018fidStaff'])
                ->setF018feffectiveDate(new \DateTime($data['f018feffectiveDate']))
                ->setF018freleaseDate($release_date)
                ->setF018fendDate(new \DateTime($data['f018fendDate']))
                ->setF018freason($data['f018freason'])
                ->setF018fstatus((int)$data['f018fstatus'])
                ->setF018fcreatedBy((int)$_SESSION['userId'])
                ->setF018fupdatedBy((int)$_SESSION['userId']);
 
        
        $blacklist->setF018fcreatedDtTm(new \DateTime())
                ->setF018fupdatedDtTm(new \DateTime());


        try
        {
            $em->persist($blacklist);
            $em->flush();
            // print_r($blacklist);
            // die();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return $blacklist;
    }

    public function updateData($blacklist, $data = []) 
    {
        $em = $this->getEntityManager();
        $release_date = NULL;
        if($data['f018freleaseDate'] != ''){
        $release_date = new \DateTime($data['f018freleaseDate']);
        }
        $blacklist->setF018fidStaff((int)$data['f018fidStaff'])
                ->setF018feffectiveDate(new \DateTime($data['f018feffectiveDate']))
                ->setF018freleaseDate($release_date)
                ->setF018fendDate(new \DateTime($data['f018fendDate']))
                ->setF018freason($data['f018freason'])
                ->setF018fstatus((int)$data['f018fstatus'])
                ->setF018fcreatedBy((int)$_SESSION['userId'])
                ->setF018fupdatedBy((int)$_SESSION['userId']);

        $blacklist->setF018fcreatedDtTm(new \DateTime())
                ->setF018fupdatedDtTm(new \DateTime());



        
        $em->persist($blacklist);
        $em->flush();

    }

    
}
