<?php
namespace Application\Repository;

use Application\Entity\T034fgrn;
use Application\Entity\T034fgrnDetails;
use Application\Entity\T091fbillRegistration;
use Application\Entity\T091fbillRegistrationDetails;
use Application\Entity\T063fsponsorDn;
use Application\Entity\T064fsponsorDnDetails;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\View\Model\JsonModel;

class GrnRepository extends EntityRepository 
{

    public function getList() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f034fid, p.f034forderType,  p.f034forderDate, p.f034fidFinancialyear, f.f015fname as budgetYear, p.f034fidSupplier, s.f030fcompanyName as vendorName, s.f030fvendorCode as vendorCode, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, pr.f034freferenceNumber as purchaseNumber, p.f034fcreatedBy, u.f014fuserName, p.f034fcreatedDtTm, p.f034frating')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'u.f014fid = p.f034fcreatedBy')
            ->orderBy('p.f034fid','DESC');



        $query = $qb->getQuery();
	$result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	$result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
	    // $date                        = $item['f034fexpiredDate'];
     //        $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
    
    }

    public function donorGrns() 
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select('p.f034fid, p.f034forderType,  p.f034forderDate, p.f034fidFinancialyear, f.f015fname as budgetYear, p.f034fidSupplier, s.f030fcompanyName as vendor, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034fapprovalStatus, p.f034ftotalAmount,p.f034fapprovedDate,p.f034fapprovedRefNum,p.f034fdonor, p.f034frating')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where("p.f034forderType = 'DONOR'")
            ->orderBy('p.f034fid','DESC');



        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    // $result1 = array();
 //        foreach ($result as $item) {
 //            $date                        = $item['f034forderDate'];
 //            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
    //     $date                        = $item['f034fexpiredDate'];
 //            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));

 //            array_push($result1, $item);
 //        }
        $result = array(
            'data' => $result
        );
        return $result;
    
    }

    public function getListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount,p.f034fapprovedDate,p.f034fapprovedRefNum,p.f034fdonor, pd.f034fidDetails, pd.f034fidItem,pd.f034fitemType, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, pd.f034fitemDescription, pd.f034forderLineNo, pd.f034fquantityReceived, pd.f034fapprovedStatus, p.f034frating, pd.f034ftypeOfItem, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity, pd.f034fitemProcurementType, pr.f034freferenceNumber as POReferenceNumber')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fgrnDetails', 'pd', 'with', 'p.f034fid = pd.f034fidGrn')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where('p.f034fid = :purchaseId')
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
	$result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fapprovedDate'];
            $item['f034fapprovedDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }

         $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];


            $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
                $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];

            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

              

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
        
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        // $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        // $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;

        }

        // print_r($result1);exit;
        return $result1;
        // return $result1;
        return $result1;
    }

    public function awaitingGrns() 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('pd.f034fidDetails, pd.f034fidItem,pd.f034fitemType, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode,  pd.f034fitemDescription, pd.f034forderLineNo, pd.f034fquantityReceived, pd.f034fapprovedStatus,  pd.f034ftypeOfItem, pd.f034fbalanceQuantity, pd.f034fitemProcurementType')
            ->from('Application\Entity\T034fgrnDetails', 'pd')
            ->where('pd.f034fapprovedStatus = 4');
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    // $result1 = array();
    //     foreach ($result as $item) {
    //         $item['f034fidItem'] = $item['f034fidItem'].$item['f034fitemType'];
    //         $date                        = $item['f034forderDate'];
    //         $item['f034forderDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034fapprovedDate'];
    //         $item['f034fapprovedDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034fexpiredDate'];
    //         $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
    //         $date                        = $item['f034frequiredDate'];
    //         $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
    //         array_push($result1, $item);
    //     }
        return $result;
    }

    public function createGrn($data)
    {
// print_r($data);
// die();
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $data1=array();
        $data1['type']='GR';
        $initialRepository = $em->getRepository("Application\Entity\T011finitialConfig");
        $generateNumber = $initialRepository->generateFIMS($data1);
        $data['f034freferenceNumber'] = $generateNumber;

        $grn = new T034fgrn();

        $grn->setF034forderType($data['f034forderType'])
            ->setF034forderDate(new \DateTime($data['f034forderDate']))
            ->setF034fidFinancialyear((int)$data['f034fidFinancialyear'])
            ->setF034fidSupplier((int)$data['f034fidSupplier'])
            ->setF034fdescription($data['f034fdescription'])
            ->setF034freferenceNumber($data['f034freferenceNumber'])
            ->setF034fidDepartment($data['f034fidDepartment'])
            ->setF034fidPurchaseOrder((int)$data['f034fidPurchaseOrder'])
            ->setF034fexpiredDate(new \DateTime($data['f034fexpiredDate']))
            ->setF034ftotalAmount((float)$data['f034ftotalAmount'])
            ->setF034fapprovedDate(new \DateTime($data['f034fapprovedDate']))
            ->setF034fapprovedRefNum($data['f034fapprovedRefNum'])
            ->setF034fdonor($data['f034fdonor'])
            ->setF034frating($data['f034frating'])
            ->setF034fapprovalStatus((int)0)
            ->setF034fbillGenerated((int)0)
            ->setF034fstatus((int)$data['f034fstatus'])
            ->setF034fcreatedBy((int)$_SESSION['userId'])
            ->setF034fupdatedBy((int)$_SESSION['userId']);

        $grn->setF034fcreatedDtTm(new \DateTime())
            ->setF034fupdatedDtTm(new \DateTime());

   	    try{     
            $em->persist($grn);
            $em->flush();
            
            // print_r($grn);
            // die();
	}
	catch(\Exception $e){
	echo $e;
	}

    $idPO = (int)$data['f034fidPurchaseOrder'];

 


        

        $grnDetails = $data['grn-details'];

        foreach ($grnDetails as $grnDetail)
        {

            if ($grnDetail['f034fitemType'] == 'A')
            {
                $idSubCat = (int)$grnDetail['f034fitemSubCategory'];
                 $query = "select itsm.f028fsub_category_name from t028fitem_sub_category as itsm inner join t089fasset_sub_categories as sc on sc.f089fid_item_group = itsm.f028fid where sc.f089fid = $idSubCat";
                // exit;

                 $result=$em->getConnection()->executeQuery($query)->fetch();
                 // print_r($result);exit;

                 $procurementSubType = $result['f028fsub_category_name'];

                 switch ($procurementSubType)
                 {
                    case 'Stock':
                        $grnDetail['f034fitemProcurementType'] = 'Stock';
                        break;
                     
                    case 'Inventory':
                        $grnDetail['f034fitemProcurementType'] = 'Inventory';
                        break;

                    case 'Asset':
                        $grnDetail['f034fitemProcurementType'] = 'Asset';
                        break;
                    default: $grnDetail['f034fitemProcurementType'] = '0';
                 }
            }
// print_r($grnDetail['f034fitemProcurementType']);exit;
            $grnDetailObj = new T034fgrnDetails();

            $grnDetailObj->setF034fidGrn((int)$grn->getF034fid())
                ->setF034fidItem((int) substr($grnDetail['f034fidItem'], 0,-1))
                ->setF034fitemType( substr($grnDetail['f034fidItem'],-1))
                ->setF034ftype($grnDetail['f034ftype'])
                ->setF034fsoCode($grnDetail['f034fsoCode'])
                ->setF034frequiredDate(new \DateTime($grnDetail['f034frequiredDate']))
                ->setF034fquantity((int)$grnDetail['f034fquantity'])
                ->setF034fquantityReceived((int)$grnDetail['f034fquantityReceived'])
                ->setF034funit($grnDetail['f034funit'])
                ->setF034fprice((float)$grnDetail['f034fprice'])
                ->setF034total((float)$grnDetail['f034total'])
                ->setF034fpercentage((float) $grnDetail['f034fpercentage'])
		        ->setF034ftaxCode((int)$grnDetail['f034ftaxCode'])
           	    ->setF034fstatus((int)$grnDetail['f034fstatus'])
	            ->setF034ftaxAmount((float) $grnDetail['f034ftaxAmount'])
                ->setF034ftotalIncTax((float) $grnDetail['f034ftotalIncTax'])
                ->setF034ffundCode($grnDetail['f034ffundCode'])
                ->setF034fdepartmentCode($grnDetail['f034fdepartmentCode'])
                ->setF034factivityCode($grnDetail['f034factivityCode'])
                ->setF034faccountCode($grnDetail['f034faccountCode'])
                ->setF034fbudgetFundCode($grnDetail['f034fbudgetFundCode'])
                ->setF034fbudgetDepartmentCode($grnDetail['f034fbudgetDepartmentCode'])
                ->setF034fbudgetActivityCode($grnDetail['f034fbudgetActivityCode'])
                ->setF034fbudgetAccountCode($grnDetail['f034fbudgetAccountCode'])
                ->setF034fitemDescription($grnDetail['f034fitemDescription'])
                ->setF034ftypeOfItem($grnDetail['f034ftypeOfItem'])
                ->setF034forderLineNo((int)$grnDetail['f034forderLineNo'])
                ->setF034fitemCategory((int)$grnDetail['f034fitemCategory'])
                ->setF034fitemSubCategory((int)$grnDetail['f034fitemSubCategory'])
                ->setF034fassetCode($grnDetail['f034fassetCode'])
                ->setF034fbalanceQuantity((int)$grnDetail['f034fbalanceQuantity'])
                ->setF034fitemProcurementType($grnDetail['f034fitemProcurementType'])
                ->setF034fapprovedStatus((int)0)
                ->setF034fcreatedBy((int)$_SESSION['userId'])
                ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $grnDetailObj->setF034fcreatedDtTm(new \DateTime())
                ->setF034fupdatedDtTm(new \DateTime());
         try{   
                $em->persist($grnDetailObj);
		
                $em->flush();
            }
            catch(\Exception $e)
            {
	           echo $e;
	        }

            $idItem = (int)$grnDetail['f034fitemCategory'];

               $query = "select f034fid_details from t034fpurchase_order_details where f034fid_purchase_order = $idPO
                and f034fid_item= $idItem"; 
    $result=$em->getConnection()->executeQuery($query)->fetch();

    $f034fidDetails = $result['f034fid_details'];


            $remaining = (int)$grnDetail['f034fbalanceQuantity'];
            // $fund =  $grnDetail['f034ffundCode'];
            // $dept = $grnDetail['f034fdepartmentCode'];
            // $activity = $grnDetail['f034factivityCode'];
            // $account = $grnDetail['f034faccountCode'];

               $query = "UPDATE t034fpurchase_order_details set f034fbalance_quantity= $remaining where f034fid_details = $f034fidDetails";
             $result=$em->getConnection()->executeQuery($query);
            
        }


           
        return $grn;
    }

    public function updateGrn($grn, $data = [])
    {
        $em = $this->getEntityManager();

        $grn->setF034forderType($data['f034forderType'])
        ->setF034forderDate(new \DateTime($data['f034forderDate']))
            ->setF034fidFinancialyear((int)$data['f034fidFinancialyear'])
        ->setF034fidSupplier((int)$data['f034fidSupplier'])
        ->setF034fdescription($data['f034fdescription'])
        ->setF034freferenceNumber($data['f034freferenceNumber'])
        ->setF034fidDepartment($data['f034fidDepartment'])
        ->setF034fidPurchaseOrder((int)$data['f034fidPurchaseOrder'])
        ->setF034fexpiredDate(new \DateTime($data['f034fexpiredDate']))
        ->setF034ftotalAmount($data['f034ftotalAmount'])
        ->setF034fapprovedDate(new \DateTime($data['f034fapprovedDate']))
        ->setF034frating($data['f034frating'])
        ->setF034fbillGenerated((int)0)
            // ->setF034fapprovedRef($data['f034fapprovedRefNum'])
            ->setF034fdonor($data['f034fdonor'])
        
        ->setF034fstatus((int)$data['f034fstatus'])
        ->setF034fupdatedBy((int)$_SESSION['userId']);

        $grn->setF034fupdatedDtTm(new \DateTime());
            


        try {
            $em->persist($grn);

            $em->flush();
        } catch (\Exception $e) {
            echo $e;
        }
        return $grn;
    }


    public function updateDetail($grnDetailObj, $grnDetail)
    {
        
        $em = $this->getEntityManager();
        
        $grnDetailObj ->setF034fidItem((int) substr($grnDetail['f034fidItem'], 0,-1))
                ->setF034fitemType( substr($grnDetail['f034fidItem'],-1))
        ->setF034ftype($grnDetail['f034ftype'])
        ->setF034fsoCode($grnDetail['f034fsoCode'])
        ->setF034frequiredDate($grnDetail['f034frequiredDate'])
        ->setF034fquantity((int)$grnDetail['f034fquantity'])
        ->setF034fquantityReceived((int)$grnDetail['f034fquantityReceived'])
        ->setF034funit($grnDetail['f034funit'])
        ->setF034fprice((float)$grnDetail['f034fprice'])
        ->setF034total((float)$grnDetail['f034total'])
        ->setF034fpercentage((int) $grnDetail['f034fpercentage'])
        ->setF034ftaxCode((int)$grnDetail['f034ftaxCode'])
        ->setF034fstatus((int)$grnDetail['f034fstatus'])
        ->setF034ftaxAmount((float) $grnDetail['f034ftaxAmount'])
        ->setF034ftotalIncTax((float) $grnDetail['f034ftotalIncTax'])
        ->setF034ffundCode($grnDetail['f034ffundCode'])
        ->setF034fdepartmentCode($grnDetail['f034fdepartmentCode'])
        ->setF034factivityCode($grnDetail['f034factivityCode'])
        ->setF034faccountCode($grnDetail['f034faccountCode'])
        ->setF034fbudgetFundCode($grnDetail['f034fbudgetFundCode'])
        ->setF034fbudgetDepartmentCode($grnDetail['f034fbudgetDepartmentCode'])
        ->setF034fbudgetActivityCode($grnDetail['f034fbudgetActivityCode'])
        ->setF034fbudgetAccountCode($grnDetail['f034fbudgetAccountCode'])
        ->setF034fitemDescription($grnDetail['f034fitemDescription'])
        ->setF034ftypeOfItem($grnDetail['f034ftypeOfItem'])
        ->setF034fitemCategory((int)$grnDetail['f034fitemCategory'])
        ->setF034fitemSubCategory((int)$grnDetail['f034fitemSubCategory'])
        ->setF034fassetCode($grnDetail['f034fassetCode'])
        ->setF034fbalanceQuantity((int)$grnDetail['f034fbalanceQuantity'])
        // ->setF034fitemProcurementType($grnDetail['f034fitemProcurementType'])
        ->setF034forderLineNo((int)$grnDetail['f034forderLineNo'])
        ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $grnDetailObj->setF034fupdatedDtTm(new \DateTime());
             try
        {
            $em->persist($grnDetailObj);
            
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }
    
    public function createDetail($grnObj, $grnDetail)
    {
        
        $em = $this->getEntityManager();

        

       $grnDetailObj = new T034fgrnDetails();

       $grnDetailObj->setF034fidGrn((int)$grnObj->getF034fid())
        ->setF034fidItem((int) substr($grnDetail['f034fidItem'], 0,-1))
                ->setF034fitemType( substr($grnDetail['f034fidItem'],-1))
       ->setF034ftype($grnDetail['f034ftype'])
       ->setF034fsoCode($grnDetail['f034fsoCode'])
       ->setF034frequiredDate($grnDetail['f034frequiredDate'])
       ->setF034fquantity((int)$grnDetail['f034fquantity'])
       ->setF034fquantityReceived((int)$grnDetail['f034fquantityReceived'])
       ->setF034funit($grnDetail['f034funit'])
       ->setF034fprice((float)$grnDetail['f034fprice'])
       ->setF034total((float)$grnDetail['f034total'])
       ->setF034fpercentage((int) $grnDetail['f034fpercentage'])
       ->setF034ftaxCode((int)$grnDetail['f034ftaxCode'])
       ->setF034fstatus((int)$grnDetail['f034fstatus'])
       ->setF034ftaxAmount((float) $grnDetail['f034ftaxAmount'])
       ->setF034ftotalIncTax((float) $grnDetail['f034ftotalIncTax'])
       ->setF034ffundCode($grnDetail['f034ffundCode'])
       ->setF034fdepartmentCode($grnDetail['f034fdepartmentCode'])
       ->setF034factivityCode($grnDetail['f034factivityCode'])
       ->setF034faccountCode($grnDetail['f034faccountCode'])
       ->setF034fbudgetFundCode($grnDetail['f034fbudgetFundCode'])
       ->setF034fbudgetDepartmentCode($grnDetail['f034fbudgetDepartmentCode'])
       ->setF034fbudgetActivityCode($grnDetail['f034fbudgetActivityCode'])
       ->setF034fbudgetAccountCode($grnDetail['f034fbudgetAccountCode'])
       ->setF034fapprovedStatus((int)0)
       ->setF034fitemDescription($grnDetail['f034fitemDescription'])
       ->setF034ftypeOfItem($grnDetail['f034ftypeOfItem'])
        ->setF034fitemCategory((int)$grnDetail['f034fitemCategory'])
        ->setF034fitemSubCategory((int)$grnDetail['f034fitemSubCategory'])
        ->setF034fassetCode($grnDetail['f034fassetCode'])
       ->setF034forderLineNo((int)$grnDetail['f034forderLineNo'])
        ->setF034fitemProcurementType($grnDetail['f034fitemProcurementType'])
        ->setF034fbalanceQuantity((int)$grnDetail['f034fbalanceQuantity'])
       ->setF034fcreatedBy((int)$_SESSION['userId'])
       ->setF034fupdatedBy((int)$_SESSION['userId']);
                

            $grnDetailObj->setF034fcreatedDtTm(new \DateTime())
                ->setF034fupdatedDtTm(new \DateTime());
        try
        {
            $em->persist($grnDetailObj);
            
            $em->flush();
        } 
        catch (\Exception $e) 
        {
            echo $e;
        }
    }

    public function getListByApprovedStatus($approvedStatus)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();
        
        // echo("hi");
        // exit;
           if($approvedStatus == 0)
           {
               $qb->select('p.f034fid, p.f034forderType,  p.f034forderDate, p.f034fidFinancialyear, f.f015fname as budgetYear, p.f034fidSupplier, s.f030fcompanyName as vendorName, s.f030fvendorCode as vendorCode, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName,  p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, pr.f034freferenceNumber as purchaseNumber, u.f014fuserName as updatedBy, p.f034fupdatedDtTm, p.f034frating, p.f034fcreatedDtTm, u1.f014fuserName as createdBy')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with', 'p.f034fcreatedBy = u1.f014fid')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
                 ->where('p.f034fapprovalStatus = 0');
                 // ->groupBy('bc.f054fidDepartment,bc.f054fidFinancialyear');
            }

            elseif($approvedStatus == 1)
            {
               $qb->select('p.f034fid, p.f034forderType,  p.f034forderDate, p.f034fidFinancialyear, f.f015fname as budgetYear, p.f034fidSupplier, s.f030fcompanyName as vendorName, s.f030fvendorCode as vendorCode, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName,  p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, pr.f034freferenceNumber as purchaseNumber, u.f014fuserName as updatedBy, p.f034fupdatedDtTm, p.f034frating, p.f034fcreatedDtTm, u1.f014fuserName as createdBy')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with', 'p.f034fcreatedBy = u1.f014fid')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
                ->where('p.f034fapprovalStatus = 1');
                // ->groupBy('bc.f054fidDepartment,bc.f054fidFinancialyear');
            }

            elseif($approvedStatus == 2) 
            {
              $qb->select('p.f034fid, p.f034forderType,  p.f034forderDate, p.f034fidFinancialyear, f.f015fname as budgetYear, p.f034fidSupplier, s.f030fcompanyName as vendorName, s.f030fvendorCode as vendorCode, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName,  p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, pr.f034freferenceNumber as purchaseNumber, u.f014fuserName as updatedBy, p.f034fupdatedDtTm, p.f034frating, p.f034fcreatedDtTm, u1.f014fuserName as createdBy')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T014fuser', 'u', 'with', 'p.f034fupdatedBy = u.f014fid')
            ->leftjoin('Application\Entity\T014fuser', 'u1', 'with', 'p.f034fcreatedBy = u1.f014fid')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid');
                // ->groupBy('bc.f054fidDepartment,bc.f054fidFinancialyear');
                
            }
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
   
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));

            array_push($result1, $item);
        }
        $result = array(
            'data' => $result1
        );
        return $result;
        
    }
 

    public function updateGrnDetailApprovalStatus($grns,$journalRepository) 
    {
        $em = $this->getEntityManager();

        $reason=$grns['reason'];
        $status=(int)$grns['status'];
        $ids=$grns['id'];
       
        foreach ($ids as $Id) 
        {
        // print_r($Id);exit;
            
            $id=(int)$Id;

            $query = "UPDATE t034fgrn set f034fapproval_status = $status where f034fid = $id"; 
            $result=$em->getConnection()->executeQuery($query);

            // $query1 = "UPDATE t034fgrn set f034freason='$reason', f034fapproval_status=$status where f034fid = $id"; 
            // $result=$em->getConnection()->executeQuery($query1);


            // print_r($result);exit;

            $query = "select f034fitem_type,f034ftotal_inc_tax from  t034fgrn_details where f034fid_details = $id"; 
            $type_result=$em->getConnection()->executeQuery($query)->fetch();
            $item_type = $type_result['f034fitem_type'];
            $total_inc_tax = $type_result['f034ftotal_inc_tax'];
            if($item_type == 'A'){
                $query = "select f034fid_item,f034ftotal_inc_tax,f088famount,f087fpercentage from  t034fgrn_details inner join t088fitem on f034fid_item = f088fid inner join t087fasset_category on f087fid = f088fid_category   where f034fid_details = $id"; 
            $amount_result=$em->getConnection()->executeQuery($query)->fetch();
            $asset_amount = $asset_amount['f088famount'];
            $asset_percentage = $asset_amount['f087fpercentage'];
            if($total_inc_tax >= (($asset_percentage / 100) * $asset_amount)){
                $query = "UPDATE t034fgrn_details set f034fapproved_status=4 where f034fid_details = $id"; 
            $result=$em->getConnection()->executeQuery($query);                
            }
            }
                        // print_r($type_result);exit;

            $query1 = "select * from t034fgrn_details where f034fid_details = $id";
            $result1=$em->getConnection()->executeQuery($query1)->fetch();
            
            $master = (int)$result1['f034fid_grn']; 
            $query2 = "select * from t034fgrn_details where f034fid_grn = $master and f034fapproved_status = 0";
            $grn_detail = $em->getConnection()->executeQuery($query2)->fetchAll();

        if (!$grn_detail)
        {
            // print_r($master);exit;
                $query = "UPDATE t034fgrn set f034freason='$reason', f034fapproval_status= 1 where f034fid = $master"; 
                $result=$em->getConnection()->executeQuery($query);
           


            $query2 = "select SUM(f034total) as item_debit,f034fbudget_account_code as account_code, f034fbudget_activity_code as activity_code, f034fbudget_department_code as department_code, f034fbudget_fund_code as fund_code, f034freason from t034fgrn_details where f034fid_details = $id group by f034fbudget_account_code, f034fbudget_activity_code, f034fbudget_department_code, f034fbudget_fund_code";
            $debit_result = $em->getConnection()->executeQuery($query2)->fetchAll();



            $query2 = "select SUM(f034total) as item_credit,f034faccount_code as account_code, f034factivity_code as activity_code, f034fdepartment_code as department_code, f034ffund_code as fund_code, f034freason from t034fgrn_details where f034fid_details = $id group by f034faccount_code, f034factivity_code, f034fdepartment_code, f034ffund_code";
           
            $credit_result = $em->getConnection()->executeQuery($query2)->fetchAll();


            $query = "select * from t034fgrn where f034fid = $master";
            $grn_data = $em->getConnection()->executeQuery($query)->fetch();
          
           
            $credit_totalAmount = 0;
            foreach($credit_result as $item)
            {

                $totalAmount = $credit_totalAmount + $item['item_credit'];
            }

            $data['f017fdescription'] = "Invoice";
            $data['f017fmodule'] = "Invoice";
            $data['f017fapprover'] = $grn_data['f034fupdated_by'];
            $data['f017fidFinancialYear'] = $grn_data['f034fid_financialyear'];
            $data['f017ftotalAmount'] = $totalAmount;
            $data['f017freferenceNumber'] = $grn_data['f034freference_number'];
            $data['f017fdateOfTransaction'] = $grn_data['f034forder_date'];
            $data['f017fapprovedStatus'] = "1";
            $details = array();
            foreach($debit_result as $item){
                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $grn_data['f034freference_number'];
                $detail['f018fdrAmount'] = (float) $item['item_debit'];
                $detail['f018fcrAmount'] = 0;
                $detail['f018fcreated_by'] = $grn_data['f071fcreated_by'];
                $detail['f018fcreated_dt_tm'] = $grn_data['f071fcreated_dt_tm'];
                array_push($details,$detail);
             }
             foreach($credit_result as $item){
                $detail['f018faccountCode'] =  $item['account_code'];
                $detail['f018ffundCode'] =  $item['fund_code'];
                $detail['f018factivityCode'] =  $item['activity_code'];
                $detail['f018fdepartmentCode'] =  $item['department_code'];
                
                $detail['f018fsoCode'] = "NULL";
                $detail['f018freferenceNumber'] = $grn_data['f034freference_number'];
                $detail['f018fdrAmount'] = 0;
                $detail['f018fcrAmount'] = (float) $item['item_credit'];
                $detail['f018fcreated_by'] = $grn_data['f034fupdated_by'];
                $detail['f018fcreated_dt_tm'] = $grn_data['f034fcreated_dt_tm'];
                array_push($details,$detail);
            }

            // print_r($details);exit;
            $data['journal-details'] = $details;
            if($status == 1){

            $journalRepository->createNewData($data);
            }

             //    $query4 = "select * from t034fgrn where f034fid = $master";
             //    $grn=$em->getConnection()->executeQuery($query4)->fetch();
             // print_r($grn);
             // die(); 

                // $grnAmount=$grn['f034ftotal_amount'];
               
                $em = $this->getEntityManager();
                $qb = $em->createQueryBuilder();
        // Query
           
                $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount, p.f034fapprovedDate, p.f034fapprovedRefNum, p.f034fdonor, pd.f034fidDetails, pd.f034fidItem, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, pd.f034fitemDescription, pd.f034forderLineNo, pr.f034ftotalAmount, p.f034frating,p.f034fupdatedBy, pd.f034ftypeOfItem, pd.f034fbalanceQuantity, pd.f034fitemProcurementType')
                ->from('Application\Entity\T034fgrn', 'p')
                ->leftjoin('Application\Entity\T034fgrnDetails', 'pd', 'with', 'p.f034fid = pd.f034fidGrn')
                ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
                ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
                ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
                ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
                ->where('p.f034fid = :grnId')
                ->setParameter('grnId',$master);
            
                $query = $qb->getQuery();
                $results = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
// print_r($results);exit;
                      

             if($status == 1){      
            $configRepository = $em->getRepository("Application\Entity\T011finitialConfig");
                $numberData = array();
                $numberData['type']= "DSP";
                $number = $configRepository->generateFIMS($numberData);
               
                $debitNote = new T063fsponsorDn();
                $debitNote->setF063fidInvoice((int)$results[0]['f034fid'])
                ->setF063fidCustomer((int)$results[0]['f034fidSupplier'])
                ->setF063ftotalAmount((float)$results[0]['f034ftotalAmount'])
                ->setF063fdescription($results[0]['f034fdescription'])
                ->setF063fidFinancialYear($results[0]['f034fidFinancialyear'])
                ->setF063ftype("DSP")
                ->setF063freason($results[0]['f034freason'])
                // ->setF031fidFinancialYear($data['f031fidFinancialYear'])
                ->setF063fbalance((float)$results[0]['f034ftotalAmount'])
                ->setF063freferenceNumber($number)
                ->setF063fdate(new \DateTime($results[0]['f034forderDate']))
                ->setF063fvoucherDays((int)0)
                ->setF063fvoucherNumber($number)
                ->setF063fnarration($results[0]['f034fdescription'])
                ->setF063fstatus((int)1)
                ->setF063fapprovedBy((int)$results[0]['f034fupdatedBy'])
                ->setF063fcreatedBy((int)$_SESSION['userId'])
                ->setF063fupdatedBy((int)$_SESSION['userId']);

                $debitNote->setF063createdDtTm(new \DateTime())
                    ->setF063updatedDtTm(new \DateTime());
                     try {
                        $em->persist($debitNote);
                        $em->flush();
                    } catch (\Exception $e) {
                        echo $e;
                    }

                 $data=array();
            $data['type']='BIL';
            $initialRepository = $em->getRepository("Application\Entity\T011finitialConfig");
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f091freferenceNumber'] = $generateNumber;


                $em = $this->getEntityManager();
                $qb = $em->createQueryBuilder();

                // $bill = new T091fbillRegistration();

                // $bill->setF091freferenceNumber($postData['f091freferenceNumber'])
                //     ->setF091fcreatedDate(new \DateTime($results[0]['f034fcreated_dt_tm']))
                //     ->setF091ftotalAmount($results[0]['f034ftotalAmount'])
                //     ->setF091fapprovedStatus((int)0)
                //     ->setF091ftype('Grn')
                //     ->setF091fcontactPerson1('person1')
                //     ->setF091fcontactPerson2('person2')
                //     ->setF091fcontactPerson3('NUll')
                //     ->setF091fpaymentStatus((int)0)
                //     ->setF091fstatus((int)1)
                //     ->setF091fcreatedBy((int)$_SESSION['userId'])
                //     ->setF091fupdatedBy((int)$_SESSION['userId']);

                // $bill->setF091fcreatedDtTm(new \DateTime())
                //     ->setF091fupdatedDtTm(new \DateTime());
                    
        
                // $em->persist($bill);
                // $em->flush();
                
            foreach ($results as $result) {
                // print_r($result['f034fidFinancialyear']);exit;
                $year = date('y');
                $Year = date('Y');
                $fund=$result['f034fbudgetFundCode'];
                $department=$result['f034fbudgetDepartmentCode'];
                $activity=$result['f034fbudgetActivityCode'];
                $financialyear=$result['f034fidFinancialyear'];
            $account=$result['f034fbudgetAccountCode'];
            // print_r($fund);
            // print_r($department);
            // print_r($activity);
            // print_r($account);
            // print_r($budgetyear);exit;
                $grnAmount = (float)$result['f034ftotalIncTax'];
                // print_r($grnAmount);exit;


                $qb = $em->createQueryBuilder();

                 $qb->select(" pd.f034fidDetails, pd.f034fidItem, pd.f034fsoCode, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftotalIncTax, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->leftjoin('Application\Entity\T034fpurchaseOrderDetails', 'pd', 'with', 'p.f034fid = pd.f034fidPurchaseOrder')
            ->leftjoin('Application\Entity\T034fgrn', 'g', 'with', 'p.f034fid = g.f034fidPurchaseOrder')
            ->where('pd.f034fbudgetFundCode = :fundId')
            ->setParameter('fundId',$fund)
            ->andWhere('pd.f034fbudgetDepartmentCode = :departId')
            ->setParameter('departId',$department)
            ->andWhere('pd.f034fbudgetActivityCode = :activityId')
            ->setParameter('activityId',$activity)
            ->andWhere('pd.f034fbudgetAccountCode = :accountId')
            ->setParameter('accountId',$account)
            ->andWhere('p.f034fidFinancialyear = :financialyear')
            ->setParameter('financialyear',(int)$financialyear)
            ->andWhere('g.f034fid = :grnId')
            ->setParameter('grnId',(int)$master);

            $query = $qb->getQuery();
            $purchase = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
// print_r($purchase);
// die();

            $year_query = "select f110fid from t110fbudgetyear inner join t015ffinancialyear on f110fyear = f015fname where f015fid = $financialyear"; 
            $year_result=$em->getConnection()->executeQuery($year_query)->fetch();
            $budgetyear=(int)$year_result['f110fid'];


            $purchaseAmount = (float)$purchase[0]['f034ftotalIncTax'];

            // print_r($department);
            // print_r($grnAmount);
            // exit();

            if($grnAmount<=$purchaseAmount)
            {
                $total= $purchaseAmount-$grnAmount;
                if($purchaseAmount == $grnAmount)
                {
                    $total = $grnAmount;
                }

                // $query = "SELECT f108fbudget_expensed from t108fbudget_summary where f108fdepartment='$department'and f108ffund='$fund'and f108factivity='$activity'and f108faccount='$account'";
                // $expenseAmount=$em->getConnection()->executeQuery($query)->fetch();

                // $amount= $expenseAmount['f108fbudget_expensed'];
                // $expense = $total+$amount;
                            ///////////////////////////////////////////////////////////////////////////////////
        $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
        $result=$em->getConnection()->executeQuery($query)->fetch();

        // print_r($result);
        // exit();
        if(!$result){
            $account=substr($account,0,2)."000"; 
            $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
            $result=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);
        // exit();
            if(!$result){
                $account=substr($account,0,1)."0000"; 
                $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
                $result=$em->getConnection()->executeQuery($query)->fetch();
                if(!$result){
                    $status=-1;
                }
                else{

                    $balance=$result['f108fbudget_pending'];

                    if($amount<$balance)
                    {
                        $status=1;
                    }
                    else
                    {
                        $status=0;
                    }
                }
            }
            else{

                $balance=$result['f108fbudget_pending'];

                if($amount<$balance)
                {
                    $status=1;
                }
                else
                {
                    $status=0;
                }
            }
        }
        else{

            $balance=$result['f108fbudget_pending'];

            if($amount<$balance)
            {
                $status=1;
            }
            else
            {
                $status=0;
            }
        }
/////////////////////////////////////////////////////////////////////////
        if($status == 1){

                $query = "UPDATE t108fbudget_summary set f108fbudget_expensed=f108fbudget_expensed+$total,f108fbudget_commitment= f108fbudget_commitment - $total where f108fdepartment='$department' and f108ffund='$fund' and f108factivity='$activity' and f108faccount='$account' and f108fid_financial_year = $budgetyear"; 

                $value=$em->getConnection()->executeQuery($query);
        }
                
                //  print_r($result);
                // die();
            }
            else 
            {
                $total= $grnAmount-$purchaseAmount;
                            ///////////////////////////////////////////////////////////////////////////////////
        $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
        $result=$em->getConnection()->executeQuery($query)->fetch();

        // print_r($result);
        // exit();
        if(!$result){
            $account=substr($account,0,2)."000"; 
            $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
            $result=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($result);
        // exit();
            if(!$result){
                $account=substr($account,0,1)."0000"; 
                $query = "select f108fbudget_pending  from t108fbudget_summary  where f108fdepartment = '$department' and f108ffund = '$fund' and f108faccount = '$account' and f108factivity = '$activity' and f108fid_financial_year= $budgetyear";
                $result=$em->getConnection()->executeQuery($query)->fetch();
                if(!$result){
                    $status=-1;
                }
                else{

                    $balance=$result['f108fbudget_pending'];

                    if($amount<$balance)
                    {
                        $status=1;
                    }
                    else
                    {
                        $status=0;
                    }
                }
            }
            else{

                $balance=$result['f108fbudget_pending'];

                if($amount<$balance)
                {
                    $status=1;
                }
                else
                {
                    $status=0;
                }
            }
        }
        else{

            $balance=$result['f108fbudget_pending'];

            if($amount<$balance)
            {
                $status=1;
            }
            else
            {
                $status=0;
            }
        }
/////////////////////////////////////////////////////////////////////////
        if($status == 1){
            
                $query = "UPDATE t108fbudget_summary set f108fbudget_expensed=f108fbudget_expensed+$total,f108fbudget_commitment= f108fbudget_commitment - $total where f108fdepartment='$department' and f108ffund='$fund' and f108factivity='$activity' and f108faccount='$account'  and f108fid_financial_year = $budgetyear"; 

                $value=$em->getConnection()->executeQuery($query);
        }

                    try {
                        $debitNoteDetails = new T064fsponsorDnDetails();
                    } catch (\Exception $e) {
                        echo $e;
                    }

                $debitNoteDetails->setF064fidItem((int)$result['f034fidItem'])
                ->setF064fgstValue((int) $result['f034fpercentage'])
                ->setF064fquantity((int) $result['f034fquantity'])
                ->setF064fprice((float) $result['f034fprice'])
                ->setF064ftoDnAmount((float) $total)
                ->setF064ftotal((float) $result['f034ftotalIncTax'])
                ->setF064fidTaxCode((int) $result['f034ftaxCode'])
                ->setF064ftotalExc((float) $result['f034total'])
                ->setF064ftaxAmount((float) $result['f034ftaxAmount'])
                ->setF064fgstCode( $result['f034ftaxCode'])
                ->setF064fdebitFundCode( $result['f034ffundCode'])
                ->setF064fdebitAccountCode( $result['f034faccountCode'])
                ->setF064fdebitActivityCode( $result['f034factivityCode'])
                ->setF064fdebitDepartmentCode( $result['f034fdepartmentCode'])
                ->setF064fcreditFundCode( $result['f034fbudgetFundCode'])
                ->setF064fcreditAccountCode( $result['f034fbudgetAccountCode'])
                ->setF064fcreditActivityCode( $result['f034fbudgetActivityCode'])
                ->setF064fcreditDepartmentCode( $result['f034fbudgetDepartmentCode'])
                ->setF064fcreatedBy((int)$_SESSION['userId'])
                ->setF064fupdatedBy((int)$_SESSION['userId'])
                ->setF064fstatus((int) $data['f034fapprovalStatus'])
                ->setF064fidsponsorDn((int)$debitNote->getF063fid());

            $debitNoteDetails->setF064fcreatedDtTm(new \DateTime())
                ->setF064fupdatedDtTm(new \DateTime());

                try {


                    $em->persist($debitNoteDetails);
                    $em->flush();

                } catch (\Exception $e) {
                    echo $e;
                }
            }
            // echo $query;
            // die();

                // $query = "SELECT f108fbudget_expensed from t108fbudget_summary where f108fdepartment='$department'and f108ffund='$fund'and f108factivity='$activity'and f108faccount='$account'";
                // $expenseAmount=$em->getConnection()->executeQuery($query)->fetch();

                // $amount= $expenseAmount['f108fbudget_expensed'];

                // $expense = $amount-$total;

                // print_r($expense);
                // exit();

                // $query = "SELECT f091freference_number FROM t091fbill_registration order by f091fid desc";
                // $result = $em->getConnection()->executeQuery($query)->fetch();
           
                // $data=$result['f091freference_number'];
                // $value=substr($data, 3,6);

           
                // $count=$value + 1;
                // $number = "BIL" . (sprintf("%'06d", $count)) . "/" . $year;
               

        
                    // $billDetailObj = new T091fbillRegistrationDetails();

                    // $billDetailObj->setF091fidBill((int)$bill->getF091fid())
                    //     ->setF091freferenceNumber($results[0]['f034freference_number'])
                    //     ->setF091fidItem((int) $result['f034fidItem'])
                    //     ->setF091ftype((int)0)
                    //     ->setF091fsoCode('NULL')
                    //     ->setF091fquantity((int)$result['f034fquantity'])
                    //     ->setF091funit($result['f034funit'])
                    //     ->setF091fprice((float)$result['f034fprice'])
                    //     ->setF091total((float)$result['f034total'])
                    //     ->setF091fpercentage((int) $result['f034fpercentage'])
                    //     ->setF091ftaxCode((int)$result['f034ftaxCode'])
                    //     ->setF091fstatus((int)$result['f034fstatus'])
                    //     ->setF091ftaxAmount((float) $result['f034ftaxAmount'])
                    //     ->setF091ftotalIncTax((float) $result['f034ftotalIncTax'])
                    //     ->setF091ffundCode($result['f034ffundCode'])
                    //     ->setF091fdepartmentCode($result['f034fdepartmentCode'])
                    //     ->setF091factivityCode($result['f034factivityCode'])
                    //     ->setF091faccountCode($result['f034faccountCode'])
                    //     ->setF091fbudgetFundCode($result['f034fbudgetFundCode'])
                    //     ->setF091fbudgetDepartmentCode($result['f034fbudgetDepartmentCode'])
                    //     ->setF091fbudgetActivityCode($result['f034fbudgetActivityCode'])
                    //     ->setF091fbudgetAccountCode($result['f034fbudgetAccountCode'])
                    //     ->setF091fcreatedBy((int)$_SESSION['userId'])
                    //     ->setF091fupdatedBy((int)$_SESSION['userId']);
                

                    // $billDetailObj->setF091fcreatedDtTm(new \DateTime())
                    //     ->setF091fupdatedDtTm(new \DateTime());
               
                    // try
                    // {    
                    //     $em->persist($billDetailObj);
        
                    //     $em->flush();

                    // }
                    // catch(\Exception $e)
                    // {
                    //     echo $e;
                    // }
            }
            
            }
            }
        }

    }

    public function grnNotIn()
    {

        $em = $this->getEntityManager();

        $query = "SELECT *, g.f034freference_number as f034freferenceNumber, g.f034fid_purchase_order as f034fidPurchaseOrder, g.f034forder_date  as f034forderDate from t034fgrn as g inner join t034fgrn_details as gd on gd.f034fid_grn =g.f034fid  where gd.f034ftype_of_item like 'Assets' and g.f034fid not in (select f085fgrn_number from t085fasset_information_new) and f034fapproval_status = 0 ";

        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();

        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fapproved_date'];
            $item['f034fapproved_date'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpired_date'];
            $item['f034fexpired_date'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        return $result1;
    }


    public function deleteGRNDetails($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query2 = "DELETE from t034fgrn_details WHERE f034fid_details = $id";
            $res = $em->getConnection()->executeQuery($query2);
        }
        return $res;
    }

    public function expenseGrns($data)
    {
        $em = $this->getEntityManager();

        foreach ($data['id'] as $id)
        {
            $query = "UPDATE t034fgrn_details set f034fapproved_status=2 where f034fid_details = $id"; 
            $result=$em->getConnection()->executeQuery($query);
        }
        return $res;
    }

    public function getGrnById($data)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $gr = $data['grnId'];
        // print_r($gr);exit;

    $grnResult = array();
    foreach ($gr as $id)
    {
        $id = (int)$id;
        // print_r($id);exit;


        $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount,p.f034fapprovedDate,p.f034fapprovedRefNum,p.f034fdonor, pd.f034fidDetails, pd.f034fidItem,pd.f034fitemType, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, pd.f034fitemDescription, pd.f034forderLineNo, pd.f034fquantityReceived, pd.f034fapprovedStatus, p.f034frating, pd.f034ftypeOfItem, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity, pd.f034fitemProcurementType')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fgrnDetails', 'pd', 'with', 'p.f034fid = pd.f034fidGrn')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            // ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where('p.f034fid = :purchaseId')
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

         $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fapprovedDate'];
            $item['f034fapprovedDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        // print_r($result1);exit;
         // $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];



        $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $purchase['categoryName'] = $categoryResult['itemCategory'];
                $purchase['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

               

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
                // print_r($categoryResult);exit;
            
                $purchase['itemName'] = $itemResult['assetItem'];
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();
                 

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
                
                $purchase['itemName'] = $itemResult['procurementItem'];
           
            }
            array_push($grnResult, $purchase);
                        // $i++;

        }
        return $grnResult;
    }
}

    public function updateGrnBillRegistration($data)
    {
        $em = $this->getEntityManager();
        $id = (int)$data['grnId'];
        $billStatus = (int)$data['billStatus'];
        // print_r($id);exit;


        
             $query = "UPDATE t034fgrn set f034fbill_generated = $billStatus where f034fid = $id"; 
            
            $result=$em->getConnection()->executeQuery($query);
        
        return $result;
    }


    public function listOfGrnByStatus($id) 
    {


        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, s.f030fcompanyName as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName, p.f034freferenceNumber, p. f034fidPurchaseOrder, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount,p.f034fapprovedDate,p.f034fapprovedRefNum,p.f034fdonor, pd.f034fidDetails, pd.f034fidItem,pd.f034fitemType, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, pd.f034fitemDescription, pd.f034forderLineNo, pd.f034fquantityReceived, pd.f034fapprovedStatus, p.f034frating, pd.f034ftypeOfItem, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity, pd.f034fitemProcurementType')
            ->from('Application\Entity\T034fgrn', 'p')
            ->leftjoin('Application\Entity\T034fgrnDetails', 'pd', 'with', 'p.f034fid = pd.f034fidGrn')
            ->leftjoin('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->leftjoin('Application\Entity\T015fdepartment', 'd', 'with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'pr', 'with', 'p.f034fidPurchaseOrder = pr.f034fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 's', 'with', 'p.f034fidSupplier = s.f030fid')
            ->where('p.f034fstatus = :purchaseId')
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    
        $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fapprovedDate'];
            $item['f034fapprovedDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }

         $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];

          $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
                $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

               

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
        
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

               

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;

        }
        // print_r($result1);exit;
        // return $result1;
        // return $result1;
        return $result1;
    }
    

    public function getGrnDetailsListById($id) 
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select('pd.f034fidDetails, pd.f034fidItem, pd.f034fitemType, pd.f034fsoCode,pd.f034ftype, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, p.f034fidDepartment, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, pd.f034fitemDescription, pd.f034forderLineNo, pd.f034fquantityReceived, pd.f034fapprovedStatus, pd.f034ftypeOfItem, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity, pd.f034fidGrn, pd.f034fcreatedDtTm, p.f034freferenceNumber as grnReference, po.f034freferenceNumber as poReference, pd.f034fitemProcurementType')
            ->from('Application\Entity\T034fgrnDetails', 'pd')
            ->leftjoin('Application\Entity\T034fgrn', 'p', 'with', 'p.f034fid = pd.f034fidGrn')
            ->leftjoin('Application\Entity\T034fpurchaseOrder', 'po', 'with', 'po.f034fid = p.f034fidPurchaseOrder')
            ->where('pd.f034fidDetails = :purchaseId')
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
    $result1 = array();
        foreach ($result as $item) {
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));

            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));

            
            array_push($result1, $item);
        }

         $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];

        $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                 $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
        $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];


            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

             

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
      
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();

                 

                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $item"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


       
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;

        }
        return $result1;
    }

    public function getGrnByAsset()
    {
        $em = $this->getEntityManager();

        $query = " SELECT g.f034fid, g.f034freference_number from t034fgrn g inner join t034fgrn_details gd on gd.f034fid_grn = g.f034fid where gd.f034fid_details not in (select i.f133fid_grn_detail from t133fasset_information i)"; 
            
            $result=$em->getConnection()->executeQuery($query)->fetchAll();
        
        return $result;
    }
}
