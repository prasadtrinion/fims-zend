<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PageRepository extends EntityRepository
{
    /**
     * Get entry by page identifier
     * 
     * @param string $identifier
     * @return mixed|NULL|\Doctrine\DBAL\Driver\Statement
     */
    public function getByIdentifier($identifier)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('p')
            ->from('Application\Entity\Page', 'p')
            ->where('p.identifier = :identifier')
            ->andWhere('p.deleteFlag = 0')
            ->setParameter('identifier', $identifier);
        
        $query = $qb->getQuery();
        
        return $query->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }
}