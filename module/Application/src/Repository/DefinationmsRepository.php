<?php
namespace Application\Repository;

use Application\Entity\T011fdefinationms;
use Application\Entity\T012fdefinationtypems;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

class DefinationmsRepository extends EntityRepository 
{

    public function getList()
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('d.f011fid, d.f011fdefinitionCode, d.f011fdefinitionDesc, d.f011fbahasaIndonesia, d.f011fdescription, d.f011fdefOrder, d.f011fstatus, d.f011fidDefinitiontypems, t.f012fdefTypeDesc as definationcode ')
            ->from('Application\Entity\T011fdefinationms', 'd')
            ->leftjoin('Application\Entity\T012fdefinationtypems', 't','with', 'd.f011fidDefinitiontypems=t.f012fid');


        $query = $qb->getQuery();
        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
    }

    public function getListById($id)
    {
        $em = $this->getEntityManager();


        $qb = $em->createQueryBuilder();
          $qb->select('d.f011fid, d.f011fdefinitionCode, d.f011fdefinitionDesc, d.f011fbahasaIndonesia, d.f011fdescription, d.f011fdefOrder, d.f011fstatus, d.f011fidDefinitiontypems, t.f012fdefTypeDesc as definationcode ')
            ->from('Application\Entity\T011fdefinationms', 'd')
            ->leftjoin('Application\Entity\T012fdefinationtypems', 't','with', 'd.f011fidDefinitiontypems=t.f012fid')
            ->where('d.f011fid = :DefinationmsId')
            ->setParameter('DefinationmsId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;

    }

     public function createNewData($data)
     {

        $em = $this->getEntityManager();

        $definationms = new T011fdefinationms();

        $definationms->setF011fidDefinitiontypems((int)$data['f011fidDefinitiontypems'])
                ->setF011fdefinitionCode($data['f011fdefinitionCode'])
                ->setF011fdefinitionDesc($data['f011fdefinitionDesc'])
                ->setF011fbahasaIndonesia($data['f011fbahasaIndonesia'])
                ->setF011fdescription($data['f011fdescription'])
                ->setF011fdefOrder($data['f011fdefOrder'])
                ->setF011fstatus((int)$data['f011fstatus'])
                ->setF011fcreatedBy((int)$_SESSION['userId'])
                ->setF011fupdatedBy((int)$_SESSION['userId']);

        $definationms->setF011fcreateDtTm(new \DateTime())
                ->setF011fupdateDtTm(new \DateTime());
        try{
        $em->persist($definationms);
        $em->flush();
        }
        catch(\Exception $e){
            echo $e;
        }
        return $definationms; 
    }


    public function updateData($definationms, $data = []) 
    {
        $em = $this->getEntityManager();
        
        $definationms->setF011fidDefinitiontypems((int)$data['f011fidDefinitiontypems'])
                ->setF011fdefinitionCode($data['f011fdefinitionCode'])
                ->setF011fdefinitionDesc($data['f011fdefinitionDesc'])
                ->setF011fbahasaIndonesia($data['f011fbahasaIndonesia'])
                ->setF011fdescription($data['f011fdescription'])
                ->setF011fdefOrder($data['f011fdefOrder'])
                ->setF011fstatus((int)$data['f011fstatus'])
                ->setF011fcreatedBy((int)$_SESSION['userId'])
                ->setF011fupdatedBy((int)$_SESSION['userId']);

        $definationms->setF011fupdateDtTm(new \DateTime());

        $em->persist($definationms);
        $em->flush();
       
    }

    public function getDefCode($defCode)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('d.f011fid, d.f011fdefinitionCode')
            ->from('Application\Entity\T011fdefinationms','d')
            ->leftjoin('Application\Entity\T012fdefinationtypems','t','with', 'd.f011fidDefinitiontypems=t.f012fid')
            ->where('t.f012fdefTypeDesc= :definationcode')
            ->setParameter('definationcode',$defCode);


        $query = $qb->getQuery();
        $result = $query->getArrayResult();

        return $result;

        
    }

    public function update($entity) 
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }
}
