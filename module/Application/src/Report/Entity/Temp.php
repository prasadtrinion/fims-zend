<?php

namespace Report\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Temp
 *
 * @ORM\Table(name="temp")
 * @ORM\Entity(repositoryClass="Report\Repository\TempRepository")
 */
class Temp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_dt_tm", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_dt_tm", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updateDtTm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDtTm
     *
     * @param \DateTime $createDtTm
     *
     * @return Temp
     */
    public function setCreateDtTm($createDtTm)
    {
        $this->createDtTm = $createDtTm;

        return $this;
    }

    /**
     * Get createDtTm
     *
     * @return \DateTime
     */
    public function getCreateDtTm()
    {
        return $this->createDtTm;
    }

    /**
     * Set updateDtTm
     *
     * @param \DateTime $updateDtTm
     *
     * @return Temp
     */
    public function setUpdateDtTm($updateDtTm)
    {
        $this->updateDtTm = $updateDtTm;

        return $this;
    }

    /**
     * Get updateDtTm
     *
     * @return \DateTime
     */
    public function getUpdateDtTm()
    {
        return $this->updateDtTm;
    }
}
