<?php
namespace Application\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct()
    {
        parent::__construct('login-form');
        
        // Set form attributes
        $this->setAttributes(array(
            'method' => 'post',
            'name' => 'login_form',
            'id' => 'login_form'
        ));
        
        $this->add(array(
            'name' => 'username',
            'type' => 'email',
            'required' => true,
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Enter email address'
            )
        ));
        
        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Enter password'
            )
        ));
        
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }
}
