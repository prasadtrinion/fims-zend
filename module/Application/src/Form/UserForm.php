<?php
namespace Application\Form;

use Application\Entity\Role;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Validator\StringLength;

class UserForm extends Form
{

    protected $config;

    protected $options = [];
    
    public function __construct(ObjectManager $objectManager, $options = array())
    {
        parent::__construct('user_form');

        // Default options
        $defaultOptions = [
            'userRole' => 'SYSTEM_ADMIN',
            'action' => 'register',
            'state' => []
        ];
        
        $this->options = array_merge($defaultOptions, $options);
        
        // Set Hydrator
        $this->setHydrator(new DoctrineObject($objectManager));
        
        // Set form attributes
        $this->setAttributes([
            'method' => 'post',
            'name' => 'user_form',
            'id' => 'user_form'
        ]);
        
        // Add form elements
        $this->add([
            'name' => 'firstName',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter first name',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'lastName',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter last name',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'username',
            'type' => Email::class,
            'attributes' => [
                'id' => 'username',
                'class' => 'form-control',
                'placeholder' => 'Enter username/Email address',
                'required' => 'required',
                'autocomplete' => 'off'
            ]
        ]);
        
        $this->add([
            'id' => 'password',
            'name' => 'password',
            'type' => Password::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter Password',
                'required' => 'required',
                'id' => 'password'
            ]
        ]);
        
        $this->add([
            'name' => 'diverseyId',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'id' => 'diverseyId',
                'readonly' => 'readonly',
                'placeholder' => 'Enter diversey id'
            ]
        ]);
        
        $this->add([
            'name' => 'phoneNumber',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter phone number',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'contactType',
            'id' => 'contactType',
            'type' => Select::class,
            'attributes' => [
                'required' => true,
                'class' => 'form-control',
                'Placeholder' => 'Select contact type',
                'required' => 'required'
            ],
            'options' => [
                'options' => [
                    '1' => 'Email',
                    '2' => 'Phone'
                ],
                'empty_option' => '(None)'
            ]
        ]);
        
        $this->add([
            'name' => 'companyName',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter distributor Name',
                'required' => 'required'
            ]
        ]);
        
        // Personal information
        $this->add([
            'name' => 'addressLine1',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter address line1',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'addressLine2',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter address line2'
            ]
        ]);
        
        $this->add([
            'name' => 'city',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter city',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'state',
            'id' => 'state',
            'type' => Select::class,
            'attributes' => [
                'required' => 'required',
                'class' => 'form-control',
                'Placeholder' => 'Select state ',
                'id' => 'state'
            ],
            'options' => [
                'options' => $this->options['state'],
                'disable_inarray_validator' => true,
                'empty_option' => '(None)'
            ]
        ]);
        
        $this->add([
            'name' => 'country',
            'id' => 'country',
            'type' => Select::class,
            'attributes' => [
                'required' => 'required',
                'class' => 'form-control',
                'Placeholder' => 'Select country',
                'id' => 'country'
            ],
            'options' => [
                'options' => [
                    'USA' => 'United States',
                    'CAN' => 'Canada'
                ],
                'empty_option' => '(None)'
            ]
        ]);
        
        $this->add([
            'name' => 'zipCode',
            'type' => Text::class,
            'attributes' => [
                'class' => 'form-control',
                'id' => 'zipCode',
                'placeholder' => 'Enter zipcode',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'id' => 'mailingList',
            'name' => 'mailingList',
            'type' => 'Checkbox'
        ]);
        
        // Distributor information
        $this->add([
            'name' => 'salesExecutive',
            'id' => 'sales_executive',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter sales executive name',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'distPhoneNumber',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter phone number',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'distAddressLine1',
            'id' => 'dist_address_line1',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter address line1',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'distAddressLine2',
            'id' => 'dist_address_line2',
            'type' => 'text',
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Enter address line2'
            ]
        ]);
        
        $this->add([
            'name' => 'distCity',
            'id' => 'dist_city',
            'type' => 'text',
            'attributes' => [
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter city',
                'required' => 'required'
            ]
        ]);
        
        $this->add([
            'name' => 'distZipCode',
            'id' => 'dist_zipcode',
            'type' => 'text',
            'attributes' => [
                'required' => 'required',
                'class' => 'form-control',
                'placeholder' => 'Enter zipcode'
            ]
        ]);
        
        $this->add([
            'name' => 'distState',
            'id' => 'dist_state',
            'type' => Select::class,
            'attributes' => [
                'required' => 'required',
                'class' => 'form-control',
                'Placeholder' => 'Select state '
            ],
            'options' => [
                'options' => $this->options['state'],
                'disable_inarray_validator' => true,
                'empty_option' => '(None)'
            ]
        ]);
        
        $this->add([
            'name' => 'distCountry',
            'id' => 'dist_country',
            'type' => Select::class,
            'attributes' => [
                'class' => 'form-control',
                'Placeholder' => 'Select country',
                'required' => 'required'
            ],
            'options' => [
                'options' => [
                    'USA' => 'United States',
                    'CAN' => 'Canada'
                ],
                'empty_option' => '(None)'
            ]
        ]);
        
        $this->add([
            'name' => 'dsrType',
            'type' => Select::class,
            'attributes' => [
                'id' => 'dsrType',
                'class' => 'form-control',
                'Placeholder' => 'Select Type',
                'required' => 'required'
            ],
            'options' => [
                'options' => [
                    '1' => 'Standard',
                    '2' => 'Premiere'
                ],
                'empty_option' => '(None)'
            ],
        ]);
        
        $this->add([
            'name' => 'status',
            'type' => Select::class,
            'attributes' => [
                'id' => 'status',
                'class' => 'form-control',
                'Placeholder' => 'Select Status',
                'required' => 'required'
            ],
            'options' => [
                'options' => [
                    '1' => 'Activated',
                    '0' => 'Deactivated',
                    '-1' => 'Rejected'
                ]
            ],
        ]);
        
        $this->add([
            'name' => 'accountType',
            'type' => Select::class,
            'attributes' => [
                'id' => 'accountType',
                'class' => 'form-control',
                'Placeholder' => 'Select account type',
                'required' => 'required'
            ],
            'options' => [
                'options' => [],
                'empty_option' => '(None)'
            ],
        ]);
        
        $this->add([
            'name' => 'csrf',
            'type' => Csrf::class,
            'options' => [
                'csrf_options' => [
                    'timeout' => 600
                ]
            ]
        ]);
        
        $this->setAccountTypeOptions($this->options['userRole']);
        
        // Set input filters
        $this->setInputFilters();

        // Remove fields for specific action
        $this->removeFields();
        
        // Remove filters for specific action
        $this->removeFilters();
    }

    protected function setInputFilters()
    {
        $inputFilter = $this->getInputFilter();
        
        $inputFilter->add([
            'name' => 'firstName',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'lastName',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'password',
            'required' => true,
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'min' => 6
                    ]
                ]
            ]
        ]);
        
        $inputFilter->add([
            'name' => 'phoneNumber',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'addressLine1',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'country',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'state',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'city',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'zipCode',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'salesExecutive',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'companyName',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distPhoneNumber',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distAddressLine1',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distCountry',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distState',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distCity',
            'required' => true
        ]);
        
        $inputFilter->add([
            'name' => 'distZipCode',
            'required' => true
        ]);
        
        $this->setInputFilter($inputFilter);
    }

    /**
     * Remove unnecessary fields for a specific page/action
     */
    protected function removeFields()
    {
        $action = $this->options['action'];
        
        if ($action == 'register' || $action == 'edit-profile') {
            $this->remove('dsrType');
            $this->remove('status');
            $this->remove('accountType');
        }
    }
    
    /**
     * Remove unnecessary filters for a specific page/action
     */
    protected function removeFilters()
    {
        $inputFilter = $this->getInputFilter();
        $action = $this->options['action'];
        
        if ($action == 'register') {
            $inputFilter->remove('dsrType');
            $inputFilter->remove('status');
            $inputFilter->remove('accountType');
        }
        
        if ($action == 'edit-profile') {
            $inputFilter->remove('password');
            $inputFilter->remove('dsrType');
            $inputFilter->remove('status');
            $inputFilter->remove('accountType');
        }
        
        if($action == 'admin_user_create') {
            $inputFilter->remove('mailingList');
            $inputFilter->remove('dsrType');
        }
        
        if($action == 'admin_user_edit') {
            $inputFilter->remove('password');
            $inputFilter->remove('dsrType');
            $inputFilter->remove('mailingList');
        }
    }
    
    protected function setAccountTypeOptions($role)
    {
        if ($role == 'SYSTEM_ADMIN') {
            $this->get('accountType')->setAttribute('options', [
                '2' => 'USER',
                '1' => 'Super Admin'
            ]);
        } else {
            $this->get('accountType')->setAttribute('options', [
                '3' => 'DSR'
            ]);
        }
    }
}
