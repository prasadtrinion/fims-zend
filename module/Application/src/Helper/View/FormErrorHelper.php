<?php 
namespace Application\Helper\View;

use Zend\Form\View\Helper\FormElementErrors;

class FormErrorHelper extends FormElementErrors
{
    protected $messageCloseString     = '</label>';
    protected $messageOpenFormat      = '<label class="text-danger">';
    protected $messageSeparatorString = '';
}