<?php
namespace Application\Helper\View;

use Zend\Form\View\Helper\AbstractHelper;

class HashidsHelper extends AbstractHelper
{
    protected $hashids;
    
    public function __construct($hashids)
    {
        $this->hashids = $hashids;
    }
    
    public function __invoke($args)
    {
        return $this->hashids->encode($args);
    }
}