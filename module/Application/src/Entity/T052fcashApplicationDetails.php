<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052fcashApplicationDetails
 *
 * @ORM\Table(name="t052fcash_application_details")
 * @ORM\Entity(repositoryClass="Application\Repository\CashApplicationRepository")
 */
class T052fcashApplicationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_cash_application", type="integer", nullable=false)
     */
    private $f052fidCashApplication;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_cash_type", type="integer", nullable=true)
     */
    private $f052fidCashType;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fitem", type="integer", nullable=true)
     */
    private $f052fitem;

    /**
     * @var string
     *
     * @ORM\Column(name="f052ffund", type="string", length=50, nullable=true)
     */
    private $f052ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f052factivity", type="string", length=50, nullable=true)
     */
    private $f052factivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fdepartment", type="string", length=50, nullable=true)
     */
    private $f052fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f052faccount", type="string", length=50, nullable=true)
     */
    private $f052faccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fso_code", type="integer", nullable=true)
     */
    private $f052fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052famount", type="integer", nullable=true)
     */
    private $f052famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fquantity", type="integer", nullable=true)
     */
    private $f052fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052ftotal_amount", type="integer", nullable=true)
     */
    private $f052ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=true)
     */
    private $f052fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=true)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=true)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f052funit", type="string", length=20, nullable=true)
     */
    private $f052funit;

    /**
     * @var string
     *
     * @ORM\Column(name="f052ftax_code", type="integer", nullable=true)
     */
    private $f052ftaxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fbudget_fund", type="string", length=50, nullable=true)
     */
    private $f052fbudgetFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fbudget_activity", type="string", length=50, nullable=true)
     */
    private $f052fbudgetActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fbudget_department", type="string", length=50, nullable=true)
     */
    private $f052fbudgetDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fbudget_account", type="string", length=50, nullable=true)
     */
    private $f052fbudgetAccount;
    



    /**
     * Get f052fidDetails
     *
     * @return integer
     */
    public function getF052fidDetails()
    {
        return $this->f052fidDetails;
    }

    /**
     * Set f052fidCashApplication
     *
     * @param integer $f052fidCashApplication
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fidCashApplication($f052fidCashApplication)
    {
        $this->f052fidCashApplication = $f052fidCashApplication;

        return $this;
    }

    /**
     * Get f052fidCashApplication
     *
     * @return integer
     */
    public function getF052fidCashApplication()
    {
        return $this->f052fidCashApplication;
    }

    /**
     * Set f052fidCashType
     *
     * @param integer $f052fidCashType
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fidCashType($f052fidCashType)
    {
        $this->f052fidCashType = $f052fidCashType;

        return $this;
    }

    /**
     * Get f052fidCashType
     *
     * @return integer
     */
    public function getF052fidCashType()
    {
        return $this->f052fidCashType;
    }

    /**
     * Set f052fitem
     *
     * @param string $f052fitem
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fitem($f052fitem)
    {
        $this->f052fitem = $f052fitem;

        return $this;
    }

    /**
     * Get f052fitem
     *
     * @return string
     */
    public function getF052fitem()
    {
        return $this->f052fitem;
    }

    /**
     * Set f052ffund
     *
     * @param string $f052ffund
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052ffund($f052ffund)
    {
        $this->f052ffund = $f052ffund;

        return $this;
    }

    /**
     * Get f052ffund
     *
     * @return string
     */
    public function getF052ffund()
    {
        return $this->f052ffund;
    }

    /**
     * Set f052factivity
     *
     * @param string $f052factivity
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052factivity($f052factivity)
    {
        $this->f052factivity = $f052factivity;

        return $this;
    }

    /**
     * Get f052factivity
     *
     * @return string
     */
    public function getF052factivity()
    {
        return $this->f052factivity;
    }

    /**
     * Set f052fdepartment
     *
     * @param string $f052fdepartment
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fdepartment($f052fdepartment)
    {
        $this->f052fdepartment = $f052fdepartment;

        return $this;
    }

    /**
     * Get f052fdepartment
     *
     * @return string
     */
    public function getF052fdepartment()
    {
        return $this->f052fdepartment;
    }

    /**
     * Set f052faccount
     *
     * @param string $f052faccount
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052faccount($f052faccount)
    {
        $this->f052faccount = $f052faccount;

        return $this;
    }

    /**
     * Get f052faccount
     *
     * @return string
     */
    public function getF052faccount()
    {
        return $this->f052faccount;
    }

    /**
     * Set f052fsoCode
     *
     * @param integer $f052fsoCode
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fsoCode($f052fsoCode)
    {
        $this->f052fsoCode = $f052fsoCode;

        return $this;
    }

    /**
     * Get f052fsoCode
     *
     * @return integer
     */
    public function getF052fsoCode()
    {
        return $this->f052fsoCode;
    }

    /**
     * Set f052famount
     *
     * @param string $f052famount
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052famount($f052famount)
    {
        $this->f052famount = $f052famount;

        return $this;
    }

    /**
     * Get f052famount
     *
     * @return string
     */
    public function getF052famount()
    {
        return $this->f052famount;
    }

    /**
     * Set f052fquantity
     *
     * @param integer $f052fquantity
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fquantity($f052fquantity)
    {
        $this->f052fquantity = $f052fquantity;

        return $this;
    }

    /**
     * Get f052fquantity
     *
     * @return integer
     */
    public function getF052fquantity()
    {
        return $this->f052fquantity;
    }

    /**
     * Set f052ftotalAmount
     *
     * @param string $f052ftotalAmount
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052ftotalAmount($f052ftotalAmount)
    {
        $this->f052ftotalAmount = $f052ftotalAmount;

        return $this;
    }

    /**
     * Get f052ftotalAmount
     *
     * @return string
     */
    public function getF052ftotalAmount()
    {
        return $this->f052ftotalAmount;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }

    /**
     * Set f052funit
     *
     * @param \DateTime $f052funit
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052funit($f052funit)
    {
        $this->f052funit = $f052funit;

        return $this;
    }

    /**
     * Get f052funit
     *
     * @return \DateTime
     */
    public function getF052funit()
    {
        return $this->f052funit;
    }

    /**
     * Set f052ftaxCode
     *
     * @param \DateTime $f052ftaxCode
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052ftaxCode($f052ftaxCode)
    {
        $this->f052ftaxCode = $f052ftaxCode;

        return $this;
    }

    /**
     * Get f052ftaxCode
     *
     * @return \DateTime
     */
    public function getF052ftaxCode()
    {
        return $this->f052ftaxCode;
    }

    /**
     * Set f052fbudgetFund
     *
     * @param string $f052fbudgetFund
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fbudgetFund($f052fbudgetFund)
    {
        $this->f052fbudgetFund = $f052fbudgetFund;

        return $this;
    }

    /**
     * Get f052fbudgetFund
     *
     * @return string
     */
    public function getF052fbudgetFund()
    {
        return $this->f052fbudgetFund;
    }

    /**
     * Set f052fbudgetActivity
     *
     * @param string $f052fbudgetActivity
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fbudgetActivity($f052fbudgetActivity)
    {
        $this->f052fbudgetActivity = $f052fbudgetActivity;

        return $this;
    }

    /**
     * Get f052fbudgetActivity
     *
     * @return string
     */
    public function getF052fbudgetActivity()
    {
        return $this->f052fbudgetActivity;
    }

    /**
     * Set f052fbudgetDepartment
     *
     * @param string $f052fbudgetDepartment
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fbudgetDepartment($f052fbudgetDepartment)
    {
        $this->f052fbudgetDepartment = $f052fbudgetDepartment;

        return $this;
    }

    /**
     * Get f052fbudgetDepartment
     *
     * @return string
     */
    public function getF052fbudgetDepartment()
    {
        return $this->f052fbudgetDepartment;
    }

    /**
     * Set f052fbudgetAccount
     *
     * @param string $f052fbudgetAccount
     *
     * @return T052fcashApplicationDetails
     */
    public function setF052fbudgetAccount($f052fbudgetAccount)
    {
        $this->f052fbudgetAccount = $f052fbudgetAccount;

        return $this;
    }

    /**
     * Get f052fbudgetAccount
     *
     * @return string
     */
    public function getF052fbudgetAccount()
    {
        return $this->f052fbudgetAccount;
    }
}