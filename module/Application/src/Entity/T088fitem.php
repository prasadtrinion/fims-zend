<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T088fitem
 *
 * @ORM\Table(name="t088fitem" ,uniqueConstraints={@ORM\UniqueConstraint(name="f088fitem_code_UNIQUE", columns={"f088fitem_code"})}) 
 * @ORM\Entity(repositoryClass="Application\Repository\ItemRepository")
 */
 
class T088fitem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f088fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_item_group", type="integer", nullable=false)
     */
    private $f088fidItemGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_category", type="integer", nullable=false)
     */
    private $f088fidCategory;

     /**
     * @var integer
     *
     * @ORM\Column(name="f088famount", type="integer", nullable=false)
     */
    private $f088famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_sub_category", type="integer", nullable=false)
     */
    private $f088fidSubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_type", type="integer", nullable=false)
     */
    private $f088fidType;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_code", type="string", length=100, nullable=false)
     */
    private $f088fitemCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_description", type="string", length=100, nullable=false)
     */
    private $f088fitemDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fstatus", type="integer", nullable=false)
     */
    private $f088fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fcreated_by", type="integer", nullable=false)
     */
    private $f088fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fupdated_by", type="integer", nullable=false)
     */
    private $f088fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f088fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f088fupdatedDtTm;



    /**
     * Get f088fid
     *
     * @return integer
     */
    public function getF088fid()
    {
        return $this->f088fid;
    }

    /**
     * Set f088fidItemGroup
     *
     * @param integer $f088fidItemGroup
     *
     * @return T088fitem
     */
    public function setF088fidItemGroup($f088fidItemGroup)
    {
        $this->f088fidItemGroup = $f088fidItemGroup;

        return $this;
    }

    /**
     * Get f088fidItemGroup
     *
     * @return integer
     */
    public function getF088fidItemGroup()
    {
        return $this->f088fidItemGroup;
    }

     /**
     * Set f088famount
     *
     * @param integer $f088famount
     *
     * @return T088fitem
     */
    public function setF088famount($f088famount)
    {
        $this->f088famount = $f088famount;

        return $this;
    }

    /**
     * Get f088famount
     *
     * @return integer
     */
    public function getF088famount()
    {
        return $this->f088famount;
    }

    /**
     * Set f088fidCategory
     *
     * @param integer $f088fidCategory
     *
     * @return T088fitem
     */
    public function setF088fidCategory($f088fidCategory)
    {
        $this->f088fidCategory = $f088fidCategory;

        return $this;
    }

    /**
     * Get f088fidCategory
     *
     * @return integer
     */
    public function getF088fidCategory()
    {
        return $this->f088fidCategory;
    }

    /**
     * Set f088fidSubCategory
     *
     * @param integer $f088fidSubCategory
     *
     * @return T088fitem
     */
    public function setF088fidSubCategory($f088fidSubCategory)
    {
        $this->f088fidSubCategory = $f088fidSubCategory;

        return $this;
    }

    /**
     * Get f088fidSubCategory
     *
     * @return integer
     */
    public function getF088fidSubCategory()
    {
        return $this->f088fidSubCategory;
    }

    /**
     * Set f088fidType
     *
     * @param integer $f088fidType
     *
     * @return T088fitem
     */
    public function setF088fidType($f088fidType)
    {
        $this->f088fidType = $f088fidType;

        return $this;
    }

    /**
     * Get f088fidType
     *
     * @return integer
     */
    public function getF088fidType()
    {
        return $this->f088fidType;
    }

    /**
     * Set f088fitemCode
     *
     * @param string $f088fitemCode
     *
     * @return T088fitem
     */
    public function setF088fitemCode($f088fitemCode)
    {
        $this->f088fitemCode = $f088fitemCode;

        return $this;
    }

    /**
     * Get f088fitemCode
     *
     * @return string
     */
    public function getF088fitemCode()
    {
        return $this->f088fitemCode;
    }

    /**
     * Set f088fitemDescription
     *
     * @param string $f088fitemDescription
     *
     * @return T088fitem
     */
    public function setF088fitemDescription($f088fitemDescription)
    {
        $this->f088fitemDescription = $f088fitemDescription;

        return $this;
    }

    /**
     * Get f088fitemDescription
     *
     * @return string
     */
    public function getF088fitemDescription()
    {
        return $this->f088fitemDescription;
    }

    /**
     * Set f088fstatus
     *
     * @param integer $f088fstatus
     *
     * @return T088fitem
     */
    public function setF088fstatus($f088fstatus)
    {
        $this->f088fstatus = $f088fstatus;

        return $this;
    }

    /**
     * Get f088fstatus
     *
     * @return integer
     */
    public function getF088fstatus()
    {
        return $this->f088fstatus;
    }

    /**
     * Set f088fcreatedBy
     *
     * @param integer $f088fcreatedBy
     *
     * @return T088fitem
     */
    public function setF088fcreatedBy($f088fcreatedBy)
    {
        $this->f088fcreatedBy = $f088fcreatedBy;

        return $this;
    }

    /**
     * Get f088fcreatedBy
     *
     * @return integer
     */
    public function getF088fcreatedBy()
    {
        return $this->f088fcreatedBy;
    }

    /**
     * Set f088fupdatedBy
     *
     * @param integer $f088fupdatedBy
     *
     * @return T088fitem
     */
    public function setF088fupdatedBy($f088fupdatedBy)
    {
        $this->f088fupdatedBy = $f088fupdatedBy;

        return $this;
    }

    /**
     * Get f088fupdatedBy
     *
     * @return integer
     */
    public function getF088fupdatedBy()
    {
        return $this->f088fupdatedBy;
    }

    /**
     * Set f088fcreatedDtTm
     *
     * @param \DateTime $f088fcreatedDtTm
     *
     * @return T088fitem
     */
    public function setF088fcreatedDtTm($f088fcreatedDtTm)
    {
        $this->f088fcreatedDtTm = $f088fcreatedDtTm;

        return $this;
    }

    /**
     * Get f088fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fcreatedDtTm()
    {
        return $this->f088fcreatedDtTm;
    }

    /**
     * Set f088fupdatedDtTm
     *
     * @param \DateTime $f088fupdatedDtTm
     *
     * @return T088fitem
     */
    public function setF088fupdatedDtTm($f088fupdatedDtTm)
    {
        $this->f088fupdatedDtTm = $f088fupdatedDtTm;

        return $this;
    }

    /**
     * Get f088fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fupdatedDtTm()
    {
        return $this->f088fupdatedDtTm;
    }
}
