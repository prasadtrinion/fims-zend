<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T060fstudentInvoices
 *
 * @ORM\Table(name="t060fstudent_invoices")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentRepository")
 */
class T060fstudentInvoices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f060fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f060fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fid_student", type="string", length=50, nullable=false)
     */
    private $f060fidStudent;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fid_fee_structure", type="integer", nullable=false)
     */
    private $f060fidFeeStructure;

    /**
     * Get f060fid
     *
     * @return integer
     */
    public function getF060fid()
    {
        return $this->f060fid;
    }

    /**
     * Set f060fidStudent
     *
     * @param string $f060fidStudent
     *
     * @return T060fstudentInvoices
     */
    public function setF060fidStudent($f060fidStudent)
    {
        $this->f060fidStudent = $f060fidStudent;

        return $this;
    }

    /**
     * Get f060fidStudent
     *
     * @return string
     */
    public function getF060fidStudent()
    {
        return $this->f060fidStudent;
    }

    /**
     * Set f060fidFeeStructure
     *
     * @param string $f060fidFeeStructure
     *
     * @return T060fstudentInvoices
     */
    public function setF060fidFeeStructure($f060fidFeeStructure)
    {
        $this->f060fidFeeStructure = $f060fidFeeStructure;

        return $this;
    }

    /**
     * Get f060fidFeeStructure
     *
     * @return string
     */
    public function getF060fidFeeStructure()
    {
        return $this->f060fidFeeStructure;
    }

   
}
?>
