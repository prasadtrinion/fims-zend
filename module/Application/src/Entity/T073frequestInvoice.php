<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T073frequestInvoice
 *
 * @ORM\Table(name="t073frequest_invoice")
 * @ORM\Entity(repositoryClass="Application\Repository\RequestInvoiceRepository")
 */
class T073frequestInvoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f071fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f071fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f071finvoice_number", type="string", length=50, nullable=false)
     */
    private $f071finvoiceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f071frevenue_type", type="integer", nullable=false)
     */
    private $f071frevenueType;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fid_customer", type="integer", nullable=true)
     */
    private $f071fidCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="f071finvoice_type", type="string", length=50, nullable=false)
     */
    private $f071finvoiceType;

     /**
     * @var string
     *
     * @ORM\Column(name="f071fdescription", type="string", length=100, nullable=false)
     */
    private $f071fdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071finvoice_date", type="datetime", nullable=false)
     */
    private $f071finvoiceDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fapproved_by", type="integer", nullable=false)
     */
    private $f071fapprovedBy;

     /**
     * @var integer
     *
     * @ORM\Column(name="f071fid_financial_year", type="integer", nullable=false)
     */
    private $f071fidFinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fstatus", type="integer", nullable=false)
     */
    private $f071fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fcreated_by", type="integer", nullable=false)
     */
    private $f071fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fupdated_by", type="integer", nullable=false)
     */
    private $f071fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071finvoice_total", type="integer", nullable=false)
     */
    private $f071finvoiceTotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fis_rcp", type="integer", nullable=true)
     */
    private $f071fisRcp;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071finvoice_from", type="integer", nullable=true)
     */
    private $f071finvoiceFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="f071freason", type="string", length=60, nullable=true)
     */
    private $f071freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fcustomer_name", type="integer",  nullable=true)
     */
    private $f071fcustomerName;



    /**
     * Get f071fid
     *
     * @return integer
     */
    public function getF071fid()
    {
        return $this->f071fid;
    }

    /**
     * Set f071finvoiceNumber
     *
     * @param string $f071finvoiceNumber
     *
     * @return T073frequestInvoice
     */
    public function setF071finvoiceNumber($f071finvoiceNumber)
    {
        $this->f071finvoiceNumber = $f071finvoiceNumber;

        return $this;
    }

    /**
     * Get f071finvoiceNumber
     *
     * @return string
     */
    public function getF071finvoiceNumber()
    {
        return $this->f071finvoiceNumber;
    }

     /**
     * Set f071frevenueType
     *
     * @param string $f071frevenueType
     *
     * @return T073frequestInvoice
     */
    public function setF071frevenueType($f071frevenueType)
    {
        $this->f071frevenueType = $f071frevenueType;

        return $this;
    }

    /**
     * Get f071frevenueType
     *
     * @return string
     */
    public function getF071frevenueType()
    {
        return $this->f071frevenueType;
    }

    /**
     * Set f071fidCustomer
     *
     * @param integer $f071fidCustomer
     *
     * @return T073frequestInvoice
     */
    public function setF071fidCustomer($f071fidCustomer)
    {
        $this->f071fidCustomer = $f071fidCustomer;

        return $this;
    }

    /**
     * Get f071fidCustomer
     *
     * @return integer
     */
    public function getF071fidCustomer()
    {
        return $this->f071fidCustomer;
    }

    /**
     * Set f071finvoiceType
     *
     * @param string $f071finvoiceType
     *
     * @return T073frequestInvoice
     */
    public function setF071finvoiceType($f071finvoiceType)
    {
        $this->f071finvoiceType = $f071finvoiceType;

        return $this;
    }

    /**
     * Get f071finvoiceType
     *
     * @return string
     */
    public function getF071finvoiceType()
    {
        return $this->f071finvoiceType;
    }

    /**
     * Set f071fdescription
     *
     * @param string $f071fdescription
     *
     * @return T073frequestInvoice
     */
    public function setF071fdescription($f071fdescription)
    {
        $this->f071fdescription = $f071fdescription;

        return $this;
    }

    /**
     * Get f071fdescription
     *
     * @return string
     */
    public function getF071fdescription()
    {
        return $this->f071fdescription;
    }

    /**
     * Set f071finvoiceDate
     *
     * @param \Date $f071finvoiceDate
     *
     * @return T073frequestInvoice
     */
    public function setF071finvoiceDate($f071finvoiceDate)
    {
        $this->f071finvoiceDate = $f071finvoiceDate;

        return $this;
    }

    /**
     * Get f071finvoiceDate
     *
     * @return \Date
     */
    public function getF071finvoiceDate()
    {
        return $this->f071finvoiceDate;
    }

    /**
     * Set f071fapprovedBy
     *
     * @param integer $f071fapprovedBy
     *
     * @return T073frequestInvoice
     */
    public function setF071fapprovedBy($f071fapprovedBy)
    {
        $this->f071fapprovedBy = $f071fapprovedBy;

        return $this;
    }

    /**
     * Get f071fapprovedBy
     *
     * @return integer
     */
    public function getF071fapprovedBy()
    {
        return $this->f071fapprovedBy;
    }

    /**
     * Set f071fidFinancialYear
     *
     * @param integer $f071fidFinancialYear
     *
     * @return T073frequestInvoice
     */
    public function setF071fidFinancialYear($f071fidFinancialYear)
    {
        $this->f071fidFinancialYear = $f071fidFinancialYear;

        return $this;
    }

    /**
     * Get f071fidFinancialYear
     *
     * @return integer
     */
    public function getF071fidFinancialYear()
    {
        return $this->f071fidFinancialYear;
    }

    /**
     * Set f071fstatus
     *
     * @param integer $f071fstatus
     *
     * @return T073frequestInvoice
     */
    public function setF071fstatus($f071fstatus)
    {
        $this->f071fstatus = $f071fstatus;

        return $this;
    }

    /**
     * Get f071fstatus
     *
     * @return integer
     */
    public function getF071fstatus()
    {
        return $this->f071fstatus;
    }

    /**
     * Set f071fcreatedBy
     *
     * @param integer $f071fcreatedBy
     *
     * @return T073frequestInvoice
     */
    public function setF071fcreatedBy($f071fcreatedBy)
    {
        $this->f071fcreatedBy = $f071fcreatedBy;

        return $this;
    }

    /**
     * Get f071fcreatedBy
     *
     * @return integer
     */
    public function getF071fcreatedBy()
    {
        return $this->f071fcreatedBy;
    }

    /**
     * Set f071fupdatedBy
     *
     * @param integer $f071fupdatedBy
     *
     * @return T073frequestInvoice
     */
    public function setF071fupdatedBy($f071fupdatedBy)
    {
        $this->f071fupdatedBy = $f071fupdatedBy;

        return $this;
    }

    /**
     * Get f071fupdatedBy
     *
     * @return integer
     */
    public function getF071fupdatedBy()
    {
        return $this->f071fupdatedBy;
    }

    /**
     * Set f071fcreatedDtTm
     *
     * @param \DateTime $f071fcreatedDtTm
     *
     * @return T073frequestInvoice
     */
    public function setF071fcreatedDtTm($f071fcreatedDtTm)
    {
        $this->f071fcreatedDtTm = $f071fcreatedDtTm;

        return $this;
    }

    /**
     * Get f071fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fcreatedDtTm()
    {
        return $this->f071fcreatedDtTm;
    }

    /**
     * Set f071fupdatedDtTm
     *
     * @param \DateTime $f071fupdatedDtTm
     *
     * @return T073frequestInvoice
     */
    public function setF071fupdatedDtTm($f071fupdatedDtTm)
    {
        $this->f071fupdatedDtTm = $f071fupdatedDtTm;

        return $this;
    }

    /**
     * Get f071fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fupdatedDtTm()
    {
        return $this->f071fupdatedDtTm;
    }

    /**
     * Set f071finvoiceTotal
     *
     * @param integer $f071finvoiceTotal
     *
     * @return T073frequestInvoice
     */
    public function setF071finvoiceTotal($f071finvoiceTotal)
    {
        $this->f071finvoiceTotal = (float)$f071finvoiceTotal;

        return $this;
    }


    /**
     * Get f071finvoiceTotal
     *
     * @return integer
     */
    public function getF071finvoiceTotal()
    {
        return $this->f071finvoiceTotal;
    }

    /**
     * Get f071fisRcp
     *
     * @return integer
     */
    public function getF071fisRcp()
    {
        return $this->f071fisRcp;
    }

    /**
     * Set f071fisRcp
     *
     * @param integer $f071fisRcp
     *
     * @return T073frequestInvoice
     */
    public function setF071fisRcp($f071fisRcp)
    {
        $this->f071fisRcp = $f071fisRcp;

        return $this;
    }
       /**
     * Get f071finvoiceFrom
     *
     * @return integer
     */
    public function getF071finvoiceFrom()
    {
        return $this->f071finvoiceFrom;
    }

    /**
     * Set f071finvoiceFrom
     *
     * @param integer $f071finvoiceFrom
     *
     * @return T071finvoice
     */
    public function setF071finvoiceFrom($f071finvoiceFrom)
    {
        $this->f071finvoiceFrom = $f071finvoiceFrom;

        return $this;
    }

    /**
     * Set f071freason
     *
     * @param string $f071freason
     *
     * @return T073frequestInvoice
     */
    public function setF071freason($f071freason)
    {
        $this->f071freason = $f071freason;

        return $this;
    }

    /**
     * Get f071freason
     *
     * @return string
     */
    public function getF071freason()
    {
        return $this->f071freason;
    }

    /**
     * Set f071fcustomerName
     *
     * @param string $f071fcustomerName
     *
     * @return T073frequestInvoice
     */
    public function setF071customerName($f071fcustomerName)
    {
        $this->f071fcustomerName = $f071fcustomerName;

        return $this;
    }

    /**
     * Get f071fcustomerName
     *
     * @return string
     */
    public function getF071fcustomerName()
    {
        return $this->f071fcustomerName;
    }

}

