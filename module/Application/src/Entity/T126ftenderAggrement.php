<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T126ftenderAggrement
 *
 * @ORM\Table(name="t126ftender_aggrement", uniqueConstraints={@ORM\UniqueConstraint(name="f126fstore_code", columns={"f126fstore_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TenderAggrementRepository")
 */
class T126ftenderAggrement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f126fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f126fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f126fregister_no", type="string", length=50, nullable=false)
     */
    private $f126fregisterNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f126fid_tender", type="integer", nullable=false)
     */
    private $f126fidTender;

    /**
     * @var string
     *
     * @ORM\Column(name="f126fid_vender", type="integer", nullable=false)
     */
    private $f126fidVender;

    /**
     * @var integer
     *
     * @ORM\Column(name="f126fstatus", type="integer", nullable=false)
     */
    private $f126fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f126fcreated_by", type="integer", nullable=false)
     */
    private $f126fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f126fupdated_by", type="integer", nullable=false)
     */
    private $f126fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f126fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f126fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f126fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f126fupdatedDtTm;



    /**
     * Get f126fid
     *
     * @return integer
     */
    public function getF126fid()
    {
        return $this->f126fid;
    }

    /**
     * Set f126fregisterNo
     *
     * @param string $f126fregisterNo
     *
     * @return T126ftenderAggrement
     */
    public function setF126fregisterNo($f126fregisterNo)
    {
        $this->f126fregisterNo = $f126fregisterNo;

        return $this;
    }

    /**
     * Get f126fregisterNo
     *
     * @return string
     */
    public function getF126fregisterNo()
    {
        return $this->f126fregisterNo;
    }

    /**
     * Set f126fidTender
     *
     * @param string $f126fidTender
     *
     * @return T126ftenderAggrement
     */
    public function setF126fidTender($f126fidTender)
    {
        $this->f126fidTender = $f126fidTender;

        return $this;
    }

    /**
     * Get f126fidTender
     *
     * @return string
     */
    public function getF126fidTender()
    {
        return $this->f126fidTender;
    }

    /**
     * Set f126fidVender
     *
     * @param string $f126fidVender
     *
     * @return T126ftenderAggrement
     */
    public function setF126fidVender($f126fidVender)
    {
        $this->f126fidVender = $f126fidVender;

        return $this;
    }

    /**
     * Get f126fidVender
     *
     * @return string
     */
    public function getF126fidVender()
    {
        return $this->f126fidVender;
    }

    /**
     * Set f126fstatus
     *
     * @param integer $f126fstatus
     *
     * @return T126ftenderAggrement
     */
    public function setF126fstatus($f126fstatus)
    {
        $this->f126fstatus = $f126fstatus;

        return $this;
    }

    /**
     * Get f126fstatus
     *
     * @return integer
     */
    public function getF126fstatus()
    {
        return $this->f126fstatus;
    }


    /**
     * Set f126fcreatedBy
     *
     * @param integer $f126fcreatedBy
     *
     * @return T126ftenderAggrement
     */
    public function setF126fcreatedBy($f126fcreatedBy)
    {
        $this->f126fcreatedBy = $f126fcreatedBy;

        return $this;
    }

    /**
     * Get f126fcreatedBy
     *
     * @return integer
     */
    public function getF126fcreatedBy()
    {
        return $this->f126fcreatedBy;
    }

    /**
     * Set f126fupdatedBy
     *
     * @param integer $f126fupdatedBy
     *
     * @return T126ftenderAggrement
     */
    public function setF126fupdatedBy($f126fupdatedBy)
    {
        $this->f126fupdatedBy = $f126fupdatedBy;

        return $this;
    }

    /**
     * Get f126fupdatedBy
     *
     * @return integer
     */
    public function getF126fupdatedBy()
    {
        return $this->f126fupdatedBy;
    }

    /**
     * Set f126fcreatedDtTm
     *
     * @param \DateTime $f126fcreatedDtTm
     *
     * @return T126ftenderAggrement
     */
    public function setF126fcreatedDtTm($f126fcreatedDtTm)
    {
        $this->f126fcreatedDtTm = $f126fcreatedDtTm;

        return $this;
    }

    /**
     * Get f126fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF126fcreatedDtTm()
    {
        return $this->f126fcreatedDtTm;
    }

    /**
     * Set f126fupdatedDtTm
     *
     * @param \DateTime $f126fupdatedDtTm
     *
     * @return T126ftenderAggrement
     */
    public function setF126fupdatedDtTm($f126fupdatedDtTm)
    {
        $this->f126fupdatedDtTm = $f126fupdatedDtTm;

        return $this;
    }

    /**
     * Get f126fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF126fupdatedDtTm()
    {
        return $this->f126fupdatedDtTm;
    }
}
