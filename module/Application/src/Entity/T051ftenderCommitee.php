<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T051ftenderCommitee
 *
 * @ORM\Table(name="t051ftender_commitee", indexes={@ORM\Index(name="f051fid_submission", columns={"f051fid_submission"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSubmissionRepository")
 */
class T051ftenderCommitee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f051fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid_tender", type="integer", nullable=false)
     */
    private $f051fidTender;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid_staff", type="integer", nullable=false)
     */
    private $f051fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f051fdescription", type="string", length=100, nullable=true)
     */
    private $f051fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fstatus", type="integer", nullable=true)
     */
    private $f051fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fcreated_by", type="integer", nullable=true)
     */
    private $f051fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fupdated_by", type="integer", nullable=true)
     */
    private $f051fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fupdatedDtTm;

    /**
     * @ORM\Column(name="f051fid_submission", type="integer", nullable=true)

     * })
     */
    private $f051fidSubmission;

    

    /**
     * Get f051fid
     *
     * @return integer
     */
    public function getF051fid()
    {
        return $this->f051fid;
    }

    /**
     * Set f051fidTender
     *
     * @param integer $f051fidTender
     *
     * @return T051ftenderCommitee
     */
    public function setF051fidTender($f051fidTender)
    {
        $this->f051fidTender = $f051fidTender;

        return $this;
    }

    /**
     * Get f051fidTender
     *
     * @return integer
     */
    public function getF051fidTender()
    {
        return $this->f051fidTender;
    }

    /**
     * Set f051fidStaff
     *
     * @param integer $f051fidStaff
     *
     * @return T051ftenderCommitee
     */
    public function setF051fidStaff($f051fidStaff)
    {
        $this->f051fidStaff = $f051fidStaff;

        return $this;
    }

    /**
     * Get f051fidStaff
     *
     * @return integer
     */
    public function getF051fidStaff()
    {
        return $this->f051fidStaff;
    }

    /**
     * Set f051fdescription
     *
     * @param string $f051fdescription
     *
     * @return T051ftenderCommitee
     */
    public function setF051fdescription($f051fdescription)
    {
        $this->f051fdescription = $f051fdescription;

        return $this;
    }

    /**
     * Get f051fdescription
     *
     * @return string
     */
    public function getF051fdescription()
    {
        return $this->f051fdescription;
    }

    /**
     * Set f051fstatus
     *
     * @param integer $f051fstatus
     *
     * @return T051ftenderCommitee
     */
    public function setF051fstatus($f051fstatus)
    {
        $this->f051fstatus = $f051fstatus;

        return $this;
    }

    /**
     * Get f051fstatus
     *
     * @return integer
     */
    public function getF051fstatus()
    {
        return $this->f051fstatus;
    }

    /**
     * Set f051fcreatedBy
     *
     * @param integer $f051fcreatedBy
     *
     * @return T051ftenderCommitee
     */
    public function setF051fcreatedBy($f051fcreatedBy)
    {
        $this->f051fcreatedBy = $f051fcreatedBy;

        return $this;
    }

    /**
     * Get f051fcreatedBy
     *
     * @return integer
     */
    public function getF051fcreatedBy()
    {
        return $this->f051fcreatedBy;
    }

    /**
     * Set f051fupdatedBy
     *
     * @param integer $f051fupdatedBy
     *
     * @return T051ftenderCommitee
     */
    public function setF051fupdatedBy($f051fupdatedBy)
    {
        $this->f051fupdatedBy = $f051fupdatedBy;

        return $this;
    }

    /**
     * Get f051fupdatedBy
     *
     * @return integer
     */
    public function getF051fupdatedBy()
    {
        return $this->f051fupdatedBy;
    }

    /**
     * Set f051fcreatedDtTm
     *
     * @param \DateTime $f051fcreatedDtTm
     *
     * @return T051ftenderCommitee
     */
    public function setF051fcreatedDtTm($f051fcreatedDtTm)
    {
        $this->f051fcreatedDtTm = $f051fcreatedDtTm;

        return $this;
    }

    /**
     * Get f051fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fcreatedDtTm()
    {
        return $this->f051fcreatedDtTm;
    }

    /**
     * Set f051fupdatedDtTm
     *
     * @param \DateTime $f051fupdatedDtTm
     *
     * @return T051ftenderCommitee
     */
    public function setF051fupdatedDtTm($f051fupdatedDtTm)
    {
        $this->f051fupdatedDtTm = $f051fupdatedDtTm;

        return $this;
    }

    /**
     * Get f051fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fupdatedDtTm()
    {
        return $this->f051fupdatedDtTm;
    }

    /**
     * Set f051fidSubmission
     *
     * @param \Application\Entity\T050ftenderSubmission $f051fidSubmission
     *
     * @return T051ftenderCommitee
     */
    public function setF051fidSubmission( $f051fidSubmission = null)
    {
        $this->f051fidSubmission = $f051fidSubmission;

        return $this;
    }

    /**
     * Get f051fidSubmission
     *
     * @return \Application\Entity\T050ftenderSubmission
     */
    public function getF051fidSubmission()
    {
        return $this->f051fidSubmission;
    }
}

