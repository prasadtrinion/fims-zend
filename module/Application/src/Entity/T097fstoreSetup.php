<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T097fstoreSetup
 *
 * @ORM\Table(name="t097fstore_setup", uniqueConstraints={@ORM\UniqueConstraint(name="f097fstore_code", columns={"f097fstore_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\StoreSetupRepository")
 */
class T097fstoreSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f097fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f097fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fstore_code", type="string", length=50, nullable=false)
     */
    private $f097fstoreCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fstore_name", type="string", length=100, nullable=false)
     */
    private $f097fstoreName;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fdepartment", type="integer", nullable=false)
     */
    private $f097fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fstatus", type="integer", nullable=false)
     */
    private $f097fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fperson_in_charge", type="integer", nullable=false)
     */
    private $f097fpersonInCharge;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fcreated_by", type="integer", nullable=false)
     */
    private $f097fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fupdated_by", type="integer", nullable=false)
     */
    private $f097fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f097fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f097fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f097fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f097fupdatedDtTm;



    /**
     * Get f097fid
     *
     * @return integer
     */
    public function getF097fid()
    {
        return $this->f097fid;
    }

    /**
     * Set f097fstoreCode
     *
     * @param string $f097fstoreCode
     *
     * @return T097fstoreSetup
     */
    public function setF097fstoreCode($f097fstoreCode)
    {
        $this->f097fstoreCode = $f097fstoreCode;

        return $this;
    }

    /**
     * Get f097fstoreCode
     *
     * @return string
     */
    public function getF097fstoreCode()
    {
        return $this->f097fstoreCode;
    }

    /**
     * Set f097fstoreName
     *
     * @param string $f097fstoreName
     *
     * @return T097fstoreSetup
     */
    public function setF097fstoreName($f097fstoreName)
    {
        $this->f097fstoreName = $f097fstoreName;

        return $this;
    }

    /**
     * Get f097fstoreName
     *
     * @return string
     */
    public function getF097fstoreName()
    {
        return $this->f097fstoreName;
    }

    /**
     * Set f097fdepartment
     *
     * @param string $f097fdepartment
     *
     * @return T097fstoreSetup
     */
    public function setF097fdepartment($f097fdepartment)
    {
        $this->f097fdepartment = $f097fdepartment;

        return $this;
    }

    /**
     * Get f097fdepartment
     *
     * @return string
     */
    public function getF097fdepartment()
    {
        return $this->f097fdepartment;
    }

    /**
     * Set f097fstatus
     *
     * @param integer $f097fstatus
     *
     * @return T097fstoreSetup
     */
    public function setF097fstatus($f097fstatus)
    {
        $this->f097fstatus = $f097fstatus;

        return $this;
    }

    /**
     * Get f097fstatus
     *
     * @return integer
     */
    public function getF097fstatus()
    {
        return $this->f097fstatus;
    }

    /**
     * Set f097fpersonInCharge
     *
     * @param integer $f097fpersonInCharge
     *
     * @return T097fstoreSetup
     */
    public function setF097fpersonInCharge($f097fpersonInCharge)
    {
        $this->f097fpersonInCharge = $f097fpersonInCharge;

        return $this;
    }

    /**
     * Get f097fpersonInCharge
     *
     * @return integer
     */
    public function getF097fpersonInCharge()
    {
        return $this->f097fpersonInCharge;
    }

    /**
     * Set f097fcreatedBy
     *
     * @param integer $f097fcreatedBy
     *
     * @return T097fstoreSetup
     */
    public function setF097fcreatedBy($f097fcreatedBy)
    {
        $this->f097fcreatedBy = $f097fcreatedBy;

        return $this;
    }

    /**
     * Get f097fcreatedBy
     *
     * @return integer
     */
    public function getF097fcreatedBy()
    {
        return $this->f097fcreatedBy;
    }

    /**
     * Set f097fupdatedBy
     *
     * @param integer $f097fupdatedBy
     *
     * @return T097fstoreSetup
     */
    public function setF097fupdatedBy($f097fupdatedBy)
    {
        $this->f097fupdatedBy = $f097fupdatedBy;

        return $this;
    }

    /**
     * Get f097fupdatedBy
     *
     * @return integer
     */
    public function getF097fupdatedBy()
    {
        return $this->f097fupdatedBy;
    }

    /**
     * Set f097fcreatedDtTm
     *
     * @param \DateTime $f097fcreatedDtTm
     *
     * @return T097fstoreSetup
     */
    public function setF097fcreatedDtTm($f097fcreatedDtTm)
    {
        $this->f097fcreatedDtTm = $f097fcreatedDtTm;

        return $this;
    }

    /**
     * Get f097fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF097fcreatedDtTm()
    {
        return $this->f097fcreatedDtTm;
    }

    /**
     * Set f097fupdatedDtTm
     *
     * @param \DateTime $f097fupdatedDtTm
     *
     * @return T097fstoreSetup
     */
    public function setF097fupdatedDtTm($f097fupdatedDtTm)
    {
        $this->f097fupdatedDtTm = $f097fupdatedDtTm;

        return $this;
    }

    /**
     * Get f097fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF097fupdatedDtTm()
    {
        return $this->f097fupdatedDtTm;
    }
}
