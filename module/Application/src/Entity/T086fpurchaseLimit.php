<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T086fpurchaseLimit
 *
 * @ORM\Table(name="t086fpurchase_limit")
 * @ORM\Entity(repositoryClass="Application\Repository\PurchaseLimitRepository")
 */
class T086fpurchaseLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f086fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086ftype", type="integer", nullable=true)
     */
    private $f086ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fmin_amount", type="integer", nullable=true)
     */
    private $f086fminAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fmax_amount", type="integer", nullable=true)
     */
    private $f086fmaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fstatus", type="integer", nullable=true)
     */
    private $f086fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fcreated_by", type="integer", nullable=true)
     */
    private $f086fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fupdated_by", type="integer", nullable=true)
     */
    private $f086fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fupdatedDtTm;



    /**
     * Get f086fid
     *
     * @return integer
     */
    public function getF086fid()
    {
        return $this->f086fid;
    }

    /**
     * Set f086ftype
     *
     * @param integer $f086ftype
     *
     * @return T086fpurchaseLimit
     */
    public function setF086ftype($f086ftype)
    {
        $this->f086ftype = $f086ftype;

        return $this;
    }

    /**
     * Get f086ftype
     *
     * @return integer
     */
    public function getF086ftype()
    {
        return $this->f086ftype;
    }

    /**
     * Set f086fminAmount
     *
     * @param integer $f086fminAmount
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fminAmount($f086fminAmount)
    {
        $this->f086fminAmount = $f086fminAmount;

        return $this;
    }

    /**
     * Get f086fminAmount
     *
     * @return integer
     */
    public function getF086fminAmount()
    {
        return $this->f086fminAmount;
    }

    /**
     * Set f086fmaxAmount
     *
     * @param integer $f086fmaxAmount
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fmaxAmount($f086fmaxAmount)
    {
        $this->f086fmaxAmount = $f086fmaxAmount;

        return $this;
    }

    /**
     * Get f086fmaxAmount
     *
     * @return integer
     */
    public function getF086fmaxAmount()
    {
        return $this->f086fmaxAmount;
    }

    /**
     * Set f086fstatus
     *
     * @param integer $f086fstatus
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fstatus($f086fstatus)
    {
        $this->f086fstatus = $f086fstatus;

        return $this;
    }

    /**
     * Get f086fstatus
     *
     * @return integer
     */
    public function getF086fstatus()
    {
        return $this->f086fstatus;
    }

    /**
     * Set f086fcreatedBy
     *
     * @param integer $f086fcreatedBy
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fcreatedBy($f086fcreatedBy)
    {
        $this->f086fcreatedBy = $f086fcreatedBy;

        return $this;
    }

    /**
     * Get f086fcreatedBy
     *
     * @return integer
     */
    public function getF086fcreatedBy()
    {
        return $this->f086fcreatedBy;
    }

    /**
     * Set f086fupdatedBy
     *
     * @param integer $f086fupdatedBy
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fupdatedBy($f086fupdatedBy)
    {
        $this->f086fupdatedBy = $f086fupdatedBy;

        return $this;
    }

    /**
     * Get f086fupdatedBy
     *
     * @return integer
     */
    public function getF086fupdatedBy()
    {
        return $this->f086fupdatedBy;
    }

    /**
     * Set f086fcreatedDtTm
     *
     * @param \DateTime $f086fcreatedDtTm
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fcreatedDtTm($f086fcreatedDtTm)
    {
        $this->f086fcreatedDtTm = $f086fcreatedDtTm;

        return $this;
    }

    /**
     * Get f086fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fcreatedDtTm()
    {
        return $this->f086fcreatedDtTm;
    }

    /**
     * Set f086fupdatedDtTm
     *
     * @param \DateTime $f086fupdatedDtTm
     *
     * @return T086fpurchaseLimit
     */
    public function setF086fupdatedDtTm($f086fupdatedDtTm)
    {
        $this->f086fupdatedDtTm = $f086fupdatedDtTm;

        return $this;
    }

    /**
     * Get f086fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fupdatedDtTm()
    {
        return $this->f086fupdatedDtTm;
    }
}
