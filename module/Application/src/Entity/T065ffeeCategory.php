<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T065ffeeCategory
 *
 * @ORM\Table(name="t065ffee_category",uniqueConstraints={@ORM\UniqueConstraint(name="f065fname_UNIQUE", columns={"f065fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\FeeCategoryRepository")
 */
class T065ffeeCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f065fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f065fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f065fname", type="string", length=25, nullable=false)
     */
    private $f065fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f065fcode", type="string", length=10, nullable=true)
     */
    private $f065fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f065fdescription", type="string", length=50, nullable=true)
     */
    private $f065fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fstatus", type="integer", nullable=false)
     */
    private $f065fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fcreated_by", type="integer", nullable=true)
     */
    private $f065fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fupdated_by", type="integer", nullable=true)
     */
    private $f065fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f065fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f065fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f065fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f065fupdatedDtTm;



    /**
     * Get f065fid
     *
     * @return integer
     */
    public function getF065fid()
    {
        return $this->f065fid;
    }

    /**
     * Set f065fname
     *
     * @param string $f065fname
     *
     * @return T065ffeeCategory
     */
    public function setF065fname($f065fname)
    {
        $this->f065fname = $f065fname;

        return $this;
    }

    /**
     * Get f065fname
     *
     * @return string
     */
    public function getF065fname()
    {
        return $this->f065fname;
    }

    /**
     * Set f065fcode
     *
     * @param string $f065fcode
     *
     * @return T065ffeeCategory
     */
    public function setF065fcode($f065fcode)
    {
        $this->f065fcode = $f065fcode;

        return $this;
    }

    /**
     * Get f065fcode
     *
     * @return string
     */
    public function getF065fcode()
    {
        return $this->f065fcode;
    }

    /**
     * Set f065fdescription
     *
     * @param string $f065fdescription
     *
     * @return T065ffeeCategory
     */
    public function setF065fdescription($f065fdescription)
    {
        $this->f065fdescription = $f065fdescription;

        return $this;
    }

    /**
     * Get f065fdescription
     *
     * @return string
     */
    public function getF065fdescription()
    {
        return $this->f065fdescription;
    }

    /**
     * Set f065fstatus
     *
     * @param integer $f065fstatus
     *
     * @return T065ffeeCategory
     */
    public function setF065fstatus($f065fstatus)
    {
        $this->f065fstatus = $f065fstatus;

        return $this;
    }

    /**
     * Get f065fstatus
     *
     * @return integer
     */
    public function getF065fstatus()
    {
        return $this->f065fstatus;
    }

    /**
     * Set f065fcreatedBy
     *
     * @param integer $f065fcreatedBy
     *
     * @return T065ffeeCategory
     */
    public function setF065fcreatedBy($f065fcreatedBy)
    {
        $this->f065fcreatedBy = $f065fcreatedBy;

        return $this;
    }

    /**
     * Get f065fcreatedBy
     *
     * @return integer
     */
    public function getF065fcreatedBy()
    {
        return $this->f065fcreatedBy;
    }

    /**
     * Set f065fupdatedBy
     *
     * @param integer $f065fupdatedBy
     *
     * @return T065ffeeCategory
     */
    public function setF065fupdatedBy($f065fupdatedBy)
    {
        $this->f065fupdatedBy = $f065fupdatedBy;

        return $this;
    }

    /**
     * Get f065fupdatedBy
     *
     * @return integer
     */
    public function getF065fupdatedBy()
    {
        return $this->f065fupdatedBy;
    }

    /**
     * Set f065fcreatedDtTm
     *
     * @param \DateTime $f065fcreatedDtTm
     *
     * @return T065ffeeCategory
     */
    public function setF065fcreatedDtTm($f065fcreatedDtTm)
    {
        $this->f065fcreatedDtTm = $f065fcreatedDtTm;

        return $this;
    }

    /**
     * Get f065fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF065fcreatedDtTm()
    {
        return $this->f065fcreatedDtTm;
    }

    /**
     * Set f065fupdatedDtTm
     *
     * @param \DateTime $f065fupdatedDtTm
     *
     * @return T065ffeeCategory
     */
    public function setF065fupdatedDtTm($f065fupdatedDtTm)
    {
        $this->f065fupdatedDtTm = $f065fupdatedDtTm;

        return $this;
    }

    /**
     * Get f065fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF065fupdatedDtTm()
    {
        return $this->f065fupdatedDtTm;
    }
}
