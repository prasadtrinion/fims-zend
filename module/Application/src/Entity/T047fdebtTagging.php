<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T047fdebtTagging
 *
 * @ORM\Table(name="t047fdebt_tagging")
 * @ORM\Entity(repositoryClass="Application\Repository\DebtTaggingRepository")
 */
class T047fdebtTagging
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f047fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f047fid;

     /**
     * @var integer
     *
     * @ORM\Column(name="f047fid_debt", type="integer", length=10, nullable=true)
     */
    private $f047fidDebt;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fid_student", type="integer", length=10, nullable=true)
     */
    private $f047fidStudent;

     /**
     * @var integer
     *
     * @ORM\Column(name="f047ftotal_debt", type="integer", nullable=true)
     */
    private $f047ftotalDebt;

    /**
     * @var string
     *
     * @ORM\Column(name="f047fdebt_details", type="string", length=250, nullable=true)
     */
    private $f047fdebtDetails;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f047fstatus", type="integer", nullable=true)
     */
    private $f047fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fcreated_by", type="integer", nullable=true)
     */
    private $f047fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fupdated_by", type="integer", nullable=true)
     */
    private $f047fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f047fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f047fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f047fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f047fupdatedDtTm;

    /**
     * Get f047fid
     *
     * @return integer
     */
    public function getF047fid()
    {
        return $this->f047fid;
    }

    
    /**
     * Set f047fidDebt
     *
     * @param integer $f047fidDebt
     *
     * @return T047fdebtTagging
     */
    public function setf047FidDebt($f047fidDebt)
    {
        $this->f047fidDebt = $f047fidDebt;

        return $this;
    }

    /**
     * Get f047fidDebt
     *
     * @return integer
     */
    public function getf047FidDebt()
    {
        return $this->f047fidDebt;
    }

    /**
     * Set f047fidStudent
     *
     * @param integer $f047fidStudent
     *
     * @return T047fdebtTagging
     */
    public function setf047fidStudent($f047fidStudent)
    {
        $this->f047fidStudent = $f047fidStudent;

        return $this;
    }

    /**
     * Get f047fidStudent
     *
     * @return integer
     */
    public function getf047fidStudent()
    {
        return $this->f047fidStudent;
    }

    /**
     * Set f047ftotalDebt
     *
     * @param integer $f047ftotalDebt
     *
     * @return T054factivityDetails
     */
    public function setF047ftotalDebt($f047ftotalDebt)
    {
        $this->f047ftotalDebt = $f047ftotalDebt;

        return $this;
    }

    /**
     * Get f047ftotalDebt
     *
     * @return integer
     */
    public function getF047ftotalDebt()
    {
        return $this->f047ftotalDebt;
    }

    /**
     * Set f047fdebtDetails
     *
     * @param string $f047fdebtDetails
     *
     * @return T047fdebtTagging
     */
    public function setF047fdebtDetails($f047fdebtDetails)
    {
        $this->f047fdebtDetails = $f047fdebtDetails;

        return $this;
    }

    /**
     * Get f047fdebtDetails
     *
     * @return string
     */
    public function getF047fdebtDetails()
    {
        return $this->f047fdebtDetails;
    }

    /**
     * Set f047fstatus
     *
     * @param integer $f047fstatus
     *
     * @return T047fdebtTagging
     */
    public function setF047fstatus($f047fstatus)
    {
        $this->f047fstatus = $f047fstatus;

        return $this;
    }

    /**
     * Get f047fstatus
     *
     * @return integer
     */
    public function getF047fstatus()
    {
        return $this->f047fstatus;
    }

    /**
     * Set f047fcreatedBy
     *
     * @param integer $f047fcreatedBy
     *
     * @return T047fdebtTagging
     */
    public function setF047fcreatedBy($f047fcreatedBy)
    {
        $this->f047fcreatedBy = $f047fcreatedBy;

        return $this;
    }

    /**
     * Get f047fcreatedBy
     *
     * @return integer
     */
    public function getF047fcreatedBy()
    {
        return $this->f047fcreatedBy;
    }

    /**
     * Set f047fupdatedBy
     *
     * @param integer $f047fupdatedBy
     *
     * @return T047fdebtTagging
     */
    public function setF047fupdatedBy($f047fupdatedBy)
    {
        $this->f047fupdatedBy = $f047fupdatedBy;

        return $this;
    }

    /**
     * Get f047fupdatedBy
     *
     * @return integer
     */
    public function getF047fupdatedBy()
    {
        return $this->f047fupdatedBy;
    }

    /**
     * Set f047fcreatedDtTm
     *
     * @param \DateTime $f047fcreatedDtTm
     *
     * @return T047fdebtTagging
     */
    public function setF047fcreatedDtTm($f047fcreatedDtTm)
    {
        $this->f047fcreatedDtTm = $f047fcreatedDtTm;

        return $this;
    }

    /**
     * Get f047fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF047fcreatedDtTm()
    {
        return $this->f047fcreatedDtTm;
    }

    /**
     * Set f047fupdatedDtTm
     *
     * @param \DateTime $f047fupdatedDtTm
     *
     * @return T047fdebtTagging
     */
    public function setF047fupdatedDtTm($f047fupdatedDtTm)
    {
        $this->f047fupdatedDtTm = $f047fupdatedDtTm;

        return $this;
    }

    /**
     * Get f047fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF047fupdatedDtTm()
    {
        return $this->f047fupdatedDtTm;
    }
}
