<?php 

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T036ftenderCommitee
 *
 * @ORM\Table(name="t036ftender_commitee")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderQuotationRepository")
 */
class T036ftenderCommitee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f036fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f036fid;

    
    /**
     * @var integer
     *string
     * @ORM\Column(name="f036fid_staff", type="integer", nullable=false)
     */
    private $f036fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f036fname", type="string", length=100, nullable=true)
     */
    private $f036fname;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="f036fstatus", type="integer", nullable=false)
     */
    private $f036fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fcreated_by", type="integer", nullable=false)
     */
    private $f036fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fupdated_by", type="integer", nullable=false)
     */
    private $f036fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f036fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f036fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f036fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f036fupdatedDtTm;

     /**
     * @var integer
     *
     * @ORM\Column(name="f036fid_tender", type="integer", nullable=false)
     */
    private $f036fidTender;

    /**
     * Get f036fid
     *
     * @return integer
     */
    public function getF036fid()
    {
        return $this->f036fid;
    }

    /**
     * Set f036fidStaff
     *
     * @param integer $f036fidStaff
     *
     * @return T036ftenderCommitee
     */
    public function setF036fidStaff($f036fidStaff)
    {
        $this->f036fidStaff = $f036fidStaff;

        return $this;
    }

    /**
     * Get f036fidStaff
     *
     * @return integer
     */
    public function getF036fidStaff()
    {
        return $this->f036fidStaff;
    }


    /**
     * Set f036fname
     *
     * @param string $f036fname
     *
     * @return T036ftenderCommitee
     */
    public function setF036fname($f036fname)
    {
        $this->f036fname = $f036fname;

        return $this;
    }

    /**
     * Get f036fname
     *
     * @return string
     */
    public function getF036fname()
    {
        return $this->f036fname;
    }

   
    /**
     * Set f036fstatus
     *
     * @param integer $f036fstatus
     *
     * @return T036ftenderCommitee
     */
    public function setF036fstatus($f036fstatus)
    {
        $this->f036fstatus = $f036fstatus;

        return $this;
    }

    /**
     * Get f036fstatus
     *
     * @return integer
     */
    public function getF036fstatus()
    {
        return $this->f036fstatus;
    }

    /**
     * Set f036fcreatedBy
     *
     * @param integer $f036fcreatedBy
     *
     * @return T036ftenderCommitee
     */
    public function setF036fcreatedBy($f036fcreatedBy)
    {
        $this->f036fcreatedBy = $f036fcreatedBy;

        return $this;
    }

    /**
     * Get f036fcreatedBy
     *
     * @return integer
     */
    public function getF036fcreatedBy()
    {
        return $this->f036fcreatedBy;
    }

    /**
     * Set f036fupdatedBy
     *
     * @param integer $f036fupdatedBy
     *
     * @return T036ftenderCommitee
     */
    public function setF036fupdatedBy($f036fupdatedBy)
    {
        $this->f036fupdatedBy = $f036fupdatedBy;

        return $this;
    }

    /**
     * Get f036fupdatedBy
     *
     * @return integer
     */
    public function getF036fupdatedBy()
    {
        return $this->f036fupdatedBy;
    }

    /**
     * Set f036fcreatedDtTm
     *
     * @param \DateTime $f036fcreatedDtTm
     *
     * @return T036ftenderCommitee
     */
    public function setF036fcreatedDtTm($f036fcreatedDtTm)
    {
        $this->f036fcreatedDtTm = $f036fcreatedDtTm;

        return $this;
    }

    /**
     * Get f036fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF036fcreatedDtTm()
    {
        return $this->f036fcreatedDtTm;
    }

    /**
     * Set f036fupdatedDtTm
     *
     * @param \DateTime $f036fupdatedDtTm
     *
     * @return T036ftenderCommitee
     */
    public function setF036fupdatedDtTm($f036fupdatedDtTm)
    {
        $this->f036fupdatedDtTm = $f036fupdatedDtTm;

        return $this;
    }

    /**
     * Get f036fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF036fupdatedDtTm()
    {
        return $this->f036fupdatedDtTm;
    }

    /**
     * Set f036fidTender
     *
     * @param integer $f036fidTender
     *
     * @return T036ftenderSpec
     */
    public function setF036fidTender($f036fidTender)
    {
        $this->f036fidTender = $f036fidTender;

        return $this;
    }

    /**
     * Get f036fidTender
     *
     * @return integer
     */
    public function getF036fidTender()
    {
        return $this->f036fidTender;
    }
}

