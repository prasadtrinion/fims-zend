<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064fsponsorDnDetails
 *
 * @ORM\Table(name="t064fsponsor_dn_details")
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorDnRepository")
 */
class T064fsponsorDnDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_item", type="integer", nullable=false)
     */
    private $f064fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fgst_value", type="integer", nullable=false)
     */
    private $f064fgstValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fquantity", type="integer", nullable=false)
     */
    private $f064fquantity;

    /**
     * @var float
     *
     * @ORM\Column(name="f064fprice", type="integer", nullable=false)
     */
    private $f064fprice;

    /**
     * @var float
     *
     * @ORM\Column(name="f064fto_dn_amount", type="integer", nullable=false)
     */
    private $f064ftoDnAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f064ftotal", type="integer", nullable=false)
     */
    private $f064ftotal;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_tax_code", type="integer", nullable=true)
     */
    private $f064fidTaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064ftotal_exc", type="integer", nullable=true)
     */
    private $f064ftotalExc;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064ftax_amount", type="integer", nullable=true)
     */
    private $f064ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=false)
     */
    private $f064fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=false)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=false)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f064fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f064fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fgst_code", type="string", length=255, nullable=false)
     */
    private $f064fgstCode;

        /**
     * @var string
     *
     * @ORM\Column(name="f064fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f064fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f064fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f064fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f064fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f064fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f064fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f064fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f064fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f064fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f064fcreditAccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_sponsor_dn", type="integer", nullable=false)
     */
    private $f064fidsponsorDn;

    

    /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064fidItem
     *
     * @param integer $f064fidItem
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fidItem($f064fidItem)
    {
        $this->f064fidItem = $f064fidItem;

        return $this;
    }

    /**
     * Get f064fidItem
     *
     * @return integer
     */
    public function getF064fidItem()
    {
        return $this->f064fidItem;
    }

    /**
     * Set f064fgstValue
     *
     * @param integer $f064fgstValue
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fgstValue($f064fgstValue)
    {
        $this->f064fgstValue = $f064fgstValue;

        return $this;
    }

    /**
     * Get f064fgstValue
     *
     * @return integer
     */
    public function getF064fgstValue()
    {
        return $this->f064fgstValue;
    }

    /**
     * Set f064fquantity
     *
     * @param integer $f064fquantity
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fquantity($f064fquantity)
    {
        $this->f064fquantity = $f064fquantity;

        return $this;
    }

    /**
     * Get f064fquantity
     *
     * @return integer
     */
    public function getF064fquantity()
    {
        return $this->f064fquantity;
    }

    /**
     * Set f064fprice
     *
     * @param float $f064fprice
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fprice($f064fprice)
    {
        $this->f064fprice = $f064fprice;

        return $this;
    }

    /**
     * Get f064fprice
     *
     * @return float
     */
    public function getF064fprice()
    {
        return $this->f064fprice;
    }

    /**
     * Set f064ftoDnAmount
     *
     * @param float $f064ftoDnAmount
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064ftoDnAmount($f064ftoDnAmount)
    {
        $this->f064ftoDnAmount = $f064ftoDnAmount;

        return $this;
    }

    /**
     * Get f064ftoDnAmount
     *
     * @return float
     */
    public function getF064ftoDnAmount()
    {
        return $this->f064ftoDnAmount;
    }

    /**
     * Set f064ftotal
     *
     * @param float $f064ftotal
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064ftotal($f064ftotal)
    {
        $this->f064ftotal = $f064ftotal;

        return $this;
    }

    /**
     * Get f064ftotal
     *
     * @return float
     */
    public function getF064ftotal()
    {
        return $this->f064ftotal;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fcreatedDtTm
     *
     * @param \DateTime $f064fcreatedDtTm
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fcreatedDtTm($f064fcreatedDtTm)
    {
        $this->f064fcreatedDtTm = $f064fcreatedDtTm;

        return $this;
    }

    /**
     * Get f064fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreatedDtTm()
    {
        return $this->f064fcreatedDtTm;
    }

    /**
     * Set f064fupdatedDtTm
     *
     * @param \DateTime $f064fupdatedDtTm
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fupdatedDtTm($f064fupdatedDtTm)
    {
        $this->f064fupdatedDtTm = $f064fupdatedDtTm;

        return $this;
    }

    /**
     * Get f064fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdatedDtTm()
    {
        return $this->f064fupdatedDtTm;
    }

    /**
     * Set f064fgstCode
     *
     * @param string $f064fgstCode
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fgstCode($f064fgstCode)
    {
        $this->f064fgstCode = $f064fgstCode;

        return $this;
    }

    /**
     * Get f064fgstCode
     *
     * @return string
     */
    public function getF064fgstCode()
    {
        return $this->f064fgstCode;
    }

    /**
     * Set f064fdebitFundCode
     *
     * @param integer $f064fdebitFundCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fdebitFundCode($f064fdebitFundCode)
    {
        $this->f064fdebitFundCode = $f064fdebitFundCode;

        return $this;
    }

    /**
     * Get f064fdebitFundCode
     *
     * @return integer
     */
    public function getF064fdebitFundCode()
    {
        return $this->f064fdebitFundCode;
    }

    /**
     * Set f064fdebitAccountCode
     *
     * @param integer $f064fdebitAccountCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fdebitAccountCode($f064fdebitAccountCode)
    {
        $this->f064fdebitAccountCode = $f064fdebitAccountCode;

        return $this;
    }

    /**
     * Get f064fdebitAccountCode
     *
     * @return integer
     */
    public function getF064fdebitAccountCode()
    {
        return $this->f064fdebitAccountCode;
    }

    /**
     * Set f064fdebitActivityCode
     *
     * @param integer $f064fdebitActivityCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fdebitActivityCode($f064fdebitActivityCode)
    {
        $this->f064fdebitActivityCode = $f064fdebitActivityCode;

        return $this;
    }

    /**
     * Get f064fdebitActivityCode
     *
     * @return integer
     */
    public function getF064fdebitActivityCode()
    {
        return $this->f064fdebitActivityCode;
    }

    /**
     * Set f064fdebitDepartmentCode
     *
     * @param integer $f064fdebitDepartmentCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fdebitDepartmentCode($f064fdebitDepartmentCode)
    {
        $this->f064fdebitDepartmentCode = $f064fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f064fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF064fdebitDepartmentCode()
    {
        return $this->f064fdebitDepartmentCode;
    }


    /**
     * Set f064fcreditFundCode
     *
     * @param integer $f064fcreditFundCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fcreditFundCode($f064fcreditFundCode)
    {
        $this->f064fcreditFundCode = $f064fcreditFundCode;

        return $this;
    }

    /**
     * Get f064fcreditFundCode
     *
     * @return integer
     */
    public function getF064fcreditFundCode()
    {
        return $this->f064fcreditFundCode;
    }

    /**
     * Set f064fcreditAccountCode
     *
     * @param integer $f064fcreditAccountCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fcreditAccountCode($f064fcreditAccountCode)
    {
        $this->f064fcreditAccountCode = $f064fcreditAccountCode;

        return $this;
    }

    /**
     * Get f064fcreditAccountCode
     *
     * @return integer
     */
    public function getF064fcreditAccountCode()
    {
        return $this->f064fcreditAccountCode;
    }

    /**
     * Set f064fcreditActivityCode
     *
     * @param integer $f064fcreditActivityCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fcreditActivityCode($f064fcreditActivityCode)
    {
        $this->f064fcreditActivityCode = $f064fcreditActivityCode;

        return $this;
    }

    /**
     * Get f064fcreditActivityCode
     *
     * @return integer
     */
    public function getF064fcreditActivityCode()
    {
        return $this->f064fcreditActivityCode;
    }

    /**
     * Set f064fcreditDepartmentCode
     *
     * @param integer $f064fcreditDepartmentCode
     *
     * @return T064fcreditNoteDetails
     */
    public function setF064fcreditDepartmentCode($f064fcreditDepartmentCode)
    {
        $this->f064fcreditDepartmentCode = $f064fcreditDepartmentCode;

        return $this;
    }


    /**
     * Set f064fidsponsorDn
     *
     * @param integer $f064fidsponsorDn
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fidsponsorDn($f064fidsponsorDn)
    {
        $this->f064fidsponsorDn = $f064fidsponsorDn;

        return $this;
    }

    /**
     * Get f064fidsponsorDn
     *
     * @return \Application\Entity\T064fsponsorDn
     */
    public function getF064fidsponsorDn()
    {
        return $this->f064fidsponsorDn;
    }

    /**
     * Set f064fidTaxCode
     *
     * @param integer $f064fidTaxCode
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064fidTaxCode($f064fidTaxCode)
    {
        $this->f064fidTaxCode = $f064fidTaxCode;

        return $this;
    }

    /**
     * Get f064fidTaxCode
     *
     * @return integer
     */
    public function getF064fidTaxCode()
    {
        return $this->f064fidTaxCode;
    }

    /**
     * Set f064ftotalExc
     *
     * @param integer $f064ftotalExc
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064ftotalExc($f064ftotalExc)
    {
        $this->f064ftotalExc = $f064ftotalExc;

        return $this;
    }

    /**
     * Get f064ftotalExc
     *
     * @return integer
     */
    public function getF064ftotalExc()
    {
        return $this->f064ftotalExc;
    }

    /**
     * Set f064ftaxAmount
     *
     * @param integer $f064ftaxAmount
     *
     * @return T064fsponsorDnDetails
     */
    public function setF064ftaxAmount($f064ftaxAmount)
    {
        $this->f064ftaxAmount = $f064ftaxAmount;

        return $this;
    }

    /**
     * Get f064ftaxAmount
     *
     * @return integer
     */
    public function getF064ftaxAmount()
    {
        return $this->f064ftaxAmount;
    }
}

