<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T020floanName
 *
 * @ORM\Table(name="t020floan_name",uniqueConstraints={@ORM\UniqueConstraint(name="f020floan_name_UNIQUE", columns={"f020floan_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\LoanNameRepository")
 */
class T020floanName
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f020fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f020fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f020floan_name", type="string", length=255, nullable=false)
     */
    private $f020floanName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020ftype_of_loan", type="integer", nullable=false)
     */
    private $f020ftypeOfLoan ;

     /**
     * @var string
     *
     * @ORM\Column(name="f020fdepartment", type="string", length=20, nullable=false)
     */
    private $f020fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fpercentage", type="integer", nullable=true)
     */
    private $f020fpercentage;

     /**
     * @var integer
     *
     * @ORM\Column(name="f020fstatus", type="integer", nullable=false)
     */
    private $f020fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fcreated_by", type="integer", nullable=false)
     */
    private $f020fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fupdated_by", type="integer", nullable=false)
     */
    private $f020fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f020fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f020fupdatedDtTm;

  /**
     * @var string
     *
     * @ORM\Column(name="f020ffund", type="string", length=25, nullable=false)
     */
    private $f020ffund='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f020fpaytype", type="string", length=25, nullable=false)
     */
    private $f020fpaytype='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f020factivity", type="string", length=25, nullable=false)
     */
    private $f020factivity='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f020faccount", type="string", length=25, nullable=false)
     */
    private $f020faccount='NULL';



    /**
     * Get f020fid
     *
     * @return integer
     */
    public function getF020fid()
    {
        return $this->f020fid;
    }

    /**
     * Set f020floanName
     *
     * @param string $f020floanName
     *
     * @return T020floanName
     */
    public function setF020floanName($f020floanName)
    {
        $this->f020floanName = $f020floanName;

        return $this;
    }

    /**
     * Get f020floanName
     *
     * @return string
     */
    public function getF020floanName()
    {
        return $this->f020floanName;
    }

    /**
     * Get f020fdepartment
     *
     * @return integer
     */
    public function getF020fdepartment()
    {
        return $this->f020fdepartment;
    }

    /**
     * Set f020fdepartment
     *
     * @param integer $f020fdepartment
     *
     * @return T020floanTypeSetup
     */
    public function setF020fdepartment($f020fdepartment)
    {
        $this->f020fdepartment = $f020fdepartment;

        return $this;
    }

    /**
     * Set f020ftypeOfLoan
     *
     * @param boolean $f020ftypeOfLoan
     *
     * @return T020floanName
     */
    public function setF020ftypeOfLoan($f020ftypeOfLoan)
    {
        $this->f020ftypeOfLoan = $f020ftypeOfLoan;

        return $this;
    }

    /**
     * Get f020ftypeOfLoan
     *
     * @return boolean
     */
    public function getF020ftypeOfLoan()
    {
        return $this->f020ftypeOfLoan;
    }

     /**
     * Set f020fstatus
     *
     * @param boolean $f020fstatus
     *
     * @return T020floanName
     */
    public function setF020fstatus($f020fstatus)
    {
        $this->f020fstatus = $f020fstatus;

        return $this;
    }

    /**
     * Get f020fstatus
     *
     * @return boolean
     */
    public function getF020fstatus()
    {
        return $this->f020fstatus;
    }

    /**
     * Set f020fpercentf020fpaytypeage
     *
     * @param integer $f020fpercentage
     *
     * @return T020floanName
     */
    public function setF020fpercentage($f020fpercentage)
    {
        $this->f020fpercentage = $f020fpercentage;

        return $this;
    }

    /**
     * Get f020fpercentage
     *
     * @return integer
     */
    public function getF020fpercentage()
    {
        return $this->f020fpercentage;
    }

    /**
     * Set f020fcreatedBy
     *
     * @param integer $f020fcreatedBy
     *
     * @return T020floanName
     */
    public function setF020fcreatedBy($f020fcreatedBy)
    {
        $this->f020fcreatedBy = $f020fcreatedBy;

        return $this;
    }

    /**
     * Get f020fcreatedBy
     *
     * @return integer
     */
    public function getF020fcreatedBy()
    {
        return $this->f020fcreatedBy;
    }

    /**
     * Set f020fupdatedBy
     *
     * @param integer $f020fupdatedBy
     *
     * @return T020floanName
     */
    public function setF020fupdatedBy($f020fupdatedBy)
    {
        $this->f020fupdatedBy = $f020fupdatedBy;

        return $this;
    }

    /**
     * Get f020fupdatedBy
     *
     * @return integer
     */
    public function getF020fupdatedBy()
    {
        return $this->f020fupdatedBy;
    }

    /**
     * Set f020fcreatedDtTm
     *
     * @param \DateTime $f020fcreatedDtTm
     *
     * @return T020floanName
     */
    public function setF020fcreatedDtTm($f020fcreatedDtTm)
    {
        $this->f020fcreatedDtTm = $f020fcreatedDtTm;

        return $this;
    }

    /**
     * Get f020fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fcreatedDtTm()
    {
        return $this->f020fcreatedDtTm;
    }

    /**
     * Set f020fupdatedDtTm
     *
     * @param \DateTime $f020fupdatedDtTm
     *
     * @return T020floanName
     */
    public function setF020fupdatedDtTm($f020fupdatedDtTm)
    {
        $this->f020fupdatedDtTm = $f020fupdatedDtTm;

        return $this;
    }

    /**
     * Get f020fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fupdatedDtTm()
    {
        return $this->f020fupdatedDtTm;
    }

    /**
     * Set f020ffund
     *
     * @param string $f020ffund
     *
     * @return T020floanName
     */
    public function setF020ffund($f020ffund)
    {
        $this->f020ffund = $f020ffund;

        return $this;
    }

    /**
     * Get f020ffund
     *
     * @return string
     */
    public function getF020ffund()
    {
        return $this->f020ffund;
    }

    /**
     * Set f020factivity
     *
     * @param string $f020factivity
     *
     * @return T020floanName
     */
    public function setF020factivity($f020factivity)
    {
        $this->f020factivity = $f020factivity;

        return $this;
    }

    /**
     * Get f020factivity
     *
     * @return string
     */
    public function getF020factivity()
    {
        return $this->f020factivity;
    }

    /**
     * Set f020fpaytype
     *
     * @param string $f020fpaytype
     *
     * @return T020floanName
     */
    public function setF020fpaytype($f020fpaytype)
    {
        $this->f020fpaytype = $f020fpaytype;

        return $this;
    }

    /**
     * Get f020fpaytype
     *
     * @return string
     */
    public function getF020fpaytype()
    {
        return $this->f020fpaytype;
    }

    /**
     * Set f020faccount
     *
     * @param string $f020faccount
     *
     * @return T020floanName
     */
    public function setF020faccount($f020faccount)
    {
        $this->f020faccount = $f020faccount;

        return $this;
    }

    /**
     * Get f020faccount
     *
     * @return string
     */
    public function getF020faccount()
    {
        return $this->f020faccount;
    }
}
