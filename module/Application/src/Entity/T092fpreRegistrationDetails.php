<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T092fpreRegistrationDetails
 *
 * @ORM\Table(name="t092fpre_registration_details")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetPreRegistrationRepository")
 */
class T092fpreRegistrationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f092fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_pre", type="integer", nullable=true)
     */
    private $f092fidPre;

    /**
     * @var string
     *
     * @ORM\Column(name="f092fpo_reference", type="string", length=20, nullable=true)
     */
    private $f092fpoReference;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fref_seq_no", type="integer", nullable=true)
     */
    private $f092frefSeqNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_category", type="integer", nullable=true)
     */
    private $f092fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_sub_category", type="integer", nullable=true)
     */
    private $f092fidSubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_asset_type", type="integer", nullable=true)
     */
    private $f092fidAssetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_asset_item", type="integer", nullable=true)
     */
    private $f092fidAssetItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f092fasset_name", type="string", length=50, nullable=true)
     */
    private $f092fassetName;

    /**
     * @var string
     *
     * @ORM\Column(name="f092fso_code", type="string", length=25, nullable=true)
     */
    private $f092fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_staff", type="integer", nullable=true)
     */
    private $f092fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f092fphoto", type="string", length=255, nullable=true)
     */
    private $f092fphoto;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fstatus", type="integer", nullable=true)
     */
    private $f092fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fcreated_by", type="integer", nullable=true)
     */
    private $f092fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fupdated_by", type="integer", nullable=true)
     */
    private $f092fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f092fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f092fupdatedDtTm;



    /**
     * Get f092fidDetails
     *
     * @return integer
     */
    public function getF092fidDetails()
    {
        return $this->f092fidDetails;
    }

    /**
     * Set f092fpoReference
     *
     * @param string $f092fpoReference
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fpoReference($f092fpoReference)
    {
        $this->f092fpoReference = $f092fpoReference;

        return $this;
    }

    /**
     * Get f092fpoReference
     *
     * @return string
     */
    public function getF092fpoReference()
    {
        return $this->f092fpoReference;
    }

    /**
     * Set f092frefSeqNo
     *
     * @param integer $f092frefSeqNo
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092frefSeqNo($f092frefSeqNo)
    {
        $this->f092frefSeqNo = $f092frefSeqNo;

        return $this;
    }

    /**
     * Get f092frefSeqNo
     *
     * @return integer
     */
    public function getF092frefSeqNo()
    {
        return $this->f092frefSeqNo;
    }

    /**
     * Set f092fidCategory
     *
     * @param integer $f092fidCategory
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidCategory($f092fidCategory)
    {
        $this->f092fidCategory = $f092fidCategory;

        return $this;
    }

    /**
     * Get f092fidCategory
     *
     * @return integer
     */
    public function getF092fidCategory()
    {
        return $this->f092fidCategory;
    }

    /**
     * Set f092fidSubCategory
     *
     * @param integer $f092fidSubCategory
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidSubCategory($f092fidSubCategory)
    {
        $this->f092fidSubCategory = $f092fidSubCategory;

        return $this;
    }

    /**
     * Get f092fidSubCategory
     *
     * @return integer
     */
    public function getF092fidSubCategory()
    {
        return $this->f092fidSubCategory;
    }

    /**
     * Set f092fidAssetType
     *
     * @param integer $f092fidAssetType
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidAssetType($f092fidAssetType)
    {
        $this->f092fidAssetType = $f092fidAssetType;

        return $this;
    }

    /**
     * Get f092fidAssetType
     *
     * @return integer
     */
    public function getF092fidAssetType()
    {
        return $this->f092fidAssetType;
    }

    /**
     * Set f092fidAssetItem
     *
     * @param integer $f092fidAssetItem
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidAssetItem($f092fidAssetItem)
    {
        $this->f092fidAssetItem = $f092fidAssetItem;

        return $this;
    }

    /**
     * Get f092fidAssetItem
     *
     * @return integer
     */
    public function getF092fidAssetItem()
    {
        return $this->f092fidAssetItem;
    }

    /**
     * Set f092fassetName
     *
     * @param string $f092fassetName
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fassetName($f092fassetName)
    {
        $this->f092fassetName = $f092fassetName;

        return $this;
    }

    /**
     * Get f092fassetName
     *
     * @return string
     */
    public function getF092fassetName()
    {
        return $this->f092fassetName;
    }

    /**
     * Set f092fsoCode
     *
     * @param string $f092fsoCode
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fsoCode($f092fsoCode)
    {
        $this->f092fsoCode = $f092fsoCode;

        return $this;
    }

    /**
     * Get f092fsoCode
     *
     * @return string
     */
    public function getF092fsoCode()
    {
        return $this->f092fsoCode;
    }

    /**
     * Set f092fidStaff
     *
     * @param integer $f092fidStaff
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidStaff($f092fidStaff)
    {
        $this->f092fidStaff = $f092fidStaff;

        return $this;
    }

    /**
     * Get f092fidStaff
     *
     * @return integer
     */
    public function getF092fidStaff()
    {
        return $this->f092fidStaff;
    }

    /**
     * Set f092fphoto
     *
     * @param string $f092fphoto
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fphoto($f092fphoto)
    {
        $this->f092fphoto = $f092fphoto;

        return $this;
    }

    /**
     * Get f092fphoto
     *
     * @return string
     */
    public function getF092fphoto()
    {
        return $this->f092fphoto;
    }

    /**
     * Set f092fstatus
     *
     * @param integer $f092fstatus
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fstatus($f092fstatus)
    {
        $this->f092fstatus = $f092fstatus;

        return $this;
    }

    /**
     * Get f092fstatus
     *
     * @return integer
     */
    public function getF092fstatus()
    {
        return $this->f092fstatus;
    }

    /**
     * Set f092fcreatedBy
     *
     * @param integer $f092fcreatedBy
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fcreatedBy($f092fcreatedBy)
    {
        $this->f092fcreatedBy = $f092fcreatedBy;

        return $this;
    }

    /**
     * Get f092fcreatedBy
     *
     * @return integer
     */
    public function getF092fcreatedBy()
    {
        return $this->f092fcreatedBy;
    }

    /**
     * Set f092fupdatedBy
     *
     * @param integer $f092fupdatedBy
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fupdatedBy($f092fupdatedBy)
    {
        $this->f092fupdatedBy = $f092fupdatedBy;

        return $this;
    }

    /**
     * Get f092fupdatedBy
     *
     * @return integer
     */
    public function getF092fupdatedBy()
    {
        return $this->f092fupdatedBy;
    }

    /**
     * Set f092fcreatedDtTm
     *
     * @param \DateTime $f092fcreatedDtTm
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fcreatedDtTm($f092fcreatedDtTm)
    {
        $this->f092fcreatedDtTm = $f092fcreatedDtTm;

        return $this;
    }

    /**
     * Get f092fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fcreatedDtTm()
    {
        return $this->f092fcreatedDtTm;
    }

    /**
     * Set f092fupdatedDtTm
     *
     * @param \DateTime $f092fupdatedDtTm
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fupdatedDtTm($f092fupdatedDtTm)
    {
        $this->f092fupdatedDtTm = $f092fupdatedDtTm;

        return $this;
    }

    /**
     * Get f092fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fupdatedDtTm()
    {
        return $this->f092fupdatedDtTm;
    }

    /**
     * Set f092fidPre
     *
     * @param integer $f092fidPre
     *
     * @return T092fpreRegistrationDetails
     */
    public function setF092fidPre($f092fidPre)
    {
        $this->f092fidPre = $f092fidPre;

        return $this;
    }

    /**
     * Get f092fidPre
     *
     * @return integer
     */
    public function getF092fidPre()
    {
        return $this->f092fidPre;
    }

}
