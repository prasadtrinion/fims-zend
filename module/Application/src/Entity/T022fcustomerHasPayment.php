<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T022fcustomerHasPayment
 *
 * @ORM\Table(name="t022fcustomer_has_payment")
 * @ORM\Entity(repositoryClass="Application\Repository\CustomerRepository")
 */
class T022fcustomerHasPayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f022fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid_customer", type="integer", nullable=false)
     */
    private $f022fidCustomer;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022famount", type="integer",scale=2, nullable=false)
     */
    private $f022famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f022ftype", type="string", length=10, nullable=false)
     */
    private $f022ftype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fdate_of_payment", type="datetime", nullable=false)
     */
    private $f022fdateOfPayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fstatus", type="integer", nullable=false)
     */
    private $f022fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcreated_by", type="integer", nullable=false)
     */
    private $f022fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fupdated_by", type="integer", nullable=false)
     */
    private $f022fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fupdatedDtTm;

    

    /**
     * Get f022fid
     *
     * @return integer
     */
    public function getF022fid()
    {
        return $this->f022fid;
    }

    /**
     * Set f022fidCustomer
     *
     * @param integer $f022fidCustomer
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fidCustomer($f022fidCustomer)
    {
        $this->f022fidCustomer = $f022fidCustomer;

        return $this;
    }

    /**
     * Get f022fidCustomer
     *
     * @return integer
     */
    public function getF022fidCustomer()
    {
        return $this->f022fidCustomer;
    }

    /**
     * Set f022famount
     *
     * @param integer $f022famount
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022famount($f022famount)
    {
        $this->f022famount = $f022famount;

        return $this;
    }

    /**
     * Get f022famount
     *
     * @return integer
     */
    public function getF022famount()
    {
        return $this->f022famount;
    }

    /**
     * Set f022ftype
     *
     * @param string $f022ftype
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022ftype($f022ftype)
    {
        $this->f022ftype = $f022ftype;

        return $this;
    }

    /**
     * Get f022ftype
     *
     * @return string
     */
    public function getF022ftype()
    {
        return $this->f022ftype;
    }

    /**
     * Set f022fdateOfPayment
     *
     * @param \DateTime $f022fdateOfPayment
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fdateOfPayment($f022fdateOfPayment)
    {
        $this->f022fdateOfPayment = $f022fdateOfPayment;

        return $this;
    }

    /**
     * Get f022fdateOfPayment
     *
     * @return \DateTime
     */
    public function getF022fdateOfPayment()
    {
        return $this->f022fdateOfPayment;
    }

    /**
     * Set f022fstatus
     *
     * @param integer $f022fstatus
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fstatus($f022fstatus)
    {
        $this->f022fstatus = $f022fstatus;

        return $this;
    }

    /**
     * Get f022fstatus
     *
     * @return integer
     */
    public function getF022fstatus()
    {
        return $this->f022fstatus;
    }

    /**
     * Set f022fcreatedBy
     *
     * @param integer $f022fcreatedBy
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fcreatedBy($f022fcreatedBy)
    {
        $this->f022fcreatedBy = $f022fcreatedBy;

        return $this;
    }

    /**
     * Get f022fcreatedBy
     *
     * @return integer
     */
    public function getF022fcreatedBy()
    {
        return $this->f022fcreatedBy;
    }

    /**
     * Set f022fupdatedBy
     *
     * @param integer $f022fupdatedBy
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fupdatedBy($f022fupdatedBy)
    {
        $this->f022fupdatedBy = $f022fupdatedBy;

        return $this;
    }

    /**
     * Get f022fupdatedBy
     *
     * @return integer
     */
    public function getF022fupdatedBy()
    {
        return $this->f022fupdatedBy;
    }

    /**
     * Set f022fcreatedDtTm
     *
     * @param \DateTime $f022fcreatedDtTm
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fcreatedDtTm($f022fcreatedDtTm)
    {
        $this->f022fcreatedDtTm = $f022fcreatedDtTm;

        return $this;
    }

    /**
     * Get f022fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fcreatedDtTm()
    {
        return $this->f022fcreatedDtTm;
    }

    /**
     * Set f022fupdatedDtTm
     *
     * @param \DateTime $f022fupdatedDtTm
     *
     * @return T022fcustomerHasPayment
     */
    public function setF022fupdatedDtTm($f022fupdatedDtTm)
    {
        $this->f022fupdatedDtTm = $f022fupdatedDtTm;

        return $this;
    }

    /**
     * Get f022fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fupdatedDtTm()
    {
        return $this->f022fupdatedDtTm;
    }
}

