<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T053fsemester
 *
 * @ORM\Table(name="t053fsemester")
 * @ORM\Entity(repositoryClass="Application\Repository\SemesterRepository")
 */
class T053fsemester
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f053fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f053fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f053fsemester", type="string", length=45, nullable=true)
     */
    private $f053fsemester;

    /**
     * @var string
     *
     * @ORM\Column(name="f053fcode", type="string", length=45, nullable=true)
     */
    private $f053fcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fstart_date", type="datetime", nullable=true)
     */
    private $f053fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fend_date", type="datetime", nullable=true)
     */
    private $f053fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053ftype", type="integer", nullable=true)
     */
    private $f053ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fsem_sequence", type="integer", nullable=true)
     */
    private $f053fsemSequence;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fstatus", type="integer", nullable=true)
     */
    private $f053fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fcreated_by", type="integer", nullable=true)
     */
    private $f053fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fupdated_by", type="integer", nullable=true)
     */
    private $f053fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f053fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f053fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="fasa", type="string", length=45, nullable=true)
     */
    private $fasa = "NULL";

    /**
     * @var string
     *
     * @ORM\Column(name="f053fgraduation_type", type="string", length=45, nullable=true)
     */
    private $f053fgraduationType;


    /**
     * Get f053fid
     *
     * @return integer
     */
    public function getF053fid()
    {
        return $this->f053fid;
    }

    /**
     * Set f053fsemester
     *
     * @param string $f053fsemester
     *
     * @return T053fsemester
     */
    public function setF053fsemester($f053fsemester)
    {
        $this->f053fsemester = $f053fsemester;

        return $this;
    }

    /**
     * Get f053fsemester
     *
     * @return string
     */
    public function getF053fsemester()
    {
        return $this->f053fsemester;
    }

 /**
     * Set f053fgraduationType
     *
     * @param string $f053fgraduationType
     *
     * @return T053fsemester
     */
    public function setF053fgraduationType($f053fgraduationType)
    {
        $this->f053fgraduationType = $f053fgraduationType;

        return $this;
    }

    /**
     * Get f053fgraduationType
     *
     * @return string
     */
    public function getF053fgraduationType()
    {
        return $this->f053fgraduationType;
    }

    /**
     * Set f053fcode
     *
     * @param string $f053fcode
     *
     * @return T053fsemester
     */
    public function setF053fcode($f053fcode)
    {
        $this->f053fcode = $f053fcode;

        return $this;
    }

    /**
     * Get f053fcode
     *
     * @return string
     */
    public function getF053fcode()
    {
        return $this->f053fcode;
    }

    /**
     * Set f053fstartDate
     *
     * @param \DateTime $f053fstartDate
     *
     * @return T053fsemester
     */
    public function setF053fstartDate($f053fstartDate)
    {
        $this->f053fstartDate = $f053fstartDate;

        return $this;
    }

    /**
     * Get f053fstartDate
     *
     * @return \DateTime
     */
    public function getF053fstartDate()
    {
        return $this->f053fstartDate;
    }

    /**
     * Set f053fendDate
     *
     * @param \DateTime $f053fendDate
     *
     * @return T053fsemester
     */
    public function setF053fendDate($f053fendDate)
    {
        $this->f053fendDate = $f053fendDate;

        return $this;
    }

    /**
     * Get f053fendDate
     *
     * @return \DateTime
     */
    public function getF053fendDate()
    {
        return $this->f053fendDate;
    }

    /**
     * Set f053ftype
     *
     * @param integer $f053ftype
     *
     * @return T053fsemester
     */
    public function setF053ftype($f053ftype)
    {
        $this->f053ftype = $f053ftype;

        return $this;
    }

    /**
     * Get f053ftype
     *
     * @return integer
     */
    public function getF053ftype()
    {
        return $this->f053ftype;
    }

     /**
     * Set f053fsemSequence
     *
     * @param integer $f053fsemSequence
     *
     * @return T053fsemester
     */
    public function setF053fsemSequence($f053fsemSequence)
    {
        $this->f053fsemSequence = $f053fsemSequence;

        return $this;
    }

    /**
     * Get f053fsemSequence
     *
     * @return integer
     */
    public function getF053fsemSequence()
    {
        return $this->f053fsemSequence;
    }

    /**
     * Set f053fstatus
     *
     * @param integer $f053fstatus
     *
     * @return T053fsemester
     */
    public function setF053fstatus($f053fstatus)
    {
        $this->f053fstatus = $f053fstatus;

        return $this;
    }

    /**
     * Get f053fstatus
     *
     * @return integer
     */
    public function getF053fstatus()
    {
        return $this->f053fstatus;
    }

    /**
     * Set f053fcreatedBy
     *
     * @param integer $f053fcreatedBy
     *
     * @return T053fsemester
     */
    public function setF053fcreatedBy($f053fcreatedBy)
    {
        $this->f053fcreatedBy = $f053fcreatedBy;

        return $this;
    }

    /**
     * Get f053fcreatedBy
     *
     * @return integer
     */
    public function getF053fcreatedBy()
    {
        return $this->f053fcreatedBy;
    }

    /**
     * Set f053fupdatedBy
     *
     * @param integer $f053fupdatedBy
     *
     * @return T053fsemester
     */
    public function setF053fupdatedBy($f053fupdatedBy)
    {
        $this->f053fupdatedBy = $f053fupdatedBy;

        return $this;
    }

    /**
     * Get f053fupdatedBy
     *
     * @return integer
     */
    public function getF053fupdatedBy()
    {
        return $this->f053fupdatedBy;
    }

    /**
     * Set f053fcreatedDtTm
     *
     * @param \DateTime $f053fcreatedDtTm
     *
     * @return T053fsemester
     */
    public function setF053fcreatedDtTm($f053fcreatedDtTm)
    {
        $this->f053fcreatedDtTm = $f053fcreatedDtTm;

        return $this;
    }

    /**
     * Get f053fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF053fcreatedDtTm()
    {
        return $this->f053fcreatedDtTm;
    }

    /**
     * Set f053fupdatedDtTm
     *
     * @param \DateTime $f053fupdatedDtTm
     *
     * @return T053fsemester
     */
    public function setF053fupdatedDtTm($f053fupdatedDtTm)
    {
        $this->f053fupdatedDtTm = $f053fupdatedDtTm;

        return $this;
    }

    /**
     * Get f053fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF053fupdatedDtTm()
    {
        return $this->f053fupdatedDtTm;
    }
}
