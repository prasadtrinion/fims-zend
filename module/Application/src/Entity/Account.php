<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="account", uniqueConstraints={@ORM\UniqueConstraint(name="username_UNIQUE", columns={"username"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AccountRepository")
 */
class Account
{
    const STATUS_UNVERIFIED = 0;
    const STATUS_VERIFIED = 1;
    const STATUS_REJECTED = -1;
     
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=150, nullable=false)
     */
    private $password;
    

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = self::STATUS_UNVERIFIED;
    

    /**
     * @var string
     *
     * @ORM\Column(name="sso_token", type="string", length=500, nullable=true)
     */
    private $ssoToken = ' ';

    /**
     * @var string
     *
     * @ORM\Column(name="reset_token", type="string", length=150, nullable=true)
     */
    private $resetToken = ' ';
    

    /**
     * @var string
     *
     * @ORM\Column(name="verification_token",type="string", length=50, nullable=true)
     */
    private $verificationToken = null;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="email_status", type="integer", nullable=false,options={"comment":" 0(unverified) 1(verified)"})
     */
    private $emailStatus = self::STATUS_UNVERIFIED;
    
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_logged_in_dt_tm", type="datetime", nullable=false)
     */
    private $lastLoggedInDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_dt_tm", type="datetime", nullable=false)
     */
    private $createDtTm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="delete_flag", type="boolean", nullable=false)
     */
    private $deleteFlag = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    
    
    public function getSsoToken()
    {
        return $this->ssoToken;
    }

    public function setSsoToken($ssoToken)
    {
        $this->ssoToken = $ssoToken;
        return $this;
    }

    public function getResetToken()
    {
        return $this->resetToken;
    }

    public function setResetToken($resetToken)
    {
        $this->resetToken = $resetToken;
        return $this;
    }

    public function getVerificationToken()
    {
        return $this->verificationToken;
    }
    
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
        return $this;
    }
    
    
    public function getLastLoggedInDtTm()
    {
        return $this->lastLoggedInDtTm;
    }

    public function setLastLoggedInDtTm(\DateTime $lastLoggedInDtTm)
    {
        $this->lastLoggedInDtTm = $lastLoggedInDtTm;
        return $this;
    }

    public function getCreateDtTm()
    {
        return $this->createDtTm;
    }

    public function setCreateDtTm(\DateTime $createDtTm)
    {
        $this->createDtTm = $createDtTm;
        return $this;
    }

    public function getDeleteFlag()
    {
        return $this->deleteFlag;
    }

    public function setDeleteFlag($deleteFlag)
    {
        $this->deleteFlag = $deleteFlag;
        return $this;
    }
    
    public function isActive(){
        return (!$this->getDeleteFlag()) ? true : false;
    }

    public function isRejected(){
        if($this->getStatus() == -1){
            return true;
        }
        else return false;
    }
    
    public function getEmailStatus()
    {
        return $this->emailStatus;
    }
    
    public function setEmailStatus($emailStatus)
    {
        $this->emailStatus = $emailStatus;
        return $this;
    }
    
}

