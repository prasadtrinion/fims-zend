<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T089fassetSubCategories
 *
 * @ORM\Table(name="t089fasset_sub_categories", uniqueConstraints={@ORM\UniqueConstraint(name="f089fsubcode_UNIQUE", columns={"f089fsubcode"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AssetSubCategoriesRepository")
 */
class T089fassetSubCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f089fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f089fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fid_category", type="integer", nullable=true)
     */
    private $f089fidCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f089fsubcode", type="string", length=25, nullable=true)
     */
    private $f089fsubcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f089fsub_description", type="string", length=100, nullable=true)
     */
    private $f089fsubDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089faccount_number", type="integer", nullable=true)
     */
    private $f089faccountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fid_item_group", type="integer", nullable=true)
     */
    private $f089fidItemGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089flife_expectancy", type="integer", nullable=true)
     */
    private $f089flifeExpectancy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fstatus", type="integer", nullable=true)
     */
    private $f089fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fcreated_by", type="integer", nullable=true)
     */
    private $f089fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fupdated_by", type="integer", nullable=true)
     */
    private $f089fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f089fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f089fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f089fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f089fupdatedDtTm;



    /**
     * Get f089fid
     *
     * @return integer
     */
    public function getF089fid()
    {
        return $this->f089fid;
    }

    /**
     * Set f089fidCategory
     *
     * @param integer $f089fidCategory
     *
     * @return T089fassetSubCategories
     */
    public function setF089fidCategory($f089fidCategory)
    {
        $this->f089fidCategory = $f089fidCategory;

        return $this;
    }

    /**
     * Get f089fidCategory
     *
     * @return integer
     */
    public function getF089fidCategory()
    {
        return $this->f089fidCategory;
    }

    /**
     * Set f089fsubcode
     *
     * @param string $f089fsubcode
     *
     * @return T089fassetSubCategories
     */
    public function setF089fsubcode($f089fsubcode)
    {
        $this->f089fsubcode = $f089fsubcode;

        return $this;
    }

    /**
     * Get f089fsubcode
     *
     * @return string
     */
    public function getF089fsubcode()
    {
        return $this->f089fsubcode;
    }


     /**
     * Set f089fsubDescription
     *
     * @param string $f089fsubDescription
     *
     * @return T089fassetSubCategories
     */
    public function setF089fsubDescription($f089fsubDescription)
    {
        $this->f089fsubDescription = $f089fsubDescription;

        return $this;
    }

    /**
     * Get f089fsubDescription
     *
     * @return string
     */
    public function getF089fsubDescription()
    {
        return $this->f089fsubDescription;
    }

    /**
     * Set f089faccountNumber
     *
     * @param integer $f089faccountNumber
     *
     * @return T089fassetSubCategories
     */
    public function setF089faccountNumber($f089faccountNumber)
    {
        $this->f089faccountNumber = $f089faccountNumber;

        return $this;
    }

    /**
     * Get f089faccountNumber
     *
     * @return integer
     */
    public function getF089faccountNumber()
    {
        return $this->f089faccountNumber;
    }

    /**
     * Set f089fidItemGroup
     *
     * @param integer $f089fidItemGroup
     *
     * @return T089fassetSubCategories
     */
    public function setF089fidItemGroup($f089fidItemGroup)
    {
        $this->f089fidItemGroup = $f089fidItemGroup;

        return $this;
    }

    /**
     * Get f089fidItemGroup
     *
     * @return integer
     */
    public function getF089fidItemGroup()
    {
        return $this->f089fidItemGroup;
    }

    /**
     * Set f089flifeExpectancy
     *
     * @param integer $f089flifeExpectancy
     *
     * @return T089fassetSubCategories
     */
    public function setF089flifeExpectancy($f089flifeExpectancy)
    {
        $this->f089flifeExpectancy = $f089flifeExpectancy;

        return $this;
    }

    /**
     * Get f089flifeExpectancy
     *
     * @return integer
     */
    public function getF089flifeExpectancy()
    {
        return $this->f089flifeExpectancy;
    }

    /**
     * Set f089fstatus
     *
     * @param integer $f089fstatus
     *
     * @return T089fassetSubCategories
     */
    public function setF089fstatus($f089fstatus)
    {
        $this->f089fstatus = $f089fstatus;

        return $this;
    }

    /**
     * Get f089fstatus
     *
     * @return integer
     */
    public function getF089fstatus()
    {
        return $this->f089fstatus;
    }

    /**
     * Set f089fcreatedBy
     *
     * @param integer $f089fcreatedBy
     *
     * @return T089fassetSubCategories
     */
    public function setF089fcreatedBy($f089fcreatedBy)
    {
        $this->f089fcreatedBy = $f089fcreatedBy;

        return $this;
    }

    /**
     * Get f089fcreatedBy
     *
     * @return integer
     */
    public function getF089fcreatedBy()
    {
        return $this->f089fcreatedBy;
    }

    /**
     * Set f089fupdatedBy
     *
     * @param integer $f089fupdatedBy
     *
     * @return T089fassetSubCategories
     */
    public function setF089fupdatedBy($f089fupdatedBy)
    {
        $this->f089fupdatedBy = $f089fupdatedBy;

        return $this;
    }

    /**
     * Get f089fupdatedBy
     *
     * @return integer
     */
    public function getF089fupdatedBy()
    {
        return $this->f089fupdatedBy;
    }

    /**
     * Set f089fcreatedDtTm
     *
     * @param \DateTime $f089fcreatedDtTm
     *
     * @return T089fassetSubCategories
     */
    public function setF089fcreatedDtTm($f089fcreatedDtTm)
    {
        $this->f089fcreatedDtTm = $f089fcreatedDtTm;

        return $this;
    }

    /**
     * Get f089fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF089fcreatedDtTm()
    {
        return $this->f089fcreatedDtTm;
    }

    /**
     * Set f089fupdatedDtTm
     *
     * @param \DateTime $f089fupdatedDtTm
     *
     * @return T089fassetSubCategories
     */
    public function setF089fupdatedDtTm($f089fupdatedDtTm)
    {
        $this->f089fupdatedDtTm = $f089fupdatedDtTm;

        return $this;
    }

    /**
     * Get f089fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF089fupdatedDtTm()
    {
        return $this->f089fupdatedDtTm;
    }
}
