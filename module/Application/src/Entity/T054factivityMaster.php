<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T054factivityMaster
 *
 * @ORM\Table(name="t054factivity_master")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetActivityRepository")
 */
class T054factivityMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f054fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fdepartment", type="string",length=20, nullable=false)
     */
    private $f054fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054ffund", type="string",length=20, nullable=false)
     */
    private $f054ffund;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid_financialyear", type="integer", nullable=false)
     */
    private $f054fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054ftotal_amount", type="integer",  nullable=false)
     */
    private $f054ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fbalance", type="integer", nullable=false)
     */
    private $f054fbalance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fapproval_status", type="integer", nullable=true)
     */
    private $f054fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fcreated_by", type="integer", nullable=false)
     */
    private $f054fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fupdated_by", type="integer", nullable=false)
     */
    private $f054fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fstatus", type="integer", nullable=false)
     */
    private $f054fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f054freason", type="string",length=50, nullable=true)
     */
    private $f054freason='NULL';


    /**
     * Get f054fid
     *
     * @return integer
     */
    public function getF054fid()
    {
        return $this->f054fid;
    }

    /**
     * Set f054fdepartment
     *
     * @param integer $f054fdepartment
     *
     * @return T054factivityMaster
     */
    public function setF054fdepartment($f054fdepartment)
    {
        $this->f054fdepartment = $f054fdepartment;

        return $this;
    }

    /**
     * Get f054fdepartment
     *
     * @return integer
     */
    public function getF054fdepartment()
    {
        return $this->f054fdepartment;
    }

    /**
     * Set f054ffund
     *
     * @param integer $f054ffund
     *
     * @return T054factivityMaster
     */
    public function setF054ffund($f054ffund)
    {
        $this->f054ffund = $f054ffund;

        return $this;
    }

    /**
     * Get f054ffund
     *
     * @return integer
     */
    public function getF054ffund()
    {
        return $this->f054ffund;
    }

    /**
     * Set f054fidFinancialyear
     *
     * @param integer $f054fidFinancialyear
     *
     * @return T054factivityMaster
     */
    public function setF054fidFinancialyear($f054fidFinancialyear)
    {
        $this->f054fidFinancialyear = $f054fidFinancialyear;

        return $this;
    }

    /**
     * Get f054fidFinancialyear
     *
     * @return integer
     */
    public function getF054fidFinancialyear()
    {
        return $this->f054fidFinancialyear;
    }


    /**
     * Set f054ftotalAmount
     *
     * @param integer $f054ftotalAmount
     *
     * @return T054factivityMaster
     */
    public function setF054ftotalAmount($f054ftotalAmount)
    {
        $this->f054ftotalAmount = $f054ftotalAmount;

        return $this;
    }

    /**
     * Get f054ftotalAmount
     *
     * @return integer
     */
    public function getF054ftotalAmount()
    {
        return $this->f054ftotalAmount;
    }

    
    /**
     * Set f054fapprovalStatus
     *
     * @param integer $f054fapprovalStatus
     *
     * @return T054factivityMaster
     */
    public function setF054fapprovalStatus($f054fapprovalStatus)
    {
        $this->f054fapprovalStatus = $f054fapprovalStatus;

        return $this;
    }

    /**
     * Get f054fapprovalStatus
     *
     * @return integer
     */
    public function getF054fapprovalStatus()
    {
        return $this->f054fapprovalStatus;
    }

    /**
     * Set f054fcreatedBy
     *
     * @param integer $f054fcreatedBy
     *
     * @return T054factivityMaster
     */
    public function setF054fcreatedBy($f054fcreatedBy)
    {
        $this->f054fcreatedBy = $f054fcreatedBy;

        return $this;
    }

    /**
     * Get f054fcreatedBy
     *
     * @return integer
     */
    public function getF054fcreatedBy()
    {
        return $this->f054fcreatedBy;
    }

    /**
     * Set f054fupdatedBy
     *
     * @param integer $f054fupdatedBy
     *
     * @return T054factivityMaster
     */
    public function setF054fupdatedBy($f054fupdatedBy)
    {
        $this->f054fupdatedBy = $f054fupdatedBy;

        return $this;
    }

    /**
     * Get f054fupdatedBy
     *
     * @return integer
     */
    public function getF054fupdatedBy()
    {
        return $this->f054fupdatedBy;
    }

    /**
     * Set f054fcreatedDtTm
     *
     * @param \DateTime $f054fcreatedDtTm
     *
     * @return T054factivityMaster
     */
    public function setF054fcreatedDtTm($f054fcreatedDtTm)
    {
        $this->f054fcreatedDtTm = $f054fcreatedDtTm;

        return $this;
    }

    /**
     * Get f054fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fcreatedDtTm()
    {
        return $this->f054fcreatedDtTm;
    }

    /**
     * Set f054fupdatedDtTm
     *
     * @param \DateTime $f054fupdatedDtTm
     *
     * @return T054factivityMaster
     */
    public function setF054fupdatedDtTm($f054fupdatedDtTm)
    {
        $this->f054fupdatedDtTm = $f054fupdatedDtTm;

        return $this;
    }

    /**
     * Get f054fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fupdatedDtTm()
    {
        return $this->f054fupdatedDtTm;
    }

    /**
     * Set f054fstatus
     *
     * @param integer $f054fstatus
     *
     * @return T054factivityMaster
     */
    public function setF054fstatus($f054fstatus)
    {
        $this->f054fstatus = $f054fstatus;

        return $this;
    }

    /**
     * Get f054fstatus
     *
     * @return integer
     */
    public function getF054fstatus()
    {
        return $this->f054fstatus;
    }

    /**
     * Set f054fbalance
     *
     * @param integer $f054fbalance
     *
     * @return T054factivityMaster
     */
    public function setF054fbalance($f054fbalance)
    {
        $this->f054fbalance = $f054fbalance;

        return $this;
    }

    /**
     * Get f054fbalance
     *
     * @return integer
     */
    public function getF054fbalance()
    {
        return $this->f054fbalance;
    }

    /**
     * Set f054freason
     *
     * @param string $f054freason
     *
     * @return T054factivityMaster
     */
    public function setF054freason($f054freason)
    {
        $this->f054freason = $f054freason;

        return $this;
    }

    /**
     * Get f054freason
     *
     * @return string
     */
    public function getF054freason()
    {
        return $this->f054freason;
    }
}
