<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T083freceiptPaymentInfo
 *
 * @ORM\Table(name="t083freceipt_payment_info", indexes={@ORM\Index(name="f083fid_receipt", columns={"f083fid_receipt"}), @ORM\Index(name="f083fid_payment", columns={"f083fid_payment"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ReceiptRepository")
 */
class T083freceiptPaymentInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f083fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f083fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fid_payment", type="string",length=30, nullable=false)
     */
    private $f083fidPayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083famount", type="integer", nullable=false)
     */
    private $f083famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f083freference_number", type="string", length=50, nullable=false)
     */
    private $f083freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fid_bank", type="integer", nullable=false)
     */
    private $f083fidBank;

     /**
     * @var integer
     *
     * @ORM\Column(name="f083fid_terminal", type="integer", nullable=false)
     */
    private $f083fidTerminal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fstatus", type="integer", nullable=false)
     */
    private $f083fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fcreated_by", type="integer", nullable=false)
     */
    private $f083fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fupdated_by", type="integer", nullable=false)
     */
    private $f083fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f083fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f083fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f083fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f083fupdatedDtTm;

    /**
     * @ORM\Column(name="f083fid_receipt", type="integer", nullable=false)
     */
    private $f083fidReceipt;

    

    /**
     * Get f083fid
     *
     * @return integer
     */
    public function getF083fid()
    {
        return $this->f083fid;
    }

    /**
     * Set f083fidPayment
     *
     * @param integer $f083fidPayment
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fidPayment($f083fidPayment)
    {
        $this->f083fidPayment = $f083fidPayment;

        return $this;
    }

    /**
     * Get f083fidPayment
     *
     * @return integer
     */
    public function getF083fidPayment()
    {
        return $this->f083fidPayment;
    }

    /**
     * Set f083famount
     *
     * @param integer $f083famount
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083famount($f083famount)
    {
        $this->f083famount = $f083famount;

        return $this;
    }

    /**
     * Get f083famount
     *
     * @return integer
     */
    public function getF083famount()
    {
        return $this->f083famount;
    }

    /**
     * Set f083freferenceNumber
     *
     * @param string $f083freferenceNumber
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083freferenceNumber($f083freferenceNumber)
    {
        $this->f083freferenceNumber = $f083freferenceNumber;

        return $this;
    }

    /**
     * Get f083freferenceNumber
     *
     * @return string
     */
    public function getF083freferenceNumber()
    {
        return $this->f083freferenceNumber;
    }

    /**
     * Set f083fidBank
     *
     * @param integer $f083fidBank
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fidBank($f083fidBank)
    {
        $this->f083fidBank = $f083fidBank;

        return $this;
    }

    /**
     * Get f083fidBank
     *
     * @return integer
     */
    public function getF083fidBank()
    {
        return $this->f083fidBank;
    }

    /**
     * Set f083fidTerminal
     *
     * @param integer $f083fidTerminal
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fidTerminal($f083fidTerminal)
    {
        $this->f083fidTerminal = $f083fidTerminal;

        return $this;
    }

    /**
     * Get f083fidTerminal
     *
     * @return integer
     */
    public function getF083fidTerminal()
    {
        return $this->f083fidTerminal;
    }

    /**
     * Set f083fstatus
     *
     * @param integer $f083fstatus
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fstatus($f083fstatus)
    {
        $this->f083fstatus = $f083fstatus;

        return $this;
    }

    /**
     * Get f083fstatus
     *
     * @return integer
     */
    public function getF083fstatus()
    {
        return $this->f083fstatus;
    }

    /**
     * Set f083fcreatedBy
     *
     * @param integer $f083fcreatedBy
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fcreatedBy($f083fcreatedBy)
    {
        $this->f083fcreatedBy = $f083fcreatedBy;

        return $this;
    }

    /**
     * Get f083fcreatedBy
     *
     * @return integer
     */
    public function getF083fcreatedBy()
    {
        return $this->f083fcreatedBy;
    }

    /**
     * Set f083fupdatedBy
     *
     * @param integer $f083fupdatedBy
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fupdatedBy($f083fupdatedBy)
    {
        $this->f083fupdatedBy = $f083fupdatedBy;

        return $this;
    }

    /**
     * Get f083fupdatedBy
     *
     * @return integer
     */
    public function getF083fupdatedBy()
    {
        return $this->f083fupdatedBy;
    }

    /**
     * Set f083fcreatedDtTm
     *
     * @param \DateTime $f083fcreatedDtTm
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fcreatedDtTm($f083fcreatedDtTm)
    {
        $this->f083fcreatedDtTm = $f083fcreatedDtTm;

        return $this;
    }

    /**
     * Get f083fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF083fcreatedDtTm()
    {
        return $this->f083fcreatedDtTm;
    }

    /**
     * Set f083fupdatedDtTm
     *
     * @param \DateTime $f083fupdatedDtTm
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fupdatedDtTm($f083fupdatedDtTm)
    {
        $this->f083fupdatedDtTm = $f083fupdatedDtTm;

        return $this;
    }

    /**
     * Get f083fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF083fupdatedDtTm()
    {
        return $this->f083fupdatedDtTm;
    }

    /**
     * Set f083fidReceipt
     *
     * @param \Application\Entity\T082freceipt $f083fidReceipt
     *
     * @return T083freceiptPaymentInfo
     */
    public function setF083fidReceipt($f083fidReceipt = null)
    {
        $this->f083fidReceipt = $f083fidReceipt;

        return $this;
    }

    /**
     * Get f083fidReceipt
     *
     * @return \Application\Entity\T082freceipt
     */
    public function getF083fidReceipt()
    {
        return $this->f083fidReceipt;
    }
}

