<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T020fapprovalLimit
 *
 * @ORM\Table(name="t020fapproval_limit")
 * @ORM\Entity(repositoryClass="Application\Repository\ApprovalLimitRepository")
 */
class T020fapprovalLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f020fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f020fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fid_user", type="integer", nullable=false)
     */
    private $f020fidUser;

    /**
     * @var string
     *
     * @ORM\Column(name="f020fmodule", type="integer", nullable=false)
     */
    private $f020fmodule;

    /**
     * @var float
     *
     * @ORM\Column(name="f020ffrom_amount", type="integer", nullable=false)
     */
    private $f020ffromAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f020fto_amount", type="integer", nullable=false)
     */
    private $f020ftoAmount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f020fstatus", type="boolean", nullable=false)
     */
    private $f020fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fupdated_by", type="integer", nullable=false)
     */
    private $f020fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fcreated_by", type="integer", nullable=false)
     */
    private $f020fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f020fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f020fupdatedDtTm;



    /**
     * Get f020fid
     *
     * @return integer
     */
    public function getF020fid()
    {
        return $this->f020fid;
    }

    /**
     * Set f020fidUser
     *
     * @param integer $f020fidUser
     *
     * @return T020fapprovalLimit
     */
    public function setF020fidUser($f020fidUser)
    {
        $this->f020fidUser = $f020fidUser;

        return $this;
    }

    /**
     * Get f020fidUser
     *
     * @return integer
     */
    public function getF020fidUser()
    {
        return $this->f020fidUser;
    }

    /**
     * Set f020fmodule
     *
     * @param string $f020fmodule
     *
     * @return T020fapprovalLimit
     */
    public function setF020fmodule($f020fmodule)
    {
        $this->f020fmodule = $f020fmodule;

        return $this;
    }

    /**
     * Get f020fmodule
     *
     * @return string
     */
    public function getF020fmodule()
    {
        return $this->f020fmodule;
    }

    /**
     * Set f020ffromAmount
     *
     * @param integer $f020ffromAmount
     *
     * @return T020fapprovalLimit
     */
    public function setF020ffromAmount($f020ffromAmount)
    {
        $this->f020ffromAmount = $f020ffromAmount;

        return $this;
    }

    /**
     * Get f020ffromAmount
     *
     * @return integer
     */
    public function getF020ffromAmount()
    {
        return $this->f020ffromAmount;
    }

    /**
     * Set f020ftoAmount
     *
     * @param integer $f020ftoAmount
     *
     * @return T020fapprovalLimit
     */
    public function setF020ftoAmount($f020ftoAmount)
    {
        $this->f020ftoAmount = $f020ftoAmount;

        return $this;
    }

    /**
     * Get f020ftoAmount
     *
     * @return integer
     */
    public function getF020ftoAmount()
    {
        return $this->f020ftoAmount;
    }

    /**
     * Set f020fstatus
     *
     * @param boolean $f020fstatus
     *
     * @return T020fapprovalLimit
     */
    public function setF020fstatus($f020fstatus)
    {
        $this->f020fstatus = $f020fstatus;

        return $this;
    }

    /**
     * Get f020fstatus
     *
     * @return boolean
     */
    public function getF020fstatus()
    {
        return $this->f020fstatus;
    }

    /**
     * Set f020fupdatedBy
     *
     * @param integer $f020fupdatedBy
     *
     * @return T020fapprovalLimit
     */
    public function setF020fupdatedBy($f020fupdatedBy)
    {
        $this->f020fupdatedBy = $f020fupdatedBy;

        return $this;
    }

    /**
     * Get f020fupdatedBy
     *
     * @return integer
     */
    public function getF020fupdatedBy()
    {
        return $this->f020fupdatedBy;
    }

    /**
     * Set f020fcreatedBy
     *
     * @param integer $f020fcreatedBy
     *
     * @return T020fapprovalLimit
     */
    public function setF020fcreatedBy($f020fcreatedBy)
    {
        $this->f020fcreatedBy = $f020fcreatedBy;

        return $this;
    }

    /**
     * Get f020fcreatedBy
     *
     * @return integer
     */
    public function getF020fcreatedBy()
    {
        return $this->f020fcreatedBy;
    }

    /**
     * Set f020fcreatedDtTm
     *
     * @param \DateTime $f020fcreatedDtTm
     *
     * @return T020fapprovalLimit
     */
    public function setF020fcreatedDtTm($f020fcreatedDtTm)
    {
        $this->f020fcreatedDtTm = $f020fcreatedDtTm;

        return $this;
    }

    /**
     * Get f020fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fcreatedDtTm()
    {
        return $this->f020fcreatedDtTm;
    }

    /**
     * Set f020fupdatedDtTm
     *
     * @param \DateTime $f020fupdatedDtTm
     *
     * @return T020fapprovalLimit
     */
    public function setF020fupdatedDtTm($f020fupdatedDtTm)
    {
        $this->f020fupdatedDtTm = $f020fupdatedDtTm;

        return $this;
    }

    /**
     * Get f020fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fupdatedDtTm()
    {
        return $this->f020fupdatedDtTm;
    }
}
