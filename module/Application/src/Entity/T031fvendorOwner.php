<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T031fvendorOwner
 *
 * @ORM\Table(name="t031fvendor_owner")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T031fvendorOwner  
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f031fid;
 
    /**
     * @var string
     *
     * @ORM\Column(name="f031fowner_name", type="string", length=25, nullable=false)
     */
    private $f031fownerName;

    /**
     * @var string
     *
     * @ORM\Column(name="f031fowner_ic", type="string", length=25, nullable=false)
     */
    private $f031fownerIc;

    /**
     * @var string
     *
     * @ORM\Column(name="f031fowner_designation", type="string", length=25, nullable=false)
     */
    private $f031fownerDesignation;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid_vendor", type="integer", nullable=false)
     */
    private $f031fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fstatus", type="integer", nullable=false)
     */
    private $f031fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fcreated_by", type="integer", nullable=false)
     */
    private $f031fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fupdated_by", type="integer", nullable=false)
     */
    private $f031fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f031fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f031fupdatedDtTm;



    /**
     * Get f031fid
     *
     * @return integer
     */
    public function getF031fid()
    {
        return $this->f031fid;
    }


    /**
     * Set f031fownerName
     *
     * @param string $f031fownerName
     *
     * @return T031fvendorOwner
     */
    public function setF031fownerName($f031fownerName)
    {
        $this->f031fownerName = $f031fownerName;

        return $this;
    }

    /**
     * Get f031fownerName
     *
     * @return string
     */
    public function getF031fownerName()
    {
        return $this->f031fownerName;
    }


    /**
     * Set f031fownerIc
     *
     * @param string $f031fownerIc
     *
     * @return T031fvendorOwner
     */
    public function setF031fownerIc($f031fownerIc)
    {
        $this->f031fownerIc = $f031fownerIc;

        return $this;
    }

    /**
     * Get f031fownerIc
     *
     * @return string
     */
    public function getF031fownerIc()
    {
        return $this->f031fownerIc;
    }


    /**
     * Set f031fownerDesignation
     *
     * @param string $f031fownerDesignation
     *
     * @return T031fvendorOwner
     */
    public function setF031fownerDesignation($f031fownerDesignation)
    {
        $this->f031fownerDesignation = $f031fownerDesignation;

        return $this;
    }

    /**
     * Get f031fownerDesignation
     *
     * @return string
     */
    public function getF031fownerDesignation()
    {
        return $this->f031fownerDesignation;
    }

    /**
     * Set f031fstatus
     *
     * @param integer $f031fstatus
     *
     * @return T031fvendorOwner
     */
    public function setF031fstatus($f031fstatus)
    {
        $this->f031fstatus = $f031fstatus;

        return $this;
    }

    /**
     * Get f031fstatus
     *
     * @return integer
     */
    public function getF031fstatus()
    {
        return $this->f031fstatus;
    }

    /**
     * Set f031fidVendor
     *
     * @param integer $f031fidVendor
     *
     * @return T031fvendorOwner
     */
    public function setF031fidVendor($f031fidVendor)
    {
        $this->f031fidVendor = $f031fidVendor;

        return $this;
    }

    /**
     * Get f031fidVendor
     *
     * @return integer
     */
    public function getF031fidVendor()
    {
        return $this->f031fidVendor;
    }

    /**
     * Set f031fcreatedBy
     *
     * @param integer $f031fcreatedBy
     *
     * @return T031fvendorOwner
     */
    public function setF031fcreatedBy($f031fcreatedBy)
    {
        $this->f031fcreatedBy = $f031fcreatedBy;

        return $this;
    }

    /**
     * Get f031fcreatedBy
     *
     * @return integer
     */
    public function getF031fcreatedBy()
    {
        return $this->f031fcreatedBy;
    }

    /**
     * Set f031fupdatedBy
     *
     * @param integer $f031fupdatedBy
     *
     * @return T031fvendorOwner
     */
    public function setF031fupdatedBy($f031fupdatedBy)
    {
        $this->f031fupdatedBy = $f031fupdatedBy;

        return $this;
    }

    /**
     * Get f031fupdatedBy
     *
     * @return integer
     */
    public function getF031fupdatedBy()
    {
        return $this->f031fupdatedBy;
    }

    /**
     * Set f031fcreatedDtTm
     *
     * @param \DateTime $f031fcreatedDtTm
     *
     * @return T031fvendorOwner
     */
    public function setF031fcreatedDtTm($f031fcreatedDtTm)
    {
        $this->f031fcreatedDtTm = $f031fcreatedDtTm;

        return $this;
    }

    /**
     * Get f031fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF031fcreatedDtTm()
    {
        return $this->f031fcreatedDtTm;
    }

    /**
     * Set f031fupdatedDtTm
     *
     * @param \DateTime $f031fupdatedDtTm
     *
     * @return T031fvendorOwner
     */
    public function setF031fupdatedDtTm($f031fupdatedDtTm)
    {
        $this->f031fupdatedDtTm = $f031fupdatedDtTm;

        return $this;
    }

    /**
     * Get f031fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF031fupdatedDtTm()
    {
        return $this->f031fupdatedDtTm;
    }
}
