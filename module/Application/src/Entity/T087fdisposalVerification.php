<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T087fdisposalVerification
 *
 * @ORM\Table(name="t087fdisposal_verification")
 * @ORM\Entity(repositoryClass="Application\Repository\DisposalVerificationRepository")
 */
class T087fdisposalVerification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f087fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f087fmeeting_number", type="string", length=30, nullable=true)
     */
    private $f087fmeetingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f087ftype", type="string", length=100, nullable=true)
     */
    private $f087ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fremarks", type="string",length=50, nullable=true)
     */
    private $f087fremarks;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087freference_number", type="string",length=50, nullable=true)
     */
    private $f087freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fstatus", type="integer", nullable=true)
     */
    private $f087fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fcreated_by", type="integer", nullable=true)
     */
    private $f087fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fupdated_by", type="integer", nullable=true)
     */
    private $f087fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f087fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f087fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fid_requisition", type="integer", nullable=true)
     */
    private $f087fidRequisition;

    



    /**
     * Get f087fid
     *
     * @return integer
     */
    public function getF087fid()
    {
        return $this->f087fid;
    }

    /**
     * Set f087fmeetingNumber
     *
     * @param string $f087fmeetingNumber
     *
     * @return T087fdisposalVerification
     */
    public function setF087fmeetingNumber($f087fmeetingNumber)
    {
        $this->f087fmeetingNumber = $f087fmeetingNumber;

        return $this;
    }

    /**
     * Get f087fmeetingNumber
     *
     * @return string
     */
    public function getF087fmeetingNumber()
    {
        return $this->f087fmeetingNumber;
    }


    /**
     * Set f087ftype
     *
     * @param string $f087ftype
     *
     * @return T087fdisposalVerification
     */
    public function setF087ftype($f087ftype)
    {
        $this->f087ftype = $f087ftype;

        return $this;
    }

    /**
     * Get f087ftype
     *
     * @return string
     */
    public function getF087ftype()
    {
        return $this->f087ftype;
    }

    /**
     * Set f087fremarks
     *
     * @param integer $f087fremarks
     *
     * @return T087fdisposalVerification
     */
    public function setF087fremarks($f087fremarks)
    {
        $this->f087fremarks = $f087fremarks;

        return $this;
    }

    /**
     * Get f087fremarks
     *
     * @return integer
     */
    public function getF087fremarks()
    {
        return $this->f087fremarks;
    }

    /**
     * Set f087fstatus
     *
     * @param integer $f087fstatus
     *
     * @return T087fdisposalVerification
     */
    public function setF087fstatus($f087fstatus)
    {
        $this->f087fstatus = $f087fstatus;

        return $this;
    }

    /**
     * Get f087freferenceNumber
     *
     * @return integer
     */
    public function getF087freferenceNumber()
    {
        return $this->f087freferenceNumber;
    }

    /**
     * Set f087freferenceNumber
     *
     * @param integer $f087freferenceNumber
     *
     * @return T087fdisposalVerification
     */
    public function setF087freferenceNumber($f087freferenceNumber)
    {
        $this->f087freferenceNumber = $f087freferenceNumber;

        return $this;
    }

    

    /**
     * Set f087fcreatedBy
     *
     * @param integer $f087fcreatedBy
     *
     * @return T087fdisposalVerification
     */
    public function setF087fcreatedBy($f087fcreatedBy)
    {
        $this->f087fcreatedBy = $f087fcreatedBy;

        return $this;
    }

    /**
     * Get f087fcreatedBy
     *
     * @return integer
     */
    public function getF087fcreatedBy()
    {
        return $this->f087fcreatedBy;
    }

    /**
     * Set f087fupdatedBy
     *
     * @param integer $f087fupdatedBy
     *
     * @return T087fdisposalVerification
     */
    public function setF087fupdatedBy($f087fupdatedBy)
    {
        $this->f087fupdatedBy = $f087fupdatedBy;

        return $this;
    }

    /**
     * Get f087fupdatedBy
     *
     * @return integer
     */
    public function getF087fupdatedBy()
    {
        return $this->f087fupdatedBy;
    }

    /**
     * Set f087fcreatedDtTm
     *
     * @param \DateTime $f087fcreatedDtTm
     *
     * @return T087fdisposalVerification
     */
    public function setF087fcreatedDtTm($f087fcreatedDtTm)
    {
        $this->f087fcreatedDtTm = $f087fcreatedDtTm;

        return $this;
    }

    /**
     * Get f087fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fcreatedDtTm()
    {
        return $this->f087fcreatedDtTm;
    }

    /**
     * Set f087fupdatedDtTm
     *
     * @param \DateTime $f087fupdatedDtTm
     *
     * @return T087fdisposalVerification
     */
    public function setF087fupdatedDtTm($f087fupdatedDtTm)
    {
        $this->f087fupdatedDtTm = $f087fupdatedDtTm;

        return $this;
    }

    /**
     * Get f087fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fupdatedDtTm()
    {
        return $this->f087fupdatedDtTm;
    }

    /**
     * Set f087fidRequisition
     *
     * @param integer $f087fidRequisition
     *
     * @return T087fdisposalVerification
     */
    public function setF087fidRequisition($f087fidRequisition)
    {
        $this->f087fidRequisition = $f087fidRequisition;

        return $this;
    }

    /**
     * Get f087fidRequisition
     *
     * @return integer
     */
    public function getF087fidRequisition()
    {
        return $this->f087fidRequisition;
    }
}
