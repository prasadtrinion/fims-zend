<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T012fstate
 *
 * @ORM\Table(name="t012fstate",uniqueConstraints={@ORM\UniqueConstraint(name="f012fstate_name_UNIQUE", columns={"f012fstate_name"})}, indexes={@ORM\Index(name="f012fupdated_by", columns={"f012fupdated_by"}), @ORM\Index(name="f012fid_country", columns={"f012fid_country"})})
 * @ORM\Entity(repositoryClass="Application\Repository\StateRepository")
 */
class T012fstate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f012fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f012fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fstate_name", type="string", length=20, nullable=false)
     */
    private $f012fstateName;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fshort_name", type="string", length=20, nullable=false)
     */
    private $f012fshortName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f012fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f012fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f012fstatus", type="integer", nullable=false)
     */
    private $f012fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f012fupdated_by", type="integer", nullable=false)
     */
    
    private $f012fupdatedBy;
    /**
     * @var integer
     *
     * @ORM\Column(name="f012fid_country", type="integer", nullable=false)
     */
    
    private $f012fidCountry;



    /**
     * Get f012fid
     *
     * @return integer
     */
    public function getF012fid()
    {
        return $this->f012fid;
    }

    /**
     * Set f012fstateName
     *
     * @param string $f012fstateName
     *
     * @return T012fstate
     */
    public function setF012fstateName($f012fstateName)
    {
        $this->f012fstateName = $f012fstateName;

        return $this;
    }

    /**
     * Get f012fstateName
     *
     * @return string
     */
    public function getF012fstateName()
    {
        return $this->f012fstateName;
    }

    /**
     * Set f012fshortName
     *
     * @param string $f012fshortName
     *
     * @return T012fstate
     */
    public function setF012fshortName($f012fshortName)
    {
        $this->f012fshortName = $f012fshortName;

        return $this;
    }

    /**
     * Get f012fshortName
     *
     * @return string
     */
    public function getF012fshortName()
    {
        return $this->f012fshortName;
    }

    /**
     * Set f012fupdatedDtTm
     *
     * @param \DateTime $f012fupdatedDtTm
     *
     * @return T012fstate
     */
    public function setF012fupdatedDtTm($f012fupdatedDtTm)
    {
        $this->f012fupdatedDtTm = $f012fupdatedDtTm;

        return $this;
    }

    /**
     * Get f012fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF012fupdatedDtTm()
    {
        return $this->f012fupdatedDtTm;
    }

    /**
     * Set f012fstatus
     *
     * @param boolean $f012fstatus
     *
     * @return T012fstate
     */
    public function setF012fstatus($f012fstatus)
    {
        $this->f012fstatus = $f012fstatus;

        return $this;
    }

    /**
     * Get f012fstatus
     *
     * @return boolean
     */
    public function getF012fstatus()
    {
        return $this->f012fstatus;
    }

    /**
     * Set f012fupdatedBy
     *
     * @param string $f012fupdatedBy
     *
     * @return T012fstate
     */
    public function setF012fupdatedBy($f012fupdatedBy)
    {
        $this->f012fupdatedBy = $f012fupdatedBy;

        return $this;
    }

    
    public function getF012fupdatedBy()
    {
        return $this->f012fupdatedBy;
    }

    /**
     * Set f012fidCountry
     *
     * @param string $f012fidCountry
     *
     * @return T012fstate
     */
    public function setF012fidCountry($f012fidCountry )
    {
        $this->f012fidCountry = $f012fidCountry;

        return $this;
    }

    
    public function getF012fidCountry()
    {
        return $this->f012fidCountry;
    }
}
