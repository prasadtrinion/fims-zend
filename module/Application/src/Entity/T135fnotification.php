<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T135fnotification
 *
 * @ORM\Table(name="t135fnotification")
 * @ORM\Entity(repositoryClass="Application\Repository\NotificationRepository")
 */
class T135fnotification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f135fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f135fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f135fdays", type="integer",  nullable=false)
     */
    private $f135fdays;

    /**
     * @var string
     *
     * @ORM\Column(name="f135fmaturity_date", type="datetime", nullable=false)
     */
    private $f135fmaturityDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f135fstatus", type="integer", nullable=false)
     */
    private $f135fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f135fcreated_by", type="integer", nullable=false)
     */
    private $f135fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f135fupdated_by", type="integer", nullable=false)
     */
    private $f135fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f135fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f135fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f135fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f135fupdatedDtTm;



    /**
     * Get f135fid
     *
     * @return integer
     */
    public function getF135fid()
    {
        return $this->f135fid;
    }

    /**
     * Set f135fdays
     *
     * @param string $f135fdays
     *
     * @return T135fnotification
     */
    public function setF135fdays($f135fdays)
    {
        $this->f135fdays = $f135fdays;

        return $this;
    }

    /**
     * Get f135fdays
     *
     * @return string
     */
    public function getF135fdays()
    {
        return $this->f135fdays;
    }

    /**
     * Set f135fmaturityDate
     *
     * @param string $f135fmaturityDate
     *
     * @return T135fnotification
     */
    public function setF135fmaturityDate($f135fmaturityDate)
    {
        $this->f135fmaturityDate = $f135fmaturityDate;

        return $this;
    }

    /**
     * Get f135fmaturityDate
     *
     * @return string
     */
    public function getF135fmaturityDate()
    {
        return $this->f135fmaturityDate;
    }


    /**
     * Set f135fstatus
     *
     * @param boolean $f135fstatus
     *
     * @return T135fnotification
     */
    public function setF135fstatus($f135fstatus)
    {
        $this->f135fstatus = $f135fstatus;

        return $this;
    }


    /**
     * Get f135fstatus
     *
     * @return boolean
     */
    public function getF135fstatus()
    {
        return $this->f135fstatus;
    }

    /**
     * Set f135fcreatedBy
     *
     * @param integer $f135fcreatedBy
     *
     * @return T135fnotification
     */
    public function setF135fcreatedBy($f135fcreatedBy)
    {
        $this->f135fcreatedBy = $f135fcreatedBy;

        return $this;
    }

    /**
     * Get f135fcreatedBy
     *
     * @return integer
     */
    public function getF135fcreatedBy()
    {
        return $this->f135fcreatedBy;
    }

    /**
     * Set f135fupdatedBy
     *
     * @param integer $f135fupdatedBy
     *
     * @return T135fnotification
     */
    public function setF135fupdatedBy($f135fupdatedBy)
    {
        $this->f135fupdatedBy = $f135fupdatedBy;

        return $this;
    }

    /**
     * Get f135fupdatedBy
     *
     * @return integer
     */
    public function getF135fupdatedBy()
    {
        return $this->f135fupdatedBy;
    }

    /**
     * Set f135fcreatedDtTm
     *
     * @param \DateTime $f135fcreatedDtTm
     *
     * @return T135fnotification
     */
    public function setF135fcreatedDtTm($f135fcreatedDtTm)
    {
        $this->f135fcreatedDtTm = $f135fcreatedDtTm;

        return $this;
    }

    /**
     * Get f135fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF135fcreatedDtTm()
    {
        return $this->f135fcreatedDtTm;
    }

    /**
     * Set f135fupdatedDtTm
     *
     * @param \DateTime $f135fupdatedDtTm
     *
     * @return T135fnotification
     */
    public function setF135fupdatedDtTm($f135fupdatedDtTm)
    {
        $this->f135fupdatedDtTm = $f135fupdatedDtTm;

        return $this;
    }

    /**
     * Get f135fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF135fupdatedDtTm()
    {
        return $this->f135fupdatedDtTm;
    }
}
