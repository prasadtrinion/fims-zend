<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T139fetfGroupingDetails
 *
 * @ORM\Table(name="t139fetf_grouping_details")
 * @ORM\Entity(repositoryClass="Application\Repository\EtfGroupingRepository")
 */
class T139fetfGroupingDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f139fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f139fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f139fid_master", type="integer",  nullable=false)
     */
    private $f139fidMaster;

    /**
     * @var string
     *
     * @ORM\Column(name="f139fid_etf", type="integer", nullable=false)
     */
    private $f139fidEtf;

    /**
     * @var integer
     *
     * @ORM\Column(name="f139famount", type="integer", nullable=false)
     * 
     */
    private $f139famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f139fstatus", type="integer", nullable=false)
     */
    private $f139fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f139fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f139fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f139fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f139fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f139fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f139fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f139fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f139fupdatedDtTm;
 	

    /**
     * Get f139fid
     *
     * @return integer
     */
    public function getF139fid()
    {
        return $this->f139fid;
    }

    /**
     * Set f139fidMaster
     *
     * @param string $f139fidMaster
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fidMaster($f139fidMaster)
    {
        $this->f139fidMaster = $f139fidMaster;

        return $this;
    }

    /**
     * Get f139fidMaster
     *
     * @return string
     */
    public function getF139fidMaster()
    {
        return $this->f139fidMaster;
    }

    /**
     * Set f139fidEtf
     *
     * @param string $f139fidEtf
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fidEtf($f139fidEtf)
    {
        $this->f139fidEtf = $f139fidEtf;

        return $this;
    }

    /**
     * Get f139fidEtf
     *
     * @return string
     */
    public function getF139fidEtf()
    {
        return $this->f139fidEtf;
    }


    /**
     * Set f139famount
     *
     * @param integer $f139famount
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139famount($f139famount)
    {
        $this->f139famount = $f139famount;

        return $this;
    }

    /**
     * Get f139famount
     *
     * @return integer
     */
    public function getF139famount()
    {
        return $this->f139famount;
    }

    /**
     * Set f139fupdatedDtTm
     *
     * @param \DateTime $f139fupdatedDtTm
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fupdatedDtTm($f139fupdatedDtTm)
    {
        $this->f139fupdatedDtTm = $f139fupdatedDtTm;

        return $this;
    }

    /**
     * Get f139fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF139fupdatedDtTm()
    {
        return $this->f139fupdatedDtTm;
    }

      /**
     * Set f139fcreatedDtTm
     *
     * @param \DateTime $f139fcreatedDtTm
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fcreatedDtTm($f139fcreatedDtTm)
    {
        $this->f139fcreatedDtTm = $f139fcreatedDtTm;

        return $this;
    }

    /**
     * Get f139fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF139fcreatedDtTm()
    {
        return $this->f139fcreatedDtTm;
    }

    /**
     * Set f139fstatus
     *
     * @param integer $f139fstatus
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fstatus($f139fstatus)
    {
        $this->f139fstatus = $f139fstatus;

        return $this;
    }

    /**
     * Get f139fstatus
     *
     * @return integer
     */
    public function getF139fstatus()
    {
        return $this->f139fstatus;
    }

    /**
     * Set f139fupdatedBy
     *
     * @param integer $f139fupdatedBy
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fupdatedBy($f139fupdatedBy)
    {
        $this->f139fupdatedBy = $f139fupdatedBy;

        return $this;
    }

    /**
     * Get f139fupdatedBy
     *
     * @return integer
     */
    public function getF139fupdatedBy()
    {
        return $this->f139fupdatedBy;
    }

     /**
     * Set f139fcreatedBy
     *
     * @param integer $f139fcreatedBy
     *
     * @return T139fetfGroupingDetails
     */
    public function setF139fcreatedBy($f139fcreatedBy)
    {
        $this->f139fcreatedBy = $f139fcreatedBy;

        return $this;
    }

    /**
     * Get f139fcreatedBy
     *
     * @return integer
     */
    public function getF139fcreatedBy()
    {
        return $this->f139fcreatedBy;
    }

}
