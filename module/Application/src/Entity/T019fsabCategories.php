<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T019fsabCategories
 *
 * @ORM\Table(name="t019fsab_categories", indexes={@ORM\Index(name="f019updatedby", columns={"f019fupdatedby"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SabCategoriesRepository")
 */
class T019fsabCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f019fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f019fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f019fdescription", type="string", length=50, nullable=false)
     */
    private $f019fdescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f019fstatus", type="integer", nullable=false)
     */
    private $f019fstatus = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="f019fcreatedby", type="integer", nullable=false)
     */
    private $f019fcreatedby;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fupdatedby", type="integer", nullable=false)
     */
    private $f019fupdatedby;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fcreateddttm", type="datetime", nullable=false)
     */
    private $f019fcreateddttm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fupdateddttm", type="datetime", nullable=false)
     */
    private $f019fupdateddttm;



    /**
     * Get f019fid
     *
     * @return integer
     */
    public function getF019fid()
    {
        return $this->f019fid;
    }

    /**
     * Set f019fdescription
     *
     * @param string $f019fdescription
     *
     * @return T019fsabCategories
     */
    public function setF019fdescription($f019fdescription)
    {
        $this->f019fdescription = $f019fdescription;

        return $this;
    }

    /**
     * Get f019fdescription
     *
     * @return string
     */
    public function getF019fdescription()
    {
        return $this->f019fdescription;
    }

    /**
     * Set f019fstatus
     *
     * @param boolean $f019fstatus
     *
     * @return T019fsabCategories
     */
    public function setF019fstatus($f019fstatus)
    {
        $this->f019fstatus = $f019fstatus;

        return $this;
    }

    /**
     * Get f019fstatus
     *
     * @return boolean
     */
    public function getF019fstatus()
    {
        return $this->f019fstatus;
    }

    /**
     * Set f019fcreatedby
     *
     * @param string $f019fcreatedby
     *
     * @return T019fsabCategories
     */
    public function setF019fcreatedby($f019fcreatedby)
    {
        $this->f019fcreatedby = $f019fcreatedby;

        return $this;
    }

    /**
     * Get f019fcreatedby
     *
     * @return string
     */
    public function getF019fcreatedby()
    {
        return $this->f019fcreatedby;
    }

    /**
     * Set f019fupdatedby
     *
     * @param integer $f019fupdatedby
     *
     * @return T019fsabCategories
     */
    public function setF019fupdatedby($f019fupdatedby)
    {
        $this->f019fupdatedby = $f019fupdatedby;

        return $this;
    }

    /**
     * Get f019fupdatedby
     *
     * @return integer
     */
    public function getF019fupdatedby()
    {
        return $this->f019fupdatedby;
    }

    /**
     * Set f019fcreateddttm
     *
     * @param \DateTime $f019fcreateddttm
     *
     * @return T019fsabCategories
     */
    public function setF019fcreateddttm($f019fcreateddttm)
    {
        $this->f019fcreateddttm = $f019fcreateddttm;

        return $this;
    }

    /**
     * Get f019fcreateddttm
     *
     * @return \DateTime
     */
    public function getF019fcreateddttm()
    {
        return $this->f019fcreateddttm;
    }

    /**
     * Set f019fupdateddttm
     *
     * @param \DateTime $f019fupdateddttm
     *
     * @return T019fsabCategories
     */
    public function setF019fupdateddttm($f019fupdateddttm)
    {
        $this->f019fupdateddttm = $f019fupdateddttm;

        return $this;
    }

    /**
     * Get f019fupdateddttm
     *
     * @return \DateTime
     */
    public function getF019fupdateddttm()
    {
        return $this->f019fupdateddttm;
    }
}
