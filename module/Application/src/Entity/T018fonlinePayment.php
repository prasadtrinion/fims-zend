<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T018fonlinePayment
 *
 * @ORM\Table(name="t018fonline_payment")
 * @ORM\Entity(repositoryClass="Application\Repository\OnlinePaymentRepository")
 */
class T018fonlinePayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f018fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fname", type="string", length=50, nullable=true)
     */
    private $f018fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fcode", type="string", length=50, nullable=true)
     */
    private $f018fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fstatus", type="integer", nullable=false)
     */
    private $f018fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcreated_by", type="integer", nullable=true)
     */
    private $f018fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fupdated_by", type="integer", nullable=true)
     */
    private $f018fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f018fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f018fupdatedDtTm;



    /**
     * Get f018fid
     *
     * @return integer
     */
    public function getF018fid()
    {
        return $this->f018fid;
    }

    /**
     * Set f018fname
     *
     * @param string $f018fname
     *
     * @return T018fonlinePayment
     */
    public function setF018fname($f018fname)
    {
        $this->f018fname = $f018fname;

        return $this;
    }

    /**
     * Get f018fcode
     *
     * @return string
     */
    public function getF018fcode()
    {
        return $this->f018fcode;
    }

     /**
     * Set f018fcode
     *
     * @param string $f018fcode
     *
     * @return T018fonlinePayment
     */
    public function setF018fcode($f018fcode)
    {
        $this->f018fcode = $f018fcode;

        return $this;
    }

    /**
     * Get f018fname
     *
     * @return string
     */
    public function getF018fname()
    {
        return $this->f018fname;
    }

    /**
     * Set f018fstatus
     *
     * @param integer $f018fstatus
     *
     * @return T018fonlinePayment
     */
    public function setF018fstatus($f018fstatus)
    {
        $this->f018fstatus = $f018fstatus;

        return $this;
    }

    /**
     * Get f018fstatus
     *
     * @return integer
     */
    public function getF018fstatus()
    {
        return $this->f018fstatus;
    }

    /**
     * Set f018fcreatedBy
     *
     * @param integer $f018fcreatedBy
     *
     * @return T018fonlinePayment
     */
    public function setF018fcreatedBy($f018fcreatedBy)
    {
        $this->f018fcreatedBy = $f018fcreatedBy;

        return $this;
    }

    /**
     * Get f018fcreatedBy
     *
     * @return integer
     */
    public function getF018fcreatedBy()
    {
        return $this->f018fcreatedBy;
    }

    /**
     * Set f018fupdatedBy
     *
     * @param integer $f018fupdatedBy
     *
     * @return T018fonlinePayment
     */
    public function setF018fupdatedBy($f018fupdatedBy)
    {
        $this->f018fupdatedBy = $f018fupdatedBy;

        return $this;
    }

    /**
     * Get f018fupdatedBy
     *
     * @return integer
     */
    public function getF018fupdatedBy()
    {
        return $this->f018fupdatedBy;
    }

    /**
     * Set f018fcreatedDtTm
     *
     * @param \DateTime $f018fcreatedDtTm
     *
     * @return T018fonlinePayment
     */
    public function setF018fcreatedDtTm($f018fcreatedDtTm)
    {
        $this->f018fcreatedDtTm = $f018fcreatedDtTm;

        return $this;
    }

    /**
     * Get f018fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF018fcreatedDtTm()
    {
        return $this->f018fcreatedDtTm;
    }

    /**
     * Set f018fupdatedDtTm
     *
     * @param \DateTime $f018fupdatedDtTm
     *
     * @return T018fonlinePayment
     */
    public function setF018fupdatedDtTm($f018fupdatedDtTm)
    {
        $this->f018fupdatedDtTm = $f018fupdatedDtTm;

        return $this;
    }

    /**
     * Get f018fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF018fupdatedDtTm()
    {
        return $this->f018fupdatedDtTm;
    }
}
