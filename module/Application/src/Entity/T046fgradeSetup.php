<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T046fgradeSetup
 *
 * @ORM\Table(name="t046fgrade_setup",uniqueConstraints={@ORM\UniqueConstraint(name="f046fgrade_name_UNIQUE", columns={"f046fgrade_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\GradeSetupRepository")
 */
class T046fgradeSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f046fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f046fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f046fgrade_name", type="string", length=50, nullable=false)
     */
    private $f046fgradeName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fgrade_from", type="integer", nullable=false)
     */
    private $f046fgradeFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fgrade_to", type="integer", nullable=false)
     */
    private $f046fgradeTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046floan_type", type="integer", nullable=false)
     */
    private $f046floanType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fvehicle_condition", type="integer", nullable=true)
     */
    private $f046fvehicleCondition;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fstatus", type="integer", nullable=false)
     */
    private $f046fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fcreated_by", type="integer", nullable=false)
     */
    private $f046fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fupdated_by", type="integer", nullable=false)
     */
    private $f046fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f046fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f046fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fschedule", type="datetime", nullable=true)
     */
    private $f046fschedule='NULL';

   /**
     * @var string
     *
     * @ORM\Column(name="f046fgrade_month_from", type="string", length=50, nullable=true)
     */
    private $f046fgradeMonthFrom='NULL';

   /**
     * @var string
     *
     * @ORM\Column(name="f046fgrade_month_to", type="string", length=50, nullable=true)
     */
    private $f046fgradeMonthTo='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f046fgrade_description", type="string", length=50, nullable=true)
     */
    private $f046fgradeDescription='NULL';



    /**
     * Get f046fid
     *
     * @return integer
     */
    public function getF046fid()
    {
        return $this->f046fid;
    }

    /**
     * Set f046fgradeName
     *
     * @param string $f046fgradeName
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeName($f046fgradeName)
    {
        $this->f046fgradeName = $f046fgradeName;

        return $this;
    }

    /**
     * Get f046fgradeName
     *
     * @return string
     */
    public function getF046fgradeName()
    {
        return $this->f046fgradeName;
    }

    /**
     * Set f046fvehicleCondition
     *
     * @param string $f046fvehicleCondition
     *
     * @return T046fgradeSetup
     */
    public function setF046fvehicleCondition($f046fvehicleCondition)
    {
        $this->f046fvehicleCondition = $f046fvehicleCondition;

        return $this;
    }

    /**
     * Get f046fgradeName
     *
     * @return string
     */
    public function getF046fvehicleCondition()
    {
        return $this->f046fvehicleCondition;
    }

    /**
     * Set f046fgradeFrom
     *
     * @param integer $f046fgradeFrom
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeFrom($f046fgradeFrom)
    {
        $this->f046fgradeFrom = $f046fgradeFrom;

        return $this;
    }

    /**
     * Get f046fgradeFrom
     *
     * @return integer
     */
    public function getF046fgradeFrom()
    {
        return $this->f046fgradeFrom;
    }

    /**
     * Set f046floanType
     *
     * @param integer $f046floanType
     *
     * @return T046fgradeSetup
     */
    public function setF046floanType($f046floanType)
    {
        $this->f046floanType = $f046floanType;

        return $this;
    }

    /**
     * Get f046floanType
     *
     * @return integer
     */
    public function getF046floanType()
    {
        return $this->f046floanType;
    }

    /**
     * Set f046fgradeTo
     *
     * @param integer $f046fgradeTo
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeTo($f046fgradeTo)
    {
        $this->f046fgradeTo = $f046fgradeTo;

        return $this;
    }

    /**
     * Get f046fgradeTo
     *
     * @return integer
     */
    public function getF046fgradeTo()
    {
        return $this->f046fgradeTo;
    }

    /**
     * Set f046fstatus
     *
     * @param integer $f046fstatus
     *
     * @return T046fgradeSetup
     */
    public function setF046fstatus($f046fstatus)
    {
        $this->f046fstatus = $f046fstatus;

        return $this;
    }

    /**
     * Get f046fstatus
     *
     * @return integer
     */
    public function getF046fstatus()
    {
        return $this->f046fstatus;
    }

    /**
     * Set f046fcreatedBy
     *
     * @param integer $f046fcreatedBy
     *
     * @return T046fgradeSetup
     */
    public function setF046fcreatedBy($f046fcreatedBy)
    {
        $this->f046fcreatedBy = $f046fcreatedBy;

        return $this;
    }

    /**
     * Get f046fcreatedBy
     *
     * @return integer
     */
    public function getF046fcreatedBy()
    {
        return $this->f046fcreatedBy;
    }

    /**
     * Set f046fupdatedBy
     *
     * @param integer $f046fupdatedBy
     *
     * @return T046fgradeSetup
     */
    public function setF046fupdatedBy($f046fupdatedBy)
    {
        $this->f046fupdatedBy = $f046fupdatedBy;

        return $this;
    }

    /**
     * Get f046fupdatedBy
     *
     * @return integer
     */
    public function getF046fupdatedBy()
    {
        return $this->f046fupdatedBy;
    }

    /**
     * Set f046fcreatedDtTm
     *
     * @param \DateTime $f046fcreatedDtTm
     *
     * @return T046fgradeSetup
     */
    public function setF046fcreatedDtTm($f046fcreatedDtTm)
    {
        $this->f046fcreatedDtTm = $f046fcreatedDtTm;

        return $this;
    }

    /**
     * Get f046fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF046fcreatedDtTm()
    {
        return $this->f046fcreatedDtTm;
    }

    /**
     * Set f046fupdatedDtTm
     *
     * @param \DateTime $f046fupdatedDtTm
     *
     * @return T046fgradeSetup
     */
    public function setF046fupdatedDtTm($f046fupdatedDtTm)
    {
        $this->f046fupdatedDtTm = $f046fupdatedDtTm;

        return $this;
    }

    /**
     * Get f046fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF046fupdatedDtTm()
    {
        return $this->f046fupdatedDtTm;
    }

    /**
     * Set f046fschedule
     *
     * @param \DateTime $f046fschedule
     *
     * @return T046fgradeSetup
     */
    public function setF046fschedule($f046fschedule)
    {
        $this->f046fschedule = $f046fschedule;

        return $this;
    }

    /**
     * Get f046fschedule
     *
     * @return \DateTime
     */
    public function getF046fschedule()
    {
        return $this->f046fschedule;
    }

    /**
     * Set f046fgradeMonthFrom
     *
     * @param string $f046fgradeMonthFrom
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeMonthFrom($f046fgradeMonthFrom)
    {
        $this->f046fgradeMonthFrom = $f046fgradeMonthFrom;

        return $this;
    }

    /**
     * Get f046fgradeMonthFrom
     *
     * @return string
     */
    public function getF046fgradeMonthFrom()
    {
        return $this->f046fgradeMonthFrom;
    }

   /**
     * Set f046fgradeMonthTo
     *
     * @param string $f046fgradeMonthTo
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeMonthTo($f046fgradeMonthTo)
    {
        $this->f046fgradeMonthTo = $f046fgradeMonthTo;

        return $this;
    }

    /**
     * Get f046fgradeMonthTo
     *
     * @return string
     */
    public function getF046fgradeMonthTo()
    {
        return $this->f046fgradeMonthTo;
    }

    /**
     * Set f046fgradeDescription
     *
     * @param string $f046fgradeDescription
     *
     * @return T046fgradeSetup
     */
    public function setF046fgradeDescription($f046fgradeDescription)
    {
        $this->f046fgradeDescription = $f046fgradeDescription;

        return $this;
    }

    /**
     * Get f046fgradeDescription
     *
     * @return string
     */
    public function getF046fgradeDescription()
    {
        return $this->f046fgradeDescription;
    }
}
