<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T116fcashAdvance
 *
 * @ORM\Table(name="t116fcash_advance")
 * @ORM\Entity(repositoryClass="Application\Repository\CashAdvanceRepository")
 */
class T116fcashAdvance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f116fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f116fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f116fstaff_type", type="string", length=255, nullable=true)
     */
    private $f116fstaffType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f116fstatus", type="integer", nullable=true)
     */
    private $f116fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f116fcreated_by", type="integer", nullable=true)
     */
    private $f116fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f116fupdated_by", type="integer", nullable=true)
     */
    private $f116fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f116fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f116fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f116fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f116fupdatedDtTm;



    /**
     * Get f116fid
     *
     * @return integer
     */
    public function getF116fid()
    {
        return $this->f116fid;
    }

    /**
     * Set f116fstaff_type
     *
     * @param string $f116fstaffType
     *
     * @return T116fcashAdvance
     */
    public function setF116fstaffType($f116fstaffType)
    {
        $this->f116fstaffType = $f116fstaffType;

        return $this;
    }

    /**
     * Get f116fstaffType
     *
     * @return string
     */
    public function getF116fstaffType()
    {
        return $this->f116fstaffType;
    }

    /**
     * Set f116fstatus
     *
     * @param integer $f116fstatus
     *
     * @return T116fcashAdvance
     */
    public function setF116fstatus($f116fstatus)
    {
        $this->f116fstatus = $f116fstatus;

        return $this;
    }

    /**
     * Get f116fstatus
     *
     * @return integer
     */
    public function getF116fstatus()
    {
        return $this->f116fstatus;
    }

    /**
     * Set f116fcreatedBy
     *
     * @param integer $f116fcreatedBy
     *
     * @return T116fcashAdvance
     */
    public function setF116fcreatedBy($f116fcreatedBy)
    {
        $this->f116fcreatedBy = $f116fcreatedBy;

        return $this;
    }

    /**
     * Get f116fcreatedBy
     *
     * @return integer
     */
    public function getF116fcreatedBy()
    {
        return $this->f116fcreatedBy;
    }

    /**
     * Set f116fupdatedBy
     *
     * @param integer $f116fupdatedBy
     *
     * @return T116fcashAdvance
     */
    public function setF116fupdatedBy($f116fupdatedBy)
    {
        $this->f116fupdatedBy = $f116fupdatedBy;

        return $this;
    }

    /**
     * Get f116fupdatedBy
     *
     * @return integer
     */
    public function getF116fupdatedBy()
    {
        return $this->f116fupdatedBy;
    }

    /**
     * Set f116fcreatedDtTm
     *
     * @param \DateTime $f116fcreatedDtTm
     *
     * @return T116fcashAdvance
     */
    public function setF116fcreatedDtTm($f116fcreatedDtTm)
    {
        $this->f116fcreatedDtTm = $f116fcreatedDtTm;

        return $this;
    }

    /**
     * Get f116fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF116fcreatedDtTm()
    {
        return $this->f116fcreatedDtTm;
    }

    /**
     * Set f116fupdatedDtTm
     *
     * @param \DateTime $f116fupdatedDtTm
     *
     * @return T116fcashAdvance
     */
    public function setF116fupdatedDtTm($f116fupdatedDtTm)
    {
        $this->f116fupdatedDtTm = $f116fupdatedDtTm;

        return $this;
    }

    /**
     * Get f116fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF116fupdatedDtTm()
    {
        return $this->f116fupdatedDtTm;
    }
}
