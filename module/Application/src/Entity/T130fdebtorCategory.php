<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T130fdebtorCategory
 *
 * @ORM\Table(name="t130fdebtor_category", uniqueConstraints={@ORM\UniqueConstraint(name="t130fdebtor_category", columns={"f130fdebtor_category_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\DebtorCategoryRepository")
 */
class T130fdebtorCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f130fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f130fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f130fdebtor_category_name", type="string", length=200, nullable=false)
     */
    private $f130fdebtorCategoryName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f130fstatus", type="integer", nullable=false)
     */
    private $f130fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f130fcreated_by", type="integer", nullable=false)
     */
    private $f130fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f130fupdated_by", type="integer", nullable=false)
     */
    private $f130fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f130fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f130fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f130fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f130fupdatedDtTm;



    /**
     * Get f130fid
     *
     * @return integer
     */
    public function getF130fid()
    {
        return $this->f130fid;
    }

    /**
     * Set f130fdebtorCategoryName
     *
     * @param string $f130fdebtorCategoryName
     *
     * @return T130fdebtorCategory
     */
    public function setF130fdebtorCategoryName($f130fdebtorCategoryName)
    {
        $this->f130fdebtorCategoryName = $f130fdebtorCategoryName;

        return $this;
    }

    /**
     * Get f130fdebtorCategoryName
     *
     * @return string
     */
    public function getF130fdebtorCategoryName()
    {
        return $this->f130fdebtorCategoryName;
    }

     /**
     * Set f130fstatus
     *
     * @param integer $f130fstatus
     *
     * @return T130loanDeductionRatio
     */
    public function setF130fstatus($f130fstatus)
    {
        $this->f130fstatus = $f130fstatus;

        return $this;
    }

    /**
     * Get f130fstatus
     *
     * @return integer
     */
    public function getF130fstatus()
    {
        return $this->f130fstatus;
    }

    /**
     * Set f130fcreatedBy
     *
     * @param integer $f130fcreatedBy
     *
     * @return T130fdebtorCategory
     */
    public function setF130fcreatedBy($f130fcreatedBy)
    {
        $this->f130fcreatedBy = $f130fcreatedBy;

        return $this;
    }

    /**
     * Get f130fcreatedBy
     *
     * @return integer
     */
    public function getF130fcreatedBy()
    {
        return $this->f130fcreatedBy;
    }

    /**
     * Set f130fupdatedBy
     *
     * @param integer $f130fupdatedBy
     *
     * @return T130fdebtorCategory
     */
    public function setF130fupdatedBy($f130fupdatedBy)
    {
        $this->f130fupdatedBy = $f130fupdatedBy;

        return $this;
    }

    /**
     * Get f130fupdatedBy
     *
     * @return integer
     */
    public function getF130fupdatedBy()
    {
        return $this->f130fupdatedBy;
    }

    /**
     * Set f130fcreatedDtTm
     *
     * @param \DateTime $f130fcreatedDtTm
     *
     * @return T130fdebtorCategory
     */
    public function setF130fcreatedDtTm($f130fcreatedDtTm)
    {
        $this->f130fcreatedDtTm = $f130fcreatedDtTm;

        return $this;
    }

    /**
     * Get f130fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF130fcreatedDtTm()
    {
        return $this->f130fcreatedDtTm;
    }

    /**
     * Set f130fupdatedDtTm
     *
     * @param \DateTime $f130fupdatedDtTm
     *
     * @return T130fdebtorCategory
     */
    public function setF130fupdatedDtTm($f130fupdatedDtTm)
    {
        $this->f130fupdatedDtTm = $f130fupdatedDtTm;

        return $this;
    }

    /**
     * Get f130fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF130fupdatedDtTm()
    {
        return $this->f130fupdatedDtTm;
    }
}
