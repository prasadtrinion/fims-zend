<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T101finsuranceCompanyDetails
 *
 * @ORM\Table(name="t101finsurance_company_details")
 * @ORM\Entity(repositoryClass="Application\Repository\InsuranceCompanyRepository")
 */
class T101finsuranceCompanyDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f101fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f101fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fid_master", type="integer",  nullable=true)
     */
    private $f101fidMaster;


    /**
     * @var string
     *
     * @ORM\Column(name="f101fage", type="string", length=50, nullable=true)
     */
    private $f101fage;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear1", type="string", length=50, nullable=true)
     */
    private $f101fyear1;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear2", type="string", length=50, nullable=true)
     */
    private $f101fyear2;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear3", type="string", length=50, nullable=true)
     */
    private $f101fyear3;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear4", type="string", length=50, nullable=true)
     */
    private $f101fyear4;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear5", type="string", length=50, nullable=true)
     */
    private $f101fyear5;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear6", type="string", length=50, nullable=true)
     */
    private $f101fyear6;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear7", type="string", length=50, nullable=true)
     */
    private $f101fyear7;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear8", type="string", length=50, nullable=true)
     */
    private $f101fyear8;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear9", type="string", length=50, nullable=true)
     */
    private $f101fyear9;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fyear10", type="string", length=50, nullable=true)
     */
    private $f101fyear10;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fstatus", type="integer", nullable=true)
     */
    private $f101fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fcreated_by", type="integer", nullable=true)
     */
    private $f101fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fupdated_by", type="integer", nullable=true)
     */
    private $f101fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f101fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f101fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f101fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f101fupdatedDtTm;



    /**
     * Get f101fidDetails
     *
     * @return integer
     */
    public function getF101fidDetails()
    {
        return $this->f101fidDetails;
    }

    /**
     * Set f101fidMaster
     *
     * @param string $f101fidMaster
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fidMaster($f101fidMaster)
    {
        $this->f101fidMaster = $f101fidMaster;

        return $this;
    }

    /**
     * Get f101fidMaster
     *
     * @return string
     */
    public function getF101fidMaster()
    {
        return $this->f101fidMaster;
    }

    /**
     * Set f101fage
     *
     * @param string $f101fage
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fage($f101fage)
    {
        $this->f101fage = $f101fage;

        return $this;
    }

    /**
     * Get f101fage
     *
     * @return string
     */
    public function getF101fage()
    {
        return $this->f101fage;
    }

    /**
     * Set f101fyear1
     *
     * @param string $f101fyear1
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear1($f101fyear1)
    {
        $this->f101fyear1 = $f101fyear1;

        return $this;
    }

    /**
     * Get f101fyear1
     *
     * @return string
     */
    public function getF101fyear1()
    {
        return $this->f101fyear1;
    }

    /**
     * Set f101fyear2
     *
     * @param string $f101fyear2
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear2($f101fyear2)
    {
        $this->f101fyear2 = $f101fyear2;

        return $this;
    }

    /**
     * Get f101fyear2
     *
     * @return string
     */
    public function getF101fyear2()
    {
        return $this->f101fyear2;
    }

    /**
     * Set f101fyear3
     *
     * @param string $f101fyear3
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear3($f101fyear3)
    {
        $this->f101fyear3 = $f101fyear3;

        return $this;
    }

    /**
     * Get f101fyear3
     *
     * @return string
     */
    public function getF101fyear3()
    {
        return $this->f101fyear3;
    }

    /**
     * Set f101fyear4
     *
     * @param string $f101fyear4
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear4($f101fyear4)
    {
        $this->f101fyear4 = $f101fyear4;

        return $this;
    }

    /**
     * Get f101fyear4
     *
     * @return string
     */
    public function getF101fyear4()
    {
        return $this->f101fyear4;
    }

    /**
     * Set f101fyear5
     *
     * @param string $f101fyear5
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear5($f101fyear5)
    {
        $this->f101fyear5 = $f101fyear5;

        return $this;
    }

    /**
     * Get f101fyear5
     *
     * @return string
     */
    public function getF101fyear5()
    {
        return $this->f101fyear5;
    }

    /**
     * Set f101fyear6
     *
     * @param string $f101fyear6
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear6($f101fyear6)
    {
        $this->f101fyear6 = $f101fyear6;

        return $this;
    }

    /**
     * Get f101fyear6
     *
     * @return string
     */
    public function getF101fyear6()
    {
        return $this->f101fyear6;
    }

    /**
     * Set f101fyear7
     *
     * @param string $f101fyear7
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear7($f101fyear7)
    {
        $this->f101fyear7 = $f101fyear7;

        return $this;
    }

    /**
     * Get f101fyear7
     *
     * @return string
     */
    public function getF101fyear7()
    {
        return $this->f101fyear7;
    }

    /**
     * Set f101fyear8
     *
     * @param string $f101fyear8
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear8($f101fyear8)
    {
        $this->f101fyear8 = $f101fyear8;

        return $this;
    }

    /**
     * Get f101fyear8
     *
     * @return string
     */
    public function getF101fyear8()
    {
        return $this->f101fyear8;
    }

    /**
     * Set f101fyear9
     *
     * @param string $f101fyear9
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear9($f101fyear9)
    {
        $this->f101fyear9 = $f101fyear9;

        return $this;
    }

    /**
     * Get f101fyear9
     *
     * @return string
     */
    public function getF101fyear9()
    {
        return $this->f101fyear9;
    }

    /**
     * Set f101fyear10
     *
     * @param string $f101fyear10
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fyear10($f101fyear10)
    {
        $this->f101fyear10 = $f101fyear10;

        return $this;
    }

    /**
     * Get f101fyear10
     *
     * @return string
     */
    public function getF101fyear10()
    {
        return $this->f101fyear10;
    }


    /**
     * Set f101fstatus
     *
     * @param integer $f101fstatus
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fstatus($f101fstatus)
    {
        $this->f101fstatus = $f101fstatus;

        return $this;
    }

    /**
     * Get f101fstatus
     *
     * @return integer
     */
    public function getF101fstatus()
    {
        return $this->f101fstatus;
    }

    /**
     * Set f101fcreatedBy
     *
     * @param integer $f101fcreatedBy
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fcreatedBy($f101fcreatedBy)
    {
        $this->f101fcreatedBy = $f101fcreatedBy;

        return $this;
    }

    /**
     * Get f101fcreatedBy
     *
     * @return integer
     */
    public function getF101fcreatedBy()
    {
        return $this->f101fcreatedBy;
    }

    /**
     * Set f101fupdatedBy
     *
     * @param integer $f101fupdatedBy
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fupdatedBy($f101fupdatedBy)
    {
        $this->f101fupdatedBy = $f101fupdatedBy;

        return $this;
    }

    /**
     * Get f101fupdatedBy
     *
     * @return integer
     */
    public function getF101fupdatedBy()
    {
        return $this->f101fupdatedBy;
    }

    /**
     * Set f101fcreatedDtTm
     *
     * @param \DateTime $f101fcreatedDtTm
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fcreatedDtTm($f101fcreatedDtTm)
    {
        $this->f101fcreatedDtTm = $f101fcreatedDtTm;

        return $this;
    }

    /**
     * Get f101fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF101fcreatedDtTm()
    {
        return $this->f101fcreatedDtTm;
    }

    /**
     * Set f101fupdatedDtTm
     *
     * @param \DateTime $f101fupdatedDtTm
     *
     * @return T101finsuranceCompanyDetails
     */
    public function setF101fupdatedDtTm($f101fupdatedDtTm)
    {
        $this->f101fupdatedDtTm = $f101fupdatedDtTm;

        return $this;
    }

    /**
     * Get f101fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF101fupdatedDtTm()
    {
        return $this->f101fupdatedDtTm;
    }


}

