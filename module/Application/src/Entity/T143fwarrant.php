<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T143fwarrant
 *
 * @ORM\Table(name="t143fwarrant")
 * @ORM\Entity(repositoryClass="Application\Repository\WarrantRepository")
 */
class T143fwarrant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f143fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f143fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f143freference_number", type="string", length=255, nullable=false)
     */
    private $f143freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f143fid_vendor", type="integer", nullable=false)
     */
    private $f143fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f143fdepartment", type="string", length=255, nullable=false)
     * 
     */
    private $f143fdepartment;

     /**
     * @var integer
     *
     * @ORM\Column(name="f143fdescription", type="string", length=255, nullable=false)
     * 
     */
    private $f143fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f143ftotal", type="integer", nullable=false)
     * 
     */
    private $f143ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f143fstatus", type="integer", nullable=false)
     */
    private $f143fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f143fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f143fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f143fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f143fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f143fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f143fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f143fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f143fupdatedDtTm;
 	

    /**
     * Get f143fid
     *
     * @return integer
     */
    public function getF143fid()
    {
        return $this->f143fid;
    }

    /**
     * Set f143freferenceNumber
     *
     * @param string $f143freferenceNumber
     *
     * @return T143fwarrant
     */
    public function setF143freferenceNumber($f143freferenceNumber)
    {
        $this->f143freferenceNumber = $f143freferenceNumber;

        return $this;
    }

    /**
     * Get f143freferenceNumber
     *
     * @return string
     */
    public function getF143freferenceNumber()
    {
        return $this->f143freferenceNumber;
    }

    /**
     * Set f143fidVendor
     *
     * @param string $f143fidVendor
     *
     * @return T143fwarrant
     */
    public function setF143fidVendor($f143fidVendor)
    {
        $this->f143fidVendor = $f143fidVendor;

        return $this;
    }

    /**
     * Get f143fidVendor
     *
     * @return string
     */
    public function getF143fidVendor()
    {
        return $this->f143fidVendor;
    }


    /**
     * Set f143fdepartment
     *
     * @param integer $f143fdepartment
     *
     * @return T143fwarrant
     */
    public function setF143fdepartment($f143fdepartment)
    {
        $this->f143fdepartment = $f143fdepartment;

        return $this;
    }

    /**
     * Get f143fdepartment
     *
     * @return integer
     */
    public function getF143fdepartment()
    {
        return $this->f143fdepartment;
    }

    /**
     * Set f143fdescription
     *
     * @param integer $f143fdescription
     *
     * @return T143fwarrant
     */
    public function setF143fdescription($f143fdescription)
    {
        $this->f143fdescription = $f143fdescription;

        return $this;
    }

    /**
     * Get f143fdescription
     *
     * @return integer
     */
    public function getF143fdescription()
    {
        return $this->f143fdescription;
    }

    /**
     * Set f143ftotal
     *
     * @param integer $f143ftotal
     *
     * @return T143fwarrant
     */
    public function setF143ftotal($f143ftotal)
    {
        $this->f143ftotal = $f143ftotal;

        return $this;
    }

    /**
     * Get f143ftotal
     *
     * @return integer
     */
    public function getF143ftotal()
    {
        return $this->f143ftotal;
    }


    /**
     * Set f143fupdatedDtTm
     *
     * @param \DateTime $f143fupdatedDtTm
     *
     * @return T143fwarrant
     */
    public function setF143fupdatedDtTm($f143fupdatedDtTm)
    {
        $this->f143fupdatedDtTm = $f143fupdatedDtTm;

        return $this;
    }

    /**
     * Get f143fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF143fupdatedDtTm()
    {
        return $this->f143fupdatedDtTm;
    }

      /**
     * Set f143fcreatedDtTm
     *
     * @param \DateTime $f143fcreatedDtTm
     *
     * @return T143fwarrant
     */
    public function setF143fcreatedDtTm($f143fcreatedDtTm)
    {
        $this->f143fcreatedDtTm = $f143fcreatedDtTm;

        return $this;
    }

    /**
     * Get f143fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF143fcreatedDtTm()
    {
        return $this->f143fcreatedDtTm;
    }

    /**
     * Set f143fstatus
     *
     * @param integer $f143fstatus
     *
     * @return T143fwarrant
     */
    public function setF143fstatus($f143fstatus)
    {
        $this->f143fstatus = $f143fstatus;

        return $this;
    }

    /**
     * Get f143fstatus
     *
     * @return integer
     */
    public function getF143fstatus()
    {
        return $this->f143fstatus;
    }

    /**
     * Set f143fupdatedBy
     *
     * @param integer $f143fupdatedBy
     *
     * @return T143fwarrant
     */
    public function setF143fupdatedBy($f143fupdatedBy)
    {
        $this->f143fupdatedBy = $f143fupdatedBy;

        return $this;
    }

    /**
     * Get f143fupdatedBy
     *
     * @return integer
     */
    public function getF143fupdatedBy()
    {
        return $this->f143fupdatedBy;
    }

     /**
     * Set f143fcreatedBy
     *
     * @param integer $f143fcreatedBy
     *
     * @return T143fwarrant
     */
    public function setF143fcreatedBy($f143fcreatedBy)
    {
        $this->f143fcreatedBy = $f143fcreatedBy;

        return $this;
    }

    /**
     * Get f143fcreatedBy
     *
     * @return integer
     */
    public function getF143fcreatedBy()
    {
        return $this->f143fcreatedBy;
    }

}
