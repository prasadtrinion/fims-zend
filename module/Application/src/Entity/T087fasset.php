<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T087fasset
 *
 * @ORM\Table(name="t087fasset", uniqueConstraints={@ORM\UniqueConstraint(name="f087fname_UNIQUE", columns={"f087fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AssetRepository")
 */
class T087fasset
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f087fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f087fname", type="string", length=50, nullable=false)
     */
    private $f087fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fstatus", type="integer", nullable=false)
     */
    private $f087fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f087fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f087fcreatedBy;


    /**
     * Get f087fid
     *
     * @return integer
     */
    public function getF087fid()
    {
        return $this->f087fid;
    }

    /**
     * Set f087fname
     *
     * @param string $f087fname
     *
     * @return T087fasset
     */
    public function setF087fname($f087fname)
    {
        $this->f087fname = $f087fname;

        return $this;
    }

    /**
     * Get f087fname
     *
     * @return string
     */
    public function getF087fname()
    {
        return $this->f087fname;
    }

    /**
     * Set f087fstatus
     *
     * @param integer $f087fstatus
     *
     * @return T087fasset
     */
    public function setF087fstatus($f087fstatus)
    {
        $this->f087fstatus = $f087fstatus;

        return $this;
    }

    /**
     * Get f087fstatus
     *
     * @return integer
     */
    public function getF087fstatus()
    {
        return $this->f087fstatus;
    }

    /**
     * Set f087fcreatedDtTm
     *
     * @param \DateTime $f087fcreatedDtTm
     *
     * @return T087fasset
     */
    public function setF087fcreatedDtTm($f087fcreatedDtTm)
    {
        $this->f087fcreatedDtTm = $f087fcreatedDtTm;

        return $this;
    }

    /**
     * Get f087fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fcreatedDtTm()
    {
        return $this->f087fcreatedDtTm;
    }

    /**
     * Set f087fupdatedDtTm
     *
     * @param \DateTime $f087fupdatedDtTm
     *
     * @return T087fasset
     */
    public function setF087fupdatedDtTm($f087fupdatedDtTm)
    {
        $this->f087fupdatedDtTm = $f087fupdatedDtTm;

        return $this;
    }

    /**
     * Get f087fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fupdatedDtTm()
    {
        return $this->f087fupdatedDtTm;
    }

    /**
     * Set f087fupdatedBy
     *
     * @param integer $f087fupdatedBy
     *
     * @return T087fasset
     */
    public function setF087fupdatedBy($f087fupdatedBy)
    {
        $this->f087fupdatedBy = $f087fupdatedBy;

        return $this;
    }

    
    public function getF087fupdatedBy()
    {
        return $this->f087fupdatedBy;
    }

    /**
     * Set f087fcreatedBy
     *
     * @param  integer $f087fcreatedBy
     *
     * @return T087fasset
     */
    public function setF087fcreatedBy($f087fcreatedBy)
    {
        $this->f087fcreatedBy = $f087fcreatedBy;

        return $this;
    }

    
    public function getF087fcreatedBy()
    {
        return $this->f087fcreatedBy;
    }
}
