<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T081ftextCode
 *
 * @ORM\Table(name="t081ftext_code",uniqueConstraints={@ORM\UniqueConstraint(name="f081fcode_UNIQUE", columns={"f081fcode"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TextCodeRepository")
 */
class T081ftextCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f081fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081ftype", type="integer", nullable=false)
     */
    private $f081ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fcode", type="string", length=50, nullable=true)
     */
    private $f081fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fpercentage", type="integer",  nullable=true)
     */
    private $f081fpercentage;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fid_account_code", type="string", length=20, nullable=false)
     */
    private $f081fidAccountCode;


    /**
     * @var string
     *
     * @ORM\Column(name="f081ftax_description", type="string", length=255, nullable=false)
     */
    private $f081ftaxDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstatus", type="integer", nullable=false)
     */
    private $f081fstatus = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fcreated_by", type="integer", nullable=true)
     */
    private $f081fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fupdated_by", type="integer", nullable=true)
     */
    private $f081fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fupdatedDtTm;



    /**
     * Get f081fid
     *
     * @return integer
     */
    public function getF081fid()
    {
        return $this->f081fid;
    }

    /**
     * Set f081ftype
     *
     * @param integer $f081ftype
     *
     * @return T081ftextCode
     */
    public function setF081ftype($f081ftype)
    {
        $this->f081ftype = $f081ftype;

        return $this;
    }

    /**
     * Get f081ftype
     *
     * @return integer
     */
    public function getF081ftype()
    {
        return $this->f081ftype;
    }

    /**
     * Set f081fcode
     *
     * @param string $f081fcode
     *
     * @return T081ftextCode
     */
    public function setF081fcode($f081fcode)
    {
        $this->f081fcode = $f081fcode;

        return $this;
    }

    /**
     * Get f081fcode
     *
     * @return string
     */
    public function getF081fcode()
    {
        return $this->f081fcode;
    }

    /**
     * Set f081fpercentage
     *
     * @param integer $f081fpercentage
     *
     * @return T081ftextCode
     */
    public function setF081fpercentage($f081fpercentage)
    {
        $this->f081fpercentage = $f081fpercentage;

        return $this;
    }

    /**
     * Get f081fpercentage
     *
     * @return integer
     */
    public function getF081fpercentage()
    {
        return $this->f081fpercentage;
    }

    /**
     * Set f081fidAccountCode
     *
     * @param string $f081fidAccountCode
     *
     * @return T081ftextCode
     */
    public function setF081fidAccountCode($f081fidAccountCode)
    {
        $this->f081fidAccountCode = $f081fidAccountCode;

        return $this;
    }

    /**
     * Get f081fidAccountCode
     *
     * @return string
     */
    public function getF081fidAccountCode()
    {
        return $this->f081fidAccountCode;
    }

    /**
     * Set f081ftaxDescription
     *
     * @param string $f081ftaxDescription
     *
     * @return T081ftextCode
     */
    public function setF081ftaxDescription($f081ftaxDescription)
    {
        $this->f081fidAccountCode = $f081ftaxDescription;

        return $this;
    }

    /**
     * Get f081ftaxDescription
     *
     * @return string
     */
    public function getF081ftaxDescription()
    {
        return $this->f081ftaxDescription;
    }


    /**
     * Set f081fstatus
     *
     * @param integer $f081fstatus
     *
     * @return T081ftextCode
     */
    public function setF081fstatus($f081fstatus)
    {
        $this->f081fstatus = $f081fstatus;

        return $this;
    }

    /**
     * Get f081fstatus
     *
     * @return integer
     */
    public function getF081fstatus()
    {
        return $this->f081fstatus;
    }

    /**
     * Set f081fcreatedBy
     *
     * @param integer $f081fcreatedBy
     *
     * @return T081ftextCode
     */
    public function setF081fcreatedBy($f081fcreatedBy)
    {
        $this->f081fcreatedBy = $f081fcreatedBy;

        return $this;
    }

    /**
     * Get f081fcreatedBy
     *
     * @return integer
     */
    public function getF081fcreatedBy()
    {
        return $this->f081fcreatedBy;
    }

    /**
     * Set f081fupdatedBy
     *
     * @param integer $f081fupdatedBy
     *
     * @return T081ftextCode
     */
    public function setF081fupdatedBy($f081fupdatedBy)
    {
        $this->f081fupdatedBy = $f081fupdatedBy;

        return $this;
    }

    /**
     * Get f081fupdatedBy
     *
     * @return integer
     */
    public function getF081fupdatedBy()
    {
        return $this->f081fupdatedBy;
    }

    /**
     * Set f081fcreatedDtTm
     *
     * @param \DateTime $f081fcreatedDtTm
     *
     * @return T081ftextCode
     */
    public function setF081fcreatedDtTm($f081fcreatedDtTm)
    {
        $this->f081fcreatedDtTm = $f081fcreatedDtTm;

        return $this;
    }

    /**
     * Get f081fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fcreatedDtTm()
    {
        return $this->f081fcreatedDtTm;
    }

    /**
     * Set f081fupdatedDtTm
     *
     * @param \DateTime $f081fupdatedDtTm
     *
     * @return T081ftextCode
     */
    public function setF081fupdatedDtTm($f081fupdatedDtTm)
    {
        $this->f081fupdatedDtTm = $f081fupdatedDtTm;

        return $this;
    }

    /**
     * Get f081fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fupdatedDtTm()
    {
        return $this->f081fupdatedDtTm;
    }
}
