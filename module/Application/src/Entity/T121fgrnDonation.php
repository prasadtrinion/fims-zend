<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T121fgrnDonation
 *
 * @ORM\Table(name="t121fgrn_donation")
 * @ORM\Entity(repositoryClass="Application\Repository\GrnDonationRepository")
 */
class T121fgrnDonation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f121fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f121fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f121forder_type", type="string", length=30, nullable=true)
     */
    private $f121forderType;

    /**
     * @var string
     *
     * @ORM\Column(name="f121freference_number", type="string", length=100, nullable=true)
     */
    private $f121freferenceNumber;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f121fapprove_date", type="datetime", nullable=true)
     */
    private $f121fapproveDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fdonor", type="string",length=50, nullable=true)
     */
    private $f121fdonor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fvalue", type="integer", nullable=true)
     */
    private $f121fvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fstatus", type="integer", nullable=true)
     */
    private $f121fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fcreated_by", type="integer", nullable=true)
     */
    private $f121fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fupdated_by", type="integer", nullable=true)
     */
    private $f121fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f121fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f121fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f121fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f121fupdatedDtTm;

    



    /**
     * Get f121fid
     *
     * @return integer
     */
    public function getF121fid()
    {
        return $this->f121fid;
    }

    /**
     * Set f121forderType
     *
     * @param string $f121forderType
     *
     * @return T121fgrnDonation
     */
    public function setF121forderType($f121forderType)
    {
        $this->f121forderType = $f121forderType;

        return $this;
    }

    /**
     * Get f121forderType
     *
     * @return string
     */
    public function getF121forderType()
    {
        return $this->f121forderType;
    }


    /**
     * Set f121freferenceNumber
     *
     * @param string $f121freferenceNumber
     *
     * @return T121fgrnDonation
     */
    public function setF121freferenceNumber($f121freferenceNumber)
    {
        $this->f121freferenceNumber = $f121freferenceNumber;

        return $this;
    }

    /**
     * Get f121freferenceNumber
     *
     * @return string
     */
    public function getF121freferenceNumber()
    {
        return $this->f121freferenceNumber;
    }

    /**
     * Set f121fapproveDate
     *
     * @param integer $f121fapproveDate
     *
     * @return T121fgrnDonation
     */
    public function setF121fapproveDate($f121fapproveDate)
    {
        $this->f121fapproveDate = $f121fapproveDate;

        return $this;
    }

    /**
     * Get f121fapproveDate
     *
     * @return integer
     */
    public function getF121fapproveDate()
    {
        return $this->f121fapproveDate;
    }


    /**
     * Set f121fdonor
     *
     * @param string $f121fdonor
     *
     * @return T121fgrnDonation
     */
    public function setF121fdonor($f121fdonor)
    {
        $this->f121fdonor = $f121fdonor;

        return $this;
    }

    /**
     * Get f121fdonor
     *
     * @return integer
     */
    public function getF121fdonor()
    {
        return $this->f121fdonor;
    }

    /**
     * Set f121fvalue
     *
     * @param integer $f121fvalue
     *
     * @return T121fgrnDonation
     */
    public function setF121fvalue($f121fvalue)
    {
        $this->f121fvalue = $f121fvalue;

        return $this;
    }

    /**
     * Get f121fvalue
     *
     * @return integer
     */
    public function getF121fvalue()
    {
        return $this->f121fvalue;
    }

    /**
     * Set f121fstatus
     *
     * @param integer $f121fstatus
     *
     * @return T121fgrnDonation
     */
    public function setF121fstatus($f121fstatus)
    {
        $this->f121fstatus = $f121fstatus;

        return $this;
    }

    /**
     * Get f121fstatus
     *
     * @return integer
     */
    public function getF121fstatus()
    {
        return $this->f121fstatus;
    }

    

    /**
     * Set f121fcreatedBy
     *
     * @param integer $f121fcreatedBy
     *
     * @return T121fgrnDonation
     */
    public function setF121fcreatedBy($f121fcreatedBy)
    {
        $this->f121fcreatedBy = $f121fcreatedBy;

        return $this;
    }

    /**
     * Get f121fcreatedBy
     *
     * @return integer
     */
    public function getF121fcreatedBy()
    {
        return $this->f121fcreatedBy;
    }

    /**
     * Set f121fupdatedBy
     *
     * @param integer $f121fupdatedBy
     *
     * @return T121fgrnDonation
     */
    public function setF121fupdatedBy($f121fupdatedBy)
    {
        $this->f121fupdatedBy = $f121fupdatedBy;

        return $this;
    }

    /**
     * Get f121fupdatedBy
     *
     * @return integer
     */
    public function getF121fupdatedBy()
    {
        return $this->f121fupdatedBy;
    }

    /**
     * Set f121fcreatedDtTm
     *
     * @param \DateTime $f121fcreatedDtTm
     *
     * @return T121fgrnDonation
     */
    public function setF121fcreatedDtTm($f121fcreatedDtTm)
    {
        $this->f121fcreatedDtTm = $f121fcreatedDtTm;

        return $this;
    }

    /**
     * Get f121fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF121fcreatedDtTm()
    {
        return $this->f121fcreatedDtTm;
    }

    /**
     * Set f121fupdatedDtTm
     *
     * @param \DateTime $f121fupdatedDtTm
     *
     * @return T121fgrnDonation
     */
    public function setF121fupdatedDtTm($f121fupdatedDtTm)
    {
        $this->f121fupdatedDtTm = $f121fupdatedDtTm;

        return $this;
    }

    /**
     * Get f121fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF121fupdatedDtTm()
    {
        return $this->f121fupdatedDtTm;
    }
}
