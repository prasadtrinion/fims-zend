<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T041fbankSetupAccount
 *
 * @ORM\Table(name="t041fbank_setup_account",uniqueConstraints={@ORM\UniqueConstraint(name="f041faccount_number_UNIQUE", columns={"f041faccount_number"})}))
 * @ORM\Entity(repositoryClass="Application\Repository\BankSetupAccountRepository")
 */
class T041fbankSetupAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f041fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f041fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fid_bank", type="integer", nullable=false)
     */
    private $f041fidBank;

          /**
     * @var string
     *
     * @ORM\Column(name="f041ffund_code", type="string",length=20, nullable=false)
     */
    private $f041ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f041fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f041fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f041factivity_code", type="string",length=20, nullable=false)
     */
    private $f041factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f041faccount_code", type="string",length=20, nullable=false)
     */
    private $f041faccountCode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f041faccount_number", type="string", nullable=false)
     */
    private $f041faccountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fprocessing", type="integer", nullable=false)
     */
    private $f041fprocessing;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fstatus", type="integer", nullable=false)
     */
    private $f041fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fcreated_by", type="integer", nullable=false)
     */
    private $f041fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fupdated_by", type="integer", nullable=false)
     */
    private $f041fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f041fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f041fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f041fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f041fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f041faddress", type="string", length=255, nullable=false)
     */
    private $f041faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fcontact_person", type="string", length=255, nullable=false)
     */
    private $f041fcontactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fphone", type="string", length=25, nullable=false)
     */
    private $f041fphone;

    /**
     * @var string
     *
     * @ORM\Column(name="f041femail", type="string", length=255, nullable=false)
     */
    private $f041femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fdescription", type="string", length=255, nullable=true)
     */
    private $f041fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fbank_no", type="string", length=50, nullable=true)
     */
    private $f041fbankNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fbranch", type="string", length=100, nullable=true)
     */
    private $f041fbranch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041faccount_type", type="integer", nullable=false)
     */
    private $f041faccountType;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fcity", type="string", length=100, nullable=false)
     */
    private $f041fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fstate", type="integer", nullable=false)
     */
    private $f041fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fcountry", type="integer", nullable=false)
     */
    private $f041fcountry;



    /**
     * Get f041fid
     *
     * @return integer
     */
    public function getF041fid()
    {
        return $this->f041fid;
    }

    /**
     * Set f041fidBank
     *
     * @param integer $f041fidBank
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fidBank($f041fidBank)
    {
        $this->f041fidBank = $f041fidBank;

        return $this;
    }

    /**
     * Get f041fidBank
     *
     * @return integer
     */
    public function getF041fidBank()
    {
        return $this->f041fidBank;
    }

    /**
     * Set f041fidGlcode
     *
     * @param integer $f041fidGlcode
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fidGlcode($f041fidGlcode)
    {
        $this->f041fidGlcode = $f041fidGlcode;

        return $this;
    }

    /**
     * Get f041fidGlcode
     *
     * @return integer
     */
    public function getF041fidGlcode()
    {
        return $this->f041fidGlcode;
    }

    /**
     * Set f041faccountNumber
     *
     * @param integer $f041faccountNumber
     *
     * @return T041fbankSetupAccount
     */
    public function setF041faccountNumber($f041faccountNumber)
    {
        $this->f041faccountNumber = $f041faccountNumber;

        return $this;
    }

    /**
     * Get f041faccountNumber
     *
     * @return integer
     */
    public function getF041faccountNumber()
    {
        return $this->f041faccountNumber;
    }

    /**
     * Set f041fprocessing
     *
     * @param integer $f041fprocessing
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fprocessing($f041fprocessing)
    {
        $this->f041fprocessing = $f041fprocessing;

        return $this;
    }

    /**
     * Get f041fprocessing
     *
     * @return integer
     */
    public function getF041fprocessing()
    {
        return $this->f041fprocessing;
    }

    /**
     * Set f041fstatus
     *
     * @param integer $f041fstatus
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fstatus($f041fstatus)
    {
        $this->f041fstatus = $f041fstatus;

        return $this;
    }

    /**
     * Get f041fstatus
     *
     * @return integer
     */
    public function getF041fstatus()
    {
        return $this->f041fstatus;
    }

    /**
     * Set f041fcreatedBy
     *
     * @param integer $f041fcreatedBy
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fcreatedBy($f041fcreatedBy)
    {
        $this->f041fcreatedBy = $f041fcreatedBy;

        return $this;
    }

    /**
     * Get f041fcreatedBy
     *
     * @return integer
     */
    public function getF041fcreatedBy()
    {
        return $this->f041fcreatedBy;
    }

    /**
     * Set f041fupdatedBy
     *
     * @param integer $f041fupdatedBy
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fupdatedBy($f041fupdatedBy)
    {
        $this->f041fupdatedBy = $f041fupdatedBy;

        return $this;
    }

    /**
     * Get f041fupdatedBy
     *
     * @return integer
     */
    public function getF041fupdatedBy()
    {
        return $this->f041fupdatedBy;
    }

    /**
     * Set f041fcreatedDtTm
     *
     * @param \DateTime $f041fcreatedDtTm
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fcreatedDtTm($f041fcreatedDtTm)
    {
        $this->f041fcreatedDtTm = $f041fcreatedDtTm;

        return $this;
    }

    /**
     * Get f041fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF041fcreatedDtTm()
    {
        return $this->f041fcreatedDtTm;
    }

    /**
     * Set f041fupdatedDtTm
     *
     * @param \DateTime $f041fupdatedDtTm
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fupdatedDtTm($f041fupdatedDtTm)
    {
        $this->f041fupdatedDtTm = $f041fupdatedDtTm;

        return $this;
    }

    /**
     * Get f041fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF041fupdatedDtTm()
    {
        return $this->f041fupdatedDtTm;
    }

    /**
     * Set f041faddress
     *
     * @param string $f041faddress
     *
     * @return T041fbankSetupAccount
     */
    public function setF041faddress($f041faddress)
    {
        $this->f041faddress = $f041faddress;

        return $this;
    }

    /**
     * Get f041faddress
     *
     * @return string
     */
    public function getF041faddress()
    {
        return $this->f041faddress;
    }

    /**
     * Set f041fcontactPerson
     *
     * @param string $f041fcontactPerson
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fcontactPerson($f041fcontactPerson)
    {
        $this->f041fcontactPerson = $f041fcontactPerson;

        return $this;
    }

    /**
     * Get f041fcontactPerson
     *
     * @return string
     */
    public function getF041fcontactPerson()
    {
        return $this->f041fcontactPerson;
    }

    /**
     * Set f041fphone
     *
     * @param string $f041fphone
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fphone($f041fphone)
    {
        $this->f041fphone = $f041fphone;

        return $this;
    }

    /**
     * Get f041fphone
     *
     * @return string
     */
    public function getF041fphone()
    {
        return $this->f041fphone;
    }

    /**
     * Set f041femail
     *
     * @param string $f041femail
     *
     * @return T041fbankSetupAccount
     */
    public function setF041femail($f041femail)
    {
        $this->f041femail = $f041femail;

        return $this;
    }

    /**
     * Get f041femail
     *
     * @return string
     */
    public function getF041femail()
    {
        return $this->f041femail;
    }

    /**
     * Set f041fdescription
     *
     * @param string $f041fdescription
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fdescription($f041fdescription)
    {
        $this->f041fdescription = $f041fdescription;

        return $this;
    }

    /**
     * Get f041fdescription
     *
     * @return string
     */
    public function getF041fdescription()
    {
        return $this->f041fdescription;
    }

    /**
     * Set f041fbankNo
     *
     * @param string $f041fbankNo
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fbankNo($f041fbankNo)
    {
        $this->f041fbankNo = $f041fbankNo;

        return $this;
    }

    /**
     * Get f041fbankNo
     *
     * @return string
     */
    public function getF041fbankNo()
    {
        return $this->f041fbankNo;
    }

    /**
     * Set f041fbranch
     *
     * @param string $f041fbranch
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fbranch($f041fbranch)
    {
        $this->f041fbranch = $f041fbranch;

        return $this;
    }

    /**
     * Get f041fbranch
     *
     * @return string
     */
    public function getF041fbranch()
    {
        return $this->f041fbranch;
    }

      /**
     * Set f041faccountType
     *
     * @param integer $f041faccountType
     *
     * @return T041fbankSetupAccount
     */
    public function setF041faccountType($f041faccountType)
    {
        $this->f041faccountType = $f041faccountType;

        return $this;
    }

    /**
     * Get f041faccountType
     *
     * @return integer
     */
    public function getF041faccountType()
    {
        return $this->f041faccountType;
    }

    /**
     * Set f041fcity
     *
     * @param string $f041fcity
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fcity($f041fcity)
    {
        $this->f041fcity = $f041fcity;

        return $this;
    }

    /**
     * Get f041fcity
     *
     * @return string
     */
    public function getF041fcity()
    {
        return $this->f041fcity;
    }

    /**
     * Set f041fstate
     *
     * @param integer $f041fstate
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fstate($f041fstate)
    {
        $this->f041fstate = $f041fstate;

        return $this;
    }

    /**
     * Get f041fstate
     *
     * @return integer
     */
    public function getF041fstate()
    {
        return $this->f041fstate;
    }

    /**
     * Set f041fcountry
     *
     * @param integer $f041fcountry
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fcountry($f041fcountry)
    {
        $this->f041fcountry = $f041fcountry;

        return $this;
    }

    /**
     * Get f041fcountry
     *
     * @return integer
     */
    public function getF041fcountry()
    {
        return $this->f041fcountry;
    }

     /**
     * Set f041ffundCode
     *
     * @param integer $f041ffundCode
     *
     * @return T041fbankSetupAccount
     */
    public function setF041ffundCode($f041ffundCode)
    {
        $this->f041ffundCode = $f041ffundCode;

        return $this;
    }

    /**
     * Get f041ffundCode
     *
     * @return integer
     */
    public function getF041ffundCode()
    {
        return $this->f041ffundCode;
    }

    /**
     * Set f041faccountCode
     *
     * @param integer $f041faccountCode
     *
     * @return T041fbankSetupAccount
     */
    public function setF041faccountCode($f041faccountCode)
    {
        $this->f041faccountCode = $f041faccountCode;

        return $this;
    }

    /**
     * Get f041faccountCode
     *
     * @return integer
     */
    public function getF041faccountCode()
    {
        return $this->f041faccountCode;
    }

    /**
     * Set f041factivityCode
     *
     * @param integer $f041factivityCode
     *
     * @return T041fbankSetupAccount
     */
    public function setF041factivityCode($f041factivityCode)
    {
        $this->f041factivityCode = $f041factivityCode;

        return $this;
    }

    /**
     * Get f041factivityCode
     *
     * @return integer
     */
    public function getF041factivityCode()
    {
        return $this->f041factivityCode;
    }

    /**
     * Set f041fdepartmentCode
     *
     * @param integer $f041fdepartmentCode
     *
     * @return T041fbankSetupAccount
     */
    public function setF041fdepartmentCode($f041fdepartmentCode)
    {
        $this->f041fdepartmentCode = $f041fdepartmentCode;

        return $this;
    }

    /**
     * Get f041fdepartmentCode
     *
     * @return integer
     */
    public function getF041fdepartmentCode()
    {
        return $this->f041fdepartmentCode;
    }
}
