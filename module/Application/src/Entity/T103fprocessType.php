<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T103fprocessType
 *
 * @ORM\Table(name="t103fprocess_type")
 * @ORM\Entity(repositoryClass="Application\Repository\ProcessTypeRepository")
 */
class T103fprocessType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f103fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f103fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f103fname", type="string", length=50, nullable=true)
     */
    private $f103fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fstatus", type="integer", nullable=true)
     */
    private $f103fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fcreated_by", type="integer", nullable=true)
     */
    private $f103fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fupdated_by", type="integer", nullable=true)
     */
    private $f103fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f103fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f103fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f103fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f103fupdatedDtTm;



    /**
     * Get f103fid
     *
     * @return integer
     */
    public function getF103fid()
    {
        return $this->f103fid;
    }

    /**
     * Set f103fname
     *
     * @param string $f103fname
     *
     * @return T103fprocessType
     */
    public function setF103fname($f103fname)
    {
        $this->f103fname = $f103fname;

        return $this;
    }

    /**
     * Get f103fname
     *
     * @return string
     */
    public function getF103fname()
    {
        return $this->f103fname;
    }


    /**
     * Set f103fstatus
     *
     * @param integer $f103fstatus
     *
     * @return T103fprocessType
     */
    public function setF103fstatus($f103fstatus)
    {
        $this->f103fstatus = $f103fstatus;

        return $this;
    }

    /**
     * Get f103fstatus
     *
     * @return integer
     */
    public function getF103fstatus()
    {
        return $this->f103fstatus;
    }

    /**
     * Set f103fcreatedBy
     *
     * @param integer $f103fcreatedBy
     *
     * @return T103fprocessType
     */
    public function setF103fcreatedBy($f103fcreatedBy)
    {
        $this->f103fcreatedBy = $f103fcreatedBy;

        return $this;
    }

    /**
     * Get f103fcreatedBy
     *
     * @return integer
     */
    public function getF103fcreatedBy()
    {
        return $this->f103fcreatedBy;
    }

    /**
     * Set f103fupdatedBy
     *
     * @param integer $f103fupdatedBy
     *
     * @return T103fprocessType
     */
    public function setF103fupdatedBy($f103fupdatedBy)
    {
        $this->f103fupdatedBy = $f103fupdatedBy;

        return $this;
    }

    /**
     * Get f103fupdatedBy
     *
     * @return integer
     */
    public function getF103fupdatedBy()
    {
        return $this->f103fupdatedBy;
    }

    /**
     * Set f103fcreatedDtTm
     *
     * @param \DateTime $f103fcreatedDtTm
     *
     * @return T103fprocessType
     */
    public function setF103fcreatedDtTm($f103fcreatedDtTm)
    {
        $this->f103fcreatedDtTm = $f103fcreatedDtTm;

        return $this;
    }

    /**
     * Get f103fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF103fcreatedDtTm()
    {
        return $this->f103fcreatedDtTm;
    }

    /**
     * Set f103fupdatedDtTm
     *
     * @param \DateTime $f103fupdatedDtTm
     *
     * @return T103fprocessType
     */
    public function setF103fupdatedDtTm($f103fupdatedDtTm)
    {
        $this->f103fupdatedDtTm = $f103fupdatedDtTm;

        return $this;
    }

    /**
     * Get f103fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF103fupdatedDtTm()
    {
        return $this->f103fupdatedDtTm;
    }
}

