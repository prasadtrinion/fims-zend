<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T081forderType
 *
 * @ORM\Table(name="t081forder_type",uniqueConstraints={@ORM\UniqueConstraint(name="f081forder_type_UNIQUE", columns={"f081forder_type"})})
 * @ORM\Entity(repositoryClass="Application\Repository\OrderTypeRepository")
 */
class T081forderType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f081fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f081forder_type", type="string", length=50, nullable=false)
     */
    private $f081forderType;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fdescription", type="string", length=100, nullable=false)
     */
    private $f081fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f081faccount_code", type="string", length=100, nullable=false)
     */
    private $f081faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstatus", type="integer", nullable=false)
     */
    private $f081fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fcreated_by", type="integer", nullable=false)
     */
    private $f081fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fupdated_by", type="integer", nullable=false)
     */
    private $f081fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fupdatedDtTm;



    /**
     * Get f081fid
     *
     * @return integer
     */
    public function getF081fid()
    {
        return $this->f081fid;
    }

    /**
     * Set f081forderType
     *
     * @param string $f081forderType
     *
     * @return T081forderType
     */
    public function setF081forderType($f081forderType)
    {
        $this->f081forderType = $f081forderType;

        return $this;
    }

    /**
     * Get f081forderType
     *
     * @return string
     */
    public function getF081forderType()
    {
        return $this->f081forderType;
    }

    /**
     * Set f081fdescription
     *
     * @param string $f081fdescription
     *
     * @return T081forderType
     */
    public function setF081fdescription($f081fdescription)
    {
        $this->f081fdescription = $f081fdescription;

        return $this;
    }

    /**
     * Get f081fdescription
     *
     * @return string
     */
    public function getF081fdescription()
    {
        return $this->f081fdescription;
    }

    /**
     * Set f081faccountCode
     *
     * @param string $f081faccountCode
     *
     * @return T081forderType
     */
    public function setF081faccountCode($f081faccountCode)
    {
        $this->f081faccountCode = $f081faccountCode;

        return $this;
    }

    /**
     * Get f081faccountCode
     *
     * @return string
     */
    public function getF081faccountCode()
    {
        return $this->f081faccountCode;
    }

    /**
     * Set f081fstatus
     *
     * @param integer $f081fstatus
     *
     * @return T081forderType
     */
    public function setF081fstatus($f081fstatus)
    {
        $this->f081fstatus = $f081fstatus;

        return $this;
    }

    /**
     * Get f081fstatus
     *
     * @return integer
     */
    public function getF081fstatus()
    {
        return $this->f081fstatus;
    }

    /**
     * Set f081fcreatedBy
     *
     * @param integer $f081fcreatedBy
     *
     * @return T081forderType
     */
    public function setF081fcreatedBy($f081fcreatedBy)
    {
        $this->f081fcreatedBy = $f081fcreatedBy;

        return $this;
    }

    /**
     * Get f081fcreatedBy
     *
     * @return integer
     */
    public function getF081fcreatedBy()
    {
        return $this->f081fcreatedBy;
    }

    /**
     * Set f081fupdatedBy
     *
     * @param integer $f081fupdatedBy
     *
     * @return T081forderType
     */
    public function setF081fupdatedBy($f081fupdatedBy)
    {
        $this->f081fupdatedBy = $f081fupdatedBy;

        return $this;
    }

    /**
     * Get f081fupdatedBy
     *
     * @return integer
     */
    public function getF081fupdatedBy()
    {
        return $this->f081fupdatedBy;
    }

    /**
     * Set f081fcreatedDtTm
     *
     * @param \DateTime $f081fcreatedDtTm
     *
     * @return T081forderType
     */
    public function setF081fcreatedDtTm($f081fcreatedDtTm)
    {
        $this->f081fcreatedDtTm = $f081fcreatedDtTm;

        return $this;
    }

    /**
     * Get f081fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fcreatedDtTm()
    {
        return $this->f081fcreatedDtTm;
    }

    /**
     * Set f081fupdatedDtTm
     *
     * @param \DateTime $f081fupdatedDtTm
     *
     * @return T081forderType
     */
    public function setF081fupdatedDtTm($f081fupdatedDtTm)
    {
        $this->f081fupdatedDtTm = $f081fupdatedDtTm;

        return $this;
    }

    /**
     * Get f081fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fupdatedDtTm()
    {
        return $this->f081fupdatedDtTm;
    }
}
