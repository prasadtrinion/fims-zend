<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T088fpurchaseRequisition
 *
 * @ORM\Table(name="t088fpurchase_requisition")
 * @ORM\Entity(repositoryClass="Application\Repository\PurchaseRequisitionRepository")
 */
class T088fpurchaseRequisition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f088fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f088freference_number", type="string", length=50, nullable=true)
     */
    private $f088freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f088forder_type",type="string", length=50, nullable=true)
     */
    private $f088forderType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fdate", type="datetime", nullable=false)
     */
    private $f088fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_financialyear", type="integer", nullable=true)
     */
    private $f088fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_supplier", type="integer", nullable=true)
     */
    private $f088fidSupplier;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fdescription", type="string", length=100, nullable=true)
     */
    private $f088fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088ftotal_amount", type="integer", nullable=true)
     */
    private $f088ftotalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fid_department", type="string", length=10, nullable=true)
     */
    private $f088fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fstatus", type="integer", nullable=true)
     */
    private $f088fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fapproval_status", type="integer", nullable=true)
     */
    private $f088fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fcreated_by", type="integer", nullable=true)
     */
    private $f088fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fupdated_by", type="integer", nullable=true)
     */
    private $f088fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f088fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f088fupdatedDtTm;



    /**
     * Get f088fid
     *
     * @return integer
     */
    public function getF088fid()
    {
        return $this->f088fid;
    }

    /**
     * Set f088forderType
     *
     * @param string $f088forderType
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088forderType($f088forderType)
    {
        $this->f088forderType = $f088forderType;

        return $this;
    }

    /**
     * Get f088forderType
     *
     * @return string
     */
    public function getF088forderType()
    {
        return $this->f088forderType;
    }

    /**
     * Set f088fdate
     *
     * @param \DateTime $f088fdate
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fdate($f088fdate)
    {
        $this->f088fdate = $f088fdate;

        return $this;
    }

    /**
     * Get f088fdate
     *
     * @return \DateTime
     */
    public function getF088fdate()
    {
        return $this->f088fdate;
    }

    /**
     * Set f088fidFinancialyear
     *
     * @param integer $f088fidFinancialyear
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fidFinancialyear($f088fidFinancialyear)
    {
        $this->f088fidFinancialyear = $f088fidFinancialyear;

        return $this;
    }

    /**
     * Get f088fidFinancialyear
     *
     * @return integer
     */
    public function getF088fidFinancialyear()
    {
        return $this->f088fidFinancialyear;
    }

    /**
     * Set f088fidSupplier
     *
     * @param integer $f088fidSupplier
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fidSupplier($f088fidSupplier)
    {
        $this->f088fidSupplier = $f088fidSupplier;

        return $this;
    }

    /**
     * Get f088fidSupplier
     *
     * @return integer
     */
    public function getF088fidSupplier()
    {
        return $this->f088fidSupplier;
    }

    /**
     * Set f088fdescription
     *
     * @param string $f088fdescription
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fdescription($f088fdescription)
    {
        $this->f088fdescription = $f088fdescription;

        return $this;
    }

    /**
     * Get f088fdescription
     *
     * @return string
     */
    public function getF088fdescription()
    {
        return $this->f088fdescription;
    }

    /**
     * Set f088freferenceNumber
     *
     * @param string $f088freferenceNumber
     *
     * @return T088fpurchaseOrder
     */
    public function setF088freferenceNumber($f088freferenceNumber)
    {
        $this->f088freferenceNumber = $f088freferenceNumber;

        return $this;
    }

    /**
     * Get f088freferenceNumber
     *
     * @return string
     */
    public function getF088freferenceNumber()
    {
        return $this->f088freferenceNumber;
    }


    /**
     * Set f088fidDepartment
     *
     * @param string $f088fidDepartment
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fidDepartment($f088fidDepartment)
    {
        $this->f088fidDepartment = $f088fidDepartment;

        return $this;
    }

    /**
     * Get f088fidDepartment
     *
     * @return string
     */
    public function getF088fidDepartment()
    {
        return $this->f088fidDepartment;
    }

    /**
     * Set f088fstatus
     *
     * @param integer $f088fstatus
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fstatus($f088fstatus)
    {
        $this->f088fstatus = $f088fstatus;

        return $this;
    }

    /**
     * Get f088fstatus
     *
     * @return integer
     */
    public function getF088fstatus()
    {
        return $this->f088fstatus;
    }

    /**
     * Set f088fapprovalStatus
     *
     * @param integer $f088fapprovalStatus
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fapprovalStatus($f088fapprovalStatus)
    {
        $this->f088fapprovalStatus = $f088fapprovalStatus;

        return $this;
    }

    /**
     * Get f088fapprovalStatus
     *
     * @return integer
     */
    public function getF088fapprovalStatus()
    {
        return $this->f088fapprovalStatus;
    }

    /**
     * Set f088fcreatedBy
     *
     * @param integer $f088fcreatedBy
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fcreatedBy($f088fcreatedBy)
    {
        $this->f088fcreatedBy = $f088fcreatedBy;

        return $this;
    }

    /**
     * Get f088fcreatedBy
     *
     * @return integer
     */
    public function getF088fcreatedBy()
    {
        return $this->f088fcreatedBy;
    }

    /**
     * Set f088fupdatedBy
     *
     * @param integer $f088fupdatedBy
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fupdatedBy($f088fupdatedBy)
    {
        $this->f088fupdatedBy = $f088fupdatedBy;

        return $this;
    }

    /**
     * Get f088fupdatedBy
     *
     * @return integer
     */
    public function getF088fupdatedBy()
    {
        return $this->f088fupdatedBy;
    }

    /**
     * Set f088fcreatedDtTm
     *
     * @param \DateTime $f088fcreatedDtTm
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fcreatedDtTm($f088fcreatedDtTm)
    {
        $this->f088fcreatedDtTm = $f088fcreatedDtTm;

        return $this;
    }

    /**
     * Get f088fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fcreatedDtTm()
    {
        return $this->f088fcreatedDtTm;
    }

    /**
     * Set f088fupdatedDtTm
     *
     * @param \DateTime $f088fupdatedDtTm
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088fupdatedDtTm($f088fupdatedDtTm)
    {
        $this->f088fupdatedDtTm = $f088fupdatedDtTm;

        return $this;
    }

    /**
     * Get f088fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fupdatedDtTm()
    {
        return $this->f088fupdatedDtTm;
    }

    /**
     * Set f088ftotalAmount
     *
     * @param integer $f088ftotalAmount
     *
     * @return T088fpurchaseRequisition
     */
    public function setF088ftotalAmount($f088ftotalAmount)
    {
        $this->f088ftotalAmount = $f088ftotalAmount;

        return $this;
    }

    /**
     * Get f088ftotalAmount
     *
     * @return integer
     */
    public function getF088ftotalAmount()
    {
        return $this->f088ftotalAmount;
    }
}
