<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T011fmenu
 *
 * @ORM\Table(name="t011fmenu", uniqueConstraints={@ORM\UniqueConstraint(name="f011fmenu_name_UNIQUE", columns={"f011fmenu_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\MenuRepository")
 */
class T011fmenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f011fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fmenu_name", type="string", length=150, nullable=false)
     */
    private $f011fmenuName;


    /**
     * @var string
     *
     * @ORM\Column(name="f011fmodule", type="string", length=45, nullable=false)
     */
    private $f011fmodule;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid_parent", type="integer", nullable=true)
     */
    private $f011fidParent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011forder", type="integer", nullable=false)
     */
    private $f011forder;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fstatus", type="boolean", nullable=false)
     */
    private $f011fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011ftype", type="integer", nullable=false)
     */
    private $f011ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f011flink", type="string", length=150, nullable=false)
     */
    private $f011flink;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcreated_by", type="string", length=50, nullable=false)
     */
    private $f011fcreatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fupdated_by", type="string", length=50, nullable=false)
     */
    private $f011fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f011fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f011fupdateDtTm;

    /**
     * Get f011fid
     *
     * @return integer
     */
    public function getF011fid()
    {
        return $this->f011fid;
    }

    /**
     * Set f011fmenuName
     *
     * @param string $f011fmenuName
     *
     * @return T011fmenu
     */
    public function setF011fmenuName($f011fmenuName)
    {
        $this->f011fmenuName = $f011fmenuName;

        return $this;
    }

    /**
     * Get f011fmenuName
     *
     * @return string
     */
    public function getF011fmenuName()
    {
        return $this->f011fmenuName;
    }

    /**
     * Set f011fidParent
     *
     * @param integer $f011fidParent
     *
     * @return T011fmenu
     */
    public function setF011fidParent($f011fidParent)
    {
        $this->f011fidParent = $f011fidParent;

        return $this;
    }

    /**
     * Get f011fidParent
     *
     * @return integer
     */
    public function getF011fidParent()
    {
        return $this->f011fidParent;
    }


    /**
     * Set f011fmodule
     *
     * @param integer $f011fmodule
     *
     * @return T011fmenu
     */
    public function setF011fmodule($f011fmodule)
    {
        $this->f011fmodule = $f011fmodule;

        return $this;
    }

    /**
     * Get f011fmodule
     *
     * @return integer
     */
    public function getF011fmodule()
    {
        return $this->f011fmodule;
    }

    /**
     * Set f011forder
     *
     * @param integer $f011forder
     *
     * @return T011fmenu
     */
    public function setF011forder($f011forder)
    {
        $this->f011forder = $f011forder;

        return $this;
    }

    /**
     * Get f011forder
     *
     * @return integer
     */
    public function getF011forder()
    {
        return $this->f011forder;
    }

    /**
     * Set f011fstatus
     *
     * @param string $f011fstatus
     *
     * @return T011fmenu
     */
    public function setF011fstatus($f011fstatus)
    {
        $this->f011fstatus = $f011fstatus;

        return $this;
    }

    /**
     * Get f011fstatus
     *
     * @return string
     */
    public function getF011fstatus()
    {
        return $this->f011fstatus;
    }



    /**
     * Set f011ftype
     *
     * @param string $f011ftype
     *
     * @return T011fmenu
     */
    public function setF011ftype($f011ftype)
    {
        $this->f011ftype = $f011ftype;

        return $this;
    }

    /**
     * Get f011ftype
     *
     * @return string
     */
    public function getF011ftype()
    {
        return $this->f011ftype;
    }




    /**
     * Set f011flink
     *
     * @param string $f011flink
     *
     * @return T011fmenu
     */
    public function setF011flink($f011flink)
    {
        $this->f011flink = $f011flink;

        return $this;
    }

    /**
     * Get f011flink
     *
     * @return string
     */
    public function getF011flink()
    {
        return $this->f011flink;
    }

    /**
     * Set f011fcreatedBy
     *
     * @param string $f011fcreatedBy
     *
     * @return T011fmenu
     */
    public function setF011fcreatedBy($f011fcreatedBy)
    {
        $this->f011fcreatedBy = $f011fcreatedBy;

        return $this;
    }

    /**
     * Get f011fcreatedBy
     *
     * @return string
     */
    public function getF011fcreatedBy()
    {
        return $this->f011fcreatedBy;
    }

    /**
     * Set f011fupdatedBy
     *
     * @param string $f011fupdatedBy
     *
     * @return T011fmenu
     */
    public function setF011fupdatedBy($f011fupdatedBy)
    {
        $this->f011fupdatedBy = $f011fupdatedBy;

        return $this;
    }

    /**
     * Get f011fupdatedBy
     *
     * @return string
     */
    public function getF011fupdatedBy()
    {
        return $this->f011fupdatedBy;
    }

    /**
     * Set f011fcreateDtTm
     *
     * @param \DateTime $f011fcreateDtTm
     *
     * @return T011fmenu
     */
    public function setF011fcreateDtTm($f011fcreateDtTm)
    {
        $this->f011fcreateDtTm = $f011fcreateDtTm;

        return $this;
    }

    /**
     * Get f011fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF011fcreateDtTm()
    {
        return $this->f011fcreateDtTm;
    }

    /**
     * Set f011fupdateDtTm
     *
     * @param \DateTime $f011fupdateDtTm
     *
     * @return T011fmenu
     */
    public function setF011fupdateDtTm($f011fupdateDtTm)
    {
        $this->f011fupdateDtTm = $f011fupdateDtTm;

        return $this;
    }

    /**
     * Get f011fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF011fupdateDtTm()
    {
        return $this->f011fupdateDtTm;
    }
}





