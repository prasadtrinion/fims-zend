<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T055fbudgetIncrement
 *
 * @ORM\Table(name="t055fbudget_increment")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetIncrementRepository")
 */
class T055fbudgetIncrement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f055fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid_financial_year", type="integer", nullable=false)
     */
    private $f055fidFinancialYear;

    

        /**
     * @var string
     *
     * @ORM\Column(name="f041ffund_code", type="string",length=20, nullable=false)
     */
    private $f055ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f041fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f055fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f041factivity_code", type="string",length=20, nullable=false)
     */
    private $f055factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f041faccount_code", type="string",length=20, nullable=false)
     */
    private $f055faccountCode; 

    /**
     * @var integer
     *
     * @ORM\Column(name="f055famount", type="integer",  nullable=false)
     */
    private $f055famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fapproval_status", type="integer", nullable=true)
     */
    private $f055fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fstatus", type="integer", nullable=false)
     */
    private $f055fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fcreated_by", type="integer", nullable=false)
     */
    private $f055fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fupdated_by", type="integer", nullable=false)
     */
    private $f055fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq1", type="integer",  nullable=false)
     */
    private $f055fq1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq2", type="integer",  nullable=false)
     */
    private $f055fq2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq3", type="integer",  nullable=false)
     */
    private $f055fq3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq4", type="integer",  nullable=false)
     */
    private $f055fq4;



    /**
     * Get f055fid
     *
     * @return integer
     */
    public function getF055fid()
    {
        return $this->f055fid;
    }

    /**
     * Set f055fidFinancialYear
     *
     * @param integer $f055fidFinancialYear
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fidFinancialYear($f055fidFinancialYear)
    {
        $this->f055fidFinancialYear = $f055fidFinancialYear;

        return $this;
    }

    /**
     * Get f055fidFinancialYear
     *
     * @return integer
     */
    public function getF055fidFinancialYear()
    {
        return $this->f055fidFinancialYear;
    }

    

    /**
     * Set f055fidGlcode
     *
     * @param integer $f055fidGlcode
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fidGlcode($f055fidGlcode)
    {
        $this->f055fidGlcode = $f055fidGlcode;

        return $this;
    }

    /**
     * Get f055fidGlcode
     *
     * @return integer
     */
    public function getF055fidGlcode()
    {
        return $this->f055fidGlcode;
    }

    /**
     * Set f055famount
     *
     * @param integer $f055famount
     *
     * @return T055fbudgetIncrement
     */
    public function setF055famount($f055famount)
    {
        $this->f055famount = $f055famount;

        return $this;
    }

    /**
     * Get f055famount
     *
     * @return integer
     */
    public function getF055famount()
    {
        return $this->f055famount;
    }

    /**
     * Set f055fapprovalStatus
     *
     * @param integer $f055fapprovalStatus
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fapprovalStatus($f055fapprovalStatus)
    {
        $this->f055fapprovalStatus = $f055fapprovalStatus;

        return $this;
    }

    /**
     * Get f055fapprovalStatus
     *
     * @return integer
     */
    public function getF055fapprovalStatus()
    {
        return $this->f055fapprovalStatus;
    }

    /**
     * Set f055fstatus
     *
     * @param integer $f055fstatus
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fstatus($f055fstatus)
    {
        $this->f055fstatus = $f055fstatus;

        return $this;
    }

    /**
     * Get f055fstatus
     *
     * @return integer
     */
    public function getF055fstatus()
    {
        return $this->f055fstatus;
    }

    /**
     * Set f055fcreatedBy
     *
     * @param integer $f055fcreatedBy
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fcreatedBy($f055fcreatedBy)
    {
        $this->f055fcreatedBy = $f055fcreatedBy;

        return $this;
    }

    /**
     * Get f055fcreatedBy
     *
     * @return integer
     */
    public function getF055fcreatedBy()
    {
        return $this->f055fcreatedBy;
    }

    /**
     * Set f055fupdatedBy
     *
     * @param integer $f055fupdatedBy
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fupdatedBy($f055fupdatedBy)
    {
        $this->f055fupdatedBy = $f055fupdatedBy;

        return $this;
    }

    /**
     * Get f055fupdatedBy
     *
     * @return integer
     */
    public function getF055fupdatedBy()
    {
        return $this->f055fupdatedBy;
    }

    /**
     * Set f055fcreatedDtTm
     *
     * @param \DateTime $f055fcreatedDtTm
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fcreatedDtTm($f055fcreatedDtTm)
    {
        $this->f055fcreatedDtTm = $f055fcreatedDtTm;

        return $this;
    }

    /**
     * Get f055fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fcreatedDtTm()
    {
        return $this->f055fcreatedDtTm;
    }

    /**
     * Set f055fupdatedDtTm
     *
     * @param \DateTime $f055fupdatedDtTm
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fupdatedDtTm($f055fupdatedDtTm)
    {
        $this->f055fupdatedDtTm = $f055fupdatedDtTm;

        return $this;
    }

    /**
     * Get f055fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fupdatedDtTm()
    {
        return $this->f055fupdatedDtTm;
    }

    /**
     * Set f055fq1
     *
     * @param integer $f055fq1
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fq1($f055fq1)
    {
        $this->f055fq1 = $f055fq1;

        return $this;
    }

    /**
     * Get f055fq1
     *
     * @return integer
     */
    public function getF055fq1()
    {
        return $this->f055fq1;
    }

    /**
     * Set f055fq2
     *
     * @param integer $f055fq2
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fq2($f055fq2)
    {
        $this->f055fq2 = $f055fq2;

        return $this;
    }

    /**
     * Get f055fq2
     *
     * @return integer
     */
    public function getF055fq2()
    {
        return $this->f055fq2;
    }

    /**
     * Set f055fq3
     *
     * @param integer $f055fq3
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fq3($f055fq3)
    {
        $this->f055fq3 = $f055fq3;

        return $this;
    }

    /**
     * Get f055fq3
     *
     * @return integer
     */
    public function getF055fq3()
    {
        return $this->f055fq3;
    }

    /**
     * Set f055fq4
     *
     * @param integer $f055fq4
     *
     * @return T055fbudgetIncrement
     */
    public function setF055fq4($f055fq4)
    {
        $this->f055fq4 = $f055fq4;

        return $this;
    }

    /**
     * Get f055fq4
     *
     * @return integer
     */
    public function getF055fq4()
    {
        return $this->f055fq4;
    }
}
