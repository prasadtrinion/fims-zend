<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T011finitialConfig
 *
 * @ORM\Table(name="t011finitial_config")
* @ORM\Entity(repositoryClass="Application\Repository\InitialConfigRepository")
 */
class T011finitialConfig
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f011fid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011ffy_starting", type="datetime", nullable=true)
     */
    private $f011ffyStarting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011ffy_ending", type="datetime", nullable=true)
     */
    private $f011ffyEnding;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fgst_no", type="string", length=20, nullable=true)
     */
    private $f011fgstNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcentral_excise", type="string", length=20, nullable=true)
     */
    private $f011fcentralExcise;

    /**
     * @var string
     *
     * @ORM\Column(name="f011faddress", type="string", length=20, nullable=true)
     */
    private $f011faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f011faddress2", type="string", length=20, nullable=true)
     */
    private $f011faddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcity", type="string", length=255, nullable=true)
     */
    private $f011fcity;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcontact_name", type="string", length=20, nullable=true)
     */
    private $f011fcontactName;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fphone_no", type="string", length=20, nullable=true)
     */
    private $f011fphoneNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f011femail", type="string", length=20, nullable=true)
     */
    private $f011femail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f011fupdatedDtTm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f011fstatus", type="boolean", nullable=true)
     */
    private $f011fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fupdated_by", type="integer", nullable=true)
     */
    private $f011fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fstate", type="integer", nullable=true)
     */
    private $f011fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fcountry", type="integer", nullable=true)
     */
    private $f011fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fdefault_state", type="integer", nullable=true)
     */
    private $f011fdefaultState;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fdefault_country", type="integer", nullable=true)
     */
    private $f011fdefaultCountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fbudget_split", type="integer", nullable=true)
     */
    private $f011fbudgetSplit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fcreated_by", type="integer", nullable=true)
     */
    private $f011fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f011fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid_financial_year", type="integer", nullable=true)
     */
    private $f011fidFinancialYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcredit_note_narration", type="string", length=255, nullable=true)
     */
    private $f011fcreditNoteNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdebit_note_narration", type="string", length=255, nullable=true)
     */
    private $f011fdebitNoteNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f011finvoice_narration", type="string", length=255, nullable=true)
     */
    private $f011finvoiceNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fnoninvoice_narration", type="string", length=255, nullable=true)
     */
    private $f011fnoninvoiceNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fcreditor_narration", type="string", length=255, nullable=true)
     */
    private $f011fcreditorNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdebitor_narration", type="string", length=255, nullable=true)
     */
    private $f011fdebitorNarration;



    /**
     * Get f011fid
     *
     * @return integer
     */
    public function getF011fid()
    {
        return $this->f011fid;
    }

    /**
     * Set f011ffyStarting
     *
     * @param \DateTime $f011ffyStarting
     *
     * @return T011finitialConfig
     */
    public function setF011ffyStarting($f011ffyStarting)
    {
        $this->f011ffyStarting = $f011ffyStarting;

        return $this;
    }

    /**
     * Get f011ffyStarting
     *
     * @return \DateTime
     */
    public function getF011ffyStarting()
    {
        return $this->f011ffyStarting;
    }

    /**
     * Set f011ffyEnding
     *
     * @param \DateTime $f011ffyEnding
     *
     * @return T011finitialConfig
     */
    public function setF011ffyEnding($f011ffyEnding)
    {
        $this->f011ffyEnding = $f011ffyEnding;

        return $this;
    }

    /**
     * Get f011ffyEnding
     *
     * @return \DateTime
     */
    public function getF011ffyEnding()
    {
        return $this->f011ffyEnding;
    }

    /**
     * Set f011fgstNo
     *
     * @param string $f011fgstNo
     *
     * @return T011finitialConfig
     */
    public function setF011fgstNo($f011fgstNo)
    {
        $this->f011fgstNo = $f011fgstNo;

        return $this;
    }

    /**
     * Get f011fgstNo
     *
     * @return string
     */
    public function getF011fgstNo()
    {
        return $this->f011fgstNo;
    }

    /**
     * Set f011fcentralExcise
     *
     * @param string $f011fcentralExcise
     *
     * @return T011finitialConfig
     */
    public function setF011fcentralExcise($f011fcentralExcise)
    {
        $this->f011fcentralExcise = $f011fcentralExcise;

        return $this;
    }

    /**
     * Get f011fcentralExcise
     *
     * @return string
     */
    public function getF011fcentralExcise()
    {
        return $this->f011fcentralExcise;
    }

    /**
     * Set f011faddress1
     *
     * @param string $f011faddress1
     *
     * @return T011finitialConfig
     */
    public function setF011faddress1($f011faddress1)
    {
        $this->f011faddress1 = $f011faddress1;

        return $this;
    }

    /**
     * Get f011faddress1
     *
     * @return string
     */
    public function getF011faddress1()
    {
        return $this->f011faddress1;
    }

    /**
     * Set f011faddress2
     *
     * @param string $f011faddress2
     *
     * @return T011finitialConfig
     */
    public function setF011faddress2($f011faddress2)
    {
        $this->f011faddress2 = $f011faddress2;

        return $this;
    }

    /**
     * Get f011faddress2
     *
     * @return string
     */
    public function getF011faddress2()
    {
        return $this->f011faddress2;
    }

    /**
     * Set f011fcity
     *
     * @param string $f011fcity
     *
     * @return T011finitialConfig
     */
    public function setF011fcity($f011fcity)
    {
        $this->f011fcity = $f011fcity;

        return $this;
    }

    /**
     * Get f011fcity
     *
     * @return string
     */
    public function getF011fcity()
    {
        return $this->f011fcity;
    }

    /**
     * Set f011fcontactName
     *
     * @param string $f011fcontactName
     *
     * @return T011finitialConfig
     */
    public function setF011fcontactName($f011fcontactName)
    {
        $this->f011fcontactName = $f011fcontactName;

        return $this;
    }

    /**
     * Get f011fcontactName
     *
     * @return string
     */
    public function getF011fcontactName()
    {
        return $this->f011fcontactName;
    }

    /**
     * Set f011fphoneNo
     *
     * @param string $f011fphoneNo
     *
     * @return T011finitialConfig
     */
    public function setF011fphoneNo($f011fphoneNo)
    {
        $this->f011fphoneNo = $f011fphoneNo;

        return $this;
    }

    /**
     * Get f011fphoneNo
     *
     * @return string
     */
    public function getF011fphoneNo()
    {
        return $this->f011fphoneNo;
    }

    /**
     * Set f011femail
     *
     * @param string $f011femail
     *
     * @return T011finitialConfig
     */
    public function setF011femail($f011femail)
    {
        $this->f011femail = $f011femail;

        return $this;
    }

    /**
     * Get f011femail
     *
     * @return string
     */
    public function getF011femail()
    {
        return $this->f011femail;
    }

    /**
     * Set f011fupdatedDtTm
     *
     * @param \DateTime $f011fupdatedDtTm
     *
     * @return T011finitialConfig
     */
    public function setF011fupdatedDtTm($f011fupdatedDtTm)
    {
        $this->f011fupdatedDtTm = $f011fupdatedDtTm;

        return $this;
    }

    /**
     * Get f011fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF011fupdatedDtTm()
    {
        return $this->f011fupdatedDtTm;
    }

    /**
     * Set f011fstatus
     *
     * @param boolean $f011fstatus
     *
     * @return T011finitialConfig
     */
    public function setF011fstatus($f011fstatus)
    {
        $this->f011fstatus = $f011fstatus;

        return $this;
    }

    /**
     * Get f011fstatus
     *
     * @return boolean
     */
    public function getF011fstatus()
    {
        return $this->f011fstatus;
    }

    /**
     * Set f011fupdatedBy
     *
     * @param integer $f011fupdatedBy
     *
     * @return T011finitialConfig
     */
    public function setF011fupdatedBy($f011fupdatedBy)
    {
        $this->f011fupdatedBy = $f011fupdatedBy;

        return $this;
    }

    /**
     * Get f011fupdatedBy
     *
     * @return integer
     */
    public function getF011fupdatedBy()
    {
        return $this->f011fupdatedBy;
    }

    /**
     * Set f011fstate
     *
     * @param integer $f011fstate
     *
     * @return T011finitialConfig
     */
    public function setF011fstate($f011fstate)
    {
        $this->f011fstate = $f011fstate;

        return $this;
    }

    /**
     * Get f011fstate
     *
     * @return integer
     */
    public function getF011fstate()
    {
        return $this->f011fstate;
    }

    /**
     * Set f011fcountry
     *
     * @param integer $f011fcountry
     *
     * @return T011finitialConfig
     */
    public function setF011fcountry($f011fcountry)
    {
        $this->f011fcountry = $f011fcountry;

        return $this;
    }

    /**
     * Get f011fcountry
     *
     * @return integer
     */
    public function getF011fcountry()
    {
        return $this->f011fcountry;
    }

    /**
     * Set f011fdefaultState
     *
     * @param integer $f011fdefaultState
     *
     * @return T011finitialConfig
     */
    public function setF011fdefaultState($f011fdefaultState)
    {
        $this->f011fdefaultState = $f011fdefaultState;

        return $this;
    }

    /**
     * Get f011fdefaultState
     *
     * @return integer
     */
    public function getF011fdefaultState()
    {
        return $this->f011fdefaultState;
    }

    /**
     * Set f011fdefaultCountry
     *
     * @param integer $f011fdefaultCountry
     *
     * @return T011finitialConfig
     */
    public function setF011fdefaultCountry($f011fdefaultCountry)
    {
        $this->f011fdefaultCountry = $f011fdefaultCountry;

        return $this;
    }

    /**
     * Get f011fdefaultCountry
     *
     * @return integer
     */
    public function getF011fdefaultCountry()
    {
        return $this->f011fdefaultCountry;
    }

    /**
     * Set f011fbudgetSplit
     *
     * @param integer $f011fbudgetSplit
     *
     * @return T011finitialConfig
     */
    public function setF011fbudgetSplit($f011fbudgetSplit)
    {
        $this->f011fbudgetSplit = $f011fbudgetSplit;

        return $this;
    }

    /**
     * Get f011fbudgetSplit
     *
     * @return integer
     */
    public function getF011fbudgetSplit()
    {
        return $this->f011fbudgetSplit;
    }

    /**
     * Set f011fcreatedBy
     *
     * @param integer $f011fcreatedBy
     *
     * @return T011finitialConfig
     */
    public function setF011fcreatedBy($f011fcreatedBy)
    {
        $this->f011fcreatedBy = $f011fcreatedBy;

        return $this;
    }

    /**
     * Get f011fcreatedBy
     *
     * @return integer
     */
    public function getF011fcreatedBy()
    {
        return $this->f011fcreatedBy;
    }

    /**
     * Set f011fcreatedDtTm
     *
     * @param \DateTime $f011fcreatedDtTm
     *
     * @return T011finitialConfig
     */
    public function setF011fcreatedDtTm($f011fcreatedDtTm)
    {
        $this->f011fcreatedDtTm = $f011fcreatedDtTm;

        return $this;
    }

    /**
     * Get f011fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF011fcreatedDtTm()
    {
        return $this->f011fcreatedDtTm;
    }

    /**
     * Set f011fidFinancialYear
     *
     * @param integer $f011fidFinancialYear
     *
     * @return T011finitialConfig
     */
    public function setF011fidFinancialYear($f011fidFinancialYear)
    {
        $this->f011fidFinancialYear = $f011fidFinancialYear;

        return $this;
    }

    /**
     * Get f011fidFinancialYear
     *
     * @return integer
     */
    public function getF011fidFinancialYear()
    {
        return $this->f011fidFinancialYear;
    }

    /**
     * Set f011fcreditNoteNarration
     *
     * @param string $f011fcreditNoteNarration
     *
     * @return T011finitialConfig
     */
    public function setF011fcreditNoteNarration($f011fcreditNoteNarration)
    {
        $this->f011fcreditNoteNarration = $f011fcreditNoteNarration;

        return $this;
    }

    /**
     * Get f011fcreditNoteNarration
     *
     * @return string
     */
    public function getF011fcreditNoteNarration()
    {
        return $this->f011fcreditNoteNarration;
    }

    /**
     * Set f011fdebitNoteNarration
     *
     * @param string $f011fdebitNoteNarration
     *
     * @return T011finitialConfig
     */
    public function setF011fdebitNoteNarration($f011fdebitNoteNarration)
    {
        $this->f011fdebitNoteNarration = $f011fdebitNoteNarration;

        return $this;
    }

    /**
     * Get f011fdebitNoteNarration
     *
     * @return string
     */
    public function getF011fdebitNoteNarration()
    {
        return $this->f011fdebitNoteNarration;
    }

    /**
     * Set f011finvoiceNarration
     *
     * @param string $f011finvoiceNarration
     *
     * @return T011finitialConfig
     */
    public function setF011finvoiceNarration($f011finvoiceNarration)
    {
        $this->f011finvoiceNarration = $f011finvoiceNarration;

        return $this;
    }

    /**
     * Get f011finvoiceNarration
     *
     * @return string
     */
    public function getF011finvoiceNarration()
    {
        return $this->f011finvoiceNarration;
    }

    /**
     * Set f011fnoninvoiceNarration
     *
     * @param string $f011fnoninvoiceNarration
     *
     * @return T011finitialConfig
     */
    public function setF011fnoninvoiceNarration($f011fnoninvoiceNarration)
    {
        $this->f011fnoninvoiceNarration = $f011fnoninvoiceNarration;

        return $this;
    }

    /**
     * Get f011fnoninvoiceNarration
     *
     * @return string
     */
    public function getF011fnoninvoiceNarration()
    {
        return $this->f011fnoninvoiceNarration;
    }

    /**
     * Set f011fcreditorNarration
     *
     * @param string $f011fcreditorNarration
     *
     * @return T011finitialConfig
     */
    public function setF011fcreditorNarration($f011fcreditorNarration)
    {
        $this->f011fcreditorNarration = $f011fcreditorNarration;

        return $this;
    }

    /**
     * Get f011fcreditorNarration
     *
     * @return string
     */
    public function getF011fcreditorNarration()
    {
        return $this->f011fcreditorNarration;
    }

    /**
     * Set f011fdebitorNarration
     *
     * @param string $f011fdebitorNarration
     *
     * @return T011finitialConfig
     */
    public function setF011fdebitorNarration($f011fdebitorNarration)
    {
        $this->f011fdebitorNarration = $f011fdebitorNarration;

        return $this;
    }

    /**
     * Get f011fdebitorNarration
     *
     * @return string
     */
    public function getF011fdebitorNarration()
    {
        return $this->f011fdebitorNarration;
    }
}
