<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T136fnotificationEmail
 *
 * @ORM\Table(name="t136fnotification_email")
 * @ORM\Entity(repositoryClass="Application\Repository\NotificationRepository")
 */
class T136fnotificationEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f136fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f136fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f136femail", type="string", length=255, nullable=false)
     */
    private $f136femail;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f136fid_notification", type="integer", nullable=false)
     */
    private $f136fidNotification;


    /**
     * @var boolean
     *
     * @ORM\Column(name="f136fstatus", type="integer", nullable=false)
     */
    private $f136fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fcreated_by", type="integer", nullable=false)
     */
    private $f136fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fupdated_by", type="integer", nullable=false)
     */
    private $f136fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f136fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f136fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f136fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f136fupdatedDtTm;



    /**
     * Get f136fid
     *
     * @return integer
     */
    public function getF136fid()
    {
        return $this->f136fid;
    }

    /**
     * Set f136femail
     *
     * @param string $f136femail
     *
     * @return T136fnotificationEmail
     */
    public function setF136femail($f136femail)
    {
        $this->f136femail = $f136femail;

        return $this;
    }

    /**
     * Get f136femail
     *
     * @return string
     */
    public function getF136femail()
    {
        return $this->f136femail;
    }

     /**
     * Set f136fidNotification
     *
     * @param string $f136fidNotification
     *
     * @return T136fnotificationEmail
     */
    public function setF136fidNotification($f136fidNotification)
    {
        $this->f136fidNotification = $f136fidNotification;

        return $this;
    }

    /**
     * Get f136fidNotification
     *
     * @return string
     */
    public function getF136fidNotification()
    {
        return $this->f136fidNotification;
    }

  
    /**
     * Set f136fstatus
     *
     * @param boolean $f136fstatus
     *
     * @return T136fnotificationEmail
     */
    public function setF136fstatus($f136fstatus)
    {
        $this->f136fstatus = $f136fstatus;

        return $this;
    }


    /**
     * Get f136fstatus
     *
     * @return boolean
     */
    public function getF136fstatus()
    {
        return $this->f136fstatus;
    }

    /**
     * Set f136fcreatedBy
     *
     * @param integer $f136fcreatedBy
     *
     * @return T136fnotificationEmail
     */
    public function setF136fcreatedBy($f136fcreatedBy)
    {
        $this->f136fcreatedBy = $f136fcreatedBy;

        return $this;
    }

    /**
     * Get f136fcreatedBy
     *
     * @return integer
     */
    public function getF136fcreatedBy()
    {
        return $this->f136fcreatedBy;
    }

    /**
     * Set f136fupdatedBy
     *
     * @param integer $f136fupdatedBy
     *
     * @return T136fnotificationEmail
     */
    public function setF136fupdatedBy($f136fupdatedBy)
    {
        $this->f136fupdatedBy = $f136fupdatedBy;

        return $this;
    }

    /**
     * Get f136fupdatedBy
     *
     * @return integer
     */
    public function getF136fupdatedBy()
    {
        return $this->f136fupdatedBy;
    }

    /**
     * Set f136fcreatedDtTm
     *
     * @param \DateTime $f136fcreatedDtTm
     *
     * @return T136fnotificationEmail
     */
    public function setF136fcreatedDtTm($f136fcreatedDtTm)
    {
        $this->f136fcreatedDtTm = $f136fcreatedDtTm;

        return $this;
    }

    /**
     * Get f136fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF136fcreatedDtTm()
    {
        return $this->f136fcreatedDtTm;
    }

    /**
     * Set f136fupdatedDtTm
     *
     * @param \DateTime $f136fupdatedDtTm
     *
     * @return T136fnotificationEmail
     */
    public function setF136fupdatedDtTm($f136fupdatedDtTm)
    {
        $this->f136fupdatedDtTm = $f136fupdatedDtTm;

        return $this;
    }

    /**
     * Get f136fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF136fupdatedDtTm()
    {
        return $this->f136fupdatedDtTm;
    }
}
