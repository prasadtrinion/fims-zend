<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T091socsoCategory
 *
 * @ORM\Table(name="t091socso_category")
 * @ORM\Entity(repositoryClass="Application\Repository\SocsoCategoryRepository")
 */
class T091socsoCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fcategory_name", type="string", length=50, nullable=false)
     */
    private $f091fcategoryName;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fdescription", type="string", length=50, nullable=false)
     */
    private $f091fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=false)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=false)
     */
    private $f091fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=false)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fupdatedDtTm;



    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    /**
     * Set f091fcategoryName
     *
     * @param string $f091fcategoryName
     *
     * @return T091socsoCategory
     */
    public function setF091fcategoryName($f091fcategoryName)
    {
        $this->f091fcategoryName = $f091fcategoryName;

        return $this;
    }

    /**
     * Get f091fcategoryName
     *
     * @return string
     */
    public function getF091fcategoryName()
    {
        return $this->f091fcategoryName;
    }

    /**
     * Set f091fdescription
     *
     * @param string $f091fdescription
     *
     * @return T091socsoCategory
     */
    public function setF091fdescription($f091fdescription)
    {
        $this->f091fdescription = $f091fdescription;

        return $this;
    }

    /**
     * Get f091fdescription
     *
     * @return string
     */
    public function getF091fdescription()
    {
        return $this->f091fdescription;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091socsoCategory
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091socsoCategory
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091socsoCategory
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091socsoCategory
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T091socsoCategory
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }
}
