<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T066ffeeItemDetails
 *
 * @ORM\Table(name="t066ffee_item_details")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeItemRepository")
 */
class T066ffeeItemDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f066fidDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fid_fee_item", type="integer", nullable=false)
     */
    private $f066fidFeeItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fcr_fund", type="string", length=50, nullable=false)
     */
    private $f066fcrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fcr_activity", type="string", length=50, nullable=false)
     */
    private $f066fcrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fcr_account", type="string", length=50, nullable=false)
     */
    private $f066fcrAccount;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fcr_department", type="string",length=20, nullable=true)
     */
    private $f066fcrDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fdr_fund", type="string", length=50, nullable=false)
     */
    private $f066fdrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fdr_activity", type="string", length=50, nullable=false)
     */
    private $f066fdrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fdr_account", type="string", length=50, nullable=false)
     */
    private $f066fdrAccount;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fdr_department", type="string",length=20, nullable=true)
     */
    private $f066fdrDepartment;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fstatus", type="integer", nullable=false)
     */
    private $f066fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fcreated_by", type="integer", nullable=true)
     */
    private $f066fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fupdated_by", type="integer", nullable=true)
     */
    private $f066fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f066fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f066fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f066fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f066fupdatedDtTm;



    /**
     * Get f066fid
     *
     * @return integer
     */
    public function getF066fid()
    {
        return $this->f066fid;
    }

    /**
     * Set f066fstatus
     *
     * @param integer $f066fstatus
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fstatus($f066fstatus)
    {
        $this->f066fstatus = $f066fstatus;

        return $this;
    }

    /**
     * Get f066fstatus
     *
     * @return integer
     */
    public function getF066fstatus()
    {
        return $this->f066fstatus;
    }

    /**
     * Set f066fidFeeItem
     *
     * @param integer $f066fidFeeItem
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fidFeeItem($f066fidFeeItem)
    {
        $this->f066fidFeeItem = $f066fidFeeItem;

        return $this;
    }

    /**
     * Get f066fidFeeItem
     *
     * @return integer
     */
    public function getF066fidFeeItem()
    {
        return $this->f066fidFeeItem;
    }

    /**
     * Set f066fcreatedBy
     *
     * @param integer $f066fcreatedBy
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcreatedBy($f066fcreatedBy)
    {
        $this->f066fcreatedBy = $f066fcreatedBy;

        return $this;
    }

    /**
     * Get f066fcreatedBy
     *
     * @return integer
     */
    public function getF066fcreatedBy()
    {
        return $this->f066fcreatedBy;
    }

    /**
     * Set f066fupdatedBy
     *
     * @param integer $f066fupdatedBy
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fupdatedBy($f066fupdatedBy)
    {
        $this->f066fupdatedBy = $f066fupdatedBy;

        return $this;
    }

    /**
     * Get f066fupdatedBy
     *
     * @return integer
     */
    public function getF066fupdatedBy()
    {
        return $this->f066fupdatedBy;
    }

    /**
     * Set f066fcreatedDtTm
     *
     * @param \DateTime $f066fcreatedDtTm
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcreatedDtTm($f066fcreatedDtTm)
    {
        $this->f066fcreatedDtTm = $f066fcreatedDtTm;

        return $this;
    }

    /**
     * Get f066fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF066fcreatedDtTm()
    {
        return $this->f066fcreatedDtTm;
    }

    /**
     * Set f066fupdatedDtTm
     *
     * @param \DateTime $f066fupdatedDtTm
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fupdatedDtTm($f066fupdatedDtTm)
    {
        $this->f066fupdatedDtTm = $f066fupdatedDtTm;

        return $this;
    }

    /**
     * Get f066fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF066fupdatedDtTm()
    {
        return $this->f066fupdatedDtTm;
    }

    /**
     * Set f066fdrFund
     *
     * @param integer $f066fdrFund
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fdrFund($f066fdrFund)
    {
        $this->f066fdrFund = $f066fdrFund;

        return $this;
    }

    /**
     * Get f066fdrFund
     *
     * @return integer
     */
    public function getF066fdrFund()
    {
        return $this->f066fdrFund;
    }

    /**
     * Set f066fdrAccount
     *
     * @param integer $f066fdrAccount
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fdrAccount($f066fdrAccount)
    {
        $this->f066fdrAccount = $f066fdrAccount;

        return $this;
    }

    /**
     * Get f066fdrAccount
     *
     * @return integer
     */
    public function getF066fdrAccount()
    {
        return $this->f066fdrAccount;
    }

    /**
     * Set f066fdrActivity
     *
     * @param integer $f066fdrActivity
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fdrActivity($f066fdrActivity)
    {
        $this->f066fdrActivity = $f066fdrActivity;

        return $this;
    }

    /**
     * Get f066fdrActivity
     *
     * @return integer
     */
    public function getF066fdrActivity()
    {
        return $this->f066fdrActivity;
    }

    /**
     * Set f066fdrDepartment
     *
     * @param integer $f066fdrDepartment
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fdrDepartment($f066fdrDepartment)
    {
        $this->f066fdrDepartment = $f066fdrDepartment;

        return $this;
    }

    /**
     * Get f066fdrDepartment
     *
     * @return integer
     */
    public function getF066fdrDepartment()
    {
        return $this->f066fdrDepartment;
    }

    /**
     * Set f066fcrFund
     *
     * @param integer $f066fcrFund
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcrFund($f066fcrFund)
    {
        $this->f066fcrFund = $f066fcrFund;

        return $this;
    }

    /**
     * Get f066fcrFund
     *
     * @return integer
     */
    public function getF066fcrFund()
    {
        return $this->f066fcrFund;
    }

    /**
     * Set f066fcrAccount
     *
     * @param integer $f066fcrAccount
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcrAccount($f066fcrAccount)
    {
        $this->f066fcrAccount = $f066fcrAccount;

        return $this;
    }

    /**
     * Get f066fcrAccount
     *
     * @return integer
     */
    public function getF066fcrAccount()
    {
        return $this->f066fcrAccount;
    }

    /**
     * Set f066fcrActivity
     *
     * @param integer $f066fcrActivity
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcrActivity($f066fcrActivity)
    {
        $this->f066fcrActivity = $f066fcrActivity;

        return $this;
    }

    /**
     * Get f066fcrActivity
     *
     * @return integer
     */
    public function getF066fcrActivity()
    {
        return $this->f066fcrActivity;
    }

    /**
     * Set f066fcrDepartment
     *
     * @param integer $f066fcrDepartment
     *
     * @return T066ffeeItemDetails
     */
    public function setF066fcrDepartment($f066fcrDepartment)
    {
        $this->f066fcrDepartment = $f066fcrDepartment;

        return $this;
    }

    /**
     * Get f066fcrDepartment
     *
     * @return integer
     */
    public function getF066fcrDepartment()
    {
        return $this->f066fcrDepartment;
    }
}
