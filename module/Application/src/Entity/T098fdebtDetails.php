<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T098fdebtDetails
 *
 * @ORM\Table(name="t098fdebt_details")
 * @ORM\Entity(repositoryClass="Application\Repository\DebtInformationRepository")
 */
class T098fdebtDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f098fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_debt", type="integer", nullable=false)
     */
    private $f098fidDebt;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fdescription", type="string", length=50, nullable=true)
     */
    private $f098fdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fstart_date", type="datetime", nullable=true)
     */
    private $f098fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fend_date", type="datetime", nullable=true)
     */
    private $f098fendDate;


    /**
     * @var integer
     *
     * @ORM\Column(name="f098fstatus", type="integer", nullable=true)
     */
    private $f098fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fcreated_by", type="integer", nullable=true)
     */
    private $f098fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fupdated_by", type="integer", nullable=true)
     */
    private $f098fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f098fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f098fupdatedDtTm;



    /**
     * Get f098fid
     *
     * @return integer
     */
    public function getF098fid()
    {
        return $this->f098fid;
    }

    /**
     * Set f098fidDebt
     *
     * @param integer $f098fidDebt
     *
     * @return T098fdebtDetails
     */
    public function setF098fidDebt($f098fidDebt)
    {
        $this->f098fidDebt = $f098fidDebt;

        return $this;
    }

    /**
     * Get f098fidDebt
     *
     * @return integer
     */
    public function getF098fidDebt()
    {
        return $this->f098fidDebt;
    }

    /**
     * Set f098fdescription
     *
     * @param string $f098fdescription
     *
     * @return T098fdebtDetails
     */
    public function setF098fdescription($f098fdescription)
    {
        $this->f098fdescription = $f098fdescription;

        return $this;
    }

    /**
     * Get f098fdescription
     *
     * @return string
     */
    public function getF098fdescription()
    {
        return $this->f098fdescription;
    }

    /**
     * Set f098fstartDate
     *
     * @param \DateTime $f098fstartDate
     *
     * @return T098fdebtDetails
     */
    public function setF098fstartDate($f098fstartDate)
    {
        $this->f098fstartDate = $f098fstartDate;

        return $this;
    }

    /**
     * Get f098fstartDate
     *
     * @return \DateTime
     */
    public function getF098fstartDate()
    {
        return $this->f098fstartDate;
    }

    /**
     * Set f098fendDate
     *
     * @param \DateTime $f098fendDate
     *
     * @return T098fdebtDetails
     */
    public function setF098fendDate($f098fendDate)
    {
        $this->f098fendDate = $f098fendDate;

        return $this;
    }

    /**
     * Get f098fendDate
     *
     * @return \DateTime
     */
    public function getF098fendDate()
    {
        return $this->f098fendDate;
    }

    /**
     * Set f098fstatus
     *
     * @param integer $f098fstatus
     *
     * @return T098fdebtDetails
     */
    public function setF098fstatus($f098fstatus)
    {
        $this->f098fstatus = $f098fstatus;

        return $this;
    }

    /**
     * Get f098fstatus
     *
     * @return integer
     */
    public function getF098fstatus()
    {
        return $this->f098fstatus;
    }

    /**
     * Set f098fcreatedBy
     *
     * @param integer $f098fcreatedBy
     *
     * @return T098fdebtDetails
     */
    public function setF098fcreatedBy($f098fcreatedBy)
    {
        $this->f098fcreatedBy = $f098fcreatedBy;

        return $this;
    }

    /**
     * Get f098fcreatedBy
     *
     * @return integer
     */
    public function getF098fcreatedBy()
    {
        return $this->f098fcreatedBy;
    }

    /**
     * Set f098fupdatedBy
     *
     * @param integer $f098fupdatedBy
     *
     * @return T098fdebtDetails
     */
    public function setF098fupdatedBy($f098fupdatedBy)
    {
        $this->f098fupdatedBy = $f098fupdatedBy;

        return $this;
    }

    /**
     * Get f098fupdatedBy
     *
     * @return integer
     */
    public function getF098fupdatedBy()
    {
        return $this->f098fupdatedBy;
    }

    /**
     * Set f098fcreatedDtTm
     *
     * @param \DateTime $f098fcreatedDtTm
     *
     * @return T098fdebtDetails
     */
    public function setF098fcreatedDtTm($f098fcreatedDtTm)
    {
        $this->f098fcreatedDtTm = $f098fcreatedDtTm;

        return $this;
    }

    /**
     * Get f098fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fcreatedDtTm()
    {
        return $this->f098fcreatedDtTm;
    }

    /**
     * Set f098fupdatedDtTm
     *
     * @param \DateTime $f098fupdatedDtTm
     *
     * @return T098fdebtDetails
     */
    public function setF098fupdatedDtTm($f098fupdatedDtTm)
    {
        $this->f098fupdatedDtTm = $f098fupdatedDtTm;

        return $this;
    }

    /**
     * Get f098fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fupdatedDtTm()
    {
        return $this->f098fupdatedDtTm;
    }
}
