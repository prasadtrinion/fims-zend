<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T061finvestmentInstitution
 *
 * @ORM\Table(name="t061finvestment_institution", uniqueConstraints={@ORM\UniqueConstraint(name="f061finstitution_code_UNIQUE", columns={"f061finstitution_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\InvestmentInstitutionRepository")
 */
class T061finvestmentInstitution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f061fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f061finstitution_code", type="string", length=255, nullable=false)
     */
    private $f061finstitutionCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fname", type="string", length=255, nullable=false)
     */
    private $f061fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f061faddress", type="string", length=255, nullable=false)
     */
    private $f061faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fcontact_no", type="string", length=100, nullable=false)
     */
    private $f061fcontactNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fcontact_person", type="string", length=200, nullable=false)
     */
    private $f061fcontactPerson;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_bank", type="integer", nullable=true)
     */
    private $f061fidBank;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fbranch", type="string", length=100, nullable=false)
     */
    private $f061fbranch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fstatus", type="integer", nullable=false)
     */
    private $f061fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fcreated_by", type="integer", nullable=false)
     */
    private $f061fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fupdated_by", type="integer", nullable=false)
     */
    private $f061fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f061fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f061fupdatedDtTm;


    /**
     * @var string
     *
     * @ORM\Column(name="f061fcountry", type="integer",  nullable=false)
     */
    private $f061fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fstate", type="integer",  nullable=false)
     */
    private $f061fstate;


    /**
     * @var integer
     *
     * @ORM\Column(name="f061fcity", type="string", length=100, nullable=false)
     */
    private $f061fcity;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fpost_code", type="string", length=100, nullable=false)
     */
    private $f061fpostCode;


    /**
     * Get f061fid
     *
     * @return integer
     */
    public function getF061fid()
    {
        return $this->f061fid;
    }

    /**
     * Set f061finstitutionCode
     *
     * @param string $f061finstitutionCode
     *
     * @return T061finvestmentInstitution
     */
    public function setF061finstitutionCode($f061finstitutionCode)
    {
        $this->f061finstitutionCode = $f061finstitutionCode;

        return $this;
    }

    /**
     * Get f061finstitutionCode
     *
     * @return string
     */
    public function getF061finstitutionCode()
    {
        return $this->f061finstitutionCode;
    }

    /**
     * Set f061fbranch
     *
     * @param string $f061fbranch
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fbranch($f061fbranch)
    {
        $this->f061fbranch = $f061fbranch;

        return $this;
    }

    /**
     * Get f061fbranch
     *
     * @return string
     */
    public function getF061fbranch()
    {
        return $this->f061fbranch;
    }

    /**
     * Set f061fname
     *
     * @param string $f061fname
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fname($f061fname)
    {
        $this->f061fname = $f061fname;

        return $this;
    }

    /**
     * Get f061fname
     *
     * @return string
     */
    public function getF061fname()
    {
        return $this->f061fname;
    }

    /**
     * Set f061faddress
     *
     * @param string $f061faddress
     *
     * @return T061finvestmentInstitution
     */
    public function setF061faddress($f061faddress)
    {
        $this->f061faddress = $f061faddress;

        return $this;
    }

    /**
     * Get f061faddress
     *
     * @return string
     */
    public function getF061faddress()
    {
        return $this->f061faddress;
    }

    /**
     * Set f061fcontactNo
     *
     * @param string $f061fcontactNo
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcontactNo($f061fcontactNo)
    {
        $this->f061fcontactNo = $f061fcontactNo;

        return $this;
    }

    /**
     * Get f061fcontactNo
     *
     * @return string
     */
    public function getF061fcontactNo()
    {
        return $this->f061fcontactNo;
    }

    /**
     * Set f061fcontactPerson
     *
     * @param string $f061fcontactPerson
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcontactPerson($f061fcontactPerson)
    {
        $this->f061fcontactPerson = $f061fcontactPerson;

        return $this;
    }

    /**
     * Get f061fcontactPerson
     *
     * @return string
     */
    public function getF061fcontactPerson()
    {
        return $this->f061fcontactPerson;
    }

    /**
     * Set f061fstatus
     *
     * @param integer $f061fstatus
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fstatus($f061fstatus)
    {
        $this->f061fstatus = $f061fstatus;

        return $this;
    }

    /**
     * Get f061fstatus
     *
     * @return integer
     */
    public function getF061fstatus()
    {
        return $this->f061fstatus;
    }

    /**
     * Set f061fidBank
     *
     * @param integer $f061fidBank
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fidBank($f061fidBank)
    {
        $this->f061fidBank = $f061fidBank;

        return $this;
    }

    /**
     * Get f061fidBank
     *
     * @return integer
     */
    public function getF061fidBank()
    {
        return $this->f061fidBank;
    }

    /**
     * Set f061fcreatedBy
     *
     * @param integer $f061fcreatedBy
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcreatedBy($f061fcreatedBy)
    {
        $this->f061fcreatedBy = $f061fcreatedBy;

        return $this;
    }

    /**
     * Get f061fcreatedBy
     *
     * @return integer
     */
    public function getF061fcreatedBy()
    {
        return $this->f061fcreatedBy;
    }

    /**
     * Set f061fupdatedBy
     *
     * @param integer $f061fupdatedBy
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fupdatedBy($f061fupdatedBy)
    {
        $this->f061fupdatedBy = $f061fupdatedBy;

        return $this;
    }

    /**
     * Get f061fupdatedBy
     *
     * @return integer
     */
    public function getF061fupdatedBy()
    {
        return $this->f061fupdatedBy;
    }

    /**
     * Set f061fcreatedDtTm
     *
     * @param \DateTime $f061fcreatedDtTm
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcreatedDtTm($f061fcreatedDtTm)
    {
        $this->f061fcreatedDtTm = $f061fcreatedDtTm;

        return $this;
    }

    /**
     * Get f061fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fcreatedDtTm()
    {
        return $this->f061fcreatedDtTm;
    }

    /**
     * Set f061fupdatedDtTm
     *
     * @param \DateTime $f061fupdatedDtTm
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fupdatedDtTm($f061fupdatedDtTm)
    {
        $this->f061fupdatedDtTm = $f061fupdatedDtTm;

        return $this;
    }

    /**
     * Get f061fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fupdatedDtTm()
    {
        return $this->f061fupdatedDtTm;
    }

     /**
     * Set f061fcountry
     *
     * @param string $f061fcountry
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcountry($f061fcountry)
    {
        $this->f061fcountry = $f061fcountry;

        return $this;
    }

    /**
     * Get f061fcountry
     *
     * @return string
     */
    public function getF061fcountry()
    {
        return $this->f061fcountry;
    }
    
    /**
     * Set f061fstate
     *
     * @param string $f061fstate
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fstate($f061fstate)
    {
        $this->f061fstate = $f061fstate;

        return $this;
    }

    /**
     * Get f061fstate
     *
     * @return string
     */
    public function getF061fstate()
    {
        return $this->f061fstate;
    }



    /**
     * Set f061fcity
     *
     * @param integer $f061fcity
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fcity($f061fcity)
    {
        $this->f061fcity = $f061fcity;

        return $this;
    }

    /**
     * Get f061fcity
     *
     * @return integer
     */
    public function getF061fcity()
    {
        return $this->f061fcity;
    }


    /**
     * Set f061fpostCode
     *
     * @param string $f061fpostCode
     *
     * @return T061finvestmentInstitution
     */
    public function setF061fpostCode($f061fpostCode)
    {
        $this->f061fpostCode = $f061fpostCode;

        return $this;
    }

    /**
     * Get f061fpostCode
     *
     * @return string
     */
    public function getF061fpostCode()
    {
        return $this->f061fpostCode;
    }
}
