<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T108fbudgetSummary
 *
 * @ORM\Table(name="t108fbudget_summary")
* @ORM\Entity(repositoryClass="Application\Repository\BudgetSummaryRepository")
 */
class T108fbudgetSummary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f108fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f108fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f108fdepartment", type="string", length=25, nullable=true)
     */
    private $f108fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f108ffund", type="string", length=25, nullable=true)
     */
    private $f108ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f108factivity", type="string", length=25, nullable=true)
     */
    private $f108factivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f108faccount", type="string", length=25, nullable=true)
     */
    private $f108faccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fid_financial_year", type="integer", nullable=true)
     */
    private $f108fidFinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fallocated_amount", type="integer",  nullable=true)
     */
    private $f108fallocatedAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fincrement_amount", type="integer",  nullable=true)
     */
    private $f108fincrementAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fdecrement_amount", type="integer",  nullable=true)
     */
    private $f108fdecrementAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fveriment_from", type="integer",  nullable=true)
     */
    private $f108fverimentFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fveriment_to", type="integer",  nullable=true)
     */
    private $f108fverimentTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fbudget_expensed", type="integer",  nullable=true)
     */
    private $f108fbudgetExpensed;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fbudget_commitment", type="integer",  nullable=true)
     */
    private $f108fbudgetCommitment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fbudget_pending", type="integer",  nullable=true)
     */
    private $f108fbudgetPending;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fstatus", type="integer", nullable=true)
     */
    private $f108fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fcreated_by", type="integer", nullable=true)
     */
    private $f108fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f108fupdated_by", type="integer", nullable=true)
     */
    private $f108fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f108fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f108fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f108fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f108fupdatedDtTm;



    /**
     * Get f108fid
     *
     * @return integer
     */
    public function getF108fid()
    {
        return $this->f108fid;
    }

    /**
     * Set f108fdepartment
     *
     * @param string $f108fdepartment
     *
     * @return T108fbudgetSummary
     */
    public function setF108fdepartment($f108fdepartment)
    {
        $this->f108fdepartment = $f108fdepartment;

        return $this;
    }

    /**
     * Get f108fdepartment
     *
     * @return string
     */
    public function getF108fdepartment()
    {
        return $this->f108fdepartment;
    }

     /**
     * Set f108ffund
     *
     * @param string $f108ffund
     *
     * @return T108fbudgetSummary
     */
    public function setF108ffund($f108ffund)
    {
        $this->f108ffund = $f108ffund;

        return $this;
    }

    /**
     * Get f108ffund
     *
     * @return string
     */
    public function getF108ffund()
    {
        return $this->f108ffund;
    }

     /**
     * Set f108faccount
     *
     * @param string $f108faccount
     *
     * @return T108fbudgetSummary
     */
    public function setF108faccount($f108faccount)
    {
        $this->f108faccount = $f108faccount;

        return $this;
    }

    /**
     * Get f108faccount
     *
     * @return string
     */
    public function getF108faccount()
    {
        return $this->f108faccount;
    }

     /**
     * Set f108factivity
     *
     * @param string $f108factivity
     *
     * @return T108fbudgetSummary
     */
    public function setF108factivity($f108factivity)
    {
        $this->f108factivity = $f108factivity;

        return $this;
    }

    /**
     * Get f108factivity
     *
     * @return string
     */
    public function getF108factivity()
    {
        return $this->f108factivity;
    }

    /**
     * Set f108fidFinancialYear
     *
     * @param integer $f108fidFinancialYear
     *
     * @return T108fbudgetSummary
     */
    public function setF108fidFinancialYear($f108fidFinancialYear)
    {
        $this->f108fidFinancialYear = $f108fidFinancialYear;

        return $this;
    }

    /**
     * Get f108fidFinancialYear
     *
     * @return integer
     */
    public function getF108fidFinancialYear()
    {
        return $this->f108fidFinancialYear;
    }

    /**
     * Set f108fallocatedAmount
     *
     * @param integer $f108fallocatedAmount
     *
     * @return T108fbudgetSummary
     */
    public function setF108fallocatedAmount($f108fallocatedAmount)
    {
        $this->f108fallocatedAmount = $f108fallocatedAmount;

        return $this;
    }

    /**
     * Get f108fallocatedAmount
     *
     * @return integer
     */
    public function getF108fallocatedAmount()
    {
        return $this->f108fallocatedAmount;
    }

    /**
     * Set f108fincrementAmount
     *
     * @param integer $f108fincrementAmount
     *
     * @return T108fbudgetSummary
     */
    public function setF108fincrementAmount($f108fincrementAmount)
    {
        $this->f108fincrementAmount = $f108fincrementAmount;

        return $this;
    }

    /**
     * Get f108fincrementAmount
     *
     * @return integer
     */
    public function getF108fincrementAmount()
    {
        return $this->f108fincrementAmount;
    }

    /**
     * Set f108fdecrementAmount
     *
     * @param integer $f108fdecrementAmount
     *
     * @return T108fbudgetSummary
     */
    public function setF108fdecrementAmount($f108fdecrementAmount)
    {
        $this->f108fdecrementAmount = $f108fdecrementAmount;

        return $this;
    }

    /**
     * Get f108fdecrementAmount
     *
     * @return integer
     */
    public function getF108fdecrementAmount()
    {
        return $this->f108fdecrementAmount;
    }

    /**
     * Set f108fverimentFrom
     *
     * @param integer $f108fverimentFrom
     *
     * @return T108fbudgetSummary
     */
    public function setF108fverimentFrom($f108fverimentFrom)
    {
        $this->f108fverimentFrom = $f108fverimentFrom;

        return $this;
    }

    /**
     * Get f108fverimentFrom
     *
     * @return integer
     */
    public function getF108fverimentFrom()
    {
        return $this->f108fverimentFrom;
    }

    /**
     * Set f108fverimentTo
     *
     * @param integer $f108fverimentTo
     *
     * @return T108fbudgetSummary
     */
    public function setF108fverimentTo($f108fverimentTo)
    {
        $this->f108fverimentTo = $f108fverimentTo;

        return $this;
    }

    /**
     * Get f108fverimentTo
     *
     * @return integer
     */
    public function getF108fverimentTo()
    {
        return $this->f108fverimentTo;
    }

    /**
     * Set f108fbudgetExpensed
     *
     * @param integer $f108fbudgetExpensed
     *
     * @return T108fbudgetSummary
     */
    public function setF108fbudgetExpensed($f108fbudgetExpensed)
    {
        $this->f108fbudgetExpensed = $f108fbudgetExpensed;

        return $this;
    }

    /**
     * Get f108fbudgetExpensed
     *
     * @return integer
     */
    public function getF108fbudgetExpensed()
    {
        return $this->f108fbudgetExpensed;
    }

    /**
     * Set f108fbudgetPending
     *
     * @param integer $f108fbudgetPending
     *
     * @return T108fbudgetSummary
     */
    public function setF108fbudgetPending($f108fbudgetPending)
    {
        $this->f108fbudgetPending = $f108fbudgetPending;

        return $this;
    }

    /**
     * Get f108fbudgetPending
     *
     * @return integer
     */
    public function getF108fbudgetPending()
    {
        return $this->f108fbudgetPending;
    }

    /**
     * Set f108fstatus
     *
     * @param integer $f108fstatus
     *
     * @return T108fbudgetSummary
     */
    public function setF108fstatus($f108fstatus)
    {
        $this->f108fstatus = $f108fstatus;

        return $this;
    }

    /**
     * Get f108fstatus
     *
     * @return integer
     */
    public function getF108fstatus()
    {
        return $this->f108fstatus;
    }

    /**
     * Set f108fcreatedBy
     *
     * @param integer $f108fcreatedBy
     *
     * @return T108fbudgetSummary
     */
    public function setF108fcreatedBy($f108fcreatedBy)
    {
        $this->f108fcreatedBy = $f108fcreatedBy;

        return $this;
    }

    /**
     * Get f108fcreatedBy
     *
     * @return integer
     */
    public function getF108fcreatedBy()
    {
        return $this->f108fcreatedBy;
    }

    /**
     * Set f108fupdatedBy
     *
     * @param integer $f108fupdatedBy
     *
     * @return T108fbudgetSummary
     */
    public function setF108fupdatedBy($f108fupdatedBy)
    {
        $this->f108fupdatedBy = $f108fupdatedBy;

        return $this;
    }

    /**
     * Get f108fupdatedBy
     *
     * @return integer
     */
    public function getF108fupdatedBy()
    {
        return $this->f108fupdatedBy;
    }

    /**
     * Set f108fcreatedDtTm
     *
     * @param \DateTime $f108fcreatedDtTm
     *
     * @return T108fbudgetSummary
     */
    public function setF108fcreatedDtTm($f108fcreatedDtTm)
    {
        $this->f108fcreatedDtTm = $f108fcreatedDtTm;

        return $this;
    }

    /**
     * Get f108fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF108fcreatedDtTm()
    {
        return $this->f108fcreatedDtTm;
    }

    /**
     * Set f108fupdatedDtTm
     *
     * @param \DateTime $f108fupdatedDtTm
     *
     * @return T108fbudgetSummary
     */
    public function setF108fupdatedDtTm($f108fupdatedDtTm)
    {
        $this->f108fupdatedDtTm = $f108fupdatedDtTm;

        return $this;
    }

    /**
     * Get f108fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF108fupdatedDtTm()
    {
        return $this->f108fupdatedDtTm;
    }

    /**
     * Set f108fbudgetCommitment
     *
     * @param string $f108fbudgetCommitment
     *
     * @return T108fbudgetSummary
     */
    public function setF108fbudgetCommitment($f108fbudgetCommitment)
    {
        $this->f108fbudgetCommitment = $f108fbudgetCommitment;

        return $this;
    }

    /**
     * Get f108fbudgetCommitment
     *
     * @return string
     */
    public function getF108fbudgetCommitment()
    {
        return $this->f108fbudgetCommitment;
    }
}
