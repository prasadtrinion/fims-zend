<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T051fcashAdvanceType
 *
 * @ORM\Table(name="t051fcash_advance_type", uniqueConstraints={@ORM\UniqueConstraint(name="f051fcash_id_UNIQUE", columns={"f051fcash_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CashAdvanceTypeRepository")
 */
class T051fcashAdvanceType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f051fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fcash_id", type="integer", nullable=false)
     */
    private $f051fcashId;

    /**
     * @var string
     *
     * @ORM\Column(name="f051fffund", type="string", length=20, nullable=false)
     */
    private $f051fffund;


    /**
     * @var string
     *
     * @ORM\Column(name="f051factivity", type="string", length=20, nullable=false)
     */
    private $f051factivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f051faccount", type="string", length=20, nullable=false)
     */
    private $f051faccount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f051fdescription", type="string", length=100, nullable=false)
     */
    private $f051fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fstatus", type="integer", nullable=false)
     */
    private $f051fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fcreated_by", type="integer", nullable=true)
     */
    private $f051fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fupdated_by", type="integer", nullable=true)
     */
    private $f051fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fupdatedDtTm;



    /**
     * Get f051fid
     *
     * @return integer
     */
    public function getF051fid()
    {
        return $this->f051fid;
    }

    /**
     * Set f051fcashId
     *
     * @param integer $f051fcashId
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fcashId($f051fcashId)
    {
        $this->f051fcashId = $f051fcashId;

        return $this;
    }

    /**
     * Get f051fcashId
     *
     * @return string
     */
    public function getF051fcashId()
    {
        return $this->f051fcashId;
    }

    /**
     * Set f051fdescription
     *
     * @param string $f051fdescription
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fdescription($f051fdescription)
    {
        $this->f051fdescription = $f051fdescription;

        return $this;
    }

    /**
     * Get f051fdescription
     *
     * @return string
     */
    public function getF051fdescription()
    {
        return $this->f051fdescription;
    }

    /**
     * Set f051fstatus
     *
     * @param integer $f051fstatus
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fstatus($f051fstatus)
    {
        $this->f051fstatus = $f051fstatus;

        return $this;
    }

    /**
     * Get f051fstatus
     *
     * @return integer
     */
    public function getF051fstatus()
    {
        return $this->f051fstatus;
    }

    /**
     * Set f051fcreatedBy
     *
     * @param integer $f051fcreatedBy
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fcreatedBy($f051fcreatedBy)
    {
        $this->f051fcreatedBy = $f051fcreatedBy;

        return $this;
    }

    /**
     * Get f051fcreatedBy
     *
     * @return integer
     */
    public function getF051fcreatedBy()
    {
        return $this->f051fcreatedBy;
    }

    /**
     * Set f051fupdatedBy
     *
     * @param integer $f051fupdatedBy
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fupdatedBy($f051fupdatedBy)
    {
        $this->f051fupdatedBy = $f051fupdatedBy;

        return $this;
    }

    /**
     * Get f051fupdatedBy
     *
     * @return integer
     */
    public function getF051fupdatedBy()
    {
        return $this->f051fupdatedBy;
    }

    /**
     * Set f051fcreatedDtTm
     *
     * @param \DateTime $f051fcreatedDtTm
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fcreatedDtTm($f051fcreatedDtTm)
    {
        $this->f051fcreatedDtTm = $f051fcreatedDtTm;

        return $this;
    }

    /**
     * Get f051fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fcreatedDtTm()
    {
        return $this->f051fcreatedDtTm;
    }

    /**
     * Set f051fupdatedDtTm
     *
     * @param \DateTime $f051fupdatedDtTm
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fupdatedDtTm($f051fupdatedDtTm)
    {
        $this->f051fupdatedDtTm = $f051fupdatedDtTm;

        return $this;
    }

    /**
     * Get f051fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fupdatedDtTm()
    {
        return $this->f051fupdatedDtTm;
    }

     /**
     * Set f051fffund
     *
     * @param string $f051fffund
     *
     * @return T051fcashAdvanceType
     */
    public function setF051fffund($f051fffund)
    {
        $this->f051fffund = $f051fffund;

        return $this;
    }

    /**
     * Get f051fffund
     *
     * @return string
     */
    public function getF051fffund()
    {
        return $this->f051fffund;
    }

    /**
     * Set f051factivity
     *
     * @param string $f051factivity
     *
     * @return T051fcashAdvanceType
     */
    public function setF051factivity($f051factivity)
    {
        $this->f051factivity = $f051factivity;

        return $this;
    }

    /**
     * Get f051factivity
     *
     * @return string
     */
    public function getF051factivity()
    {
        return $this->f051factivity;
    }


    /**
     * Set f051faccount
     *
     * @param string $f051faccount
     *
     * @return T051fcashAdvanceType
     */
    public function setF051faccount($f051faccount)
    {
        $this->f051faccount = $f051faccount;

        return $this;
    }

    /**
     * Get f051faccount
     *
     * @return string
     */
    public function getF051faccount()
    {
        return $this->f051faccount;
    }
}
