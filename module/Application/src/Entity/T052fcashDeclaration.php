<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052fcashDeclaration
 *
 * @ORM\Table(name="t052fcash_declaration")
 * @ORM\Entity(repositoryClass="Application\Repository\CashDeclarationRepository")
 */
class T052fcashDeclaration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_declare", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fidDeclare;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fcash_advance", type="string", length=20, nullable=true)
     */
    private $f052fcashAdvance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_cash", type="integer", nullable=true)
     */
    private $f052fidCash;

    /**
     * @var string
     *

     * @ORM\Column(name="f052fid_staff", type="integer", nullable=true)
     */
    private $f052fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fid_department", type="string", length=50, nullable=true)
     */
    private $f052fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_financial_year", type="integer", nullable=true)
     */
    private $f052fidFinancialYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f052freceipt_no", type="string", length=50, nullable=true)
     */
    private $f052freceiptNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052freceipt_amount",  type="integer", nullable=true)
     */
    private $f052freceiptAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052ftotal", type="integer", nullable=true)
     */
    private $f052ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=true)
     */
    private $f052fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fapproval_status", type="integer", nullable=true)
     */
    private $f052fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=true)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=true)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fupdatedDtTm;



    /**
     * Get f052fidDeclare
     *
     * @return integer
     */
    public function getF052fidDeclare()
    {
        return $this->f052fidDeclare;
    }

    /**
     * Set f052fcashAdvance
     *
     * @param string $f052fcashAdvance
     *
     * @return T052fcashDeclaration
     */
    public function setF052fcashAdvance($f052fcashAdvance)
    {
        $this->f052fcashAdvance = $f052fcashAdvance;

        return $this;
    }

    /**
     * Get f052fcashAdvance
     *
     * @return string
     */
    public function getF052fcashAdvance()
    {
        return $this->f052fcashAdvance;
    }

    /**
     * Set f052ftotal
     *
     * @param integer $f052ftotal
     *
     * @return T052fcashDeclaration
     */
    public function setF052ftotal($f052ftotal)
    {
        $this->f052ftotal = $f052ftotal;

        return $this;
    }

    /**
     * Get f052ftotal
     *
     * @return integer
     */
    public function getF052ftotal()
    {
        return $this->f052ftotal;
    }

    /**
     * Set f052fidStaff
     *
     * @param string $f052fidStaff
     *
     * @return T052fcashDeclaration
     */
    public function setF052fidStaff($f052fidStaff)
    {
        $this->f052fidStaff = $f052fidStaff;

        return $this;
    }

    /**
     * Get f052fidStaff
     *
     * @return string
     */
    public function getF052fidStaff()
    {
        return $this->f052fidStaff;
    }

    /**
     * Set f052fidCash
     *
     * @param integer $f052fidCash
     *
     * @return T052fcashDeclaration
     */
    public function setF052fidCash($f052fidCash)
    {
        $this->f052fidCash = $f052fidCash;

        return $this;
    }

    /**
     * Get f052fidCash
     *
     * @return integer
     */
    public function getF052fidCash()
    {
        return $this->f052fidCash;
    }

    /**
     * Set f052fidDepartment
     *
     * @param string $f052fidDepartment
     *
     * @return T052fcashDeclaration
     */
    public function setF052fidDepartment($f052fidDepartment)
    {
        $this->f052fidDepartment = $f052fidDepartment;

        return $this;
    }

    /**
     * Get f052fidDepartment
     *
     * @return string
     */
    public function getF052fidDepartment()
    {
        return $this->f052fidDepartment;
    }

    /**
     * Set f052fidFinancialYear
     *
     * @param integer $f052fidFinancialYear
     *
     * @return T052fcashDeclaration
     */
    public function setF052fidFinancialYear($f052fidFinancialYear)
    {
        $this->f052fidFinancialYear = $f052fidFinancialYear;

        return $this;
    }

    /**
     * Get f052fidFinancialYear
     *
     * @return integer
     */
    public function getF052fidFinancialYear()
    {
        return $this->f052fidFinancialYear;
    }

    /**
     * Set f052freceiptAmount
     *
     * @param integer $f052freceiptAmount
     *
     * @return T052fcashDeclaration
     */
    public function setF052freceiptAmount($f052freceiptAmount)
    {
        $this->f052freceiptAmount = $f052freceiptAmount;

        return $this;
    }

    /**
     * Get f052freceiptAmount
     *
     * @return integer
     */
    public function getF052freceiptAmount()
    {
        return $this->f052freceiptAmount;
    }

    /**
     * Set f052freceiptNo
     *
     * @param integer $f052freceiptNo
     *
     * @return T052fcashDeclaration
     */
    public function setF052freceiptNo($f052freceiptNo)
    {
        $this->f052freceiptNo = $f052freceiptNo;

        return $this;
    }

    /**
     * Get f052freceiptNo
     *
     * @return integer
     */
    public function getF052freceiptNo()
    {
        return $this->f052freceiptNo;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052fcashDeclaration
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }

     /**
     * Set f052fapprovalStatus
     *
     * @param integer $f052fapprovalStatus
     *
     * @return T052fcashDeclaration
     */
    public function setF052fapprovalStatus($f052fapprovalStatus)
    {
        $this->f052fapprovalStatus = $f052fapprovalStatus;

        return $this;
    }

    /**
     * Get f052fapprovalStatus
     *
     * @return integer
     */
    public function getF052fapprovalStatus()
    {
        return $this->f052fapprovalStatus;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052fcashDeclaration
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052fcashDeclaration
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052fcashDeclaration
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052fcashDeclaration
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }
}
