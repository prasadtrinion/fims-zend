<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T099fstudentInsurance
 *
 * @ORM\Table(name="t099fstudent_insurance")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentInsuranceRepository")
 */
class T099fstudentInsurance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f099fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fname", type="string", length=25, nullable=false)
     */
    private $f099fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fins_code", type="string", length=20, nullable=false)
     */
    private $f099finsCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fcontact_person", type="string", length=20, nullable=false)
     */
    private $f099fcontactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fphone", type="string", length=20, nullable=false)
     */
    private $f099fphone;

    /**
     * @var string
     *
     * @ORM\Column(name="f099femail", type="string", length=25, nullable=true)
     */
    private $f099femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f099ffax", type="string", length=20, nullable=true)
     */
    private $f099ffax;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fend_date", type="datetime", nullable=true)
     */
    private $f099fendDate;


    /**
     * @var string
     *
     * @ORM\Column(name="f099faddress", type="string", length=100, nullable=true)
     */
    private $f099faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fcity", type="string", length=15, nullable=true)
     */
    private $f099fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fstate", type="integer", nullable=true)
     */
    private $f099fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fcountry", type="integer", nullable=true)
     */
    private $f099fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fcreated_by", type="integer", nullable=true)
     */
    private $f099fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fupdated_by", type="integer", nullable=true)
     */
    private $f099fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f099fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f099fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fstatus", type="integer", nullable=false)
     */
    private $f099fstatus = '1';



    /**
     * Get f099fid
     *
     * @return integer
     */
    public function getF099fid()
    {
        return $this->f099fid;
    }

    /**
     * Set f099fname
     *
     * @param string $f099fname
     *
     * @return T099fstudentInsurance
     */
    public function setF099fname($f099fname)
    {
        $this->f099fname = $f099fname;

        return $this;
    }

    /**
     * Get f099fname
     *
     * @return string
     */
    public function getF099fname()
    {
        return $this->f099fname;
    }

    /**
     * Set f099finsCode
     *
     * @param string $f099finsCode
     *
     * @return T099fstudentInsurance
     */
    public function setF099finsCode($f099finsCode)
    {
        $this->f099finsCode = $f099finsCode;

        return $this;
    }

    /**
     * Get f099finsCode
     *
     * @return string
     */
    public function getF099finsCode()
    {
        return $this->f099finsCode;
    }

    /**
     * Set f099fcontactPerson
     *
     * @param string $f099fcontactPerson
     *
     * @return T099fstudentInsurance
     */
    public function setF099fcontactPerson($f099fcontactPerson)
    {
        $this->f099fcontactPerson = $f099fcontactPerson;

        return $this;
    }

    /**
     * Get f099fcontactPerson
     *
     * @return string
     */
    public function getF099fcontactPerson()
    {
        return $this->f099fcontactPerson;
    }

    /**
     * Set f099fphone
     *
     * @param string $f099fphone
     *
     * @return T099fstudentInsurance
     */
    public function setF099fphone($f099fphone)
    {
        $this->f099fphone = $f099fphone;

        return $this;
    }

    /**
     * Get f099fphone
     *
     * @return string
     */
    public function getF099fphone()
    {
        return $this->f099fphone;
    }

    /**
     * Set f099femail
     *
     * @param string $f099femail
     *
     * @return T099fstudentInsurance
     */
    public function setF099femail($f099femail)
    {
        $this->f099femail = $f099femail;

        return $this;
    }

    /**
     * Get f099femail
     *
     * @return string
     */
    public function getF099femail()
    {
        return $this->f099femail;
    }

    /**
     * Set f099ffax
     *
     * @param string $f099ffax
     *
     * @return T099fstudentInsurance
     */
    public function setF099ffax($f099ffax)
    {
        $this->f099ffax = $f099ffax;

        return $this;
    }

    /**
     * Get f099ffax
     *
     * @return string
     */
    public function getF099ffax()
    {
        return $this->f099ffax;
    }

    /**
     * Set f099faddress
     *
     * @param string $f099faddress
     *
     * @return T099fstudentInsurance
     */
    public function setF099faddress($f099faddress)
    {
        $this->f099faddress = $f099faddress;

        return $this;
    }

    /**
     * Get f099faddress
     *
     * @return string
     */
    public function getF099faddress()
    {
        return $this->f099faddress;
    }

    /**
     * Set f099fcity
     *
     * @param string $f099fcity
     *
     * @return T099fstudentInsurance
     */
    public function setF099fcity($f099fcity)
    {
        $this->f099fcity = $f099fcity;

        return $this;
    }

    /**
     * Get f099fcity
     *
     * @return string
     */
    public function getF099fcity()
    {
        return $this->f099fcity;
    }

    /**
     * Set f099fstate
     *
     * @param integer $f099fstate
     *
     * @return T099fstudentInsurance
     */
    public function setF099fstate($f099fstate)
    {
        $this->f099fstate = $f099fstate;

        return $this;
    }

    /**
     * Get f099fstate
     *
     * @return integer
     */
    public function getF099fstate()
    {
        return $this->f099fstate;
    }

    /**
     * Set f099fcountry
     *
     * @param integer $f099fcountry
     *
     * @return T099fstudentInsurance
     */
    public function setF099fcountry($f099fcountry)
    {
        $this->f099fcountry = $f099fcountry;

        return $this;
    }

    /**
     * Get f099fcountry
     *
     * @return integer
     */
    public function getF099fcountry()
    {
        return $this->f099fcountry;
    }

    /**
     * Set f099fcreatedBy
     *
     * @param integer $f099fcreatedBy
     *
     * @return T099fstudentInsurance
     */
    public function setF099fcreatedBy($f099fcreatedBy)
    {
        $this->f099fcreatedBy = $f099fcreatedBy;

        return $this;
    }

    /**
     * Get f099fcreatedBy
     *
     * @return integer
     */
    public function getF099fcreatedBy()
    {
        return $this->f099fcreatedBy;
    }

    /**
     * Set f099fupdatedBy
     *
     * @param integer $f099fupdatedBy
     *
     * @return T099fstudentInsurance
     */
    public function setF099fupdatedBy($f099fupdatedBy)
    {
        $this->f099fupdatedBy = $f099fupdatedBy;

        return $this;
    }

    /**
     * Get f099fupdatedBy
     *
     * @return integer
     */
    public function getF099fupdatedBy()
    {
        return $this->f099fupdatedBy;
    }

    /**
     * Set f099fcreatedDtTm
     *
     * @param \DateTime $f099fcreatedDtTm
     *
     * @return T099fstudentInsurance
     */
    public function setF099fcreatedDtTm($f099fcreatedDtTm)
    {
        $this->f099fcreatedDtTm = $f099fcreatedDtTm;

        return $this;
    }

    /**
     * Get f099fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fcreatedDtTm()
    {
        return $this->f099fcreatedDtTm;
    }

    /**
     * Set f099fupdatedDtTm
     *
     * @param \DateTime $f099fupdatedDtTm
     *
     * @return T099fstudentInsurance
     */
    public function setF099fupdatedDtTm($f099fupdatedDtTm)
    {
        $this->f099fupdatedDtTm = $f099fupdatedDtTm;

        return $this;
    }

    /**
     * Get f099fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fupdatedDtTm()
    {
        return $this->f099fupdatedDtTm;
    }

    /**
     * Set f099fstatus
     *
     * @param integer $f099fstatus
     *
     * @return T099fstudentInsurance
     */
    public function setF099fstatus($f099fstatus)
    {
        $this->f099fstatus = $f099fstatus;

        return $this;
    }

    /**
     * Get f099fstatus
     *
     * @return integer
     */
    public function getF099fstatus()
    {
        return $this->f099fstatus;
    }

    /**
     * Set f099fendDate
     *
     * @param \DateTime $f099fendDate
     *
     * @return T099ftaggingSponsors
     */
    public function setF099fendDate($f099fendDate)
    {
        $this->f099fendDate = $f099fendDate;

        return $this;
    }

    /**
     * Get f099fendDate
     *
     * @return \DateTime
     */
    public function getF099fendDate()
    {
        return $this->f099fendDate;
    }

}

