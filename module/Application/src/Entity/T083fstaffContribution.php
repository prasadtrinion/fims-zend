<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T083fstaffContribution
 *
 * @ORM\Table(name="t083fstaff_contribution", uniqueConstraints={@ORM\UniqueConstraint(name="f083fid_staff_UNIQUE", columns={"f083fid_staff"})})
 * @ORM\Entity(repositoryClass="Application\Repository\StaffContributionRepository")
 */
class T083fstaffContribution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f083fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f083fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f083fid_staff", type="string", length=50, nullable=true)
     */
    private $f083fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f083ffund_code", type="string", length=100, nullable=true)
     */
    private $f083ffundCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f083fdepartment_code", type="string", length=100, nullable=true)
     */
    private $f083fdepartmentCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f083factivity_code", type="string", length=100, nullable=true)
     */
    private $f083factivityCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fepf_deductable", type="integer", nullable=true)
     */
    private $f083fepfDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fsocso_deductable", type="integer", nullable=true)
     */
    private $f083fsocsoDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083ftax_deductable", type="integer", nullable=true)
     */
    private $f083ftaxDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083femployee", type="integer", nullable=true)
     */
    private $f083femployee;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083femployer", type="integer", nullable=true)
     */
    private $f083femployer;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fstatus", type="integer", nullable=true)
     */
    private $f083fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fcreated_by", type="integer", nullable=true)
     */
    private $f083fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f083fupdated_by", type="integer", nullable=true)
     */
    private $f083fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f083fcreated_dt_tm", type="date", nullable=true)
     */
    private $f083fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f083fupdated_dt_tm", type="date", nullable=true)
     */
    private $f083fupdatedDtTm;



    /**
     * Get f083fid
     *
     * @return integer
     */
    public function getF083fid()
    {
        return $this->f083fid;
    }


    /**
     * Set f083fidStaff
     *
     * @param string $f083fidStaff
     *
     * @return T083fstaffContribution
     */
    public function setF083fidStaff($f083fidStaff)
    {
        $this->f083fidStaff = $f083fidStaff;

        return $this;
    }

    /**
     * Get f083fidStaff
     *
     * @return string
     */
    public function getF083fidStaff()
    {
        return $this->f083fidStaff;
    }

    /**
     * Set f083ffundCode
     *
     * @param string $f083ffundCode
     *
     * @return T083fstaffContribution
     */
    public function setF083ffundCode($f083ffundCode)
    {
        $this->f083ffundCode = $f083ffundCode;

        return $this;
    }

    /**
     * Get f083ffundCode
     *
     * @return string
     */
    public function getF083ffundCode()
    {
        return $this->f083ffundCode;
    }

    /**
     * Set f083fdepartmentCode
     *
     * @param string $f083fdepartmentCode
     *
     * @return T083fstaffContribution
     */
    public function setF083fdepartmentCode($f083fdepartmentCode)
    {
        $this->f083fdepartmentCode = $f083fdepartmentCode;

        return $this;
    }

    /**
     * Get f083fdepartmentCode
     *
     * @return string
     */
    public function getF083fdepartmentCode()
    {
        return $this->f083fdepartmentCode;
    }

    /**
     * Set f083factivityCode
     *
     * @param string $f083factivityCode
     *
     * @return T083fstaffContribution
     */
    public function setF083factivityCode($f083factivityCode)
    {
        $this->f083factivityCode = $f083factivityCode;

        return $this;
    }

    /**
     * Get f083factivityCode
     *
     * @return string
     */
    public function getF083factivityCode()
    {
        return $this->f083factivityCode;
    }

     /**
     * Set f083fepfDeductable
     *
     * @param integer $f083fepfDeductable
     *
     * @return T083fstaffContribution
     */
    public function setF083fepfDeductable($f083fepfDeductable)
    {
        $this->f083fepfDeductable = $f083fepfDeductable;

        return $this;
    }

    /**
     * Get f083fepfDeductable
     *
     * @return integer
     */
    public function getF083fepfDeductable()
    {
        return $this->f083fepfDeductable;
    }

    /**
     * Set f083fsocsoDeductable
     *
     * @param integer $f083fsocsoDeductable
     *
     * @return T083fstaffContribution
     */
    public function setF083fsocsoDeductable($f083fsocsoDeductable)
    {
        $this->f083fsocsoDeductable = $f083fsocsoDeductable;

        return $this;
    }

    /**
     * Get f083fsocsoDeductable
     *
     * @return integer
     */
    public function getF083fsocsoDeductable()
    {
        return $this->f083fsocsoDeductable;
    }

    /**
     * Set f083ftaxDeductable
     *
     * @param integer $f083ftaxDeductable
     *
     * @return T083fstaffContribution
     */
    public function setF083ftaxDeductable($f083ftaxDeductable)
    {
        $this->f083ftaxDeductable = $f083ftaxDeductable;

        return $this;
    }

    /**
     * Get f083ftaxDeductable
     *
     * @return integer
     */
    public function getF083ftaxDeductable()
    {
        return $this->f083ftaxDeductable;
    }

    /**
     * Set f083femployee
     *
     * @param integer $f083femployee
     *
     * @return T083fstaffContribution
     */
    public function setF083femployee($f083femployee)
    {
        $this->f083femployee = $f083femployee;

        return $this;
    }

    /**
     * Get f083femployee
     *
     * @return integer
     */
    public function getF083femployee()
    {
        return $this->f083femployee;
    }

    /**
     * Set f083femployer
     *
     * @param integer $f083femployer
     *
     * @return T083fstaffContribution
     */
    public function setF083femployer($f083femployer)
    {
        $this->f083femployer = $f083femployer;

        return $this;
    }

    /**
     * Get f083femployer
     *
     * @return integer
     */
    public function getF083femployer()
    {
        return $this->f083femployer;
    }

    /**
     * Set f083fstatus
     *
     * @param integer $f083fstatus
     *
     * @return T083fstaffContribution
     */
    public function setF083fstatus($f083fstatus)
    {
        $this->f083fstatus = $f083fstatus;

        return $this;
    }

    /**
     * Get f083fstatus
     *
     * @return integer
     */
    public function getF083fstatus()
    {
        return $this->f083fstatus;
    }

    /**
     * Set f083fcreatedBy
     *
     * @param integer $f083fcreatedBy
     *
     * @return T083fstaffContribution
     */
    public function setF083fcreatedBy($f083fcreatedBy)
    {
        $this->f083fcreatedBy = $f083fcreatedBy;

        return $this;
    }

    /**
     * Get f083fcreatedBy
     *
     * @return integer
     */
    public function getF083fcreatedBy()
    {
        return $this->f083fcreatedBy;
    }

    /**
     * Set f083fupdatedBy
     *
     * @param integer $f083fupdatedBy
     *
     * @return T083fstaffContribution
     */
    public function setF083fupdatedBy($f083fupdatedBy)
    {
        $this->f083fupdatedBy = $f083fupdatedBy;

        return $this;
    }

    /**
     * Get f083fupdatedBy
     *
     * @return integer
     */
    public function getF083fupdatedBy()
    {
        return $this->f083fupdatedBy;
    }

    /**
     * Set f083fcreatedDtTm
     *
     * @param \DateTime $f083fcreatedDtTm
     *
     * @return T083fstaffContribution
     */
    public function setF083fcreatedDtTm($f083fcreatedDtTm)
    {
        $this->f083fcreatedDtTm = $f083fcreatedDtTm;

        return $this;
    }

    /**
     * Get f083fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF083fcreatedDtTm()
    {
        return $this->f083fcreatedDtTm;
    }

    /**
     * Set f083fupdatedDtTm
     *
     * @param \DateTime $f083fupdatedDtTm
     *
     * @return T083fstaffContribution
     */
    public function setF083fupdatedDtTm($f083fupdatedDtTm)
    {
        $this->f083fupdatedDtTm = $f083fupdatedDtTm;

        return $this;
    }

    /**
     * Get f083fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF083fupdatedDtTm()
    {
        return $this->f083fupdatedDtTm;
    }
}
