<?php
 
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T085fassetInformationHistory
 *
 * @ORM\Table(name="t085fasset_information_history")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetInformationRepository")
 */
class T085fassetInformationHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f085fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fasset_code", type="string", length=225, nullable=true)
     */
    private $f085fassetCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fasset_description", type="string", length=50, nullable=true)
     */
    private $f085fassetDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_department", type="string",length=20, nullable=true)
     */
    private $f085fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_asset_category", type="integer", nullable=true)
     */
    private $f085fidAssetCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085f_acquisition_date", type="datetime", nullable=true)
     */
    private $f085fAcquisitionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f085forder_number", type="string", length=20, nullable=true)
     */
    private $f085forderNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fstore_code", type="string", length=20, nullable=true)
     */
    private $f085fstoreCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f085faccount_code", type="string", length=20, nullable=true)
     */
    private $f085faccountCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fapprove_date", type="datetime", nullable=true)
     */
    private $f085fapproveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fexpire_date", type="datetime", nullable=true)
     */
    private $f085fexpireDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_asset_type", type="integer", nullable=true)
     */
    private $f085fidAssetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085finstall_cost", type="integer", nullable=true)
     */
    private $f085finstallCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085faccum_depr_cost",  type="integer", nullable=true)
     */
    private $f085faccumDeprCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fnet_value",  type="integer", nullable=true)
     */
    private $f085fnetValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fyear_depr_cost",  type="integer", nullable=true)
     */
    private $f085fyearDeprCost;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fresidual_value",  type="integer", nullable=true)
     */
    private $f085fresidualValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fstatus", type="integer", nullable=true)
     */
    private $f085fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcreated_by", type="integer", nullable=true)
     */
    private $f085fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fupdated_by", type="integer", nullable=true)
     */
    private $f085fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f085fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f085fupdatedDtTm;

  /**
     * @var integer
     *
     * @ORM\Column(name="f085fasset_owner", type="string",length=20, nullable=true)
     */
    private $f085fassetOwner;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fbar_code", type="string", length=20, nullable=true)
     */
    private $f085fbarCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fserial_number", type="string", length=20, nullable=true)
     */
    private $f085fserialNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fbrand_name", type="string", length=20, nullable=true)
     */
    private $f085fbrandName;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fcurrent_building", type="integer", nullable=true)
     */
    private $f085fcurrentBuilding;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fcurrent_room", type="integer", nullable=true)
     */
    private $f085fcurrentRoom;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fcurrent_block", type="integer", nullable=true)
     */
    private $f085fcurrentBlock;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fcurrent_campus", type="integer", nullable=true)
     */
    private $f085fcurrentCampus;

     /**
     * @var string
     *
     * @ORM\Column(name="f085fsub_category", type="integer", nullable=true)
     */
    private $f085fsubCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fpo_number", type="string",length=50, nullable=true)
     */
    private $f085fpoNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fgrn_number", type="string",length=50, nullable=true)
     */
    private $f085fgrnNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fso_code", type="string",length=50, nullable=true)
     */
    private $f085fsoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f085freferrer", type="string",length=50, nullable=true)
     */
    private $f085freferrer;

     /**
     * @var string
     *
     * @ORM\Column(name="f085fitem_name", type="string",length=50, nullable=true)
     */
    private $f085fitemName;

    /**
     * Get f085fid
     *
     * @return integer
     */
    public function getF085fid()
    {
        return $this->f085fid;
    }

    /**
     * Set f085fassetCode
     *
     * @param string $f085fassetCode
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fassetCode($f085fassetCode)
    {
        $this->f085fassetCode = $f085fassetCode;

        return $this;
    }

    /**
     * Get f085fassetCode
     *
     * @return string
     */
    public function getF085fassetCode()
    {
        return $this->f085fassetCode;
    }

    /**
     * Set f085fassetDescription
     *
     * @param string $f085fassetDescription
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fassetDescription($f085fassetDescription)
    {
        $this->f085fassetDescription = $f085fassetDescription;

        return $this;
    }

    /**
     * Get f085fassetDescription
     *
     * @return string
     */
    public function getF085fassetDescription()
    {
        return $this->f085fassetDescription;
    }

    /**
     * Set f085fidDepartment
     *
     * @param integer $f085fidDepartment
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fidDepartment($f085fidDepartment)
    {
        $this->f085fidDepartment = $f085fidDepartment;

        return $this;
    }

    /**
     * Get f085fidDepartment
     *
     * @return integer
     */
    public function getF085fidDepartment()
    {
        return $this->f085fidDepartment;
    }

    /**
     * Set f085fidAssetCategory
     *
     * @param integer $f085fidAssetCategory
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fidAssetCategory($f085fidAssetCategory)
    {
        $this->f085fidAssetCategory = $f085fidAssetCategory;

        return $this;
    }

    /**
     * Get f085fidAssetCategory
     *
     * @return integer
     */
    public function getF085fidAssetCategory()
    {
        return $this->f085fidAssetCategory;
    }

    /**
     * Set f085fAcquisitionDate
     *
     * @param \DateTime $f085fAcquisitionDate
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fAcquisitionDate($f085fAcquisitionDate)
    {
        $this->f085fAcquisitionDate = $f085fAcquisitionDate;

        return $this;
    }

    /**
     * Get f085fAcquisitionDate
     *
     * @return \DateTime
     */
    public function getF085fAcquisitionDate()
    {
        return $this->f085fAcquisitionDate;
    }

    /**
     * Set f085forderNumber
     *
     * @param string $f085forderNumber
     *
     * @return T085fassetInformationHistory
     */
    public function setF085forderNumber($f085forderNumber)
    {
        $this->f085forderNumber = $f085forderNumber;

        return $this;
    }

    /**
     * Get f085forderNumber
     *
     * @return string
     */
    public function getF085forderNumber()
    {
        return $this->f085forderNumber;
    }

    /**
     * Set f085fstoreCode
     *
     * @param string $f085fstoreCode
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fstoreCode($f085fstoreCode)
    {
        $this->f085fstoreCode = $f085fstoreCode;

        return $this;
    }

    /**
     * Get f085fstoreCode
     *
     * @return string
     */
    public function getF085fstoreCode()
    {
        return $this->f085fstoreCode;
    }

    /**
     * Set f085faccountCode
     *
     * @param string $f085faccountCode
     *
     * @return T085fassetInformationHistory
     */
    public function setF085faccountCode($f085faccountCode)
    {
        $this->f085faccountCode = $f085faccountCode;

        return $this;
    }

    /**
     * Get f085faccountCode
     *
     * @return string
     */
    public function getF085faccountCode()
    {
        return $this->f085faccountCode;
    }

    /**
     * Set f085fapproveDate
     *
     * @param \DateTime $f085fapproveDate
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fapproveDate($f085fapproveDate)
    {
        $this->f085fapproveDate = $f085fapproveDate;

        return $this;
    }

    /**
     * Get f085fapproveDate
     *
     * @return \DateTime
     */
    public function getF085fapproveDate()
    {
        return $this->f085fapproveDate;
    }

    /**
     * Set f085fexpireDate
     *
     * @param \DateTime $f085fexpireDate
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fexpireDate($f085fexpireDate)
    {
        $this->f085fexpireDate = $f085fexpireDate;

        return $this;
    }

    /**
     * Get f085fexpireDate
     *
     * @return \DateTime
     */
    public function getF085fexpireDate()
    {
        return $this->f085fexpireDate;
    }

    /**
     * Set f085fidAssetType
     *
     * @param integer $f085fidAssetType
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fidAssetType($f085fidAssetType)
    {
        $this->f085fidAssetType = $f085fidAssetType;

        return $this;
    }

    /**
     * Get f085fidAssetType
     *
     * @return integer
     */
    public function getF085fidAssetType()
    {
        return $this->f085fidAssetType;
    }

    /**
     * Set f085finstallCost
     *
     * @param string $f085finstallCost
     *
     * @return T085fassetInformationHistory
     */
    public function setF085finstallCost($f085finstallCost)
    {
        $this->f085finstallCost = $f085finstallCost;

        return $this;
    }

    /**
     * Get f085finstallCost
     *
     * @return string
     */
    public function getF085finstallCost()
    {
        return $this->f085finstallCost;
    }

    /**
     * Set f085faccumDeprCost
     *
     * @param string $f085faccumDeprCost
     *
     * @return T085fassetInformationHistory
     */
    public function setF085faccumDeprCost($f085faccumDeprCost)
    {
        $this->f085faccumDeprCost = $f085faccumDeprCost;

        return $this;
    }

    /**
     * Get f085faccumDeprCost
     *
     * @return string
     */
    public function getF085faccumDeprCost()
    {
        return $this->f085faccumDeprCost;
    }

    /**
     * Set f085fnetValue
     *
     * @param string $f085fnetValue
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fnetValue($f085fnetValue)
    {
        $this->f085fnetValue = $f085fnetValue;

        return $this;
    }

    /**
     * Get f085fnetValue
     *
     * @return string
     */
    public function getF085fnetValue()
    {
        return $this->f085fnetValue;
    }

    /**
     * Set f085fyearDeprCost
     *
     * @param string $f085fyearDeprCost
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fyearDeprCost($f085fyearDeprCost)
    {
        $this->f085fyearDeprCost = $f085fyearDeprCost;

        return $this;
    }

    /**
     * Get f085fyearDeprCost
     *
     * @return string
     */
    public function getF085fyearDeprCost()
    {
        return $this->f085fyearDeprCost;
    }

    /**
     * Set f085fresidualValue
     *
     * @param string $f085fresidualValue
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fresidualValue($f085fresidualValue)
    {
        $this->f085fresidualValue = $f085fresidualValue;

        return $this;
    }

    /**
     * Get f085fresidualValue
     *
     * @return string
     */
    public function getF085fresidualValue()
    {
        return $this->f085fresidualValue;
    }

    /**
     * Set f085fstatus
     *
     * @param integer $f085fstatus
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fstatus($f085fstatus)
    {
        $this->f085fstatus = $f085fstatus;

        return $this;
    }

    /**
     * Get f085fstatus
     *
     * @return integer
     */
    public function getF085fstatus()
    {
        return $this->f085fstatus;
    }

    /**
     * Set f085fcreatedBy
     *
     * @param integer $f085fcreatedBy
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fcreatedBy($f085fcreatedBy)
    {
        $this->f085fcreatedBy = $f085fcreatedBy;

        return $this;
    }

    /**
     * Get f085fcreatedBy
     *
     * @return integer
     */
    public function getF085fcreatedBy()
    {
        return $this->f085fcreatedBy;
    }

    /**
     * Set f085fupdatedBy
     *
     * @param integer $f085fupdatedBy
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fupdatedBy($f085fupdatedBy)
    {
        $this->f085fupdatedBy = $f085fupdatedBy;

        return $this;
    }

    /**
     * Get f085fupdatedBy
     *
     * @return integer
     */
    public function getF085fupdatedBy()
    {
        return $this->f085fupdatedBy;
    }

    /**
     * Set f085fcreatedDtTm
     *
     * @param \DateTime $f085fcreatedDtTm
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fcreatedDtTm($f085fcreatedDtTm)
    {
        $this->f085fcreatedDtTm = $f085fcreatedDtTm;

        return $this;
    }

    /**
     * Get f085fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF085fcreatedDtTm()
    {
        return $this->f085fcreatedDtTm;
    }

    /**
     * Set f085fupdatedDtTm
     *
     * @param \DateTime $f085fupdatedDtTm
     *
     * @return T085fassetInformationHistory
     */
    public function setF085fupdatedDtTm($f085fupdatedDtTm)
    {
        $this->f085fupdatedDtTm = $f085fupdatedDtTm;

        return $this;
    }

    /**
     * Get f085fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF085fupdatedDtTm()
    {
        return $this->f085fupdatedDtTm;
    }
    /**
     * Set f085fassetOwner
     *
     * @param integer $f085fassetOwner
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fassetOwner($f085fassetOwner)
    {
        $this->f085fassetOwner = $f085fassetOwner;

        return $this;
    }

    /**
     * Get f085fassetOwner
     *
     * @return integer
     */
    public function getF085fassetOwner()
    {
        return $this->f085fassetOwner;
    }

    /**
     * Set f085fbarCode
     *
     * @param string $f085fbarCode
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fbarCode($f085fbarCode)
    {
        $this->f085fbarCode = $f085fbarCode;

        return $this;
    }

    /**
     * Get f085fbarCode
     *
     * @return string
     */
    public function getF085fbarCode()
    {
        return $this->f085fbarCode;
    }

    /**
     * Set f085fserialNumber
     *
     * @param string $f085fserialNumber
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fserialNumber($f085fserialNumber)
    {
        $this->f085fserialNumber = $f085fserialNumber;

        return $this;
    }

    /**
     * Get f085fserialNumber
     *
     * @return string
     */
    public function getF085fserialNumber()
    {
        return $this->f085fserialNumber;
    }

    /**
     * Set f085fbrandName
     *
     * @param string $f085fbrandName
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fbrandName($f085fbrandName)
    {
        $this->f085fbrandName = $f085fbrandName;

        return $this;
    }

    /**
     * Get f085fbrandName
     *
     * @return string
     */
    public function getF085fbrandName()
    {
        return $this->f085fbrandName;
    }

    /**
     * Set f085fcurrentBuilding
     *
     * @param string $f085fcurrentBuilding
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcurrentBuilding($f085fcurrentBuilding)
    {
        $this->f085fcurrentBuilding = $f085fcurrentBuilding;

        return $this;
    }

    /**
     * Get f085fcurrentBuilding
     *
     * @return string
     */
    public function getF085fcurrentBuilding()
    {
        return $this->f085fcurrentBuilding;
    }

    /**
     * Set f085fcurrentRoom
     *
     * @param string $f085fcurrentRoom
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcurrentRoom($f085fcurrentRoom)
    {
        $this->f085fcurrentRoom = $f085fcurrentRoom;

        return $this;
    }

    /**
     * Get f085fcurrentRoom
     *
     * @return string
     */
    public function getF085fcurrentRoom()
    {
        return $this->f085fcurrentRoom;
    }

     /**
     * Set f085fcurrentBlock
     *
     * @param string $f085fcurrentBlock
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcurrentBlock($f085fcurrentBlock)
    {
        $this->f085fcurrentBlock = $f085fcurrentBlock;

        return $this;
    }

    /**
     * Get f085fcurrentBlock
     *
     * @return string
     */
    public function getF085fcurrentBlock()
    {
        return $this->f085fcurrentBlock;
    }

    /**
     * Set f085fcurrentCampus
     *
     * @param string $f085fcurrentCampus
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcurrentCampus($f085fcurrentCampus)
    {
        $this->f085fcurrentCampus = $f085fcurrentCampus;

        return $this;
    }

    /**
     * Get f085fcurrentCampus
     *
     * @return string
     */
    public function getF085fcurrentCampus()
    {
        return $this->f085fcurrentCampus;
    }

    /**
     * Set f085fsubCategory
     *
     * @param string $f085fsubCategory
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fsubCategory($f085fsubCategory)
    {
        $this->f085fsubCategory = $f085fsubCategory;

        return $this;
    }

    /**
     * Get f085fsubCategory
     *
     * @return string
     */
    public function getF085fsubCategory()
    {
        return $this->f085fsubCategory;
    }
   
    /**
     * Set f085fpoNumber
     *
     * @param string $f085fpoNumber
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fpoNumber($f085fpoNumber)
    {
        $this->f085fpoNumber = $f085fpoNumber;

        return $this;
    }

    /**
     * Get f085fpoNumber
     *
     * @return string
     */
    public function getF085fpoNumber()
    {
        return $this->f085fpoNumber;
    }
    /**
     * Set f085fgrnNumber
     *
     * @param string $f085fgrnNumber
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fgrnNumber($f085fgrnNumber)
    {
        $this->f085fgrnNumber = $f085fgrnNumber;

        return $this;
    }

    /**
     * Get f085fgrnNumber
     *
     * @return string
     */
    public function getF085fgrnNumber()
    {
        return $this->f085fgrnNumber;
    }
    /**
     * Set f085fsoCode
     *
     * @param string $f085fsoCode
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fsoCode($f085fsoCode)
    {
        $this->f085fsoCode = $f085fsoCode;

        return $this;
    }

    /**
     * Get f085fsoCode
     *
     * @return string
     */
    public function getF085fsoCode()
    {
        return $this->f085fsoCode;
    }
    
    /**
     * Set f085freferrer
     *
     * @param string $f085freferrer
     *
     * @return T085fassetInfoDetails
     */
    public function setF085freferrer($f085freferrer)
    {
        $this->f085freferrer = $f085freferrer;

        return $this;
    }

    /**
     * Get f085freferrer
     *
     * @return string
     */
    public function getF085freferrer()
    {
        return $this->f085freferrer;
    }

    /**
     * Set f085fitemName
     *
     * @param string $f085fitemName
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fitemName($f085fitemName)
    {
        $this->f085fitemName = $f085fitemName;

        return $this;
    }

    /**
     * Get f085fitemName
     *
     * @return string
     */
    public function getF085fitemName()
    {
        return $this->f085fitemName;
    }
}
