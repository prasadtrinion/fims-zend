<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T123fpattyCashEntry
 *
 * @ORM\Table(name="t123fpatty_cash_entry")
 * @ORM\Entity(repositoryClass="Application\Repository\PattyCashEntryRepository")
 */
class T123fpattyCashEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f123fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f123fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f123fdescription", type="string",length=255, nullable=true)
     */
    private $f123fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f123frequested_date", type="datetime", nullable=true)
     */
    private $f123frequestedDate;

    /** 
     * @var string
     *
     * @ORM\Column(name="f123fdepartment_code", type="string",length=50, nullable=true)
     */
    private $f123fdepartmentCode;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f123famount", type="integer", nullable=true)
     */
    private $f123famount;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f123ftransaction_type", type="integer", nullable=true)
     */
    private $f123ftransactionType;

    /** 
     * @var string
     *
     * @ORM\Column(name="f123freason", type="string",length=50, nullable=true)
     */
    private $f123freason;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f123freemberasment", type="integer", nullable=true)
     */
    private $f123freemberasment;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f123fstatus", type="integer", nullable=true)
     */
    private $f123fstatus;


    /** 
     * @var integer
     *
     * @ORM\Column(name="f123fid_allocation", type="integer", nullable=true)
     */
    private $f123fidAllocation;

    /**
     * @var integer
     *
     * @ORM\Column(name="f123fcreated_by", type="integer", nullable=true)
     */
    private $f123fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f123fupdated_by", type="integer", nullable=true)
     */
    private $f123fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f123fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f123fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f123fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f123fupdatedDtTm;



    /**
     * Get f123fid
     *
     * @return integer
     */
    public function getF123fid()
    {
        return $this->f123fid;
    }

    /**
     * Set f123fdescription
     *
     * @param \DateTime $f123fdescription
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fdescription($f123fdescription)
    {
        $this->f123fdescription = $f123fdescription;

        return $this;
    }

    /**
     * Get f123fdescription
     *
     * @return \DateTime
     */
    public function getF123fdescription()
    {
        return $this->f123fdescription;
    }

    /**
     * Set f123frequestedDate
     *
     * @param integer $f123frequestedDate
     *
     * @return T123fpattyCashEntry
     */
    public function setF123frequestedDate($f123frequestedDate)
    {
        $this->f123frequestedDate = $f123frequestedDate;

        return $this;
    }

    /**
     * Get f123frequestedDate
     *
     * @return integer
     */
    public function getF123frequestedDate()
    {
        return $this->f123frequestedDate;
    }

    /**
     * Set f123fdepartmentCode
     *
     * @param integer $f123fdepartmentCode
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fdepartmentCode($f123fdepartmentCode)
    {
        $this->f123fdepartmentCode = $f123fdepartmentCode;

        return $this;
    }

    /**
     * Get f123fdepartmentCode
     *
     * @return integer
     */
    public function getF123fdepartmentCode()
    {
        return $this->f123fdepartmentCode;
    }

    /**
     * Set f123famount
     *
     * @param integer $f123famount
     *
     * @return T123fpattyCashEntry
     */
    public function setF123famount($f123famount)
    {
        $this->f123famount = $f123famount;

        return $this;
    }

    /**
     * Get f123famount
     *
     * @return integer
     */
    public function getF123famount()
    {
        return $this->f123famount;
    }

    /**
     * Set f123ftransactionType
     *
     * @param integer $f123ftransactionType
     *
     * @return T123fpattyCashEntry
     */
    public function setF123ftransactionType($f123ftransactionType)
    {
        $this->f123ftransactionType = $f123ftransactionType;

        return $this;
    }

    /**
     * Get f123ftransactionType
     *
     * @return integer
     */
    public function getF123ftransactionType()
    {
        return $this->f123ftransactionType;
    }

    /**
     * Set f123freason
     *
     * @param integer $f123freason
     *
     * @return T123fpattyCashEntry
     */
    public function setF123freason($f123freason)
    {
        $this->f123freason = $f123freason;

        return $this;
    }

    /**
     * Get f123freason
     *
     * @return integer
     */
    public function getF123freason()
    {
        return $this->f123freason;
    }
    

    /**
     * Set f123fstatus
     *
     * @param integer $f123fstatus
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fstatus($f123fstatus)
    {
        $this->f123fstatus = $f123fstatus;

        return $this;
    }

    /**
     * Get f123fstatus
     *
     * @return integer
     */
    public function getF123fstatus()
    {
        return $this->f123fstatus;
    }

    /**
     * Set f123freemberasment
     *
     * @param integer $f123freemberasment
     *
     * @return T123fpattyCashEntry
     */
    public function setF123freemberasment($f123freemberasment)
    {
        $this->f123freemberasment = $f123freemberasment;

        return $this;
    }

    /**
     * Get f123freemberasment
     *
     * @return integer
     */
    public function getF123freemberasment()
    {
        return $this->f123freemberasment;
    }


    /**
     * Set f123fidAllocation
     *
     * @param integer $f123fidAllocation
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fidAllocation($f123fidAllocation)
    {
        $this->f123fidAllocation = $f123fidAllocation;

        return $this;
    }

    /**
     * Get f123fidAllocation
     *
     * @return integer
     */
    public function getF123fidAllocation()
    {
        return $this->f123fidAllocation;
    }

    /**
     * Set f123fcreatedBy
     *
     * @param integer $f123fcreatedBy
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fcreatedBy($f123fcreatedBy)
    {
        $this->f123fcreatedBy = $f123fcreatedBy;

        return $this;
    }

    /**
     * Get f123fcreatedBy
     *
     * @return integer
     */
    public function getF123fcreatedBy()
    {
        return $this->f123fcreatedBy;
    }

    /**
     * Set f123fupdatedBy
     *
     * @param integer $f123fupdatedBy
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fupdatedBy($f123fupdatedBy)
    {
        $this->f123fupdatedBy = $f123fupdatedBy;

        return $this;
    }

    /**
     * Get f123fupdatedBy
     *
     * @return integer
     */
    public function getF123fupdatedBy()
    {
        return $this->f123fupdatedBy;
    }

    /**
     * Set f123fcreatedDtTm
     *
     * @param \DateTime $f123fcreatedDtTm
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fcreatedDtTm($f123fcreatedDtTm)
    {
        $this->f123fcreatedDtTm = $f123fcreatedDtTm;

        return $this;
    }

    /**
     * Get f123fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF123fcreatedDtTm()
    {
        return $this->f123fcreatedDtTm;
    }

    /**
     * Set f123fupdatedDtTm
     *
     * @param \DateTime $f123fupdatedDtTm
     *
     * @return T123fpattyCashEntry
     */
    public function setF123fupdatedDtTm($f123fupdatedDtTm)
    {
        $this->f123fupdatedDtTm = $f123fupdatedDtTm;

        return $this;
    }

    /**
     * Get f123fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF123fupdatedDtTm()
    {
        return $this->f123fupdatedDtTm;
    }
}
