<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T078fonlineBankPayment
 *
 * @ORM\Table(name="t078fonline_bank_payment")
 * @ORM\Entity(repositoryClass="Application\Repository\OnlineBankPaymentRepository")
 */
class T078fonlineBankPayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f078fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid_bank", type="integer", nullable=false)
     */
    private $f078fidBank;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fbill_type", type="string",length=50, nullable=false)
     */
    private $f078fbillType;

    /**
     * @var string
     *
     * @ORM\Column(name="f078ffile_name", type="string",length=50, nullable=false)
     */
    private $f078ffileName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078ftype", type="string",length=50, nullable=false)
     */
    private $f078ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fmatric_number", type="string",length=50, nullable=false)
     */
    private $f078fmatricNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078ftransaction_date", type="datetime", nullable=false)
     */
    private $f078transactionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fdate", type="datetime", nullable=false)
     */
    private $f078fdate;

    /**
     * @var string
     *
     * @ORM\Column(name="f078freference_number", type="string", length=20, nullable=false)
     */
    private $f078freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f078freference_number1", type="string", length=20, nullable=false)
     */
    private $f078freferenceNumber1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078famount", type="integer", nullable=false)
     */
    private $f078famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fic_number", type="string", length=20, nullable=false)
     */
    private $f078ficNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fstatus", type="integer", nullable=false)
     */
    private $f078fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fcreated_by", type="integer", nullable=false)
     */
    private $f078fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fupdated_by", type="integer", nullable=false)
     */
    private $f078fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fupdatedDtTm;

	

    /**
     * Get f078fid
     *
     * @return integer
     */
    public function getF078fid()
    {
        return $this->f078fid;
    }

    /**
     * Set f078fbillType
     *
     * @param integer $f078fbillType
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fbillType($f078fbillType)
    {
        $this->f078fbillType = $f078fbillType;

        return $this;
    }

     /**
     * Get f078fbillType
     *
     * @return integer
     */
    public function getF078fbillType()
    {
        return $this->f078fbillType;
    }

    /**
     * Set f078ffileName
     *
     * @param integer $f078ffileName
     *
     * @return T078fonlineBankPayment
     */
    public function setF078ffileName($f078ffileName)
    {
        $this->f078ffileName = $f078ffileName;

        return $this;
    }

     /**
     * Get f078ffileName
     *
     * @return integer
     */
    public function getF078ffileName()
    {
        return $this->f078ffileName;
    }

    /**
     * Get f078ftype
     *
     * @return integer
     */
    public function getF078ftype()
    {
        return $this->f078ftype;
    }

     /**
     * Set f078ftype
     *
     * @param integer $f078ftype
     *
     * @return T078fonlineBankPayment
     */
    public function setF078ftype($f078ftype)
    {
        $this->f078ftype = $f078ftype;

        return $this;
    }

   

    /**
     * Set f078fmatricNumber
     *
     * @param integer $f078fmatricNumber
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fmatricNumber($f078fmatricNumber)
    {
        $this->f078fmatricNumber = $f078fmatricNumber;

        return $this;
    }

    /**
     * Get f078fmatricNumber
     *
     * @return integer
     */
    public function getF078fmatricNumber()
    {
        return $this->f078fmatricNumber;
    }
    /**
     * Set f078fidBank
     *
     * @param integer $f078fidBank
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fidBank($f078fidBank)
    {
        $this->f078fidBank = $f078fidBank;

        return $this;
    }

    /**
     * Get f078fidBank
     *
     * @return integer
     */
    public function getF078fidBank()
    {
        return $this->f078fidBank;
    }



    /**
     * Set f078fstatus
     *
     * @param integer $f078fstatus
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fstatus($f078fstatus)
    {
        $this->f078fstatus = $f078fstatus;

        return $this;
    }

    /**
     * Get f078fstatus
     *
     * @return integer
     */
    public function getF078fstatus()
    {
        return $this->f078fstatus;
    }

    /**
     * Set f078fcreatedBy
     *
     * @param integer $f078fcreatedBy
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fcreatedBy($f078fcreatedBy)
    {
        $this->f078fcreatedBy = $f078fcreatedBy;

        return $this;
    }

    /**
     * Get f078fcreatedBy
     *
     * @return integer
     */
    public function getF078fcreatedBy()
    {
        return $this->f078fcreatedBy;
    }

    /**
     * Set f078fupdatedBy
     *
     * @param integer $f078fupdatedBy
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fupdatedBy($f078fupdatedBy)
    {
        $this->f078fupdatedBy = $f078fupdatedBy;

        return $this;
    }

    /**
     * Get f078fupdatedBy
     *
     * @return integer
     */
    public function getF078fupdatedBy()
    {
        return $this->f078fupdatedBy;
    }

    /**
     * Set f078fcreatedDtTm
     *
     * @param \DateTime $f078fcreatedDtTm
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fcreatedDtTm($f078fcreatedDtTm)
    {
        $this->f078fcreatedDtTm = $f078fcreatedDtTm;

        return $this;
    }

    /**
     * Get f078fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fcreatedDtTm()
    {
        return $this->f078fcreatedDtTm;
    }

    /**
     * Set f078transactionDate
     *
     * @param \DateTime $f078transactionDate
     *
     * @return T078fonlineBankPayment
     */
    public function setF078transactionDate($f078transactionDate)
    {
        $this->f078transactionDate = $f078transactionDate;

        return $this;
    }

    /**
     * Get f078transactionDate
     *
     * @return \DateTime
     */
    public function getF078transactionDate()
    {
        return $this->f078transactionDate;
    }

    /**
     * Set f078fupdatedDtTm
     *
     * @param \DateTime $f078fupdatedDtTm
     *
     * @return T078fonlineBankPayment
     */
    public function setF078fupdatedDtTm($f078fupdatedDtTm)
    {
        $this->f078fupdatedDtTm = $f078fupdatedDtTm;

        return $this;
    }

    /**
     * Get f078fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fupdatedDtTm()
    {
        return $this->f078fupdatedDtTm;
    }

    /**
     * Set f078fdate
     *
     * @param string $f078fdate
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fdate($f078fdate)
    {
        $this->f078fdate = $f078fdate;

        return $this;
    }

    /**
     * Get f078fdate
     *
     * @return string
     */
    public function getF078fdate()
    {
        return $this->f078fdate;
    }

    /**
     * Set f078famount
     *
     * @param integer $f078famount
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078famount($f078famount)
    {
        $this->f078famount = $f078famount;

        return $this;
    }

    /**
     * Get f078famount
     *
     * @return integer
     */
    public function getF078famount()
    {
        return $this->f078famount;
    }

    /**
     * Set f078freferenceNumber
     *
     * @param string $f078freferenceNumber
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078freferenceNumber($f078freferenceNumber)
    {
        $this->f078freferenceNumber = $f078freferenceNumber;

        return $this;
    }

    /**
     * Get f078freferenceNumber1
     *
     * @return string
     */
    public function getF078freferenceNumber1()
    {
        return $this->f078freferenceNumber1;
    }

    /**
     * Set f078freferenceNumber1
     *
     * @param string $f078freferenceNumber1
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078freferenceNumber1($f078freferenceNumber1)
    {
        $this->f078freferenceNumber1 = $f078freferenceNumber1;

        return $this;
    }

    /**
     * Get f078freferenceNumber
     *
     * @return string
     */
    public function getF078freferenceNumber()
    {
        return $this->f078freferenceNumber;
    }

    /**
     * Set f078ficNumber
     *
     * @param string $f078ficNumber
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078ficNumber($f078ficNumber)
    {
        $this->f078ficNumber = $f078ficNumber;

        return $this;
    }

    /**
     * Get f078ficNumber
     *
     * @return string
     */
    public function getF078ficNumber()
    {
        return $this->f078ficNumber;
    }

}

