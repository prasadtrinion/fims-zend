<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T034fgrnDetails
 *
 * @ORM\Table(name="t034fgrn_details")
 * @ORM\Entity(repositoryClass="Application\Repository\GrnRepository")
 */
class T034fgrnDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f034fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_grn", type="integer", nullable=true)
     */
    private $f034fidGrn;

     /**
     * @var integer
     *
     * @ORM\Column(name="f034ftype", type="string",length=20, nullable=true)
     */
    private $f034ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_item", type="integer", nullable=true)
     */
    private $f034fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fso_code", type="string", length=15, nullable=true)
     */
    private $f034fsoCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034frequired_date", type="datetime", nullable=true)
     */
    private $f034frequiredDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fquantity", type="integer", nullable=true)
     */
    private $f034fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fquantity_received", type="integer", nullable=true)
     */
    private $f034fquantityReceived;

    /**
     * @var string
     *
     * @ORM\Column(name="f034funit", type="string", length=15, nullable=true)
     */
    private $f034funit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fprice", type="integer", nullable=true)
     */
    private $f034fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034total", type="integer", nullable=true)
     */
    private $f034total;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034ftax_code", type="integer", nullable=true)
     */
    private $f034ftaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fpercentage", type="integer", nullable=true)
     */
    private $f034fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034ftax_amount", type="integer", nullable=true)
     */
    private $f034ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034ftotal_inc_tax", type="integer", nullable=true)
     */
    private $f034ftotalIncTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fstatus", type="integer", nullable=true)
     */
    private $f034fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fcreated_by", type="integer", nullable=true)
     */
    private $f034fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fupdated_by", type="integer", nullable=true)
     */
    private $f034fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f034ffund_code", type="string",length=20, nullable=true)
     */
    private $f034ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fdepartment_code", type="string",length=20, nullable=true)
     */
    private $f034fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034factivity_code", type="string",length=20, nullable=true)
     */
    private $f034factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034faccount_code", type="string",length=20, nullable=true)
     */
    private $f034faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fbudget_fund_code", type="string",length=20, nullable=true)
     */
    private $f034fbudgetFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fbudget_department_code", type="string",length=20, nullable=true)
     */
    private $f034fbudgetDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fbudget_activity_code", type="string",length=20, nullable=true)
     */
    private $f034fbudgetActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fbudget_account_code", type="string",length=20, nullable=true)
     */
    private $f034fbudgetAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fitem_description", type="string",length=100, nullable=true)
     */
    private $f034fitemDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034forder_line_no", type="integer", nullable=true)
     */
    private $f034forderLineNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fapproved_status", type="integer", nullable=true)
     */
    private $f034fapprovedStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f034freason", type="string",length=50, nullable=true)
     */
    private $f034freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f034ftype_of_item", type="string",length=60, nullable=true)
     */
    private $f034ftypeOfItem;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fitem_type", type="string",length=60, nullable=true)
     */
    private $f034fitemType;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fitem_category", type="integer", nullable=true)
     */
    private $f034fitemCategory;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fitem_sub_category", type="integer", nullable=true)
     */
    private $f034fitemSubCategory;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fasset_code", type="string",length=60, nullable=true)
     */
    private $f034fassetCode;


    /**
     * @var string
     *
     * @ORM\Column(name="f034fbalance_quantity", type="integer", nullable=true)
     */
    private $f034fbalanceQuantity;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fitem_procurement_type", type="string",length=255, nullable=true)
     */
    private $f034fitemProcurementType;


    /**
     * Get f034fidDetails
     *
     * @return integer
     */
    public function getF034fidDetails()
    {
        return $this->f034fidDetails;
    }

    /**
     * Set f034fitemType
     *
     * @param integer $f034fitemType
     *
     * @return T034fgrnDetails
     */
    public function setF034fitemType($f034fitemType)
    {
        $this->f034fitemType = $f034fitemType;

        return $this;
    }

    /**
     * Get f034fitemType
     *
     * @return integer
     */
    public function getF034fitemType()
    {
        return $this->f034fitemType;
    }

    /**
     * Set f034ftype
     *
     * @param integer $f034ftype
     *
     * @return T034fgrnDetails
     */
    public function setF034ftype($f034ftype)
    {
        $this->f034ftype = $f034ftype;

        return $this;
    }

    /**
     * Get f034ftype
     *
     * @return integer
     */
    public function getF034ftype()
    {
        return $this->f034ftype;
    }

     /**
     * Set f034fidGrn
     *
     * @param integer $f034fidGrn
     *
     * @return T034fgrnDetails
     */
    public function setF034fidGrn($f034fidGrn)
    {
        $this->f034fidGrn = $f034fidGrn;

        return $this;
    }

    /**
     * Get f034fidGrn
     *
     * @return integer
     */
    public function getF034fidGrn()
    {
        return $this->f034fidGrn;
    }

    /**
     * Set f034fidItem
     *
     * @param integer $f034fidItem
     *
     * @return T034fgrnDetails
     */
    public function setF034fidItem($f034fidItem)
    {
        $this->f034fidItem = $f034fidItem;

        return $this;
    }

    /**
     * Get f034fidItem
     *
     * @return integer
     */
    public function getF034fidItem()
    {
        return $this->f034fidItem;
    }

    /**
     * Set f034fsoCode
     *
     * @param string $f034fsoCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fsoCode($f034fsoCode)
    {
        $this->f034fsoCode = $f034fsoCode;

        return $this;
    }

    /**
     * Get f034fsoCode
     *
     * @return string
     */
    public function getF034fsoCode()
    {
        return $this->f034fsoCode;
    }

    /**
     * Set f034frequiredDate
     *
     * @param \DateTime $f034frequiredDate
     *
     * @return T034fgrnDetails
     */
    public function setF034frequiredDate($f034frequiredDate)
    {
        $this->f034frequiredDate =  $f034fapprovedDate;

        return $this;
    }

    /**
     * Get f034frequiredDate
     *
     * @return \DateTime
     */
    public function getF034frequiredDate()
    {
        return $this->f034frequiredDate;
    }

    /**
     * Set f034fquantity
     *
     * @param integer $f034fquantity
     *
     * @return T034fgrnDetails
     */
    public function setF034fquantity($f034fquantity)
    {
        $this->f034fquantity = $f034fquantity;

        return $this;
    }

    /**
     * Get f034fquantity
     *
     * @return integer
     */
    public function getF034fquantity()
    {
        return $this->f034fquantity;
    }

    /**
     * Set f034fquantityReceived
     *
     * @param integer $f034fquantityReceived
     *
     * @return T034fgrnDetails
     */
    public function setF034fquantityReceived($f034fquantityReceived)
    {
        $this->f034fquantityReceived = $f034fquantityReceived;

        return $this;
    }

    /**
     * Get f034fquantityReceived
     *
     * @return integer
     */
    public function getF034fquantityReceived()
    {
        return $this->f034fquantityReceived;
    }

    /**
     * Set f034funit
     *
     * @param string $f034funit
     *
     * @return T034fgrnDetails
     */
    public function setF034funit($f034funit)
    {
        $this->f034funit = $f034funit;

        return $this;
    }

    /**
     * Get f034funit
     *
     * @return string
     */
    public function getF034funit()
    {
        return $this->f034funit;
    }

    /**
     * Set f034fprice
     *
     * @param integer $f034fprice
     *
     * @return T034fgrnDetails
     */
    public function setF034fprice($f034fprice)
    {
        $this->f034fprice = $f034fprice;

        return $this;
    }

    /**
     * Get f034fprice
     *
     * @return integer
     */
    public function getF034fprice()
    {
        return $this->f034fprice;
    }

    /**
     * Set f034total
     *
     * @param float $f034total
     *
     * @return T034fgrnDetails
     */
    public function setF034total($f034total)
    {
        $this->f034total = $f034total;

        return $this;
    }

    /**
     * Get f034total
     *
     * @return float
     */
    public function getF034total()
    {
        return $this->f034total;
    }

    /**
     * Set f034ftaxCode
     *
     * @param integer $f034ftaxCode
     *
     * @return T034fgrnDetails
     */
    public function setF034ftaxCode($f034ftaxCode)
    {
        $this->f034ftaxCode = $f034ftaxCode;

        return $this;
    }

    /**
     * Get f034ftaxCode
     *
     * @return integer
     */
    public function getF034ftaxCode()
    {
        return $this->f034ftaxCode;
    }

    /**
     * Set f034fpercentage
     *
     * @param integer $f034fpercentage
     *
     * @return T034fgrnDetails
     */
    public function setF034fpercentage($f034fpercentage)
    {
        $this->f034fpercentage = $f034fpercentage;

        return $this;
    }

    /**
     * Get f034fpercentage
     *
     * @return integer
     */
    public function getF034fpercentage()
    {
        return $this->f034fpercentage;
    }

    /**
     * Set f034ftaxAmount
     *
     * @param float $f034ftaxAmount
     *
     * @return T034fgrnDetails
     */
    public function setF034ftaxAmount($f034ftaxAmount)
    {
        $this->f034ftaxAmount = $f034ftaxAmount;

        return $this;
    }

    /**
     * Get f034ftaxAmount
     *
     * @return float
     */
    public function getF034ftaxAmount()
    {
        return $this->f034ftaxAmount;
    }

    /**
     * Set f034ftotalIncTax
     *
     * @param float $f034ftotalIncTax
     *
     * @return T034fgrnDetails
     */
    public function setF034ftotalIncTax($f034ftotalIncTax)
    {
        $this->f034ftotalIncTax = $f034ftotalIncTax;

        return $this;
    }

    /**
     * Get f034ftotalIncTax
     *
     * @return float
     */
    public function getF034ftotalIncTax()
    {
        return $this->f034ftotalIncTax;
    }

    /**
     * Set f034fstatus
     *
     * @param integer $f034fstatus
     *
     * @return T034fgrnDetails
     */
    public function setF034fstatus($f034fstatus)
    {
        $this->f034fstatus = $f034fstatus;

        return $this;
    }

    /**
     * Get f034fstatus
     *
     * @return integer
     */
    public function getF034fstatus()
    {
        return $this->f034fstatus;
    }

    /**
     * Set f034fcreatedBy
     *
     * @param integer $f034fcreatedBy
     *
     * @return T034fgrnDetails
     */
    public function setF034fcreatedBy($f034fcreatedBy)
    {
        $this->f034fcreatedBy = $f034fcreatedBy;

        return $this;
    }

    /**
     * Get f034fcreatedBy
     *
     * @return integer
     */
    public function getF034fcreatedBy()
    {
        return $this->f034fcreatedBy;
    }

    /**
     * Set f034fupdatedBy
     *
     * @param integer $f034fupdatedBy
     *
     * @return T034fgrnDetails
     */
    public function setF034fupdatedBy($f034fupdatedBy)
    {
        $this->f034fupdatedBy = $f034fupdatedBy;

        return $this;
    }

    /**
     * Get f034fupdatedBy
     *
     * @return integer
     */
    public function getF034fupdatedBy()
    {
        return $this->f034fupdatedBy;
    }

    /**
     * Set f034fcreatedDtTm
     *
     * @param \DateTime $f034fcreatedDtTm
     *
     * @return T034fgrnDetails
     */
    public function setF034fcreatedDtTm($f034fcreatedDtTm)
    {
        $this->f034fcreatedDtTm = $f034fcreatedDtTm;

        return $this;
    }

    /**
     * Get f034fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fcreatedDtTm()
    {
        return $this->f034fcreatedDtTm;
    }

    /**
     * Set f034fupdatedDtTm
     *
     * @param \DateTime $f034fupdatedDtTm
     *
     * @return T034fgrnDetails
     */
    public function setF034fupdatedDtTm($f034fupdatedDtTm)
    {
        $this->f034fupdatedDtTm = $f034fupdatedDtTm;

        return $this;
    }

    /**
     * Get f034fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fupdatedDtTm()
    {
        return $this->f034fupdatedDtTm;
    }

    /**
     * Set f034ffundCode
     *
     * @param string $f034ffundCode
     *
     * @return T034fgrnDetails
     */
    public function setF034ffundCode($f034ffundCode)
    {
        $this->f034ffundCode = $f034ffundCode;

        return $this;
    }

    /**
     * Get f034ffundCode
     *
     * @return string
     */
    public function getF034ffundCode()
    {
        return $this->f034ffundCode;
    }

    /**
     * Set f034factivityCode
     *
     * @param string $f034factivityCode
     *
     * @return T034fgrnDetails
     */
    public function setF034factivityCode($f034factivityCode)
    {
        $this->f034factivityCode = $f034factivityCode;

        return $this;
    }

    /**
     * Get f034factivityCode
     *
     * @return string
     */
    public function getF034factivityCode()
    {
        return $this->f034factivityCode;
    }

    /**
     * Set f034fdepartmentCode
     *
     * @param string $f034fdepartmentCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fdepartmentCode($f034fdepartmentCode)
    {
        $this->f034fdepartmentCode = $f034fdepartmentCode;

        return $this;
    }

    /**
     * Get f034fdepartmentCode
     *
     * @return string
     */
    public function getF034fdepartmentCode()
    {
        return $this->f034fdepartmentCode;
    }

    /**
     * Set f034faccountCode
     *
     * @param string $f034faccountCode
     *
     * @return T034fgrnDetails
     */
    public function setF034faccountCode($f034faccountCode)
    {
        $this->f034faccountCode = $f034faccountCode;

        return $this;
    }

    /**
     * Get f034faccountCode
     *
     * @return string
     */
    public function getF034faccountCode()
    {
        return $this->f034faccountCode;
    }

    /**
     * Set f034fbudgetFundCode
     *
     * @param string $f034fbudgetFundCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fbudgetFundCode($f034fbudgetFundCode)
    {
        $this->f034fbudgetFundCode = $f034fbudgetFundCode;

        return $this;
    }

    /**
     * Get f034fbudgetFundCode
     *
     * @return string
     */
    public function getF034fbudgetFundCode()
    {
        return $this->f034fbudgetFundCode;
    }

    /**
     * Set f034fbudgetActivityCode
     *
     * @param string $f034fbudgetActivityCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fbudgetActivityCode($f034fbudgetActivityCode)
    {
        $this->f034fbudgetActivityCode = $f034fbudgetActivityCode;

        return $this;
    }

    /**
     * Get f034fbudgetActivityCode
     *
     * @return string
     */
    public function getF034fbudgetActivityCode()
    {
        return $this->f034fbudgetActivityCode;
    }

    /**
     * Set f034fbudgetDepartmentCode
     *
     * @param string $f034fbudgetDepartmentCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fbudgetDepartmentCode($f034fbudgetDepartmentCode)
    {
        $this->f034fbudgetDepartmentCode = $f034fbudgetDepartmentCode;

        return $this;
    }

    /**
     * Get f034fbudgetDepartmentCode
     *
     * @return string
     */
    public function getF034fbudgetDepartmentCode()
    {
        return $this->f034fbudgetDepartmentCode;
    }

    /**
     * Set f034fbudgetAccountCode
     *
     * @param string $f034fbudgetAccountCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fbudgetAccountCode($f034fbudgetAccountCode)
    {
        $this->f034fbudgetAccountCode = $f034fbudgetAccountCode;

        return $this;
    }

    /**
     * Get f034fbudgetAccountCode
     *
     * @return string
     */
    public function getF034fbudgetAccountCode()
    {
        return $this->f034fbudgetAccountCode;
    }


    /**
     * Set f034fitemDescription
     *
     * @param string $f034fitemDescription
     *
     * @return T034fgrnDetails
     */
    public function setF034fitemDescription($f034fitemDescription)
    {
        $this->f034fitemDescription = $f034fitemDescription;

        return $this;
    }

    /**
     * Get f034fitemDescription
     *
     * @return string
     */
    public function getF034fitemDescription()
    {
        return $this->f034fitemDescription;
    }

    /**
     * Set f034forderLineNo
     *
     * @param integer $f034forderLineNo
     *
     * @return T034fgrnDetails
     */
    public function setF034forderLineNo($f034fitemDescription)
    {
        $this->f034forderLineNo = $f034forderLineNo;

        return $this;
    }

    /**
     * Get f034forderLineNo
     *
     * @return integer
     */
    public function getF034forderLineNo()
    {
        return $this->f034forderLineNo;
    }

    /**
     * Set f034fapprovedStatus
     *
     * @param integer $f034fapprovedStatus
     *
     * @return T034fgrnDetails
     */
    public function setF034fapprovedStatus($f034fapprovedStatus)
    {
        $this->f034fapprovedStatus = $f034fapprovedStatus;

        return $this;
    }

    /**
     * Get f034fapprovedStatus
     *
     * @return integer
     */
    public function getF034fapprovedStatus()
    {
        return $this->f034fapprovedStatus;
    }

    /**
     * Set f034freason
     *
     * @param integer $f034freason
     *
     * @return T034fgrnDetails
     */
    public function setF034freason($f034freason)
    {
        $this->f034freason = $f034freason;

        return $this;
    }

    /**
     * Get f034freason
     *
     * @return integer
     */
    public function getF034freason()
    {
        return $this->f034freason;
    }

    /**
     * Set f034ftypeOfItem
     *
     * @param integer $f034ftypeOfItem
     *
     * @return T034fgrnDetails
     */
    public function setF034ftypeOfItem($f034ftypeOfItem)
    {
        $this->f034ftypeOfItem = $f034ftypeOfItem;

        return $this;
    }

    /**
     * Get f034ftypeOfItem
     *
     * @return integer
     */
    public function getF034ftypeOfItem()
    {
        return $this->f034ftypeOfItem;
    }

    /**
     * Set f034fitemCategory
     *
     * @param integer $f034fitemCategory
     *
     * @return T034fgrnDetails
     */
    public function setF034fitemCategory($f034fitemCategory)
    {
        $this->f034fitemCategory = $f034fitemCategory;

        return $this;
    }

    /**
     * Get f034fitemCategory
     *
     * @return integer
     */
    public function getF034fitemCategory()
    {
        return $this->f034fitemCategory;
    }

    
    /**
     * Set f034fitemSubCategory
     *
     * @param integer $f034fitemSubCategory
     *
     * @return T034fgrnDetails
     */
    public function setF034fitemSubCategory($f034fitemSubCategory)
    {
        $this->f034fitemSubCategory = $f034fitemSubCategory;

        return $this;
    }

    /**
     * Get f034fitemSubCategory
     *
     * @return integer
     */
    public function getF034fitemSubCategory()
    {
        return $this->f034fitemSubCategory;
    }
    

    /**
     * Set f034fassetCode
     *
     * @param integer $f034fassetCode
     *
     * @return T034fgrnDetails
     */
    public function setF034fassetCode($f034fassetCode)
    {
        $this->f034fassetCode = $f034fassetCode;

        return $this;
    }

    /**
     * Get f034fassetCode
     *
     * @return integer
     */
    public function getF034fassetCode()
    {
        return $this->f034fassetCode;
    }

    /**
     * Set f034fbalanceQuantity
     *
     * @param integer $f034fbalanceQuantity
     *
     * @return T034fgrnDetails
     */
    public function setF034fbalanceQuantity($f034fbalanceQuantity)
    {
        $this->f034fbalanceQuantity = $f034fbalanceQuantity;

        return $this;
    }

    /**
     * Get f034fbalanceQuantity
     *
     * @return integer
     */
    public function getF034fbalanceQuantity()
    {
        return $this->f034fbalanceQuantity;
    }

    /**
     * Set f034fitemProcurementType
     *
     * @param integer $f034fitemProcurementType
     *
     * @return T034fgrnDetails
     */
    public function setF034fitemProcurementType($f034fitemProcurementType)
    {
        $this->f034fitemProcurementType = $f034fitemProcurementType;

        return $this;
    }

    /**
     * Get f034fitemProcurementType
     *
     * @return integer
     */
    public function getF034fitemProcurementType()
    {
        return $this->f034fitemProcurementType;
    }

}
