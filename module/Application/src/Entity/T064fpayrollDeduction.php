<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064fpayrollDeduction
 *
 * @ORM\Table(name="t064fpayroll_deduction")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollDeductionRepository")
 */
class T064fpayrollDeduction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f064freference_number", type="string", length=50, nullable=true)
     */
    private $f064freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fdescription", type="string", length=250, nullable=true)
     */
    private $f064fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f064flimit", type="string", length=250, nullable=true)
     */
    private $f064flimit;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcombine_limit", type="string", length=250, nullable=true)
     */
    private $f064fcombineLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=true)
     */
    private $f064fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=true)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=true)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fupdatedDtTm;



    /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064freferenceNumber
     *
     * @param string $f064freferenceNumber
     *
     * @return T064fpayrollDeduction
     */
    public function setF064freferenceNumber($f064freferenceNumber)
    {
        $this->f064freferenceNumber = $f064freferenceNumber;

        return $this;
    }

    /**
     * Get f064freferenceNumber
     *
     * @return string
     */
    public function getF064freferenceNumber()
    {
        return $this->f064freferenceNumber;
    }

    /**
     * Set f064fdescription
     *
     * @param string $f064fdescription
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fdescription($f064fdescription)
    {
        $this->f064fdescription = $f064fdescription;

        return $this;
    }

    /**
     * Get f064fdescription
     *
     * @return string
     */
    public function getF064fdescription()
    {
        return $this->f064fdescription;
    }

    /**
     * Set f064flimit
     *
     * @param string $f064flimit
     *
     * @return T064fpayrollDeduction
     */
    public function setF064flimit($f064flimit)
    {
        $this->f064flimit = $f064flimit;

        return $this;
    }

    /**
     * Get f064flimit
     *
     * @return string
     */
    public function getF064flimit()
    {
        return $this->f064flimit;
    }

    /**
     * Set f064fcombineLimit
     *
     * @param string $f064fcombineLimit
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fcombineLimit($f064fcombineLimit)
    {
        $this->f064fcombineLimit = $f064fcombineLimit;

        return $this;
    }

    /**
     * Get f064fcombineLimit
     *
     * @return string
     */
    public function getF064fcombineLimit()
    {
        return $this->f064fcombineLimit;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fcreatedDtTm
     *
     * @param \DateTime $f064fcreatedDtTm
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fcreatedDtTm($f064fcreatedDtTm)
    {
        $this->f064fcreatedDtTm = $f064fcreatedDtTm;

        return $this;
    }

    /**
     * Get f064fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreatedDtTm()
    {
        return $this->f064fcreatedDtTm;
    }

    /**
     * Set f064fupdatedDtTm
     *
     * @param \DateTime $f064fupdatedDtTm
     *
     * @return T064fpayrollDeduction
     */
    public function setF064fupdatedDtTm($f064fupdatedDtTm)
    {
        $this->f064fupdatedDtTm = $f064fupdatedDtTm;

        return $this;
    }

    /**
     * Get f064fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdatedDtTm()
    {
        return $this->f064fupdatedDtTm;
    }
}
