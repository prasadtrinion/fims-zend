<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T095fcreditorSetup
 *
 * @ORM\Table(name="t095fcreditor_setup")
* @ORM\Entity(repositoryClass="Application\Repository\CreditorSetupRepository")
 */
class T095fcreditorSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f095fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f095fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="t095fsetup_id", type="integer", nullable=true)
     */
    private $f095fsetupId;

    /**
     * @var string
     *
     * @ORM\Column(name="f095fdescription", type="string", length=300, nullable=true)
     */
    private $f095fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f095faccount_code", type="string",length=20, nullable=true)
     */
    private $f095faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f095ftype", type="string",length=30, nullable=true)
     */
    private $f095ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f095fstatus", type="integer", nullable=true)
     */
    private $f095fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f095fcreated_by", type="integer", nullable=true)
     */
    private $f095fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f095fupdated_by", type="integer", nullable=true)
     */
    private $f095fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f095fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f095fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f095fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f095fupdatedDtTm;



    /**
     * Get f095fid
     *
     * @return integer
     */
    public function getF095fid()
    {
        return $this->f095fid;
    }

    /**
     * Set f095fsetupId
     *
     * @param integer $f095fsetupId
     *
     * @return T095fcreditorSetup
     */
    public function setF095fsetupId($f095fsetupId)
    {
        $this->f095fsetupId = $f095fsetupId;

        return $this;
    }

    /**
     * Get f095fsetupId
     *
     * @return integer
     */
    public function getF095fsetupId()
    {
        return $this->f095fsetupId;
    }

     /**
     * Set f095ftype
     *
     * @param integer $f095ftype
     *
     * @return T095fcreditorSetup
     */
    public function setF095ftype($f095ftype)
    {
        $this->f095ftype = $f095ftype;

        return $this;
    }

    /**
     * Get f095ftype
     *
     * @return integer
     */
    public function getF095ftype()
    {
        return $this->f095ftype;
    }

    /**
     * Set f095fdescription
     *
     * @param string $f095fdescription
     *
     * @return T095fcreditorSetup
     */
    public function setF095fdescription($f095fdescription)
    {
        $this->f095fdescription = $f095fdescription;

        return $this;
    }

    /**
     * Get f095fdescription
     *
     * @return string
     */
    public function getF095fdescription()
    {
        return $this->f095fdescription;
    }

    /**
     * Set f095faccountCode
     *
     * @param integer $f095faccountCode
     *
     * @return T095fcreditorSetup
     */
    public function setF095faccountCode($f095faccountCode)
    {
        $this->f095faccountCode = $f095faccountCode;

        return $this;
    }

    /**
     * Get f095faccountCode
     *
     * @return integer
     */
    public function getF095faccountCode()
    {
        return $this->f095faccountCode;
    }

    /**
     * Set f095fstatus
     *
     * @param integer $f095fstatus
     *
     * @return T095fcreditorSetup
     */
    public function setF095fstatus($f095fstatus)
    {
        $this->f095fstatus = $f095fstatus;

        return $this;
    }

    /**
     * Get f095fstatus
     *
     * @return integer
     */
    public function getF095fstatus()
    {
        return $this->f095fstatus;
    }

    /**
     * Set f095fcreatedBy
     *
     * @param integer $f095fcreatedBy
     *
     * @return T095fcreditorSetup
     */
    public function setF095fcreatedBy($f095fcreatedBy)
    {
        $this->f095fcreatedBy = $f095fcreatedBy;

        return $this;
    }

    /**
     * Get f095fcreatedBy
     *
     * @return integer
     */
    public function getF095fcreatedBy()
    {
        return $this->f095fcreatedBy;
    }

    /**
     * Set f095fupdatedBy
     *
     * @param integer $f095fupdatedBy
     *
     * @return T095fcreditorSetup
     */
    public function setF095fupdatedBy($f095fupdatedBy)
    {
        $this->f095fupdatedBy = $f095fupdatedBy;

        return $this;
    }

    /**
     * Get f095fupdatedBy
     *
     * @return integer
     */
    public function getF095fupdatedBy()
    {
        return $this->f095fupdatedBy;
    }

    /**
     * Set f095fcreatedDtTm
     *
     * @param \DateTime $f095fcreatedDtTm
     *
     * @return T095fcreditorSetup
     */
    public function setF095fcreatedDtTm($f095fcreatedDtTm)
    {
        $this->f095fcreatedDtTm = $f095fcreatedDtTm;

        return $this;
    }

    /**
     * Get f095fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF095fcreatedDtTm()
    {
        return $this->f095fcreatedDtTm;
    }

    /**
     * Set f095fupdatedDtTm
     *
     * @param \DateTime $f095fupdatedDtTm
     *
     * @return T095fcreditorSetup
     */
    public function setF095fupdatedDtTm($f095fupdatedDtTm)
    {
        $this->f095fupdatedDtTm = $f095fupdatedDtTm;

        return $this;
    }

    /**
     * Get f095fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF095fupdatedDtTm()
    {
        return $this->f095fupdatedDtTm;
    }
}
