<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T075fitemCodeSetUp
 *
 * @ORM\Table(name="t075fitem_code_set_up")
 * @ORM\Entity
 */
class T075fitemCodeSetUp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f075fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f075fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f075fitem_code", type="string", length=255, nullable=false)
     */
    private $f075fitemCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f075fglcode_id", type="integer", nullable=false)
     */
    private $f075fglcodeId;

    /**
     * @var string
     *
     * @ORM\Column(name="f075fitem_description", type="string", length=255, nullable=false)
     */
    private $f075fitemDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f075fgst", type="string", length=255, nullable=false)
     */
    private $f075fgst;

    /**
     * @var integer
     *
     * @ORM\Column(name="f075fpercentage", type="integer",  nullable=false)
     */
    private $f075fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f075fstatus", type="integer", nullable=false)
     */
    private $f075fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f075fcreated_by", type="integer", nullable=false)
     */
    private $f075fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f075fupdated_by", type="integer", nullable=false)
     */
    private $f075fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f075fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f075fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f075fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f075fupdatedDtTm;



    /**
     * Get f075fid
     *
     * @return integer
     */
    public function getF075fid()
    {
        return $this->f075fid;
    }

    /**
     * Set f075fitemCode
     *
     * @param string $f075fitemCode
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fitemCode($f075fitemCode)
    {
        $this->f075fitemCode = $f075fitemCode;

        return $this;
    }

    /**
     * Get f075fitemCode
     *
     * @return string
     */
    public function getF075fitemCode()
    {
        return $this->f075fitemCode;
    }

    /**
     * Set f075fglcodeId
     *
     * @param integer $f075fglcodeId
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fglcodeId($f075fglcodeId)
    {
        $this->f075fglcodeId = $f075fglcodeId;

        return $this;
    }

    /**
     * Get f075fglcodeId
     *
     * @return integer
     */
    public function getF075fglcodeId()
    {
        return $this->f075fglcodeId;
    }

    /**
     * Set f075fitemDescription
     *
     * @param string $f075fitemDescription
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fitemDescription($f075fitemDescription)
    {
        $this->f075fitemDescription = $f075fitemDescription;

        return $this;
    }

    /**
     * Get f075fitemDescription
     *
     * @return string
     */
    public function getF075fitemDescription()
    {
        return $this->f075fitemDescription;
    }

    /**
     * Set f075fgst
     *
     * @param string $f075fgst
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fgst($f075fgst)
    {
        $this->f075fgst = $f075fgst;

        return $this;
    }

    /**
     * Get f075fgst
     *
     * @return string
     */
    public function getF075fgst()
    {
        return $this->f075fgst;
    }

    /**
     * Set f075fpercentage
     *
     * @param integer $f075fpercentage
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fpercentage($f075fpercentage)
    {
        $this->f075fpercentage = $f075fpercentage;

        return $this;
    }

    /**
     * Get f075fpercentage
     *
     * @return integer
     */
    public function getF075fpercentage()
    {
        return $this->f075fpercentage;
    }

    /**
     * Set f075fstatus
     *
     * @param boolean $f075fstatus
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fstatus($f075fstatus)
    {
        $this->f075fstatus = $f075fstatus;

        return $this;
    }

    /**
     * Get f075fstatus
     *
     * @return boolean
     */
    public function getF075fstatus()
    {
        return $this->f075fstatus;
    }

    /**
     * Set f075fcreatedBy
     *
     * @param integer $f075fcreatedBy
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fcreatedBy($f075fcreatedBy)
    {
        $this->f075fcreatedBy = $f075fcreatedBy;

        return $this;
    }

    /**
     * Get f075fcreatedBy
     *
     * @return integer
     */
    public function getF075fcreatedBy()
    {
        return $this->f075fcreatedBy;
    }

    /**
     * Set f075fupdatedBy
     *
     * @param integer $f075fupdatedBy
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fupdatedBy($f075fupdatedBy)
    {
        $this->f075fupdatedBy = $f075fupdatedBy;

        return $this;
    }

    /**
     * Get f075fupdatedBy
     *
     * @return integer
     */
    public function getF075fupdatedBy()
    {
        return $this->f075fupdatedBy;
    }

    /**
     * Set f075fcreatedDtTm
     *
     * @param \DateTime $f075fcreatedDtTm
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fcreatedDtTm($f075fcreatedDtTm)
    {
        $this->f075fcreatedDtTm = $f075fcreatedDtTm;

        return $this;
    }

    /**
     * Get f075fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF075fcreatedDtTm()
    {
        return $this->f075fcreatedDtTm;
    }

    /**
     * Set f075fupdatedDtTm
     *
     * @param \DateTime $f075fupdatedDtTm
     *
     * @return T075fitemCodeSetUp
     */
    public function setF075fupdatedDtTm($f075fupdatedDtTm)
    {
        $this->f075fupdatedDtTm = $f075fupdatedDtTm;

        return $this;
    }

    /**
     * Get f075fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF075fupdatedDtTm()
    {
        return $this->f075fupdatedDtTm;
    }
}
