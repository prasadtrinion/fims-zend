<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T045fapplyLoanDetails
 *
 * @ORM\Table(name="t045fapply_loan_details")
* @ORM\Entity(repositoryClass="Application\Repository\ApplyLoanRepository")
 */
class T045fapplyLoanDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f045fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid_apply_loan", type="integer", nullable=true)
     */
    private $f045fidApplyLoan;

    /**
     * @var string
     *
     * @ORM\Column(name="f045fname", type="string", length=50, nullable=true)
     */
    private $f045fname;

     /**
     * @var string
     *
     * @ORM\Column(name="f045faccount_code", type="string", length=50, nullable=true)
     */
    private $f045faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f045fdeduction", type="string", length=50, nullable=true)
     */
    private $f045fdeduction;

     /**
     * @var integer
     *
     * @ORM\Column(name="f045fpercentage", type="integer", nullable=true)
     */
    private $f045fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fstatus", type="integer", nullable=true)
     */
    private $f045fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fcreated_by", type="integer", nullable=true)
     */
    private $f045fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fupdated_by", type="integer", nullable=true)
     */
    private $f045fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f045fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f045fupdatedDtTm;

    /**
     * Get f045fidDetails
     *
     * @return integer
     */
    public function getF045fidDetails()
    {
        return $this->f045fidDetails;
    }


    /**
     * Set f045fidapplyLoan
     *
     * @param integer $f045fidapplyLoan
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fidApplyLoan($f045fidapplyLoan)
    {
        $this->f045fidapplyLoan = $f045fidapplyLoan;

        return $this;
    }

    /**
     * Get f045fidapplyLoan
     *
     * @return integer
     */
    public function getF045fidApplyLoan()
    {
        return $this->f045fidapplyLoan;
    }

    /**
     * Set f045fname
     *
     * @param string $f045fname
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fname($f045fname)
    {
        $this->f045fname = $f045fname;

        return $this;
    }

    /**
     * Get f045fname
     *
     * @return string
     */
    public function getF045fname()
    {
        return $this->f045fname;
    }

    /**
     * Set f045faccountCode
     *
     * @param string $f045faccountCode
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045faccountCode($f045faccountCode)
    {
        $this->f045faccountCode = $f045faccountCode;

        return $this;
    }

    /**
     * Get f045faccountCode
     *
     * @return string
     */
    public function getF045faccountCode()
    {
        return $this->f045faccountCode;
    }

    /**
     * Set f045fdeduction
     *
     * @param string $f045fdeduction
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fdeduction($f045fdeduction)
    {
        $this->f045fdeduction = $f045fdeduction;

        return $this;
    }

    /**
     * Get f045fdeduction
     *
     * @return string
     */
    public function getF045fdeduction()
    {
        return $this->f045fdeduction;
    }

    /**
     * Set f045fpercentage
     *
     * @param integer $f045fpercentage
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fpercentage($f045fpercentage)
    {
        $this->f045fpercentage = $f045fpercentage;

        return $this;
    }

    /**
     * Get f045fpercentage
     *
     * @return integer
     */
    public function getF045fpercentage()
    {
        return $this->f045fpercentage;
    }

    /**
     * Set f045fcreatedBy
     *
     * @param integer $f045fcreatedBy
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fcreatedBy($f045fcreatedBy)
    {
        $this->f045fcreatedBy = $f045fcreatedBy;

        return $this;
    }

    /**
     * Get f045fcreatedBy
     *
     * @return integer
     */
    public function getF045fcreatedBy()
    {
        return $this->f045fcreatedBy;
    }

    /**
     * Set f045fupdatedBy
     *
     * @param integer $f045fupdatedBy
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fupdatedBy($f045fupdatedBy)
    {
        $this->f045fupdatedBy = $f045fupdatedBy;

        return $this;
    }

    /**
     * Get f045fupdatedBy
     *
     * @return integer
     */
    public function getF045fupdatedBy()
    {
        return $this->f045fupdatedBy;
    }

    /**
     * Set f045fcreatedDtTm
     *
     * @param \DateTime $f045fcreatedDtTm
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fcreatedDtTm($f045fcreatedDtTm)
    {
        $this->f045fcreatedDtTm = $f045fcreatedDtTm;

        return $this;
    }

    /**
     * Get f045fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF045fcreatedDtTm()
    {
        return $this->f045fcreatedDtTm;
    }

    /**
     * Set f045fupdatedDtTm
     *
     * @param \DateTime $f045fupdatedDtTm
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fupdatedDtTm($f045fupdatedDtTm)
    {
        $this->f045fupdatedDtTm = $f045fupdatedDtTm;

        return $this;
    }

    /**
     * Get f045fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF045fupdatedDtTm()
    {
        return $this->f045fupdatedDtTm;
    }

    /**
     * Set f045fstatus
     *
     * @param integer $f045fstatus
     *
     * @return T045fapplyLoanDetails
     */
    public function setF045fstatus($f045fstatus)
    {
        $this->f045fstatus = $f045fstatus;

        return $this;
    }

    /**
     * Get f045fstatus
     *
     * @return integer
     */
    public function getF045fstatus()
    {
        return $this->f045fstatus;
    }

    
}
