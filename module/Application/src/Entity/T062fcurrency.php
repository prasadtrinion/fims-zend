<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T062fcurrency
 *
 * @ORM\Table(name="t062fcurrency",uniqueConstraints={@ORM\UniqueConstraint(name="f062fcur_code_UNIQUE", columns={"f062fcur_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CurrencyRepository")
 */
class T062fcurrency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f062fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f062fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_code", type="string", length=200, nullable=false)
     */
    private $f062fcurCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_desc", type="string", length=255, nullable=false)
     */
    private $f062fcurDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_desc_default_lang", type="string", length=200, nullable=false)
     */
    private $f062fcurDescDefaultLang;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_symbol_prefix", type="string", length=200, nullable=false)
     */
    private $f062fcurSymbolPrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_symbol_suffix", type="string", length=200, nullable=false)
     */
    private $f062fcurSymbolSuffix;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_default", type="string", length=200, nullable=false)
     */
    private $f062fcurDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcur_decimal", type="string", length=200, nullable=false)
     */
    private $f062fcurDecimal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fcur_status", type="integer", nullable=false)
     */
    private $f062fcurStatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fcur_created_by", type="integer", nullable=false)
     */
    private $f062fcurCreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fcur_updated_by", type="integer", nullable=false)
     */
    private $f062fcurUpdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fcur_created_dt_tm", type="datetime", nullable=false)
     */
    private $f062fcurCreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fcur_updated_dt_tm", type="datetime", nullable=false)
     */
    private $f062fcurUpdatedDtTm;



    /**
     * Get f062fid
     *
     * @return integer
     */
    public function getF062fid()
    {
        return $this->f062fid;
    }

    /**
     * Set f062fcurCode
     *
     * @param string $f062fcurCode
     *
     * @return T062fcurrency
     */
    public function setF062fcurCode($f062fcurCode)
    {
        $this->f062fcurCode = $f062fcurCode;

        return $this;
    }

    /**
     * Get f062fcurCode
     *
     * @return string
     */
    public function getF062fcurCode()
    {
        return $this->f062fcurCode;
    }

    /**
     * Set f062fcurDesc
     *
     * @param string $f062fcurDesc
     *
     * @return T062fcurrency
     */
    public function setF062fcurDesc($f062fcurDesc)
    {
        $this->f062fcurDesc = $f062fcurDesc;

        return $this;
    }

    /**
     * Get f062fcurDesc
     *
     * @return string
     */
    public function getF062fcurDesc()
    {
        return $this->f062fcurDesc;
    }

    /**
     * Set f062fcurDescDefaultLang
     *
     * @param string $f062fcurDescDefaultLang
     *
     * @return T062fcurrency
     */
    public function setF062fcurDescDefaultLang($f062fcurDescDefaultLang)
    {
        $this->f062fcurDescDefaultLang = $f062fcurDescDefaultLang;

        return $this;
    }

    /**
     * Get f062fcurDescDefaultLang
     *
     * @return string
     */
    public function getF062fcurDescDefaultLang()
    {
        return $this->f062fcurDescDefaultLang;
    }

    /**
     * Set f062fcurSymbolPrefix
     *
     * @param string $f062fcurSymbolPrefix
     *
     * @return T062fcurrency
     */
    public function setF062fcurSymbolPrefix($f062fcurSymbolPrefix)
    {
        $this->f062fcurSymbolPrefix = $f062fcurSymbolPrefix;

        return $this;
    }

    /**
     * Get f062fcurSymbolPrefix
     *
     * @return string
     */
    public function getF062fcurSymbolPrefix()
    {
        return $this->f062fcurSymbolPrefix;
    }

    /**
     * Set f062fcurSymbolSuffix
     *
     * @param string $f062fcurSymbolSuffix
     *
     * @return T062fcurrency
     */
    public function setF062fcurSymbolSuffix($f062fcurSymbolSuffix)
    {
        $this->f062fcurSymbolSuffix = $f062fcurSymbolSuffix;

        return $this;
    }

    /**
     * Get f062fcurSymbolSuffix
     *
     * @return string
     */
    public function getF062fcurSymbolSuffix()
    {
        return $this->f062fcurSymbolSuffix;
    }

    /**
     * Set f062fcurDefault
     *
     * @param string $f062fcurDefault
     *
     * @return T062fcurrency
     */
    public function setF062fcurDefault($f062fcurDefault)
    {
        $this->f062fcurDefault = $f062fcurDefault;

        return $this;
    }

    /**
     * Get f062fcurDefault
     *
     * @return string
     */
    public function getF062fcurDefault()
    {
        return $this->f062fcurDefault;
    }

    /**
     * Set f062fcurDecimal
     *
     * @param string $f062fcurDecimal
     *
     * @return T062fcurrency
     */
    public function setF062fcurDecimal($f062fcurDecimal)
    {
        $this->f062fcurDecimal = $f062fcurDecimal;

        return $this;
    }

    /**
     * Get f062fcurDecimal
     *
     * @return string
     */
    public function getF062fcurDecimal()
    {
        return $this->f062fcurDecimal;
    }

    /**
     * Set f062fcurStatus
     *
     * @param integer $f062fcurStatus
     *
     * @return T062fcurrency
     */
    public function setF062fcurStatus($f062fcurStatus)
    {
        $this->f062fcurStatus = $f062fcurStatus;

        return $this;
    }

    /**
     * Get f062fcurStatus
     *
     * @return integer
     */
    public function getF062fcurStatus()
    {
        return $this->f062fcurStatus;
    }

    /**
     * Set f062fcurCreatedBy
     *
     * @param integer $f062fcurCreatedBy
     *
     * @return T062fcurrency
     */
    public function setF062fcurCreatedBy($f062fcurCreatedBy)
    {
        $this->f062fcurCreatedBy = $f062fcurCreatedBy;

        return $this;
    }

    /**
     * Get f062fcurCreatedBy
     *
     * @return integer
     */
    public function getF062fcurCreatedBy()
    {
        return $this->f062fcurCreatedBy;
    }

    /**
     * Set f062fcurUpdatedBy
     *
     * @param integer $f062fcurUpdatedBy
     *
     * @return T062fcurrency
     */
    public function setF062fcurUpdatedBy($f062fcurUpdatedBy)
    {
        $this->f062fcurUpdatedBy = $f062fcurUpdatedBy;

        return $this;
    }

    /**
     * Get f062fcurUpdatedBy
     *
     * @return integer
     */
    public function getF062fcurUpdatedBy()
    {
        return $this->f062fcurUpdatedBy;
    }

    /**
     * Set f062fcurCreatedDtTm
     *
     * @param \DateTime $f062fcurCreatedDtTm
     *
     * @return T062fcurrency
     */
    public function setF062fcurCreatedDtTm($f062fcurCreatedDtTm)
    {
        $this->f062fcurCreatedDtTm = $f062fcurCreatedDtTm;

        return $this;
    }

    /**
     * Get f062fcurCreatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fcurCreatedDtTm()
    {
        return $this->f062fcurCreatedDtTm;
    }

    /**
     * Set f062fcurUpdatedDtTm
     *
     * @param \DateTime $f062fcurUpdatedDtTm
     *
     * @return T062fcurrency
     */
    public function setF062fcurUpdatedDtTm($f062fcurUpdatedDtTm)
    {
        $this->f062fcurUpdatedDtTm = $f062fcurUpdatedDtTm;

        return $this;
    }

    /**
     * Get f062fcurUpdatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fcurUpdatedDtTm()
    {
        return $this->f062fcurUpdatedDtTm;
    }
}
