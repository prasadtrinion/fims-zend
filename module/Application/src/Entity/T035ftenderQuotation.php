<?php 
 
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T035ftenderQuotation
 *
 * @ORM\Table(name="t035ftender_quotation")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderQuotationRepository")
 */
class T035ftenderQuotation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f035fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f035fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f035fquotation_number", type="string", length=30, nullable=false)
     */
    private $f035fquotationNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fid_category", type="string",length=20, nullable=false)
     */
    private $f035fidCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f035fstart_date", type="datetime", nullable=false)
     */
    private $f035fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f035fend_date", type="datetime", nullable=false)
     */
    private $f035fendDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f035ftitle", type="string", length=50, nullable=false)
     */
    private $f035ftitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fid_department", type="string",length=20, nullable=false)
     */
    private $f035fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035ftotal_amount", type="integer", nullable=false)
     */
    private $f035ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035ftender_status", type="string",length=50, nullable=false)
     */
    private $f035ftenderStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035ftype", type="string",length=50, nullable=false)
     */
    private $f035ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fsubmitted", type="integer", nullable=false)
     */
    private $f035fsubmitted;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fshortlisted", type="integer", nullable=false)
     */
    private $f035fshortlisted;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fawarded", type="integer", nullable=false)
     */
    private $f035fawarded;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fstatus", type="integer", nullable=false)
     */
    private $f035fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f035fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f035fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f035fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f035fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fcreated_by", type="integer", nullable=false)
     */
    private $f035fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fupdated_by", type="integer", nullable=false)
     */
    private $f035fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f035fopening_date", type="datetime", nullable=true)
     */
    private $f035fopeningDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f035fid_financial_year", type="integer", nullable=true)
     */
    private $f035fidFinancialYear;

        /**
     * Get f035fid
     *
     * @return integer
     */
    public function getF035fid()
    {
        return $this->f035fid;
    }

    /**
     * Set f035fidFinancialYear

     *
     * @param string $f035fidFinancialYear
     *
     * @return T035ftenderQuotation
     */
    public function setF035fidFinancialYear($f035fidFinancialYear)
    {
        $this->f035fidFinancialYear = $f035fidFinancialYear;

        return $this;
    }

    /**
     * Get f035fidFinancialYear
     *
     * @return string
     */
    public function getF035fidFinancialYear()
    {
        return $this->f035fidFinancialYear;
    }

     /**
     * Set f035fquotationNumber
     *
     * @param string $f035fquotationNumber
     *
     * @return T035ftenderQuotation
     */
    public function setF035fquotationNumber($f035fquotationNumber)
    {
        $this->f035fquotationNumber = $f035fquotationNumber;

        return $this;
    }

    /**
     * Get f035fquotationNumber
     *
     * @return string
     */
    public function getF035fquotationNumber()
    {
        return $this->f035fquotationNumber;
    }

    /**
     * Set f035fidCategory
     *
     * @param integer $f035fidCategory
     *
     * @return T035ftenderQuotation
     */
    public function setF035fidCategory($f035fidCategory)
    {
        $this->f035fidCategory = $f035fidCategory;

        return $this;
    }

    /**
     * Get f035fidCategory
     *
     * @return integer
     */
    public function getF035fidCategory()
    {
        return $this->f035fidCategory;
    }

    /**
     * Set f035fstartDate
     *
     * @param \DateTime $f035fstartDate
     *
     * @return T035ftenderQuotation
     */
    public function setF035fstartDate($f035fstartDate)
    {
        $this->f035fstartDate = $f035fstartDate;

        return $this;
    }

    /**
     * Get f035fstartDate
     *
     * @return \DateTime
     */
    public function getF035fstartDate()
    {
        return $this->f035fstartDate;
    }

    /**
     * Set f035fendDate
     *
     * @param \DateTime $f035fendDate
     *
     * @return T035ftenderQuotation
     */
    public function setF035fendDate($f035fendDate)
    {
        $this->f035fendDate = $f035fendDate;

        return $this;
    }

    /**
     * Get f035fendDate
     *
     * @return \DateTime
     */
    public function getF035fendDate()
    {
        return $this->f035fendDate;
    }

    /**
     * Set f035ftitle
     *
     * @param string $f035ftitle
     *
     * @return T035ftenderQuotation
     */
    public function setF035ftitle($f035ftitle)
    {
        $this->f035ftitle = $f035ftitle;

        return $this;
    }

    /**
     * Get f035ftitle
     *
     * @return string
     */
    public function getF035ftitle()
    {
        return $this->f035ftitle;
    }

    /**
     * Set f035fidDepartment
     *
     * @param integer $f035fidDepartment
     *
     * @return T035ftenderQuotation
     */
    public function setF035fidDepartment($f035fidDepartment)
    {
        $this->f035fidDepartment = $f035fidDepartment;

        return $this;
    }

    /**
     * Get f035fidDepartment
     *
     * @return integer
     */
    public function getF035fidDepartment()
    {
        return $this->f035fidDepartment;
    }

    /**
     * Set f035ftotalAmount
     *
     * @param integer $f035ftotalAmount
     *
     * @return T035ftenderQuotation
     */
    public function setF035ftotalAmount($f035ftotalAmount)
    {
        $this->f035ftotalAmount = $f035ftotalAmount;

        return $this;
    }

    /**
     * Get f035ftotalAmount
     *
     * @return integer
     */
    public function getF035ftotalAmount()
    {
        return $this->f035ftotalAmount;
    }

    /**
     * Set f035ftenderStatus
     *
     * @param integer $f035ftenderStatus
     *
     * @return T035ftenderQuotation
     */
    public function setF035ftenderStatus($f035ftenderStatus)
    {
        $this->f035ftenderStatus = $f035ftenderStatus;

        return $this;
    }

    /**
     * Get f035ftenderStatus
     *
     * @return integer
     */
    public function getF035ftenderStatus()
    {
        return $this->f035ftenderStatus;
    }

    /**
     * Set f035ftype
     *
     * @param integer $f035ftype
     *
     * @return T035ftenderQuotation
     */
    public function setF035ftype($f035ftype)
    {
        $this->f035ftype = $f035ftype;

        return $this;
    }

    /**
     * Get f035ftype
     *
     * @return integer
     */
    public function getF035ftype()
    {
        return $this->f035ftype;
    }


    /**
     * Set f035fsubmitted
     *
     * @param integer $f035fsubmitted
     *
     * @return T035ftenderQuotation
     */
    public function setF035fsubmitted($f035fsubmitted)
    {
        $this->f035fsubmitted = $f035fsubmitted;

        return $this;
    }

    /**
     * Get f035fsubmitted
     *
     * @return integer
     */
    public function getF035fsubmitted()
    {
        return $this->f035fsubmitted;
    }


    /**
     * Set f035fshortlisted
     *
     * @param integer $f035fshortlisted
     *
     * @return T035ftenderQuotation
     */
    public function setF035fshortlisted($f035fshortlisted)
    {
        $this->f035fshortlisted = $f035fshortlisted;

        return $this;
    }

    /**
     * Get f035fshortlisted
     *
     * @return integer
     */
    public function getF035fshortlisted()
    {
        return $this->f035fshortlisted;
    }

    /**
     * Set f035fawarded
     *
     * @param integer $f035fawarded
     *
     * @return T035ftenderQuotation
     */
    public function setF035fawarded($f035fawarded)
    {
        $this->f035fawarded = $f035fawarded;

        return $this;
    }

    /**
     * Get f035fawarded
     *
     * @return integer
     */
    public function getF035fawarded()
    {
        return $this->f035fawarded;
    }















    /**
     * Set f035fstatus
     *
     * @param integer $f035fstatus
     *
     * @return T035ftenderQuotation
     */
    public function setF035fstatus($f035fstatus)
    {
        $this->f035fstatus = $f035fstatus;

        return $this;
    }

    /**
     * Get f035fstatus
     *
     * @return integer
     */
    public function getF035fstatus()
    {
        return $this->f035fstatus;
    }

    /**
     * Set f035fcreatedDtTm
     *
     * @param \DateTime $f035fcreatedDtTm
     *
     * @return T035ftenderQuotation
     */
    public function setF035fcreatedDtTm($f035fcreatedDtTm)
    {
        $this->f035fcreatedDtTm = $f035fcreatedDtTm;

        return $this;
    }

    /**
     * Get f035fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF035fcreatedDtTm()
    {
        return $this->f035fcreatedDtTm;
    }

    /**
     * Set f035fupdatedDtTm
     *
     * @param \DateTime $f035fupdatedDtTm
     *
     * @return T035ftenderQuotation
     */
    public function setF035fupdatedDtTm($f035fupdatedDtTm)
    {
        $this->f035fupdatedDtTm = $f035fupdatedDtTm;

        return $this;
    }

    /**
     * Get f035fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF035fupdatedDtTm()
    {
        return $this->f035fupdatedDtTm;
    }

    /**
     * Set f035fcreatedBy
     *
     * @param integer $f035fcreatedBy
     *
     * @return T035ftenderQuotation
     */
    public function setF035fcreatedBy($f035fcreatedBy)
    {
        $this->f035fcreatedBy = $f035fcreatedBy;

        return $this;
    }

    /**
     * Get f035fcreatedBy
     *
     * @return integer
     */
    public function getF035fcreatedBy()
    {
        return $this->f035fcreatedBy;
    }

    /**
     * Set f035fupdatedBy
     *
     * @param integer $f035fupdatedBy
     *
     * @return T035ftenderQuotation
     */
    public function setF035fupdatedBy($f035fupdatedBy)
    {
        $this->f035fupdatedBy = $f035fupdatedBy;

        return $this;
    }

    /**
     * Get f035fupdatedBy
     *
     * @return integer
     */
    public function getF035fupdatedBy()
    {
        return $this->f035fupdatedBy;
    }

    /**
     * Set f035fopeningDate
     *
     * @param \DateTime $f035fopeningDate
     *
     * @return T035ftenderQuotation
     */
    public function setF035fopeningDate($f035fopeningDate)
    {
        $this->f035fopeningDate = $f035fopeningDate;

        return $this;
    }

    /**
     * Get f035fopeningDate
     *
     * @return \DateTime
     */
    public function getF035fopeningDate()
    {
        return $this->f035fopeningDate;
    }



}

