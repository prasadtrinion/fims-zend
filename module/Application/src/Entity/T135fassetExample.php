<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T135fassetExample
 *
 * @ORM\Table(name="t135fasset_example")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetExampleRepository")
 */
class T135fassetExample
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f135fid", type="integer", nullable=false)
     * @ORM\Id 
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f135fid;
    /**
     * @var integer
     *
     * @ORM\Column(name="f135fasset_category", type="integer", nullable=true)
     */
    private $f135fassetCategory;

     /**
     * @var integer
     *
     * @ORM\Column(name="f135fasset_subcategory", type="integer", nullable=true)
     */
    private $f135fassetSubcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f135fasset_type", type="integer", nullable=true)
     */
    private $f135fassetType;
 
    /**
     * @var string
     *
     * @ORM\Column(name="f135fasset_code", type="string",length=25, nullable=true)
     */
    private $f135fassetCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f135fstatus", type="integer", nullable=true)
     */
    private $f135fstatus;

      /**
     * @var integer
     *
     * @ORM\Column(name="f135fcreated_by", type="integer", nullable=true)
     */
    private $f135fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f135fupdated_by", type="integer", nullable=true)
     */
    private $f135fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f135fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f135fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f135fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f135fupdatedDtTm;

  
    /**
     * Get f135fid
     *
     * @return integer
     */
    public function getF135fid()
    {
        return $this->f135fid;
    }

  
    /**
     * Set f135fassetCategory
     *
     * @param integer $f135fassetCategory
     *
     * @return T135fassetExample
     */
    public function setF135fassetCategory($f135fassetCategory)
    {
        $this->f135fassetCategory = $f135fassetCategory;

        return $this;
    }

    /**
     * Get f135fassetCategory
     *
     * @return integer
     */
    public function getF135fassetCategory()
    {
        return $this->f135fassetCategory;
    }

      /**
     * Set f135fassetCode
     *
     * @param string $f135fassetCode
     *
     * @return T135fassetExample
     */
    public function setF135fassetCode($f135fassetCode)
    {
        $this->f135fassetCode = $f135fassetCode;

        return $this;
    }

    /**
     * Get f135fassetCode
     *
     * @return string
     */
    public function getF135fassetCode()
    {
        return $this->f135fassetCode;
    }

   /**
     * Set f135fassetSubcategory
     *
     * @param integer $f135fassetSubcategory
     *
     * @return T135fassetExample
     */
    public function setF135fassetSubcategory($f135fassetSubcategory)
    {
        $this->f135fassetSubcategory = $f135fassetSubcategory;

        return $this;
    }

    /**
     * Get f135fassetSubcategory
     *
     * @return integer
     */
    public function getF135fassetSubcategory()
    {
        return $this->f135fassetSubcategory;
    }

   
    /**
     * Set f135fassetType
     *
     * @param integer $f135fassetType
     *
     * @return T135fassetExample
     */
    public function setF135fassetType($f135fassetType)
    {
        $this->f135fassetType = $f135fassetType;

        return $this;
    }

    /**
     * Get f135fassetType
     *
     * @return integer
     */
    public function getF135fassetType()
    {
        return $this->f135fassetType;
    }

      /**
     * Set f135fstatus
     *
     * @param integer $f135fstatus
     *
     * @return T135fassetExample
     */
    public function setF135fstatus($f135fstatus)
    {
        $this->f135fstatus = $f135fstatus;

        return $this;
    }

    /**
     * Get f135fstatus
     *
     * @return integer
     */
    public function getF135fstatus()
    {
        return $this->f135fstatus;
    }

    /**
     * Set f135fcreatedBy
     *
     * @param integer $f135fcreatedBy
     *
     * @return T135fassetExample
     */
    public function setF135fcreatedBy($f135fcreatedBy)
    {
        $this->f135fcreatedBy = $f135fcreatedBy;

        return $this;
    }

    /**
     * Get f135fcreatedBy
     *
     * @return integer
     */
    public function getF135fcreatedBy()
    {
        return $this->f135fcreatedBy;
    }

    /**
     * Set f135fupdatedBy
     *
     * @param integer $f135fupdatedBy
     *
     * @return T135fassetExample
     */
    public function setF135fupdatedBy($f135fupdatedBy)
    {
        $this->f135fupdatedBy = $f135fupdatedBy;

        return $this;
    }

    /**
     * Get f135fupdatedBy
     *
     * @return integer
     */
    public function getF135fupdatedBy()
    {
        return $this->f135fupdatedBy;
    }

    /**
     * Set f135fcreatedDtTm
     *
     * @param \DateTime $f135fcreatedDtTm
     *
     * @return T135fassetExample
     */
    public function setF135fcreatedDtTm($f135fcreatedDtTm)
    {
        $this->f135fcreatedDtTm = $f135fcreatedDtTm;

        return $this;
    }

    /**
     * Get f135fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF135fcreatedDtTm()
    {
        return $this->f135fcreatedDtTm;
    }

    /**
     * Set f135fupdatedDtTm
     *
     * @param \DateTime $f135fupdatedDtTm
     *
     * @return T135fassetExample
     */
    public function setF135fupdatedDtTm($f135fupdatedDtTm)
    {
        $this->f135fupdatedDtTm = $f135fupdatedDtTm;

        return $this;
    }

    /**
     * Get f135fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF135fupdatedDtTm()
    {
        return $this->f135fupdatedDtTm;
    }

  
}

