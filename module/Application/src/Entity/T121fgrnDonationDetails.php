<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T121fgrnDonationDetails
 *
 * @ORM\Table(name="t121fgrn_donation_details")
 * @ORM\Entity(repositoryClass="Application\Repository\GrnDonationRepository")
 */
class T121fgrnDonationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f121fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f121fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fid_donation", type="integer", nullable=true)
     */
    private $f121fidDonation;

    /**
     * @var string
     *
     * @ORM\Column(name="f121fasset_type", type="string", length=30, nullable=true)
     */
    private $f121fassetType;

    /**
     * @var string
     *
     * @ORM\Column(name="f121ffund_code", type="string", length=100, nullable=true)
     */
    private $f121ffundCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121factivity_code", type="string",length=50, nullable=true)
     */
    private $f121factivityCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fdepartment_code", type="string",length=50, nullable=true)
     */
    private $f121fdepartmentCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121faccount_code", type="string",length=50, nullable=true)
     */
    private $f121faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fquantity", type="integer", nullable=true)
     */
    private $f121fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fprice", type="integer", nullable=true)
     */
    private $f121fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121ftotal", type="integer", nullable=true)
     */
    private $f121ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fstatus", type="integer", nullable=true)
     */
    private $f121fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fcreated_by", type="integer", nullable=true)
     */
    private $f121fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f121fupdated_by", type="integer", nullable=true)
     */
    private $f121fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f121fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f121fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f121fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f121fupdatedDtTm;

    



    /**
     * Get f121fidDetails
     *
     * @return integer
     */
    public function getF121fidDetails()
    {
        return $this->f121fidDetails;
    }

    /**
     * Set f121fidDonation
     *
     * @param integer $f121fidDonation
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fidDonation($f121fidDonation)
    {
        $this->f121fidDonation = $f121fidDonation;

        return $this;
    }

    /**
     * Get f121fidDonation
     *
     * @return integer
     */
    public function getF121fidDonation()
    {
        return $this->f121fidDonation;
    }

    /**
     * Set f121fassetType
     *
     * @param string $f121fassetType
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fassetType($f121fassetType)
    {
        $this->f121fassetType = $f121fassetType;

        return $this;
    }

    /**
     * Get f121fassetType
     *
     * @return string
     */
    public function getF121fassetType()
    {
        return $this->f121fassetType;
    }


    /**
     * Set f121ffundCode
     *
     * @param string $f121ffundCode
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121ffundCode($f121ffundCode)
    {
        $this->f121ffundCode = $f121ffundCode;

        return $this;
    }

    /**
     * Get f121ffundCode
     *
     * @return string
     */
    public function getF121ffundCode()
    {
        return $this->f121ffundCode;
    }


    /**
     * Set f121factivityCode
     *
     * @param integer $f121factivityCode
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121factivityCode($f121factivityCode)
    {
        $this->f121factivityCode = $f121factivityCode;

        return $this;
    }

    /**
     * Get f121factivityCode
     *
     * @return integer
     */
    public function getF121factivityCode()
    {
        return $this->f121factivityCode;
    }


    /**
     * Set f121fdepartmentCode
     *
     * @param string $f121fdepartmentCode
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fdepartmentCode($f121fdepartmentCode)
    {
        $this->f121fdepartmentCode = $f121fdepartmentCode;
        return $this;
    }

    /**
     * Get f121fdepartmentCode
     *
     * @return integer
     */
    public function getF121fdepartmentCode()
    {
        return $this->f121fdepartmentCode;
    }

    /**
     * Set f121faccountCode
     *
     * @param string $f121faccountCode
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121faccountCode($f121faccountCode)
    {
        $this->f121faccountCode = $f121faccountCode;
        return $this;
    }

    /**
     * Get f121faccountCode
     *
     * @return integer
     */
    public function getF121faccountCode()
    {
        return $this->f121faccountCode;
    }

    /**
     * Set f121fquantity
     *
     * @param integer $f121fquantity
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fquantity($f121fquantity)
    {
        $this->f121fquantity = $f121fquantity;

        return $this;
    }

    /**
     * Get f121fquantity
     *
     * @return integer
     */
    public function getF121fquantity()
    {
        return $this->f121fquantity;
    }

    /**
     * Set f121fprice
     *
     * @param integer $f121fprice
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fprice($f121fprice)
    {
        $this->f121fprice = $f121fprice;

        return $this;
    }

    /**
     * Get f121fprice
     *
     * @return integer
     */
    public function getF121fprice()
    {
        return $this->f121fprice;
    }

    /**
     * Set f121ftotal
     *
     * @param integer $f121ftotal
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121ftotal($f121ftotal)
    {
        $this->f121ftotal = $f121ftotal;

        return $this;
    }

    /**
     * Get f121ftotal
     *
     * @return integer
     */
    public function getF121ftotal()
    {
        return $this->f121ftotal;
    }

    /**
     * Set f121fstatus
     *
     * @param integer $f121fstatus
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fstatus($f121fstatus)
    {
        $this->f121fstatus = $f121fstatus;

        return $this;
    }

    /**
     * Get f121fstatus
     *
     * @return integer
     */
    public function getF121fstatus()
    {
        return $this->f121fstatus;
    }

    

    /**
     * Set f121fcreatedBy
     *
     * @param integer $f121fcreatedBy
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fcreatedBy($f121fcreatedBy)
    {
        $this->f121fcreatedBy = $f121fcreatedBy;

        return $this;
    }

    /**
     * Get f121fcreatedBy
     *
     * @return integer
     */
    public function getF121fcreatedBy()
    {
        return $this->f121fcreatedBy;
    }

    /**
     * Set f121fupdatedBy
     *
     * @param integer $f121fupdatedBy
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fupdatedBy($f121fupdatedBy)
    {
        $this->f121fupdatedBy = $f121fupdatedBy;

        return $this;
    }

    /**
     * Get f121fupdatedBy
     *
     * @return integer
     */
    public function getF121fupdatedBy()
    {
        return $this->f121fupdatedBy;
    }

    /**
     * Set f121fcreatedDtTm
     *
     * @param \DateTime $f121fcreatedDtTm
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fcreatedDtTm($f121fcreatedDtTm)
    {
        $this->f121fcreatedDtTm = $f121fcreatedDtTm;

        return $this;
    }

    /**
     * Get f121fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF121fcreatedDtTm()
    {
        return $this->f121fcreatedDtTm;
    }

    /**
     * Set f121fupdatedDtTm
     *
     * @param \DateTime $f121fupdatedDtTm
     *
     * @return T121fgrnDonationDetails
     */
    public function setF121fupdatedDtTm($f121fupdatedDtTm)
    {
        $this->f121fupdatedDtTm = $f121fupdatedDtTm;

        return $this;
    }

    /**
     * Get f121fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF121fupdatedDtTm()
    {
        return $this->f121fupdatedDtTm;
    }
}
