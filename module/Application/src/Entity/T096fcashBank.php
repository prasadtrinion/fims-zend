<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T096fcashBank
 *
 * @ORM\Table(name="t096fcash_bank")
 * @ORM\Entity(repositoryClass="Application\Repository\CashBankRepository")
 */
class T096fcashBank
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f096fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f096fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fid_bank", type="integer", nullable=true)
     */
    private $f096fidBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fid_payment", type="integer", nullable=true)
     */
    private $f096fidPayment;
     /**
     * @var string
     *
     * @ORM\Column(name="f096ftype", type="string", length=200, nullable=true)
     */
    private $f096ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096famount", type="integer",  nullable=true)
     */
    private $f096famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fstatus", type="integer", nullable=true)
     */
    private $f096fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f096ftransaction_date", type="datetime", nullable=true)
     */
    private $f096ftransactionDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fcreated_by", type="integer", nullable=true)
     */
    private $f096fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fupdated_by", type="integer", nullable=true)
     */
    private $f096fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f096fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f096fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f096fupdated_dt_m", type="datetime", nullable=true)
     */
    private $f096fupdatedDtM;

    /**
     * @var integer
     *
     * @ORM\Column(name="f096fapproval_status", type="integer", nullable=true)
     */
    private $f096fapprovalStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f096freason", type="string",length=50, nullable=true)
     */
    private $f096freason='NULL';



    /**
     * Get f096fid
     *
     * @return integer
     */
    public function getF096fid()
    {
        return $this->f096fid;
    }

    /**
     * Set f096fidBank
     *
     * @param integer $f096fidBank
     *
     * @return T096fcashBank
     */
    public function setF096fidBank($f096fidBank)
    {
        $this->f096fidBank = $f096fidBank;

        return $this;
    }

    /**
     * Get f096fidBank
     *
     * @return integer
     */
    public function getF096fidBank()
    {
        return $this->f096fidBank;
    }

    /**
     * Set f096fidPayment
     *
     * @param integer $f096fidPayment
     *
     * @return T096fcashBank
     */
    public function setF096fidPayment($f096fidPayment)
    {
        $this->f096fidPayment = $f096fidPayment;

        return $this;
    }

    /**
     * Get f096fidPayment
     *
     * @return integer
     */
    public function getF096fidPayment()
    {
        return $this->f096fidPayment;
    }

    /**
     * Set f096famount
     *
     * @param integer $f096famount
     *
     * @return T096fcashBank
     */
    public function setF096famount($f096famount)
    {
        $this->f096famount = $f096famount;

        return $this;
    }

    /**
     * Get f096famount
     *
     * @return integer
     */
    public function getF096famount()
    {
        return $this->f096famount;
    }

    /**
     * Set f096fstatus
     *
     * @param integer $f096fstatus
     *
     * @return T096fcashBank
     */
    public function setF096fstatus($f096fstatus)
    {
        $this->f096fstatus = $f096fstatus;

        return $this;
    }

    /**
     * Get f096fstatus
     *
     * @return integer
     */
    public function getF096fstatus()
    {
        return $this->f096fstatus;
    }

    /**
     * Set f096fcreatedBy
     *
     * @param integer $f096fcreatedBy
     *
     * @return T096fcashBank
     */
    public function setF096fcreatedBy($f096fcreatedBy)
    {
        $this->f096fcreatedBy = $f096fcreatedBy;

        return $this;
    }

    /**
     * Get f096fcreatedBy
     *
     * @return integer
     */
    public function getF096fcreatedBy()
    {
        return $this->f096fcreatedBy;
    }

    /**
     * Set f096fupdatedBy
     *
     * @param integer $f096fupdatedBy
     *
     * @return T096fcashBank
     */
    public function setF096fupdatedBy($f096fupdatedBy)
    {
        $this->f096fupdatedBy = $f096fupdatedBy;

        return $this;
    }

    /**
     * Get f096fupdatedBy
     *
     * @return integer
     */
    public function getF096fupdatedBy()
    {
        return $this->f096fupdatedBy;
    }

    /**
     * Set f096fcreatedDtTm
     *
     * @param \DateTime $f096fcreatedDtTm
     *
     * @return T096fcashBank
     */
    public function setF096fcreatedDtTm($f096fcreatedDtTm)
    {
        $this->f096fcreatedDtTm = $f096fcreatedDtTm;

        return $this;
    }

    /**
     * Get f096fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF096fcreatedDtTm()
    {
        return $this->f096fcreatedDtTm;
    }

    /**
     * Set f096fupdatedDtM
     *
     * @param \DateTime $f096fupdatedDtM
     *
     * @return T096fcashBank
     */
    public function setF096fupdatedDtM($f096fupdatedDtM)
    {
        $this->f096fupdatedDtM = $f096fupdatedDtM;

        return $this;
    }

    /**
     * Get f096fupdatedDtM
     *
     * @return \DateTime
     */
    public function getF096fupdatedDtM()
    {
        return $this->f096fupdatedDtM;
    }

     /**
     * Set f096ftype
     *
     * @param string $f096ftype
     *
     * @return T096fcashBank
     */
    public function setF096ftype($f096ftype)
    {
        $this->f096ftype = $f096ftype;

        return $this;
    }

    /**
     * Get f096ftype
     *
     * @return string
     */
    public function getF096ftype()
    {
        return $this->f096ftype;
    }

     /**
     * Set f096ftransactionDate
     *
     * @param \DateTime $f096ftransactionDate
     *
     * @return T096fcashBank
     */
    public function setF096ftransactionDate($f096ftransactionDate)
    {
        $this->f096ftransactionDate = $f096ftransactionDate;

        return $this;
    }

    /**
     * Get f096ftransactionDate
     *
     * @return \DateTime
     */
    public function getF096ftransactionDate()
    {
        return $this->f096ftransactionDate;
    }

    /**
     * Set f096fapprovalStatus
     *
     * @param integer $f096fapprovalStatus
     *
     * @return T096fcashBank
     */
    public function setF096fapprovalStatus($f096fapprovalStatus)
    {
        $this->f096fapprovalStatus = $f096fapprovalStatus;

        return $this;
    }

    /**
     * Get f096fapprovalStatus
     *
     * @return integer
     */
    public function getF096fapprovalStatus()
    {
        return $this->f096fapprovalStatus;
    }

    /**
     * Set f096freason
     *
     * @param string $f096freason
     *
     * @return T096fcashBank
     */
    public function setF096freason($f096freason)
    {
        $this->f096freason = $f096freason;

        return $this;
    }

    /**
     * Get f096freason
     *
     * @return string
     */
    public function getF096freason()
    {
        return $this->f096freason;
    }
}
