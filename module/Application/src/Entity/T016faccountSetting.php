<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T016faccountSetting
 *
 * @ORM\Table(name="t016faccount_setting", indexes={@ORM\Index(name="f016fupdated_by", columns={"f016fupdated_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AccountSettingRepository")
 */
class T016faccountSetting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f016fid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f016fid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fsub_acc_under_ledger", type="boolean", nullable=false)
     */
    private $f016fsubAccUnderLedger;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fenable_tds", type="boolean", nullable=false)
     */
    private $f016fenableTds;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fallow_cheque_printing", type="boolean", nullable=false)
     */
    private $f016fallowChequePrinting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fbudget_yn", type="boolean", nullable=false)
     */
    private $f016fbudgetYn;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fmqy_budgeting", type="boolean", nullable=true)
     */
    private $f016fmqyBudgeting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f016fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f016fupdatedDtTm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f016fstatus", type="boolean", nullable=false)
     */
    private $f016fstatus = '1';

    /**
     * @var \Application\Entity\T014fuser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T014fuser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f016fupdated_by", referencedColumnName="f014fid")
     * })
     */
    private $f016fupdatedBy;



    /**
     * Get f016fid
     *
     * @return integer
     */
    public function getF016fid()
    {
        return $this->f016fid;
    }

    /**
     * Set f016fsubAccUnderLedger
     *
     * @param boolean $f016fsubAccUnderLedger
     *
     * @return T016faccountSetting
     */
    public function setF016fsubAccUnderLedger($f016fsubAccUnderLedger)
    {
        $this->f016fsubAccUnderLedger = $f016fsubAccUnderLedger;

        return $this;
    }

    /**
     * Get f016fsubAccUnderLedger
     *
     * @return boolean
     */
    public function getF016fsubAccUnderLedger()
    {
        return $this->f016fsubAccUnderLedger;
    }

    /**
     * Set f016fenableTds
     *
     * @param boolean $f016fenableTds
     *
     * @return T016faccountSetting
     */
    public function setF016fenableTds($f016fenableTds)
    {
        $this->f016fenableTds = $f016fenableTds;

        return $this;
    }

    /**
     * Get f016fenableTds
     *
     * @return boolean
     */
    public function getF016fenableTds()
    {
        return $this->f016fenableTds;
    }

    /**
     * Set f016fallowChequePrinting
     *
     * @param boolean $f016fallowChequePrinting
     *
     * @return T016faccountSetting
     */
    public function setF016fallowChequePrinting($f016fallowChequePrinting)
    {
        $this->f016fallowChequePrinting = $f016fallowChequePrinting;

        return $this;
    }

    /**
     * Get f016fallowChequePrinting
     *
     * @return boolean
     */
    public function getF016fallowChequePrinting()
    {
        return $this->f016fallowChequePrinting;
    }

    /**
     * Set f016fbudgetYn
     *
     * @param boolean $f016fbudgetYn
     *
     * @return T016faccountSetting
     */
    public function setF016fbudgetYn($f016fbudgetYn)
    {
        $this->f016fbudgetYn = $f016fbudgetYn;

        return $this;
    }

    /**
     * Get f016fbudgetYn
     *
     * @return boolean
     */
    public function getF016fbudgetYn()
    {
        return $this->f016fbudgetYn;
    }

    /**
     * Set f016fmqyBudgeting
     *
     * @param boolean $f016fmqyBudgeting
     *
     * @return T016faccountSetting
     */
    public function setF016fmqyBudgeting($f016fmqyBudgeting)
    {
        $this->f016fmqyBudgeting = $f016fmqyBudgeting;

        return $this;
    }

    /**
     * Get f016fmqyBudgeting
     *
     * @return boolean
     */
    public function getF016fmqyBudgeting()
    {
        return $this->f016fmqyBudgeting;
    }

    /**
     * Set f016fupdatedDtTm
     *
     * @param \DateTime $f016fupdatedDtTm
     *
     * @return T016faccountSetting
     */
    public function setF016fupdatedDtTm($f016fupdatedDtTm)
    {
        $this->f016fupdatedDtTm = $f016fupdatedDtTm;

        return $this;
    }

    /**
     * Get f016fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF016fupdatedDtTm()
    {
        return $this->f016fupdatedDtTm;
    }

    /**
     * Set f016fstatus
     *
     * @param boolean $f016fstatus
     *
     * @return T016faccountSetting
     */
    public function setF016fstatus($f016fstatus)
    {
        $this->f016fstatus = $f016fstatus;

        return $this;
    }

    /**
     * Get f016fstatus
     *
     * @return boolean
     */
    public function getF016fstatus()
    {
        return $this->f016fstatus;
    }

    /**
     * Set f016fupdatedBy
     *
     * @param \Application\Entity\T014fuser $f016fupdatedBy
     *
     * @return T016faccountSetting
     */
    public function setF016fupdatedBy(\Application\Entity\T014fuser $f016fupdatedBy = null)
    {
        $this->f016fupdatedBy = $f016fupdatedBy;

        return $this;
    }

    /**
     * Get f016fupdatedBy
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF016fupdatedBy()
    {
        return $this->f016fupdatedBy;
    }
}
