<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T081fsalaryEntry
 *
 * @ORM\Table(name="t081fsalary_entry", uniqueConstraints={@ORM\UniqueConstraint(name="f081fid_staff", columns={"f081fid_staff"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SalaryEntryRepository")
 */
class T081fsalaryEntry  
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f081fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid_staff", type="integer", nullable=false)
     */
    private $f081fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fpayment_type", type="string", length=25, nullable=false)
     */
    private $f081fpaymentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstatus", type="integer", nullable=false)
     */
    private $f081fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f081ftype", type="string", length=25, nullable=false)
     */
    private $f081ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fcreated_by", type="integer", nullable=false)
     */
    private $f081fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fupdated_by", type="integer", nullable=false)
     */
    private $f081fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f081fupdatedDtTm;



    /**
     * Get f081fid
     *
     * @return integer
     */
    public function getF081fid()
    {
        return $this->f081fid;
    }

    /**
     * Set f081fidStaff
     *
     * @param integer $f081fidStaff
     *
     * @return T081fsalaryEntry
     */
    public function setF081fidStaff($f081fidStaff)
    {
        $this->f081fidStaff = $f081fidStaff;

        return $this;
    }

    /**
     * Get f081fidStaff
     *
     * @return integer
     */
    public function getF081fidStaff()
    {
        return $this->f081fidStaff;
    }

    /**
     * Set f081fpaymentType
     *
     * @param string $f081fpaymentType
     *
     * @return T081fsalaryEntry
     */
    public function setF081fpaymentType($f081fpaymentType)
    {
        $this->f081fpaymentType = $f081fpaymentType;

        return $this;
    }

    /**
     * Get f081fpaymentType
     *
     * @return string
     */
    public function getF081fpaymentType()
    {
        return $this->f081fpaymentType;
    }

    /**
     * Set f081fstatus
     *
     * @param integer $f081fstatus
     *
     * @return T081fsalaryEntry
     */
    public function setF081fstatus($f081fstatus)
    {
        $this->f081fstatus = $f081fstatus;

        return $this;
    }

    /**
     * Get f081fstatus
     *
     * @return integer
     */
    public function getF081fstatus()
    {
        return $this->f081fstatus;
    }

    /**
     * Set f081ftype
     *
     * @param string $f081ftype
     *
     * @return T081fsalaryEntry
     */
    public function setF081ftype($f081ftype)
    {
        $this->f081ftype = $f081ftype;

        return $this;
    }

    /**
     * Get f081ftype
     *
     * @return string
     */
    public function getF081ftype()
    {
        return $this->f081ftype;
    }

    /**
     * Set f081fcreatedBy
     *
     * @param integer $f081fcreatedBy
     *
     * @return T081fsalaryEntry
     */
    public function setF081fcreatedBy($f081fcreatedBy)
    {
        $this->f081fcreatedBy = $f081fcreatedBy;

        return $this;
    }

    /**
     * Get f081fcreatedBy
     *
     * @return integer
     */
    public function getF081fcreatedBy()
    {
        return $this->f081fcreatedBy;
    }

    /**
     * Set f081fupdatedBy
     *
     * @param integer $f081fupdatedBy
     *
     * @return T081fsalaryEntry
     */
    public function setF081fupdatedBy($f081fupdatedBy)
    {
        $this->f081fupdatedBy = $f081fupdatedBy;

        return $this;
    }

    /**
     * Get f081fupdatedBy
     *
     * @return integer
     */
    public function getF081fupdatedBy()
    {
        return $this->f081fupdatedBy;
    }

    /**
     * Set f081fcreatedDtTm
     *
     * @param \DateTime $f081fcreatedDtTm
     *
     * @return T081fsalaryEntry
     */
    public function setF081fcreatedDtTm($f081fcreatedDtTm)
    {
        $this->f081fcreatedDtTm = $f081fcreatedDtTm;

        return $this;
    }

    /**
     * Get f081fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fcreatedDtTm()
    {
        return $this->f081fcreatedDtTm;
    }

    /**
     * Set f081fupdatedDtTm
     *
     * @param \DateTime $f081fupdatedDtTm
     *
     * @return T081fsalaryEntry
     */
    public function setF081fupdatedDtTm($f081fupdatedDtTm)
    {
        $this->f081fupdatedDtTm = $f081fupdatedDtTm;

        return $this;
    }

    /**
     * Get f081fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fupdatedDtTm()
    {
        return $this->f081fupdatedDtTm;
    }
}
