<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T016fopeningbalance
 *
 * @ORM\Table(name="t016fopeningbalance", indexes={@ORM\Index(name="IDX_C459A192CE8C65BF", columns={"f016fid_financialyear"})})
 * @ORM\Entity(repositoryClass="Application\Repository\OpeningbalanceRepository")
 */
class T016fopeningbalance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f016fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f016fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fdr_amount", type="integer", nullable=false)
     */
    private $f016fdrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fcr_amount", type="integer", nullable=false)
     */
    private $f016fcrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fstatus", type="integer", nullable=false)
     */
    private $f016fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fcreated_by", type="integer", nullable=false)
     */
    private $f016fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fupdated_by", type="integer", nullable=false)
     */
    private $f016fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f016fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f016fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f016fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f016fupdateDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fid_financialyear", type="integer", nullable=false)
     */
    private $f016fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fbalance_dr_amount", type="integer", nullable=true)
     */
    private $f016fbalanceDrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fbalance_cr_amount", type="integer", nullable=true)
     */
    private $f016fbalanceCrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f016fapproved_status", type="integer", nullable=true)
     */
    private $f016fapprovedStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f016ffund", type="string", length=100, nullable=true)
     */
    private $f016ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f016factivity_code", type="string", length=100, nullable=true)
     */
    private $f016factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f016fdepartment", type="string", length=100, nullable=true)
     */
    private $f016fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f016faccount_code", type="string", length=100, nullable=true)
     */
    private $f016faccountCode;



    /**
     * Get f016fid
     *
     * @return integer
     */
    public function getF016fid()
    {
        return $this->f016fid;
    }

    /**
     * Set f016fdrAmount
     *
     * @param integer $f016fdrAmount
     *
     * @return T016fopeningbalance
     */
    public function setF016fdrAmount($f016fdrAmount)
    {
        $this->f016fdrAmount = $f016fdrAmount;

        return $this;
    }

    /**
     * Get f016fdrAmount
     *
     * @return integer
     */
    public function getF016fdrAmount()
    {
        return $this->f016fdrAmount;
    }

    /**
     * Set f016fcrAmount
     *
     * @param integer $f016fcrAmount
     *
     * @return T016fopeningbalance
     */
    public function setF016fcrAmount($f016fcrAmount)
    {
        $this->f016fcrAmount = $f016fcrAmount;

        return $this;
    }

    /**
     * Get f016fcrAmount
     *
     * @return integer
     */
    public function getF016fcrAmount()
    {
        return $this->f016fcrAmount;
    }

    /**
     * Set f016fstatus
     *
     * @param integer $f016fstatus
     *
     * @return T016fopeningbalance
     */
    public function setF016fstatus($f016fstatus)
    {
        $this->f016fstatus = $f016fstatus;

        return $this;
    }

    /**
     * Get f016fstatus
     *
     * @return integer
     */
    public function getF016fstatus()
    {
        return $this->f016fstatus;
    }

    /**
     * Set f016fcreatedBy
     *
     * @param integer $f016fcreatedBy
     *
     * @return T016fopeningbalance
     */
    public function setF016fcreatedBy($f016fcreatedBy)
    {
        $this->f016fcreatedBy = $f016fcreatedBy;

        return $this;
    }

    /**
     * Get f016fcreatedBy
     *
     * @return integer
     */
    public function getF016fcreatedBy()
    {
        return $this->f016fcreatedBy;
    }

    /**
     * Set f016fupdatedBy
     *
     * @param integer $f016fupdatedBy
     *
     * @return T016fopeningbalance
     */
    public function setF016fupdatedBy($f016fupdatedBy)
    {
        $this->f016fupdatedBy = $f016fupdatedBy;

        return $this;
    }

    /**
     * Get f016fupdatedBy
     *
     * @return integer
     */
    public function getF016fupdatedBy()
    {
        return $this->f016fupdatedBy;
    }

    /**
     * Set f016fcreateDtTm
     *
     * @param \DateTime $f016fcreateDtTm
     *
     * @return T016fopeningbalance
     */
    public function setF016fcreateDtTm($f016fcreateDtTm)
    {
        $this->f016fcreateDtTm = $f016fcreateDtTm;

        return $this;
    }

    /**
     * Get f016fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF016fcreateDtTm()
    {
        return $this->f016fcreateDtTm;
    }

    /**
     * Set f016fupdateDtTm
     *
     * @param \DateTime $f016fupdateDtTm
     *
     * @return T016fopeningbalance
     */
    public function setF016fupdateDtTm($f016fupdateDtTm)
    {
        $this->f016fupdateDtTm = $f016fupdateDtTm;

        return $this;
    }

    /**
     * Get f016fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF016fupdateDtTm()
    {
        return $this->f016fupdateDtTm;
    }

    /**
     * Set f016fidFinancialyear
     *
     * @param integer $f016fidFinancialyear
     *
     * @return T016fopeningbalance
     */
    public function setF016fidFinancialyear($f016fidFinancialyear)
    {
        $this->f016fidFinancialyear = $f016fidFinancialyear;

        return $this;
    }

    /**
     * Get f016fidFinancialyear
     *
     * @return integer
     */
    public function getF016fidFinancialyear()
    {
        return $this->f016fidFinancialyear;
    }

    /**
     * Set f016fbalanceDrAmount
     *
     * @param integer $f016fbalanceDrAmount
     *
     * @return T016fopeningbalance
     */
    public function setF016fbalanceDrAmount($f016fbalanceDrAmount)
    {
        $this->f016fbalanceDrAmount = $f016fbalanceDrAmount;

        return $this;
    }

    /**
     * Get f016fbalanceDrAmount
     *
     * @return integer
     */
    public function getF016fbalanceDrAmount()
    {
        return $this->f016fbalanceDrAmount;
    }

    /**
     * Set f016fbalanceCrAmount
     *
     * @param integer $f016fbalanceCrAmount
     *
     * @return T016fopeningbalance
     */
    public function setF016fbalanceCrAmount($f016fbalanceCrAmount)
    {
        $this->f016fbalanceCrAmount = $f016fbalanceCrAmount;

        return $this;
    }

    /**
     * Get f016fbalanceCrAmount
     *
     * @return integer
     */
    public function getF016fbalanceCrAmount()
    {
        return $this->f016fbalanceCrAmount;
    }

    /**
     * Set f016fapprovedStatus
     *
     * @param integer $f016fapprovedStatus
     *
     * @return T016fopeningbalance
     */
    public function setF016fapprovedStatus($f016fapprovedStatus)
    {
        $this->f016fapprovedStatus = $f016fapprovedStatus;

        return $this;
    }

    /**
     * Get f016fapprovedStatus
     *
     * @return integer
     */
    public function getF016fapprovedStatus()
    {
        return $this->f016fapprovedStatus;
    }

    /**
     * Set f016ffund
     *
     * @param string $f016ffund
     *
     * @return T016fopeningbalance
     */
    public function setF016ffund($f016ffund)
    {
        $this->f016ffund = $f016ffund;

        return $this;
    }

    /**
     * Get f016ffund
     *
     * @return string
     */
    public function getF016ffund()
    {
        return $this->f016ffund;
    }

    /**
     * Set f016factivityCode
     *
     * @param string $f016factivityCode
     *
     * @return T016fopeningbalance
     */
    public function setF016factivityCode($f016factivityCode)
    {
        $this->f016factivityCode = $f016factivityCode;

        return $this;
    }

    /**
     * Get f016factivityCode
     *
     * @return string
     */
    public function getF016factivityCode()
    {
        return $this->f016factivityCode;
    }

    /**
     * Set f016fdepartment
     *
     * @param string $f016fdepartment
     *
     * @return T016fopeningbalance
     */
    public function setF016fdepartment($f016fdepartment)
    {
        $this->f016fdepartment = $f016fdepartment;

        return $this;
    }

    /**
     * Get f016fdepartment
     *
     * @return string
     */
    public function getF016fdepartment()
    {
        return $this->f016fdepartment;
    }

    /**
     * Set f016faccountCode
     *
     * @param string $f016faccountCode
     *
     * @return T016fopeningbalance
     */
    public function setF016faccountCode($f016faccountCode)
    {
        $this->f016faccountCode = $f016faccountCode;

        return $this;
    }

    /**
     * Get f016faccountCode
     *
     * @return string
     */
    public function getF016faccountCode()
    {
        return $this->f016faccountCode;
    }
}
