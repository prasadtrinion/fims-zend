<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T040fdebitSetup
 *
 * @ORM\Table(name="t040fdebit_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\CreditSetupRepository")
 */
class T040fdebitSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f040fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f040fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f040fname", type="string", length=50, nullable=true)
     */
    private $f040fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f040fid_glcode", type="integer", nullable=true)
     */
    private $f040fidGlcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f040fstatus", type="integer", nullable=false)
     */
    private $f040fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f040fcreated_by", type="integer", nullable=true)
     */
    private $f040fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f040fupdated_by", type="integer", nullable=true)
     */
    private $f040fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f040fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f040fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f040fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f040fupdatedDtTm;



    /**
     * Get f040fid
     *
     * @return integer
     */
    public function getF040fid()
    {
        return $this->f040fid;
    }

    /**
     * Set f040fname
     *
     * @param string $f040fname
     *
     * @return T040fdebitSetup
     */
    public function setF040fname($f040fname)
    {
        $this->f040fname = $f040fname;

        return $this;
    }

    /**
     * Get f040fname
     *
     * @return string
     */
    public function getF040fname()
    {
        return $this->f040fname;
    }

    /**
     * Set f040fstatus
     *
     * @param integer $f040fstatus
     *
     * @return T040fdebitSetup
     */
    public function setF040fstatus($f040fstatus)
    {
        $this->f040fstatus = $f040fstatus;

        return $this;
    }

    /**
     * Get f040fstatus
     *
     * @return integer
     */
    public function getF040fstatus()
    {
        return $this->f040fstatus;
    }

    /**
     * Set f040fcreatedBy
     *
     * @param integer $f040fcreatedBy
     *
     * @return T040fdebitSetup
     */
    public function setF040fcreatedBy($f040fcreatedBy)
    {
        $this->f040fcreatedBy = $f040fcreatedBy;

        return $this;
    }

    /**
     * Get f040fcreatedBy
     *
     * @return integer
     */
    public function getF040fcreatedBy()
    {
        return $this->f040fcreatedBy;
    }

    /**
     * Set f040fupdatedBy
     *
     * @param integer $f040fupdatedBy
     *
     * @return T040fdebitSetup
     */
    public function setF040fupdatedBy($f040fupdatedBy)
    {
        $this->f040fupdatedBy = $f040fupdatedBy;

        return $this;
    }

    /**
     * Get f040fupdatedBy
     *
     * @return integer
     */
    public function getF040fupdatedBy()
    {
        return $this->f040fupdatedBy;
    }

    /**
     * Set f040fcreatedDtTm
     *
     * @param \DateTime $f040fcreatedDtTm
     *
     * @return T040fdebitSetup
     */
    public function setF040fcreatedDtTm($f040fcreatedDtTm)
    {
        $this->f040fcreatedDtTm = $f040fcreatedDtTm;

        return $this;
    }

    /**
     * Get f040fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF040fcreatedDtTm()
    {
        return $this->f040fcreatedDtTm;
    }

    /**
     * Set f040fupdatedDtTm
     *
     * @param \DateTime $f040fupdatedDtTm
     *
     * @return T040fdebitSetup
     */
    public function setF040fupdatedDtTm($f040fupdatedDtTm)
    {
        $this->f040fupdatedDtTm = $f040fupdatedDtTm;

        return $this;
    }

    /**
     * Get f040fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF040fupdatedDtTm()
    {
        return $this->f040fupdatedDtTm;
    }
}
