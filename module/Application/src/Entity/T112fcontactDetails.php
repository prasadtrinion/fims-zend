<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T112fcontactDetails
 *
 * @ORM\Table(name="t112fcontact_details")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T112fcontactDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f112fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f112fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f112fid_vendor", type="integer", nullable=true)
     */
    private $f112fidVendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f112fcontact_person", type="string", length=50, nullable=true)
     */
    private $f112fcontactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="f112fic_number", type="string", length=20, nullable=true)
     */
    private $f112ficNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f112femail", type="string", length=25, nullable=true)
     */
    private $f112femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f112fcontact_number", type="string", length=20, nullable=true)
     */
    private $f112fcontactNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f112fstatus", type="integer", nullable=true)
     */
    private $f112fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f112fcreated_by", type="integer", nullable=true)
     */
    private $f112fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f112fupdated_by", type="integer", nullable=true)
     */
    private $f112fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f112fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f112fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f112fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f112fupdatedDtTm;


    /**
     * @var string
     *
     * @ORM\Column(name="f112fid_type", type="string", length =255, nullable=true)
     */
    private $f112fidType;


    /**
     * Get f112fid
     *
     * @return integer
     */
    public function getF112fid()
    {
        return $this->f112fid;
    }

    /**
     * Set f112fcontactPerson
     *
     * @param string $f112fcontactPerson
     *
     * @return T112fcontactDetails
     */
    public function setF112fcontactPerson($f112fcontactPerson)
    {
        $this->f112fcontactPerson = $f112fcontactPerson;

        return $this;
    }

    /**
     * Get f112fcontactPerson
     *
     * @return string
     */
    public function getF112fcontactPerson()
    {
        return $this->f112fcontactPerson;
    }

    /**
     * Set f112ficNumber
     *
     * @param string $f112ficNumber
     *
     * @return T112fcontactDetails
     */
    public function setF112ficNumber($f112ficNumber)
    {
        $this->f112ficNumber = $f112ficNumber;

        return $this;
    }

    /**
     * Get f112ficNumber
     *
     * @return string
     */
    public function getF112ficNumber()
    {
        return $this->f112ficNumber;
    }

    /**
     * Set f112femail
     *
     * @param string $f112femail
     *
     * @return T112fcontactDetails
     */
    public function setF112femail($f112femail)
    {
        $this->f112femail = $f112femail;

        return $this;
    }

    /**
     * Get f112femail
     *
     * @return string
     */
    public function getF112femail()
    {
        return $this->f112femail;
    }

    /**
     * Set f112fcontactNumber
     *
     * @param string $f112fcontactNumber
     *
     * @return T112fcontactDetails
     */
    public function setF112fcontactNumber($f112fcontactNumber)
    {
        $this->f112fcontactNumber = $f112fcontactNumber;

        return $this;
    }

    /**
     * Get f112fcontactNumber
     *
     * @return string
     */
    public function getF112fcontactNumber()
    {
        return $this->f112fcontactNumber;
    }

    /**
     * Set f112fidVendor
     *
     * @param integer $f112fidVendor
     *
     * @return T112fcontactDetails
     */
    public function setF112fidVendor($f112fidVendor)
    {
        $this->f112fidVendor = $f112fidVendor;

        return $this;
    }

    /**
     * Get f112fidVendor
     *
     * @return integer
     */
    public function getF112fidVendor()
    {
        return $this->f112fidVendor;
    }

    /**
     * Set f112fstatus
     *
     * @param integer $f112fstatus
     *
     * @return T112fcontactDetails
     */
    public function setF112fstatus($f112fstatus)
    {
        $this->f112fstatus = $f112fstatus;

        return $this;
    }

    /**
     * Get f112fstatus
     *
     * @return integer
     */
    public function getF112fstatus()
    {
        return $this->f112fstatus;
    }

    /**
     * Set f112fcreatedBy
     *
     * @param integer $f112fcreatedBy
     *
     * @return T112fcontactDetails
     */
    public function setF112fcreatedBy($f112fcreatedBy)
    {
        $this->f112fcreatedBy = $f112fcreatedBy;

        return $this;
    }

    /**
     * Get f112fcreatedBy
     *
     * @return integer
     */
    public function getF112fcreatedBy()
    {
        return $this->f112fcreatedBy;
    }

    /**
     * Set f112fupdatedBy
     *
     * @param integer $f112fupdatedBy
     *
     * @return T112fcontactDetails
     */
    public function setF112fupdatedBy($f112fupdatedBy)
    {
        $this->f112fupdatedBy = $f112fupdatedBy;

        return $this;
    }

    /**
     * Get f112fupdatedBy
     *
     * @return integer
     */
    public function getF112fupdatedBy()
    {
        return $this->f112fupdatedBy;
    }

    /**
     * Set f112fcreatedDtTm
     *
     * @param \DateTime $f112fcreatedDtTm
     *
     * @return T112fcontactDetails
     */
    public function setF112fcreatedDtTm($f112fcreatedDtTm)
    {
        $this->f112fcreatedDtTm = $f112fcreatedDtTm;

        return $this;
    }

    /**
     * Get f112fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF112fcreatedDtTm()
    {
        return $this->f112fcreatedDtTm;
    }


    /**
     * Set f112fupdatedDtTm
     *
     * @param \DateTime $f112fupdatedDtTm
     *
     * @return T112fcontactDetails
     */
    public function setF112fupdatedDtTm($f112fupdatedDtTm)
    {
        $this->f112fupdatedDtTm = $f112fupdatedDtTm;

        return $this;
    }

    /**
     * Get f112fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF112fupdatedDtTm()
    {
        return $this->f112fupdatedDtTm;
    }


    /**
     * Set f112fidType
     *
     * @param \DateTime $f112fidType
     *
     * @return T112fcontactDetails
     */
    public function setF112fidType($f112fidType)
    {
        $this->f112fidType = $f112fidType;

        return $this;
    }

    /**
     * Get f112fidType
     *
     * @return \DateTime
     */
    public function getF112fidType()
    {
        return $this->f112fidType;
    }
}
