<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T085fassetInfoDetails
 *
 * @ORM\Table(name="t085fasset_info_details")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetInformationRepository")
 */
class T085fassetInfoDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f085fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_asset_info", type="integer", nullable=true)
     */
    private $f085fidAssetInfo;

   

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fstatus", type="integer", nullable=true)
     */
    private $f085fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcreated_by", type="integer", nullable=true)
     */
    private $f085fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fupdated_by", type="integer", nullable=true)
     */
    private $f085fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fcreated_dt_tm", type="date", nullable=true)
     */
    private $f085fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fupdated_dt_tm", type="date", nullable=true)
     */
    private $f085fupdatedDtTm;



    /**
     * Get f085fidDetails
     *
     * @return integer
     */
    public function getF085fidDetails()
    {
        return $this->f085fidDetails;
    }

    /**
     * Set f085fidAssetInfo
     *
     * @param integer $f085fidAssetInfo
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fidAssetInfo($f085fidAssetInfo)
    {
        $this->f085fidAssetInfo = $f085fidAssetInfo;

        return $this;
    }

    /**
     * Get f085fidAssetInfo
     *
     * @return integer
     */
    public function getF085fidAssetInfo()
    {
        return $this->f085fidAssetInfo;
    }

    

    /**
     * Set f085fstatus
     *
     * @param integer $f085fstatus
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fstatus($f085fstatus)
    {
        $this->f085fstatus = $f085fstatus;

        return $this;
    }

    /**
     * Get f085fstatus
     *
     * @return integer
     */
    public function getF085fstatus()
    {
        return $this->f085fstatus;
    }

    /**
     * Set f085fcreatedBy
     *
     * @param integer $f085fcreatedBy
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcreatedBy($f085fcreatedBy)
    {
        $this->f085fcreatedBy = $f085fcreatedBy;

        return $this;
    }

    /**
     * Get f085fcreatedBy
     *
     * @return integer
     */
    public function getF085fcreatedBy()
    {
        return $this->f085fcreatedBy;
    }

    /**
     * Set f085fupdatedBy
     *
     * @param integer $f085fupdatedBy
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fupdatedBy($f085fupdatedBy)
    {
        $this->f085fupdatedBy = $f085fupdatedBy;

        return $this;
    }

    /**
     * Get f085fupdatedBy
     *
     * @return integer
     */
    public function getF085fupdatedBy()
    {
        return $this->f085fupdatedBy;
    }

    /**
     * Set f085fcreatedDtTm
     *
     * @param \DateTime $f085fcreatedDtTm
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fcreatedDtTm($f085fcreatedDtTm)
    {
        $this->f085fcreatedDtTm = $f085fcreatedDtTm;

        return $this;
    }

    /**
     * Get f085fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF085fcreatedDtTm()
    {
        return $this->f085fcreatedDtTm;
    }

    /**
     * Set f085fupdatedDtTm
     *
     * @param \DateTime $f085fupdatedDtTm
     *
     * @return T085fassetInfoDetails
     */
    public function setF085fupdatedDtTm($f085fupdatedDtTm)
    {
        $this->f085fupdatedDtTm = $f085fupdatedDtTm;

        return $this;
    }

    /**
     * Get f085fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF085fupdatedDtTm()
    {
        return $this->f085fupdatedDtTm;
    }
}
