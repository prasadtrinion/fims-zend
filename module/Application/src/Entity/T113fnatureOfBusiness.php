<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T113fnatureOfBusiness
 *
 * @ORM\Table(name="t113fnature_of_business")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T113fnatureOfBusiness
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f113fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f113fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f113fid_vendor", type="integer", nullable=true)
     */
    private $f113fidVendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f113fnature_of_business", type="string", length=50, nullable=true)
     */
    private $f113fnatureOfBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="f113fcategory", type="string", length=50, nullable=true)
     */
    private $f113fcategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f113fsub_category", type="string", length=50, nullable=true)
     */
    private $f113fsubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f113fstatus", type="integer", nullable=true)
     */
    private $f113fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f113fcreated_by", type="integer", nullable=true)
     */
    private $f113fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f113fupdated_by", type="integer", nullable=true)
     */
    private $f113fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f113fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f113fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f113fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f113fupdatedDtTm;



    /**
     * Get f113fid
     *
     * @return integer
     */
    public function getF113fid()
    {
        return $this->f113fid;
    }

    /**
     * Set f113fidVendor
     *
     * @param integer $f113fidVendor
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fidVendor($f113fidVendor)
    {
        $this->f113fidVendor = $f113fidVendor;

        return $this;
    }

    /**
     * Get f113fidVendor
     *
     * @return integer
     */
    public function getF113fidVendor()
    {
        return $this->f113fidVendor;
    }

    /**
     * Set f113fnatureOfBusiness
     *
     * @param string $f113fnatureOfBusiness
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fnatureOfBusiness($f113fnatureOfBusiness)
    {
        $this->f113fnatureOfBusiness = $f113fnatureOfBusiness;

        return $this;
    }

    /**
     * Get f113fnatureOfBusiness
     *
     * @return string
     */
    public function getF113fnatureOfBusiness()
    {
        return $this->f113fnatureOfBusiness;
    }


    /**
     * Set f113fcategory
     *
     * @param string $f113fcategory
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fcategory($f113fcategory)
    {
        $this->f113fcategory = $f113fcategory;

        return $this;
    }

    /**
     * Get f113fcategory
     *
     * @return string
     */
    public function getF113fcategory()
    {
        return $this->f113fcategory;
    }


    /**
     * Set f113fsubCategory
     *
     * @param string $f113fsubCategory
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fsubCategory($f113fsubCategory)
    {
        $this->f113fsubCategory = $f113fsubCategory;

        return $this;
    }

    /**
     * Get f113fsubCategory
     *
     * @return string
     */
    public function getF113fsubCategory()
    {
        return $this->f113fsubCategory;
    }

    
    /**
     * Set f113fstatus
     *
     * @param integer $f113fstatus
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fstatus($f113fstatus)
    {
        $this->f113fstatus = $f113fstatus;

        return $this;
    }

    /**
     * Get f113fstatus
     *
     * @return integer
     */
    public function getF113fstatus()
    {
        return $this->f113fstatus;
    }

    /**
     * Set f113fcreatedBy
     *
     * @param integer $f113fcreatedBy
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fcreatedBy($f113fcreatedBy)
    {
        $this->f113fcreatedBy = $f113fcreatedBy;

        return $this;
    }

    /**
     * Get f113fcreatedBy
     *
     * @return integer
     */
    public function getF113fcreatedBy()
    {
        return $this->f113fcreatedBy;
    }

    /**
     * Set f113fupdatedBy
     *
     * @param integer $f113fupdatedBy
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fupdatedBy($f113fupdatedBy)
    {
        $this->f113fupdatedBy = $f113fupdatedBy;

        return $this;
    }

    /**
     * Get f113fupdatedBy
     *
     * @return integer
     */
    public function getF113fupdatedBy()
    {
        return $this->f113fupdatedBy;
    }

    /**
     * Set f113fcreatedDtTm
     *
     * @param \DateTime $f113fcreatedDtTm
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fcreatedDtTm($f113fcreatedDtTm)
    {
        $this->f113fcreatedDtTm = $f113fcreatedDtTm;

        return $this;
    }

    /**
     * Get f113fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF113fcreatedDtTm()
    {
        return $this->f113fcreatedDtTm;
    }

    /**
     * Set f113fupdatedDtTm
     *
     * @param \DateTime $f113fupdatedDtTm
     *
     * @return T113fnatureOfBusiness
     */
    public function setF113fupdatedDtTm($f113fupdatedDtTm)
    {
        $this->f113fupdatedDtTm = $f113fupdatedDtTm;

        return $this;
    }

    /**
     * Get f113fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF113fupdatedDtTm()
    {
        return $this->f113fupdatedDtTm;
    }
}
