<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T042fbankDetails
 *
 * @ORM\Table(name="t042fbank_details", indexes={@ORM\Index(name="f042fid_ledger", columns={"f042fid_ledger"}), @ORM\Index(name="f042fupdated_by", columns={"f042fupdated_by"}), @ORM\Index(name="f042fcreated_by", columns={"f042fcreated_by"})})
 * @ORM\Entity
 */
class T042fbankDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f042fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f042fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fbank_name", type="string", length=50, nullable=false)
     */
    private $f042fbankName;


    /**
     * @var string
     *
     * @ORM\Column(name="f042fbranch_name", type="string", length=50, nullable=false)
     */
    private $f042fbranchName;

    /**
     * @var string
     *
     * @ORM\Column(name="f042facc_no", type="string", length=50, nullable=false)
     */
    private $f042faccNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f042facc_type", type="string", length=50, nullable=false)
     */
    private $f042faccType;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fswift_code", type="string", length=50, nullable=false)
     */
    private $f042fswiftCode;


    /**
     * @var string
     *
     * @ORM\Column(name="f042fbrs_code", type="string", length=50, nullable=false)
     */
    private $f042fbrsCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f042fstatus", type="boolean", nullable=false)
     */
    private $f042fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f042fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f042fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fid_ledger", type="integer", length=50, nullable=false)
     */
    private $f042fidLedger;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f042fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f042fcreatedBy;



    /**
     * Get f042fid
     *
     * @return integer
     */
    public function getF042fid()
    {
        return $this->f042fid;
    }

    /**
     * Set f042fbankName
     *
     * @param string $f042fbankName
     *
     * @return T042fbankDetails
     */
    public function setF042fbankName($f042fbankName)
    {
        $this->f042fbankName = $f042fbankName;

        return $this;
    }

    /**
     * Get f042fbankName
     *
     * @return string
     */
    public function getF042fbankName()
    {
        return $this->f042fbankName;
    }

    /**
     * Set f042fbranchName
     *
     * @param string $f042fbranchName
     *
     * @return T042fbankDetails
     */
    public function setF042fbranchName($f042fbranchName)
    {
        $this->f042fbranchName = $f042fbranchName;

        return $this;
    }

    /**
     * Get f042fbranchName
     *
     * @return string
     */
    public function getF042fbranchName()
    {
        return $this->f042fbranchName;
    }


    /**
     * Set f042faccNo
     *
     * @param string $f042faccNo
     *
     * @return T042fbankDetails
     */
    public function setF042faccNo($f042faccNo)
    {
        $this->f042faccNo = $f042faccNo;

        return $this;
    }

    /**
     * Get f042faccNo
     *
     * @return string
     */
    public function getF042faccNo()
    {
        return $this->f042faccNo;
    }

    /**
     * Set f042faccType
     *
     * @param string $f042faccType
     *
     * @return T042fbankDetails
     */
    public function setF042faccType($f042faccType)
    {
        $this->f042faccType = $f042faccType;

        return $this;
    }

    /**
     * Get f042faccType
     *
     * @return string
     */
    public function getF042faccType()
    {
        return $this->f042faccType;
    }

    /**
     * Set f042fswiftCode
     *
     * @param string $f042fswiftCode
     *
     * @return T042fbankDetails
     */
    public function setF042fswiftCode($f042fswiftCode)
    {
        $this->f042fswiftCode = $f042fswiftCode;

        return $this;
    }

    /**
     * Get f042fswiftCode
     *
     * @return string
     */
    public function getF042fswiftCode()
    {
        return $this->f042fswiftCode;
    }

    /**
     * Set f042fbrsCode
     *
     * @param string $f042fbrsCode
     *
     * @return T042fbankDetails
     */
    public function setF042fbrsCode($f042fbrsCode)
    {
        $this->f042fbrsCode = $f042fbrsCode;

        return $this;
    }

    /**
     * Get f042fbrsCode
     *
     * @return string
     */
    public function getF042fbrsCode()
    {
        return $this->f042fbrsCode;
    }

    /**
     * Set f042fstatus
     *
     * @param boolean $f042fstatus
     *
     * @return T042fbankDetails
     */
    public function setF042fstatus($f042fstatus)
    {
        $this->f042fstatus = $f042fstatus;

        return $this;
    }

    /**
     * Get f042fstatus
     *
     * @return boolean
     */
    public function getF042fstatus()
    {
        return $this->f042fstatus;
    }

    /**
     * Set f042fcreatedDtTm
     *
     * @param \DateTime $f042fcreatedDtTm
     *
     * @return T042fbankDetails
     */
    public function setF042fcreatedDtTm($f042fcreatedDtTm)
    {
        $this->f042fcreatedDtTm = $f042fcreatedDtTm;

        return $this;
    }

    /**
     * Get f042fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fcreatedDtTm()
    {
        return $this->f042fcreatedDtTm;
    }

    /**
     * Set f042fupdatedDtTm
     *
     * @param \DateTime $f042fupdatedDtTm
     *
     * @return T042fbankDetails
     */
    public function setF042fupdatedDtTm($f042fupdatedDtTm)
    {
        $this->f042fupdatedDtTm = $f042fupdatedDtTm;

        return $this;
    }

    /**
     * Get f042fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fupdatedDtTm()
    {
        return $this->f042fupdatedDtTm;
    }

    /**
     * Set f042fidLedger
     *
     * @param string $f042fidLedger
     *
     * @return T042fbankDetails
     */
    public function setF042fidLedger( $f042fidLedger )
    {
        $this->f042fidLedger = $f042fidLedger;

        return $this;
    }

    /**
     * Get f042fidLedger
     *
     * @return T042fbankDetails
     */
    public function getF042fidLedger()
    {
        return $this->f042fidLedger;
    }

    /**
     * Set f042fupdatedBy
     *
     * @param integer $f042fupdatedBy
     *
     * @return T042fbankDetails
     */
    public function setF042fupdatedBy($f042fupdatedBy )
    {
        $this->f042fupdatedBy = $f042fupdatedBy;

        return $this;
    }

    /**
     * Get f042fupdatedBy
     *
     * @return integer
     */
    public function getF042fupdatedBy()
    {
        return $this->f042fupdatedBy;
    }

    /**
     * Set f042fcreatedBy
     *
     * @param integer $f042fcreatedBy
     *
     * @return T042fbankDetails
     */
    public function setF042fcreatedBy( $f042fcreatedBy )
    {
        $this->f042fcreatedBy = $f042fcreatedBy;

        return $this;
    }

    /**
     * Get f042fcreatedBy
     *
     * @return integer
     */
    public function getF042fcreatedBy()
    {
        return $this->f042fcreatedBy;
    }
}
