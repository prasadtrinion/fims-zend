<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T078futilityRateSetup
 *
 * @ORM\Table(name="t078futility_rate_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\UtilityRateSetupRepository")
 */
class T078futilityRateSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f078fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fmin_unit", type="integer", nullable=false)
     */
    private $f078fminUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fmax_unit", type="integer", nullable=false)
     */
    private $f078fmaxUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078famount", type="integer",  nullable=true)
     */
    private $f078famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fstatus", type="integer", nullable=false)
     */
    private $f078fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fcreated_by", type="integer", nullable=false)
     */
    private $f078fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fupdated_by", type="integer", nullable=false)
     */
    private $f078fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fupdatedDtTm;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078feffective_date", type="datetime", nullable=false)
     */
    private $f078feffectiveDate;


    /**
     * Get f078fid
     *
     * @return integer
     */
    public function getF078fid()
    {
        return $this->f078fid;
    }


    /**
     * Set f078fminUnit
     *
     * @param string $f078fminUnit
     *
     * @return T078fminUnit
     */
    public function setF078fminUnit($f078fminUnit)
    {
        $this->f078fminUnit = $f078fminUnit;

        return $this;
    }

    /**
     * Get f078fminUnit
     *
     * @return string
     */
    public function getF078fminUnit()
    {
        return $this->f078fminUnit;
    }

    /**
     * Set f078fmaxUnit
     *
     * @param string $f078fmaxUnit
     *
     * @return T078fmaxUnit
     */
    public function setF078fmaxUnit($f078fmaxUnit)
    {
        $this->f078fmaxUnit = $f078fmaxUnit;

        return $this;
    }

    /**
     * Get f078fmaxUnit
     *
     * @return string
     */
    public function getF078fmaxUnit()
    {
        return $this->f078fmaxUnit;
    }

    /**
     * Set f078famount
     *
     * @param integer $f078famount
     *
     * @return T078futilityRateSetup
     */
    public function setF078famount($f078famount)
    {
        $this->f078famount = $f078famount;

        return $this;
    }

    /**
     * Get f078famount
     *
     * @return integer
     */
    public function getF078famount()
    {
        return $this->f078famount;
    }

    
    /**
     * Set f078fstatus
     *
     * @param integer $f078fstatus
     *
     * @return T078futilityRateSetup
     */
    public function setF078fstatus($f078fstatus)
    {
        $this->f078fstatus = $f078fstatus;

        return $this;
    }

    /**
     * Get f078fstatus
     *
     * @return integer
     */
    public function getF078fstatus()
    {
        return $this->f078fstatus;
    }


    // /**
    //  * Set f078fidUtility
    //  *
    //  * @param integer $f078fidUtility
    //  *
    //  * @return T078futilityRateSetup
    //  */
    // public function setF078fidUtility($f078fidUtility)
    // {
    //     $this->f078fidUtility = $f078fidUtility;

    //     return $this;
    // }

    // /**
    //  * Get f078fidUtility
    //  *
    //  * @return integer
    //  */
    // public function getF078fidUtility()
    // {
    //     return $this->f078fidUtility;
    // }

    /**
     * Set f078fcreatedBy
     *
     * @param integer $f078fcreatedBy
     *
     * @return T078futilityRateSetup
     */
    public function setF078fcreatedBy($f078fcreatedBy)
    {
        $this->f078fcreatedBy = $f078fcreatedBy;

        return $this;
    }

    /**
     * Get f078fcreatedBy
     *
     * @return integer
     */
    public function getF078fcreatedBy()
    {
        return $this->f078fcreatedBy;
    }

    /**
     * Set f078fupdatedBy
     *
     * @param integer $f078fupdatedBy
     *
     * @return T078futilityRateSetup
     */
    public function setF078fupdatedBy($f078fupdatedBy)
    {
        $this->f078fupdatedBy = $f078fupdatedBy;

        return $this;
    }

    /**
     * Get f078fupdatedBy
     *
     * @return integer
     */
    public function getF078fupdatedBy()
    {
        return $this->f078fupdatedBy;
    }

    /**
     * Set f078fcreatedDtTm
     *
     * @param \DateTime $f078fcreatedDtTm
     *
     * @return T078futilityRateSetup
     */
    public function setF078fcreatedDtTm($f078fcreatedDtTm)
    {
        $this->f078fcreatedDtTm = $f078fcreatedDtTm;

        return $this;
    }

    /**
     * Get f078fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fcreatedDtTm()
    {
        return $this->f078fcreatedDtTm;
    }

    /**
     * Set f078fupdatedDtTm
     *
     * @param \DateTime $f078fupdatedDtTm
     *
     * @return T078futilityRateSetup
     */
    public function setF078fupdatedDtTm($f078fupdatedDtTm)
    {
        $this->f078fupdatedDtTm = $f078fupdatedDtTm;

        return $this;
    }

    /**
     * Get f078fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fupdatedDtTm()
    {
        return $this->f078fupdatedDtTm;
    }

    /**
     * Set f078feffectiveDate
     *
     * @param \DateTime $f078feffectiveDate
     *
     * @return T078futilityRateSetup
     */
    public function setF078feffectiveDate($f078feffectiveDate)
    {
        $this->f078feffectiveDate = $f078feffectiveDate;

        return $this;
    }

    /**
     * Get f078feffectiveDate
     *
     * @return \DateTime
     */
    public function getF078feffectiveDate()
    {
        return $this->f078feffectiveDate;
    }
}

