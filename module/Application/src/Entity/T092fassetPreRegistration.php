<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T092fassetPreRegistration
 *
 * @ORM\Table(name="t092fasset_pre_registration")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetPreRegistrationRepository")
 */
class T092fassetPreRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f092fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_grn", type="integer", nullable=true)
     */
    private $f092fidGrn;

    /**
     * @var string
     *
     * @ORM\Column(name="f092fgrn_number", type="string", length=20, nullable=true)
     */
    private $f092fgrnNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fgrn_date", type="datetime", nullable=true)
     */
    private $f092fgrnDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fasset_item", type="integer", nullable=true)
     */
    private $f092fassetItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fquantity", type="integer", nullable=true)
     */
    private $f092fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fstatus", type="integer", nullable=true)
     */
    private $f092fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fcreated_by", type="integer", nullable=true)
     */
    private $f092fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fupdated_by", type="integer", nullable=true)
     */
    private $f092fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f092fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f092fupdatedDtTm;



    /**
     * Get f092fid
     *
     * @return integer
     */
    public function getF092fid()
    {
        return $this->f092fid;
    }

    /**
     * Set f092fidGrn
     *
     * @param integer $f092fidGrn
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fidGrn($f092fidGrn)
    {
        $this->f092fidGrn = $f092fidGrn;

        return $this;
    }

    /**
     * Get f092fidGrn
     *
     * @return integer
     */
    public function getF092fidGrn()
    {
        return $this->f092fidGrn;
    }

    /**
     * Set f092fgrnNumber
     *
     * @param string $f092fgrnNumber
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fgrnNumber($f092fgrnNumber)
    {
        $this->f092fgrnNumber = $f092fgrnNumber;

        return $this;
    }

    /**
     * Get f092fgrnNumber
     *
     * @return string
     */
    public function getF092fgrnNumber()
    {
        return $this->f092fgrnNumber;
    }

    /**
     * Set f092fgrnDate
     *
     * @param \DateTime $f092fgrnDate
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fgrnDate($f092fgrnDate)
    {
        $this->f092fgrnDate = $f092fgrnDate;

        return $this;
    }

    /**
     * Get f092fgrnDate
     *
     * @return \DateTime
     */
    public function getF092fgrnDate()
    {
        return $this->f092fgrnDate;
    }

    /**
     * Set f092fassetItem
     *
     * @param integer $f092fassetItem
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fassetItem($f092fassetItem)
    {
        $this->f092fassetItem = $f092fassetItem;

        return $this;
    }

    /**
     * Get f092fassetItem
     *
     * @return integer
     */
    public function getF092fassetItem()
    {
        return $this->f092fassetItem;
    }

    /**
     * Set f092fquantity
     *
     * @param integer $f092fquantity
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fquantity($f092fquantity)
    {
        $this->f092fquantity = $f092fquantity;

        return $this;
    }

    /**
     * Get f092fquantity
     *
     * @return integer
     */
    public function getF092fquantity()
    {
        return $this->f092fquantity;
    }

    /**
     * Set f092fstatus
     *
     * @param integer $f092fstatus
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fstatus($f092fstatus)
    {
        $this->f092fstatus = $f092fstatus;

        return $this;
    }

    /**
     * Get f092fstatus
     *
     * @return integer
     */
    public function getF092fstatus()
    {
        return $this->f092fstatus;
    }

    /**
     * Set f092fcreatedBy
     *
     * @param integer $f092fcreatedBy
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fcreatedBy($f092fcreatedBy)
    {
        $this->f092fcreatedBy = $f092fcreatedBy;

        return $this;
    }

    /**
     * Get f092fcreatedBy
     *
     * @return integer
     */
    public function getF092fcreatedBy()
    {
        return $this->f092fcreatedBy;
    }

    /**
     * Set f092fupdatedBy
     *
     * @param integer $f092fupdatedBy
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fupdatedBy($f092fupdatedBy)
    {
        $this->f092fupdatedBy = $f092fupdatedBy;

        return $this;
    }

    /**
     * Get f092fupdatedBy
     *
     * @return integer
     */
    public function getF092fupdatedBy()
    {
        return $this->f092fupdatedBy;
    }

    /**
     * Set f092fcreatedDtTm
     *
     * @param \DateTime $f092fcreatedDtTm
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fcreatedDtTm($f092fcreatedDtTm)
    {
        $this->f092fcreatedDtTm = $f092fcreatedDtTm;

        return $this;
    }

    /**
     * Get f092fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fcreatedDtTm()
    {
        return $this->f092fcreatedDtTm;
    }

    /**
     * Set f092fupdatedDtTm
     *
     * @param \DateTime $f092fupdatedDtTm
     *
     * @return T092fassetPreRegistration
     */
    public function setF092fupdatedDtTm($f092fupdatedDtTm)
    {
        $this->f092fupdatedDtTm = $f092fupdatedDtTm;

        return $this;
    }

    /**
     * Get f092fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fupdatedDtTm()
    {
        return $this->f092fupdatedDtTm;
    }
}
