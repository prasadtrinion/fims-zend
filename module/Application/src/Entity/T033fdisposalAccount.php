<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T033fdisposalAccount
 *
 * @ORM\Table(name="t033fdisposal_account" ,uniqueConstraints={@ORM\UniqueConstraint(name="f033fcode_UNIQUE", columns={"f033fcode"})}) 
 * @ORM\Entity(repositoryClass="Application\Repository\DisposalAccountRepository")
 */
class T033fdisposalAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f033fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f033fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f033fcode", type="string", length=30, nullable=true)
     */
    private $f033fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f033faccount_code", type="string",length=30, nullable=true)
     */
    private $f033faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f033ftype", type="integer", nullable=true)
     */
    private $f033ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f033fstatus", type="integer", nullable=true)
     */
    private $f033fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f033fcreated_by", type="integer", nullable=true)
     */
    private $f033fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f033fupdated_by", type="integer", nullable=true)
     */
    private $f033fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f033fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f033fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f033fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f033fupdatedDtTm;



    /**
     * Get f033fid
     *
     * @return integer
     */
    public function getF033fid()
    {
        return $this->f033fid;
    }

    /**
     * Set f033fcode
     *
     * @param string $f033fcode
     *
     * @return T033fdisposalAccount
     */
    public function setF033fcode($f033fcode)
    {
        $this->f033fcode = $f033fcode;

        return $this;
    }

    /**
     * Get f033fcode
     *
     * @return string
     */
    public function getF033fcode()
    {
        return $this->f033fcode;
    }

    /**
     * Set f033faccountCode
     *
     * @param string $f033faccountCode
     *
     * @return T033fdisposalAccount
     */
    public function setF033faccountCode($f033faccountCode)
    {
        $this->f033faccountCode = $f033faccountCode;

        return $this;
    }

    /**
     * Get f033faccountCode
     *
     * @return string
     */
    public function getF033faccountCode()
    {
        return $this->f033faccountCode;
    }

    /**
     * Set f033ftype
     *
     * @param integer $f033ftype
     *
     * @return T033fdisposalAccount
     */
    public function setF033ftype($f033ftype)
    {
        $this->f033ftype = $f033ftype;

        return $this;
    }

    /**
     * Get f033ftype
     *
     * @return integer
     */
    public function getF033ftype()
    {
        return $this->f033ftype;
    }

    /**
     * Set f033fstatus
     *
     * @param integer $f033fstatus
     *
     * @return T033fdisposalAccount
     */
    public function setF033fstatus($f033fstatus)
    {
        $this->f033fstatus = $f033fstatus;

        return $this;
    }

    /**
     * Get f033fstatus
     *
     * @return integer
     */
    public function getF033fstatus()
    {
        return $this->f033fstatus;
    }

    /**
     * Set f033fcreatedBy
     *
     * @param integer $f033fcreatedBy
     *
     * @return T033fdisposalAccount
     */
    public function setF033fcreatedBy($f033fcreatedBy)
    {
        $this->f033fcreatedBy = $f033fcreatedBy;

        return $this;
    }

    /**
     * Get f033fcreatedBy
     *
     * @return integer
     */
    public function getF033fcreatedBy()
    {
        return $this->f033fcreatedBy;
    }

    /**
     * Set f033fupdatedBy
     *
     * @param integer $f033fupdatedBy
     *
     * @return T033fdisposalAccount
     */
    public function setF033fupdatedBy($f033fupdatedBy)
    {
        $this->f033fupdatedBy = $f033fupdatedBy;

        return $this;
    }

    /**
     * Get f033fupdatedBy
     *
     * @return integer
     */
    public function getF033fupdatedBy()
    {
        return $this->f033fupdatedBy;
    }

    /**
     * Set f033fcreatedDtTm
     *
     * @param \DateTime $f033fcreatedDtTm
     *
     * @return T033fdisposalAccount
     */
    public function setF033fcreatedDtTm($f033fcreatedDtTm)
    {
        $this->f033fcreatedDtTm = $f033fcreatedDtTm;

        return $this;
    }

    /**
     * Get f033fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF033fcreatedDtTm()
    {
        return $this->f033fcreatedDtTm;
    }

    /**
     * Set f033fupdatedDtTm
     *
     * @param \DateTime $f033fupdatedDtTm
     *
     * @return T033fdisposalAccount
     */
    public function setF033fupdatedDtTm($f033fupdatedDtTm)
    {
        $this->f033fupdatedDtTm = $f033fupdatedDtTm;

        return $this;
    }

    /**
     * Get f033fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF033fupdatedDtTm()
    {
        return $this->f033fupdatedDtTm;
    }
}
