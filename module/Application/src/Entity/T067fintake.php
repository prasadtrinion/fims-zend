<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T067fintake
 *
 * @ORM\Table(name="t067fintake")
 * @ORM\Entity(repositoryClass="Application\Repository\IntakeRepository")
 */
class T067fintake
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f067fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f067fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f067fname", type="string", length=25, nullable=true)
     */
    private $f067fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fsem_sequence", type="integer", nullable=true)
     */
    private $f067fsemSequence;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fintake_year", type="string",length=30, nullable=true)
     */
    private $f067fintakeYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f067fdescription", type="string", length=50, nullable=true)
     */
    private $f067fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fstatus", type="integer", nullable=true)
     */
    private $f067fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fcreated_by", type="integer", nullable=true)
     */
    private $f067fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fupdated_by", type="integer", nullable=true)
     */
    private $f067fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f067fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f067fupdatedDtTm;

      /**
     * @var string
     *
     * @ORM\Column(name="f067ftype", type="string",length=30, nullable=true)
     */
    private $f067ftype;



    /**
     * Get f067fid
     *
     * @return integer
     */
    public function getF067fid()
    {
        return $this->f067fid;
    }

    /**
     * Set f067fname
     *
     * @param string $f067fname
     *
     * @return T067fintake
     */
    public function setF067fname($f067fname)
    {
        $this->f067fname = $f067fname;

        return $this;
    }

    /**
     * Get f067fname
     *
     * @return string
     */
    public function getF067fname()
    {
        return $this->f067fname;
    }

    /**
     * Set f067ftype
     *
     * @param string $f067ftype
     *
     * @return T067fintake
     */
    public function setF067ftype($f067ftype)
    {
        $this->f067ftype = $f067ftype;

        return $this;
    }

    /**
     * Get f067ftype
     *
     * @return string
     */
    public function getF067ftype()
    {
        return $this->f067ftype;
    }

    /**
     * Set f067fsemSequence
     *
     * @param integer $f067fsemSequence
     *
     * @return T067fintake
     */
    public function setF067fsemSequence($f067fsemSequence)
    {
        $this->f067fsemSequence = $f067fsemSequence;

        return $this;
    }

    /**
     * Get f067fsemSequence
     *
     * @return integer
     */
    public function getF067fsemSequence()
    {
        return $this->f067fsemSequence;
    }

    /**
     * Set f067fintakeYear
     *
     * @param integer $f067fintakeYear
     *
     * @return T067fintake
     */
    public function setF067fintakeYear($f067fintakeYear)
    {
        $this->f067fintakeYear = $f067fintakeYear;

        return $this;
    }

    /**
     * Get f067fintakeYear
     *
     * @return integer
     */
    public function getF067fintakeYear()
    {
        return $this->f067fintakeYear;
    }

    /**
     * Set f067fdescription
     *
     * @param string $f067fdescription
     *
     * @return T067fintake
     */
    public function setF067fdescription($f067fdescription)
    {
        $this->f067fdescription = $f067fdescription;

        return $this;
    }

    /**
     * Get f067fdescription
     *
     * @return string
     */
    public function getF067fdescription()
    {
        return $this->f067fdescription;
    }

    /**
     * Set f067fstatus
     *
     * @param integer $f067fstatus
     *
     * @return T067fintake
     */
    public function setF067fstatus($f067fstatus)
    {
        $this->f067fstatus = $f067fstatus;

        return $this;
    }

    /**
     * Get f067fstatus
     *
     * @return integer
     */
    public function getF067fstatus()
    {
        return $this->f067fstatus;
    }

    /**
     * Set f067fcreatedBy
     *
     * @param integer $f067fcreatedBy
     *
     * @return T067fintake
     */
    public function setF067fcreatedBy($f067fcreatedBy)
    {
        $this->f067fcreatedBy = $f067fcreatedBy;

        return $this;
    }

    /**
     * Get f067fcreatedBy
     *
     * @return integer
     */
    public function getF067fcreatedBy()
    {
        return $this->f067fcreatedBy;
    }

    /**
     * Set f067fupdatedBy
     *
     * @param integer $f067fupdatedBy
     *
     * @return T067fintake
     */
    public function setF067fupdatedBy($f067fupdatedBy)
    {
        $this->f067fupdatedBy = $f067fupdatedBy;

        return $this;
    }

    /**
     * Get f067fupdatedBy
     *
     * @return integer
     */
    public function getF067fupdatedBy()
    {
        return $this->f067fupdatedBy;
    }

    /**
     * Set f067fcreatedDtTm
     *
     * @param \DateTime $f067fcreatedDtTm
     *
     * @return T067fintake
     */
    public function setF067fcreatedDtTm($f067fcreatedDtTm)
    {
        $this->f067fcreatedDtTm = $f067fcreatedDtTm;

        return $this;
    }

    /**
     * Get f067fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF067fcreatedDtTm()
    {
        return $this->f067fcreatedDtTm;
    }

    /**
     * Set f067fupdatedDtTm
     *
     * @param \DateTime $f067fupdatedDtTm
     *
     * @return T067fintake
     */
    public function setF067fupdatedDtTm($f067fupdatedDtTm)
    {
        $this->f067fupdatedDtTm = $f067fupdatedDtTm;

        return $this;
    }

    /**
     * Get f067fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF067fupdatedDtTm()
    {
        return $this->f067fupdatedDtTm;
    }
}
