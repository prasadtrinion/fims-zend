<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T037ftenderCommitee
 *
 * @ORM\Table(name="t037ftender_commitee")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderCommiteeRepository")
 */
class T037ftenderCommitee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f037fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f037fname", type="string", length=50, nullable=false)
     */
    private $f037fname;

     /**
     * @var integer
     *
     * @ORM\Column(name="f037fmember_id", type="integer", nullable=false)
     */
    private $f037fmemberId;

     /**
     * @var string
     *
     * @ORM\Column(name="f037ftype", type="string", nullable=false)
     */
    private $f037ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fstatus", type="integer", nullable=false)
     */
    private $f037fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f037fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f037fcreatedBy;


    /**
     * Get f037fid
     *
     * @return integer
     */
    public function getF037fid()
    {
        return $this->f037fid;
    }

    /**
     * Set f037fname
     *
     * @param string $f037fname
     *
     * @return T037ftenderCommitee
     */
    public function setF037fname($f037fname)
    {
        $this->f037fname = $f037fname;

        return $this;
    }

    /**
     * Get f037fname
     *
     * @return string
     */
    public function getF037fname()
    {
        return $this->f037fname;
    }

    /**
     * Set f037fstatus
     *
     * @param integer $f037fstatus
     *
     * @return T037ftenderCommitee
     */
    public function setF037fstatus($f037fstatus)
    {
        $this->f037fstatus = $f037fstatus;

        return $this;
    }

    /**
     * Get f037fstatus
     *
     * @return integer
     */
    public function getF037fstatus()
    {
        return $this->f037fstatus;
    }

    /**
     * Set f037ftype
     *
     * @param integer $f037ftype
     *
     * @return T037ftenderCommitee
     */
    public function setF037ftype($f037ftype)
    {
        $this->f037ftype = $f037ftype;

        return $this;
    }

    /**
     * Get f037ftype
     *
     * @return integer
     */
    public function getF037ftype()
    {
        return $this->f037ftype;
    }


    /**
     * Set f037fmemberId
     *
     * @param integer $f037fmemberId
     *
     * @return T037ftenderCommitee
     */
    public function setF037fmemberId($f037fmemberId)
    {
        $this->f037fmemberId = $f037fmemberId;

        return $this;
    }

    /**
     * Get f037fmemberId
     *
     * @return integer
     */
    public function getF037fmemberId()
    {
        return $this->f037fmemberId;
    }

    /**
     * Set f037fcreatedDtTm
     *
     * @param \DateTime $f037fcreatedDtTm
     *
     * @return T037ftenderCommitee
     */
    public function setF037fcreatedDtTm($f037fcreatedDtTm)
    {
        $this->f037fcreatedDtTm = $f037fcreatedDtTm;

        return $this;
    }

    /**
     * Get f037fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getf037fcreatedDtTm()
    {
        return $this->f037fcreatedDtTm;
    }

    /**
     * Set f037fupdatedDtTm
     *
     * @param \DateTime $f037fupdatedDtTm
     *
     * @return T037ftenderCommitee
     */
    public function setf037fupdatedDtTm($f037fupdatedDtTm)
    {
        $this->f037fupdatedDtTm = $f037fupdatedDtTm;

        return $this;
    }

    /**
     * Get f037fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getf037fupdatedDtTm()
    {
        return $this->f037fupdatedDtTm;
    }

    /**
     * Set f037fupdatedBy
     *
     * @param integer $f037fupdatedBy
     *
     * @return T037ftenderCommitee
     */
    public function setf037fupdatedBy($f037fupdatedBy)
    {
        $this->f037fupdatedBy = $f037fupdatedBy;

        return $this;
    }

    
    public function getf037fupdatedBy()
    {
        return $this->f037fupdatedBy;
    }

    /**
     * Set f037fcreatedBy
     *
     * @param  integer $f037fcreatedBy
     *
     * @return T037ftenderCommitee
     */
    public function setf037fcreatedBy($f037fcreatedBy)
    {
        $this->f037fcreatedBy = $f037fcreatedBy;

        return $this;
    }

    
    public function getf037fcreatedBy()
    {
        return $this->f037fcreatedBy;
    }
}
