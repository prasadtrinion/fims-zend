<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * T044floanGurantors
 *
 * @ORM\Table(name="t044floan_gurantors")
 * @ORM\Entity(repositoryClass="Application\Repository\LoanGurantorRepository")
 */
class T044floanGurantors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f044fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid_staff", type="integer", nullable=true)
     */
    private $f044fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fid_loan", type="integer", nullable=true)
     */
    private $f044fidLoan;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fcreated_by", type="integer", nullable=true)
     */
    private $f044fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f044fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fapproved_date", type="datetime", nullable=true)
     */
    private $f044fapprovedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fupdated_by", type="integer", nullable=true)
     */
    private $f044fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f044fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fstatus", type="integer", nullable=true)
     */
    private $f044fstatus;



    /**
     * Get f044fid
     *
     * @return integer
     */
    public function getF044fid()
    {
        return $this->f044fid;
    }

    /**
     * Set f044fidStaff
     *
     * @param integer $f044fidStaff
     *
     * @return T044floanGurantors
     */
    public function setF044fidStaff($f044fidStaff)
    {
        $this->f044fidStaff = $f044fidStaff;

        return $this;
    }

    /**
     * Get f044fidStaff
     *
     * @return integer
     */
    public function getF044fidStaff()
    {
        return $this->f044fidStaff;
    }

    /**
     * Set f044fidLoan
     *
     * @param string $f044fidLoan
     *
     * @return T044floanGurantors
     */
    public function setF044fidLoan($f044fidLoan)
    {
        $this->f044fidLoan = $f044fidLoan;

        return $this;
    }

    /**
     * Get f044fidLoan
     *
     * @return string
     */
    public function getF044fidLoan()
    {
        return $this->f044fidLoan;
    }

    /**
     * Set f044fcreatedBy
     *
     * @param integer $f044fcreatedBy
     *
     * @return T044floanGurantors
     */
    public function setF044fcreatedBy($f044fcreatedBy)
    {
        $this->f044fcreatedBy = $f044fcreatedBy;

        return $this;
    }

    /**
     * Get f044fcreatedBy
     *
     * @return integer
     */
    public function getF044fcreatedBy()
    {
        return $this->f044fcreatedBy;
    }

    /**
     * Set f044fcreatedDtTm
     *
     * @param \DateTime $f044fcreatedDtTm
     *
     * @return T044floanGurantors
     */
    public function setF044fcreatedDtTm($f044fcreatedDtTm)
    {
        $this->f044fcreatedDtTm = $f044fcreatedDtTm;

        return $this;
    }

    /**
     * Get f044fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fcreatedDtTm()
    {
        return $this->f044fcreatedDtTm;
    }

    /**
     * Set f044fapprovedDate
     *
     * @param \DateTime $f044fapprovedDate
     *
     * @return T044floanGurantors
     */
    public function setF044fapprovedDate($f044fapprovedDate)
    {
        $this->f044fapprovedDate = $f044fapprovedDate;

        return $this;
    }

    /**
     * Get f044fapprovedDate
     *
     * @return \DateTime
     */
    public function getF044fapprovedDate()
    {
        return $this->f044fapprovedDate;
    }

    /**
     * Set f044fupdatedBy
     *
     * @param integer $f044fupdatedBy
     *
     * @return T044floanGurantors
     */
    public function setF044fupdatedBy($f044fupdatedBy)
    {
        $this->f044fupdatedBy = $f044fupdatedBy;

        return $this;
    }

    /**
     * Get f044fupdatedBy
     *
     * @return integer
     */
    public function getF044fupdatedBy()
    {
        return $this->f044fupdatedBy;
    }

    /**
     * Set f044fupdatedDtTm
     *
     * @param \DateTime $f044fupdatedDtTm
     *
     * @return T044floanGurantors
     */
    public function setF044fupdatedDtTm($f044fupdatedDtTm)
    {
        $this->f044fupdatedDtTm = $f044fupdatedDtTm;

        return $this;
    }

    /**
     * Get f044fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fupdatedDtTm()
    {
        return $this->f044fupdatedDtTm;
    }

    /**
     * Set f044fstatus
     *
     * @param integer $f044fstatus
     *
     * @return T044floanGurantors
     */
    public function setF044fstatus($f044fstatus)
    {
        $this->f044fstatus = $f044fstatus;

        return $this;
    }

    /**
     * Get f044fstatus
     *
     * @return integer
     */
    public function getF044fstatus()
    {
        return $this->f044fstatus;
    }
}
