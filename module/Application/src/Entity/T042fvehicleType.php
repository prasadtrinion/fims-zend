<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T042fvehicleType
 *
 * @ORM\Table(name="t042fvehicle_type", uniqueConstraints={@ORM\UniqueConstraint(name="f042fname_UNIQUE", columns={"f042fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\VehicleTypeRepository")
 */
class T042fvehicleType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f042fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f042fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fname", type="string", length=50, nullable=false)
     */
    private $f042fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fmonths", type="string",  length=20, nullable=false)
     */
    private $f042fmonths;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fstatus", type="integer", nullable=false)
     */
    private $f042fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fcreated_by", type="integer", nullable=false)
     */
    private $f042fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fupdated_by", type="integer", nullable=false)
     */
    private $f042fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f042fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f042fupdatedDtTm;



    /**
     * Get f042fid
     *
     * @return integer
     */
    public function getF042fid()
    {
        return $this->f042fid;
    }

    /**
     * Set f042fname
     *
     * @param string $f042fname
     *
     * @return T042fvehicleType
     */
    public function setF042fname($f042fname)
    {
        $this->f042fname = $f042fname;

        return $this;
    }

    /**
     * Get f042fname
     *
     * @return string
     */
    public function getF042fname()
    {
        return $this->f042fname;
    }

    /**
     * Set f042fmonths
     *
     * @param string $f042fmonths
     *
     * @return T042fvehicleType
     */
    public function setF042fmonths($f042fmonths)
    {
        $this->f042fmonths = $f042fmonths;

        return $this;
    }

    /**
     * Get f042fmonths
     *
     * @return string
     */
    public function getF042fmonths()
    {
        return $this->f042fmonths;
    }

    /**
     * Set f042fstatus
     *
     * @param integer $f042fstatus
     *
     * @return T042fvehicleType
     */
    public function setF042fstatus($f042fstatus)
    {
        $this->f042fstatus = $f042fstatus;

        return $this;
    }

    /**
     * Get f042fstatus
     *
     * @return integer
     */
    public function getF042fstatus()
    {
        return $this->f042fstatus;
    }

    /**
     * Set f042fcreatedBy
     *
     * @param integer $f042fcreatedBy
     *
     * @return T042fvehicleType
     */
    public function setF042fcreatedBy($f042fcreatedBy)
    {
        $this->f042fcreatedBy = $f042fcreatedBy;

        return $this;
    }

    /**
     * Get f042fcreatedBy
     *
     * @return integer
     */
    public function getF042fcreatedBy()
    {
        return $this->f042fcreatedBy;
    }

    /**
     * Set f042fupdatedBy
     *
     * @param integer $f042fupdatedBy
     *
     * @return T042fvehicleType
     */
    public function setF042fupdatedBy($f042fupdatedBy)
    {
        $this->f042fupdatedBy = $f042fupdatedBy;

        return $this;
    }

    /**
     * Get f042fupdatedBy
     *
     * @return integer
     */
    public function getF042fupdatedBy()
    {
        return $this->f042fupdatedBy;
    }

    /**
     * Set f042fcreatedDtTm
     *
     * @param \DateTime $f042fcreatedDtTm
     *
     * @return T042fvehicleType
     */
    public function setF042fcreatedDtTm($f042fcreatedDtTm)
    {
        $this->f042fcreatedDtTm = $f042fcreatedDtTm;

        return $this;
    }

    /**
     * Get f042fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fcreatedDtTm()
    {
        return $this->f042fcreatedDtTm;
    }

    /**
     * Set f042fupdatedDtTm
     *
     * @param \DateTime $f042fupdatedDtTm
     *
     * @return T042fvehicleType
     */
    public function setF042fupdatedDtTm($f042fupdatedDtTm)
    {
        $this->f042fupdatedDtTm = $f042fupdatedDtTm;

        return $this;
    }

    /**
     * Get f042fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fupdatedDtTm()
    {
        return $this->f042fupdatedDtTm;
    }
}
