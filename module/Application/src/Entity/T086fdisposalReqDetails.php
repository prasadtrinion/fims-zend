<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T086fdisposalReqDetails
 *
 * @ORM\Table(name="t086fdisposal_req_details")
* @ORM\Entity(repositoryClass="Application\Repository\DisposalRequisitionRepository")
 */
class T086fdisposalReqDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f086fidDetails;


    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid_disposal", type="integer", nullable=false)
     */
    private $f086fidDisposal;

     /**
     * @var integer
     *
     * @ORM\Column(name="f086fid_asset", type="integer", nullable=false)
     */
    private $f086fidAsset;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fasset_code", type="string",length=100, nullable=false)
     */
    private $f086fassetCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdescription", type="string", length=100, nullable=true)
     */
    private $f086fdescription;


    /**
     * @var integer
     *
     * @ORM\Column(name="f086fnet_value", type="integer", nullable=true)
     */
    private $f086fnetValue;


    /**
     * @var integer
     *
     * @ORM\Column(name="f086ftype", type="integer", nullable=true)
     */
    private $f086ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fstatus", type="integer", nullable=true)
     */
    private $f086fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fcreated_by", type="integer", nullable=true)
     */
    private $f086fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fupdated_by", type="integer", nullable=true)
     */
    private $f086fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fupdatedDtTm;

    /**
     * Get f086fidDetails
     *
     * @return integer
     */
    public function getF086fidDetails()
    {
        return $this->f086fidDetails;
    }


    /**
     * Set f086fidDisposal
     *
     * @param integer $f086fidDisposal
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fidDisposal($f086fidDisposal)
    {
        $this->f086fidDisposal = $f086fidDisposal;

        return $this;
    }

    /**
     * Get f086fidDisposal
     *
     * @return integer
     */
    public function getF086fidDisposal()
    {
        return $this->f086fidDisposal;
    }

    /**
     * Set f086fidAsset
     *
     * @param integer $f086fidAsset
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fidAsset($f086fidAsset)
    {
        $this->f086fidAsset = $f086fidAsset;

        return $this;
    }

    /**
     * Get f086fidAsset
     *
     * @return integer
     */
    public function getF086fidAsset()
    {
        return $this->f086fidAsset;
    }

    /**
     * Set f086fassetCode
     *
     * @param integer $f086fassetCode
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fassetCode($f086fassetCode)
    {
        $this->f086fassetCode = $f086fassetCode;

        return $this;
    }

    /**
     * Get f086fassetCode
     *
     * @return integer
     */
    public function getF086fassetCode()
    {
        return $this->f086fassetCode;
    }

    /**
     * Set f086fdescription
     *
     * @param string $f086fdescription
     *
     * @return T086fdisposalDisposal
     */
    public function setF086fdescription($f086fdescription)
    {
        $this->f086fdescription = $f086fdescription;

        return $this;
    }

    /**
     * Get f086fdescription
     *
     * @return string
     */
    public function getF086fdescription()
    {
        return $this->f086fdescription;
    }

    /**
     * Set f086fcreatedBy
     *
     * @param integer $f086fcreatedBy
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fcreatedBy($f086fcreatedBy)
    {
        $this->f086fcreatedBy = $f086fcreatedBy;

        return $this;
    }

    /**
     * Get f086fcreatedBy
     *
     * @return integer
     */
    public function getF086fcreatedBy()
    {
        return $this->f086fcreatedBy;
    }


    /**
     * Set f086fnetValue
     *
     * @param integer $f086fnetValue
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fnetValue($f086fnetValue)
    {
        $this->f086fnetValue = $f086fnetValue;

        return $this;
    }

    /**
     * Get f086fnetValue
     *
     * @return integer
     */
    public function getF086fnetValue()
    {
        return $this->f086fnetValue;
    }


    /**
     * Set f086ftype
     *
     * @param integer $f086ftype
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086ftype($f086ftype)
    {
        $this->f086ftype = $f086ftype;

        return $this;
    }

    /**
     * Get f086ftype
     *
     * @return integer
     */
    public function getF086ftype()
    {
        return $this->f086ftype;
    }

    /**
     * Set f086fupdatedBy
     *
     * @param integer $f086fupdatedBy
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fupdatedBy($f086fupdatedBy)
    {
        $this->f086fupdatedBy = $f086fupdatedBy;

        return $this;
    }

    /**
     * Get f086fupdatedBy
     *
     * @return integer
     */
    public function getF086fupdatedBy()
    {
        return $this->f086fupdatedBy;
    }

    /**
     * Set f086fcreatedDtTm
     *
     * @param \DateTime $f086fcreatedDtTm
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fcreatedDtTm($f086fcreatedDtTm)
    {
        $this->f086fcreatedDtTm = $f086fcreatedDtTm;

        return $this;
    }

    /**
     * Get f086fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fcreatedDtTm()
    {
        return $this->f086fcreatedDtTm;
    }

    /**
     * Set f086fupdatedDtTm
     *
     * @param \DateTime $f086fupdatedDtTm
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fupdatedDtTm($f086fupdatedDtTm)
    {
        $this->f086fupdatedDtTm = $f086fupdatedDtTm;

        return $this;
    }

    /**
     * Get f086fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fupdatedDtTm()
    {
        return $this->f086fupdatedDtTm;
    }

    /**
     * Set f086fstatus
     *
     * @param integer $f086fstatus
     *
     * @return T086fdisposalReqDetails
     */
    public function setF086fstatus($f086fstatus)
    {
        $this->f086fstatus = $f086fstatus;

        return $this;
    }

    /**
     * Get f086fstatus
     *
     * @return integer
     */
    public function getF086fstatus()
    {
        return $this->f086fstatus;
    }

    
}
