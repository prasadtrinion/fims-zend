<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T082ftaxEmployeeDeduction
 *
 * @ORM\Table(name="t082ftax_employee_deduction")
 * @ORM\Entity(repositoryClass="Application\Repository\TaxEmployeeDeductionRepository")
 */
class T082ftaxEmployeeDeduction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f082fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_staff", type="integer", nullable=false)
     */
    private $f082fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f082ftax_code", type="string", length=100, nullable=false)
     */
    private $f082ftaxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f082freference_number", type="string", length=100, nullable=false)
     */
    private $f082freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fq1", type="integer",  nullable=false)
     */
    private $f082fq1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fq2", type="integer",  nullable=false)
     */
    private $f082fq2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fq3", type="integer",  nullable=false)
     */
    private $f082fq3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fq4", type="integer",  nullable=false)
     */
    private $f082fq4;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fcreated_by", type="integer", nullable=true)
     */
    private $f082fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fupdated_by", type="integer", nullable=true)
     */
    private $f082fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fstatus", type="integer", nullable=true)
     */
    private $f082fstatus;



    /**
     * Get f082fid
     *
     * @return integer
     */
    public function getF082fid()
    {
        return $this->f082fid;
    }

    /**
     * Set f082fidStaff
     *
     * @param integer $f082fidStaff
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fidStaff($f082fidStaff)
    {
        $this->f082fidStaff = $f082fidStaff;

        return $this;
    }

    /**
     * Get f082fidStaff
     *
     * @return integer
     */
    public function getF082fidStaff()
    {
        return $this->f082fidStaff;
    }

    /**
     * Set f082ftaxCode
     *
     * @param string $f082ftaxCode
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082ftaxCode($f082ftaxCode)
    {
        $this->f082ftaxCode = $f082ftaxCode;

        return $this;
    }

    /**
     * Get f082ftaxCode
     *
     * @return string
     */
    public function getF082ftaxCode()
    {
        return $this->f082ftaxCode;
    }

    /**
     * Set f082freferenceNumber
     *
     * @param string $f082freferenceNumber
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082freferenceNumber($f082freferenceNumber)
    {
        $this->f082freferenceNumber = $f082freferenceNumber;

        return $this;
    }

    /**
     * Get f082freferenceNumber
     *
     * @return string
     */
    public function getF082freferenceNumber()
    {
        return $this->f082freferenceNumber;
    }

    /**
     * Set f082fq1
     *
     * @param integer $f082fq1
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fq1($f082fq1)
    {
        $this->f082fq1 = $f082fq1;

        return $this;
    }

    /**
     * Get f082fq1
     *
     * @return integer
     */
    public function getF082fq1()
    {
        return $this->f082fq1;
    }

    /**
     * Set f082fq2
     *
     * @param integer $f082fq2
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fq2($f082fq2)
    {
        $this->f082fq2 = $f082fq2;

        return $this;
    }

    /**
     * Get f082fq2
     *
     * @return integer
     */
    public function getF082fq2()
    {
        return $this->f082fq2;
    }
    /**
     * Set f082fq3
     *
     * @param integer $f082fq3
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fq3($f082fq3)
    {
        $this->f082fq3 = $f082fq3;

        return $this;
    }

    /**
     * Get f082fq3
     *
     * @return integer
     */
    public function getF082fq3()
    {
        return $this->f082fq3;
    }
    /**
     * Set f082fq4
     *
     * @param integer $f082fq4
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fq4($f082fq4)
    {
        $this->f082fq4 = $f082fq4;

        return $this;
    }

    /**
     * Get f082fq4
     *
     * @return integer
     */
    public function getF082fq4()
    {
        return $this->f082fq4;
    }

    /**
     * Set f082fcreatedBy
     *
     * @param integer $f082fcreatedBy
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fcreatedBy($f082fcreatedBy)
    {
        $this->f082fcreatedBy = $f082fcreatedBy;

        return $this;
    }

    /**
     * Get f082fcreatedBy
     *
     * @return integer
     */
    public function getF082fcreatedBy()
    {
        return $this->f082fcreatedBy;
    }

    /**
     * Set f082fcreatedDtTm
     *
     * @param \DateTime $f082fcreatedDtTm
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fcreatedDtTm($f082fcreatedDtTm)
    {
        $this->f082fcreatedDtTm = $f082fcreatedDtTm;

        return $this;
    }

    /**
     * Get f082fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fcreatedDtTm()
    {
        return $this->f082fcreatedDtTm;
    }

    /**
     * Set f082fupdatedBy
     *
     * @param integer $f082fupdatedBy
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fupdatedBy($f082fupdatedBy)
    {
        $this->f082fupdatedBy = $f082fupdatedBy;

        return $this;
    }

    /**
     * Get f082fupdatedBy
     *
     * @return integer
     */
    public function getF082fupdatedBy()
    {
        return $this->f082fupdatedBy;
    }

    /**
     * Set f082fupdatedDtTm
     *
     * @param \DateTime $f082fupdatedDtTm
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fupdatedDtTm($f082fupdatedDtTm)
    {
        $this->f082fupdatedDtTm = $f082fupdatedDtTm;

        return $this;
    }

    /**
     * Get f082fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fupdatedDtTm()
    {
        return $this->f082fupdatedDtTm;
    }

    /**
     * Set f082fstatus
     *
     * @param integer $f082fstatus
     *
     * @return T082ftaxEmployeeDeduction
     */
    public function setF082fstatus($f082fstatus)
    {
        $this->f082fstatus = $f082fstatus;

        return $this;
    }

    /**
     * Get f082fstatus
     *
     * @return integer
     */
    public function getF082fstatus()
    {
        return $this->f082fstatus;
    }
}
