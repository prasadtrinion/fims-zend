<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T138fetfGrouping
 *
 * @ORM\Table(name="t138fetf_grouping")
 * @ORM\Entity(repositoryClass="Application\Repository\EtfGroupingRepository")
 */
class T138fetfGrouping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f138fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f138fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f138fbatch_id", type="string",length=50,  nullable=false)
     */
    private $f138fbatchId;

    /**
     * @var string
     *
     * @ORM\Column(name="f138freference_number", type="string",length=50, nullable=false)
     */
    private $f138freferenceNumber;


    /**
     * @var integer
     *
     * @ORM\Column(name="f138fdescription", type="string",length=255, nullable=false)
     * 
     */
    private $f138fdescription;


    /**
     * @var integer
     *
     * @ORM\Column(name="f138famount", type="integer", nullable=false)
     * 
     */
    private $f138famount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f138fstatus", type="integer", nullable=false)
     */
    private $f138fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f138fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f138fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f138fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f138fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f138fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f138fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f138fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f138fupdatedDtTm;
 	

    /**
     * Get f138fid
     *
     * @return integer
     */
    public function getF138fid()
    {
        return $this->f138fid;
    }

    /**
     * Set f138fbatchId
     *
     * @param string $f138fbatchId
     *
     * @return T138fetfGrouping
     */
    public function setF138fbatchId($f138fbatchId)
    {
        $this->f138fbatchId = $f138fbatchId;

        return $this;
    }

    /**
     * Get f138fbatchId
     *
     * @return string
     */
    public function getF138fbatchId()
    {
        return $this->f138fbatchId;
    }

    /**
     * Set f138freferenceNumber
     *
     * @param string $f138freferenceNumber
     *
     * @return T138fetfGrouping
     */
    public function setF138freferenceNumber($f138freferenceNumber)
    {
        $this->f138freferenceNumber = $f138freferenceNumber;

        return $this;
    }

    /**
     * Get f138freferenceNumber
     *
     * @return string
     */
    public function getF138freferenceNumber()
    {
        return $this->f138freferenceNumber;
    }


    /**
     * Set f138fdescription
     *
     * @param integer $f138fdescription
     *
     * @return T138fetfGrouping
     */
    public function setF138fdescription($f138fdescription)
    {
        $this->f138fdescription = $f138fdescription;

        return $this;
    }

    /**
     * Get f138fdescription
     *
     * @return integer
     */
    public function getF138fdescription()
    {
        return $this->f138fdescription;
    }

    /**
     * Set f138famount
     *
     * @param integer $f138famount
     *
     * @return T138fetfGrouping
     */
    public function setF138famount($f138famount)
    {
        $this->f138famount = $f138famount;

        return $this;
    }

    /**
     * Get f138famount
     *
     * @return integer
     */
    public function getF138famount()
    {
        return $this->f138famount;
    }


    /**
     * Set f138fupdatedDtTm
     *
     * @param \DateTime $f138fupdatedDtTm
     *
     * @return T138fetfGrouping
     */
    public function setF138fupdatedDtTm($f138fupdatedDtTm)
    {
        $this->f138fupdatedDtTm = $f138fupdatedDtTm;

        return $this;
    }

    /**
     * Get f138fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF138fupdatedDtTm()
    {
        return $this->f138fupdatedDtTm;
    }

    /**
     * Set f138fcreatedDtTm
     *
     * @param \DateTime $f138fcreatedDtTm
     *
     * @return T138fetfGrouping
     */
    public function setF138fcreatedDtTm($f138fcreatedDtTm)
    {
        $this->f138fcreatedDtTm = $f138fcreatedDtTm;

        return $this;
    }

    /**
     * Get f138fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF138fcreatedDtTm()
    {
        return $this->f138fcreatedDtTm;
    }

    /**
     * Set f138fstatus
     *
     * @param integer $f138fstatus
     *
     * @return T138fetfGrouping
     */
    public function setF138fstatus($f138fstatus)
    {
        $this->f138fstatus = $f138fstatus;

        return $this;
    }

    /**
     * Get f138fstatus
     *
     * @return integer
     */
    public function getF138fstatus()
    {
        return $this->f138fstatus;
    }

    /**
     * Set f138fupdatedBy
     *
     * @param integer $f138fupdatedBy
     *
     * @return T138fetfGrouping
     */
    public function setF138fupdatedBy($f138fupdatedBy)
    {
        $this->f138fupdatedBy = $f138fupdatedBy;

        return $this;
    }

    /**
     * Get f138fupdatedBy
     *
     * @return integer
     */
    public function getF138fupdatedBy()
    {
        return $this->f138fupdatedBy;
    }

    /**
     * Set f138fcreatedBy
     *
     * @param integer $f138fcreatedBy
     *
     * @return T138fetfGrouping
     */
    public function setF138fcreatedBy($f138fcreatedBy)
    {
        $this->f138fcreatedBy = $f138fcreatedBy;

        return $this;
    }

    /**
     * Get f138fupdatedBy
     *
     * @return integer
     */
    public function getF138fcreatedBy()
    {
        return $this->f138fcreatedBy;
    }

}
