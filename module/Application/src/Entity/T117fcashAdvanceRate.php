<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T117fcashAdvanceRate
 *
 * @ORM\Table(name="t117fcash_advance_rate")
 * @ORM\Entity(repositoryClass="Application\Repository\CashAdvanceRateRepository")
 */
class T117fcashAdvanceRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f117fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f117fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f117fstaff_type", type="integer", nullable=true)
     */
    private $f117fstaffType;

    /**
     * @var string
     *
     * @ORM\Column(name="f117ffrom_position_grade", type="string", length=255, nullable=true)
     */
    private $f117ffromPositionGrade;

    /**
     * @var string
     *
     * @ORM\Column(name="f117fto_position_grade", type="string", length=255, nullable=true)
     */
    private $f117ftoPositionGrade;

    /**
     * @var string
     *
     * @ORM\Column(name="f117famount", type="integer", nullable=true)
     */
    private $f117famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f117fstatus", type="integer", nullable=true)
     */
    private $f117fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f117fcreated_by", type="integer", nullable=true)
     */
    private $f117fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f117fupdated_by", type="integer", nullable=true)
     */
    private $f117fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f117fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f117fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f117fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f117fupdatedDtTm;



    /**
     * Get f117fid
     *
     * @return integer
     */
    public function getF117fid()
    {
        return $this->f117fid;
    }

    /**
     * Set f117fstaffType
     *
     * @param string $f117fstaffType
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fstaffType($f117fstaffType)
    {
        $this->f117fstaffType = $f117fstaffType;

        return $this;
    }

    /**
     * Get f117fstaffType
     *
     * @return string
     */
    public function getF117fstaffType()
    {
        return $this->f117fstaffType;
    }

    /**
     * Set f117ffromPositionGrade
     *
     * @param string $f117ffromPositionGrade
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117ffromPositionGrade($f117ffromPositionGrade)
    {
        $this->f117ffromPositionGrade = $f117ffromPositionGrade;

        return $this;
    }

    /**
     * Get f117ffromPositionGrade
     *
     * @return string
     */
    public function getF117ffromPositionGrade()
    {
        return $this->f117ffromPositionGrade;
    }

    /**
     * Set f117ftoPositionGrade
     *
     * @param string $f117ftoPositionGrade
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117ftoPositionGrade($f117ftoPositionGrade)
    {
        $this->f117ftoPositionGrade = $f117ftoPositionGrade;

        return $this;
    }

    /**
     * Get f117ftoPositionGrade
     *
     * @return string
     */
    public function getF117ftoPositionGrade()
    {
        return $this->f117ftoPositionGrade;
    }

    /**
     * Set f117famount
     *
     * @param integer $f117famount
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117famount($f117famount)
    {
        $this->f117famount = $f117famount;

        return $this;
    }

    /**
     * Get f117famount
     *
     * @return integer
     */
    public function getF117famount()
    {
        return $this->f117famount;
    }

    /**
     * Set f117fstatus
     *
     * @param integer $f117fstatus
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fstatus($f117fstatus)
    {
        $this->f117fstatus = $f117fstatus;

        return $this;
    }

    /**
     * Get f117fstatus
     *
     * @return integer
     */
    public function getF117fstatus()
    {
        return $this->f117fstatus;
    }

    /**
     * Set f117fcreatedBy
     *
     * @param integer $f117fcreatedBy
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fcreatedBy($f117fcreatedBy)
    {
        $this->f117fcreatedBy = $f117fcreatedBy;

        return $this;
    }

    /**
     * Get f117fcreatedBy
     *
     * @return integer
     */
    public function getF117fcreatedBy()
    {
        return $this->f117fcreatedBy;
    }

    /**
     * Set f117fupdatedBy
     *
     * @param integer $f117fupdatedBy
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fupdatedBy($f117fupdatedBy)
    {
        $this->f117fupdatedBy = $f117fupdatedBy;

        return $this;
    }

    /**
     * Get f117fupdatedBy
     *
     * @return integer
     */
    public function getF117fupdatedBy()
    {
        return $this->f117fupdatedBy;
    }

    /**
     * Set f117fcreatedDtTm
     *
     * @param \DateTime $f117fcreatedDtTm
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fcreatedDtTm($f117fcreatedDtTm)
    {
        $this->f117fcreatedDtTm = $f117fcreatedDtTm;

        return $this;
    }

    /**
     * Get f117fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF117fcreatedDtTm()
    {
        return $this->f117fcreatedDtTm;
    }

    /**
     * Set f117fupdatedDtTm
     *
     * @param \DateTime $f117fupdatedDtTm
     *
     * @return T117fcashAdvanceRate
     */
    public function setF117fupdatedDtTm($f117fupdatedDtTm)
    {
        $this->f117fupdatedDtTm = $f117fupdatedDtTm;

        return $this;
    }

    /**
     * Get f117fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF117fupdatedDtTm()
    {
        return $this->f117fupdatedDtTm;
    }
}
