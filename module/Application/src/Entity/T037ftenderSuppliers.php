<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T037ftenderSuppliers
 *
 * @ORM\Table(name="t037ftender_suppliers")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSuppliersRepository")
 */
class T037ftenderSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f037fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid_tender", type="integer", nullable=false)
     */
    private $f037fidTender;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fapplied", type="integer", nullable=false)
     */
    private $f037fapplied;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fshortlisted", type="integer", nullable=true)
     */
    private $f037fshortlisted;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037ffinalized", type="integer", nullable=false)
     */
    private $f037ffinalised;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid_supplier", type="integer", nullable=false)
     */
    private $f037fidSupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fstatus", type="integer", nullable=false)
     */
    private $f037fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f037fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f037fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f037fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f037fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f037fupdateDtTm;



    /**
     * Get f037fid
     *
     * @return integer
     */
    public function getF037fid()
    {
        return $this->f037fid;
    }

    /**
     * Set f037fidTender
     *
     * @param string $f037fidTender
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fidTender($f037fidTender)
    {
        $this->f037fidTender = $f037fidTender;

        return $this;
    }

    /**
     * Get f037fidTender
     *
     * @return string
     */
    public function getF037fidTender()
    {
        return $this->f037fidTender;
    }

    /**
     * Set f037fapplied
     *
     * @param integer $f037fapplied
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fapplied($f037fapplied)
    {
        $this->f037fapplied = $f037fapplied;

        return $this;
    }

    /**
     * Get f037fapplied
     *
     * @return integer
     */
    public function getF037fapplied()
    {
        return $this->f037fapplied;
    }

    /**
     * Set f037fshortlisted
     *
     * @param integer $f037fshortlisted
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fshortlisted($f037fshortlisted)
    {
        $this->f037fshortlisted = $f037fshortlisted;

        return $this;
    }

    /**
     * Get f037fshortlisted
     *
     * @return integer
     */
    public function getF037fshortlisted()
    {
        return $this->f037fshortlisted;
    }

    /**
     * Set f037ffinalised
     *
     * @param integer $f037ffinalised
     *
     * @return T037ftenderSuppliers
     */
    public function setF037ffinalised($f037ffinalised)
    {
        $this->f037ffinalised = $f037ffinalised;

        return $this;
    }

    /**
     * Get f037ffinalised
     *
     * @return integer
     */
    public function getF037ffinalised()
    {
        return $this->f037ffinalised;
    }


    /**
     * Get f037fstatus
     *
     * @return integer
     */
    public function getF037fstatus()
    {
        return $this->f037fstatus;
    }

     /**
     * Set f037fstatus
     *
     * @param integer $f037fstatus
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fstatus($f037fstatus)
    {
        $this->f037fstatus = $f037fstatus;

        return $this;
    }

     /**
     * Get f037fidSupplier
     *
     * @return integer
     */
    public function getF037fidSupplier()
    {
        return $this->f037fidSupplier;
    }

     /**
     * Set f037fidSupplier
     *
     * @param integer $f037fidSupplier
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fidSupplier($f037fidSupplier)
    {
        $this->f037fidSupplier = $f037fidSupplier;

        return $this;
    }
    
     /**
     * Set f037fcreatedBy
     *
     * @param integer $f037fcreatedBy
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fcreatedBy($f037fcreatedBy)
    {
        $this->f037fcreatedBy = $f037fcreatedBy;

        return $this;
    }
   
   
    /**
     * Get f037fcreatedBy
     *
     * @return integer
     */
    public function getF037fcreatedBy()
    {
        return $this->f037fcreatedBy;
    }

    /**
     * Set f037fupdatedBy
     *
     * @param integer $f037fupdatedBy
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fupdatedBy($f037fupdatedBy)
    {
        $this->f037fupdatedBy = $f037fupdatedBy;

        return $this;
    }

    /**
     * Get f037fupdatedBy
     *
     * @return integer
     */
    public function getF037fupdatedBy()
    {
        return $this->f037fupdatedBy;
    }

    /**
     * Set f037fcreateDtTm
     *
     * @param \DateTime $f037fcreateDtTm
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fcreateDtTm($f037fcreateDtTm)
    {
        $this->f037fcreateDtTm = $f037fcreateDtTm;

        return $this;
    }

    /**
     * Get f037fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF037fcreateDtTm()
    {
        return $this->f037fcreateDtTm;
    }

    /**
     * Set f037fupdateDtTm
     *
     * @param \DateTime $f037fupdateDtTm
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fupdateDtTm($f037fupdateDtTm)
    {
        $this->f037fupdateDtTm = $f037fupdateDtTm;

        return $this;
    }

    /**
     * Get f037fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF037fupdateDtTm()
    {
        return $this->f037fupdateDtTm;
    }
}

























