<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T019fremunerationType
 *
 * @ORM\Table(name="t019fremuneration_type", indexes={@ORM\Index(name="f019fupdated_by", columns={"f019fupdated_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\RemunerationtypeRepository")
 */
class T019fremunerationType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f019fid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f019fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f019ftype", type="string", length=50, nullable=false)
     */
    private $f019ftype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f019fstatus", type="boolean", nullable=false)
     */
    private $f019fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fupdated_by", type="bigint", nullable=false)
     */
    private $f019fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f019fcreated_by", type="string", length=30, nullable=false)
     */
    private $f019fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fcreatedDtTm;



    /**
     * Get f019fid
     *
     * @return integer
     */
    public function getF019fid()
    {
        return $this->f019fid;
    }

    /**
     * Set f019ftype
     *
     * @param string $f019ftype
     *
     * @return T019fremunerationType
     */
    public function setF019ftype($f019ftype)
    {
        $this->f019ftype = $f019ftype;

        return $this;
    }

    /**
     * Get f019ftype
     *
     * @return string
     */
    public function getF019ftype()
    {
        return $this->f019ftype;
    }

    /**
     * Set f019fstatus
     *
     * @param boolean $f019fstatus
     *
     * @return T019fremunerationType
     */
    public function setF019fstatus($f019fstatus)
    {
        $this->f019fstatus = $f019fstatus;

        return $this;
    }

    /**
     * Get f019fstatus
     *
     * @return boolean
     */
    public function getF019fstatus()
    {
        return $this->f019fstatus;
    }

    /**
     * Set f019fupdatedBy
     *
     * @param integer $f019fupdatedBy
     *
     * @return T019fremunerationType
     */
    public function setF019fupdatedBy($f019fupdatedBy)
    {
        $this->f019fupdatedBy = $f019fupdatedBy;

        return $this;
    }

    /**
     * Get f019fupdatedBy
     *
     * @return integer
     */
    public function getF019fupdatedBy()
    {
        return $this->f019fupdatedBy;
    }

    /**
     * Set f019fupdatedDtTm
     *
     * @param \DateTime $f019fupdatedDtTm
     *
     * @return T019fremunerationType
     */
    public function setF019fupdatedDtTm($f019fupdatedDtTm)
    {
        $this->f019fupdatedDtTm = $f019fupdatedDtTm;

        return $this;
    }

    /**
     * Get f019fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fupdatedDtTm()
    {
        return $this->f019fupdatedDtTm;
    }

    /**
     * Set f019fcreatedBy
     *
     * @param string $f019fcreatedBy
     *
     * @return T019fremunerationType
     */
    public function setF019fcreatedBy($f019fcreatedBy)
    {
        $this->f019fcreatedBy = $f019fcreatedBy;

        return $this;
    }

    /**
     * Get f019fcreatedBy
     *
     * @return string
     */
    public function getF019fcreatedBy()
    {
        return $this->f019fcreatedBy;
    }

    /**
     * Set f019fcreatedDtTm
     *
     * @param \DateTime $f019fcreatedDtTm
     *
     * @return T019fremunerationType
     */
    public function setF019fcreatedDtTm($f019fcreatedDtTm)
    {
        $this->f019fcreatedDtTm = $f019fcreatedDtTm;

        return $this;
    }

    /**
     * Get f019fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fcreatedDtTm()
    {
        return $this->f019fcreatedDtTm;
    }
}
