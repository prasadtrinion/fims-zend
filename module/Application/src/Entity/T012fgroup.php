<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T012fgroup
 *
 * @ORM\Table(name="t012fgroup", uniqueConstraints={@ORM\UniqueConstraint(name="f012fgroup_name_UNIQUE", columns={"f012fgroup_name"})})
* @ORM\Entity(repositoryClass="Application\Repository\GroupRepository")
 */
class T012fgroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f012fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f012fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fgroup_name", type="string", length=45, nullable=false)
     */
    private $f012fgroupName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f012fid_parent", type="integer", nullable=true)
     */
    private $f012fidParent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f012forder", type="integer", nullable=false)
     */
    private $f012forder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f012fstatus", type="boolean", length=40, nullable=false)
     */
    private $f012fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fcreated_by", type="string", length=50, nullable=false)
     */
    private $f012fcreatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fupdated_by", type="string", length=50, nullable=false)
     */
    private $f012fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f012fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f012fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f012fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f012fupdateDtTm;

    /**
     * Get f012fid
     *
     * @return integer
     */
    public function getF012fid()
    {
        return $this->f012fid;
    }

    /**
     * Set f012fgroupName
     *
     * @param string $f012fgroupName
     *
     * @return T012fgroup
     */
    public function setF012fgroupName($f012fgroupName)
    {
        $this->f012fgroupName = $f012fgroupName;

        return $this;
    }

    /**
     * Get f012fgroupName
     *
     * @return string
     */
    public function getF012fgroupName()
    {
        return $this->f012fgroupName;
    }

    /**
     * Set f012fidParent
     *
     * @param integer $f012fidParent
     *
     * @return T012fgroup
     */
    public function setF012fidParent($f012fidParent)
    {
        $this->f012fidParent = $f012fidParent;

        return $this;
    }

    /**
     * Get f012fidParent
     *
     * @return integer
     */
    public function getF012fidParent()
    {
        return $this->f012fidParent;
    }

    /**
     * Set f012forder
     *
     * @param integer $f012forder
     *
     * @return T012fgroup
     */
    public function setF012forder($f012forder)
    {
        $this->f012forder = $f012forder;

        return $this;
    }

    /**
     * Get f012forder
     *
     * @return integer
     */
    public function getF012forder()
    {
        return $this->f012forder;
    }

    /**
     * Set f012fstatus
     *
     * @param string $f012fstatus
     *
     * @return T012fgroup
     */
    public function setF012fstatus($f012fstatus)
    {
        $this->f012fstatus = $f012fstatus;

        return $this;
    }

    /**
     * Get f012fstatus
     *
     * @return string
     */
    public function getF012fstatus()
    {
        return $this->f012fstatus;
    }

    /**
     * Set f012fcreatedBy
     *
     * @param string $f012fcreatedBy
     *
     * @return T012fgroup
     */
    public function setF012fcreatedBy($f012fcreatedBy)
    {
        $this->f012fcreatedBy = $f012fcreatedBy;

        return $this;
    }

    /**
     * Get f012fcreatedBy
     *
     * @return string
     */
    public function getF012fcreatedBy()
    {
        return $this->f012fcreatedBy;
    }

    /**
     * Set f012fupdatedBy
     *
     * @param string $f012fupdatedBy
     *
     * @return T012fgroup
     */
    public function setF012fupdatedBy($f012fupdatedBy)
    {
        $this->f012fupdatedBy = $f012fupdatedBy;

        return $this;
    }

    /**
     * Get f012fupdatedBy
     *
     * @return string
     */
    public function getF012fupdatedBy()
    {
        return $this->f012fupdatedBy;
    }

    /**
     * Set f012fcreateDtTm
     *
     * @param \DateTime $f012fcreateDtTm
     *
     * @return T012fgroup
     */
    public function setF012fcreateDtTm($f012fcreateDtTm)
    {
        $this->f012fcreateDtTm = $f012fcreateDtTm;

        return $this;
    }

    /**
     * Get f012fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF012fcreateDtTm()
    {
        return $this->f012fcreateDtTm;
    }

    /**
     * Set f012fupdateDtTm
     *
     * @param \DateTime $f012fupdateDtTm
     *
     * @return T012fgroup
     */
    public function setF012fupdateDtTm($f012fupdateDtTm)
    {
        $this->f012fupdateDtTm = $f012fupdateDtTm;

        return $this;
    }

    /**
     * Get f012fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF012fupdateDtTm()
    {
        return $this->f012fupdateDtTm;
    }
}





