<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T055fincrementMaster
 *
 * @ORM\Table(name="t055fincrement_master")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetIncrementRepository")
 */
class T055fincrementMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f055fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid_financial_year", type="integer", nullable=false)
     */
    private $f055fidFinancialYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f055fdepartment", type="string",length=20, nullable=false)
     */
    private $f055fdepartment;

     /**
     * @var string
     *
     * @ORM\Column(name="f055ffund", type="string",length=20, nullable=false)
     */
    private $f055ffund;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f055ftotal_amount", type="integer", nullable=false)
     */
    private $f055ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fapproval_status", type="integer", nullable=true)
     */
    private $f055fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fstatus", type="integer", nullable=false)
     */
    private $f055fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fcreated_by", type="integer", nullable=false)
     */
    private $f055fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fupdated_by", type="integer", nullable=false)
     */
    private $f055fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fupdatedDtTm;


    /**
     * @var string
     *
     * @ORM\Column(name="f055freason", type="string",length=50, nullable=true)
     */
    private $f055freason='NULL';

    /**
     * Get f055fid
     *
     * @return integer
     */
    public function getF055fid()
    {
        return $this->f055fid;
    }

    /**
     * Set f055fidFinancialYear
     *
     * @param integer $f055fidFinancialYear
     *
     * @return T055fincrementMaster
     */
    public function setF055fidFinancialYear($f055fidFinancialYear)
    {
        $this->f055fidFinancialYear = $f055fidFinancialYear;

        return $this;
    }

    /**
     * Get f055fidFinancialYear
     *
     * @return integer
     */
    public function getF055fidFinancialYear()
    {
        return $this->f055fidFinancialYear;
    }

    /**
     * Set f055fdepartment
     *
     * @param string $f055fdepartment
     *
     * @return T055fincrementMaster
     */
    public function setF055fdepartment($f055fdepartment)
    {
        $this->f055fdepartment = $f055fdepartment;

        return $this;
    }

    /**
     * Get f055fdepartment
     *
     * @return string
     */
    public function getF055fdepartment()
    {
        return $this->f055fdepartment;
    }

     /**
     * Set f055ffund
     *
     * @param string $f055ffund
     *
     * @return T055fincrementMaster
     */
    public function setF055ffund($f055ffund)
    {
        $this->f055ffund = $f055ffund;

        return $this;
    }

    /**
     * Get f055ffund
     *
     * @return string
     */
    public function getF055ffund()
    {
        return $this->f055ffund;
    }

    
    /**
     * Set f055ftotalAmount
     *
     * @param integer $f055ftotalAmount
     *
     * @return T055fincrementMaster
     */
    public function setF055ftotalAmount($f055ftotalAmount)
    {
        $this->f055ftotalAmount = $f055ftotalAmount;

        return $this;
    }

    /**
     * Get f055ftotalAmount
     *
     * @return integer
     */
    public function getF055ftotalAmount()
    {
        return $this->f055ftotalAmount;
    }

    /**
     * Set f055fapprovalStatus
     *
     * @param integer $f055fapprovalStatus
     *
     * @return T055fincrementMaster
     */
    public function setF055fapprovalStatus($f055fapprovalStatus)
    {
        $this->f055fapprovalStatus = $f055fapprovalStatus;

        return $this;
    }

    /**
     * Get f055fapprovalStatus
     *
     * @return integer
     */
    public function getF055fapprovalStatus()
    {
        return $this->f055fapprovalStatus;
    }

    /**
     * Set f055fstatus
     *
     * @param integer $f055fstatus
     *
     * @return T055fincrementMaster
     */
    public function setF055fstatus($f055fstatus)
    {
        $this->f055fstatus = $f055fstatus;

        return $this;
    }

    /**
     * Get f055fstatus
     *
     * @return integer
     */
    public function getF055fstatus()
    {
        return $this->f055fstatus;
    }

    /**
     * Set f055fcreatedBy
     *
     * @param integer $f055fcreatedBy
     *
     * @return T055fincrementMaster
     */
    public function setF055fcreatedBy($f055fcreatedBy)
    {
        $this->f055fcreatedBy = $f055fcreatedBy;

        return $this;
    }

    /**
     * Get f055fcreatedBy
     *
     * @return integer
     */
    public function getF055fcreatedBy()
    {
        return $this->f055fcreatedBy;
    }

    /**
     * Set f055fupdatedBy
     *
     * @param integer $f055fupdatedBy
     *
     * @return T055fincrementMaster
     */
    public function setF055fupdatedBy($f055fupdatedBy)
    {
        $this->f055fupdatedBy = $f055fupdatedBy;

        return $this;
    }

    /**
     * Get f055fupdatedBy
     *
     * @return integer
     */
    public function getF055fupdatedBy()
    {
        return $this->f055fupdatedBy;
    }

    /**
     * Set f055fcreatedDtTm
     *
     * @param \DateTime $f055fcreatedDtTm
     *
     * @return T055fincrementMaster
     */
    public function setF055fcreatedDtTm($f055fcreatedDtTm)
    {
        $this->f055fcreatedDtTm = $f055fcreatedDtTm;

        return $this;
    }

    /**
     * Get f055fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fcreatedDtTm()
    {
        return $this->f055fcreatedDtTm;
    }

    /**
     * Set f055fupdatedDtTm
     *
     * @param \DateTime $f055fupdatedDtTm
     *
     * @return T055fincrementMaster
     */
    public function setF055fupdatedDtTm($f055fupdatedDtTm)
    {
        $this->f055fupdatedDtTm = $f055fupdatedDtTm;

        return $this;
    }

    /**
     * Get f055fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fupdatedDtTm()
    {
        return $this->f055fupdatedDtTm;
    }

    

    /**
     * Set f055freason
     *
     * @param string $f055freason
     *
     * @return T055fincrementMaster
     */
    public function setF055freason($f055freason)
    {
        $this->f055freason = $f055freason;

        return $this;
    }

    /**
     * Get f055freason
     *
     * @return string
     */
    public function getF055freason()
    {
        return $this->f055freason;
    }

    
}
