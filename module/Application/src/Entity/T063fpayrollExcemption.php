<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fpayrollExcemption
 *
 * @ORM\Table(name="t063fpayroll_excemption")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollExcemptionRepository")
 */
class T063fpayrollExcemption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fcategory", type="string", length=25, nullable=true)
     */
    private $f063fcategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f063findividual_expenses", type="string", length=50, nullable=true)
     */
    private $f063findividualExpenses;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fspouse_expenses", type="string", length=50, nullable=true)
     */
    private $f063fspouseExpenses;

     /**
     * @var string
     *
     * @ORM\Column(name="f063fchild_expenses", type="string", length=50, nullable=true)
     */
    private $f063fchildExpenses;

     /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=true)
     */
    private $f063fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=true)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=true)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f063fupdatedDtTm;



    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063fcategory
     *
     * @param string $f063fcategory
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fcategory($f063fcategory)
    {
        $this->f063fcategory = $f063fcategory;

        return $this;
    }

    /**
     * Get f063fcategory
     *
     * @return string
     */
    public function getF063fcategory()
    {
        return $this->f063fcategory;
    }

    /**
     * Set f063findividualExpenses
     *
     * @param string $f063findividualExpenses
     *
     * @return T063fpayrollExcemption
     */
    public function setF063findividualExpenses($f063findividualExpenses)
    {
        $this->f063findividualExpenses = $f063findividualExpenses;

        return $this;
    }

    /**
     * Get f063findividualExpenses
     *
     * @return string
     */
    public function getF063findividualExpenses()
    {
        return $this->f063findividualExpenses;
    }

    /**
     * Set f063fspouseExpenses
     *
     * @param string $f063fspouseExpenses
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fspouseExpenses($f063fspouseExpenses)
    {
        $this->f063fspouseExpenses = $f063fspouseExpenses;

        return $this;
    }

    /**
     * Get f063fspouseExpenses
     *
     * @return string
     */
    public function getF063fspouseExpenses()
    {
        return $this->f063fspouseExpenses;
    }

    /**
     * Set f063fchildExpenses
     *
     * @param string $f063fchildExpenses
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fchildExpenses($f063fchildExpenses)
    {
        $this->f063fchildExpenses = $f063fchildExpenses;

        return $this;
    }

    /**
     * Get f063fchildExpenses
     *
     * @return string
     */
    public function getF063fchildExpenses()
    {
        return $this->f063fchildExpenses;
    }

    
     /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fpayrollExcemption
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

    
}
