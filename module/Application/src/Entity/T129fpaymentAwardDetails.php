<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T129fpaymentAwardDetails
 *
 * @ORM\Table(name="t129fpayment_award_details")
 * @ORM\Entity(repositoryClass="Application\Repository\PaymentAwardRepository")
 */
class T129fpaymentAwardDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f129fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f129fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f129fid_award", type="integer", nullable=false)
     */
    private $f129fidAward;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f129fcalender", type="datetime", nullable=false)
     */
    private $f129fcalender;

    /**
     * @var integer
     *
     * @ORM\Column(name="f129famount", type="integer", nullable=false)
     */
    private $f129famount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f129fstatus", type="integer", nullable=true)
     */
    private $f129fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f129fcreated_by", type="integer", nullable=false)
     */
    private $f129fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f129fupdated_by", type="integer", nullable=false)
     */
    private $f129fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f129fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f129fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f129fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f129fupdatedDtTm;


    /**
     * Get f129fid
     *
     * @return integer
     */
    public function getF129fid()
    {
        return $this->f129fid;
    }


    /**
     * Set f129fidAward
     *
     * @param integer $f129fidAward
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fidAward($f129fidAward)
    {
        $this->f129fidAward = $f129fidAward;

        return $this;
    }

    /**
     * Get f129fidAward
     *
     * @return integer
     */
    public function getF129fidAward()
    {
        return $this->f129fidAward;
    }

    /**
     * Set f129fcalender
     *
     * @param integer $f129fcalender
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fcalender($f129fcalender)
    {
        $this->f129fcalender = $f129fcalender;

        return $this;
    }

    /**
     * Get f129fcalender
     *
     * @return integer
     */
    public function getF129fcalender()
    {
        return $this->f129fcalender;
    }


    /**
     * Set f129famount
     *
     * @param integer $f129famount
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129famount($f129famount)
    {
        $this->f129famount = $f129famount;

        return $this;
    }

    /**
     * Get f129famount
     *
     * @return integer
     */
    public function getF129famount()
    {
        return $this->f129famount;
    }


    /**
     * Set f129fstatus
     *
     * @param integer $f129fstatus
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fstatus($f129fstatus)
    {
        $this->f129fstatus = $f129fstatus;

        return $this;
    }

    /**
     * Get f129fstatus
     *
     * @return integer
     */
    public function getF129fstatus()
    {
        return $this->f129fstatus;
    }

    /**
     * Set f129fcreatedBy
     *
     * @param integer $f129fcreatedBy
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fcreatedBy($f129fcreatedBy)
    {
        $this->f129fcreatedBy = $f129fcreatedBy;

        return $this;
    }

    /**
     * Get f129fcreatedBy
     *
     * @return integer
     */
    public function getF129fcreatedBy()
    {
        return $this->f129fcreatedBy;
    }

    /**
     * Set f129fupdatedBy
     *
     * @param integer $f129fupdatedBy
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fupdatedBy($f129fupdatedBy)
    {
        $this->f129fupdatedBy = $f129fupdatedBy;

        return $this;
    }

    /**
     * Get f129fupdatedBy
     *
     * @return integer
     */
    public function getF129fupdatedBy()
    {
        return $this->f129fupdatedBy;
    }

    /**
     * Set f129fcreatedDtTm
     *
     * @param \DateTime $f129fcreatedDtTm
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fcreatedDtTm($f129fcreatedDtTm)
    {
        $this->f129fcreatedDtTm = $f129fcreatedDtTm;

        return $this;
    }

    /**
     * Get f129fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF129fcreatedDtTm()
    {
        return $this->f129fcreatedDtTm;
    }

    /**
     * Set f129fupdatedDtTm
     *
     * @param \DateTime $f129fupdatedDtTm
     *
     * @return T129fpaymentAwardDetails
     */
    public function setF129fupdatedDtTm($f129fupdatedDtTm)
    {
        $this->f129fupdatedDtTm = $f129fupdatedDtTm;

        return $this;
    }

    /**
     * Get f129fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF129fupdatedDtTm()
    {
        return $this->f129fupdatedDtTm;
    }
}

