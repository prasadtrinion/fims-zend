<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T094fpaymentVoucherDetails
 *
 * @ORM\Table(name="t094fpayment_voucher_details")
 * @ORM\Entity(repositoryClass="Application\Repository\PaymentVoucherRepository")
 */
class T094fpaymentVoucherDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f094fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f094fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094fid_payment", type="integer", nullable=false)
     */
    private $f094fidPayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094id_bill_registration", type="integer", nullable=false)
     */
    private $f094idBillRegistration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f094fvoucher_date", type="datetime", nullable=false)
     */
    private $f094fvoucherDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094ftotal_amount", type="integer", nullable=false)
     */
    private $f094ftotalAmount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f094fpaid_amount", type="integer", nullable=false)
     */
    private $f094fpaidAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094fstatus", type="integer", nullable=true)
     */
    private $f094fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094fcreated_by", type="integer", nullable=false)
     */
    private $f094fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f094fupdated_by", type="integer", nullable=false)
     */
    private $f094fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f094fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f094fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f094fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f094fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f094fcustomer_name", type="string", nullable=true)
     */
    private $f094fcustomerName;



    /**
     * Get f094fid
     *
     * @return integer
     */
    public function getF094fid()
    {
        return $this->f094fid;
    }

    /**
     * Set f094fidPayment
     *
     * @param integer $f094fidPayment
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fidPayment($f094fidPayment)
    {
        $this->f094fidPayment = $f094fidPayment;

        return $this;
    }

    /**
     * Get f094fidPayment
     *
     * @return integer
     */
    public function getF094fidPayment()
    {
        return $this->f094fidPayment;
    }

    /**
     * Set f094idBillRegistration
     *
     * @param integer $f094idBillRegistration
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094idBillRegistration($f094idBillRegistration)
    {
        $this->f094idBillRegistration = $f094idBillRegistration;

        return $this;
    }

    /**
     * Get f094idBillRegistration
     *
     * @return integer
     */
    public function getF094idBillRegistration()
    {
        return $this->f094idBillRegistration;
    }


    /**
     * Set f094fvoucherDate
     *
     * @param \DateTime $f094fvoucherDate
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fvoucherDate($f094fvoucherDate)
    {
        $this->f094fvoucherDate = $f094fvoucherDate;

        return $this;
    }

    /**
     * Get f094fvoucherDate
     *
     * @return \DateTime
     */
    public function getF094fvoucherDate()
    {
        return $this->f094fvoucherDate;
    }

    /**
     * Set f094ftotalAmount
     *
     * @param integer $f094ftotalAmount
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094ftotalAmount($f094ftotalAmount)
    {
        $this->f094ftotalAmount = $f094ftotalAmount;

        return $this;
    }

    /**
     * Get f094ftotalAmount
     *
     * @return integer
     */
    public function getF094ftotalAmount()
    {
        return $this->f094ftotalAmount;
    }


    /**
     * Set f094fpaidAmount
     *
     * @param integer $f094fpaidAmount
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fpaidAmount($f094fpaidAmount)
    {
        $this->f094fpaidAmount = $f094fpaidAmount;

        return $this;
    }

    /**
     * Get f094fpaidAmount
     *
     * @return integer
     */
    public function getF094fpaidAmount()
    {
        return $this->f094fpaidAmount;
    }

    /**
     * Set f094fstatus
     *
     * @param integer $f094fstatus
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fstatus($f094fstatus)
    {
        $this->f094fstatus = $f094fstatus;

        return $this;
    }

    /**
     * Get f094fstatus
     *
     * @return integer
     */
    public function getF094fstatus()
    {
        return $this->f094fstatus;
    }

    /**
     * Set f094fcreatedBy
     *
     * @param integer $f094fcreatedBy
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fcreatedBy($f094fcreatedBy)
    {
        $this->f094fcreatedBy = $f094fcreatedBy;

        return $this;
    }

    /**
     * Get f094fcreatedBy
     *
     * @return integer
     */
    public function getF094fcreatedBy()
    {
        return $this->f094fcreatedBy;
    }

    /**
     * Set f094fupdatedBy
     *
     * @param integer $f094fupdatedBy
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fupdatedBy($f094fupdatedBy)
    {
        $this->f094fupdatedBy = $f094fupdatedBy;

        return $this;
    }

    /**
     * Get f094fupdatedBy
     *
     * @return integer
     */
    public function getF094fupdatedBy()
    {
        return $this->f094fupdatedBy;
    }

    /**
     * Set f094fcreatedDtTm
     *
     * @param \DateTime $f094fcreatedDtTm
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fcreatedDtTm($f094fcreatedDtTm)
    {
        $this->f094fcreatedDtTm = $f094fcreatedDtTm;

        return $this;
    }

    /**
     * Get f094fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF094fcreatedDtTm()
    {
        return $this->f094fcreatedDtTm;
    }

    /**
     * Set f094fupdatedDtTm
     *
     * @param \DateTime $f094fupdatedDtTm
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fupdatedDtTm($f094fupdatedDtTm)
    {
        $this->f094fupdatedDtTm = $f094fupdatedDtTm;

        return $this;
    }

    /**
     * Get f094fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF094fupdatedDtTm()
    {
        return $this->f094fupdatedDtTm;
    }

    /**
     * Set f094fcustomerName
     *
     * @param integer $f094fcustomerName
     *
     * @return T094fpaymentVoucherDetails
     */
    public function setF094fcustomerName($f094fcustomerName)
    {
        $this->f094fcustomerName = $f094fcustomerName;

        return $this;
    }

    /**
     * Get f094fcustomerName
     *
     * @return integer
     */
    public function getF094fcustomerName()
    {
        return $this->f094fcustomerName;
    }
}

