<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T048ffeeStructureDetails
 *
 * @ORM\Table(name="t048ffee_structure_details")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeStructureRepository")
 */
class T048ffeeStructureDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f048fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_fee", type="integer", nullable=true)
     */
    private $f048fidFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_fee_item", type="integer", nullable=true)
     */
    private $f048fidFeeItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_fee_category", type="integer", nullable=true)
     */
    private $f048fidFeeCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_currency", type="integer", nullable=true)
     */
    private $f048fidCurrency;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048ffrequency_mode", type="integer", nullable=true)
     */
    private $f048ffrequencyMode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcal_type", type="integer", nullable=true)
     */
    private $f048fcalType;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fcr_fund", type="string", length=20, nullable=false)
     */
    private $f048fcrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fcr_activity", type="string", length=20, nullable=true)
     */
    private $f048fcrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fcr_department", type="string", length=20, nullable=false)
     */
    private $f048fcrDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fcr_account", type="string", length=20, nullable=false)
     */
    private $f048fcrAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fdr_fund", type="string", length=20, nullable=false)
     */
    private $f048fdrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fdr_activity", type="string", length=20, nullable=true)
     */
    private $f048fdrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fdr_department", type="string", length=20, nullable=false)
     */
    private $f048fdrDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fdr_account", type="string", length=20, nullable=false)
     */
    private $f048fdrAccount;


    /**
     * @var float
     *
     * @ORM\Column(name="f048famount", type="integer", nullable=true)
     */
    private $f048famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048ffee_type", type="integer", nullable=true)
     */
    private $f048ffeeType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstatus", type="integer", nullable=true)
     */
    private $f048fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcreated_by", type="integer", nullable=true)
     */
    private $f048fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fupdated_by", type="integer", nullable=true)
     */
    private $f048fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fupdatedDtTm;



    /**
     * Get f048fidDetails
     *
     * @return integer
     */
    public function getF048fidDetails()
    {
        return $this->f048fidDetails;
    }

    /**
     * Set f048fidFee
     *
     * @param integer $f048fidFee
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fidFee($f048fidFee)
    {
        $this->f048fidFee = $f048fidFee;

        return $this;
    }

    /**
     * Get f048fidFee
     *
     * @return integer
     */
    public function getF048fidFee()
    {
        return $this->f048fidFee;
    }

    /**
     * Set f048fidFeeItem
     *
     * @param integer $f048fidFeeItem
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fidFeeItem($f048fidFeeItem)
    {
        $this->f048fidFeeItem = $f048fidFeeItem;

        return $this;
    }

    /**
     * Get f048fidFeeItem
     *
     * @return integer
     */
    public function getF048fidFeeItem()
    {
        return $this->f048fidFeeItem;
    }

    /**
     * Set f048fidFeeCategory
     *
     * @param integer $f048fidFeeCategory
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fidFeeCategory($f048fidFeeCategory)
    {
        $this->f048fidFeeCategory = $f048fidFeeCategory;

        return $this;
    }

    /**
     * Get f048fidFeeCategory
     *
     * @return integer
     */
    public function getF048fidFeeCategory()
    {
        return $this->f048fidFeeCategory;
    }
    /**
     * Set f048fidCurrency
     *
     * @param integer $f048fidCurrency
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fidCurrency($f048fidCurrency)
    {
        $this->f048fidCurrency = $f048fidCurrency;

        return $this;
    }

    /**
     * Get f048fidCurrency
     *
     * @return integer
     */
    public function getF048fidCurrency()
    {
        return $this->f048fidCurrency;
    }

    /**
     * Set f048fcalType
     *
     * @param integer $f048fcalType
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcalType($f048fcalType)
    {
        $this->f048fcalType = $f048fcalType;

        return $this;
    }

    /**
     * Get f048fcalType
     *
     * @return integer
     */
    public function getF048fcalType()
    {
        return $this->f048fcalType;
    }

    /**
     * Set f048famount
     *
     * @param float $f048famount
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048famount($f048famount)
    {
        $this->f048famount = $f048famount;

        return $this;
    }

    /**
     * Get f048famount
     *
     * @return float
     */
    public function getF048famount()
    {
        return $this->f048famount;
    }

    /**
     * Set f048ffrequencyMode
     *
     * @param integer $f048ffrequencyMode
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048ffrequencyMode($f048ffrequencyMode)
    {
        $this->f048ffrequencyMode = $f048ffrequencyMode;

        return $this;
    }

    /**
     * Get f048ffrequencyMode
     *
     * @return integer
     */
    public function getF048ffrequencyMode()
    {
        return $this->f048ffrequencyMode;
    }

    /**
     * Set f048fstatus
     *
     * @param integer $f048fstatus
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fstatus($f048fstatus)
    {
        $this->f048fstatus = $f048fstatus;

        return $this;
    }

    /**
     * Get f048fstatus
     *
     * @return integer
     */
    public function getF048fstatus()
    {
        return $this->f048fstatus;
    }

    /**
     * Set f048ffeeType
     *
     * @param integer $f048ffeeType
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048ffeeType($f048ffeeType)
    {
        $this->f048ffeeType = $f048ffeeType;

        return $this;
    }

    /**
     * Get f048ffeeType
     *
     * @return integer
     */
    public function getF048ffeeType()
    {
        return $this->f048ffeeType;
    }

    /**
     * Set f048fcreatedBy
     *
     * @param integer $f048fcreatedBy
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcreatedBy($f048fcreatedBy)
    {
        $this->f048fcreatedBy = $f048fcreatedBy;

        return $this;
    }

    /**
     * Get f048fcreatedBy
     *
     * @return integer
     */
    public function getF048fcreatedBy()
    {
        return $this->f048fcreatedBy;
    }

    /**
     * Set f048fupdatedBy
     *
     * @param integer $f048fupdatedBy
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fupdatedBy($f048fupdatedBy)
    {
        $this->f048fupdatedBy = $f048fupdatedBy;

        return $this;
    }

    /**
     * Get f048fupdatedBy
     *
     * @return integer
     */
    public function getF048fupdatedBy()
    {
        return $this->f048fupdatedBy;
    }

    /**
     * Set f048fcreatedDtTm
     *
     * @param \DateTime $f048fcreatedDtTm
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcreatedDtTm($f048fcreatedDtTm)
    {
        $this->f048fcreatedDtTm = $f048fcreatedDtTm;

        return $this;
    }

    /**
     * Get f048fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fcreatedDtTm()
    {
        return $this->f048fcreatedDtTm;
    }

    /**
     * Set f048fupdatedDtTm
     *
     * @param \DateTime $f048fupdatedDtTm
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fupdatedDtTm($f048fupdatedDtTm)
    {
        $this->f048fupdatedDtTm = $f048fupdatedDtTm;

        return $this;
    }

    /**
     * Get f048fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fupdatedDtTm()
    {
        return $this->f048fupdatedDtTm;
    }

    /**
     * Set f048fcrFund
     *
     * @param string $f048fcrFund
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcrFund($f048fcrFund)
    {
        $this->f048fcrFund = $f048fcrFund;

        return $this;
    }

    /**
     * Get f048fcrFund
     *
     * @return string
     */
    public function getF048fcrFund()
    {
        return $this->f048fcrFund;
    }

    /**
     * Set f048fcrActivity
     *
     * @param string $f048fcrActivity
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcrActivity($f048fcrActivity)
    {
        $this->f048fcrActivity = $f048fcrActivity;

        return $this;
    }

    /**
     * Get f048fcrActivity
     *
     * @return string
     */
    public function getF048fcrActivity()
    {
        return $this->f048fcrActivity;
    }

    /**
     * Set f048fcrDepartment
     *
     * @param string $f048fcrDepartment
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcrDepartment($f048fcrDepartment)
    {
        $this->f048fcrDepartment = $f048fcrDepartment;

        return $this;
    }

    /**
     * Get f048fcrDepartment
     *
     * @return string
     */
    public function getF048fcrDepartment()
    {
        return $this->f048fcrDepartment;
    }

    /**
     * Set f048fcrAccount
     *
     * @param string $f048fcrAccount
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fcrAccount($f048fcrAccount)
    {
        $this->f048fcrAccount = $f048fcrAccount;

        return $this;
    }

    /**
     * Get f048fcrAccount
     *
     * @return string
     */
    public function getF048fcrAccount()
    {
        return $this->f048fcrAccount;
    }

    /**
     * Set f048fdrFund
     *
     * @param string $f048fdrFund
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fdrFund($f048fdrFund)
    {
        $this->f048fdrFund = $f048fdrFund;

        return $this;
    }

    /**
     * Get f048fdrFund
     *
     * @return string
     */
    public function getF048fdrFund()
    {
        return $this->f048fdrFund;
    }

    /**
     * Set f048fdrActivity
     *
     * @param string $f048fdrActivity
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fdrActivity($f048fdrActivity)
    {
        $this->f048fdrActivity = $f048fdrActivity;

        return $this;
    }

    /**
     * Get f048fdrActivity
     *
     * @return string
     */
    public function getF048fdrActivity()
    {
        return $this->f048fdrActivity;
    }

    /**
     * Set f048fdrDepartment
     *
     * @param string $f048fdrDepartment
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fdrDepartment($f048fdrDepartment)
    {
        $this->f048fdrDepartment = $f048fdrDepartment;

        return $this;
    }

    /**
     * Get f048fdrDepartment
     *
     * @return string
     */
    public function getF048fdrDepartmentId()
    {
        return $this->f048fdrDepartmentId;
    }

    /**
     * Set f048fdrAccount
     *
     * @param string $f048fdrAccount
     *
     * @return T048ffeeStructureDetails
     */
    public function setF048fdrAccount($f048fdrAccount)
    {
        $this->f048fdrAccount = $f048fdrAccount;

        return $this;
    }

    /**
     * Get f048fdrAccount
     *
     * @return string
     */
    public function getF048fdrAccount()
    {
        return $this->f048fdrAccount;
    }
}
