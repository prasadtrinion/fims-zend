<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063finvestmentApplication
 *
 * @ORM\Table(name="t063finvestment_application")
 * @ORM\Entity(repositoryClass="Application\Repository\InvestmentApplicationRepository")
 */
class T063finvestmentApplication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fdescription", type="string", length=255, nullable=false)
     */
    private $f063fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063finvestment_amount", type="integer", nullable=false)
     */
    private $f063finvestmentAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fduration", type="string", length=100, nullable=false)
     */
    private $f063fduration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063finstitution_id", type="integer", nullable=false)
     */
    private $f063finstitutionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063finterest_rate", type="integer", nullable=true)
     */
    private $f063finterestRate;

    /**
     * @var string
     *
     * @ORM\Column(name="f063ffile_upload", type="string", length=100, nullable=true)
     */
    private $f063ffileUpload;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=false)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=false)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=false)
     */
    private $f063fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved1_status", type="integer", nullable=true)
     */
    private $f063fapproved1Status;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved2_status", type="integer", nullable=true)
     */
    private $f063fapproved2Status;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fapprove1_reason", type="string", length=300, nullable=true)
     */
    private $f063fapprove1Reason;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fapprove2_reason", type="string", length=300, nullable=true)
     */
    private $f063fapprove2Reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved1_by", type="integer", nullable=true)
     */
    private $f063fapproved1By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved2_by", type="integer", nullable=true)
     */
    private $f063fapproved2By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved1_updated_by", type="integer", nullable=true)
     */
    private $f063fapproved1UpdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved2_updated_by", type="integer", nullable=true)
     */
    private $f063fapproved2UpdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fis_online", type="integer", nullable=true)
     */
    private $f063fisOnline;



    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063fdescription
     *
     * @param string $f063fdescription
     *
     * @return T063finvestmentApplication
     */
    public function setF063fdescription($f063fdescription)
    {
        $this->f063fdescription = $f063fdescription;

        return $this;
    }

    /**
     * Get f063fdescription
     *
     * @return string
     */
    public function getF063fdescription()
    {
        return $this->f063fdescription;
    }

    /**
     * Set f063finvestmentAmount
     *
     * @param float $f063finvestmentAmount
     *
     * @return T063finvestmentApplication
     */
    public function setF063finvestmentAmount($f063finvestmentAmount)
    {
        $this->f063finvestmentAmount = $f063finvestmentAmount;

        return $this;
    }

    /**
     * Get f063finvestmentAmount
     *
     * @return float
     */
    public function getF063finvestmentAmount()
    {
        return $this->f063finvestmentAmount;
    }

    /**
     * Set f063fduration
     *
     * @param string $f063fduration
     *
     * @return T063finvestmentApplication
     */
    public function setF063fduration($f063fduration)
    {
        $this->f063fduration = $f063fduration;

        return $this;
    }

    /**
     * Get f063fduration
     *
     * @return string
     */
    public function getF063fduration()
    {
        return $this->f063fduration;
    }

    /**
     * Set f063finstitutionId
     *
     * @param integer $f063finstitutionId
     *
     * @return T063finvestmentApplication
     */
    public function setF063finstitutionId($f063finstitutionId)
    {
        $this->f063finstitutionId = $f063finstitutionId;

        return $this;
    }

    /**
     * Get f063finstitutionId
     *
     * @return integer
     */
    public function getF063finstitutionId()
    {
        return $this->f063finstitutionId;
    }

    /**
     * Set f063finterestRate
     *
     * @param string $f063finterestRate
     *
     * @return T063finvestmentApplication
     */
    public function setF063finterestRate($f063finterestRate)
    {
        $this->f063finterestRate = $f063finterestRate;

        return $this;
    }

    /**
     * Get f063finterestRate
     *
     * @return string
     */
    public function getF063finterestRate()
    {
        return $this->f063finterestRate;
    }

    /**
     * Set f063ffileUpload
     *
     * @param string $f063ffileUpload
     *
     * @return T063finvestmentApplication
     */
    public function setF063ffileUpload($f063ffileUpload)
    {
        $this->f063ffileUpload = $f063ffileUpload;

        return $this;
    }

    /**
     * Get f063ffileUpload
     *
     * @return string
     */
    public function getF063ffileUpload()
    {
        return $this->f063ffileUpload;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063finvestmentApplication
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063finvestmentApplication
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fisOnline
     *
     * @param integer $f063fisOnline
     *
     * @return T063finvestmentApplication
     */
    public function setF063fisOnline($f063fisOnline)
    {
        $this->f063fisOnline = $f063fisOnline;

        return $this;
    }

    /**
     * Get f063fisOnline
     *
     * @return integer
     */
    public function getF063fisOnline()
    {
        return $this->f063fisOnline;
    }
    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063finvestmentApplication
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063finvestmentApplication
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063finvestmentApplication
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063fapproved1Status
     *
     * @param integer $f063fapproved1Status
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved1Status($f063fapproved1Status)
    {
        $this->f063fapproved1Status = $f063fapproved1Status;

        return $this;
    }

    /**
     * Get f063fapproved1Status
     *
     * @return integer
     */
    public function getF063fapproved1Status()
    {
        return $this->f063fapproved1Status;
    }

    /**
     * Set f063fapproved2Status
     *
     * @param integer $f063fapproved2Status
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved2Status($f063fapproved2Status)
    {
        $this->f063fapproved2Status = $f063fapproved2Status;

        return $this;
    }

    /**
     * Get f063fapproved2Status
     *
     * @return integer
     */
    public function getF063fapproved2Status()
    {
        return $this->f063fapproved2Status;
    }

    /**
     * Set f063fapprove1Reson
     *
     * @param string $f063fapprove1Reson
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapprove1Reson($f063fapprove1Reson)
    {
        $this->f063fapprove1Reson = $f063fapprove1Reson;

        return $this;
    }

    /**
     * Get f063fapprove1Reson
     *
     * @return string
     */
    public function getF063fapprove1Reson()
    {
        return $this->f063fapprove1Reson;
    }

    /**
     * Set f063fapprove2Reason
     *
     * @param string $f063fapprove2Reason
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapprove2Reason($f063fapprove2Reason)
    {
        $this->f063fapprove2Reason = $f063fapprove2Reason;

        return $this;
    }

    /**
     * Get f063fapprove2Reason
     *
     * @return string
     */
    public function getF063fapprove2Reason()
    {
        return $this->f063fapprove2Reason;
    }

    /**
     * Set f063fapproved1By
     *
     * @param integer $f063fapproved1By
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved1By($f063fapproved1By)
    {
        $this->f063fapproved1By = $f063fapproved1By;

        return $this;
    }

    /**
     * Get f063fapproved1By
     *
     * @return integer
     */
    public function getF063fapproved1By()
    {
        return $this->f063fapproved1By;
    }

    /**
     * Set f063fapproved2By
     *
     * @param integer $f063fapproved2By
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved2By($f063fapproved2By)
    {
        $this->f063fapproved2By = $f063fapproved2By;

        return $this;
    }

    /**
     * Get f063fapproved2By
     *
     * @return integer
     */
    public function getF063fapproved2By()
    {
        return $this->f063fapproved2By;
    }

    /**
     * Set f063fapproved1UpdatedBy
     *
     * @param integer $f063fapproved1UpdatedBy
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved1UpdatedBy($f063fapproved1UpdatedBy)
    {
        $this->f063fapproved1UpdatedBy = $f063fapproved1UpdatedBy;

        return $this;
    }

    /**
     * Get f063fapproved1UpdatedBy
     *
     * @return integer
     */
    public function getF063fapproved1UpdatedBy()
    {
        return $this->f063fapproved1UpdatedBy;
    }

    /**
     * Set f063fapproved2UpdatedBy
     *
     * @param integer $f063fapproved2UpdatedBy
     *
     * @return T063finvestmentApplication
     */
    public function setF063fapproved2UpdatedBy($f063fapproved2UpdatedBy)
    {
        $this->f063fapproved2UpdatedBy = $f063fapproved2UpdatedBy;

        return $this;
    }

    /**
     * Get f063fapproved2UpdatedBy
     *
     * @return integer
     */
    public function getF063fapproved2UpdatedBy()
    {
        return $this->f063fapproved2UpdatedBy;
    }
}
