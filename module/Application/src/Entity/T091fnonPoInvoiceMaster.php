<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T091fnonPoInvoiceMaster
 *
 * @ORM\Table(name="t091fnon_po_invoice_master")
 * @ORM\Entity(repositoryClass="Application\Repository\NonPoInvoiceRepository")
 */
class T091fnonPoInvoiceMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_invoice", type="integer", nullable=false)
     */
    private $f091fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_vendor", type="integer", nullable=false)
     */
    private $f091fidVendor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091finvoice_date", type="datetime", nullable=false)
     */
    private $f091finvoiceDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091finvoice_type", type="integer", nullable=false)
     */
    private $f091finvoiceType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091freceived_date", type="datetime", nullable=false)
     */
    private $f091freceivedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091famount", type="integer", nullable=true)
     */
    private $f091famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=false)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=true)
     */
    private $f091fcreatedBy = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=true)
     */
    private $f091fupdatedBy = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fupdatedDtTm;



    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    /**
     * Set f091fidInvoice
     *
     * @param integer $f091fidInvoice
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091fidInvoice($f091fidInvoice)
    {
        $this->f091fidInvoice = $f091fidInvoice;

        return $this;
    }

    /**
     * Get f091fidInvoice
     *
     * @return integer
     */
    public function getF091fidInvoice()
    {
        return $this->f091fidInvoice;
    }

    /**
     * Set f091fidVendor
     *
     * @param integer $f091fidVendor
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091fidVendor($f091fidVendor)
    {
        $this->f091fidVendor = $f091fidVendor;

        return $this;
    }

    /**
     * Get f091fidVendor
     *
     * @return integer
     */
    public function getF091fidVendor()
    {
        return $this->f091fidVendor;
    }

    /**
     * Set f091finvoiceType
     *
     * @param integer $f091finvoiceType
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091finvoiceType($f091finvoiceType)
    {
        $this->f091finvoiceType = $f091finvoiceType;

        return $this;
    }

    /**
     * Get f091finvoiceType
     *
     * @return integer
     */
    public function getF091finvoiceType()
    {
        return $this->f091finvoiceType;
    }

    /**
     * Set f091finvoiceDate
     *
     * @param \DateTime $f091finvoiceDate
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091finvoiceDate($f091finvoiceDate)
    {
        $this->f091finvoiceDate = $f091finvoiceDate;

        return $this;
    }

    /**
     * Get f091finvoiceDate
     *
     * @return \DateTime
     */
    public function getF091finvoiceDate()
    {
        return $this->f091finvoiceDate;
    }

    /**
     * Set f091freceivedDate
     *
     * @param \DateTime $f091freceivedDate
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091freceivedDate($f091freceivedDate)
    {
        $this->f091freceivedDate = $f091freceivedDate;

        return $this;
    }

    /**
     * Get f091freceivedDate
     *
     * @return \DateTime
     */
    public function getF091freceivedDate()
    {
        return $this->f091freceivedDate;
    }

    /**
     * Set f091famount
     *
     * @param integer $f091famount
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091famount($f091famount)
    {
        $this->f091famount = $f091famount;

        return $this;
    }

    /**
     * Get f091famount
     *
     * @return integer
     */
    public function getF091famount()
    {
        return $this->f091famount;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fnonPoInvoiceMaster
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091fnonPoInvoiceDetails
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }
}
