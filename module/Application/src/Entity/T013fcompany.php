<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T013fcompany
 *
 * @ORM\Table(name="t013fcompany", uniqueConstraints={@ORM\UniqueConstraint(name="f013fname_UNIQUE", columns={"f013fname"})})
 *  @ORM\Entity(repositoryClass="Application\Repository\CompanyRepository")
 */
class T013fcompany
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f013fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f013fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fname", type="string", length=45, nullable=false)
     */
    private $f013fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f013faddress", type="string", length=100, nullable=false)
     */
    private $f013faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f013femail", type="string", length=45, nullable=true)
     */
    private $f013femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fphone_number", type="string", length=45, nullable=false)
     */
    private $f013fphoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fcity", type="string", length=100, nullable=false)
     */
    private $f013fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fstate_code", type="integer", nullable=false)
     */
    private $f013fstateCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fcountry_code", type="integer", nullable=false)
     */
    private $f013fcountryCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fcreated_by", type="integer", nullable=false)
     */
    private $f013fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fupdated_by", type="integer", nullable=false)
     */
    private $f013fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f013fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f013fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f013fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f013fupdateDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fstatus", type="integer", nullable=false)
     */
    private $f013fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013ftax", type="integer", nullable=true)
     */
    private $f013ftax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013ftax_type", type="integer", nullable=true)
     */
    private $f013ftaxType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013ftax_excempted", type="integer", nullable=true)
     */
    private $f013ftaxExcempted;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fregistration_number", type="string", length=100, nullable=true)
     */
    private $f013fregistrationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fcode", type="string", length=50, nullable=true)
     */
    private $f013fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fref_number", type="string", length=50, nullable=true)
     */
    private $f013frefNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f013ffax_number", type="string", length=50, nullable=false)
     */
    private $f013ffaxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fpost_code", type="string", length=50, nullable=false)
     */
    private $f013fpostCode;



    /**
     * Get f013fid
     *
     * @return integer
     */
    public function getF013fid()
    {
        return $this->f013fid;
    }

    /**
     * Set f013fname
     *
     * @param string $f013fname
     *
     * @return T013fcompany
     */
    public function setF013fname($f013fname)
    {
        $this->f013fname = $f013fname;

        return $this;
    }

    /**
     * Get f013fname
     *
     * @return string
     */
    public function getF013fname()
    {
        return $this->f013fname;
    }

    /**
     * Set f013faddress
     *
     * @param string $f013faddress
     *
     * @return T013fcompany
     */
    public function setF013faddress($f013faddress)
    {
        $this->f013faddress = $f013faddress;

        return $this;
    }

    /**
     * Get f013faddress
     *
     * @return string
     */
    public function getF013faddress()
    {
        return $this->f013faddress;
    }

    /**
     * Set f013femail
     *
     * @param string $f013femail
     *
     * @return T013fcompany
     */
    public function setF013femail($f013femail)
    {
        $this->f013femail = $f013femail;

        return $this;
    }

    /**
     * Get f013femail
     *
     * @return string
     */
    public function getF013femail()
    {
        return $this->f013femail;
    }

    /**
     * Set f013fphoneNumber
     *
     * @param string $f013fphoneNumber
     *
     * @return T013fcompany
     */
    public function setF013fphoneNumber($f013fphoneNumber)
    {
        $this->f013fphoneNumber = $f013fphoneNumber;

        return $this;
    }

    /**
     * Get f013fphoneNumber
     *
     * @return string
     */
    public function getF013fphoneNumber()
    {
        return $this->f013fphoneNumber;
    }

    /**
     * Set f013fcity
     *
     * @param string $f013fcity
     *
     * @return T013fcompany
     */
    public function setF013fcity($f013fcity)
    {
        $this->f013fcity = $f013fcity;

        return $this;
    }

    /**
     * Get f013fcity
     *
     * @return string
     */
    public function getF013fcity()
    {
        return $this->f013fcity;
    }

    /**
     * Set f013fstateCode
     *
     * @param integer $f013fstateCode
     *
     * @return T013fcompany
     */
    public function setF013fstateCode($f013fstateCode)
    {
        $this->f013fstateCode = $f013fstateCode;

        return $this;
    }

    /**
     * Get f013fstateCode
     *
     * @return integer
     */
    public function getF013fstateCode()
    {
        return $this->f013fstateCode;
    }

    /**
     * Set f013fcountryCode
     *
     * @param integer $f013fcountryCode
     *
     * @return T013fcompany
     */
    public function setF013fcountryCode($f013fcountryCode)
    {
        $this->f013fcountryCode = $f013fcountryCode;

        return $this;
    }

    /**
     * Get f013fcountryCode
     *
     * @return integer
     */
    public function getF013fcountryCode()
    {
        return $this->f013fcountryCode;
    }

    /**
     * Set f013fcreatedBy
     *
     * @param integer $f013fcreatedBy
     *
     * @return T013fcompany
     */
    public function setF013fcreatedBy($f013fcreatedBy)
    {
        $this->f013fcreatedBy = $f013fcreatedBy;

        return $this;
    }

    /**
     * Get f013fcreatedBy
     *
     * @return integer
     */
    public function getF013fcreatedBy()
    {
        return $this->f013fcreatedBy;
    }

    /**
     * Set f013fupdatedBy
     *
     * @param integer $f013fupdatedBy
     *
     * @return T013fcompany
     */
    public function setF013fupdatedBy($f013fupdatedBy)
    {
        $this->f013fupdatedBy = $f013fupdatedBy;

        return $this;
    }

    /**
     * Get f013fupdatedBy
     *
     * @return integer
     */
    public function getF013fupdatedBy()
    {
        return $this->f013fupdatedBy;
    }

    /**
     * Set f013fcreateDtTm
     *
     * @param \DateTime $f013fcreateDtTm
     *
     * @return T013fcompany
     */
    public function setF013fcreateDtTm($f013fcreateDtTm)
    {
        $this->f013fcreateDtTm = $f013fcreateDtTm;

        return $this;
    }

    /**
     * Get f013fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF013fcreateDtTm()
    {
        return $this->f013fcreateDtTm;
    }

    /**
     * Set f013fupdateDtTm
     *
     * @param \DateTime $f013fupdateDtTm
     *
     * @return T013fcompany
     */
    public function setF013fupdateDtTm($f013fupdateDtTm)
    {
        $this->f013fupdateDtTm = $f013fupdateDtTm;

        return $this;
    }

    /**
     * Get f013fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF013fupdateDtTm()
    {
        return $this->f013fupdateDtTm;
    }

    /**
     * Set f013fstatus
     *
     * @param integer $f013fstatus
     *
     * @return T013fcompany
     */
    public function setF013fstatus($f013fstatus)
    {
        $this->f013fstatus = $f013fstatus;

        return $this;
    }

    /**
     * Get f013fstatus
     *
     * @return integer
     */
    public function getF013fstatus()
    {
        return $this->f013fstatus;
    }

    /**
     * Set f013ftax
     *
     * @param integer $f013ftax
     *
     * @return T013fcompany
     */
    public function setF013ftax($f013ftax)
    {
        $this->f013ftax = $f013ftax;

        return $this;
    }

    /**
     * Get f013ftax
     *
     * @return integer
     */
    public function getF013ftax()
    {
        return $this->f013ftax;
    }

    /**
     * Set f013ftaxType
     *
     * @param integer $f013ftaxType
     *
     * @return T013fcompany
     */
    public function setF013ftaxType($f013ftaxType)
    {
        $this->f013ftaxType = $f013ftaxType;

        return $this;
    }

    /**
     * Get f013ftaxType
     *
     * @return integer
     */
    public function getF013ftaxType()
    {
        return $this->f013ftaxType;
    }

    /**
     * Set f013ftaxExcempted
     *
     * @param integer $f013ftaxExcempted
     *
     * @return T013fcompany
     */
    public function setF013ftaxExcempted($f013ftaxExcempted)
    {
        $this->f013ftaxExcempted = $f013ftaxExcempted;

        return $this;
    }

    /**
     * Get f013ftaxExcempted
     *
     * @return integer
     */
    public function getF013ftaxExcempted()
    {
        return $this->f013ftaxExcempted;
    }

    /**
     * Set f013fregistrationNumber
     *
     * @param string $f013fregistrationNumber
     *
     * @return T013fcompany
     */
    public function setF013fregistrationNumber($f013fregistrationNumber)
    {
        $this->f013fregistrationNumber = $f013fregistrationNumber;

        return $this;
    }

    /**
     * Get f013fregistrationNumber
     *
     * @return string
     */
    public function getF013fregistrationNumber()
    {
        return $this->f013fregistrationNumber;
    }

    /**
     * Set f013fcode
     *
     * @param string $f013fcode
     *
     * @return T013fcompany
     */
    public function setF013fcode($f013fcode)
    {
        $this->f013fcode = $f013fcode;

        return $this;
    }

    /**
     * Get f013fcode
     *
     * @return string
     */
    public function getF013fcode()
    {
        return $this->f013fcode;
    }

    /**
     * Set f013frefNumber
     *
     * @param string $f013frefNumber
     *
     * @return T013fcompany
     */
    public function setF013frefNumber($f013frefNumber)
    {
        $this->f013frefNumber = $f013frefNumber;

        return $this;
    }

    /**
     * Get f013frefNumber
     *
     * @return string
     */
    public function getF013frefNumber()
    {
        return $this->f013frefNumber;
    }

    /**
     * Set f013ffaxNumber
     *
     * @param string $f013ffaxNumber
     *
     * @return T013fcompany
     */
    public function setF013ffaxNumber($f013ffaxNumber)
    {
        $this->f013ffaxNumber = $f013ffaxNumber;

        return $this;
    }

    /**
     * Get f013ffaxNumber
     *
     * @return string
     */
    public function getF013ffaxNumber()
    {
        return $this->f013ffaxNumber;
    }

    /**
     * Set f013fpostCode
     *
     * @param string $f013fpostCode
     *
     * @return T013fcompany
     */
    public function setF013fpostCode($f013fpostCode)
    {
        $this->f013fpostCode = $f013fpostCode;

        return $this;
    }

    /**
     * Get f013fpostCode
     *
     * @return string
     */
    public function getF013fpostCode()
    {
        return $this->f013fpostCode;
    }
}
