<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T028fitemSubCategory
 *
 * @ORM\Table(name="t028fitem_sub_category",uniqueConstraints={@ORM\UniqueConstraint(name="f028fsub_category_name_UNIQUE", columns={"f028fsub_category_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ItemSubCategoryRepository")
 */
class T028fitemSubCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f028fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f028fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f028fid_category", type="integer", nullable=false)
     */
    private $f028fidCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f028fsub_category_name", type="string", length=50, nullable=false)
     */
    private $f028fsubCategoryName;

     /**
     * @var string
     *
     * @ORM\Column(name="f028fcode", type="string", length=50, nullable=false)
     */
    private $f028fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f028fstatus", type="integer", nullable=false)
     */
    private $f028fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f028fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f028fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f028fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f028fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f028fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f028fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f028fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f028fcreatedBy;


    /**
     * Get f028fid
     *
     * @return integer
     */
    public function getF028fid()
    {
        return $this->f028fid;
    }

    /**
     * Set f028fidCategory
     *
     * @param integer $f028fidCategory
     *
     * @return T028fitemSubCategory
     */
    public function setF028fidCategory($f028fidCategory)
    {
        $this->f028fidCategory = $f028fidCategory;

        return $this;
    }

    
    public function getF028fidCategory()
    {
        return $this->f028fidCategory;
    }

     /**
     * Set f028fcode
     *
     * @param string $f028fcode
     *
     * @return T028fitemCategory
     */
    public function setF028fcode($f028fcode)
    {
        $this->f028fcode = $f028fcode;

        return $this;
    }

    /**
     * Get f028fcode
     *
     * @return string
     */
    public function getF028fcode()
    {
        return $this->f028fcode;
    }
    /**
     * Set f028fsubCategoryName
     *
     * @param string $f028fsubCategoryName
     *
     * @return T028fitemSubCategory
     */
    public function setF028fsubCategoryName($f028fsubCategoryName)
    {
        $this->f028fsubCategoryName = $f028fsubCategoryName;

        return $this;
    }

    /**
     * Get f028fsubCategoryName
     *
     * @return string
     */
    public function getF028fsubCategoryName()
    {
        return $this->f028fsubCategoryName;
    }

    /**
     * Set f028fstatus
     *
     * @param integer $f028fstatus
     *
     * @return T028fitemSubCategory
     */
    public function setF028fstatus($f028fstatus)
    {
        $this->f028fstatus = $f028fstatus;

        return $this;
    }

    /**
     * Get f028fstatus
     *
     * @return integer
     */
    public function getF028fstatus()
    {
        return $this->f028fstatus;
    }

    /**
     * Set f028fcreatedDtTm
     *
     * @param \DateTime $f028fcreatedDtTm
     *
     * @return T028fitemSubCategory
     */
    public function setF028fcreatedDtTm($f028fcreatedDtTm)
    {
        $this->f028fcreatedDtTm = $f028fcreatedDtTm;

        return $this;
    }

    /**
     * Get f028fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF028fcreatedDtTm()
    {
        return $this->f028fcreatedDtTm;
    }

    /**
     * Set f028fupdatedDtTm
     *
     * @param \DateTime $f028fupdatedDtTm
     *
     * @return T028fitemSubCategory
     */
    public function setF028fupdatedDtTm($f028fupdatedDtTm)
    {
        $this->f028fupdatedDtTm = $f028fupdatedDtTm;

        return $this;
    }

    /**
     * Get f028fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF028fupdatedDtTm()
    {
        return $this->f028fupdatedDtTm;
    }

    /**
     * Set f028fupdatedBy
     *
     * @param integer $f028fupdatedBy
     *
     * @return T028fitemSubCategory
     */
    public function setF028fupdatedBy($f028fupdatedBy)
    {
        $this->f028fupdatedBy = $f028fupdatedBy;

        return $this;
    }

    
    public function getF028fupdatedBy()
    {
        return $this->f028fupdatedBy;
    }

    /**
     * Set f028fcreatedBy
     *
     * @param  integer $f028fcreatedBy
     *
     * @return T028fitemSubCategory
     */
    public function setF028fcreatedBy($f028fcreatedBy)
    {
        $this->f028fcreatedBy = $f028fcreatedBy;

        return $this;
    }

    
    public function getF028fcreatedBy()
    {
        return $this->f028fcreatedBy;
    }
}
