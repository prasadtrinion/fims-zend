<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T099fapplyStockDetails
 *
 * @ORM\Table(name="t099fapply_stock_details")
 * @ORM\Entity(repositoryClass="Application\Repository\ApplyStockRepository")
 */
class T099fapplyStockDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f099fidDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fkod_stock", type="string", length=50, nullable=false)
     */
    private $f099fkodStock;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_apply_stock", type="integer", nullable=false)
     */
    private $f099fidApplyStock;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_category", type="integer", nullable=false)
     */
    private $f099fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_sub_category", type="integer", nullable=false)
     */
    private $f099fidSubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_asset_type", type="integer", nullable=false)
     */
    private $f099fidAssetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid_item", type="integer", nullable=false)
     */
    private $f099fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fcurrent_stock", type="string", length=40, nullable=false)
     */
    private $f099fcurrentStock;

    /**
     * @var string
     *
     * @ORM\Column(name="f099frequested_qty", type="string", length=40, nullable=false)
     */
    private $f099frequestedQty;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fstatus", type="integer", nullable=false)
     */
    private $f099fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fcreated_by", type="integer", nullable=false)
     */
    private $f099fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fupdated_by", type="integer", nullable=false)
     */
    private $f099fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f099fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f099fupdatedDtTm;



    /**
     * Get f099fid
     *
     * @return integer
     */
    public function getF099fid()
    {
        return $this->f099fid;
    }

    /**
     * Set f099fkodStock
     *
     * @param string $f099fkodStock
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fkodStock($f099fkodStock)
    {
        $this->f099fkodStock = $f099fkodStock;

        return $this;
    }

    /**
     * Get f099fkodStock
     *
     * @return string
     */
    public function getF099fkodStock()
    {
        return $this->f099fkodStock;
    }

    /**
     * Set f099fidCategory
     *
     * @param integer $f099fidCategory
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fidCategory($f099fidCategory)
    {
        $this->f099fidCategory = $f099fidCategory;

        return $this;
    }

    /**
     * Get f099fidCategory
     *
     * @return integer
     */
    public function getF099fidCategory()
    {
        return $this->f099fidCategory;
    }


    /**
     * Set f099fidApplyStock
     *
     * @param integer $f099fidApplyStock
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fidApplyStock($f099fidApplyStock)
    {
        $this->f099fidApplyStock = $f099fidApplyStock;

        return $this;
    }

    /**
     * Get f099fidApplyStock
     *
     * @return integer
     */
    public function getF099fidApplyStock()
    {
        return $this->f099fidApplyStock;
    }

    /**
     * Set f099fidSubCategory
     *
     * @param integer $f099fidSubCategory
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fidSubCategory($f099fidSubCategory)
    {
        $this->f099fidSubCategory = $f099fidSubCategory;

        return $this;
    }

    /**
     * Get f099fidSubCategory
     *
     * @return integer
     */
    public function getF099fidSubCategory()
    {
        return $this->f099fidSubCategory;
    }

    /**
     * Set f099fidAssetType
     *
     * @param integer $f099fidAssetType
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fidAssetType($f099fidAssetType)
    {
        $this->f099fidAssetType = $f099fidAssetType;

        return $this;
    }

    /**
     * Get f099fidAssetType
     *
     * @return integer
     */
    public function getF099fidAssetType()
    {
        return $this->f099fidAssetType;
    }

    /**
     * Set f099fidItem
     *
     * @param integer $f099fidItem
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fidItem($f099fidItem)
    {
        $this->f099fidItem = $f099fidItem;

        return $this;
    }

    /**
     * Get f099fidItem
     *
     * @return integer
     */
    public function getF099fidItem()
    {
        return $this->f099fidItem;
    }

    /**
     * Set f099fcurrentStock
     *
     * @param string $f099fcurrentStock
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fcurrentStock($f099fcurrentStock)
    {
        $this->f099fcurrentStock = $f099fcurrentStock;

        return $this;
    }

    /**
     * Get f099fcurrentStock
     *
     * @return string
     */
    public function getF099fcurrentStock()
    {
        return $this->f099fcurrentStock;
    }

    /**
     * Set f099frequestedQty
     *
     * @param integer $f099frequestedQty
     *
     * @return T099fapplyStockDetails
     */
    public function setF099frequestedQty($f099frequestedQty)
    {
        $this->f099frequestedQty = $f099frequestedQty;

        return $this;
    }

    /**
     * Get f099frequestedQty
     *
     * @return integer
     */
    public function getF099frequestedQty()
    {
        return $this->f099frequestedQty;
    }

    /**
     * Set f099fstatus
     *
     * @param integer $f099fstatus
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fstatus($f099fstatus)
    {
        $this->f099fstatus = $f099fstatus;

        return $this;
    }

    /**
     * Get f099fstatus
     *
     * @return integer
     */
    public function getF099fstatus()
    {
        return $this->f099fstatus;
    }

    /**
     * Set f099fcreatedBy
     *
     * @param integer $f099fcreatedBy
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fcreatedBy($f099fcreatedBy)
    {
        $this->f099fcreatedBy = $f099fcreatedBy;

        return $this;
    }

    /**
     * Get f099fcreatedBy
     *
     * @return integer
     */
    public function getF099fcreatedBy()
    {
        return $this->f099fcreatedBy;
    }

    /**
     * Set f099fupdatedBy
     *
     * @param integer $f099fupdatedBy
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fupdatedBy($f099fupdatedBy)
    {
        $this->f099fupdatedBy = $f099fupdatedBy;

        return $this;
    }

    /**
     * Get f099fupdatedBy
     *
     * @return integer
     */
    public function getF099fupdatedBy()
    {
        return $this->f099fupdatedBy;
    }

    /**
     * Set f099fcreatedDtTm
     *
     * @param \DateTime $f099fcreatedDtTm
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fcreatedDtTm($f099fcreatedDtTm)
    {
        $this->f099fcreatedDtTm = $f099fcreatedDtTm;

        return $this;
    }

    /**
     * Get f099fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fcreatedDtTm()
    {
        return $this->f099fcreatedDtTm;
    }

    /**
     * Set f099fupdatedDtTm
     *
     * @param \DateTime $f099fupdatedDtTm
     *
     * @return T099fapplyStockDetails
     */
    public function setF099fupdatedDtTm($f099fupdatedDtTm)
    {
        $this->f099fupdatedDtTm = $f099fupdatedDtTm;

        return $this;
    }

    /**
     * Get f099fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fupdatedDtTm()
    {
        return $this->f099fupdatedDtTm;
    }
}
