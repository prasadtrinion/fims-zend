<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T014fglcode
 *
 * @ORM\Table(name="t014fglcode", uniqueConstraints={@ORM\UniqueConstraint(name="f014fgl_code_UNIQUE", columns={"f014fgl_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\GlcodeRepository")
 */
class T014fglcode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f014fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f014fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f014fgl_code", type="string", length=45, nullable=false)
     */
    private $f014fglCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014ffund_id", type="integer", nullable=false)
     */
    private $f014ffundId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014factivity_id", type="integer", nullable=true)
     */
    private $f014factivityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fdepartment_id", type="integer", nullable=false)
     */
    private $f014fdepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014faccount_id", type="integer", nullable=false)
     */
    private $f014faccountId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fstatus", type="integer", nullable=false)
     */
    private $f014fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f014fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f014fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f014fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f014fupdateDtTm;



    /**
     * Get f014fid
     *
     * @return integer
     */
    public function getF014fid()
    {
        return $this->f014fid;
    }

    /**
     * Set f014fglCode
     *
     * @param string $f014fglCode
     *
     * @return T014fglcode
     */
    public function setF014fglCode($f014fglCode)
    {
        $this->f014fglCode = $f014fglCode;

        return $this;
    }

    /**
     * Get f014fglCode
     *
     * @return string
     */
    public function getF014fglCode()
    {
        return $this->f014fglCode;
    }

    /**
     * Set f014ffundId
     *
     * @param integer $f014ffundId
     *
     * @return T014fglcode
     */
    public function setF014ffundId($f014ffundId)
    {
        $this->f014ffundId = $f014ffundId;

        return $this;
    }

    /**
     * Get f014ffundId
     *
     * @return integer
     */
    public function getF014ffundId()
    {
        return $this->f014ffundId;
    }

    /**
     * Set f014factivityId
     *
     * @param integer $f014factivityId
     *
     * @return T014fglcode
     */
    public function setF014factivityId($f014factivityId)
    {
        $this->f014factivityId = $f014factivityId;

        return $this;
    }

    /**
     * Get f014factivityId
     *
     * @return integer
     */
    public function getF014factivityId()
    {
        return $this->f014factivityId;
    }

    /**
     * Set f014fdepartmentId
     *
     * @param integer $f014fdepartmentId
     *
     * @return T014fglcode
     */
    public function setF014fdepartmentId($f014fdepartmentId)
    {
        $this->f014fdepartmentId = $f014fdepartmentId;

        return $this;
    }

    /**
     * Get f014fdepartmentId
     *
     * @return integer
     */
    public function getF014fdepartmentId()
    {
        return $this->f014fdepartmentId;
    }

    /**
     * Set f014faccountId
     *
     * @param integer $f014faccountId
     *
     * @return T014fglcode
     */
    public function setF014faccountId($f014faccountId)
    {
        $this->f014faccountId = $f014faccountId;

        return $this;
    }

    /**
     * Get f014faccountId
     *
     * @return integer
     */
    public function getF014faccountId()
    {
        return $this->f014faccountId;
    }

    /**
     * Set f014fstatus
     *
     * @param integer $f014fstatus
     *
     * @return T014fglcode
     */
    public function setF014fstatus($f014fstatus)
    {
        $this->f014fstatus = $f014fstatus;

        return $this;
    }

    /**
     * Get f014fstatus
     *
     * @return integer
     */
    public function getF014fstatus()
    {
        return $this->f014fstatus;
    }

    /**
     * Set f014fcreatedBy
     *
     * @param integer $f014fcreatedBy
     *
     * @return T014fglcode
     */
    public function setF014fcreatedBy($f014fcreatedBy)
    {
        $this->f014fcreatedBy = $f014fcreatedBy;

        return $this;
    }

    /**
     * Get f014fcreatedBy
     *
     * @return integer
     */
    public function getF014fcreatedBy()
    {
        return $this->f014fcreatedBy;
    }

    /**
     * Set f014fupdatedBy
     *
     * @param integer $f014fupdatedBy
     *
     * @return T014fglcode
     */
    public function setF014fupdatedBy($f014fupdatedBy)
    {
        $this->f014fupdatedBy = $f014fupdatedBy;

        return $this;
    }

    /**
     * Get f014fupdatedBy
     *
     * @return integer
     */
    public function getF014fupdatedBy()
    {
        return $this->f014fupdatedBy;
    }

    /**
     * Set f014fcreateDtTm
     *
     * @param \DateTime $f014fcreateDtTm
     *
     * @return T014fglcode
     */
    public function setF014fcreateDtTm($f014fcreateDtTm)
    {
        $this->f014fcreateDtTm = $f014fcreateDtTm;

        return $this;
    }

    /**
     * Get f014fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF014fcreateDtTm()
    {
        return $this->f014fcreateDtTm;
    }

    /**
     * Set f014fupdateDtTm
     *
     * @param \DateTime $f014fupdateDtTm
     *
     * @return T014fglcode
     */
    public function setF014fupdateDtTm($f014fupdateDtTm)
    {
        $this->f014fupdateDtTm = $f014fupdateDtTm;

        return $this;
    }

    /**
     * Get f014fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF014fupdateDtTm()
    {
        return $this->f014fupdateDtTm;
    }
}

























