<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T134femployeeDelay
 *
 * @ORM\Table(name="t134femployee_delay")
 * @ORM\Entity(repositoryClass="Application\Repository\EmpolyeeDelayRepository")
 */

class T134femployeeDelay
{
	/**
     * @var integer
     *
     * @ORM\Column(name="f134fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f134fid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f134fdate", type="datetime", nullable=false)
     */
    private $f134fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fproject", type="integer", nullable=false)
     */
    private $f134fproject;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fmodule", type="integer", nullable=false)
     */
    private $f134fmodule;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fscreen", type="integer", nullable=false)
     */
    private $f134fscreen;

    /**
     * @var string
     *
     * @ORM\Column(name="f134ftitle", type="string", length=255, nullable=false)
     */
    private $f134ftitle;

    /**
     * @var string
     *
     * @ORM\Column(name="f134fdescription", type="string", length=255, nullable=false)
     */
    private $f134fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fhours", type="integer", nullable=false)
     */
    private $f134fhours;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fstatus", type="integer", nullable=false)
     */
    private $f134fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fcreated_by", type="integer", nullable=false)
     */
    private $f134fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fupdated_by", type="integer", nullable=false)
     */
    private $f134fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f134fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f134fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f134fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f134fupdateDtTm;

    /**
     * Get f134fid
     *
     * @return integer
     */
    public function getF134fid()
    {
        return $this->f134fid;
    }

    /**
     * Set f134fdate
     *
     * @param \DateTime $f134fdate
     *
     * @return T134femployeeDelay
     */
    public function setF134fdate($f134fdate)
    {
        $this->f134fdate = $f134fdate;

        return $this;
    }
    /**
     * Get f134fdate
     *
     * @return \DateTime
     */
    public function getf134fdate()
    {
        return $this->f134fdate;
    }
    /**
     * Set f134fproject
     *
     * @param integer $f134fproject
     *
     * @return T134femployeeDelay
     */
    public function setF134fproject($f134fproject)
    {
        $this->f134fproject = $f134fproject;

        return $this;
    }
    /**
     * Get f134fproject
     *
     * @return integer
     */
    public function getF134fproject()
    {
        return $this->f134fproject;
    }

    /**
     * Set f134fmodule
     *
     * @param integer $f134fmodule
     *
     * @return T134femployeeDelay
     */
    public function setF134fmodule($f134fmodule)
    {
        $this->f134fmodule = $f134fmodule;

        return $this;
    }
    /**
     * Get f134fmodule
     *
     * @return integer
     */
    public function getF134fmodule()
    {
        return $this->f134fmodule;
    }
     /**
     * Set f134fscreen
     *
     * @param integer $f134fscreen
     *
     * @return T134femployeeDelay
     */
    public function setF134fscreen($f134fscreen)
    {
        $this->f134fscreen = $f134fscreen;

        return $this;
    }
    /**
     * Get f134fscreen
     *
     * @return integer
     */
    public function getF134fscreen()
    {
        return $this->f134fscreen;
    }
    
     /**
     * Set f134ftitle
     *
     * @param string $f134ftitle
     *
     * @return T134femployeeDelay
     */
    public function setF134ftitle($f134ftitle)
    {
        $this->f134ftitle = $f134ftitle;

        return $this;
    }
    /**
     * Get f134ftitle
     *
     * @return string
     */
    public function getF134ftitle()
    {
        return $this->f134ftitle;
    }
    /**
     * Set f134fdescription
     *
     * @param string $f134fdescription
     *
     * @return T134femployeeDelay
     */
    public function setF134fdescription($f134fdescription)
    {
        $this->f134fdescription = $f134fdescription;

        return $this;
    }
    /**
     * Get f134fdescription
     *
     * @return string
     */
    public function getF134fdescription()
    {
        return $this->f134fdescription;
    }
    /**
     * Set f134fhours
     *
     * @param integer $f134fhours
     *
     * @return T134femployeeDelay
     */
    public function setF134fhours($f134fhours)
    {
        $this->f134fhours = $f134fhours;

        return $this;
    }
    /**
     * Get f134fhours
     *
     * @return integer
     */
    public function getF134fhours()
    {
        return $this->f134fhours;
    }
    /**
     * Set f134fstatus
     *
     * @param integer $f134fstatus
     *
     * @return T134femployeeDelay
     */
    public function setF134fstatus($f134fstatus)
    {
        $this->f134fstatus = $f134fstatus;

        return $this;
    }
    /**
     * Get f134fstatus
     *
     * @return integer
     */
    public function getF134fstatus()
    {
        return $this->f134fstatus;
    }
    /**
     * Set f134fcreatedBy
     *
     * @param integer $f134fcreatedBy
     *
     * @return T134femployeeDelay
     */
    public function setF134fcreatedBy($f134fcreatedBy)
    {
        $this->f134fcreatedBy = $f134fcreatedBy;

        return $this;
    }
    /**
     * Get f134fcreatedBy
     *
     * @return integer
     */
    public function getF134fcreatedBy()
    {
        return $this->f134fcreatedBy;
    }
    /**
     * Set f134fupdatedBy
     *
     * @param integer $f134fupdatedBy
     *
     * @return T134femployeeDelay
     */
    public function setF134fupdatedBy($f134fupdatedBy)
    {
        $this->f134fupdatedBy = $f134fupdatedBy;

        return $this;
    }
    /**
     * Get f134fupdatedBy
     *
     * @return integer
     */
    public function getF134fupdatedBy()
    {
        return $this->f134fupdatedBy;
    }
    /**
     * Set f134fcreateDtTm
     *
     * @param \DateTime $f134fcreateDtTm
     *
     * @return T134femployeeDelay
     */
    public function setF134fcreateDtTm($f134fcreateDtTm)
    {
        $this->f134fcreateDtTm = $f134fcreateDtTm;

        return $this;
    }
    /**
     * Get f134fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF134fcreateDtTm()
    {
        return $this->f134fcreateDtTm;
    }
     /**
     * Set f134fupdateDtTm
     *
     * @param \DateTime $f134fupdateDtTm
     *
     * @return T134femployeeDelay
     */
    public function setF134fupdateDtTm($f134fupdateDtTm)
    {
        $this->f134fupdateDtTm = $f134fupdateDtTm;

        return $this;
    }
    /**
     * Get f134fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF134fupdateDtTm()
    {
        return $this->f134fupdateDtTm;
    }
}
?>