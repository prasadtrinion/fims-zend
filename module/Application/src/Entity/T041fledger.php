<?php

namespace Application\Entity;
use Application\Repository\LedgerRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * T041fledger
 *
 * @ORM\Table(name="t041fledger", indexes={@ORM\Index(name="f041fupdated_by", columns={"f041fupdated_by"}), @ORM\Index(name="f041fcreated_by", columns={"f041fcreated_by"}), @ORM\Index(name="f041fglcode", columns={"f041fgl_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\LedgerRepository")
 */
class T041fledger
{

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f041fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fname", type="string", length=50, nullable=false)
     */
    private $f041fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f041falt_name", type="string", length=50, nullable=false)
     */
    private $f041faltName;

    /**
     * @var string
     *
     * @ORM\Column(name="f041fgl_code", type="string", length=50, nullable=false)
     */
    private $f041fglCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f041fgst_number", type="string", length=50, nullable=false)
     */
    private $f041fgstNumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f041fgroup_name", type="string", length=50, nullable=false)
     */
    private $f041fgroupName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fopening_balance", type="integer", nullable=false)
     */
    private $f041fopeningBalance;


    /**
     * @var boolean
     *
     * @ORM\Column(name="f041factive", type="boolean", nullable=false)
     */
    private $f041factive;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fis_sub_ledger", type="integer", nullable=false)
     */
    private $f041fisSubLedger;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f041fallow_sub_account", type="integer", nullable=false)
     */

    private $f041fallowSubAccount;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f041fapply_billwise", type="integer", nullable=false)
     */
    private $f041fapplyBillwise;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f041fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f041fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f041fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f041fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f041fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f041fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f041fcreatedBy;



    /**
     * Get f041fid
     *
     * @return integer
     */
    public function getF041fid()
    {
        return $this->f041fid;
    }

    /**
     * Set f041fname
     *
     * @param string $f041fname
     *
     * @return T041fledger
     */
    public function setF041fname($f041fname)
    {
        $this->f041fname = $f041fname;

        return $this;
    }

    /**
     * Get f041fname
     *
     * @return string
     */
    public function getF041fname()
    {
        return $this->f041fname;
    }

    /**
     * Set f041faltName
     *
     * @param string $f041faltName
     *
     * @return T041fledger
     */
    public function setF041faltName($f041faltName)
    {
        $this->f041faltName = $f041faltName;

        return $this;
    }

    /**
     * Get f041faltName
     *
     * @return string
     */
    public function getF041faltName()
    {
        return $this->f041faltName;
    }

    /**
     * Set f041fglCode
     *
     * @param string $f041fglCode
     *
     * @return T014fglcode
     */
    public function setF041fglCode( $f041fglCode )
    {
        $this->f041fglCode = $f041fglCode;

        return $this;
    }

    /**
     * Get f041fglCode
     *
     * @return string
     */
    public function getF041fglCode()
    {
        return $this->f041fglCode;
    }

    /**
     * Set f041fgstNumber
     *
     * @param string $f041fgstNumber
     *
     * @return T041fledger
     */
    public function setF041fgstNumber($f041fgstNumber)
    {
        $this->f041fgstNumber = $f041fgstNumber;

        return $this;
    }

    /**
     * Get f041fgstNumber
     *
     * @return string
     */
    public function getF041fgstNumber()
    {
        return $this->f041fgstNumber;
    }


    /**
     * Set f041fgroupName
     *
     * @param string $f041fgroupName
     *
     * @return T012fgroup
     */
    public function setF041fgroupName( $f041fgroupName )
    {
        $this->f041fgroupName = $f041fgroupName;

        return $this;
    }

    /**
     * Get f041fgroupName
     *
     * @return string
     */
    public function getF041fgroupName()
    {
        return $this->f041fgroupName;
    }

    /**
     * Set f041fopeningBalance
     *
     * @param integer $f041fopeningBalance
     *
     * @return T041fledger
     */
    public function setF041fopeningBalance($f041fopeningBalance)
    {
        $this->f041fopeningBalance = $f041fopeningBalance;

        return $this;
    }

    /**
     * Get f041fopeningBalance
     *
     * @return integer
     */
    public function getF041fopeningBalance()
    {
        return $this->f041fopeningBalance;
    }

    /**
     * Set f041fisSubLedger
     *
     * @param boolean $f041fisSubLedger
     *
     * @return T041fledger
     */
    public function setF041fisSubLedger($f041fisSubLedger)
    {
        $this->f041fisSubLedger = $f041fisSubLedger;

        return $this;
    }

    /**
     * Get f041fisSubLedger
     *
     * @return boolean
     */
    public function getF041fisSubLedger()
    {
        return $this->f041fisSubLedger;
    }


    /**
     * Set f041fallowSubAccount
     *
     * @param boolean $f041fallowSubAccount
     *
     * @return T041fledger
     */
    public function setF041fallowSubAccount($f041fallowSubAccount)
    {
        $this->f041fallowSubAccount = $f041fallowSubAccount;

        return $this;
    }

    /**
     * Get f041fallowSubAccount
     *
     * @return boolean
     */
    public function getF041fallowSubAccount()
    {
        return $this->f041fallowSubAccount;
    }


    /**
     * Set f041fapplyBillwise
     *
     * @param boolean $f041fapplyBillwise
     *
     * @return T041fledger
     */
    public function setF041fapplyBillwise($f041fapplyBillwise)
    {
        $this->f041fapplyBillwise = $f041fapplyBillwise;

        return $this;
    }

    /**
     * Get f041fapplyBillwise
     *
     * @return boolean
     */
    public function getF041fapplyBillwise()
    {
        return $this->f041fapplyBillwise;
    }

    /**
     * Set f041factive
     *
     * @param integer $f041factive
     *
     * @return T041fledger
     */
    public function setF041factive($f041factive)
    {
        $this->f041factive = $f041factive;

        return $this;
    }

    /**
     * Get f041factive
     *
     * @return integer
     */
    public function getF041factive()
    {
        return $this->f041factive;
    }

    /**
     * Set f041fcreatedDtTm
     *
     * @param \DateTime $f041fcreatedDtTm
     *
     * @return T041fledger
     */
    public function setF041fcreatedDtTm($f041fcreatedDtTm)
    {
        $this->f041fcreatedDtTm = $f041fcreatedDtTm;

        return $this;
    }

    /**
     * Get f041fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF041fcreatedDtTm()
    {
        return $this->f041fcreatedDtTm;
    }

    /**
     * Set f041fupdatedDtTm
     *
     * @param \DateTime $f041fupdatedDtTm
     *
     * @return T041fledger
     */
    public function setF041fupdatedDtTm($f041fupdatedDtTm)
    {
        $this->f041fupdatedDtTm = $f041fupdatedDtTm;

        return $this;
    }

    /**
     * Get f041fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF041fupdatedDtTm()
    {
        return $this->f041fupdatedDtTm;
    }

    /**
     * Set f041fupdatedBy
     *
     * @param integer $f041fupdatedBy
     *
     * @return T041fledger
     */
    public function setF041fupdatedBy( $f041fupdatedBy )
    {
        $this->f041fupdatedBy = $f041fupdatedBy;

        return $this;
    }

    /**
     * Get f041fupdatedBy
     *
     * @return integer
     */
    public function getF041fupdatedBy()
    {
        return $this->f041fupdatedBy;
    }

    /**
     * Set f041fcreatedBy
     *
     * @param integer $f041fcreatedBy
     *
     * @return T041fledger
     */
    public function setF041fcreatedBy( $f041fcreatedBy )
    {
        $this->f041fcreatedBy = $f041fcreatedBy;

        return $this;
    }

    /**
     * Get f041fcreatedBy
     *
     * @return integer
     */
    public function getF041fcreatedBy()
    {
        return $this->f041fcreatedBy;
    }
}
