<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T136fetf
 *
 * @ORM\Table(name="t136fetf")
 * @ORM\Entity(repositoryClass="Application\Repository\EtfRepository")
 */
class T136fetf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f136fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f136fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f136fpayment_mode", type="integer", nullable=true)
     */
    private $f136fpaymentMode;

    /**
     * @var string
     *
     * @ORM\Column(name="f136fvoucher_type", type="integer", nullable=true)
     */
    private $f136fvoucherType;


    /**
     * @var integer
     *
     * @ORM\Column(name="f136fbatch_id",type="string",length=50, nullable=true)
     * 
     */
    private $f136fbatchId;


    /**
     * @var integer
     *
     * @ORM\Column(name="f136fuum_bank", type="integer", nullable=true)
     * 
     */
    private $f136fuumBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136famount", type="integer", nullable=true)
     * 
     */
    private $f136famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136freference_number",type="string",length=50, nullable=true)
     * 
     */
    private $f136freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fstatus", type="integer", nullable=true)
     */
    private $f136fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fcreated_by", type="integer", nullable=true)
     * 
     */
    private $f136fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f136fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f136fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f136fupdated_by", type="integer", nullable=true)
     * 
     */
    private $f136fupdatedBy;
    /**
     * @var integer
     *
     * @ORM\Column(name="f136fapproved_by", type="integer", nullable=true)
     * 
     */
    private $f136fapprovedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f136fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f136fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136freason",type="string",length=50, nullable=true)
     * 
     */
    private $f136freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fletter_reference",type="string",length=50, nullable=true)
     * 
     */
    private $f136fletterReference;

    /**
     * @var integer
     *
     * @ORM\Column(name="f136fpay_date",type="datetime", nullable=true)
     * 
     */
    private $f136fpayDate;
 	

    /**
     * Get f136fid
     *
     * @return integer
     */
    public function getF136fid()
    {
        return $this->f136fid;
    }

    /**
     * Set f136fpaymentMode
     *
     * @param string $f136fpaymentMode
     *
     * @return T136fetf
     */
    public function setF136fpaymentMode($f136fpaymentMode)
    {
        $this->f136fpaymentMode = $f136fpaymentMode;

        return $this;
    }

    /**
     * Get f136fpaymentMode
     *
     * @return string
     */
    public function getF136fpaymentMode()
    {
        return $this->f136fpaymentMode;
    }

    /**
     * Set f136fvoucherType
     *
     * @param string $f136fvoucherType
     *
     * @return T136fetf
     */
    public function setF136fvoucherType($f136fvoucherType)
    {
        $this->f136fvoucherType = $f136fvoucherType;

        return $this;
    }

    /**
     * Get f136fvoucherType
     *
     * @return string
     */
    public function getF136fvoucherType()
    {
        return $this->f136fvoucherType;
    }


    /**
     * Set f136fbatchId
     *
     * @param integer $f136fbatchId
     *
     * @return T136fetf
     */
    public function setF136fbatchId($f136fbatchId)
    {
        $this->f136fbatchId = $f136fbatchId;

        return $this;
    }

    /**
     * Get f136fbatchId
     *
     * @return integer
     */
    public function getF136fbatchId()
    {
        return $this->f136fbatchId;
    }

    /**
     * Set f136fuumBank
     *
     * @param integer $f136fuumBank
     *
     * @return T136fetf
     */
    public function setF136fuumBank($f136fuumBank)
    {
        $this->f136fuumBank = $f136fuumBank;

        return $this;
    }

    /**
     * Get f136fuumBank
     *
     * @return integer
     */
    public function getF136fuumBank()
    {
        return $this->f136fuumBank;
    }

    /**
     * Set f136famount
     *
     * @param integer $f136famount
     *
     * @return T136fetf
     */
    public function setF136famount($f136famount)
    {
        $this->f136famount = $f136famount;

        return $this;
    }

    /**
     * Get f136famount
     *
     * @return integer
     */
    public function getF136famount()
    {
        return $this->f136famount;
    }



    /**
     * Set f136freferenceNumber
     *
     * @param integer $f136freferenceNumber
     *
     * @return T136fetf
     */
    public function setF136freferenceNumber($f136freferenceNumber)
    {
        $this->f136freferenceNumber = $f136freferenceNumber;

        return $this;
    }

    /**
     * Get f136freferenceNumber
     *
     * @return integer
     */
    public function getF136freferenceNumber()
    {
        return $this->f136freferenceNumber;
    }

    /**
     * Set f136fupdatedDtTm
     *
     * @param \DateTime $f136fupdatedDtTm
     *
     * @return T136fetf
     */
    public function setF136fupdatedDtTm($f136fupdatedDtTm)
    {
        $this->f136fupdatedDtTm = $f136fupdatedDtTm;

        return $this;
    }

    /**
     * Get f136fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF136fupdatedDtTm()
    {
        return $this->f136fupdatedDtTm;
    }


    /**
     * Set f136fcreatedDtTm
     *
     * @param \DateTime $f136fcreatedDtTm
     *
     * @return T136fetf
     */
    public function setF136fcreatedDtTm($f136fcreatedDtTm)
    {
        $this->f136fcreatedDtTm = $f136fcreatedDtTm;

        return $this;
    }

    /**
     * Get f136fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF136fcreatedDtTm()
    {
        return $this->f136fcreatedDtTm;
    }

    /**
     * Set f136fstatus
     *
     * @param integer $f136fstatus
     *
     * @return T136fetf
     */
    public function setF136fstatus($f136fstatus)
    {
        $this->f136fstatus = $f136fstatus;

        return $this;
    }

    /**
     * Get f136fstatus
     *
     * @return integer
     */
    public function getF136fstatus()
    {
        return $this->f136fstatus;
    }

    /**
     * Set f136fupdatedBy
     *
     * @param integer $f136fupdatedBy
     *
     * @return T136fetf
     */
    public function setF136fupdatedBy($f136fupdatedBy)
    {
        $this->f136fupdatedBy = $f136fupdatedBy;

        return $this;
    }

    /**
     * Get f136fupdatedBy
     *
     * @return integer
     */
    public function getF136fupdatedBy()
    {
        return $this->f136fupdatedBy;
    }

    /**
     * Set f136fcreatedBy
     *
     * @param integer $f136fcreatedBy
     *
     * @return T136fetf
     */
    public function setF136fcreatedBy($f136fcreatedBy)
    {
        $this->f136fcreatedBy = $f136fcreatedBy;

        return $this;
    }

    /**
     * Get f136fcreatedBy
     *
     * @return integer
     */
    public function getF136fcreatedBy()
    {
        return $this->f136fcreatedBy;
    }

     /**
     * Set f136fapprovedBy
     *
     * @param integer $f136fapprovedBy
     *
     * @return T136fetf
     */
    public function setF136fapprovedBy($f136fapprovedBy)
    {
        $this->f136fapprovedBy = $f136fapprovedBy;

        return $this;
    }

    /**
     * Get f136fapprovedBy
     *
     * @return integer
     */
    public function getF136fapprovedBy()
    {
        return $this->f136fapprovedBy;
    }

     /**
     * Set f136freason
     *
     * @param integer $f136freason
     *
     * @return T136fetf
     */
    public function setF136freason($f136freason)
    {
        $this->f136freason = $f136freason;

        return $this;
    }

    /**
     * Get f136freason
     *
     * @return integer
     */
    public function getF136freason()
    {
        return $this->f136freason;
    }

     /**
     * Set f136fletterReference
     *
     * @param integer $f136fletterReference
     *
     * @return T136fetf
     */
    public function setF136fletterReference($f136fletterReference)
    {
        $this->f136fletterReference = $f136fletterReference;

        return $this;
    }

    /**
     * Get f136fletterReference
     *
     * @return integer
     */
    public function getF136fletterReference()
    {
        return $this->f136fletterReference;
    }

     /**
     * Set f136fpayDate
     *
     * @param integer $f136fpayDate
     *
     * @return T136fetf
     */
    public function setF136fpayDate($f136fpayDate)
    {
        $this->f136fpayDate = $f136fpayDate;

        return $this;
    }

    /**
     * Get f136fpayDate
     *
     * @return integer
     */
    public function getF136fpayDate()
    {
        return $this->f136fpayDate;
    }

}
