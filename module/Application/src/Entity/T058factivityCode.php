<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T058factivityCode
 *
 * @ORM\Table(name="t058factivity_code", uniqueConstraints={@ORM\UniqueConstraint(name="f058fcomplete_code_UNIQUE", columns={"f058fcomplete_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ActivityCodeRepository")
 */
class T058factivityCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f058fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f058fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f058fcode", type="string", length=255, nullable=false)
     */
    private $f058fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f058fname", type="string", length=255, nullable=false)
     */
    private $f058fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f058ftype", type="string", length=100, nullable=true)
     */
    private $f058ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f058fdescription", type="string", length=100, nullable=true)
     */
    private $f058fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f058fshort_code", type="string", length=100, nullable=true)
     */
    private $f058fshortCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058fparent_code", type="integer", nullable=true)
     */
    private $f058fparentCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058fstatus", type="integer", nullable=false)
     */
    private $f058fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058fref_code", type="integer", nullable=true)
     */
    private $f058frefCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058flevel_status", type="integer", nullable=true)
     */
    private $f058flevelStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f058fcomplete_code", type="string", length=10, nullable=true)
     */
    private $f058fcompleteCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058fcreated_by", type="integer", nullable=true)
     */
    private $f058fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f058fupdated_by", type="integer", nullable=true)
     */
    private $f058fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f058fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f058fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f058fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f058fupdatedDtTm;



    /**
     * Get f058fid
     *
     * @return integer
     */
    public function getF058fid()
    {
        return $this->f058fid;
    }

    /**
     * Set f058fcode
     *
     * @param string $f058fcode
     *
     * @return T058factivityCode
     */
    public function setF058fcode($f058fcode)
    {
        $this->f058fcode = $f058fcode;

        return $this;
    }

    /**
     * Get f058fcode
     *
     * @return string
     */
    public function getF058fcode()
    {
        return $this->f058fcode;
    }

    /**
     * Set f058fname
     *
     * @param string $f058fname
     *
     * @return T058factivityCode
     */
    public function setF058fname($f058fname)
    {
        $this->f058fname = $f058fname;

        return $this;
    }

    /**
     * Get f058fname
     *
     * @return string
     */
    public function getF058fname()
    {
        return $this->f058fname;
    }

    /**
     * Set f058ftype
     *
     * @param string $f058ftype
     *
     * @return T058factivityCode
     */
    public function setF058ftype($f058ftype)
    {
        $this->f058ftype = $f058ftype;

        return $this;
    }

    /**
     * Get f058ftype
     *
     * @return string
     */
    public function getF058ftype()
    {
        return $this->f058ftype;
    }

    /**
     * Set f058fdescription
     *
     * @param string $f058fdescription
     *
     * @return T058factivityCode
     */
    public function setF058fdescription($f058fdescription)
    {
        $this->f058fdescription = $f058fdescription;

        return $this;
    }

    /**
     * Get f058fdescription
     *
     * @return string
     */
    public function getF058fdescription()
    {
        return $this->f058fdescription;
    }

    /**
     * Set f058fshortCode
     *
     * @param string $f058fshortCode
     *
     * @return T058factivityCode
     */
    public function setF058fshortCode($f058fshortCode)
    {
        $this->f058fshortCode = $f058fshortCode;

        return $this;
    }

    /**
     * Get f058fshortCode
     *
     * @return string
     */
    public function getF058fshortCode()
    {
        return $this->f058fshortCode;
    }

    /**
     * Set f058fparentCode
     *
     * @param integer $f058fparentCode
     *
     * @return T058factivityCode
     */
    public function setF058fparentCode($f058fparentCode)
    {
        $this->f058fparentCode = $f058fparentCode;

        return $this;
    }

    /**
     * Get f058fparentCode
     *
     * @return integer
     */
    public function getF058fparentCode()
    {
        return $this->f058fparentCode;
    }

    /**
     * Set f058fstatus
     *
     * @param integer $f058fstatus
     *
     * @return T058factivityCode
     */
    public function setF058fstatus($f058fstatus)
    {
        $this->f058fstatus = $f058fstatus;

        return $this;
    }

    /**
     * Get f058fstatus
     *
     * @return integer
     */
    public function getF058fstatus()
    {
        return $this->f058fstatus;
    }

    /**
     * Set f058frefCode
     *
     * @param integer $f058frefCode
     *
     * @return T058factivityCode
     */
    public function setF058frefCode($f058frefCode)
    {
        $this->f058frefCode = $f058frefCode;

        return $this;
    }

    /**
     * Get f058frefCode
     *
     * @return integer
     */
    public function getF058frefCode()
    {
        return $this->f058frefCode;
    }

    /**
     * Set f058flevelStatus
     *
     * @param integer $f058flevelStatus
     *
     * @return T058factivityCode
     */
    public function setF058flevelStatus($f058flevelStatus)
    {
        $this->f058flevelStatus = $f058flevelStatus;

        return $this;
    }

    /**
     * Get f058flevelStatus
     *
     * @return integer
     */
    public function getF058flevelStatus()
    {
        return $this->f058flevelStatus;
    }

    /**
     * Set f058fcompleteCode
     *
     * @param string $f058fcompleteCode
     *
     * @return T058factivityCode
     */
    public function setF058fcompleteCode($f058fcompleteCode)
    {
        $this->f058fcompleteCode = $f058fcompleteCode;

        return $this;
    }

    /**
     * Get f058fcompleteCode
     *
     * @return string
     */
    public function getF058fcompleteCode()
    {
        return $this->f058fcompleteCode;
    }

    /**
     * Set f058fcreatedBy
     *
     * @param integer $f058fcreatedBy
     *
     * @return T058factivityCode
     */
    public function setF058fcreatedBy($f058fcreatedBy)
    {
        $this->f058fcreatedBy = $f058fcreatedBy;

        return $this;
    }

    /**
     * Get f058fcreatedBy
     *
     * @return integer
     */
    public function getF058fcreatedBy()
    {
        return $this->f058fcreatedBy;
    }

    /**
     * Set f058fupdatedBy
     *
     * @param integer $f058fupdatedBy
     *
     * @return T058factivityCode
     */
    public function setF058fupdatedBy($f058fupdatedBy)
    {
        $this->f058fupdatedBy = $f058fupdatedBy;

        return $this;
    }

    /**
     * Get f058fupdatedBy
     *
     * @return integer
     */
    public function getF058fupdatedBy()
    {
        return $this->f058fupdatedBy;
    }

    /**
     * Set f058fcreatedDtTm
     *
     * @param \DateTime $f058fcreatedDtTm
     *
     * @return T058factivityCode
     */
    public function setF058fcreatedDtTm($f058fcreatedDtTm)
    {
        $this->f058fcreatedDtTm = $f058fcreatedDtTm;

        return $this;
    }

    /**
     * Get f058fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF058fcreatedDtTm()
    {
        return $this->f058fcreatedDtTm;
    }

    /**
     * Set f058fupdatedDtTm
     *
     * @param \DateTime $f058fupdatedDtTm
     *
     * @return T058factivityCode
     */
    public function setF058fupdatedDtTm($f058fupdatedDtTm)
    {
        $this->f058fupdatedDtTm = $f058fupdatedDtTm;

        return $this;
    }

    /**
     * Get f058fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF058fupdatedDtTm()
    {
        return $this->f058fupdatedDtTm;
    }
}
