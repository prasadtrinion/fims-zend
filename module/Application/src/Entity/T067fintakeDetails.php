<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T067fintakeDetails
 *
 * @ORM\Table(name="t067fintake_details")
 * @ORM\Entity(repositoryClass="Application\Repository\IntakeRepository")
 */
class T067fintakeDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f067fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f067fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fid_intake", type="integer", nullable=false)
     */
    private $f067fidIntake;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fid_programme", type="integer", nullable=false)
     */
    private $f067fidProgramme;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fstart_date", type="datetime", nullable=false)
     */
    private $f067fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fend_date", type="datetime", nullable=false)
     */
    private $f067fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fid_category", type="integer", nullable=false)
     */
    private $f067fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fstatus", type="integer", nullable=true)
     */
    private $f067fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fcreated_by", type="integer", nullable=true)
     */
    private $f067fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f067fupdated_by", type="integer", nullable=true)
     */
    private $f067fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f067fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f067fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f067fupdatedDtTm;



    /**
     * Get f067fid
     *
     * @return integer
     */
    public function getF067fid()
    {
        return $this->f067fid;
    }

    /**
     * Set f067fidIntake
     *
     * @param integer $f067fidIntake
     *
     * @return T067fintakeDetails
     */
    public function setF067fidIntake($f067fidIntake)
    {
        $this->f067fidIntake = $f067fidIntake;

        return $this;
    }

    /**
     * Get f067fidIntake
     *
     * @return integer
     */
    public function getF067fidIntake()
    {
        return $this->f067fidIntake;
    }

    /**
     * Set f067fidProgramme
     *
     * @param integer $f067fidProgramme
     *
     * @return T067fintakeDetails
     */
    public function setF067fidProgramme($f067fidProgramme)
    {
        $this->f067fidProgramme = $f067fidProgramme;

        return $this;
    }

    /**
     * Get f067fidProgramme
     *
     * @return integer
     */
    public function getF067fidProgramme()
    {
        return $this->f067fidProgramme;
    }

    /**
     * Set f067fstartDate
     *
     * @param \DateTime $f067fstartDate
     *
     * @return T067fintakeDetails
     */
    public function setF067fstartDate($f067fstartDate)
    {
        $this->f067fstartDate = $f067fstartDate;

        return $this;
    }

    /**
     * Get f067fstartDate
     *
     * @return \DateTime
     */
    public function getF067fstartDate()
    {
        return $this->f067fstartDate;
    }

    /**
     * Set f067fendDate
     *
     * @param \DateTime $f067fendDate
     *
     * @return T067fintakeDetails
     */
    public function setF067fendDate($f067fendDate)
    {
        $this->f067fendDate = $f067fendDate;

        return $this;
    }

    /**
     * Get f067fendDate
     *
     * @return \DateTime
     */
    public function getF067fendDate()
    {
        return $this->f067fendDate;
    }

    /**
     * Set f067fidCategory
     *
     * @param integer $f067fidCategory
     *
     * @return T067fintakeDetails
     */
    public function setF067fidCategory($f067fidCategory)
    {
        $this->f067fidCategory = $f067fidCategory;

        return $this;
    }

    /**
     * Get f067fidCategory
     *
     * @return integer
     */
    public function getF067fidCategory()
    {
        return $this->f067fidCategory;
    }

    /**
     * Set f067fstatus
     *
     * @param integer $f067fstatus
     *
     * @return T067fintakeDetails
     */
    public function setF067fstatus($f067fstatus)
    {
        $this->f067fstatus = $f067fstatus;

        return $this;
    }

    /**
     * Get f067fstatus
     *
     * @return integer
     */
    public function getF067fstatus()
    {
        return $this->f067fstatus;
    }

    /**
     * Set f067fcreatedBy
     *
     * @param integer $f067fcreatedBy
     *
     * @return T067fintakeDetails
     */
    public function setF067fcreatedBy($f067fcreatedBy)
    {
        $this->f067fcreatedBy = $f067fcreatedBy;

        return $this;
    }

    /**
     * Get f067fcreatedBy
     *
     * @return integer
     */
    public function getF067fcreatedBy()
    {
        return $this->f067fcreatedBy;
    }

    /**
     * Set f067fupdatedBy
     *
     * @param integer $f067fupdatedBy
     *
     * @return T067fintakeDetails
     */
    public function setF067fupdatedBy($f067fupdatedBy)
    {
        $this->f067fupdatedBy = $f067fupdatedBy;

        return $this;
    }

    /**
     * Get f067fupdatedBy
     *
     * @return integer
     */
    public function getF067fupdatedBy()
    {
        return $this->f067fupdatedBy;
    }

    /**
     * Set f067fcreatedDtTm
     *
     * @param \DateTime $f067fcreatedDtTm
     *
     * @return T067fintakeDetails
     */
    public function setF067fcreatedDtTm($f067fcreatedDtTm)
    {
        $this->f067fcreatedDtTm = $f067fcreatedDtTm;

        return $this;
    }

    /**
     * Get f067fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF067fcreatedDtTm()
    {
        return $this->f067fcreatedDtTm;
    }

    /**
     * Set f067fupdatedDtTm
     *
     * @param \DateTime $f067fupdatedDtTm
     *
     * @return T067fintakeDetails
     */
    public function setF067fupdatedDtTm($f067fupdatedDtTm)
    {
        $this->f067fupdatedDtTm = $f067fupdatedDtTm;

        return $this;
    }

    /**
     * Get f067fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF067fupdatedDtTm()
    {
        return $this->f067fupdatedDtTm;
    }
}
