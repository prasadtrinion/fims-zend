<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T099fapplyStock
 *
 * @ORM\Table(name="t099fapply_stock")
 * @ORM\Entity(repositoryClass="Application\Repository\ApplyStockRepository")
 */
class T099fapplyStock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f099fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f099fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f099fapplicant_name", type="integer", nullable=true)
     */
    private $f099fapplicantName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fposition", type="integer", nullable=true)
     */
    private $f099fposition;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fdepartment", type="integer", nullable=true)
     */
    private $f099fdepartment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099frequired_date", type="datetime", nullable=true)
     */
    private $f099frequiredDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fstatus", type="integer", nullable=true)
     */
    private $f099fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fapproval_status", type="integer", nullable=true)
     */
    private $f099fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fcreated_by", type="integer", nullable=true)
     */
    private $f099fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f099fupdated_by", type="integer", nullable=true)
     */
    private $f099fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f099fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f099fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f099fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f099freason", type="string", length=50, nullable=true)
     */
    private $f099freason;



    /**
     * Get f099fid
     *
     * @return integer
     */
    public function getF099fid()
    {
        return $this->f099fid;
    }

    /**
     * Set f099fapplicantName
     *
     * @param string $f099fapplicantName
     *
     * @return T099fapplyStock
     */
    public function setF099fapplicantName($f099fapplicantName)
    {
        $this->f099fapplicantName = $f099fapplicantName;

        return $this;
    }

    /**
     * Get f099fapplicantName
     *
     * @return string
     */
    public function getF099fapplicantName()
    {
        return $this->f099fapplicantName;
    }

    /**
     * Set f099fposition
     *
     * @param integer $f099fposition
     *
     * @return T099fapplyStock
     */
    public function setF099fposition($f099fposition)
    {
        $this->f099fposition = $f099fposition;

        return $this;
    }

    /**
     * Get f099fposition
     *
     * @return integer
     */
    public function getF099fposition()
    {
        return $this->f099fposition;
    }

    /**
     * Set f099fdepartment
     *
     * @param integer $f099fdepartment
     *
     * @return T099fapplyStock
     */
    public function setF099fdepartment($f099fdepartment)
    {
        $this->f099fdepartment = $f099fdepartment;

        return $this;
    }

    /**
     * Get f099fdepartment
     *
     * @return integer
     */
    public function getF099fdepartment()
    {
        return $this->f099fdepartment;
    }

    /**
     * Set f099frequiredDate
     *
     * @param \DateTime $f099frequiredDate
     *
     * @return T099fapplyStock
     */
    public function setF099frequiredDate($f099frequiredDate)
    {
        $this->f099frequiredDate = $f099frequiredDate;

        return $this;
    }

    /**
     * Get f099frequiredDate
     *
     * @return \DateTime
     */
    public function getF099frequiredDate()
    {
        return $this->f099frequiredDate;
    }

    /**
     * Set f099fstatus
     *
     * @param integer $f099fstatus
     *
     * @return T099fapplyStock
     */
    public function setF099fstatus($f099fstatus)
    {
        $this->f099fstatus = $f099fstatus;

        return $this;
    }

    /**
     * Get f099fstatus
     *
     * @return integer
     */
    public function getF099fstatus()
    {
        return $this->f099fstatus;
    }

    /**
     * Set f099fapprovalStatus
     *
     * @param integer $f099fapprovalStatus
     *
     * @return T099fapplyStock
     */
    public function setF099fapprovalStatus($f099fapprovalStatus)
    {
        $this->f099fapprovalStatus = $f099fapprovalStatus;

        return $this;
    }

    /**
     * Get f099fapprovalStatus
     *
     * @return integer
     */
    public function getF099fapprovalStatus()
    {
        return $this->f099fapprovalStatus;
    }

    /**
     * Set f099fcreatedBy
     *
     * @param integer $f099fcreatedBy
     *
     * @return T099fapplyStock
     */
    public function setF099fcreatedBy($f099fcreatedBy)
    {
        $this->f099fcreatedBy = $f099fcreatedBy;

        return $this;
    }

    /**
     * Get f099fcreatedBy
     *
     * @return integer
     */
    public function getF099fcreatedBy()
    {
        return $this->f099fcreatedBy;
    }

    /**
     * Set f099fupdatedBy
     *
     * @param integer $f099fupdatedBy
     *
     * @return T099fapplyStock
     */
    public function setF099fupdatedBy($f099fupdatedBy)
    {
        $this->f099fupdatedBy = $f099fupdatedBy;

        return $this;
    }

    /**
     * Get f099fupdatedBy
     *
     * @return integer
     */
    public function getF099fupdatedBy()
    {
        return $this->f099fupdatedBy;
    }

    /**
     * Set f099fcreatedDtTm
     *
     * @param \DateTime $f099fcreatedDtTm
     *
     * @return T099fapplyStock
     */
    public function setF099fcreatedDtTm($f099fcreatedDtTm)
    {
        $this->f099fcreatedDtTm = $f099fcreatedDtTm;

        return $this;
    }

    /**
     * Get f099fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fcreatedDtTm()
    {
        return $this->f099fcreatedDtTm;
    }

    /**
     * Set f099fupdatedDtTm
     *
     * @param \DateTime $f099fupdatedDtTm
     *
     * @return T099fapplyStock
     */
    public function setF099fupdatedDtTm($f099fupdatedDtTm)
    {
        $this->f099fupdatedDtTm = $f099fupdatedDtTm;

        return $this;
    }

    /**
     * Get f099fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF099fupdatedDtTm()
    {
        return $this->f099fupdatedDtTm;
    }

    /**
     * Set f099freason
     *
     * @param string $f099freason
     *
     * @return T099fapplyStock
     */
    public function setF099freason($f099freason)
    {
        $this->f099freason = $f099freason;

        return $this;
    }

    /**
     * Get f099freason
     *
     * @return string
     */
    public function getF099freasone()
    {
        return $this->f099freason;
    }
}
