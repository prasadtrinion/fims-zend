<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fapplicationDetails
 *
 * @ORM\Table(name="t063fapplication_details")
 * @ORM\Entity(repositoryClass="Application\Repository\InvestmentApplicationRepository")
 */
class T063fapplicationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fidDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fbranch", type="string", length=255, nullable=true)
     */
    private $f063fbranch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063famount", type="integer", nullable=false)
     */
    private $f063famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fduration", type="string", length=100, nullable=true)
     */
    private $f063fduration;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fduration_type", type="string", length=100, nullable=true)
     */
    private $f063fdurationType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063finstitution_id", type="integer", nullable=false)
     */
    private $f063finstitutionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_master", type="integer", nullable=false)
     */
    private $f063fidMaster;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fprofit_rate", type="integer", nullable=true)
     */
    private $f063fprofitRate;


    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=false)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=false)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=true)
     */
    private $f063fstatus=1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fmaturity_date", type="datetime", nullable=true)
     */
    private $f063fmaturityDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_investment_type", type="integer", nullable=true)
     */
    private $f063fidInvestmentType;

     /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved1_status", type="integer", nullable=true)
     */
    private $f063fapproved1Status;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved2_status", type="integer", nullable=true)
     */
    private $f063fapproved2Status;

     /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved3_status", type="integer", nullable=true)
     */
    private $f063fapproved3Status;

     /**
     * @var string
     *
     * @ORM\Column(name="f063freason1", type="string", length=300, nullable=true)
     */
    private $f063freason1='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f063freason2", type="string", length=300, nullable=true)
     */
    private $f063freason2='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f063freason3", type="string", length=300, nullable=true)
     */
    private $f063freason3='NULL';
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved1_by", type="integer", nullable=true)
     */
    private $f063fapproved1By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved2_by", type="integer", nullable=true)
     */
    private $f063fapproved2By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved3_by", type="integer", nullable=true)
     */
    private $f063fapproved3By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fbank_id", type="integer", nullable=true)
     */
    private $f063fbankId;


    /**
     * @var string
     *
     * @ORM\Column(name="f063fbank_batch",  type="string",length=50, nullable=true)
     */
    private $f063fbankBatch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fall_approved", type="integer", nullable=true)
     */
    private $f063fallApproved=0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063frenewal1", type="integer", nullable=true)
     */
    private $f063frenewal1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063frenewal2", type="integer", nullable=true)
     */
    private $f063frenewal2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063frenewal3", type="integer", nullable=true)
     */
    private $f063frenewal3;

     /**
     * @var integer
     *
     * @ORM\Column(name="f063flevel1", type="integer", nullable=true)
     */
    private $f063flevel1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063flevel2", type="integer", nullable=true)
     */
    private $f063flevel2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063flevel3", type="integer", nullable=true)
     */
    private $f063flevel3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063ffinal_status", type="integer", nullable=true)
     */
    private $f063ffinalStatus = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063ffinal_reason", type="string", length=255, nullable=true)
     */
    private $f063ffinalReason;


    /**
     * Get f063fidDetails
     *
     * @return integer
     */
    public function getF063fidDetails()
    {
        return $this->f063fidDetails;
    }

    /**
     * Set f063fbranch
     *
     * @param string $f063fbranch
     *
     * @return T063fapplicationDetails
     */
    public function setF063fbranch($f063fbranch)
    {
        $this->f063fbranch = $f063fbranch;

        return $this;
    }

    /**
     * Get f063fbranch
     *
     * @return string
     */
    public function getF063fbranch()
    {
        return $this->f063fbranch;
    }

    /**
     * Set f063famount
     *
     * @param integer $f063famount
     *
     * @return T063fapplicationDetails
     */
    public function setF063famount($f063famount)
    {
        $this->f063famount = $f063famount;

        return $this;
    }

    /**
     * Get f063famount
     *
     * @return integer
     */
    public function getF063famount()
    {
        return $this->f063famount;
    }

    /**
     * Set f063fduration
     *
     * @param string $f063fduration
     *
     * @return T063fapplicationDetails
     */
    public function setF063fduration($f063fduration)
    {
        $this->f063fduration = $f063fduration;

        return $this;
    }

    /**
     * Get f063fduration
     *
     * @return string
     */
    public function getF063fduration()
    {
        return $this->f063fduration;
    }



    /**
     * Set f063fdurationType
     *
     * @param string $f063fdurationType
     *
     * @return T063fapplicationDetails
     */
    public function setF063fdurationType($f063fdurationType)
    {
        $this->f063fdurationType = $f063fdurationType;

        return $this;
    }

    /**
     * Get f063fdurationType
     *
     * @return string
     */
    public function getF063fdurationType()
    {
        return $this->f063fdurationType;
    }

    /**
     * Get f063finstitutionId
     *
     * @return integer
     */
    public function getF063finstitutionId()
    {
        return $this->f063finstitutionId;
    }

    /**
     * Set f063finstitutionId
     *
     * @param integer $f063finstitutionId
     *
     * @return T063fapplicationDetails
     */
    public function setF063finstitutionId($f063finstitutionId)
    {
        $this->f063finstitutionId = $f063finstitutionId;

        return $this;
    }

    /**
     * Get f063fidMaster
     *
     * @return integer
     */
    public function getF063fidMaster()
    {
        return $this->f063fidMaster;
    }

     /**
     * Set f063fidMaster
     *
     * @param integer $f063fidMaster
     *
     * @return T063fapplicationDetails
     */
    public function setF063fidMaster($f063fidMaster)
    {
        $this->f063fidMaster = $f063fidMaster;

        return $this;
    }

    /**
     * Get f063fprofitRate
     *
     * @return integer
     */
    public function getFf063fprofitRate()
    {
        return $this->f063fprofitRate;
    }
    

    /**
     * Set f063fprofitRate
     *
     * @param integer $f063fprofitRate
     *
     * @return T063fapplicationDetails
     */
    public function setF063fprofitRate($f063fprofitRate)
    {
        $this->f063fprofitRate = $f063fprofitRate;

        return $this;
    }

    /**
     * Set f063fmaturityDate
     *
     * @param \DateTime $f063fmaturityDate
     *
     * @return T063fapplicationDetails
     */
    public function setF063fmaturityDate($f063fmaturityDate)
    {
        $this->f063fmaturityDate = $f063fmaturityDate;

        return $this;
    }

    /**
     * Get f063fmaturityDate
     *
     * @return \DateTime
     */
    public function getF063fmaturityDate()
    {
        return $this->f063fmaturityDate;
    }

    /**
     * Set f063fidInvestmentType
     *
     * @param integer $f063fidInvestmentType
     *
     * @return T063fapplicationDetails
     */
    public function setF063fidInvestmentType($f063fidInvestmentType)
    {
        $this->f063fidInvestmentType = $f063fidInvestmentType;

        return $this;
    }

    /**
     * Get f063fidInvestmentType
     *
     * @return integer
     */
    public function getF063fidInvestmentType()
    {
        return $this->f063fidInvestmentType;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fapplicationDetails
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }


    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fapplicationDetails
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    
    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fapplicationDetails
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fapplicationDetails
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fapplicationDetails
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063frenewal1
     *
     * @param integer $f063frenewal1
     *
     * @return T063fapplicationDetails
     */
    public function setF063frenewal1($f063frenewal1)
    {
        $this->f063frenewal1 = $f063frenewal1;

        return $this;
    }

    /**
     * Get f063frenewal1
     *
     * @return integer
     */
    public function getF063frenewal1()
    {
        return $this->f063frenewal1;
    }

    /**
     * Set f063frenewal2
     *
     * @param integer $f063frenewal2
     *
     * @return T063fapplicationDetails
     */
    public function setF063frenewal2($f063frenewal2)
    {
        $this->f063frenewal2 = $f063frenewal2;

        return $this;
    }

    /**
     * Get f063frenewal2
     *
     * @return integer
     */
    public function getFf063frenewal2()
    {
        return $this->f063frenewal2;
    }

     /**
     * Set f063frenewal3
     *
     * @param integer $f063frenewal3
     *
     * @return T063fapplicationDetails
     */
    public function setF063frenewal3($f063frenewal3)
    {
        $this->f063frenewal3 = $f063frenewal3;

        return $this;
    }

    /**
     * Get f063frenewal3
     *
     * @return integer
     */
    public function getF063frenewal3()
    {
        return $this->f063frenewal3;
    }

    /**
     * Set f063fapproved1Status
     *
     * @param integer $f063fapproved1Status
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved1Status($f063fapproved1Status)
    {
        $this->f063fapproved1Status = $f063fapproved1Status;

        return $this;
    }

    /**
     * Get f063fapproved1Status
     *
     * @return integer
     */
    public function getF063fapproved1Status()
    {
        return $this->f063fapproved1Status;
    }

    /**
     * Set f063fapproved2Status
     *
     * @param integer $f063fapproved2Status
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved2Status($f063fapproved2Status)
    {
        $this->f063fapproved2Status = $f063fapproved2Status;

        return $this;
    }

    /**
     * Get f063fapproved2Status
     *
     * @return integer
     */
    public function getF063fapproved2Status()
    {
        return $this->f063fapproved2Status;
    }

    /**
     * Set f063fapproved3Status
     *
     * @param integer $f063fapproved3Status
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved3Status($f063fapproved3Status)
    {
        $this->f063fapproved3Status = $f063fapproved3Status;

        return $this;
    }

    /**
     * Get f063fapproved3Status
     *
     * @return integer
     */
    public function getF063fapproved3Status()
    {
        return $this->f063fapproved3Status;
    }

    

    /**
     * Set f063freason1
     *
     * @param string $f063freason1
     *
     * @return T063fapplicationDetails
     */
    public function setF063freason1($f063freason1)
    {
        $this->f063freason1 = $f063freason1;

        return $this;
    }

    /**
     * Get f063freason1
     *
     * @return string
     */
    public function getF063freason1()
    {
        return $this->f063freason1;
    }

    /**
     * Set f063freason2
     *
     * @param string $f063freason2
     *
     * @return T063fapplicationDetails
     */
    public function setF063freason2($f063freason2)
    {
        $this->f063freason2 = $f063freason2;

        return $this;
    }

    /**
     * Get f063freason2
     *
     * @return string
     */
    public function getF063freason2()
    {
        return $this->f063freason2;
    }

    /**
     * Set f063freason3
     *
     * @param string $f063freason3
     *
     * @return T063fapplicationDetails
     */
    public function setF063freason3($f063freason3)
    {
        $this->f063freason3 = $f063freason3;

        return $this;
    }

    /**
     * Get f063freason3
     *
     * @return string
     */
    public function getF063freason3()
    {
        return $this->f063freason3;
    }
    

    /**
     * Set f063fapproved1By
     *
     * @param integer $f063fapproved1By
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved1By($f063fapproved1By)
    {
        $this->f063fapproved1By = $f063fapproved1By;

        return $this;
    }

    /**
     * Get f063fapproved1By
     *
     * @return integer
     */
    public function getF063fapproved1By()
    {
        return $this->f063fapproved1By;
    }

    /**
     * Set f063fapproved2By
     *
     * @param integer $f063fapproved2By
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved2By($f063fapproved2By)
    {
        $this->f063fapproved2By = $f063fapproved2By;

        return $this;
    }

    /**
     * Get f063fapproved2By
     *
     * @return integer
     */
    public function getF063fapproved2By()
    {
        return $this->f063fapproved2By;
    }

    /**
     * Set f063fapproved3By
     *
     * @param integer $f063fapproved3By
     *
     * @return T063fapplicationDetails
     */
    public function setF063fapproved3By($f063fapproved3By)
    {
        $this->f063fapproved3By = $f063fapproved3By;

        return $this;
    }

    /**
     * Get f063fapproved3By
     *
     * @return integer
     */
    public function getF063fapproved3By()
    {
        return $this->f063fapproved3By;
    }


    /**
     * Set f063fbankId
     *
     * @param integer $f063fbankId
     *
     * @return T063fapplicationDetails
     */
    public function setF063fbankId($f063fbankId)
    {
        $this->f063fbankId = $f063fbankId;

        return $this;
    }

    /**
     * Get f063fbankId
     *
     * @return integer
     */
    public function getF063fbankId()
    {
        return $this->f063fbankId;
    }



    /**
     * Set f063fbankBatch
     *
     * @param string $f063fbankBatch
     *
     * @return T063fapplicationDetails
     */
    public function setF063fbankBatch($f063fbankBatch)
    {
        $this->f063fbankBatch = $f063fbankBatch;

        return $this;
    }

    /**
     * Get f063fbankBatch
     *
     * @return string
     */
    public function getF063fbankBatch()
    {
        return $this->f063fbankBatch;
    }

    /**
     * Get f063fallApproved
     *
     * @return integer
     */
    public function getF063fallApproved()
    {
        return $this->f063fallApproved;
    }

    /**
     * Set f063fallApproved
     *
     * @param integer $f063finstitutionId
     *
     * @return T063fapplicationDetails
     */
    public function setF063fallApproved($f063fallApproved)
    {
        $this->f063fallApproved = $f063fallApproved;

        return $this;
    }

    /**
     * Set f063flevel1
     *
     * @param integer $f063flevel1
     *
     * @return T063fapplicationDetails
     */
    public function setF063flevel1($f063flevel1)
    {
        $this->f063flevel1 = $f063flevel1;

        return $this;
    }

    /**
     * Get f063flevel1
     *
     * @return integer
     */
    public function getF063flevel1()
    {
        return $this->f063flevel1;
    }

    /**
     * Set f063flevel2
     *
     * @param integer $f063flevel2
     *
     * @return T063fapplicationDetails
     */
    public function setF063flevel2($f063flevel2)
    {
        $this->f063flevel2 = $f063flevel2;

        return $this;
    }

    /**
     * Get f063flevel2
     *
     * @return integer
     */
    public function getF063flevel2()
    {
        return $this->f063flevel2;
    }

     /**
     * Set f063flevel3
     *
     * @param integer $f063flevel3
     *
     * @return T063fapplicationDetails
     */
    public function setF063flevel3($f063flevel3)
    {
        $this->f063flevel3 = $f063flevel3;

        return $this;
    }

    /**
     * Get f063flevel3
     *
     * @return integer
     */
    public function getF063flevel3()
    {
        return $this->f063flevel3;
    }

    /**
     * Set f063ffinalStatus
     *
     * @param integer $f063ffinalStatus
     *
     * @return T063fapplicationDetails
     */
    public function setF063ffinalStatus($f063ffinalStatus)
    {
        $this->f063ffinalStatus = $f063ffinalStatus;

        return $this;
    }

    /**
     * Get f063ffinalStatus
     *
     * @return integer
     */
    public function getF063ffinalStatus()
    {
        return $this->f063ffinalStatus;
    }
    

    /**
     * Set f063ffinalReason
     *
     * @param integer $f063ffinalReason
     *
     * @return T063fapplicationDetails
     */
    public function setF063ffinalReason($f063ffinalReason)
    {
        $this->f063ffinalReason = $f063ffinalReason;

        return $this;
    }

    /**
     * Get f063ffinalReason
     *
     * @return integer
     */
    public function getF063ffinalReason()
    {
        return $this->f063ffinalReason;
    }

    
}
