<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T049fcreditCardTerminal
 *
 * @ORM\Table(name="t049fcredit_card_terminal")
 * @ORM\Entity(repositoryClass="Application\Repository\CreditCardTerminalRepository")
 */
class T049fcreditCardTerminal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f049fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f049fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f049fterminal_no", type="string", length=20, nullable=true)
     */
    private $f049fterminalNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f049fid_department", type="string", length=60, nullable=true)
     */
    private $f049fidDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f049fbank_card_type", type="string", length=25, nullable=true)
     */
    private $f049fbankCardType;


    /**
     * @var integer
     *
     * @ORM\Column(name="f049fbank", type="integer", nullable=true)
     */
    private $f049fbank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f049fstatus", type="integer", nullable=true)
     */
    private $f049fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f049fcreated_by", type="integer", nullable=true)
     */
    private $f049fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f049fupdated_by", type="integer", nullable=true)
     */
    private $f049fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f049fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f049fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f049fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f049fupdatedDtTm;



    /**
     * Get f049fid
     *
     * @return integer
     */
    public function getF049fid()
    {
        return $this->f049fid;
    }

    /**
     * Set f049fterminalNo
     *
     * @param string $f049fterminalNo
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fterminalNo($f049fterminalNo)
    {
        $this->f049fterminalNo = $f049fterminalNo;

        return $this;
    }

    /**
     * Get f049fterminalNo
     *
     * @return string
     */
    public function getF049fterminalNo()
    {
        return $this->f049fterminalNo;
    }

    /**
     * Set f049fidDepartment
     *
     * @param integer $f049fidDepartment
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fidDepartment($f049fidDepartment)
    {
        $this->f049fidDepartment = $f049fidDepartment;

        return $this;
    }

    /**
     * Get f049fidDepartment
     *
     * @return integer
     */
    public function getF049fidDepartment()
    {
        return $this->f049fidDepartment;
    }

    /**
     * Set f049fbankCardType
     *
     * @param string $f049fbankCardType
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fbankCardType($f049fbankCardType)
    {
        $this->f049fbankCardType = $f049fbankCardType;

        return $this;
    }

    /**
     * Get f049fbankCardType
     *
     * @return string
     */
    public function getF049fbankCardType()
    {
        return $this->f049fbankCardType;
    }

    /**
     * Set f049fbank
     *
     * @param integer $f049fbank
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fbank($f049fbank)
    {
        $this->f049fbank = $f049fbank;

        return $this;
    }

    /**
     * Get f049fbank
     *
     * @return integer
     */
    public function getF049fbank()
    {
        return $this->f049fbank;
    }


    /**
     * Set f049fstatus
     *
     * @param integer $f049fstatus
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fstatus($f049fstatus)
    {
        $this->f049fstatus = $f049fstatus;

        return $this;
    }

    /**
     * Get f049fstatus
     *
     * @return integer
     */
    public function getF049fstatus()
    {
        return $this->f049fstatus;
    }

    /**
     * Set f049fcreatedBy
     *
     * @param integer $f049fcreatedBy
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fcreatedBy($f049fcreatedBy)
    {
        $this->f049fcreatedBy = $f049fcreatedBy;

        return $this;
    }

    /**
     * Get f049fcreatedBy
     *
     * @return integer
     */
    public function getF049fcreatedBy()
    {
        return $this->f049fcreatedBy;
    }

    /**
     * Set f049fupdatedBy
     *
     * @param integer $f049fupdatedBy
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fupdatedBy($f049fupdatedBy)
    {
        $this->f049fupdatedBy = $f049fupdatedBy;

        return $this;
    }

    /**
     * Get f049fupdatedBy
     *
     * @return integer
     */
    public function getF049fupdatedBy()
    {
        return $this->f049fupdatedBy;
    }

    /**
     * Set f049fcreatedDtTm
     *
     * @param \DateTime $f049fcreatedDtTm
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fcreatedDtTm($f049fcreatedDtTm)
    {
        $this->f049fcreatedDtTm = $f049fcreatedDtTm;

        return $this;
    }

    /**
     * Get f049fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF049fcreatedDtTm()
    {
        return $this->f049fcreatedDtTm;
    }

    /**
     * Set f049fupdatedDtTm
     *
     * @param \DateTime $f049fupdatedDtTm
     *
     * @return T049fcreditCardTerminal
     */
    public function setF049fupdatedDtTm($f049fupdatedDtTm)
    {
        $this->f049fupdatedDtTm = $f049fupdatedDtTm;

        return $this;
    }

    /**
     * Get f049fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF049fupdatedDtTm()
    {
        return $this->f049fupdatedDtTm;
    }
}
