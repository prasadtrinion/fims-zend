<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T030epfCategory
 *
 * @ORM\Table(name="t030epf_category")
 *  @ORM\Entity(repositoryClass="Application\Repository\EpfCategoryRepository")
 */
class T030epfCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f030fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f030fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fcategory_name", type="string", length=50, nullable=false)
     */
    private $f030fcategoryName;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fdiscription", type="string", length=50, nullable=false)
     */
    private $f030fdiscription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fstatus", type="integer", nullable=false)
     */
    private $f030fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fcreated_by", type="integer", nullable=false)
     */
    private $f030fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fupdated_by", type="integer", nullable=false)
     */
    private $f030fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f030fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f030fupdatedDtTm;



    /**
     * Get f030fid
     *
     * @return integer
     */
    public function getF030fid()
    {
        return $this->f030fid;
    }

    /**
     * Set f030fcategoryName
     *
     * @param string $f030fcategoryName
     *
     * @return T030epfCategory
     */
    public function setF030fcategoryName($f030fcategoryName)
    {
        $this->f030fcategoryName = $f030fcategoryName;

        return $this;
    }

    /**
     * Get f030fcategoryName
     *
     * @return string
     */
    public function getF030fcategoryName()
    {
        return $this->f030fcategoryName;
    }

    /**
     * Set f030fdiscription
     *
     * @param string $f030fdiscription
     *
     * @return T030epfCategory
     */
    public function setF030fdiscription($f030fdiscription)
    {
        $this->f030fdiscription = $f030fdiscription;

        return $this;
    }

    /**
     * Get f030fdiscription
     *
     * @return string
     */
    public function getF030fdiscription()
    {
        return $this->f030fdiscription;
    }

    /**
     * Set f030fstatus
     *
     * @param integer $f030fstatus
     *
     * @return T030epfCategory
     */
    public function setF030fstatus($f030fstatus)
    {
        $this->f030fstatus = $f030fstatus;

        return $this;
    }

    /**
     * Get f030fstatus
     *
     * @return integer
     */
    public function getF030fstatus()
    {
        return $this->f030fstatus;
    }

    /**
     * Set f030fcreatedBy
     *
     * @param integer $f030fcreatedBy
     *
     * @return T030epfCategory
     */
    public function setF030fcreatedBy($f030fcreatedBy)
    {
        $this->f030fcreatedBy = $f030fcreatedBy;

        return $this;
    }

    /**
     * Get f030fcreatedBy
     *
     * @return integer
     */
    public function getF030fcreatedBy()
    {
        return $this->f030fcreatedBy;
    }

    /**
     * Set f030fupdatedBy
     *
     * @param integer $f030fupdatedBy
     *
     * @return T030epfCategory
     */
    public function setF030fupdatedBy($f030fupdatedBy)
    {
        $this->f030fupdatedBy = $f030fupdatedBy;

        return $this;
    }

    /**
     * Get f030fupdatedBy
     *
     * @return integer
     */
    public function getF030fupdatedBy()
    {
        return $this->f030fupdatedBy;
    }

    /**
     * Set f030fcreatedDtTm
     *
     * @param \DateTime $f030fcreatedDtTm
     *
     * @return T030epfCategory
     */
    public function setF030fcreatedDtTm($f030fcreatedDtTm)
    {
        $this->f030fcreatedDtTm = $f030fcreatedDtTm;

        return $this;
    }

    /**
     * Get f030fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fcreatedDtTm()
    {
        return $this->f030fcreatedDtTm;
    }

    /**
     * Set f030fupdatedDtTm
     *
     * @param \DateTime $f030fupdatedDtTm
     *
     * @return T030epfCategory
     */
    public function setF030fupdatedDtTm($f030fupdatedDtTm)
    {
        $this->f030fupdatedDtTm = $f030fupdatedDtTm;

        return $this;
    }

    /**
     * Get f030fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fupdatedDtTm()
    {
        return $this->f030fupdatedDtTm;
    }
}
