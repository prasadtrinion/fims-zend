<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
  
/**
 * T120ffeeTypes
 *
 * @ORM\Table(name="t120ffee_type")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeTypeRepository")
 */
class T120ffeeType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f120fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f120fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f120fname", type="string",length=50, nullable=true)
     */
    private $f120fname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f120fcode", type="string",length=20, nullable=true)
     */
    private $f120fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f120fid_category", type="integer", nullable=true)
     */
    private $f120fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f120fstatus", type="integer", nullable=true)
     */
    private $f120fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f120fcreated_by", type="integer", nullable=true)
     */
    private $f120fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f120fupdated_by", type="integer", nullable=true)
     */
    private $f120fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f120fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f120fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f120fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f120fupdatedDtTm;


    /**
     * Get f120fid
     *
     * @return integer
     */
    public function getF120fid()
    {
        return $this->f120fid;
    }

    /**
     * Set f120fname
     *
     * @param string $f120fname
     *
     * @return T120ffeeType
     */
    public function setF120fname($f120fname)
    {
        $this->f120fname = $f120fname;

        return $this;
    }

    /**
     * Get f120fname
     *
     * @return string
     */
    public function getF120fname()
    {
        return $this->f120fname;
    }

    /**
     * Set f120fcode
     *
     * @param string $f120fcode
     *
     * @return T120ffeeType
     */
    public function setF120fcode($f120fcode)
    {
        
        $this->f120fcode = $f120fcode;

        return $this;
    }

    /**
     * Get f120fcode
     *
     * @return string
     */
    public function getF120fcode()
    {
        return $this->f120fcode;
    }

    /**
     * Set f120fidCategory
     *
     * @param integer $f120fidCategory
     *
     * @return T120ffeeType
     */
    public function setF120fidCategory($f120fidCategory)
    {
        $this->f120fidCategory = $f120fidCategory;

        return $this;
    }

    /**
     * Get f120fidCategory
     *
     * @return integer
     */
    public function getF120fidCategory()
    {
        return $this->f120fidCategory;
    }


    /**
     * Set f120fstatus
     *
     * @param integer $f120fstatus
     *
     * @return T120ffeeType
     */
    public function setF120fstatus($f120fstatus)
    {
        $this->f120fstatus = $f120fstatus;

        return $this;
    }

    /**
     * Get f120fstatus
     *
     * @return integer
     */
    public function getF120fstatus()
    {
        return $this->f120fstatus;
    }


    /**
     * Set f120fcreatedBy
     *
     * @param integer $f120fcreatedBy
     *
     * @return T120ffeeType
     */
    public function setF120fcreatedBy($f120fcreatedBy)
    {
        $this->f120fcreatedBy = $f120fcreatedBy;

        return $this;
    }

    /**
     * Get f120fcreatedBy
     *
     * @return integer
     */
    public function getF120fcreatedBy()
    {
        return $this->f120fcreatedBy;
    }

    /**
     * Set f120fupdatedBy
     *
     * @param integer $f120fupdatedBy
     *
     * @return T120ffeeType
     */
    public function setF120fupdatedBy($f120fupdatedBy)
    {
        $this->f120fupdatedBy = $f120fupdatedBy;

        return $this;
    }

    /**
     * Get f120fupdatedBy
     *
     * @return integer
     */
    public function getF120fupdatedBy()
    {
        return $this->f120fupdatedBy;
    }

    /**
     * Set f120fcreatedDtTm
     *
     * @param \DateTime $f120fcreatedDtTm
     *
     * @return T120ffeeType
     */
    public function setF120fcreatedDtTm($f120fcreatedDtTm)
    {
        $this->f120fcreatedDtTm = $f120fcreatedDtTm;

        return $this;
    }

    /**
     * Get f120fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF120fcreatedDtTm()
    {
        return $this->f120fcreatedDtTm;
    }

    /**
     * Set f120fupdatedDtTm
     *
     * @param \DateTime $f120fupdatedDtTm
     *
     * @return T120ffeeType
     */
    public function setF120fupdatedDtTm($f120fupdatedDtTm)
    {
        $this->f120fupdatedDtTm = $f120fupdatedDtTm;

        return $this;
    }

    /**
     * Get f120fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF120fupdatedDtTm()
    {
        return $this->f120fupdatedDtTm;
    }
}
