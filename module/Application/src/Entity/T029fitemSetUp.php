<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T029fitemSetUp
 *
 * @ORM\Table(name="t029fitem_set_up", uniqueConstraints={@ORM\UniqueConstraint(name="f029fitem_name_UNIQUE", columns={"f029fitem_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ItemSetUpRepository")
 */
class T029fitemSetUp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f029fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f029fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f029fitem_name", type="string", length=50, nullable=true)
     */
    private $f029fitemName;

    /**
     * @var string
     *
     * @ORM\Column(name="f029funit", type="string", length=10, nullable=true)
     */
    private $f029funit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f029fid_category", type="integer", nullable=true)
     */
    private $f029fidCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f029faccount_code", type="string", length=25, nullable=true)
     */
    private $f029faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f029fitem_code", type="string", length=25, nullable=true)
     */
    private $f029fitemCode;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="f029fid_tax", type="integer", nullable=true)
     */
    private $f029fidTax;


    /**
     * @var integer
     *
     * @ORM\Column(name="f029fid_sub_category", type="integer", nullable=true)
     */
    private $f029fidSubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f029fstatus", type="integer", nullable=true)
     */
    private $f029fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f029fcreated_by", type="integer", nullable=true)
     */
    private $f029fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f029fupdated_by", type="integer", nullable=true)
     */
    private $f029fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f029fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f029fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f029fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f029fupdatedDtTm;



    /**
     * Get f029fid
     *
     * @return integer
     */
    public function getF029fid()
    {
        return $this->f029fid;
    }

    /**
     * Set f029fitemName
     *
     * @param string $f029fitemName
     *
     * @return T029fitemSetUp
     */
    public function setF029fitemName($f029fitemName)
    {
        $this->f029fitemName = $f029fitemName;

        return $this;
    }

    /**
     * Get f029fitemName
     *
     * @return string
     */
    public function getF029fitemName()
    {
        return $this->f029fitemName;
    }

    /**
     * Set f029funit
     *
     * @param string $f029funit
     *
     * @return T029fitemSetUp
     */
    public function setF029funit($f029funit)
    {
        $this->f029funit = $f029funit;

        return $this;
    }

    /**
     * Get f029funit
     *
     * @return string
     */
    public function getF029funit()
    {
        return $this->f029funit;
    }

    

    /**
     * Set f029fidCategory
     *
     * @param integer $f029fidCategory
     *
     * @return T029fitemSetUp
     */
    public function setF029fidCategory($f029fidCategory)
    {
        $this->f029fidCategory = $f029fidCategory;

        return $this;
    }

    /**
     * Get f029fidCategory
     *
     * @return integer
     */
    public function getF029fidCategory()
    {
        return $this->f029fidCategory;
    }

    /**
     * Set f029faccountCode
     *
     * @param string $f029faccountCode
     *
     * @return T029fitemSetUp
     */
    public function setF029faccountCode($f029faccountCode)
    {
        $this->f029faccountCode = $f029faccountCode;

        return $this;
    }

    /**
     * Get f029faccountCode
     *
     * @return string
     */
    public function getF029faccountCode()
    {
        return $this->f029faccountCode;
    }

    /**
     * Set f029fidTax
     *
     * @param integer $f029fidTax
     *
     * @return T029fitemSetUp
     */
    public function setF029fidTax($f029fidTax)
    {
        $this->f029fidTax = $f029fidTax;

        return $this;
    }

    /**
     * Get f029fidTax
     *
     * @return integer
     */
    public function getF029fidTax()
    {
        return $this->f029fidTax;
    }


    
    /**
     * Set f029fitemCode
     *
     * @param integer $f029fitemCode
     *
     * @return T029fitemSetUp
     */
    public function setF029fitemCode($f029fitemCode)
    {
        $this->f029fitemCode = $f029fitemCode;

        return $this;
    }

    /**
     * Get f029fitemCode
     *
     * @return integer
     */
    public function getF029fitemCode()
    {
        return $this->f029fitemCode;
    }

    /**
     * Set f029fidSubCategory
     *
     * @param integer $f029fidSubCategory
     *
     * @return T029fitemSetUp
     */
    public function setF029fidSubCategory($f029fidSubCategory)
    {
        $this->f029fidSubCategory = $f029fidSubCategory;

        return $this;
    }

    /**
     * Get f029fidSubCategory
     *
     * @return integer
     */
    public function getF029fidSubCategory()
    {
        return $this->f029fidSubCategory;
    }

    /**
     * Set f029fstatus
     *
     * @param integer $f029fstatus
     *
     * @return T029fitemSetUp
     */
    public function setF029fstatus($f029fstatus)
    {
        $this->f029fstatus = $f029fstatus;

        return $this;
    }

    /**
     * Get f029fstatus
     *
     * @return integer
     */
    public function getF029fstatus()
    {
        return $this->f029fstatus;
    }

    /**
     * Set f029fcreatedBy
     *
     * @param integer $f029fcreatedBy
     *
     * @return T029fitemSetUp
     */
    public function setF029fcreatedBy($f029fcreatedBy)
    {
        $this->f029fcreatedBy = $f029fcreatedBy;

        return $this;
    }

    /**
     * Get f029fcreatedBy
     *
     * @return integer
     */
    public function getF029fcreatedBy()
    {
        return $this->f029fcreatedBy;
    }

    /**
     * Set f029fupdatedBy
     *
     * @param integer $f029fupdatedBy
     *
     * @return T029fitemSetUp
     */
    public function setF029fupdatedBy($f029fupdatedBy)
    {
        $this->f029fupdatedBy = $f029fupdatedBy;

        return $this;
    }

    /**
     * Get f029fupdatedBy
     *
     * @return integer
     */
    public function getF029fupdatedBy()
    {
        return $this->f029fupdatedBy;
    }

    /**
     * Set f029fcreatedDtTm
     *
     * @param \DateTime $f029fcreatedDtTm
     *
     * @return T029fitemSetUp
     */
    public function setF029fcreatedDtTm($f029fcreatedDtTm)
    {
        $this->f029fcreatedDtTm = $f029fcreatedDtTm;

        return $this;
    }

    /**
     * Get f029fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF029fcreatedDtTm()
    {
        return $this->f029fcreatedDtTm;
    }

    /**
     * Set f029fupdatedDtTm
     *
     * @param \DateTime $f029fupdatedDtTm
     *
     * @return T029fitemSetUp
     */
    public function setF029fupdatedDtTm($f029fupdatedDtTm)
    {
        $this->f029fupdatedDtTm = $f029fupdatedDtTm;

        return $this;
    }

    /**
     * Get f029fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF029fupdatedDtTm()
    {
        return $this->f029fupdatedDtTm;
    }
}
