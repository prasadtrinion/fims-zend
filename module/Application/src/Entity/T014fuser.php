<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T014fuser
 *
 * @ORM\Table(name="t014fuser")
 * @ORM\Entity(repositoryClass="Application\Repository\T014fuserRepository")
 */
class T014fuser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f014fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f014fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f014fuser_name", type="string", length=30, nullable=false)
     */
    private $f014fuserName;


    /**
     * @var string
     *
     * @ORM\Column(name="f014femail", type="string", length=30, nullable=false)
     */
    private $f014femail;


    /**
     * @var string
     *
     * @ORM\Column(name="f014ftoken", type="string", length=100, nullable=false)
     */
    private $f014ftoken;


    /**
     * @var string
     *
     * @ORM\Column(name="f014fpassword", type="string", length=150, nullable=false)
     */
    private $f014fpassword;


    /**
     * @var integer
     *
     * @ORM\Column(name="f014fid_role", type="integer", nullable=false)
     */
    private $f014fidRole;

     /**
     * @var integer
     *
     * @ORM\Column(name="f014fid_staff", type="integer", nullable=true)
     */
    private $f014fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fstatus", type="integer", nullable=false)
     */
    private $f014fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fcreated_by", type="integer", nullable=false)
     */
    private $f014fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fupdated_by", type="integer", nullable=false)
     */
    private $f014fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f014fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f014fupdatedDtTm;



    /**
     * Get f014fid
     *
     * @return integer
     */
    public function getF014fid()
    {
        return $this->f014fid;
    }

    /**
     * Set f014fstatus
     *
     * @param string $f014fuserName
     *
     * @return T014fuser
     */
    public function setF014fuserName($f014fuserName)
    {
        $this->f014fuserName = $f014fuserName;

        return $this;
    }

    /**
     * Get f014fuserName
     *
     * @return string
     */
    public function getF014fuserName()
    {
        return $this->f014fuserName;
    }


    /**
     * Set f014femail
     *
     * @param string $f014femail
     *
     * @return T014fuser
     */
    public function setF014femail($f014femail)
    {
        $this->f014femail = $f014femail;

        return $this;
    }

    /**
     * Get f014femail
     *
     * @return string
     */
    public function getF014femail()
    {
        return $this->f014femail;
    }


    /**
     * Set f014ftoken
     *
     * @param string $f014ftoken
     *
     * @return T014fuser
     */
    public function setF014ftoken($f014ftoken)
    {
        $this->f014ftoken = $f014ftoken;

        return $this;
    }

    /**
     * Get f014ftoken
     *
     * @return string
     */
    public function getF014ftoken()
    {
        return $this->f014ftoken;
    }

    /**
     * Set f014fpassword
     *
     * @param string $f014fpassword
     *
     * @return T014fuser
     */
    public function setF014fpassword($f014fpassword)
    {
        $this->f014fpassword = $f014fpassword;

        return $this;
    }

    /**
     * Get f014fpassword
     *
     * @return string
     */
    public function getF014fpassword()
    {
        return $this->f014fpassword;
    }

    /**
     * Set f014fidRole
     *
     * @param integer $f014fidRole
     *
     * @return T014fuser
     */
    public function setF014fidRole($f014fidRole)
    {
        $this->f014fidRole = $f014fidRole;

        return $this;
    }

    /**
     * Get f014fidRole
     *
     * @return integer
     */
    public function getF014fidRole()
    {
        return $this->f014fidRole;
    }

     /**
     * Set f014fidStaff
     *
     * @param integer $f014fidStaff
     *
     * @return T014fuser
     */
    public function setF014fidStaff($f014fidStaff)
    {
        $this->f014fidStaff = $f014fidStaff;

        return $this;
    }

    /**
     * Get f014fidStaff
     *
     * @return integer
     */
    public function getF014fidStaff()
    {
        return $this->f014fidStaff;
    }

    /**
     * Set f014fstatus
     *
     * @param integer $f014fstatus
     *
     * @return T014fuser
     */
    public function setF014fstatus($f014fstatus)
    {
        $this->f014fstatus = $f014fstatus;

        return $this;
    }

    /**
     * Get f014fstatus
     *
     * @return integer
     */
    public function getF014fstatus()
    {
        return $this->f014fstatus;
    }

    /**
     * Set f014fcreatedBy
     *
     * @param integer $f014fcreatedBy
     *
     * @return T014fuser
     */
    public function setF014fcreatedBy($f014fcreatedBy)
    {
        $this->f014fcreatedBy = $f014fcreatedBy;

        return $this;
    }

    /**
     * Get f014fcreatedBy
     *
     * @return integer
     */
    public function getF014fcreatedBy()
    {
        return $this->f014fcreatedBy;
    }

    /**
     * Set f014fupdatedBy
     *
     * @param integer $f014fupdatedBy
     *
     * @return T014fuser
     */
    public function setF014fupdatedBy($f014fupdatedBy)
    {
        $this->f014fupdatedBy = $f014fupdatedBy;

        return $this;
    }

    /**
     * Get f014fupdatedBy
     *
     * @return integer
     */
    public function getF014fupdatedBy()
    {
        return $this->f014fupdatedBy;
    }

    /**
     * Set f014fcreatedDtTm
     *
     * @param \DateTime $f014fcreatedDtTm
     *
     * @return T014fuser
     */
    public function setF014fcreatedDtTm($f014fcreatedDtTm)
    {
        $this->f014fcreatedDtTm = $f014fcreatedDtTm;

        return $this;
    }

    /**
     * Get f014fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF014fcreatedDtTm()
    {
        return $this->f014fcreatedDtTm;
    }

    /**
     * Set f014fupdatedDtTm
     *
     * @param \DateTime $f014fupdatedDtTm
     *
     * @return T014fuser
     */
    public function setF014fupdatedDtTm($f014fupdatedDtTm)
    {
        $this->f014fupdatedDtTm = $f014fupdatedDtTm;

        return $this;
    }

    /**
     * Get f014fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF014fupdatedDtTm()
    {
        return $this->f014fupdatedDtTm;
    }
}
