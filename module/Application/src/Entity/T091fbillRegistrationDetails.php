<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T091fbillRegistrationDetails
 *
 * @ORM\Table(name="t091fbill_registration_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BillRegistrationRepository")
 */
class T091fbillRegistrationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fidDetails;

     /**
     * @var string
     *
     * @ORM\Column(name="f091freference_number", type="string", length=50, nullable=true)
     */
    private $f091freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_bill", type="integer", nullable=true)
     */
    private $f091fidBill;

     /**
     * @var string
     *
     * @ORM\Column(name="f091ftype", type="string",length=20, nullable=true)
     */
    private $f091ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_item", type="integer", nullable=true)
     */
    private $f091fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fso_code", type="string", length=15, nullable=true)
     */
    private $f091fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fquantity", type="integer", nullable=true)
     */
    private $f091fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fquantity_received", type="integer", nullable=true)
     */
    private $f091fquantityReceived = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="f091funit", type="string", length=15, nullable=true)
     */
    private $f091funit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fprice", type="integer", nullable=true)
     */
    private $f091fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091total", type="integer", nullable=true)
     */
    private $f091total;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091ftax_code", type="integer", nullable=true)
     */
    private $f091ftaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fpercentage", type="integer", nullable=true)
     */
    private $f091fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091ftax_amount", type="integer", nullable=true)
     */
    private $f091ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091ftotal_inc_tax", type="integer", nullable=true)
     */
    private $f091ftotalIncTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=true)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=true)
     */
    private $f091fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=true)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f091fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f091ffund_code", type="string",length=20, nullable=true)
     */
    private $f091ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f091fdepartment_code", type="string",length=20, nullable=true)
     */
    private $f091fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091factivity_code", type="string",length=20, nullable=true)
     */
    private $f091factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091faccount_code", type="string",length=20, nullable=true)
     */
    private $f091faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fbudget_fund_code", type="string",length=20, nullable=true)
     */
    private $f091fbudgetFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f091fbudget_department_code", type="string",length=20, nullable=true)
     */
    private $f091fbudgetDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fbudget_activity_code", type="string",length=20, nullable=true)
     */
    private $f091fbudgetActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fbudget_account_code", type="string",length=20, nullable=true)
     */
    private $f091fbudgetAccountCode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_grn_details", type="integer", nullable=true)
     */
    private $f091fidGrnDetails;

    


    /**
     * Get f091fidDetails
     *
     * @return integer
     */
    public function getF091fidDetails()
    {
        return $this->f091fidDetails;
    }

    /**
     * Set f091ftype
     *
     * @param integer $f091ftype
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091ftype($f091ftype)
    {
        $this->f091ftype = $f091ftype;

        return $this;
    }

    /**
     * Get f091ftype
     *
     * @return integer
     */
    public function getF091ftype()
    {
        return $this->f091ftype;
    }

     /**
     * Set f091fidBill
     *
     * @param integer $f091fidBill
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fidBill($f091fidBill)
    {
        $this->f091fidBill = $f091fidBill;

        return $this;
    }

    /**
     * Get f091fidBill
     *
     * @return integer
     */
    public function getF091fidBill()
    {
        return $this->f091fidBill;
    }

    /**
     * Set f091fidItem
     *
     * @param integer $f091fidItem
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fidItem($f091fidItem)
    {
        $this->f091fidItem = $f091fidItem;

        return $this;
    }

    /**
     * Get f091fidItem
     *
     * @return integer
     */
    public function getF091fidItem()
    {
        return $this->f091fidItem;
    }

    /**
     * Set f091fsoCode
     *
     * @param string $f091fsoCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fsoCode($f091fsoCode)
    {
        $this->f091fsoCode = $f091fsoCode;

        return $this;
    }

    /**
     * Get f091fsoCode
     *
     * @return string
     */
    public function getF091fsoCode()
    {
        return $this->f091fsoCode;
    }


    /**
     * Set f091fquantity
     *
     * @param integer $f091fquantity
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fquantity($f091fquantity)
    {
        $this->f091fquantity = $f091fquantity;

        return $this;
    }

    /**
     * Get f091fquantity
     *
     * @return integer
     */
    public function getF091fquantity()
    {
        return $this->f091fquantity;
    }

    /**
     * Set f091fquantityReceived
     *
     * @param integer $f091fquantityReceived
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fquantityReceived($f091fquantityReceived)
    {
        $this->f091fquantityReceived = $f091fquantityReceived;

        return $this;
    }

    /**
     * Get f091fquantityReceived
     *
     * @return integer
     */
    public function getF091fquantityReceived()
    {
        return $this->f091fquantityReceived;
    }

    /**
     * Set f091funit
     *
     * @param string $f091funit
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091funit($f091funit)
    {
        $this->f091funit = $f091funit;

        return $this;
    }

    /**
     * Get f091funit
     *
     * @return string
     */
    public function getF091funit()
    {
        return $this->f091funit;
    }

    /**
     * Set f091fprice
     *
     * @param integer $f091fprice
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fprice($f091fprice)
    {
        $this->f091fprice = $f091fprice;

        return $this;
    }

    /**
     * Get f091fprice
     *
     * @return integer
     */
    public function getF091fprice()
    {
        return $this->f091fprice;
    }

    /**
     * Set f091total
     *
     * @param integer $f091total
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091total($f091total)
    {
        $this->f091total = $f091total;

        return $this;
    }

    /**
     * Get f091total
     *
     * @return integer
     */
    public function getF091total()
    {
        return $this->f091total;
    }

    /**
     * Set f091ftaxCode
     *
     * @param integer $f091ftaxCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091ftaxCode($f091ftaxCode)
    {
        $this->f091ftaxCode = $f091ftaxCode;

        return $this;
    }

    /**
     * Get f091ftaxCode
     *
     * @return integer
     */
    public function getF091ftaxCode()
    {
        return $this->f091ftaxCode;
    }

    /**
     * Set f091fpercentage
     *
     * @param integer $f091fpercentage
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fpercentage($f091fpercentage)
    {
        $this->f091fpercentage = $f091fpercentage;

        return $this;
    }

    /**
     * Get f091fpercentage
     *
     * @return integer
     */
    public function getF091fpercentage()
    {
        return $this->f091fpercentage;
    }

    /**
     * Set f091ftaxAmount
     *
     * @param integer $f091ftaxAmount
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091ftaxAmount($f091ftaxAmount)
    {
        $this->f091ftaxAmount = $f091ftaxAmount;

        return $this;
    }

    /**
     * Get f091ftaxAmount
     *
     * @return integer
     */
    public function getF091ftaxAmount()
    {
        return $this->f091ftaxAmount;
    }

    /**
     * Set f091ftotalIncTax
     *
     * @param integer $f091ftotalIncTax
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091ftotalIncTax($f091ftotalIncTax)
    {
        $this->f091ftotalIncTax = $f091ftotalIncTax;

        return $this;
    }

    /**
     * Get f091ftotalIncTax
     *
     * @return integer
     */
    public function getF091ftotalIncTax()
    {
        return $this->f091ftotalIncTax;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }

    /**
     * Set f091ffundCode
     *
     * @param string $f091ffundCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091ffundCode($f091ffundCode)
    {
        $this->f091ffundCode = $f091ffundCode;

        return $this;
    }

    /**
     * Get f091ffundCode
     *
     * @return string
     */
    public function getF091ffundCode()
    {
        return $this->f091ffundCode;
    }

    /**
     * Set f091factivityCode
     *
     * @param string $f091factivityCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091factivityCode($f091factivityCode)
    {
        $this->f091factivityCode = $f091factivityCode;

        return $this;
    }

    /**
     * Get f091factivityCode
     *
     * @return string
     */
    public function getF091factivityCode()
    {
        return $this->f091factivityCode;
    }

    /**
     * Set f091fdepartmentCode
     *
     * @param string $f091fdepartmentCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fdepartmentCode($f091fdepartmentCode)
    {
        $this->f091fdepartmentCode = $f091fdepartmentCode;

        return $this;
    }

    /**
     * Get f091fdepartmentCode
     *
     * @return string
     */
    public function getF091fdepartmentCode()
    {
        return $this->f091fdepartmentCode;
    }

    /**
     * Set f091faccountCode
     *
     * @param string $f091faccountCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091faccountCode($f091faccountCode)
    {
        $this->f091faccountCode = $f091faccountCode;

        return $this;
    }

    /**
     * Get f091faccountCode
     *
     * @return string
     */
    public function getF091faccountCode()
    {
        return $this->f091faccountCode;
    }

    /**
     * Set f091fbudgetFundCode
     *
     * @param string $f091fbudgetFundCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fbudgetFundCode($f091fbudgetFundCode)
    {
        $this->f091fbudgetFundCode = $f091fbudgetFundCode;

        return $this;
    }

    /**
     * Get f091fbudgetFundCode
     *
     * @return string
     */
    public function getF091fbudgetFundCode()
    {
        return $this->f091fbudgetFundCode;
    }

    /**
     * Set f091fbudgetActivityCode
     *
     * @param string $f091fbudgetActivityCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fbudgetActivityCode($f091fbudgetActivityCode)
    {
        $this->f091fbudgetActivityCode = $f091fbudgetActivityCode;

        return $this;
    }

    /**
     * Get f091fbudgetActivityCode
     *
     * @return string
     */
    public function getF091fbudgetActivityCode()
    {
        return $this->f091fbudgetActivityCode;
    }

    /**
     * Set f091fbudgetDepartmentCode
     *
     * @param string $f091fbudgetDepartmentCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fbudgetDepartmentCode($f091fbudgetDepartmentCode)
    {
        $this->f091fbudgetDepartmentCode = $f091fbudgetDepartmentCode;

        return $this;
    }

    /**
     * Get f091fbudgetDepartmentCode
     *
     * @return string
     */
    public function getF091fbudgetDepartmentCode()
    {
        return $this->f091fbudgetDepartmentCode;
    }

    /**
     * Set f091fbudgetAccountCode
     *
     * @param string $f091fbudgetAccountCode
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fbudgetAccountCode($f091fbudgetAccountCode)
    {
        $this->f091fbudgetAccountCode = $f091fbudgetAccountCode;

        return $this;
    }

    /**
     * Get f091fbudgetAccountCode
     *
     * @return string
     */
    public function getF091fbudgetAccountCode()
    {
        return $this->f091fbudgetAccountCode;
    }

    /**
     * Set f091freferenceNumber
     *
     * @param string $f091freferenceNumber
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091freferenceNumber($f091freferenceNumber)
    {
        $this->f091freferenceNumber = $f091freferenceNumber;

        return $this;
    }

    /**
     * Get f091freferenceNumber
     *
     * @return string
     */
    public function getF091freferenceNumber()
    {
        return $this->f091freferenceNumber;
    }

    /**
     * Set f091fidGrnDetails
     *
     * @param string $f091fidGrnDetails
     *
     * @return T091fbillRegistrationDetails
     */
    public function setF091fidGrnDetails($f091fidGrnDetails)
    {
        $this->f091fidGrnDetails = $f091fidGrnDetails;

        return $this;
    }

    /**
     * Get f091fidGrnDetails
     *
     * @return string
     */
    public function getF091fidGrnDetails()
    {
        return $this->f091fidGrnDetails;
    }
}

