<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T142floanProcessingFee
 *
 * @ORM\Table(name="t142floan_processing_fee", uniqueConstraints={})
 * @ORM\Entity(repositoryClass="Application\Repository\LoanProcessingFeeRepository")
 */
class T142floanProcessingFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f142fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f142fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f142fid_loan_type", type="integer", nullable=false)
     */
    private $f142fidLoanType;

    /**
     * @var string
     *
     * @ORM\Column(name="f142ffactor", type="string", length=200, nullable=false)
     */
    private $f142ffactor;

    /**
     * @var string
     *
     * @ORM\Column(name="f142feffective_date", type="datetime", nullable=false)
     */
    private $f142feffectiveDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f142famount", type="integer", nullable=false)
     */
    private $f142famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f142fstatus", type="integer", nullable=false)
     */
    private $f142fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f142fcreated_by", type="integer", nullable=false)
     */
    private $f142fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f142fupdated_by", type="integer", nullable=false)
     */
    private $f142fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f142fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f142fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f142fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f142fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f142ffee_type", type="integer", nullable=false)
     */
    private $f142ffeeType;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f142ffactor_amount", type="integer", nullable=false)
     */
    private $f142ffactorAmount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f142ffactor_detail", type="integer", nullable=false)
     */
    private $f142ffactorDetail;

    /**
     * @var string
     *
     * @ORM\Column(name="f142ffund", type="string", length=25, nullable=false)
     */
    private $f142ffund='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f142fdepartment", type="string", length=25, nullable=false)
     */
    private $f142fdepartment='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f142factivity", type="string", length=25, nullable=false)
     */
    private $f142factivity='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f142faccount", type="string", length=25, nullable=false)
     */
    private $f142faccount='NULL';


    /**
     * Get f142fid
     *
     * @return integer
     */
    public function getF142fid()
    {
        return $this->f142fid;
    }

    /**
     * Set f142fidLoanType
     *
     * @param string $f142fidLoanType
     *
     * @return T142floanProcessingFee
     */
    public function setF142fidLoanType($f142fidLoanType)
    {
        $this->f142fidLoanType = $f142fidLoanType;

        return $this;
    }

    /**
     * Get f142fidLoanType
     *
     * @return string
     */
    public function getF142fidLoanType()
    {
        return $this->f142fidLoanType;
    }

    /**
     * Set f142ffactor
     *
     * @param string $f142ffactor
     *
     * @return T142floanProcessingFee
     */
    public function setF142ffactor($f142ffactor)
    {
        $this->f142ffactor = $f142ffactor;

        return $this;
    }

    /**
     * Get f142ffactor
     *
     * @return string
     */
    public function getF142ffactor()
    {
        return $this->f142ffactor;
    }

      /**
     * Set f142famount
     *
     * @param string $f142famount
     *
     * @return T142floanProcessingFee
     */
    public function setF142famount($f142famount)
    {
        $this->f142famount = $f142famount;

        return $this;
    }

    /**
     * Get f142famount
     *
     * @return string
     */
    public function getF142famount()
    {
        return $this->f142famount;
    }


      /**
     * Set f142feffectiveDate
     *
     * @param string $f142feffectiveDate
     *
     * @return T142floanProcessingFee
     */
    public function setF142feffectiveDate($f142feffectiveDate)
    {
        $this->f142feffectiveDate = $f142feffectiveDate;

        return $this;
    }

    /**
     * Get f142feffectiveDate
     *
     * @return string
     */
    public function getF142feffectiveDate()
    {
        return $this->f142feffectiveDate;
    }

    

    /**
     * Set f142fcreatedBy
     *
     * @param integer $f142fcreatedBy
     *
     * @return T142floanProcessingFee
     */
    public function setF142fcreatedBy($f142fcreatedBy)
    {
        $this->f142fcreatedBy = $f142fcreatedBy;

        return $this;
    }

    /**
     * Get f142fcreatedBy
     *
     * @return integer
     */
    public function getF142fcreatedBy()
    {
        return $this->f142fcreatedBy;
    }

    /**
     * Set f142fupdatedBy
     *
     * @param integer $f142fupdatedBy
     *
     * @return T142floanProcessingFee
     */
    public function setF142fupdatedBy($f142fupdatedBy)
    {
        $this->f142fupdatedBy = $f142fupdatedBy;

        return $this;
    }

    /**
     * Get f142fupdatedBy
     *
     * @return integer
     */
    public function getF142fupdatedBy()
    {
        return $this->f142fupdatedBy;
    }

    /**
     * Set f142fcreatedDtTm
     *
     * @param \DateTime $f142fcreatedDtTm
     *
     * @return T142floanProcessingFee
     */
    public function setF142fcreatedDtTm($f142fcreatedDtTm)
    {
        $this->f142fcreatedDtTm = $f142fcreatedDtTm;

        return $this;
    }

    /**
     * Get f142fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF142fcreatedDtTm()
    {
        return $this->f142fcreatedDtTm;
    }

    /**
     * Set f142fupdatedDtTm
     *
     * @param \DateTime $f142fupdatedDtTm
     *
     * @return T142floanProcessingFee
     */
    public function setF142fupdatedDtTm($f142fupdatedDtTm)
    {
        $this->f142fupdatedDtTm = $f142fupdatedDtTm;

        return $this;
    }

    /**
     * Get f142fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF142fupdatedDtTm()
    {
        return $this->f142fupdatedDtTm;
    }

    /**
     * Set f142fstatus
     *
     * @param boolean $f142fstatus
     *
     * @return T142floanProcessingFee
     */
    public function setF142fstatus($f142fstatus)
    {
        $this->f142fstatus = $f142fstatus;

        return $this;
    }

    /**
     * Get f142fstatus
     *
     * @return boolean
     */
    public function getF142fstatus()
    {
        return $this->f142fstatus;
    }


    /**
     * Set f142ffeeType
     *
     * @param boolean $f142ffeeType
     *
     * @return T142floanProcessingFee
     */
    public function setF142ffeeType($f142ffeeType)
    {
        $this->f142ffeeType = $f142ffeeType;

        return $this;
    }

    /**
     * Get f142ffeeType
     *
     * @return boolean
     */
    public function getF142ffeeType()
    {
        return $this->f142ffeeType;
    }

    
    /**
     * Set f142ffactorAmount
     *
     * @param boolean $f142ffactorAmount
     *
     * @return T142floanProcessingFee
     */
    public function setF142ffactorAmount($f142ffactorAmount)
    {
        $this->f142ffactorAmount = $f142ffactorAmount;

        return $this;
    }

    /**
     * Get f142ffactorAmount
     *
     * @return boolean
     */
    public function getF142ffactorAmount()
    {
        return $this->f142ffactorAmount;
    }


    /**
     * Set f142ffactorDetail
     *
     * @param boolean $f142ffactorDetail
     *
     * @return T142floanProcessingFee
     */
    public function setF142ffactorDetail($f142ffactorDetail)
    {
        $this->f142ffactorDetail = $f142ffactorDetail;

        return $this;
    }

    /**
     * Get f142ffactorDetail
     *
     * @return boolean
     */
    public function getF142ffactorDetail()
    {
        return $this->f142ffactorDetail;
    }

    /**
     * Set f142ffund
     *
     * @param string $f142ffund
     *
     * @return T142fprocessingFee
     */
    public function setF142ffund($f142ffund)
    {
        $this->f142ffund = $f142ffund;

        return $this;
    }

    /**
     * Get f142ffund
     *
     * @return string
     */
    public function getF142ffund()
    {
        return $this->f142ffund;
    }

    /**
     * Set f142factivity
     *
     * @param string $f142factivity
     *
     * @return T142fprocessingFee
     */
    public function setF142factivity($f142factivity)
    {
        $this->f142factivity = $f142factivity;

        return $this;
    }

    /**
     * Get f142factivity
     *
     * @return string
     */
    public function getF142factivity()
    {
        return $this->f142factivity;
    }

    /**
     * Set f142fdepartment
     *
     * @param string $f142fdepartment
     *
     * @return T142fprocessingFee
     */
    public function setF142fdepartment($f142fdepartment)
    {
        $this->f142fdepartment = $f142fdepartment;

        return $this;
    }

    /**
     * Get f142fdepartment
     *
     * @return string
     */
    public function getF142fdepartment()
    {
        return $this->f142fdepartment;
    }

    /**
     * Set f142faccount
     *
     * @param string $f142faccount
     *
     * @return T142fprocessingFee
     */
    public function setF142faccount($f142faccount)
    {
        $this->f142faccount = $f142faccount;

        return $this;
    }

    /**
     * Get f142faccount
     *
     * @return string
     */
    public function getF142faccount()
    {
        return $this->f142faccount;
    }

}
