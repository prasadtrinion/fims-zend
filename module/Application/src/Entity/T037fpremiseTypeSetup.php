<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T037fpremiseTypeSetup
 *
 * @ORM\Table(name="t037fpremise_type_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\PremiseTypeSetupRepository")
 */
class T037fpremiseTypeSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f037fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f037fpremise_type", type="string", length=50, nullable=true)
     */
    private $f037fpremiseType;

        /**
     * @var string
     *
     * @ORM\Column(name="f037fcode", type="string", length=50, nullable=true)
     */
    private $f037fcode;

        /**
     * @var string
     *
     * @ORM\Column(name="f037fprefix_code", type="string", length=60, nullable=true)
     */
    private $f037fprefixCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fstatus", type="integer", nullable=false)
     */
    private $f037fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fcreated_by", type="integer", nullable=true)
     */
    private $f037fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fupdated_by", type="integer", nullable=true)
     */
    private $f037fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fupdatedDtTm;



    /**
     * Get f037fid
     *
     * @return integer
     */
    public function getF037fid()
    {
        return $this->f037fid;
    }

    /**
     * Set f037fpremiseType
     *
     * @param string $f037fpremiseType
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fpremiseType($f037fpremiseType)
    {
        $this->f037fpremiseType = $f037fpremiseType;

        return $this;
    }

     /**
     * Get f037fpremiseType
     *
     * @return string
     */
    public function getF037fpremiseType()
    {
        return $this->f037fpremiseType;
    }

    /**
     * Get f037fcode
     *
     * @return string
     */
    public function getF037fcode()
    {
        return $this->f037fcode;
    }

    /**
     * Set f037fcode
     *
     * @param string $f037fcode
     *
     * @return T037fcodeSetup
     */
    public function setF037fcode($f037fcode)
    {
        $this->f037fcode = $f037fcode;

        return $this;
    }

    /**
     * Get f037fprefixCode
     *
     * @return string
     */
    public function getF037fprefixCode()
    {
        return $this->f037fprefixCode;
    }

    /**
     * Set f037fprefixCode
     *
     * @param string $f037fprefixCode
     *
     * @return T037fcodeSetup
     */
    public function setF037fprefixCode($f037fprefixCode)
    {
        $this->f037fprefixCode = $f037fprefixCode;

        return $this;
    }

   

    /**
     * Set f037fstatus
     *
     * @param integer $f037fstatus
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fstatus($f037fstatus)
    {
        $this->f037fstatus = $f037fstatus;

        return $this;
    }

    /**
     * Get f037fstatus
     *
     * @return integer
     */
    public function getF037fstatus()
    {
        return $this->f037fstatus;
    }

    /**
     * Set f037fcreatedBy
     *
     * @param integer $f037fcreatedBy
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fcreatedBy($f037fcreatedBy)
    {
        $this->f037fcreatedBy = $f037fcreatedBy;

        return $this;
    }

    /**
     * Get f037fcreatedBy
     *
     * @return integer
     */
    public function getF037fcreatedBy()
    {
        return $this->f037fcreatedBy;
    }

    /**
     * Set f037fupdatedBy
     *
     * @param integer $f037fupdatedBy
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fupdatedBy($f037fupdatedBy)
    {
        $this->f037fupdatedBy = $f037fupdatedBy;

        return $this;
    }

    /**
     * Get f037fupdatedBy
     *
     * @return integer
     */
    public function getF037fupdatedBy()
    {
        return $this->f037fupdatedBy;
    }

    /**
     * Set f037fcreatedDtTm
     *
     * @param \DateTime $f037fcreatedDtTm
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fcreatedDtTm($f037fcreatedDtTm)
    {
        $this->f037fcreatedDtTm = $f037fcreatedDtTm;

        return $this;
    }

    /**
     * Get f037fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF037fcreatedDtTm()
    {
        return $this->f037fcreatedDtTm;
    }

    /**
     * Set f037fupdatedDtTm
     *
     * @param \DateTime $f037fupdatedDtTm
     *
     * @return T037fpremiseTypeSetup
     */
    public function setF037fupdatedDtTm($f037fupdatedDtTm)
    {
        $this->f037fupdatedDtTm = $f037fupdatedDtTm;

        return $this;
    }

    /**
     * Get f037fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF037fupdatedDtTm()
    {
        return $this->f037fupdatedDtTm;
    }
}
