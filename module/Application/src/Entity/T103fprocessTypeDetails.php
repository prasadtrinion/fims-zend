<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T103fprocessTypeDetails
 *
 * @ORM\Table(name="t103fprocess_type_details")
 * @ORM\Entity(repositoryClass="Application\Repository\ProcessTypeRepository")
 */
class T103fprocessTypeDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f103fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f103fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fid_type", type="integer",  nullable=true)
     */
    private $f103fidType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fstatus", type="integer", nullable=true)
     */
    private $f103fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fcreated_by", type="integer", nullable=true)
     */
    private $f103fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fid_master", type="integer", nullable=true)
     */
    private $f103fidMaster;

    /**
     * @var integer
     *
     * @ORM\Column(name="f103fupdated_by", type="integer", nullable=true)
     */
    private $f103fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f103fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f103fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f103fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f103fupdatedDtTm;



    /**
     * Get f103fidDetails
     *
     * @return integer
     */
    public function getF103fidDetails()
    {
        return $this->f103fidDetails;
    }

    /**
     * Set f103fidType
     *
     * @param string $f103fidType
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fidType($f103fidType)
    {
        $this->f103fidType = $f103fidType;

        return $this;
    }

    /**
     * Get f103fidType
     *
     * @return string
     */
    public function getF103fidType()
    {
        return $this->f103fidType;
    }

    /**
     * Set f103fstatus
     *
     * @param integer $f103fstatus
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fstatus($f103fstatus)
    {
        $this->f103fstatus = $f103fstatus;

        return $this;
    }

    /**
     * Get f103fstatus
     *
     * @return integer
     */
    public function getF103fstatus()
    {
        return $this->f103fstatus;
    }

    /**
     * Set f103fcreatedBy
     *
     * @param integer $f103fcreatedBy
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fcreatedBy($f103fcreatedBy)
    {
        $this->f103fcreatedBy = $f103fcreatedBy;

        return $this;
    }

    /**
     * Get f103fcreatedBy
     *
     * @return integer
     */
    public function getF103fcreatedBy()
    {
        return $this->f103fcreatedBy;
    }

    /**
     * Set f103fupdatedBy
     *
     * @param integer $f103fupdatedBy
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fupdatedBy($f103fupdatedBy)
    {
        $this->f103fupdatedBy = $f103fupdatedBy;

        return $this;
    }

    /**
     * Get f103fupdatedBy
     *
     * @return integer
     */
    public function getF103fupdatedBy()
    {
        return $this->f103fupdatedBy;
    }

     /**
     * Set f103fidMaster
     *
     * @param integer $f103fidMaster
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fidMaster($f103fidMaster)
    {
        $this->f103fidMaster = $f103fidMaster;

        return $this;
    }

    /**
     * Get f103fidMaster
     *
     * @return integer
     */
    public function getF103fidMaster()
    {
        return $this->f103fidMaster;
    }

    /**
     * Set f103fcreatedDtTm
     *
     * @param \DateTime $f103fcreatedDtTm
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fcreatedDtTm($f103fcreatedDtTm)
    {
        $this->f103fcreatedDtTm = $f103fcreatedDtTm;

        return $this;
    }

    /**
     * Get f103fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF103fcreatedDtTm()
    {
        return $this->f103fcreatedDtTm;
    }

    /**
     * Set f103fupdatedDtTm
     *
     * @param \DateTime $f103fupdatedDtTm
     *
     * @return T103fprocessTypeDetails
     */
    public function setF103fupdatedDtTm($f103fupdatedDtTm)
    {
        $this->f103fupdatedDtTm = $f103fupdatedDtTm;

        return $this;
    }

    /**
     * Get f103fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF103fupdatedDtTm()
    {
        return $this->f103fupdatedDtTm;
    }


}

