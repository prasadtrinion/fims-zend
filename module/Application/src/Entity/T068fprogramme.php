<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T068fprogramme
 *
 * @ORM\Table(name="t068fprogramme",uniqueConstraints={@ORM\UniqueConstraint(name="f068fname_UNIQUE", columns={"f068fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ProgrammeRepository")
 */
class T068fprogramme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f068fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f068fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f068fname", type="string", length=150, nullable=false)
     */
    private $f068fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f068fcode", type="string", length=10, nullable=false)
     */
    private $f068fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068ftotal_credit_hours", type="string",length=20, nullable=true)
     */
    private $f068ftotalCreditHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068ftype", type="string",length=20, nullable=true)
     */
    private $f068ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068fid_scheme", type="integer", nullable=true)
     */
    private $f068fidScheme;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068faward", type="integer", nullable=true)
     */
    private $f068faward;


    /**
     * @var integer
     *
     * @ORM\Column(name="f068fstatus", type="integer", nullable=false)
     */
    private $f068fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068fcreated_by", type="integer", nullable=true)
     */
    private $f068fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f068fupdated_by", type="integer", nullable=true)
     */
    private $f068fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f068fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f068fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f068fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f068fupdatedDtTm;



    /**
     * Get f068fid
     *
     * @return integer
     */
    public function getF068fid()
    {
        return $this->f068fid;
    }

    /**
     * Set f068fname
     *
     * @param string $f068fname
     *
     * @return T068fprogramme
     */
    public function setF068fname($f068fname)
    {
        $this->f068fname = $f068fname;

        return $this;
    }

    /**
     * Get f068fname
     *
     * @return string
     */
    public function getF068fname()
    {
        return $this->f068fname;
    }
     /**
     * Set f068ftype
     *
     * @param string $f068ftype
     *
     * @return T068fprogramme
     */
    public function setF068ftype($f068ftype)
    {
        $this->f068ftype = $f068ftype;

        return $this;
    }

    /**
     * Get f068ftype
     *
     * @return string
     */
    public function getF068ftype()
    {
        return $this->f068ftype;
    }

    /**
     * Set f068fcode
     *
     * @param string $f068fcode
     *
     * @return T068fprogramme
     */
    public function setF068fcode($f068fcode)
    {
        $this->f068fcode = $f068fcode;

        return $this;
    }

    /**
     * Get f068fcode
     *
     * @return string
     */
    public function getF068fcode()
    {
        return $this->f068fcode;
    }

    /**
     * Set f068ftotalCreditHours
     *
     * @param integer $f068ftotalCreditHours
     *
     * @return T068fprogramme
     */
    public function setF068ftotalCreditHours($f068ftotalCreditHours)
    {
        $this->f068ftotalCreditHours = $f068ftotalCreditHours;

        return $this;
    }

    /**
     * Get f068ftotalCreditHours
     *
     * @return integer
     */
    public function getF068ftotalCreditHours()
    {
        return $this->f068ftotalCreditHours;
    }

    /**
     * Set f068fidScheme
     *
     * @param integer $f068fidScheme
     *
     * @return T068fprogramme
     */
    public function setF068fidScheme($f068fidScheme)
    {
        $this->f068fidScheme = $f068fidScheme;

        return $this;
    }

    /**
     * Get f068fidScheme
     *
     * @return integer
     */
    public function getF068fidScheme()
    {
        return $this->f068fidScheme;
    }

    /**
     * Set f068faward
     *
     * @param integer $f068faward
     *
     * @return T068fprogramme
     */
    public function setF068faward($f068faward)
    {
        $this->f068faward = $f068faward;

        return $this;
    }

    /**
     * Get f068faward
     *
     * @return integer
     */
    public function getF068faward()
    {
        return $this->f068faward;
    }

    /**
     * Set f068fstatus
     *
     * @param integer $f068fstatus
     *
     * @return T068fprogramme
     */
    public function setF068fstatus($f068fstatus)
    {
        $this->f068fstatus = $f068fstatus;

        return $this;
    }

    /**
     * Get f068fstatus
     *
     * @return integer
     */
    public function getF068fstatus()
    {
        return $this->f068fstatus;
    }

    /**
     * Set f068fcreatedBy
     *
     * @param integer $f068fcreatedBy
     *
     * @return T068fprogramme
     */
    public function setF068fcreatedBy($f068fcreatedBy)
    {
        $this->f068fcreatedBy = $f068fcreatedBy;

        return $this;
    }

    /**
     * Get f068fcreatedBy
     *
     * @return integer
     */
    public function getF068fcreatedBy()
    {
        return $this->f068fcreatedBy;
    }

    /**
     * Set f068fupdatedBy
     *
     * @param integer $f068fupdatedBy
     *
     * @return T068fprogramme
     */
    public function setF068fupdatedBy($f068fupdatedBy)
    {
        $this->f068fupdatedBy = $f068fupdatedBy;

        return $this;
    }

    /**
     * Get f068fupdatedBy
     *
     * @return integer
     */
    public function getF068fupdatedBy()
    {
        return $this->f068fupdatedBy;
    }

    /**
     * Set f068fcreatedDtTm
     *
     * @param \DateTime $f068fcreatedDtTm
     *
     * @return T068fprogramme
     */
    public function setF068fcreatedDtTm($f068fcreatedDtTm)
    {
        $this->f068fcreatedDtTm = $f068fcreatedDtTm;

        return $this;
    }

    /**
     * Get f068fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF068fcreatedDtTm()
    {
        return $this->f068fcreatedDtTm;
    }

    /**
     * Set f068fupdatedDtTm
     *
     * @param \DateTime $f068fupdatedDtTm
     *
     * @return T068fprogramme
     */
    public function setF068fupdatedDtTm($f068fupdatedDtTm)
    {
        $this->f068fupdatedDtTm = $f068fupdatedDtTm;

        return $this;
    }

    /**
     * Get f068fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF068fupdatedDtTm()
    {
        return $this->f068fupdatedDtTm;
    }
}
