<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T070floanCancelRequest
 *
 * @ORM\Table(name="t070floan_cancel_request",uniqueConstraints={@ORM\UniqueConstraint(name="f070fname_UNIQUE", columns={"f070fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\LoanCancelRequestRepository")
 */
class T070floanCancelRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f070fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f070fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fname", type="string", length=50, nullable=false)
     */
    private $f070fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fcancel_reason", type="string", length=200, nullable=true)
     */
    private $f070fcancelReason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fid_staff", type="integer", nullable=true)
     */
    private $f070fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fstatus", type="integer", nullable=true)
     */
    private $f070fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproval_status", type="integer", nullable=true)
     */
    private $f070fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fcreated_by", type="integer", nullable=true)
     */
    private $f070fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f070fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fupdated_by", type="integer", nullable=true)
     */
    private $f070fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f070fupdatedDtTm;



    /**
     * Get f070fid
     *
     * @return integer
     */
    public function getF070fid()
    {
        return $this->f070fid;
    }

    /**
     * Set f070fname
     *
     * @param string $f070fname
     *
     * @return T070floanCancelRequest
     */
    public function setF070fname($f070fname)
    {
        $this->f070fname = $f070fname;

        return $this;
    }

    /**
     * Get f070fname
     *
     * @return string
     */
    public function getF070fname()
    {
        return $this->f070fname;
    }

    /**
     * Set f070fcancelReason
     *
     * @param string $f070fcancelReason
     *
     * @return T070floanCancelRequest
     */
    public function setF070fcancelReason($f070fcancelReason)
    {
        $this->f070fcancelReason = $f070fcancelReason;

        return $this;
    }

    /**
     * Get f070fcancelReason
     *
     * @return string
     */
    public function getF070fcancelReason()
    {
        return $this->f070fcancelReason;
    }

    /**
     * Set f070fidStaff
     *
     * @param integer $f070fidStaff
     *
     * @return T070floanCancelRequest
     */
    public function setF070fidStaff($f070fidStaff)
    {
        $this->f070fidStaff = $f070fidStaff;

        return $this;
    }

    /**
     * Get f070fidStaff
     *
     * @return integer
     */
    public function getF070fidStaff()
    {
        return $this->f070fidStaff;
    }

    /**
     * Set f070fstatus
     *
     * @param integer $f070fstatus
     *
     * @return T070floanCancelRequest
     */
    public function setF070fstatus($f070fstatus)
    {
        $this->f070fstatus = $f070fstatus;

        return $this;
    }

    /**
     * Get f070fstatus
     *
     * @return integer
     */
    public function getF070fstatus()
    {
        return $this->f070fstatus;
    }

    /**
     * Set f070fapprovalStatus
     *
     * @param integer $f070fapprovalStatus
     *
     * @return T070floanCancelRequest
     */
    public function setF070fapprovalStatus($f070fapprovalStatus)
    {
        $this->f070fapprovalStatus = $f070fapprovalStatus;

        return $this;
    }

    /**
     * Get f070fapprovalStatus
     *
     * @return integer
     */
    public function getF070fapprovalStatus()
    {
        return $this->f070fapprovalStatus;
    }

    /**
     * Set f070fcreatedBy
     *
     * @param integer $f070fcreatedBy
     *
     * @return T070floanCancelRequest
     */
    public function setF070fcreatedBy($f070fcreatedBy)
    {
        $this->f070fcreatedBy = $f070fcreatedBy;

        return $this;
    }

    /**
     * Get f070fcreatedBy
     *
     * @return integer
     */
    public function getF070fcreatedBy()
    {
        return $this->f070fcreatedBy;
    }

    /**
     * Set f070fcreatedDtTm
     *
     * @param \DateTime $f070fcreatedDtTm
     *
     * @return T070floanCancelRequest
     */
    public function setF070fcreatedDtTm($f070fcreatedDtTm)
    {
        $this->f070fcreatedDtTm = $f070fcreatedDtTm;

        return $this;
    }

    /**
     * Get f070fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fcreatedDtTm()
    {
        return $this->f070fcreatedDtTm;
    }

    /**
     * Set f070fupdatedBy
     *
     * @param integer $f070fupdatedBy
     *
     * @return T070floanCancelRequest
     */
    public function setF070fupdatedBy($f070fupdatedBy)
    {
        $this->f070fupdatedBy = $f070fupdatedBy;

        return $this;
    }

    /**
     * Get f070fupdatedBy
     *
     * @return integer
     */
    public function getF070fupdatedBy()
    {
        return $this->f070fupdatedBy;
    }

    /**
     * Set f070fupdatedDtTm
     *
     * @param \DateTime $f070fupdatedDtTm
     *
     * @return T070floanCancelRequest
     */
    public function setF070fupdatedDtTm($f070fupdatedDtTm)
    {
        $this->f070fupdatedDtTm = $f070fupdatedDtTm;

        return $this;
    }

    /**
     * Get f070fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fupdatedDtTm()
    {
        return $this->f070fupdatedDtTm;
    }
}
