<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T045fshowTabs
 *
 * @ORM\Table(name="t045fshow_tabs", indexes={@ORM\Index(name="f045fupdated_by", columns={"f045fupdated_by"}), @ORM\Index(name="f045fcreated_by", columns={"f045fcreated_by"}), @ORM\Index(name="f045fgroup_id", columns={"f045fgroup_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ShowTabsRepository")
 */
class T045fshowTabs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f045fid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f045fshow_bank_details", type="boolean", nullable=false)
     */
    private $f045fshowBankDetails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f045fshow_address_details", type="boolean", nullable=false)
     */
    private $f045fshowAddressDetails;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f045fshow_registration", type="boolean", nullable=false)
     */
    private $f045fshowRegistration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f045fstatus", type="boolean", nullable=false)
     */
    private $f045fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f045fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f045fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f045fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f045fcreatedBy;

    /**
     * @var \Application\Entity\T012fgroup
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T012fgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f045fgroup_id", referencedColumnName="f012fid")
     * })
     */
    private $f045fgroupId;


    public function getF045fid()
    {
        return $this->f045fid;
    }

    
    public function setF045fshowBankDetails($f045fshowBankDetails)
    {
        $this->f045fshowBankDetails = $f045fshowBankDetails;

        return $this;
    }

   
    public function getF045fshowBankDetails()
    {
        return $this->f045fshowBankDetails;
    }


    public function setF045fshowAddressDetails($f045faddressDetails)
    {
        $this->f045fshowAddressDetails = $f045faddressDetails;

        return $this;
    }

    public function getF045fshowAddressDetails()
    {
        return $this->f045fshowAddressDetails;
    }

    
    public function setF045fshowRegistration($f045fregistration)
    {
        $this->f045fshowRegistration = $f045fregistration;

        return $this;
    }

    public function getF045fshowRegistration()
    {
        return $this->f045fshowRegistration;
    }

  
    public function setF045fcreatedDtTm($f045fcreatedDtTm)
    {
        $this->f045fcreatedDtTm = $f045fcreatedDtTm;

        return $this;
    }

  
    public function getF045fcreatedDtTm()
    {
        return $this->f045fcreatedDtTm;
    }

   
    public function setF045fupdatedDtTm($f045fupdatedDtTm)
    {
        $this->f045fupdatedDtTm = $f045fupdatedDtTm;

        return $this;
    }

    
    public function getF045fupdatedDtTm()
    {
        return $this->f045fupdatedDtTm;
    }

   
    public function setF045fgroupId(\Application\Entity\T012fgroup $f045fgroupId = null)
    {
        $this->f045fgroupId = $f045fgroupId;

        return $this;
    }

   
    public function getF045fgroupId()
    {
        return $this->f045fgroupId;
    }

   
    public function setF045fupdatedBy($f045fupdatedBy )
    {
        $this->f045fupdatedBy = $f045fupdatedBy;

        return $this;
    }

    
    public function getF045fupdatedBy()
    {
        return $this->f045fupdatedBy;
    }

    
    public function setF045fcreatedBy( $f045fcreatedBy )
    {
        $this->f045fcreatedBy = $f045fcreatedBy;

        return $this;
    }

   
    public function getF045fcreatedBy()
    {
        return $this->f045fcreatedBy;
    }

    public function setF045fstatus($f045fstatus)
    {
        $this->f045fstatus = $f045fstatus;

        return $this;
    }

    public function getF045fstatus()
    {
        return $this->f045fstatus;
    }
}
