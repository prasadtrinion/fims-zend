<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T070fdateMaintenance
 *
 * @ORM\Table(name="t070fdate_maintenance")
 * @ORM\Entity(repositoryClass="Application\Repository\DateMaintenanceRepository")
 */
class T070fdateMaintenance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f070fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f070fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fstafftype", type="string", length=50, nullable=false)
     */
    private $f070fstafftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fmonth", type="string", length=50, nullable=false)
     */
    private $f070fmonth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fstart_date", type="datetime", nullable=false)
     */
    private $f070fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fend_date", type="datetime", nullable=false)
     */
    private $f070fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fstatus", type="integer", nullable=false)
     */
    private $f070fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fcreated_by", type="integer", nullable=false)
     */
    private $f070fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f070fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fupdated_by", type="integer", nullable=false)
     */
    private $f070fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f070fupdatedDtTm;



    /**
     * Get f070fid
     *
     * @return integer
     */
    public function getF070fid()
    {
        return $this->f070fid;
    }

    /**
     * Set f070fstafftype
     *
     * @param integer $f070fstafftype
     *
     * @return T070fdateMaintenance
     */
    public function setF070fstafftype($f070fstafftype)
    {
        $this->f070fstafftype = $f070fstafftype;

        return $this;
    }

    /**
     * Get f070fstafftype
     *
     * @return integer
     */
    public function getF070fstafftype()
    {
        return $this->f070fstafftype;
    }

    /**
     * Set f070fmonth
     *
     * @param integer $f070fmonth
     *
     * @return T070fdateMaintenance
     */
    public function setF070fmonth($f070fmonth)
    {
        $this->f070fmonth = $f070fmonth;

        return $this;
    }

    /**
     * Get f070fmonth
     *
     * @return integer
     */
    public function getF070fmonth()
    {
        return $this->f070fmonth;
    }

    /**
     * Set f070fstartDate
     *
     * @param \DateTime $f070fstartDate
     *
     * @return T070fdateMaintenance
     */
    public function setF070fstartDate($f070fstartDate)
    {
        $this->f070fstartDate = $f070fstartDate;

        return $this;
    }

    /**
     * Get f070fstartDate
     *
     * @return \DateTime
     */
    public function getF070fstartDate()
    {
        return $this->f070fstartDate;
    }

    /**
     * Set f070fendDate
     *
     * @param \DateTime $f070fendDate
     *
     * @return T070fdateMaintenance
     */
    public function setF070fendDate($f070fendDate)
    {
        $this->f070fendDate = $f070fendDate;

        return $this;
    }

    /**
     * Get f070fendDate
     *
     * @return \DateTime
     */
    public function getF070fendDate()
    {
        return $this->f070fendDate;
    }

    /**
     * Set f070fstatus
     *
     * @param integer $f070fstatus
     *
     * @return T070fdateMaintenance
     */
    public function setF070fstatus($f070fstatus)
    {
        $this->f070fstatus = $f070fstatus;

        return $this;
    }

    /**
     * Get f070fstatus
     *
     * @return integer
     */
    public function getF070fstatus()
    {
        return $this->f070fstatus;
    }

    /**
     * Set f070fcreatedBy
     *
     * @param integer $f070fcreatedBy
     *
     * @return T070fdateMaintenance
     */
    public function setF070fcreatedBy($f070fcreatedBy)
    {
        $this->f070fcreatedBy = $f070fcreatedBy;

        return $this;
    }

    /**
     * Get f070fcreatedBy
     *
     * @return integer
     */
    public function getF070fcreatedBy()
    {
        return $this->f070fcreatedBy;
    }

    /**
     * Set f070fcreatedDtTm
     *
     * @param \DateTime $f070fcreatedDtTm
     *
     * @return T070fdateMaintenance
     */
    public function setF070fcreatedDtTm($f070fcreatedDtTm)
    {
        $this->f070fcreatedDtTm = $f070fcreatedDtTm;

        return $this;
    }

    /**
     * Get f070fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fcreatedDtTm()
    {
        return $this->f070fcreatedDtTm;
    }

    /**
     * Set f070fupdatedBy
     *
     * @param integer $f070fupdatedBy
     *
     * @return T070fdateMaintenance
     */
    public function setF070fupdatedBy($f070fupdatedBy)
    {
        $this->f070fupdatedBy = $f070fupdatedBy;

        return $this;
    }

    /**
     * Get f070fupdatedBy
     *
     * @return integer
     */
    public function getF070fupdatedBy()
    {
        return $this->f070fupdatedBy;
    }

    /**
     * Set f070fupdatedDtTm
     *
     * @param \DateTime $f070fupdatedDtTm
     *
     * @return T070fdateMaintenance
     */
    public function setF070fupdatedDtTm($f070fupdatedDtTm)
    {
        $this->f070fupdatedDtTm = $f070fupdatedDtTm;

        return $this;
    }

    /**
     * Get f070fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fupdatedDtTm()
    {
        return $this->f070fupdatedDtTm;
    }
}
