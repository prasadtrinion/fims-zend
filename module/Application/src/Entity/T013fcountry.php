<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T013fcountry
 *
 * @ORM\Table(name="t013fcountry",uniqueConstraints={@ORM\UniqueConstraint(name="f013fcountry_name_UNIQUE", columns={"f013fcountry_name"})}, indexes={@ORM\Index(name="f013fupdated_by", columns={"f013fupdated_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CountryRepository")
 */
class T013fcountry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f013fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f013fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fcountry_name", type="string", length=50, nullable=false)
     */
    private $f013fcountryName;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fshort_name", type="string", length=20, nullable=false)
     */
    private $f013fshortName;

    /**
     * @var string
     *
     * @ORM\Column(name="f013fiso", type="string", length=20, nullable=true)
     */
    private $f013fiso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f013fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f013fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fstatus", type="integer", nullable=false)
     */
    private $f013fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f013fupdated_by", type="integer", nullable=false)
     *
     */
    private $f013fupdatedBy;



    /**
     * Get f013fid
     *
     * @return integer
     */
    public function getF013fid()
    {
        return $this->f013fid;
    }

    /**
     * Set f013fcountryName
     *
     * @param string $f013fcountryName
     *
     * @return T013fcountry
     */
    public function setF013fcountryName($f013fcountryName)
    {
        $this->f013fcountryName = $f013fcountryName;

        return $this;
    }

    /**
     * Get f013fcountryName
     *
     * @return string
     */
    public function getF013fcountryName()
    {
        return $this->f013fcountryName;
    }

    /**
     * Set f013fshortName
     *
     * @param string $f013fshortName
     *
     * @return T013fcountry
     */
    public function setF013fshortName($f013fshortName)
    {
        $this->f013fshortName = $f013fshortName;

        return $this;
    }

    /**
     * Get f013fshortName
     *
     * @return string
     */
    public function getF013fshortName()
    {
        return $this->f013fshortName;
    }

    /**
     * Set f013fiso
     *
     * @param string $f013fiso
     *
     * @return T013fcountry
     */
    public function setF013fiso($f013fiso)
    {
        $this->f013fiso = $f013fiso;

        return $this;
    }

    /**
     * Get f013fiso
     *
     * @return string
     */
    public function getF013fiso()
    {
        return $this->f013fiso;
    }

    /**
     * Set f013fupdatedDtTm
     *
     * @param \DateTime $f013fupdatedDtTm
     *
     * @return T013fcountry
     */
    public function setF013fupdatedDtTm($f013fupdatedDtTm)
    {
        $this->f013fupdatedDtTm = $f013fupdatedDtTm;

        return $this;
    }

    /**
     * Get f013fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF013fupdatedDtTm()
    {
        return $this->f013fupdatedDtTm;
    }

    /**
     * Set f013fstatus
     *
     * @param integer $f013fstatus
     *
     * @return T013fcountry
     */
    public function setF013fstatus($f013fstatus)
    {
        $this->f013fstatus = $f013fstatus;

        return $this;
    }

    /**
     * Get f013fstatus
     *
     * @return integer
     */
    public function getF013fstatus()
    {
        return $this->f013fstatus;
    }

    /**
     * Set f013fupdatedBy
     *
     * @param integer $f013fupdatedBy
     *
     * @return T013fcountry
     */
    public function setF013fupdatedBy($f013fupdatedBy)
    {
        $this->f013fupdatedBy = $f013fupdatedBy;

        return $this;
    }

    /**
     * Get f013fupdatedBy
     *
     * @return integer
     */
    public function getF013fupdatedBy()
    {
        return $this->f013fupdatedBy;
    }
}
