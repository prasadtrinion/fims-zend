<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T081fsalary
 *
 * @ORM\Table(name="t081fsalary")
 * @ORM\Entity(repositoryClass="Application\Repository\SalaryCalculationRepository")
 */
class T081fsalary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f081fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fcode", type="string", length=50, nullable=true)
     */
    private $f081fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fname", type="string", length=50, nullable=true)
     */
    private $f081fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f081fshort_name", type="string", length=20, nullable=true)
     */
    private $f081fshortName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081ftype_of_component", type="integer", nullable=true)
     */
    private $f081ftypeOfComponent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fmode", type="integer", nullable=true)
     */
    private $f081fmode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081ffrequency", type="integer", nullable=true)
     */
    private $f081ffrequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fsalary_type", type="integer", nullable=true)
     */
    private $f081fsalaryType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081faccounts_code_deduction", type="string",length=50, nullable=true)
     */
    private $f081faccountsCodeDeduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081faccounts_code_cr", type="string",length=50, nullable=true)
     */
    private $f081faccountsCodeCr;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fpf_deductable", type="string",length=20, nullable=true)
     */
    private $f081fpfDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fsocso_deductable", type="string",length=20, nullable=true)
     */
    private $f081fsocsoDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081ftax_deductable", type="string",length=20, nullable=true)
     */
    private $f081ftaxDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstatus", type="integer", nullable=true)
     */
    private $f081fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081feis_deductable", type="string",length=20, nullable=true)
     */
    private $f081feisDeductable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fvendor_code", type="string",length=50, nullable=true)
     */
    private $f081fvendorCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fcreated_by", type="integer", nullable=true)
     */
    private $f081fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fupdated_by", type="integer", nullable=true)
     */
    private $f081fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f081fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f081fupdatedDtTm;



    /**
     * Get f081fid
     *
     * @return integer
     */
    public function getF081fid()
    {
        return $this->f081fid;
    }

    /**
     * Set f081fcode
     *
     * @param string $f081fcode
     *
     * @return T081fsalary
     */
    public function setF081fcode($f081fcode)
    {
        $this->f081fcode = $f081fcode;

        return $this;
    }

     /**
     * Get f081fcode
     *
     * @return string
     */
    public function getF081fcode()
    {
        return $this->f081fcode;
    }

    /**
     * Set f081fvendorCode
     *
     * @param string $f081fvendorCode
     *
     * @return T081fsalary
     */
    public function setF081fvendorCode($f081fvendorCode)
    {
        $this->f081fvendorCode = $f081fvendorCode;

        return $this;
    }

     /**
     * Get f081fvendorCode
     *
     * @return string
     */
    public function getF081fvendorCode()
    {
        return $this->f081fvendorCode;
    }


    /**
     * Get f081feisDeductable
     *
     * @return string
     */
    public function getF081feisDeductable()
    {
        return $this->f081feisDeductable;
    }

     /**
     * Set f081feisDeductable
     *
     * @param string $f081feisDeductable
     *
     * @return T081fsalary
     */
    public function setF081feisDeductable($f081feisDeductable)
    {
        $this->f081feisDeductable = $f081feisDeductable;

        return $this;
    }

   
    /**
     * Set f081fname
     *
     * @param string $f081fname
     *
     * @return T081fsalary
     */
    public function setF081fname($f081fname)
    {
        $this->f081fname = $f081fname;

        return $this;
    }

    /**
     * Get f081fname
     *
     * @return string
     */
    public function getF081fname()
    {
        return $this->f081fname;
    }

    /**
     * Set f081fshortName
     *
     * @param string $f081fshortName
     *
     * @return T081fsalary
     */
    public function setF081fshortName($f081fshortName)
    {
        $this->f081fshortName = $f081fshortName;

        return $this;
    }

    /**
     * Get f081fshortName
     *
     * @return string
     */
    public function getF081fshortName()
    {
        return $this->f081fshortName;
    }

    /**
     * Set f081ftypeOfComponent
     *
     * @param integer $f081ftypeOfComponent
     *
     * @return T081fsalary
     */
    public function setF081ftypeOfComponent($f081ftypeOfComponent)
    {
        $this->f081ftypeOfComponent = $f081ftypeOfComponent;

        return $this;
    }

    /**
     * Get f081ftypeOfComponent
     *
     * @return integer
     */
    public function getF081ftypeOfComponent()
    {
        return $this->f081ftypeOfComponent;
    }

    /**
     * Set f081fmode
     *
     * @param integer $f081fmode
     *
     * @return T081fsalary
     */
    public function setF081fmode($f081fmode)
    {
        $this->f081fmode = $f081fmode;

        return $this;
    }

    /**
     * Get f081fmode
     *
     * @return integer
     */
    public function getF081fmode()
    {
        return $this->f081fmode;
    }

    /**
     * Set f081ffrequency
     *
     * @param integer $f081ffrequency
     *
     * @return T081fsalary
     */
    public function setF081ffrequency($f081ffrequency)
    {
        $this->f081ffrequency = $f081ffrequency;

        return $this;
    }

    /**
     * Get f081ffrequency
     *
     * @return integer
     */
    public function getF081ffrequency()
    {
        return $this->f081ffrequency;
    }

    /**
     * Set f081fsalaryType
     *
     * @param integer $f081fsalaryType
     *
     * @return T081fsalary
     */
    public function setF081fsalaryType($f081fsalaryType)
    {
        $this->f081fsalaryType = $f081fsalaryType;

        return $this;
    }

    /**
     * Get f081fsalaryType
     *
     * @return integer
     */
    public function getF081fsalaryType()
    {
        return $this->f081fsalaryType;
    }

    /**
     * Set f081faccountsCodeDeduction
     *
     * @param integer $f081faccountsCodeDeduction
     *
     * @return T081fsalary
     */
    public function setF081faccountsCodeDeduction($f081faccountsCodeDeduction)
    {
        $this->f081faccountsCodeDeduction = $f081faccountsCodeDeduction;

        return $this;
    }

    /**
     * Get f081faccountsCodeDeduction
     *
     * @return integer
     */
    public function getF081faccountsCodeDeduction()
    {
        return $this->f081faccountsCodeDeduction;
    }

    /**
     * Set f081faccountsCodeCr
     *
     * @param integer $f081faccountsCodeCr
     *
     * @return T081fsalary
     */
    public function setF081faccountsCodeCr($f081faccountsCodeCr)
    {
        $this->f081faccountsCodeCr = $f081faccountsCodeCr;

        return $this;
    }

    /**
     * Get f081faccountsCodeCr
     *
     * @return integer
     */
    public function getF081faccountsCodeCr()
    {
        return $this->f081faccountsCodeCr;
    }

    /**
     * Set f081fpfDeductable
     *
     * @param integer $f081fpfDeductable
     *
     * @return T081fsalary
     */
    public function setF081fpfDeductable($f081fpfDeductable)
    {
        $this->f081fpfDeductable = $f081fpfDeductable;

        return $this;
    }

    /**
     * Get f081fpfDeductable
     *
     * @return integer
     */
    public function getF081fpfDeductable()
    {
        return $this->f081fpfDeductable;
    }

    /**
     * Set f081fsocsoDeductable
     *
     * @param integer $f081fsocsoDeductable
     *
     * @return T081fsalary
     */
    public function setF081fsocsoDeductable($f081fsocsoDeductable)
    {
        $this->f081fsocsoDeductable = $f081fsocsoDeductable;

        return $this;
    }

    /**
     * Get f081fsocsoDeductable
     *
     * @return integer
     */
    public function getF081fsocsoDeductable()
    {
        return $this->f081fsocsoDeductable;
    }

    /**
     * Set f081ftaxDeductable
     *
     * @param integer $f081ftaxDeductable
     *
     * @return T081fsalary
     */
    public function setF081ftaxDeductable($f081ftaxDeductable)
    {
        $this->f081ftaxDeductable = $f081ftaxDeductable;

        return $this;
    }

    /**
     * Get f081ftaxDeductable
     *
     * @return integer
     */
    public function getF081ftaxDeductable()
    {
        return $this->f081ftaxDeductable;
    }

    /**
     * Set f081fstatus
     *
     * @param integer $f081fstatus
     *
     * @return T081fsalary
     */
    public function setF081fstatus($f081fstatus)
    {
        $this->f081fstatus = $f081fstatus;

        return $this;
    }

    /**
     * Get f081fstatus
     *
     * @return integer
     */
    public function getF081fstatus()
    {
        return $this->f081fstatus;
    }

    /**
     * Set f081fcreatedBy
     *
     * @param integer $f081fcreatedBy
     *
     * @return T081fsalary
     */
    public function setF081fcreatedBy($f081fcreatedBy)
    {
        $this->f081fcreatedBy = $f081fcreatedBy;

        return $this;
    }

    /**
     * Get f081fcreatedBy
     *
     * @return integer
     */
    public function getF081fcreatedBy()
    {
        return $this->f081fcreatedBy;
    }

    /**
     * Set f081fupdatedBy
     *
     * @param integer $f081fupdatedBy
     *
     * @return T081fsalary
     */
    public function setF081fupdatedBy($f081fupdatedBy)
    {
        $this->f081fupdatedBy = $f081fupdatedBy;

        return $this;
    }

    /**
     * Get f081fupdatedBy
     *
     * @return integer
     */
    public function getF081fupdatedBy()
    {
        return $this->f081fupdatedBy;
    }

    /**
     * Set f081fcreatedDtTm
     *
     * @param \DateTime $f081fcreatedDtTm
     *
     * @return T081fsalary
     */
    public function setF081fcreatedDtTm($f081fcreatedDtTm)
    {
        $this->f081fcreatedDtTm = $f081fcreatedDtTm;

        return $this;
    }

    /**
     * Get f081fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fcreatedDtTm()
    {
        return $this->f081fcreatedDtTm;
    }

    /**
     * Set f081fupdatedDtTm
     *
     * @param \DateTime $f081fupdatedDtTm
     *
     * @return T081fsalary
     */
    public function setF081fupdatedDtTm($f081fupdatedDtTm)
    {
        $this->f081fupdatedDtTm = $f081fupdatedDtTm;

        return $this;
    }

    /**
     * Get f081fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fupdatedDtTm()
    {
        return $this->f081fupdatedDtTm;
    }
}
