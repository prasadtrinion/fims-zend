<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T015ftemporaryDepartmentCode
 *
 * @ORM\Table(name="temporary_department_code")
 * @ORM\Entity(repositoryClass="Application\Repository\ViewsRepository")
 */
class T015ftemporaryDepartmentCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f015fid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f015fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdepartment_name", type="string", length=255, nullable=false)
     */
    private $f015fdepartmentName;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdepartment_code", type="string", length=255, nullable=false)
     */
    private $f015fdepartmentCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f015fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f015fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fstatus", type="integer", nullable=false)
     */
    private $f015fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f015fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f015fcreatedBy;



    /**
     * Get f015fid
     *
     * @return integer
     */
    public function getF015fid()
    {
        return $this->f015fid;
    }

    /**
     * Set f015fdepartmentName
     *
     * @param string $f015fdepartmentName
     *
     * @return T015fdepartment
     */
    public function setF015fdepartmentName($f015fdepartmentName)
    {
        $this->f015fdepartmentName = $f015fdepartmentName;

        return $this;
    }

    /**
     * Get f015fdepartmentName
     *
     * @return string
     */
    public function getF015fdepartmentName()
    {
        return $this->f015fdepartmentName;
    }

    /**
     * Set f015fdepartmentCode
     *
     * @param string $f015fdepartmentCode
     *
     * @return T015fdepartment
     */
    public function setF015fdepartmentCode($f015fdepartmentCode)
    {
        $this->f015fdepartmentCode = $f015fdepartmentCode;

        return $this;
    }

    /**
     * Get f015fdepartmentCode
     *
     * @return string
     */
    public function getF015fdepartmentCode()
    {
        return $this->f015fdepartmentCode;
    }

    /**
     * Set f015fupdatedDtTm
     *
     * @param \DateTime $f015fupdatedDtTm
     *
     * @return T015fdepartment
     */
    public function setF015fupdatedDtTm($f015fupdatedDtTm)
    {
        $this->f015fupdatedDtTm = $f015fupdatedDtTm;

        return $this;
    }

    /**
     * Get f015fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF015fupdatedDtTm()
    {
        return $this->f015fupdatedDtTm;
    }

    /**
     * Set f015fcreatedDtTm
     *
     * @param \DateTime $f015fcreatedDtTm
     *
     * @return T015fdepartment
     */
    public function setF015fcreatedDtTm($f015fcreatedDtTm)
    {
        $this->f015fcreatedDtTm = $f015fcreatedDtTm;

        return $this;
    }

    /**
     * Get f015fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF015fcreatedDtTm()
    {
        return $this->f015fcreatedDtTm;
    }

    /**
     * Set f015fstatus
     *
     * @param integer $f015fstatus
     *
     * @return T015fdepartment
     */
    public function setF015fstatus($f015fstatus)
    {
        $this->f015fstatus = $f015fstatus;

        return $this;
    }

    /**
     * Get f015fstatus
     *
     * @return integer
     */
    public function getF015fstatus()
    {
        return $this->f015fstatus;
    }

    /**
     * Set f015fupdatedBy
     *
     * @param integer $f015fupdatedBy
     *
     * @return T015fdepartment
     */
    public function setF015fupdatedBy($f015fupdatedBy)
    {
        $this->f015fupdatedBy = $f015fupdatedBy;

        return $this;
    }

    /**
     * Get f015fupdatedBy
     *
     * @return integer
     */
    public function getF015fupdatedBy()
    {
        return $this->f015fupdatedBy;
    }

     /**
     * Set f015fcreatedBy
     *
     * @param integer $f015fcreatedBy
     *
     * @return T015fdepartment
     */
    public function setF015fcreatedBy($f015fcreatedBy)
    {
        $this->f015fcreatedBy = $f015fcreatedBy;

        return $this;
    }

    /**
     * Get f015fcreatedBy
     *
     * @return integer
     */
    public function getF015fcreatedBy()
    {
        return $this->f015fcreatedBy;
    }
}

/*

CREATE VIEW temporary_department_code as  
SELECT f015fid,f015fdepartment_name,f015fdepartment_code,f015fupdated_dt_tm,f015fstatus,f015fupdated_by from t015fdepartment_code
*/
