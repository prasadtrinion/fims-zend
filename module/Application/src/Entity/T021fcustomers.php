<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T021fcustomers
 *
 * @ORM\Table(name="t021fcustomers")
 * @ORM\Entity(repositoryClass="Application\Repository\CustomerRepository")
 */
class T021fcustomers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f021fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f021ffirst_name", type="string", length=50, nullable=false)
     */
    private $f021ffirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="f021flast_name", type="string", length=100, nullable=true)
     */
    private $f021flastName;

    /**
     * @var string
     *
     * @ORM\Column(name="f021femail_id", type="string", length=50, nullable=true)
     */
    private $f021femailId;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fpassword", type="string", length=200, nullable=true)
     */
    private $f021fpassword;

    /**
     * @var string
     *
     * @ORM\Column(name="f021faddress1", type="string", length=255, nullable=true)
     */
    private $f021faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f021faddress2", type="text", length=65535, nullable=true)
     */
    private $f021faddress2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcountry", type="integer", nullable=true)
     */
    private $f021fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fcity", type="string", length=50, nullable=true)
     */
    private $f021fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fstate", type="integer", nullable=true)
     */
    private $f021fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcustomer_debit", type="integer", nullable=true)
     */
    private $f021fcustomerDebit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcustomer_crebit", type="integer", nullable=true)
     */
    private $f021fcustomerCrebit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcustomer_balance", type="integer", nullable=true)
     */
    private $f021fcustomerBalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fzip_code", type="string", length=20, nullable=true)
     */
    private $f021fzipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fcontact", type="string", length=20, nullable=true)
     */
    private $f021fcontact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f021fsupplier", type="boolean", nullable=true)
     */
    private $f021fsupplier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f021ftax_free", type="string",length=50, nullable=true)
     */
    private $f021ftaxFree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f021fstatus", type="boolean", nullable=true)
     */
    private $f021fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcreated_by", type="integer", nullable=true)
     */
    private $f021fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fupdated_by", type="integer", nullable=true)
     */
    private $f021fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid_supplier", type="integer", nullable=true)
     */
    private $f021fidSupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid_tax_free", type="integer", nullable=true)
     */
    private $f021fidTaxFree;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fcontact_person", type="string", length=255, nullable=true)
     */
    private $f021fcontactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fdebtor_code", type="string", length=255, nullable=true)
     */
    private $f021fdebtorCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fbank", type="integer", nullable=true)
     */
    private $f021fbank;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fbranch", type="string", length=255, nullable=true)
     */
    private $f021fbranch;

    /**
     * @var string
     *
     * @ORM\Column(name="f021faccount_number", type="string", length=255, nullable=true)
     */
    private $f021faccountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fdebtor_category", type="integer", nullable=true)
     */
    private $f021fdebtorCategory;


    /**
     * Get f021fid
     *
     * @return integer
     */
    public function getF021fid()
    {
        return $this->f021fid;
    }

    /**
     * Set f021ffirstName
     *
     * @param string $f021ffirstName
     *
     * @return T021fcustomers
     */
    public function setF021ffirstName($f021ffirstName)
    {
        $this->f021ffirstName = $f021ffirstName;

        return $this;
    }

    /**
     * Get f021ffirstName
     *
     * @return string
     */
    public function getF021ffirstName()
    {
        return $this->f021ffirstName;
    }

    /**
     * Set f021flastName
     *
     * @param string $f021flastName
     *
     * @return T021fcustomers
     */
    public function setF021flastName($f021flastName)
    {
        $this->f021flastName = $f021flastName;

        return $this;
    }

    /**
     * Get f021flastName
     *
     * @return string
     */
    public function getF021flastName()
    {
        return $this->f021flastName;
    }

    /**
     * Set f021femailId
     *
     * @param string $f021femailId
     *
     * @return T021fcustomers
     */
    public function setF021femailId($f021femailId)
    {
        $this->f021femailId = $f021femailId;

        return $this;
    }

    /**
     * Get f021femailId
     *
     * @return string
     */
    public function getF021femailId()
    {
        return $this->f021femailId;
    }

    /**
     * Set f021fpassword
     *
     * @param string $f021fpassword
     *
     * @return T021fcustomers
     */
    public function setF021fpassword($f021fpassword)
    {
        $this->f021fpassword = $f021fpassword;

        return $this;
    }

    /**
     * Get f021fpassword
     *
     * @return string
     */
    public function getF021fpassword()
    {
        return $this->f021fpassword;
    }

    /**
     * Set f021faddress1
     *
     * @param string $f021faddress1
     *
     * @return T021fcustomers
     */
    public function setF021faddress1($f021faddress1)
    {
        $this->f021faddress1 = $f021faddress1;

        return $this;
    }

    /**
     * Get f021faddress1
     *
     * @return string
     */
    public function getF021faddress1()
    {
        return $this->f021faddress1;
    }

    /**
     * Set f021faddress2
     *
     * @param string $f021faddress2
     *
     * @return T021fcustomers
     */
    public function setF021faddress2($f021faddress2)
    {
        $this->f021faddress2 = $f021faddress2;

        return $this;
    }

    /**
     * Get f021faddress2
     *
     * @return string
     */
    public function getF021faddress2()
    {
        return $this->f021faddress2;
    }

    /**
     * Set f021fcountry
     *
     * @param integer $f021fcountry
     *
     * @return T021fcustomers
     */
    public function setF021fcountry($f021fcountry)
    {
        $this->f021fcountry = $f021fcountry;

        return $this;
    }

    /**
     * Get f021fcountry
     *
     * @return integer
     */
    public function getF021fcountry()
    {
        return $this->f021fcountry;
    }

    /**
     * Set f021fcity
     *
     * @param string $f021fcity
     *
     * @return T021fcustomers
     */
    public function setF021fcity($f021fcity)
    {
        $this->f021fcity = $f021fcity;

        return $this;
    }

    /**
     * Get f021fcity
     *
     * @return string
     */
    public function getF021fcity()
    {
        return $this->f021fcity;
    }

    /**
     * Set f021fstate
     *
     * @param integer $f021fstate
     *
     * @return T021fcustomers
     */
    public function setF021fstate($f021fstate)
    {
        $this->f021fstate = $f021fstate;

        return $this;
    }

    /**
     * Get f021fstate
     *
     * @return integer
     */
    public function getF021fstate()
    {
        return $this->f021fstate;
    }

    /**
     * Set f021fcustomerDebit
     *
     * @param integer $f021fcustomerDebit
     *
     * @return T021fcustomers
     */
    public function setF021fcustomerDebit($f021fcustomerDebit)
    {
        $this->f021fcustomerDebit = $f021fcustomerDebit;

        return $this;
    }

    /**
     * Get f021fcustomerDebit
     *
     * @return integer
     */
    public function getF021fcustomerDebit()
    {
        return $this->f021fcustomerDebit;
    }

    /**
     * Set f021fcustomerCrebit
     *
     * @param integer $f021fcustomerCrebit
     *
     * @return T021fcustomers
     */
    public function setF021fcustomerCrebit($f021fcustomerCrebit)
    {
        $this->f021fcustomerCrebit = $f021fcustomerCrebit;

        return $this;
    }

    /**
     * Get f021fcustomerCrebit
     *
     * @return integer
     */
    public function getF021fcustomerCrebit()
    {
        return $this->f021fcustomerCrebit;
    }

    /**
     * Set f021fcustomerBalance
     *
     * @param integer $f021fcustomerBalance
     *
     * @return T021fcustomers
     */
    public function setF021fcustomerBalance($f021fcustomerBalance)
    {
        $this->f021fcustomerBalance = $f021fcustomerBalance;

        return $this;
    }

    /**
     * Get f021fcustomerBalance
     *
     * @return integer
     */
    public function getF021fcustomerBalance()
    {
        return $this->f021fcustomerBalance;
    }

    /**
     * Set f021fzipCode
     *
     * @param string $f021fzipCode
     *
     * @return T021fcustomers
     */
    public function setF021fzipCode($f021fzipCode)
    {
        $this->f021fzipCode = $f021fzipCode;

        return $this;
    }

    /**
     * Get f021fzipCode
     *
     * @return string
     */
    public function getF021fzipCode()
    {
        return $this->f021fzipCode;
    }

    /**
     * Set f021fcontact
     *
     * @param string $f021fcontact
     *
     * @return T021fcustomers
     */
    public function setF021fcontact($f021fcontact)
    {
        $this->f021fcontact = $f021fcontact;

        return $this;
    }

    /**
     * Get f021fcontact
     *
     * @return string
     */
    public function getF021fcontact()
    {
        return $this->f021fcontact;
    }

    /**
     * Set f021fsupplier
     *
     * @param boolean $f021fsupplier
     *
     * @return T021fcustomers
     */
    public function setF021fsupplier($f021fsupplier)
    {
        $this->f021fsupplier = $f021fsupplier;

        return $this;
    }

    /**
     * Get f021fsupplier
     *
     * @return boolean
     */
    public function getF021fsupplier()
    {
        return $this->f021fsupplier;
    }

    /**
     * Set f021ftaxFree
     *
     * @param boolean $f021ftaxFree
     *
     * @return T021fcustomers
     */
    public function setF021ftaxFree($f021ftaxFree)
    {
        $this->f021ftaxFree = $f021ftaxFree;

        return $this;
    }

    /**
     * Get f021ftaxFree
     *
     * @return boolean
     */
    public function getF021ftaxFree()
    {
        return $this->f021ftaxFree;
    }

    /**
     * Set f021fstatus
     *
     * @param boolean $f021fstatus
     *
     * @return T021fcustomers
     */
    public function setF021fstatus($f021fstatus)
    {
        $this->f021fstatus = $f021fstatus;

        return $this;
    }

    /**
     * Get f021fstatus
     *
     * @return boolean
     */
    public function getF021fstatus()
    {
        return $this->f021fstatus;
    }

    /**
     * Set f021fcreatedBy
     *
     * @param integer $f021fcreatedBy
     *
     * @return T021fcustomers
     */
    public function setF021fcreatedBy($f021fcreatedBy)
    {
        $this->f021fcreatedBy = $f021fcreatedBy;

        return $this;
    }

    /**
     * Get f021fcreatedBy
     *
     * @return integer
     */
    public function getF021fcreatedBy()
    {
        return $this->f021fcreatedBy;
    }

    /**
     * Set f021fupdatedBy
     *
     * @param integer $f021fupdatedBy
     *
     * @return T021fcustomers
     */
    public function setF021fupdatedBy($f021fupdatedBy)
    {
        $this->f021fupdatedBy = $f021fupdatedBy;

        return $this;
    }

    /**
     * Get f021fupdatedBy
     *
     * @return integer
     */
    public function getF021fupdatedBy()
    {
        return $this->f021fupdatedBy;
    }

    /**
     * Set f021fcreatedDtTm
     *
     * @param \DateTime $f021fcreatedDtTm
     *
     * @return T021fcustomers
     */
    public function setF021fcreatedDtTm($f021fcreatedDtTm)
    {
        $this->f021fcreatedDtTm = $f021fcreatedDtTm;

        return $this;
    }

    /**
     * Get f021fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fcreatedDtTm()
    {
        return $this->f021fcreatedDtTm;
    }

    /**
     * Set f021fupdatedDtTm
     *
     * @param \DateTime $f021fupdatedDtTm
     *
     * @return T021fcustomers
     */
    public function setF021fupdatedDtTm($f021fupdatedDtTm)
    {
        $this->f021fupdatedDtTm = $f021fupdatedDtTm;

        return $this;
    }

    /**
     * Get f021fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fupdatedDtTm()
    {
        return $this->f021fupdatedDtTm;
    }

    /**
     * Set f021fidSupplier
     *
     * @param integer $f021fidSupplier
     *
     * @return T021fcustomers
     */
    public function setF021fidSupplier($f021fidSupplier)
    {
        $this->f021fidSupplier = $f021fidSupplier;

        return $this;
    }

    /**
     * Get f021fidSupplier
     *
     * @return integer
     */
    public function getF021fidSupplier()
    {
        return $this->f021fidSupplier;
    }

    /**
     * Set f021fidTaxFree
     *
     * @param integer $f021fidTaxFree
     *
     * @return T021fcustomers
     */
    public function setF021fidTaxFree($f021fidTaxFree)
    {
        $this->f021fidTaxFree = $f021fidTaxFree;

        return $this;
    }

    /**
     * Get f021fidTaxFree
     *
     * @return integer
     */
    public function getF021fidTaxFree()
    {
        return $this->f021fidTaxFree;
    }

    /**
     * Set f021fcontactPerson
     *
     * @param string $f021fcontactPerson
     *
     * @return T021fcustomers
     */
    public function setF021fcontactPerson($f021fcontactPerson)
    {
        $this->f021fcontactPerson = $f021fcontactPerson;

        return $this;
    }

    /**
     * Get f021fcontactPerson
     *
     * @return string
     */
    public function getF021fcontactPerson()
    {
        return $this->f021fcontactPerson;
    }

    /**
     * Set f021fdebtorCode
     *
     * @param string $f021fdebtorCode
     *
     * @return T021fcustomers
     */
    public function setF021fdebtorCode($f021fdebtorCode)
    {
        $this->f021fdebtorCode = $f021fdebtorCode;

        return $this;
    }

    /**
     * Get f021fdebtorCode
     *
     * @return string
     */
    public function getF021fdebtorCode()
    {
        return $this->f021fdebtorCode;
    }

    /**
     * Set f021fbank
     *
     * @param integer $f021fbank
     *
     * @return T021fcustomers
     */
    public function setF021fbank($f021fbank)
    {
        $this->f021fbank = $f021fbank;

        return $this;
    }

    /**
     * Get f021fbank
     *
     * @return integer
     */
    public function getF021fbank()
    {
        return $this->f021fbank;
    }

    /**
     * Set f021fbranch
     *
     * @param string $f021fbranch
     *
     * @return T021fcustomers
     */
    public function setF021fbranch($f021fbranch)
    {
        $this->f021fbranch = $f021fbranch;

        return $this;
    }

    /**
     * Get f021fbranch
     *
     * @return string
     */
    public function getF021fbranch()
    {
        return $this->f021fbranch;
    }

    /**
     * Set f021faccountNumber
     *
     * @param string $f021faccountNumber
     *
     * @return T021fcustomers
     */
    public function setF021faccountNumber($f021faccountNumber)
    {
        $this->f021faccountNumber = $f021faccountNumber;

        return $this;
    }

    /**
     * Get f021faccountNumber
     *
     * @return string
     */
    public function getF021faccountNumber()
    {
        return $this->f021faccountNumber;
    }

    /**
     * Set f021fdebtorCategory
     *
     * @param string $f021fdebtorCategory
     *
     * @return T021fcustomers
     */
    public function setF021fdebtorCategory($f021fdebtorCategory)
    {
        $this->f021fdebtorCategory = $f021fdebtorCategory;

        return $this;
    }

    /**
     * Get f021fdebtorCategory
     *
     * @return string
     */
    public function getF021fdebtorCategory()
    {
        return $this->f021fdebtorCategory;
    }

}
