<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T038finvestmentType
 *
 * @ORM\Table(name="t038finvestment_type", uniqueConstraints={@ORM\UniqueConstraint(name="f038ftype_UNIQUE", columns={"f038ftype"})})
 * @ORM\Entity(repositoryClass="Application\Repository\InvestmentTypeRepository")
 */
class T038finvestmentType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f038fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f038fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f038ftype", type="string", length=100, nullable=true)
     */
    private $f038ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f038faccount_code", type="string", length=50, nullable=true)
     */
    private $f038faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038fperson_name", type="string", length=50, nullable=true)
     */
    private $f038fpersonName;

    /**
     * @var string
     *
     * @ORM\Column(name="f038fcontact_number", type="string", length=50, nullable=true)
     */
    private $f038fcontactNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fstatus", type="integer", nullable=false)
     */
    private $f038fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fcreated_by", type="integer", nullable=true)
     */
    private $f038fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fupdated_by", type="integer", nullable=true)
     */
    private $f038fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f038fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f038fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f038fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f038fupdatedDtTm;


    /**
     * @var string
     *
     * @ORM\Column(name="f038ffund_code", type="string", length=255, nullable=true)
     */
    private $f038ffundCode;
    

    /**
     * @var string
     *
     * @ORM\Column(name="f038fdepartment_code", type="string", length=255, nullable=true)
     */
    private $f038fdepartmentCode;
    


    /**
     * @var string
     *
     * @ORM\Column(name="f038factivity_code", type="string", length=255, nullable=true)
     */
    private $f038factivityCode;
    



    /**
     * Get f038fid
     *
     * @return integer
     */
    public function getF038fid()
    {
        return $this->f038fid;
    }

    /**
     * Set f038ftype
     *
     * @param string $f038ftype
     *
     * @return T038finvestmentType
     */
    public function setF038ftype($f038ftype)
    {
        $this->f038ftype = $f038ftype;

        return $this;
    }

    /**
     * Get f038ftype
     *
     * @return string
     */
    public function getF038ftype()
    {
        return $this->f038ftype;
    }


    /**
     * Set f038faccountCode
     *
     * @param string $f038faccountCode
     *
     * @return T038finvestmentType
     */
    public function setF038faccountCode($f038faccountCode)
    {
        $this->f038faccountCode = $f038faccountCode;

        return $this;
    }

    /**
     * Get accountCode
     *
     * @return string
     */
    public function getF038faccountCode()
    {
        return $this->f038faccountCode;
    }



    /**
     * Set f038fstatus
     *
     * @param integer $f038fstatus
     *
     * @return T038finvestmentType
     */
    public function setF038fstatus($f038fstatus)
    {
        $this->f038fstatus = $f038fstatus;

        return $this;
    }

    /**
     * Get f038fstatus
     *
     * @return integer
     */
    public function getF038fstatus()
    {
        return $this->f038fstatus;
    }

    /**
     * Set f038fcreatedBy
     *
     * @param integer $f038fcreatedBy
     *
     * @return T038finvestmentType
     */
    public function setF038fcreatedBy($f038fcreatedBy)
    {
        $this->f038fcreatedBy = $f038fcreatedBy;

        return $this;
    }

    /**
     * Get f038fcreatedBy
     *
     * @return integer
     */
    public function getF038fcreatedBy()
    {
        return $this->f038fcreatedBy;
    }

    /**
     * Set f038fupdatedBy
     *
     * @param integer $f038fupdatedBy
     *
     * @return T038finvestmentType
     */
    public function setF038fupdatedBy($f038fupdatedBy)
    {
        $this->f038fupdatedBy = $f038fupdatedBy;

        return $this;
    }

    /**
     * Get f038fupdatedBy
     *
     * @return integer
     */
    public function getF038fupdatedBy()
    {
        return $this->f038fupdatedBy;
    }

    /**
     * Set f038fcreatedDtTm
     *
     * @param \DateTime $f038fcreatedDtTm
     *
     * @return T038finvestmentType
     */
    public function setF038fcreatedDtTm($f038fcreatedDtTm)
    {
        $this->f038fcreatedDtTm = $f038fcreatedDtTm;

        return $this;
    }

    /**
     * Get f038fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF038fcreatedDtTm()
    {
        return $this->f038fcreatedDtTm;
    }

    /**
     * Set f038fupdatedDtTm
     *
     * @param \DateTime $f038fupdatedDtTm
     *
     * @return T038finvestmentType
     */
    public function setF038fupdatedDtTm($f038fupdatedDtTm)
    {
        $this->f038fupdatedDtTm = $f038fupdatedDtTm;

        return $this;
    }

    /**
     * Get f038fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF038fupdatedDtTm()
    {
        return $this->f038fupdatedDtTm;
    }

    /**
     * Set f038fpersonName
     *
     * @param string $f038fpersonName
     *
     * @return T038finvestmentType
     */
    public function setF038fpersonName($f038fpersonName)
    {
        $this->f038fpersonName = $f038fpersonName;

        return $this;
    }

    /**
     * Get f038fpersonName
     *
     * @return string
     */
    public function getF038fpersonName()
    {
        return $this->f038fpersonName;
    }

    /**
     * Set f038fcontactNumber
     *
     * @param string $f038fcontactNumber
     *
     * @return T038finvestmentType
     */
    public function setF038fcontactNumber($f038fcontactNumber)
    {
        $this->f038fcontactNumber = $f038fcontactNumber;

        return $this;
    }

    /**
     * Get f038fcontactNumber
     *
     * @return string
     */
    public function getF038fcontactNumber()
    {
        return $this->f038fcontactNumber;
    }

    /**
     * Set f038ffundCode
     *
     * @param string $f038ffundCode
     *
     * @return T038finvestmentType
     */
    public function setF038ffundCode($f038ffundCode)
    {
        $this->f038ffundCode = $f038ffundCode;

        return $this;
    }

    /**
     * Get f038ffundCode
     *
     * @return string
     */
    public function getF038ffundCode()
    {
        return $this->f038ffundCode;
    }
    

    /**
     * Set f038fdepartmentCode
     *
     * @param string $f038fdepartmentCode
     *
     * @return T038finvestmentType
     */
    public function setF038fdepartmentCode($f038fdepartmentCode)
    {
        $this->f038fdepartmentCode = $f038fdepartmentCode;

        return $this;
    }

    /**
     * Get f038fdepartmentCode
     *
     * @return string
     */
    public function getF038fdepartmentCode()
    {
        return $this->f038fdepartmentCode;
    }


    /**
     * Set f038factivityCode
     *
     * @param string $f038factivityCode
     *
     * @return T038finvestmentType
     */
    public function setF038factivityCode($f038factivityCode)
    {
        $this->f038factivityCode = $f038factivityCode;

        return $this;
    }

    /**
     * Get f038factivityCode
     *
     * @return string
     */
    public function getF038factivityCode()
    {
        return $this->f038factivityCode;
    }

}
