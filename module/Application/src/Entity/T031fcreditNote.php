<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T031fcredit_note
 *
 * @ORM\Table(name="t031fcredit_note", indexes={@ORM\Index(name="f031fapproved_by", columns={"f031fapproved_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CreditNoteRepository")
 */
class T031fcreditNote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f031fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid_invoice", type="integer", nullable=true)
     */
    private $f031fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid_financial_year", type="integer", nullable=true)
     */
    private $f031fidFinancialYear;

     /**
     * @var string
     *
     * @ORM\Column(name="f031fdescription", type="string", length=100, nullable=true)
     */
    private $f031fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid_customer", type="integer", nullable=true)
     */
    private $f031fidCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="f031ftype", type="string", length=10, nullable=true)
     */
    private $f031ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031ftotal_amount", type="integer", nullable=true)
     */
    private $f031ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fbalance", type="integer", nullable=true)
     */
    private $f031fbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f031freference_number", type="string", length=50, nullable=true)
     */
    private $f031freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f031freason", type="string", length=60, nullable=true)
     */
    private $f031freason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031fdate", type="datetime", nullable=true)
     */
    private $f031fdate;


    /**
     * @var string
     *
     * @ORM\Column(name="f031fvoucher_type", type="string", length=10, nullable=true)
     */
    private $f031fvoucherType = 'CN';

    /**
     * @var string
     *
     * @ORM\Column(name="f031fvoucher_number", type="string", length=50, nullable=true)
     */
    private $f031fvoucherNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="f031fnarration", type="string", length=50, nullable=true)
     */
    private $f031fnarration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fvoucher_days", type="integer", nullable=true)
     */
    private $f031fvoucherDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fupdated_by", type="integer", nullable=true)
     */
    private $f031fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fcreated_by", type="integer", nullable=false)
     */
    private $f031fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fapproved_by", type="integer", nullable=true)
     */
    private $f031fapprovedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031created_dt_tm", type="datetime", nullable=false)
     */
    private $f031createdDtTm;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031updated_dt_tm", type="datetime", nullable=false)
     */
    private $f031updatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fstatus", type="integer", nullable=true)
     */
    private $f031fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031frevenue_type", type="integer", nullable=true)
     */
    private $f031frevenueType;



    /**
     * Get f031fid
     *
     * @return integer
     */
    public function getF031fid()
    {
        return $this->f031fid;
    }

    /**
     * Set f031frevenueType
     *
     * @param integer $f031frevenueType
     *
     * @return T031fcreditNote
     */
    public function setF031frevenueType($f031frevenueType)
    {
        $this->f031frevenueType = $f031frevenueType;

        return $this;
    }

    /**
     * Get f031frevenueType
     *
     * @return integer
     */
    public function getF031frevenueType()
    {
        return $this->f031frevenueType;
    }

    /**
     * Set f031fidInvoice
     *
     * @param integer $f031fidInvoice
     *
     * @return T031fcreditNote
     */
    public function setF031fidInvoice($f031fidInvoice)
    {
        $this->f031fidInvoice = $f031fidInvoice;

        return $this;
    }

    /**
     * Get f031fidInvoice
     *
     * @return integer
     */
    public function getF031fidInvoice()
    {
        return $this->f031fidInvoice;
    }

    /**
     * Set f031fidCustomer
     *
     * @param integer $f031fidCustomer
     *
     * @return T031fcreditNote
     */
    public function setF031fidCustomer($f031fidCustomer)
    {
        $this->f031fidCustomer = $f031fidCustomer;

        return $this;
    }

    /**
     * Get f031fidCustomer
     *
     * @return integer
     */
    public function getF031fidCustomer()
    {
        return $this->f031fidCustomer;
    }

    /**
     * Set f031ftotalAmount
     *
     * @param integer $f031ftotalAmount
     *
     * @return T031fcreditNote
     */
    public function setF031ftotalAmount($f031ftotalAmount)
    {
        $this->f031ftotalAmount = $f031ftotalAmount;

        return $this;
    }

    /**
     * Get f031ftotalAmount
     *
     * @return integer
     */
    public function getF031ftotalAmount()
    {
        return $this->f031ftotalAmount;
    }

    /**
     * Set f031fbalance
     *
     * @param integer $f031fbalance
     *
     * @return T031fcreditNote
     */
    public function setF031fbalance($f031fbalance)
    {
        $this->f031fbalance = $f031fbalance;

        return $this;
    }

    /**
     * Get f031fbalance
     *
     * @return integer
     */
    public function getF031fbalance()
    {
        return $this->f031fbalance;
    }

     /**
     * Set f031fidFinancialYear
     *
     * @param integer $f031fidFinancialYear
     *
     * @return T031fcreditNote
     */
    public function setF031fidFinancialYear($f031fidFinancialYear)
    {
        $this->f031fidFinancialYear = $f031fidFinancialYear;

        return $this;
    }

    /**
     * Get f031fidFinancialYear
     *
     * @return integer
     */
    public function getF031fidFinancialYear()
    {
        return $this->f031fidFinancialYear;
    }

     /**
     * Set f031ftype
     *
     * @param string $f031ftype
     *
     * @return T031fcreditNote
     */
    public function setF031ftype($f031ftype)
    {
        $this->f031ftype = $f031ftype;

        return $this;
    }

    /**
     * Get f031ftype
     *
     * @return string
     */
    public function getF031ftype()
    {
        return $this->f031ftype;
    }

    /**
     * Set f031freferenceNumber
     *
     * @param string $f031freferenceNumber
     *
     * @return T031fcreditNote
     */
    public function setF031freferenceNumber($f031freferenceNumber)
    {
        $this->f031freferenceNumber = $f031freferenceNumber;

        return $this;
    }

    /**
     * Get f031freferenceNumber
     *
     * @return string
     */
    public function getF031freferenceNumber()
    {
        return $this->f031freferenceNumber;
    }

    /**
     * Set f031fdate
     *
     * @param \DateTime $f031fdate
     *
     * @return T031fcreditNote
     */
    public function setF031fdate($f031fdate)
    {
        $this->f031fdate = $f031fdate;

        return $this;
    }

    /**
     * Get f031fdate
     *
     * @return \DateTime
     */
    public function getF031fdate()
    {
        return $this->f031fdate;
    }


    /**
     * Set f031fvoucherType
     *
     * @param string $f031fvoucherType
     *
     * @return T031fcreditNote
     */
    public function setF031fvoucherType($f031fvoucherType)
    {
        $this->f031fvoucherType = $f031fvoucherType;

        return $this;
    }

    /**
     * Get f031fvoucherType
     *
     * @return string
     */
    public function getF031fvoucherType()
    {
        return $this->f031fvoucherType;
    }

    /**
     * Set f031fvoucherNumber
     *
     * @param string $f031fvoucherNumber
     *
     * @return T031fcreditNote
     */
    public function setF031fvoucherNumber($f031fvoucherNumber)
    {
        $this->f031fvoucherNumber = $f031fvoucherNumber;

        return $this;
    }

    /**
     * Get f031fvoucherNumber
     *
     * @return string
     */
    public function getF031fvoucherNumber()
    {
        return $this->f031fvoucherNumber;
    }
    /**
     * Get f031fnarration
     *
     * @return string
     */
    public function getF031fnarration()
    {
        return $this->f031fnarration;
    }


    /**
     * Set f031fnarration
     *
     * @param string $f031fnarration
     *
     * @return T031fcreditNote
     */
    public function setF031fnarration($f031fnarration)
    {
        $this->f031fnarration = $f031fnarration;

        return $this;
    }


    /**
     * Set f031fvoucherDays
     *
     * @param integer $f031fvoucherDays
     *
     * @return T031fvoucher
     */
    public function setF031fvoucherDays($f031fvoucherDays)
    {
        $this->f031fvoucherDays = $f031fvoucherDays;

        return $this;
    }

    /**
     * Get f031fvoucherDays
     *
     * @return integer
     */
    public function getF031fvoucherDays()
    {
        return $this->f031fvoucherDays;
    }

    /**
     * Set f031fupdatedBy
     *
     * @param integer $f031fupdatedBy
     *
     * @return T031fcreditNote
     */
    public function setF031fupdatedBy($f031fupdatedBy)
    {
        $this->f031fupdatedBy = $f031fupdatedBy;

        return $this;
    }

    /**
     * Get f031fupdatedBy
     *
     * @return integer
     */
    public function getF031fupdatedBy()
    {
        return $this->f031fupdatedBy;
    }

    /**
     * Set f031fcreatedBy
     *
     * @param integer $f031fcreatedBy
     *
     * @return T031fcreditNote
     */
    public function setF031fcreatedBy($f031fcreatedBy)
    {
        $this->f031fcreatedBy = $f031fcreatedBy;

        return $this;
    }

    /**
     * Get f031fcreatedBy
     *
     * @return integer
     */
    public function getF031fcreatedBy()
    {
        return $this->f031fcreatedBy;
    }

    /**
     * Set f031fapprovedBy
     *
     * @param integer $f031fapprovedBy
     *
     * @return T031fcreditNote
     */
    public function setF031fapprovedBy($f031fapprovedBy)
    {
        $this->f031fapprovedBy = $f031fapprovedBy;

        return $this;
    }

    /**
     * Get f031fapprovedBy
     *
     * @return integer
     */
    public function getF031fapprovedBy()
    {
        return $this->f031fapprovedBy;
    }

    /**
     * Set f031createdDtTm
     *
     * @param \DateTime $f031createdDtTm
     *
     * @return T031fcreditNote
     */
    public function setF031createdDtTm($f031createdDtTm)
    {
        $this->f031createdDtTm = $f031createdDtTm;

        return $this;
    }

    /**
     * Get f031createdDtTm
     *
     * @return \DateTime
     */
    public function getF031createdDtTm()
    {
        return $this->f031createdDtTm;
    }


    /**
     * Set f031updatedDtTm
     *
     * @param \DateTime $f031updatedDtTm
     *
     * @return T031fcreditNote
     */
    public function setF031updatedDtTm($f031updatedDtTm)
    {
        $this->f031updatedDtTm = $f031updatedDtTm;

        return $this;
    }

    /**
     * Get f031updatedDtTm
     *
     * @return \DateTime
     */
    public function getF031updatedDtTm()
    {
        return $this->f031updatedDtTm;
    }
    /**
     * Set f031fstatus
     *
     * @param integer $f031fstatus
     *
     * @return T031fcreditNote
     */
    public function setF031fstatus($f031fstatus)
    {
        $this->f031fstatus = $f031fstatus;

        return $this;
    }

    /**
     * Get f031fstatus
     *
     * @return integer
     */
    public function getF031fstatus()
    {
        return $this->f031fstatus;
    }

     /**
     * Set f031fdescription
     *
     * @param string $f031fdescription
     *
     * @return T031fcreditNote
     */
    public function setF031fdescription($f031fdescription)
    {
        $this->f031fdescription = $f031fdescription;

        return $this;
    }

    /**
     * Get f031fdescription
     *
     * @return string
     */
    public function getF031fdescription()
    {
        return $this->f031fdescription;
    }

     /**
     * Set f031freason
     *
     * @param string $f031freason
     *
     * @return T031fcreditNote
     */
    public function setF031freason($f031freason)
    {
        $this->f031freason = $f031freason;

        return $this;
    }

    /**
     * Get f031freason
     *
     * @return string
     */
    public function getF031freason()
    {
        return $this->f031freason;
    }

}