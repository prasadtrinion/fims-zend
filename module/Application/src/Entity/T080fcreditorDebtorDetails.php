<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T080fcreditorDebtorDetails
 *
 * @ORM\Table(name="t080fcreditor_debtor_details", indexes={@ORM\Index(name="f080fid_creditor_debtor", columns={"f080fid_creditor_debtor"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CreditorDebtorRepository")
 */
class T080fcreditorDebtorDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f080fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f080fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fid_item", type="integer", nullable=false)
     */
    private $f080fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fid_glcode", type="integer", nullable=false)
     */
    private $f080fidGlcode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f080fgst", type="integer", nullable=true)
     */
    private $f080fgst;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fdiscount", type="integer", nullable=true)
     */
    private $f080fdiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fquantity", type="integer", nullable=false)
     */
    private $f080fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fprice", type="integer", nullable=false)
     */
    private $f080fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080ftotal", type="integer", nullable=false)
     */
    private $f080ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fstatus", type="integer", nullable=false)
     */
    private $f080fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fcreated_by", type="integer", nullable=false)
     */
    private $f080fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f080fupdated_by", type="integer", nullable=false)
     */
    private $f080fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f080fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f080fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f080fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f080fupdatedDtTm;

    /**
     * @var \Application\Entity\T079fcreditorDebtor
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T079fcreditorDebtor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f080fid_creditor_debtor", referencedColumnName="f079fid")
     * })
     */
    private $f080fidCreditorDebtor;

    

    /**
     * Get f080fid
     *
     * @return integer
     */
    public function getF080fid()
    {
        return $this->f080fid;
    }

    /**
     * Set f080fidItem
     *
     * @param integer $f080fidItem
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fidItem($f080fidItem)
    {
        $this->f080fidItem = $f080fidItem;

        return $this;
    }

    /**
     * Get f080fidItem
     *
     * @return integer
     */
    public function getF080fidItem()
    {
        return $this->f080fidItem;
    }

    /**
     * Set f080fidGlcode
     *
     * @param integer $f080fidGlcode
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fidGlcode($f080fidGlcode)
    {
        $this->f080fidGlcode = $f080fidGlcode;

        return $this;
    }

    /**
     * Get f080fidGlcode
     *
     * @return integer
     */
    public function getF080fidGlcode()
    {
        return $this->f080fidGlcode;
    }

    /**
     * Set f080fgst
     *
     * @param integer $f080fgst
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fgst($f080fgst)
    {
        $this->f080fgst = $f080fgst;

        return $this;
    }

    /**
     * Get f080fgst
     *
     * @return integer
     */
    public function getF080fgst()
    {
        return $this->f080fgst;
    }

    /**
     * Set f080fdiscount
     *
     * @param integer $f080fdiscount
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fdiscount($f080fdiscount)
    {
        $this->f080fdiscount = $f080fdiscount;

        return $this;
    }

    /**
     * Get f080fdiscount
     *
     * @return integer
     */
    public function getF080fdiscount()
    {
        return $this->f080fdiscount;
    }

    /**
     * Set f080fquantity
     *
     * @param integer $f080fquantity
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fquantity($f080fquantity)
    {
        $this->f080fquantity = $f080fquantity;

        return $this;
    }

    /**
     * Get f080fquantity
     *
     * @return integer
     */
    public function getF080fquantity()
    {
        return $this->f080fquantity;
    }

    /**
     * Set f080fprice
     *
     * @param integer $f080fprice
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fprice($f080fprice)
    {
        $this->f080fprice = $f080fprice;

        return $this;
    }

    /**
     * Get f080fprice
     *
     * @return integer
     */
    public function getF080fprice()
    {
        return $this->f080fprice;
    }

    /**
     * Set f080ftotal
     *
     * @param integer $f080ftotal
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080ftotal($f080ftotal)
    {
        $this->f080ftotal = $f080ftotal;

        return $this;
    }

    /**
     * Get f080ftotal
     *
     * @return integer
     */
    public function getF080ftotal()
    {
        return $this->f080ftotal;
    }

    /**
     * Set f080fstatus
     *
     * @param integer $f080fstatus
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fstatus($f080fstatus)
    {
        $this->f080fstatus = $f080fstatus;

        return $this;
    }

    /**
     * Get f080fstatus
     *
     * @return integer
     */
    public function getF080fstatus()
    {
        return $this->f080fstatus;
    }

    /**
     * Set f080fcreatedBy
     *
     * @param integer $f080fcreatedBy
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fcreatedBy($f080fcreatedBy)
    {
        $this->f080fcreatedBy = $f080fcreatedBy;

        return $this;
    }

    /**
     * Get f080fcreatedBy
     *
     * @return integer
     */
    public function getF080fcreatedBy()
    {
        return $this->f080fcreatedBy;
    }

    /**
     * Set f080fupdatedBy
     *
     * @param integer $f080fupdatedBy
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fupdatedBy($f080fupdatedBy)
    {
        $this->f080fupdatedBy = $f080fupdatedBy;

        return $this;
    }

    /**
     * Get f080fupdatedBy
     *
     * @return integer
     */
    public function getF080fupdatedBy()
    {
        return $this->f080fupdatedBy;
    }

    /**
     * Set f080fcreatedDtTm
     *
     * @param \DateTime $f080fcreatedDtTm
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fcreatedDtTm($f080fcreatedDtTm)
    {
        $this->f080fcreatedDtTm = $f080fcreatedDtTm;

        return $this;
    }

    /**
     * Get f080fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF080fcreatedDtTm()
    {
        return $this->f080fcreatedDtTm;
    }

    /**
     * Set f080fupdatedDtTm
     *
     * @param \DateTime $f080fupdatedDtTm
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fupdatedDtTm($f080fupdatedDtTm)
    {
        $this->f080fupdatedDtTm = $f080fupdatedDtTm;

        return $this;
    }

    /**
     * Get f080fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF080fupdatedDtTm()
    {
        return $this->f080fupdatedDtTm;
    }

    /**
     * Set f080fidCreditorDebtor
     *
     * @param \Application\Entity\T079fcreditorDebtor $f080fidCreditorDebtor
     *
     * @return T080fcreditorDebtorDetails
     */
    public function setF080fidCreditorDebtor(\Application\Entity\T079fcreditorDebtor $f080fidCreditorDebtor = null)
    {
        $this->f080fidCreditorDebtor = $f080fidCreditorDebtor;

        return $this;
    }

    /**
     * Get f080fidCreditorDebtor
     *
     * @return \Application\Entity\T079fcreditorDebtor
     */
    public function getF080fidCreditorDebtor()
    {
        return $this->f080fidCreditorDebtor;
    }
}
