<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T030fvendorRegistration
 *
 * @ORM\Table(name="t030fvendor_registration", uniqueConstraints={@ORM\UniqueConstraint(name="EMAIL_UNIQUE", columns={"f030femail"})})
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T030fvendorRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f030fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f030fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fcompany_name", type="string", length=25, nullable=true)
     */
    private $f030fcompanyName;

    /**
     * @var string
     *
     * @ORM\Column(name="f030femail", type="string", length=25, nullable=true)
     */
    private $f030femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f030faddress1", type="string", length=100, nullable=true)
     */
    private $f030faddress1 = 'null1';

    /**
     * @var string
     *
     * @ORM\Column(name="f030faddress2", type="string", length=100, nullable=true)
     */
    private $f030faddress2 = 'null1';

    /**
     * @var string
     *
     * @ORM\Column(name="f030faddress3", type="string", length=100, nullable=true)
     */
    private $f030faddress3 = 'null1';

    /**
     * @var string
     *
     * @ORM\Column(name="f030faddress4", type="string", length=100, nullable=true)
     */
    private $f030faddress4 = 'null1';

    /**
     * @var string
     *
     * @ORM\Column(name="f030fphone", type="string", length=20, nullable=true)
     */
    private $f030fphone;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030ffax", type="string", length=30, nullable=true)
     */
    private $f030ffax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fstatus", type="integer", nullable=true)
     */
    private $f030fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fapproval_status", type="integer", nullable=true)
     */
    private $f030fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fcreated_by", type="integer", nullable=true)
     */
    private $f030fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fupdated_by", type="integer", nullable=true)
     */
    private $f030fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f030fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f030fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fcompany_registration", type="string", length=225, nullable=true)
     */
    private $f030fcompanyRegistration;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fvendor_code", type="string", length=20, nullable=true)
     */
    private $f030fvendorCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fstart_date", type="datetime", nullable=false)
     */
    private $f030fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fend_date", type="datetime", nullable=false)
     */
    private $f030fendDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fcity", type="string", length=50, nullable=true)
     */
    private $f030fcity;


    /**
     * @var string
     *
     * @ORM\Column(name="f030frace", type="integer", nullable=true)
     */
    private $f030frace;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fuser_status", type="integer", nullable=true)
     */
    private $f030fuserStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fpost_code", type="string", length=20, nullable=true)
     */
    private $f030fpostCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fstate", type="integer", nullable=false)
     */
    private $f030fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fcountry", type="integer", nullable=false)
     */
    private $f030fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fwebsite", type="string", length=30, nullable=true)
     */
    private $f030fwebsite;

      /**
     * @var string
     *
     * @ORM\Column(name="f030fid_bank", type="integer", nullable=false)
     */
    private $f030fidBank;

      /**
     * @var string
     *
     * @ORM\Column(name="f030faccount_number", type="string", length=30, nullable=false)
     */
    private $f030faccountNumber;

     /**
     * @var string
     *
     * @ORM\Column(name="f030fpassword", type="string", length=150, nullable=true)
     */
    private $f030fpassword = 'e10adc3949ba59abbe56e057f20f883e';

     /**
     * @var string
     *
     * @ORM\Column(name="f030ftoken", type="string", length=150, nullable=true)
     */
    private $f030ftoken = 'vendor';

     /**
     * @var string
     *
     * @ORM\Column(name="f030ffile", type="string", length=150, nullable=true)
     */
    private $f030ffile ;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fpay_to_profile", type="string", length=150, nullable=true)
     */
    private $f030fpayToProfile;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fdebtor_code", type="string", length=150, nullable=true)
     */
    private $f030fdebtorCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fnumber_receipt", type="string", length=30, nullable=true)
     */
    private $f030fnumberReceipt;

    /**
     * @var string
     *
     * @ORM\Column(name="f030freason", type="string", length=50, nullable=true)
     */
    private $f030freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f030fcompany_reg_no", type="string", length=60, nullable=true)
     */
    private $f030fcompanyRegNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f030faccount_no", type="string", length=60, nullable=true)
     */
    private $f030faccountNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f030faccount_code", type="string", length=60, nullable=true)
     */
    private $f030faccountCode;

    /**
     * Get f030fid
     *
     * @return integer
     */
    public function getF030fid()
    {
        return $this->f030fid;
    }

    /**
     * Set f030fcompanyName
     *
     * @param string $f030fcompanyName
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcompanyName($f030fcompanyName)
    {
        $this->f030fcompanyName = $f030fcompanyName;

        return $this;
    }

    /**
     * Get f030fcompanyName
     *
     * @return string
     */
    public function getF030fcompanyName()
    {
        return $this->f030fcompanyName;
    }

     /**
     * Set f030fpayToProfile
     *
     * @param string $f030fpayToProfile
     *
     * @return T030fvendorRegistration
     */
    public function setF030fpayToProfile($f030fpayToProfile)
    {
        $this->f030fpayToProfile = $f030fpayToProfile;

        return $this;
    }

    /**
     * Get f030fpayToProfile
     *
     * @return string
     */
    public function getF030fpayToProfile()
    {
        return $this->f030fpayToProfile;
    }

     /**
     * Set f030fdebtorCode
     *
     * @param string $f030fdebtorCode
     *
     * @return T030fvendorRegistration
     */
    public function setF030fdebtorCode($f030fdebtorCode)
    {
        $this->f030fdebtorCode = $f030fdebtorCode;

        return $this;
    }

    /**
     * Get f030fdebtorCode
     *
     * @return string
     */
    public function getF030fdebtorCode()
    {
        return $this->f030fdebtorCode;
    }

    /**
     * Set f030frace
     *
     * @param string $f030frace
     *
     * @return T030fvendorRegistration
     */
    public function setF030frace($f030frace)
    {
        $this->f030frace = $f030frace;

        return $this;
    }

    /**
     * Get f030frace
     *
     * @return string
     */
    public function getF030frace()
    {
        return $this->f030frace;
    }
        

    /**
     * Set f030femail
     *
     * @param string $f030femail
     *
     * @return T030fvendorRegistration
     */
    public function setF030femail($f030femail)
    {
        $this->f030femail = $f030femail;

        return $this;
    }

    /**
     * Get f030femail
     *
     * @return string
     */
    public function getF030femail()
    {
        return $this->f030femail;
    }

    /**
     * Set f030faddress4
     *
     * @param string $f030faddress4
     *
     * @return T030fvendorRegistration
     */
    public function setF030faddress4($f030faddress4)
    {
        $this->f030faddress4 = $f030faddress4;

        return $this;
    }

    /**
     * Get f030faddress4
     *
     * @return string
     */
    public function getF030faddress4()
    {
        return $this->f030faddress4;
    }

    /**
     * Set f030fuserStatus
     *
     * @param string $f030fuserStatus
     *
     * @return T030fvendorRegistration
     */
    public function setF030fuserStatus($f030fuserStatus)
    {
        $this->f030fuserStatus = $f030fuserStatus;

        return $this;
    }

    /**
     * Get f030fuserStatus
     *
     * @return string
     */
    public function getF030fuserStatus()
    {
        return $this->f030fuserStatus;
    }

    /**
     * Set f030faddress1
     *
     * @param string $f030faddress1
     *
     * @return T030fvendorRegistration
     */
    public function setF030faddress1($f030faddress1)
    {
        $this->f030faddress1 = $f030faddress1;

        return $this;
    }

    /**
     * Get f030faddress1
     *
     * @return string
     */
    public function getF030faddress1()
    {
        return $this->f030faddress1;
    }

    /**
     * Set f030faddress2
     *
     * @param string $f030faddress2
     *
     * @return T030fvendorRegistration
     */
    public function setF030faddress2($f030faddress2)
    {
        $this->f030faddress2 = $f030faddress2;

        return $this;
    }

    /**
     * Get f030faddress2
     *
     * @return string
     */
    public function getF030faddress2()
    {
        return $this->f030faddress2;
    }

    /**
     * Set f030faddress3
     *
     * @param string $f030faddress3
     *
     * @return T030fvendorRegistration
     */
    public function setF030faddress3($f030faddress3)
    {
        $this->f030faddress3 = $f030faddress3;

        return $this;
    }

    /**
     * Get f030faddress3
     *
     * @return string
     */
    public function getF030faddress3()
    {
        return $this->f030faddress3;
    }

    /**
     * Set f030fphone
     *
     * @param string $f030fphone
     *
     * @return T030fvendorRegistration
     */
    public function setF030fphone($f030fphone)
    {
        $this->f030fphone = $f030fphone;

        return $this;
    }

    /**
     * Get f030fphone
     *
     * @return string
     */
    public function getF030fphone()
    {
        return $this->f030fphone;
    }

    /**
     * Set f030ffax
     *
     * @param integer $f030ffax
     *
     * @return T030fvendorRegistration
     */
    public function setF030ffax($f030ffax)
    {
        $this->f030ffax = $f030ffax;

        return $this;
    }

    /**
     * Get f030ffax
     *
     * @return integer
     */
    public function getF030ffax()
    {
        return $this->f030ffax;
    }

    /**
     * Set f030fstatus
     *
     * @param integer $f030fstatus
     *
     * @return T030fvendorRegistration
     */
    public function setF030fstatus($f030fstatus)
    {
        $this->f030fstatus = $f030fstatus;

        return $this;
    }

    /**
     * Get f030fstatus
     *
     * @return integer
     */
    public function getF030fstatus()
    {
        return $this->f030fstatus;
    }

    /**
     * Set f030fapprovalStatus
     *
     * @param integer $f030fapprovalStatus
     *
     * @return T030fvendorRegistration
     */
    public function setF030fapprovalStatus($f030fapprovalStatus)
    {
        $this->f030fapprovalStatus = $f030fapprovalStatus;

        return $this;
    }

    /**
     * Get f030fapprovalStatus
     *
     * @return integer
     */
    public function getF030fapprovalStatus()
    {
        return $this->f030fapprovalStatus;
    }

    /**
     * Set f030fcreatedBy
     *
     * @param integer $f030fcreatedBy
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcreatedBy($f030fcreatedBy)
    {
        $this->f030fcreatedBy = $f030fcreatedBy;

        return $this;
    }

    /**
     * Get f030fcreatedBy
     *
     * @return integer
     */
    public function getF030fcreatedBy()
    {
        return $this->f030fcreatedBy;
    }

    /**
     * Set f030fupdatedBy
     *
     * @param integer $f030fupdatedBy
     *
     * @return T030fvendorRegistration
     */
    public function setF030fupdatedBy($f030fupdatedBy)
    {
        $this->f030fupdatedBy = $f030fupdatedBy;

        return $this;
    }

    /**
     * Get f030fupdatedBy
     *
     * @return integer
     */
    public function getF030fupdatedBy()
    {
        return $this->f030fupdatedBy;
    }

    /**
     * Set f030fcreatedDtTm
     *
     * @param \DateTime $f030fcreatedDtTm
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcreatedDtTm($f030fcreatedDtTm)
    {
        $this->f030fcreatedDtTm = $f030fcreatedDtTm;

        return $this;
    }

    /**
     * Get f030fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fcreatedDtTm()
    {
        return $this->f030fcreatedDtTm;
    }

    /**
     * Set f030fupdatedDtTm
     *
     * @param \DateTime $f030fupdatedDtTm
     *
     * @return T030fvendorRegistration
     */
    public function setF030fupdatedDtTm($f030fupdatedDtTm)
    {
        $this->f030fupdatedDtTm = $f030fupdatedDtTm;

        return $this;
    }

    /**
     * Get f030fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fupdatedDtTm()
    {
        return $this->f030fupdatedDtTm;
    }

    /**
     * Set f030fcompanyRegistration
     *
     * @param string $f030fcompanyRegistration
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcompanyRegistration($f030fcompanyRegistration)
    {
        $this->f030fcompanyRegistration = $f030fcompanyRegistration;

        return $this;
    }

    /**
     * Get f030fcompanyRegistration
     *
     * @return string
     */
    public function getF030fcompanyRegistration()
    {
        return $this->f030fcompanyRegistration;
    }

    /**
     * Set f030fvendorCode
     *
     * @param string $f030fvendorCode
     *
     * @return T030fvendorRegistration
     */
    public function setF030fvendorCode($f030fvendorCode)
    {
        $this->f030fvendorCode = $f030fvendorCode;

        return $this;
    }

    /**
     * Get f030fvendorCode
     *
     * @return string
     */
    public function getF030fvendorCode()
    {
        return $this->f030fvendorCode;
    }

    /**
     * Set f030fstartDate
     *
     * @param \DateTime $f030fstartDate
     *
     * @return T030fvendorRegistration
     */
    public function setF030fstartDate($f030fstartDate)
    {
        $this->f030fstartDate = $f030fstartDate;

        return $this;
    }

    /**
     * Get f030fstartDate
     *
     * @return \DateTime
     */
    public function getF030fstartDate()
    {
        return $this->f030fstartDate;
    }

    /**
     * Set f030fendDate
     *
     * @param \DateTime $f030fendDate
     *
     * @return T030fvendorRegistration
     */
    public function setF030fendDate($f030fendDate)
    {
        $this->f030fendDate = $f030fendDate;

        return $this;
    }

    /**
     * Get f030fendDate
     *
     * @return \DateTime
     */
    public function getF030fendDate()
    {
        return $this->f030fendDate;
    }

    /**
     * Set f030fcity
     *
     * @param string $f030fcity
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcity($f030fcity)
    {
        $this->f030fcity = $f030fcity;

        return $this;
    }

    /**
     * Get f030fcity
     *
     * @return string
     */
    public function getF030fcity()
    {
        return $this->f030fcity;
    }

    /**
     * Set f030fpostCode
     *
     * @param string $f030fpostCode
     *
     * @return T030fvendorRegistration
     */
    public function setF030fpostCode($f030fpostCode)
    {
        $this->f030fpostCode = $f030fpostCode;

        return $this;
    }

    /**
     * Get f030fpostCode
     *
     * @return string
     */
    public function getF030fpostCode()
    {
        return $this->f030fpostCode;
    }

    /**
     * Set f030fstate
     *
     * @param integer $f030fstate
     *
     * @return T030fvendorRegistration
     */
    public function setF030fstate($f030fstate)
    {
        $this->f030fstate = $f030fstate;

        return $this;
    }

    /**
     * Get f030fstate
     *
     * @return integer
     */
    public function getF030fstate()
    {
        return $this->f030fstate;
    }

    /**
     * Set f030fcountry
     *
     * @param integer $f030fcountry
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcountry($f030fcountry)
    {
        $this->f030fcountry = $f030fcountry;

        return $this;
    }

    /**
     * Get f030fcountry
     *
     * @return integer
     */
    public function getF030fcountry()
    {
        return $this->f030fcountry;
    }

    /**
     * Set f030fwebsite
     *
     * @param string $f030fwebsite
     *
     * @return T030fvendorRegistration
     */
    public function setF030fwebsite($f030fwebsite)
    {
        $this->f030fwebsite = $f030fwebsite;

        return $this;
    }

    /**
     * Get f030fwebsite
     *
     * @return string
     */
    public function getF030fwebsite()
    {
        return $this->f030fwebsite;
    }


    

    /**
     * Set f030fidBank
     *
     * @param string $f030fidBank
     *
     * @return T030fvendorRegistration
     */
    public function setF030fidBank($f030fidBank)
    {
        $this->f030fidBank = $f030fidBank;

        return $this;
    }

    /**
     * Get f030fidBank
     *
     * @return string
     */
    public function getF030fidBank()
    {
        return $this->f030fidBank;
    }

    /**
     * Set f030faccountNumber
     *
     * @param string $f030faccountNumber
     *
     * @return T030fvendorRegistration
     */
    public function setF030faccountNumber($f030faccountNumber)
    {
        $this->f030faccountNumber = $f030faccountNumber;

        return $this;
    }

    /**
     * Get f030faccountNumber
     *
     * @return string
     */
    public function getF030faccountNumber()
    {
        return $this->f030faccountNumber;
    }

     /**
     * Set f030fpassword
     *
     * @param string $f030fpassword
     *
     * @return T030fvendorRegistration
     */
    public function setF030fpassword($f030fpassword)
    {
        $this->f030fpassword = $f030fpassword;

        return $this;
    }

    /**
     * Get f030fpassword
     *
     * @return string
     */
    public function getF030fpassword()
    {
        return $this->f030fpassword;
    }

     /**
     * Set f030ftoken
     *
     * @param string $f030ftoken
     *
     * @return T030fvendorRegistration
     */
    public function setF030ftoken($f030ftoken)
    {
        $this->f030ftoken = $f030ftoken;

        return $this;
    }

    /**
     * Get f030ffile
     *
     * @return string
     */
    public function getF030ffile()
    {
        return $this->f030ffile;
    }
     /**
     * Set f030ffile
     *
     * @param string $f030ffile
     *
     * @return T030fvendorRegistration
     */
    public function setF030ffile($f030ffile)
    {
        $this->f030ffile = $f030ffile;

        return $this;
    }

    /**
     * Get f030ftoken
     *
     * @return string
     */
    public function getF030ftoken()
    {
        return $this->f030ftoken;
    }

     /**
     * Set f030fnumberReceipt
     *
     * @param string $f030fnumberReceipt
     *
     * @return T030fvendorRegistration
     */
    public function setF030fnumberReceipt($f030fnumberReceipt)
    {
        $this->f030fnumberReceipt = $f030fnumberReceipt;

        return $this;
    }

    /**
     * Get f030fnumberReceipt
     *
     * @return string
     */
    public function getF030fnumberReceipt()
    {
        return $this->f030fnumberReceipt;
    }

    /**
     * Set f030freason
     *
     * @param string $f030freason
     *
     * @return T030fvendorRegistration
     */
    public function setF030freason($f030freason)
    {
        $this->f030freason = $f030freason;

        return $this;
    }

    /**
     * Get f030freason
     *
     * @return string
     */
    public function getF030freason()
    {
        return $this->f030freason;
    }

    /**
     * Set f030fcompanyRegNo
     *
     * @param string $f030fcompanyRegNo
     *
     * @return T030fvendorRegistration
     */
    public function setF030fcompanyRegNo($f030fcompanyRegNo)
    {
        $this->f030fcompanyRegNo = $f030fcompanyRegNo;

        return $this;
    }

    /**
     * Get f030fcompanyRegNo
     *
     * @return string
     */
    public function getF030fcompanyRegNo()
    {
        return $this->f030fcompanyRegNo;
    }

    /**
     * Set f030faccountNo
     *
     * @param string $f030faccountNo
     *
     * @return T030fvendorRegistration
     */
    public function setF030faccountNo($f030faccountNo)
    {
        $this->f030faccountNo = $f030faccountNo;

        return $this;
    }

    /**
     * Get f030faccountNo
     *
     * @return string
     */
    public function getF030faccountNo()
    {
        return $this->f030faccountNo;
    }

    /**
     * Set f030faccountCode
     *
     * @param string $f030faccountCode
     *
     * @return T030fvendorRegistration
     */
    public function setF030faccountCode($f030faccountCode)
    {
        $this->f030faccountCode = $f030faccountCode;

        return $this;
    }

    /**
     * Get f030faccountCode
     *
     * @return string
     */
    public function getF030faccountCode()
    {
        return $this->f030faccountCode;
    }

}