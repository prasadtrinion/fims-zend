<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T144fwarrantDetails
 *
 * @ORM\Table(name="t144fwarrant_details")
 * @ORM\Entity(repositoryClass="Application\Repository\WarrantRepository")
 */
class T144fwarrantDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f144fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f144fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f144fclass", type="string", length=255, nullable=false)
     */
    private $f144fclass;

    /**
     * @var string
     *
     * @ORM\Column(name="f144fid_warrant", type="integer", nullable=false)
     */
    private $f144fidWarrant;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144ffrom", type="string", length=255, nullable=false)
     * 
     */
    private $f144ffrom;

     /**
     * @var integer
     *
     * @ORM\Column(name="f144fto", type="string", length=255, nullable=false)
     * 
     */
    private $f144fto;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144fid_staff", type="integer", nullable=false)
     * 
     */
    private $f144fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144fdate", type="datetime", nullable=false)
     * 
     */
    private $f144fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144famount", type="integer", nullable=false)
     * 
     */
    private $f144famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144fstatus", type="integer", nullable=false)
     */
    private $f144fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f144fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f144fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f144fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f144fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f144fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f144fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f144fupdatedDtTm;
 	

    /**
     * Get f144fid
     *
     * @return integer
     */
    public function getF144fid()
    {
        return $this->f144fid;
    }

    /**
     * Set f144fclass
     *
     * @param string $f144fclass
     *
     * @return T144fwarrantDetails
     */
    public function setF144fclass($f144fclass)
    {
        $this->f144fclass = $f144fclass;

        return $this;
    }

    /**
     * Get f144fclass
     *
     * @return string
     */
    public function getF144fclass()
    {
        return $this->f144fclass;
    }

    /**
     * Set f144fidWarrant
     *
     * @param string $f144fidWarrant
     *
     * @return T144fwarrantDetails
     */
    public function setF144fidWarrant($f144fidWarrant)
    {
        $this->f144fidWarrant = $f144fidWarrant;

        return $this;
    }

    /**
     * Get f144fidWarrant
     *
     * @return string
     */
    public function getF144fidWarrant()
    {
        return $this->f144fidWarrant;
    }


    /**
     * Set f144ffrom
     *
     * @param integer $f144ffrom
     *
     * @return T144fwarrantDetails
     */
    public function setF144ffrom($f144ffrom)
    {
        $this->f144ffrom = $f144ffrom;

        return $this;
    }

    /**
     * Get f144ffrom
     *
     * @return integer
     */
    public function getF144ffrom()
    {
        return $this->f144ffrom;
    }

    /**
     * Set f144fto
     *
     * @param integer $f144fto
     *
     * @return T144fwarrantDetails
     */
    public function setF144fto($f144fto)
    {
        $this->f144fto = $f144fto;

        return $this;
    }

    /**
     * Get f144fto
     *
     * @return integer
     */
    public function getF144fto()
    {
        return $this->f144fto;
    }

    /**
     * Set f144fidStaff
     *
     * @param integer $f144fidStaff
     *
     * @return T144fwarrantDetails
     */
    public function setF144fidStaff($f144fidStaff)
    {
        $this->f144fidStaff = $f144fidStaff;

        return $this;
    }

    /**
     * Get f144fidStaff
     *
     * @return integer
     */
    public function getF144fidStaff()
    {
        return $this->f144fidStaff;
    }


    /**
     * Set f144fupdatedDtTm
     *
     * @param \DateTime $f144fupdatedDtTm
     *
     * @return T144fwarrantDetails
     */
    public function setF144fupdatedDtTm($f144fupdatedDtTm)
    {
        $this->f144fupdatedDtTm = $f144fupdatedDtTm;

        return $this;
    }

    /**
     * Get f144fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF144fupdatedDtTm()
    {
        return $this->f144fupdatedDtTm;
    }

      /**
     * Set f144fcreatedDtTm
     *
     * @param \DateTime $f144fcreatedDtTm
     *
     * @return T144fwarrantDetails
     */
    public function setF144fcreatedDtTm($f144fcreatedDtTm)
    {
        $this->f144fcreatedDtTm = $f144fcreatedDtTm;

        return $this;
    }

    /**
     * Get f144fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF144fcreatedDtTm()
    {
        return $this->f144fcreatedDtTm;
    }

    /**
     * Set f144fstatus
     *
     * @param integer $f144fstatus
     *
     * @return T144fwarrantDetails
     */
    public function setF144fstatus($f144fstatus)
    {
        $this->f144fstatus = $f144fstatus;

        return $this;
    }

    /**
     * Get f144fstatus
     *
     * @return integer
     */
    public function getF144fstatus()
    {
        return $this->f144fstatus;
    }

    /**
     * Set f144fupdatedBy
     *
     * @param integer $f144fupdatedBy
     *
     * @return T144fwarrantDetails
     */
    public function setF144fupdatedBy($f144fupdatedBy)
    {
        $this->f144fupdatedBy = $f144fupdatedBy;

        return $this;
    }

    /**
     * Get f144fupdatedBy
     *
     * @return integer
     */
    public function getF144fupdatedBy()
    {
        return $this->f144fupdatedBy;
    }

     /**
     * Set f144fcreatedBy
     *
     * @param integer $f144fcreatedBy
     *
     * @return T144fwarrantDetails
     */
    public function setF144fcreatedBy($f144fcreatedBy)
    {
        $this->f144fcreatedBy = $f144fcreatedBy;

        return $this;
    }

    /**
     * Get f144fcreatedBy
     *
     * @return integer
     */
    public function getF144fcreatedBy()
    {
        return $this->f144fcreatedBy;
    }

     /**
     * Set f144fdate
     *
     * @param integer $f144fdate
     *
     * @return T144fwarrantDetails
     */
    public function setF144fdate($f144fdate)
    {
        $this->f144fdate = $f144fdate;

        return $this;
    }

    /**
     * Get f144fdate
     *
     * @return integer
     */
    public function getF144fdate()
    {
        return $this->f144fdate;
    }

    
    
     /**
     * Set f144famount
     *
     * @param integer $f144famount
     *
     * @return T144fwarrantDetails
     */
    public function setF144famount($f144famount)
    {
        $this->f144famount = $f144famount;

        return $this;
    }

    /**
     * Get f144famount
     *
     * @return integer
     */
    public function getF144famount()
    {
        return $this->f144famount;
    }


}
