<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T114fmofCategory
 *
 * @ORM\Table(name="t114fmof_category")
 * @ORM\Entity(repositoryClass="Application\Repository\MofCategoryRepository")
 */
class T114fMofCategory  
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f114fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f114fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f114fname", type="string", length=25, nullable=false)
     */
    private $f114fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f114fdescription", type="string", length=100, nullable=false)
     */
    private $f114fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f114fstatus", type="integer", nullable=false)
     */
    private $f114fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f114fcreated_by", type="integer", nullable=false)
     */
    private $f114fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f114fupdated_by", type="integer", nullable=false)
     */
    private $f114fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f114fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f114fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f114fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f114fupdatedDtTm;



    /**
     * Get f114fid
     *
     * @return integer
     */
    public function getF114fid()
    {
        return $this->f114fid;
    }

    /**
     * Set f114fname
     *
     * @param string $f114fname
     *
     * @return T114fmofCategory
     */
    public function setF114fname($f114fname)
    {
        $this->f114fname = $f114fname;

        return $this;
    }

    /**
     * Get f114fname
     *
     * @return string
     */
    public function getF114fname()
    {
        return $this->f114fname;
    }

    /**
     * Set f114fdescription
     *
     * @param string $f114fdescription
     *
     * @return T114fmofCategory
     */
    public function setF114fdescription($f114fdescription)
    {
        $this->f114fdescription = $f114fdescription;

        return $this;
    }

    /**
     * Get f114fdescription
     *
     * @return string
     */
    public function getF114fdescription()
    {
        return $this->f114fdescription;
    }

    /**
     * Set f114fstatus
     *
     * @param integer $f114fstatus
     *
     * @return T114fmofCategory
     */
    public function setF114fstatus($f114fstatus)
    {
        $this->f114fstatus = $f114fstatus;

        return $this;
    }

    /**
     * Get f114fstatus
     *
     * @return integer
     */
    public function getF114fstatus()
    {
        return $this->f114fstatus;
    }

    /**
     * Set f114fcreatedBy
     *
     * @param integer $f114fcreatedBy
     *
     * @return T114fmofCategory
     */
    public function setF114fcreatedBy($f114fcreatedBy)
    {
        $this->f114fcreatedBy = $f114fcreatedBy;

        return $this;
    }

    /**
     * Get f114fcreatedBy
     *
     * @return integer
     */
    public function getF114fcreatedBy()
    {
        return $this->f114fcreatedBy;
    }

    /**
     * Set f114fupdatedBy
     *
     * @param integer $f114fupdatedBy
     *
     * @return T114fmofCategory
     */
    public function setF114fupdatedBy($f114fupdatedBy)
    {
        $this->f114fupdatedBy = $f114fupdatedBy;

        return $this;
    }

    /**
     * Get f114fupdatedBy
     *
     * @return integer
     */
    public function getF114fupdatedBy()
    {
        return $this->f114fupdatedBy;
    }

    /**
     * Set f114fcreatedDtTm
     *
     * @param \DateTime $f114fcreatedDtTm
     *
     * @return T114fmofCategory
     */
    public function setF114fcreatedDtTm($f114fcreatedDtTm)
    {
        $this->f114fcreatedDtTm = $f114fcreatedDtTm;

        return $this;
    }

    /**
     * Get f114fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF114fcreatedDtTm()
    {
        return $this->f114fcreatedDtTm;
    }

    /**
     * Set f114fupdatedDtTm
     *
     * @param \DateTime $f114fupdatedDtTm
     *
     * @return T114fmofCategory
     */
    public function setF114fupdatedDtTm($f114fupdatedDtTm)
    {
        $this->f114fupdatedDtTm = $f114fupdatedDtTm;

        return $this;
    }

    /**
     * Get f114fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF114fupdatedDtTm()
    {
        return $this->f114fupdatedDtTm;
    }
}
