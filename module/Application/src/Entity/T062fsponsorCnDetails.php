<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T062fsponsorCnDetails
 *
 * @ORM\Table(name="t062fsponsor_cn_details")
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorCnRepository")
 */
class T062fsponsorCnDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f062fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f062fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fid_item", type="integer", nullable=false)
     */
    private $f062fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fgst_value", type="integer", nullable=false)
     */
    private $f062fgstValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fquantity", type="integer", nullable=false)
     */
    private $f062fquantity;

    /**
     * @var float
     *
     * @ORM\Column(name="f062fprice", type="integer", nullable=false)
     */
    private $f062fprice;

    /**
     * @var float
     *
     * @ORM\Column(name="f062fto_cn_amount", type="integer", nullable=false)
     */
    private $f062ftoCnAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f062ftotal", type="integer", nullable=false)
     */
    private $f062ftotal;

        /**
     * @var integer
     *
     * @ORM\Column(name="f062fid_tax_code", type="integer", nullable=true)
     */
    private $f062fidTaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062ftotal_exc", type="integer", nullable=true)
     */
    private $f062ftotalExc;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062ftax_amount", type="integer", nullable=true)
     */
    private $f062ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fstatus", type="integer", nullable=false)
     */
    private $f062fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fcreated_by", type="integer", nullable=false)
     */
    private $f062fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fupdated_by", type="integer", nullable=false)
     */
    private $f062fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f062fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f062fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fgst_code", type="string", length=255, nullable=false)
     */
    private $f062fgstCode;

        /**
     * @var string
     *
     * @ORM\Column(name="f062fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f062fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f062fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f062fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f062fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f062fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f062fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f062fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f062fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f062fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f062fcreditAccountCode;

    /**
     * @var intger
     *
     * @ORM\Column(name="f062fid_sponsor_cn", type="integer", nullable=false)
     */
    private $f062fidsponsorCn;

    

    /**
     * Get f062fid
     *
     * @return integer
     */
    public function getF062fid()
    {
        return $this->f062fid;
    }

    /**
     * Set f062fidItem
     *
     * @param integer $f062fidItem
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fidItem($f062fidItem)
    {
        $this->f062fidItem = $f062fidItem;

        return $this;
    }

    /**
     * Get f062fidItem
     *
     * @return integer
     */
    public function getF062fidItem()
    {
        return $this->f062fidItem;
    }

    /**
     * Set f062fgstValue
     *
     * @param integer $f062fgstValue
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fgstValue($f062fgstValue)
    {
        $this->f062fgstValue = $f062fgstValue;

        return $this;
    }

    /**
     * Get f062fgstValue
     *
     * @return integer
     */
    public function getF062fgstValue()
    {
        return $this->f062fgstValue;
    }

    /**
     * Set f062fquantity
     *
     * @param integer $f062fquantity
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fquantity($f062fquantity)
    {
        $this->f062fquantity = $f062fquantity;

        return $this;
    }

    /**
     * Get f062fquantity
     *
     * @return integer
     */
    public function getF062fquantity()
    {
        return $this->f062fquantity;
    }

    /**
     * Set f062fprice
     *
     * @param float $f062fprice
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fprice($f062fprice)
    {
        $this->f062fprice = $f062fprice;

        return $this;
    }

    /**
     * Get f062fprice
     *
     * @return float
     */
    public function getF062fprice()
    {
        return $this->f062fprice;
    }

    /**
     * Set f062ftoCnAmount
     *
     * @param float $f062ftoCnAmount
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062ftoCnAmount($f062ftoCnAmount)
    {
        $this->f062ftoCnAmount = $f062ftoCnAmount;

        return $this;
    }

    /**
     * Get f062ftoCnAmount
     *
     * @return float
     */
    public function getF062ftoCnAmount()
    {
        return $this->f062ftoCnAmount;
    }

    /**
     * Set f062ftotal
     *
     * @param float $f062ftotal
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062ftotal($f062ftotal)
    {
        $this->f062ftotal = $f062ftotal;

        return $this;
    }

    /**
     * Get f062ftotal
     *
     * @return float
     */
    public function getF062ftotal()
    {
        return $this->f062ftotal;
    }

    /**
     * Set f062fstatus
     *
     * @param integer $f062fstatus
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fstatus($f062fstatus)
    {
        $this->f062fstatus = $f062fstatus;

        return $this;
    }

    /**
     * Get f062fstatus
     *
     * @return integer
     */
    public function getF062fstatus()
    {
        return $this->f062fstatus;
    }

    /**
     * Set f062fcreatedBy
     *
     * @param integer $f062fcreatedBy
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreatedBy($f062fcreatedBy)
    {
        $this->f062fcreatedBy = $f062fcreatedBy;

        return $this;
    }

    /**
     * Get f062fcreatedBy
     *
     * @return integer
     */
    public function getF062fcreatedBy()
    {
        return $this->f062fcreatedBy;
    }

    /**
     * Set f062fupdatedBy
     *
     * @param integer $f062fupdatedBy
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fupdatedBy($f062fupdatedBy)
    {
        $this->f062fupdatedBy = $f062fupdatedBy;

        return $this;
    }

    /**
     * Get f062fupdatedBy
     *
     * @return integer
     */
    public function getF062fupdatedBy()
    {
        return $this->f062fupdatedBy;
    }

    /**
     * Set f062fcreatedDtTm
     *
     * @param \DateTime $f062fcreatedDtTm
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreatedDtTm($f062fcreatedDtTm)
    {
        $this->f062fcreatedDtTm = $f062fcreatedDtTm;

        return $this;
    }

    /**
     * Get f062fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fcreatedDtTm()
    {
        return $this->f062fcreatedDtTm;
    }

    /**
     * Set f062fupdatedDtTm
     *
     * @param \DateTime $f062fupdatedDtTm
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fupdatedDtTm($f062fupdatedDtTm)
    {
        $this->f062fupdatedDtTm = $f062fupdatedDtTm;

        return $this;
    }

    /**
     * Get f062fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fupdatedDtTm()
    {
        return $this->f062fupdatedDtTm;
    }

    /**
     * Set f062fgstCode
     *
     * @param string $f062fgstCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fgstCode($f062fgstCode)
    {
        $this->f062fgstCode = $f062fgstCode;

        return $this;
    }

    /**
     * Get f062fgstCode
     *
     * @return string
     */
    public function getF062fgstCode()
    {
        return $this->f062fgstCode;
    }

     /**
     * Set f062fdebitFundCode
     *
     * @param integer $f062fdebitFundCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fdebitFundCode($f062fdebitFundCode)
    {
        $this->f062fdebitFundCode = $f062fdebitFundCode;

        return $this;
    }

    /**
     * Get f062fdebitFundCode
     *
     * @return integer
     */
    public function getF062fdebitFundCode()
    {
        return $this->f062fdebitFundCode;
    }

    /**
     * Set f062fdebitAccountCode
     *
     * @param integer $f062fdebitAccountCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fdebitAccountCode($f062fdebitAccountCode)
    {
        $this->f062fdebitAccountCode = $f062fdebitAccountCode;

        return $this;
    }

    /**
     * Get f062fdebitAccountCode
     *
     * @return integer
     */
    public function getF062fdebitAccountCode()
    {
        return $this->f062fdebitAccountCode;
    }

    /**
     * Set f062fdebitActivityCode
     *
     * @param integer $f062fdebitActivityCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fdebitActivityCode($f062fdebitActivityCode)
    {
        $this->f062fdebitActivityCode = $f062fdebitActivityCode;

        return $this;
    }

    /**
     * Get f062fdebitActivityCode
     *
     * @return integer
     */
    public function getF062fdebitActivityCode()
    {
        return $this->f062fdebitActivityCode;
    }

    /**
     * Set f062fdebitDepartmentCode
     *
     * @param integer $f062fdebitDepartmentCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fdebitDepartmentCode($f062fdebitDepartmentCode)
    {
        $this->f062fdebitDepartmentCode = $f062fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f062fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF062fdebitDepartmentCode()
    {
        return $this->f062fdebitDepartmentCode;
    }


    /**
     * Set f062fcreditFundCode
     *
     * @param integer $f062fcreditFundCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreditFundCode($f062fcreditFundCode)
    {
        $this->f062fcreditFundCode = $f062fcreditFundCode;

        return $this;
    }

    /**
     * Get f062fcreditFundCode
     *
     * @return integer
     */
    public function getF062fcreditFundCode()
    {
        return $this->f062fcreditFundCode;
    }

    /**
     * Set f062fcreditAccountCode
     *
     * @param integer $f062fcreditAccountCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreditAccountCode($f062fcreditAccountCode)
    {
        $this->f062fcreditAccountCode = $f062fcreditAccountCode;

        return $this;
    }

    /**
     * Get f062fcreditAccountCode
     *
     * @return integer
     */
    public function getF062fcreditAccountCode()
    {
        return $this->f062fcreditAccountCode;
    }

    /**
     * Set f062fcreditActivityCode
     *
     * @param integer $f062fcreditActivityCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreditActivityCode($f062fcreditActivityCode)
    {
        $this->f062fcreditActivityCode = $f062fcreditActivityCode;

        return $this;
    }

    /**
     * Get f062fcreditActivityCode
     *
     * @return integer
     */
    public function getF062fcreditActivityCode()
    {
        return $this->f062fcreditActivityCode;
    }

    /**
     * Set f062fcreditDepartmentCode
     *
     * @param integer $f062fcreditDepartmentCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fcreditDepartmentCode($f062fcreditDepartmentCode)
    {
        $this->f062fcreditDepartmentCode = $f062fcreditDepartmentCode;

        return $this;
    }

    /**
     * Set f062fidsponsorCn
     *
     * @param integer $f062fidsponsorCn
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fidsponsorCn($f062fidsponsorCn)
    {
        $this->f062fidsponsorCn = $f062fidsponsorCn;

        return $this;
    }

    /**
     * Get f062fidsponsorCn
     *
     * @return \Application\Entity\T062fsponsorCn
     */
    public function getF062fidsponsorCn()
    {
        return $this->f062fidsponsorCn;
    }

    /**
     * Set f062fidTaxCode
     *
     * @param integer $f062fidTaxCode
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062fidTaxCode($f062fidTaxCode)
    {
        $this->f062fidTaxCode = $f062fidTaxCode;

        return $this;
    }

    /**
     * Get f062fidTaxCode
     *
     * @return integer
     */
    public function getF062fidTaxCode()
    {
        return $this->f062fidTaxCode;
    }

    /**
     * Set f062ftotalExc
     *
     * @param integer $f062ftotalExc
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062ftotalExc($f062ftotalExc)
    {
        $this->f062ftotalExc = $f062ftotalExc;

        return $this;
    }

    /**
     * Get f062ftotalExc
     *
     * @return integer
     */
    public function getF062ftotalExc()
    {
        return $this->f062ftotalExc;
    }

    /**
     * Set f062ftaxAmount
     *
     * @param integer $f062ftaxAmount
     *
     * @return T062fsponsorCnDetails
     */
    public function setF062ftaxAmount($f062ftaxAmount)
    {
        $this->f062ftaxAmount = $f062ftaxAmount;

        return $this;
    }

    /**
     * Get f062ftaxAmount
     *
     * @return integer
     */
    public function getF062ftaxAmount()
    {
        return $this->f062ftaxAmount;
    }
}

