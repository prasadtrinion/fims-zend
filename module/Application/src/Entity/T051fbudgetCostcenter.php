<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T051fbudgetCostcenter
 *
 * @ORM\Table(name="t051fbudget_costcenter")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetCostcenterRepository")
 */
class T051fbudgetCostcenter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f051fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f051fdepartment", type="string",length=10, nullable=false)
     */
    private $f051fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f051ffund", type="string",length=10, nullable=false)
     */
    private $f051ffund;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051famount", type="integer", nullable=false)
     */
    private $f051famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fbalance_amount", type="integer", nullable=false)
     */
    private $f051fbalanceAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid_financialyear", type="integer", nullable=false)
     */
    private $f051fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fapproval_status", type="integer", nullable=true)
     */
    private $f051fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fcreated_by", type="integer", nullable=false)
     */
    private $f051fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fupdated_by", type="integer", nullable=false)
     */
    private $f051fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f051fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fstatus", type="integer", nullable=false)
     */
    private $f051fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f051freason", type="string",length=50, nullable=true)
     */
    private $f051freason='NULL';



    /**
     * Get f051fid
     *
     * @return integer
     */
    public function getF051fid()
    {
        return $this->f051fid;
    }

    /**
     * Set f051fdepartment
     *
     * @param string $f051fdepartment
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fdepartment($f051fdepartment)
    {
        $this->f051fdepartment = $f051fdepartment;

        return $this;
    }

    /**
     * Get f051fdepartment
     *
     * @return string
     */
    public function getF051fdepartment()
    {
        return $this->f051fdepartment;
    }

     /**
     * Set f051fdepartment
     *
     * @param string $f051ffund
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051ffund($f051ffund)
    {
        $this->f051ffund = $f051ffund;

        return $this;
    }

    /**
     * Get f051ffund
     *
     * @return string
     */
    public function getF051ffund()
    {
        return $this->f051ffund;
    }

    /**
     * Set f051famount
     *
     * @param integer $f051famount
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051famount($f051famount)
    {
        $this->f051famount = $f051famount;

        return $this;
    }

    /**
     * Get f051famount
     *
     * @return integer
     */
    public function getF051famount()
    {
        return $this->f051famount;
    }

    /**
     * Set f051fbalanceAmount
     *
     * @param string $f051fbalanceAmount
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fbalanceAmount($f051fbalanceAmount)
    {
        $this->f051fbalanceAmount = $f051fbalanceAmount;

        return $this;
    }

    /**
     * Get f051fbalanceAmount
     *
     * @return string
     */
    public function getF051fbalanceAmount()
    {
        return $this->f051fbalanceAmount;
    }
    
    /**
     * Set f051fidFinancialyear
     *
     * @param integer $f051fidFinancialyear
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fidFinancialyear($f051fidFinancialyear)
    {
        $this->f051fidFinancialyear = $f051fidFinancialyear;

        return $this;
    }

    /**
     * Get f051fidFinancialyear
     *
     * @return integer
     */
    public function getF051fidFinancialyear()
    {
        return $this->f051fidFinancialyear;
    }

    /**
     * Set f051fapprovalStatus
     *
     * @param integer $f051fapprovalStatus
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fapprovalStatus($f051fapprovalStatus)
    {
        $this->f051fapprovalStatus = $f051fapprovalStatus;

        return $this;
    }

    /**
     * Get f051fapprovalStatus
     *
     * @return integer
     */
    public function getF051fapprovalStatus()
    {
        return $this->f051fapprovalStatus;
    }

    /**
     * Set f051fcreatedBy
     *
     * @param integer $f051fcreatedBy
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fcreatedBy($f051fcreatedBy)
    {
        $this->f051fcreatedBy = $f051fcreatedBy;

        return $this;
    }

    /**
     * Get f051fcreatedBy
     *
     * @return integer
     */
    public function getF051fcreatedBy()
    {
        return $this->f051fcreatedBy;
    }

    /**
     * Set f051fupdatedBy
     *
     * @param integer $f051fupdatedBy
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fupdatedBy($f051fupdatedBy)
    {
        $this->f051fupdatedBy = $f051fupdatedBy;

        return $this;
    }

    /**
     * Get f051fupdatedBy
     *
     * @return integer
     */
    public function getF051fupdatedBy()
    {
        return $this->f051fupdatedBy;
    }

    /**
     * Set f051fcreatedDtTm
     *
     * @param \DateTime $f051fcreatedDtTm
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fcreatedDtTm($f051fcreatedDtTm)
    {
        $this->f051fcreatedDtTm = $f051fcreatedDtTm;

        return $this;
    }

    /**
     * Get f051fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fcreatedDtTm()
    {
        return $this->f051fcreatedDtTm;
    }

    /**
     * Set f051fupdatedDtTm
     *
     * @param \DateTime $f051fupdatedDtTm
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fupdatedDtTm($f051fupdatedDtTm)
    {
        $this->f051fupdatedDtTm = $f051fupdatedDtTm;

        return $this;
    }

    /**
     * Get f051fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF051fupdatedDtTm()
    {
        return $this->f051fupdatedDtTm;
    }

    /**
     * Set f051fstatus
     *
     * @param integer $f051fstatus
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051fstatus($f051fstatus)
    {
        $this->f051fstatus = $f051fstatus;

        return $this;
    }

    /**
     * Get f051fstatus
     *
     * @return integer
     */
    public function getF051fstatus()
    {
        return $this->f051fstatus;
    }

    /**
     * Set f051freason
     *
     * @param string $f051freason
     *
     * @return T051fbudgetCostcenter
     */
    public function setF051freason($f051freason)
    {
        $this->f051freason = $f051freason;

        return $this;
    }

    /**
     * Get f051freason
     *
     * @return string
     */
    public function getF051freason()
    {
        return $this->f051freason;
    }
}
