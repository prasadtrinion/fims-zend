<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T036frevenueItemSetup
 *
 * @ORM\Table(name="t036frevenue_item_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\RevenueItemSetupRepository")
 */
class T036frevenueItemSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f036fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f036fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f036fname", type="string", length=255, nullable=false)
     */
    private $f036fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fid_glcode", type="integer", nullable=false)
     */
    private $f036fidGlcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fstatus", type="integer", nullable=false)
     */
    private $f036fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fcreated_by", type="integer", nullable=false)
     */
    private $f036fcreatedBy = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f036fupdated_by", type="integer", nullable=false)
     */
    private $f036fupdatedBy = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f036fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f036fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f036fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f036fupdatedDtTm;



    /**
     * Get f036fid
     *
     * @return integer
     */
    public function getF036fid()
    {
        return $this->f036fid;
    }

    /**
     * Set f036fname
     *
     * @param string $f036fname
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fname($f036fname)
    {
        $this->f036fname = $f036fname;

        return $this;
    }

    /**
     * Get f036fname
     *
     * @return string
     */
    public function getF036fname()
    {
        return $this->f036fname;
    }

    /**
     * Set f036fidGlcode
     *
     * @param integer $f036fidGlcode
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fidGlcode($f036fidGlcode)
    {
        $this->f036fidGlcode = $f036fidGlcode;

        return $this;
    }

    /**
     * Get f036fidGlcode
     *
     * @return integer
     */
    public function getF036fidGlcode()
    {
        return $this->f036fidGlcode;
    }

    /**
     * Set f036fstatus
     *
     * @param integer $f036fstatus
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fstatus($f036fstatus)
    {
        $this->f036fstatus = $f036fstatus;

        return $this;
    }

    /**
     * Get f036fstatus
     *
     * @return integer
     */
    public function getF036fstatus()
    {
        return $this->f036fstatus;
    }

    /**
     * Set f036fcreatedBy
     *
     * @param integer $f036fcreatedBy
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fcreatedBy($f036fcreatedBy)
    {
        $this->f036fcreatedBy = $f036fcreatedBy;

        return $this;
    }

    /**
     * Get f036fcreatedBy
     *
     * @return integer
     */
    public function getF036fcreatedBy()
    {
        return $this->f036fcreatedBy;
    }

    /**
     * Set f036fupdatedBy
     *
     * @param integer $f036fupdatedBy
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fupdatedBy($f036fupdatedBy)
    {
        $this->f036fupdatedBy = $f036fupdatedBy;

        return $this;
    }

    /**
     * Get f036fupdatedBy
     *
     * @return integer
     */
    public function getF036fupdatedBy()
    {
        return $this->f036fupdatedBy;
    }

    /**
     * Set f036fcreatedDtTm
     *
     * @param \DateTime $f036fcreatedDtTm
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fcreatedDtTm($f036fcreatedDtTm)
    {
        $this->f036fcreatedDtTm = $f036fcreatedDtTm;

        return $this;
    }

    /**
     * Get f036fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF036fcreatedDtTm()
    {
        return $this->f036fcreatedDtTm;
    }

    /**
     * Set f036fupdatedDtTm
     *
     * @param \DateTime $f036fupdatedDtTm
     *
     * @return T036frevenueItemSetup
     */
    public function setF036fupdatedDtTm($f036fupdatedDtTm)
    {
        $this->f036fupdatedDtTm = $f036fupdatedDtTm;

        return $this;
    }

    /**
     * Get f036fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF036fupdatedDtTm()
    {
        return $this->f036fupdatedDtTm;
    }
}
