<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T061fpayrollContribution
 *
 * @ORM\Table(name="t061fpayroll_contribution")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollContributionRepository")
 */
class T061fpayrollContribution
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f061fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fmonth", type="string", length=25, nullable=false)
     */
    private $f061fmonth;


     /**
     * @var integer
     *
     * @ORM\Column(name="f061fstatus", type="integer", nullable=false)
     */
    private $f061fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fcreated_by", type="integer", nullable=false)
     */
    private $f061fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fupdated_by", type="integer", nullable=false)
     */
    private $f061fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f061fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f061fupdatedDtTm;



    /**
     * Get f061fid
     *
     * @return integer
     */
    public function getF061fid()
    {
        return $this->f061fid;
    }

    /**
     * Set f061fmonth
     *
     * @param string $f061fmonth
     *
     * @return T061fpayrollContribution
     */
    public function setF061fmonth($f061fmonth)
    {
        $this->f061fmonth = $f061fmonth;

        return $this;
    }

    /**
     * Get f061fmonth
     *
     * @return string
     */
    public function getF061fmonth()
    {
        return $this->f061fmonth;
    }

     /**
     * Set f061fstatus
     *
     * @param boolean $f061fstatus
     *
     * @return T061fpayrollContribution
     */
    public function setF061fstatus($f061fstatus)
    {
        $this->f061fstatus = $f061fstatus;

        return $this;
    }

    /**
     * Get f061fstatus
     *
     * @return boolean
     */
    public function getF061fstatus()
    {
        return $this->f061fstatus;
    }

    /**
     * Set f061fcreatedBy
     *
     * @param integer $f061fcreatedBy
     *
     * @return T061fpayrollContribution
     */
    public function setF061fcreatedBy($f061fcreatedBy)
    {
        $this->f061fcreatedBy = $f061fcreatedBy;

        return $this;
    }

    /**
     * Get f061fcreatedBy
     *
     * @return integer
     */
    public function getF061fcreatedBy()
    {
        return $this->f061fcreatedBy;
    }

    /**
     * Set f061fupdatedBy
     *
     * @param integer $f061fupdatedBy
     *
     * @return T061fpayrollContribution
     */
    public function setF061fupdatedBy($f061fupdatedBy)
    {
        $this->f061fupdatedBy = $f061fupdatedBy;

        return $this;
    }

    /**
     * Get f061fupdatedBy
     *
     * @return integer
     */
    public function getF061fupdatedBy()
    {
        return $this->f061fupdatedBy;
    }

    /**
     * Set f061fcreatedDtTm
     *
     * @param \DateTime $f061fcreatedDtTm
     *
     * @return T061fpayrollContribution
     */
    public function setF061fcreatedDtTm($f061fcreatedDtTm)
    {
        $this->f061fcreatedDtTm = $f061fcreatedDtTm;

        return $this;
    }

    /**
     * Get f061fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fcreatedDtTm()
    {
        return $this->f061fcreatedDtTm;
    }

    /**
     * Set f061fupdatedDtTm
     *
     * @param \DateTime $f061fupdatedDtTm
     *
     * @return T061fpayrollContribution
     */
    public function setF061fupdatedDtTm($f061fupdatedDtTm)
    {
        $this->f061fupdatedDtTm = $f061fupdatedDtTm;

        return $this;
    }

    /**
     * Get f061fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fupdatedDtTm()
    {
        return $this->f061fupdatedDtTm;
    }

    
}
