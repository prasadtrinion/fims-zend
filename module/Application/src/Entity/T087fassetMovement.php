<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T087fassetMovement
 *
 * @ORM\Table(name="t087fasset_movement")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetMovementRepository")
 */
class T087fassetMovement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f087fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fasset_code", type="integer", nullable=true)
     */
    private $f087fassetCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_type", type="integer", nullable=true)
     */
    private $f087fidType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_receiver", type="integer", nullable=true)
     */
    private $f087fidReceiver;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087id_department", type="string",length=30, nullable=true)
     */
    private $f087idDepartment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fstart_date", type="datetime", nullable=true)
     */
    private $f087fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087ffrom_date", type="datetime", nullable=true)
     */
    private $f087ffromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fto_date", type="datetime", nullable=true)
     */
    private $f087ftoDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f087freason", type="string", length=100, nullable=true)
     */
    private $f087freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fapproval1_status", type="integer", nullable=true)
     */
    private $f087fapproval1Status;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fapproval2_status", type="integer", nullable=true)
     */
    private $f087fapproval2Status;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fapproved1_by", type="integer", nullable=true)
     */
    private $f087fapproved1By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fapproved2_by", type="integer", nullable=true)
     */
    private $f087fapproved2By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fstatus", type="integer", nullable=false)
     */
    private $f087fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fcreated_by", type="integer", nullable=false)
     */
    private $f087fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fupdated_by", type="integer", nullable=false)
     */
    private $f087fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fupdatedDtTm;



    /**
     * Get f087fid
     *
     * @return integer
     */
    public function getF087fid()
    {
        return $this->f087fid;
    }

    /**
     * Set f087fassetCode
     *
     * @param integer $f087fassetCode
     *
     * @return T087fassetMovement
     */
    public function setF087fassetCode($f087fassetCode)
    {
        $this->f087fassetCode = $f087fassetCode;

        return $this;
    }

    /**
     * Get f087fassetCode
     *
     * @return integer
     */
    public function getF087fassetCode()
    {
        return $this->f087fassetCode;
    }

    /**
     * Set f087fidType
     *
     * @param integer $f087fidType
     *
     * @return T087fassetMovement
     */
    public function setF087fidType($f087fidType)
    {
        $this->f087fidType = $f087fidType;

        return $this;
    }

    /**
     * Get f087fidType
     *
     * @return integer
     */
    public function getF087fidType()
    {
        return $this->f087fidType;
    }

    /**
     * Set f087fidReceiver
     *
     * @param integer $f087fidReceiver
     *
     * @return T087fassetMovement
     */
    public function setF087fidReceiver($f087fidReceiver)
    {
        $this->f087fidReceiver = $f087fidReceiver;

        return $this;
    }

    /**
     * Get f087fidReceiver
     *
     * @return integer
     */
    public function getF087fidReceiver()
    {
        return $this->f087fidReceiver;
    }

    /**
     * Set f087fapproved1By
     *
     * @param integer $f087fapproved1By
     *
     * @return T087fassetMovement
     */
    public function setF087fapproved1By($f087fapproved1By)
    {
        $this->f087fapproved1By = $f087fapproved1By;

        return $this;
    }

    /**
     * Get f087fapproved1By
     *
     * @return integer
     */
    public function getF087fapproved1By()
    {
        return $this->f087fapproved1By;
    }

    /**
     * Set f087fapproved2By
     *
     * @param integer $f087fapproved2By
     *
     * @return T087fassetMovement
     */
    public function setF087fapproved2By($f087fapproved2By)
    {
        $this->f087fapproved2By = $f087fapproved2By;

        return $this;
    }

    /**
     * Get f087fapproved2By
     *
     * @return integer
     */
    public function getF087fapproved2By()
    {
        return $this->f087fapproved2By;
    }

    /**
     * Set f087idDepartment
     *
     * @param integer $f087idDepartment
     *
     * @return T087fassetMovement
     */
    public function setF087idDepartment($f087idDepartment)
    {
        $this->f087idDepartment = $f087idDepartment;

        return $this;
    }

    /**
     * Get f087idDepartment
     *
     * @return integer
     */
    public function getF087idDepartment()
    {
        return $this->f087idDepartment;
    }

    /**
     * Set f087fstartDate
     *
     * @param \DateTime $f087fstartDate
     *
     * @return T087fassetMovement
     */
    public function setF087fstartDate($f087fstartDate)
    {
        $this->f087fstartDate = $f087fstartDate;

        return $this;
    }

    /**
     * Get f087fstartDate
     *
     * @return \DateTime
     */
    public function getF087fstartDate()
    {
        return $this->f087fstartDate;
    }

    /**
     * Set f087ffromDate
     *
     * @param \DateTime $f087ffromDate
     *
     * @return T087fassetMovement
     */
    public function setF087ffromDate($f087ffromDate)
    {
        $this->f087ffromDate = $f087ffromDate;

        return $this;
    }

    /**
     * Get f087ffromDate
     *
     * @return \DateTime
     */
    public function getF087ffromDate()
    {
        return $this->f087ffromDate;
    }

    /**
     * Set f087ftoDate
     *
     * @param \DateTime $f087ftoDate
     *
     * @return T087fassetMovement
     */
    public function setF087ftoDate($f087ftoDate)
    {
        $this->f087ftoDate = $f087ftoDate;

        return $this;
    }

    /**
     * Get f087ftoDate
     *
     * @return \DateTime
     */
    public function getF087ftoDate()
    {
        return $this->f087ftoDate;
    }

    /**
     * Set f087freason
     *
     * @param string $f087freason
     *
     * @return T087fassetMovement
     */
    public function setF087freason($f087freason)
    {
        $this->f087freason = $f087freason;

        return $this;
    }

    /**
     * Get f087freason
     *
     * @return string
     */
    public function getF087freason()
    {
        return $this->f087freason;
    }

    /**
     * Set f087fapproval1Status
     *
     * @param integer $f087fapproval1Status
     *
     * @return T087fassetMovement
     */
    public function setF087fapproval1Status($f087fapproval1Status)
    {
        $this->f087fapproval1Status = $f087fapproval1Status;

        return $this;
    }

    /**
     * Get f087fapproval1Status
     *
     * @return integer
     */
    public function getF087fapproval1Status()
    {
        return $this->f087fapproval1Status;
    }

    /**
     * Set f087fapproval2Status
     *
     * @param integer $f087fapproval2Status
     *
     * @return T087fassetMovement
     */
    public function setF087fapproval2Status($f087fapproval2Status)
    {
        $this->f087fapproval2Status = $f087fapproval2Status;

        return $this;
    }

    /**
     * Get f087fapproval2Status
     *
     * @return integer
     */
    public function getF087fapproval2Status()
    {
        return $this->f087fapproval2Status;
    }

    /**
     * Set f087fstatus
     *
     * @param integer $f087fstatus
     *
     * @return T087fassetMovement
     */
    public function setF087fstatus($f087fstatus)
    {
        $this->f087fstatus = $f087fstatus;

        return $this;
    }

    /**
     * Get f087fstatus
     *
     * @return integer
     */
    public function getF087fstatus()
    {
        return $this->f087fstatus;
    }

    /**
     * Set f087fcreatedBy
     *
     * @param integer $f087fcreatedBy
     *
     * @return T087fassetMovement
     */
    public function setF087fcreatedBy($f087fcreatedBy)
    {
        $this->f087fcreatedBy = $f087fcreatedBy;

        return $this;
    }

    /**
     * Get f087fcreatedBy
     *
     * @return integer
     */
    public function getF087fcreatedBy()
    {
        return $this->f087fcreatedBy;
    }

    /**
     * Set f087fupdatedBy
     *
     * @param integer $f087fupdatedBy
     *
     * @return T087fassetMovement
     */
    public function setF087fupdatedBy($f087fupdatedBy)
    {
        $this->f087fupdatedBy = $f087fupdatedBy;

        return $this;
    }

    /**
     * Get f087fupdatedBy
     *
     * @return integer
     */
    public function getF087fupdatedBy()
    {
        return $this->f087fupdatedBy;
    }

    /**
     * Set f087fcreatedDtTm
     *
     * @param \DateTime $f087fcreatedDtTm
     *
     * @return T087fassetMovement
     */
    public function setF087fcreatedDtTm($f087fcreatedDtTm)
    {
        $this->f087fcreatedDtTm = $f087fcreatedDtTm;

        return $this;
    }

    /**
     * Get f087fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fcreatedDtTm()
    {
        return $this->f087fcreatedDtTm;
    }

    /**
     * Set f087fupdatedDtTm
     *
     * @param \DateTime $f087fupdatedDtTm
     *
     * @return T087fassetMovement
     */
    public function setF087fupdatedDtTm($f087fupdatedDtTm)
    {
        $this->f087fupdatedDtTm = $f087fupdatedDtTm;

        return $this;
    }

    /**
     * Get f087fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fupdatedDtTm()
    {
        return $this->f087fupdatedDtTm;
    }
}
