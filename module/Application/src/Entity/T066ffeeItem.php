<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T066ffeeItem
 *
 * @ORM\Table(name="t066ffee_item")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeItemRepository")
 */
class T066ffeeItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f066fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fname", type="string", length=50, nullable=false)
     */
    private $f066fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fcode", type="string", length=50, nullable=false)
     */
    private $f066fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f066fdescription", type="string", length=50, nullable=false)
     */
    private $f066fdescription;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fid_fee_category", type="integer", nullable=true)
     */
    private $f066fidFeeCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fid_tax_code", type="integer", nullable=true)
     */
    private $f066fidTaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066finsured_item", type="string",length=50, nullable=true)
     */
    private $f066finsuredItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fso_code", type="string",length=50, nullable=true)
     */
    private $f066fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066frefundable", type="integer", nullable=false)
     */
    private $f066frefundable;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066finvoice_item", type="integer", nullable=false)
     */
    private $f066finvoiceItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fsoa", type="integer", nullable=false)
     */
    private $f066fsoa;
    /**
     * @var integer
     *
     * @ORM\Column(name="f066fstatus", type="integer", nullable=false)
     */
    private $f066fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fcreated_by", type="integer", nullable=true)
     */
    private $f066fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f066fupdated_by", type="integer", nullable=true)
     */
    private $f066fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f066fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f066fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f066fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f066fupdatedDtTm;



    /**
     * Get f066fid
     *
     * @return integer
     */
    public function getF066fid()
    {
        return $this->f066fid;
    }

    /**
     * Set f066fname
     *
     * @param string $f066fname
     *
     * @return T066ffeeItem
     */
    public function setF066fname($f066fname)
    {
        $this->f066fname = $f066fname;

        return $this;
    }

    /**
     * Get f066fname
     *
     * @return string
     */
    public function getF066fname()
    {
        return $this->f066fname;
    }

    /**
     * Set f066fcode
     *
     * @param string $f066fcode
     *
     * @return T066ffeeItem
     */
    public function setF066fcode($f066fcode)
    {
        $this->f066fcode = $f066fcode;

        return $this;
    }

    /**
     * Get f066fcode
     *
     * @return string
     */
    public function getF066fcode()
    {
        return $this->f066fcode;
    }

    /**
     * Set f066fsoCode
     *
     * @param string $f066fsoCode
     *
     * @return T066ffeeItem
     */
    public function setF066fsoCode($f066fsoCode)
    {
        $this->f066fsoCode = $f066fsoCode;

        return $this;
    }

    /**
     * Get f066fsoCode
     *
     * @return string
     */
    public function getF066fsoCode()
    {
        return $this->f066fsoCode;
    }

    /**
     * Set f066finsuredItem
     *
     * @param integer $f066finsuredItem
     *
     * @return T066ffeeItem
     */
    public function setF066finsuredItem($f066finsuredItem)
    {
        $this->f066finsuredItem = $f066finsuredItem;

        return $this;
    }

    /**
     * Get f066finsuredItem
     *
     * @return integer
     */
    public function getF066finsuredItem()
    {
        return $this->f066finsuredItem;
    }

    /**
     * Set f066fdescription
     *
     * @param string $f066fdescription
     *
     * @return T066ffeeItem
     */
    public function setF066fdescription($f066fdescription)
    {
        $this->f066fdescription = $f066fdescription;

        return $this;
    }

    /**
     * Get f066fdescription
     *
     * @return string
     */
    public function getF066fdescription()
    {
        return $this->f066fdescription;
    }

    /**
     * Set f066fidFeeCategory
     *
     * @param integer $f066fidFeeCategory
     *
     * @return T066ffeeItem
     */
    public function setF066fidFeeCategory($f066fidFeeCategory)
    {
        $this->f066fidFeeCategory = $f066fidFeeCategory;

        return $this;
    }

    /**
     * Get f066fidFeeCategory
     *
     * @return integer
     */
    public function getF066fidFeeCategory()
    {
        return $this->f066fidFeeCategory;
    }

    /**
     * Set f066fidTaxCode
     *
     * @param integer $f066fidTaxCode
     *
     * @return T066ffeeItem
     */
    public function setF066fidTaxCode($f066fidTaxCode)
    {
        $this->f066fidTaxCode = $f066fidTaxCode;

        return $this;
    }

    /**
     * Get f066fidTaxCode
     *
     * @return integer
     */
    public function getF066fidTaxCode()
    {
        return $this->f066fidTaxCode;
    }

    /**
     * Set f066finvoiceItem
     *
     * @param integer $f066finvoiceItem
     *
     * @return T066ffeeItem
     */
    public function setF066finvoiceItem($f066finvoiceItem)
    {
        $this->f066finvoiceItem = $f066finvoiceItem;

        return $this;
    }

    /**
     * Get f066finvoiceItem
     *
     * @return integer
     */
    public function getF066finvoiceItem()
    {
        return $this->f066finvoiceItem;
    }

    /**
     * Set f066fstatus
     *
     * @param integer $f066fstatus
     *
     * @return T066ffeeItem
     */
    public function setF066fstatus($f066fstatus)
    {
        $this->f066fstatus = $f066fstatus;

        return $this;
    }

    /**
     * Get f066fstatus
     *
     * @return integer
     */
    public function getF066fstatus()
    {
        return $this->f066fstatus;
    }

    /**
     * Set f066frefundable
     *
     * @param integer $f066frefundable
     *
     * @return T066ffeeItem
     */
    public function setF066frefundable($f066frefundable)
    {
        $this->f066frefundable = $f066frefundable;

        return $this;
    }

    /**
     * Get f066frefundable
     *
     * @return integer
     */
    public function getF066frefundable()
    {
        return $this->f066frefundable;
    }

    /**
     * Set f066fsoa
     *
     * @param integer $f066fsoa
     *
     * @return T066ffeeItem
     */
    public function setF066fsoa($f066fsoa)
    {
        $this->f066fsoa = $f066fsoa;

        return $this;
    }

    /**
     * Get f066fsoa
     *
     * @return integer
     */
    public function getF066fsoa()
    {
        return $this->f066fsoa;
    }

    /**
     * Set f066fcreatedBy
     *
     * @param integer $f066fcreatedBy
     *
     * @return T066ffeeItem
     */
    public function setF066fcreatedBy($f066fcreatedBy)
    {
        $this->f066fcreatedBy = $f066fcreatedBy;

        return $this;
    }

    /**
     * Get f066fcreatedBy
     *
     * @return integer
     */
    public function getF066fcreatedBy()
    {
        return $this->f066fcreatedBy;
    }

    /**
     * Set f066fupdatedBy
     *
     * @param integer $f066fupdatedBy
     *
     * @return T066ffeeItem
     */
    public function setF066fupdatedBy($f066fupdatedBy)
    {
        $this->f066fupdatedBy = $f066fupdatedBy;

        return $this;
    }

    /**
     * Get f066fupdatedBy
     *
     * @return integer
     */
    public function getF066fupdatedBy()
    {
        return $this->f066fupdatedBy;
    }

    /**
     * Set f066fcreatedDtTm
     *
     * @param \DateTime $f066fcreatedDtTm
     *
     * @return T066ffeeItem
     */
    public function setF066fcreatedDtTm($f066fcreatedDtTm)
    {
        $this->f066fcreatedDtTm = $f066fcreatedDtTm;

        return $this;
    }

    /**
     * Get f066fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF066fcreatedDtTm()
    {
        return $this->f066fcreatedDtTm;
    }

    /**
     * Set f066fupdatedDtTm
     *
     * @param \DateTime $f066fupdatedDtTm
     *
     * @return T066ffeeItem
     */
    public function setF066fupdatedDtTm($f066fupdatedDtTm)
    {
        $this->f066fupdatedDtTm = $f066fupdatedDtTm;

        return $this;
    }

    /**
     * Get f066fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF066fupdatedDtTm()
    {
        return $this->f066fupdatedDtTm;
    }
}
