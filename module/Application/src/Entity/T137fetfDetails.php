<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T137fetfDetails
 *
 * @ORM\Table(name="t137fetf_details")
 * @ORM\Entity(repositoryClass="Application\Repository\EtfRepository")
 */
class T137fetfDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f137fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f137fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f137fid_master", type="integer",  nullable=false)
     */
    private $f137fidMaster;

    /**
     * @var string
     *
     * @ORM\Column(name="f137fvoucher_id", type="integer", nullable=false)
     */
    private $f137fvoucherId;


    /**
     * @var integer
     *
     * @ORM\Column(name="f137fpay_to_id", type="string",length=50, nullable=false)
     * 
     */
    private $f137fpayToId;


    /**
     * @var integer
     *
     * @ORM\Column(name="f137fpay_to_bank", type="string",length=50, nullable=false)
     * 
     */
    private $f137fpayToBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f137fpay_to_account", type="string",length=50, nullable=false)
     * 
     */
    private $f137fpayToaccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f137fpay_to_email", type="string",length=50, nullable=false)
     * 
     */
    private $f137fpayToEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="f137fstatus", type="integer", nullable=false)
     */
    private $f137fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f137fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f137fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f137fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f137fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f137fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f137fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f137fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f137fupdatedDtTm;
 	

    /**
     * Get f137fid
     *
     * @return integer
     */
    public function getF137fid()
    {
        return $this->f137fid;
    }

    /**
     * Set f137fidMaster
     *
     * @param string $f137fidMaster
     *
     * @return T137fetfDetails
     */
    public function setF137fidMaster($f137fidMaster)
    {
        $this->f137fidMaster = $f137fidMaster;

        return $this;
    }

    /**
     * Get f137fidMaster
     *
     * @return string
     */
    public function getF137fidMaster()
    {
        return $this->f137fidMaster;
    }

    /**
     * Set f137fvoucherId
     *
     * @param string $f137fvoucherId
     *
     * @return T137fetfDetails
     */
    public function setF137fvoucherId($f137fvoucherId)
    {
        $this->f137fvoucherId = $f137fvoucherId;

        return $this;
    }

    /**
     * Get f137fvoucherId
     *
     * @return string
     */
    public function getF137fvoucherId()
    {
        return $this->f137fvoucherId;
    }


    /**
     * Set f137fpayToId
     *
     * @param integer $f137fpayToId
     *
     * @return T137fetfDetails
     */
    public function setF137fpayToId($f137fpayToId)
    {
        $this->f137fpayToId = $f137fpayToId;

        return $this;
    }

    /**
     * Get f137fpayToId
     *
     * @return integer
     */
    public function getF137fpayToId()
    {
        return $this->f137fpayToId;
    }

    /**
     * Set f137fpayToBank
     *
     * @param integer $f137fpayToBank
     *
     * @return T137fetfDetails
     */
    public function setF137fpayToBank($f137fpayToBank)
    {
        $this->f137fpayToBank = $f137fpayToBank;

        return $this;
    }

    /**
     * Get f137fpayToBank
     *
     * @return integer
     */
    public function getF137fpayToBank()
    {
        return $this->f137fpayToBank;
    }

    /**
     * Set f137fpayToaccount
     *
     * @param integer $f137fpayToaccount
     *
     * @return T137fetfDetails
     */
    public function setF137fpayToaccount($f137fpayToaccount)
    {
        $this->f137fpayToaccount = $f137fpayToaccount;

        return $this;
    }

    /**
     * Get f137fpayToaccount
     *
     * @return integer
     */
    public function getF137fpayToaccount()
    {
        return $this->f137fpayToaccount;
    }



    /**
     * Set f137fpayToEmail
     *
     * @param integer $f137fpayToEmail
     *
     * @return T137fetfDetails
     */
    public function setF137fpayToEmail($f137fpayToEmail)
    {
        $this->f137fpayToEmail = $f137fpayToEmail;

        return $this;
    }

    /**
     * Get f137fpayToEmail
     *
     * @return integer
     */
    public function getF137fpayToEmail()
    {
        return $this->f137fpayToEmail;
    }

    /**
     * Set f137fupdatedDtTm
     *
     * @param \DateTime $f137fupdatedDtTm
     *
     * @return T137fetfDetails
     */
    public function setF137fupdatedDtTm($f137fupdatedDtTm)
    {
        $this->f137fupdatedDtTm = $f137fupdatedDtTm;

        return $this;
    }

    /**
     * Get f137fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF137fupdatedDtTm()
    {
        return $this->f137fupdatedDtTm;
    }

    /**
     * Set f137fstatus
     *
     * @param integer $f137fstatus
     *
     * @return T137fetfDetails
     */
    public function setF137fstatus($f137fstatus)
    {
        $this->f137fstatus = $f137fstatus;

        return $this;
    }

    /**
     * Get f137fstatus
     *
     * @return integer
     */
    public function getF137fstatus()
    {
        return $this->f137fstatus;
    }

    /**
     * Set f137fupdatedBy
     *
     * @param integer $f137fupdatedBy
     *
     * @return T137fetfDetails
     */
    public function setF137fupdatedBy($f137fupdatedBy)
    {
        $this->f137fupdatedBy = $f137fupdatedBy;

        return $this;
    }

    /**
     * Get f137fupdatedBy
     *
     * @return integer
     */
    public function getF137fupdatedBy()
    {
        return $this->f137fupdatedBy;
    }

    /**
     * Set f137fcreatedDtTm
     *
     * @param \DateTime $f137fcreatedDtTm
     *
     * @return T137fetf
     */
    public function setF137fcreatedDtTm($f137fcreatedDtTm)
    {
        $this->f137fcreatedDtTm = $f137fcreatedDtTm;

        return $this;
    }

    /**
     * Get f137fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF137fcreatedDtTm()
    {
        return $this->f137fcreatedDtTm;
    }

    /**
     * Set f137fcreatedBy
     *
     * @param integer $f137fcreatedBy
     *
     * @return T137fetf
     */
    public function setF137fcreatedBy($f137fcreatedBy)
    {
        $this->f137fcreatedBy = $f137fcreatedBy;

        return $this;
    }

    /**
     * Get f137fcreatedBy
     *
     * @return integer
     */
    public function getF137fcreatedBy()
    {
        return $this->f137fcreatedBy;
    }
}
