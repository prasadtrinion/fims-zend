<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T056fverimentMaster
 *
 * @ORM\Table(name="t056fveriment_master")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetVerimentRepository")
 */
class T056fverimentMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f056fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f056fid; 

    /**
     * @var integer 
     *
     * @ORM\Column(name="f056fid_financial_year", type="integer", nullable=false)
     */
    private $f056fidFinancialYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f056ffrom_department", type="string",length=20, nullable=false)
     */
    private $f056ffromDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f056ffrom_fund_code", type="string",length=20, nullable=false)
     */
    private $f056ffromFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f056ffrom_department_code", type="string",length=20, nullable=false)
     */
    private $f056ffromDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f056ffrom_activity_code", type="string",length=20, nullable=false)
     */
    private $f056ffromActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f056ffrom_account_code", type="string",length=20, nullable=false)
     */
    private $f056ffromAccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056ftotal_amount", type="integer",  nullable=false)
     */
    private $f056ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fapproval", type="integer", nullable=true)
     */
    private $f056fapproval;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fcreated_by", type="integer", nullable=false)
     */
    private $f056fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fupdated_by", type="integer", nullable=false)
     */
    private $f056fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f056fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f056fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f056fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f056fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fstatus", type="integer", nullable=false)
     */
    private $f056fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f056freason", type="string",length=50, nullable=true)
     */
    private $f056freason='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f056fveriment_type", type="string",length=50, nullable=true)
     */
    private $f056fverimentType;



    /**
     * Get f056fid
     *
     * @return integer
     */
    public function getF056fid()
    {
        return $this->f056fid;
    }

    /**
     * Set f056fidFinancialYear
     *
     * @param integer $f056fidFinancialYear
     *
     * @return T056fverimentMaster
     */
    public function setF056fidFinancialYear($f056fidFinancialYear)
    {
        $this->f056fidFinancialYear = $f056fidFinancialYear;

        return $this;
    }

    /**
     * Get f056fidFinancialYear
     *
     * @return integer
     */
    public function getF056fidFinancialYear()
    {
        return $this->f056fidFinancialYear;
    }

    /**
     * Set f056fverimentType
     *
     * @param integer $f056fverimentType
     *
     * @return T056fverimentMaster
     */
    public function setF056fverimentType($f056fverimentType)
    {
        $this->f056fverimentType = $f056fverimentType;

        return $this;
    }

    /**
     * Get f056fverimentType
     *
     * @return integer
     */
    public function getF056fverimentType()
    {
        return $this->f056fverimentType;
    }

    /**
     * Set f056ffromDepartment
     *
     * @param string $f056ffromDepartment
     *
     * @return T056fverimentMaster
     */
    public function setF056ffromDepartment($f056ffromDepartment)
    {
        $this->f056ffromDepartment = $f056ffromDepartment;

        return $this;
    }

    /**
     * Get f056ffromDepartment
     *
     * @return string
     */
    public function getF056ffromDepartment()
    {
        return $this->f056ffromDepartment;
    }

    /**
     * Set f056ffromFundCode
     *
     * @param string $f056ffromFundCode
     *
     * @return T056fverimentMaster
     */
    public function setF056ffromFundCode($f056ffromFundCode)
    {
        $this->f056ffromFundCode = $f056ffromFundCode;

        return $this;
    }

    /**
     * Get f056ffromFundCode
     *
     * @return string
     */
    public function getF056ffromFundCode()
    {
        return $this->f056ffromFundCode;
    }

    /**
     * Set f056ffromAccountCode
     *
     * @param string $f056ffromAccountCode
     *
     * @return T056fverimentMaster
     */
    public function setF056ffromAccountCode($f056ffromAccountCode)
    {
        $this->f056ffromAccountCode = $f056ffromAccountCode;

        return $this;
    }

    /**
     * Get f056ffromAccountCode
     *
     * @return string
     */
    public function getF056ffromAccountCode()
    {
        return $this->f056ffromAccountCode;
    }

    /**
     * Set f056ffromActivityCode
     *
     * @param string $f056ffromActivityCode
     *
     * @return T056fverimentMaster
     */
    public function setF056ffromActivityCode($f056ffromActivityCode)
    {
        $this->f056ffromActivityCode = $f056ffromActivityCode;

        return $this;
    }

    /**
     * Get f056ffromActivityCode
     *
     * @return string
     */
    public function getF056ffromActivityCode()
    {
        return $this->f056ffromActivityCode;
    }

    /**
     * Set f056ffromDepartmentCode
     *
     * @param string $f056ffromDepartmentCode
     *
     * @return T056fverimentMaster
     */
    public function setF056ffromDepartmentCode($f056ffromDepartmentCode)
    {
        $this->f056ffromDepartmentCode = $f056ffromDepartmentCode;

        return $this;
    }

    /**
     * Get f056ffromDepartmentCode
     *
     * @return string
     */
    public function getF056ffromDepartmentCode()
    {
        return $this->f056ffromDepartmentCode;
    }

    /**
     * Set f056ftotalAmount
     *
     * @param string $f056ftotalAmount
     *
     * @return T056fverimentMaster
     */
    public function setF056ftotalAmount($f056ftotalAmount)
    {
        $this->f056ftotalAmount = $f056ftotalAmount;

        return $this;
    }

    /**
     * Get f056ftotalAmount
     *
     * @return string
     */
    public function getF056ftotalAmount()
    {
        return $this->f056ftotalAmount;
    }

    /**
     * Set f056fapproval
     *
     * @param integer $f056fapproval
     *
     * @return T056fverimentMaster
     */
    public function setF056fapproval($f056fapproval)
    {
        $this->f056fapproval = $f056fapproval;

        return $this;
    }

    /**
     * Get f056fapproval
     *
     * @return integer
     */
    public function getF056fapproval()
    {
        return $this->f056fapproval;
    }

    /**
     * Set f056fcreatedBy
     *
     * @param integer $f056fcreatedBy
     *
     * @return T056fverimentMaster
     */
    public function setF056fcreatedBy($f056fcreatedBy)
    {
        $this->f056fcreatedBy = $f056fcreatedBy;

        return $this;
    }

    /**
     * Get f056fcreatedBy
     *
     * @return integer
     */
    public function getF056fcreatedBy()
    {
        return $this->f056fcreatedBy;
    }

    /**
     * Set f056fupdatedBy
     *
     * @param integer $f056fupdatedBy
     *
     * @return T056fverimentMaster
     */
    public function setF056fupdatedBy($f056fupdatedBy)
    {
        $this->f056fupdatedBy = $f056fupdatedBy;

        return $this;
    }

    /**
     * Get f056fupdatedBy
     *
     * @return integer
     */
    public function getF056fupdatedBy()
    {
        return $this->f056fupdatedBy;
    }

    /**
     * Set f056fcreatedDtTm
     *
     * @param \DateTime $f056fcreatedDtTm
     *
     * @return T056fverimentMaster
     */
    public function setF056fcreatedDtTm($f056fcreatedDtTm)
    {
        $this->f056fcreatedDtTm = $f056fcreatedDtTm;

        return $this;
    }

    /**
     * Get f056fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF056fcreatedDtTm()
    {
        return $this->f056fcreatedDtTm;
    }

    /**
     * Set f056fupdatedDtTm
     *
     * @param \DateTime $f056fupdatedDtTm
     *
     * @return T056fverimentMaster
     */
    public function setF056fupdatedDtTm($f056fupdatedDtTm)
    {
        $this->f056fupdatedDtTm = $f056fupdatedDtTm;

        return $this;
    }

    /**
     * Get f056fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF056fupdatedDtTm()
    {
        return $this->f056fupdatedDtTm;
    }

    /**
     * Set f056fstatus
     *
     * @param integer $f056fstatus
     *
     * @return T056fverimentMaster
     */
    public function setF056fstatus($f056fstatus)
    {
        $this->f056fstatus = $f056fstatus;

        return $this;
    }

    /**
     * Get f056fstatus
     *
     * @return integer
     */
    public function getF056fstatus()
    {
        return $this->f056fstatus;
    }

    /**
     * Set f056freason
     *
     * @param string $f056freason
     *
     * @return T056fverimentMaster
     */
    public function setF056freason($f056freason)
    {
        $this->f056freason = $f056freason;

        return $this;
    }

    /**
     * Get f056freason
     *
     * @return string
     */
    public function getF056freason()
    {
        return $this->f056freason;
    }
}
