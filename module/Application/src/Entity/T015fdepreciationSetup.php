<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T015fdepreciationSetup
 *
 * @ORM\Table(name="t015fdepreciation_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\DepreciationSetupRepository")
 */
class T015fdepreciationSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f015fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f015fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdep_account_code", type="string", length=255, nullable=false)
     */
    private $f015fdepAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fap_account_code", type="string", length=225, nullable=false)
     */
    private $f015fapAccountCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f015fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fstatus", type="integer", nullable=false)
     */
    private $f015fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f015fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fdepreciation_code", type="string",length=60, nullable=false)
     * 
     */
    private $f015fdepreciationCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fdepreciation_description", type="string",length=60, nullable=false)
     * 
     */
    private $f015fdepreciationDescription;


    /**
     * @var integer
     *
     * @ORM\Column(name="f015flife_span", type="string",length=60, nullable=false)
     * 
     */
    private $f015flifeSpan;


    /**
     * @var integer
     *
     * @ORM\Column(name="f015fdepreciation_rate", type="string",length=60, nullable=false)
     * 
     */
    private $f015fdepreciationRate;



    /**
     * Get f015fid
     *
     * @return integer
     */
    public function getF015fid()
    {
        return $this->f015fid;
    }

    /**
     * Set f015fdepAccountCode
     *
     * @param string $f015fdepAccountCode
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fdepAccountCode($f015fdepAccountCode)
    {
        $this->f015fdepAccountCode = $f015fdepAccountCode;

        return $this;
    }

    /**
     * Get f015fdepAccountCode
     *
     * @return string
     */
    public function getF015fdepAccountCode()
    {
        return $this->f015fdepAccountCode;
    }

    /**
     * Set f015fapAccountCode
     *
     * @param string $f015fapAccountCode
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fapAccountCode($f015fapAccountCode)
    {
        $this->f015fapAccountCode = $f015fapAccountCode;

        return $this;
    }

    /**
     * Get f015fapAccountCode
     *
     * @return string
     */
    public function getF015fapAccountCode()
    {
        return $this->f015fapAccountCode;
    }


    /**
     * Set f015fupdatedDtTm
     *
     * @param \DateTime $f015fupdatedDtTm
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fupdatedDtTm($f015fupdatedDtTm)
    {
        $this->f015fupdatedDtTm = $f015fupdatedDtTm;

        return $this;
    }

    /**
     * Get f015fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF015fupdatedDtTm()
    {
        return $this->f015fupdatedDtTm;
    }

    /**
     * Set f015fstatus
     *
     * @param integer $f015fstatus
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fstatus($f015fstatus)
    {
        $this->f015fstatus = $f015fstatus;

        return $this;
    }

    /**
     * Get f015fstatus
     *
     * @return integer
     */
    public function getF015fstatus()
    {
        return $this->f015fstatus;
    }

    /**
     * Set f015fupdatedBy
     *
     * @param integer $f015fupdatedBy
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fupdatedBy($f015fupdatedBy)
    {
        $this->f015fupdatedBy = $f015fupdatedBy;

        return $this;
    }

    /**
     * Get f015fupdatedBy
     *
     * @return integer
     */
    public function getF015fupdatedBy()
    {
        return $this->f015fupdatedBy;
    }

    /**
     * Set f015fdepreciationCode
     *
     * @param integer $f015fdepreciationCode
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fdepreciationCode($f015fdepreciationCode)
    {
        $this->f015fdepreciationCode = $f015fdepreciationCode;

        return $this;
    }

    /**
     * Get f015fdepreciationCode
     *
     * @return integer
     */
    public function getF015fdepreciationCode()
    {
        return $this->f015fdepreciationCode;
    }


    /**
     * Set f015fdepreciationDescription
     *
     * @param integer $f015fdepreciationDescription
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fdepreciationDescription($f015fdepreciationDescription)
    {
        $this->f015fdepreciationDescription = $f015fdepreciationDescription;

        return $this;
    }

    /**
     * Get f015fdepreciationDescription
     *
     * @return integer
     */
    public function getF015fdepreciationDescription()
    {
        return $this->f015fdepreciationDescription;
    }



    /**
     * Set f015flifeSpan
     *
     * @param integer $f015flifeSpan
     *
     * @return T015fdepreciationSetup
     */
    public function setF015flifeSpan($f015flifeSpan)
    {
        $this->f015flifeSpan = $f015flifeSpan;

        return $this;
    }

    /**
     * Get f015flifeSpan
     *
     * @return integer
     */
    public function getF015flifeSpan()
    {
        return $this->f015flifeSpan;
    }



    /**
     * Set f015fdepreciationRate
     *
     * @param integer $f015fdepreciationRate
     *
     * @return T015fdepreciationSetup
     */
    public function setF015fdepreciationRate($f015fdepreciationRate)
    {
        $this->f015fdepreciationRate = $f015fdepreciationRate;

        return $this;
    }

    /**
     * Get f015fdepreciationRate
     *
     * @return integer
     */
    public function getF015fdepreciationRate()
    {
        return $this->f015fdepreciationRate;
    }

}
