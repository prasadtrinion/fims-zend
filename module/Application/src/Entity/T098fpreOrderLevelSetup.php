<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T098fpreOrderLevelSetup
 *
 * @ORM\Table(name="t098fpre_order_level_setup", uniqueConstraints={@ORM\UniqueConstraint(name="f098fkod_stock", columns={"f098fkod_stock"})})
 * @ORM\Entity(repositoryClass="Application\Repository\PreOrderLevelSetupRepository")
 */
class T098fpreOrderLevelSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f098fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fkod_stock", type="string", length=50, nullable=false)
     */
    private $f098fkodStock;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_category", type="integer", nullable=true)
     */
    private $f098fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_sub_category", type="integer", nullable=true)
     */
    private $f098fidSubCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_asset_type", type="integer", nullable=true)
     */
    private $f098fidAssetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid_item", type="integer", nullable=true)
     */
    private $f098fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fpre_order_level", type="integer", nullable=true)
     */
    private $f098fpreOrderLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fstatus", type="integer", nullable=true)
     */
    private $f098fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fcreated_by", type="integer", nullable=true)
     */
    private $f098fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fupdated_by", type="integer", nullable=true)
     */
    private $f098fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f098fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f098fupdatedDtTm;



    /**
     * Get f098fid
     *
     * @return integer
     */
    public function getF098fid()
    {
        return $this->f098fid;
    }

    /**
     * Set f098fkodStock
     *
     * @param string $f098fkodStock
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fkodStock($f098fkodStock)
    {
        $this->f098fkodStock = $f098fkodStock;

        return $this;
    }

    /**
     * Get f098fkodStock
     *
     * @return string
     */
    public function getF098fkodStock()
    {
        return $this->f098fkodStock;
    }

    /**
     * Set f098fidCategory
     *
     * @param integer $f098fidCategory
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fidCategory($f098fidCategory)
    {
        $this->f098fidCategory = $f098fidCategory;

        return $this;
    }

    /**
     * Get f098fidCategory
     *
     * @return integer
     */
    public function getF098fidCategory()
    {
        return $this->f098fidCategory;
    }

    /**
     * Set f098fidSubCategory
     *
     * @param integer $f098fidSubCategory
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fidSubCategory($f098fidSubCategory)
    {
        $this->f098fidSubCategory = $f098fidSubCategory;

        return $this;
    }

    /**
     * Get f098fidSubCategory
     *
     * @return integer
     */
    public function getF098fidSubCategory()
    {
        return $this->f098fidSubCategory;
    }

    /**
     * Set f098fidAssetType
     *
     * @param integer $f098fidAssetType
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fidAssetType($f098fidAssetType)
    {
        $this->f098fidAssetType = $f098fidAssetType;

        return $this;
    }

    /**
     * Get f098fidAssetType
     *
     * @return integer
     */
    public function getF098fidAssetType()
    {
        return $this->f098fidAssetType;
    }

    /**
     * Set f098fidItem
     *
     * @param integer $f098fidItem
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fidItem($f098fidItem)
    {
        $this->f098fidItem = $f098fidItem;

        return $this;
    }

    /**
     * Get f098fidItem
     *
     * @return integer
     */
    public function getF098fidItem()
    {
        return $this->f098fidItem;
    }

    /**
     * Set f098fpreOrderLevel
     *
     * @param integer $f098fpreOrderLevel
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fpreOrderLevel($f098fpreOrderLevel)
    {
        $this->f098fpreOrderLevel = $f098fpreOrderLevel;

        return $this;
    }

    /**
     * Get f098fpreOrderLevel
     *
     * @return integer
     */
    public function getF098fpreOrderLevel()
    {
        return $this->f098fpreOrderLevel;
    }

    /**
     * Set f098fstatus
     *
     * @param integer $f098fstatus
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fstatus($f098fstatus)
    {
        $this->f098fstatus = $f098fstatus;

        return $this;
    }

    /**
     * Get f098fstatus
     *
     * @return integer
     */
    public function getF098fstatus()
    {
        return $this->f098fstatus;
    }

    /**
     * Set f098fcreatedBy
     *
     * @param integer $f098fcreatedBy
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fcreatedBy($f098fcreatedBy)
    {
        $this->f098fcreatedBy = $f098fcreatedBy;

        return $this;
    }

    /**
     * Get f098fcreatedBy
     *
     * @return integer
     */
    public function getF098fcreatedBy()
    {
        return $this->f098fcreatedBy;
    }

    /**
     * Set f098fupdatedBy
     *
     * @param integer $f098fupdatedBy
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fupdatedBy($f098fupdatedBy)
    {
        $this->f098fupdatedBy = $f098fupdatedBy;

        return $this;
    }

    /**
     * Get f098fupdatedBy
     *
     * @return integer
     */
    public function getF098fupdatedBy()
    {
        return $this->f098fupdatedBy;
    }

    /**
     * Set f098fcreatedDtTm
     *
     * @param \DateTime $f098fcreatedDtTm
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fcreatedDtTm($f098fcreatedDtTm)
    {
        $this->f098fcreatedDtTm = $f098fcreatedDtTm;

        return $this;
    }

    /**
     * Get f098fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fcreatedDtTm()
    {
        return $this->f098fcreatedDtTm;
    }

    /**
     * Set f098fupdatedDtTm
     *
     * @param \DateTime $f098fupdatedDtTm
     *
     * @return T098fpreOrderLevelSetup
     */
    public function setF098fupdatedDtTm($f098fupdatedDtTm)
    {
        $this->f098fupdatedDtTm = $f098fupdatedDtTm;

        return $this;
    }

    /**
     * Get f098fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fupdatedDtTm()
    {
        return $this->f098fupdatedDtTm;
    }
}
