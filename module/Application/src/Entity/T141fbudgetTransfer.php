<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T141fbudgetTransfer
 *
 * @ORM\Table(name="t141fbudget_transfer")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetTransferRepository")
 */
class T141fbudgetTransfer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f141fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f141fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f141ffrom_fund", type="string", length=255, nullable=false)
     */
    private $f141ffromFund;


    /**
     * @var string
     *
     * @ORM\Column(name="f141fto_fund", type="string", length=255, nullable=false)
     */
    private $f141ftoFund;

    /**
     * @var integer
     *
     * @ORM\Column(name="f141fstatus", type="integer", nullable=false)
     */
    private $f141fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f141fcreated_by", type="integer", nullable=false)
     */
    private $f141fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f141fupdated_by", type="integer", nullable=false)
     */
    private $f141fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f141fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f141fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f141fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f141fupdatedDtTm;



    /**
     * Get f141fid
     *
     * @return integer
     */
    public function getF141fid()
    {
        return $this->f141fid;
    }

    /**
     * Set f141ffromFund
     *
     * @param string $f141ffromFund
     *
     * @return T141fbudgetTransfer
     */
    public function setF141ffromFund($f141ffromFund)
    {
        $this->f141ffromFund = $f141ffromFund;

        return $this;
    }

    /**
     * Get f141ffromFund
     *
     * @return string
     */
    public function getF141ffromFund()
    {
        return $this->f141ffromFund;
    }


    /**
     * Set f141ftoFund
     *
     * @param string $f141ftoFund
     *
     * @return T141fbudgetTransfer
     */
    public function setF141ftoFund($f141ftoFund)
    {
        $this->f141ftoFund = $f141ftoFund;

        return $this;
    }

    /**
     * Get f141ftoFund
     *
     * @return string
     */
    public function getF141ftoFund()
    {
        return $this->f141ftoFund;
    }


    /**
     * Set f141fstatus
     *
     * @param integer $f141fstatus
     *
     * @return T141fbudgetTransfer
     */
    public function setF141fstatus($f141fstatus)
    {
        $this->f141fstatus = $f141fstatus;

        return $this;
    }

    /**
     * Get f141fstatus
     *
     * @return integer
     */
    public function getF141fstatus()
    {
        return $this->f141fstatus;
    }

    /**
     * Set f141fcreatedBy
     *
     * @param integer $f141fcreatedBy
     *
     * @return T141fbudgetTransfer
     */
    public function setF141fcreatedBy($f141fcreatedBy)
    {
        $this->f141fcreatedBy = $f141fcreatedBy;

        return $this;
    }

    /**
     * Get f141fcreatedBy
     *
     * @return integer
     */
    public function getF141fcreatedBy()
    {
        return $this->f141fcreatedBy;
    }

    /**
     * Set f141fupdatedBy
     *
     * @param integer $f141fupdatedBy
     *
     * @return T141fbudgetTransfer
     */
    public function setF141fupdatedBy($f141fupdatedBy)
    {
        $this->f141fupdatedBy = $f141fupdatedBy;

        return $this;
    }

    /**
     * Get f141fupdatedBy
     *
     * @return integer
     */
    public function getF141fupdatedBy()
    {
        return $this->f141fupdatedBy;
    }

    /**
     * Set f141fcreatedDtTm
     *
     * @param \DateTime $f141fcreatedDtTm
     *
     * @return T141fbudgetTransfer
     */
    public function setF141fcreatedDtTm($f141fcreatedDtTm)
    {
        $this->f141fcreatedDtTm = $f141fcreatedDtTm;

        return $this;
    }

    /**
     * Get f141fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF141fcreatedDtTm()
    {
        return $this->f141fcreatedDtTm;
    }

    /**
     * Set f141fupdatedDtTm
     *
     * @param \DateTime $f141fupdatedDtTm
     *
     * @return T141fbudgetTransfer
     */
    public function setF141fupdatedDtTm($f141fupdatedDtTm)
    {
        $this->f141fupdatedDtTm = $f141fupdatedDtTm;

        return $this;
    }

    /**
     * Get f141fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF141fupdatedDtTm()
    {
        return $this->f141fupdatedDtTm;
    }
}
