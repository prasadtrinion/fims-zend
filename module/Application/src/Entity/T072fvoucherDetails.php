<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T072fvoucherDetails
 *
 * @ORM\Table(name="t072fvoucher_details")
 * @ORM\Entity(repositoryClass="Application\Repository\VoucherRepository")
 */
class T072fvoucherDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f072fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid_item", type="integer", nullable=false)
     */
    private $f072fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fgst_code", type="string", nullable=false)
     */
    private $f072fgstCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fgst_value", type="integer", nullable=false)
     */
    private $f072fgstValue;
 
    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid_debit_glcode", type="integer", nullable=false)
     */
    private $f072fidDebitGlcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid_credit_glcode", type="integer", nullable=false)
     */
    private $f072fidCreditGlcode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f072fquantity", type="integer", nullable=false)
     */
    private $f072fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fprice", type="float",scale=2, nullable=false)
     */
    private $f072fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072ftotal", type="float",scale=2, nullable=false)
     */
    private $f072ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fstatus", type="integer", nullable=false)
     */
    private $f072fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fcreated_by", type="integer", nullable=false)
     */
    private $f072fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fupdated_by", type="integer", nullable=false)
     */
    private $f072fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f072fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f072fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f072fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f072fupdatedDtTm;

    /**
     * @var \Application\Entity\T071fvoucher
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T071fvoucher")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f072fid_voucher", referencedColumnName="f071fid")
     * })
     */
    private $f072fidVoucher;

    /**
     * Get f072fid
     *
     * @return integer
     */
    public function getF072fid()
    {
        return $this->f072fid;
    }

    /**
     * Set f072fidItem
     *
     * @param integer $f072fidItem
     *
     * @return T072fvoucherDetails
     */
    public function setF072fidItem($f072fidItem)
    {
        $this->f072fidItem = $f072fidItem;

        return $this;
    }

    /**
     * Get f072fidItem
     *
     * @return integer
     */
    public function getF072fidItem()
    {
        return $this->f072fidItem;
    }


    /**
     * Set f072fgstCode
     *
     * @param string $f072fgstCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fgstCode($f072fgstCode)
    {
        $this->f072fgstCode = $f072fgstCode;

        return $this;
    }

    /**
     * Get f072fgstCode
     *
     * @return string
     */
    public function getF072fgstCode()
    {
        return $this->f072fgstCode;
    }


    /**
     * Set f072fgstValue
     *
     * @param integer $f072fgstValue
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fgstValue($f072fgstValue)
    {
        $this->f072fgstValue = $f072fgstValue;

        return $this;
    }

    /**
     * Get f072fgstValue
     *
     * @return integer
     */
    public function getF072fgstValue()
    {
        return $this->f072fgstValue;
    }

    /**
     * Set f072fidDebitGlcode
     *
     * @param integer $f072fidDebitGlcode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fidDebitGlcode($f072fidDebitGlcode)
    {
        $this->f072fidDebitGlcode = $f072fidDebitGlcode;

        return $this;
    }

    /**
     * Get f072fidDebitGlcode
     *
     * @return integer
     */
    public function getF072fidDebitGlcode()
    {
        return $this->f072fidDebitGlcode;
    }


    /**
     * Set f072fidCreditGlcode
     *
     * @param integer $f072fidCreditGlcode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fidCreditGlcode($f072fidCreditGlcode)
    {
        $this->f072fidCreditGlcode = $f072fidCreditGlcode;

        return $this;
    }

    /**
     * Get f072fidCreditGlcode
     *
     * @return integer
     */
    public function getF072fidCreditGlcode()
    {
        return $this->f072fidCreditGlcode;
    }

    /**
     * Set f072fquantity
     *
     * @param integer $f072fquantity
     *
     * @return T072fvoucherDetails
     */
    public function setF072fquantity($f072fquantity)
    {
        $this->f072fquantity = $f072fquantity;

        return $this;
    }

    /**
     * Get f072fquantity
     *
     * @return integer
     */
    public function getF072fquantity()
    {
        return $this->f072fquantity;
    }

    /**
     * Set f072fprice
     *
     * @param integer $f072fprice
     *
     * @return T072fvoucherDetails
     */
    public function setF072fprice($f072fprice)
    {
        $this->f072fprice = $f072fprice;

        return $this;
    }

    /**
     * Get f072fprice
     *
     * @return integer
     */
    public function getF072fprice()
    {
        return $this->f072fprice;
    }

    /**
     * Set f072ftotal
     *
     * @param integer $f072ftotal
     *
     * @return T072fvoucherDetails
     */
    public function setF072ftotal($f072ftotal)
    {
        $this->f072ftotal = $f072ftotal;

        return $this;
    }

    /**
     * Get f072ftotal
     *
     * @return integer
     */
    public function getF072ftotal()
    {
        return $this->f072ftotal;
    }

    /**
     * Set f072fstatus
     *
     * @param integer $f072fstatus
     *
     * @return T072fvoucherDetails
     */
    public function setF072fstatus($f072fstatus)
    {
        $this->f072fstatus = $f072fstatus;

        return $this;
    }

    /**
     * Get f072fstatus
     *
     * @return integer
     */
    public function getF072fstatus()
    {
        return $this->f072fstatus;
    }

    /**
     * Set f072fcreatedBy
     *
     * @param integer $f072fcreatedBy
     *
     * @return T072fvoucherDetails
     */
    public function setF072fcreatedBy($f072fcreatedBy)
    {
        $this->f072fcreatedBy = $f072fcreatedBy;

        return $this;
    }

    /**
     * Get f072fcreatedBy
     *
     * @return integer
     */
    public function getF072fcreatedBy()
    {
        return $this->f072fcreatedBy;
    }

    /**
     * Set f072fupdatedBy
     *
     * @param integer $f072fupdatedBy
     *
     * @return T072fvoucherDetails
     */
    public function setF072fupdatedBy($f072fupdatedBy)
    {
        $this->f072fupdatedBy = $f072fupdatedBy;

        return $this;
    }

    /**
     * Get f072fupdatedBy
     *
     * @return integer
     */
    public function getF072fupdatedBy()
    {
        return $this->f072fupdatedBy;
    }

    /**
     * Set f072fcreatedDtTm
     *
     * @param \DateTime $f072fcreatedDtTm
     *
     * @return T072fvoucherDetails
     */
    public function setF072fcreatedDtTm($f072fcreatedDtTm)
    {
        $this->f072fcreatedDtTm = $f072fcreatedDtTm;

        return $this;
    }

    /**
     * Get f072fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF072fcreatedDtTm()
    {
        return $this->f072fcreatedDtTm;
    }

    /**
     * Set f072fupdatedDtTm
     *
     * @param \DateTime $f072fupdatedDtTm
     *
     * @return T072fvoucherDetails
     */
    public function setF072fupdatedDtTm($f072fupdatedDtTm)
    {
        $this->f072fupdatedDtTm = $f072fupdatedDtTm;

        return $this;
    }

    /**
     * Get f072fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF072fupdatedDtTm()
    {
        return $this->f072fupdatedDtTm;
    }
    
    /**
     * Set f072fidVoucher
     *
     * @param \Application\Entity\T071fvoucher $f072fidVoucher
     *
     * @return T072fvoucherDetails
     */
    public function setF072fidVoucher(\Application\Entity\T071fvoucher $f072fidVoucher = null)
    {
        $this->f072fidVoucher = $f072fidVoucher;

        return $this;
    }

    /**
     * Get f072fidVoucher
     *
     * @return \Application\Entity\T071fvoucher
     */
    public function getF072fidVoucher()
    {
        return $this->f072fidVoucher;
    }

}

