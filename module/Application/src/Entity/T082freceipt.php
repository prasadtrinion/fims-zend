<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T082freceipt
 *
 * @ORM\Table(name="t082freceipt", indexes={@ORM\Index(name="f082fpayment_bank", columns={"f082fpayment_bank"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ReceiptRepository")
 */
class T082freceipt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f082fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f082freceipt_number", type="string", length=50, nullable=false)
     */
    private $f082freceiptNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_customer", type="integer", nullable=true)
     */
    private $f082fidCustomer;


    /**
     * @var integer
     *
     * @ORM\Column(name="f082fpayee_type", type="string",length=50, nullable=false)
     */
    private $f082fpayeeType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fpayment_bank", type="integer", nullable=false)
     */
    private $f082fpaymentBank;  

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_invoice", type="integer", nullable=true)
     */
    private $f082fidInvoice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fdate", type="datetime", nullable=false)
     */
    private $f082fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082ftotal_amount", type="integer", nullable=false)
     */
    private $f082ftotalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f082fdescription", type="text", length=65535, nullable=false)
     */
    private $f082fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fstatus", type="integer", nullable=false)
     */
    private $f082fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f082fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f082fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fcreated_by", type="integer", nullable=false)
     */
    private $f082fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fupdated_by", type="integer", nullable=false)
     */
    private $f082fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082freason", type="string",length=50, nullable=true)
     */
    private $f082freason;

    

    /**
     * Get f082fid
     *
     * @return integer
     */
    public function getF082fid()
    {
        return $this->f082fid;
    }

    /**
     * Set f082freceiptNumber
     *
     * @param string $f082freceiptNumber
     *
     * @return T082freceipt
     */
    public function setF082freceiptNumber($f082freceiptNumber)
    {
        $this->f082freceiptNumber = $f082freceiptNumber;

        return $this;
    }

    /**
     * Get f082freceiptNumber
     *
     * @return string
     */
    public function getF082freceiptNumber()
    {
        return $this->f082freceiptNumber;
    }
      /**
     * Set f082fidCustomer
     *
     * @param integer $f082fidCustomer
     *
     * @return T082freceipt
     */
    public function setF082fidCustomer($f082fidCustomer)
    {
        $this->f082fidCustomer = $f082fidCustomer;

        return $this;
    }

    /**
     * Get f082fidCustomer
     *
     * @return integer
     */
    public function getF082fidCustomer()
    {
        return $this->f082fidCustomer;
    }

       /**
     * Set f082fidInvoice
     *
     * @param integer $f082fidInvoice
     *
     * @return T082freceipt
     */
    public function setF082fidInvoice($f082fidInvoice)
    {
        $this->f082fidInvoice = $f082fidInvoice;

        return $this;
    }

    /**
     * Get f082fidInvoice
     *
     * @return integer
     */
    public function getF082fidInvoice()
    {
        return $this->f082fidInvoice;
    }

    /**
     * Set f082fpayeeType
     *
     * @param integer $f082fpayeeType
     *
     * @return T082freceipt
     */
    public function setF082fpayeeType($f082fpayeeType)
    {
        $this->f082fpayeeType = $f082fpayeeType;

        return $this;
    }

    /**
     * Get f082fpayeeType
     *
     * @return integer
     */
    public function getF082fpayeeType()
    {
        return $this->f082fpayeeType;
    }

    /**
     * Set f082fpaymentBank
     *
     * @param integer $f082fpaymentBank
     *
     * @return T082freceipt
     */
    public function setF082fpaymentBank($f082fpaymentBank)
    {
        $this->f082fpaymentBank = $f082fpaymentBank;

        return $this;
    }

    /**
     * Get f082fpaymentBank
     *
     * @return integer
     */
    public function getF082fpaymentBank()
    {
        return $this->f082fpaymentBank;
    }

    /**
     * Set f082fdate
     *
     * @param \DateTime $f082fdate
     *
     * @return T082freceipt
     */
    public function setF082fdate($f082fdate)
    {
        $this->f082fdate = $f082fdate;

        return $this;
    }

    /**
     * Get f082fdate
     *
     * @return \DateTime
     */
    public function getF082fdate()
    {
        return $this->f082fdate;
    }

    /**
     * Set f082ftotalAmount
     *
     * @param integer $f082ftotalAmount
     *
     * @return T082freceipt
     */
    public function setF082ftotalAmount($f082ftotalAmount)
    {
        $this->f082ftotalAmount = $f082ftotalAmount;

        return $this;
    }

    /**
     * Get f082ftotalAmount
     *
     * @return integer
     */
    public function getF082ftotalAmount()
    {
        return $this->f082ftotalAmount;
    }

    /**
     * Set f082fdescription
     *
     * @param string $f082fdescription
     *
     * @return T082freceipt
     */
    public function setF082fdescription($f082fdescription)
    {
        $this->f082fdescription = $f082fdescription;

        return $this;
    }

    /**
     * Get f082fdescription
     *
     * @return string
     */
    public function getF082fdescription()
    {
        return $this->f082fdescription;
    }

    /**
     * Set f082fstatus
     *
     * @param integer $f082fstatus
     *
     * @return T082freceipt
     */
    public function setF082fstatus($f082fstatus)
    {
        $this->f082fstatus = $f082fstatus;

        return $this;
    }

    /**
     * Get f082fstatus
     *
     * @return integer
     */
    public function getF082fstatus()
    {
        return $this->f082fstatus;
    }

    /**
     * Set f082fcreatedDtTm
     *
     * @param \DateTime $f082fcreatedDtTm
     *
     * @return T082freceipt
     */
    public function setF082fcreatedDtTm($f082fcreatedDtTm)
    {
        $this->f082fcreatedDtTm = $f082fcreatedDtTm;

        return $this;
    }

    /**
     * Get f082fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fcreatedDtTm()
    {
        return $this->f082fcreatedDtTm;
    }

    /**
     * Set f082fupdatedDtTm
     *
     * @param \DateTime $f082fupdatedDtTm
     *
     * @return T082freceipt
     */
    public function setF082fupdatedDtTm($f082fupdatedDtTm)
    {
        $this->f082fupdatedDtTm = $f082fupdatedDtTm;

        return $this;
    }

    /**
     * Get f082fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fupdatedDtTm()
    {
        return $this->f082fupdatedDtTm;
    }

    /**
     * Set f082fcreatedBy
     *
     * @param integer $f082fcreatedBy
     *
     * @return T082freceipt
     */
    public function setF082fcreatedBy($f082fcreatedBy)
    {
        $this->f082fcreatedBy = $f082fcreatedBy;

        return $this;
    }

    /**
     * Get f082fcreatedBy
     *
     * @return integer
     */
    public function getF082fcreatedBy()
    {
        return $this->f082fcreatedBy;
    }

    /**
     * Set f082fupdatedBy
     *
     * @param integer $f082fupdatedBy
     *
     * @return T082freceipt
     */
    public function setF082fupdatedBy($f082fupdatedBy)
    {
        $this->f082fupdatedBy = $f082fupdatedBy;

        return $this;
    }

    /**
     * Get f082fupdatedBy
     *
     * @return integer
     */
    public function getF082fupdatedBy()
    {
        return $this->f082fupdatedBy;
    }

    /**
     * Set f082freason
     *
     * @param string $f082freason
     *
     * @return T082freceipt
     */
    public function setF082freason($f082freason)
    {
        $this->f082freason = $f082freason;

        return $this;
    }

    /**
     * Get f082freason
     *
     * @return string
     */
    public function getF082freason()
    {
        return $this->f082freason;
    }
}
