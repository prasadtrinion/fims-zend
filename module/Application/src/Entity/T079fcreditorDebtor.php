<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T079fcreditorDebtor
 *
 * @ORM\Table(name="t079fcreditor_debtor")
 * @ORM\Entity(repositoryClass="Application\Repository\CreditorDebtorRepository")
 */
class T079fcreditorDebtor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f079fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f079fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079fcustomer", type="integer", nullable=false)
     */
    private $f079fcustomer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f079fdate", type="datetime", nullable=false)
     */
    private $f079fdate;

    /**
     * @var float
     *
     * @ORM\Column(name="f079fadvance_payment", type="float",scale=2, nullable=false)
     */
    private $f079fadvancePayment;

    /**
     * @var float
     *
     * @ORM\Column(name="f079fapproved_by", type="integer",scale=2, nullable=false)
     */
    private $f079fapprovedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079ftotal_amount", type="integer", nullable=false)
     */
    private $f079ftotalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f079fnarration", type="string", length=50, nullable=false)
     */
    private $f079fnarration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079fstatus", type="integer", nullable=false)
     */
    private $f079fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079fcreated_by", type="integer", nullable=false)
     */
    private $f079fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079fupdated_by", type="integer", nullable=false)
     */
    private $f079fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f079fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f079fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f079fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f079fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f079fcr_dr_flag", type="integer", nullable=false)
     */
    private $f079fcrDrFlag;

    
    /**
     * Get f079fid
     *
     * @return integer
     */
    public function getF079fid()
    {
        return $this->f079fid;
    }

    /**
     * Set f079fcustomer
     *
     * @param integer $f079fcustomer
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fcustomer($f079fcustomer)
    {
        $this->f079fcustomer = $f079fcustomer;

        return $this;
    }

    /**
     * Get f079fcustomer
     *
     * @return integer
     */
    public function getF079fcustomer()
    {
        return $this->f079fcustomer;
    }

    /**
     * Set f079fdate
     *
     * @param \DateTime $f079fdate
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fdate($f079fdate)
    {
        $this->f079fdate = $f079fdate;

        return $this;
    }

    /**
     * Get f079fdate
     *
     * @return \DateTime
     */
    public function getF079fdate()
    {
        return $this->f079fdate;
    }

    /**
     * Set f079fadvancePayment
     *
     * @param integer $f079fadvancePayment
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fadvancePayment($f079fadvancePayment)
    {
        $this->f079fadvancePayment = $f079fadvancePayment;

        return $this;
    }

    /**
     * Get f079fadvancePayment
     *
     * @return integer
     */
    public function getF079fadvancePayment()
    {
        return $this->f079fadvancePayment;
    }

    /**
     * Set f079fapprovedBy
     *
     * @param integer $f079fapprovedBy
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fapprovedBy($f079fapprovedBy)
    {
        $this->f079fapprovedBy = $f079fapprovedBy;

        return $this;
    }

    /**
     * Get f079fapprovedBy
     *
     * @return integer
     */
    public function getF079fapprovedBy()
    {
        return $this->f079fapprovedBy;
    }

    /**
     * Set f079ftotalAmount
     *
     * @param integer $f079ftotalAmount
     *
     * @return T079fcreditorDebtor
     */
    public function setF079ftotalAmount($f079ftotalAmount)
    {
        $this->f079ftotalAmount = $f079ftotalAmount;

        return $this;
    }

    /**
     * Get f079ftotalAmount
     *
     * @return integer
     */
    public function getF079ftotalAmount()
    {
        return $this->f079ftotalAmount;
    }

    /**
     * Set f079fnarration
     *
     * @param string $f079fnarration
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fnarration($f079fnarration)
    {
        $this->f079fnarration = $f079fnarration;

        return $this;
    }

    /**
     * Get f079fnarration
     *
     * @return string
     */
    public function getF079fnarration()
    {
        return $this->f079fnarration;
    }

    /**
     * Set f079fstatus
     *
     * @param integer $f079fstatus
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fstatus($f079fstatus)
    {
        $this->f079fstatus = $f079fstatus;

        return $this;
    }

    /**
     * Get f079fstatus
     *
     * @return integer
     */
    public function getF079fstatus()
    {
        return $this->f079fstatus;
    }

    /**
     * Set f079fcreatedBy
     *
     * @param integer $f079fcreatedBy
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fcreatedBy($f079fcreatedBy)
    {
        $this->f079fcreatedBy = $f079fcreatedBy;

        return $this;
    }

    /**
     * Get f079fcreatedBy
     *
     * @return integer
     */
    public function getF079fcreatedBy()
    {
        return $this->f079fcreatedBy;
    }

    /**
     * Set f079fupdatedBy
     *
     * @param integer $f079fupdatedBy
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fupdatedBy($f079fupdatedBy)
    {
        $this->f079fupdatedBy = $f079fupdatedBy;

        return $this;
    }

    /**
     * Get f079fupdatedBy
     *
     * @return integer
     */
    public function getF079fupdatedBy()
    {
        return $this->f079fupdatedBy;
    }

    /**
     * Set f079fcreatedDtTm
     *
     * @param \DateTime $f079fcreatedDtTm
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fcreatedDtTm($f079fcreatedDtTm)
    {
        $this->f079fcreatedDtTm = $f079fcreatedDtTm;

        return $this;
    }

    /**
     * Get f079fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF079fcreatedDtTm()
    {
        return $this->f079fcreatedDtTm;
    }

    /**
     * Set f079fupdatedDtTm
     *
     * @param \DateTime $f079fupdatedDtTm
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fupdatedDtTm($f079fupdatedDtTm)
    {
        $this->f079fupdatedDtTm = $f079fupdatedDtTm;

        return $this;
    }

    /**
     * Get f079fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF079fupdatedDtTm()
    {
        return $this->f079fupdatedDtTm;
    }

    /**
     * Set f079fcrDrFlag
     *
     * @param integer $f079fcrDrFlag
     *
     * @return T079fcreditorDebtor
     */
    public function setF079fcrDrFlag($f079fcrDrFlag)
    {
        $this->f079fcrDrFlag = $f079fcrDrFlag;

        return $this;
    }

    /**
     * Get f079fcrDrFlag
     *
     * @return integer
     */
    public function getF079fcrDrFlag()
    {
        return $this->f079fcrDrFlag;
    }
}
