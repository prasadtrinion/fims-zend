<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T043faddressDetails
 *
 * @ORM\Table(name="t043faddress_details", indexes={@ORM\Index(name="f043fid_ledger", columns={"f043fid_ledger"}), @ORM\Index(name="f043fupdated_by", columns={"f043fupdated_by"}), @ORM\Index(name="f043fcreated_by", columns={"f043fcreated_by"})})
 * @ORM\Entity
 */
class T043faddressDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f043fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fprint_name", type="string", length=50, nullable=false)
     */
    private $f043fprintName;


    /**
     * @var string
     *
     * @ORM\Column(name="f043fdob", type="string", nullable=false)
     */
    private $f043fdob;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fmobile", type="string", length=50, nullable=false)
     */
    private $f043fmobile;

    /**
     * @var string
     *
     * @ORM\Column(name="f043femail", type="string", length=50, nullable=false)
     */
    private $f043femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fcountry", type="string", length=50, nullable=false)
     */
    private $f043fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fstate", type="string", length=50, nullable=false)
     */
    private $f043fstate;


    /**
     * @var string
     *
     * @ORM\Column(name="f043fcity", type="string", length=50, nullable=false)
     */
    private $f043fcity;


    /**
     * @var string
     *
     * @ORM\Column(name="f043fpost_code", type="string", length=50, nullable=false)
     */
    private $f043fpostCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f043faddress1", type="string", length=50, nullable=false)
     */
    private $f043faddress1;


    /**
     * @var string
     *
     * @ORM\Column(name="f043faddress2", type="string", length=50, nullable=false)
     */
    private $f043faddress2;
    /**
     * @var boolean
     *
     * @ORM\Column(name="f043fstatus", type="boolean", nullable=false)
     */
    private $f043fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f043fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f043fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid_ledger", type="integer", length=50, nullable=false)
     */
    private $f043fidLedger;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f043fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f043fcreatedBy;



    /**
     * Get f043fid
     *
     * @return integer
     */
    public function getF043fid()
    {
        return $this->f043fid;
    }

    /**
     * Set f043fprintName
     *
     * @param string $f043fprintName
     *
     * @return T043faddressDetails
     */
    public function setF043fprintName($f043fprintName)
    {
        $this->f043fprintName = $f043fprintName;

        return $this;
    }

    /**
     * Get f043fprintName
     *
     * @return string
     */
    public function getF043fprintName()
    {
        return $this->f043fprintName;
    }

    /**
     * Set f043fdob
     *
     * @param string $f043fdob
     *
     * @return T043faddressDetails
     */
    public function setF043fdob($f043fdob)
    {
        $this->f043fdob = $f043fdob;

        return $this;
    }

    /**
     * Get f043fdob
     *
     * @return string
     */
    public function getF043fdob()
    {
        return $this->f043fdob;
    }


    /**
     * Set f043fmobile
     *
     * @param string $f043fmobile
     *
     * @return T043faddressDetails
     */
    public function setF043fmobile($f043fmobile)
    {
        $this->f043fmobile = $f043fmobile;

        return $this;
    }

    /**
     * Get f043fmobile
     *
     * @return string
     */
    public function getF043fmobile()
    {
        return $this->f043fmobile;
    }

    /**
     * Set f043femail
     *
     * @param string $f043femail
     *
     * @return T043faddressDetails
     */
    public function setF043femail($f043femail)
    {
        $this->f043femail = $f043femail;

        return $this;
    }

    /**
     * Get f043femail
     *
     * @return string
     */
    public function getF043femail()
    {
        return $this->f043femail;
    }


    /**
     * Set f043fcountry
     *
     * @param string $f043fcountry
     *
     * @return T043faddressDetails
     */
    public function setF043fcountry($f043fcountry)
    {
        $this->f043fcountry = $f043fcountry;

        return $this;
    }

    /**
     * Get f043fcountry
     *
     * @return string
     */
    public function getF043fcountry()
    {
        return $this->f043fcountry;
    }

    /**
     * Set f043fcity
     *
     * @param string $f043fcity
     *
     * @return T043faddressDetails
     */
    public function setF043fcity($f043fcity)
    {
        $this->f043fcity = $f043fcity;

        return $this;
    }

    /**
     * Get f043fcity
     *
     * @return string
     */
    public function getF043fcity()
    {
        return $this->f043fcity;
    }

    /**
     * Set f043fstate
     *
     * @param string $f043fstate
     *
     * @return T043faddressDetails
     */
    public function setF043fstate($f043fstate)
    {
        $this->f043fstate = $f043fstate;

        return $this;
    }

    /**
     * Get f043fstate
     *
     * @return string
     */
    public function getF043fstate()
    {
        return $this->f043fstate;
    }

    /**
     * Set f043faddress1
     *
     * @param string $f043faddress1
     *
     * @return T043faddressDetails
     */
    public function setF043faddress1($f043faddress1)
    {
        $this->f043faddress1 = $f043faddress1;

        return $this;
    }

    /**
     * Get f043faddress1
     *
     * @return string
     */
    public function getF043faddress1()
    {
        return $this->f043faddress1;
    }

    /**
     * Set f043faddress2
     *
     * @param string $f043faddress2
     *
     * @return T043faddressDetails
     */
    public function setF043faddress2($f043faddress2)
    {
        $this->f043faddress2 = $f043faddress2;

        return $this;
    }

    /**
     * Get f043faddress2
     *
     * @return string
     */
    public function getF043faddress2()
    {
        $this->f043faddress2;
    }


    /**
     * Set f043fpostCode
     *
     * @param string $f043fpostCode
     *
     * @return T043faddressDetails
     */
    public function setF043fpostCode($f043fpostCode)
    {
        $this->f043fpostCode = $f043fpostCode;

        return $this;
    }

    /**
     * Get f043fpostCode
     *
     * @return string
     */
    public function getF043fpostCode()
    {
        $this->f043fpostCode;
    }

    /**
     * Set f043fstatus
     *
     * @param boolean $f043status
     *
     * @return T043faddressDetails
     */
    public function setF043fstatus($f043fstatus)
    {
        $this->f043fstatus = $f043fstatus ;

        return $this;
    }

    /**
     * Get f043fstatus
     *
     * @return boolean
     */
    public function getF043fstatus()
    {
        return $this->f043fstatus;
    }

    /**
     * Set f043fcreatedDtTm
     *
     * @param \DateTime $f043fcreatedDtTm
     *
     * @return T043faddressDetails
     */
    public function setF043fcreatedDtTm($f043fcreatedDtTm)
    {
        $this->f043fcreatedDtTm = $f043fcreatedDtTm;

        return $this;
    }

    /**
     * Get f043fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fupdatedDtTm()
    {
        return $this->f043fupdatedDtTm;
    }
    /**
     * Set f043fupdatedDtTm
     *
     * @param \DateTime $f043fupdatedDtTm
     *
     * @return T043faddressDetails
     */
    public function setF043fupdatedDtTm($f043fupdatedDtTm)
    {
        $this->f043fupdatedDtTm = $f043fupdatedDtTm;

        return $this;
    }

    /**
     * Get f043fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fcreatedDtTm()
    {
        return $this->f043fcreatedDtTm;
    }

    /**
     * Set f043fidLedger
     *
     * @param string $f043fidLedger
     *
     * @return T043faddressDetails
     */
    public function setF043fidLedger( $f043fidLedger )
    {
        $this->f043fidLedger = $f043fidLedger;

        return $this;
    }

    /**
     * Get f043fidLedger
     *
     * @return T043faddressDetails
     */
    public function getF043fidLedger()
    {
        return $this->f043fidLedger;
    }

    /**
     * Set f043fupdatedBy
     *
     * @param integer $f043fupdatedBy
     *
     * @return T043faddressDetails
     */
    public function setF043fupdatedBy($f043fupdatedBy)
    {
        $this->f043fupdatedBy = $f043fupdatedBy;

        return $this;
    }

    /**
     * Get f043fupdatedBy
     *
     * @return integer
     */
    public function getF043fupdatedBy()
    {
        return $this->f043fupdatedBy;
    }

    /**
     * Set f043fcreatedBy
     *
     * @param integer $f043fcreatedBy
     *
     * @return T043faddressDetails
     */
    public function setF043fcreatedBy( $f043fcreatedBy )
    {
        $this->f043fcreatedBy = $f043fcreatedBy;

        return $this;
    }

    /**
     * Get f043fcreatedBy
     *
     * @return integer
     */
    public function getF043fcreatedBy()
    {
        return $this->f043fcreatedBy;
    }
}
