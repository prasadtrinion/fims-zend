<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T109fprocurementLimit
 *
 * @ORM\Table(name="t109fprocurement_limit", uniqueConstraints={@ORM\UniqueConstraint(name="f109ftype_UNIQUE", columns={"f109ftype"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ProcurementLimitRepository")
 */
class T109fprocurementLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f109fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f109fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f109ftype", type="string", length=50, nullable=true)
     */
    private $f109ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f109fmin_limit", type="string", length=10, nullable=true)
     */
    private $f109fminLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="f109fmax_limit", type="string", length=10, nullable=true)
     */
    private $f109fmaxLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f109fstatus", type="integer", nullable=true)
     */
    private $f109fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f109fcreated_by", type="integer", nullable=true)
     */
    private $f109fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f109fupdated_by", type="integer", nullable=true)
     */
    private $f109fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f109fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f109fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f109fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f109fupdatedDtTm;



    /**
     * Get f109fid
     *
     * @return integer
     */
    public function getF109fid()
    {
        return $this->f109fid;
    }

    /**
     * Set f109ftype
     *
     * @param string $f109ftype
     *
     * @return T109fprocurementLimit
     */
    public function setF109ftype($f109ftype)
    {
        $this->f109ftype = $f109ftype;

        return $this;
    }

    /**
     * Get f109ftype
     *
     * @return string
     */
    public function getF109ftype()
    {
        return $this->f109ftype;
    }

    /**
     * Set f109fminLimit
     *
     * @param string $f109fminLimit
     *
     * @return T109fprocurementLimit
     */
    public function setF109fminLimit($f109fminLimit)
    {
        $this->f109fminLimit = $f109fminLimit;

        return $this;
    }

    /**
     * Get f109fminLimit
     *
     * @return string
     */
    public function getF109fminLimit()
    {
        return $this->f109fminLimit;
    }

    /**
     * Set f109fmaxLimit
     *
     * @param string $f109fmaxLimit
     *
     * @return T109fprocurementLimit
     */
    public function setF109fmaxLimit($f109fmaxLimit)
    {
        $this->f109fmaxLimit = $f109fmaxLimit;

        return $this;
    }

    /**
     * Get f109fmaxLimit
     *
     * @return string
     */
    public function getF109fmaxLimit()
    {
        return $this->f109fmaxLimit;
    }

    /**
     * Set f109fstatus
     *
     * @param integer $f109fstatus
     *
     * @return T109fprocurementLimit
     */
    public function setF109fstatus($f109fstatus)
    {
        $this->f109fstatus = $f109fstatus;

        return $this;
    }

    /**
     * Get f109fstatus
     *
     * @return integer
     */
    public function getF109fstatus()
    {
        return $this->f109fstatus;
    }

    /**
     * Set f109fcreatedBy
     *
     * @param integer $f109fcreatedBy
     *
     * @return T109fprocurementLimit
     */
    public function setF109fcreatedBy($f109fcreatedBy)
    {
        $this->f109fcreatedBy = $f109fcreatedBy;

        return $this;
    }

    /**
     * Get f109fcreatedBy
     *
     * @return integer
     */
    public function getF109fcreatedBy()
    {
        return $this->f109fcreatedBy;
    }

    /**
     * Set f109fupdatedBy
     *
     * @param integer $f109fupdatedBy
     *
     * @return T109fprocurementLimit
     */
    public function setF109fupdatedBy($f109fupdatedBy)
    {
        $this->f109fupdatedBy = $f109fupdatedBy;

        return $this;
    }

    /**
     * Get f109fupdatedBy
     *
     * @return integer
     */
    public function getF109fupdatedBy()
    {
        return $this->f109fupdatedBy;
    }

    /**
     * Set f109fcreatedDtTm
     *
     * @param \DateTime $f109fcreatedDtTm
     *
     * @return T109fprocurementLimit
     */
    public function setF109fcreatedDtTm($f109fcreatedDtTm)
    {
        $this->f109fcreatedDtTm = $f109fcreatedDtTm;

        return $this;
    }

    /**
     * Get f109fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF109fcreatedDtTm()
    {
        return $this->f109fcreatedDtTm;
    }

    /**
     * Set f109fupdatedDtTm
     *
     * @param \DateTime $f109fupdatedDtTm
     *
     * @return T109fprocurementLimit
     */
    public function setF109fupdatedDtTm($f109fupdatedDtTm)
    {
        $this->f109fupdatedDtTm = $f109fupdatedDtTm;

        return $this;
    }

    /**
     * Get f109fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF109fupdatedDtTm()
    {
        return $this->f109fupdatedDtTm;
    }
}
