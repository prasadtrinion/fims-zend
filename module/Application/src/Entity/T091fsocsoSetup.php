<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T091fsocsoSetup
 *
 * @ORM\Table(name="t091fsocso_setup", uniqueConstraints={@ORM\UniqueConstraint(name="f091fcode_UNIQUE", columns={"f091fcode"})}))
 * @ORM\Entity(repositoryClass="Application\Repository\SocsoSetupRepository")
 */
class T091fsocsoSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fcode", type="string", length=50, nullable=false)
     */
    private $f091fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fcategory", type="integer", nullable=false)
     */
    private $f091fcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstart_amount", type="integer",  nullable=false)
     */
    private $f091fstartAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fend_amount", type="integer",  nullable=false)
     */
    private $f091fendAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091femployer_contribution", type="integer",  nullable=false)
     */
    private $f091femployerContribution;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091femployer_deduction", type="integer",  nullable=false)
     */
    private $f091femployerDeduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=false)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=false)
     */
    private $f091fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=false)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fupdatedDtTm;



    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    /**
     * Set f091fcode
     *
     * @param string $f091fcode
     *
     * @return T091fsocsoSetup
     */
    public function setF091fcode($f091fcode)
    {
        $this->f091fcode = $f091fcode;

        return $this;
    }

    /**
     * Get f091fcode
     *
     * @return string
     */
    public function getF091fcode()
    {
        return $this->f091fcode;
    }

    /**
     * Set f091fcategory
     *
     * @param string $f091fcategory
     *
     * @return T091fsocsoSetup
     */
    public function setF091fcategory($f091fcategory)
    {
        $this->f091fcategory = $f091fcategory;

        return $this;
    }

    /**
     * Get f091fcategory
     *
     * @return string
     */
    public function getF091fcategory()
    {
        return $this->f091fcategory;
    }

    /**
     * Set f091fstartAmount
     *
     * @param integer $f091fstartAmount
     *
     * @return T091fsocsoSetup
     */
    public function setF091fstartAmount($f091fstartAmount)
    {
        $this->f091fstartAmount = $f091fstartAmount;

        return $this;
    }

    /**
     * Get f091fstartAmount
     *
     * @return integer
     */
    public function getF091fstartAmount()
    {
        return $this->f091fstartAmount;
    }

    /**
     * Set f091fendAmount
     *
     * @param integer $f091fendAmount
     *
     * @return T091fsocsoSetup
     */
    public function setF091fendAmount($f091fendAmount)
    {
        $this->f091fendAmount = $f091fendAmount;

        return $this;
    }

    /**
     * Get f091fendAmount
     *
     * @return integer
     */
    public function getF091fendAmount()
    {
        return $this->f091fendAmount;
    }

    /**
     * Set f091femployerContribution
     *
     * @param integer $f091femployerContribution
     *
     * @return T091fsocsoSetup
     */
    public function setF091femployerContribution($f091femployerContribution)
    {
        $this->f091femployerContribution = $f091femployerContribution;

        return $this;
    }

    /**
     * Get f091femployerContribution
     *
     * @return integer
     */
    public function getF091femployerContribution()
    {
        return $this->f091femployerContribution;
    }

    /**
     * Set f091femployerDeduction
     *
     * @param integer $f091femployerDeduction
     *
     * @return T091fsocsoSetup
     */
    public function setF091femployerDeduction($f091femployerDeduction)
    {
        $this->f091femployerDeduction = $f091femployerDeduction;

        return $this;
    }

    /**
     * Get f091femployerDeduction
     *
     * @return integer
     */
    public function getF091femployerDeduction()
    {
        return $this->f091femployerDeduction;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fsocsoSetup
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fsocsoSetup
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fsocsoSetup
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091fsocsoSetup
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T091fsocsoSetup
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }
}
