<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T022floanPeriod
 *
 * @ORM\Table(name="t022floan_period")
 * @ORM\Entity(repositoryClass="Application\Repository\LoanPeriodRepository")
 */
class T022floanPeriod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f022fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fid_loan_name", type="integer", nullable=false)
     */
    private $f022fidLoanName;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fgrade", type="string",length=50, nullable=false)
     */
    private $f022fgrade;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fminimum_month", type="integer", nullable=false)
     */
    private $f022fminimumMonth ;

     /**
     * @var integer
     *
     * @ORM\Column(name="f022fmaximum_month", type="integer", nullable=false)
     */
    private $f022fmaximumMonth ;

     /**
     * @var integer
     *
     * @ORM\Column(name="f022fstatus", type="integer", nullable=false)
     */
    private $f022fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcreated_by", type="integer", nullable=false)
     */
    private $f022fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fupdated_by", type="integer", nullable=false)
     */
    private $f022fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fupdatedDtTm;



    /**
     * Get f022fid
     *
     * @return integer
     */
    public function getF022fid()
    {
        return $this->f022fid;
    }

    /**
     * Set f022fidLoanName
     *
     * @param string $f022fidLoanName
     *
     * @return T022floanPeriod
     */
    public function setF022fidLoanName($f022fidLoanName)
    {
        $this->f022fidLoanName = $f022fidLoanName;

        return $this;
    }

    /**
     * Get f022fidLoanName
     *
     * @return string
     */
    public function getF022fidLoanName()
    {
        return $this->f022fidLoanName;
    }

    /**
     * Set f022fminimumMonth
     *
     * @param boolean $f022fminimumMonth
     *
     * @return T022floanPeriod
     */
    public function setF022fminimumMonth($f022fminimumMonth)
    {
        $this->f022fminimumMonth = $f022fminimumMonth;

        return $this;
    }

    /**
     * Get f022fminimumMonth
     *
     * @return boolean
     */
    public function getF022fminimumMonth()
    {
        return $this->f022fminimumMonth;
    }

    /**
     * Set f022fmaximumMonth
     *
     * @param boolean $f022fmaximumMonth
     *
     * @return T022floanPeriod
     */
    public function setF022fmaximumMonth($f022fmaximumMonth)
    {
        $this->f022fmaximumMonth = $f022fmaximumMonth;

        return $this;
    }

    /**
     * Get f022fmaximumMonth
     *
     * @return boolean
     */
    public function getF022fmaximumMonth()
    {
        return $this->f022fmaximumMonth;
    }

     /**
     * Set f022fstatus
     *
     * @param boolean $f022fstatus
     *
     * @return T022floanPeriod
     */
    public function setF022fstatus($f022fstatus)
    {
        $this->f022fstatus = $f022fstatus;

        return $this;
    }

    /**
     * Get f022fstatus
     *
     * @return boolean
     */
    public function getF022fstatus()
    {
        return $this->f022fstatus;
    }

    /**
     * Set f022fcreatedBy
     *
     * @param integer $f022fcreatedBy
     *
     * @return T022floanPeriod
     */
    public function setF022fcreatedBy($f022fcreatedBy)
    {
        $this->f022fcreatedBy = $f022fcreatedBy;

        return $this;
    }

    /**
     * Get f022fcreatedBy
     *
     * @return integer
     */
    public function getF022fcreatedBy()
    {
        return $this->f022fcreatedBy;
    }

    /**
     * Set f022fupdatedBy
     *
     * @param integer $f022fupdatedBy
     *
     * @return T022floanPeriod
     */
    public function setF022fupdatedBy($f022fupdatedBy)
    {
        $this->f022fupdatedBy = $f022fupdatedBy;

        return $this;
    }

    /**
     * Get f022fupdatedBy
     *
     * @return integer
     */
    public function getF022fupdatedBy()
    {
        return $this->f022fupdatedBy;
    }

    /**
     * Set f022fcreatedDtTm
     *
     * @param \DateTime $f022fcreatedDtTm
     *
     * @return T022floanPeriod
     */
    public function setF022fcreatedDtTm($f022fcreatedDtTm)
    {
        $this->f022fcreatedDtTm = $f022fcreatedDtTm;

        return $this;
    }

    /**
     * Get f022fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fcreatedDtTm()
    {
        return $this->f022fcreatedDtTm;
    }

    /**
     * Set f022fupdatedDtTm
     *
     * @param \DateTime $f022fupdatedDtTm
     *
     * @return T022floanPeriod
     */
    public function setF022fupdatedDtTm($f022fupdatedDtTm)
    {
        $this->f022fupdatedDtTm = $f022fupdatedDtTm;

        return $this;
    }

    /**
     * Get f022fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fupdatedDtTm()
    {
        return $this->f022fupdatedDtTm;
    }

     /**
     * Set f022fgrade
     *
     * @param \DateTime $f022fgrade
     *
     * @return T022floanPeriod
     */
    public function setF022fgrade($f022fgrade)
    {
        $this->f022fgrade = $f022fgrade;

        return $this;
    }

    /**
     * Get f022fgrade
     *
     * @return \DateTime
     */
    public function getF022fgrade()
    {
        return $this->f022fgrade;
    }
}
