<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T023fuserRoles
 *
 * @ORM\Table(name="t023fuser_roles")
 * @ORM\Entity(repositoryClass="Application\Repository\RoleRepository")
 */
class T023fuserRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f023fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f023fid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f023fstatus", type="integer", nullable=true)
     */
    private $f023fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f023fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f023fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f023fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f023fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f023fid_role", type="integer", nullable=true)
     */
    private $f023fidRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="f023fid_user", type="integer", nullable=true)
     */
    private $f023fidUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="f023fupdated_by", type="integer", nullable=true)
     */
    private $f023fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f023fcreated_by", type="integer", nullable=true)
     */
    private $f023fcreatedBy;

    /**
     * Get f023fid
     *
     * @return integer
     */
    public function getF023fid()
    {
        return $this->f023fid;
    }

    /**
     * Set f023fstatus
     *
     * @param boolean $f023fstatus
     *
     * @return T023fuserRoles
     */
    public function setF023fstatus($f023fstatus)
    {
        $this->f023fstatus = $f023fstatus;

        return $this;
    }

    /**
     * Get f023fstatus
     *
     * @return boolean
     */
    public function getF023fstatus()
    {
        return $this->f023fstatus;
    }

    /**
     * Set f023fcreatedDtTm
     *
     * @param \DateTime $f023fcreatedDtTm
     *
     * @return T023fuserRoles
     */
    public function setF023fcreatedDtTm($f023fcreatedDtTm)
    {
        $this->f023fcreatedDtTm = $f023fcreatedDtTm;

        return $this;
    }

    /**
     * Get f023fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF023fcreatedDtTm()
    {
        return $this->f023fcreatedDtTm;
    }

    /**
     * Set f023fupdatedDtTm
     *
     * @param \DateTime $f023fupdatedDtTm
     *
     * @return T023fuserRoles
     */
    public function setF023fupdatedDtTm($f023fupdatedDtTm)
    {
        $this->f023fupdatedDtTm = $f023fupdatedDtTm;

        return $this;
    }

    /**
     * Get f023fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF023fupdatedDtTm()
    {
        return $this->f023fupdatedDtTm;
    }

    /**
     * Set f023fidRole
     *
     * @param integer $f023fidRole
     *
     * @return T023fuserRoles
     */
    public function setF023fidRole($f023fidRole )
    {
        $this->f023fidRole = $f023fidRole;

        return $this;
    }

    /**
     * Get f023fidRole
     *
     * @return \Application\Entity\T021frole
     */
    public function getF023fidRole()
    {
        return $this->f023fidRole;
    }

    /**
     * Set f023fidUser
     *
     * @param integer $f023fidUser
     *
     * @return T023fuserRoles
     */
    public function setF023fidUser($f023fidUser )
    {
        $this->f023fidUser = $f023fidUser;

        return $this;
    }

    /**
     * Get f023fidUser
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF023fidUser()
    {
        return $this->f023fidUser;
    }

    /**
     * Set f023fupdatedBy
     *
     * @param integer $f023fupdatedBy
     *
     * @return T023fuserRoles
     */
    public function setF023fupdatedBy($f023fupdatedBy)
    {
        $this->f023fupdatedBy = $f023fupdatedBy;

        return $this;
    }

    /**
     * Get f023fupdatedBy
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF023fupdatedBy()
    {
        return $this->f023fupdatedBy;
    }

    /**
     * Set f023fcreatedBy
     *
     * @param integer $f023fcreatedBy
     *
     * @return T023fuserRoles
     */
    public function setF023fcreatedBy($f023fcreatedBy )
    {
        $this->f023fcreatedBy = $f023fcreatedBy;

        return $this;
    }

    /**
     * Get f023fcreatedBy
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF023fcreatedBy()
    {
        return $this->f023fcreatedBy;
    }
}