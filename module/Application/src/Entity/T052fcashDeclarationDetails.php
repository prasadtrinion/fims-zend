<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052fcashDeclarationDetails
 *
 * @ORM\Table(name="t052fcash_declaration_details")
 * @ORM\Entity(repositoryClass="Application\Repository\CashDeclarationRepository")
 */
class T052fcashDeclarationDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_declare_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fidDeclareDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_cash_Declaration", type="integer", nullable=false)
     */
    private $f052fidCashDeclaration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_cash_type", type="integer", nullable=true)
     */
    private $f052fidCashType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fitem", type="string",length=200, nullable=true)
     */
    private $f052fitem;

    /**
     * @var string
     *
     * @ORM\Column(name="f052ffund", type="string", length=50, nullable=true)
     */
    private $f052ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f052factivity", type="string", length=50, nullable=true)
     */
    private $f052factivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fdepartment", type="string", length=50, nullable=true)
     */
    private $f052fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f052faccount", type="string", length=50, nullable=true)
     */
    private $f052faccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fso_code", type="integer", nullable=true)
     */
    private $f052fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052famount", type="integer", nullable=true)
     */
    private $f052famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fquantity", type="integer", nullable=true)
     */
    private $f052fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052ftotal_amount", type="integer",  nullable=true)
     */
    private $f052ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=true)
     */
    private $f052fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=true)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=true)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f052frcp_no", type="string", length=50, nullable=true)
     */
    private $f052frcpNo;




    /**
     * Get f052fidDeclareDetails
     *
     * @return integer
     */
    public function getF052fidDeclareDetails()
    {
        return $this->f052fidDeclareDetails;
    }

    /**
     * Set f052fidCashDeclaration
     *
     * @param integer $f052fidCashDeclaration
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fidCashDeclaration($f052fidCashDeclaration)
    {
        $this->f052fidCashDeclaration = $f052fidCashDeclaration;

        return $this;
    }

    /**
     * Get f052fidCashDeclaration
     *
     * @return integer
     */
    public function getF052fidCashDeclaration()
    {
        return $this->f052fidCashDeclaration;
    }

    /**
     * Set f052fidCashType
     *
     * @param integer $f052fidCashType
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fidCashType($f052fidCashType)
    {
        $this->f052fidCashType = $f052fidCashType;

        return $this;
    }

    /**
     * Get f052fidCashType
     *
     * @return integer
     */
    public function getF052fidCashType()
    {
        return $this->f052fidCashType;
    }

    /**
     * Set f052fitem
     *
     * @param integer $f052fitem
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fitem($f052fitem)
    {
        $this->f052fitem = $f052fitem;

        return $this;
    }

    /**
     * Get f052fitem
     *
     * @return integer
     */
    public function getF052fitem()
    {
        return $this->f052fitem;
    }

    /**
     * Set f052ffund
     *
     * @param string $f052ffund
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052ffund($f052ffund)
    {
        $this->f052ffund = $f052ffund;

        return $this;
    }

    /**
     * Get f052ffund
     *
     * @return string
     */
    public function getF052ffund()
    {
        return $this->f052ffund;
    }

    /**
     * Set f052factivity
     *
     * @param string $f052factivity
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052factivity($f052factivity)
    {
        $this->f052factivity = $f052factivity;

        return $this;
    }

    /**
     * Get f052factivity
     *
     * @return string
     */
    public function getF052factivity()
    {
        return $this->f052factivity;
    }

    /**
     * Set f052fdepartment
     *
     * @param string $f052fdepartment
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fdepartment($f052fdepartment)
    {
        $this->f052fdepartment = $f052fdepartment;

        return $this;
    }

    /**
     * Get f052fdepartment
     *
     * @return string
     */
    public function getF052fdepartment()
    {
        return $this->f052fdepartment;
    }

    /**
     * Set f052faccount
     *
     * @param string $f052faccount
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052faccount($f052faccount)
    {
        $this->f052faccount = $f052faccount;

        return $this;
    }

    /**
     * Get f052faccount
     *
     * @return string
     */
    public function getF052faccount()
    {
        return $this->f052faccount;
    }

    /**
     * Set f052fsoCode
     *
     * @param integer $f052fsoCode
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fsoCode($f052fsoCode)
    {
        $this->f052fsoCode = $f052fsoCode;

        return $this;
    }

    /**
     * Get f052fsoCode
     *
     * @return integer
     */
    public function getF052fsoCode()
    {
        return $this->f052fsoCode;
    }

    /**
     * Set f052famount
     *
     * @param integer $f052famount
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052famount($f052famount)
    {
        $this->f052famount = $f052famount;

        return $this;
    }

    /**
     * Get f052famount
     *
     * @return integer
     */
    public function getF052famount()
    {
        return $this->f052famount;
    }

    /**
     * Set f052fquantity
     *
     * @param integer $f052fquantity
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fquantity($f052fquantity)
    {
        $this->f052fquantity = $f052fquantity;

        return $this;
    }

    /**
     * Get f052fquantity
     *
     * @return integer
     */
    public function getF052fquantity()
    {
        return $this->f052fquantity;
    }

    /**
     * Set f052ftotalAmount
     *
     * @param integer $f052ftotalAmount
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052ftotalAmount($f052ftotalAmount)
    {
        $this->f052ftotalAmount = $f052ftotalAmount;

        return $this;
    }

    /**
     * Get f052ftotalAmount
     *
     * @return integer
     */
    public function getF052ftotalAmount()
    {
        return $this->f052ftotalAmount;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052fcashDeclarationDetails
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }

     /**
     * Set f052frcpNo
     *
     * @param integer $f052frcpNo
     *
     * @return T052fcashDeclaration
     */
    public function setF052frcpNo($f052frcpNo)
    {
        $this->f052frcpNo = $f052frcpNo;

        return $this;
    }

    /**
     * Get f052frcpNo
     *
     * @return integer
     */
    public function getF052frcpNo()
    {
        return $this->f052frcpNo;
    }
}
