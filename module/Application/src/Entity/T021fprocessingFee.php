<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T021fprocessingFee
 *
 * @ORM\Table(name="t021fprocessing_fee")
 * @ORM\Entity(repositoryClass="Application\Repository\ProcessingFeeRepository")
 */
class T021fprocessingFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f021fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f021fid_loan_name", type="integer", nullable=false)
     */
    private $f021fidLoanName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021feffective_date", type="datetime", nullable=false)
     */
    private $f021feffectiveDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021famount", type="integer", nullable=false)
     */
    private $f021famount ;

     /**
     * @var integer
     *
     * @ORM\Column(name="f021fstatus", type="integer", nullable=false)
     */
    private $f021fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcreated_by", type="integer", nullable=false)
     */
    private $f021fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fupdated_by", type="integer", nullable=false)
     */
    private $f021fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fupdatedDtTm;
    
    /**
     * @var string
     *
     * @ORM\Column(name="f021ffund", type="string", length=25, nullable=false)
     */
    private $f021ffund='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f021fdepartment", type="string", length=25, nullable=false)
     */
    private $f021fdepartment='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f021factivity", type="string", length=25, nullable=false)
     */
    private $f021factivity='NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f021faccount", type="string", length=25, nullable=false)
     */
    private $f021faccount='NULL';

 



    /**
     * Get f021fid
     *
     * @return integer
     */
    public function getF021fid()
    {
        return $this->f021fid;
    }

    /**
     * Set f021fidLoanName
     *
     * @param string $f021fidLoanName
     *
     * @return T021fprocessingFee
     */
    public function setF021fidLoanName($f021fidLoanName)
    {
        $this->f021fidLoanName = $f021fidLoanName;

        return $this;
    }

    /**
     * Get f021fidLoanName
     *
     * @return string
     */
    public function getF021fidLoanName()
    {
        return $this->f021fidLoanName;
    }

    /**
     * Set f021famount
     *
     * @param boolean $f021famount
     *
     * @return T021fprocessingFee
     */
    public function setF021famount($f021famount)
    {
        $this->f021famount = $f021famount;

        return $this;
    }

    /**
     * Get f021famount
     *
     * @return boolean
     */
    public function getF021famount()
    {
        return $this->f021famount;
    }

     /**
     * Set f021fstatus
     *
     * @param boolean $f021fstatus
     *
     * @return T021fprocessingFee
     */
    public function setF021fstatus($f021fstatus)
    {
        $this->f021fstatus = $f021fstatus;

        return $this;
    }

    /**
     * Get f021fstatus
     *
     * @return boolean
     */
    public function getF021fstatus()
    {
        return $this->f021fstatus;
    }

    /**
     * Set f021fcreatedBy
     *
     * @param integer $f021fcreatedBy
     *
     * @return T021fprocessingFee
     */
    public function setF021fcreatedBy($f021fcreatedBy)
    {
        $this->f021fcreatedBy = $f021fcreatedBy;

        return $this;
    }

    /**
     * Get f021fcreatedBy
     *
     * @return integer
     */
    public function getF021fcreatedBy()
    {
        return $this->f021fcreatedBy;
    }

    /**
     * Set f021fupdatedBy
     *
     * @param integer $f021fupdatedBy
     *
     * @return T021fprocessingFee
     */
    public function setF021fupdatedBy($f021fupdatedBy)
    {
        $this->f021fupdatedBy = $f021fupdatedBy;

        return $this;
    }

    /**
     * Get f021fupdatedBy
     *
     * @return integer
     */
    public function getF021fupdatedBy()
    {
        return $this->f021fupdatedBy;
    }

    /**
     * Set f021fcreatedDtTm
     *
     * @param \DateTime $f021fcreatedDtTm
     *
     * @return T021fprocessingFee
     */
    public function setF021fcreatedDtTm($f021fcreatedDtTm)
    {
        $this->f021fcreatedDtTm = $f021fcreatedDtTm;

        return $this;
    }

    /**
     * Get f021fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fcreatedDtTm()
    {
        return $this->f021fcreatedDtTm;
    }

    /**
     * Set f021fupdatedDtTm
     *
     * @param \DateTime $f021fupdatedDtTm
     *
     * @return T021fprocessingFee
     */
    public function setF021fupdatedDtTm($f021fupdatedDtTm)
    {
        $this->f021fupdatedDtTm = $f021fupdatedDtTm;

        return $this;
    }

    /**
     * Get f021fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fupdatedDtTm()
    {
        return $this->f021fupdatedDtTm;
    }

     /**
     * Set f021feffectiveDate
     *
     * @param \DateTime $f021feffectiveDate
     *
     * @return T021fprocessingFee
     */
    public function setF021feffectiveDate($f021feffectiveDate)
    {
        $this->f021feffectiveDate = $f021feffectiveDate;

        return $this;
    }

    /**
     * Get f021feffectiveDate
     *
     * @return \DateTime
     */
    public function getF021feffectiveDate()
    {
        return $this->f021feffectiveDate;
    }

     /**
     * Set f021ffund
     *
     * @param string $f021ffund
     *
     * @return T021fprocessingFee
     */
    public function setF021ffund($f021ffund)
    {
        $this->f021ffund = $f021ffund;

        return $this;
    }

    /**
     * Get f021ffund
     *
     * @return string
     */
    public function getF021ffund()
    {
        return $this->f021ffund;
    }

    /**
     * Set f021factivity
     *
     * @param string $f021factivity
     *
     * @return T021fprocessingFee
     */
    public function setF021factivity($f021factivity)
    {
        $this->f021factivity = $f021factivity;

        return $this;
    }

    /**
     * Get f021factivity
     *
     * @return string
     */
    public function getF021factivity()
    {
        return $this->f021factivity;
    }

    /**
     * Set f021fdepartment
     *
     * @param string $f021fdepartment
     *
     * @return T021fprocessingFee
     */
    public function setF021fdepartment($f021fdepartment)
    {
        $this->f021fdepartment = $f021fdepartment;

        return $this;
    }

    /**
     * Get f021fdepartment
     *
     * @return string
     */
    public function getF021fdepartment()
    {
        return $this->f021fdepartment;
    }

    /**
     * Set f021faccount
     *
     * @param string $f021faccount
     *
     * @return T021fprocessingFee
     */
    public function setF021faccount($f021faccount)
    {
        $this->f021faccount = $f021faccount;

        return $this;
    }

    /**
     * Get f021faccount
     *
     * @return string
     */
    public function getF021faccount()
    {
        return $this->f021faccount;
    }
}
