<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T088fassetDepreciation
 *
 * @ORM\Table(name="t088fasset_depreciation")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetDepreciationRepository")
 */
class T088fassetDepreciation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f088fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fasset_number", type="string",length=225, nullable=false)
     */
    private $f088fassetNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fmonth", type="integer", nullable=false)
     */
    private $f088fmonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fday", type="integer", nullable=false)
     */
    private $f088fday;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fstatus", type="integer", nullable=true)
     */
    private $f088fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fupdated_by", type="integer", nullable=true)
     */
    private $f088fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fcreated_by", type="integer", nullable=true)
     */
    private $f088fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f088fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f088fcreatedDtTm;



    /**
     * Get f088fid
     *
     * @return integer
     */
    public function getF088fid()
    {
        return $this->f088fid;
    }

    /**
     * Set f088fassetNumber
     *
     * @param integer $f088fassetNumber
     *
     * @return T088fassetDepreciation
     */
    public function setF088fassetNumber($f088fassetNumber)
    {
        $this->f088fassetNumber = $f088fassetNumber;

        return $this;
    }

    /**
     * Get f088fassetNumber
     *
     * @return integer
     */
    public function getF088fassetNumber()
    {
        return $this->f088fassetNumber;
    }

    /**
     * Set f088fmonth
     *
     * @param integer $f088fmonth
     *
     * @return T088fassetDepreciation
     */
    public function setF088fmonth($f088fmonth)
    {
        $this->f088fmonth = $f088fmonth;

        return $this;
    }

    /**
     * Get f088fmonth
     *
     * @return integer
     */
    public function getF088fmonth()
    {
        return $this->f088fmonth;
    }

    /**
     * Set f088fday
     *
     * @param integer $f088fday
     *
     * @return T088fassetDepreciation
     */
    public function setF088fday($f088fday)
    {
        $this->f088fday = $f088fday;

        return $this;
    }

    /**
     * Get f088fday
     *
     * @return integer
     */
    public function getF088fday()
    {
        return $this->f088fday;
    }

    /**
     * Set f088fstatus
     *
     * @param integer $f088fstatus
     *
     * @return T088fassetDepreciation
     */
    public function setF088fstatus($f088fstatus)
    {
        $this->f088fstatus = $f088fstatus;

        return $this;
    }

    /**
     * Get f088fstatus
     *
     * @return integer
     */
    public function getF088fstatus()
    {
        return $this->f088fstatus;
    }

    /**
     * Set f088fupdatedBy
     *
     * @param integer $f088fupdatedBy
     *
     * @return T088fassetDepreciation
     */
    public function setF088fupdatedBy($f088fupdatedBy)
    {
        $this->f088fupdatedBy = $f088fupdatedBy;

        return $this;
    }

    /**
     * Get f088fupdatedBy
     *
     * @return integer
     */
    public function getF088fupdatedBy()
    {
        return $this->f088fupdatedBy;
    }

    /**
     * Set f088fcreatedBy
     *
     * @param integer $f088fcreatedBy
     *
     * @return T088fassetDepreciation
     */
    public function setF088fcreatedBy($f088fcreatedBy)
    {
        $this->f088fcreatedBy = $f088fcreatedBy;

        return $this;
    }

    /**
     * Get f088fcreatedBy
     *
     * @return integer
     */
    public function getF088fcreatedBy()
    {
        return $this->f088fcreatedBy;
    }

    /**
     * Set f088fupdatedDtTm
     *
     * @param \DateTime $f088fupdatedDtTm
     *
     * @return T088fassetDepreciation
     */
    public function setF088fupdatedDtTm($f088fupdatedDtTm)
    {
        $this->f088fupdatedDtTm = $f088fupdatedDtTm;

        return $this;
    }

    /**
     * Get f088fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fupdatedDtTm()
    {
        return $this->f088fupdatedDtTm;
    }

    /**
     * Set f088fcreatedDtTm
     *
     * @param \DateTime $f088fcreatedDtTm
     *
     * @return T088fassetDepreciation
     */
    public function setF088fcreatedDtTm($f088fcreatedDtTm)
    {
        $this->f088fcreatedDtTm = $f088fcreatedDtTm;

        return $this;
    }

    /**
     * Get f088fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fcreatedDtTm()
    {
        return $this->f088fcreatedDtTm;
    }
}
