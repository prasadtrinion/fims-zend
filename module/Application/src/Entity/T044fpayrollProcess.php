<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T044fpayrollProcess
 *
 * @ORM\Table(name="t044fpayroll_process")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollProcessRepository")
 */
class T044fpayrollProcess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f044fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fcompany", type="integer", nullable=true)
     */
    private $f044fcompany;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fmonth", type="string", length=50, nullable=true)
     */
    private $f044fmonth;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fstaff_type", type="string", length=50, nullable=true)
     */
    private $f044fstaffType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fstaff_ip", type="integer", nullable=true)
     */
    private $f044fstaffIp;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fstatus", type="integer", nullable=true)
     */
    private $f044fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fcreated_by", type="integer", nullable=true)
     */
    private $f044fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f044fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fupdated_by", type="integer", nullable=true)
     */
    private $f044fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f044fupdatedDtTm;



    /**
     * Get f044fid
     *
     * @return integer
     */
    public function getF044fid()
    {
        return $this->f044fid;
    }

    /**
     * Set f044fcompany
     *
     * @param integer $f044fcompany
     *
     * @return T044fpayrollProcess
     */
    public function setF044fcompany($f044fcompany)
    {
        $this->f044fcompany = $f044fcompany;

        return $this;
    }

    /**
     * Get f044fcompany
     *
     * @return integer
     */
    public function getF044fcompany()
    {
        return $this->f044fcompany;
    }

    /**
     * Set f044fmonth
     *
     * @param string $f044fmonth
     *
     * @return T044fpayrollProcess
     */
    public function setF044fmonth($f044fmonth)
    {
        $this->f044fmonth = $f044fmonth;

        return $this;
    }

    /**
     * Get f044fmonth
     *
     * @return string
     */
    public function getF044fmonth()
    {
        return $this->f044fmonth;
    }

    /**
     * Set f044fstaffType
     *
     * @param string $f044fstaffType
     *
     * @return T044fpayrollProcess
     */
    public function setF044fstaffType($f044fstaffType)
    {
        $this->f044fstaffType = $f044fstaffType;

        return $this;
    }

    /**
     * Get f044fstaffType
     *
     * @return string
     */
    public function getF044fstaffType()
    {
        return $this->f044fstaffType;
    }

    /**
     * Set f044fstaffIp
     *
     * @param integer $f044fstaffIp
     *
     * @return T044fpayrollProcess
     */
    public function setF044fstaffIp($f044fstaffIp)
    {
        $this->f044fstaffIp = $f044fstaffIp;

        return $this;
    }

    /**
     * Get f044fstaffIp
     *
     * @return integer
     */
    public function getF044fstaffIp()
    {
        return $this->f044fstaffIp;
    }

    /**
     * Set f044fstatus
     *
     * @param integer $f044fstatus
     *
     * @return T044fpayrollProcess
     */
    public function setF044fstatus($f044fstatus)
    {
        $this->f044fstatus = $f044fstatus;

        return $this;
    }

    /**
     * Get f044fstatus
     *
     * @return integer
     */
    public function getF044fstatus()
    {
        return $this->f044fstatus;
    }

    /**
     * Set f044fcreatedBy
     *
     * @param integer $f044fcreatedBy
     *
     * @return T044fpayrollProcess
     */
    public function setF044fcreatedBy($f044fcreatedBy)
    {
        $this->f044fcreatedBy = $f044fcreatedBy;

        return $this;
    }

    /**
     * Get f044fcreatedBy
     *
     * @return integer
     */
    public function getF044fcreatedBy()
    {
        return $this->f044fcreatedBy;
    }

    /**
     * Set f044fcreatedDtTm
     *
     * @param \DateTime $f044fcreatedDtTm
     *
     * @return T044fpayrollProcess
     */
    public function setF044fcreatedDtTm($f044fcreatedDtTm)
    {
        $this->f044fcreatedDtTm = $f044fcreatedDtTm;

        return $this;
    }

    /**
     * Get f044fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fcreatedDtTm()
    {
        return $this->f044fcreatedDtTm;
    }

    /**
     * Set f044fupdatedBy
     *
     * @param integer $f044fupdatedBy
     *
     * @return T044fpayrollProcess
     */
    public function setF044fupdatedBy($f044fupdatedBy)
    {
        $this->f044fupdatedBy = $f044fupdatedBy;

        return $this;
    }

    /**
     * Get f044fupdatedBy
     *
     * @return integer
     */
    public function getF044fupdatedBy()
    {
        return $this->f044fupdatedBy;
    }

    /**
     * Set f044fupdatedDtTm
     *
     * @param \DateTime $f044fupdatedDtTm
     *
     * @return T044fpayrollProcess
     */
    public function setF044fupdatedDtTm($f044fupdatedDtTm)
    {
        $this->f044fupdatedDtTm = $f044fupdatedDtTm;

        return $this;
    }

    /**
     * Get f044fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fupdatedDtTm()
    {
        return $this->f044fupdatedDtTm;
    }
}
