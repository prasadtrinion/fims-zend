<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T015ffinancialyear
 *
 * @ORM\Table(name="t015ffinancialyear")
 * @ORM\Entity(repositoryClass="Application\Repository\FinancialyearRepository")
 */
class T015ffinancialyear
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f015fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f015fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fname", type="string", length=45, nullable=false)
     */
    private $f015fname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fstart_date", type="datetime", nullable=false)
     */
    private $f015fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fend_date", type="datetime", nullable=false)
     */
    private $f015fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fstatus", type="integer", nullable=false)
     */
    private $f015fstatus = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f015fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f015fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f015fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f015fupdateDtTm;



    /**
     * Get f015fid
     *
     * @return integer
     */
    public function getF015fid()
    {
        return $this->f015fid;
    }

    /**
     * Set f015fname
     *
     * @param string $f015fname
     *
     * @return T015ffinancialyear
     */
    public function setF015fname($f015fname)
    {
        $this->f015fname = $f015fname;

        return $this;
    }

    /**
     * Get f015fname
     *
     * @return string
     */
    public function getF015fname()
    {
        return $this->f015fname;
    }

    /**
     * Set f015fstartDate
     *
     * @param \DateTime $f015fstartDate
     *
     * @return T015ffinancialyear
     */
    public function setF015fstartDate($f015fstartDate)
    {
        $this->f015fstartDate = $f015fstartDate;

        return $this;
    }

    /**
     * Get f015fstartDate
     *
     * @return \DateTime
     */
    public function getF015fstartDate()
    {
        return $this->f015fstartDate;
    }

    /**
     * Set f015fendDate
     *
     * @param \DateTime $f015fendDate
     *
     * @return T015ffinancialyear
     */
    public function setF015fendDate($f015fendDate)
    {
        $this->f015fendDate = $f015fendDate;

        return $this;
    }

    /**
     * Get f015fendDate
     *
     * @return \DateTime
     */
    public function getF015fendDate()
    {
        return $this->f015fendDate;
    }

    /**
     * Set f015fstatus
     *
     * @param integer $f015fstatus
     *
     * @return T015ffinancialyear
     */
    public function setF015fstatus($f015fstatus)
    {
        $this->f015fstatus = $f015fstatus;

        return $this;
    }

    /**
     * Get f015fstatus
     *
     * @return integer
     */
    public function getF015fstatus()
    {
        return $this->f015fstatus;
    }

    /**
     * Set f015fcreatedBy
     *
     * @param integer $f015fcreatedBy
     *
     * @return T015ffinancialyear
     */
    public function setF015fcreatedBy($f015fcreatedBy)
    {
        $this->f015fcreatedBy = $f015fcreatedBy;

        return $this;
    }

    /**
     * Get f015fcreatedBy
     *
     * @return integer
     */
    public function getF015fcreatedBy()
    {
        return $this->f015fcreatedBy;
    }

    /**
     * Set f015fupdatedBy
     *
     * @param integer $f015fupdatedBy
     *
     * @return T015ffinancialyear
     */
    public function setF015fupdatedBy($f015fupdatedBy)
    {
        $this->f015fupdatedBy = $f015fupdatedBy;

        return $this;
    }

    /**
     * Get f015fupdatedBy
     *
     * @return integer
     */
    public function getF015fupdatedBy()
    {
        return $this->f015fupdatedBy;
    }

    /**
     * Set f015fcreateDtTm
     *
     * @param \DateTime $f015fcreateDtTm
     *
     * @return T015ffinancialyear
     */
    public function setF015fcreateDtTm($f015fcreateDtTm)
    {
        $this->f015fcreateDtTm = $f015fcreateDtTm;

        return $this;
    }

    /**
     * Get f015fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF015fcreateDtTm()
    {
        return $this->f015fcreateDtTm;
    }

    /**
     * Set f015fupdateDtTm
     *
     * @param \DateTime $f015fupdateDtTm
     *
     * @return T015ffinancialyear
     */
    public function setF015fupdateDtTm($f015fupdateDtTm)
    {
        $this->f015fupdateDtTm = $f015fupdateDtTm;

        return $this;
    }

    /**
     * Get f015fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF015fupdateDtTm()
    {
        return $this->f015fupdateDtTm;
    }
}
