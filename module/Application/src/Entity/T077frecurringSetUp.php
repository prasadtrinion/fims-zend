<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T077frecurringSetUp
 *
 * @ORM\Table(name="t077frecurring_set_up")
 * @ORM\Entity(repositoryClass="Application\Repository\RecurringSetUpRepository")
 */
class T077frecurringSetUp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f077fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f077fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fcustomer", type="integer", nullable=false)
     */
    private $f077fcustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fcontract_number", type="string", length=11, nullable=false)
     */
    private $f077fcontractNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f077freference_number", type="string", length=50, nullable=false)
     */
    private $f077freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f077faddress", type="string", length=50, nullable=false)
     */
    private $f077faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdebitor_type", type="string", length=100, nullable=false)
     */
    private $f077fdebitorType;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdate_invoice", type="string", length=10, nullable=false)
     */
    private $f077fdateInvoice;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdiscount_entitlement", type="string", length=50, nullable=false)
     */
    private $f077fdiscountEntitlement;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fperiod", type="integer", nullable=false)
     */
    private $f077fperiod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077feffective_date", type="datetime", nullable=true)
     */
    private $f077feffectiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fend_date", type="datetime", nullable=false)
     */
    private $f077fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fpayment", type="integer", nullable=false)
     */
    private $f077fpayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fpremise", type="integer", nullable=false)
     */
    private $f077fpremise;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fcategory", type="integer", nullable=false)
     */
    private $f077fcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fstatus", type="integer", nullable=false)
     */
    private $f077fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fcreated_by", type="integer", nullable=false)
     */
    private $f077fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fupdated_by", type="integer", nullable=false)
     */
    private $f077fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f077fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f077fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fid_premise_category", type="integer", nullable=false)
     */
    private $f077fidPremiseCategory;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fpayment_method", type="integer", nullable=false)
     */
    private $f077fpaymentMethod;
    

    
    /**
     * Get f077fid
     *
     * @return integer
     */
    public function getF077fid()
    {
        return $this->f077fid;
    }

    /**
     * Set f077fcustomer
     *
     * @param integer $f077fcustomer
     *
     * @return T077frecurringSetUp
     */
    public function setF077fcustomer($f077fcustomer)
    {
        $this->f077fcustomer = $f077fcustomer;

        return $this;
    }

    /**
     * Get f077fcustomer
     *
     * @return integer
     */
    public function getF077fcustomer()
    {
        return $this->f077fcustomer;
    }

    /**
     * Set f077fdebitorType
     *
     * @param integer $f077fdebitorType
     *
     * @return T077frecurringSetUp
     */
    public function setF077fdebitorType($f077fdebitorType)
    {
        $this->f077fdebitorType = $f077fdebitorType;

        return $this;
    }

    /**
     * Get f077fdebitorType
     *
     * @return integer
     */
    public function getF077fdebitorType()
    {
        return $this->f077fdebitorType;
    }

    /**
     * Set f077fcontractNumber
     *
     * @param string $f077fcontractNumber
     *
     * @return T077frecurringSetUp
     */
    public function setF077fcontractNumber($f077fcontractNumber)
    {
        $this->f077fcontractNumber = $f077fcontractNumber;

        return $this;
    }

    /**
     * Get f077fcontractNumber
     *
     * @return string
     */
    public function getF077fcontractNumber()
    {
        return $this->f077fcontractNumber;
    }

    /**
     * Set f077freferenceNumber
     *
     * @param string $f077freferenceNumber
     *
     * @return T077frecurringSetUp
     */
    public function setF077freferenceNumber($f077freferenceNumber)
    {
        $this->f077freferenceNumber = $f077freferenceNumber;

        return $this;
    }

    /**
     * Get f077freferenceNumber
     *
     * @return string
     */
    public function getF077freferenceNumber()
    {
        return $this->f077freferenceNumber;
    }

    /**
     * Set f077fdateInvoice
     *
     * @param string $f077fdateInvoice
     *
     * @return T077frecurringSetUp
     */
    public function setF077fdateInvoice($f077fdateInvoice)
    {
        $this->f077fdateInvoice = $f077fdateInvoice;

        return $this;
    }

    /**
     * Get f077fdateInvoice
     *
     * @return string
     */
    public function getF077fdateInvoice()
    {
        return $this->f077fdateInvoice;
    }

    /**
     * Set f077fdiscountEntitlement
     *
     * @param string $f077fdiscountEntitlement
     *
     * @return T077frecurringSetUp
     */
    public function setF077fdiscountEntitlement($f077fdiscountEntitlement)
    {
        $this->f077fdiscountEntitlement = $f077fdiscountEntitlement;

        return $this;
    }

    /**
     * Get f077fdiscountEntitlement
     *
     * @return string
     */
    public function getF077fdiscountEntitlement()
    {
        return $this->f077fdiscountEntitlement;
    }

    /**
     * Set f077faddress
     *
     * @param string $f077faddress
     *
     * @return T077frecurringSetUp
     */
    public function setF077faddress($f077faddress)
    {
        $this->f077faddress = $f077faddress;

        return $this;
    }

    /**
     * Get f077faddress
     *
     * @return string
     */
    public function getF077faddress()
    {
        return $this->f077faddress;
    }

    /**
     * Set f077fperiod
     *
     * @param integer $f077fperiod
     *
     * @return T077frecurringSetUp
     */
    public function setF077fperiod($f077fperiod)
    {
        $this->f077fperiod = $f077fperiod;

        return $this;
    }

    /**
     * Get f077fperiod
     *
     * @return integer
     */
    public function getF077fperiod()
    {
        return $this->f077fperiod;
    }

    /**
     * Set f077feffectiveDate
     *
     * @param \DateTime $f077feffectiveDate
     *
     * @return T077frecurringSetUp
     */
    public function setF077feffectiveDate($f077feffectiveDate)
    {
        $this->f077feffectiveDate = $f077feffectiveDate;

        return $this;
    }

    /**
     * Get f077feffectiveDate
     *
     * @return \DateTime
     */
    public function getF077feffectiveDate()
    {
        return $this->f077feffectiveDate;
    }

     /**
     * Set f077fendDate
     *
     * @param \DateTime $f077fendDate
     *
     * @return T077frecurringSetUp
     */
    public function setF077fendDate($f077fendDate)
    {
        $this->f077fendDate = $f077fendDate;

        return $this;
    }

    /**
     * Get f077fendDate
     *
     * @return \DateTime
     */
    public function getF077fendDate()
    {
        return $this->f077fendDate;
    }

    /**
     * Set f077fpayment
     *
     * @param integer $f077fpayment
     *
     * @return T077frecurringSetUp
     */
    public function setF077fpayment($f077fpayment)
    {
        $this->f077fpayment = $f077fpayment;

        return $this;
    }

    /**
     * Get f077fpayment
     *
     * @return integer
     */
    public function getF077fpayment()
    {
        return $this->f077fpayment;
    }

    /**
     * Set f077fpremise
     *
     * @param integer $f077fpremise
     *
     * @return T077frecurringSetUp
     */
    public function setF077fpremise($f077fpremise)
    {
        $this->f077fpremise = $f077fpremise;

        return $this;
    }

    /**
     * Get f077fpremise
     *
     * @return integer
     */
    public function getF077fpremise()
    {
        return $this->f077fpremise;
    }

    /**
     * Set f077fcategory
     *
     * @param integer $f077fcategory
     *
     * @return T077frecurringSetUp
     */
    public function setF077fcategory($f077fcategory)
    {
        $this->f077fcategory = $f077fcategory;

        return $this;
    }

    /**
     * Get f077fcategory
     *
     * @return integer
     */
    public function getF077fcategory()
    {
        return $this->f077fcategory;
    }

    /**
     * Set f077fstatus
     *
     * @param integer $f077fstatus
     *
     * @return T077frecurringSetUp
     */
    public function setF077fstatus($f077fstatus)
    {
        $this->f077fstatus = $f077fstatus;

        return $this;
    }

    /**
     * Get f077fstatus
     *
     * @return integer
     */
    public function getF077fstatus()
    {
        return $this->f077fstatus;
    }

    /**
     * Set f077fcreatedBy
     *
     * @param integer $f077fcreatedBy
     *
     * @return T077frecurringSetUp
     */
    public function setF077fcreatedBy($f077fcreatedBy)
    {
        $this->f077fcreatedBy = $f077fcreatedBy;

        return $this;
    }

    /**
     * Get f077fcreatedBy
     *
     * @return integer
     */
    public function getF077fcreatedBy()
    {
        return $this->f077fcreatedBy;
    }

    /**
     * Set f077fupdatedBy
     *
     * @param integer $f077fupdatedBy
     *
     * @return T077frecurringSetUp
     */
    public function setF077fupdatedBy($f077fupdatedBy)
    {
        $this->f077fupdatedBy = $f077fupdatedBy;

        return $this;
    }

    /**
     * Get f077fupdatedBy
     *
     * @return integer
     */
    public function getF077fupdatedBy()
    {
        return $this->f077fupdatedBy;
    }

    /**
     * Set f077fcreatedDtTm
     *
     * @param \DateTime $f077fcreatedDtTm
     *
     * @return T077frecurringSetUp
     */
    public function setF077fcreatedDtTm($f077fcreatedDtTm)
    {
        $this->f077fcreatedDtTm = $f077fcreatedDtTm;

        return $this;
    }

    /**
     * Get f077fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF077fcreatedDtTm()
    {
        return $this->f077fcreatedDtTm;
    }

    /**
     * Set f077fupdatedDtTm
     *
     * @param \DateTime $f077fupdatedDtTm
     *
     * @return T077frecurringSetUp
     */
    public function setF077fupdatedDtTm($f077fupdatedDtTm)
    {
        $this->f077fupdatedDtTm = $f077fupdatedDtTm;

        return $this;
    }

    /**
     * Get f077fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF077fupdatedDtTm()
    {
        return $this->f077fupdatedDtTm;
    }


    /**
     * Set f077fidPremiseCategory
     *
     * @param \DateTime $f077fidPremiseCategory
     *
     * @return T077frecurringSetUp
     */
    public function setF077fidPremiseCategory($f077fidPremiseCategory)
    {
        $this->f077fidPremiseCategory = $f077fidPremiseCategory;

        return $this;
    }

    /**
     * Get f077fidPremiseCategory
     *
     * @return \DateTime
     */
    public function getF077fidPremiseCategory()
    {
        return $this->f077fidPremiseCategory;
    }


    /**
     * Set f077fpaymentMethod
     *
     * @param \DateTime $f077fpaymentMethod
     *
     * @return T077frecurringSetUp
     */
    public function setF077fpaymentMethod($f077fpaymentMethod)
    {
        $this->f077fpaymentMethod = $f077fpaymentMethod;

        return $this;
    }

    /**
     * Get f077fpaymentMethod
     *
     * @return \DateTime
     */
    public function getF077fpaymentMethod()
    {
        return $this->f077fpaymentMethod;
    }

}

