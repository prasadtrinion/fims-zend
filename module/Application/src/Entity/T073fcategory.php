<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T073fcategory
 *
 * @ORM\Table(name="t073fcategory")
 * @ORM\Entity(repositoryClass="Application\Repository\CategoryRepository")
 */
class T073fcategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f073fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f073fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f073fcategory_name", type="string", length=255, nullable=false)
     */
    private $f073fcategoryName;

    /**
     * @var string
     *
     * @ORM\Column(name="f073fcode", type="string", length=255, nullable=false)
     */
    private $f073fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f073frevenue_code", type="string", length=55, nullable=true)
     */
    private $f073frevenueCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f073ftax_code", type="integer", nullable=true)
     */
    private $f073ftaxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f073fprefix_code", type="string", length=55, nullable=true)
     */
    private $f073fprefixCode = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="f073fstatus", type="integer", nullable=false)
     */
    private $f073fstatus = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f073fcreated_by", type="integer", nullable=false)
     */
    private $f073fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f073fupdated_by", type="integer", nullable=false)
     */
    private $f073fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f073fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f073fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f073fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f073fupdatedDtTm;



    /**
     * Get f073fid
     *
     * @return integer
     */
    public function getF073fid()
    {
        return $this->f073fid;
    }

    /**
     * Set f073fcategoryName
     *
     * @param string $f073fcategoryName
     *
     * @return T073fcategory
     */
    public function setF073fcategoryName($f073fcategoryName)
    {
        $this->f073fcategoryName = $f073fcategoryName;

        return $this;
    }

    /**
     * Get f073fcategoryName
     *
     * @return string
     */
    public function getF073fcategoryName()
    {
        return $this->f073fcategoryName;
    }

    /**
     * Set f073fcode
     *
     * @param string $f073fcode
     *
     * @return T073fcategory
     */
    public function setF073fcode($f073fcode)
    {
        $this->f073fcode = $f073fcode;

        return $this;
    }

    /**
     * Get f073fcode
     *
     * @return string
     */
    public function getF073fcode()
    {
        return $this->f073fcode;
    }

    /**
     * Set f073frevenueCode
     *
     * @param string $f073frevenueCode
     *
     * @return T073fcategory
     */
    public function setF073frevenueCode($f073frevenueCode)
    {
        $this->f073frevenueCode = $f073frevenueCode;

        return $this;
    }

    /**
     * Get f073frevenueCode
     *
     * @return string
     */
    public function getF073frevenueCode()
    {
        return $this->f073frevenueCode;
    }

    /**
     * Set f073ftaxCode
     *
     * @param integer $f073ftaxCode
     *
     * @return T073fcategory
     */
    public function setF073ftaxCode($f073ftaxCode)
    {
        $this->f073ftaxCode = $f073ftaxCode;

        return $this;
    }

    /**
     * Get f073ftaxCode
     *
     * @return integer
     */
    public function getF073ftaxCode()
    {
        return $this->f073ftaxCode;
    }

    /**
     * Set f073fprefixCode
     *
     * @param string $f073fprefixCode
     *
     * @return T073fcategory
     */
    public function setF073fprefixCode($f073fprefixCode)
    {
        $this->f073fprefixCode = $f073fprefixCode;

        return $this;
    }

    /**
     * Get f073fprefixCode
     *
     * @return string
     */
    public function getF073fprefixCode()
    {
        return $this->f073fprefixCode;
    }
    
    /**
     * Set f073fstatus
     *
     * @param boolean $f073fstatus
     *
     * @return T073fcategory
     */
    public function setF073fstatus($f073fstatus)
    {
        $this->f073fstatus = $f073fstatus;

        return $this;
    }

    /**
     * Get f073fstatus
     *
     * @return boolean
     */
    public function getF073fstatus()
    {
        return $this->f073fstatus;
    }

    /**
     * Set f073fcreatedBy
     *
     * @param integer $f073fcreatedBy
     *
     * @return T073fcategory
     */
    public function setF073fcreatedBy($f073fcreatedBy)
    {
        $this->f073fcreatedBy = $f073fcreatedBy;

        return $this;
    }

    /**
     * Get f073fcreatedBy
     *
     * @return integer
     */
    public function getF073fcreatedBy()
    {
        return $this->f073fcreatedBy;
    }

    /**
     * Set f073fupdatedBy
     *
     * @param integer $f073fupdatedBy
     *
     * @return T073fcategory
     */
    public function setF073fupdatedBy($f073fupdatedBy)
    {
        $this->f073fupdatedBy = $f073fupdatedBy;

        return $this;
    }

    /**
     * Get f073fupdatedBy
     *
     * @return integer
     */
    public function getF073fupdatedBy()
    {
        return $this->f073fupdatedBy;
    }

    /**
     * Set f073fcreatedDtTm
     *
     * @param \DateTime $f073fcreatedDtTm
     *
     * @return T073fcategory
     */
    public function setF073fcreatedDtTm($f073fcreatedDtTm)
    {
        $this->f073fcreatedDtTm = $f073fcreatedDtTm;

        return $this;
    }

    /**
     * Get f073fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF073fcreatedDtTm()
    {
        return $this->f073fcreatedDtTm;
    }

    /**
     * Set f073fupdatedDtTm
     *
     * @param \DateTime $f073fupdatedDtTm
     *
     * @return T073fcategory
     */
    public function setF073fupdatedDtTm($f073fupdatedDtTm)
    {
        $this->f073fupdatedDtTm = $f073fupdatedDtTm;

        return $this;
    }

    /**
     * Get f073fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF073fupdatedDtTm()
    {
        return $this->f073fupdatedDtTm;
    }
}
