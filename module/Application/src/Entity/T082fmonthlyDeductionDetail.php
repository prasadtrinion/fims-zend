<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T082fmonthlyDeductionDetail
 *
 * @ORM\Table(name="t082fmonthly_deduction_detail")
 * @ORM\Entity(repositoryClass="Application\Repository\MonthlyDeductionRepository")
 */
class T082fmonthlyDeductionDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f082fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_master", type="integer", nullable=false)
     */
    private $f082fidMaster;

    /**
     * @var string
     *
     * @ORM\Column(name="f082fdeduction_code", type="string", length=250, nullable=true)
     */
    private $f082fdeductionCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082famount", type="integer", nullable=true)
     */
    private $f082famount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fstart_date", type="datetime", nullable=true)
     */
    private $f082fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fend_date", type="datetime", nullable=true)
     */
    private $f082fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fcreated_by", type="integer", nullable=true)
     */
    private $f082fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fupdated_by", type="integer", nullable=true)
     */
    private $f082fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fstatus", type="integer", nullable=true)
     */
    private $f082fstatus;



    /**
     * Get f082fidDetails
     *
     * @return integer
     */
    public function getF082fidDetails()
    {
        return $this->f082fidDetails;
    }

    /**
     * Set f082fidMaster
     *
     * @param integer $f082fidMaster
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fidMaster($f082fidMaster)
    {
        $this->f082fidMaster = $f082fidMaster;

        return $this;
    }

    /**
     * Get f082fidMaster
     *
     * @return integer
     */
    public function getF082fidMaster()
    {
        return $this->f082fidMaster;
    }

    /**
     * Set f082fdeductionCode
     *
     * @param string $f082fdeductionCode
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fdeductionCode($f082fdeductionCode)
    {
        $this->f082fdeductionCode = $f082fdeductionCode;

        return $this;
    }

    /**
     * Get f082fdeductionCode
     *
     * @return string
     */
    public function getF082fdeductionCode()
    {
        return $this->f082fdeductionCode;
    }

    /**
     * Set f082famount
     *
     * @param integer $f082famount
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082famount($f082famount)
    {
        $this->f082famount = $f082famount;

        return $this;
    }

    /**
     * Get f082famount
     *
     * @return integer
     */
    public function getF082famount()
    {
        return $this->f082famount;
    }

    /**
     * Set f082fstartDate
     *
     * @param \DateTime $f082fstartDate
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fstartDate($f082fstartDate)
    {
        $this->f082fstartDate = $f082fstartDate;

        return $this;
    }

    /**
     * Get f082fstartDate
     *
     * @return \DateTime
     */
    public function getF082fstartDate()
    {
        return $this->f082fstartDate;
    }

    /**
     * Set f082fendDate
     *
     * @param \DateTime $f082fendDate
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fendDate($f082fendDate)
    {
        $this->f082fendDate = $f082fendDate;

        return $this;
    }

    /**
     * Get f082fendDate
     *
     * @return \DateTime
     */
    public function getF082fendDate()
    {
        return $this->f082fendDate;
    }

    /**
     * Set f082fcreatedBy
     *
     * @param integer $f082fcreatedBy
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fcreatedBy($f082fcreatedBy)
    {
        $this->f082fcreatedBy = $f082fcreatedBy;

        return $this;
    }

    /**
     * Get f082fcreatedBy
     *
     * @return integer
     */
    public function getF082fcreatedBy()
    {
        return $this->f082fcreatedBy;
    }

    /**
     * Set f082fcreatedDtTm
     *
     * @param \DateTime $f082fcreatedDtTm
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fcreatedDtTm($f082fcreatedDtTm)
    {
        $this->f082fcreatedDtTm = $f082fcreatedDtTm;

        return $this;
    }

    /**
     * Get f082fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fcreatedDtTm()
    {
        return $this->f082fcreatedDtTm;
    }

    /**
     * Set f082fupdatedBy
     *
     * @param integer $f082fupdatedBy
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fupdatedBy($f082fupdatedBy)
    {
        $this->f082fupdatedBy = $f082fupdatedBy;

        return $this;
    }

    /**
     * Get f082fupdatedBy
     *
     * @return integer
     */
    public function getF082fupdatedBy()
    {
        return $this->f082fupdatedBy;
    }

    /**
     * Set f082fupdatedDtTm
     *
     * @param \DateTime $f082fupdatedDtTm
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fupdatedDtTm($f082fupdatedDtTm)
    {
        $this->f082fupdatedDtTm = $f082fupdatedDtTm;

        return $this;
    }

    /**
     * Get f082fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fupdatedDtTm()
    {
        return $this->f082fupdatedDtTm;
    }

    /**
     * Set f082fstatus
     *
     * @param integer $f082fstatus
     *
     * @return T082fmonthlyDeductionDetail
     */
    public function setF082fstatus($f082fstatus)
    {
        $this->f082fstatus = $f082fstatus;

        return $this;
    }

    /**
     * Get f082fstatus
     *
     * @return integer
     */
    public function getF082fstatus()
    {
        return $this->f082fstatus;
    }
}
