<?php 

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T037ftenderMaster
 *
 * @ORM\Table(name="t037ftender_master")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSuppliersRepository")
 */
class T037ftenderMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f037fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037ftender_number", type="string",length=50, nullable=false)
     */
    private $f037ftenderNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037famount", type="integer", nullable=false)
     */
    private $f037famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037ftitle", type="string",length=100, nullable=true)
     */
    private $f037ftitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fperiod", type="string",length=20, nullable=false)
     */
    private $f037fperiod;


    /**
     * @var integer
     *
     * @ORM\Column(name="f037fstatus", type="integer", nullable=false)
     */
    private $f037fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f037fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f037fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f037fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f037fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f037fupdateDtTm;



    /**
     * Get f037fid
     *
     * @return integer
     */
    public function getF037fid()
    {
        return $this->f037fid;
    }

    /**
     * Set f037ftenderNumber
     *
     * @param string $f037ftenderNumber
     *
     * @return T037ftenderSuppliers
     */
    public function setF037ftenderNumber($f037ftenderNumber)
    {
        $this->f037ftenderNumber = $f037ftenderNumber;

        return $this;
    }

    /**
     * Get f037ftenderNumber
     *
     * @return string
     */
    public function getF037ftenderNumber()
    {
        return $this->f037ftenderNumber;
    }

    /**
     * Set f037famount
     *
     * @param integer $f037famount
     *
     * @return T037ftenderSuppliers
     */
    public function setF037famount($f037famount)
    {
        $this->f037famount = $f037famount;

        return $this;
    }

    /**
     * Get f037famount
     *
     * @return integer
     */
    public function getF037famount()
    {
        return $this->f037famount;
    }

    /**
     * Set f037ftitle
     *
     * @param integer $f037ftitle
     *
     * @return T037ftenderSuppliers
     */
    public function setF037ftitle($f037ftitle)
    {
        $this->f037ftitle = $f037ftitle;

        return $this;
    }

    /**
     * Get f037ftitle
     *
     * @return integer
     */
    public function getF037ftitle()
    {
        return $this->f037ftitle;
    }

    /**
     * Set f037fperiod
     *
     * @param integer $f037fperiod
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fperiod($f037fperiod)
    {
        $this->f037fperiod = $f037fperiod;

        return $this;
    }

    /**
     * Get f037fperiod
     *
     * @return integer
     */
    public function getF037fperiod()
    {
        return $this->f037fperiod;
    }


    /**
     * Get f037fstatus
     *
     * @return integer
     */
    public function getF037fstatus()
    {
        return $this->f037fstatus;
    }

     /**
     * Set f037fstatus
     *
     * @param integer $f037fstatus
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fstatus($f037fstatus)
    {
        $this->f037fstatus = $f037fstatus;

        return $this;
    }

    /**
     * Set f037fcreatedBy
     *
     * @param integer $f037fcreatedBy
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fcreatedBy($f037fcreatedBy)
    {
        $this->f037fcreatedBy = $f037fcreatedBy;

        return $this;
    }
   
    /**
     * Get f037fcreatedBy
     *
     * @return integer
     */
    public function getF037fcreatedBy()
    {
        return $this->f037fcreatedBy;
    }

    /**
     * Set f037fupdatedBy
     *
     * @param integer $f037fupdatedBy
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fupdatedBy($f037fupdatedBy)
    {
        $this->f037fupdatedBy = $f037fupdatedBy;

        return $this;
    }

    /**
     * Get f037fupdatedBy
     *
     * @return integer
     */
    public function getF037fupdatedBy()
    {
        return $this->f037fupdatedBy;
    }

    /**
     * Set f037fcreateDtTm
     *
     * @param \DateTime $f037fcreateDtTm
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fcreateDtTm($f037fcreateDtTm)
    {
        $this->f037fcreateDtTm = $f037fcreateDtTm;

        return $this;
    }

    /**
     * Get f037fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF037fcreateDtTm()
    {
        return $this->f037fcreateDtTm;
    }

    /**
     * Set f037fupdateDtTm
     *
     * @param \DateTime $f037fupdateDtTm
     *
     * @return T037ftenderSuppliers
     */
    public function setF037fupdateDtTm($f037fupdateDtTm)
    {
        $this->f037fupdateDtTm = $f037fupdateDtTm;

        return $this;
    }

    /**
     * Get f037fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF037fupdateDtTm()
    {
        return $this->f037fupdateDtTm;
    }
}

























