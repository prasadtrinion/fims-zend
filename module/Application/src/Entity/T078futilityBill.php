<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T078futilityBill
 *
 * @ORM\Table(name="t078futility_bill")
 * @ORM\Entity(repositoryClass="Application\Repository\UtilityBillRepository")
 */
class T078futilityBill
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f078fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid_utility", type="integer", nullable=false)
     */
    private $f078fidUtility;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid_premise", type="integer", nullable=true)
     */
    private $f078fidPremise;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fcurrent_reading_value", type="string", length=100, nullable=true)
     */
    private $f078fcurrentReadingValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078ftotal_usage", type="integer",  nullable=true)
     */
    private $f078ftotalUsage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078famount", type="integer",  nullable=true)
     */
    private $f078famount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078ffrom_date", type="datetime", nullable=false)
     */
    private $f078ffromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fto_date", type="datetime", nullable=false)
     */
    private $f078ftoDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078freading_date", type="datetime", nullable=false)
     */
    private $f078freadingDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078funits", type="integer", nullable=false)
     */
    private $f078funits;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fstatus", type="integer", nullable=false)
     */
    private $f078fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fcreated_by", type="integer", nullable=false)
     */
    private $f078fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fupdated_by", type="integer", nullable=false)
     */
    private $f078fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fcalculated_amount", type="integer", nullable=true)
     */
    private $f078fcalculatedAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fgenerated_bill", type="integer", nullable=true)
     */
    private $f078fgeneratedBill;


	

    /**
     * Get f078fid
     *
     * @return integer
     */
    public function getF078fid()
    {
        return $this->f078fid;
    }

    /**
     * Set f078fidUtility
     *
     * @param integer $f078fidUtility
     *
     * @return T078futilityBill
     */
    public function setF078fidUtility($f078fidUtility)
    {
        $this->f078fidUtility = $f078fidUtility;

        return $this;
    }

    /**
     * Get f078fidUtility
     *
     * @return integer
     */
    public function getF078fidUtility()
    {
        return $this->f078fidUtility;
    }

    /**
     * Set f078fidPremise
     *
     * @param integer $f078fidPremise
     *
     * @return T078futilityBill
     */
    public function setF078fidPremise($f078fidPremise)
    {
        $this->f078fidPremise = $f078fidPremise;

        return $this;
    }

    /**
     * Get f078fidPremise
     *
     * @return integer
     */
    public function getF078fidPremise()
    {
        return $this->f078fidPremise;
    }


    /**
     * Set f078ffromDate
     *
     * @param string $f078ffromDate
     *
     * @return T078futilityBill
     */
    public function setF078ffromDate($f078ffromDate)
    {
        $this->f078ffromDate = $f078ffromDate;

        return $this;
    }

    /**
     * Get f078ffromDate
     *
     * @return string
     */
    public function getF078ffromDate()
    {
        return $this->f078ffromDate;
    }

    /**
     * Set f078freadingDate
     *
     * @param string $f078freadingDate
     *
     * @return T078futilityBill
     */
    public function setF078freadingDate($f078freadingDate)
    {
        $this->f078freadingDate = $f078freadingDate;

        return $this;
    }

    /**
     * Get f078freadingDate
     *
     * @return string
     */
    public function getF078freadingDate()
    {
        return $this->f078freadingDate;
    }

    /**
     * Set f078fcurrentReadingValue
     *
     * @param string $f078fcurrentReadingValue
     *
     * @return T078futilityBill
     */
    public function setF078fcurrentReadingValue($f078fcurrentReadingValue)
    {
        $this->f078fcurrentReadingValue = $f078fcurrentReadingValue;

        return $this;
    }

    /**
     * Get f078fcurrentReadingValue
     *
     * @return string
     */
    public function getF078fcurrentReadingValue()
    {
        return $this->f078fcurrentReadingValue;
    }

    /**
     * Set f078ftotalUsage
     *
     * @param integer $f078ftotalUsage
     *
     * @return T078futilityBill
     */
    public function setF078ftotalUsage($f078ftotalUsage)
    {
        $this->f078ftotalUsage = $f078ftotalUsage;

        return $this;
    }

    /**
     * Get f078ftotalUsage
     *
     * @return integer
     */
    public function getF078ftotalUsage()
    {
        return $this->f078ftotalUsage;
    }

    /**
     * Set f078famount
     *
     * @param integer $f078famount
     *
     * @return T078futilityBill
     */
    public function setF078famount($f078famount)
    {
        $this->f078famount = $f078famount;

        return $this;
    }

    /**
     * Get f078famount
     *
     * @return integer
     */
    public function getF078famount()
    {
        return $this->f078famount;
    }

    /**
     * Set f078ftoDate
     *
     * @param string $f078ftoDate
     *
     * @return T078futilityBill
     */
    public function setF078ftoDate($f078ftoDate)
    {
        $this->f078ftoDate = $f078ftoDate;

        return $this;
    }

    /**
     * Get f078ftoDate
     *
     * @return string
     */
    public function getF078ftoDate()
    {
        return $this->f078ftoDate;
    }

    /**
     * Set f078funits
     *
     * @param string $f078funits
     *
     * @return T078futilityBill
     */
    public function setF078funits($f078funits)
    {
        $this->f078funits = $f078funits;

        return $this;
    }

    /**
     * Get f078funits
     *
     * @return string
     */
    public function getF078funits()
    {
        return $this->f078funits;
    }


    /**
     * Set f078fstatus
     *
     * @param integer $f078fstatus
     *
     * @return T078futilityBill
     */
    public function setF078fstatus($f078fstatus)
    {
        $this->f078fstatus = $f078fstatus;

        return $this;
    }

    /**
     * Get f078fstatus
     *
     * @return integer
     */
    public function getF078fstatus()
    {
        return $this->f078fstatus;
    }

    /**
     * Set f078fcreatedBy
     *
     * @param integer $f078fcreatedBy
     *
     * @return T078futilityBill
     */
    public function setF078fcreatedBy($f078fcreatedBy)
    {
        $this->f078fcreatedBy = $f078fcreatedBy;

        return $this;
    }

    /**
     * Get f078fcreatedBy
     *
     * @return integer
     */
    public function getF078fcreatedBy()
    {
        return $this->f078fcreatedBy;
    }

    /**
     * Set f078fupdatedBy
     *
     * @param integer $f078fupdatedBy
     *
     * @return T078futilityBill
     */
    public function setF078fupdatedBy($f078fupdatedBy)
    {
        $this->f078fupdatedBy = $f078fupdatedBy;

        return $this;
    }

    /**
     * Get f078fupdatedBy
     *
     * @return integer
     */
    public function getF078fupdatedBy()
    {
        return $this->f078fupdatedBy;
    }

    /**
     * Set f078fcreatedDtTm
     *
     * @param \DateTime $f078fcreatedDtTm
     *
     * @return T078futilityBill
     */
    public function setF078fcreatedDtTm($f078fcreatedDtTm)
    {
        $this->f078fcreatedDtTm = $f078fcreatedDtTm;

        return $this;
    }

    /**
     * Get f078fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fcreatedDtTm()
    {
        return $this->f078fcreatedDtTm;
    }

    /**
     * Set f078fupdatedDtTm
     *
     * @param \DateTime $f078fupdatedDtTm
     *
     * @return T078futilityBill
     */
    public function setF078fupdatedDtTm($f078fupdatedDtTm)
    {
        $this->f078fupdatedDtTm = $f078fupdatedDtTm;

        return $this;
    }

    /**
     * Get f078fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fupdatedDtTm()
    {
        return $this->f078fupdatedDtTm;
    }

    /**
     * Set f078fcalculatedAmount
     *
     * @param integer $f078fcalculatedAmount
     *
     * @return T078futilityBill
     */
    public function setF078fcalculatedAmount($f078fcalculatedAmount)
    {
        $this->f078fcalculatedAmount = $f078fcalculatedAmount;

        return $this;
    }

    /**
     * Get f078fcalculatedAmount
     *
     * @return integer
     */
    public function getF078fcalculatedAmount()
    {
        return $this->f078fcalculatedAmount;
    }

    /**
     * Set f078fgeneratedBill
     *
     * @param integer $f078fgeneratedBill
     *
     * @return T078futilityBill
     */
    public function setF078fgeneratedBill($f078fgeneratedBill)
    {
        $this->f078fgeneratedBill = $f078fgeneratedBill;

        return $this;
    }

    /**
     * Get f078fgeneratedBill
     *
     * @return integer
     */
    public function getF078fgeneratedBill()
    {
        return $this->f078fgeneratedBill;
    }
}

