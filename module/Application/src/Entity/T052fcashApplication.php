<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052fcashApplication
 *
 * @ORM\Table(name="t052fcash_application")
 * @ORM\Entity(repositoryClass="Application\Repository\CashApplicationRepository")
 */
class T052fcashApplication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fcash_advance", type="string", length=20, nullable=true)
     */
    private $f052fcashAdvance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_staff", type="integer", nullable=true)
     */
    private $f052fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f052fid_department", type="string",length=50, nullable=true)
     */
    private $f052fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_financial_year", type="integer", nullable=true)
     */
    private $f052fidFinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fapproval_status", type="integer", nullable=true)
     */
    private $f052fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fis_processed", type="integer", nullable=true)
     */
    private $f052fisProcessed;

    /**
     * @var string
     *
     * @ORM\Column(name="f052freason", type="string", length=255, nullable=true)
     */
    private $f052freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052ftotal", type="integer", nullable=true)
     */
    private $f052ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=true)
     */
    private $f052fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=true)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=true)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f052fupdatedDtTm;



    /**
     * Get f052fid
     *
     * @return integer
     */
    public function getF052fid()
    {
        return $this->f052fid;
    }

    /**
     * Set f052fcashAdvance
     *
     * @param string $f052fcashAdvance
     *
     * @return T052fcashApplication
     */
    public function setF052fcashAdvance($f052fcashAdvance)
    {
        $this->f052fcashAdvance = $f052fcashAdvance;

        return $this;
    }

    /**
     * Get f052fcashAdvance
     *
     * @return string
     */
    public function getF052fcashAdvance()
    {
        return $this->f052fcashAdvance;
    }

    /**
     * Set f052ftotal
     *
     * @param integer $f052ftotal
     *
     * @return T052fcashApplication
     */
    public function setF052ftotal($f052ftotal)
    {
        $this->f052ftotal = $f052ftotal;

        return $this;
    }

    /**
     * Get f052ftotal
     *
     * @return integer
     */
    public function getF052ftotal()
    {
        return $this->f052ftotal;
    }

    /**
     * Set f052fidStaff
     *
     * @param integer $f052fidStaff
     *
     * @return T052fcashApplication
     */
    public function setF052fidStaff($f052fidStaff)
    {
        $this->f052fidStaff = $f052fidStaff;

        return $this;
    }

    /**
     * Get f052fidStaff
     *
     * @return integer
     */
    public function getF052fidStaff()
    {
        return $this->f052fidStaff;
    }

    /**
     * Set f052fidDepartment
     *
     * @param string $f052fidDepartment
     *
     * @return T052fcashApplication
     */
    public function setF052fidDepartment($f052fidDepartment)
    {
        $this->f052fidDepartment = $f052fidDepartment;

        return $this;
    }

    /**
     * Get f052fidDepartment
     *
     * @return string
     */
    public function getF052fidDepartment()
    {
        return $this->f052fidDepartment;
    }

    /**
     * Set f052fidFinancialYear
     *
     * @param integer $f052fidFinancialYear
     *
     * @return T052fcashApplication
     */
    public function setF052fidFinancialYear($f052fidFinancialYear)
    {
        $this->f052fidFinancialYear = $f052fidFinancialYear;

        return $this;
    }

    /**
     * Get f052fidFinancialYear
     *
     * @return integer
     */
    public function getF052fidFinancialYear()
    {
        return $this->f052fidFinancialYear;
    }

    /**
     * Set f052fapprovalStatus
     *
     * @param integer $f052fapprovalStatus
     *
     * @return T052fcashApplication
     */
    public function setF052fapprovalStatus($f052fapprovalStatus)
    {
        $this->f052fapprovalStatus = $f052fapprovalStatus;

        return $this;
    }

    /**
     * Get f052fapprovalStatus
     *
     * @return integer
     */
    public function getF052fapprovalStatus()
    {
        return $this->f052fapprovalStatus;
    }

    /**
     * Set f052fisProcessed
     *
     * @param integer $f052fisProcessed
     *
     * @return T052fcashApplication
     */
    public function setF052fisProcessed($f052fisProcessed)
    {
        $this->f052fisProcessed = $f052fisProcessed;

        return $this;
    }

    /**
     * Get f052fisProcessed
     *
     * @return integer
     */
    public function getF052fisProcessed()
    {
        return $this->f052fisProcessed;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052fcashApplication
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052fcashApplication
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052fcashApplication
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052fcashApplication
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052fcashApplication
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }

    /**
     * Set f052freason
     *
     * @param string $f052freason
     *
     * @return T052fcashApplication
     */
    public function setF052freason($f052freason)
    {
        $this->f052freason = $f052freason;

        return $this;
    }

    /**
     * Get f052freason
     *
     * @return string
     */
    public function getF052freason()
    {
        return $this->f052freason;
    }
}
