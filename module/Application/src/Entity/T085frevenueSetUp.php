<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T085frevenueSetUp
 *
 * @ORM\Table(name="t085frevenue_set_up",uniqueConstraints={@ORM\UniqueConstraint(name="f085fcode_UNIQUE", columns={"f085fcode"})})
 * @ORM\Entity(repositoryClass="Application\Repository\RevenueSetUpRepository")
 */
class T085frevenueSetUp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f085fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fname", type="string", length=50, nullable=false)
     */
    private $f085fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fcode", type="string", length=50, nullable=false)
     */
    private $f085fcode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_revenue_category", type="integer", nullable=false)
     */
    private $f085fidRevenueCategory;

     /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_tax_code", type="integer", nullable=false)
     */
    private $f085fidTaxCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f085faccount_id", type="string", length=10, nullable=true)
     */
    private $f085faccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="f085ffund", type="string", length=55, nullable=true)
     */
    private $f085ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f085fdepartment", type="string", length=55, nullable=true)
     */
    private $f085fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f085factivity", type="string", length=55, nullable=true)
     */
    private $f085factivity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fstatus", type="integer", nullable=false)
     */
    private $f085fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f085fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f085fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fupdateDtTm;



    /**
     * Get f085fid
     *
     * @return integer
     */
    public function getF085fid()
    {
        return $this->f085fid;
    }

    /**
     * Set f085fname
     *
     * @param string $f085fname
     *
     * @return T085frevenueSetUp
     */
    public function setF085fname($f085fname)
    {
        $this->f085fname = $f085fname;

        return $this;
    }

    /**
     * Get f085fname
     *
     * @return string
     */
    public function getF085fname()
    {
        return $this->f085fname;
    }

    /**
     * Set f085fcode
     *
     * @param string $f085fcode
     *
     * @return T085frevenueSetUp
     */
    public function setF085fcode($f085fcode)
    {
        $this->f085fcode = $f085fcode;

        return $this;
    }

    /**
     * Get f085fcode
     *
     * @return string
     */
    public function getF085fcode()
    {
        return $this->f085fcode;
    }

    /**
     * Set f085ffundId
     *
     * @param integer $f085ffundId
     *
     * @return T085frevenueSetUp
     */
    public function setF085ffundId($f085ffundId)
    {
        $this->f085ffundId = $f085ffundId;

        return $this;
    }

    /**
     * Get f085ffundId
     *
     * @return integer
     */
    public function getF085ffundId()
    {
        return $this->f085ffundId;
    }

     /**
     * Set f085fidRevenueCategory
     *
     * @param integer $f085fidRevenueCategory
     *
     * @return T085frevenueSetUp
     */
    public function setF085fidRevenueCategory($f085fidRevenueCategory)
    {
        $this->f085fidRevenueCategory = $f085fidRevenueCategory;

        return $this;
    }

    /**
     * Get f085fidRevenueCategory
     *
     * @return integer
     */
    public function getF085fidRevenueCategory()
    {
        return $this->f085fidRevenueCategory;
    }

     /**
     * Set f085fidTaxCode
     *
     * @param integer $f085fidTaxCode
     *
     * @return T085frevenueSetUp
     */
    public function setF085fidTaxCode($f085fidTaxCode)
    {
        $this->f085fidTaxCode = $f085fidTaxCode;

        return $this;
    }

    /**
     * Get f085fidTaxCode
     *
     * @return integer
     */
    public function getF085fidTaxCode()
    {
        return $this->f085fidTaxCode;
    }


    /**
     * Set f085faccountId
     *
     * @param string $f085faccountId
     *
     * @return T085frevenueSetUp
     */
    public function setF085faccountId($f085faccountId)
    {
        $this->f085faccountId = $f085faccountId;

        return $this;
    }

    /**
     * Get f085faccountId
     *
     * @return string
     */
    public function getF085faccountId()
    {
        return $this->f085faccountId;
    }

    /**
     * Set f085ffund
     *
     * @param string $f085ffund
     *
     * @return T085frevenueSetUp
     */
    public function setF085ffund($f085ffund)
    {
        $this->f085ffund = $f085ffund;

        return $this;
    }

    /**
     * Get f085ffund
     *
     * @return string
     */
    public function getF085ffund()
    {
        return $this->f085ffund;
    }

    /**
     * Set f085fdepartment
     *
     * @param string $f085fdepartment
     *
     * @return T085frevenueSetUp
     */
    public function setF085fdepartment($f085fdepartment)
    {
        $this->f085fdepartment = $f085fdepartment;

        return $this;
    }

    /**
     * Get f085fdepartment
     *
     * @return string
     */
    public function getF085fdepartment()
    {
        return $this->f085fdepartment;
    }

    /**
     * Set f085factivity
     *
     * @param string $f085factivity
     *
     * @return T085frevenueSetUp
     */
    public function setF085factivity($f085factivity)
    {
        $this->f085factivity = $f085factivity;

        return $this;
    }

    /**
     * Get f085factivity
     *
     * @return string
     */
    public function getF085factivity()
    {
        return $this->f085factivity;
    }


    /**
     * Set f085fstatus
     *
     * @param integer $f085fstatus
     *
     * @return T085frevenueSetUp
     */
    public function setF085fstatus($f085fstatus)
    {
        $this->f085fstatus = $f085fstatus;

        return $this;
    }

    /**
     * Get f085fstatus
     *
     * @return integer
     */
    public function getF085fstatus()
    {
        return $this->f085fstatus;
    }

    /**
     * Set f085fcreatedBy
     *
     * @param integer $f085fcreatedBy
     *
     * @return T085frevenueSetUp
     */
    public function setF085fcreatedBy($f085fcreatedBy)
    {
        $this->f085fcreatedBy = $f085fcreatedBy;

        return $this;
    }

    /**
     * Get f085fcreatedBy
     *
     * @return integer
     */
    public function getF085fcreatedBy()
    {
        return $this->f085fcreatedBy;
    }

    /**
     * Set f085fupdatedBy
     *
     * @param integer $f085fupdatedBy
     *
     * @return T085frevenueSetUp
     */
    public function setF085fupdatedBy($f085fupdatedBy)
    {
        $this->f085fupdatedBy = $f085fupdatedBy;

        return $this;
    }

    /**
     * Get f085fupdatedBy
     *
     * @return integer
     */
    public function getF085fupdatedBy()
    {
        return $this->f085fupdatedBy;
    }

    /**
     * Set f085fcreateDtTm
     *
     * @param \DateTime $f085fcreateDtTm
     *
     * @return T085frevenueSetUp
     */
    public function setF085fcreateDtTm($f085fcreateDtTm)
    {
        $this->f085fcreateDtTm = $f085fcreateDtTm;

        return $this;
    }

    /**
     * Get f085fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF085fcreateDtTm()
    {
        return $this->f085fcreateDtTm;
    }

    /**
     * Set f085fupdateDtTm
     *
     * @param \DateTime $f085fupdateDtTm
     *
     * @return T085frevenueSetUp
     */
    public function setF085fupdateDtTm($f085fupdateDtTm)
    {
        $this->f085fupdateDtTm = $f085fupdateDtTm;

        return $this;
    }

    /**
     * Get f085fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF085fupdateDtTm()
    {
        return $this->f085fupdateDtTm;
    }
}

























