<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T124fstockRegistration
 *
 * @ORM\Table(name="t124fstock_registration")
 * @ORM\Entity(repositoryClass="Application\Repository\StockRegistrationRepository")
 */
class T124fstockRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f124fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f124fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f124fasset_category", type="integer", nullable=false)
     */
    private $f124fassetCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fasset_sub_category", type="integer", nullable=false)
     */
    private $f124fassetSubCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fasset_item", type="integer", nullable=false)
     */
    private $f124fassetItem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fstore", type="integer", nullable=false)
     */
    private $f124fstore;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fquantity", type="integer", nullable=false)
     */
    private $f124fquantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fdate", type="datetime", nullable=false)
     */
    private $f124fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f124fstatus", type="integer", nullable=false)
     */
    private $f124fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f124fcreated_by", type="integer", nullable=false)
     */
    private $f124fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f124fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f124fupdated_by", type="integer", nullable=false)
     */
    private $f124fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f124fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f124fupdatedDtTm;



    /**
     * Get f124fid
     *
     * @return integer
     */
    public function getF124fid()
    {
        return $this->f124fid;
    }

    /**
     * Set f124fassetCategory
     *
     * @param integer $f124fassetCategory
     *
     * @return T124fstockRegistration
     */
    public function setF124fassetCategory($f124fassetCategory)
    {
        $this->f124fassetCategory = $f124fassetCategory;

        return $this;
    }

    /**
     * Get f124fassetCategory
     *
     * @return integer
     */
    public function getF124fassetCategory()
    {
        return $this->f124fassetCategory;
    }

    /**
     * Set f124fassetSubCategory
     *
     * @param \DateTime $f124fassetSubCategory
     *
     * @return T124fstockRegistration
     */
    public function setF124fassetSubCategory($f124fassetSubCategory)
    {
        $this->f124fassetSubCategory = $f124fassetSubCategory;

        return $this;
    }

    /**
     * Get f124fassetSubCategory
     *
     * @return \DateTime
     */
    public function getF124fassetSubCategory()
    {
        return $this->f124fassetSubCategory;
    }

    /**
     * Set f124fassetItem
     *
     * @param \DateTime $f124fassetItem
     *
     * @return T124fstockRegistration
     */
    public function setF124fassetItem($f124fassetItem)
    {
        $this->f124fassetItem = $f124fassetItem;

        return $this;
    }

    /**
     * Get f124fassetItem
     *
     * @return \DateTime
     */
    public function getF124fassetItem()
    {
        return $this->f124fassetItem;
    }


    /**
     * Set f124fstore
     *
     * @param \DateTime $f124fstore
     *
     * @return T124fstockRegistration
     */
    public function setF124fstore($f124fstore)
    {
        $this->f124fstore = $f124fstore;

        return $this;
    }

    /**
     * Get f124fstore
     *
     * @return \DateTime
     */
    public function getF124fstore()
    {
        return $this->f124fstore;
    }

    /**
     * Set f124fquantity
     *
     * @param \DateTime $f124fquantity
     *
     * @return T124fstockRegistration
     */
    public function setF124fquantity($f124fquantity)
    {
        $this->f124fquantity = $f124fquantity;

        return $this;
    }

    /**
     * Get f124fquantity
     *
     * @return \DateTime
     */
    public function getF124fquantity()
    {
        return $this->f124fquantity;
    }

    /**
     * Set f124fdate
     *
     * @param \DateTime $f124fdate
     *
     * @return T124fstockRegistration
     */
    public function setF124fdate($f124fdate)
    {
        $this->f124fdate = $f124fdate;

        return $this;
    }

    /**
     * Get f124fdate
     *
     * @return \DateTime
     */
    public function getF124fdate()
    {
        return $this->f124fdate;
    }


    /**
     * Set f124fstatus
     *
     * @param integer $f124fstatus
     *
     * @return T124fstockRegistration
     */
    public function setF124fstatus($f124fstatus)
    {
        $this->f124fstatus = $f124fstatus;

        return $this;
    }

    /**
     * Get f124fstatus
     *
     * @return integer
     */
    public function getF124fstatus()
    {
        return $this->f124fstatus;
    }

    /**
     * Set f124fcreatedBy
     *
     * @param integer $f124fcreatedBy
     *
     * @return T124fstockRegistration
     */
    public function setF124fcreatedBy($f124fcreatedBy)
    {
        $this->f124fcreatedBy = $f124fcreatedBy;

        return $this;
    }

    /**
     * Get f124fcreatedBy
     *
     * @return integer
     */
    public function getF124fcreatedBy()
    {
        return $this->f124fcreatedBy;
    }

    /**
     * Set f124fcreatedDtTm
     *
     * @param \DateTime $f124fcreatedDtTm
     *
     * @return T124fstockRegistration
     */
    public function setF124fcreatedDtTm($f124fcreatedDtTm)
    {
        $this->f124fcreatedDtTm = $f124fcreatedDtTm;

        return $this;
    }

    /**
     * Get f124fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF124fcreatedDtTm()
    {
        return $this->f124fcreatedDtTm;
    }

    /**
     * Set f124fupdatedBy
     *
     * @param integer $f124fupdatedBy
     *
     * @return T124fstockRegistration
     */
    public function setF124fupdatedBy($f124fupdatedBy)
    {
        $this->f124fupdatedBy = $f124fupdatedBy;

        return $this;
    }

    /**
     * Get f124fupdatedBy
     *
     * @return integer
     */
    public function getF124fupdatedBy()
    {
        return $this->f124fupdatedBy;
    }

    /**
     * Set f124fupdatedDtTm
     *
     * @param \DateTime $f124fupdatedDtTm
     *
     * @return T124fstockRegistration
     */
    public function setF124fupdatedDtTm($f124fupdatedDtTm)
    {
        $this->f124fupdatedDtTm = $f124fupdatedDtTm;

        return $this;
    }

    /**
     * Get f124fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF124fupdatedDtTm()
    {
        return $this->f124fupdatedDtTm;
    }
}
