<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T050ftenderSubmission
 *
 * @ORM\Table(name="t050ftender_submission")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSubmissionRepository")
 */
class T050ftenderSubmission
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f050fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid_vendor", type="integer", nullable=true)
     */
    private $f050fidVendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f050fvendor_code", type="string", length=20, nullable=true)
     */
    private $f050fvendorCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f050fgrade", type="string", length=20, nullable=true)
     */
    private $f050fgrade;

    /**
     * @var float
     *
     * @ORM\Column(name="f050famount", type="integer", nullable=false)
     */
    private $f050famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f050fcompletion_duration", type="string", length=50, nullable=true)
     */
    private $f050fcompletionDuration;

    /**
     * @var string
     *
     * @ORM\Column(name="f050fdescription", type="string", length=100, nullable=true)
     */
    private $f050fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fis_shortlisted", type="integer", nullable=false)
     */
    private $f050fisShortlisted;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fis_awarded", type="integer", nullable=false)
     */
    private $f050fisAwarded;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid_tendor", type="integer", nullable=false)
     */
    private $f050fidTendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fcreated_by", type="integer", nullable=false)
     */
    private $f050fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fupdated_by", type="integer", nullable=false)
     */
    private $f050fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f050fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f050fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fstatus", type="integer", nullable=false)
     */
    private $f050fstatus;

     /**
     * @var string
     *
     * @ORM\Column(name="f050fvendor_name", type="string", length=150, nullable=true)
     */
    private $f050fvendorName;


    


    /**
     * Get f050fid
     *
     * @return integer
     */
    public function getF050fid()
    {
        return $this->f050fid;
    }

    /**
     * Set f050fidVendor
     *
     * @param integer $f050fidVendor
     *
     * @return T050ftenderSubmission
     */
    public function setF050fidVendor($f050fidVendor)
    {
        $this->f050fidVendor = $f050fidVendor;

        return $this;
    }

    /**
     * Get f050fidVendor
     *
     * @return integer
     */
    public function getF050fidVendor()
    {
        return $this->f050fidVendor;
    }

    /**
     * Set f050fvendorCode
     *
     * @param string $f050fvendorCode
     *
     * @return T050ftenderSubmission
     */
    public function setF050fvendorCode($f050fvendorCode)
    {
        $this->f050fvendorCode = $f050fvendorCode;

        return $this;
    }

    /**
     * Get f050fvendorCode
     *
     * @return string
     */
    public function getF050fvendorCode()
    {
        return $this->f050fvendorCode;
    }

    /**
     * Set f050fgrade
     *
     * @param string $f050fgrade
     *
     * @return T050ftenderSubmission
     */
    public function setF050fgrade($f050fgrade)
    {
        $this->f050fgrade = $f050fgrade;

        return $this;
    }

    /**
     * Get f050fgrade
     *
     * @return string
     */
    public function getF050fgrade()
    {
        return $this->f050fgrade;
    }

    /**
     * Set f050famount
     *
     * @param float $f050famount
     *
     * @return T050ftenderSubmission
     */
    public function setF050famount($f050famount)
    {
        $this->f050famount = $f050famount;

        return $this;
    }

    /**
     * Get f050famount
     *
     * @return float
     */
    public function getF050famount()
    {
        return $this->f050famount;
    }

    /**
     * Set f050fcompletionDuration
     *
     * @param string $f050fcompletionDuration
     *
     * @return T050ftenderSubmission
     */
    public function setF050fcompletionDuration($f050fcompletionDuration)
    {
        $this->f050fcompletionDuration = $f050fcompletionDuration;

        return $this;
    }

    /**
     * Get f050fcompletionDuration
     *
     * @return string
     */
    public function getF050fcompletionDuration()
    {
        return $this->f050fcompletionDuration;
    }

    /**
     * Set f050fdescription
     *
     * @param string $f050fdescription
     *
     * @return T050ftenderSubmission
     */
    public function setF050fdescription($f050fdescription)
    {
        $this->f050fdescription = $f050fdescription;

        return $this;
    }

    /**
     * Get f050fdescription
     *
     * @return string
     */
    public function getF050fdescription()
    {
        return $this->f050fdescription;
    }

    /**
     * Set f050fisShortlisted
     *
     * @param integer $f050fisShortlisted
     *
     * @return T050ftenderSubmission
     */
    public function setF050fisShortlisted($f050fisShortlisted)
    {
        $this->f050fisShortlisted = $f050fisShortlisted;

        return $this;
    }

    /**
     * Get f050fisShortlisted
     *
     * @return integer
     */
    public function getF050fisShortlisted()
    {
        return $this->f050fisShortlisted;
    }

    /**
     * Set f050fisAwarded
     *
     * @param integer $f050fisAwarded
     *
     * @return T050ftenderSubmission
     */
    public function setF050fisAwarded($f050fisAwarded)
    {
        $this->f050fisAwarded = $f050fisAwarded;

        return $this;
    }

    /**
     * Get f050fisAwarded
     *
     * @return integer
     */
    public function getF050fisAwarded()
    {
        return $this->f050fisAwarded;
    }

    /**
     * Set f050fidTendor
     *
     * @param integer $f050fidTendor
     *
     * @return T050ftenderSubmission
     */
    public function setF050fidTendor($f050fidTendor)
    {
        $this->f050fidTendor = $f050fidTendor;

        return $this;
    }

    /**
     * Get f050fidTendor
     *
     * @return integer
     */
    public function getF050fidTendor()
    {
        return $this->f050fidTendor;
    }

    /**
     * Set f050fcreatedBy
     *
     * @param integer $f050fcreatedBy
     *
     * @return T050ftenderSubmission
     */
    public function setF050fcreatedBy($f050fcreatedBy)
    {
        $this->f050fcreatedBy = $f050fcreatedBy;

        return $this;
    }

    /**
     * Get f050fcreatedBy
     *
     * @return integer
     */
    public function getF050fcreatedBy()
    {
        return $this->f050fcreatedBy;
    }

    /**
     * Set f050fupdatedBy
     *
     * @param integer $f050fupdatedBy
     *
     * @return T050ftenderSubmission
     */
    public function setF050fupdatedBy($f050fupdatedBy)
    {
        $this->f050fupdatedBy = $f050fupdatedBy;

        return $this;
    }

    /**
     * Get f050fupdatedBy
     *
     * @return integer
     */
    public function getF050fupdatedBy()
    {
        return $this->f050fupdatedBy;
    }

    /**
     * Set f050fcreatedDtTm
     *
     * @param \DateTime $f050fcreatedDtTm
     *
     * @return T050ftenderSubmission
     */
    public function setF050fcreatedDtTm($f050fcreatedDtTm)
    {
        $this->f050fcreatedDtTm = $f050fcreatedDtTm;

        return $this;
    }

    /**
     * Get f050fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF050fcreatedDtTm()
    {
        return $this->f050fcreatedDtTm;
    }

    /**
     * Set f050fupdatedDtTm
     *
     * @param \DateTime $f050fupdatedDtTm
     *
     * @return T050ftenderSubmission
     */
    public function setF050fupdatedDtTm($f050fupdatedDtTm)
    {
        $this->f050fupdatedDtTm = $f050fupdatedDtTm;

        return $this;
    }

    /**
     * Get f050fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF050fupdatedDtTm()
    {
        return $this->f050fupdatedDtTm;
    }

    /**
     * Set f050fstatus
     *
     * @param integer $f050fstatus
     *
     * @return T050ftenderSubmission
     */
    public function setF050fstatus($f050fstatus)
    {
        $this->f050fstatus = $f050fstatus;

        return $this;
    }

    /**
     * Get f050fstatus
     *
     * @return integer
     */
    public function getF050fstatus()
    {
        return $this->f050fstatus;
    }

    /**
     * Set f050fvendorName
     *
     * @param string $f050fvendorName
     *
     * @return T050ftenderSubmission
     */
    public function setF050fvendorName($f050fvendorName)
    {
        $this->f050fvendorName = $f050fvendorName;

        return $this;
    }

    /**
     * Get f050fvendorName
     *
     * @return string
     */
    public function getF050fvendorName()
    {
        return $this->f050fvendorName;
    }
}

