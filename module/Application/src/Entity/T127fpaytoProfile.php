<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T127fpaytoProfile
 *
 * @ORM\Table(name="t127fpayto_profile")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T127fpaytoProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f127fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f127fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f127fname", type="string", length=60, nullable=true)
     */
    private $f127fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f127fbank", type="integer", nullable=true)
     */
    private $f127fbank;

    /**
     * @var string
     *
     * @ORM\Column(name="f127faccount_no", type="string", length=50, nullable=true)
     */
    private $f127faccountNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f127fid_vendor", type="integer", nullable=true)
     */
    private $f127fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f127fstatus", type="integer", nullable=true)
     */
    private $f127fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f127fcreated_by", type="integer", nullable=true)
     */
    private $f127fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f127fupdated_by", type="integer", nullable=true)
     */
    private $f127fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f127fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f127fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f127fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f127fupdatedDtTm;


    /**
     * Get f127fid
     *
     * @return integer
     */
    public function getF127fid()
    {
        return $this->f127fid;
    }

    /**
     * Set f127fname
     *
     * @param string $f127fname
     *
     * @return T127fpaytoProfile
     */
    public function setF127fname($f127fname)
    {
        $this->f127fname = $f127fname;

        return $this;
    }

    /**
     * Get f127fname
     *
     * @return string
     */
    public function getF127fname()
    {
        return $this->f127fname;
    }

    /**
     * Set f127fbank
     *
     * @param integer $f127fbank
     *
     * @return T127fpaytoProfile
     */
    public function setF127fbank($f127fbank)
    {
        $this->f127fbank = $f127fbank;

        return $this;
    }

    /**
     * Get f127fbank
     *
     * @return integer
     */
    public function getF127fbank()
    {
        return $this->f127fbank;
    }

    /**
     * Set f127faccountNo
     *
     * @param string $f127faccountNo
     *
     * @return T127fpaytoProfile
     */
    public function setF127faccountNo($f127faccountNo)
    {
        $this->f127faccountNo = $f127faccountNo;

        return $this;
    }

    /**
     * Get f127faccountNo
     *
     * @return string
     */
    public function getF127faccountNo()
    {
        return $this->f127faccountNo;
    }

    /**
     * Set f127fidVendor
     *
     * @param integer $f127fidVendor
     *
     * @return T127fpaytoProfile
     */
    public function setF127fidVendor($f127fidVendor)
    {
        $this->f127fidVendor = $f127fidVendor;

        return $this;
    }

    /**
     * Get f127fidVendor
     *
     * @return integer
     */
    public function getF127fidVendor()
    {
        return $this->f127fidVendor;
    }


    /**
     * Set f127fstatus
     *
     * @param integer $f127fstatus
     *
     * @return T127fpaytoProfile
     */
    public function setF127fstatus($f127fstatus)
    {
        $this->f127fstatus = $f127fstatus;

        return $this;
    }

    /**
     * Get f127fstatus
     *
     * @return integer
     */
    public function getF127fstatus()
    {
        return $this->f127fstatus;
    }

    /**
     * Set f127fcreatedBy
     *
     * @param integer $f127fcreatedBy
     *
     * @return T127fpaytoProfile
     */
    public function setF127fcreatedBy($f127fcreatedBy)
    {
        $this->f127fcreatedBy = $f127fcreatedBy;

        return $this;
    }

    /**
     * Get f127fcreatedBy
     *
     * @return integer
     */
    public function getF127fcreatedBy()
    {
        return $this->f127fcreatedBy;
    }

    /**
     * Set f127fupdatedBy
     *
     * @param integer $f127fupdatedBy
     *
     * @return T127fpaytoProfile
     */
    public function setF127fupdatedBy($f127fupdatedBy)
    {
        $this->f127fupdatedBy = $f127fupdatedBy;

        return $this;
    }

    /**
     * Get f127fupdatedBy
     *
     * @return integer
     */
    public function getF127fupdatedBy()
    {
        return $this->f127fupdatedBy;
    }

    /**
     * Set f127fcreatedDtTm
     *
     * @param \DateTime $f127fcreatedDtTm
     *
     * @return T127fpaytoProfile
     */
    public function setF127fcreatedDtTm($f127fcreatedDtTm)
    {
        $this->f127fcreatedDtTm = $f127fcreatedDtTm;

        return $this;
    }

    /**
     * Get f127fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF127fcreatedDtTm()
    {
        return $this->f127fcreatedDtTm;
    }

    /**
     * Set f127fupdatedDtTm
     *
     * @param \DateTime $f127fupdatedDtTm
     *
     * @return T127fpaytoProfile
     */
    public function setF127fupdatedDtTm($f127fupdatedDtTm)
    {
        $this->f127fupdatedDtTm = $f127fupdatedDtTm;

        return $this;
    }

    /**
     * Get f127fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF127fupdatedDtTm()
    {
        return $this->f127fupdatedDtTm;
    }
}
