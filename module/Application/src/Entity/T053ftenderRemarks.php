<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T053ftenderRemarks
 *
 * @ORM\Table(name="t053ftender_remarks", indexes={@ORM\Index(name="f053fid_submission", columns={"f053fid_submission"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSubmissionRepository")
 */
class T053ftenderRemarks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f053fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f053fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fremarks", type="string",length=100, nullable=false)
     */
    private $f053fremarks;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fstatus", type="integer", nullable=false)
     */
    private $f053fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fid_tendor", type="integer", nullable=false)
     */
    private $f053fidTendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fcreated_by", type="integer", nullable=false)
     */
    private $f053fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f053fupdated_by", type="integer", nullable=false)
     */
    private $f053fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f053fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f053fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f053fupdatedDtTm;

    /**
     * @var \Application\Entity\T050ftenderSubmission
     *
     * @ORM\Column(name="f053fid_submission", type="integer", nullable=true)
     
     */
    private $f053fidSubmission;

     /**
     * Get f053fid
     *
     * @return integer
     */
    public function getF053fid()
    {
        return $this->f053fid;
    }

    /**
     * Set f053fremarks
     *
     * @param integer $f053fremarks
     *
     * @return T053ftenderStaff
     */
    public function setF053fremarks($f053fremarks)
    {
        $this->f053fremarks = $f053fremarks;

        return $this;
    }

    /**
     * Get f053fremarks
     *
     * @return integer
     */
    public function getF053fremarks()
    {
        return $this->f053fremarks;
    }

   
    /**
     * Set f053fstatus
     *
     * @param integer $f053fstatus
     *
     * @return T053ftenderStaff
     */
    public function setF053fstatus($f053fstatus)
    {
        $this->f053fstatus = $f053fstatus;

        return $this;
    }

    /**
     * Get f053fstatus
     *
     * @return integer
     */
    public function getF053fstatus()
    {
        return $this->f053fstatus;
    }

    /**
     * Set f053fcreatedBy
     *
     * @param integer $f053fcreatedBy
     *
     * @return T053ftenderStaff
     */
    public function setF053fcreatedBy($f053fcreatedBy)
    {
        $this->f053fcreatedBy = $f053fcreatedBy;

        return $this;
    }

    /**
     * Get f053fcreatedBy
     *
     * @return integer
     */
    public function getF053fcreatedBy()
    {
        return $this->f053fcreatedBy;
    }

    /**
     * Set f053fupdatedBy
     *
     * @param integer $f053fupdatedBy
     *
     * @return T053ftenderStaff
     */
    public function setF053fupdatedBy($f053fupdatedBy)
    {
        $this->f053fupdatedBy = $f053fupdatedBy;

        return $this;
    }

    /**
     * Get f053fupdatedBy
     *
     * @return integer
     */
    public function getF053fupdatedBy()
    {
        return $this->f053fupdatedBy;
    }

    /**
     * Set f053fcreatedDtTm
     *
     * @param \DateTime $f053fcreatedDtTm
     *
     * @return T053ftenderStaff
     */
    public function setF053fcreatedDtTm($f053fcreatedDtTm)
    {
        $this->f053fcreatedDtTm = $f053fcreatedDtTm;

        return $this;
    }

    /**
     * Get f053fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF053fcreatedDtTm()
    {
        return $this->f053fcreatedDtTm;
    }

    /**
     * Set f053fupdatedDtTm
     *
     * @param \DateTime $f053fupdatedDtTm
     *
     * @return T053ftenderStaff
     */
    public function setF053fupdatedDtTm($f053fupdatedDtTm)
    {
        $this->f053fupdatedDtTm = $f053fupdatedDtTm;

        return $this;
    }

    /**
     * Get f053fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF053fupdatedDtTm()
    {
        return $this->f053fupdatedDtTm;
    }

    /**
     * Set f053fidSubmission
     *
     * @param \Application\Entity\T050ftenderSubmission $f053fidSubmission
     *
     * @return T053ftenderStaff
     */
    public function setF053fidSubmission( $f053fidSubmission = null)
    {
        $this->f053fidSubmission = $f053fidSubmission;

        return $this;
    }

    /**
     * Get f053fidSubmission
     *
     * @return \Application\Entity\T050ftenderSubmission
     */
    public function getF053fidSubmission()
    {
        return $this->f053fidSubmission;
    }

    /**
     * Set f053fidTendor
     *
     * @param integer $f053fidTendor
     *
     * @return T053ftenderStaff
     */
    public function setF053fidTendor( $f053fidTendor = null)
    {
        $this->f053fidTendor = $f053fidTendor;

        return $this;
    }

    /**
     * Get f053fidTendor
     *
     * @return integer
     */
    public function getF053fidTendor()
    {
        return $this->f053fidTendor;
    }

}

