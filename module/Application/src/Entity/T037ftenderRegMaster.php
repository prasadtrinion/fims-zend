<?php 
 
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T037ftenderRegMaster
 *
 * @ORM\Table(name="t037ftender_reg_master")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderRegistrationRepository")
 */
class T037ftenderRegMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f037fid;

    
    /**
     * @var string
     *
     * @ORM\Column(name="f037fid_vendor", type="integer", nullable=false)
     */
    private $f037fidVendor;

   
    /**
     * @var integer
     *
     * @ORM\Column(name="f037fstatus", type="integer", nullable=false)
     */
    private $f037fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fshortlisted", type="integer", nullable=false)
     */
    private $f037fshortlisted;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fawarded", type="integer", nullable=false)
     */
    private $f037fawarded;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fcreated_by", type="integer", nullable=false)
     */
    private $f037fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f037fupdated_by", type="integer", nullable=false)
     */
    private $f037fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f037fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f037fupdatedDtTm;

     /**
     * @var integer
     *
     * @ORM\Column(name="f037fid_tender", type="integer", nullable=false)
     */
    private $f037fidTender;

    /**
     * Get f037fid
     *
     * @return integer
     */
    public function getF037fid()
    {
        return $this->f037fid;
    }



    /**
     * Set f037fidVendor
     *
     * @param string $f037fidVendor
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fidVendor($f037fidVendor)
    {
        $this->f037fidVendor = $f037fidVendor;

        return $this;
    }

    /**
     * Get f037fidVendor
     *
     * @return string
     */
    public function getF037fidVendor()
    {
        return $this->f037fidVendor;
    }

     /**
     * Set f037fshortlisted
     *
     * @param string $f037fshortlisted
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fshortlisted($f037fshortlisted)
    {
        $this->f037fshortlisted = $f037fshortlisted;

        return $this;
    }

    /**
     * Get f037fshortlisted
     *
     * @return string
     */
    public function getF037fshortlisted()
    {
        return $this->f037fshortlisted;
    }

   
    /**
     * Set f037fstatus
     *
     * @param integer $f037fstatus
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fstatus($f037fstatus)
    {
        $this->f037fstatus = $f037fstatus;

        return $this;
    }

    /**
     * Get f037fstatus
     *
     * @return integer
     */
    public function getF037fstatus()
    {
        return $this->f037fstatus;
    }

    /**
     * Set f037fcreatedBy
     *
     * @param integer $f037fcreatedBy
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fcreatedBy($f037fcreatedBy)
    {
        $this->f037fcreatedBy = $f037fcreatedBy;

        return $this;
    }

    /**
     * Get f037fcreatedBy
     *
     * @return integer
     */
    public function getF037fcreatedBy()
    {
        return $this->f037fcreatedBy;
    }

    /**
     * Set f037fupdatedBy
     *
     * @param integer $f037fupdatedBy
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fupdatedBy($f037fupdatedBy)
    {
        $this->f037fupdatedBy = $f037fupdatedBy;

        return $this;
    }

    /**
     * Get f037fupdatedBy
     *
     * @return integer
     */
    public function getF037fupdatedBy()
    {
        return $this->f037fupdatedBy;
    }

    /**
     * Set f037fcreatedDtTm
     *
     * @param \DateTime $f037fcreatedDtTm
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fcreatedDtTm($f037fcreatedDtTm)
    {
        $this->f037fcreatedDtTm = $f037fcreatedDtTm;

        return $this;
    }

    /**
     * Get f037fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF037fcreatedDtTm()
    {
        return $this->f037fcreatedDtTm;
    }

    /**
     * Set f037fupdatedDtTm
     *
     * @param \DateTime $f037fupdatedDtTm
     *
     * @return T037ftenderRegMaster
     */
    public function setF037fupdatedDtTm($f037fupdatedDtTm)
    {
        $this->f037fupdatedDtTm = $f037fupdatedDtTm;

        return $this;
    }

    /**
     * Get f037fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF037fupdatedDtTm()
    {
        return $this->f037fupdatedDtTm;
    }

    /**
     * Set f037fidTender
     *
     * @param integer $f037fidTender
     *
     * @return T037ftenderSpec
     */
    public function setF037fidTender($f037fidTender)
    {
        $this->f037fidTender = $f037fidTender;

        return $this;
    }

/**
     * Get f037fidTender
     *
     * @return integer
     */
    public function getF037fidTender()
    {
        return $this->f037fidTender;
    }
    /**
     * Get f037fawarded
     *
     * @return integer
     */
    public function getF037fawarded()
    {
        return $this->f037fawarded;
    }

    /**
     * Set f037fawarded
     *
     * @param integer $f037fawarded
     *
     * @return T037ftenderSpec
     */
    public function setF037fawarded($f037fawarded)
    {
        $this->f037fawarded = $f037fawarded;

        return $this;
    }

    
}

