<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T104femployeeDetails
 *
 * @ORM\Table(name="t104femployee_details")
 * @ORM\Entity(repositoryClass="Application\Repository\SalaryRepository")
 */
class T104femployeeDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="employeeid", type="integer", nullable=false)
     */
    private $employeeid;

    /**
     * @var string
     *
     * @ORM\Column(name="employeefirstname", type="string", length=50, nullable=true)
     */
    private $employeefirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="employeemiddlename", type="string", length=1, nullable=false)
     */
    private $employeemiddlename;

    /**
     * @var string
     *
     * @ORM\Column(name="employeelastname", type="string", length=1, nullable=false)
     */
    private $employeelastname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="employeedateofjoining", type="datetime", nullable=true)
     */
    private $employeedateofjoining;

    /**
     * @var string
     *
     * @ORM\Column(name="employeetype", type="string", length=1, nullable=true)
     */
    private $employeetype;

    /**
     * @var string
     *
     * @ORM\Column(name="employeetypedesc", type="string", length=14, nullable=false)
     */
    private $employeetypedesc;

    /**
     * @var string
     *
     * @ORM\Column(name="employeecategory", type="string", length=2, nullable=true)
     */
    private $employeecategory;

    /**
     * @var string
     *
     * @ORM\Column(name="employeecategorydesc", type="string", length=100, nullable=true)
     */
    private $employeecategorydesc;

    /**
     * @var string
     *
     * @ORM\Column(name="employeedepartment", type="string", length=5, nullable=true)
     */
    private $employeedepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="employeedesignation", type="smallint", nullable=true)
     */
    private $employeedesignation;

    /**
     * @var string
     *
     * @ORM\Column(name="employeedesignationname", type="string", length=100, nullable=true)
     */
    private $employeedesignationname;

    /**
     * @var string
     *
     * @ORM\Column(name="employeegradeid", type="string", length=7, nullable=true)
     */
    private $employeegradeid;

    /**
     * @var string
     *
     * @ORM\Column(name="employeegradename", type="string", length=40, nullable=true)
     */
    private $employeegradename;

    /**
     * @var string
     *
     * @ORM\Column(name="employeepaymenttype", type="string", length=7, nullable=false)
     */
    private $employeepaymenttype;

    /**
     * @var string
     *
     * @ORM\Column(name="employeeaccountbank", type="string", length=10, nullable=true)
     */
    private $employeeaccountbank;

    /**
     * @var string
     *
     * @ORM\Column(name="employeeaccountnumber", type="string", length=50, nullable=true)
     */
    private $employeeaccountnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="employeetaxstatus", type="string", length=1, nullable=false)
     */
    private $employeetaxstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="taxmaritalstatus", type="string", length=1, nullable=true)
     */
    private $taxmaritalstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="spouseworking", type="string", length=1, nullable=true)
     */
    private $spouseworking;

    /**
     * @var string
     *
     * @ORM\Column(name="stafftype", type="string", length=1, nullable=true)
     */
    private $stafftype;

    /**
     * @var string
     *
     * @ORM\Column(name="stafftypedesc", type="string", length=100, nullable=true)
     */
    private $stafftypedesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateofbirth", type="datetime", nullable=true)
     */
    private $dateofbirth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endofservice", type="datetime", nullable=true)
     */
    private $endofservice;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmationstatus", type="string", length=1, nullable=true)
     */
    private $confirmationstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="idsalarycomponent", type="string", length=10, nullable=true)
     */
    private $idsalarycomponent;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=2, nullable=false)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=2, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="financialperiod", type="string", length=10, nullable=true)
     */
    private $financialperiod;

    /**
     * @var integer
     *
     * @ORM\Column(name="financialyear", type="integer", nullable=true)
     */
    private $financialyear;

    /**
     * @var string
     *
     * @ORM\Column(name="kodptganIFAS", type="string", length=3, nullable=false)
     */
    private $kodptganifas;

    /**
     * @var string
     *
     * @ORM\Column(name="employeaccounttype", type="string", length=50, nullable=true)
     */
    private $employeaccounttype;

    /**
     * @var integer
     *
     * @ORM\Column(name="employeecontribution", type="integer", nullable=false)
     */
    private $employeecontribution;

    /**
     * @var integer
     *
     * @ORM\Column(name="process_id", type="integer", nullable=true)
     */
    private $processId;


}

