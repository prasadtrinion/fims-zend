<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T039fcreditSetup
 *
 * @ORM\Table(name="t039fcredit_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\CreditSetupRepository")
 */
class T039fcreditSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f039fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f039fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f039fname", type="integer", nullable=true)
     */
    private $f039fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f039fid_glcode", type="integer", nullable=true)
     */
    private $f039fidGlcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f039fstatus", type="integer", nullable=false)
     */
    private $f039fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f039fcreated_by", type="integer", nullable=true)
     */
    private $f039fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f039fupdated_by", type="integer", nullable=true)
     */
    private $f039fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f039fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f039fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f039fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f039fupdatedDtTm;



    /**
     * Get f039fid
     *
     * @return integer
     */
    public function getF039fid()
    {
        return $this->f039fid;
    }

    /**
     * Set f039fname
     *
     * @param integer $f039fname
     *
     * @return T039fcreditSetup
     */
    public function setF039fname($f039fname)
    {
        $this->f039fname = $f039fname;

        return $this;
    }

    /**
     * Get f039fname
     *
     * @return integer
     */
    public function getF039fname()
    {
        return $this->f039fname;
    }

    /**
     * Set f039fidGlcode
     *
     * @param integer $f039fidGlcode
     *
     * @return T039fcreditSetup
     */
    public function setF039fidGlcode($f039fidGlcode)
    {
        $this->f039fidGlcode = $f039fidGlcode;

        return $this;
    }

    /**
     * Get f039fidGlcode
     *
     * @return integer
     */
    public function getF039fidGlcode()
    {
        return $this->f039fidGlcode;
    }

    /**
     * Set f039fstatus
     *
     * @param integer $f039fstatus
     *
     * @return T039fcreditSetup
     */
    public function setF039fstatus($f039fstatus)
    {
        $this->f039fstatus = $f039fstatus;

        return $this;
    }

    /**
     * Get f039fstatus
     *
     * @return integer
     */
    public function getF039fstatus()
    {
        return $this->f039fstatus;
    }

    /**
     * Set f039fcreatedBy
     *
     * @param integer $f039fcreatedBy
     *
     * @return T039fcreditSetup
     */
    public function setF039fcreatedBy($f039fcreatedBy)
    {
        $this->f039fcreatedBy = $f039fcreatedBy;

        return $this;
    }

    /**
     * Get f039fcreatedBy
     *
     * @return integer
     */
    public function getF039fcreatedBy()
    {
        return $this->f039fcreatedBy;
    }

    /**
     * Set f039fupdatedBy
     *
     * @param integer $f039fupdatedBy
     *
     * @return T039fcreditSetup
     */
    public function setF039fupdatedBy($f039fupdatedBy)
    {
        $this->f039fupdatedBy = $f039fupdatedBy;

        return $this;
    }

    /**
     * Get f039fupdatedBy
     *
     * @return integer
     */
    public function getF039fupdatedBy()
    {
        return $this->f039fupdatedBy;
    }

    /**
     * Set f039fcreatedDtTm
     *
     * @param \DateTime $f039fcreatedDtTm
     *
     * @return T039fcreditSetup
     */
    public function setF039fcreatedDtTm($f039fcreatedDtTm)
    {
        $this->f039fcreatedDtTm = $f039fcreatedDtTm;

        return $this;
    }

    /**
     * Get f039fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF039fcreatedDtTm()
    {
        return $this->f039fcreatedDtTm;
    }

    /**
     * Set f039fupdatedDtTm
     *
     * @param \DateTime $f039fupdatedDtTm
     *
     * @return T039fcreditSetup
     */
    public function setF039fupdatedDtTm($f039fupdatedDtTm)
    {
        $this->f039fupdatedDtTm = $f039fupdatedDtTm;

        return $this;
    }

    /**
     * Get f039fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF039fupdatedDtTm()
    {
        return $this->f039fupdatedDtTm;
    }
}
