<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T110fbudgetyear
 *
 * @ORM\Table(name="t110fbudgetyear", uniqueConstraints={@ORM\UniqueConstraint(name="f110fyear_UNIQUE", columns={"f110fyear"})}))
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetYearRepository")
 */
class T110fbudgetyear
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f110fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f110fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f110fyear", type="string", length=45, nullable=true)
     */
    private $f110fyear;

     /**
     * @var string
     *
     * @ORM\Column(name="f110fdescription", type="string", length=45, nullable=true)
     */
    private $f110fdescription;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="f110fstatus", type="integer", nullable=true)
     */
    private $f110fstatus = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f110fcreated_by", type="integer", length=50, nullable=true)
     */
    private $f110fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f110fupdated_by", type="integer", length=50, nullable=true)
     */
    private $f110fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f110fcreate_dt_tm", type="datetime", nullable=true)
     */
    private $f110fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f110fupdate_dt_tm", type="datetime", nullable=true)
     */
    private $f110fupdateDtTm;
    
    /**
     * Get f110fid
     *
     * @return integer
     */
    public function getF110fid()
    {
        return $this->f110fid;
    }

    /**
     * Set f110fyear
     *
     * @param string $f110fyear
     *
     * @return T110fbudgetyear
     */
    public function setF110fyear($f110fyear)
    {
        $this->f110fyear = $f110fyear;

        return $this;
    }

    /**
     * Get f110fyear
     *
     * @return string
     */
    public function getF110fyear()
    {
        return $this->f110fyear;
    }

    /**
     * Set f110fdescription
     *
     * @param string $f110fdescription
     *
     * @return T110fbudgetyear
     */
    public function setF110fdescription($f110fdescription)
    {
        $this->f110fdescription = $f110fdescription;

        return $this;
    }

    /**
     * Get f110fdescription
     *
     * @return string
     */
    public function getF110fdescription()
    {
        return $this->f110fdescription;
    }


    /**
     * Set f110fstatus
     *
     * @param integer $f110fstatus
     *
     * @return T110fbudgetyear
     */
    public function setF110fstatus($f110fstatus)
    {
        $this->f110fstatus = $f110fstatus;

        return $this;
    }

    /**
     * Get f110fstatus
     *
     * @return integer
     */
    public function getF110fstatus()
    {
        return $this->f110fstatus;
    }

    /**
     * Set f110fcreatedBy
     *
     * @param integer $f110fcreatedBy
     *
     * @return T110fbudgetyear
     */
    public function setF110fcreatedBy($f110fcreatedBy)
    {
        $this->f110fcreatedBy = $f110fcreatedBy;

        return $this;
    }

    /**
     * Get f110fcreatedBy
     *
     * @return integer
     */
    public function getF110fcreatedBy()
    {
        return $this->f110fcreatedBy;
    }

    /**
     * Set f110fupdatedBy
     *
     * @param integer $f110fupdatedBy
     *
     * @return T110fbudgetyear
     */
    public function setF110fupdatedBy($f110fupdatedBy)
    {
        $this->f110fupdatedBy = $f110fupdatedBy;

        return $this;
    }

    /**
     * Get f110fupdatedBy
     *
     * @return integer
     */
    public function getF110fupdatedBy()
    {
        return $this->f110fupdatedBy;
    }

    /**
     * Set f110fcreateDtTm
     *
     * @param \DateTime $f110fcreateDtTm
     *
     * @return T110fbudgetyear
     */
    public function setF110fcreateDtTm($f110fcreateDtTm)
    {
        $this->f110fcreateDtTm = $f110fcreateDtTm;

        return $this;
    }

    /**
     * Get f110fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF110fcreateDtTm()
    {
        return $this->f110fcreateDtTm;
    }

    /**
     * Set f110fupdateDtTm
     *
     * @param \DateTime $f110fupdateDtTm
     *
     * @return T110fbudgetyear
     */
    public function setF110fupdateDtTm($f110fupdateDtTm)
    {
        $this->f110fupdateDtTm = $f110fupdateDtTm;

        return $this;
    }

    /**
     * Get f110fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF110fupdateDtTm()
    {
        return $this->f110fupdateDtTm;
    }
}
