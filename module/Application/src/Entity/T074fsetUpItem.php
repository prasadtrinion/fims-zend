<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T074fsetUpItem
 *
 * @ORM\Table(name="t074fset_up_item")
 * @ORM\Entity
 */
class T074fsetUpItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f074fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f074fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f074frevenue_type_set_up_id", type="integer", nullable=false)
     */
    private $f074frevenueTypeSetUpId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f074fgst_applicable", type="boolean", nullable=false)
     */
    private $f074fgstApplicable;

    /**
     * @var string
     *
     * @ORM\Column(name="f074fgst_code", type="string", length=255, nullable=false)
     */
    private $f074fgstCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f074fstatus", type="boolean", nullable=false)
     */
    private $f074fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f074fcreated_by", type="integer", nullable=false)
     */
    private $f074fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f074fupdated_by", type="integer", nullable=false)
     */
    private $f074fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f074fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f074fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f074fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f074fupdatedDtTm;



    /**
     * Get f074fid
     *
     * @return integer
     */
    public function getF074fid()
    {
        return $this->f074fid;
    }

    /**
     * Set f074frevenueTypeSetUpId
     *
     * @param integer $f074frevenueTypeSetUpId
     *
     * @return T074fsetUpItem
     */
    public function setF074frevenueTypeSetUpId($f074frevenueTypeSetUpId)
    {
        $this->f074frevenueTypeSetUpId = $f074frevenueTypeSetUpId;

        return $this;
    }

    /**
     * Get f074frevenueTypeSetUpId
     *
     * @return integer
     */
    public function getF074frevenueTypeSetUpId()
    {
        return $this->f074frevenueTypeSetUpId;
    }

    /**
     * Set f074fgstApplicable
     *
     * @param boolean $f074fgstApplicable
     *
     * @return T074fsetUpItem
     */
    public function setF074fgstApplicable($f074fgstApplicable)
    {
        $this->f074fgstApplicable = $f074fgstApplicable;

        return $this;
    }

    /**
     * Get f074fgstApplicable
     *
     * @return boolean
     */
    public function getF074fgstApplicable()
    {
        return $this->f074fgstApplicable;
    }

    /**
     * Set f074fgstCode
     *
     * @param string $f074fgstCode
     *
     * @return T074fsetUpItem
     */
    public function setF074fgstCode($f074fgstCode)
    {
        $this->f074fgstCode = $f074fgstCode;

        return $this;
    }

    /**
     * Get f074fgstCode
     *
     * @return string
     */
    public function getF074fgstCode()
    {
        return $this->f074fgstCode;
    }

    /**
     * Set f074fstatus
     *
     * @param boolean $f074fstatus
     *
     * @return T074fsetUpItem
     */
    public function setF074fstatus($f074fstatus)
    {
        $this->f074fstatus = $f074fstatus;

        return $this;
    }

    /**
     * Get f074fstatus
     *
     * @return boolean
     */
    public function getF074fstatus()
    {
        return $this->f074fstatus;
    }

    /**
     * Set f074fcreatedBy
     *
     * @param integer $f074fcreatedBy
     *
     * @return T074fsetUpItem
     */
    public function setF074fcreatedBy($f074fcreatedBy)
    {
        $this->f074fcreatedBy = $f074fcreatedBy;

        return $this;
    }

    /**
     * Get f074fcreatedBy
     *
     * @return integer
     */
    public function getF074fcreatedBy()
    {
        return $this->f074fcreatedBy;
    }

    /**
     * Set f074fupdatedBy
     *
     * @param integer $f074fupdatedBy
     *
     * @return T074fsetUpItem
     */
    public function setF074fupdatedBy($f074fupdatedBy)
    {
        $this->f074fupdatedBy = $f074fupdatedBy;

        return $this;
    }

    /**
     * Get f074fupdatedBy
     *
     * @return integer
     */
    public function getF074fupdatedBy()
    {
        return $this->f074fupdatedBy;
    }

    /**
     * Set f074fcreatedDtTm
     *
     * @param \DateTime $f074fcreatedDtTm
     *
     * @return T074fsetUpItem
     */
    public function setF074fcreatedDtTm($f074fcreatedDtTm)
    {
        $this->f074fcreatedDtTm = $f074fcreatedDtTm;

        return $this;
    }

    /**
     * Get f074fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF074fcreatedDtTm()
    {
        return $this->f074fcreatedDtTm;
    }

    /**
     * Set f074fupdatedDtTm
     *
     * @param \DateTime $f074fupdatedDtTm
     *
     * @return T074fsetUpItem
     */
    public function setF074fupdatedDtTm($f074fupdatedDtTm)
    {
        $this->f074fupdatedDtTm = $f074fupdatedDtTm;

        return $this;
    }

    /**
     * Get f074fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF074fupdatedDtTm()
    {
        return $this->f074fupdatedDtTm;
    }
}
