<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T062fpayrollSchedule
 *
 * @ORM\Table(name="t062fpayroll_schedule")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollScheduleRepository")
 */
class T062fpayrollSchedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f062fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f062fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fp_start", type="string", length=25, nullable=true)
     */
    private $f062fpStart;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fp_end", type="string", length=25, nullable=true)
     */
    private $f062fpEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fm", type="string", length=25, nullable=true)
     */
    private $f062fm;

     /**
     * @var string
     *
     * @ORM\Column(name="f062fr", type="string", length=25, nullable=true)
     */
    private $f062fr;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fc1", type="string", length=25, nullable=true)
     */
    private $f062fc1;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fc2", type="string", length=25, nullable=true)
     */
    private $f062fc2;

    /**
     * @var string
     *
     * @ORM\Column(name="f062fc3", type="string", length=25, nullable=true)
     */
    private $f062fc3;


     /**
     * @var integer
     *
     * @ORM\Column(name="f062fstatus", type="integer", nullable=true)
     */
    private $f062fstatus ;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fcreated_by", type="integer", nullable=true)
     */
    private $f062fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f062fupdated_by", type="integer", nullable=true)
     */
    private $f062fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f062fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f062fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f062fupdatedDtTm;



    /**
     * Get f062fid
     *
     * @return integer
     */
    public function getF062fid()
    {
        return $this->f062fid;
    }

    /**
     * Set f062fpStart
     *
     * @param string $f062fpStart
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fpStart($f062fpStart)
    {
        $this->f062fpStart = $f062fpStart;

        return $this;
    }

    /**
     * Get f062fpStart
     *
     * @return string
     */
    public function getF062fpStart()
    {
        return $this->f062fpStart;
    }

    /**
     * Set f062fpEnd
     *
     * @param string $f062fpEnd
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fpEnd($f062fpEnd)
    {
        $this->f062fpEnd = $f062fpEnd;

        return $this;
    }

    /**
     * Get f062fpEnd
     *
     * @return string
     */
    public function getF062fpEnd()
    {
        return $this->f062fpEnd;
    }

    /**
     * Set f062fm
     *
     * @param string $f062fm
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fm($f062fm)
    {
        $this->f062fm = $f062fm;

        return $this;
    }

    /**
     * Get f062fm
     *
     * @return string
     */
    public function getF062fm()
    {
        return $this->f062fm;
    }

    /**
     * Set f062fr
     *
     * @param string $f062fr
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fr($f062fr)
    {
        $this->f062fr = $f062fr;

        return $this;
    }

    /**
     * Get f062fr
     *
     * @return string
     */
    public function getF062fr()
    {
        return $this->f062fr;
    }

    /**
     * Set f062fc1
     *
     * @param string $f062fc1
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fc1($f062fc1)
    {
        $this->f062fc1 = $f062fc1;

        return $this;
    }

    /**
     * Get f062fc1
     *
     * @return string
     */
    public function getF062fc1()
    {
        return $this->f062fc1;
    }

    /**
     * Set f062fc2
     *
     * @param string $f062fc2
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fc2($f062fc2)
    {
        $this->f062fc2 = $f062fc2;

        return $this;
    }

    /**
     * Get f062fc2
     *
     * @return string
     */
    public function getF062fc2()
    {
        return $this->f062fc2;
    }

    /**
     * Set f062fc3
     *
     * @param string $f062fc3
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fc3($f062fc3)
    {
        $this->f062fc3 = $f062fc3;

        return $this;
    }

    /**
     * Get f062fc3
     *
     * @return string
     */
    public function getF062fc3()
    {
        return $this->f062fc3;
    }

     /**
     * Set f062fstatus
     *
     * @param boolean $f062fstatus
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fstatus($f062fstatus)
    {
        $this->f062fstatus = $f062fstatus;

        return $this;
    }

    /**
     * Get f062fstatus
     *
     * @return boolean
     */
    public function getF062fstatus()
    {
        return $this->f062fstatus;
    }

    /**
     * Set f062fcreatedBy
     *
     * @param integer $f062fcreatedBy
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fcreatedBy($f062fcreatedBy)
    {
        $this->f062fcreatedBy = $f062fcreatedBy;

        return $this;
    }

    /**
     * Get f062fcreatedBy
     *
     * @return integer
     */
    public function getF062fcreatedBy()
    {
        return $this->f062fcreatedBy;
    }

    /**
     * Set f062fupdatedBy
     *
     * @param integer $f062fupdatedBy
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fupdatedBy($f062fupdatedBy)
    {
        $this->f062fupdatedBy = $f062fupdatedBy;

        return $this;
    }

    /**
     * Get f062fupdatedBy
     *
     * @return integer
     */
    public function getF062fupdatedBy()
    {
        return $this->f062fupdatedBy;
    }

    /**
     * Set f062fcreatedDtTm
     *
     * @param \DateTime $f062fcreatedDtTm
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fcreatedDtTm($f062fcreatedDtTm)
    {
        $this->f062fcreatedDtTm = $f062fcreatedDtTm;

        return $this;
    }

    /**
     * Get f062fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fcreatedDtTm()
    {
        return $this->f062fcreatedDtTm;
    }

    /**
     * Set f062fupdatedDtTm
     *
     * @param \DateTime $f062fupdatedDtTm
     *
     * @return T062fpayrollSchedule
     */
    public function setF062fupdatedDtTm($f062fupdatedDtTm)
    {
        $this->f062fupdatedDtTm = $f062fupdatedDtTm;

        return $this;
    }

    /**
     * Get f062fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF062fupdatedDtTm()
    {
        return $this->f062fupdatedDtTm;
    }

    
}
