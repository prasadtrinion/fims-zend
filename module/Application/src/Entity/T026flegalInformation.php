<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T026flegalInformation
 *
 * @ORM\Table(name="t026flegal_information")
 * @ORM\Entity(repositoryClass="Application\Repository\LegalInformationRepository")
 */
class T026flegalInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f026fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f026fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f026fname", type="string", length=100, nullable=true)
     */
    private $f026fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f026fphone", type="string", length=20, nullable=true)
     */
    private $f026fphone;

    /**
     * @var string
     *
     * @ORM\Column(name="f026femail", type="string", length=20, nullable=true)
     */
    private $f026femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f026ffax", type="string", length=20, nullable=true)
     */
    private $f026ffax;

    /**
     * @var string
     *
     * @ORM\Column(name="f026address", type="string", length=100, nullable=true)
     */
    private $f026address;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fstatus", type="integer", nullable=true)
     */
    private $f026fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fcreated_by", type="integer", nullable=true)
     */
    private $f026fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fupdated_by", type="integer", nullable=true)
     */
    private $f026fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f026fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f026fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f026fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f026fupdatedDtTm;



    /**
     * Get f026fid
     *
     * @return integer
     */
    public function getF026fid()
    {
        return $this->f026fid;
    }

    /**
     * Set f026fname
     *
     * @param string $f026fname
     *
     * @return T026flegalInformation
     */
    public function setF026fname($f026fname)
    {
        $this->f026fname = $f026fname;

        return $this;
    }

    /**
     * Get f026fname
     *
     * @return string
     */
    public function getF026fname()
    {
        return $this->f026fname;
    }

    /**
     * Set f026fphone
     *
     * @param string $f026fphone
     *
     * @return T026flegalInformation
     */
    public function setF026fphone($f026fphone)
    {
        $this->f026fphone = $f026fphone;

        return $this;
    }

    /**
     * Get f026fphone
     *
     * @return string
     */
    public function getF026fphone()
    {
        return $this->f026fphone;
    }

    /**
     * Set f026femail
     *
     * @param string $f026femail
     *
     * @return T026flegalInformation
     */
    public function setF026femail($f026femail)
    {
        $this->f026femail = $f026femail;

        return $this;
    }

    /**
     * Get f026femail
     *
     * @return string
     */
    public function getF026femail()
    {
        return $this->f026femail;
    }

    /**
     * Set f026ffax
     *
     * @param string $f026ffax
     *
     * @return T026flegalInformation
     */
    public function setF026ffax($f026ffax)
    {
        $this->f026ffax = $f026ffax;

        return $this;
    }

    /**
     * Get f026ffax
     *
     * @return string
     */
    public function getF026ffax()
    {
        return $this->f026ffax;
    }

    /**
     * Set f026address
     *
     * @param string $f026address
     *
     * @return T026flegalInformation
     */
    public function setF026address($f026address)
    {
        $this->f026address = $f026address;

        return $this;
    }

    /**
     * Get f026address
     *
     * @return string
     */
    public function getF026address()
    {
        return $this->f026address;
    }

    /**
     * Set f026fstatus
     *
     * @param integer $f026fstatus
     *
     * @return T026flegalInformation
     */
    public function setF026fstatus($f026fstatus)
    {
        $this->f026fstatus = $f026fstatus;

        return $this;
    }

    /**
     * Get f026fstatus
     *
     * @return integer
     */
    public function getF026fstatus()
    {
        return $this->f026fstatus;
    }

    /**
     * Set f026fcreatedBy
     *
     * @param integer $f026fcreatedBy
     *
     * @return T026flegalInformation
     */
    public function setF026fcreatedBy($f026fcreatedBy)
    {
        $this->f026fcreatedBy = $f026fcreatedBy;

        return $this;
    }

    /**
     * Get f026fcreatedBy
     *
     * @return integer
     */
    public function getF026fcreatedBy()
    {
        return $this->f026fcreatedBy;
    }

    /**
     * Set f026fupdatedBy
     *
     * @param integer $f026fupdatedBy
     *
     * @return T026flegalInformation
     */
    public function setF026fupdatedBy($f026fupdatedBy)
    {
        $this->f026fupdatedBy = $f026fupdatedBy;

        return $this;
    }

    /**
     * Get f026fupdatedBy
     *
     * @return integer
     */
    public function getF026fupdatedBy()
    {
        return $this->f026fupdatedBy;
    }

    /**
     * Set f026fcreatedDtTm
     *
     * @param \DateTime $f026fcreatedDtTm
     *
     * @return T026flegalInformation
     */
    public function setF026fcreatedDtTm($f026fcreatedDtTm)
    {
        $this->f026fcreatedDtTm = $f026fcreatedDtTm;

        return $this;
    }

    /**
     * Get f026fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF026fcreatedDtTm()
    {
        return $this->f026fcreatedDtTm;
    }

    /**
     * Set f026fupdatedDtTm
     *
     * @param \DateTime $f026fupdatedDtTm
     *
     * @return T026flegalInformation
     */
    public function setF026fupdatedDtTm($f026fupdatedDtTm)
    {
        $this->f026fupdatedDtTm = $f026fupdatedDtTm;

        return $this;
    }

    /**
     * Get f026fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF026fupdatedDtTm()
    {
        return $this->f026fupdatedDtTm;
    }
}
