<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T021frole
 *
 * @ORM\Table(name="t021frole",uniqueConstraints={@ORM\UniqueConstraint(name="f021frole_name_UNIQUE", columns={"f021frole_name"})}, indexes={@ORM\Index(name="f021fupdated_by", columns={"f021fupdated_by"}), @ORM\Index(name="f021fcreated_by", columns={"f021fcreated_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\RoleRepository")
 */
class T021frole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f021fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f021frole_name", type="string", length=50, nullable=false)
     */
    private $f021froleName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f021fstatus", type="boolean", nullable=false)
     */
    private $f021fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f021fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f021fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f021fcreatedBy;


    /**
     * Get f021fid
     *
     * @return integer
     */
    public function getF021fid()
    {
        return $this->f021fid;
    }

    /**
     * Set f021froleName
     *
     * @param string $f021froleName
     *
     * @return T021frole
     */
    public function setF021froleName($f021froleName)
    {
        $this->f021froleName = $f021froleName;

        return $this;
    }

    /**
     * Get f021froleName
     *
     * @return string
     */
    public function getF021froleName()
    {
        return $this->f021froleName;
    }

    /**
     * Set f021fstatus
     *
     * @param boolean $f021fstatus
     *
     * @return T021frole
     */
    public function setF021fstatus($f021fstatus)
    {
        $this->f021fstatus = $f021fstatus;

        return $this;
    }

    /**
     * Get f021fstatus
     *
     * @return boolean
     */
    public function getF021fstatus()
    {
        return $this->f021fstatus;
    }

    /**
     * Set f021fcreatedDtTm
     *
     * @param \DateTime $f021fcreatedDtTm
     *
     * @return T021frole
     */
    public function setF021fcreatedDtTm($f021fcreatedDtTm)
    {
        $this->f021fcreatedDtTm = $f021fcreatedDtTm;

        return $this;
    }

    /**
     * Get f021fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fcreatedDtTm()
    {
        return $this->f021fcreatedDtTm;
    }

    /**
     * Set f021fupdatedDtTm
     *
     * @param \DateTime $f021fupdatedDtTm
     *
     * @return T021frole
     */
    public function setF021fupdatedDtTm($f021fupdatedDtTm)
    {
        $this->f021fupdatedDtTm = $f021fupdatedDtTm;

        return $this;
    }

    /**
     * Get f021fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fupdatedDtTm()
    {
        return $this->f021fupdatedDtTm;
    }

    /**
     * Set f021fupdatedBy
     *
     * @param integer $f021fupdatedBy
     *
     * @return T021frole
     */
    public function setF021fupdatedBy($f021fupdatedBy)
    {
        $this->f021fupdatedBy = $f021fupdatedBy;

        return $this;
    }

    
    public function getF021fupdatedBy()
    {
        return $this->f021fupdatedBy;
    }

    /**
     * Set f021fcreatedBy
     *
     * @param  integer $f021fcreatedBy
     *
     * @return T021frole
     */
    public function setF021fcreatedBy($f021fcreatedBy)
    {
        $this->f021fcreatedBy = $f021fcreatedBy;

        return $this;
    }

    
    public function getF021fcreatedBy()
    {
        return $this->f021fcreatedBy;
    }
}
