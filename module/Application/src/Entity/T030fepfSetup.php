<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T030fepfSetup
 *
 * @ORM\Table(name="t030fepf_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\EpfSetupRepository")
 */
class T030fepfSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f030fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f030fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fcategory", type="integer", nullable=false)
     */
    private $f030fcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fstart_amount", type="integer",  nullable=false)
     */
    private $f030fstartAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fend_amount", type="integer",  nullable=false)
     */
    private $f030fendAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030femployer_contribution", type="integer",  nullable=false)
     */
    private $f030femployerContribution;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030femployer_deduction", type="integer",  nullable=false)
     */
    private $f030femployerDeduction;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fstatus", type="integer", nullable=false)
     */
    private $f030fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fcreatedby", type="integer", nullable=false)
     */
    private $f030fcreatedby;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f030fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f030fupdated_by", type="integer", nullable=false)
     */
    private $f030fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f030fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f030fupdatedDtTm;



    /**
     * Get f030fid
     *
     * @return integer
     */
    public function getF030fid()
    {
        return $this->f030fid;
    }

    /**
     * Set f030fcategory
     *
     * @param integer $f030fcategory
     *
     * @return T030fepfSetup
     */
    public function setF030fcategory($f030fcategory)
    {
        $this->f030fcategory = $f030fcategory;

        return $this;
    }

    /**
     * Get f030fcategory
     *
     * @return integer
     */
    public function getF030fcategory()
    {
        return $this->f030fcategory;
    }

    /**
     * Set f030fstartAmount
     *
     * @param integer $f030fstartAmount
     *
     * @return T030fepfSetup
     */
    public function setF030fstartAmount($f030fstartAmount)
    {
        $this->f030fstartAmount = $f030fstartAmount;

        return $this;
    }

    /**
     * Get f030fstartAmount
     *
     * @return integer
     */
    public function getF030fstartAmount()
    {
        return $this->f030fstartAmount;
    }

    /**
     * Set f030fendAmount
     *
     * @param integer $f030fendAmount
     *
     * @return T030fepfSetup
     */
    public function setF030fendAmount($f030fendAmount)
    {
        $this->f030fendAmount = $f030fendAmount;

        return $this;
    }

    /**
     * Get f030fendAmount
     *
     * @return integer
     */
    public function getF030fendAmount()
    {
        return $this->f030fendAmount;
    }

    /**
     * Set f030femployerContribution
     *
     * @param string $f030femployerContribution
     *
     * @return T030fepfSetup
     */
    public function setF030femployerContribution($f030femployerContribution)
    {
        $this->f030femployerContribution = $f030femployerContribution;

        return $this;
    }

    /**
     * Get f030femployerContribution
     *
     * @return string
     */
    public function getF030femployerContribution()
    {
        return $this->f030femployerContribution;
    }

    /**
     * Set f030femployerDeduction
     *
     * @param string $f030femployerDeduction
     *
     * @return T030fepfSetup
     */
    public function setF030femployerDeduction($f030femployerDeduction)
    {
        $this->f030femployerDeduction = $f030femployerDeduction;

        return $this;
    }

    /**
     * Get f030femployerDeduction
     *
     * @return string
     */
    public function getF030femployerDeduction()
    {
        return $this->f030femployerDeduction;
    }

    /**
     * Set f030fstatus
     *
     * @param integer $f030fstatus
     *
     * @return T030fepfSetup
     */
    public function setF030fstatus($f030fstatus)
    {
        $this->f030fstatus = $f030fstatus;

        return $this;
    }

    /**
     * Get f030fstatus
     *
     * @return integer
     */
    public function getF030fstatus()
    {
        return $this->f030fstatus;
    }

    /**
     * Set f030fcreatedby
     *
     * @param integer $f030fcreatedby
     *
     * @return T030fepfSetup
     */
    public function setF030fcreatedby($f030fcreatedby)
    {
        $this->f030fcreatedby = $f030fcreatedby;

        return $this;
    }

    /**
     * Get f030fcreatedby
     *
     * @return integer
     */
    public function getF030fcreatedby()
    {
        return $this->f030fcreatedby;
    }

    /**
     * Set f030fcreatedDtTm
     *
     * @param \DateTime $f030fcreatedDtTm
     *
     * @return T030fepfSetup
     */
    public function setF030fcreatedDtTm($f030fcreatedDtTm)
    {
        $this->f030fcreatedDtTm = $f030fcreatedDtTm;

        return $this;
    }

    /**
     * Get f030fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fcreatedDtTm()
    {
        return $this->f030fcreatedDtTm;
    }

    /**
     * Set f030fupdatedBy
     *
     * @param integer $f030fupdatedBy
     *
     * @return T030fepfSetup
     */
    public function setF030fupdatedBy($f030fupdatedBy)
    {
        $this->f030fupdatedBy = $f030fupdatedBy;

        return $this;
    }

    /**
     * Get f030fupdatedBy
     *
     * @return integer
     */
    public function getF030fupdatedBy()
    {
        return $this->f030fupdatedBy;
    }

    /**
     * Set f030fupdatedDtTm
     *
     * @param \DateTime $f030fupdatedDtTm
     *
     * @return T030fepfSetup
     */
    public function setF030fupdatedDtTm($f030fupdatedDtTm)
    {
        $this->f030fupdatedDtTm = $f030fupdatedDtTm;

        return $this;
    }

    /**
     * Get f030fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF030fupdatedDtTm()
    {
        return $this->f030fupdatedDtTm;
    }
}
