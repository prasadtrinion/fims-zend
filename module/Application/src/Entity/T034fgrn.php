<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
  
/**
 * T034fgrn
 *
 * @ORM\Table(name="t034fgrn")
 * @ORM\Entity(repositoryClass="Application\Repository\GrnRepository")
 */
class T034fgrn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f034fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f034forder_type", type="string",length=20, nullable=true)
     */
    private $f034forderType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034forder_date", type="datetime", nullable=true)
     */
    private $f034forderDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_financialyear", type="integer", nullable=true)
     */
    private $f034fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_supplier", type="integer", nullable=true)
     */
    private $f034fidSupplier;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fdescription", type="string", length=100, nullable=true)
     */
    private $f034fdescription;

     /**
     * @var string
     *
     * @ORM\Column(name="f034freference_number", type="string", length=50, nullable=true)
     */
    private $f034freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_department", type="string",length=30, nullable=true)
     */
    private $f034fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fapproval_status", type="integer", nullable=true)
     */
    private $f034fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_purchase_order", type="integer", nullable=true)
     */
    private $f034fidPurchaseOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fexpired_date", type="datetime", nullable=true)
     */
    private $f034fexpiredDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fapproved_date", type="datetime", nullable=true)
     */
    private $f034fapprovedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fapproved_ref_num", type="string",length=50, nullable=true)
     */
    private $f034fapprovedRefNum = "NULL VALUE";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fdonor", type="string",length=50, nullable=true)
     */
    private $f034fdonor = "a";

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fstatus", type="integer", nullable=true)
     */
    private $f034fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fcreated_by", type="integer", nullable=true)
     */
    private $f034fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fupdated_by", type="integer", nullable=true)
     */
    private $f034fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034ftotal_amount", type="integer", nullable=true)
     */
    private $f034ftotalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f034frating", type="string",length=20, nullable=true)
     */
    private $f034frating;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fbill_generated", type="integer", nullable=true)
     */
    private $f034fbillGenerated;



    /**
     * Get f034fid
     *
     * @return integer
     */
    public function getF034fid()
    {
        return $this->f034fid;
    }

    /**
     * Set f034forderType
     *
     * @param string $f034forderType
     *
     * @return T034fgrn
     */
    public function setF034forderType($f034forderType)
    {
        $this->f034forderType = $f034forderType;

        return $this;
    }

    /**
     * Get f034forderType
     *
     * @return string
     */
    public function getF034forderType()
    {
        return $this->f034forderType;
    }

    /**
     * Set f034forderDate
     *
     * @param \DateTime $f034forderDate
     *
     * @return T034fgrn
     */
    public function setF034forderDate($f034forderDate)
    {
        
        $this->f034forderDate = $f034forderDate;

        return $this;
    }

    /**
     * Get f034forderDate
     *
     * @return \DateTime
     */
    public function getF034forderDate()
    {
        return $this->f034forderDate;
    }

    /**
     * Set f034fidFinancialyear
     *
     * @param integer $f034fidFinancialyear
     *
     * @return T034fgrn
     */
    public function setF034fidFinancialyear($f034fidFinancialyear)
    {
        $this->f034fidFinancialyear = $f034fidFinancialyear;

        return $this;
    }

    /**
     * Get f034fidFinancialyear
     *
     * @return integer
     */
    public function getF034fidFinancialyear()
    {
        return $this->f034fidFinancialyear;
    }

    /**
     * Set f034fidPurchaseOrder
     *
     * @param integer $f034fidPurchaseOrder
     *
     * @return T034fgrn
     */
    public function setF034fidPurchaseOrder($f034fidPurchaseOrder)
    {
        $this->f034fidPurchaseOrder = $f034fidPurchaseOrder;

        return $this;
    }

    /**
     * Get f034fidPurchaseOrder
     *
     * @return integer
     */
    public function getF034fidPurchaseOrder()
    {
        return $this->f034fidPurchaseOrder;
    }

    /**
     * Set f034fidSupplier
     *
     * @param integer $f034fidSupplier
     *
     * @return T034fgrn
     */
    public function setF034fidSupplier($f034fidSupplier)
    {
        $this->f034fidSupplier = $f034fidSupplier;

        return $this;
    }

    /**
     * Get f034fidSupplier
     *
     * @return integer
     */
    public function getF034fidSupplier()
    {
        return $this->f034fidSupplier;
    }

    /**
     * Set f034fdescription
     *
     * @param string $f034fdescription
     *
     * @return T034fgrn
     */
    public function setF034fdescription($f034fdescription)
    {
        $this->f034fdescription = $f034fdescription;

        return $this;
    }

    /**
     * Get f034fdescription
     *
     * @return string
     */
    public function getF034fdescription()
    {
        return $this->f034fdescription;
    }

    /**
     * Set f034fidDepartment
     *
     * @param integer $f034fidDepartment
     *
     * @return T034fgrn
     */
    public function setF034fidDepartment($f034fidDepartment)
    {
        $this->f034fidDepartment = $f034fidDepartment;

        return $this;
    }

    /**
     * Get f034fidDepartment
     *
     * @return integer
     */
    public function getF034fidDepartment()
    {
        return $this->f034fidDepartment;
    }

    /**
     * Set f034freferenceNumber
     *
     * @param string $f034freferenceNumber
     *
     * @return T034fgrn
     */
    public function setF034freferenceNumber($f034freferenceNumber)
    {
        $this->f034freferenceNumber = $f034freferenceNumber;

        return $this;
    }

    /**
     * Get f034freferenceNumber
     *
     * @return string
     */
    public function getF034freferenceNumber()
    {
        return $this->f034freferenceNumber;
    }

    /**
     * Set f034fexpiredDate
     *
     * @param \DateTime $f034fexpiredDate
     *
     * @return T034fgrn
     */
    public function setF034fexpiredDate($f034fexpiredDate)
    {
        $this->f034fexpiredDate = $f034fexpiredDate;

        return $this;
    }

    /**
     * Get f034fexpiredDate
     *
     * @return \DateTime
     */
    public function getF034fexpiredDate()
    {
        return $this->f034fexpiredDate;
    }

    /**
     * Set f034fstatus
     *
     * @param integer $f034fstatus
     *
     * @return T034fgrn
     */
    public function setF034fstatus($f034fstatus)
    {
        $this->f034fstatus = $f034fstatus;

        return $this;
    }

    /**
     * Get f034fstatus
     *
     * @return integer
     */
    public function getF034fstatus()
    {
        return $this->f034fstatus;
    }

    /**
     * Set f034fapprovalStatus
     *
     * @param integer $f034fapprovalStatus
     *
     * @return T034fgrn
     */
    public function setF034fapprovalStatus($f034fapprovalStatus)
    {
        $this->f034fapprovalStatus = $f034fapprovalStatus;

        return $this;
    }

    /**
     * Get f034fapprovalStatus
     *
     * @return integer
     */
    public function getF034fapprovalStatus()
    {
        return $this->f034fapprovalStatus;
    }

    /**
     * Set f034fcreatedBy
     *
     * @param integer $f034fcreatedBy
     *
     * @return T034fgrn
     */
    public function setF034fcreatedBy($f034fcreatedBy)
    {
        $this->f034fcreatedBy = $f034fcreatedBy;

        return $this;
    }

    /**
     * Get f034fcreatedBy
     *
     * @return integer
     */
    public function getF034fcreatedBy()
    {
        return $this->f034fcreatedBy;
    }

    /**
     * Set f034fupdatedBy
     *
     * @param integer $f034fupdatedBy
     *
     * @return T034fgrn
     */
    public function setF034fupdatedBy($f034fupdatedBy)
    {
        $this->f034fupdatedBy = $f034fupdatedBy;

        return $this;
    }

    /**
     * Get f034fupdatedBy
     *
     * @return integer
     */
    public function getF034fupdatedBy()
    {
        return $this->f034fupdatedBy;
    }

    /**
     * Set f034fcreatedDtTm
     *
     * @param \DateTime $f034fcreatedDtTm
     *
     * @return T034fgrn
     */
    public function setF034fcreatedDtTm($f034fcreatedDtTm)
    {
        $this->f034fcreatedDtTm = $f034fcreatedDtTm;

        return $this;
    }

    /**
     * Get f034fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fcreatedDtTm()
    {
        return $this->f034fcreatedDtTm;
    }

    /**
     * Set f034fupdatedDtTm
     *
     * @param \DateTime $f034fupdatedDtTm
     *
     * @return T034fgrn
     */
    public function setF034fupdatedDtTm($f034fupdatedDtTm)
    {
        $this->f034fupdatedDtTm = $f034fupdatedDtTm;

        return $this;
    }

    /**
     * Get f034fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fupdatedDtTm()
    {
        return $this->f034fupdatedDtTm;
    }

    /**
     * Set f034ftotalAmount
     *
     * @param string $f034ftotalAmount
     *
     * @return T034fpurchaseRequisition
     */
    public function setF034ftotalAmount($f034ftotalAmount)
    {
        $this->f034ftotalAmount = $f034ftotalAmount;

        return $this;
    }

    /**
     * Get f034ftotalAmount
     *
     * @return string
     */
    public function getF034ftotalAmount()
    {
        return $this->f034ftotalAmount;
    }

    /**
     * Set f034fapprovedDate
     *
     * @param string $f034fapprovedDate
     *
     * @return T034fpurchaseRequisition
     */
    public function setF034fapprovedDate($f034fapprovedDate)
    {
        $this->f034fapprovedDate = $f034fapprovedDate;

        return $this;
    }

    /**
     * Get f034fapprovedDate
     *
     * @return string
     */
    public function getF034fapprovedDate()
    {
        return $this->f034fapprovedDate;
    }

     /**
     * Set f034fapprovedRefNum
     *
     * @param string $f034fapprovedRefNum
     *
     * @return T034fpurchaseRequisition
     */
    public function setF034fapprovedRefNum($f034fapprovedRefNum)
    {
        $this->f034fapprovedRefNum = $f034fapprovedRefNum;

        return $this;
    }

    /**
     * Get f034fapprovedRefNum
     *
     * @return string
     */
    public function getF034fapprovedRefNum()
    {
        return $this->f034fapprovedRefNum;
    }

     /**
     * Set f034fdonor
     *
     * @param string $f034fdonor
     *
     * @return T034fpurchaseRequisition
     */
    public function setF034fdonor($f034fdonor)
    {
        $this->f034fdonor = $f034fdonor;

        return $this;
    }

    /**
     * Get f034fdonor
     *
     * @return string
     */
    public function getF034fdonor()
    {
        return $this->f034fdonor;
    }

    /**
     * Set f034frating
     *
     * @param string $f034frating
     *
     * @return T034fgrn
     */
    public function setF034frating($f034frating)
    {
        $this->f034frating = $f034frating;

        return $this;
    }

    /**
     * Get f034frating
     *
     * @return string
     */
    public function getF034frating()
    {
        return $this->f034frating;
    }


    /**
     * Set f034fbillGenerated
     *
     * @param string $f034fbillGenerated
     *
     * @return T034fgrn
     */
    public function setF034fbillGenerated($f034fbillGenerated)
    {
        $this->f034fbillGenerated = $f034fbillGenerated;

        return $this;
    }

    /**
     * Get f034fbillGenerated
     *
     * @return string
     */
    public function getF034fbillGenerated()
    {
        return $this->f034fbillGenerated;
    }
    
}
