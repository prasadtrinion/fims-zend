<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T098fdebtInformation
 *
 * @ORM\Table(name="t098fdebt_information")
 * @ORM\Entity(repositoryClass="Application\Repository\DebtInformationRepository")
 */
class T098fdebtInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f098fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f098fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fcompany_name", type="string", length=25, nullable=false)
     */
    private $f098fcompanyName;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fcontact_officer", type="string", length=20, nullable=false)
     */
    private $f098fcontactOfficer;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fphone", type="string", length=20, nullable=false)
     */
    private $f098fphone;

    /**
     * @var string
     *
     * @ORM\Column(name="f098femail", type="string", length=25, nullable=true)
     */
    private $f098femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f098ffax", type="string", length=20, nullable=true)
     */
    private $f098ffax;

    /**
     * @var string
     *
     * @ORM\Column(name="f098faddress", type="string", length=100, nullable=true)
     */
    private $f098faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f098fcity", type="string", length=15, nullable=true)
     */
    private $f098fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fstate", type="integer", nullable=true)
     */
    private $f098fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fcountry", type="integer", nullable=true)
     */
    private $f098fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fcreated_by", type="integer", nullable=true)
     */
    private $f098fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fupdated_by", type="integer", nullable=true)
     */
    private $f098fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f098fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f098fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f098fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f098fstatus", type="integer", nullable=false)
     */
    private $f098fstatus = '1';

    


    /**
     * Get f098fid
     *
     * @return integer
     */
    public function getF098fid()
    {
        return $this->f098fid;
    }

    /**
     * Set f098fcompanyName
     *
     * @param string $f098fcompanyName
     *
     * @return T098fdebtInformation
     */
    public function setF098fcompanyName($f098fcompanyName)
    {
        $this->f098fcompanyName = $f098fcompanyName;

        return $this;
    }

    /**
     * Get f098fcompanyName
     *
     * @return string
     */
    public function getF098fcompanyName()
    {
        return $this->f098fcompanyName;
    }

    /**
     * Set f098fcontactOfficer
     *
     * @param string $f098fcontactOfficer
     *
     * @return T098fdebtInformation
     */
    public function setF098fcontactOfficer($f098fcontactOfficer)
    {
        $this->f098fcontactOfficer = $f098fcontactOfficer;

        return $this;
    }

    /**
     * Get f098fcontactOfficer
     *
     * @return string
     */
    public function getF098fcontactOfficer()
    {
        return $this->f098fcontactOfficer;
    }

    /**
     * Set f098fphone
     *
     * @param string $f098fphone
     *
     * @return T098fdebtInformation
     */
    public function setF098fphone($f098fphone)
    {
        $this->f098fphone = $f098fphone;

        return $this;
    }

    /**
     * Get f098fphone
     *
     * @return string
     */
    public function getF098fphone()
    {
        return $this->f098fphone;
    }

    /**
     * Set f098femail
     *
     * @param string $f098femail
     *
     * @return T098fdebtInformation
     */
    public function setF098femail($f098femail)
    {
        $this->f098femail = $f098femail;

        return $this;
    }

    /**
     * Get f098femail
     *
     * @return string
     */
    public function getF098femail()
    {
        return $this->f098femail;
    }

    /**
     * Set f098ffax
     *
     * @param string $f098ffax
     *
     * @return T098fdebtInformation
     */
    public function setF098ffax($f098ffax)
    {
        $this->f098ffax = $f098ffax;

        return $this;
    }

    /**
     * Get f098ffax
     *
     * @return string
     */
    public function getF098ffax()
    {
        return $this->f098ffax;
    }

    /**
     * Set f098faddress
     *
     * @param string $f098faddress
     *
     * @return T098fdebtInformation
     */
    public function setF098faddress($f098faddress)
    {
        $this->f098faddress = $f098faddress;

        return $this;
    }

    /**
     * Get f098faddress
     *
     * @return string
     */
    public function getF098faddress()
    {
        return $this->f098faddress;
    }

    /**
     * Set f098fcity
     *
     * @param string $f098fcity
     *
     * @return T098fdebtInformation
     */
    public function setF098fcity($f098fcity)
    {
        $this->f098fcity = $f098fcity;

        return $this;
    }

    /**
     * Get f098fcity
     *
     * @return string
     */
    public function getF098fcity()
    {
        return $this->f098fcity;
    }

    /**
     * Set f098fstate
     *
     * @param integer $f098fstate
     *
     * @return T098fdebtInformation
     */
    public function setF098fstate($f098fstate)
    {
        $this->f098fstate = $f098fstate;

        return $this;
    }

    /**
     * Get f098fstate
     *
     * @return integer
     */
    public function getF098fstate()
    {
        return $this->f098fstate;
    }

    /**
     * Set f098fcountry
     *
     * @param integer $f098fcountry
     *
     * @return T098fdebtInformation
     */
    public function setF098fcountry($f098fcountry)
    {
        $this->f098fcountry = $f098fcountry;

        return $this;
    }

    /**
     * Get f098fcountry
     *
     * @return integer
     */
    public function getF098fcountry()
    {
        return $this->f098fcountry;
    }

    /**
     * Set f098fcreatedBy
     *
     * @param integer $f098fcreatedBy
     *
     * @return T098fdebtInformation
     */
    public function setF098fcreatedBy($f098fcreatedBy)
    {
        $this->f098fcreatedBy = $f098fcreatedBy;

        return $this;
    }

    /**
     * Get f098fcreatedBy
     *
     * @return integer
     */
    public function getF098fcreatedBy()
    {
        return $this->f098fcreatedBy;
    }

    /**
     * Set f098fupdatedBy
     *
     * @param integer $f098fupdatedBy
     *
     * @return T098fdebtInformation
     */
    public function setF098fupdatedBy($f098fupdatedBy)
    {
        $this->f098fupdatedBy = $f098fupdatedBy;

        return $this;
    }

    /**
     * Get f098fupdatedBy
     *
     * @return integer
     */
    public function getF098fupdatedBy()
    {
        return $this->f098fupdatedBy;
    }

    /**
     * Set f098fcreatedDtTm
     *
     * @param \DateTime $f098fcreatedDtTm
     *
     * @return T098fdebtInformation
     */
    public function setF098fcreatedDtTm($f098fcreatedDtTm)
    {
        $this->f098fcreatedDtTm = $f098fcreatedDtTm;

        return $this;
    }

    /**
     * Get f098fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fcreatedDtTm()
    {
        return $this->f098fcreatedDtTm;
    }

    /**
     * Set f098fupdatedDtTm
     *
     * @param \DateTime $f098fupdatedDtTm
     *
     * @return T098fdebtInformation
     */
    public function setF098fupdatedDtTm($f098fupdatedDtTm)
    {
        $this->f098fupdatedDtTm = $f098fupdatedDtTm;

        return $this;
    }

    /**
     * Get f098fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF098fupdatedDtTm()
    {
        return $this->f098fupdatedDtTm;
    }

    /**
     * Set f098fstatus
     *
     * @param integer $f098fstatus
     *
     * @return T098fdebtInformation
     */
    public function setF098fstatus($f098fstatus)
    {
        $this->f098fstatus = $f098fstatus;

        return $this;
    }

    /**
     * Get f098fstatus
     *
     * @return integer
     */
    public function getF098fstatus()
    {
        return $this->f098fstatus;
    }
}
