<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T018fblacklistEmp
 *
 * @ORM\Table(name="t018fblacklist_emp")
 * @ORM\Entity(repositoryClass="Application\Repository\BlacklistEmpRepository")
 */
class T018fblacklistEmp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f018fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fid_staff", type="integer", nullable=false)
     */
    private $f018fidStaff;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018feffective_date", type="datetime", nullable=true)
     */
    private $f018feffectiveDate;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fend_date", type="datetime", nullable=true)
     */
    private $f018fendDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f018freason", type="string", length=100, nullable=false)
     */
    private $f018freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fstatus", type="integer", nullable=false)
     */
    private $f018fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcreated_by", type="integer", nullable=false)
     */
    private $f018fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f018fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fupdated_by", type="integer", nullable=false)
     */
    private $f018fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f018fupdatedDtTm;

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018frelease_date", type="datetime", nullable=true)
     */
    private $f018freleaseDate;



    /**
     * Get f018fid
     *
     * @return integer
     */
    public function getF018fid()
    {
        return $this->f018fid;
    }

    /**
     * Set f018fidStaff
     *
     * @param string $f018fidStaff
     *
     * @return T018fblacklistEmp
     */
    public function setF018fidStaff($f018fidStaff)
    {
        $this->f018fidStaff = $f018fidStaff;

        return $this;
    }

    /**
     * Get f018fidStaff
     *
     * @return string
     */
    public function getF018fidStaff()
    {
        return $this->f018fidStaff;
    }

    /**
     * Set f018fendDate
     *
     * @param \DateTime $f018fendDate
     *
     * @return T018fblacklistEmp
     */
    public function setF018fendDate($f018fendDate)
    {
        $this->f018fendDate = $f018fendDate;

        return $this;
    }

    /**
     * Get f018fendDate
     *
     * @return \DateTime
     */
    public function getF018fendDate()
    {
        return $this->f018fendDate;
    }

    /**
     * Set f018feffectiveDate
     *
     * @param \DateTime $f018feffectiveDate
     *
     * @return T018fblacklistEmp
     */
    public function setF018feffectiveDate($f018feffectiveDate)
    {
        $this->f018feffectiveDate = $f018feffectiveDate;

        return $this;
    }

    /**
     * Get f018feffectiveDate
     *
     * @return \DateTime
     */
    public function getF018feffectiveDate()
    {
        return $this->f018feffectiveDate;
    }

    /**
     * Set f018freason
     *
     * @param string $f018freason
     *
     * @return T018fblacklistEmp
     */
    public function setF018freason($f018freason)
    {
        $this->f018freason = $f018freason;

        return $this;
    }

    /**
     * Get f018freason
     *
     * @return string
     */
    public function getF018freason()
    {
        return $this->f018freason;
    }

    /**
     * Set f018fstatus
     *
     * @param integer $f018fstatus
     *
     * @return T018fblacklistEmp
     */
    public function setF018fstatus($f018fstatus)
    {
        $this->f018fstatus = $f018fstatus;

        return $this;
    }

    /**
     * Get f018fstatus
     *
     * @return integer
     */
    public function getF018fstatus()
    {
        return $this->f018fstatus;
    }

    /**
     * Set f018fcreatedBy
     *
     * @param integer $f018fcreatedBy
     *
     * @return T018fblacklistEmp
     */
    public function setF018fcreatedBy($f018fcreatedBy)
    {
        $this->f018fcreatedBy = $f018fcreatedBy;

        return $this;
    }

    /**
     * Get f018fcreatedBy
     *
     * @return integer
     */
    public function getF018fcreatedBy()
    {
        return $this->f018fcreatedBy;
    }

    /**
     * Set f018fcreatedDtTm
     *
     * @param \DateTime $f018fcreatedDtTm
     *
     * @return T018fblacklistEmp
     */
    public function setF018fcreatedDtTm($f018fcreatedDtTm)
    {
        $this->f018fcreatedDtTm = $f018fcreatedDtTm;

        return $this;
    }

    /**
     * Get f018fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF018fcreatedDtTm()
    {
        return $this->f018fcreatedDtTm;
    }

    /**
     * Set f018fupdatedBy
     *
     * @param integer $f018fupdatedBy
     *
     * @return T018fblacklistEmp
     */
    public function setF018fupdatedBy($f018fupdatedBy)
    {
        $this->f018fupdatedBy = $f018fupdatedBy;

        return $this;
    }

    /**
     * Get f018fupdatedBy
     *
     * @return integer
     */
    public function getF018fupdatedBy()
    {
        return $this->f018fupdatedBy;
    }

    /**
     * Set f018fupdatedDtTm
     *
     * @param \DateTime $f018fupdatedDtTm
     *
     * @return T018fblacklistEmp
     */
    public function setF018fupdatedDtTm($f018fupdatedDtTm)
    {
        $this->f018fupdatedDtTm = $f018fupdatedDtTm;

        return $this;
    }

    /**
     * Get f018fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF018fupdatedDtTm()
    {
        return $this->f018fupdatedDtTm;
    }

    /**
     * Set f018freleaseDate
     *
     * @param \DateTime $f018freleaseDate
     *
     * @return T018fblacklistEmp
     */
    public function setF018freleaseDate($f018freleaseDate)
    {
        $this->f018freleaseDate = $f018freleaseDate;

        return $this;
    }

    /**
     * Get f018freleaseDate
     *
     * @return \DateTime
     */
    public function getF018freleaseDate()
    {
        return $this->f018freleaseDate;
    }

}
