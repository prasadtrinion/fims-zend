<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T020fadvancePayment
 *
 * @ORM\Table(name="t020fadvance_payment")
 * @ORM\Entity(repositoryClass="Application\Repository\AdvancePaymentRepository")
 */
class T020fadvancePayment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f020fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f020fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fid_customer", type="integer", nullable=true)
     */
    private $f020fidCustomer;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fdocument_number", type="string",length=20, nullable=true)
     */
    private $f020fdocumentNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020famount", type="integer", nullable=true)
     */
    private $f020famount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fid_number", type="string",length=20, nullable=true)
     */
    private $f020fidNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f020ftype", type="string", length=20, nullable=true)
     */
    private $f020ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f020fpayment_status", type="string", length=50, nullable=true)
     */
    private $f020fpaymentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fbalance_amount", type="integer", nullable=true)
     */
    private $f020fbalanceAmount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f020fstatus", type="integer", nullable=true)
     */
    private $f020fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fcreated_by", type="integer", nullable=true)
     */
    private $f020fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f020fupdated_by", type="integer", nullable=true)
     */
    private $f020fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f020fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f020fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f020fupdatedDtTm;

	

    /**
     * Get f020fid
     *
     * @return integer
     */
    public function getF020fid()
    {
        return $this->f020fid;
    }

    /**
     * Set f020fidNumber
     *
     * @param integer $f020fidNumber
     *
     * @return T020fadvancePayment
     */
    public function setF020fidNumber($f020fidNumber)
    {
        $this->f020fidNumber = $f020fidNumber;

        return $this;
    }

    /**
     * Get f020fidNumber
     *
     * @return integer
     */
    public function getF020fidNumber()
    {
        return $this->f020fidNumber;
    }

    /**
     * Set f020fidCustomer
     *
     * @param integer $f020fidCustomer
     *
     * @return T020fadvancePayment
     */
    public function setF020fidCustomer($f020fidCustomer)
    {
        $this->f020fidCustomer = $f020fidCustomer;

        return $this;
    }

    /**
     * Get f020fidCustomer
     *
     * @return integer
     */
    public function getF020fidCustomer()
    {
        return $this->f020fidCustomer;
    }
    /**
     * Set f020fdocumentNumber
     *
     * @param integer $f020fdocumentNumber
     *
     * @return T020fadvancePayment
     */
    public function setF020fdocumentNumber($f020fdocumentNumber)
    {
        $this->f020fdocumentNumber = $f020fdocumentNumber;

        return $this;
    }

    /**
     * Get f020fdocumentNumber
     *
     * @return integer
     */
    public function getF020fdocumentNumber()
    {
        return $this->f020fdocumentNumber;
    }

    /**
     * Set f020famount
     *
     * @param integer $f020famount
     *
     * @return T020fadvancePayment
     */
    public function setF020famount($f020famount)
    {
        $this->f020famount = $f020famount;

        return $this;
    }

    /**
     * Get f020famount
     *
     * @return integer
     */
    public function getF020famount()
    {
        return $this->f020famount;
    }

    /**
     * Set f020ftype
     *
     * @param string $f020ftype
     *
     * @return T020fadvancePayment
     */
    public function setF020ftype($f020ftype)
    {
        $this->f020ftype = $f020ftype;

        return $this;
    }

    /**
     * Get f020ftype
     *
     * @return string
     */
    public function getF020ftype()
    {
        return $this->f020ftype;
    }

    /**
     * Set f020fpaymentStatus
     *
     * @param string $f020fpaymentStatus
     *
     * @return T020fadvancePayment
     */
    public function setF020fpaymentStatus($f020fpaymentStatus)
    {
        $this->f020fpaymentStatus = $f020fpaymentStatus;

        return $this;
    }

    /**
     * Get f020fpaymentStatus
     *
     * @return string
     */
    public function getF020fpaymentStatus()
    {
        return $this->f020fpaymentStatus;
    }

    /**
     * Set f020fbalanceAmount
     *
     * @param integer $f020fbalanceAmount
     *
     * @return T020fadvancePayment
     */
    public function setF020fbalanceAmount($f020fbalanceAmount)
    {
        $this->f020fbalanceAmount = $f020fbalanceAmount;

        return $this;
    }

    /**
     * Get f020fbalanceAmount
     *
     * @return integer
     */
    public function getF020fbalanceAmount()
    {
        return $this->f020fbalanceAmount;
    }


    /**
     * Set f020fstatus
     *
     * @param integer $f020fstatus
     *
     * @return T020fadvancePayment
     */
    public function setF020fstatus($f020fstatus)
    {
        $this->f020fstatus = $f020fstatus;

        return $this;
    }

    /**
     * Get f020fstatus
     *
     * @return integer
     */
    public function getF020fstatus()
    {
        return $this->f020fstatus;
    }

    /**
     * Set f020fcreatedBy
     *
     * @param integer $f020fcreatedBy
     *
     * @return T020fadvancePayment
     */
    public function setF020fcreatedBy($f020fcreatedBy)
    {
        $this->f020fcreatedBy = $f020fcreatedBy;

        return $this;
    }

    /**
     * Get f020fcreatedBy
     *
     * @return integer
     */
    public function getF020fcreatedBy()
    {
        return $this->f020fcreatedBy;
    }

    /**
     * Set f020fupdatedBy
     *
     * @param integer $f020fupdatedBy
     *
     * @return T020fadvancePayment
     */
    public function setF020fupdatedBy($f020fupdatedBy)
    {
        $this->f020fupdatedBy = $f020fupdatedBy;

        return $this;
    }

    /**
     * Get f020fupdatedBy
     *
     * @return integer
     */
    public function getF020fupdatedBy()
    {
        return $this->f020fupdatedBy;
    }

    /**
     * Set f020fcreatedDtTm
     *
     * @param \DateTime $f020fcreatedDtTm
     *
     * @return T020fadvancePayment
     */
    public function setF020fcreatedDtTm($f020fcreatedDtTm)
    {
        $this->f020fcreatedDtTm = $f020fcreatedDtTm;

        return $this;
    }

    /**
     * Get f020fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fcreatedDtTm()
    {
        return $this->f020fcreatedDtTm;
    }

    /**
     * Set f020fdate
     *
     * @param \DateTime $f020fdate
     *
     * @return T020fadvancePayment
     */
    public function setF020fdate($f020fdate)
    {
        $this->f020fdate = $f020fdate;

        return $this;
    }

    /**
     * Get f020fdate
     *
     * @return \DateTime
     */
    public function getF020fdate()
    {
        return $this->f020fdate;
    }

    /**
     * Set f020fupdatedDtTm
     *
     * @param \DateTime $f020fupdatedDtTm
     *
     * @return T020fadvancePayment
     */
    public function setF020fupdatedDtTm($f020fupdatedDtTm)
    {
        $this->f020fupdatedDtTm = $f020fupdatedDtTm;

        return $this;
    }

    /**
     * Get f020fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF020fupdatedDtTm()
    {
        return $this->f020fupdatedDtTm;
    }

   
}

