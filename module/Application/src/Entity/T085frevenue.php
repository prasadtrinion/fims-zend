<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T085frevenue
 *
 * @ORM\Table(name="t085frevenue")
 * @ORM\Entity(repositoryClass="Application\Repository\RevenueRepository")
 */
class T085frevenue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f085fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085ffee_item", type="integer", nullable=false)
     */
    private $f085ffeeItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085ffund_id", type="string", length=20, nullable=false)
     */
    private $f085ffundId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085factivity_id", type="string", length=20, nullable=true)
     */
    private $f085factivityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fdepartment_id", type="string", length=20, nullable=false)
     */
    private $f085fdepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085faccount_id", type="string", length=20, nullable=false)
     */
    private $f085faccountId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fstatus", type="integer", nullable=false)
     */
    private $f085fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f085fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f085fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fupdateDtTm;



    /**
     * Get f085fid
     *
     * @return integer
     */
    public function getF085fid()
    {
        return $this->f085fid;
    }

    /**
     * Set f085ffeeItem
     *
     * @param string $f085ffeeItem
     *
     * @return T085frevenue
     */
    public function setF085ffeeItem($f085ffeeItem)
    {
        $this->f085ffeeItem = $f085ffeeItem;

        return $this;
    }

    /**
     * Get f085ffeeItem
     *
     * @return string
     */
    public function getF085ffeeItem()
    {
        return $this->f085ffeeItem;
    }

    /**
     * Set f085ffundId
     *
     * @param integer $f085ffundId
     *
     * @return T085frevenue
     */
    public function setF085ffundId($f085ffundId)
    {
        $this->f085ffundId = $f085ffundId;

        return $this;
    }

    /**
     * Get f085ffundId
     *
     * @return integer
     */
    public function getF085ffundId()
    {
        return $this->f085ffundId;
    }

    /**
     * Set f085factivityId
     *
     * @param integer $f085factivityId
     *
     * @return T085frevenue
     */
    public function setF085factivityId($f085factivityId)
    {
        $this->f085factivityId = $f085factivityId;

        return $this;
    }

    /**
     * Get f085factivityId
     *
     * @return integer
     */
    public function getF085factivityId()
    {
        return $this->f085factivityId;
    }

    /**
     * Set f085fdepartmentId
     *
     * @param integer $f085fdepartmentId
     *
     * @return T085frevenue
     */
    public function setF085fdepartmentId($f085fdepartmentId)
    {
        $this->f085fdepartmentId = $f085fdepartmentId;

        return $this;
    }

    /**
     * Get f085fdepartmentId
     *
     * @return integer
     */
    public function getF085fdepartmentId()
    {
        return $this->f085fdepartmentId;
    }

    /**
     * Set f085faccountId
     *
     * @param integer $f085faccountId
     *
     * @return T085frevenue
     */
    public function setF085faccountId($f085faccountId)
    {
        $this->f085faccountId = $f085faccountId;

        return $this;
    }

    /**
     * Get f085faccountId
     *
     * @return integer
     */
    public function getF085faccountId()
    {
        return $this->f085faccountId;
    }

    /**
     * Set f085fstatus
     *
     * @param integer $f085fstatus
     *
     * @return T085frevenue
     */
    public function setF085fstatus($f085fstatus)
    {
        $this->f085fstatus = $f085fstatus;

        return $this;
    }

    /**
     * Get f085fstatus
     *
     * @return integer
     */
    public function getF085fstatus()
    {
        return $this->f085fstatus;
    }

    /**
     * Set f085fcreatedBy
     *
     * @param integer $f085fcreatedBy
     *
     * @return T085frevenue
     */
    public function setF085fcreatedBy($f085fcreatedBy)
    {
        $this->f085fcreatedBy = $f085fcreatedBy;

        return $this;
    }

    /**
     * Get f085fcreatedBy
     *
     * @return integer
     */
    public function getF085fcreatedBy()
    {
        return $this->f085fcreatedBy;
    }

    /**
     * Set f085fupdatedBy
     *
     * @param integer $f085fupdatedBy
     *
     * @return T085frevenue
     */
    public function setF085fupdatedBy($f085fupdatedBy)
    {
        $this->f085fupdatedBy = $f085fupdatedBy;

        return $this;
    }

    /**
     * Get f085fupdatedBy
     *
     * @return integer
     */
    public function getF085fupdatedBy()
    {
        return $this->f085fupdatedBy;
    }

    /**
     * Set f085fcreateDtTm
     *
     * @param \DateTime $f085fcreateDtTm
     *
     * @return T085frevenue
     */
    public function setF085fcreateDtTm($f085fcreateDtTm)
    {
        $this->f085fcreateDtTm = $f085fcreateDtTm;

        return $this;
    }

    /**
     * Get f085fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF085fcreateDtTm()
    {
        return $this->f085fcreateDtTm;
    }

    /**
     * Set f085fupdateDtTm
     *
     * @param \DateTime $f085fupdateDtTm
     *
     * @return T085frevenue
     */
    public function setF085fupdateDtTm($f085fupdateDtTm)
    {
        $this->f085fupdateDtTm = $f085fupdateDtTm;

        return $this;
    }

    /**
     * Get f085fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF085fupdateDtTm()
    {
        return $this->f085fupdateDtTm;
    }
}

























