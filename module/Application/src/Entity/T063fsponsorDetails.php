<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fsponsorDetailsDetails
 *
 * @ORM\Table(name="t063fsponsor_details")
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorRepository")
 */
class T063fsponsorDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_sponsor", type="integer", nullable=true)
     */
    private $f063fidSponsor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_fee_type", type="integer", nullable=true)
     */
    private $f063fidFeeType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063ffee_category", type="integer", nullable=true)
     */
    private $f063ffeeCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fsponsor_code", type="string",length=50, nullable=true)
     */
    private $f063fsponsorCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=true)
     */
    private $f063fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fallow_second", type="integer", nullable=true)
     */
    private $f063fallowSecond;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=true)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=true)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f063fupdatedDtTm;

    /**
     * Get f063fidDetails
     *
     * @return integer
     */
    public function getF063fidDetails()
    {
        return $this->f063fidDetails;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fsponsorDetails
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063fallowSecond
     *
     * @param integer $f063fallowSecond
     *
     * @return T063fsponsorDetails
     */
    public function setF063fallowSecond($f063fallowSecond)
    {
        $this->f063fallowSecond = $f063fallowSecond;

        return $this;
    }

    /**
     * Get f063fallowSecond
     *
     * @return integer
     */
    public function getF063fallowSecond()
    {
        return $this->f063fallowSecond;
    }

    /**
     * Set f063fsponsorCode
     *
     * @param integer $f063fsponsorCode
     *
     * @return T063fsponsorDetails
     */
    public function setF063fsponsorCode($f063fsponsorCode)
    {
        $this->f063fsponsorCode = $f063fsponsorCode;

        return $this;
    }

    /**
     * Get f063fsponsorCode
     *
     * @return integer
     */
    public function getF063fsponsorCode()
    {
        return $this->f063fsponsorCode;
    }

    /**
     * Set f063ffeeCategory
     *
     * @param integer $f063ffeeCategory
     *
     * @return T063fsponsorDetails
     */
    public function setF063ffeeCategory($f063ffeeCategory)
    {
        $this->f063ffeeCategory = $f063ffeeCategory;

        return $this;
    }

    /**
     * Get f063ffeeCategory
     *
     * @return integer
     */
    public function getF063ffeeCategory()
    {
        return $this->f063ffeeCategory;
    }

    /**
     * Set f063fidSponsor
     *
     * @param integer $f063fidSponsor
     *
     * @return T063fsponsorDetails
     */
    public function setF063fidSponsor($f063fidSponsor)
    {
        $this->f063fidSponsor = $f063fidSponsor;

        return $this;
    }

    /**
     * Get f063fidSponsor
     *
     * @return integer
     */
    public function getF063fidSponsor()
    {
        return $this->f063fidSponsor;
    }

    /**
     * Set f063fidFeeType
     *
     * @param integer $f063fidFeeType
     *
     * @return T063fsponsorDetails
     */
    public function setF063fidFeeType($f063fidFeeType)
    {
        $this->f063fidFeeType = $f063fidFeeType;

        return $this;
    }

    /**
     * Get f063fidFeeType
     *
     * @return integer
     */
    public function getF063fidFeeType()
    {
        return $this->f063fidFeeType;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fsponsorDetails
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fsponsorDetails
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fsponsorDetails
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fsponsorDetails
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }
}

