<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052ftenderStaff
 *
 * @ORM\Table(name="t052ftender_staff", indexes={@ORM\Index(name="f052fid_submission", columns={"f052fid_submission"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TenderSubmissionRepository")
 */
class T052ftenderStaff
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_tender", type="integer", nullable=false)
     */
    private $f052fidTender;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_staff", type="integer", nullable=false)
     */
    private $f052fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=false)
     */
    private $f052fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=false)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=false)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f052fupdatedDtTm;

    /**
     * @var \Application\Entity\T050ftenderSubmission
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T050ftenderSubmission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f052fid_submission", referencedColumnName="f050fid")
     * })
     */
    private $f052fidSubmission;

     /**
     * Get f052fid
     *
     * @return integer
     */
    public function getF052fid()
    {
        return $this->f052fid;
    }

    /**
     * Set f052fidTender
     *
     * @param integer $f052fidTender
     *
     * @return T052ftenderStaff
     */
    public function setF052fidTender($f052fidTender)
    {
        $this->f052fidTender = $f052fidTender;

        return $this;
    }

    /**
     * Get f052fidTender
     *
     * @return integer
     */
    public function getF052fidTender()
    {
        return $this->f052fidTender;
    }

    /**
     * Set f052fidStaff
     *
     * @param integer $f052fidStaff
     *
     * @return T052ftenderStaff
     */
    public function setF052fidStaff($f052fidStaff)
    {
        $this->f052fidStaff = $f052fidStaff;

        return $this;
    }

    /**
     * Get f052fidStaff
     *
     * @return integer
     */
    public function getF052fidStaff()
    {
        return $this->f052fidStaff;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052ftenderStaff
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052ftenderStaff
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052ftenderStaff
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052ftenderStaff
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052ftenderStaff
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }

    /**
     * Set f052fidSubmission
     *
     * @param \Application\Entity\T050ftenderSubmission $f052fidSubmission
     *
     * @return T052ftenderStaff
     */
    public function setF052fidSubmission(\Application\Entity\T050ftenderSubmission $f052fidSubmission = null)
    {
        $this->f052fidSubmission = $f052fidSubmission;

        return $this;
    }

    /**
     * Get f052fidSubmission
     *
     * @return \Application\Entity\T050ftenderSubmission
     */
    public function getF052fidSubmission()
    {
        return $this->f052fidSubmission;
    }

}

