<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T077frecurringSetupDetails
 *
 * @ORM\Table(name="t077frecurring_setup_details")
 * @ORM\Entity(repositoryClass="Application\Repository\RecurringSetUpRepository")
 */
class T077frecurringSetupDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f077fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f077fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fid_recurring_setup", type="integer", nullable=false)
     */
    private $f077fidRecurringSetup;

    /**
     * @var string
     *
     * @ORM\Column(name="f077ffee_setup", type="integer", nullable=false)
     */
    private $f077ffeeSetup;

     /**
     * @var integer
     *
     * @ORM\Column(name="f077fgst_percentage", type="integer", nullable=true)
     */
    private $f077fgstPercentage;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fgst_code", type="string", length=30, nullable=true)
     */
    private $f077fgstCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fcr_fund", type="string", length=20, nullable=false)
     */
    private $f077fcrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fcr_activity", type="string", length=20, nullable=true)
     */
    private $f077fcrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fcr_department", type="string", length=20, nullable=false)
     */
    private $f077fcrDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fcr_account", type="string", length=20, nullable=false)
     */
    private $f077fcrAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdr_fund", type="string", length=20, nullable=false)
     */
    private $f077fdrFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdr_activity", type="string", length=20, nullable=true)
     */
    private $f077fdrActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdr_department", type="string", length=20, nullable=false)
     */
    private $f077fdrDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f077fdr_account", type="string", length=20, nullable=false)
     */
    private $f077fdrAccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077famount", type="integer",  nullable=false)
     */
    private $f077famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077ftax_code", type="integer",  nullable=false)
     */
    private $f077ftaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fstatus", type="integer", nullable=false)
     */
    private $f077fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f077fcreated_by", type="integer", nullable=false)
     */
    private $f077fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f077fupdated_by", type="integer",  nullable=false)
     */
    private $f077fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f077fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f077fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f077fupdateDtTm;



    /**
     * Get f077fidDetails
     *
     * @return integer
     */
    public function getF077fidDetails()
    {
        return $this->f077fidDetails;
    }

    /**
     * Set f077fidRecurringSetup
     *
     * @param integer $f077fidRecurringSetup
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fidRecurringSetup($f077fidRecurringSetup)
    {
        $this->f077fidRecurringSetup = $f077fidRecurringSetup;

        return $this;
    }

    /**
     * Get f077fidRecurringSetup
     *
     * @return integer
     */
    public function getF077fidRecurringSetup()
    {
        return $this->f077fidRecurringSetup;
    }

    /**
     * Set f077ffeeSetup
     *
     * @param string $f077ffeeSetup
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077ffeeSetup($f077ffeeSetup)
    {
        $this->f077ffeeSetup = $f077ffeeSetup;

        return $this;
    }

    /**
     * Get f077ffeeSetup
     *
     * @return string
     */
    public function getF077ffeeSetup()
    {
        return $this->f077ffeeSetup;
    }

    /**
     * Set f077fgstPercentage
     *
     * @param integer $f077fgstPercentage
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fgstPercentage($f077fgstPercentage)
    {
        $this->f077fgstPercentage = $f077fgstPercentage;

        return $this;
    }

    /**
     * Get f077fgstPercentage
     *
     * @return integer
     */
    public function getF077fgstPercentage()
    {
        return $this->f077fgstPercentage;
    }

     /**
     * Set f077fgstCode
     *
     * @param string $f077fgstCode
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fgstCode($f077fgstCode)
    {
        $this->f077fgstCode = $f077fgstCode;

        return $this;
    }

    /**
     * Get f077fgstCode
     *
     * @return string
     */
    public function getF077fgstCode()
    {
        return $this->f077fgstCode;
    }

    /**
     * Set f077fcrFund
     *
     * @param string $f077fcrFund
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcrFund($f077fcrFund)
    {
        $this->f077fcrFund = $f077fcrFund;

        return $this;
    }

    /**
     * Get f077fcrFund
     *
     * @return string
     */
    public function getF077fcrFund()
    {
        return $this->f077fcrFund;
    }

    /**
     * Set f077fcrActivity
     *
     * @param string $f077fcrActivity
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcrActivity($f077fcrActivity)
    {
        $this->f077fcrActivity = $f077fcrActivity;

        return $this;
    }

    /**
     * Get f077fcrActivity
     *
     * @return string
     */
    public function getF077fcrActivity()
    {
        return $this->f077fcrActivity;
    }

    /**
     * Set f077fcrDepartment
     *
     * @param string $f077fcrDepartment
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcrDepartment($f077fcrDepartment)
    {
        $this->f077fcrDepartment = $f077fcrDepartment;

        return $this;
    }

    /**
     * Get f077fcrDepartment
     *
     * @return string
     */
    public function getF077fcrDepartment()
    {
        return $this->f077fcrDepartment;
    }

    /**
     * Set f077fcrAccount
     *
     * @param string $f077fcrAccount
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcrAccount($f077fcrAccount)
    {
        $this->f077fcrAccount = $f077fcrAccount;

        return $this;
    }

    /**
     * Get f077fcrAccount
     *
     * @return string
     */
    public function getF077fcrAccount()
    {
        return $this->f077fcrAccount;
    }

    /**
     * Set f077fdrFund
     *
     * @param string $f077fdrFund
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fdrFund($f077fdrFund)
    {
        $this->f077fdrFund = $f077fdrFund;

        return $this;
    }

    /**
     * Get f077fdrFund
     *
     * @return string
     */
    public function getF077fdrFund()
    {
        return $this->f077fdrFund;
    }

    /**
     * Set f077fdrActivity
     *
     * @param string $f077fdrActivity
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fdrActivity($f077fdrActivity)
    {
        $this->f077fdrActivity = $f077fdrActivity;

        return $this;
    }

    /**
     * Get f077fdrActivity
     *
     * @return string
     */
    public function getF077fdrActivity()
    {
        return $this->f077fdrActivity;
    }

    /**
     * Set f077fdrDepartment
     *
     * @param string $f077fdrDepartment
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fdrDepartment($f077fdrDepartment)
    {
        $this->f077fdrDepartment = $f077fdrDepartment;

        return $this;
    }

    /**
     * Get f077fdrDepartment
     *
     * @return string
     */
    public function getF077fdrDepartmentId()
    {
        return $this->f077fdrDepartmentId;
    }

    /**
     * Set f077fdrAccount
     *
     * @param string $f077fdrAccount
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fdrAccount($f077fdrAccount)
    {
        $this->f077fdrAccount = $f077fdrAccount;

        return $this;
    }

    /**
     * Get f077fdrAccount
     *
     * @return string
     */
    public function getF077fdrAccount()
    {
        return $this->f077fdrAccount;
    }

    /**
     * Set f077ftaxCode
     *
     * @param string $f077ftaxCode
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077ftaxCode($f077ftaxCode)
    {
        $this->f077ftaxCode = $f077ftaxCode;

        return $this;
    }
    

    /**
     * Get f077ftaxCode
     *
     * @return string
     */
    public function getF077ftaxCode()
    {
        return $this->f077ftaxCode;
    }

    /**
     * Set f077fstatus
     *
     * @param string $f077fstatus
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fstatus($f077fstatus)
    {
        $this->f077fstatus = $f077fstatus;

        return $this;
    }
    

    /**
     * Get f077fstatus
     *
     * @return string
     */
    public function getF077fstatus()
    {
        return $this->f077fstatus;
    }

     /**
     * Set f077famount
     *
     * @param integer $f077famount
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077famount($f077famount)
    {
        $this->f077famount = $f077famount;

        return $this;
    }
    

    /**
     * Get f077famount
     *
     * @return integer
     */
    public function getF077famount()
    {
        return $this->f077famount;
    }

    /**
     * Set f077fcreatedBy
     *
     * @param string $f077fcreatedBy
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcreatedBy($f077fcreatedBy)
    {
        $this->f077fcreatedBy = $f077fcreatedBy;

        return $this;
    }

    /**
     * Get f077fcreatedBy
     *
     * @return string
     */
    public function getF077fcreatedBy()
    {
        return $this->f077fcreatedBy;
    }

    /**
     * Set f077fupdatedBy
     *
     * @param string $f077fupdatedBy
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fupdatedBy($f077fupdatedBy)
    {
        $this->f077fupdatedBy = $f077fupdatedBy;

        return $this;
    }

    /**
     * Get f077fupdatedBy
     *
     * @return string
     */
    public function getF077fupdatedBy()
    {
        return $this->f077fupdatedBy;
    }

    /**
     * Set f077fcreateDtTm
     *
     * @param \DateTime $f077fcreateDtTm
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fcreateDtTm($f077fcreateDtTm)
    {
        $this->f077fcreateDtTm = $f077fcreateDtTm;

        return $this;
    }

    /**
     * Get f077fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF077fcreateDtTm()
    {
        return $this->f077fcreateDtTm;
    }

    /**
     * Set f077fupdateDtTm
     *
     * @param \DateTime $f077fupdateDtTm
     *
     * @return T077frecurringSetupDetails
     */
    public function setF077fupdateDtTm($f077fupdateDtTm)
    {
        $this->f077fupdateDtTm = $f077fupdateDtTm;

        return $this;
    }

    /**
     * Get f077fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF077fupdateDtTm()
    {
        return $this->f077fupdateDtTm;
    }
}