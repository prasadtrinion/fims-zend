<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T060fstudent
 *
 * @ORM\Table(name="vw_all_students")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentRepository")
 */
class T060fstudent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f060fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f060fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f060femail", type="string", length=45, nullable=true)
     */
    private $f060femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fname", type="string", length=45, nullable=true)
     */
    private $f060fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fphone_number", type="string", length=45, nullable=true)
     */
    private $f060fphoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f060faddress", type="string", length=250, nullable=true)
     */
    private $f060faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fcity", type="string", length=100, nullable=true)
     */
    private $f060fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fstate", type="integer", nullable=true)
     */
    private $f060fstate;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fgender", type="string", length=10, nullable=true)
     */
    private $f060fgender;

    /**
     * @var string
     *
     * @ORM\Column(name="f060ftype", type="string", length=10, nullable=true)
     */
    private $f060ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fid_program", type="string",length=20, nullable=true)
     */
    private $f060fidProgram;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fid_intake", type="string",length=30, nullable=true)
     */
    private $f060fidIntake;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fid_semester", type="string", length=60, nullable=true)
     */
    private $f060fidSemester;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fdepartment", type="string", length=60, nullable=true)
     */
    private $f060fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fstatus", type="integer", nullable=true)
     */
    private $f060fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fcreated_by", type="integer", nullable=true)
     */
    private $f060fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fupdated_by", type="integer", nullable=true)
     */
    private $f060fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f060fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f060fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f060fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f060fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fcountry", type="integer", nullable=true)
     */
    private $f060fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fcitizen", type="integer", nullable=true)
     */
    private $f060fcitizen;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fregion", type="string",length=30, nullable=true)
     */
    private $f060fregion;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fstudent_category", type="integer", nullable=true)
     */
    private $f060fstudentCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f060fstudy_mode", type="integer", nullable=true)
     */
    private $f060fstudyMode;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fpassword", type="string", length=150, nullable=true)
     */
    private $f060fpassword;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fmatric_number", type="string", length=150, nullable=true)
     */
    private $f060fmatricNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fic_number", type="string", length=150, nullable=true)
     */
    private $f060ficNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f060fprogram_code", type="string", length=75, nullable=true)
     */
    private $f060fprogramCode;



    /**
     * Get f060fid
     *
     * @return integer
     */
    public function getF060fid()
    {
        return $this->f060fid;
    }

    /**
     * Set f060femail
     *
     * @param string $f060femail
     *
     * @return T060fstudent
     */
    public function setF060femail($f060femail)
    {
        $this->f060femail = $f060femail;

        return $this;
    }

    /**
     * Get f060femail
     *
     * @return string
     */
    public function getF060femail()
    {
        return $this->f060femail;
    }

    /**
     * Set f060ftype
     *
     * @param string $f060ftype
     *
     * @return T060fstudent
     */
    public function setF060ftype($f060ftype)
    {
        $this->f060ftype = $f060ftype;

        return $this;
    }

    /**
     * Get f060ftype
     *
     * @return string
     */
    public function getF060ftype()
    {
        return $this->f060ftype;
    }

    /**
     * Set f060fname
     *
     * @param string $f060fname
     *
     * @return T060fstudent
     */
    public function setF060fname($f060fname)
    {
        $this->f060fname = $f060fname;

        return $this;
    }

    /**
     * Get f060fname
     *
     * @return string
     */
    public function getF060fname()
    {
        return $this->f060fname;
    }

    /**
     * Set f060fphoneNumber
     *
     * @param string $f060fphoneNumber
     *
     * @return T060fstudent
     */
    public function setF060fphoneNumber($f060fphoneNumber)
    {
        $this->f060fphoneNumber = $f060fphoneNumber;

        return $this;
    }

    /**
     * Get f060fphoneNumber
     *
     * @return string
     */
    public function getF060fphoneNumber()
    {
        return $this->f060fphoneNumber;
    }

    /**
     * Set f060faddress
     *
     * @param string $f060faddress
     *
     * @return T060fstudent
     */
    public function setF060faddress($f060faddress)
    {
        $this->f060faddress = $f060faddress;

        return $this;
    }

    /**
     * Get f060faddress
     *
     * @return string
     */
    public function getF060faddress()
    {
        return $this->f060faddress;
    }

    /**
     * Set f060fdepartment
     *
     * @param string $f060fdepartment
     *
     * @return T060fstudent
     */
    public function setF060fdepartment($f060fdepartment)
    {
        $this->f060fdepartment = $f060fdepartment;

        return $this;
    }

    /**
     * Get f060fdepartment
     *
     * @return string
     */
    public function getF060fdepartment()
    {
        return $this->f060fdepartment;
    }
    

    /**
     * Set f060fcity
     *
     * @param string $f060fcity
     *
     * @return T060fstudent
     */
    public function setF060fcity($f060fcity)
    {
        $this->f060fcity = $f060fcity;

        return $this;
    }

    /**
     * Get f060fcity
     *
     * @return string
     */
    public function getF060fcity()
    {
        return $this->f060fcity;
    }

    /**
     * Set f060fstate
     *
     * @param integer $f060fstate
     *
     * @return T060fstudent
     */
    public function setF060fstate($f060fstate)
    {
        $this->f060fstate = $f060fstate;

        return $this;
    }

    /**
     * Get f060fstate
     *
     * @return integer
     */
    public function getF060fstate()
    {
        return $this->f060fstate;
    }

    /**
     * Set f060fgender
     *
     * @param string $f060fgender
     *
     * @return T060fstudent
     */
    public function setF060fgender($f060fgender)
    {
        $this->f060fgender = $f060fgender;

        return $this;
    }

    /**
     * Get f060fgender
     *
     * @return string
     */
    public function getF060fgender()
    {
        return $this->f060fgender;
    }

    /**
     * Set f060fidProgram
     *
     * @param integer $f060fidProgram
     *
     * @return T060fstudent
     */
    public function setF060fidProgram($f060fidProgram)
    {
        $this->f060fidProgram = $f060fidProgram;

        return $this;
    }

    /**
     * Get f060fidProgram
     *
     * @return integer
     */
    public function getF060fidProgram()
    {
        return $this->f060fidProgram;
    }

    /**
     * Set f060fidIntake
     *
     * @param integer $f060fidIntake
     *
     * @return T060fstudent
     */
    public function setF060fidIntake($f060fidIntake)
    {
        $this->f060fidIntake = $f060fidIntake;

        return $this;
    }

    /**
     * Get f060fidIntake
     *
     * @return integer
     */
    public function getF060fidIntake()
    {
        return $this->f060fidIntake;
    }

    /**
     * Set f060fidSemester
     *
     * @param integer $f060fidSemester
     *
     * @return T060fstudent
     */
    public function setF060fidSemester($f060fidSemester)
    {
        $this->f060fidSemester = $f060fidSemester;

        return $this;
    }

    /**
     * Get f060fidSemester
     *
     * @return integer
     */
    public function getF060fidSemester()
    {
        return $this->f060fidSemester;
    }

    /**
     * Set f060fstatus
     *
     * @param integer $f060fstatus
     *
     * @return T060fstudent
     */
    public function setF060fstatus($f060fstatus)
    {
        $this->f060fstatus = $f060fstatus;

        return $this;
    }

    /**
     * Get f060fstatus
     *
     * @return integer
     */
    public function getF060fstatus()
    {
        return $this->f060fstatus;
    }

    /**
     * Set f060fcreatedBy
     *
     * @param integer $f060fcreatedBy
     *
     * @return T060fstudent
     */
    public function setF060fcreatedBy($f060fcreatedBy)
    {
        $this->f060fcreatedBy = $f060fcreatedBy;

        return $this;
    }

    /**
     * Get f060fcreatedBy
     *
     * @return integer
     */
    public function getF060fcreatedBy()
    {
        return $this->f060fcreatedBy;
    }

    /**
     * Set f060fupdatedBy
     *
     * @param integer $f060fupdatedBy
     *
     * @return T060fstudent
     */
    public function setF060fupdatedBy($f060fupdatedBy)
    {
        $this->f060fupdatedBy = $f060fupdatedBy;

        return $this;
    }

    /**
     * Get f060fupdatedBy
     *
     * @return integer
     */
    public function getF060fupdatedBy()
    {
        return $this->f060fupdatedBy;
    }

    /**
     * Set f060fcreatedDtTm
     *
     * @param \DateTime $f060fcreatedDtTm
     *
     * @return T060fstudent
     */
    public function setF060fcreatedDtTm($f060fcreatedDtTm)
    {
        $this->f060fcreatedDtTm = $f060fcreatedDtTm;

        return $this;
    }

    /**
     * Get f060fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF060fcreatedDtTm()
    {
        return $this->f060fcreatedDtTm;
    }

    /**
     * Set f060fupdatedDtTm
     *
     * @param \DateTime $f060fupdatedDtTm
     *
     * @return T060fstudent
     */
    public function setF060fupdatedDtTm($f060fupdatedDtTm)
    {
        $this->f060fupdatedDtTm = $f060fupdatedDtTm;

        return $this;
    }

    /**
     * Get f060fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF060fupdatedDtTm()
    {
        return $this->f060fupdatedDtTm;
    }

    /**
     * Set f060fcountry
     *
     * @param integer $f060fcountry
     *
     * @return T060fstudent
     */
    public function setF060fcountry($f060fcountry)
    {
        $this->f060fcountry = $f060fcountry;

        return $this;
    }

    /**
     * Get f060fcountry
     *
     * @return integer
     */
    public function getF060fcountry()
    {
        return $this->f060fcountry;
    }

    /**
     * Set f060fcitizen
     *
     * @param integer $f060fcitizen
     *
     * @return T060fstudent
     */
    public function setF060fcitizen($f060fcitizen)
    {
        $this->f060fcitizen = $f060fcitizen;

        return $this;
    }

    /**
     * Get f060fcitizen
     *
     * @return integer
     */
    public function getF060fcitizen()
    {
        return $this->f060fcitizen;
    }

    /**
     * Set f060fregion
     *
     * @param integer $f060fregion
     *
     * @return T060fstudent
     */
    public function setF060fregion($f060fregion)
    {
        $this->f060fregion = $f060fregion;

        return $this;
    }

    /**
     * Get f060fregion
     *
     * @return integer
     */
    public function getF060fregion()
    {
        return $this->f060fregion;
    }

    /**
     * Set f060fstudentCategory
     *
     * @param integer $f060fstudentCategory
     *
     * @return T060fstudent
     */
    public function setF060fstudentCategory($f060fstudentCategory)
    {
        $this->f060fstudentCategory = $f060fstudentCategory;

        return $this;
    }

    /**
     * Get f060fstudentCategory
     *
     * @return integer
     */
    public function getF060fstudentCategory()
    {
        return $this->f060fstudentCategory;
    }

    /**
     * Set f060fstudyMode
     *
     * @param integer $f060fstudyMode
     *
     * @return T060fstudent
     */
    public function setF060fstudyMode($f060fstudyMode)
    {
        $this->f060fstudyMode = $f060fstudyMode;

        return $this;
    }

    /**
     * Get f060fstudyMode
     *
     * @return integer
     */
    public function getF060fstudyMode()
    {
        return $this->f060fstudyMode;
    }

    /**
     * Set f060fpassword
     *
     * @param string $f060fpassword
     *
     * @return T060fstudent
     */
    public function setF060fpassword($f060fpassword)
    {
        $this->f060fpassword = $f060fpassword;

        return $this;
    }

    /**
     * Get f060fpassword
     *
     * @return string
     */
    public function getF060fpassword()
    {
        return $this->f060fpassword;
    }

    /**
     * Set f060fmatricNumber
     *
     * @param string $f060fmatricNumber
     *
     * @return T060fstudent
     */
    public function setF060fmatricNumber($f060fmatricNumber)
    {
        $this->f060fmatricNumber = $f060fmatricNumber;

        return $this;
    }

    /**
     * Get f060fmatricNumber
     *
     * @return string
     */
    public function getF060fmatricNumber()
    {
        return $this->f060fmatricNumber;
    }

    /**
     * Set f060ficNumber
     *
     * @param string $f060ficNumber
     *
     * @return T060fstudent
     */
    public function setF060ficNumber($f060ficNumber)
    {
        $this->f060ficNumber = $f060ficNumber;

        return $this;
    }

    /**
     * Get f060ficNumber
     *
     * @return string
     */
    public function getF060ficNumber()
    {
        return $this->f060ficNumber;
    }

    /**
     * Set f060fprogramCode
     *
     * @param string $f060fprogramCode
     *
     * @return T060fstudent
     */
    public function setF060fprogramCode($f060fprogramCode)
    {
        $this->f060fprogramCode = $f060fprogramCode;

        return $this;
    }

    /**
     * Get f060fprogramCode
     *
     * @return string
     */
    public function getF060fprogramCode()
    {
        return $this->f060fprogramCode;
    }
}
