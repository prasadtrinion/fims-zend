<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T024fdiscount
 *
 * @ORM\Table(name="t024fdiscount")
 * @ORM\Entity(repositoryClass="Application\Repository\DiscountRepository")
 */
class T024fdiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f024fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f024fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fid_invoice", type="integer", nullable=true)
     */
    private $f024fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fid_financial_year", type="integer", nullable=true)
     */
    private $f024fidFinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fid_customer", type="integer", nullable=true)
     */
    private $f024fidCustomer;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024ftotal_amount", type="integer", nullable=true)
     */
    private $f024ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fbalance", type="integer", nullable=true)
     */
    private $f024fbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f024freference_number", type="string", length=50, nullable=true)
     */
    private $f024freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f024freason", type="string", length=50, nullable=true)
     */
    private $f024freason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f024fdate", type="datetime", nullable=true)
     */
    private $f024fdate;

    /**
     * @var string
     *
     * @ORM\Column(name="f024fdescription", type="string", length=10, nullable=true)
     */
    private $f024fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f024fdiscount_number", type="string", length=50, nullable=true)
     */
    private $f024fdiscountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fupdated_by", type="integer", nullable=true)
     */
    private $f024fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fcreated_by", type="integer", nullable=false)
     */
    private $f024fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fapproval_status", type="integer", nullable=true)
     */
    private $f024fapprovalStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f024fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f024fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f024fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f024fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fstatus", type="integer", nullable=true)
     */
    private $f024fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024ftype", type="string",length=20, nullable=true)
     */
    private $f024ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024frevenue_type", type="integer", nullable=true)
     */
    private $f024frevenueType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f024fdiscount_type", type="integer", nullable=false)
     */
    private $f024fdiscountType;

    /**
     * Get f024fid
     *
     * @return integer
     */
    public function getF024fid()
    {
        return $this->f024fid;
    }

    /**
     * Set f024frevenueType
     *
     * @param integer $f024frevenueType
     *
     * @return T024fdiscount
     */
    public function setF024frevenueType($f024frevenueType)
    {
        $this->f024frevenueType = $f024frevenueType;

        return $this;
    }


    /**
     * Get f024frevenueType
     *
     * @return integer
     */
    public function getF024frevenueType()
    {
        return $this->f024frevenueType;
    }

     /**
     * Set f024fdiscountType
     *
     * @param integer $f024fdiscountType
     *
     * @return T024fdiscountDetails
     */
    public function setF024fdiscountType($f024fdiscountType)
    {
        $this->f024fdiscountType = $f024fdiscountType;

        return $this;
    }

    /**
     * Get f024fdiscountType
     *
     * @return integer
     */
    public function getF024fdiscountType()
    {
        return $this->f024fdiscountType;
    }

    /**
     * Set f024fidInvoice
     *
     * @param integer $f024fidInvoice
     *
     * @return T024fdiscount
     */
    public function setF024fidInvoice($f024fidInvoice)
    {
        $this->f024fidInvoice = $f024fidInvoice;

        return $this;
    }


    /**
     * Get f024fidInvoice
     *
     * @return integer
     */
    public function getF024fidInvoice()
    {
        return $this->f024fidInvoice;
    }



    /**
     * Get f024ftype
     *
     * @return integer
     */
    public function getF024ftype()
    {
        return $this->f024ftype;
    }

    /**
     * Set f024ftype
     *
     * @param integer $f024ftype
     *
     * @return T024fdiscount
     */
    public function setF024ftype($f024ftype)
    {
        $this->f024ftype = $f024ftype;

        return $this;
    }




    /**
     * Set f024fidFinancialYear
     *
     * @param integer $f024fidFinancialYear
     *
     * @return T024fdiscount
     */
    public function setF024fidFinancialYear($f024fidFinancialYear)
    {
        $this->f024fidFinancialYear = $f024fidFinancialYear;

        return $this;
    }

    /**
     * Get f024fidFinancialYear
     *
     * @return integer
     */
    public function getF024fidFinancialYear()
    {
        return $this->f024fidFinancialYear;
    }

    /**
     * Set f024fidCustomer
     *
     * @param integer $f024fidCustomer
     *
     * @return T024fdiscount
     */
    public function setF024fidCustomer($f024fidCustomer)
    {
        $this->f024fidCustomer = $f024fidCustomer;

        return $this;
    }

    /**
     * Get f024fidCustomer
     *
     * @return integer
     */
    public function getF024fidCustomer()
    {
        return $this->f024fidCustomer;
    }

    /**
     * Set f024ftotalAmount
     *
     * @param float $f024ftotalAmount
     *
     * @return T024fdiscount
     */
    public function setF024ftotalAmount($f024ftotalAmount)
    {
        $this->f024ftotalAmount = $f024ftotalAmount;

        return $this;
    }

    /**
     * Get f024ftotalAmount
     *
     * @return float
     */
    public function getF024ftotalAmount()
    {
        return $this->f024ftotalAmount;
    }

    /**
     * Set f024fbalance
     *
     * @param float $f024fbalance
     *
     * @return T024fdiscount
     */
    public function setF024fbalance($f024fbalance)
    {
        $this->f024fbalance = $f024fbalance;

        return $this;
    }

    /**
     * Get f024fbalance
     *
     * @return float
     */
    public function getF024fbalance()
    {
        return $this->f024fbalance;
    }

    /**
     * Set f024freferenceNumber
     *
     * @param string $f024freferenceNumber
     *
     * @return T024fdiscount
     */
    public function setF024freferenceNumber($f024freferenceNumber)
    {
        $this->f024freferenceNumber = $f024freferenceNumber;

        return $this;
    }

    /**
     * Get f024freferenceNumber
     *
     * @return string
     */
    public function getF024freferenceNumber()
    {
        return $this->f024freferenceNumber;
    }

    /**
     * Set f024fdate
     *
     * @param \DateTime $f024fdate
     *
     * @return T024fdiscount
     */
    public function setF024fdate($f024fdate)
    {
        $this->f024fdate = $f024fdate;

        return $this;
    }

    /**
     * Get f024fdate
     *
     * @return \DateTime
     */
    public function getF024fdate()
    {
        return $this->f024fdate;
    }

    /**
     * Set f024fdescription
     *
     * @param string $f024fdescription
     *
     * @return T024fdiscount
     */
    public function setF024fdescription($f024fdescription)
    {
        $this->f024fdescription = $f024fdescription;

        return $this;
    }

    /**
     * Get f024fdescription
     *
     * @return string
     */
    public function getF024fdescription()
    {
        return $this->f024fdescription;
    }

    /**
     * Set f024fdiscountNumber
     *
     * @param string $f024fdiscountNumber
     *
     * @return T024fdiscount
     */
    public function setF024fdiscountNumber($f024fdiscountNumber)
    {
        $this->f024fdiscountNumber = $f024fdiscountNumber;

        return $this;
    }

    /**
     * Get f024fdiscountNumber
     *
     * @return string
     */
    public function getF024fdiscountNumber()
    {
        return $this->f024fdiscountNumber;
    }

    /**
     * Set f024fupdatedBy
     *
     * @param integer $f024fupdatedBy
     *
     * @return T024fdiscount
     */
    public function setF024fupdatedBy($f024fupdatedBy)
    {
        $this->f024fupdatedBy = $f024fupdatedBy;

        return $this;
    }

    /**
     * Get f024fupdatedBy
     *
     * @return integer
     */
    public function getF024fupdatedBy()
    {
        return $this->f024fupdatedBy;
    }

    /**
     * Set f024fcreatedBy
     *
     * @param integer $f024fcreatedBy
     *
     * @return T024fdiscount
     */
    public function setF024fcreatedBy($f024fcreatedBy)
    {
        $this->f024fcreatedBy = $f024fcreatedBy;

        return $this;
    }

    /**
     * Get f024fcreatedBy
     *
     * @return integer
     */
    public function getF024fcreatedBy()
    {
        return $this->f024fcreatedBy;
    }

    /**
     * Set f024fapprovalStatus
     *
     * @param integer $f024fapprovalStatus
     *
     * @return T024fdiscount
     */
    public function setF024fapprovalStatus($f024fapprovalStatus)
    {
        $this->f024fapprovalStatus = $f024fapprovalStatus;

        return $this;
    }

    /**
     * Get f024fapprovalStatus
     *
     * @return integer
     */
    public function getF024fapprovalStatus()
    {
        return $this->f024fapprovalStatus;
    }

    /**
     * Set f024fcreatedDtTm
     *
     * @param \DateTime $f024fcreatedDtTm
     *
     * @return T024fdiscount
     */
    public function setF024fcreatedDtTm($f024fcreatedDtTm)
    {
        $this->f024fcreatedDtTm = $f024fcreatedDtTm;

        return $this;
    }

    /**
     * Get f024fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF024fcreatedDtTm()
    {
        return $this->f024fcreatedDtTm;
    }

    /**
     * Set f024fupdatedDtTm
     *
     * @param \DateTime $f024fupdatedDtTm
     *
     * @return T024fdiscount
     */
    public function setF024fupdatedDtTm($f024fupdatedDtTm)
    {
        $this->f024fupdatedDtTm = $f024fupdatedDtTm;

        return $this;
    }

    /**
     * Get f024fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF024fupdatedDtTm()
    {
        return $this->f024fupdatedDtTm;
    }

    /**
     * Set f024fstatus
     *
     * @param integer $f024fstatus
     *
     * @return T024fdiscount
     */
    public function setF024fstatus($f024fstatus)
    {
        $this->f024fstatus = $f024fstatus;

        return $this;
    }

    /**
     * Get f024fstatus
     *
     * @return integer
     */
    public function getF024fstatus()
    {
        return $this->f024fstatus;
    }

    /**
     * Set f024freason
     *
     * @param integer $f024freason
     *
     * @return T024fdiscount
     */
    public function setF024freason($f024freason)
    {
        $this->f024freason = $f024freason;

        return $this;
    }

    /**
     * Get f024freason
     *
     * @return integer
     */
    public function getF024freason()
    {
        return $this->f024freason;
    }
}
