<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T074frequestInvoiceDetails
 *
 * @ORM\Table(name="t074frequest_invoice_details")
 * @ORM\Entity(repositoryClass="Application\Repository\RequestInvoiceRepository")
 */
class T074frequestInvoiceDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f072fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid_item", type="integer", nullable=false)
     */
    private $f072fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fgst_code", type="string", length=255, nullable=false)
     */
    private $f072fgstCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fgst_value", type="integer", nullable=false)
     */
    private $f072fgstValue;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f072fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f072fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f072fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f072fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f072fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f072fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f072fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f072fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f072fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f072fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f072fcreditAccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fquantity", type="integer", nullable=false)
     */
    private $f072fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fprice", type="integer", nullable=false)
     */
    private $f072fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072ftotal", type="integer", nullable=false)
     */
    private $f072ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fid_tax_code", type="integer", nullable=true)
     */
    private $f072fidTaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072ftotal_exc", type="integer", nullable=true)
     */
    private $f072ftotalExc;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072ftax_amount", type="integer", nullable=true)
     */
    private $f072ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fstatus", type="integer", nullable=false)
     */
    private $f072fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fcreated_by", type="integer", nullable=false)
     */
    private $f072fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f072fupdated_by", type="integer", nullable=false)
     */
    private $f072fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f072fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f072fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f072fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f072fupdatedDtTm;

    /**
     * @var \Application\Entity\T073frequestInvoice
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T073frequestInvoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f072fid_invoice", referencedColumnName="f071fid")
     * })
     */
    private $f072fidInvoice;

    /**
     * Get f072fid
     *
     * @return integer
     */
    public function getF072fid()
    {
        return $this->f072fid;
    }

    /**
     * Set f072fidItem
     *
     * @param integer $f072fidItem
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fidItem($f072fidItem)
    {
        $this->f072fidItem = $f072fidItem;

        return $this;
    }

    /**
     * Get f072fidItem
     *
     * @return integer
     */
    public function getF072fidItem()
    {
        return $this->f072fidItem;
    }

    /**
     * Set f072fgstCode
     *
     * @param string $f072fgstCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fgstCode($f072fgstCode)
    {
        $this->f072fgstCode = $f072fgstCode;

        return $this;
    }

    /**
     * Get f072fgstCode
     *
     * @return string
     */
    public function getF072fgstCode()
    {
        return $this->f072fgstCode;
    }


    /**
     * Set f072fgstValue
     *
     * @param integer $f072fgstValue
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fgstValue($f072fgstValue)
    {
        $this->f072fgstValue = $f072fgstValue;

        return $this;
    }

    /**
     * Get f072fgstValue
     *
     * @return integer
     */
    public function getF072fgstValue()
    {
        return $this->f072fgstValue;
    }

    /**
     * Set f072fdebitFundCode
     *
     * @param integer $f072fdebitFundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fdebitFundCode($f072fdebitFundCode)
    {
        $this->f072fdebitFundCode = $f072fdebitFundCode;

        return $this;
    }

    /**
     * Get f072fdebitFundCode
     *
     * @return integer
     */
    public function getF072fdebitFundCode()
    {
        return $this->f072fdebitFundCode;
    }

    /**
     * Set f072fdebitAccountCode
     *
     * @param integer $f072fdebitAccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fdebitAccountCode($f072fdebitAccountCode)
    {
        $this->f072fdebitAccountCode = $f072fdebitAccountCode;

        return $this;
    }

    /**
     * Get f072fdebitAccountCode
     *
     * @return integer
     */
    public function getF072fdebitAccountCode()
    {
        return $this->f072fdebitAccountCode;
    }

    /**
     * Set f072fdebitActivityCode
     *
     * @param integer $f072fdebitActivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fdebitActivityCode($f072fdebitActivityCode)
    {
        $this->f072fdebitActivityCode = $f072fdebitActivityCode;

        return $this;
    }

    /**
     * Get f072fdebitActivityCode
     *
     * @return integer
     */
    public function getF072fdebitActivityCode()
    {
        return $this->f072fdebitActivityCode;
    }

    /**
     * Set f072fdebitDepartmentCode
     *
     * @param integer $f072fdebitDepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fdebitDepartmentCode($f072fdebitDepartmentCode)
    {
        $this->f072fdebitDepartmentCode = $f072fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f072fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF072fdebitDepartmentCode()
    {
        return $this->f072fdebitDepartmentCode;
    }


    /**
     * Set f072fcreditFundCode
     *
     * @param integer $f072fcreditFundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreditFundCode($f072fcreditFundCode)
    {
        $this->f072fcreditFundCode = $f072fcreditFundCode;

        return $this;
    }

    /**
     * Get f072fcreditFundCode
     *
     * @return integer
     */
    public function getF072fcreditFundCode()
    {
        return $this->f072fcreditFundCode;
    }

    /**
     * Set f072fcreditAccountCode
     *
     * @param integer $f072fcreditAccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreditAccountCode($f072fcreditAccountCode)
    {
        $this->f072fcreditAccountCode = $f072fcreditAccountCode;

        return $this;
    }

    /**
     * Get f072fcreditAccountCode
     *
     * @return integer
     */
    public function getF072fcreditAccountCode()
    {
        return $this->f072fcreditAccountCode;
    }

    /**
     * Set f072fcreditActivityCode
     *
     * @param integer $f072fcreditActivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreditActivityCode($f072fcreditActivityCode)
    {
        $this->f072fcreditActivityCode = $f072fcreditActivityCode;

        return $this;
    }

    /**
     * Get f072fcreditActivityCode
     *
     * @return integer
     */
    public function getF072fcreditActivityCode()
    {
        return $this->f072fcreditActivityCode;
    }

    /**
     * Set f072fcreditDepartmentCode
     *
     * @param integer $f072fcreditDepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreditDepartmentCode($f072fcreditDepartmentCode)
    {
        $this->f072fcreditDepartmentCode = $f072fcreditDepartmentCode;

        return $this;
    }

    /**
     * Get f072fcreditDepartmentCode
     *
     * @return integer
     */
    public function getF072fcreditDepartmentCode()
    {
        return $this->f072fcreditDepartmentCode;
    }

    /**
     * Set f072fquantity
     *
     * @param integer $f072fquantity
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fquantity($f072fquantity)
    {
        $this->f072fquantity = $f072fquantity;

        return $this;
    }

    /**
     * Get f072fquantity
     *
     * @return integer
     */
    public function getF072fquantity()
    {
        return $this->f072fquantity;
    }

    /**
     * Set f072fprice
     *
     * @param integer $f072fprice
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fprice($f072fprice)
    {
        $this->f072fprice = (float)$f072fprice;

        return $this;
    }

    /**
     * Get f072fprice
     *
     * @return integer
     */
    public function getF072fprice()
    {
        return $this->f072fprice;
    }

    /**
     * Set f072ftotal
     *
     * @param integer $f072ftotal
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072ftotal($f072ftotal)
    {
        $this->f072ftotal = (float)$f072ftotal;

        return $this;
    }

    /**
     * Get f072ftotal
     *
     * @return integer
     */
    public function getF072ftotal()
    {
        return $this->f072ftotal;
    }

    /**
     * Set f072fstatus
     *
     * @param integer $f072fstatus
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fstatus($f072fstatus)
    {
        $this->f072fstatus = $f072fstatus;

        return $this;
    }

    /**
     * Get f072fstatus
     *
     * @return integer
     */
    public function getF072fstatus()
    {
        return $this->f072fstatus;
    }

    /**
     * Set f072fcreatedBy
     *
     * @param integer $f072fcreatedBy
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreatedBy($f072fcreatedBy)
    {
        $this->f072fcreatedBy = $f072fcreatedBy;

        return $this;
    }

    /**
     * Get f072fcreatedBy
     *
     * @return integer
     */
    public function getF072fcreatedBy()
    {
        return $this->f072fcreatedBy;
    }

    /**
     * Set f072fupdatedBy
     *
     * @param integer $f072fupdatedBy
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fupdatedBy($f072fupdatedBy)
    {
        $this->f072fupdatedBy = $f072fupdatedBy;

        return $this;
    }

    /**
     * Get f072fupdatedBy
     *
     * @return integer
     */
    public function getF072fupdatedBy()
    {
        return $this->f072fupdatedBy;
    }

    /**
     * Set f072fcreatedDtTm
     *
     * @param \DateTime $f072fcreatedDtTm
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fcreatedDtTm($f072fcreatedDtTm)
    {
        $this->f072fcreatedDtTm = $f072fcreatedDtTm;

        return $this;
    }

    /**
     * Get f072fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF072fcreatedDtTm()
    {
        return $this->f072fcreatedDtTm;
    }

    /**
     * Set f072fupdatedDtTm
     *
     * @param \DateTime $f072fupdatedDtTm
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fupdatedDtTm($f072fupdatedDtTm)
    {
        $this->f072fupdatedDtTm = $f072fupdatedDtTm;

        return $this;
    }

    /**
     * Get f072fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF072fupdatedDtTm()
    {
        return $this->f072fupdatedDtTm;
    }
    
    /**
     * Set f072fidInvoice
     *
     * @param \Application\Entity\T073frequestInvoice $f072fidInvoice
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fidInvoice(\Application\Entity\T073frequestInvoice $f072fidInvoice = null)
    {
        $this->f072fidInvoice = $f072fidInvoice;

        return $this;
    }

    /**
     * Get f072fidInvoice
     *
     * @return \Application\Entity\T073frequestInvoice
     */
    public function getF072fidInvoice()
    {
        return $this->f072fidInvoice;
    }
    /**
     * Set f072fidTaxCode
     *
     * @param integer $f072fidTaxCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072fidTaxCode($f072fidTaxCode)
    {
        $this->f072fidTaxCode = $f072fidTaxCode;

        return $this;
    }

    /**
     * Get f072fidTaxCode
     *
     * @return integer
     */
    public function getF072fidTaxCode()
    {
        return $this->f072fidTaxCode;
    }

    /**
     * Set f072ftotalExc
     *
     * @param integer $f072ftotalExc
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072ftotalExc($f072ftotalExc)
    {
        $this->f072ftotalExc = $f072ftotalExc;

        return $this;
    }

    /**
     * Get f072ftotalExc
     *
     * @return integer
     */
    public function getF072ftotalExc()
    {
        return $this->f072ftotalExc;
    }

    /**
     * Set f072ftaxAmount
     *
     * @param integer $f072ftaxAmount
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF072ftaxAmount($f072ftaxAmount)
    {
        $this->f072ftaxAmount = $f072ftaxAmount;

        return $this;
    }

    /**
     * Get f072ftaxAmount
     *
     * @return integer
     */
    public function getF072ftaxAmount()
    {
        return $this->f072ftaxAmount;
    }
}

