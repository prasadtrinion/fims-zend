<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
  
/**
 * T091fbillRegistration
 *
 * @ORM\Table(name="t091fbill_registration")
 * @ORM\Entity(repositoryClass="Application\Repository\BillRegistrationRepository")
 */
class T091fbillRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

     /**
     * @var string
     *
     * @ORM\Column(name="f091freference_number", type="string", length=50, nullable=true)
     */
    private $f091freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fapproved_status", type="integer", nullable=true)
     */
    private $f091fapprovedStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_date", type="datetime", nullable=true)
     */
    private $f091fcreatedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091ftotal_amount", type="integer", nullable=true)
     */
    private $f091ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=true)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=true)
     */
    private $f091fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=true)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f091fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f091ftype", type="string", length=50, nullable=true)
     */
    private $f091ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fpayment_status", type="integer", nullable=true)
     */
    private $f091fpaymentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fcontact_person1", type="string", length=100, nullable=true)
     */
    private $f091fcontactPerson1;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fcontact_person2", type="string", length=50, nullable=true)
     */
    private $f091fcontactPerson2;
     
    /**
     * @var string
     *
     * @ORM\Column(name="f091fcontact_person3", type="string", length=50, nullable=true)
     */
    private $f091fcontactPerson3;

    /**
     * @var string
     *
     * @ORM\Column(name="f091freason", type="string", length=50, nullable=true)
     */
    private $f091freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fid_vendor", type="integer", nullable=true)
     */
    private $f091fidVendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fbank_ac_no", type="string", length=255, nullable=true)
     */
    private $f091fbankAcNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f091femail", type="string", length=255, nullable=true)
     */
    private $f091femail;


    /**
     * @var string
     *
     * @ORM\Column(name="f091finvoice_date", type="datetime", nullable=true)
     */
    private $f091finvoiceDate;


    /**
     * @var string
     *
     * @ORM\Column(name="f091fdescription", type="string", length=255, nullable=true)
     */
    private $f091fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fvoucher_type", type="integer", nullable=true)
     */
    private $f091fvoucherType;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fuum_bank", type="integer", nullable=true)
     */
    private $f091fuumBank;


    

    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    
    /**
     * Set f091freferenceNumber
     *
     * @param string $f091freferenceNumber
     *
     * @return T091fbillRegistration
     */
    public function setF091freferenceNumber($f091freferenceNumber)
    {
        $this->f091freferenceNumber = $f091freferenceNumber;

        return $this;
    }

    /**
     * Get f091freferenceNumber
     *
     * @return string
     */
    public function getF091freferenceNumber()
    {
        return $this->f091freferenceNumber;
    }

     /**
     * Set f091freason
     *
     * @param string $f091freason
     *
     * @return T091fbillRegistration
     */
    public function setF091freason($f091freason)
    {
        $this->f091freason = $f091freason;

        return $this;
    }

    /**
     * Get f091freason
     *
     * @return string
     */
    public function getF091freason()
    {
        return $this->f091freason;
    }

    /**
     * Set f091fcreatedDate
     *
     * @param \DateTime $f091fcreatedDate
     *
     * @return T091fbillRegistration
     */
    public function setF091fcreatedDate($f091fcreatedDate)
    {
        $this->f091fcreatedDate = $f091fcreatedDate;

        return $this;
    }

    /**
     * Get f091fcreatedDate
     *
     * @return \DateTime
     */
    public function getF091fcreatedDate()
    {
        return $this->f091fcreatedDate;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fbillRegistration
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fapprovedStatus
     *
     * @param integer $f091fapprovedStatus
     *
     * @return T091fbillRegistration
     */
    public function setF091fapprovedStatus($f091fapprovedStatus)
    {
        $this->f091fapprovedStatus = $f091fapprovedStatus;

        return $this;
    }

    /**
     * Get f091fapprovedStatus
     *
     * @return integer
     */
    public function getF091fapprovedStatus()
    {
        return $this->f091fapprovedStatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fbillRegistration
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fbillRegistration
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091fbillRegistration
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T091fbillRegistration
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }

    /**
     * Set f091ftotalAmount
     *
     * @param string $f091ftotalAmount
     *
     * @return T091fbillRegistration
     */
    public function setF091ftotalAmount($f091ftotalAmount)
    {
        $this->f091ftotalAmount = $f091ftotalAmount;

        return $this;
    }

    /**
     * Get f091ftotalAmount
     *
     * @return string
     */
    public function getF091ftotalAmount()
    {
        return $this->f091ftotalAmount;
    }
    
     /**
     * Set f091ftype
     *
     * @param string $f091ftype
     *
     * @return T091fbillRegistration
     */
    public function setF091ftype($f091ftype)
    {
        $this->f091ftype = $f091ftype;

        return $this;
    }

    /**
     * Get f091ftype
     *
     * @return string
     */
    public function getF091ftype()
    {
        return $this->f091ftype;
    }


    /**
     * Set f091fpaymentStatus
     *
     * @param integer $f091fpaymentStatus
     *
     * @return T091fbillRegistration
     */
    public function setF091fpaymentStatus($f091fpaymentStatus)
    {
        $this->f091fpaymentStatus = $f091fpaymentStatus;

        return $this;
    }

    /**
     * Get f091fpaymentStatus
     *
     * @return integer
     */
    public function getF091fpaymentStatus()
    {
        return $this->f091fpaymentStatus;
    }


    /**
     * Set f091fcontactPerson1
     *
     * @param string $f091fcontactPerson1
     *
     * @return T091fbillRegistration
     */
    public function setF091fcontactPerson1($f091fcontactPerson1)
    {
        $this->f091fcontactPerson1 = $f091fcontactPerson1;

        return $this;
    }

    /**
     * Get f091fcontactPerson1
     *
     * @return string
     */
    public function getF091fcontactPerson1()
    {
        return $this->f091fcontactPerson1;
    }
    
    /**
     * Set f091fcontactPerson2
     *
     * @param string $f091fcontactPerson2
     *
     * @return T091fbillRegistration
     */
    public function setF091fcontactPerson2($f091fcontactPerson2)
    {
        $this->f091fcontactPerson2 = $f091fcontactPerson2;

        return $this;
    }

    /**
     * Get f091fcontactPerson2
     *
     * @return string
     */
    public function getF091fcontactPerson2()
    {
        return $this->f091fcontactPerson2;
    }
    

    /**
     * Set f091fcontactPerson3
     *
     * @param string $f091fcontactPerson3
     *
     * @return T091fbillRegistration
     */
    public function setF091fcontactPerson3($f091fcontactPerson3)
    {
        $this->f091fcontactPerson3 = $f091fcontactPerson3;

        return $this;
    }

    /**
     * Get f091fcontactPerson3
     *
     * @return string
     */
    public function getF091fcontactPerson3()
    {
        return $this->f091fcontactPerson3;
    }

     /**
     * Set f091fidVendor
     *
     * @param string $f091fidVendor
     *
     * @return T091fbillRegistration
     */
    public function setF091fidVendor($f091fidVendor)
    {
        $this->f091fidVendor = $f091fidVendor;

        return $this;
    }

    /**
     * Get f091fidVendor
     *
     * @return string
     */
    public function getF091fidVendor()
    {
        return $this->f091fidVendor;
    }

    /**
     * Set f091fbankAcNo
     *
     * @param integer $f091fbankAcNo
     *
     * @return T091fbillRegistration
     */
    public function setF091fbankAcNo($f091fbankAcNo)
    {
        $this->f091fbankAcNo = $f091fbankAcNo;

        return $this;
    }

    /**
     * Get f091fbankAcNo
     *
     * @return integer
     */
    public function getF091fbankAcNo()
    {
        return $this->f091fbankAcNo;
    }

    /**
     * Set f091femail
     *
     * @param string $f091femail
     *
     * @return T091fbillRegistration
     */
    public function setF091femail($f091femail)
    {
        $this->f091femail = $f091femail;

        return $this;
    }

    /**
     * Get f091femail
     *
     * @return string
     */
    public function getF091femail()
    {
        return $this->f091femail;
    }
    
    /**
     * Set f091finvoiceDate
     *
     * @param string $f091finvoiceDate
     *
     * @return T091fbillRegistration
     */
    public function setF091finvoiceDate($f091finvoiceDate)
    {
        $this->f091finvoiceDate = $f091finvoiceDate;

        return $this;
    }

    /**
     * Get f091finvoiceDate
     *
     * @return string
     */
    public function getF091finvoiceDate()
    {
        return $this->f091finvoiceDate;
    }
    

    /**
     * Set f091fdescription
     *
     * @param string $f091fdescription
     *
     * @return T091fbillRegistration
     */
    public function setF091fdescription($f091fdescription)
    {
        $this->f091fdescription = $f091fdescription;

        return $this;
    }

    /**
     * Get f091fdescription
     *
     * @return string
     */
    public function getF091fdescription()
    {
        return $this->f091fdescription;
    }
    

    /**
     * Set f091fvoucherType
     *
     * @param string $f091fvoucherType
     *
     * @return T091fbillRegistration
     */
    public function setF091voucherType($f091fvoucherType)
    {
        $this->f091fvoucherType = $f091fvoucherType;

        return $this;
    }

    /**
     * Get f091fvoucherType
     *
     * @return string
     */
    public function getF091fvoucherType()
    {
        return $this->f091fvoucherType;
    }


    /**
     * Set f091fuumBank
     *
     * @param string $f091fuumBank
     *
     * @return T091fbillRegistration
     */
    public function setF091fuumBank($f091fuumBank)
    {
        $this->f091fuumBank = $f091fuumBank;

        return $this;
    }

    /**
     * Get f091fuumBank
     *
     * @return string
     */
    public function getF091fuumBank()
    {
        return $this->f091fuumBank;
    }
}

