<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T132frevenueType
 *
 * @ORM\Table(name="t132frevenue_type")
 * @ORM\Entity(repositoryClass="Application\Repository\RevenueTypeRepository")
 */
class T132frevenueType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f132fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f132fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f132frevenue_name", type="string", length=255, nullable=false)
     */
    private $f132frevenueName;

    /**
     * @var string
     *
     * @ORM\Column(name="f132ftype_code", type="string", length=255, nullable=false)
     */
    private $f132ftypeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f132fid_category", type="integer", nullable=true)
     */
    private $f132fidCategory;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f132fstatus", type="integer", nullable=false)
     */
    private $f132fstatus = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f132fcreated_by", type="integer", nullable=false)
     */
    private $f132fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f132fupdated_by", type="integer", nullable=false)
     */
    private $f132fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f132fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f132fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f132fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f132fupdatedDtTm;



    /**
     * Get f132fid
     *
     * @return integer
     */
    public function getF132fid()
    {
        return $this->f132fid;
    }

    /**
     * Set f132frevenueName
     *
     * @param string $f132frevenueName
     *
     * @return T132frevenueType
     */
    public function setF132frevenueName($f132frevenueName)
    {
        $this->f132frevenueName = $f132frevenueName;

        return $this;
    }

    /**
     * Get f132frevenueName
     *
     * @return string
     */
    public function getF132frevenueName()
    {
        return $this->f132frevenueName;
    }

    /**
     * Set f132ftypeCode
     *
     * @param string $f132ftypeCode
     *
     * @return T132frevenueType
     */
    public function setF132ftypeCode($f132ftypeCode)
    {
        $this->f132ftypeCode = $f132ftypeCode;

        return $this;
    }

    /**
     * Get f132ftypeCode
     *
     * @return string
     */
    public function getF132ftypeCode()
    {
        return $this->f132ftypeCode;
    }

    /**
     * Set f132fidCategory
     *
     * @param string $f132fidCategory
     *
     * @return T132frevenueType
     */
    public function setF132fidCategory($f132fidCategory)
    {
        $this->f132fidCategory = $f132fidCategory;

        return $this;
    }

    /**
     * Get f132fidCategory
     *
     * @return string
     */
    public function getF132fidCategory()
    {
        return $this->f132fidCategory;
    }

    
    /**
     * Set f132fstatus
     *
     * @param boolean $f132fstatus
     *
     * @return T132frevenueType
     */
    public function setF132fstatus($f132fstatus)
    {
        $this->f132fstatus = $f132fstatus;

        return $this;
    }


    /**
     * Get f132fstatus
     *
     * @return boolean
     */
    public function getF132fstatus()
    {
        return $this->f132fstatus;
    }

    /**
     * Set f132fcreatedBy
     *
     * @param integer $f132fcreatedBy
     *
     * @return T132frevenueType
     */
    public function setF132fcreatedBy($f132fcreatedBy)
    {
        $this->f132fcreatedBy = $f132fcreatedBy;

        return $this;
    }

    /**
     * Get f132fcreatedBy
     *
     * @return integer
     */
    public function getF132fcreatedBy()
    {
        return $this->f132fcreatedBy;
    }

    /**
     * Set f132fupdatedBy
     *
     * @param integer $f132fupdatedBy
     *
     * @return T132frevenueType
     */
    public function setF132fupdatedBy($f132fupdatedBy)
    {
        $this->f132fupdatedBy = $f132fupdatedBy;

        return $this;
    }

    /**
     * Get f132fupdatedBy
     *
     * @return integer
     */
    public function getF132fupdatedBy()
    {
        return $this->f132fupdatedBy;
    }

    /**
     * Set f132fcreatedDtTm
     *
     * @param \DateTime $f132fcreatedDtTm
     *
     * @return T132frevenueType
     */
    public function setF132fcreatedDtTm($f132fcreatedDtTm)
    {
        $this->f132fcreatedDtTm = $f132fcreatedDtTm;

        return $this;
    }

    /**
     * Get f132fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF132fcreatedDtTm()
    {
        return $this->f132fcreatedDtTm;
    }

    /**
     * Set f132fupdatedDtTm
     *
     * @param \DateTime $f132fupdatedDtTm
     *
     * @return T132frevenueType
     */
    public function setF132fupdatedDtTm($f132fupdatedDtTm)
    {
        $this->f132fupdatedDtTm = $f132fupdatedDtTm;

        return $this;
    }

    /**
     * Get f132fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF132fupdatedDtTm()
    {
        return $this->f132fupdatedDtTm;
    }
}
