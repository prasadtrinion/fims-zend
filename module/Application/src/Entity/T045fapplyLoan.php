<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T045fapplyLoan
 *
 * @ORM\Table(name="t045fapply_loan")
 * @ORM\Entity(repositoryClass="Application\Repository\ApplyLoanRepository")
 */

class T045fapplyLoan

{
    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f045fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid_name", type="integer", nullable=true)
     */
    private $f045fidName;

    /**
     * @var string
     *
     * @ORM\Column(name="f045fdescription", type="string", length=100, nullable=true)
     */
    private $f045fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f045ffile", type="string", length=255, nullable=true)
     */
    private $f045ffile;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fno_of_months", type="integer", nullable=true)
     */
    private $f045fnoOfMonths;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fid_staff", type="integer", nullable=true)
     */
    private $f045fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fguarantee", type="integer",  nullable=true)
     */
    private $f045fguarantee;


    /**
     * @var integer
     *
     * @ORM\Column(name="f045fupdated_by", type="integer", nullable=true)
     */
    private $f045fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fcreated_by", type="integer", nullable=false)
     */
    private $f045fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f045fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f045fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f045fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fstatus", type="integer", nullable=true)
     */
    private $f045fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f045fapproval_status", type="integer", nullable=true)
     */
    private $f045fapprovalStatus;



    /**
     * Get f045fid
     *
     * @return integer
     */
    public function getF045fid()
    {
        return $this->f045fid;
    }

    /**
     * Set f045fidName
     *
     * @param integer $f045fidName
     *
     * @return T045fapplyLoan
     */
    public function setF045fidName($f045fidName)
    {
        $this->f045fidName = $f045fidName;

        return $this;
    }

    /**
     * Get f045fidName
     *
     * @return integer
     */
    public function getF045fidName()
    {
        return $this->f045fidName;
    }

    /**
     * Set f045fdescription
     *
     * @param string $f045fdescription
     *
     * @return T045fapplyLoan
     */
    public function setF045fdescription($f045fdescription)
    {
        $this->f045fdescription = $f045fdescription;

        return $this;
    }

    /**
     * Get f045fdescription
     *
     * @return string
     */
    public function getF045fdescription()
    {
        return $this->f045fdescription;
    }

    /**
     * Set f045ffile
     *
     * @param string $f045ffile
     *
     * @return T045fapplyLoan
     */
    public function setF045ffile($f045ffile)
    {
        $this->f045ffile = $f045ffile;

        return $this;
    }

    /**
     * Get f045ffile
     *
     * @return string
     */
    public function getF045ffile()
    {
        return $this->f045ffile;
    }

    /**
     * Set f045fnoOfMonths
     *
     * @param integer $f045fnoOfMonths
     *
     * @return T045fapplyLoan
     */
    public function setF045fnoOfMonths($f045fnoOfMonths)
    {
        $this->f045fnoOfMonths = $f045fnoOfMonths;

        return $this;
    }

    /**
     * Get f045fnoOfMonths
     *
     * @return integer
     */
    public function getF045fnoOfMonths()
    {
        return $this->f045fnoOfMonths;
    }

    /**
     * Set f045fidStaff
     *
     * @param integer $f045fidStaff
     *
     * @return T045fapplyLoan
     */
    public function setF045fidStaff($f045fidStaff)
    {
        $this->f045fidStaff = $f045fidStaff;

        return $this;
    }

    /**
     * Get f045fidStaff
     *
     * @return integer
     */
    public function getF045fidStaff()
    {
        return $this->f045fidStaff;
    }

    /**
     * Set f045fguarantee
     *
     * @param integer $f045fguarantee
     *
     * @return T045fapplyLoan
     */
    public function setF045fguarantee($f045fguarantee)
    {
        $this->f045fguarantee = $f045fguarantee;

        return $this;
    }

    /**
     * Get f045fguarantee
     *
     * @return integer
     */
    public function getF045fguarantee()
    {
        return $this->f045fguarantee;
    }

    /**
     * Set f045fupdatedBy
     *
     * @param integer $f045fupdatedBy
     *
     * @return T045fapplyLoan
     */
    public function setF045fupdatedBy($f045fupdatedBy)
    {
        $this->f045fupdatedBy = $f045fupdatedBy;

        return $this;
    }

    /**
     * Get f045fupdatedBy
     *
     * @return integer
     */
    public function getF045fupdatedBy()
    {
        return $this->f045fupdatedBy;
    }

    /**
     * Set f045fcreatedBy
     *
     * @param integer $f045fcreatedBy
     *
     * @return T045fapplyLoan
     */
    public function setF045fcreatedBy($f045fcreatedBy)
    {
        $this->f045fcreatedBy = $f045fcreatedBy;

        return $this;
    }

    /**
     * Get f045fcreatedBy
     *
     * @return integer
     */
    public function getF045fcreatedBy()
    {
        return $this->f045fcreatedBy;
    }

    /**
     * Set f045fcreatedDtTm
     *
     * @param \DateTime $f045fcreatedDtTm
     *
     * @return T045fapplyLoan
     */
    public function setF045fcreatedDtTm($f045fcreatedDtTm)
    {
        $this->f045fcreatedDtTm = $f045fcreatedDtTm;

        return $this;
    }

    /**
     * Get f045fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF045fcreatedDtTm()
    {
        return $this->f045fcreatedDtTm;
    }

    /**
     * Set f045fupdatedDtTm
     *
     * @param \DateTime $f045fupdatedDtTm
     *
     * @return T045fapplyLoan
     */
    public function setF045fupdatedDtTm($f045fupdatedDtTm)
    {
        $this->f045fupdatedDtTm = $f045fupdatedDtTm;

        return $this;
    }

    /**
     * Get f045fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF045fupdatedDtTm()
    {
        return $this->f045fupdatedDtTm;
    }

    /**
     * Set f045fstatus
     *
     * @param integer $f045fstatus
     *
     * @return T045fapplyLoan
     */
    public function setF045fstatus($f045fstatus)
    {
        $this->f045fstatus = $f045fstatus;

        return $this;
    }

    /**
     * Get f045fstatus
     *
     * @return integer
     */
    public function getF045fstatus()
    {
        return $this->f045fstatus;
    }

    /**
     * Set f045fapprovalStatus
     *
     * @param integer $f045fapprovalStatus
     *
     * @return T045fapplyLoan
     */
    public function setF045fapprovalStatus($f045fapprovalStatus)
    {
        $this->f045fapprovalStatus = $f045fapprovalStatus;

        return $this;
    }

    /**
     * Get f045fapprovalStatus
     *
     * @return integer
     */
    public function getF045fapprovalStatus()
    {
        return $this->f045fapprovalStatus;
    }
}
