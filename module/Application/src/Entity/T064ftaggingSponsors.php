<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064ftaggingSponsors
 *
 * @ORM\Table(name="t064ftagging_sponsors")
 * @ORM\Entity(repositoryClass="Application\Repository\TaggingSponsorRepository")
 */
class T064ftaggingSponsors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_sponsor", type="integer", nullable=false)
     */
    private $f064fidSponsor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_student", type="integer", nullable=false)
     */
    private $f064fidStudent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_semester", type="integer", nullable=false)
     */
    private $f064fidSemester;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fagreement_no", type="string", length=20, nullable=true)
     */
    private $f064fagreementNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f064ffile_upload", type="string", length=200, nullable=true)
     */
    private $f064ffileUpload;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fstart_date", type="datetime", nullable=true)
     */
    private $f064fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fend_date", type="datetime", nullable=true)
     */
    private $f064fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=true)
     */
    private $f064fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fmetric_number", type="string",length=55, nullable=true)
     */
    private $f064fmetricNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fsponsor_code", type="string", length=50, nullable=true)
     */
    private $f064fsponsorCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fpassport_number", type="string", length=50, nullable=true)
     */
    private $f064fpassportNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fdocument_ref_no", type="string", length=50, nullable=true)
     */
    private $f064fdocumentRefNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fpath", type="string", length=255, nullable=true)
     */
    private $f064fpath;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=true)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=true)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fextension_date", type="datetime", nullable=true)
     */
    private $f064fextensionDate;



    /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064fidSponsor
     *
     * @param integer $f064fidSponsor
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fidSponsor($f064fidSponsor)
    {
        $this->f064fidSponsor = $f064fidSponsor;

        return $this;
    }

    /**
     * Get f064fidSponsor
     *
     * @return integer
     */
    public function getF064fidSponsor()
    {
        return $this->f064fidSponsor;
    }

    /**
     * Set f064fidStudent
     *
     * @param integer $f064fidStudent
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fidStudent($f064fidStudent)
    {
        $this->f064fidStudent = $f064fidStudent;

        return $this;
    }

    /**
     * Get f064fidStudent
     *
     * @return integer
     */
    public function getF064fidStudent()
    {
        return $this->f064fidStudent;
    }

    /**
     * Set f064fidSemester
     *
     * @param integer $f064fidSemester
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fidSemester($f064fidSemester)
    {
        $this->f064fidSemester = $f064fidSemester;

        return $this;
    }

    /**
     * Get f064fidSemester
     *
     * @return integer
     */
    public function getF064fidSemester()
    {
        return $this->f064fidSemester;
    }

    /**
     * Set f064fagreementNo
     *
     * @param string $f064fagreementNo
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fagreementNo($f064fagreementNo)
    {
        $this->f064fagreementNo = $f064fagreementNo;

        return $this;
    }

    /**
     * Get f064fagreementNo
     *
     * @return string
     */
    public function getF064fagreementNo()
    {
        return $this->f064fagreementNo;
    }

    /**
     * Set f064ffileUpload
     *
     * @param string $f064ffileUpload
     *
     * @return T064ftaggingSponsors
     */
    public function setF064ffileUpload($f064ffileUpload)
    {
        $this->f064ffileUpload = $f064ffileUpload;

        return $this;
    }

    /**
     * Get f064ffileUpload
     *
     * @return string
     */
    public function getF064ffileUpload()
    {
        return $this->f064ffileUpload;
    }

    /**
     * Set f064fstartDate
     *
     * @param \DateTime $f064fstartDate
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fstartDate($f064fstartDate)
    {
        $this->f064fstartDate = $f064fstartDate;

        return $this;
    }

    /**
     * Get f064fstartDate
     *
     * @return \DateTime
     */
    public function getF064fstartDate()
    {
        return $this->f064fstartDate;
    }

    /**
     * Set f064fendDate
     *
     * @param \DateTime $f064fendDate
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fendDate($f064fendDate)
    {
        $this->f064fendDate = $f064fendDate;

        return $this;
    }

    /**
     * Get f064fendDate
     *
     * @return \DateTime
     */
    public function getF064fendDate()
    {
        return $this->f064fendDate;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fmetricNumber
     *
     * @param integer $f064fmetricNumber
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fmetricNumber($f064fmetricNumber)
    {
        $this->f064fmetricNumber = $f064fmetricNumber;

        return $this;
    }

    /**
     * Get f064fmetricNumber
     *
     * @return integer
     */
    public function getF064fmetricNumber()
    {
        return $this->f064fmetricNumber;
    }

    /**
     * Set f064fsponsorCode
     *
     * @param string $f064fsponsorCode
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fsponsorCode($f064fsponsorCode)
    {
        $this->f064fsponsorCode = $f064fsponsorCode;

        return $this;
    }

    /**
     * Get f064fsponsorCode
     *
     * @return string
     */
    public function getF064fsponsorCode()
    {
        return $this->f064fsponsorCode;
    }

    /**
     * Set f064fpassportNumber
     *
     * @param string $f064fpassportNumber
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fpassportNumber($f064fpassportNumber)
    {
        $this->f064fpassportNumber = $f064fpassportNumber;

        return $this;
    }

    /**
     * Get f064fpassportNumber
     *
     * @return string
     */
    public function getF064fpassportNumber()
    {
        return $this->f064fpassportNumber;
    }

    /**
     * Set f064fdocumentRefNo
     *
     * @param string $f064fdocumentRefNo
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fdocumentRefNo($f064fdocumentRefNo)
    {
        $this->f064fdocumentRefNo = $f064fdocumentRefNo;

        return $this;
    }

    /**
     * Get f064fdocumentRefNo
     *
     * @return string
     */
    public function getF064fdocumentRefNo()
    {
        return $this->f064fdocumentRefNo;
    }
    
    /**
     * Set f064fpath
     *
     * @param string $f064fpath
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fpath($f064fpath)
    {
        $this->f064fpath = $f064fpath;

        return $this;
    }

    /**
     * Get f064fpath
     *
     * @return string
     */
    public function getF064fpath()
    {
        return $this->f064fpath;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fcreatedDtTm
     *
     * @param \DateTime $f064fcreatedDtTm
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fcreatedDtTm($f064fcreatedDtTm)
    {
        $this->f064fcreatedDtTm = $f064fcreatedDtTm;

        return $this;
    }

    /**
     * Get f064fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreatedDtTm()
    {
        return $this->f064fcreatedDtTm;
    }

    /**
     * Set f064fupdatedDtTm
     *
     * @param \DateTime $f064fupdatedDtTm
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fupdatedDtTm($f064fupdatedDtTm)
    {
        $this->f064fupdatedDtTm = $f064fupdatedDtTm;

        return $this;
    }

    /**
     * Get f064fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdatedDtTm()
    {
        return $this->f064fupdatedDtTm;
    }

    /**
     * Set f064fextensionDate
     *
     * @param \DateTime $f064fextensionDate
     *
     * @return T064ftaggingSponsors
     */
    public function setF064fextensionDate($f064fextensionDate)
    {
        $this->f064fextensionDate = $f064fextensionDate;

        return $this;
    }

    /**
     * Get f064fextensionDate
     *
     * @return \DateTime
     */
    public function getF064fextensionDate()
    {
        return $this->f064fextensionDate;
    }
}
