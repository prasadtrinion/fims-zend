<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T044floanTypeSetup
 *
 * @ORM\Table(name="t044floan_type_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\LoanTypeSetupRepository")
 */
class T044floanTypeSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f044fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid_loan", type="integer", nullable=false)
     */
    private $f044fidLoan;

    /**
     * @var string
     *
     * @ORM\Column(name="f044femp_type", type="string", length=20, nullable=false)
     */
    private $f044fempType;

   

    /**
     * @var string
     *
     * @ORM\Column(name="f044femp_status", type="string", length=50, nullable=false)
     */
    private $f044fempStatus;
    

    /**
     * @var string
     *
     * @ORM\Column(name="f044fguarantee1", type="string", length=50, nullable=true)
     */
    private $f044fguarantee1;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fguarantee2", type="string", length=50, nullable=true)
     */
    private $f044fguarantee2='NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fno_guarantee", type="integer", nullable=false)
     */
    private $f044fnoGuarantee='NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fstatus", type="integer", nullable=false)
     */
    private $f044fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fcreated_by", type="integer", nullable=false)
     */
    private $f044fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fupdated_by", type="integer", nullable=false)
     */
    private $f044fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f044fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f044fupdatedDtTm;



    /**
     * Get f044fid
     *
     * @return integer
     */
    public function getF044fid()
    {
        return $this->f044fid;
    }

    /**
     * Set f044fidLoan
     *
     * @param integer $f044fidLoan
     *
     * @return T044floanTypeSetup
     */
    public function setF044fidLoan($f044fidLoan)
    {
        $this->f044fidLoan = $f044fidLoan;

        return $this;
    }

    

    /**
     * Get f044fidLoan
     *
     * @return integer
     */
    public function getF044fidLoan()
    {
        return $this->f044fidLoan;
    }

    /**
     * Set f044fempType
     *
     * @param string $f044fempType
     *
     * @return T044floanTypeSetup
     */
    public function setF044fempType($f044fempType)
    {
        $this->f044fempType = $f044fempType;

        return $this;
    }

    /**
     * Get f044fempType
     *
     * @return string
     */
    public function getF044fempType()
    {
        return $this->f044fempType;
    }

    /**
     * Set f044fempStatus
     *
     * @param string $f044fempStatus
     *
     * @return T044floanTypeSetup
     */
    public function setF044fempStatus($f044fempStatus)
    {
        $this->f044fempStatus = $f044fempStatus;

        return $this;
    }

    /**
     * Get f044fempStatus
     *
     * @return string
     */
    public function getF044fempStatus()
    {
        return $this->f044fempStatus;
    }

    /**
     * Set f044fguarantee1
     *
     * @param string $f044fguarantee1
     *
     * @return T044floanTypeSetup
     */
    public function setF044fguarantee1($f044fguarantee1)
    {
        $this->f044fguarantee1 = $f044fguarantee1;

        return $this;
    }

    /**
     * Get f044fguarantee1
     *
     * @return integer
     */
    public function getF044fguarantee1()
    {
        return $this->f044fguarantee1;
    }

    /**
     * Set f044fguarantee2
     *
     * @param string $f044fguarantee2
     *
     * @return T044floanTypeSetup
     */
    public function setF044fguarantee2($f044fguarantee2)
    {
        $this->f044fguarantee2 = $f044fguarantee2;

        return $this;
    }

    /**
     * Get f044fguarantee2
     *
     * @return string
     */
    public function getF044fguarantee2()
    {
        return $this->f044fguarantee2;
    }

    /**
     * Set f044fnoGuarantee
     *
     * @param string $f044fnoGuarantee
     *
     * @return T044floanTypeSetup
     */
    public function setF044fnoGuarantee($f044fnoGuarantee)
    {
        $this->f044fnoGuarantee = $f044fnoGuarantee;

        return $this;
    }

    /**
     * Get f044fnoGuarantee
     *
     * @return integer
     */
    public function getF044fnoGuarantee()
    {
        return $this->f044fnoGuarantee;
    }

    /**
     * Set f044fstatus
     *
     * @param integer $f044fstatus
     *
     * @return T044floanTypeSetup
     */
    public function setF044fstatus($f044fstatus)
    {
        $this->f044fstatus = $f044fstatus;

        return $this;
    }

    /**
     * Get f044fstatus
     *
     * @return integer
     */
    public function getF044fstatus()
    {
        return $this->f044fstatus;
    }

    /**
     * Set f044fcreatedBy
     *
     * @param integer $f044fcreatedBy
     *
     * @return T044floanTypeSetup
     */
    public function setF044fcreatedBy($f044fcreatedBy)
    {
        $this->f044fcreatedBy = $f044fcreatedBy;

        return $this;
    }

    /**
     * Get f044fcreatedBy
     *
     * @return integer
     */
    public function getF044fcreatedBy()
    {
        return $this->f044fcreatedBy;
    }

    /**
     * Set f044fupdatedBy
     *
     * @param integer $f044fupdatedBy
     *
     * @return T044floanTypeSetup
     */
    public function setF044fupdatedBy($f044fupdatedBy)
    {
        $this->f044fupdatedBy = $f044fupdatedBy;

        return $this;
    }

    /**
     * Get f044fupdatedBy
     *
     * @return integer
     */
    public function getF044fupdatedBy()
    {
        return $this->f044fupdatedBy;
    }

    /**
     * Set f044fcreatedDtTm
     *
     * @param \DateTime $f044fcreatedDtTm
     *
     * @return T044floanTypeSetup
     */
    public function setF044fcreatedDtTm($f044fcreatedDtTm)
    {
        $this->f044fcreatedDtTm = $f044fcreatedDtTm;

        return $this;
    }

    /**
     * Get f044fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fcreatedDtTm()
    {
        return $this->f044fcreatedDtTm;
    }

    /**
     * Set f044fupdatedDtTm
     *
     * @param \DateTime $f044fupdatedDtTm
     *
     * @return T044floanTypeSetup
     */
    public function setF044fupdatedDtTm($f044fupdatedDtTm)
    {
        $this->f044fupdatedDtTm = $f044fupdatedDtTm;

        return $this;
    }

    /**
     * Get f044fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fupdatedDtTm()
    {
        return $this->f044fupdatedDtTm;
    }
}
