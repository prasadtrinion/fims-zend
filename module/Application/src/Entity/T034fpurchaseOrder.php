<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T034fpurchaseOrder
 *
 * @ORM\Table(name="t034fpurchase_order")
 * @ORM\Entity(repositoryClass="Application\Repository\PurchaseOrderRepository")
 */
class T034fpurchaseOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f034fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f034forder_type", type="string",length=20, nullable=true)
     */
    private $f034forderType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034forder_date", type="datetime", nullable=false)
     */
    private $f034forderDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_financialyear", type="integer", nullable=true)
     */
    private $f034fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_supplier", type="integer", nullable=true)
     */
    private $f034fidSupplier;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fdescription", type="string", length=100, nullable=true)
     */
    private $f034fdescription;

     /**
     * @var string
     *
     * @ORM\Column(name="f034freference_number", type="string", length=50, nullable=true)
     */
    private $f034freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fid_department", type="string",length=50, nullable=true)
     */
    private $f034fidDepartment;


    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_purchasereq", type="integer", nullable=true)
     */
    private $f034fidPurchasereq;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fexpired_date", type="datetime", nullable=false)
     */
    private $f034fexpiredDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fstatus", type="integer", nullable=true)
     */
    private $f034fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fcreated_by", type="integer", nullable=true)
     */
    private $f034fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fupdated_by", type="integer", nullable=true)
     */
    private $f034fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fapproval_status", type="integer", nullable=true)
     */
    private $f034fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034ftotal_amount", type="integer", nullable=true)
     */
    private $f034ftotalAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f034freason", type="string",length=10, nullable=true)
     */
    private $f034freason;

    
    /**
     * @var string
     *
     * @ORM\Column(name="f034fprint_date", type="datetime", nullable=true)
     */
    private $f034fprintDate;


    /**
     * @var string
     *
     * @ORM\Column(name="f034fre_print_date", type="datetime", nullable=true)
     */
    private $f034frePrintDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fprint_count", type="integer", nullable=true)
     */
    private $f034fprintCount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f034fprinted_by", type="integer", nullable=true)
     */
    private $f034fprintedBy;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fre_printed_by", type="integer", nullable=true)
     */
    private $f034frePrintedBy;




    /**
     * Get f034fid
     *
     * @return integer
     */
    public function getF034fid()
    {
        return $this->f034fid;
    }

    /**
     * Set f034forderType
     *
     * @param string $f034forderType
     *
     * @return T034fpurchaseOrder
     */
    public function setF034forderType($f034forderType)
    {
        $this->f034forderType = $f034forderType;

        return $this;
    }

    /**
     * Get f034forderType
     *
     * @return string
     */
    public function getF034forderType()
    {
        return $this->f034forderType;
    }

    /**
     * Set f034forderDate
     *
     * @param \DateTime $f034forderDate
     *
     * @return T034fpurchaseOrder
     */
    public function setF034forderDate($f034forderDate)
    {
        $this->f034forderDate = $f034forderDate;

        return $this;
    }

    /**
     * Get f034forderDate
     *
     * @return \DateTime
     */
    public function getF034forderDate()
    {
        return $this->f034forderDate;
    }

    /**
     * Set f034fidFinancialyear
     *
     * @param integer $f034fidFinancialyear
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fidFinancialyear($f034fidFinancialyear)
    {
        $this->f034fidFinancialyear = $f034fidFinancialyear;

        return $this;
    }

    /**
     * Get f034fidFinancialyear
     *
     * @return integer
     */
    public function getF034fidFinancialyear()
    {
        return $this->f034fidFinancialyear;
    }

    /**
     * Set f034fidPurchasereq
     *
     * @param integer $f034fidPurchasereq
     *
     * @return T034fpurchaseOrder
     */
    public function setf034fidPurchasereq($f034fidPurchasereq)
    {
        $this->f034fidPurchasereq = $f034fidPurchasereq;

        return $this;
    }

    /**
     * Get f034fidPurchasereq
     *
     * @return integer
     */
    public function getf034fidPurchasereq()
    {
        return $this->f034fidPurchasereq;
    }

    /**
     * Set f034fidSupplier
     *
     * @param integer $f034fidSupplier
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fidSupplier($f034fidSupplier)
    {
        $this->f034fidSupplier = $f034fidSupplier;

        return $this;
    }

    /**
     * Get f034fidSupplier
     *
     * @return integer
     */
    public function getF034fidSupplier()
    {
        return $this->f034fidSupplier;
    }

    /**
     * Set f034fdescription
     *
     * @param string $f034fdescription
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fdescription($f034fdescription)
    {
        $this->f034fdescription = $f034fdescription;

        return $this;
    }

    /**
     * Get f034fdescription
     *
     * @return string
     */
    public function getF034fdescription()
    {
        return $this->f034fdescription;
    }

    /**
     * Set f034fidDepartment
     *
     * @param integer $f034fidDepartment
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fidDepartment($f034fidDepartment)
    {
        $this->f034fidDepartment = $f034fidDepartment;

        return $this;
    }

    /**
     * Get f034fidDepartment
     *
     * @return integer
     */
    public function getF034fidDepartment()
    {
        return $this->f034fidDepartment;
    }

    /**
     * Set f034freferenceNumber
     *
     * @param string $f034freferenceNumber
     *
     * @return T034fpurchaseOrder
     */
    public function setF034freferenceNumber($f034freferenceNumber)
    {
        $this->f034freferenceNumber = $f034freferenceNumber;

        return $this;
    }

    /**
     * Get f034freferenceNumber
     *
     * @return string
     */
    public function getF034freferenceNumber()
    {
        return $this->f034freferenceNumber;
    }

    /**
     * Set f034fexpiredDate
     *
     * @param \DateTime $f034fexpiredDate
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fexpiredDate($f034fexpiredDate)
    {
        $this->f034fexpiredDate = $f034fexpiredDate;

        return $this;
    }

    /**
     * Get f034fexpiredDate
     *
     * @return \DateTime
     */
    public function getF034fexpiredDate()
    {
        return $this->f034fexpiredDate;
    }

    /**
     * Set f034fstatus
     *
     * @param integer $f034fstatus
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fstatus($f034fstatus)
    {
        $this->f034fstatus = $f034fstatus;

        return $this;
    }

    /**
     * Get f034fstatus
     *
     * @return integer
     */
    public function getF034fstatus()
    {
        return $this->f034fstatus;
    }

    /**
     * Set f034fapprovalStatus
     *
     * @param integer $f034fapprovalStatus
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fapprovalStatus($f034fapprovalStatus)
    {
        $this->f034fapprovalStatus = $f034fapprovalStatus;

        return $this;
    }

    /**
     * Get f034fapprovalStatus
     *
     * @return integer
     */
    public function getF034fapprovalStatus()
    {
        return $this->f034fapprovalStatus;
    }

    /**
     * Set f034fcreatedBy
     *
     * @param integer $f034fcreatedBy
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fcreatedBy($f034fcreatedBy)
    {
        $this->f034fcreatedBy = $f034fcreatedBy;

        return $this;
    }

    /**
     * Get f034fcreatedBy
     *
     * @return integer
     */
    public function getF034fcreatedBy()
    {
        return $this->f034fcreatedBy;
    }

    /**
     * Set f034fupdatedBy
     *
     * @param integer $f034fupdatedBy
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fupdatedBy($f034fupdatedBy)
    {
        $this->f034fupdatedBy = $f034fupdatedBy;

        return $this;
    }

    /**
     * Get f034fupdatedBy
     *
     * @return integer
     */
    public function getF034fupdatedBy()
    {
        return $this->f034fupdatedBy;
    }

    /**
     * Set f034fcreatedDtTm
     *
     * @param \DateTime $f034fcreatedDtTm
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fcreatedDtTm($f034fcreatedDtTm)
    {
        $this->f034fcreatedDtTm = $f034fcreatedDtTm;

        return $this;
    }

    /**
     * Get f034fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fcreatedDtTm()
    {
        return $this->f034fcreatedDtTm;
    }

    /**
     * Set f034fupdatedDtTm
     *
     * @param \DateTime $f034fupdatedDtTm
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fupdatedDtTm($f034fupdatedDtTm)
    {
        $this->f034fupdatedDtTm = $f034fupdatedDtTm;

        return $this;
    }

    /**
     * Get f034fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fupdatedDtTm()
    {
        return $this->f034fupdatedDtTm;
    }

    /**
     * Set f034ftotalAmount
     *
     * @param integer $f034ftotalAmount
     *
     * @return T034fpurchaseOrder
     */
    public function setF034ftotalAmount($f034ftotalAmount)
    {
        $this->f034ftotalAmount = $f034ftotalAmount;

        return $this;
    }

    /**
     * Get f034ftotalAmount
     *
     * @return integer
     */
    public function getF034ftotalAmount()
    {
        return $this->f034ftotalAmount;
    }

    /**
     * Set f034freason
     *
     * @param string $f034freason
     *
     * @return T034fpurchaseOrder
     */
    public function setF034freason($f034freason)
    {
        $this->f034freason = $f034freason;

        return $this;
    }

    /**
     * Get f034freason
     *
     * @return string
     */
    public function getF034freason()
    {
        return $this->f034freason;
    }

    /**
     * Set f034fprintDate
     *
     * @param string $f034fprintDate
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fprintDate($f034fprintDate)
    {
        $this->f034fprintDate = $f034fprintDate;

        return $this;
    }

    /**
     * Get f034fprintDate
     *
     * @return string
     */
    public function getF034fprintDate()
    {
        return $this->f034fprintDate;
    }

    /**
     * Set f034frePrintDate
     *
     * @param string $f034frePrintDate
     *
     * @return T034fpurchaseOrder
     */
    public function setF034frePrintDate($f034frePrintDate)
    {
        $this->f034frePrintDate = $f034frePrintDate;

        return $this;
    }

    /**
     * Get f034frePrintDate
     *
     * @return string
     */
    public function getF034frePrintDate()
    {
        return $this->f034frePrintDate;
    }

    /**
     * Set f034fprintCount
     *
     * @param string $f034fprintCount
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fprintCount($f034fprintCount)
    {
        $this->f034fprintCount = $f034fprintCount;

        return $this;
    }

    /**
     * Get f034fprintCount
     *
     * @return string
     */
    public function getF034fprintCount()
    {
        return $this->f034fprintCount;
    }

    /**
     * Set f034fprintedBy
     *
     * @param string $f034fprintedBy
     *
     * @return T034fpurchaseOrder
     */
    public function setF034fprintedBy($f034fprintedBy)
    {
        $this->f034fprintedBy = $f034fprintedBy;

        return $this;
    }

    /**
     * Get f034fprintedBy
     *
     * @return string
     */
    public function getF034fprintedBy()
    {
        return $this->f034fprintedBy;
    }



    /**
     * Set f034frePrintedBy
     *
     * @param string $f034frePrintedBy
     *
     * @return T034fpurchaseOrder
     */
    public function setF034frePrintedBy($f034frePrintedBy)
    {
        $this->f034frePrintedBy = $f034frePrintedBy;

        return $this;
    }

    /**
     * Get f034frePrintedBy
     *
     * @return string
     */
    public function getF034frePrintedBy()
    {
        return $this->f034frePrintedBy;
    }

}
