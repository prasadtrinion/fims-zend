<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T082ftaxdeductionMaster
 *
 * @ORM\Table(name="t082ftax_deduction_master", uniqueConstraints={@ORM\UniqueConstraint(name="f082fname", columns={"f082fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\TaxDeductionRepository")
 */
class T082ftaxdeductionMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f082fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fid_staff", type="integer",nullable=false)
     */
    private $f082fidStaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f082fname", type="string", length=100, nullable=false)
     */
    private $f082fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fcreated_by", type="integer", nullable=true)
     */
    private $f082fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fupdated_by", type="integer", nullable=true)
     */
    private $f082fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f082fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f082fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f082fstatus", type="integer", nullable=true)
     */
    private $f082fstatus;



    /**
     * Get f082fid
     *
     * @return integer
     */
    public function getF082fid()
    {
        return $this->f082fid;
    }

    /**
     * Set f082fidStaff
     *
     * @param integer $f082fidStaff
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fidStaff($f082fidStaff)
    {
        $this->f082fidStaff = $f082fidStaff;

        return $this;
    }

    /**
     * Get f082fidStaff
     *
     * @return integer
     */
    public function getF082fidStaff()
    {
        return $this->f082fidStaff;
    }

    /**
     * Set f082fname
     *
     * @param string $f082fname
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fname($f082fname)
    {
        $this->f082fname = $f082fname;

        return $this;
    }

    /**
     * Get f082fname
     *
     * @return string
     */
    public function getF082fname()
    {
        return $this->f082fname;
    }

    /**
     * Set f082fcreatedBy
     *
     * @param integer $f082fcreatedBy
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fcreatedBy($f082fcreatedBy)
    {
        $this->f082fcreatedBy = $f082fcreatedBy;

        return $this;
    }

    /**
     * Get f082fcreatedBy
     *
     * @return integer
     */
    public function getF082fcreatedBy()
    {
        return $this->f082fcreatedBy;
    }

    /**
     * Set f082fcreatedDtTm
     *
     * @param \DateTime $f082fcreatedDtTm
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fcreatedDtTm($f082fcreatedDtTm)
    {
        $this->f082fcreatedDtTm = $f082fcreatedDtTm;

        return $this;
    }

    /**
     * Get f082fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fcreatedDtTm()
    {
        return $this->f082fcreatedDtTm;
    }

    /**
     * Set f082fupdatedBy
     *
     * @param integer $f082fupdatedBy
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fupdatedBy($f082fupdatedBy)
    {
        $this->f082fupdatedBy = $f082fupdatedBy;

        return $this;
    }

    /**
     * Get f082fupdatedBy
     *
     * @return integer
     */
    public function getF082fupdatedBy()
    {
        return $this->f082fupdatedBy;
    }

    /**
     * Set f082fupdatedDtTm
     *
     * @param \DateTime $f082fupdatedDtTm
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fupdatedDtTm($f082fupdatedDtTm)
    {
        $this->f082fupdatedDtTm = $f082fupdatedDtTm;

        return $this;
    }

    /**
     * Get f082fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF082fupdatedDtTm()
    {
        return $this->f082fupdatedDtTm;
    }

    /**
     * Set f082fstatus
     *
     * @param integer $f082fstatus
     *
     * @return T082ftaxdeductionMaster
     */
    public function setF082fstatus($f082fstatus)
    {
        $this->f082fstatus = $f082fstatus;

        return $this;
    }

    /**
     * Get f082fstatus
     *
     * @return integer
     */
    public function getF082fstatus()
    {
        return $this->f082fstatus;
    }
}
