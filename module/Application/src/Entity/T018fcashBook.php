<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T018fcashBook
 *
 * @ORM\Table(name="t018fcash_book")
 * @ORM\Entity(repositoryClass="Application\Repository\JournalRepository")
 */
class T018fcashBook
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f018fid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fdate_of_transaction", type="datetime", nullable=true)
     */
    private $f018fdateOfTransaction;

     /**
     * @var string
     *
     * @ORM\Column(name="f018freference_number", type="string", length=50, nullable=false)
     */
    private $f018freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fdr_amount", type="integer", nullable=false)
     */
    private $f018fdrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcr_amount", type="integer", nullable=false)
     */
    private $f018fcrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fstatus", type="integer", nullable=false)
     */
    private $f018fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcreated_by", type="integer", nullable=false)
     */
    private $f018fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fupdated_by", type="integer", nullable=false)
     */
    private $f018fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fupdateDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid_glcode", type="integer", nullable=true)
     */
    private $f018fidGlcode;




    /**
     * Get f018fid
     *
     * @return integer
     */
    public function getF018fid()
    {
        return $this->f018fid;
    }


    /**
     * Set f018fidGlcode
     *
     * @param integer $f018fidGlcode
     *
     * @return T018fjournalDetails
     */
    public function setF018fidGlcode($f018fidGlcode)
    {
        $this->f018fidGlcode = $f018fidGlcode;

        return $this;
    }

    /**
     * Get f018fidGlcode
     *
     * @return integer
     */
    public function getF018fidGlcode()
    {
        return $this->f018fidGlcode;
    }


        /**
     * Set f018fdateOfTransaction
     *
     * @param \DateTime $f018fdateOfTransaction
     *
     * @return T018fjournalDetails
     */
    public function setF018fdateOfTransaction($f018fdateOfTransaction)
    {
        $this->f018fdateOfTransaction = $f018fdateOfTransaction;

        return $this;
    }

    /**
     * Get f018fdateOfTransaction
     *
     * @return \DateTime
     */
    public function getF018fdateOfTransaction()
    {
        return $this->f018fdateOfTransaction;
    }
    


    /**
     * Set f018fdrAmount
     *
     * @param integer $f018fdrAmount
     *
     * @return T018fjournalDetails
     */
    public function setF018fdrAmount($f018fdrAmount)
    {
        $this->f018fdrAmount = $f018fdrAmount;

        return $this;
    }

    /**
     * Get f018fdrAmount
     *
     * @return integer
     */
    public function getF018fdrAmount()
    {
        return $this->f018fdrAmount;
    }

    /**
     * Set f018fcrAmount
     *
     * @param integer $f018fcrAmount
     *
     * @return T018fjournalDetails
     */
    public function setF018fcrAmount($f018fcrAmount)
    {
        $this->f018fcrAmount = $f018fcrAmount;

        return $this;
    }

    /**
     * Get f018fcrAmount
     *
     * @return integer
     */
    public function getF018fcrAmount()
    {
        return $this->f018fcrAmount;
    }

    /**
     * Set f018fstatus
     *
     * @param integer $f018fstatus
     *
     * @return T018fjournalDetails
     */
    public function setF018fstatus($f018fstatus)
    {
        $this->f018fstatus = $f018fstatus;

        return $this;
    }

    /**
     * Get f018fstatus
     *
     * @return integer
     */
    public function getF018fstatus()
    {
        return $this->f018fstatus;
    }

    /**
     * Set f018fcreatedBy
     *
     * @param integer $f018fcreatedBy
     *
     * @return T018fjournalDetails
     */
    public function setF018fcreatedBy($f018fcreatedBy)
    {
        $this->f018fcreatedBy = $f018fcreatedBy;

        return $this;
    }

    /**
     * Get f018fcreatedBy
     *
     * @return integer
     */
    public function getF018fcreatedBy()
    {
        return $this->f018fcreatedBy;
    }

    /**
     * Set f018fupdatedBy
     *
     * @param integer $f018fupdatedBy
     *
     * @return T018fjournalDetails
     */
    public function setF018fupdatedBy($f018fupdatedBy)
    {
        $this->f018fupdatedBy = $f018fupdatedBy;

        return $this;
    }

    /**
     * Get f018fupdatedBy
     *
     * @return integer
     */
    public function getF018fupdatedBy()
    {
        return $this->f018fupdatedBy;
    }

    /**
     * Set f018fcreateDtTm
     *
     * @param \DateTime $f018fcreateDtTm
     *
     * @return T018fjournalDetails
     */
    public function setF018fcreateDtTm($f018fcreateDtTm)
    {
        $this->f018fcreateDtTm = $f018fcreateDtTm;

        return $this;
    }

    /**
     * Get f018fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF018fcreateDtTm()
    {
        return $this->f018fcreateDtTm;
    }

    /**
     * Set f018fupdateDtTm
     *
     * @param \DateTime $f018fupdateDtTm
     *
     * @return T018fjournalDetails
     */
    public function setF018fupdateDtTm($f018fupdateDtTm)
    {
        $this->f018fupdateDtTm = $f018fupdateDtTm;

        return $this;
    }

    /**
     * Get f018fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF018fupdateDtTm()
    {
        return $this->f018fupdateDtTm;
    }


    /**
     * Get f018freferenceNumber
     *
     * @return string
     */
    public function getF018freferenceNumber()
    {
        return $this->f018freferenceNumber;
    }

        /**
     * Set f018freferenceNumber
     *
     * @param string $f018freferenceNumber
     *
     * @return T018fjournalDetails
     */
    public function setF018freferenceNumber($f018freferenceNumber)
    {
        $this->f018freferenceNumber = $f018freferenceNumber;

        return $this;
    }
}