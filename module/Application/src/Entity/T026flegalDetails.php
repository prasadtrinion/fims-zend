<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T026flegalDetails
 *
 * @ORM\Table(name="t026flegal_details")
 * @ORM\Entity(repositoryClass="Application\Repository\LegalInformationRepository")
 */
class T026flegalDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f026fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f026fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fid_legal", type="integer", nullable=true)
     */
    private $f026fidLegal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fid_student", type="integer", nullable=true)
     */
    private $f026fidStudent;

    /**
     * @var string
     *
     * @ORM\Column(name="f026fdescription", type="string", length=200, nullable=true)
     */
    private $f026fdescription;


    /**
     * @var integer
     *
     * @ORM\Column(name="f026fstatus", type="integer", nullable=true)
     */
    private $f026fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fcreated_by", type="integer", nullable=true)
     */
    private $f026fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f026fupdated_by", type="integer", nullable=true)
     */
    private $f026fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f026fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f026fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f026fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f026fupdatedDtTm;



    /**
     * Get f026fid
     *
     * @return integer
     */
    public function getF026fidDetails()
    {
        return $this->f026fidDetails;
    }

    /**
     * Set f026fdescription
     *
     * @param string $f026fdescription
     *
     * @return T026flegalInformation
     */
    public function setF026fdescription($f026fdescription)
    {
        $this->f026fdescription = $f026fdescription;

        return $this;
    }

    /**
     * Get f026fdescription
     *
     * @return string
     */
    public function getF026fdescription()
    {
        return $this->f026fdescription;
    }

    /**
     * Set f026fidLegal
     *
     * @param integer $f026fidLegal
     *
     * @return T026flegalInformation
     */
    public function setF026fidLegal($f026fidLegal)
    {
        $this->f026fidLegal = $f026fidLegal;

        return $this;
    }

    /**
     * Get f026fidLegal
     *
     * @return integer
     */
    public function getF026fidLeagl()
    {
        return $this->f026fidLegal;
    }

   

    /**
     * Set f026fidStudent
     *
     * @param integer $f026fidStudent
     *
     * @return T026flegalInformation
     */
    public function setF026fidStudent($f026fidStudent)
    {
        $this->f026fidStudent = $f026fidStudent;

        return $this;
    }

    /**
     * Get f026fidStudent
     *
     * @return integer
     */
    public function getF026fidStudent()
    {
        return $this->f026fidStudent;
    }

    /**
     * Set f026fstatus
     *
     * @param integer $f026fstatus
     *
     * @return T026flegalInformation
     */
    public function setF026fstatus($f026fstatus)
    {
        $this->f026fstatus = $f026fstatus;

        return $this;
    }

    /**
     * Get f026fstatus
     *
     * @return integer
     */
    public function getF026fstatus()
    {
        return $this->f026fstatus;
    }

    /**
     * Set f026fcreatedBy
     *
     * @param integer $f026fcreatedBy
     *
     * @return T026flegalInformation
     */
    public function setF026fcreatedBy($f026fcreatedBy)
    {
        $this->f026fcreatedBy = $f026fcreatedBy;

        return $this;
    }

    /**
     * Get f026fcreatedBy
     *
     * @return integer
     */
    public function getF026fcreatedBy()
    {
        return $this->f026fcreatedBy;
    }

    /**
     * Set f026fupdatedBy
     *
     * @param integer $f026fupdatedBy
     *
     * @return T026flegalInformation
     */
    public function setF026fupdatedBy($f026fupdatedBy)
    {
        $this->f026fupdatedBy = $f026fupdatedBy;

        return $this;
    }

    /**
     * Get f026fupdatedBy
     *
     * @return integer
     */
    public function getF026fupdatedBy()
    {
        return $this->f026fupdatedBy;
    }

    /**
     * Set f026fcreatedDtTm
     *
     * @param \DateTime $f026fcreatedDtTm
     *
     * @return T026flegalInformation
     */
    public function setF026fcreatedDtTm($f026fcreatedDtTm)
    {
        $this->f026fcreatedDtTm = $f026fcreatedDtTm;

        return $this;
    }

    /**
     * Get f026fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF026fcreatedDtTm()
    {
        return $this->f026fcreatedDtTm;
    }

    /**
     * Set f026fupdatedDtTm
     *
     * @param \DateTime $f026fupdatedDtTm
     *
     * @return T026flegalInformation
     */
    public function setF026fupdatedDtTm($f026fupdatedDtTm)
    {
        $this->f026fupdatedDtTm = $f026fupdatedDtTm;

        return $this;
    }

    /**
     * Get f026fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF026fupdatedDtTm()
    {
        return $this->f026fupdatedDtTm;
    }
}
