<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T131loanDeductionRatio
 *
 * @ORM\Table(name="t131loan_deduction_ratio", uniqueConstraints={})
 * @ORM\Entity(repositoryClass="Application\Repository\LoanDeductionRatioRepository")
 */
class T131loanDeductionRatio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f131fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f131fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f131fdeduction_percentage", type="integer", nullable=false)
     */
    private $f131fdeductionPercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f131fstatus", type="integer", nullable=false)
     */
    private $f131fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f131fcreated_by", type="integer", nullable=false)
     */
    private $f131fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f131fupdated_by", type="integer", nullable=false)
     */
    private $f131fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f131fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f131fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f131fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f131fupdatedDtTm;



    /**
     * Get f131fid
     *
     * @return integer
     */
    public function getF131fid()
    {
        return $this->f131fid;
    }

    /**
     * Set f131fdeductionPercentage
     *
     * @param string $f131fdeductionPercentage
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fdeductionPercentage($f131fdeductionPercentage)
    {
        $this->f131fdeductionPercentage = $f131fdeductionPercentage;

        return $this;
    }

    /**
     * Get f131fdeductionPercentage
     *
     * @return string
     */
    public function getF131fdeductionPercentage()
    {
        return $this->f131fdeductionPercentage;
    }


     /**
     * Set f131fstatus
     *
     * @param integer $f131fstatus
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fstatus($f131fstatus)
    {
        $this->f131fstatus = $f131fstatus;

        return $this;
    }

    /**
     * Get f131fstatus
     *
     * @return integer
     */
    public function getF131fstatus()
    {
        return $this->f131fstatus;
    }


    /**
     * Set f131fcreatedBy
     *
     * @param integer $f131fcreatedBy
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fcreatedBy($f131fcreatedBy)
    {
        $this->f131fcreatedBy = $f131fcreatedBy;

        return $this;
    }

    /**
     * Get f131fcreatedBy
     *
     * @return integer
     */
    public function getF131fcreatedBy()
    {
        return $this->f131fcreatedBy;
    }

    /**
     * Set f131fupdatedBy
     *
     * @param integer $f131fupdatedBy
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fupdatedBy($f131fupdatedBy)
    {
        $this->f131fupdatedBy = $f131fupdatedBy;

        return $this;
    }

    /**
     * Get f131fupdatedBy
     *
     * @return integer
     */
    public function getF131fupdatedBy()
    {
        return $this->f131fupdatedBy;
    }

    /**
     * Set f131fcreatedDtTm
     *
     * @param \DateTime $f131fcreatedDtTm
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fcreatedDtTm($f131fcreatedDtTm)
    {
        $this->f131fcreatedDtTm = $f131fcreatedDtTm;

        return $this;
    }

    /**
     * Get f131fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF131fcreatedDtTm()
    {
        return $this->f131fcreatedDtTm;
    }

    /**
     * Set f131fupdatedDtTm
     *
     * @param \DateTime $f131fupdatedDtTm
     *
     * @return T131loanDeductionRatio
     */
    public function setF131fupdatedDtTm($f131fupdatedDtTm)
    {
        $this->f131fupdatedDtTm = $f131fupdatedDtTm;

        return $this;
    }

    /**
     * Get f131fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF131fupdatedDtTm()
    {
        return $this->f131fupdatedDtTm;
    }
}
