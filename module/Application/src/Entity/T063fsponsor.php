<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fsponsor
 *
 * @ORM\Table(name="t063fsponsor")
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorRepository")
 */
class T063fsponsor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f063ffirst_name", type="string", length=50, nullable=false)
     */
    private $f063ffirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="f063flast_name", type="string", length=50, nullable=false)
     */
    private $f063flastName;

    /**
     * @var string
     *
     * @ORM\Column(name="f063femail", type="string", length=50, nullable=false)
     */
    private $f063femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f063ffax_number", type="string", length=50, nullable=false)
     */
    private $f063ffaxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fcontact_no", type="string", length=25, nullable=false)
     */
    private $f063fcontactNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f063faddress1", type="string", length=100, nullable=false)
     */
    private $f063faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f063faddress2", type="string", length=100, nullable=false)
     */
    private $f063faddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fpost_code", type="string", length=100, nullable=false)
     */
    private $f063fpostCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fcountry", type="integer", nullable=false)
     */
    private $f063fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fstate", type="integer", nullable=false)
     */
    private $f063fstate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fsponsorship_type", type="integer", nullable=true)
     */
    private $f063fsponsorshipType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fpayment_status", type="integer", nullable=true)
     */
    private $f063fpaymentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fjob_type", type="integer", nullable=true)
     */
    private $f063fjobType;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fsponsor_type", type="string", length=60, nullable=true)
     */
    private $f063fsponsorType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063freceivable_status", type="integer", nullable=true)
     */
    private $f063freceivableStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fffund", type="string", length=20, nullable=false)
     */
    private $f063ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fdepartment", type="string", length=20, nullable=false)
     */
    private $f063fdepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f063factivity", type="string", length=20, nullable=false)
     */
    private $f063factivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f063faccount", type="string", length=20, nullable=false)
     */
    private $f063faccount;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fincharge_officer", type="string", length=50, nullable=false)
     */
    private $f063finchargeOfficer;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fcode", type="string", length=60, nullable=false)
     */
    private $f063fcode;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=false)
     */
    private $f063fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=true)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=true)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fupdatedDtTm;



    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063ffirstName
     *
     * @param string $f063ffirstName
     *
     * @return T063fsponsor
     */
    public function setF063ffirstName($f063ffirstName)
    {
        $this->f063ffirstName = $f063ffirstName;

        return $this;
    }

    /**
     * Get f063ffirstName
     *
     * @return string
     */
    public function getF063ffirstName()
    {
        return $this->f063ffirstName;
    }

    /**
     * Set f063ffaxNumber
     *
     * @param string $f063ffaxNumber
     *
     * @return T063fsponsor
     */
    public function setF063ffaxNumber($f063ffaxNumber)
    {
        $this->f063ffaxNumber = $f063ffaxNumber;

        return $this;
    }

    /**
     * Get f063ffaxNumber
     *
     * @return string
     */
    public function getF063ffaxNumber()
    {
        return $this->f063ffaxNumber;
    }

    /**
     * Set f063fcountry
     *
     * @param string $f063fcountry
     *
     * @return T063fsponsor
     */
    public function setF063fcountry($f063fcountry)
    {
        $this->f063fcountry = $f063fcountry;

        return $this;
    }

    /**
     * Get f063fcountry
     *
     * @return string
     */
    public function getF063fcountry()
    {
        return $this->f063fcountry;
    }

    /**
     * Set f063fstate
     *
     * @param string $f063fstate
     *
     * @return T063fsponsor
     */
    public function setF063fstate($f063fstate)
    {
        $this->f063fstate = $f063fstate;

        return $this;
    }

    /**
     * Get f063fstate
     *
     * @return string
     */
    public function getF063fstate()
    {
        return $this->f063fstate;
    }

    /**
     * Set f063femail
     *
     * @param string $f063femail
     *
     * @return T063fsponsor
     */
    public function setF063femail($f063femail)
    {
        $this->f063femail = $f063femail;

        return $this;
    }

    /**
     * Get f063femail
     *
     * @return string
     */
    public function getF063femail()
    {
        return $this->f063femail;
    }

    /**
     * Set f063finchargeOfficer
     *
     * @param string $f063finchargeOfficer
     *
     * @return T063fsponsor
     */
    public function setF063finchargeOfficer($f063finchargeOfficer)
    {
        $this->f063finchargeOfficer = $f063finchargeOfficer;

        return $this;
    }

    /**
     * Get f063finchargeOfficer
     *
     * @return string
     */
    public function getF063finchargeOfficer()
    {
        return $this->f063finchargeOfficer;
    }

    /**
     * Set f063fpostCode
     *
     * @param string $f063fpostCode
     *
     * @return T063fsponsor
     */
    public function setF063fpostCode($f063fpostCode)
    {
        $this->f063fpostCode = $f063fpostCode;

        return $this;
    }

    /**
     * Get f063fpostCode
     *
     * @return string
     */
    public function getF063fpostCode()
    {
        return $this->f063fpostCode;
    }

    /**
     * Set f063flastName
     *
     * @param string $f063flastName
     *
     * @return T063fsponsor
     */
    public function setF063flastName($f063flastName)
    {
        $this->f063flastName = $f063flastName;

        return $this;
    }

    /**
     * Get f063flastName
     *
     * @return string
     */
    public function getF063flastName()
    {
        return $this->f063flastName;
    }

    /**
     * Set f063fcontactNo
     *
     * @param string $f063fcontactNo
     *
     * @return T063fsponsor
     */
    public function setF063fcontactNo($f063fcontactNo)
    {
        $this->f063fcontactNo = $f063fcontactNo;

        return $this;
    }

    /**
     * Get f063fcontactNo
     *
     * @return string
     */
    public function getF063fcontactNo()
    {
        return $this->f063fcontactNo;
    }

    /**
     * Set f063faddress1
     *
     * @param string $f063faddress1
     *
     * @return T063fsponsor
     */
    public function setF063faddress1($f063faddress1)
    {
        $this->f063faddress1 = $f063faddress1;

        return $this;
    }

    /**
     * Get f063faddress1
     *
     * @return string
     */
    public function getF063faddress1()
    {
        return $this->f063faddress1;
    }

    /**
     * Set f063faddress2
     *
     * @param string $f063faddress2
     *
     * @return T063fsponsor
     */
    public function setF063faddress2($f063faddress2)
    {
        $this->f063faddress2 = $f063faddress2;

        return $this;
    }

    /**
     * Get f063faddress2
     *
     * @return string
     */
    public function getF063faddress2()
    {
        return $this->f063faddress2;
    }

    /**
     * Set f063fsponsorshipType
     *
     * @param integer $f063fsponsorshipType
     *
     * @return T063fsponsor
     */
    public function setF063fsponsorshipType($f063fsponsorshipType)
    {
        $this->f063fsponsorshipType = $f063fsponsorshipType;

        return $this;
    }

    /**
     * Get f063fsponsorshipType
     *
     * @return integer
     */
    public function getF063fsponsorshipType()
    {
        return $this->f063fsponsorshipType;
    }

    /**
     * Set f063fpaymentStatus
     *
     * @param integer $f063fpaymentStatus
     *
     * @return T063fsponsor
     */
    public function setF063fpaymentStatus($f063fpaymentStatus)
    {
        $this->f063fpaymentStatus = $f063fpaymentStatus;

        return $this;
    }

    /**
     * Get f063fpaymentStatus
     *
     * @return integer
     */
    public function getF063fpaymentStatus()
    {
        return $this->f063fpaymentStatus;
    }

    /**
     * Set f063fjobType
     *
     * @param integer $f063fjobType
     *
     * @return T063fsponsor
     */
    public function setF063fjobType($f063fjobType)
    {
        $this->f063fjobType = $f063fjobType;

        return $this;
    }

    /**
     * Get f063fjobType
     *
     * @return integer
     */
    public function getF063fjobType()
    {
        return $this->f063fjobType;
    }

    /**
     * Set f063fsponsorType
     *
     * @param integer $f063fsponsorType
     *
     * @return T063fsponsor
     */
    public function setF063fsponsorType($f063fsponsorType)
    {
        $this->f063fsponsorType = $f063fsponsorType;

        return $this;
    }

    /**
     * Get f063fsponsorType
     *
     * @return integer
     */
    public function getF063fsponsorType()
    {
        return $this->f063fsponsorType;
    }

    /**
     * Set f063freceivableStatus
     *
     * @param integer $f063freceivableStatus
     *
     * @return T063fsponsor
     */
    public function setF063freceivableStatus($f063freceivableStatus)
    {
        $this->f063freceivableStatus = $f063freceivableStatus;

        return $this;
    }

    /**
     * Get f063freceivableStatus
     *
     * @return integer
     */
    public function getF063freceivableStatus()
    {
        return $this->f063freceivableStatus;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fsponsor
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }


    /**
     * Set f063fcode
     *
     * @param integer $f063fcode
     *
     * @return T063fsponsor
     */
    public function setF063fcode($f063fcode)
    {
        $this->f063fcode = $f063fcode;

        return $this;
    }

    /**
     * Get f063fcode
     *
     * @return integer
     */
    public function getF063fcode()
    {
        return $this->f063fcode;
    }


    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fsponsor
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fsponsor
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fsponsor
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fsponsor
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

     /**
     * Set f063ffund
     *
     * @param string $f063ffund
     *
     * @return T063fsponsor
     */
    public function setF063ffund($f063ffund)
    {
        $this->f063ffund = $f063ffund;

        return $this;
    }

    /**
     * Get f063ffund
     *
     * @return string
     */
    public function getF063ffund()
    {
        return $this->f063ffund;
    }

    /**
     * Set f063factivity
     *
     * @param string $f063factivity
     *
     * @return T063fsponsor
     */
    public function setF063factivity($f063factivity)
    {
        $this->f063factivity = $f063factivity;

        return $this;
    }

    /**
     * Get f063factivity
     *
     * @return string
     */
    public function getF063factivity()
    {
        return $this->f063factivity;
    }

    /**
     * Set f063fdepartment
     *
     * @param string $f063fdepartment
     *
     * @return T063fsponsor
     */
    public function setF063fdepartment($f063fdepartment)
    {
        $this->f063fdepartment = $f063fdepartment;

        return $this;
    }

    /**
     * Get f063fdepartment
     *
     * @return string
     */
    public function getF063fdepartment()
    {
        return $this->f063fdepartment;
    }

    /**
     * Set f063faccount
     *
     * @param string $f063faccount
     *
     * @return T063fsponsor
     */
    public function setF063faccount($f063faccount)
    {
        $this->f063faccount = $f063faccount;

        return $this;
    }

    /**
     * Get f063faccount
     *
     * @return string
     */
    public function getF063faccount()
    {
        return $this->f063faccount;
    }
}
