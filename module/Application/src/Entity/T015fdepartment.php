<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T015fdepartment
 *
 * @ORM\Table(name="t015fdepartment", uniqueConstraints={@ORM\UniqueConstraint(name="f015fdepartment_code_UNIQUE", columns={"f015fdepartment_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\DepartmentRepository")
 */
class T015fdepartment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f015fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f015fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdepartment_name", type="string", length=255, nullable=false)
     */
    private $f015fdepartmentName;

    /**
     * @var string
     *
     * @ORM\Column(name="f015funit", type="string", length=10, nullable=false)
     */
    private $f015funit;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdept_code", type="string", length=10, nullable=false)
     */
    private $f015fdeptCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f015fdepartment_code", type="string", length=255, nullable=false)
     */
    private $f015fdepartmentCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f015fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f015fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fstatus", type="integer", nullable=false)
     */
    private $f015fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f015fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f015fupdatedBy;



    /**
     * Get f015fid
     *
     * @return integer
     */
    public function getF015fid()
    {
        return $this->f015fid;
    }

    /**
     * Set f015fdepartmentName
     *
     * @param string $f015fdepartmentName
     *
     * @return T015fdepartment
     */
    public function setF015fdepartmentName($f015fdepartmentName)
    {
        $this->f015fdepartmentName = $f015fdepartmentName;

        return $this;
    }

    /**
     * Get f015fdepartmentName
     *
     * @return string
     */
    public function getF015fdepartmentName()
    {
        return $this->f015fdepartmentName;
    }

    /**
     * Set f015funit
     *
     * @param string $f015funit
     *
     * @return T015fdepartment
     */
    public function setF015funit($f015funit)
    {
        $this->f015funit = $f015funit;

        return $this;
    }

    /**
     * Get f015funit
     *
     * @return string
     */
    public function getF015funit()
    {
        return $this->f015funit;
    }

    /**
     * Set f015fdeptCode
     *
     * @param string $f015fdeptCode
     *
     * @return T015fdepartment
     */
    public function setF015fdeptCode($f015fdeptCode)
    {
        $this->f015fdeptCode = $f015fdeptCode;

        return $this;
    }

    /**
     * Get f015fdeptCode
     *
     * @return string
     */
    public function getF015fdeptCode()
    {
        return $this->f015fdeptCode;
    }

    /**
     * Set f015fdepartmentCode
     *
     * @param string $f015fdepartmentCode
     *
     * @return T015fdepartment
     */
    public function setF015fdepartmentCode($f015fdepartmentCode)
    {
        $this->f015fdepartmentCode = $f015fdepartmentCode;

        return $this;
    }

    /**
     * Get f015fdepartmentCode
     *
     * @return string
     */
    public function getF015fdepartmentCode()
    {
        return $this->f015fdepartmentCode;
    }

    /**
     * Set f015fupdatedDtTm
     *
     * @param \DateTime $f015fupdatedDtTm
     *
     * @return T015fdepartment
     */
    public function setF015fupdatedDtTm($f015fupdatedDtTm)
    {
        $this->f015fupdatedDtTm = $f015fupdatedDtTm;

        return $this;
    }

    /**
     * Get f015fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF015fupdatedDtTm()
    {
        return $this->f015fupdatedDtTm;
    }

    /**
     * Set f015fstatus
     *
     * @param integer $f015fstatus
     *
     * @return T015fdepartment
     */
    public function setF015fstatus($f015fstatus)
    {
        $this->f015fstatus = $f015fstatus;

        return $this;
    }

    /**
     * Get f015fstatus
     *
     * @return integer
     */
    public function getF015fstatus()
    {
        return $this->f015fstatus;
    }

    /**
     * Set f015fupdatedBy
     *
     * @param integer $f015fupdatedBy
     *
     * @return T015fdepartment
     */
    public function setF015fupdatedBy($f015fupdatedBy)
    {
        $this->f015fupdatedBy = $f015fupdatedBy;

        return $this;
    }

    /**
     * Get f015fupdatedBy
     *
     * @return integer
     */
    public function getF015fupdatedBy()
    {
        return $this->f015fupdatedBy;
    }
}
