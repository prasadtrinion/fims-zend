<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T071finvoice
 *
 * @ORM\Table(name="t071finvoice")
 * @ORM\Entity(repositoryClass="Application\Repository\MasterInvoiceRepository")
 */
class T071finvoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f071fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f071fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f071finvoice_number", type="string", length=50, nullable=false)
     */
    private $f071finvoiceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fbill_type", type="integer", nullable=false)
     */
    private $f071fbillType;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fid_customer", type="integer", nullable=true)
     */
    private $f071fidCustomer;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fcr_amount", type="integer", nullable=true)
     */
    private $f071fcrAmount;
    /**
     * @var integer
     *
     * @ORM\Column(name="f071fdr_amount", type="integer", nullable=true)
     */
    private $f071fdrAmount;
    /**
     * @var integer
     *
     * @ORM\Column(name="f071ftotal_paid", type="integer", nullable=true)
     */
    private $f071ftotalPaid;
    /**
     * @var integer
     *
     * @ORM\Column(name="f071fbalance", type="integer", nullable=true)
     */
    private $f071fbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f071finvoice_type", type="string", length=50, nullable=false)
     */
    private $f071finvoiceType;
    

     /**
     * @var string
     *
     * @ORM\Column(name="f071fdescription", type="string", length=100, nullable=false)
     */
    private $f071fdescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071finvoice_date", type="datetime", nullable=false)
     */
    private $f071finvoiceDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fapproved_by", type="integer", nullable=false)
     */
    private $f071fapprovedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fstatus", type="integer", nullable=false)
     */
    private $f071fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fcreated_by", type="integer", nullable=false)
     */
    private $f071fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fupdated_by", type="integer", nullable=false)
     */
    private $f071fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f071finvoice_total", type="integer", nullable=false)
     */
    private $f071finvoiceTotal;

        /**
     * @var integer
     *
     * @ORM\Column(name="f071fdiscount", type="integer", nullable=true)
     */
    private $f071fdiscount;

        /**
     * @var integer
     *
     * @ORM\Column(name="f071fonline_payment", type="integer", nullable=true)
     */
    private $f071fonlinePayment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fis_rcp", type="integer", nullable=true)
     */
    private $f071fisRcp;


    /**
     * @var integer
     *
     * @ORM\Column(name="f071fprocessed", type="integer", nullable=true)
     */
    private $f071fprocessed = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071finvoice_from", type="integer", nullable=true)
     */
    private $f071finvoiceFrom;

     /**
     * @var integer
     *
     * @ORM\Column(name="f071fid_financial_year", type="integer", nullable=false)
     */
    private $f071fidFinancialYear;

     /**
     * @var string
     *
     * @ORM\Column(name="f071freason", type="string", length=60, nullable=false)
     */
    private $f071freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fis_paid", type="integer", nullable=true)
     */
    private $f071fisPaid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071ftype", type="string",length=30, nullable=true)
     */
    private $f071ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fid_request_invoice", type="integer", nullable=true)
     */
    private $f071fidRequestInvoice;

    /**
     * Get f071fid
     *
     * @return integer
     */
    public function getF071fid()
    {
        return $this->f071fid;
    }

    /**
     * Set f071fidRequestInvoice
     *
     * @param string $f071fidRequestInvoice
     *
     * @return T071finvoice
     */
    public function setF071fidRequestInvoice($f071fidRequestInvoice)
    {
        $this->f071fidRequestInvoice = $f071fidRequestInvoice;

        return $this;
    }

    /**
     * Get f071fidRequestInvoice
     *
     * @return string
     */
    public function getF071fidRequestInvoice()
    {
        return $this->f071fidRequestInvoice;
    }

    /**
     * Set f071finvoiceNumber
     *
     * @param string $f071finvoiceNumber
     *
     * @return T071finvoice
     */
    public function setF071finvoiceNumber($f071finvoiceNumber)
    {
        $this->f071finvoiceNumber = $f071finvoiceNumber;

        return $this;
    }

    /**
     * Get f071finvoiceNumber
     *
     * @return string
     */
    public function getF071finvoiceNumber()
    {
        return $this->f071finvoiceNumber;
    }

    /**
     * Set f071ftype
     *
     * @param string $f071ftype
     *
     * @return T071finvoice
     */
    public function setF071ftype($f071ftype)
    {
        $this->f071ftype = $f071ftype;

        return $this;
    }

    /**
     * Get f071ftype
     *
     * @return string
     */
    public function getF071ftype()
    {
        return $this->f071ftype;
    }

    /**
     * Set f071fidCustomer
     *
     * @param integer $f071fidCustomer
     *
     * @return T071finvoice
     */
    public function setF071fidCustomer($f071fidCustomer)
    {
        $this->f071fidCustomer = $f071fidCustomer;

        return $this;
    }

    /**
     * Get f071fidCustomer
     *
     * @return integer
     */
    public function getF071fidCustomer()
    {
        return $this->f071fidCustomer;
    }

    /**
     * Set f071fcrAmount
     *
     * @param integer $f071fcrAmount
     *
     * @return T071finvoice
     */
    public function setF071fcrAmount($f071fcrAmount)
    {
        $this->f071fcrAmount = $f071fcrAmount;

        return $this;
    }

    /**
     * Get f071fcrAmount
     *
     * @return integer
     */
    public function getF071fcrAmount()
    {
        return $this->f071fcrAmount;
    }

    /**
     * Set f071fdrAmount
     *
     * @param integer $f071fdrAmount
     *
     * @return T071finvoice
     */
    public function setF071fdrAmount($f071fdrAmount)
    {
        $this->f071fdrAmount = $f071fdrAmount;

        return $this;
    }

    /**
     * Get f071fdrAmount
     *
     * @return integer
     */
    public function getF071fdrAmount()
    {
        return $this->f071fdrAmount;
    }

    /**
     * Set f071fbalance
     *
     * @param integer $f071fbalance
     *
     * @return T071finvoice
     */
    public function setF071fbalance($f071fbalance)
    {
        $this->f071fbalance = $f071fbalance;

        return $this;
    }

    /**
     * Get f071fbalance
     *
     * @return integer
     */
    public function getF071fbalance()
    {
        return $this->f071fbalance;
    }

    /**
     * Set f071ftotalPaid
     *
     * @param integer $f071ftotalPaid
     *
     * @return T071finvoice
     */
    public function setF071ftotalPaid($f071ftotalPaid)
    {
        $this->f071ftotalPaid = $f071ftotalPaid;

        return $this;
    }

    /**
     * Get f071ftotalPaid
     *
     * @return integer
     */
    public function getF071ftotalPaid()
    {
        return $this->f071ftotalPaid;
    }

     /**
     * Set f071fbillType
     *
     * @param string $f071fbillType
     *
     * @return T073frequestInvoice
     */
    public function setF071fbillType($f071fbillType)
    {
        $this->f071fbillType = $f071fbillType;

        return $this;
    }

    /**
     * Get f071fbillType
     *
     * @return string
     */
    public function getF071fbillType()
    {
        return $this->f071fbillType;
    }
    
    /**
     * Set f071finvoiceType
     *
     * @param string $f071finvoiceType
     *
     * @return T071finvoice
     */
    public function setF071finvoiceType($f071finvoiceType)
    {
        $this->f071finvoiceType = $f071finvoiceType;

        return $this;
    }

    /**
     * Get f071finvoiceType
     *
     * @return string
     */
    public function getF071finvoiceType()
    {
        return $this->f071finvoiceType;
    }

    /**
     * Set f071finvoiceDate
     *
     * @param \DateTime $f071finvoiceDate
     *
     * @return T071finvoice
     */
    public function setF071finvoiceDate($f071finvoiceDate)
    {
        $this->f071finvoiceDate = $f071finvoiceDate;

        return $this;
    }

    /**
     * Get f071finvoiceDate
     *
     * @return \DateTime
     */
    public function getF071finvoiceDate()
    {
        return $this->f071finvoiceDate;
    }

    /**
     * Set f071fapprovedBy
     *
     * @param integer $f071fapprovedBy
     *
     * @return T071finvoice
     */
    public function setF071fapprovedBy($f071fapprovedBy)
    {
        $this->f071fapprovedBy = $f071fapprovedBy;

        return $this;
    }

    /**
     * Get f071fapprovedBy
     *
     * @return integer
     */
    public function getF071fapprovedBy()
    {
        return $this->f071fapprovedBy;
    }

    /**
     * Set f071fstatus
     *
     * @param integer $f071fstatus
     *
     * @return T071finvoice
     */
    public function setF071fstatus($f071fstatus)
    {
        $this->f071fstatus = $f071fstatus;

        return $this;
    }

    /**
     * Get f071fstatus
     *
     * @return integer
     */
    public function getF071fstatus()
    {
        return $this->f071fstatus;
    }

    /**
     * Set f071fcreatedBy
     *
     * @param integer $f071fcreatedBy
     *
     * @return T071finvoice
     */
    public function setF071fcreatedBy($f071fcreatedBy)
    {
        $this->f071fcreatedBy = $f071fcreatedBy;

        return $this;
    }

    /**
     * Get f071fcreatedBy
     *
     * @return integer
     */
    public function getF071fcreatedBy()
    {
        return $this->f071fcreatedBy;
    }

    /**
     * Set f071fupdatedBy
     *
     * @param integer $f071fupdatedBy
     *
     * @return T071finvoice
     */
    public function setF071fupdatedBy($f071fupdatedBy)
    {
        $this->f071fupdatedBy = $f071fupdatedBy;

        return $this;
    }

    /**
     * Get f071fupdatedBy
     *
     * @return integer
     */
    public function getF071fupdatedBy()
    {
        return $this->f071fupdatedBy;
    }

    /**
     * Set f071fcreatedDtTm
     *
     * @param \DateTime $f071fcreatedDtTm
     *
     * @return T071finvoice
     */
    public function setF071fcreatedDtTm($f071fcreatedDtTm)
    {
        $this->f071fcreatedDtTm = $f071fcreatedDtTm;

        return $this;
    }

    /**
     * Get f071fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fcreatedDtTm()
    {
        return $this->f071fcreatedDtTm;
    }

    /**
     * Set f071fupdatedDtTm
     *
     * @param \DateTime $f071fupdatedDtTm
     *
     * @return T071finvoice
     */
    public function setF071fupdatedDtTm($f071fupdatedDtTm)
    {
        $this->f071fupdatedDtTm = $f071fupdatedDtTm;

        return $this;
    }

    /**
     * Get f071fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fupdatedDtTm()
    {
        return $this->f071fupdatedDtTm;
    }

    /**
     * Set f071finvoiceTotal
     *
     * @param string $f071finvoiceTotal
     *
     * @return T071finvoice
     */
    public function setF071finvoiceTotal($f071finvoiceTotal)
    {
        $this->f071finvoiceTotal = $f071finvoiceTotal;

        return $this;
    }


    /**
     * Get f071fdiscount
     *
     * @return integer
     */
    public function getF071fdiscount()
    {
        return $this->f071fdiscount;
    }

    /**
     * Set f071fdiscount
     *
     * @param integer $f071fdiscount
     *
     * @return T071finvoice
     */
    public function setF071fdiscount($f071fdiscount)
    {
        $this->f071fdiscount = $f071fdiscount;

        return $this;
    }


    /**
     * Get f071finvoiceTotal
     *
     * @return string
     */
    public function getF071finvoiceTotal()
    {
        return $this->f071finvoiceTotal;
    }

    /**
     * Get f071fisRcp
     *
     * @return integer
     */
    public function getF071fisRcp()
    {
        return $this->f071fisRcp;
    }

    /**
     * Set f071fisRcp
     *
     * @param integer $f071fisRcp
     *
     * @return T071finvoice
     */
    public function setF071fisRcp($f071fisRcp)
    {
        $this->f071fisRcp = $f071fisRcp;

        return $this;
    }


    /**
     * Get f071fprocessed
     *
     * @return integer
     */
    public function getF071fprocessed()
    {
        return $this->f071fprocessed;
    }

    /**
     * Set f071fprocessed
     *
     * @param integer $f071fprocessed
     *
     * @return T071finvoice
     */
    public function setF071fprocessed($f071fprocessed)
    {
        $this->f071fprocessed = $f071fprocessed;

        return $this;
    }

    /**
     * Get f071finvoiceFrom
     *
     * @return integer
     */
    public function getF071finvoiceFrom()
    {
        return $this->f071finvoiceFrom;
    }

    /**
     * Set f071finvoiceFrom
     *
     * @param integer $f071finvoiceFrom
     *
     * @return T071finvoice
     */
    public function setF071finvoiceFrom($f071finvoiceFrom)
    {
        $this->f071finvoiceFrom = $f071finvoiceFrom;

        return $this;
    }

     /**
     * Set f071fidFinancialYear
     *
     * @param integer $f071fidFinancialYear
     *
     * @return T071finvoice
     */
    public function setF071fidFinancialYear($f071fidFinancialYear)
    {
        $this->f071fidFinancialYear = $f071fidFinancialYear;

        return $this;
    }

    /**
     * Get f071fidFinancialYear
     *
     * @return integer
     */
    public function getF071fidFinancialYear()
    {
        return $this->f071fidFinancialYear;
    }

    /**
     * Set f071fdescription
     *
     * @param string $f071fdescription
     *
     * @return T073frequestInvoice
     */
    public function setF071fdescription($f071fdescription)
    {
        $this->f071fdescription = $f071fdescription;

        return $this;
    }

    /**
     * Get f071fdescription
     *
     * @return string
     */
    public function getF071fdescription()
    {
        return $this->f071fdescription;
    }

     /**
     * Set f071fonlinePayment
     *
     * @param string $f071fonlinePayment
     *
     * @return T073frequestInvoice
     */
    public function setF071fonlinePayment($f071fonlinePayment)
    {
        $this->f071fonlinePayment = $f071fonlinePayment;

        return $this;
    }

    /**
     * Get f071fonlinePayment
     *
     * @return string
     */
    public function getF071fonlinePayment()
    {
        return $this->f071fonlinePayment;
    }

    /**
     * Set f071freason
     *
     * @param string $f071freason
     *
     * @return T071finvoice
     */
    public function setF071freason($f071freason)
    {
        $this->f071freason = $f071freason;

        return $this;
    }

    /**
     * Get f071freason
     *
     * @return string
     */
    public function getF071freason()
    {
        return $this->f071freason;
    }

     /**
     * Get f071fisPaid
     *
     * @return integer
     */
    public function getF071fisPaid()
    {
        return $this->f071fisPaid;
    }

    /**
     * Set f071fisPaid
     *
     * @param integer $f071fisPaid
     *
     * @return T071finvoice
     */
    public function setF071fisPaid($f071fisPaid)
    {
        $this->f071fisPaid = $f071fisPaid;

        return $this;
    }
}

