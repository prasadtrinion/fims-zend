<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T069finsuranceSetup
 *
 * @ORM\Table(name="t069finsurance_setup",uniqueConstraints={@ORM\UniqueConstraint(name="f069finstitution_code_UNIQUE", columns={"f069finstitution_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\InsuranceSetupRepository")
 */
class T069finsuranceSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f069fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f069fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f069finstitution_code", type="string", length=25, nullable=true)
     */
    private $f069finstitutionCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f069fname", type="string", length=50, nullable=true)
     */
    private $f069fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f069faddress", type="string", length=100, nullable=true)
     */
    private $f069faddress;

    /**
     * @var string
     *
     * @ORM\Column(name="f069fphone", type="string", length=15, nullable=true)
     */
    private $f069fphone;

    /**
     * @var string
     *
     * @ORM\Column(name="f069femail", type="string", length=20, nullable=true)
     */
    private $f069femail;

    /**
     * @var string
     *
     * @ORM\Column(name="f069fcontact_person", type="string", length=25, nullable=true)
     */
    private $f069fcontactPerson;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f069feffective_date", type="date", nullable=false)
     */
    private $f069feffectiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f069fend_date", type="date", nullable=false)
     */
    private $f069fendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f069fstatus", type="integer", nullable=false)
     */
    private $f069fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f069fcreated_by", type="integer", nullable=true)
     */
    private $f069fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f069fupdated_by", type="integer", nullable=true)
     */
    private $f069fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f069fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f069fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f069fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f069fupdatedDtTm;



    /**
     * Get f069fid
     *
     * @return integer
     */
    public function getF069fid()
    {
        return $this->f069fid;
    }

    /**
     * Set f069finstitutionCode
     *
     * @param string $f069finstitutionCode
     *
     * @return T069finsuranceSetup
     */
    public function setF069finstitutionCode($f069finstitutionCode)
    {
        $this->f069finstitutionCode = $f069finstitutionCode;

        return $this;
    }

    /**
     * Get f069finstitutionCode
     *
     * @return string
     */
    public function getF069finstitutionCode()
    {
        return $this->f069finstitutionCode;
    }

    /**
     * Set f069fname
     *
     * @param string $f069fname
     *
     * @return T069finsuranceSetup
     */
    public function setF069fname($f069fname)
    {
        $this->f069fname = $f069fname;

        return $this;
    }

    /**
     * Get f069fname
     *
     * @return string
     */
    public function getF069fname()
    {
        return $this->f069fname;
    }

    /**
     * Set f069faddress
     *
     * @param string $f069faddress
     *
     * @return T069finsuranceSetup
     */
    public function setF069faddress($f069faddress)
    {
        $this->f069faddress = $f069faddress;

        return $this;
    }

    /**
     * Get f069faddress
     *
     * @return string
     */
    public function getF069faddress()
    {
        return $this->f069faddress;
    }

    /**
     * Set f069fphone
     *
     * @param string $f069fphone
     *
     * @return T069finsuranceSetup
     */
    public function setF069fphone($f069fphone)
    {
        $this->f069fphone = $f069fphone;

        return $this;
    }

    /**
     * Get f069fphone
     *
     * @return string
     */
    public function getF069fphone()
    {
        return $this->f069fphone;
    }

    /**
     * Set f069femail
     *
     * @param string $f069femail
     *
     * @return T069finsuranceSetup
     */
    public function setF069femail($f069femail)
    {
        $this->f069femail = $f069femail;

        return $this;
    }

    /**
     * Get f069femail
     *
     * @return string
     */
    public function getF069femail()
    {
        return $this->f069femail;
    }

    /**
     * Set f069fcontactPerson
     *
     * @param string $f069fcontactPerson
     *
     * @return T069finsuranceSetup
     */
    public function setF069fcontactPerson($f069fcontactPerson)
    {
        $this->f069fcontactPerson = $f069fcontactPerson;

        return $this;
    }

    /**
     * Get f069fcontactPerson
     *
     * @return string
     */
    public function getF069fcontactPerson()
    {
        return $this->f069fcontactPerson;
    }

    /**
     * Set f069feffectiveDate
     *
     * @param \DateTime $f069feffectiveDate
     *
     * @return T069finsuranceSetup
     */
    public function setF069feffectiveDate($f069feffectiveDate)
    {
        $this->f069feffectiveDate = $f069feffectiveDate;

        return $this;
    }

    /**
     * Get f069feffectiveDate
     *
     * @return \DateTime
     */
    public function getF069feffectiveDate()
    {
        return $this->f069feffectiveDate;
    }

    /**
     * Set f069fendDate
     *
     * @param \DateTime $f069fendDate
     *
     * @return T069finsuranceSetup
     */
    public function setF069fendDate($f069fendDate)
    {
        $this->f069fendDate = $f069fendDate;

        return $this;
    }

    /**
     * Get f069fendDate
     *
     * @return \DateTime
     */
    public function getF069fendDate()
    {
        return $this->f069fendDate;
    }

    /**
     * Set f069fstatus
     *
     * @param integer $f069fstatus
     *
     * @return T069finsuranceSetup
     */
    public function setF069fstatus($f069fstatus)
    {
        $this->f069fstatus = $f069fstatus;

        return $this;
    }

    /**
     * Get f069fstatus
     *
     * @return integer
     */
    public function getF069fstatus()
    {
        return $this->f069fstatus;
    }

    /**
     * Set f069fcreatedBy
     *
     * @param integer $f069fcreatedBy
     *
     * @return T069finsuranceSetup
     */
    public function setF069fcreatedBy($f069fcreatedBy)
    {
        $this->f069fcreatedBy = $f069fcreatedBy;

        return $this;
    }

    /**
     * Get f069fcreatedBy
     *
     * @return integer
     */
    public function getF069fcreatedBy()
    {
        return $this->f069fcreatedBy;
    }

    /**
     * Set f069fupdatedBy
     *
     * @param integer $f069fupdatedBy
     *
     * @return T069finsuranceSetup
     */
    public function setF069fupdatedBy($f069fupdatedBy)
    {
        $this->f069fupdatedBy = $f069fupdatedBy;

        return $this;
    }

    /**
     * Get f069fupdatedBy
     *
     * @return integer
     */
    public function getF069fupdatedBy()
    {
        return $this->f069fupdatedBy;
    }

    /**
     * Set f069fcreatedDtTm
     *
     * @param \DateTime $f069fcreatedDtTm
     *
     * @return T069finsuranceSetup
     */
    public function setF069fcreatedDtTm($f069fcreatedDtTm)
    {
        $this->f069fcreatedDtTm = $f069fcreatedDtTm;

        return $this;
    }

    /**
     * Get f069fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF069fcreatedDtTm()
    {
        return $this->f069fcreatedDtTm;
    }

    /**
     * Set f069fupdatedDtTm
     *
     * @param \DateTime $f069fupdatedDtTm
     *
     * @return T069finsuranceSetup
     */
    public function setF069fupdatedDtTm($f069fupdatedDtTm)
    {
        $this->f069fupdatedDtTm = $f069fupdatedDtTm;

        return $this;
    }

    /**
     * Get f069fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF069fupdatedDtTm()
    {
        return $this->f069fupdatedDtTm;
    }
}
