<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T021fadvancePaymentDetails
 *
 * @ORM\Table(name="t021fadvance_payment_details")
 * @ORM\Entity(repositoryClass="Application\Repository\AdvancePaymentRepository")
 */
class T021fadvancePaymentDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f021fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fid_advance", type="integer", nullable=true)
     */
    private $f021fidAdvance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021famount", type="integer", nullable=true)
     */
    private $f021famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fstatus", type="integer", nullable=true)
     */
    private $f021fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fcreated_by", type="integer", nullable=true)
     */
    private $f021fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f021fupdated_by", type="integer", nullable=true)
     */
    private $f021fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f021fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f021fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f021fupdatedDtTm;

	

    /**
     * Get f021fid
     *
     * @return integer
     */
    public function getF021fid()
    {
        return $this->f021fid;
    }


    /**
     * Set f021fidAdvance
     *
     * @param integer $f021fidAdvance
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fidAdvance($f021fidAdvance)
    {
        $this->f021fidAdvance = $f021fidAdvance;

        return $this;
    }

    /**
     * Get f021fidAdvance
     *
     * @return integer
     */
    public function getF021fidAdvance()
    {
        return $this->f021fidAdvance;
    }

    /**
     * Set f021famount
     *
     * @param string $f021famount
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021famount($f021famount)
    {
        $this->f021famount = $f021famount;

        return $this;
    }

    /**
     * Get f021famount
     *
     * @return string
     */
    public function getF021famount()
    {
        return $this->f021famount;
    }

    /**
     * Set f021fstatus
     *
     * @param integer $f021fstatus
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fstatus($f021fstatus)
    {
        $this->f021fstatus = $f021fstatus;

        return $this;
    }

    /**
     * Get f021fstatus
     *
     * @return integer
     */
    public function getF021fstatus()
    {
        return $this->f021fstatus;
    }

    /**
     * Set f021fcreatedBy
     *
     * @param integer $f021fcreatedBy
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fcreatedBy($f021fcreatedBy)
    {
        $this->f021fcreatedBy = $f021fcreatedBy;

        return $this;
    }

    /**
     * Get f021fcreatedBy
     *
     * @return integer
     */
    public function getF021fcreatedBy()
    {
        return $this->f021fcreatedBy;
    }

    /**
     * Set f021fupdatedBy
     *
     * @param integer $f021fupdatedBy
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fupdatedBy($f021fupdatedBy)
    {
        $this->f021fupdatedBy = $f021fupdatedBy;

        return $this;
    }

    /**
     * Get f021fupdatedBy
     *
     * @return integer
     */
    public function getF021fupdatedBy()
    {
        return $this->f021fupdatedBy;
    }

    /**
     * Set f021fcreatedDtTm
     *
     * @param \DateTime $f021fcreatedDtTm
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fcreatedDtTm($f021fcreatedDtTm)
    {
        $this->f021fcreatedDtTm = $f021fcreatedDtTm;

        return $this;
    }

    /**
     * Get f021fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fcreatedDtTm()
    {
        return $this->f021fcreatedDtTm;
    }

    /**
     * Set f021fdate
     *
     * @param \DateTime $f021fdate
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fdate($f021fdate)
    {
        $this->f021fdate = $f021fdate;

        return $this;
    }

    /**
     * Get f021fdate
     *
     * @return \DateTime
     */
    public function getF021fdate()
    {
        return $this->f021fdate;
    }

    /**
     * Set f021fupdatedDtTm
     *
     * @param \DateTime $f021fupdatedDtTm
     *
     * @return T021fadvancePaymentDetails
     */
    public function setF021fupdatedDtTm($f021fupdatedDtTm)
    {
        $this->f021fupdatedDtTm = $f021fupdatedDtTm;

        return $this;
    }

    /**
     * Get f021fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF021fupdatedDtTm()
    {
        return $this->f021fupdatedDtTm;
    }

   
}

