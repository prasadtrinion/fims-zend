<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T091fupdateAsset
 *
 * @ORM\Table(name="t091fupdate_asset", uniqueConstraints={@ORM\UniqueConstraint(name="f091fasset_code", columns={"f091fasset_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\UpdateAssetRepository")
 */
class T091fupdateAsset
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fasset_code", type="string", length=25, nullable=false)
     */
    private $f091fassetCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fasset_description", type="string", length=255, nullable=true)
     */
    private $f091fassetDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_category", type="integer", nullable=true)
     */
    private $f091fidCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_subcategory", type="integer", nullable=true)
     */
    private $f091fidSubcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_type", type="integer", nullable=true)
     */
    private $f091fidType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid_department", type="integer", nullable=true)
     */
    private $f091fidDepartment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091facquisition_date", type="date", nullable=true)
     */
    private $f091facquisitionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fregistration_date", type="date", nullable=true)
     */
    private $f091fregistrationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fapproval_status", type="integer", nullable=true)
     */
    private $f091fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=true)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=true)
     */
    private $f091fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt", type="datetime", nullable=true)
     */
    private $f091fcreatedDt;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=true)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt", type="datetime", nullable=true)
     */
    private $f091fupdatedDt;



    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    /**
     * Set f091fassetCode
     *
     * @param string $f091fassetCode
     *
     * @return T091fupdateAsset
     */
    public function setF091fassetCode($f091fassetCode)
    {
        $this->f091fassetCode = $f091fassetCode;

        return $this;
    }

    /**
     * Get f091fassetCode
     *
     * @return string
     */
    public function getF091fassetCode()
    {
        return $this->f091fassetCode;
    }

    /**
     * Set f091fassetDescription
     *
     * @param string $f091fassetDescription
     *
     * @return T091fupdateAsset
     */
    public function setF091fassetDescription($f091fassetDescription)
    {
        $this->f091fassetDescription = $f091fassetDescription;

        return $this;
    }

    /**
     * Get f091fassetDescription
     *
     * @return string
     */
    public function getF091fassetDescription()
    {
        return $this->f091fassetDescription;
    }

    /**
     * Set f091fidCategory
     *
     * @param integer $f091fidCategory
     *
     * @return T091fupdateAsset
     */
    public function setF091fidCategory($f091fidCategory)
    {
        $this->f091fidCategory = $f091fidCategory;

        return $this;
    }

    /**
     * Get f091fidCategory
     *
     * @return integer
     */
    public function getF091fidCategory()
    {
        return $this->f091fidCategory;
    }

    /**
     * Set f091fidSubcategory
     *
     * @param integer $f091fidSubcategory
     *
     * @return T091fupdateAsset
     */
    public function setF091fidSubcategory($f091fidSubcategory)
    {
        $this->f091fidSubcategory = $f091fidSubcategory;

        return $this;
    }

    /**
     * Get f091fidSubcategory
     *
     * @return integer
     */
    public function getF091fidSubcategory()
    {
        return $this->f091fidSubcategory;
    }

    /**
     * Set f091fidType
     *
     * @param integer $f091fidType
     *
     * @return T091fupdateAsset
     */
    public function setF091fidType($f091fidType)
    {
        $this->f091fidType = $f091fidType;

        return $this;
    }

    /**
     * Get f091fidType
     *
     * @return integer
     */
    public function getF091fidType()
    {
        return $this->f091fidType;
    }

    /**
     * Set f091fidDepartment
     *
     * @param integer $f091fidDepartment
     *
     * @return T091fupdateAsset
     */
    public function setF091fidDepartment($f091fidDepartment)
    {
        $this->f091fidDepartment = $f091fidDepartment;

        return $this;
    }

    /**
     * Get f091fidDepartment
     *
     * @return integer
     */
    public function getF091fidDepartment()
    {
        return $this->f091fidDepartment;
    }

    /**
     * Set f091facquisitionDate
     *
     * @param \DateTime $f091facquisitionDate
     *
     * @return T091fupdateAsset
     */
    public function setF091facquisitionDate($f091facquisitionDate)
    {
        $this->f091facquisitionDate = $f091facquisitionDate;

        return $this;
    }

    /**
     * Get f091facquisitionDate
     *
     * @return \DateTime
     */
    public function getF091facquisitionDate()
    {
        return $this->f091facquisitionDate;
    }

    /**
     * Set f091fregistrationDate
     *
     * @param \DateTime $f091fregistrationDate
     *
     * @return T091fupdateAsset
     */
    public function setF091fregistrationDate($f091fregistrationDate)
    {
        $this->f091fregistrationDate = $f091fregistrationDate;

        return $this;
    }

    /**
     * Get f091fregistrationDate
     *
     * @return \DateTime
     */
    public function getF091fregistrationDate()
    {
        return $this->f091fregistrationDate;
    }

    /**
     * Set f091fapprovalStatus
     *
     * @param integer $f091fapprovalStatus
     *
     * @return T091fupdateAsset
     */
    public function setF091fapprovalStatus($f091fapprovalStatus)
    {
        $this->f091fapprovalStatus = $f091fapprovalStatus;

        return $this;
    }

    /**
     * Get f091fapprovalStatus
     *
     * @return integer
     */
    public function getF091fapprovalStatus()
    {
        return $this->f091fapprovalStatus;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fupdateAsset
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fupdateAsset
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fcreatedDt
     *
     * @param \DateTime $f091fcreatedDt
     *
     * @return T091fupdateAsset
     */
    public function setF091fcreatedDt($f091fcreatedDt)
    {
        $this->f091fcreatedDt = $f091fcreatedDt;

        return $this;
    }

    /**
     * Get f091fcreatedDt
     *
     * @return \DateTime
     */
    public function getF091fcreatedDt()
    {
        return $this->f091fcreatedDt;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fupdateAsset
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fupdatedDt
     *
     * @param \DateTime $f091fupdatedDt
     *
     * @return T091fupdateAsset
     */
    public function setF091fupdatedDt($f091fupdatedDt)
    {
        $this->f091fupdatedDt = $f091fupdatedDt;

        return $this;
    }

    /**
     * Get f091fupdatedDt
     *
     * @return \DateTime
     */
    public function getF091fupdatedDt()
    {
        return $this->f091fupdatedDt;
    }
}
