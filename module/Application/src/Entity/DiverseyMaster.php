<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomerMaster
 *
 * @ORM\Table(name="diversey_master", uniqueConstraints={@ORM\UniqueConstraint(name="diversey_id_UNIQUE", columns={"diversey_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\DiverseyMasterRepository")
 */
class DiverseyMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_name", type="string", length=255, nullable=true)
     */
    private $customerName;

    /**
     * @var string
     *
     * @ORM\Column(name="business_name", type="string", length=255, nullable=true)
     */
    private $businessName;

    /**
     * @var string
     *
     * @ORM\Column(name="diversey_id", type="string", length=45, nullable=false)
     */
    private $diverseyId;

    /**
     * @var string
     *
     * @ORM\Column(name="business_id", type="string", length=45, nullable=true)
     */
    private $businessId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=45, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=45, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=45, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="r12_sales", type="string", length=45, nullable=true)
     */
    private $r12Sales;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return CustomerMaster
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set businessName
     *
     * @param string $businessName
     *
     * @return CustomerMaster
     */
    public function setBusinessName($businessName)
    {
        $this->businessName = $businessName;

        return $this;
    }

    /**
     * Get businessName
     *
     * @return string
     */
    public function getBusinessName()
    {
        return $this->businessName;
    }

    /**
     * Set diverseyId
     *
     * @param string $diverseyId
     *
     * @return CustomerMaster
     */
    public function setDiverseyId($diverseyId)
    {
        $this->diverseyId = $diverseyId;

        return $this;
    }

    /**
     * Get diverseyId
     *
     * @return string
     */
    public function getDiverseyId()
    {
        return $this->diverseyId;
    }

    /**
     * Set businessId
     *
     * @param string $businessId
     *
     * @return CustomerMaster
     */
    public function setBusinessId($businessId)
    {
        $this->businessId = $businessId;

        return $this;
    }

    /**
     * Get businessId
     *
     * @return string
     */
    public function getBusinessId()
    {
        return $this->businessId;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return CustomerMaster
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return CustomerMaster
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return CustomerMaster
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return CustomerMaster
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set r12Sales
     *
     * @param string $r12Sales
     *
     * @return CustomerMaster
     */
    public function setR12Sales($r12Sales)
    {
        $this->r12Sales = $r12Sales;

        return $this;
    }

    /**
     * Get r12Sales
     *
     * @return string
     */
    public function getR12Sales()
    {
        return $this->r12Sales;
    }
}
