<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T056fverimentDetails
 *
 * @ORM\Table(name="t056fveriment_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetVerimentRepository")
 */
class T056fverimentDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f056fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f056fidDetails;

     /**
     * @var integer
     *
     * @ORM\Column(name="f056fid_veriment", type="integer", nullable=false)
     */
    private $f056fidVeriment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fto_id_financial_year", type="integer", nullable=false)
     */
    private $f056ftoIdFinancialYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f056fto_department", type="string",length=20, nullable=false)
     */
    private $f056ftoDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f056fto_fund_code", type="string",length=20, nullable=false)
     */
    private $f056ftoFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f056fto_department_code", type="string",length=20, nullable=false)
     */
    private $f056ftoDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f056fto_activity_code", type="string",length=20, nullable=false)
     */
    private $f056ftoActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f056fto_account_code", type="string",length=20, nullable=false)
     */
    private $f056ftoAccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056famount", type="integer",  nullable=false)
     */
    private $f056famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fcreated_by", type="integer", nullable=false)
     */
    private $f056fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fupdated_by", type="integer", nullable=false)
     */
    private $f056fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f056fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f056fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f056fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f056fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fstatus", type="integer", nullable=false)
     */
    private $f056fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f056fdescription", type="string",length=255, nullable=false)
     */
    private $f056fdescription;

   

    /**
     * Get f056fidDetails
     *
     * @return integer
     */
    public function getF056fidDetails()
    {
        return $this->f056fidDetails;
    }

    /**
     * Set f056fidVeriment
     *
     * @param integer $f056fidVeriment
     *
     * @return T056fverimentDetails
     */
    public function setF056fidVeriment($f056fidVeriment)
    {
        $this->f056fidVeriment = $f056fidVeriment;

        return $this;
    }

    /**
     * Get f056fidVeriment
     *
     * @return integer
     */
    public function getF056fidVeriment()
    {
        return $this->f056fidVeriment;
    }

    /**
     * Set f056fdescription
     *
     * @param integer $f056fdescription
     *
     * @return T056fverimentDetails
     */
    public function setF056fdescription($f056fdescription)
    {
        $this->f056fdescription = $f056fdescription;

        return $this;
    }

    /**
     * Get f056fdescription
     *
     * @return integer
     */
    public function getF056fdescription()
    {
        return $this->f056fdescription;
    }

     /**
     * Set f056fvirementAmount
     *
     * @param integer $f056fvirementAmount
     *
     * @return T056fverimentDetails
     */
    public function setF056fvirementAmount($f056fvirementAmount)
    {
        $this->f056fvirementAmount = $f056fvirementAmount;

        return $this;
    }


    /**
     * Set f056ftoIdFinancialYear
     *
     * @param integer $f056ftoIdFinancialYear
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoIdFinancialYear($f056ftoIdFinancialYear)
    {
        $this->f056ftoIdFinancialYear = $f056ftoIdFinancialYear;

        return $this;
    }

    /**
     * Get f056ftoIdFinancialYear
     *
     * @return integer
     */
    public function getF056ftoIdFinancialYear()
    {
        return $this->f056ftoIdFinancialYear;
    }

    /**
     * Set f056ftoDepartment
     *
     * @param string $f056ftoDepartment
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoDepartment($f056ftoDepartment)
    {
        $this->f056ftoDepartment = $f056ftoDepartment;

        return $this;
    }

    /**
     * Get f056ftoDepartment
     *
     * @return string
     */
    public function getF056ftoDepartment()
    {
        return $this->f056ftoDepartment;
    }

     
    /**
     * Set f056ftoFundCode
     *
     * @param string $f056ftoFundCode
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoFundCode($f056ftoFundCode)
    {
        $this->f056ftoFundCode = $f056ftoFundCode;

        return $this;
    }

    /**
     * Get f056ftoFundCode
     *
     * @return string
     */
    public function getF056ftoFundCode()
    {
        return $this->f056ftoFundCode;
    }

    /**
     * Set f056ftoAccountCode
     *
     * @param string $f056ftoAccountCode
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoAccountCode($f056ftoAccountCode)
    {
        $this->f056ftoAccountCode = $f056ftoAccountCode;

        return $this;
    }

    /**
     * Get f056ftoAccountCode
     *
     * @return string
     */
    public function getF056ftoAccountCode()
    {
        return $this->f056ftoAccountCode;
    }

    /**
     * Set f056ftoActivityCode
     *
     * @param string $f056ftoActivityCode
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoActivityCode($f056ftoActivityCode)
    {
        $this->f056ftoActivityCode = $f056ftoActivityCode;

        return $this;
    }

    /**
     * Get f056ftoActivityCode
     *
     * @return string
     */
    public function getF056ftoActivityCode()
    {
        return $this->f056ftoActivityCode;
    }

    /**
     * Set f056ftoDepartmentCode
     *
     * @param string $f056ftoDepartmentCode
     *
     * @return T056fverimentDetails
     */
    public function setF056ftoDepartmentCode($f056ftoDepartmentCode)
    {
        $this->f056ftoDepartmentCode = $f056ftoDepartmentCode;

        return $this;
    }

    /**
     * Get f056ftoDepartmentCode
     *
     * @return string
     */
    public function getF056ftoDepartmentCode()
    {
        return $this->f056ftoDepartmentCode;
    }

    /**
     * Set f056famount
     *
     * @param integer $f056famount
     *
     * @return T056fverimentDetails
     */
    public function setF056famount($f056famount)
    {
        $this->f056famount = $f056famount;

        return $this;
    }

    /**
     * Get f056famount
     *
     * @return integer
     */
    public function getF056famount()
    {
        return $this->f056famount;
    }


    /**
     * Set f056fcreatedBy
     *
     * @param integer $f056fcreatedBy
     *
     * @return T056fverimentDetails
     */
    public function setF056fcreatedBy($f056fcreatedBy)
    {
        $this->f056fcreatedBy = $f056fcreatedBy;

        return $this;
    }

    /**
     * Get f056fcreatedBy
     *
     * @return integer
     */
    public function getF056fcreatedBy()
    {
        return $this->f056fcreatedBy;
    }

    /**
     * Set f056fupdatedBy
     *
     * @param integer $f056fupdatedBy
     *
     * @return T056fverimentDetails
     */
    public function setF056fupdatedBy($f056fupdatedBy)
    {
        $this->f056fupdatedBy = $f056fupdatedBy;

        return $this;
    }

    /**
     * Get f056fupdatedBy
     *
     * @return integer
     */
    public function getF056fupdatedBy()
    {
        return $this->f056fupdatedBy;
    }

    /**
     * Set f056fcreatedDtTm
     *
     * @param \DateTime $f056fcreatedDtTm
     *
     * @return T056fverimentDetails
     */
    public function setF056fcreatedDtTm($f056fcreatedDtTm)
    {
        $this->f056fcreatedDtTm = $f056fcreatedDtTm;

        return $this;
    }

    /**
     * Get f056fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF056fcreatedDtTm()
    {
        return $this->f056fcreatedDtTm;
    }

    /**
     * Set f056fupdatedDtTm
     *
     * @param \DateTime $f056fupdatedDtTm
     *
     * @return T056fverimentDetails
     */
    public function setF056fupdatedDtTm($f056fupdatedDtTm)
    {
        $this->f056fupdatedDtTm = $f056fupdatedDtTm;

        return $this;
    }

    /**
     * Get f056fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF056fupdatedDtTm()
    {
        return $this->f056fupdatedDtTm;
    }

    /**
     * Set f056fstatus
     *
     * @param integer $f056fstatus
     *
     * @return T056fverimentDetails
     */
    public function setF056fstatus($f056fstatus)
    {
        $this->f056fstatus = $f056fstatus;

        return $this;
    }

    /**
     * Get f056fstatus
     *
     * @return integer
     */
    public function getF056fstatus()
    {
        return $this->f056fstatus;
    }
}
