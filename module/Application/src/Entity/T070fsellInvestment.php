<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T070fsellInvestment
 *
 * @ORM\Table(name="t070fsell_investment")
 * @ORM\Entity(repositoryClass="Application\Repository\SellInvestmentRepository")
 */
class T070fsellInvestment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f070fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f070fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fid_application", type="integer", nullable=false)
     */
    private $f070fidApplication;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fcreated_by", type="integer", nullable=true)
     */
    private $f070fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fupdated_by", type="integer", nullable=true)
     */
    private $f070fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f070fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f070fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f070fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fstatus", type="integer", nullable=false)
     */
    private $f070fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved1_status", type="integer", nullable=true)
     */
    private $f070fapproved1Status;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved2_status", type="integer", nullable=true)
     */
    private $f070fapproved2Status;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fapprove1_reason", type="string", length=300, nullable=true)
     */
    private $f070fapprove1Reason;

    /**
     * @var string
     *
     * @ORM\Column(name="f070fapprove2_reason", type="string", length=300, nullable=true)
     */
    private $f070fapprove2Reason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved1_by", type="integer", nullable=true)
     */
    private $f070fapproved1By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved2_by", type="integer", nullable=true)
     */
    private $f070fapproved2By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved1_updated_by", type="integer", nullable=true)
     */
    private $f070fapproved1UpdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f070fapproved2_updated_by", type="integer", nullable=true)
     */
    private $f070fapproved2UpdatedBy;



    /**
     * Get f070fid
     *
     * @return integer
     */
    public function getF070fid()
    {
        return $this->f070fid;
    }

    /**
     * Set f070fidApplication
     *
     * @param integer $f070fidApplication
     *
     * @return T070fsellInvestment
     */
    public function setF070fidApplication($f070fidApplication)
    {
        $this->f070fidApplication = $f070fidApplication;

        return $this;
    }

    /**
     * Get f070fidApplication
     *
     * @return integer
     */
    public function getF070fidApplication()
    {
        return $this->f070fidApplication;
    }

    /**
     * Set f070fcreatedBy
     *
     * @param integer $f070fcreatedBy
     *
     * @return T070fsellInvestment
     */
    public function setF070fcreatedBy($f070fcreatedBy)
    {
        $this->f070fcreatedBy = $f070fcreatedBy;

        return $this;
    }

    /**
     * Get f070fcreatedBy
     *
     * @return integer
     */
    public function getF070fcreatedBy()
    {
        return $this->f070fcreatedBy;
    }

    /**
     * Set f070fupdatedBy
     *
     * @param integer $f070fupdatedBy
     *
     * @return T070fsellInvestment
     */
    public function setF070fupdatedBy($f070fupdatedBy)
    {
        $this->f070fupdatedBy = $f070fupdatedBy;

        return $this;
    }

    /**
     * Get f070fupdatedBy
     *
     * @return integer
     */
    public function getF070fupdatedBy()
    {
        return $this->f070fupdatedBy;
    }

    /**
     * Set f070fcreatedDtTm
     *
     * @param \DateTime $f070fcreatedDtTm
     *
     * @return T070fsellInvestment
     */
    public function setF070fcreatedDtTm($f070fcreatedDtTm)
    {
        $this->f070fcreatedDtTm = $f070fcreatedDtTm;

        return $this;
    }

    /**
     * Get f070fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fcreatedDtTm()
    {
        return $this->f070fcreatedDtTm;
    }

    /**
     * Set f070fupdatedDtTm
     *
     * @param \DateTime $f070fupdatedDtTm
     *
     * @return T070fsellInvestment
     */
    public function setF070fupdatedDtTm($f070fupdatedDtTm)
    {
        $this->f070fupdatedDtTm = $f070fupdatedDtTm;

        return $this;
    }

    /**
     * Get f070fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF070fupdatedDtTm()
    {
        return $this->f070fupdatedDtTm;
    }

    /**
     * Set f070fstatus
     *
     * @param integer $f070fstatus
     *
     * @return T070fsellInvestment
     */
    public function setF070fstatus($f070fstatus)
    {
        $this->f070fstatus = $f070fstatus;

        return $this;
    }

    /**
     * Get f070fstatus
     *
     * @return integer
     */
    public function getF070fstatus()
    {
        return $this->f070fstatus;
    }

    /**
     * Set f070fapproved1Status
     *
     * @param integer $f070fapproved1Status
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved1Status($f070fapproved1Status)
    {
        $this->f070fapproved1Status = $f070fapproved1Status;

        return $this;
    }

    /**
     * Get f070fapproved1Status
     *
     * @return integer
     */
    public function getF070fapproved1Status()
    {
        return $this->f070fapproved1Status;
    }

    /**
     * Set f070fapproved2Status
     *
     * @param integer $f070fapproved2Status
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved2Status($f070fapproved2Status)
    {
        $this->f070fapproved2Status = $f070fapproved2Status;

        return $this;
    }

    /**
     * Get f070fapproved2Status
     *
     * @return integer
     */
    public function getF070fapproved2Status()
    {
        return $this->f070fapproved2Status;
    }

    /**
     * Set f070fapprove1Reason
     *
     * @param string $f070fapprove1Reason
     *
     * @return T070fsellInvestment
     */
    public function setF070fapprove1Reason($f070fapprove1Reason)
    {
        $this->f070fapprove1Reason = $f070fapprove1Reason;

        return $this;
    }

    /**
     * Get f070fapprove1Reason
     *
     * @return string
     */
    public function getF070fapprove1Reason()
    {
        return $this->f070fapprove1Reason;
    }

    /**
     * Set f070fapprove2Reason
     *
     * @param string $f070fapprove2Reason
     *
     * @return T070fsellInvestment
     */
    public function setF070fapprove2Reason($f070fapprove2Reason)
    {
        $this->f070fapprove2Reason = $f070fapprove2Reason;

        return $this;
    }

    /**
     * Get f070fapprove2Reason
     *
     * @return string
     */
    public function getF070fapprove2Reason()
    {
        return $this->f070fapprove2Reason;
    }

    /**
     * Set f070fapproved1By
     *
     * @param integer $f070fapproved1By
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved1By($f070fapproved1By)
    {
        $this->f070fapproved1By = $f070fapproved1By;

        return $this;
    }

    /**
     * Get f070fapproved1By
     *
     * @return integer
     */
    public function getF070fapproved1By()
    {
        return $this->f070fapproved1By;
    }

    /**
     * Set f070fapproved2By
     *
     * @param integer $f070fapproved2By
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved2By($f070fapproved2By)
    {
        $this->f070fapproved2By = $f070fapproved2By;

        return $this;
    }

    /**
     * Get f070fapproved2By
     *
     * @return integer
     */
    public function getF070fapproved2By()
    {
        return $this->f070fapproved2By;
    }

    /**
     * Set f070fapproved1UpdatedBy
     *
     * @param integer $f070fapproved1UpdatedBy
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved1UpdatedBy($f070fapproved1UpdatedBy)
    {
        $this->f070fapproved1UpdatedBy = $f070fapproved1UpdatedBy;

        return $this;
    }

    /**
     * Get f070fapproved1UpdatedBy
     *
     * @return integer
     */
    public function getF070fapproved1UpdatedBy()
    {
        return $this->f070fapproved1UpdatedBy;
    }

    /**
     * Set f070fapproved2UpdatedBy
     *
     * @param integer $f070fapproved2UpdatedBy
     *
     * @return T070fsellInvestment
     */
    public function setF070fapproved2UpdatedBy($f070fapproved2UpdatedBy)
    {
        $this->f070fapproved2UpdatedBy = $f070fapproved2UpdatedBy;

        return $this;
    }

    /**
     * Get f070fapproved2UpdatedBy
     *
     * @return integer
     */
    public function getF070fapproved2UpdatedBy()
    {
        return $this->f070fapproved2UpdatedBy;
    }
}
