<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T134fstockRegistration
 *
 * @ORM\Table(name="t134fstock_registration")
 * @ORM\Entity(repositoryClass="Application\Repository\StockRegistrationMirrorRepository")
 */
class T134fstockRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f134fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f134fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f134fasset_category", type="integer",  nullable=false)
     */
    private $f134fassetCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f134fasset_sub_category", type="integer", nullable=false)
     */
    private $f134fassetSubCategory;


    /**
     * @var integer
     *
     * @ORM\Column(name="f134fasset_item", type="integer", nullable=false)
     * 
     */
    private $f134fassetItem;


    /**
     * @var integer
     *
     * @ORM\Column(name="f134fstore", type="integer", nullable=false)
     * 
     */
    private $f134fstore;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fquantity", type="integer", nullable=false)
     * 
     */
    private $f134fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fdate", type="datetime", nullable=false)
     * 
     */
    private $f134fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fstatus", type="integer", nullable=false)
     */
    private $f134fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f134fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f134fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f134fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f134fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f134fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f134fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f134fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f134fupdatedDtTm;
 	

    /**
     * Get f134fid
     *
     * @return integer
     */
    public function getF134fid()
    {
        return $this->f134fid;
    }

    /**
     * Set f134fassetCategory
     *
     * @param string $f134fassetCategory
     *
     * @return T134fstockRegistration
     */
    public function setF134fassetCategory($f134fassetCategory)
    {
        $this->f134fassetCategory = $f134fassetCategory;

        return $this;
    }

    /**
     * Get f134fassetCategory
     *
     * @return string
     */
    public function getF134fassetCategory()
    {
        return $this->f134fassetCategory;
    }

    /**
     * Set f134fassetSubCategory
     *
     * @param string $f134fassetSubCategory
     *
     * @return T134fstockRegistration
     */
    public function setF134fassetSubCategory($f134fassetSubCategory)
    {
        $this->f134fassetSubCategory = $f134fassetSubCategory;

        return $this;
    }

    /**
     * Get f134fassetSubCategory
     *
     * @return string
     */
    public function getF134fassetSubCategory()
    {
        return $this->f134fassetSubCategory;
    }


    /**
     * Set f134fassetItem
     *
     * @param integer $f134fassetItem
     *
     * @return T134fstockRegistration
     */
    public function setF134fassetItem($f134fassetItem)
    {
        $this->f134fassetItem = $f134fassetItem;

        return $this;
    }

    /**
     * Get f134fassetItem
     *
     * @return integer
     */
    public function getF134fassetItem()
    {
        return $this->f134fassetItem;
    }

    /**
     * Set f134fstore
     *
     * @param integer $f134fstore
     *
     * @return T134fstockRegistration
     */
    public function setF134fstore($f134fstore)
    {
        $this->f134fstore = $f134fstore;

        return $this;
    }

    /**
     * Get f134fstore
     *
     * @return integer
     */
    public function getF134fstore()
    {
        return $this->f134fstore;
    }

    /**
     * Set f134fquantity
     *
     * @param integer $f134fquantity
     *
     * @return T134fstockRegistration
     */
    public function setF134fquantity($f134fquantity)
    {
        $this->f134fquantity = $f134fquantity;

        return $this;
    }

    /**
     * Get f134fquantity
     *
     * @return integer
     */
    public function getF134fquantity()
    {
        return $this->f134fquantity;
    }



    /**
     * Set f134fdate
     *
     * @param integer $f134fdate
     *
     * @return T134fstockRegistration
     */
    public function setF134fdate($f134fdate)
    {
        $this->f134fdate = $f134fdate;

        return $this;
    }

    /**
     * Get f134fdate
     *
     * @return integer
     */
    public function getF134fdate()
    {
        return $this->f134fdate;
    }

    /**
     * Set f134fupdatedDtTm
     *
     * @param \DateTime $f134fupdatedDtTm
     *
     * @return T134fstockRegistration
     */
    public function setF134fupdatedDtTm($f134fupdatedDtTm)
    {
        $this->f134fupdatedDtTm = $f134fupdatedDtTm;

        return $this;
    }

    /**
     * Get f134fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF134fupdatedDtTm()
    {
        return $this->f134fupdatedDtTm;
    }

    /**
     * Set f134fstatus
     *
     * @param integer $f134fstatus
     *
     * @return T134fstockRegistration
     */
    public function setF134fstatus($f134fstatus)
    {
        $this->f134fstatus = $f134fstatus;

        return $this;
    }

    /**
     * Get f134fstatus
     *
     * @return integer
     */
    public function getF134fstatus()
    {
        return $this->f134fstatus;
    }

    /**
     * Set f134fupdatedBy
     *
     * @param integer $f134fupdatedBy
     *
     * @return T134fstockRegistration
     */
    public function setF134fupdatedBy($f134fupdatedBy)
    {
        $this->f134fupdatedBy = $f134fupdatedBy;

        return $this;
    }

    /**
     * Get f134fupdatedBy
     *
     * @return integer
     */
    public function getF134fupdatedBy()
    {
        return $this->f134fupdatedBy;
    }

}
