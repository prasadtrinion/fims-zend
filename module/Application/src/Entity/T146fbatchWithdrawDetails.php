<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T146fbatchWithdrawDetails
 *
 * @ORM\Table(name="t146fbatch_withdraw_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BatchWithdrawRepository")
 */
class T146fbatchWithdrawDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f146fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f146fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f146fapplication", type="integer", nullable=false)
     */
    private $f146fapplication;

    /**
     * @var string
     *
     * @ORM\Column(name="f146fid_batch", type="integer", nullable=false)
     */
    private $f146fidBatch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f146famount", type="integer", nullable=false)
     * 
     */
    private $f146famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f146fmaturity_date", type="datetime", nullable=false)
     * 
     */
    private $f146fmaturityDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f146fstatus", type="integer", nullable=false)
     */
    private $f146fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f146fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f146fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f146fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f146fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f146fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f146fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f146fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f146fupdatedDtTm;
 	

    /**
     * Get f146fid
     *
     * @return integer
     */
    public function getF146fid()
    {
        return $this->f146fid;
    }

    /**
     * Set f146fapplication
     *
     * @param string $f146fapplication
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fapplication($f146fapplication)
    {
        $this->f146fapplication = $f146fapplication;

        return $this;
    }

    /**
     * Get f146fapplication
     *
     * @return string
     */
    public function getF146fapplication()
    {
        return $this->f146fapplication;
    }

    /**
     * Set f146fidBatch
     *
     * @param string $f146fidBatch
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fidBatch($f146fidBatch)
    {
        $this->f146fidBatch = $f146fidBatch;

        return $this;
    }

    /**
     * Get f146fidBatch
     *
     * @return string
     */
    public function getF146fidBatch()
    {
        return $this->f146fidBatch;
    }


    /**
     * Set f146fmaturityDate
     *
     * @param integer $f146fmaturityDate
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fmaturityDate($f146fmaturityDate)
    {
        $this->f146fmaturityDate = $f146fmaturityDate;

        return $this;
    }

    /**
     * Get f146fmaturityDate
     *
     * @return integer
     */
    public function getF146fmaturityDate()
    {
        return $this->f146fmaturityDate;
    }

    /**
     * Set f146famount
     *
     * @param integer $f146famount
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146famount($f146famount)
    {
        $this->f146famount = $f146famount;

        return $this;
    }

    /**
     * Get f146famount
     *
     * @return integer
     */
    public function getF146famount()
    {
        return $this->f146famount;
    }


    /**
     * Set f146fupdatedDtTm
     *
     * @param \DateTime $f146fupdatedDtTm
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fupdatedDtTm($f146fupdatedDtTm)
    {
        $this->f146fupdatedDtTm = $f146fupdatedDtTm;

        return $this;
    }

    /**
     * Get f146fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF146fupdatedDtTm()
    {
        return $this->f146fupdatedDtTm;
    }

      /**
     * Set f146fcreatedDtTm
     *
     * @param \DateTime $f146fcreatedDtTm
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fcreatedDtTm($f146fcreatedDtTm)
    {
        $this->f146fcreatedDtTm = $f146fcreatedDtTm;

        return $this;
    }

    /**
     * Get f146fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF146fcreatedDtTm()
    {
        return $this->f146fcreatedDtTm;
    }

    /**
     * Set f146fstatus
     *
     * @param integer $f146fstatus
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fstatus($f146fstatus)
    {
        $this->f146fstatus = $f146fstatus;

        return $this;
    }

    /**
     * Get f146fstatus
     *
     * @return integer
     */
    public function getF146fstatus()
    {
        return $this->f146fstatus;
    }

    /**
     * Set f146fupdatedBy
     *
     * @param integer $f146fupdatedBy
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fupdatedBy($f146fupdatedBy)
    {
        $this->f146fupdatedBy = $f146fupdatedBy;

        return $this;
    }

    /**
     * Get f146fupdatedBy
     *
     * @return integer
     */
    public function getF146fupdatedBy()
    {
        return $this->f146fupdatedBy;
    }

     /**
     * Set f146fcreatedBy
     *
     * @param integer $f146fcreatedBy
     *
     * @return T146fbatchWithdrawDetails
     */
    public function setF146fcreatedBy($f146fcreatedBy)
    {
        $this->f146fcreatedBy = $f146fcreatedBy;

        return $this;
    }

    /**
     * Get f146fcreatedBy
     *
     * @return integer
     */
    public function getF146fcreatedBy()
    {
        return $this->f146fcreatedBy;
    }

}
