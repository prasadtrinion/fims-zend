<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T078futility
 *
 * @ORM\Table(name="t078futility")
 * @ORM\Entity(repositoryClass="Application\Repository\UtilityRepository")
 */
class T078futility
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f078fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fpremise", type="integer", nullable=false)
     */
    private $f078fpremise;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid_number", type="integer", nullable=false)
     */
    private $f078fidNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fid_revenue", type="integer", nullable=false)
     */
    private $f078fidRevenue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fdate", type="datetime", nullable=false)
     */
    private $f078fdate;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fcredit_fund", type="string", length=20, nullable=false)
     */
    private $f078fcreditFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fcredit_department", type="string", length=20, nullable=false)
     */
    private $f078fcreditDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fcredit_activity", type="string", length=20, nullable=false)
     */
    private $f078fcreditActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fcredit_account", type="string", length=20, nullable=false)
     */
    private $f078fcreditAccount;

     /**
     * @var string
     *
     * @ORM\Column(name="f078fdebit_fund", type="string", length=20, nullable=false)
     */
    private $f078fdebitFund;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fdebit_department", type="string", length=20, nullable=false)
     */
    private $f078fdebitDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fdebit_activity", type="string", length=20, nullable=false)
     */
    private $f078fdebitActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fdebit_account", type="string", length=20, nullable=false)
     */
    private $f078fdebitAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="f078finitial_reading", type="integer", nullable=false)
     */
    private $f078finitialReading;

    /**
     * @var string
     *
     * @ORM\Column(name="f078flast_reading", type="integer", nullable=false)
     */
    private $f078flastReading;

    /**
     * @var string
     *
     * @ORM\Column(name="f078fdescription", type="string", length=20, nullable=false)
     */
    private $f078fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fstatus", type="integer", nullable=false)
     */
    private $f078fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fcreated_by", type="integer", nullable=false)
     */
    private $f078fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f078fupdated_by", type="integer", nullable=false)
     */
    private $f078fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f078fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f078fupdatedDtTm;

	

    /**
     * Get f078fid
     *
     * @return integer
     */
    public function getF078fid()
    {
        return $this->f078fid;
    }

    /**
     * Set f078fidNumber
     *
     * @param integer $f078fidNumber
     *
     * @return T078futility
     */
    public function setF078fidNumber($f078fidNumber)
    {
        $this->f078fidNumber = $f078fidNumber;

        return $this;
    }

    /**
     * Get f078fidNumber
     *
     * @return integer
     */
    public function getF078fidNumber()
    {
        return $this->f078fidNumber;
    }

    /**
     * Set f078fidRevenue
     *
     * @param integer $f078fidRevenue
     *
     * @return T078futility
     */
    public function setF078fidRevenue($f078fidRevenue)
    {
        $this->f078fidRevenue = $f078fidRevenue;

        return $this;
    }

    /**
     * Get f078fidRevenue
     *
     * @return integer
     */
    public function getF078fidRevenue()
    {
        return $this->f078fidRevenue;
    }
    /**
     * Set f078fpremise
     *
     * @param integer $f078fpremise
     *
     * @return T078futility
     */
    public function setF078fpremise($f078fpremise)
    {
        $this->f078fpremise = $f078fpremise;

        return $this;
    }

    /**
     * Get f078fpremise
     *
     * @return integer
     */
    public function getF078fpremise()
    {
        return $this->f078fpremise;
    }

    /**
     * Set f078finitialReading
     *
     * @param string $f078finitialReading
     *
     * @return T078futility
     */
    public function setF078finitialReading($f078finitialReading)
    {
        $this->f078finitialReading = $f078finitialReading;

        return $this;
    }

    /**
     * Get f078finitialReading
     *
     * @return string
     */
    public function getF078finitialReading()
    {
        return $this->f078finitialReading;
    }

    /**
     * Set f078flastReading
     *
     * @param string $f078flastReading
     *
     * @return T078futility
     */
    public function setF078flastReading($f078flastReading)
    {
        $this->f078flastReading = $f078flastReading;

        return $this;
    }

    /**
     * Get f078flastReading
     *
     * @return string
     */
    public function getF078flastReading()
    {
        return $this->f078flastReading;
    }

    /**
     * Set f078fdescription
     *
     * @param string $f078fdescription
     *
     * @return T078futility
     */
    public function setF078fdescription($f078fdescription)
    {
        $this->f078fdescription = $f078fdescription;

        return $this;
    }

    /**
     * Get f078fdescription
     *
     * @return string
     */
    public function getF078fdescription()
    {
        return $this->f078fdescription;
    }


    /**
     * Set f078fstatus
     *
     * @param integer $f078fstatus
     *
     * @return T078futility
     */
    public function setF078fstatus($f078fstatus)
    {
        $this->f078fstatus = $f078fstatus;

        return $this;
    }

    /**
     * Get f078fstatus
     *
     * @return integer
     */
    public function getF078fstatus()
    {
        return $this->f078fstatus;
    }

    /**
     * Set f078fcreatedBy
     *
     * @param integer $f078fcreatedBy
     *
     * @return T078futility
     */
    public function setF078fcreatedBy($f078fcreatedBy)
    {
        $this->f078fcreatedBy = $f078fcreatedBy;

        return $this;
    }

    /**
     * Get f078fcreatedBy
     *
     * @return integer
     */
    public function getF078fcreatedBy()
    {
        return $this->f078fcreatedBy;
    }

    /**
     * Set f078fupdatedBy
     *
     * @param integer $f078fupdatedBy
     *
     * @return T078futility
     */
    public function setF078fupdatedBy($f078fupdatedBy)
    {
        $this->f078fupdatedBy = $f078fupdatedBy;

        return $this;
    }

    /**
     * Get f078fupdatedBy
     *
     * @return integer
     */
    public function getF078fupdatedBy()
    {
        return $this->f078fupdatedBy;
    }

    /**
     * Set f078fcreatedDtTm
     *
     * @param \DateTime $f078fcreatedDtTm
     *
     * @return T078futility
     */
    public function setF078fcreatedDtTm($f078fcreatedDtTm)
    {
        $this->f078fcreatedDtTm = $f078fcreatedDtTm;

        return $this;
    }

    /**
     * Get f078fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fcreatedDtTm()
    {
        return $this->f078fcreatedDtTm;
    }

    /**
     * Set f078fdate
     *
     * @param \DateTime $f078fdate
     *
     * @return T078futility
     */
    public function setF078fdate($f078fdate)
    {
        $this->f078fdate = $f078fdate;

        return $this;
    }

    /**
     * Get f078fdate
     *
     * @return \DateTime
     */
    public function getF078fdate()
    {
        return $this->f078fdate;
    }

    /**
     * Set f078fupdatedDtTm
     *
     * @param \DateTime $f078fupdatedDtTm
     *
     * @return T078futility
     */
    public function setF078fupdatedDtTm($f078fupdatedDtTm)
    {
        $this->f078fupdatedDtTm = $f078fupdatedDtTm;

        return $this;
    }

    /**
     * Get f078fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF078fupdatedDtTm()
    {
        return $this->f078fupdatedDtTm;
    }

    /**
     * Set f078fcreditFund
     *
     * @param string $f078fcreditFund
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fcreditFund($f078fcreditFund)
    {
        $this->f078fcreditFund = $f078fcreditFund;

        return $this;
    }

    /**
     * Get f078fcreditFund
     *
     * @return string
     */
    public function getF078fcreditFund()
    {
        return $this->f078fcreditFund;
    }

    /**
     * Set f078fcreditActivity
     *
     * @param string $f078fcreditActivity
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fcreditActivity($f078fcreditActivity)
    {
        $this->f078fcreditActivity = $f078fcreditActivity;

        return $this;
    }

    /**
     * Get f078fcreditActivity
     *
     * @return string
     */
    public function getF078fcreditActivity()
    {
        return $this->f078fcreditActivity;
    }

    /**
     * Set f078fcreditDepartment
     *
     * @param string $f078fcreditDepartment
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fcreditDepartment($f078fcreditDepartment)
    {
        $this->f078fcreditDepartment = $f078fcreditDepartment;

        return $this;
    }

    /**
     * Get f078fcreditDepartment
     *
     * @return string
     */
    public function getF078fcreditDepartment()
    {
        return $this->f078fcreditDepartment;
    }

    /**
     * Set f078fcreditAccount
     *
     * @param string $f078fcreditAccount
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fcreditAccount($f078fcreditAccount)
    {
        $this->f078fcreditAccount = $f078fcreditAccount;

        return $this;
    }

    /**
     * Get f078fcreditAccount
     *
     * @return string
     */
    public function getF078fcreditAccount()
    {
        return $this->f078fcreditAccount;
    }

    /**
     * Set f078fdebitFund
     *
     * @param string $f078fdebitFund
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fdebitFund($f078fdebitFund)
    {
        $this->f078fdebitFund = $f078fdebitFund;

        return $this;
    }

    /**
     * Get f078fdebitFund
     *
     * @return string
     */
    public function getF078fdebitFund()
    {
        return $this->f078fdebitFund;
    }

    /**
     * Set f078fdebitActivity
     *
     * @param string $f078fdebitActivity
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fdebitActivity($f078fdebitActivity)
    {
        $this->f078fdebitActivity = $f078fdebitActivity;

        return $this;
    }

    /**
     * Get f078fdebitActivity
     *
     * @return string
     */
    public function getF078fdebitActivity()
    {
        return $this->f078fdebitActivity;
    }

    /**
     * Set f078fdebitDepartment
     *
     * @param string $f078fdebitDepartment
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fdebitDepartment($f078fdebitDepartment)
    {
        $this->f078fdebitDepartment = $f078fdebitDepartment;

        return $this;
    }

    /**
     * Get f078fdebitDepartment
     *
     * @return string
     */
    public function getF078fdebitDepartmentId()
    {
        return $this->f078fdebitDepartmentId;
    }

    /**
     * Set f078fdebitAccount
     *
     * @param string $f078fdebitAccount
     *
     * @return T078frecurringSetupDetails
     */
    public function setF078fdebitAccount($f078fdebitAccount)
    {
        $this->f078fdebitAccount = $f078fdebitAccount;

        return $this;
    }

    /**
     * Get f078fdebitAccount
     *
     * @return string
     */
    public function getF078fdebitAccount()
    {
        return $this->f078fdebitAccount;
    }
}

