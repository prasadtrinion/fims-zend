<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T043fsmartLoan
 *
 * @ORM\Table(name="t043fsmart_loan",uniqueConstraints={@ORM\UniqueConstraint(name="f043fmodel_UNIQUE", columns={"f043fmodel"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SmartLoanRepository")
 */
class T043fsmartLoan
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f043fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fmodel", type="string", length=25, nullable=false)
     */
    private $f043fmodel;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid_vendor", type="integer", nullable=false)
     */
    private $f043fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043finsurance", type="integer", nullable=false)
     */
    private $f043finsurance;

    /**
     * @var string
     *
     * @ORM\Column(name="f043floan_entitlement", type="string", length=100, nullable=false)
     */
    private $f043floanEntitlement;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fpurchase_price", type="string", length=100, nullable=false)
     */
    private $f043fpurchasePrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043floan_amount", type="integer", nullable=false)
     */
    private $f043floanAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f043finstallment_period", type="string", length=100, nullable=false)
     */
    private $f043finstallmentPeriod;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043femp_id", type="integer", nullable=false)
     */
    private $f043fempId;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freference", type="string", length=100, nullable=false)
     */
    private $f043freference;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043ffirst_guarantor", type="integer", nullable=false)
     */
    private $f043ffirstGuarantor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fstatus", type="integer", nullable=false)
     */
    private $f043fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fcreated_by", type="integer", nullable=false)
     */
    private $f043fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fupdated_by", type="integer", nullable=false)
     */
    private $f043fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f043fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f043fupdatedDtTm;



    /**
     * Get f043fid
     *
     * @return integer
     */
    public function getF043fid()
    {
        return $this->f043fid;
    }

    /**
     * Set f043fmodel
     *
     * @param integer $f043fmodel
     *
     * @return T043fsmartLoan
     */
    public function setF043fmodel($f043fmodel)
    {
        $this->f043fmodel = $f043fmodel;

        return $this;
    }

    /**
     * Get f043fmodel
     *
     * @return integer
     */
    public function getF043fmodel()
    {
        return $this->f043fmodel;
    }

    /**
     * Set f043fidVendor
     *
     * @param integer $f043fidVendor
     *
     * @return T043fsmartLoan
     */
    public function setF043fidVendor($f043fidVendor)
    {
        $this->f043fidVendor = $f043fidVendor;

        return $this;
    }

    /**
     * Get f043fidVendor
     *
     * @return integer
     */
    public function getF043fidVendor()
    {
        return $this->f043fidVendor;
    }

    /**
     * Set f043finsurance
     *
     * @param integer $f043finsurance
     *
     * @return T043fsmartLoan
     */
    public function setF043finsurance($f043finsurance)
    {
        $this->f043finsurance = $f043finsurance;

        return $this;
    }

    /**
     * Get f043finsurance
     *
     * @return integer
     */
    public function getF043finsurance()
    {
        return $this->f043finsurance;
    }

    /**
     * Set f043floanEntitlement
     *
     * @param string $f043floanEntitlement
     *
     * @return T043fsmartLoan
     */
    public function setF043floanEntitlement($f043floanEntitlement)
    {
        $this->f043floanEntitlement = $f043floanEntitlement;

        return $this;
    }

    /**
     * Get f043floanEntitlement
     *
     * @return string
     */
    public function getF043floanEntitlement()
    {
        return $this->f043floanEntitlement;
    }

    /**
     * Set f043fpurchasePrice
     *
     * @param string $f043fpurchasePrice
     *
     * @return T043fsmartLoan
     */
    public function setF043fpurchasePrice($f043fpurchasePrice)
    {
        $this->f043fpurchasePrice = $f043fpurchasePrice;

        return $this;
    }

    /**
     * Get f043fpurchasePrice
     *
     * @return string
     */
    public function getF043fpurchasePrice()
    {
        return $this->f043fpurchasePrice;
    }

    /**
     * Set f043floanAmount
     *
     * @param string $f043floanAmount
     *
     * @return T043fsmartLoan
     */
    public function setF043floanAmount($f043floanAmount)
    {
        $this->f043floanAmount = $f043floanAmount;

        return $this;
    }

    /**
     * Get f043floanAmount
     *
     * @return string
     */
    public function getF043floanAmount()
    {
        return $this->f043floanAmount;
    }

    /**
     * Set f043finstallmentPeriod
     *
     * @param string $f043finstallmentPeriod
     *
     * @return T043fsmartLoan
     */
    public function setF043finstallmentPeriod($f043finstallmentPeriod)
    {
        $this->f043finstallmentPeriod = $f043finstallmentPeriod;

        return $this;
    }

    /**
     * Get f043finstallmentPeriod
     *
     * @return string
     */
    public function getF043finstallmentPeriod()
    {
        return $this->f043finstallmentPeriod;
    }

    /**
     * Set f043fempId
     *
     * @param integer $f043fempId
     *
     * @return T043fsmartLoan
     */
    public function setF043fempId($f043fempId)
    {
        $this->f043fempId = $f043fempId;

        return $this;
    }

    /**
     * Get f043fempId
     *
     * @return integer
     */
    public function getF043fempId()
    {
        return $this->f043fempId;
    }

    /**
     * Set f043freference
     *
     * @param string $f043freference
     *
     * @return T043fsmartLoan
     */
    public function setF043freference($f043freference)
    {
        $this->f043freference = $f043freference;

        return $this;
    }

    /**
     * Get f043freference
     *
     * @return string
     */
    public function getF043freference()
    {
        return $this->f043freference;
    }

    /**
     * Set f043ffirstGuarantor
     *
     * @param integer $f043ffirstGuarantor
     *
     * @return T043fsmartLoan
     */
    public function setF043ffirstGuarantor($f043ffirstGuarantor)
    {
        $this->f043ffirstGuarantor = $f043ffirstGuarantor;

        return $this;
    }

    /**
     * Get f043ffirstGuarantor
     *
     * @return integer
     */
    public function getF043ffirstGuarantor()
    {
        return $this->f043ffirstGuarantor;
    }

    /**
     * Set f043fstatus
     *
     * @param integer $f043fstatus
     *
     * @return T043fsmartLoan
     */
    public function setF043fstatus($f043fstatus)
    {
        $this->f043fstatus = $f043fstatus;

        return $this;
    }

    /**
     * Get f043fstatus
     *
     * @return integer
     */
    public function getF043fstatus()
    {
        return $this->f043fstatus;
    }

    /**
     * Set f043fcreatedBy
     *
     * @param integer $f043fcreatedBy
     *
     * @return T043fsmartLoan
     */
    public function setF043fcreatedBy($f043fcreatedBy)
    {
        $this->f043fcreatedBy = $f043fcreatedBy;

        return $this;
    }

    /**
     * Get f043fcreatedBy
     *
     * @return integer
     */
    public function getF043fcreatedBy()
    {
        return $this->f043fcreatedBy;
    }

    /**
     * Set f043fupdatedBy
     *
     * @param integer $f043fupdatedBy
     *
     * @return T043fsmartLoan
     */
    public function setF043fupdatedBy($f043fupdatedBy)
    {
        $this->f043fupdatedBy = $f043fupdatedBy;

        return $this;
    }

    /**
     * Get f043fupdatedBy
     *
     * @return integer
     */
    public function getF043fupdatedBy()
    {
        return $this->f043fupdatedBy;
    }

    /**
     * Set f043fcreatedDtTm
     *
     * @param \DateTime $f043fcreatedDtTm
     *
     * @return T043fsmartLoan
     */
    public function setF043fcreatedDtTm($f043fcreatedDtTm)
    {
        $this->f043fcreatedDtTm = $f043fcreatedDtTm;

        return $this;
    }

    /**
     * Get f043fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fcreatedDtTm()
    {
        return $this->f043fcreatedDtTm;
    }

    /**
     * Set f043fupdatedDtTm
     *
     * @param \DateTime $f043fupdatedDtTm
     *
     * @return T043fsmartLoan
     */
    public function setF043fupdatedDtTm($f043fupdatedDtTm)
    {
        $this->f043fupdatedDtTm = $f043fupdatedDtTm;

        return $this;
    }

    /**
     * Get f043fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fupdatedDtTm()
    {
        return $this->f043fupdatedDtTm;
    }
}
