<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064fsponsorBillDetails
 *
 * @ORM\Table(name="t064fsponsor_bill_details", indexes={@ORM\Index(name="f064fid_sponser_bill", columns={"f064fid_sponser_bill"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorBillRepository")
 */
class T064fsponsorBillDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_invoice", type="integer", nullable=false)
     */
    private $f064fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_student", type="integer", nullable=false)
     */
    private $f064fidStudent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064famount", type="integer",  nullable=false)
     */
    private $f064famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=false)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=false)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f064fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f064fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=false)
     */
    private $f064fstatus;

    /**
     * @var \Application\Entity\T063fsponsorBill
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T063fsponsorBill")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f064fid_sponser_bill", referencedColumnName="f063fid")
     * })
     */
    private $f064fidSponserBill;

        /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064fidInvoice
     *
     * @param integer $f064fidInvoice
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fidInvoice($f064fidInvoice)
    {
        $this->f064fidInvoice = $f064fidInvoice;

        return $this;
    }

    /**
     * Get f064fidInvoice
     *
     * @return integer
     */
    public function getF064fidInvoice()
    {
        return $this->f064fidInvoice;
    }

    /**
     * Set f064fidStudent
     *
     * @param integer $f064fidStudent
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fidStudent($f064fidStudent)
    {
        $this->f064fidStudent = $f064fidStudent;

        return $this;
    }

    /**
     * Get f064fidStudent
     *
     * @return integer
     */
    public function getF064fidStudent()
    {
        return $this->f064fidStudent;
    }

    /**
     * Set f064famount
     *
     * @param integer $f064famount
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064famount($f064famount)
    {
        $this->f064famount = $f064famount;

        return $this;
    }

    /**
     * Get f064famount
     *
     * @return integer
     */
    public function getF064famount()
    {
        return $this->f064famount;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fupdatedDtTm
     *
     * @param \DateTime $f064fupdatedDtTm
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fupdatedDtTm($f064fupdatedDtTm)
    {
        $this->f064fupdatedDtTm = $f064fupdatedDtTm;

        return $this;
    }

    /**
     * Get f064fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdatedDtTm()
    {
        return $this->f064fupdatedDtTm;
    }

    /**
     * Set f064fcreatedDtTm
     *
     * @param \DateTime $f064fcreatedDtTm
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fcreatedDtTm($f064fcreatedDtTm)
    {
        $this->f064fcreatedDtTm = $f064fcreatedDtTm;

        return $this;
    }

    /**
     * Get f064fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreatedDtTm()
    {
        return $this->f064fcreatedDtTm;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fidSponserBill
     *
     * @param \Application\Entity\T063fsponsorBill $f064fidSponserBill
     *
     * @return T064fsponsorBillDetails
     */
    public function setF064fidSponserBill(\Application\Entity\T063fsponsorBill $f064fidSponserBill = null)
    {
        $this->f064fidSponserBill = $f064fidSponserBill;

        return $this;
    }

    /**
     * Get f064fidSponserBill
     *
     * @return \Application\Entity\T063fsponsorBill
     */
    public function getF064fidSponserBill()
    {
        return $this->f064fidSponserBill;
    }

}

