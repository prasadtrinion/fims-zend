<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T093feaFormProcess
 *
 * @ORM\Table(name="t093fea_form_process")
 * @ORM\Entity(repositoryClass="Application\Repository\EaFormProcessRepository")
 */
class T093feaFormProcess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f093fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid_company", type="integer", nullable=false)
     */
    private $f093fidCompany;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fyear", type="integer", nullable=false)
     */
    private $f093fyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid_staff", type="integer", nullable=false)
     */
    private $f093fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fstatus", type="integer", nullable=false)
     */
    private $f093fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fcreated_by", type="integer", nullable=false)
     */
    private $f093fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fupdated_by", type="integer", nullable=false)
     */
    private $f093fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f093fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f093fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f093fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f093fupdatedDtTm;



    /**
     * Get f093fid
     *
     * @return integer
     */
    public function getF093fid()
    {
        return $this->f093fid;
    }

    /**
     * Set f093fidCompany
     *
     * @param integer $f093fidCompany
     *
     * @return T093feaFormProcess
     */
    public function setF093fidCompany($f093fidCompany)
    {
        $this->f093fidCompany = $f093fidCompany;

        return $this;
    }

    /**
     * Get f093fidCompany
     *
     * @return integer
     */
    public function getF093fidCompany()
    {
        return $this->f093fidCompany;
    }

    /**
     * Set f093fyear
     *
     * @param integer $f093fyear
     *
     * @return T093feaFormProcess
     */
    public function setF093fyear($f093fyear)
    {
        $this->f093fyear = $f093fyear;

        return $this;
    }

    /**
     * Get f093fyear
     *
     * @return integer
     */
    public function getF093fyear()
    {
        return $this->f093fyear;
    }

    /**
     * Set f093fidStaff
     *
     * @param integer $f093fidStaff
     *
     * @return T093feaFormProcess
     */
    public function setF093fidStaff($f093fidStaff)
    {
        $this->f093fidStaff = $f093fidStaff;

        return $this;
    }

    /**
     * Get f093fidStaff
     *
     * @return integer
     */
    public function getF093fidStaff()
    {
        return $this->f093fidStaff;
    }

    /**
     * Set f093fstatus
     *
     * @param integer $f093fstatus
     *
     * @return T093feaFormProcess
     */
    public function setF093fstatus($f093fstatus)
    {
        $this->f093fstatus = $f093fstatus;

        return $this;
    }

    /**
     * Get f093fstatus
     *
     * @return integer
     */
    public function getF093fstatus()
    {
        return $this->f093fstatus;
    }

    /**
     * Set f093fcreatedBy
     *
     * @param integer $f093fcreatedBy
     *
     * @return T093feaFormProcess
     */
    public function setF093fcreatedBy($f093fcreatedBy)
    {
        $this->f093fcreatedBy = $f093fcreatedBy;

        return $this;
    }

    /**
     * Get f093fcreatedBy
     *
     * @return integer
     */
    public function getF093fcreatedBy()
    {
        return $this->f093fcreatedBy;
    }

    /**
     * Set f093fupdatedBy
     *
     * @param integer $f093fupdatedBy
     *
     * @return T093feaFormProcess
     */
    public function setF093fupdatedBy($f093fupdatedBy)
    {
        $this->f093fupdatedBy = $f093fupdatedBy;

        return $this;
    }

    /**
     * Get f093fupdatedBy
     *
     * @return integer
     */
    public function getF093fupdatedBy()
    {
        return $this->f093fupdatedBy;
    }

    /**
     * Set f093fcreatedDtTm
     *
     * @param \DateTime $f093fcreatedDtTm
     *
     * @return T093feaFormProcess
     */
    public function setF093fcreatedDtTm($f093fcreatedDtTm)
    {
        $this->f093fcreatedDtTm = $f093fcreatedDtTm;

        return $this;
    }

    /**
     * Get f093fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF093fcreatedDtTm()
    {
        return $this->f093fcreatedDtTm;
    }

    /**
     * Set f093fupdatedDtTm
     *
     * @param \DateTime $f093fupdatedDtTm
     *
     * @return T093feaFormProcess
     */
    public function setF093fupdatedDtTm($f093fupdatedDtTm)
    {
        $this->f093fupdatedDtTm = $f093fupdatedDtTm;

        return $this;
    }

    /**
     * Get f093fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF093fupdatedDtTm()
    {
        return $this->f093fupdatedDtTm;
    }
}
