<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T031flicenseInformation
 *
 * @ORM\Table(name="t031flicense_information")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T031flicenseInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f031fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f031fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f031fid_license", type="integer",  nullable=true)
     */
    private $f031fidLicense;


    /**
     * @var string
     *
     * @ORM\Column(name="f031fid_supplier_reg", type="integer",  nullable=true)
     */
    private $f031fidSupplierReg;

    /**
     * @var string
     *
     * @ORM\Column(name="f031fexpire_date", type="datetime", nullable=true)
     */
    private $f031fexpireDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fcreated_by", type="integer", nullable=true)
     */
    private $f031fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fupdated_by", type="integer", nullable=true)
     */
    private $f031fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f031fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f031fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f031fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f031fstatus", type="integer", nullable=true)
     */
    private $f031fstatus = '1';



    /**
     * Get f031fid
     *
     * @return integer
     */
    public function getF031fid()
    {
        return $this->f031fid;
    }

    /**
     * Set f031fidLicense
     *
     * @param string $f031fidLicense
     *
     * @return T031flicenseInformation
     */
    public function setF031fidLicense($f031fidLicense)
    {
        $this->f031fidLicense = $f031fidLicense;

        return $this;
    }

    /**
     * Get f031fidLicense
     *
     * @return string
     */
    public function getF031fidLicense()
    {
        return $this->f031fidLicense;
    }

    /**
     * Set f031fexpireDate
     *
     * @param string $f031fexpireDate
     *
     * @return T031flicenseInformation
     */
    public function setF031fexpireDate($f031fexpireDate)
    {
        $this->f031fexpireDate = $f031fexpireDate;

        return $this;
    }

    /**
     * Get f031fexpireDate
     *
     * @return string
     */
    public function getF031fexpireDate()
    {
        return $this->f031fexpireDate;
    }

  
    /**
     * Set f031fcreatedBy
     *
     * @param integer $f031fcreatedBy
     *
     * @return T031flicenseInformation
     */
    public function setF031fcreatedBy($f031fcreatedBy)
    {
        $this->f031fcreatedBy = $f031fcreatedBy;

        return $this;
    }

    /**
     * Get f031fcreatedBy
     *
     * @return integer
     */
    public function getF031fcreatedBy()
    {
        return $this->f031fcreatedBy;
    }

    /**
     * Set f031fupdatedBy
     *
     * @param integer $f031fupdatedBy
     *
     * @return T031flicenseInformation
     */
    public function setF031fupdatedBy($f031fupdatedBy)
    {
        $this->f031fupdatedBy = $f031fupdatedBy;

        return $this;
    }

    /**
     * Get f031fupdatedBy
     *
     * @return integer
     */
    public function getF031fupdatedBy()
    {
        return $this->f031fupdatedBy;
    }

    /**
     * Set f031fcreatedDtTm
     *
     * @param \DateTime $f031fcreatedDtTm
     *
     * @return T031flicenseInformation
     */
    public function setF031fcreatedDtTm($f031fcreatedDtTm)
    {
        $this->f031fcreatedDtTm = $f031fcreatedDtTm;

        return $this;
    }

    /**
     * Get f031fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF031fcreatedDtTm()
    {
        return $this->f031fcreatedDtTm;
    }

    /**
     * Set f031fupdatedDtTm
     *
     * @param \DateTime $f031fupdatedDtTm
     *
     * @return T031flicenseInformation
     */
    public function setF031fupdatedDtTm($f031fupdatedDtTm)
    {
        $this->f031fupdatedDtTm = $f031fupdatedDtTm;

        return $this;
    }

    /**
     * Get f031fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF031fupdatedDtTm()
    {
        return $this->f031fupdatedDtTm;
    }

    /**
     * Set f031fstatus
     *
     * @param integer $f031fstatus
     *
     * @return T031flicenseInformation
     */
    public function setF031fstatus($f031fstatus)
    {
        $this->f031fstatus = $f031fstatus;

        return $this;
    }

    /**
     * Get f031fstatus
     *
     * @return integer
     */
    public function getF031fstatus()
    {
        return $this->f031fstatus;
    }

     /**
     * Set f031fidSupplierReg
     *
     * @param integer $f031fidSupplierReg
     *
     * @return T031flicenseInformation
     */
    public function setF031fidSupplierReg($f031fidSupplierReg)
    {
        $this->f031fidSupplierReg = $f031fidSupplierReg;

        return $this;
    }

    /**
     * Get f031fidSupplierReg
     *
     * @return integer
     */
    public function getF031fidSupplierReg()
    {
        return $this->f031fidSupplierReg;
    }
}
