<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T115fvendorMof
 *
 * @ORM\Table(name="t115fvendor_mof")
 * @ORM\Entity(repositoryClass="Application\Repository\VendorRegistrationRepository")
 */
class T115fvendorMof  
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f115fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f115fid;
 
    /**
     * @var string
     *
     * @ORM\Column(name="f115fmode_of_category", type="string", length=25, nullable=false)
     */
    private $f115fmodeOfCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="f115fid_vendor", type="integer", nullable=false)
     */
    private $f115fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f115fstatus", type="integer", nullable=false)
     */
    private $f115fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f115fcreated_by", type="integer", nullable=false)
     */
    private $f115fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f115fupdated_by", type="integer", nullable=false)
     */
    private $f115fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f115fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f115fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f115fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f115fupdatedDtTm;



    /**
     * Get f115fid
     *
     * @return integer
     */
    public function getF115fid()
    {
        return $this->f115fid;
    }


    /**
     * Set f115fmodeOfCategory
     *
     * @param string $f115fmodeOfCategory
     *
     * @return T115fvendorMof
     */
    public function setF115fmodeOfCategory($f115fmodeOfCategory)
    {
        $this->f115fmodeOfCategory = $f115fmodeOfCategory;

        return $this;
    }

    /**
     * Get f115fmodeOfCategory
     *
     * @return string
     */
    public function getF115fmodeOfCategory()
    {
        return $this->f115fmodeOfCategory;
    }

    /**
     * Set f115fstatus
     *
     * @param integer $f115fstatus
     *
     * @return T115fvendorMof
     */
    public function setF115fstatus($f115fstatus)
    {
        $this->f115fstatus = $f115fstatus;

        return $this;
    }

    /**
     * Get f115fstatus
     *
     * @return integer
     */
    public function getF115fstatus()
    {
        return $this->f115fstatus;
    }

    /**
     * Set f115fidVendor
     *
     * @param integer $f115fidVendor
     *
     * @return T115fvendorMof
     */
    public function setF115fidVendor($f115fidVendor)
    {
        $this->f115fidVendor = $f115fidVendor;

        return $this;
    }

    /**
     * Get f115fidVendor
     *
     * @return integer
     */
    public function getF115fidVendor()
    {
        return $this->f115fidVendor;
    }

    /**
     * Set f115fcreatedBy
     *
     * @param integer $f115fcreatedBy
     *
     * @return T115fvendorMof
     */
    public function setF115fcreatedBy($f115fcreatedBy)
    {
        $this->f115fcreatedBy = $f115fcreatedBy;

        return $this;
    }

    /**
     * Get f115fcreatedBy
     *
     * @return integer
     */
    public function getF115fcreatedBy()
    {
        return $this->f115fcreatedBy;
    }

    /**
     * Set f115fupdatedBy
     *
     * @param integer $f115fupdatedBy
     *
     * @return T115fvendorMof
     */
    public function setF115fupdatedBy($f115fupdatedBy)
    {
        $this->f115fupdatedBy = $f115fupdatedBy;

        return $this;
    }

    /**
     * Get f115fupdatedBy
     *
     * @return integer
     */
    public function getF115fupdatedBy()
    {
        return $this->f115fupdatedBy;
    }

    /**
     * Set f115fcreatedDtTm
     *
     * @param \DateTime $f115fcreatedDtTm
     *
     * @return T115fvendorMof
     */
    public function setF115fcreatedDtTm($f115fcreatedDtTm)
    {
        $this->f115fcreatedDtTm = $f115fcreatedDtTm;

        return $this;
    }

    /**
     * Get f115fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF115fcreatedDtTm()
    {
        return $this->f115fcreatedDtTm;
    }

    /**
     * Set f115fupdatedDtTm
     *
     * @param \DateTime $f115fupdatedDtTm
     *
     * @return T115fvendorMof
     */
    public function setF115fupdatedDtTm($f115fupdatedDtTm)
    {
        $this->f115fupdatedDtTm = $f115fupdatedDtTm;

        return $this;
    }

    /**
     * Get f115fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF115fupdatedDtTm()
    {
        return $this->f115fupdatedDtTm;
    }
}
