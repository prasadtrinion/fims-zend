<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T084freceiptInvoice
 *
 * @ORM\Table(name="t084freceipt_invoice")
 * @ORM\Entity(repositoryClass="Application\Repository\ReceiptRepository")
 */
class T084freceiptInvoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f084fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f084fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f084fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f084fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f084fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f084fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f084fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f084fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f084fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f084fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f084fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f084fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f084fcreditAccountCode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f084ftotal", type="integer", nullable=false)
     */
    private $f084ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f084fid_invoice", type="integer", nullable=false)
     */
    private $f084fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f084fstatus", type="integer", nullable=false)
     */
    private $f084fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f084fcreated_by", type="integer", nullable=false)
     */
    private $f084fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f084fupdated_by", type="integer", nullable=false)
     */
    private $f084fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f084fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f084fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f084fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f084fupdatedDtTm;

    /**
     * @var \Application\Entity\T082freceipt
     *
     * @ORM\Column(name="f084fid_receipt", type="integer", nullable=false)
     */
    private $f084fidReceipt;

    /**
     * Get f084fid
     *
     * @return integer
     */
    public function getF084fid()
    {
        return $this->f084fid;
    }

    
    /**
     * Set f084fdebitFundCode
     *
     * @param integer $f084fdebitFundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fdebitFundCode($f084fdebitFundCode)
    {
        $this->f084fdebitFundCode = $f084fdebitFundCode;

        return $this;
    }

    /**
     * Get f084fdebitFundCode
     *
     * @return integer
     */
    public function getF084fdebitFundCode()
    {
        return $this->f084fdebitFundCode;
    }

    /**
     * Set f084fdebitAccountCode
     *
     * @param integer $f084fdebitAccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fdebitAccountCode($f084fdebitAccountCode)
    {
        $this->f084fdebitAccountCode = $f084fdebitAccountCode;

        return $this;
    }

    /**
     * Get f084fdebitAccountCode
     *
     * @return integer
     */
    public function getF084fdebitAccountCode()
    {
        return $this->f084fdebitAccountCode;
    }

    /**
     * Set f084fdebitActivityCode
     *
     * @param integer $f084fdebitActivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fdebitActivityCode($f084fdebitActivityCode)
    {
        $this->f084fdebitActivityCode = $f084fdebitActivityCode;

        return $this;
    }

    /**
     * Get f084fdebitActivityCode
     *
     * @return integer
     */
    public function getF084fdebitActivityCode()
    {
        return $this->f084fdebitActivityCode;
    }

    /**
     * Set f084fdebitDepartmentCode
     *
     * @param integer $f084fdebitDepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fdebitDepartmentCode($f084fdebitDepartmentCode)
    {
        $this->f084fdebitDepartmentCode = $f084fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f084fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF084fdebitDepartmentCode()
    {
        return $this->f084fdebitDepartmentCode;
    }


    /**
     * Set f084fcreditFundCode
     *
     * @param integer $f084fcreditFundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fcreditFundCode($f084fcreditFundCode)
    {
        $this->f084fcreditFundCode = $f084fcreditFundCode;

        return $this;
    }

    /**
     * Get f084fcreditFundCode
     *
     * @return integer
     */
    public function getF084fcreditFundCode()
    {
        return $this->f084fcreditFundCode;
    }

    /**
     * Set f084fcreditAccountCode
     *
     * @param integer $f084fcreditAccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fcreditAccountCode($f084fcreditAccountCode)
    {
        $this->f084fcreditAccountCode = $f084fcreditAccountCode;

        return $this;
    }

    /**
     * Get f084fcreditAccountCode
     *
     * @return integer
     */
    public function getF084fcreditAccountCode()
    {
        return $this->f084fcreditAccountCode;
    }

    /**
     * Set f084fcreditActivityCode
     *
     * @param integer $f084fcreditActivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fcreditActivityCode($f084fcreditActivityCode)
    {
        $this->f084fcreditActivityCode = $f084fcreditActivityCode;

        return $this;
    }

    /**
     * Get f084fcreditActivityCode
     *
     * @return integer
     */
    public function getF084fcreditActivityCode()
    {
        return $this->f084fcreditActivityCode;
    }

    /**
     * Set f084fcreditDepartmentCode
     *
     * @param integer $f084fcreditDepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF084fcreditDepartmentCode($f084fcreditDepartmentCode)
    {
        $this->f084fcreditDepartmentCode = $f084fcreditDepartmentCode;

        return $this;
    }

    /**
     * Set f084ftotal
     *
     * @param integer $f084ftotal
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084ftotal($f084ftotal)
    {
        $this->f084ftotal = $f084ftotal;

        return $this;
    }

    /**
     * Get f084ftotal
     *
     * @return integer
     */
    public function getF084ftotal()
    {
        return $this->f084ftotal;
    }

    /**
     * Set f084fstatus
     *
     * @param integer $f084fstatus
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084fstatus($f084fstatus)
    {
        $this->f084fstatus = $f084fstatus;

        return $this;
    }

    /**
     * Get f084fstatus
     *
     * @return integer
     */
    public function getF084fstatus()
    {
        return $this->f084fstatus;
    }

    /**
     * Set f084fcreatedBy
     *
     * @param integer $f084fcreatedBy
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084fcreatedBy($f084fcreatedBy)
    {
        $this->f084fcreatedBy = $f084fcreatedBy;

        return $this;
    }

    /**
     * Get f084fcreatedBy
     *
     * @return integer
     */
    public function getF084fcreatedBy()
    {
        return $this->f084fcreatedBy;
    }

    /**
     * Set f084fupdatedBy
     *
     * @param integer $f084fupdatedBy
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084fupdatedBy($f084fupdatedBy)
    {
        $this->f084fupdatedBy = $f084fupdatedBy;

        return $this;
    }

    /**
     * Get f084fupdatedBy
     *
     * @return integer
     */
    public function getF084fupdatedBy()
    {
        return $this->f084fupdatedBy;
    }

    /**
     * Set f084fcreatedDtTm
     *
     * @param \DateTime $f084fcreatedDtTm
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084fcreatedDtTm($f084fcreatedDtTm)
    {
        $this->f084fcreatedDtTm = $f084fcreatedDtTm;

        return $this;
    }

    /**
     * Get f084fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF084fcreatedDtTm()
    {
        return $this->f084fcreatedDtTm;
    }

    /**
     * Set f084fupdatedDtTm
     *
     * @param \DateTime $f084fupdatedDtTm
     *
     * @return T084freceiptNonInvoice
     */
    public function setF084fupdatedDtTm($f084fupdatedDtTm)
    {
        $this->f084fupdatedDtTm = $f084fupdatedDtTm;

        return $this;
    }

    /**
     * Get f084fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF084fupdatedDtTm()
    {
        return $this->f084fupdatedDtTm;
    }
    
     /**
     * Set f084fidInvoice
     *
     * @param integer $f084fidInvoice
     *
     * @return T084freceiptPaymentInfo
     */
    public function setF084fidInvoice($f084fidInvoice)
    {
        $this->f084fidInvoice = $f084fidInvoice;

        return $this;
    }

    /**
     * Get f084fidInvoice
     *
     * @return integer
     */
    public function getF084fidInvoice()
    {
        return $this->f084fidInvoice;
    }
   
     /**
     * Set f084fidReceipt
     *
     * @param \Application\Entity\T082freceipt $f084fidReceipt
     *
     * @return T084freceiptPaymentInfo
     */
    public function setF084fidReceipt($f084fidReceipt = null)
    {
        $this->f084fidReceipt = $f084fidReceipt;

        return $this;
    }

    /**
     * Get f084fidReceipt
     *
     * @return \Application\Entity\T082freceipt
     */
    public function getF084fidReceipt()
    {
        return $this->f084fidReceipt;
    }
}

