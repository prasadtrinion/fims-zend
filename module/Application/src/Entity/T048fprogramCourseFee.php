<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T048fprogramCourseFee
 *
 * @ORM\Table(name="t048fprogram_Course_fee")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeStructureRepository")
 */
class T048fprogramCourseFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_program_course", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f048fidProgramCourse;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_course_fee", type="integer", nullable=true)
     */
    private $f048fidCourseFee;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_program", type="integer", nullable=true)
     */
    private $f048fidProgram;

     /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_subject", type="integer", nullable=true)
     */
    private $f048fidSubject;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048famount", type="integer", nullable=true)
     */
    private $f048famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstatus", type="integer", nullable=true)
     */
    private $f048fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcreated_by", type="integer", nullable=true)
     */
    private $f048fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fupdated_by", type="integer", nullable=true)
     */
    private $f048fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fupdatedDtTm;



    /**
     * Get f048fidProgramCourse
     *
     * @return integer
     */
    public function getF048fidProgramCourse()
    {
        return $this->f048fidProgramCourse;
    }

    /**
     * Set f048fidCourseFee
     *
     * @param integer $f048fidCourseFee
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fidCourseFee($f048fidCourseFee)
    {
        $this->f048fidCourseFee = $f048fidCourseFee;

        return $this;
    }

    /**
     * Get f048fidCourseFee
     *
     * @return integer
     */
    public function getF048fidCourseFee()
    {
        return $this->f048fidCourseFee;
    }

    /**
     * Set f048fidSubject
     *
     * @param integer $f048fidSubject
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fidSubject($f048fidSubject)
    {
        $this->f048fidSubject = $f048fidSubject;

        return $this;
    }

    /**
     * Get f048fidSubject
     *
     * @return integer
     */
    public function getF048fidSubject()
    {
        return $this->f048fidSubject;
    }

    /**
     * Set f048fidProgram
     *
     * @param integer $f048fidProgram
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fidProgram($f048fidProgram)
    {
        $this->f048fidProgram = $f048fidProgram;

        return $this;
    }

    /**
     * Get f048fidProgram
     *
     * @return integer
     */
    public function getF048fidProgram()
    {
        return $this->f048fidProgram;
    }

    /**
     * Set f048famount
     *
     * @param string $f048famount
     *
     * @return T048fprogramCourseFee
     */
    public function setF048famount($f048famount)
    {
        $this->f048famount = $f048famount;

        return $this;
    }

    /**
     * Get f048famount
     *
     * @return string
     */
    public function getF048famount()
    {
        return $this->f048famount;
    }

    /**
     * Set f048fstatus
     *
     * @param integer $f048fstatus
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fstatus($f048fstatus)
    {
        $this->f048fstatus = $f048fstatus;

        return $this;
    }

    /**
     * Get f048fstatus
     *
     * @return integer
     */
    public function getF048fstatus()
    {
        return $this->f048fstatus;
    }

    /**
     * Set f048fcreatedBy
     *
     * @param integer $f048fcreatedBy
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fcreatedBy($f048fcreatedBy)
    {
        $this->f048fcreatedBy = $f048fcreatedBy;

        return $this;
    }

    /**
     * Get f048fcreatedBy
     *
     * @return integer
     */
    public function getF048fcreatedBy()
    {
        return $this->f048fcreatedBy;
    }

    /**
     * Set f048fupdatedBy
     *
     * @param integer $f048fupdatedBy
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fupdatedBy($f048fupdatedBy)
    {
        $this->f048fupdatedBy = $f048fupdatedBy;

        return $this;
    }

    /**
     * Get f048fupdatedBy
     *
     * @return integer
     */
    public function getF048fupdatedBy()
    {
        return $this->f048fupdatedBy;
    }

    /**
     * Set f048fcreatedDtTm
     *
     * @param \DateTime $f048fcreatedDtTm
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fcreatedDtTm($f048fcreatedDtTm)
    {
        $this->f048fcreatedDtTm = $f048fcreatedDtTm;

        return $this;
    }

    /**
     * Get f048fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fcreatedDtTm()
    {
        return $this->f048fcreatedDtTm;
    }

    /**
     * Set f048fupdatedDtTm
     *
     * @param \DateTime $f048fupdatedDtTm
     *
     * @return T048fprogramCourseFee
     */
    public function setF048fupdatedDtTm($f048fupdatedDtTm)
    {
        $this->f048fupdatedDtTm = $f048fupdatedDtTm;

        return $this;
    }

    /**
     * Get f048fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fupdatedDtTm()
    {
        return $this->f048fupdatedDtTm;
    }
}
