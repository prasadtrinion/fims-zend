<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T047fsubject
 *
 * @ORM\Table(name="t047fsubject")
 * @ORM\Entity(repositoryClass="Application\Repository\SubjectRepository")
 */
class T047fsubject
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f047fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f047fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f047fdescription", type="string", length=50, nullable=false)
     */
    private $f047fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f047fcode", type="string", length=20, nullable=false)
     */
    private $f047fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fstatus", type="integer", nullable=false)
     */
    private $f047fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fcreated_by", type="integer", nullable=false)
     */
    private $f047fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f047fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f047fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f047fupdated_by", type="integer", nullable=false)
     */
    private $f047fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f047fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f047fupdatedDtTm;



    /**
     * Get f047fid
     *
     * @return integer
     */
    public function getF047fid()
    {
        return $this->f047fid;
    }

    /**
     * Set f047fdescription
     *
     * @param string $f047fdescription
     *
     * @return T047fsubject
     */
    public function setF047fdescription($f047fdescription)
    {
        $this->f047fdescription = $f047fdescription;

        return $this;
    }

    /**
     * Get f047fdescription
     *
     * @return string
     */
    public function getF047fdescription()
    {
        return $this->f047fdescription;
    }

    /**
     * Set f047fcode
     *
     * @param string $f047fcode
     *
     * @return T047fsubject
     */
    public function setF047fcode($f047fcode)
    {
        $this->f047fcode = $f047fcode;

        return $this;
    }

    /**
     * Get f047fcode
     *
     * @return string
     */
    public function getF047fcode()
    {
        return $this->f047fcode;
    }

    /**
     * Set f047fstatus
     *
     * @param integer $f047fstatus
     *
     * @return T047fsubject
     */
    public function setF047fstatus($f047fstatus)
    {
        $this->f047fstatus = $f047fstatus;

        return $this;
    }

    /**
     * Get f047fstatus
     *
     * @return integer
     */
    public function getF047fstatus()
    {
        return $this->f047fstatus;
    }

    /**
     * Set f047fcreatedBy
     *
     * @param integer $f047fcreatedBy
     *
     * @return T047fsubject
     */
    public function setF047fcreatedBy($f047fcreatedBy)
    {
        $this->f047fcreatedBy = $f047fcreatedBy;

        return $this;
    }

    /**
     * Get f047fcreatedBy
     *
     * @return integer
     */
    public function getF047fcreatedBy()
    {
        return $this->f047fcreatedBy;
    }

    /**
     * Set f047fcreatedDtTm
     *
     * @param \DateTime $f047fcreatedDtTm
     *
     * @return T047fsubject
     */
    public function setF047fcreatedDtTm($f047fcreatedDtTm)
    {
        $this->f047fcreatedDtTm = $f047fcreatedDtTm;

        return $this;
    }

    /**
     * Get f047fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF047fcreatedDtTm()
    {
        return $this->f047fcreatedDtTm;
    }

    /**
     * Set f047fupdatedBy
     *
     * @param integer $f047fupdatedBy
     *
     * @return T047fsubject
     */
    public function setF047fupdatedBy($f047fupdatedBy)
    {
        $this->f047fupdatedBy = $f047fupdatedBy;

        return $this;
    }

    /**
     * Get f047fupdatedBy
     *
     * @return integer
     */
    public function getF047fupdatedBy()
    {
        return $this->f047fupdatedBy;
    }

    /**
     * Set f047fupdatedDtTm
     *
     * @param \DateTime $f047fupdatedDtTm
     *
     * @return T047fsubject
     */
    public function setF047fupdatedDtTm($f047fupdatedDtTm)
    {
        $this->f047fupdatedDtTm = $f047fupdatedDtTm;

        return $this;
    }

    /**
     * Get f047fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF047fupdatedDtTm()
    {
        return $this->f047fupdatedDtTm;
    }
}
