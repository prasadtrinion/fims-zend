<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T128fpaymentAward
 *
 * @ORM\Table(name="t128fpayment_award")
 * @ORM\Entity(repositoryClass="Application\Repository\PaymentAwardRepository")
 */
class T128fpaymentAward
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f128fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f128fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f128faward", type="integer", nullable=false)
     */
    private $f128faward;

    /**
     * @var integer
     *
     * @ORM\Column(name="f128fstatus", type="integer", nullable=true)
     */
    private $f128fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f128fcreated_by", type="integer", nullable=false)
     */
    private $f128fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f128fupdated_by", type="integer", nullable=false)
     */
    private $f128fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f128fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f128fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f128fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f128fupdatedDtTm;


    /**
     * Get f128fid
     *
     * @return integer
     */
    public function getF128fid()
    {
        return $this->f128fid;
    }


    /**
     * Set f128faward
     *
     * @param integer $f128faward
     *
     * @return T128fpaymentAward
     */
    public function setF128faward($f128faward)
    {
        $this->f128faward = $f128faward;

        return $this;
    }

    /**
     * Get f128faward
     *
     * @return integer
     */
    public function getF128faward()
    {
        return $this->f128faward;
    }


    /**
     * Set f128fstatus
     *
     * @param integer $f128fstatus
     *
     * @return T128fpaymentAward
     */
    public function setF128fstatus($f128fstatus)
    {
        $this->f128fstatus = $f128fstatus;

        return $this;
    }

    /**
     * Get f128fstatus
     *
     * @return integer
     */
    public function getF128fstatus()
    {
        return $this->f128fstatus;
    }

    /**
     * Set f128fcreatedBy
     *
     * @param integer $f128fcreatedBy
     *
     * @return T128fpaymentAward
     */
    public function setF128fcreatedBy($f128fcreatedBy)
    {
        $this->f128fcreatedBy = $f128fcreatedBy;

        return $this;
    }

    /**
     * Get f128fcreatedBy
     *
     * @return integer
     */
    public function getF128fcreatedBy()
    {
        return $this->f128fcreatedBy;
    }

    /**
     * Set f128fupdatedBy
     *
     * @param integer $f128fupdatedBy
     *
     * @return T128fpaymentAward
     */
    public function setF128fupdatedBy($f128fupdatedBy)
    {
        $this->f128fupdatedBy = $f128fupdatedBy;

        return $this;
    }

    /**
     * Get f128fupdatedBy
     *
     * @return integer
     */
    public function getF128fupdatedBy()
    {
        return $this->f128fupdatedBy;
    }

    /**
     * Set f128fcreatedDtTm
     *
     * @param \DateTime $f128fcreatedDtTm
     *
     * @return T128fpaymentAward
     */
    public function setF128fcreatedDtTm($f128fcreatedDtTm)
    {
        $this->f128fcreatedDtTm = $f128fcreatedDtTm;

        return $this;
    }

    /**
     * Get f128fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF128fcreatedDtTm()
    {
        return $this->f128fcreatedDtTm;
    }

    /**
     * Set f128fupdatedDtTm
     *
     * @param \DateTime $f128fupdatedDtTm
     *
     * @return T128fpaymentAward
     */
    public function setF128fupdatedDtTm($f128fupdatedDtTm)
    {
        $this->f128fupdatedDtTm = $f128fupdatedDtTm;

        return $this;
    }

    /**
     * Get f128fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF128fupdatedDtTm()
    {
        return $this->f128fupdatedDtTm;
    }
}

