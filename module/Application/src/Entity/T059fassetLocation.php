<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T059fassetLocation
 *
 * @ORM\Table(name="t059fasset_location")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetLocationRepository")
 */
class T059fassetLocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f059fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f059fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fcode", type="string", length=255, nullable=false)
     */
    private $f059fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fname", type="string", length=255, nullable=false)
     */
    private $f059fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f059ftype", type="string", length=100, nullable=true)
     */
    private $f059ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fcategory", type="string", length=100, nullable=true)
     */
    private $f059fcategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fdescription", type="string", length=100, nullable=true)
     */
    private $f059fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fshort_code", type="string", length=100, nullable=true)
     */
    private $f059fshortCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fparent_code", type="integer", nullable=false)
     */
    private $f059fparentCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fstatus", type="integer", nullable=false)
     */
    private $f059fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fref_code", type="integer", nullable=true)
     */
    private $f059frefCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059flevel_status", type="integer", nullable=true)
     */
    private $f059flevelStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fcomplete_code", type="string", length=10, nullable=true)
     */
    private $f059fcompleteCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fallow_negative", type="integer", nullable=true)
     */
    private $f059fallowNegative;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fcreated_by", type="integer", nullable=true)
     */
    private $f059fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f059fupdated_by", type="integer", nullable=true)
     */
    private $f059fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f059fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f059fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f059fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f059fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f059fbalance_type", type="string", length=100, nullable=true)
     */
    private $f059fbalanceType;



    /**
     * Get f059fid
     *
     * @return integer
     */
    public function getF059fid()
    {
        return $this->f059fid;
    }

    /**
     * Set f059fcode
     *
     * @param string $f059fcode
     *
     * @return T059fassetLocation
     */
    public function setF059fcode($f059fcode)
    {
        $this->f059fcode = $f059fcode;

        return $this;
    }

    /**
     * Get f059fcode
     *
     * @return string
     */
    public function getF059fcode()
    {
        return $this->f059fcode;
    }

    /**
     * Set f059fname
     *
     * @param string $f059fname
     *
     * @return T059fassetLocation
     */
    public function setF059fname($f059fname)
    {
        $this->f059fname = $f059fname;

        return $this;
    }

    /**
     * Get f059fname
     *
     * @return string
     */
    public function getF059fname()
    {
        return $this->f059fname;
    }

    /**
     * Set f059ftype
     *
     * @param string $f059ftype
     *
     * @return T059fassetLocation
     */
    public function setF059ftype($f059ftype)
    {
        $this->f059ftype = $f059ftype;

        return $this;
    }

    /**
     * Get f059ftype
     *
     * @return string
     */
    public function getF059ftype()
    {
        return $this->f059ftype;
    }

    /**
     * Set f059fcategory
     *
     * @param string $f059fcategory
     *
     * @return T059fassetLocation
     */
    public function setF059fcategory($f059fcategory)
    {
        $this->f059fcategory = $f059fcategory;

        return $this;
    }

    /**
     * Get f059fcategory
     *
     * @return string
     */
    public function getF059fcategory()
    {
        return $this->f059fcategory;
    }

    /**
     * Set f059fdescription
     *
     * @param string $f059fdescription
     *
     * @return T059fassetLocation
     */
    public function setF059fdescription($f059fdescription)
    {
        $this->f059fdescription = $f059fdescription;

        return $this;
    }

    /**
     * Get f059fdescription
     *
     * @return string
     */
    public function getF059fdescription()
    {
        return $this->f059fdescription;
    }

    /**
     * Set f059fshortCode
     *
     * @param string $f059fshortCode
     *
     * @return T059fassetLocation
     */
    public function setF059fshortCode($f059fshortCode)
    {
        $this->f059fshortCode = $f059fshortCode;

        return $this;
    }

    /**
     * Get f059fshortCode
     *
     * @return string
     */
    public function getF059fshortCode()
    {
        return $this->f059fshortCode;
    }

    /**
     * Set f059fparentCode
     *
     * @param integer $f059fparentCode
     *
     * @return T059fassetLocation
     */
    public function setF059fparentCode($f059fparentCode)
    {
        $this->f059fparentCode = $f059fparentCode;

        return $this;
    }

    /**
     * Get f059fparentCode
     *
     * @return integer
     */
    public function getF059fparentCode()
    {
        return $this->f059fparentCode;
    }

    /**
     * Set f059fstatus
     *
     * @param integer $f059fstatus
     *
     * @return T059fassetLocation
     */
    public function setF059fstatus($f059fstatus)
    {
        $this->f059fstatus = $f059fstatus;

        return $this;
    }

    /**
     * Get f059fstatus
     *
     * @return integer
     */
    public function getF059fstatus()
    {
        return $this->f059fstatus;
    }

    /**
     * Set f059frefCode
     *
     * @param integer $f059frefCode
     *
     * @return T059fassetLocation
     */
    public function setF059frefCode($f059frefCode)
    {
        $this->f059frefCode = $f059frefCode;

        return $this;
    }

    /**
     * Get f059frefCode
     *
     * @return integer
     */
    public function getF059frefCode()
    {
        return $this->f059frefCode;
    }

    /**
     * Set f059flevelStatus
     *
     * @param integer $f059flevelStatus
     *
     * @return T059fassetLocation
     */
    public function setF059flevelStatus($f059flevelStatus)
    {
        $this->f059flevelStatus = $f059flevelStatus;

        return $this;
    }

    /**
     * Get f059flevelStatus
     *
     * @return integer
     */
    public function getF059flevelStatus()
    {
        return $this->f059flevelStatus;
    }

    /**
     * Set f059fcompleteCode
     *
     * @param string $f059fcompleteCode
     *
     * @return T059fassetLocation
     */
    public function setF059fcompleteCode($f059fcompleteCode)
    {
        $this->f059fcompleteCode = $f059fcompleteCode;

        return $this;
    }

    /**
     * Get f059fcompleteCode
     *
     * @return string
     */
    public function getF059fcompleteCode()
    {
        return $this->f059fcompleteCode;
    }

    /**
     * Set f059fallowNegative
     *
     * @param integer $f059fallowNegative
     *
     * @return T059fassetLocation
     */
    public function setF059fallowNegative($f059fallowNegative)
    {
        $this->f059fallowNegative = $f059fallowNegative;

        return $this;
    }

    /**
     * Get f059fallowNegative
     *
     * @return integer
     */
    public function getF059fallowNegative()
    {
        return $this->f059fallowNegative;
    }

    /**
     * Set f059fbalanceType
     *
     * @param string $f059fbalanceType
     *
     * @return T059fassetLocation
     */
    public function setF059fbalanceType($f059fbalanceType)
    {
        $this->f059fbalanceType = $f059fbalanceType;

        return $this;
    }

    /**
     * Get f059fbalanceType
     *
     * @return string
     */
    public function getF059fbalanceType()
    {
        return $this->f059fbalanceType;
    }

    /**
     * Set f059fcreatedBy
     *
     * @param integer $f059fcreatedBy
     *
     * @return T059fassetLocation
     */
    public function setF059fcreatedBy($f059fcreatedBy)
    {
        $this->f059fcreatedBy = $f059fcreatedBy;

        return $this;
    }

    /**
     * Get f059fcreatedBy
     *
     * @return integer
     */
    public function getF059fcreatedBy()
    {
        return $this->f059fcreatedBy;
    }

    /**
     * Set f059fupdatedBy
     *
     * @param integer $f059fupdatedBy
     *
     * @return T059fassetLocation
     */
    public function setF059fupdatedBy($f059fupdatedBy)
    {
        $this->f059fupdatedBy = $f059fupdatedBy;

        return $this;
    }

    /**
     * Get f059fupdatedBy
     *
     * @return integer
     */
    public function getF059fupdatedBy()
    {
        return $this->f059fupdatedBy;
    }

    /**
     * Set f059fcreatedDtTm
     *
     * @param \DateTime $f059fcreatedDtTm
     *
     * @return T059fassetLocation
     */
    public function setF059fcreatedDtTm($f059fcreatedDtTm)
    {
        $this->f059fcreatedDtTm = $f059fcreatedDtTm;

        return $this;
    }

    /**
     * Get f059fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF059fcreatedDtTm()
    {
        return $this->f059fcreatedDtTm;
    }

    /**
     * Set f059fupdatedDtTm
     *
     * @param \DateTime $f059fupdatedDtTm
     *
     * @return T059fassetLocation
     */
    public function setF059fupdatedDtTm($f059fupdatedDtTm)
    {
        $this->f059fupdatedDtTm = $f059fupdatedDtTm;

        return $this;
    }

    /**
     * Get f059fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF059fupdatedDtTm()
    {
        return $this->f059fupdatedDtTm;
    }
}
