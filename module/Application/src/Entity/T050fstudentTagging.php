<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T050fstudentTagging
 *
 * @ORM\Table(name="t050fstudent_tagging")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentTaggingRepository")
 */
class T050fstudentTagging
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f050fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid_student", type="integer", nullable=true)
     */
    private $f050fidStudent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fdue_date", type="datetime", nullable=true)
     */
    private $f050fdueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f050fpolicy_no", type="string", length=25, nullable=true)
     */
    private $f050fpolicyNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid_insurance", type="integer", nullable=true)
     */
    private $f050fidInsurance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050ftagging_status", type="integer", nullable=true)
     */
    private $f050ftaggingStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fstatus", type="integer", nullable=true)
     */
    private $f050fstatus = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fcreated_by", type="integer", nullable=true)
     */
    private $f050fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fupdated_by", type="integer", nullable=true)
     */
    private $f050fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f050fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f050fupdatedDtTm;



    /**
     * Get f050fid
     *
     * @return integer
     */
    public function getF050fid()
    {
        return $this->f050fid;
    }

    /**
     * Set f050fidStudent
     *
     * @param integer $f050fidStudent
     *
     * @return T050fstudentTagging
     */
    public function setF050fidStudent($f050fidStudent)
    {
        $this->f050fidStudent = $f050fidStudent;

        return $this;
    }

    /**
     * Get f050fidStudent
     *
     * @return integer
     */
    public function getF050fidStudent()
    {
        return $this->f050fidStudent;
    }

    /**
     * Set f050fdueDate
     *
     * @param \DateTime $f050fdueDate
     *
     * @return T050fstudentTagging
     */
    public function setF050fdueDate($f050fdueDate)
    {
        $this->f050fdueDate = $f050fdueDate;

        return $this;
    }

    /**
     * Get f050fdueDate
     *
     * @return \DateTime
     */
    public function getF050fdueDate()
    {
        return $this->f050fdueDate;
    }

    /**
     * Set f050fpolicyNo
     *
     * @param string $f050fpolicyNo
     *
     * @return T050fstudentTagging
     */
    public function setF050fpolicyNo($f050fpolicyNo)
    {
        $this->f050fpolicyNo = $f050fpolicyNo;

        return $this;
    }

    /**
     * Get f050fpolicyNo
     *
     * @return string
     */
    public function getF050fpolicyNo()
    {
        return $this->f050fpolicyNo;
    }

    /**
     * Set f050fidInsurance
     *
     * @param integer $f050fidInsurance
     *
     * @return T050fstudentTagging
     */
    public function setF050fidInsurance($f050fidInsurance)
    {
        $this->f050fidInsurance = $f050fidInsurance;

        return $this;
    }

    /**
     * Get f050fidInsurance
     *
     * @return integer
     */
    public function getF050fidInsurance()
    {
        return $this->f050fidInsurance;
    }

    /**
     * Set f050ftaggingStatus
     *
     * @param integer $f050ftaggingStatus
     *
     * @return T050fstudentTagging
     */
    public function setF050ftaggingStatus($f050ftaggingStatus)
    {
        $this->f050ftaggingStatus = $f050ftaggingStatus;

        return $this;
    }

    /**
     * Get f050ftaggingStatus
     *
     * @return integer
     */
    public function getF050ftaggingStatus()
    {
        return $this->f050ftaggingStatus;
    }

    /**
     * Set f050fstatus
     *
     * @param integer $f050fstatus
     *
     * @return T050fstudentTagging
     */
    public function setF050fstatus($f050fstatus)
    {
        $this->f050fstatus = $f050fstatus;

        return $this;
    }

    /**
     * Get f050fstatus
     *
     * @return integer
     */
    public function getF050fstatus()
    {
        return $this->f050fstatus;
    }

    /**
     * Set f050fcreatedBy
     *
     * @param integer $f050fcreatedBy
     *
     * @return T050fstudentTagging
     */
    public function setF050fcreatedBy($f050fcreatedBy)
    {
        $this->f050fcreatedBy = $f050fcreatedBy;

        return $this;
    }

    /**
     * Get f050fcreatedBy
     *
     * @return integer
     */
    public function getF050fcreatedBy()
    {
        return $this->f050fcreatedBy;
    }

    /**
     * Set f050fupdatedBy
     *
     * @param integer $f050fupdatedBy
     *
     * @return T050fstudentTagging
     */
    public function setF050fupdatedBy($f050fupdatedBy)
    {
        $this->f050fupdatedBy = $f050fupdatedBy;

        return $this;
    }

    /**
     * Get f050fupdatedBy
     *
     * @return integer
     */
    public function getF050fupdatedBy()
    {
        return $this->f050fupdatedBy;
    }

    /**
     * Set f050fcreatedDtTm
     *
     * @param \DateTime $f050fcreatedDtTm
     *
     * @return T050fstudentTagging
     */
    public function setF050fcreatedDtTm($f050fcreatedDtTm)
    {
        $this->f050fcreatedDtTm = $f050fcreatedDtTm;

        return $this;
    }

    /**
     * Get f050fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF050fcreatedDtTm()
    {
        return $this->f050fcreatedDtTm;
    }

    /**
     * Set f050fupdatedDtTm
     *
     * @param \DateTime $f050fupdatedDtTm
     *
     * @return T050fstudentTagging
     */
    public function setF050fupdatedDtTm($f050fupdatedDtTm)
    {
        $this->f050fupdatedDtTm = $f050fupdatedDtTm;

        return $this;
    }

    /**
     * Get f050fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF050fupdatedDtTm()
    {
        return $this->f050fupdatedDtTm;
    }
}
