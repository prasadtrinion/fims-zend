<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T014fawardNote
 *
 * @ORM\Table(name="t014faward_note")
 * @ORM\Entity(repositoryClass="Application\Repository\AwardNoteRepository")
 */
class T014fawardNote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f014fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f014fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fid_tendor", type="integer", length=20, nullable=true)
     */
    private $f014fidTendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f014ftext", type="string", length=255, nullable=true)
     */
    private $f014ftext;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014famount", type="integer", nullable=true)
     */
    private $f014famount;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="f014fstatus", type="integer", nullable=true)
     */
    private $f014fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fcreated_by", type="integer", length=50, nullable=true)
     */
    private $f014fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f014fupdated_by", type="integer", length=50, nullable=true)
     */
    private $f014fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f014fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f014fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f014fupdatedDtTm;



    /**
     * Get f014fid
     *
     * @return integer
     */
    public function getF014fid()
    {
        return $this->f014fid;
    }

    /**
     * Set f014fidTendor
     *
     * @param integer $f014fidTendor
     *
     * @return T014fawardNote
     */
    public function setF014fidTendor($f014fidTendor)
    {
        $this->f014fidTendor = $f014fidTendor;

        return $this;
    }

    /**
     * Get f014fidTendor
     *
     * @return integer
     */
    public function getF014fidTendor()
    {
        return $this->f014fidTendor;
    }

    /**
     * Set f014ftext
     *
     * @param integer $f014ftext
     *
     * @return T014fawardNote
     */
    public function setF014ftext($f014ftext)
    {
        $this->f014ftext = $f014ftext;

        return $this;
    }

    /**
     * Get f014ftext
     *
     * @return integer
     */
    public function getF014ftext()
    {
        return $this->f014ftext;
    }

    /**
     * Set f014famount
     *
     * @param integer $f014famount
     *
     * @return T014fawardNote
     */
    public function setF014famount($f014famount)
    {
        $this->f014famount = $f014famount;

        return $this;
    }

    /**
     * Get f014famount
     *
     * @return integer
     */
    public function getF014famount()
    {
        return $this->f014famount;
    }

    /**
     * Set f014fstatus
     *
     * @param integer $f014fstatus
     *
     * @return T014fawardNote
     */
    public function setF014fstatus($f014fstatus)
    {
        $this->f014fstatus = $f014fstatus;

        return $this;
    }

    /**
     * Get f014fstatus
     *
     * @return integer
     */
    public function getF014fstatus()
    {
        return $this->f014fstatus;
    }

    /**
     * Set f014fcreatedBy
     *
     * @param integer $f014fcreatedBy
     *
     * @return T014fawardNote
     */
    public function setF014fcreatedBy($f014fcreatedBy)
    {
        $this->f014fcreatedBy = $f014fcreatedBy;

        return $this;
    }

    /**
     * Get f014fcreatedBy
     *
     * @return integer
     */
    public function getF014fcreatedBy()
    {
        return $this->f014fcreatedBy;
    }

    /**
     * Set f014fupdatedBy
     *
     * @param integer $f014fupdatedBy
     *
     * @return T014fawardNote
     */
    public function setF014fupdatedBy($f014fupdatedBy)
    {
        $this->f014fupdatedBy = $f014fupdatedBy;

        return $this;
    }

    /**
     * Get f014fupdatedBy
     *
     * @return integer
     */
    public function getF014fupdatedBy()
    {
        return $this->f014fupdatedBy;
    }

    /**
     * Set f014fcreatedDtTm
     *
     * @param \DateTime $f014fcreatedDtTm
     *
     * @return T014fawardNote
     */
    public function setF014fcreatedDtTm($f014fcreatedDtTm)
    {
        $this->f014fcreatedDtTm = $f014fcreatedDtTm;

        return $this;
    }

    /**
     * Get f014fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF014fcreatedDtTm()
    {
        return $this->f014fcreatedDtTm;
    }

    /**
     * Set f014fupdatedDtTm
     *
     * @param \DateTime $f014fupdatedDtTm
     *
     * @return T014fawardNote
     */
    public function setF014fupdatedDtTm($f014fupdatedDtTm)
    {
        $this->f014fupdatedDtTm = $f014fupdatedDtTm;

        return $this;
    }

    /**
     * Get f014fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF014fupdatedDtTm()
    {
        return $this->f014fupdatedDtTm;
    }
}

























