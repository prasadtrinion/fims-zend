<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T025fdiscountDetails
 *
 * @ORM\Table(name="t025fdiscount_details")
 * @ORM\Entity(repositoryClass="Application\Repository\DiscountRepository")
 */
class T025fdiscountDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f025fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f025fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fid_item", type="integer", nullable=false)
     */
    private $f025fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fgst_value", type="integer", nullable=false)
     */
    private $f025fgstValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fquantity", type="integer", nullable=false)
     */
    private $f025fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fprice", type="integer", nullable=false)
     */
    private $f025fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025ftotal", type="integer", nullable=false)
     */
    private $f025ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fstatus", type="integer", nullable=false)
     */
    private $f025fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fcreated_by", type="integer", nullable=false)
     */
    private $f025fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fupdated_by", type="integer", nullable=false)
     */
    private $f025fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f025fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f025fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f025fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f025fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f025ftax_code", type="string", length=255, nullable=false)
     */
    private $f025ftaxCode;

         /**
     * @var string
     *
     * @ORM\Column(name="f025fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f025fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f025fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f025fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f025fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f025fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f025fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f025fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f025fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f025fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f025fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f025fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f025fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f025fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f025fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f025fcreditAccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fid_discount", type="integer", nullable=false)
     */
    private $f025fidDiscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fdiscount_type", type="integer", nullable=false)
     */
    private $f025fdiscountType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025ftax_amount", type="integer", nullable=false)
     */
    private $f025ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fdiscount_amount", type="integer", nullable=false)
     */
    private $f025fdiscountAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fdiscount_percent", type="integer", nullable=false)
     */
    private $f025fdiscountPercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fbalance_amount", type="integer", nullable=false)
     */
    private $f025fbalanceAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f025fid_invoice_detail", type="integer", nullable=false)
     */
    private $f025fidInvoiceDetail;



    /**
     * Get f025fid
     *
     * @return integer
     */
    public function getF025fid()
    {
        return $this->f025fid;
    }

    /**
     * Set f025fbalanceAmount
     *
     * @param integer $f025fbalanceAmount
     *
     * @return T025fdiscountDetails
     */
    public function setF025fbalanceAmount($f025fbalanceAmount)
    {
        $this->f025fbalanceAmount = $f025fbalanceAmount;

        return $this;
    }

    /**
     * Get f025fbalanceAmount
     *
     * @return integer
     */
    public function getF025fbalanceAmount()
    {
        return $this->f025fbalanceAmount;
    }

    /**
     * Set f025fidItem
     *
     * @param integer $f025fidItem
     *
     * @return T025fdiscountDetails
     */
    public function setF025fidItem($f025fidItem)
    {
        $this->f025fidItem = $f025fidItem;

        return $this;
    }

    /**
     * Get f025fidItem
     *
     * @return integer
     */
    public function getF025fidItem()
    {
        return $this->f025fidItem;
    }

    /**
     * Set f025fgstValue
     *
     * @param integer $f025fgstValue
     *
     * @return T025fdiscountDetails
     */
    public function setF025fgstValue($f025fgstValue)
    {
        $this->f025fgstValue = $f025fgstValue;

        return $this;
    }

    /**
     * Get f025fgstValue
     *
     * @return integer
     */
    public function getF025fgstValue()
    {
        return $this->f025fgstValue;
    }

    /**
     * Set f025fquantity
     *
     * @param integer $f025fquantity
     *
     * @return T025fdiscountDetails
     */
    public function setF025fquantity($f025fquantity)
    {
        $this->f025fquantity = $f025fquantity;

        return $this;
    }

    /**
     * Get f025fquantity
     *
     * @return integer
     */
    public function getF025fquantity()
    {
        return $this->f025fquantity;
    }

    /**
     * Set f025fprice
     *
     * @param integer $f025fprice
     *
     * @return T025fdiscountDetails
     */
    public function setF025fprice($f025fprice)
    {
        $this->f025fprice = $f025fprice;

        return $this;
    }

    /**
     * Get f025fprice
     *
     * @return integer
     */
    public function getF025fprice()
    {
        return $this->f025fprice;
    }

    /**
     * Set f025ftotal
     *
     * @param integer $f025ftotal
     *
     * @return T025fdiscountDetails
     */
    public function setF025ftotal($f025ftotal)
    {
        $this->f025ftotal = $f025ftotal;

        return $this;
    }

    /**
     * Get f025ftotal
     *
     * @return integer
     */
    public function getF025ftotal()
    {
        return $this->f025ftotal;
    }

    /**
     * Set f025fdiscountPercentage
     *
     * @param integer $f025fdiscountPercentage
     *
     * @return T025fdiscountDetails
     */
    public function setF025fdiscountPercentage($f025fdiscountPercentage)
    {
        $this->f025fdiscountPercentage = $f025fdiscountPercentage;

        return $this;
    }

    /**
     * Get f025fdiscountPercentage
     *
     * @return integer
     */
    public function getF025fdiscountPercentage()
    {
        return $this->f025fdiscountPercentage;
    }

    /**
     * Set f025fstatus
     *
     * @param integer $f025fstatus
     *
     * @return T025fdiscountDetails
     */
    public function setF025fstatus($f025fstatus)
    {
        $this->f025fstatus = $f025fstatus;

        return $this;
    }

    /**
     * Get f025fstatus
     *
     * @return integer
     */
    public function getF025fstatus()
    {
        return $this->f025fstatus;
    }

    /**
     * Set f025fcreatedBy
     *
     * @param integer $f025fcreatedBy
     *
     * @return T025fdiscountDetails
     */
    public function setF025fcreatedBy($f025fcreatedBy)
    {
        $this->f025fcreatedBy = $f025fcreatedBy;

        return $this;
    }

    /**
     * Get f025fcreatedBy
     *
     * @return integer
     */
    public function getF025fcreatedBy()
    {
        return $this->f025fcreatedBy;
    }

    /**
     * Set f025fupdatedBy
     *
     * @param integer $f025fupdatedBy
     *
     * @return T025fdiscountDetails
     */
    public function setF025fupdatedBy($f025fupdatedBy)
    {
        $this->f025fupdatedBy = $f025fupdatedBy;

        return $this;
    }

    /**
     * Get f025fupdatedBy
     *
     * @return integer
     */
    public function getF025fupdatedBy()
    {
        return $this->f025fupdatedBy;
    }

    /**
     * Set f025fcreatedDtTm
     *
     * @param \DateTime $f025fcreatedDtTm
     *
     * @return T025fdiscountDetails
     */
    public function setF025fcreatedDtTm($f025fcreatedDtTm)
    {
        $this->f025fcreatedDtTm = $f025fcreatedDtTm;

        return $this;
    }

    /**
     * Get f025fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF025fcreatedDtTm()
    {
        return $this->f025fcreatedDtTm;
    }

    /**
     * Set f025fupdatedDtTm
     *
     * @param \DateTime $f025fupdatedDtTm
     *
     * @return T025fdiscountDetails
     */
    public function setF025fupdatedDtTm($f025fupdatedDtTm)
    {
        $this->f025fupdatedDtTm = $f025fupdatedDtTm;

        return $this;
    }

    /**
     * Get f025fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF025fupdatedDtTm()
    {
        return $this->f025fupdatedDtTm;
    }

    /**
     * Set f025ftaxCode
     *
     * @param string $f025ftaxCode
     *
     * @return T025fdiscountDetails
     */
    public function setF025ftaxCode($f025ftaxCode)
    {
        $this->f025ftaxCode = $f025ftaxCode;

        return $this;
    }

    /**
     * Get f025ftaxCode
     *
     * @return string
     */
    public function getF025ftaxCode()
    {
        return $this->f025ftaxCode;
    }

     /**
     * Set f025fdebitFundCode
     *
     * @param integer $f025fdebitFundCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fdebitFundCode($f025fdebitFundCode)
    {
        $this->f025fdebitFundCode = $f025fdebitFundCode;

        return $this;
    }

    /**
     * Get f025fdebitFundCode
     *
     * @return integer
     */
    public function getF025fdebitFundCode()
    {
        return $this->f025fdebitFundCode;
    }

    /**
     * Set f025fdebitAccountCode
     *
     * @param integer $f025fdebitAccountCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fdebitAccountCode($f025fdebitAccountCode)
    {
        $this->f025fdebitAccountCode = $f025fdebitAccountCode;

        return $this;
    }

    /**
     * Get f025fdebitAccountCode
     *
     * @return integer
     */
    public function getF025fdebitAccountCode()
    {
        return $this->f025fdebitAccountCode;
    }

    /**
     * Set f025fdebitActivityCode
     *
     * @param integer $f025fdebitActivityCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fdebitActivityCode($f025fdebitActivityCode)
    {
        $this->f025fdebitActivityCode = $f025fdebitActivityCode;

        return $this;
    }

    /**
     * Get f025fdebitActivityCode
     *
     * @return integer
     */
    public function getF025fdebitActivityCode()
    {
        return $this->f025fdebitActivityCode;
    }

    /**
     * Set f025fdebitDepartmentCode
     *
     * @param integer $f025fdebitDepartmentCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fdebitDepartmentCode($f025fdebitDepartmentCode)
    {
        $this->f025fdebitDepartmentCode = $f025fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f025fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF025fdebitDepartmentCode()
    {
        return $this->f025fdebitDepartmentCode;
    }


    /**
     * Set f025fcreditFundCode
     *
     * @param integer $f025fcreditFundCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fcreditFundCode($f025fcreditFundCode)
    {
        $this->f025fcreditFundCode = $f025fcreditFundCode;

        return $this;
    }

    /**
     * Get f025fcreditFundCode
     *
     * @return integer
     */
    public function getF025fcreditFundCode()
    {
        return $this->f025fcreditFundCode;
    }

    /**
     * Set f025fcreditAccountCode
     *
     * @param integer $f025fcreditAccountCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fcreditAccountCode($f025fcreditAccountCode)
    {
        $this->f025fcreditAccountCode = $f025fcreditAccountCode;

        return $this;
    }

    /**
     * Get f025fcreditAccountCode
     *
     * @return integer
     */
    public function getF025fcreditAccountCode()
    {
        return $this->f025fcreditAccountCode;
    }

    /**
     * Set f025fcreditActivityCode
     *
     * @param integer $f025fcreditActivityCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fcreditActivityCode($f025fcreditActivityCode)
    {
        $this->f025fcreditActivityCode = $f025fcreditActivityCode;

        return $this;
    }

    /**
     * Get f025fcreditActivityCode
     *
     * @return integer
     */
    public function getF025fcreditActivityCode()
    {
        return $this->f025fcreditActivityCode;
    }

    /**
     * Set f025fcreditDepartmentCode
     *
     * @param integer $f025fcreditDepartmentCode
     *
     * @return T025fcreditNoteDetails
     */
    public function setF025fcreditDepartmentCode($f025fcreditDepartmentCode)
    {
        $this->f025fcreditDepartmentCode = $f025fcreditDepartmentCode;

        return $this;
    }

    /**
     * Set f025fidDiscount
     *
     * @param integer $f025fidDiscount
     *
     * @return T025fdiscountDetails
     */
    public function setF025fidDiscount($f025fidDiscount)
    {
        $this->f025fidDiscount = $f025fidDiscount;

        return $this;
    }

    /**
     * Get f025fidDiscount
     *
     * @return integer
     */
    public function getF025fidDiscount()
    {
        return $this->f025fidDiscount;
    }

    /**
     * Set f025ftaxAmount
     *
     * @param integer $f025ftaxAmount
     *
     * @return T025fdiscountDetails
     */
    public function setF025ftaxAmount($f025ftaxAmount)
    {
        $this->f025ftaxAmount = $f025ftaxAmount;

        return $this;
    }

    /**
     * Get f025ftaxAmount
     *
     * @return integer
     */
    public function getF025ftaxAmount()
    {
        return $this->f025ftaxAmount;
    }

    /**
     * Set f025fdiscountAmount
     *
     * @param integer $f025fdiscountAmount
     *
     * @return T025fdiscountDetails
     */
    public function setF025fdiscountAmount($f025fdiscountAmount)
    {
        $this->f025fdiscountAmount = $f025fdiscountAmount;

        return $this;
    }

    /**
     * Get f025fdiscountAmount
     *
     * @return integer
     */
    public function getF025fdiscountAmount()
    {
        return $this->f025fdiscountAmount;
    }

     /**
     * Set f025fdiscountType
     *
     * @param integer $f025fdiscountType
     *
     * @return T025fdiscountDetails
     */
    public function setF025fdiscountType($f025fdiscountType)
    {
        $this->f025fdiscountType = $f025fdiscountType;

        return $this;
    }

    /**
     * Get f025fdiscountType
     *
     * @return integer
     */
    public function getF025fdiscountType()
    {
        return $this->f025fdiscountType;
    }

     /**
     * Set f025fidInvoiceDetail
     *
     * @param integer $f025fidInvoiceDetail
     *
     * @return T025fdiscountDetails
     */
    public function setF025fidInvoiceDetail($f025fidInvoiceDetail)
    {
        $this->f025fidInvoiceDetail = $f025fidInvoiceDetail;

        return $this;
    }

    /**
     * Get f025fidInvoiceDetail
     *
     * @return integer
     */
    public function getF025fidInvoiceDetail()
    {
        return $this->f025fidInvoiceDetail;
    }

}
