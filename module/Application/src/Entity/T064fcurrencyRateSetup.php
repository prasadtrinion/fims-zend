<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064fcurrencyRateSetup
 *
 * @ORM\Table(name="t064fcurrency_rate_setup", uniqueConstraints={@ORM\UniqueConstraint(name="f064feffective_date", columns={"f064feffective_date"})})
 *@ORM\Entity(repositoryClass="Application\Repository\CurrencyRateSetupRepository")
 */
class T064fcurrencyRateSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_currency", type="integer", nullable=false)
     */
    private $f064fidCurrency;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fexchange_rate", type="integer", nullable=true)
     */
    private $f064fexchangeRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fmin_rate", type="integer", nullable=true)
     */
    private $f064fminRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fmax_rate", type="integer", nullable=true)
     */
    private $f064fmaxRate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064feffective_date", type="datetime", nullable=false)
     */
    private $f064feffectiveDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=false)
     */
    private $f064fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=true)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=true)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f064fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f064fupdateDtTm;



    /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064fidCurrency
     *
     * @param integer $f064fidCurrency
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fidCurrency($f064fidCurrency)
    {
        $this->f064fidCurrency = $f064fidCurrency;

        return $this;
    }

    /**
     * Get f064fidCurrency
     *
     * @return integer
     */
    public function getF064fidCurrency()
    {
        return $this->f064fidCurrency;
    }

    /**
     * Set f064fexchangeRate
     *
     * @param float $f064fexchangeRate
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fexchangeRate($f064fexchangeRate)
    {
        $this->f064fexchangeRate = $f064fexchangeRate;

        return $this;
    }

    /**
     * Get f064fexchangeRate
     *
     * @return float
     */
    public function getF064fexchangeRate()
    {
        return $this->f064fexchangeRate;
    }

    /**
     * Set f064fminRate
     *
     * @param float $f064fminRate
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fminRate($f064fminRate)
    {
        $this->f064fminRate = $f064fminRate;

        return $this;
    }

    /**
     * Get f064fminRate
     *
     * @return float
     */
    public function getF064fminRate()
    {
        return $this->f064fminRate;
    }

    /**
     * Set f064fmaxRate
     *
     * @param float $f064fmaxRate
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fmaxRate($f064fmaxRate)
    {
        $this->f064fmaxRate = $f064fmaxRate;

        return $this;
    }

    /**
     * Get f064fmaxRate
     *
     * @return float
     */
    public function getF064fmaxRate()
    {
        return $this->f064fmaxRate;
    }

    /**
     * Set f064feffectiveDate
     *
     * @param \DateTime $f064feffectiveDate
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064feffectiveDate($f064feffectiveDate)
    {
        $this->f064feffectiveDate = $f064feffectiveDate;

        return $this;
    }

    /**
     * Get f064feffectiveDate
     *
     * @return \DateTime
     */
    public function getF064feffectiveDate()
    {
        return $this->f064feffectiveDate;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fcreateDtTm
     *
     * @param \DateTime $f064fcreateDtTm
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fcreateDtTm($f064fcreateDtTm)
    {
        $this->f064fcreateDtTm = $f064fcreateDtTm;

        return $this;
    }

    /**
     * Get f064fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreateDtTm()
    {
        return $this->f064fcreateDtTm;
    }

    /**
     * Set f064fupdateDtTm
     *
     * @param \DateTime $f064fupdateDtTm
     *
     * @return T064fcurrencyRateSetup
     */
    public function setF064fupdateDtTm($f064fupdateDtTm)
    {
        $this->f064fupdateDtTm = $f064fupdateDtTm;

        return $this;
    }

    /**
     * Get f064fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdateDtTm()
    {
        return $this->f064fupdateDtTm;
    }
}
