<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T119fstaffCashAdvanceLimit
 *
 * @ORM\Table(name="t119fstaff_cash_advance_limit")
 * @ORM\Entity(repositoryClass="Application\Repository\StaffCashAdvanceLimitRepository")
 */
class T119fstaffCashAdvanceLimit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f119fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f119fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f119fstaff_id", type="integer", nullable=true)
     */
    private $f119fstaffId;

    /**
     * @var string
     *
     * @ORM\Column(name="f119fcash_advance", type="integer", nullable=true)
     */
    private $f119fcashAdvance;

    /**
     * @var string
     *
     * @ORM\Column(name="f119famount", type="integer", nullable=true)
     */
    private $f119famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f119fstatus", type="integer", nullable=true)
     */
    private $f119fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f119fcreated_by", type="integer", nullable=true)
     */
    private $f119fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f119fupdated_by", type="integer", nullable=true)
     */
    private $f119fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f119fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f119fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f119fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f119fupdatedDtTm;



    /**
     * Get f119fid
     *
     * @return integer
     */
    public function getF119fid()
    {
        return $this->f119fid;
    }

    /**
     * Set f119fstaffId
     *
     * @param string $f119fstaffId
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fstaffId($f119fstaffId)
    {
        $this->f119fstaffId = $f119fstaffId;

        return $this;
    }

    /**
     * Get f119fstaffId
     *
     * @return string
     */
    public function getF119fstaffId()
    {
        return $this->f119fstaffId;
    }

    /**
     * Set f119fcashAdvance
     *
     * @param string $f119fcashAdvance
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fcashAdvance($f119fcashAdvance)
    {
        $this->f119fcashAdvance = $f119fcashAdvance;

        return $this;
    }

    /**
     * Get f119fcashAdvance
     *
     * @return string
     */
    public function getF119fcashAdvance()
    {
        return $this->f119fcashAdvance;
    }

    /**
     * Set f119famount
     *
     * @param string $f119famount
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119famount($f119famount)
    {
        $this->f119famount = $f119famount;

        return $this;
    }

    /**
     * Get f119famount
     *
     * @return string
     */
    public function getF119famount()
    {
        return $this->f119famount;
    }

    /**
     * Set f119fstatus
     *
     * @param integer $f119fstatus
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fstatus($f119fstatus)
    {
        $this->f119fstatus = $f119fstatus;

        return $this;
    }

    /**
     * Get f119fstatus
     *
     * @return integer
     */
    public function getF119fstatus()
    {
        return $this->f119fstatus;
    }

    /**
     * Set f119fcreatedBy
     *
     * @param integer $f119fcreatedBy
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fcreatedBy($f119fcreatedBy)
    {
        $this->f119fcreatedBy = $f119fcreatedBy;

        return $this;
    }

    /**
     * Get f119fcreatedBy
     *
     * @return integer
     */
    public function getF119fcreatedBy()
    {
        return $this->f119fcreatedBy;
    }

    /**
     * Set f119fupdatedBy
     *
     * @param integer $f119fupdatedBy
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fupdatedBy($f119fupdatedBy)
    {
        $this->f119fupdatedBy = $f119fupdatedBy;

        return $this;
    }

    /**
     * Get f119fupdatedBy
     *
     * @return integer
     */
    public function getF119fupdatedBy()
    {
        return $this->f119fupdatedBy;
    }

    /**
     * Set f119fcreatedDtTm
     *
     * @param \DateTime $f119fcreatedDtTm
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fcreatedDtTm($f119fcreatedDtTm)
    {
        $this->f119fcreatedDtTm = $f119fcreatedDtTm;

        return $this;
    }

    /**
     * Get f119fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF119fcreatedDtTm()
    {
        return $this->f119fcreatedDtTm;
    }

    /**
     * Set f119fupdatedDtTm
     *
     * @param \DateTime $f119fupdatedDtTm
     *
     * @return T119fstaffCashAdvanceLimit
     */
    public function setF119fupdatedDtTm($f119fupdatedDtTm)
    {
        $this->f119fupdatedDtTm = $f119fupdatedDtTm;

        return $this;
    }

    /**
     * Get f119fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF119fupdatedDtTm()
    {
        return $this->f119fupdatedDtTm;
    }
}
