<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T044fregistration
 *
 * @ORM\Table(name="t044fregistration", indexes={@ORM\Index(name="f044fid_ledger", columns={"f044fid_ledger"}), @ORM\Index(name="f044fupdated_by", columns={"f044fupdated_by"}), @ORM\Index(name="f044fcreated_by", columns={"f044fcreated_by"})})
 * @ORM\Entity
 */
class T044fregistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f044fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fsupplier_type", type="string", length=50, nullable=false)
     */
    private $f044fsupplierType;

    /**
     * @var string
     *
     * @ORM\Column(name="f044ftax_deduction_number", type="string", length=50, nullable=false)
     */
    private $f044ftaxDeductionNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fgst_number", type="string", length=50, nullable=false)
     */
    private $f044fgstNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fnric_number", type="string", length=50, nullable=false)
     */
    private $f044fnricNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f044fcin", type="string", length=50, nullable=false)
     */
    private $f044fcin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f044fstatus", type="boolean", nullable=false)
     */
    private $f044fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f044fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f044fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f044fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fid_ledger", type="integer", length=50, nullable=false)
     */
    private $f044fidLedger;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f044fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f044fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f044fcreatedBy;



    /**
     * Get f044fid
     *
     * @return integer
     */
    public function getF044fid()
    {
        return $this->f044fid;
    }

    /**
     * Set f044fsupplierType
     *
     * @param string $f044fsupplierType
     *
     * @return T044fregistration
     */
    public function setF044fsupplierType($f044fsupplierType)
    {
        $this->f044fsupplierType = $f044fsupplierType;

        return $this;
    }

    /**
     * Get f044fsupplierType
     *
     * @return string
     */
    public function getF044fsupplierType()
    {
        return $this->f044fsupplierType;
    }

    /**
     * Set f044ftaxDeductionNumber
     *
     * @param string $f044ftaxDeductionNumber
     *
     * @return T044fregistration
     */
    public function setF044ftaxDeductionNumber($f044ftaxDeductionNumber)
    {
        $this->f044ftaxDeductionNumber = $f044ftaxDeductionNumber;

        return $this;
    }

    /**
     * Get f044ftaxDeductionNumber
     *
     * @return string
     */
    public function getF044ftaxDeductionNumber()
    {
        return $this->f044ftaxDeductionNumber;
    }

    /**
     * Set f044fgstNumber
     *
     * @param string $f044fgstNumber
     *
     * @return T044fregistration
     */
    public function setF044fgstNumber($f044fgstNumber)
    {
        $this->f044fgstNumber = $f044fgstNumber;

        return $this;
    }

    /**
     * Get f044fgstNumber
     *
     * @return string
     */
    public function getF044fgstNumber()
    {
        return $this->f044fgstNumber;
    }

    /**
     * Set f044fnricNumber
     *
     * @param string $f044fnricNumber
     *
     * @return T044fregistration
     */
    public function setF044fnricNumber($f044fnricNumber)
    {
        $this->f044fnricNumber = $f044fnricNumber;

        return $this;
    }

    /**
     * Get f044fnricNumber
     *
     * @return string
     */
    public function getF044fnricNumber()
    {
        return $this->f044fnricNumber;
    }

    /**
     * Set f044fcin
     *
     * @param string $f044fcin
     *
     * @return T044fregistration
     */
    public function setF044fcin($f044fcin)
    {
        $this->f044fcin = $f044fcin;

        return $this;
    }

    /**
     * Get f044fcin
     *
     * @return string
     */
    public function getF044fcin()
    {
        return $this->f044fcin;
    }

    /**
     * Set f044fstatus
     *
     * @param boolean $f044fstatus
     *
     * @return T044fregistration
     */
    public function setF044fstatus($f044fstatus)
    {
        $this->f044fstatus = $f044fstatus;

        return $this;
    }

    /**
     * Get f044fstatus
     *
     * @return boolean
     */
    public function getF044fstatus()
    {
        return $this->f044fstatus;
    }

    /**
     * Set f044fcreatedDtTm
     *
     * @param \DateTime $f044fcreatedDtTm
     *
     * @return T044fregistration
     */
    public function setF044fcreatedDtTm($f044fcreatedDtTm)
    {
        $this->f044fcreatedDtTm = $f044fcreatedDtTm;

        return $this;
    }

    /**
     * Get f044fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fcreatedDtTm()
    {
        return $this->f044fcreatedDtTm;
    }

    /**
     * Set f044fupdatedDtTm
     *
     * @param \DateTime $f044fupdatedDtTm
     *
     * @return T044fregistration
     */
    public function setF044fupdatedDtTm($f044fupdatedDtTm)
    {
        $this->f044fupdatedDtTm = $f044fupdatedDtTm;

        return $this;
    }

    /**
     * Get f044fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF044fupdatedDtTm()
    {
        return $this->f044fupdatedDtTm;
    }

    /**
     * Set f044fidLedger
     *
     * @param $f044fidLedger
     *
     * @return T044fregistration
     */
    public function setF044fidLedger($f044fidLedger)
    {
        $this->f044fidLedger = $f044fidLedger;

        return $this;
    }

    /**
     * Get f044fidLedger
     *
     * @return T044fregistration
     */
    public function getF044fidLedger()
    {
        return $this->f044fidLedger;
    }

    /**
     * Set f044fupdatedBy
     *
     * @param integer $f044fupdatedBy
     *
     * @return T044fregistration
     */
    public function setF044fupdatedBy($f044fupdatedBy )
    {
        $this->f044fupdatedBy = $f044fupdatedBy;

        return $this;
    }

    /**
     * Get f044fupdatedBy
     *
     * @return integer
     */
    public function getF044fupdatedBy()
    {
        return $this->f044fupdatedBy;
    }

    /**
     * Set f044fcreatedBy
     *
     * @param integer $f044fcreatedBy
     *
     * @return T044fregistration
     */
    public function setF044fcreatedBy( $f044fcreatedBy )
    {
        $this->f044fcreatedBy = $f044fcreatedBy;

        return $this;
    }

    /**
     * Get f044fcreatedBy
     *
     * @return integer
     */
    public function getF044fcreatedBy()
    {
        return $this->f044fcreatedBy;
    }
}
