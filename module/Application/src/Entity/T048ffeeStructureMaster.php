<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T048ffeeStructureMaster
 *
 * @ORM\Table(name="t048ffee_structure_master")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeStructureRepository")
 */
class T048ffeeStructureMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f048fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fname", type="string", length=50, nullable=true)
     */
    private $f048fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fcode", type="string", length=25, nullable=true)
     */
    private $f048fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fgraduation_type", type="string", length=10, nullable=true)
     */
    private $f048fgraduationType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048feffective_date", type="datetime", nullable=true)
     */
    private $f048feffectiveDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_intake", type="string",length=20, nullable=true)
     */
    private $f048fidIntake;

    /**
     * @var string
     *
     * @ORM\Column(name="f048flearning_centre", type="string", length=50, nullable=true)
     */
    private $f048flearningCentre;

    /**
     * @var string
     *
     * @ORM\Column(name="f048fpartner", type="string", length=50, nullable=true)
     */
    private $f048fpartner;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcitizen", type="string",length=20, nullable=true)
     */
    private $f048fcitizen;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fregion", type="string",length=20, nullable=true)
     */
    private $f048fregion;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstudent_category", type="integer", nullable=true)
     */
    private $f048fstudentCategory;


    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstudy_mode", type="integer", nullable=true)
     */
    private $f048fstudyMode;

    /**
     * @var string
     *
     * @ORM\Column(name="f048ftotal", type="integer", nullable=true)
     */
    private $f048ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstatus", type="integer", nullable=true)
     */
    private $f048fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fis_program_attached", type="integer", nullable=true)
     */
    private $f048fisProgramAttached;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcreated_by", type="integer", nullable=true)
     */
    private $f048fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fupdated_by", type="integer", nullable=true)
     */
    private $f048fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fis_program", type="integer", nullable=true)
     */
    private $f048fisProgram;

     /**
     * @var integer
     *
     * @ORM\Column(name="f048ffee_type", type="integer", nullable=true)
     */
    private $f048ffeeType;



    /**
     * Get f048fid
     *
     * @return integer
     */
    public function getF048fid()
    {
        return $this->f048fid;
    }

    /**
     * Set f048fname
     *
     * @param string $f048fname
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fname($f048fname)
    {
        $this->f048fname = $f048fname;

        return $this;
    }

    /**
     * Get f048fname
     *
     * @return string
     */
    public function getF048fname()
    {
        return $this->f048fname;
    }

     /**
     * Set f048ffeeType
     *
     * @param string $f048ffeeType
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048ffeeType($f048ffeeType)
    {
        $this->f048ffeeType = $f048ffeeType;

        return $this;
    }

    /**
     * Get f048ffeeType
     *
     * @return string
     */
    public function getF048ffeeType()
    {
        return $this->f048ffeeType;
    }

    /**
     * Set f048fcode
     *
     * @param string $f048fcode
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fcode($f048fcode)
    {
        $this->f048fcode = $f048fcode;

        return $this;
    }

    /**
     * Get f048fcode
     *
     * @return string
     */
    public function getF048fcode()
    {
        return $this->f048fcode;
    }

    /**
     * Set f048fgraduationType
     *
     * @param string $f048fgraduationType
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fgraduationType($f048fgraduationType)
    {
        $this->f048fgraduationType = $f048fgraduationType;

        return $this;
    }

    /**
     * Get f048fgraduationType
     *
     * @return string
     */
    public function getF048fgraduationType()
    {
        return $this->f048fgraduationType;
    }

    /**
     * Set f048feffectiveDate
     *
     * @param \DateTime $f048feffectiveDate
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048feffectiveDate($f048feffectiveDate)
    {
        $this->f048feffectiveDate = $f048feffectiveDate;

        return $this;
    }

    /**
     * Get f048feffectiveDate
     *
     * @return \DateTime
     */
    public function getF048feffectiveDate()
    {
        return $this->f048feffectiveDate;
    }

    /**
     * Set f048fidIntake
     *
     * @param integer $f048fidIntake
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fidIntake($f048fidIntake)
    {
        $this->f048fidIntake = $f048fidIntake;

        return $this;
    }

    /**
     * Get f048fidIntake
     *
     * @return integer
     */
    public function getF048fidIntake()
    {
        return $this->f048fidIntake;
    }

    /**
     * Set f048flearningCentre
     *
     * @param string $f048flearningCentre
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048flearningCentre($f048flearningCentre)
    {
        $this->f048flearningCentre = $f048flearningCentre;

        return $this;
    }

    /**
     * Get f048flearningCentre
     *
     * @return string
     */
    public function getF048flearningCentre()
    {
        return $this->f048flearningCentre;
    }

    /**
     * Set f048fpartner
     *
     * @param string $f048fpartner
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fpartner($f048fpartner)
    {
        $this->f048fpartner = $f048fpartner;

        return $this;
    }

    /**
     * Get f048fpartner
     *
     * @return string
     */
    public function getF048fpartner()
    {
        return $this->f048fpartner;
    }

    /**
     * Set f048fcitizen
     *
     * @param integer $f048fcitizen
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fcitizen($f048fcitizen)
    {
        $this->f048fcitizen = $f048fcitizen;

        return $this;
    }

    /**
     * Get f048fcitizen
     *
     * @return integer
     */
    public function getF048fcitizen()
    {
        return $this->f048fcitizen;
    }

    /**
     * Set f048fregion
     *
     * @param integer $f048fregion
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fregion($f048fregion)
    {
        $this->f048fregion = $f048fregion;

        return $this;
    }

    /**
     * Get f048fregion
     *
     * @return integer
     */
    public function getF048fregion()
    {
        return $this->f048fregion;
    }

     /**
     * Set f048fisProgramAttached
     *
     * @param integer $f048fisProgramAttached
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fisProgramAttached($f048fisProgramAttached)
    {
        $this->f048fisProgramAttached = $f048fisProgramAttached;

        return $this;
    }

    /**
     * Get f048fisProgramAttached
     *
     * @return integer
     */
    public function getF048fisProgramAttached()
    {
        return $this->f048fisProgramAttached;
    }

    /**
     * Set f048fstudentCategory
     *
     * @param integer $f048fstudentCategory
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fstudentCategory($f048fstudentCategory)
    {
        $this->f048fstudentCategory = $f048fstudentCategory;

        return $this;
    }

    /**
     * Get f048fstudentCategory
     *
     * @return integer
     */
    public function getF048fstudentCategory()
    {
        return $this->f048fstudentCategory;
    }


    /**
     * Set f048fstudyMode
     *
     * @param integer $f048fstudyMode
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fstudyMode($f048fstudyMode)
    {
        $this->f048fstudyMode = $f048fstudyMode;

        return $this;
    }

    /**
     * Get f048fstudyMode
     *
     * @return integer
     */
    public function getF048fstudyMode()
    {
        return $this->f048fstudyMode;
    }



    /**
     * Set f048ftotal
     *
     * @param string $f048ftotal
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048ftotal($f048ftotal)
    {
        $this->f048ftotal = $f048ftotal;

        return $this;
    }

    /**
     * Get f048ftotal
     *
     * @return string
     */
    public function getF048ftotal()
    {
        return $this->f048ftotal;
    }

    /**
     * Set f048fstatus
     *
     * @param integer $f048fstatus
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fstatus($f048fstatus)
    {
        $this->f048fstatus = $f048fstatus;

        return $this;
    }

    /**
     * Get f048fstatus
     *
     * @return integer
     */
    public function getF048fstatus()
    {
        return $this->f048fstatus;
    }

    /**
     * Set f048fcreatedBy
     *
     * @param integer $f048fcreatedBy
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fcreatedBy($f048fcreatedBy)
    {
        $this->f048fcreatedBy = $f048fcreatedBy;

        return $this;
    }

    /**
     * Get f048fcreatedBy
     *
     * @return integer
     */
    public function getF048fcreatedBy()
    {
        return $this->f048fcreatedBy;
    }

    /**
     * Set f048fupdatedBy
     *
     * @param integer $f048fupdatedBy
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fupdatedBy($f048fupdatedBy)
    {
        $this->f048fupdatedBy = $f048fupdatedBy;

        return $this;
    }

    /**
     * Get f048fupdatedBy
     *
     * @return integer
     */
    public function getF048fupdatedBy()
    {
        return $this->f048fupdatedBy;
    }

    /**
     * Set f048fcreatedDtTm
     *
     * @param \DateTime $f048fcreatedDtTm
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fcreatedDtTm($f048fcreatedDtTm)
    {
        $this->f048fcreatedDtTm = $f048fcreatedDtTm;

        return $this;
    }

    /**
     * Get f048fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fcreatedDtTm()
    {
        return $this->f048fcreatedDtTm;
    }

    /**
     * Set f048fupdatedDtTm
     *
     * @param \DateTime $f048fupdatedDtTm
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fupdatedDtTm($f048fupdatedDtTm)
    {
        $this->f048fupdatedDtTm = $f048fupdatedDtTm;

        return $this;
    }

    /**
     * Get f048fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fupdatedDtTm()
    {
        return $this->f048fupdatedDtTm;
    }

    /**
     * Set f048fisProgram
     *
     * @param integer $f048fisProgram
     *
     * @return T048ffeeStructureMaster
     */
    public function setF048fisProgram($f048fisProgram)
    {
        $this->f048fisProgram = $f048fisProgram;

        return $this;
    }

    /**
     * Get f048fisProgram
     *
     * @return integer
     */
    public function getF048fisProgram()
    {
        return $this->f048fisProgram;
    }
}
