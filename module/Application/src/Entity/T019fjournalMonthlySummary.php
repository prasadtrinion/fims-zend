<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T019fjournalMonthlySummary
 *
 * @ORM\Table(name="t018fmonthly_summary")
 * @ORM\Entity(repositoryClass="Application\Repository\JournalMonthlySummaryRepository")
 */
class T019fjournalMonthlySummary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f018fid;

       /**
     * @var string
     *
     * @ORM\Column(name="f018ffund_code", type="string",length=20, nullable=false)
     */
    private $f018ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f018fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f018fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f018factivity_code", type="string",length=20, nullable=false)
     */
    private $f018factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f018faccount_code", type="string",length=20, nullable=false)
     */
    private $f018faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fmonth", type="string",length=60, nullable=true)
     */
    private $f018fmonth;


    /**
     * @var integer
     *
     * @ORM\Column(name="f018fdr_amount", type="integer", nullable=false)
     */
    private $f018fdrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcr_amount", type="integer", nullable=false)
     */
    private $f018fcrAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fstatus", type="integer", nullable=false)
     */
    private $f018fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fcreated_by", type="integer", nullable=false)
     */
    private $f018fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f018fupdated_by", type="integer", nullable=false)
     */
    private $f018fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fupdateDtTm;

    /**
     * Get f018fid
     *
     * @return integer
     */
    public function getF018fid()
    {
        return $this->f018fid;
    }


    /**
     * Set f018ffundCode
     *
     * @param integer $f018ffundCode
     *
     * @return T018fjournalDetails
     */
    public function setF018ffundCode($f018ffundCode)
    {
        $this->f018ffundCode = $f018ffundCode;

        return $this;
    }

    /**
     * Get f018ffundCode
     *
     * @return integer
     */
    public function getF018ffundCode()
    {
        return $this->f018ffundCode;
    }

    /**
     * Set f018faccountCode
     *
     * @param integer $f018faccountCode
     *
     * @return T018fjournalDetails
     */
    public function setF018faccountCode($f018faccountCode)
    {
        $this->f018faccountCode = $f018faccountCode;

        return $this;
    }

    /**
     * Get f018faccountCode
     *
     * @return integer
     */
    public function getF018faccountCode()
    {
        return $this->f018faccountCode;
    }

    /**
     * Set f018factivityCode
     *
     * @param integer $f018factivityCode
     *
     * @return T018fjournalDetails
     */
    public function setF018factivityCode($f018factivityCode)
    {
        $this->f018factivityCode = $f018factivityCode;

        return $this;
    }

    /**
     * Get f018factivityCode
     *
     * @return integer
     */
    public function getF018factivityCode()
    {
        return $this->f018factivityCode;
    }

    /**
     * Set f018fdepartmentCode
     *
     * @param integer $f018fdepartmentCode
     *
     * @return T018fjournalDetails
     */
    public function setF018fdepartmentCode($f018fdepartmentCode)
    {
        $this->f018fdepartmentCode = $f018fdepartmentCode;

        return $this;
    }

    /**
     * Get f018fdepartmentCode
     *
     * @return integer
     */
    public function getF018fdepartmentCode()
    {
        return $this->f018fdepartmentCode;
    }

    /**
     * Set f018fsoCode
     *
     * @param string $f018fsoCode
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fsoCode($f018fsoCode)
    {
        $this->f018fsoCode = $f018fsoCode;

        return $this;
    }
    
    /**
     * Get f018fsoCode
     *
     * @return string
     */
    public function getF018fsoCode()
    {
        return $this->f018fsoCode;
    }

    /**
     * Set f018fmonth
     *
     * @param string $f018fmonth
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fmonth($f018fmonth)
    {
        $this->f018fmonth = $f018fmonth;

        return $this;
    }
    
    /**
     * Get f018fmonth
     *
     * @return string
     */
    public function getF018fmonth()
    {
        return $this->f018fmonth;
    }

    /**
     * Get f018freferenceNumber
     *
     * @return string
     */
    public function getF018freferenceNumber()
    {
        return $this->f018freferenceNumber;
    }

        /**
     * Set f018freferenceNumber
     *
     * @param string $f018freferenceNumber
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018freferenceNumber($f018freferenceNumber)
    {
        $this->f018freferenceNumber = $f018freferenceNumber;

        return $this;
    }

    


    /**
     * Set f018fdrAmount
     *
     * @param integer $f018fdrAmount
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fdrAmount($f018fdrAmount)
    {
        $this->f018fdrAmount = $f018fdrAmount;

        return $this;
    }

    /**
     * Get f018fdrAmount
     *
     * @return integer
     */
    public function getF018fdrAmount()
    {
        return $this->f018fdrAmount;
    }

    /**
     * Set f018fcrAmount
     *
     * @param integer $f018fcrAmount
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fcrAmount($f018fcrAmount)
    {
        $this->f018fcrAmount = $f018fcrAmount;

        return $this;
    }

    /**
     * Get f018fcrAmount
     *
     * @return integer
     */
    public function getF018fcrAmount()
    {
        return $this->f018fcrAmount;
    }

    /**
     * Set f018fstatus
     *
     * @param integer $f018fstatus
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fstatus($f018fstatus)
    {
        $this->f018fstatus = $f018fstatus;

        return $this;
    }

    /**
     * Get f018fstatus
     *
     * @return integer
     */
    public function getF018fstatus()
    {
        return $this->f018fstatus;
    }

    /**
     * Set f018fcreatedBy
     *
     * @param integer $f018fcreatedBy
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fcreatedBy($f018fcreatedBy)
    {
        $this->f018fcreatedBy = $f018fcreatedBy;

        return $this;
    }

    /**
     * Get f018fcreatedBy
     *
     * @return integer
     */
    public function getF018fcreatedBy()
    {
        return $this->f018fcreatedBy;
    }

    /**
     * Set f018fupdatedBy
     *
     * @param integer $f018fupdatedBy
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fupdatedBy($f018fupdatedBy)
    {
        $this->f018fupdatedBy = $f018fupdatedBy;

        return $this;
    }

    /**
     * Get f018fupdatedBy
     *
     * @return integer
     */
    public function getF018fupdatedBy()
    {
        return $this->f018fupdatedBy;
    }

    /**
     * Set f018fcreateDtTm
     *
     * @param \DateTime $f018fcreateDtTm
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fcreateDtTm($f018fcreateDtTm)
    {
        $this->f018fcreateDtTm = $f018fcreateDtTm;

        return $this;
    }

    /**
     * Get f018fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF018fcreateDtTm()
    {
        return $this->f018fcreateDtTm;
    }

    /**
     * Set f018fupdateDtTm
     *
     * @param \DateTime $f018fupdateDtTm
     *
     * @return T018fjournalMonthlySummary
     */
    public function setF018fupdateDtTm($f018fupdateDtTm)
    {
        $this->f018fupdateDtTm = $f018fupdateDtTm;

        return $this;
    }

    /**
     * Get f018fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF018fupdateDtTm()
    {
        return $this->f018fupdateDtTm;
    }

}