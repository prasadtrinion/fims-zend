<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fdebit_note
 *
 * @ORM\Table(name="t063fsponsor_dn", indexes={@ORM\Index(name="f063fapproved_by", columns={"f063fapproved_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorDnRepository")
 */
class T063fsponsorDn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_invoice", type="integer", nullable=true)
     */
    private $f063fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_customer", type="integer", nullable=true)
     */
    private $f063fidCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fdescription", type="string", length=100, nullable=true)
     */
    private $f063fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063ftotal_amount", type="integer", nullable=true)
     */
    private $f063ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fbalance", type="integer", nullable=true)
     */
    private $f063fbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f063freference_number", type="string", length=50, nullable=true)
     */
    private $f063freferenceNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fdate", type="datetime", nullable=true)
     */
    private $f063fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063ftype", type="string", length=10, nullable=true)
     */
    private $f063ftype;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fvoucher_type", type="string", length=10, nullable=true)
     */
    private $f063fvoucherType;

    /**
     * @var string
     *
     * @ORM\Column(name="f063freason", type="string", length=60, nullable=true)
     */
    private $f063freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fvoucher_number", type="string", length=50, nullable=true)
     */
    private $f063fvoucherNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="f063fnarration", type="string", length=50, nullable=true)
     */
    private $f063fnarration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fvoucher_days", type="integer", nullable=true)
     */
    private $f063fvoucherDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=true)
     */
    private $f063fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=false)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fapproved_by", type="integer", nullable=true)
     */
    private $f063fapprovedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063created_dt_tm", type="datetime", nullable=false)
     */
    private $f063createdDtTm;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063updated_dt_tm", type="datetime", nullable=false)
     */
    private $f063updatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=true)
     */
    private $f063fstatus;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_financial_year", type="integer", nullable=true)
     */
    private $f063fidFinancialYear;


    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063fidInvoice
     *
     * @param integer $f063fidInvoice
     *
     * @return T063fsponsorDn
     */
    public function setF063fidInvoice($f063fidInvoice)
    {
        $this->f063fidInvoice = $f063fidInvoice;

        return $this;
    }

    /**
     * Get f063fidInvoice
     *
     * @return integer
     */
    public function getF063fidInvoice()
    {
        return $this->f063fidInvoice;
    }
    
  /**
     * Set f063fidCustomer
     *
     * @param integer $f063fidCustomer
     *
     * @return T063fsponsorDn
     */
    public function setF063fidCustomer($f063fidCustomer)
    {
        $this->f063fidCustomer = $f063fidCustomer;

        return $this;
    }

    /**
     * Get f063fidCustomer
     *
     * @return integer
     */
    public function getF063fidCustomer()
    {
        return $this->f063fidCustomer;
    }
    /**
     * Set f063ftotalAmount
     *
     * @param integer $f063ftotalAmount
     *
     * @return T063fsponsorDn
     */
    public function setF063ftotalAmount($f063ftotalAmount)
    {
        $this->f063ftotalAmount = $f063ftotalAmount;

        return $this;
    }

    /**
     * Get f063ftotalAmount
     *
     * @return integer
     */
    public function getF063ftotalAmount()
    {
        return $this->f063ftotalAmount;
    }

    /**
     * Set f063fbalance
     *
     * @param integer $f063fbalance
     *
     * @return T063fsponsorDn
     */
    public function setF063fbalance($f063fbalance)
    {
        $this->f063fbalance = $f063fbalance;

        return $this;
    }

    /**
     * Get f063fbalance
     *
     * @return integer
     */
    public function getF063fbalance()
    {
        return $this->f063fbalance;
    }

    /**
     * Set f063freferenceNumber
     *
     * @param string $f063freferenceNumber
     *
     * @return T063fsponsorDn
     */
    public function setF063freferenceNumber($f063freferenceNumber)
    {
        $this->f063freferenceNumber = $f063freferenceNumber;

        return $this;
    }

    /**
     * Get f063freferenceNumber
     *
     * @return string
     */
    public function getF063freferenceNumber()
    {
        return $this->f063freferenceNumber;
    }

    /**
     * Set f063fdate
     *
     * @param \DateTime $f063fdate
     *
     * @return T063fsponsorDn
     */
    public function setF063fdate($f063fdate)
    {
        $this->f063fdate = $f063fdate;

        return $this;
    }

    /**
     * Get f063fdate
     *
     * @return \DateTime
     */
    public function getF063fdate()
    {
        return $this->f063fdate;
    }

     /**
     * Set f063ftype
     *
     * @param integer $f063ftype
     *
     * @return T063fsponsorDn
     */
    public function setF063ftype($f063ftype)
    {
        $this->f063ftype = $f063ftype;

        return $this;
    }

    /**
     * Get f063ftype
     *
     * @return integer
     */
    public function getF063ftype()
    {
        return $this->f063ftype;
    }

    /**
     * Set f063fvoucherType
     *
     * @param string $f063fvoucherType
     *
     * @return T063fsponsorDn
     */
    public function setF063fvoucherType($f063fvoucherType)
    {
        $this->f063fvoucherType = $f063fvoucherType;

        return $this;
    }

    /**
     * Get f063fvoucherType
     *
     * @return string
     */
    public function getF063fvoucherType()
    {
        return $this->f063fvoucherType;
    }

    /**
     * Set f063fvoucherNumber
     *
     * @param string $f063fvoucherNumber
     *
     * @return T063fsponsorDn
     */
    public function setF063fvoucherNumber($f063fvoucherNumber)
    {
        $this->f063fvoucherNumber = $f063fvoucherNumber;

        return $this;
    }

    /**
     * Get f063fvoucherNumber
     *
     * @return string
     */
    public function getF063fvoucherNumber()
    {
        return $this->f063fvoucherNumber;
    }
    /**
     * Get f063fnarration
     *
     * @return string
     */
    public function getF063fnarration()
    {
        return $this->f063fnarration;
    }


    /**
     * Set f063fnarration
     *
     * @param string $f063fnarration
     *
     * @return T063fsponsorDn
     */
    public function setF063fnarration($f063fnarration)
    {
        $this->f063fnarration = $f063fnarration;

        return $this;
    }


    /**
     * Set f063fvoucherDays
     *
     * @param integer $f063fvoucherDays
     *
     * @return T063fvoucher
     */
    public function setF063fvoucherDays($f063fvoucherDays)
    {
        $this->f063fvoucherDays = $f063fvoucherDays;

        return $this;
    }

    /**
     * Get f063fvoucherDays
     *
     * @return integer
     */
    public function getF063fvoucherDays()
    {
        return $this->f063fvoucherDays;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fsponsorDn
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fsponsorDn
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fapprovedBy
     *
     * @param integer $f063fapprovedBy
     *
     * @return T063fsponsorDn
     */
    public function setF063fapprovedBy($f063fapprovedBy)
    {
        $this->f063fapprovedBy = $f063fapprovedBy;

        return $this;
    }

    /**
     * Get f063fapprovedBy
     *
     * @return integer
     */
    public function getF063fapprovedBy()
    {
        return $this->f063fapprovedBy;
    }

    /**
     * Set f063createdDtTm
     *
     * @param \DateTime $f063createdDtTm
     *
     * @return T063fsponsorDn
     */
    public function setF063createdDtTm($f063createdDtTm)
    {
        $this->f063createdDtTm = $f063createdDtTm;

        return $this;
    }

    /**
     * Get f063createdDtTm
     *
     * @return \DateTime
     */
    public function getF063createdDtTm()
    {
        return $this->f063createdDtTm;
    }


    /**
     * Set f063updatedDtTm
     *
     * @param \DateTime $f063updatedDtTm
     *
     * @return T063fsponsorDn
     */
    public function setF063updatedDtTm($f063updatedDtTm)
    {
        $this->f063updatedDtTm = $f063updatedDtTm;

        return $this;
    }

    /**
     * Get f063updatedDtTm
     *
     * @return \DateTime
     */
    public function getF063updatedDtTm()
    {
        return $this->f063updatedDtTm;
    }
    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fsponsorDn
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

      /**
     * Set f063fidFinancialYear
     *
     * @param integer $f063fidFinancialYear
     *
     * @return T063fsponsorDn
     */
    public function setF063fidFinancialYear($f063fidFinancialYear)
    {
        $this->f063fidFinancialYear = $f063fidFinancialYear;

        return $this;
    }

    /**
     * Get f063fidFinancialYear
     *
     * @return integer
     */
    public function getF063fidFinancialYear()
    {
        return $this->f063fidFinancialYear;
    }

    /**
     * Set f063fdescription
     *
     * @param string $f063fdescription
     *
     * @return T063fsponsorDn
     */
    public function setF063fdescription($f063fdescription)
    {
        $this->f063fdescription = $f063fdescription;

        return $this;
    }

    /**
     * Get f063fdescription
     *
     * @return string
     */
    public function getF063fdescription()
    {
        return $this->f063fdescription;
    }

    /**
     * Set f063freason
     *
     * @param string $f063freason
     *
     * @return T063fsponsorDn
     */
    public function setF063freason($f063freason)
    {
        $this->f063freason = $f063freason;

        return $this;
    }

    /**
     * Get f063freason
     *
     * @return string
     */
    public function getF063freason()
    {
        return $this->f063freason;
    }
}