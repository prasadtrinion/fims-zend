<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T086fsetupDisposal
 *
 * @ORM\Table(name="t086fsetup_disposal", uniqueConstraints={@ORM\UniqueConstraint(name="f086fdisposal_code_UNIQUE", columns={"f086fdisposal_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SetupDisposalRepository")
 */
class T086fsetupDisposal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f086fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdisposal_code", type="string", length=50, nullable=true)
     */
    private $f086fdisposalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdisposal_type", type="string", length=100, nullable=true)
     */
    private $f086fdisposalType;

    /**
     * @var string
     *
     * @ORM\Column(name="f086faccount_code", type="string", length=100, nullable=true)
     */
    private $f086faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fstatus", type="integer", nullable=true)
     */
    private $f086fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fcreated_by", type="integer", nullable=true)
     */
    private $f086fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fupdated_by", type="integer", nullable=true)
     */
    private $f086fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fupdatedDtTm;



    /**
     * Get f086fid
     *
     * @return integer
     */
    public function getF086fid()
    {
        return $this->f086fid;
    }

    /**
     * Set f086fdisposalCode
     *
     * @param string $f086fdisposalCode
     *
     * @return T086fsetupDisposal
     */
    public function setF086fdisposalCode($f086fdisposalCode)
    {
        $this->f086fdisposalCode = $f086fdisposalCode;

        return $this;
    }

    /**
     * Get f086fdisposalCode
     *
     * @return string
     */
    public function getF086fdisposalCode()
    {
        return $this->f086fdisposalCode;
    }

    /**
     * Set f086fdisposalType
     *
     * @param string $f086fdisposalType
     *
     * @return T086fsetupDisposal
     */
    public function setF086fdisposalType($f086fdisposalType)
    {
        $this->f086fdisposalType = $f086fdisposalType;

        return $this;
    }

    /**
     * Get f086fdisposalType
     *
     * @return string
     */
    public function getF086fdisposalType()
    {
        return $this->f086fdisposalType;
    }

    /**
     * Set f086faccountCode
     *
     * @param string $f086faccountCode
     *
     * @return T086fsetupDisposal
     */
    public function setF086faccountCode($f086faccountCode)
    {
        $this->f086faccountCode = $f086faccountCode;

        return $this;
    }

    /**
     * Get f086faccountCode
     *
     * @return string
     */
    public function getF086faccountCode()
    {
        return $this->f086faccountCode;
    }

    /**
     * Set f086fstatus
     *
     * @param integer $f086fstatus
     *
     * @return T086fsetupDisposal
     */
    public function setF086fstatus($f086fstatus)
    {
        $this->f086fstatus = $f086fstatus;

        return $this;
    }

    /**
     * Get f086fstatus
     *
     * @return integer
     */
    public function getF086fstatus()
    {
        return $this->f086fstatus;
    }

    /**
     * Set f086fcreatedBy
     *
     * @param integer $f086fcreatedBy
     *
     * @return T086fsetupDisposal
     */
    public function setF086fcreatedBy($f086fcreatedBy)
    {
        $this->f086fcreatedBy = $f086fcreatedBy;

        return $this;
    }

    /**
     * Get f086fcreatedBy
     *
     * @return integer
     */
    public function getF086fcreatedBy()
    {
        return $this->f086fcreatedBy;
    }

    /**
     * Set f086fupdatedBy
     *
     * @param integer $f086fupdatedBy
     *
     * @return T086fsetupDisposal
     */
    public function setF086fupdatedBy($f086fupdatedBy)
    {
        $this->f086fupdatedBy = $f086fupdatedBy;

        return $this;
    }

    /**
     * Get f086fupdatedBy
     *
     * @return integer
     */
    public function getF086fupdatedBy()
    {
        return $this->f086fupdatedBy;
    }

    /**
     * Set f086fcreatedDtTm
     *
     * @param \DateTime $f086fcreatedDtTm
     *
     * @return T086fsetupDisposal
     */
    public function setF086fcreatedDtTm($f086fcreatedDtTm)
    {
        $this->f086fcreatedDtTm = $f086fcreatedDtTm;

        return $this;
    }

    /**
     * Get f086fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fcreatedDtTm()
    {
        return $this->f086fcreatedDtTm;
    }

    /**
     * Set f086fupdatedDtTm
     *
     * @param \DateTime $f086fupdatedDtTm
     *
     * @return T086fsetupDisposal
     */
    public function setF086fupdatedDtTm($f086fupdatedDtTm)
    {
        $this->f086fupdatedDtTm = $f086fupdatedDtTm;

        return $this;
    }

    /**
     * Get f086fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fupdatedDtTm()
    {
        return $this->f086fupdatedDtTm;
    }
}
