<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T097fcashBankDetails
 *
 * @ORM\Table(name="t097fcash_bank_details")
 * @ORM\Entity(repositoryClass="Application\Repository\CashBankRepository")
 */
class T097fcashBankDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f097fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f097fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fid_cash_bank", type="integer", nullable=true)
     */
    private $f097fidCashBank;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f097fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f097fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f097fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f097fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f097fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f097fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f097fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f097fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f097fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f097fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f097fcreditAccountCode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f097fcredit_amount", type="integer",  nullable=true)
     */
    private $f097fcreditAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fdebit_amount", type="integer",  nullable=true)
     */
    private $f097fdebitAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fstatus", type="integer", nullable=true)
     */
    private $f097fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fcreated_by", type="integer", nullable=true)
     */
    private $f097fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f097fupdated_by", type="integer", nullable=true)
     */
    private $f097fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f097fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f097fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f097fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f097fupdatedDtTm;



    /**
     * Get f097fid
     *
     * @return integer
     */
    public function getF097fid()
    {
        return $this->f097fid;
    }

    /**
     * Set f097fidCashBank
     *
     * @param integer $f097fidCashBank
     *
     * @return T097fcashBankDetails
     */
    public function setF097fidCashBank($f097fidCashBank)
    {
        $this->f097fidCashBank = $f097fidCashBank;

        return $this;
    }

    /**
     * Get f097fidCashBank
     *
     * @return integer
     */
    public function getF097fidCashBank()
    {
        return $this->f097fidCashBank;
    }

    

    /**
     * Set f097fcreditAmount
     *
     * @param integer $f097fcreditAmount
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreditAmount($f097fcreditAmount)
    {
        $this->f097fcreditAmount = $f097fcreditAmount;

        return $this;
    }

    /**
     * Get f097fcreditAmount
     *
     * @return integer
     */
    public function getF097fcreditAmount()
    {
        return $this->f097fcreditAmount;
    }

    /**
     * Set f097fdebitAmount
     *
     * @param integer $f097fdebitAmount
     *
     * @return T097fcashBankDetails
     */
    public function setF097fdebitAmount($f097fdebitAmount)
    {
        $this->f097fdebitAmount = $f097fdebitAmount;

        return $this;
    }

    /**
     * Get f097fdebitAmount
     *
     * @return integer
     */
    public function getF097fdebitAmount()
    {
        return $this->f097fdebitAmount;
    }

    /**
     * Set f097fstatus
     *
     * @param integer $f097fstatus
     *
     * @return T097fcashBankDetails
     */
    public function setF097fstatus($f097fstatus)
    {
        $this->f097fstatus = $f097fstatus;

        return $this;
    }

    /**
     * Get f097fstatus
     *
     * @return integer
     */
    public function getF097fstatus()
    {
        return $this->f097fstatus;
    }

    /**
     * Set f097fcreatedBy
     *
     * @param integer $f097fcreatedBy
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreatedBy($f097fcreatedBy)
    {
        $this->f097fcreatedBy = $f097fcreatedBy;

        return $this;
    }

    /**
     * Get f097fcreatedBy
     *
     * @return integer
     */
    public function getF097fcreatedBy()
    {
        return $this->f097fcreatedBy;
    }

    /**
     * Set f097fupdatedBy
     *
     * @param integer $f097fupdatedBy
     *
     * @return T097fcashBankDetails
     */
    public function setF097fupdatedBy($f097fupdatedBy)
    {
        $this->f097fupdatedBy = $f097fupdatedBy;

        return $this;
    }

    /**
     * Get f097fupdatedBy
     *
     * @return integer
     */
    public function getF097fupdatedBy()
    {
        return $this->f097fupdatedBy;
    }

    /**
     * Set f097fcreatedDtTm
     *
     * @param \DateTime $f097fcreatedDtTm
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreatedDtTm($f097fcreatedDtTm)
    {
        $this->f097fcreatedDtTm = $f097fcreatedDtTm;

        return $this;
    }

    /**
     * Get f097fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF097fcreatedDtTm()
    {
        return $this->f097fcreatedDtTm;
    }

    /**
     * Set f097fupdatedDtTm
     *
     * @param \DateTime $f097fupdatedDtTm
     *
     * @return T097fcashBankDetails
     */
    public function setF097fupdatedDtTm($f097fupdatedDtTm)
    {
        $this->f097fupdatedDtTm = $f097fupdatedDtTm;

        return $this;
    }

    /**
     * Get f097fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF097fupdatedDtTm()
    {
        return $this->f097fupdatedDtTm;
    }

    /**
     * Set f097fdebitFundCode
     *
     * @param string $f097fdebitFundCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fdebitFundCode($f097fdebitFundCode)
    {
        $this->f097fdebitFundCode = $f097fdebitFundCode;

        return $this;
    }

    /**
     * Get f097fdebitFundCode
     *
     * @return string
     */
    public function getF097fdebitFundCode()
    {
        return $this->f097fdebitFundCode;
    }

    /**
     * Set f097fdebitAccountCode
     *
     * @param string $f097fdebitAccountCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fdebitAccountCode($f097fdebitAccountCode)
    {
        $this->f097fdebitAccountCode = $f097fdebitAccountCode;

        return $this;
    }

    /**
     * Get f097fdebitAccountCode
     *
     * @return string
     */
    public function getF097fdebitAccountCode()
    {
        return $this->f097fdebitAccountCode;
    }

    /**
     * Set f097fdebitActivityCode
     *
     * @param string $f097fdebitActivityCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fdebitActivityCode($f097fdebitActivityCode)
    {
        $this->f097fdebitActivityCode = $f097fdebitActivityCode;

        return $this;
    }

    /**
     * Get f097fdebitActivityCode
     *
     * @return string
     */
    public function getF097fdebitActivityCode()
    {
        return $this->f097fdebitActivityCode;
    }

    /**
     * Set f097fdebitDepartmentCode
     *
     * @param string $f097fdebitDepartmentCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fdebitDepartmentCode($f097fdebitDepartmentCode)
    {
        $this->f097fdebitDepartmentCode = $f097fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f097fdebitDepartmentCode
     *
     * @return string
     */
    public function getF097fdebitDepartmentCode()
    {
        return $this->f097fdebitDepartmentCode;
    }


    /**
     * Set f097fcreditFundCode
     *
     * @param string $f097fcreditFundCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreditFundCode($f097fcreditFundCode)
    {
        $this->f097fcreditFundCode = $f097fcreditFundCode;

        return $this;
    }

    /**
     * Get f097fcreditFundCode
     *
     * @return string
     */
    public function getF097fcreditFundCode()
    {
        return $this->f097fcreditFundCode;
    }

    /**
     * Set f097fcreditAccountCode
     *
     * @param string $f097fcreditAccountCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreditAccountCode($f097fcreditAccountCode)
    {
        $this->f097fcreditAccountCode = $f097fcreditAccountCode;

        return $this;
    }

    /**
     * Get f097fcreditAccountCode
     *
     * @return string
     */
    public function getF097fcreditAccountCode()
    {
        return $this->f097fcreditAccountCode;
    }

    /**
     * Set f097fcreditActivityCode
     *
     * @param string $f097fcreditActivityCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreditActivityCode($f097fcreditActivityCode)
    {
        $this->f097fcreditActivityCode = $f097fcreditActivityCode;

        return $this;
    }

    /**
     * Get f097fcreditActivityCode
     *
     * @return string
     */
    public function getF097fcreditActivityCode()
    {
        return $this->f097fcreditActivityCode;
    }

    /**
     * Set f097fcreditDepartmentCode
     *
     * @param string $f097fcreditDepartmentCode
     *
     * @return T097fcashBankDetails
     */
    public function setF097fcreditDepartmentCode($f097fcreditDepartmentCode)
    {
        $this->f097fcreditDepartmentCode = $f097fcreditDepartmentCode;

        return $this;
    }

    /**
     * Get f097fcreditDepartmentCode
     *
     * @return string
     */
    public function getF097fcreditDepartmentCode()
    {
        return $this->f097fcreditDepartmentCode;
    }
}
