<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T057ffund
 *
 * @ORM\Table(name="t057ffund", uniqueConstraints={@ORM\UniqueConstraint(name="f057fcode_UNIQUE", columns={"f057fcode"})})
 * @ORM\Entity(repositoryClass="Application\Repository\FundRepository")
 */
class T057ffund
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f057fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f057fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f057fname", type="string", length=200, nullable=false)
     */
    private $f057fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f057fcode", type="string", length=200, nullable=false)
     */
    private $f057fcode;


    /**
     * @var string
     *
     * @ORM\Column(name="f057fold_code", type="string", length=200, nullable=false)
     */
    private $f057foldCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f057fcreated_by", type="integer", nullable=false)
     */
    private $f057fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f057fupdated_by", type="integer", nullable=false)
     */
    private $f057fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f057fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f057fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f057fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f057fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f057fstatus", type="integer", nullable=false)
     */
    private $f057fstatus;



    /**
     * Get f057fid
     *
     * @return integer
     */
    public function getF057fid()
    {
        return $this->f057fid;
    }

    /**
     * Set f057fname
     *
     * @param string $f057fname
     *
     * @return T057ffund
     */
    public function setF057fname($f057fname)
    {
        $this->f057fname = $f057fname;

        return $this;
    }

    /**
     * Get f057fname
     *
     * @return string
     */
    public function getF057fname()
    {
        return $this->f057fname;
    }

    /**
     * Set f057fcode
     *
     * @param string $f057fcode
     *
     * @return T057ffund
     */
    public function setF057fcode($f057fcode)
    {
        $this->f057fcode = $f057fcode;

        return $this;
    }

    /**
     * Get f057fcode
     *
     * @return string
     */
    public function getF057fcode()
    {
        return $this->f057fcode;
    }

      /**
     * Set f057foldCode
     *
     * @param string $f057foldCode
     *
     * @return T057ffund
     */
    public function setF057foldCode($f057foldCode)
    {
        $this->f057foldCode = $f057foldCode;

        return $this;
    }

    /**
     * Get f057foldCode
     *
     * @return string
     */
    public function getF057foldCode()
    {
        return $this->f057foldCode;
    }

    /**
     * Set f057fcreatedBy
     *
     * @param integer $f057fcreatedBy
     *
     * @return T057ffund
     */
    public function setF057fcreatedBy($f057fcreatedBy)
    {
        $this->f057fcreatedBy = $f057fcreatedBy;

        return $this;
    }

    /**
     * Get f057fcreatedBy
     *
     * @return integer
     */
    public function getF057fcreatedBy()
    {
        return $this->f057fcreatedBy;
    }

    /**
     * Set f057fupdatedBy
     *
     * @param integer $f057fupdatedBy
     *
     * @return T057ffund
     */
    public function setF057fupdatedBy($f057fupdatedBy)
    {
        $this->f057fupdatedBy = $f057fupdatedBy;

        return $this;
    }

    /**
     * Get f057fupdatedBy
     *
     * @return integer
     */
    public function getF057fupdatedBy()
    {
        return $this->f057fupdatedBy;
    }

    /**
     * Set f057fcreatedDtTm
     *
     * @param \DateTime $f057fcreatedDtTm
     *
     * @return T057ffund
     */
    public function setF057fcreatedDtTm($f057fcreatedDtTm)
    {
        $this->f057fcreatedDtTm = $f057fcreatedDtTm;

        return $this;
    }

    /**
     * Get f057fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF057fcreatedDtTm()
    {
        return $this->f057fcreatedDtTm;
    }

    /**
     * Set f057fupdatedDtTm
     *
     * @param \DateTime $f057fupdatedDtTm
     *
     * @return T057ffund
     */
    public function setF057fupdatedDtTm($f057fupdatedDtTm)
    {
        $this->f057fupdatedDtTm = $f057fupdatedDtTm;

        return $this;
    }

    /**
     * Get f057fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF057fupdatedDtTm()
    {
        return $this->f057fupdatedDtTm;
    }

    /**
     * Set f057fstatus
     *
     * @param boolean $f057fstatus
     *
     * @return T057ffund
     */
    public function setF057fstatus($f057fstatus)
    {
        $this->f057fstatus = $f057fstatus;

        return $this;
    }

    /**
     * Get f057fstatus
     *
     * @return boolean
     */
    public function getF057fstatus()
    {
        return $this->f057fstatus;
    }
}
