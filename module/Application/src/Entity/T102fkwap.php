<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T102fkwap
 *
 * @ORM\Table(name="t102fkwap")
 * @ORM\Entity(repositoryClass="Application\Repository\KwapRepository")
 */
class T102fkwap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f102fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f102fid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f102feffective_date", type="datetime", nullable=true)
     */
    private $f102feffectiveDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f102fkwap_percentage", type="integer", nullable=true)
     */
    private $f102fkwapPercentage;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f102fstatus", type="integer", nullable=true)
     */
    private $f102fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f102fcreated_by", type="integer", nullable=true)
     */
    private $f102fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f102fupdated_by", type="integer", nullable=true)
     */
    private $f102fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f102fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f102fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f102fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f102fupdatedDtTm;



    /**
     * Get f102fid
     *
     * @return integer
     */
    public function getF102fid()
    {
        return $this->f102fid;
    }

    /**
     * Set f102feffectiveDate
     *
     * @param \DateTime $f102feffectiveDate
     *
     * @return T102fkwap
     */
    public function setF102feffectiveDate($f102feffectiveDate)
    {
        $this->f102feffectiveDate = $f102feffectiveDate;

        return $this;
    }

    /**
     * Get f102feffectiveDate
     *
     * @return \DateTime
     */
    public function getF102feffectiveDate()
    {
        return $this->f102feffectiveDate;
    }

    /**
     * Set f102fkwapPercentage
     *
     * @param integer $f102fkwapPercentage
     *
     * @return T102fkwap
     */
    public function setF102fkwapPercentage($f102fkwapPercentage)
    {
        $this->f102fkwapPercentage = $f102fkwapPercentage;

        return $this;
    }

    /**
     * Get f102fkwapPercentage
     *
     * @return integer
     */
    public function getF102fkwapPercentage()
    {
        return $this->f102fkwapPercentage;
    }

    /**
     * Set f102fstatus
     *
     * @param integer $f102fstatus
     *
     * @return T102fkwap
     */
    public function setF102fstatus($f102fstatus)
    {
        $this->f102fstatus = $f102fstatus;

        return $this;
    }

    /**
     * Get f102fstatus
     *
     * @return integer
     */
    public function getF102fstatus()
    {
        return $this->f102fstatus;
    }

    /**
     * Set f102fcreatedBy
     *
     * @param integer $f102fcreatedBy
     *
     * @return T102fkwap
     */
    public function setF102fcreatedBy($f102fcreatedBy)
    {
        $this->f102fcreatedBy = $f102fcreatedBy;

        return $this;
    }

    /**
     * Get f102fcreatedBy
     *
     * @return integer
     */
    public function getF102fcreatedBy()
    {
        return $this->f102fcreatedBy;
    }

    /**
     * Set f102fupdatedBy
     *
     * @param integer $f102fupdatedBy
     *
     * @return T102fkwap
     */
    public function setF102fupdatedBy($f102fupdatedBy)
    {
        $this->f102fupdatedBy = $f102fupdatedBy;

        return $this;
    }

    /**
     * Get f102fupdatedBy
     *
     * @return integer
     */
    public function getF102fupdatedBy()
    {
        return $this->f102fupdatedBy;
    }

    /**
     * Set f102fcreatedDtTm
     *
     * @param \DateTime $f102fcreatedDtTm
     *
     * @return T102fkwap
     */
    public function setF102fcreatedDtTm($f102fcreatedDtTm)
    {
        $this->f102fcreatedDtTm = $f102fcreatedDtTm;

        return $this;
    }

    /**
     * Get f102fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF102fcreatedDtTm()
    {
        return $this->f102fcreatedDtTm;
    }

    /**
     * Set f102fupdatedDtTm
     *
     * @param \DateTime $f102fupdatedDtTm
     *
     * @return T102fkwap
     */
    public function setF102fupdatedDtTm($f102fupdatedDtTm)
    {
        $this->f102fupdatedDtTm = $f102fupdatedDtTm;

        return $this;
    }

    /**
     * Get f102fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF102fupdatedDtTm()
    {
        return $this->f102fupdatedDtTm;
    }
}
