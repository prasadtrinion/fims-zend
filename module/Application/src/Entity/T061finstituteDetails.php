<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T061finstituteDetails
 *
 * @ORM\Table(name="t061finstitute_details")
* @ORM\Entity(repositoryClass="Application\Repository\InvestmentInstitutionRepository")
 */
class T061finstituteDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f061fidDetails;


    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_institute", type="integer", nullable=true)
     */
    private $f061fidInstitute;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_investment_type", type="integer", nullable=true)
     */
    private $f061fidInvestmentType;


    /**
     * @var integer
     *
     * @ORM\Column(name="f061fstatus", type="integer", nullable=true)
     */
    private $f061fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fcreated_by", type="integer", nullable=true)
     */
    private $f061fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fupdated_by", type="integer", nullable=true)
     */
    private $f061fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f061fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f061fupdatedDtTm;

    /**
     * Get f061fidDetails
     *
     * @return integer
     */
    public function getF061fidDetails()
    {
        return $this->f061fidDetails;
    }


    /**
     * Set f061fidInstitute
     *
     * @param integer $f061fidInstitute
     *
     * @return T061finstituteDetails
     */
    public function setF061fidInstitute($f061fidInstitute)
    {
        $this->f061fidInstitute = $f061fidInstitute;

        return $this;
    }

    /**
     * Get f061fidInstitute
     *
     * @return integer
     */
    public function getF061fidInstitute()
    {
        return $this->f061fidInstitute;
    }

    /**
     * Set f061fidInvestmentType
     *
     * @param integer $f061fidInvestmentType
     *
     * @return T061finstituteDetails
     */
    public function setF061fidInvestmentType($f061fidInvestmentType)
    {
        $this->f061fidInvestmentType = $f061fidInvestmentType;

        return $this;
    }

    /**
     * Get f061fidInvestmentType
     *
     * @return integer
     */
    public function getF061fidInvestmentType()
    {
        return $this->f061fidInvestmentType;
    }

    /**
     * Set f061fcreatedBy
     *
     * @param integer $f061fcreatedBy
     *
     * @return T061finstituteDetails
     */
    public function setF061fcreatedBy($f061fcreatedBy)
    {
        $this->f061fcreatedBy = $f061fcreatedBy;

        return $this;
    }

    /**
     * Get f061fcreatedBy
     *
     * @return integer
     */
    public function getF061fcreatedBy()
    {
        return $this->f061fcreatedBy;
    }

    /**
     * Set f061fupdatedBy
     *
     * @param integer $f061fupdatedBy
     *
     * @return T061finstituteDetails
     */
    public function setF061fupdatedBy($f061fupdatedBy)
    {
        $this->f061fupdatedBy = $f061fupdatedBy;

        return $this;
    }

    /**
     * Get f061fupdatedBy
     *
     * @return integer
     */
    public function getF061fupdatedBy()
    {
        return $this->f061fupdatedBy;
    }

    /**
     * Set f061fcreatedDtTm
     *
     * @param \DateTime $f061fcreatedDtTm
     *
     * @return T061finstituteDetails
     */
    public function setF061fcreatedDtTm($f061fcreatedDtTm)
    {
        $this->f061fcreatedDtTm = $f061fcreatedDtTm;

        return $this;
    }

    /**
     * Get f061fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fcreatedDtTm()
    {
        return $this->f061fcreatedDtTm;
    }

    /**
     * Set f061fupdatedDtTm
     *
     * @param \DateTime $f061fupdatedDtTm
     *
     * @return T061finstituteDetails
     */
    public function setF061fupdatedDtTm($f061fupdatedDtTm)
    {
        $this->f061fupdatedDtTm = $f061fupdatedDtTm;

        return $this;
    }

    /**
     * Get f061fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF061fupdatedDtTm()
    {
        return $this->f061fupdatedDtTm;
    }

    /**
     * Set f061fstatus
     *
     * @param integer $f061fstatus
     *
     * @return T061finstituteDetails
     */
    public function setF061fstatus($f061fstatus)
    {
        $this->f061fstatus = $f061fstatus;

        return $this;
    }

    /**
     * Get f061fstatus
     *
     * @return integer
     */
    public function getF061fstatus()
    {
        return $this->f061fstatus;
    }

    
}
