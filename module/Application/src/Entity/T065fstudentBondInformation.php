<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T065fstudentBondInformation
 *
 * @ORM\Table(name="t065fstudent_bond_information")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentBondRepository")
 */
class T065fstudentBondInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f065fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f065fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fid_student", type="integer", nullable=false)
     */
    private $f065fidStudent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fid_invoice", type="integer", nullable=false)
     */
    private $f065fidInvoice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f065freturn_date", type="datetime", nullable=false)
     */
    private $f065freturnDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fid_invoice_details", type="integer", nullable=true)
     */
    private $f065fidInvoiceDetails;


    /**
     * @var string
     *
     * @ORM\Column(name="f065famount", type="integer", nullable=true)
     */
    private $f065famount;


    /**
     * @var integer
     *
     * @ORM\Column(name="f065fstatus", type="integer",  nullable=false)
     */
    private $f065fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f065fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f065fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f065fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f065fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fupdated_by", type="integer", nullable=false)
     */
    private $f065fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f065fcreated_by", type="integer", nullable=false)
     */
    private $f065fcreatedBy;

    /**
     * Get f065fid
     *
     * @return integer
     */
    public function getF065fid()
    {
        return $this->f065fid;
    }

    /**
     * Set f065fidStudent
     *
     * @param integer $f065fidStudent
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fidStudent($f065fidStudent)
    {
        $this->f065fidStudent = $f065fidStudent;

        return $this;
    }

    /**
     * Get f065fidStudent
     *
     * @return integer
     */
    public function getF065fidStudent()
    {
        return $this->f065fidStudent;
    }

    /**
     * Set f065fidInvoice
     *
     * @param integer $f065fidInvoice
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fidInvoice($f065fidInvoice)
    {
        $this->f065fidInvoice = $f065fidInvoice;

        return $this;
    }

    /**
     * Get f065fidInvoice
     *
     * @return integer
     */
    public function getF065fidInvoice()
    {
        return $this->f065fidInvoice;
    }



     /**
     * Set f065famount
     *
     * @param string $f065famount
     *
     * @return T065fstudentBondInformation
     */
    public function setF065famount($f065famount)
    {
        $this->f065famount = $f065famount;

        return $this;
    }

    /**
     * Get f065famount
     *
     * @return string
     */
    public function getF065famount()
    {
        return $this->f065famount;
    }


    /**
     * Set f065freturnDate
     *
     * @param \DateTime $f065freturnDate
     *
     * @return T065fstudentBondInformation
     */
    public function setF065freturnDate($f065freturnDate)
    {
        $this->f065freturnDate = $f065freturnDate;

        return $this;
    }

    /**
     * Get f065freturnDate
     *
     * @return \DateTime
     */
    public function getF065freturnDate()
    {
        return $this->f065freturnDate;
    }

    /**
     * Set f065fidInvoiceDetails
     *
     * @param integer $f065fidInvoiceDetails
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fidInvoiceDetails($f065fidInvoiceDetails)
    {
        $this->f065fidInvoiceDetails = $f065fidInvoiceDetails;

        return $this;
    }

    /**
     * Get f065fidInvoiceDetails
     *
     * @return integer
     */
    public function getF065fidInvoiceDetails()
    {
        return $this->f065fidInvoiceDetails;
    }

    /**
     * Set f065fstatus
     *
     * @param integer $f065fstatus
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fstatus($f065fstatus)
    {
        $this->f065fstatus = $f065fstatus;

        return $this;
    }

    /**
     * Get f065fstatus
     *
     * @return integer
     */
    public function getF065fstatus()
    {
        return $this->f065fstatus;
    }

    /**
     * Set f065fcreatedDtTm
     *
     * @param \DateTime $f065fcreatedDtTm
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fcreatedDtTm($f065fcreatedDtTm)
    {
        $this->f065fcreatedDtTm = $f065fcreatedDtTm;

        return $this;
    }

    /**
     * Get f065fcreatedDtTm
     *
     * @return \DateTime
     */
    public function geF065fcreatedDtTm()
    {
        return $this->f065fcreatedDtTm;
    }

    /**
     * Set f065fupdatedDtTm
     *
     * @param \DateTime $f065fupdatedDtTm
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fupdatedDtTm($f065fupdatedDtTm)
    {
        $this->f065fupdatedDtTm = $f065fupdatedDtTm;

        return $this;
    }

    /**
     * Get t065fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getT065fupdatedDtTm()
    {
        return $this->t065fupdatedDtTm;
    }

    /**
     * Set f065fupdatedBy
     *
     * @param integer $f065fupdatedBy
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fupdatedBy($f065fupdatedBy)
    {
        $this->f065fupdatedBy = $f065fupdatedBy;

        return $this;
    }

    /**
     * Get f065fupdatedBy
     *
     * @return integer
     */
    public function getF065fupdatedBy()
    {
        return $this->f065fupdatedBy;
    }

    /**
     * Set f065fcreatedBy
     *
     * @param integer $f065fcreatedBy
     *
     * @return T065fstudentBondInformation
     */
    public function setF065fcreatedBy($f065fcreatedBy)
    {
        $this->f065fcreatedBy = $f065fcreatedBy;

        return $this;
    }

    /**
     * Get f065fcreatedBy
     *
     * @return integer
     */
    public function getF065fcreatedBy()
    {
        return $this->f065fcreatedBy;
    }

}

