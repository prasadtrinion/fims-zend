<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T019fmonthlyProcess
 *
 * @ORM\Table(name="t019fmonthly_process")
 * @ORM\Entity(repositoryClass="Application\Repository\MonthlyProcessRepository")
 */
class T019fmonthlyProcess
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f019fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f019fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f019fmonthly_name", type="string", length=100, nullable=false)
     */
    private $f019fmonthlyName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fdate", type="datetime", nullable=false)
     */
    private $f019fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fyear", type="integer", nullable=false)
     */
    private $f019fyear;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f019fstatus", type="boolean", nullable=false)
     */
    private $f019fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fcreated_by", type="integer", nullable=false)
     */
    private $f019fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fupdated_by", type="integer", nullable=false)
     */
    private $f019fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fupdatedDtTm;



    /**
     * Get f019fid
     *
     * @return integer
     */
    public function getF019fid()
    {
        return $this->f019fid;
    }

    /**
     * Set f019fmonthlyName
     *
     * @param string $f019fmonthlyName
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fmonthlyName($f019fmonthlyName)
    {
        $this->f019fmonthlyName = $f019fmonthlyName;

        return $this;
    }

    /**
     * Get f019fmonthlyName
     *
     * @return string
     */
    public function getF019fmonthlyName()
    {
        return $this->f019fmonthlyName;
    }

    /**
     * Set f019fdate
     *
     * @param \DateTime $f019fdate
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fdate($f019fdate)
    {
        $this->f019fdate = $f019fdate;

        return $this;
    }

    /**
     * Get f019fdate
     *
     * @return \DateTime
     */
    public function getF019fdate()
    {
        return $this->f019fdate;
    }

    /**
     * Set f019fyear
     *
     * @param integer $f019fyear
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fyear($f019fyear)
    {
        $this->f019fyear = $f019fyear;

        return $this;
    }

    /**
     * Get f019fyear
     *
     * @return integer
     */
    public function getF019fyear()
    {
        return $this->f019fyear;
    }

    /**
     * Set f019fstatus
     *
     * @param boolean $f019fstatus
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fstatus($f019fstatus)
    {
        $this->f019fstatus = $f019fstatus;

        return $this;
    }

    /**
     * Get f019fstatus
     *
     * @return boolean
     */
    public function getF019fstatus()
    {
        return $this->f019fstatus;
    }

    /**
     * Set f019fcreatedBy
     *
     * @param integer $f019fcreatedBy
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fcreatedBy($f019fcreatedBy)
    {
        $this->f019fcreatedBy = $f019fcreatedBy;

        return $this;
    }

    /**
     * Get f019fcreatedBy
     *
     * @return integer
     */
    public function getF019fcreatedBy()
    {
        return $this->f019fcreatedBy;
    }

    /**
     * Set f019fupdatedBy
     *
     * @param integer $f019fupdatedBy
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fupdatedBy($f019fupdatedBy)
    {
        $this->f019fupdatedBy = $f019fupdatedBy;

        return $this;
    }

    /**
     * Get f019fupdatedBy
     *
     * @return integer
     */
    public function getF019fupdatedBy()
    {
        return $this->f019fupdatedBy;
    }

    /**
     * Set f019fcreatedDtTm
     *
     * @param \DateTime $f019fcreatedDtTm
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fcreatedDtTm($f019fcreatedDtTm)
    {
        $this->f019fcreatedDtTm = $f019fcreatedDtTm;

        return $this;
    }

    /**
     * Get f019fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fcreatedDtTm()
    {
        return $this->f019fcreatedDtTm;
    }

    /**
     * Set f019fupdatedDtTm
     *
     * @param \DateTime $f019fupdatedDtTm
     *
     * @return T019fmonthlyProcess
     */
    public function setF019fupdatedDtTm($f019fupdatedDtTm)
    {
        $this->f019fupdatedDtTm = $f019fupdatedDtTm;

        return $this;
    }

    /**
     * Get f019fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fupdatedDtTm()
    {
        return $this->f019fupdatedDtTm;
    }
}
