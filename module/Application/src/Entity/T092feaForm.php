<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T092feaForm
 *
 * @ORM\Table(name="t092fea_form")
 * @ORM\Entity(repositoryClass="Application\Repository\EaformRepository")
 */
class T092feaForm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f092fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fyear", type="integer", nullable=false)
     */
    private $f092fyear;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092freleasing_date", type="datetime", nullable=false)
     */
    private $f092freleasingDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fstatus", type="integer", nullable=false)
     */
    private $f092fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fcreated_by", type="integer", nullable=false)
     */
    private $f092fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f092fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fupdated_by", type="integer", nullable=false)
     */
    private $f092fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f092fupdatedDtTm;



    /**
     * Get f092fid
     *
     * @return integer
     */
    public function getF092fid()
    {
        return $this->f092fid;
    }

    /**
     * Set f092fyear
     *
     * @param integer $f092fyear
     *
     * @return T092feaForm
     */
    public function setF092fyear($f092fyear)
    {
        $this->f092fyear = $f092fyear;

        return $this;
    }

    /**
     * Get f092fyear
     *
     * @return integer
     */
    public function getF092fyear()
    {
        return $this->f092fyear;
    }

    /**
     * Set f092freleasingDate
     *
     * @param \DateTime $f092freleasingDate
     *
     * @return T092feaForm
     */
    public function setF092freleasingDate($f092freleasingDate)
    {
        $this->f092freleasingDate = $f092freleasingDate;

        return $this;
    }

    /**
     * Get f092freleasingDate
     *
     * @return \DateTime
     */
    public function getF092freleasingDate()
    {
        return $this->f092freleasingDate;
    }

    /**
     * Set f092fstatus
     *
     * @param integer $f092fstatus
     *
     * @return T092feaForm
     */
    public function setF092fstatus($f092fstatus)
    {
        $this->f092fstatus = $f092fstatus;

        return $this;
    }

    /**
     * Get f092fstatus
     *
     * @return integer
     */
    public function getF092fstatus()
    {
        return $this->f092fstatus;
    }

    /**
     * Set f092fcreatedBy
     *
     * @param integer $f092fcreatedBy
     *
     * @return T092feaForm
     */
    public function setF092fcreatedBy($f092fcreatedBy)
    {
        $this->f092fcreatedBy = $f092fcreatedBy;

        return $this;
    }

    /**
     * Get f092fcreatedBy
     *
     * @return integer
     */
    public function getF092fcreatedBy()
    {
        return $this->f092fcreatedBy;
    }

    /**
     * Set f092fcreatedDtTm
     *
     * @param \DateTime $f092fcreatedDtTm
     *
     * @return T092feaForm
     */
    public function setF092fcreatedDtTm($f092fcreatedDtTm)
    {
        $this->f092fcreatedDtTm = $f092fcreatedDtTm;

        return $this;
    }

    /**
     * Get f092fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fcreatedDtTm()
    {
        return $this->f092fcreatedDtTm;
    }

    /**
     * Set f092fupdatedBy
     *
     * @param integer $f092fupdatedBy
     *
     * @return T092feaForm
     */
    public function setF092fupdatedBy($f092fupdatedBy)
    {
        $this->f092fupdatedBy = $f092fupdatedBy;

        return $this;
    }

    /**
     * Get f092fupdatedBy
     *
     * @return integer
     */
    public function getF092fupdatedBy()
    {
        return $this->f092fupdatedBy;
    }

    /**
     * Set f092fupdatedDtTm
     *
     * @param \DateTime $f092fupdatedDtTm
     *
     * @return T092feaForm
     */
    public function setF092fupdatedDtTm($f092fupdatedDtTm)
    {
        $this->f092fupdatedDtTm = $f092fupdatedDtTm;

        return $this;
    }

    /**
     * Get f092fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fupdatedDtTm()
    {
        return $this->f092fupdatedDtTm;
    }
}
