<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T052fbudgetControl
 *
 * @ORM\Table(name="t052fbudget_control")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetControlRepository")
 */
class T052fbudgetControl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f052fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fdepartment", type="string",length=20, nullable=false)
     */
    private $f052fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_financialyear", type="integer", nullable=false)
     */
    private $f052fidFinancialyear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fid_user", type="integer", nullable=false)
     */
    private $f052fidUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fcreated_by", type="integer", nullable=false)
     */
    private $f052fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fupdated_by", type="integer", nullable=false)
     */
    private $f052fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f052fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f052fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f052fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f052fstatus", type="integer", nullable=false)
     */
    private $f052fstatus;



    /**
     * Get f052fid
     *
     * @return integer
     */
    public function getF052fid()
    {
        return $this->f052fid;
    }

    /**
     * Set f052fdepartment
     *
     * @param integer $f052fdepartment
     *
     * @return T052fbudgetControl
     */
    public function setF052fdepartment($f052fdepartment)
    {
        $this->f052fdepartment = $f052fdepartment;

        return $this;
    }

    /**
     * Get f052fdepartment
     *
     * @return integer
     */
    public function getF052fdepartment()
    {
        return $this->f052fdepartment;
    }

    /**
     * Set f052fidFinancialyear
     *
     * @param integer $f052fidFinancialyear
     *
     * @return T052fbudgetControl
     */
    public function setF052fidFinancialyear($f052fidFinancialyear)
    {
        $this->f052fidFinancialyear = $f052fidFinancialyear;

        return $this;
    }

    /**
     * Get f052fidFinancialyear
     *
     * @return integer
     */
    public function getF052fidFinancialyear()
    {
        return $this->f052fidFinancialyear;
    }

    /**
     * Set f052fidUser
     *
     * @param integer $f052fidUser
     *
     * @return T052fbudgetControl
     */
    public function setF052fidUser($f052fidUser)
    {
        $this->f052fidUser = $f052fidUser;

        return $this;
    }

    /**
     * Get f052fidUser
     *
     * @return integer
     */
    public function getF052fidUser()
    {
        return $this->f052fidUser;
    }

    /**
     * Set f052fcreatedBy
     *
     * @param integer $f052fcreatedBy
     *
     * @return T052fbudgetControl
     */
    public function setF052fcreatedBy($f052fcreatedBy)
    {
        $this->f052fcreatedBy = $f052fcreatedBy;

        return $this;
    }

    /**
     * Get f052fcreatedBy
     *
     * @return integer
     */
    public function getF052fcreatedBy()
    {
        return $this->f052fcreatedBy;
    }

    /**
     * Set f052fupdatedBy
     *
     * @param integer $f052fupdatedBy
     *
     * @return T052fbudgetControl
     */
    public function setF052fupdatedBy($f052fupdatedBy)
    {
        $this->f052fupdatedBy = $f052fupdatedBy;

        return $this;
    }

    /**
     * Get f052fupdatedBy
     *
     * @return integer
     */
    public function getF052fupdatedBy()
    {
        return $this->f052fupdatedBy;
    }

    /**
     * Set f052fcreatedDtTm
     *
     * @param \DateTime $f052fcreatedDtTm
     *
     * @return T052fbudgetControl
     */
    public function setF052fcreatedDtTm($f052fcreatedDtTm)
    {
        $this->f052fcreatedDtTm = $f052fcreatedDtTm;

        return $this;
    }

    /**
     * Get f052fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fcreatedDtTm()
    {
        return $this->f052fcreatedDtTm;
    }

    /**
     * Set f052fupdatedDtTm
     *
     * @param \DateTime $f052fupdatedDtTm
     *
     * @return T052fbudgetControl
     */
    public function setF052fupdatedDtTm($f052fupdatedDtTm)
    {
        $this->f052fupdatedDtTm = $f052fupdatedDtTm;

        return $this;
    }

    /**
     * Get f052fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF052fupdatedDtTm()
    {
        return $this->f052fupdatedDtTm;
    }

    /**
     * Set f052fstatus
     *
     * @param integer $f052fstatus
     *
     * @return T052fbudgetControl
     */
    public function setF052fstatus($f052fstatus)
    {
        $this->f052fstatus = $f052fstatus;

        return $this;
    }

    /**
     * Get f052fstatus
     *
     * @return integer
     */
    public function getF052fstatus()
    {
        return $this->f052fstatus;
    }
}
