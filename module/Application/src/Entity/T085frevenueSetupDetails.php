<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T085frevenueSetupDetails
 *
 * @ORM\Table(name="t085frevenue_setup_details")
 * @ORM\Entity(repositoryClass="Application\Repository\RevenueSetupRepository")
 */
class T085frevenueSetupDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f085fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fid_revenue_setup", type="integer", nullable=false)
     */
    private $f085fidRevenueSetup;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcr_fund_id", type="integer", nullable=false)
     */
    private $f085fcrFundId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcr_activity_id", type="integer", nullable=true)
     */
    private $f085fcrActivityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcr_department_id", type="integer", nullable=false)
     */
    private $f085fcrDepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcr_account_id", type="integer", nullable=false)
     */
    private $f085fcrAccountId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fdr_fund_id", type="integer", nullable=false)
     */
    private $f085fdrFundId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fdr_activity_id", type="integer", nullable=true)
     */
    private $f085fdrActivityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fdr_department_id", type="integer", nullable=false)
     */
    private $f085fdrDepartmentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fdr_account_id", type="integer", nullable=false)
     */
    private $f085fdrAccountId;

        /**
     * @var integer
     *
     * @ORM\Column(name="f085famount", type="integer", nullable=false)
     */
    private $f085famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fstatus", type="integer", nullable=false)
     */
    private $f085fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f085fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f085fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f085fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f085fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f085fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f085fupdateDtTm;



    /**
     * Get f085fid
     *
     * @return integer
     */
    public function getF085fid()
    {
        return $this->f085fid;
    }

    /**
     * Set f085fidRevenueSetup
     *
     * @param string $f085fidRevenueSetup
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fidRevenueSetup($f085fidRevenueSetup)
    {
        $this->f085fidRevenueSetup = $f085fidRevenueSetup;

        return $this;
    }

    /**
     * Get f085fidRevenueSetup
     *
     * @return string
     */
    public function getF085fidRevenueSetup()
    {
        return $this->f085fidRevenueSetup;
    }

    /**
     * Set f085fcrFundId
     *
     * @param integer $f085fcrFundId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcrFundId($f085fcrFundId)
    {
        $this->f085fcrFundId = $f085fcrFundId;

        return $this;
    }

    /**
     * Get f085fcrFundId
     *
     * @return integer
     */
    public function getF085fcrFundId()
    {
        return $this->f085fcrFundId;
    }

    /**
     * Set f085fcrActivityId
     *
     * @param integer $f085fcrActivityId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcrActivityId($f085fcrActivityId)
    {
        $this->f085fcrActivityId = $f085fcrActivityId;

        return $this;
    }

    /**
     * Get f085fcrActivityId
     *
     * @return integer
     */
    public function getF085fcrActivityId()
    {
        return $this->f085fcrActivityId;
    }

    /**
     * Set f085fcrDepartmentId
     *
     * @param integer $f085fcrDepartmentId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcrDepartmentId($f085fcrDepartmentId)
    {
        $this->f085fcrDepartmentId = $f085fcrDepartmentId;

        return $this;
    }

    /**
     * Get f085fcrDepartmentId
     *
     * @return integer
     */
    public function getF085fcrDepartmentId()
    {
        return $this->f085fcrDepartmentId;
    }

    /**
     * Set f085fcrAccountId
     *
     * @param integer $f085fcrAccountId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcrAccountId($f085fcrAccountId)
    {
        $this->f085fcrAccountId = $f085fcrAccountId;

        return $this;
    }

    /**
     * Get f085fcrAccountId
     *
     * @return integer
     */
    public function getF085fcrAccountId()
    {
        return $this->f085fcrAccountId;
    }

    /**
     * Set f085fdrFundId
     *
     * @param integer $f085fdrFundId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fdrFundId($f085fdrFundId)
    {
        $this->f085fdrFundId = $f085fdrFundId;

        return $this;
    }

    /**
     * Get f085fdrFundId
     *
     * @return integer
     */
    public function getF085fdrFundId()
    {
        return $this->f085fdrFundId;
    }

    /**
     * Set f085fdrActivityId
     *
     * @param integer $f085fdrActivityId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fdrActivityId($f085fdrActivityId)
    {
        $this->f085fdrActivityId = $f085fdrActivityId;

        return $this;
    }

    /**
     * Get f085fdrActivityId
     *
     * @return integer
     */
    public function getF085fdrActivityId()
    {
        return $this->f085fdrActivityId;
    }

    /**
     * Set f085fdrDepartmentId
     *
     * @param integer $f085fdrDepartmentId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fdrDepartmentId($f085fdrDepartmentId)
    {
        $this->f085fdrDepartmentId = $f085fdrDepartmentId;

        return $this;
    }

    /**
     * Get f085fdrDepartmentId
     *
     * @return integer
     */
    public function getF085fdrDepartmentId()
    {
        return $this->f085fdrDepartmentId;
    }

    /**
     * Set f085fdrAccountId
     *
     * @param integer $f085fdrAccountId
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fdrAccountId($f085fdrAccountId)
    {
        $this->f085fdrAccountId = $f085fdrAccountId;

        return $this;
    }

    /**
     * Get f085fdrAccountId
     *
     * @return integer
     */
    public function getF085fdrAccountId()
    {
        return $this->f085fdrAccountId;
    }

    /**
     * Set f085fstatus
     *
     * @param integer $f085fstatus
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fstatus($f085fstatus)
    {
        $this->f085fstatus = $f085fstatus;

        return $this;
    }
    

    /**
     * Get f085fstatus
     *
     * @return integer
     */
    public function getF085fstatus()
    {
        return $this->f085fstatus;
    }

     /**
     * Set f085famount
     *
     * @param integer $f085famount
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085famount($f085famount)
    {
        $this->f085famount = $f085famount;

        return $this;
    }
    

    /**
     * Get f085famount
     *
     * @return integer
     */
    public function getF085famount()
    {
        return $this->f085famount;
    }

    /**
     * Set f085fcreatedBy
     *
     * @param integer $f085fcreatedBy
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcreatedBy($f085fcreatedBy)
    {
        $this->f085fcreatedBy = $f085fcreatedBy;

        return $this;
    }

    /**
     * Get f085fcreatedBy
     *
     * @return integer
     */
    public function getF085fcreatedBy()
    {
        return $this->f085fcreatedBy;
    }

    /**
     * Set f085fupdatedBy
     *
     * @param integer $f085fupdatedBy
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fupdatedBy($f085fupdatedBy)
    {
        $this->f085fupdatedBy = $f085fupdatedBy;

        return $this;
    }

    /**
     * Get f085fupdatedBy
     *
     * @return integer
     */
    public function getF085fupdatedBy()
    {
        return $this->f085fupdatedBy;
    }

    /**
     * Set f085fcreateDtTm
     *
     * @param \DateTime $f085fcreateDtTm
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fcreateDtTm($f085fcreateDtTm)
    {
        $this->f085fcreateDtTm = $f085fcreateDtTm;

        return $this;
    }

    /**
     * Get f085fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF085fcreateDtTm()
    {
        return $this->f085fcreateDtTm;
    }

    /**
     * Set f085fupdateDtTm
     *
     * @param \DateTime $f085fupdateDtTm
     *
     * @return T085frevenueSetupDetails
     */
    public function setF085fupdateDtTm($f085fupdateDtTm)
    {
        $this->f085fupdateDtTm = $f085fupdateDtTm;

        return $this;
    }

    /**
     * Get f085fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF085fupdateDtTm()
    {
        return $this->f085fupdateDtTm;
    }
}

























