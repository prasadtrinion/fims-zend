<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T111fblockVendor
 *
 * @ORM\Table(name="t111fblock_vendor") 
 * @ORM\Entity(repositoryClass="Application\Repository\BlockVendorRepository")
 */
class T111fblockVendor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f111fid", type="integer", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f111fid;

     /**
     * @var integer
     *
     * @ORM\Column(name="f111fvendor", type="integer", length=20, nullable=false)
     */
    private $f111fvendor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f111fblocklist_from", type="datetime", nullable=false)
     */
    private $f111fblocklistFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="f111freason", type="string", length=255, nullable=false)
     */
    private $f111freason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f111frelease_date", type="datetime", nullable=false)
     */
    private $f111freleaseDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f111fstatus", type="integer", nullable=false)
     */
    private $f111fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f111fcreated_by", type="integer", nullable=false)
     */
    private $f111fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f111fupdated_by", type="integer", nullable=false)
     */
    private $f111fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f111fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f111fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f111fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f111fupdatedDtTm;
  

    /**
     * Get f111fid
     *
     * @return integer
     */
    public function getF111fid()
    {
        return $this->f111fid;
    }

    /**
     * Set f111fvendor
     *
     * @param integer $f111fvendor
     *
     * @return T111fblockVendor
     */
    public function setF111fvendor($f111fvendor)
    {
        $this->f111fvendor = $f111fvendor;

        return $this;
    }

    /**
     * Get f111fvendor
     *
     * @return integer
     */
    public function getF111fvendor()
    {
        return $this->f111fvendor;
    }

    /**
     * Set f111fblocklistFrom
     *
     * @param \DateTime $f111fblocklistFrom
     *
     * @return T111fblockVendor
     */
    public function setF111fblocklistFrom($f111fblocklistFrom)
    {
        $this->f111fblocklistFrom = $f111fblocklistFrom;

        return $this;
    }

    /**
     * Get f111fblocklistFrom
     *
     * @return \DateTime
     */
    public function getF111fblocklistFrom()
    {
        return $this->f111fblocklistFrom;
    }

    /**
     * Set f111freason
     *
     * @param string $f111freason
     *
     * @return T111fblockVendor
     */
    public function setF111freason($f111freason)
    {
        $this->f111freason = $f111freason;

        return $this;
    }

    /**
     * Get f111freason
     *
     * @return string
     */
    public function getF111freason()
    {
        return $this->f111freason;
    }

    /**
     * Set f111freleaseDate
     *
     * @param \DateTime $f111freleaseDate
     *
     * @return T111fblockVendor
     */
    public function setF111freleaseDate($f111freleaseDate)
    {
        $this->f111freleaseDate = $f111freleaseDate;

        return $this;
    }

    /**
     * Get f111freleaseDate
     *
     * @return \DateTime
     */
    public function getF111freleaseDate()
    {
        return $this->f111freleaseDate;
    }

    /**
     * Set f111fstatus
     *
     * @param integer $f111fstatus
     *
     * @return T111fblockVendor
     */
    public function setF111fstatus($f111fstatus)
    {
        $this->f111fstatus = $f111fstatus;

        return $this;
    }

    /**
     * Get f111fstatus
     *
     * @return integer
     */
    public function getF111fstatus()
    {
        return $this->f111fstatus;
    }

    /**
     * Set f111fcreatedBy
     *
     * @param integer $f111fcreatedBy
     *
     * @return T111fblockVendor
     */
    public function setF111fcreatedBy($f111fcreatedBy)
    {
        $this->f111fcreatedBy = $f111fcreatedBy;

        return $this;
    }

    /**
     * Get f111fcreatedBy
     *
     * @return integer
     */
    public function getF111fcreatedBy()
    {
        return $this->f111fcreatedBy;
    }

    /**
     * Set f111fupdatedBy
     *
     * @param integer $f111fupdatedBy
     *
     * @return T111fblockVendor
     */
    public function setF111fupdatedBy($f111fupdatedBy)
    {
        $this->f111fupdatedBy = $f111fupdatedBy;

        return $this;
    }

    /**
     * Get f111fupdatedBy
     *
     * @return integer
     */
    public function getF111fupdatedBy()
    {
        return $this->f111fupdatedBy;
    }

    /**
     * Set f111fcreatedDtTm
     *
     * @param \DateTime $f111fcreatedDtTm
     *
     * @return T111fblockVendor
     */
    public function setF111fcreatedDtTm($f111fcreatedDtTm)
    {
        $this->f111fcreatedDtTm = $f111fcreatedDtTm;

        return $this;
    }

    /**
     * Get f111fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF111fcreatedDtTm()
    {
        return $this->f111fcreatedDtTm;
    }

    /**
     * Set f111fupdatedDtTm
     *
     * @param \DateTime $f111fupdatedDtTm
     *
     * @return T111fblockVendor
     */
    public function setF111fupdatedDtTm($f111fupdatedDtTm)
    {
        $this->f111fupdatedDtTm = $f111fupdatedDtTm;

        return $this;
    }

    /**
     * Get f111fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF111fupdatedDtTm()
    {
        return $this->f111fupdatedDtTm;
    }
}
