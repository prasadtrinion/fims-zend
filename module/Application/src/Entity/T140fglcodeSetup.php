<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T140fglcodeSetup
 *
 * @ORM\Table(name="t140fglcode_setup")
 * @ORM\Entity(repositoryClass="Application\Repository\GlcodeSetupRepository")
 */
class T140fglcodeSetup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f140fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f140fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f140id_voucher_type", type="integer",  nullable=false)
     */
    private $f140fidVoucherType;

    /**
     * @var string
     *
     * @ORM\Column(name="f140fcredit_fund", type="string",length=50, nullable=false)
     */
    private $f140fcreditFund;

    /**
     * @var integer
     *
     * @ORM\Column(name="f140fcredit_department", type="string",length=50, nullable=false)
     * 
     */
    private $f140fcreditDepartment;

     /**
     * @var integer
     *
     * @ORM\Column(name="f140fcredit_activity", type="string",length=50, nullable=false)
     * 
     */
    private $f140fcreditActivity;

     /**
     * @var integer
     *
     * @ORM\Column(name="f140fcredit_account", type="string",length=50, nullable=false)
     * 
     */
    private $f140fcreditAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="f140fdebit_fund", type="string",length=50, nullable=false)
     */
    private $f140fdebitFund;

    /**
     * @var integer
     *
     * @ORM\Column(name="f140fdebit_department", type="string",length=50, nullable=false)
     * 
     */
    private $f140fdebitDepartment;

     /**
     * @var integer
     *
     * @ORM\Column(name="f140fdebit_activity", type="string",length=50, nullable=false)
     * 
     */
    private $f140fdebitActivity;

     /**
     * @var integer
     *
     * @ORM\Column(name="f140fdebit_account", type="string",length=50, nullable=false)
     * 
     */
    private $f140fdebitAccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f140fstatus", type="integer", nullable=false)
     */
    private $f140fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f140fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f140fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f140fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f140fcreatedDtTm;


    /**
     * @var integer
     *
     * @ORM\Column(name="f140fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f140fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f140fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f140fupdatedDtTm;
 	

    /**
     * Get f140fid
     *
     * @return integer
     */
    public function getF140fid()
    {
        return $this->f140fid;
    }

    /**
     * Set f140fidVoucherType
     *
     * @param string $f140fidVoucherType
     *
     * @return T140fglcodeSetup
     */
    public function setF140fidVoucherType($f140fidVoucherType)
    {
        $this->f140fidVoucherType = $f140fidVoucherType;

        return $this;
    }

    /**
     * Get f140fidVoucherType
     *
     * @return string
     */
    public function getF140fidVoucherType()
    {
        return $this->f140fidVoucherType;
    }

    /**
     * Set f140fcreditFund
     *
     * @param string $f140fcreditFund
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreditFund($f140fcreditFund)
    {
        $this->f140fcreditFund = $f140fcreditFund;

        return $this;
    }

    /**
     * Get f140fcreditFund
     *
     * @return string
     */
    public function getF140fcreditFund()
    {
        return $this->f140fcreditFund;
    }


    /**
     * Set f140fcreditDepartment
     *
     * @param integer $f140fcreditDepartment
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreditDepartment($f140fcreditDepartment)
    {
        $this->f140fcreditDepartment = $f140fcreditDepartment;

        return $this;
    }

    /**
     * Get f140fcreditDepartment
     *
     * @return integer
     */
    public function getF140fcreditDepartment()
    {
        return $this->f140fcreditDepartment;
    }

    /**
     * Set f140fcreditActivity
     *
     * @param integer $f140fcreditActivity
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreditActivity($f140fcreditActivity)
    {
        $this->f140fcreditActivity = $f140fcreditActivity;

        return $this;
    }

    /**
     * Get f140fcreditActivity
     *
     * @return integer
     */
    public function getF140fcreditActivity()
    {
        return $this->f140fcreditActivity;
    }

    /**
     * Set f140fcreditAccount
     *
     * @param integer $f140fcreditAccount
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreditAccount($f140fcreditAccount)
    {
        $this->f140fcreditAccount = $f140fcreditAccount;

        return $this;
    }

    /**
     * Get f140fcreditAccount
     *
     * @return integer
     */
    public function getF140fcreditAccount()
    {
        return $this->f140fcreditAccount;
    }

    /**
     * Set f140fdebitFund
     *
     * @param string $f140fdebitFund
     *
     * @return T140fglcodeSetup
     */
    public function setF140fdebitFund($f140fdebitFund)
    {
        $this->f140fdebitFund = $f140fdebitFund;

        return $this;
    }

    /**
     * Get f140fdebitFund
     *
     * @return string
     */
    public function getF140fdebitFund()
    {
        return $this->f140fdebitFund;
    }


    /**
     * Set f140fdebitDepartment
     *
     * @param integer $f140fdebitDepartment
     *
     * @return T140fglcodeSetup
     */
    public function setF140fdebitDepartment($f140fdebitDepartment)
    {
        $this->f140fdebitDepartment = $f140fdebitDepartment;

        return $this;
    }

    /**
     * Get f140fdebitDepartment
     *
     * @return integer
     */
    public function getF140fdebitDepartment()
    {
        return $this->f140fdebitDepartment;
    }

    /**
     * Set f140fdebitActivity
     *
     * @param integer $f140fdebitActivity
     *
     * @return T140fglcodeSetup
     */
    public function setF140fdebitActivity($f140fdebitActivity)
    {
        $this->f140fdebitActivity = $f140fdebitActivity;

        return $this;
    }

    /**
     * Get f140fdebitActivity
     *
     * @return integer
     */
    public function getF140fdebitActivity()
    {
        return $this->f140fdebitActivity;
    }

    /**
     * Set f140fdebitAccount
     *
     * @param integer $f140fdebitAccount
     *
     * @return T140fglcodeSetup
     */
    public function setF140fdebitAccount($f140fdebitAccount)
    {
        $this->f140fdebitAccount = $f140fdebitAccount;

        return $this;
    }

    /**
     * Get f140fdebitAccount
     *
     * @return integer
     */
    public function getF140fdebitAccount()
    {
        return $this->f140fdebitAccount;
    }

    /**
     * Set f140fupdatedDtTm
     *
     * @param \DateTime $f140fupdatedDtTm
     *
     * @return T140fglcodeSetup
     */
    public function setF140fupdatedDtTm($f140fupdatedDtTm)
    {
        $this->f140fupdatedDtTm = $f140fupdatedDtTm;

        return $this;
    }

    /**
     * Get f140fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF140fupdatedDtTm()
    {
        return $this->f140fupdatedDtTm;
    }

      /**
     * Set f140fcreatedDtTm
     *
     * @param \DateTime $f140fcreatedDtTm
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreatedDtTm($f140fcreatedDtTm)
    {
        $this->f140fcreatedDtTm = $f140fcreatedDtTm;

        return $this;
    }

    /**
     * Get f140fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF140fcreatedDtTm()
    {
        return $this->f140fcreatedDtTm;
    }

    /**
     * Set f140fstatus
     *
     * @param integer $f140fstatus
     *
     * @return T140fglcodeSetup
     */
    public function setF140fstatus($f140fstatus)
    {
        $this->f140fstatus = $f140fstatus;

        return $this;
    }

    /**
     * Get f140fstatus
     *
     * @return integer
     */
    public function getF140fstatus()
    {
        return $this->f140fstatus;
    }

    /**
     * Set f140fupdatedBy
     *
     * @param integer $f140fupdatedBy
     *
     * @return T140fglcodeSetup
     */
    public function setF140fupdatedBy($f140fupdatedBy)
    {
        $this->f140fupdatedBy = $f140fupdatedBy;

        return $this;
    }

    /**
     * Get f140fupdatedBy
     *
     * @return integer
     */
    public function getF140fupdatedBy()
    {
        return $this->f140fupdatedBy;
    }

     /**
     * Set f140fcreatedBy
     *
     * @param integer $f140fcreatedBy
     *
     * @return T140fglcodeSetup
     */
    public function setF140fcreatedBy($f140fcreatedBy)
    {
        $this->f140fcreatedBy = $f140fcreatedBy;

        return $this;
    }

    /**
     * Get f140fcreatedBy
     *
     * @return integer
     */
    public function getF140fcreatedBy()
    {
        return $this->f140fcreatedBy;
    }

}
