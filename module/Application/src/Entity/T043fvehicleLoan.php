<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T043fvehicleLoan
 *
 * @ORM\Table(name="t043fvehicle_loan")
 * @ORM\Entity(repositoryClass="Application\Repository\VehicleLoanRepository")
 */
class T043fvehicleLoan
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f043fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid_loan_name", type="integer", nullable=true)
     */
    private $f043fidLoanName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid_vendor", type="integer", nullable=true)
     */
    private $f043fidVendor;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freference_number", type="string", length=15, nullable=false)
     */
    private $f043freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043ffinal_status", type="integer", nullable=true)
     */
    private $f043ffinalStatus= '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fvehicle_condition", type="integer", nullable=true)
     */
    private $f043fvehicleCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fmanufacturer", type="string", length=25, nullable=true)
     */
    private $f043fmanufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fmodel", type="string", length=25, nullable=true)
     */
    private $f043fmodel;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fcc", type="string", length=25, nullable=true)
     */
    private $f043fcc;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fcompany_name", type="string", length=50, nullable=true)
     */
    private $f043fcompanyName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043finsurance_comp", type="integer", nullable=true)
     */
    private $f043finsuranceComp;

    /**
     * @var string
     *
     * @ORM\Column(name="f043floan_entitlement", type="string",length=20, nullable=true)
     */
    private $f043floanEntitlement;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fpurchase_price", type="integer", nullable=true)
     */
    private $f043fpurchasePrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043floan_amount",type="integer", nullable=true)
     */
    private $f043floanAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f043finstallment_period", type="string", length=25, nullable=true)
     */
    private $f043finstallmentPeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fdriving_license", type="string", length=25, nullable=true)
     */
    private $f043fdrivingLicense ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fchassis_no", type="string", length=25, nullable=true)
     */
    private $f043fchasisNo ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fengine_no", type="string", length=25, nullable=true)
     */
    private $f043fengineNo ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fregistration_no", type="string", length=25, nullable=true)
     */
    private $f043fregistrationNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="jkr_amount", type="integer",  nullable=true)
     */
    private $f043fjkrAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fic_no", type="string", length=25, nullable=true)
     */
    private $f043ficNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fjkr_certificate_no", type="string", length=25, nullable=true)
     */
    private $f043fjkrCertificateNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fjkr_period", type="string", length=25, nullable=true)
     */
    private $f043fjkrPeriod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapply_date", type="datetime", nullable=true)
     */
    private $f043fapplyDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fstart_date", type="datetime", nullable=true)
     */
    private $f043fstartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fend_date", type="datetime", nullable=true)
     */
    private $f043fendDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043flast_date", type="datetime", nullable=true)
     */
    private $f043flastDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f043femp_name", type="integer", nullable=true)
     */
    private $f043fempName;

    /**
     * @var string
     *
     * @ORM\Column(name="f043ffirst_guarantor", type="string", length=25, nullable=true)
     */
    private $f043ffirstGuarantor;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fsecond_guarantor", type="string", length=25, nullable=true)
     */
    private $f043fsecondGuarantor='0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover1", type="integer", nullable=true)
     */
    private $f043fapprover1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover2", type="integer", nullable=true)
     */
    private $f043fapprover2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover3", type="integer", nullable=true)
     */
    private $f043fapprover3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover4", type="integer", nullable=true)
     */
    private $f043fapprover4;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover5", type="integer", nullable=true)
     */
    private $f043fapprover5;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover6", type="integer", nullable=true)
     */
    private $f043fapprover6;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover7", type="integer", nullable=true)
     */
    private $f043fapprover7;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapprover8", type="integer", nullable=true)
     */
    private $f043fapprover8;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason1", type="string", length=200, nullable=true)
     */
    private $f043freason1 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason2", type="string", length=200, nullable=true)
     */
    private $f043freason2 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason3", type="string", length=200, nullable=true)
     */
    private $f043freason3 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason4", type="string", length=200, nullable=true)
     */
    private $f043freason4 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason5", type="string", length=200, nullable=true)
     */
    private $f043freason5 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason6", type="string", length=200, nullable=true)
     */
    private $f043freason6;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason7", type="string", length=200, nullable=true)
     */
    private $f043freason7;

    /**
     * @var string
     *
     * @ORM\Column(name="f043freason8", type="string", length=200, nullable=true)
     */
    private $f043freason8 ;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved1_date", type="datetime", nullable=true)
     */
    private $f043fapproved1Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved2_date", type="datetime", nullable=true)
     */
    private $f043fapproved2Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved3_date", type="datetime", nullable=true)
     */
    private $f043fapproved3Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved4_date", type="datetime", nullable=true)
     */
    private $f043fapproved4Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved5_date", type="datetime", nullable=true)
     */
    private $f043fapproved5Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved6_date", type="datetime", nullable=true)
     */
    private $f043fapproved6Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved7_date", type="datetime", nullable=true)
     */
    private $f043fapproved7Date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fapproved8_date", type="datetime", nullable=true)
     */
    private $f043fapproved8Date;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved1_by", type="integer", nullable=true)
     */
    private $f043fapproved1By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved2_by", type="integer", nullable=true)
     */
    private $f043fapproved2By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved3_by", type="integer", nullable=true)
     */
    private $f043fapproved3By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved4_by", type="integer", nullable=true)
     */
    private $f043fapproved4By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved5_by", type="integer", nullable=true)
     */
    private $f043fapproved5By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved6_by", type="integer", nullable=true)
     */
    private $f043fapproved6By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved7_by", type="integer", nullable=true)
     */
    private $f043fapproved7By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fapproved8_by", type="integer", nullable=true)
     */
    private $f043fapproved8By;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fstatus", type="integer", nullable=true)
     */
    private $f043fstatus= '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fcreated_by", type="integer", nullable=true)
     */
    private $f043fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fupdated_by", type="integer", nullable=true)
     */
    private $f043fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f043fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f043fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043finsurance_amt", type="integer", nullable=true)
     */
    private $f043insuranceAmt;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043ftotal_loan",type="integer", nullable=true)
     */
    private $f043ftotalLoan;

     /**
     * @var integer
     *
     * @ORM\Column(name="f043finterest_rate", type="integer", nullable=true)
     */
    private $f043finterestRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fmonth_interest_rate", type="integer", nullable=true)
     */
    private $f043fmonthInterestRate;

     /**
     * @var integer
     *
     * @ORM\Column(name="f043fprofit_ratio", type="integer", nullable=true)
     */
    private $f043fprofitRatio;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fmonthly_installment", type="integer", nullable=true)
     */
    private $f043fmonthlyInstallment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fprincipal", type="integer", nullable=true)
     */
    private $f043fprincipal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fprofit_amount", type="integer", nullable=true)
     */
    private $f043fprofitAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f043floan_year", type="string", length=25, nullable=true)
     */
    private $f043floanYear = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="f043finsurance_rate", type="integer", nullable=true)
     */
    private $f043finsuranceRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043floan_posting", type="integer", nullable=true)
     */
    private $f043floanPosting;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fpaid", type="integer", nullable=true)
     */
    private $f043fpaid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fbalance", type="integer", nullable=true)
     */
    private $f043fbalance;

    /**
     * Get f043fid
     *
     * @return integer
     */
    public function getF043fid()
    {
        return $this->f043fid;
    }

    /**
     * Set f043freferenceNumber
     *
     * @param string $f043freferenceNumber
     *
     * @return T043fvehicleLoan
     */
    public function setF043freferenceNumber($f043freferenceNumber)
    {
        $this->f043freferenceNumber = $f043freferenceNumber;

        return $this;
    }

    /**
     * Get f043freferenceNumber
     *
     * @return string
     */
    public function getF043freferenceNumber()
    {
        return $this->f043freferenceNumber;
    }

    /**
     * Set f043fidLoanName
     *
     * @param integer $f043fidLoanName
     *
     * @return T043fvehicleLoan
     */
    public function setF043fidLoanName($f043fidLoanName)
    {
        $this->f043fidLoanName = $f043fidLoanName;

        return $this;
    }

    /**
     * Get f043fidLoanName
     *
     * @return integer
     */
    public function getF043fidLoanName()
    {
        return $this->f043fidLoanName;
    }

    /**
     * Set f043fidVendor
     *
     * @param integer $f043fidVendor
     *
     * @return T043fvehicleLoan
     */
    public function setF043fidVendor($f043fidVendor)
    {
        $this->f043fidVendor = $f043fidVendor;

        return $this;
    }

    /**
     * Get f043fidVendor
     *
     * @return integer
     */
    public function getF043fidVendor()
    {
        return $this->f043fidVendor;
    }

    /**
     * Set f043ffinalStatus
     *
     * @param integer $f043ffinalStatus
     *
     * @return T043fvehicleLoan
     */
    public function setF043ffinalStatus($f043ffinalStatus)
    {
        $this->f043ffinalStatus = $f043ffinalStatus;

        return $this;
    }

    /**
     * Get f043ffinalStatus
     *
     * @return integer
     */
    public function getF043ffinalStatus()
    {
        return $this->f043ffinalStatus;
    }

    /**
     * Set f043fvehicleCondition
     *
     * @param integer $f043fvehicleCondition
     *
     * @return T043fvehicleLoan
     */
    public function setF043fvehicleCondition($f043fvehicleCondition)
    {
        $this->f043fvehicleCondition = $f043fvehicleCondition;

        return $this;
    }

    /**
     * Get f043fvehicleCondition
     *
     * @return integer
     */
    public function getF043fvehicleCondition()
    {
        return $this->f043fvehicleCondition;
    }

    /**
     * Set f043fmanufacturer
     *
     * @param string $f043fmanufacturer
     *
     * @return T043fvehicleLoan
     */
    public function setF043fmanufacturer($f043fmanufacturer)
    {
        $this->f043fmanufacturer = $f043fmanufacturer;

        return $this;
    }

    /**
     * Get f043fmanufacturer
     *
     * @return string
     */
    public function getF043fmanufacturer()
    {
        return $this->f043fmanufacturer;
    }

    /**
     * Set f043fmodel
     *
     * @param string $f043fmodel
     *
     * @return T043fvehicleLoan
     */
    public function setF043fmodel($f043fmodel)
    {
        $this->f043fmodel = $f043fmodel;

        return $this;
    }

    /**
     * Get f043fmodel
     *
     * @return string
     */
    public function getF043fmodel()
    {
        return $this->f043fmodel;
    }

    /**
     * Set f043fcc
     *
     * @param string $f043fcc
     *
     * @return T043fvehicleLoan
     */
    public function setF043fcc($f043fcc)
    {
        $this->f043fcc = $f043fcc;

        return $this;
    }

    /**
     * Get f043fcc
     *
     * @return string
     */
    public function getF043fcc()
    {
        return $this->f043fcc;
    }

    /**
     * Set f043fcompanyName
     *
     * @param string $f043fcompanyName
     *
     * @return T043fvehicleLoan
     */
    public function setF043fcompanyName($f043fcompanyName)
    {
        $this->f043fcompanyName = $f043fcompanyName;

        return $this;
    }

    /**
     * Get f043fcompanyName
     *
     * @return string
     */
    public function getF043fcompanyName()
    {
        return $this->f043fcompanyName;
    }

    /**
     * Set f043finsuranceComp
     *
     * @param integer $f043finsuranceComp
     *
     * @return T043fvehicleLoan
     */
    public function setF043finsuranceComp($f043finsuranceComp)
    {
        $this->f043finsuranceComp = $f043finsuranceComp;

        return $this;
    }

    /**
     * Get f043finsuranceComp
     *
     * @return integer
     */
    public function getF043finsuranceComp()
    {
        return $this->f043finsuranceComp;
    }

    /**
     * Set f043floanEntitlement
     *
     * @param string $f043floanEntitlement
     *
     * @return T043fvehicleLoan
     */
    public function setF043floanEntitlement($f043floanEntitlement)
    {
        $this->f043floanEntitlement = $f043floanEntitlement;

        return $this;
    }

    /**
     * Get f043floanEntitlement
     *
     * @return string
     */
    public function getF043floanEntitlement()
    {
        return $this->f043floanEntitlement;
    }

    /**
     * Set f043fpurchasePrice
     *
     * @param integer $f043fpurchasePrice
     *
     * @return T043fvehicleLoan
     */
    public function setF043fpurchasePrice($f043fpurchasePrice)
    {
        $this->f043fpurchasePrice = $f043fpurchasePrice;

        return $this;
    }

    /**
     * Get f043fpurchasePrice
     *
     * @return integer
     */
    public function getF043fpurchasePrice()
    {
        return $this->f043fpurchasePrice;
    }

    /**
     * Set f043floanAmount
     *
     * @param integer $f043floanAmount
     *
     * @return T043fvehicleLoan
     */
    public function setF043floanAmount($f043floanAmount)
    {
        $this->f043floanAmount = $f043floanAmount;

        return $this;
    }

    /**
     * Get f043floanAmount
     *
     * @return integer
     */
    public function getF043floanAmount()
    {
        return $this->f043floanAmount;
    }

    /**
     * Set f043finstallmentPeriod
     *
     * @param integer $f043finstallmentPeriod
     *
     * @return T043fvehicleLoan
     */
    public function setF043finstallmentPeriod($f043finstallmentPeriod)
    {
        $this->f043finstallmentPeriod = $f043finstallmentPeriod;

        return $this;
    }

    /**
     * Get f043finstallmentPeriod
     *
     * @return integer
     */
    public function getF043finstallmentPeriod()
    {
        return $this->f043finstallmentPeriod;
    }

    /**
     * Set f043fempName
     *
     * @param string $f043fempName
     *
     * @return T043fvehicleLoan
     */
    public function setF043fempName($f043fempName)
    {
        $this->f043fempName = $f043fempName;

        return $this;
    }

    /**
     * Get f043fempName
     *
     * @return string
     */
    public function getF043fempName()
    {
        return $this->f043fempName;
    }

    /**
     * Set f043ffirstGuarantor
     *
     * @param string $f043ffirstGuarantor
     *
     * @return T043fvehicleLoan
     */
    public function setF043ffirstGuarantor($f043ffirstGuarantor)
    {
        $this->f043ffirstGuarantor = $f043ffirstGuarantor;

        return $this;
    }

    /**
     * Get f043ffirstGuarantor
     *
     * @return string
     */
    public function getF043ffirstGuarantor()
    {
        return $this->f043ffirstGuarantor;
    }

    /**
     * Set f043fsecondGuarantor
     *
     * @param string $f043fsecondGuarantor
     *
     * @return T043fvehicleLoan
     */
    public function setF043fsecondGuarantor($f043fsecondGuarantor)
    {
        $this->f043fsecondGuarantor = $f043fsecondGuarantor;

        return $this;
    }

    /**
     * Get f043fsecondGuarantor
     *
     * @return string
     */
    public function getF043fsecondGuarantor()
    {
        return $this->f043fsecondGuarantor;
    }

    /**
     * Set f043fapprover1
     *
     * @param integer $f043fapprover1
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover1($f043fapprover1)
    {
        $this->f043fapprover1 = $f043fapprover1;

        return $this;
    }

    /**
     * Get f043fapprover1
     *
     * @return integer
     */
    public function getF043fapprover1()
    {
        return $this->f043fapprover1;
    }

    /**
     * Set f043fapprover2
     *
     * @param integer $f043fapprover2
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover2($f043fapprover2)
    {
        $this->f043fapprover2 = $f043fapprover2;

        return $this;
    }

    /**
     * Get f043fapprover2
     *
     * @return integer
     */
    public function getF043fapprover2()
    {
        return $this->f043fapprover2;
    }

    /**
     * Set f043fapprover3
     *
     * @param integer $f043fapprover3
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover3($f043fapprover3)
    {
        $this->f043fapprover3 = $f043fapprover3;

        return $this;
    }

    /**
     * Get f043fapprover3
     *
     * @return integer
     */
    public function getF043fapprover3()
    {
        return $this->f043fapprover3;
    }

    /**
     * Set f043fapprover4
     *
     * @param integer $f043fapprover4
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover4($f043fapprover4)
    {
        $this->f043fapprover4 = $f043fapprover4;

        return $this;
    }

    /**
     * Get f043fapprover4
     *
     * @return integer
     */
    public function getF043fapprover4()
    {
        return $this->f043fapprover4;
    }

     /**
     * Set f043fapprover5
     *
     * @param integer $f043fapprover5
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover5($f043fapprover5)
    {
        $this->f043fapprover5 = $f043fapprover5;

        return $this;
    }

    /**
     * Get f043fapprover5
     *
     * @return integer
     */
    public function getF043fapprover5()
    {
        return $this->f043fapprover5;
    }

    /**
     * Set f043fapprover6
     *
     * @param integer $f043fapprover6
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover6($f043fapprover6)
    {
        $this->f043fapprover6 = $f043fapprover6;

        return $this;
    }

    /**
     * Get f043fapprover6
     *
     * @return integer
     */
    public function getF043fapprover6()
    {
        return $this->f043fapprover6;
    }

    /**
     * Set f043fapprover7
     *
     * @param integer $f043fapprover7
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover7($f043fapprover7)
    {
        $this->f043fapprover7 = $f043fapprover7;

        return $this;
    }

    /**
     * Get f043fapprover7
     *
     * @return integer
     */
    public function getF043fapprover7()
    {
        return $this->f043fapprover7;
    }

    /**
     * Set f043fapprover8
     *
     * @param integer $f043fapprover8
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapprover8($f043fapprover8)
    {
        $this->f043fapprover8 = $f043fapprover8;

        return $this;
    }

    /**
     * Get f043fapprover8
     *
     * @return integer
     */
    public function getF043fapprover8()
    {
        return $this->f043fapprover8;
    }

    /**
     * Set f043freason1
     *
     * @param string $f043freason1
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason1($f043freason1)
    {
        $this->f043freason1 = $f043freason1;

        return $this;
    }

    /**
     * Get f043freason1
     *
     * @return string
     */
    public function getF043freason1()
    {
        return $this->f043freason1;
    }

    /**
     * Set f043freason2
     *
     * @param string $f043freason2
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason2($f043freason2)
    {
        $this->f043freason2 = $f043freason2;

        return $this;
    }

    /**
     * Get f043freason2
     *
     * @return string
     */
    public function getF043freason2()
    {
        return $this->f043freason2;
    }

    /**
     * Set f043freason3
     *
     * @param string $f043freason3
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason3($f043freason3)
    {
        $this->f043freason3 = $f043freason3;

        return $this;
    }

    /**
     * Get f043freason3
     *
     * @return string
     */
    public function getF043freason3()
    {
        return $this->f043freason3;
    }

    /**
     * Set f043freason4
     *
     * @param string $f043freason4
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason4($f043freason4)
    {
        $this->f043freason4 = $f043freason4;

        return $this;
    }

    /**
     * Get f043freason4
     *
     * @return string
     */
    public function getF043freason4()
    {
        return $this->f043freason4;
    }

    /**
     * Set f043freason5
     *
     * @param string $f043freason5
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason5($f043freason5)
    {
        $this->f043freason5 = $f043freason5;

        return $this;
    }

    /**
     * Get f043freason5
     *
     * @return string
     */
    public function getF043freason5()
    {
        return $this->f043freason5;
    }

    /**
     * Set f043freason6
     *
     * @param string $f043freason6
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason6($f043freason6)
    {
        $this->f043freason6 = $f043freason6;

        return $this;
    }

    /**
     * Get f043freason6
     *
     * @return string
     */
    public function getF043freason6()
    {
        return $this->f043freason6;
    }

    /**
     * Set f043freason7
     *
     * @param string $f043freason7
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason7($f043freason7)
    {
        $this->f043freason7 = $f043freason7;

        return $this;
    }

    /**
     * Get f043freason7
     *
     * @return string
     */
    public function getF043freason7()
    {
        return $this->f043freason7;
    }

    /**
     * Set f043freason8
     *
     * @param string $f043freason8
     *
     * @return T043fvehicleLoan
     */
    public function setF043freason8($f043freason8)
    {
        $this->f043freason8 = $f043freason8;

        return $this;
    }

    /**
     * Get f043freason8
     *
     * @return string
     */
    public function getF043freason8()
    {
        return $this->f043freason8;
    }

    /**
     * Set f043fapproved1By
     *
     * @param integer $f043fapproved1By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved1By($f043fapproved1By)
    {
        $this->f043fapproved1By = $f043fapproved1By;

        return $this;
    }

    /**
     * Get f043fapproved1By
     *
     * @return integer
     */
    public function getF043fapproved1By()
    {
        return $this->f043fapproved1By;
    }

    /**
     * Set f043fapproved2By
     *
     * @param integer $f043fapproved2By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved2By($f043fapproved2By)
    {
        $this->f043fapproved2By = $f043fapproved2By;

        return $this;
    }

    /**
     * Get f043fapproved2By
     *
     * @return integer
     */
    public function getF043fapproved2By()
    {
        return $this->f043fapproved2By;
    }

    /**
     * Set f043fapproved3By
     *
     * @param integer $f043fapproved3By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved3By($f043fapproved3By)
    {
        $this->f043fapproved3By = $f043fapproved3By;

        return $this;
    }

    /**
     * Get f043fapproved3By
     *
     * @return integer
     */
    public function getF043fapproved3By()
    {
        return $this->f043fapproved3By;
    }

    /**
     * Set f043fapproved4By
     *
     * @param integer $f043fapproved4By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved4By($f043fapproved4By)
    {
        $this->f043fapproved4By = $f043fapproved4By;

        return $this;
    }

    /**
     * Get f043fapproved4By
     *
     * @return integer
     */
    public function getF043fapproved4By()
    {
        return $this->f043fapproved4By;
    }

    /**
     * Set f043fapproved5By
     *
     * @param integer $f043fapproved5By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved5By($f043fapproved5By)
    {
        $this->f043fapproved5By = $f043fapproved5By;

        return $this;
    }

    /**
     * Get f043fapproved5By
     *
     * @return integer
     */
    public function getF043fapproved5By()
    {
        return $this->f043fapproved5By;
    }

    /**
     * Set f043fapproved6By
     *
     * @param integer $f043fapproved6By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved6By($f043fapproved6By)
    {
        $this->f043fapproved6By = $f043fapproved6By;

        return $this;
    }

    /**
     * Get f043fapproved6By
     *
     * @return integer
     */
    public function getF043fapproved6By()
    {
        return $this->f043fapproved6By;
    }

    /**
     * Set f043fapproved7By
     *
     * @param integer $f043fapproved7By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved7By($f043fapproved7By)
    {
        $this->f043fapproved7By = $f043fapproved7By;

        return $this;
    }

    /**
     * Get f043fapproved7By
     *
     * @return integer
     */
    public function getF043fapproved7By()
    {
        return $this->f043fapproved7By;
    }

    /**
     * Set f043fapproved8By
     *
     * @param integer $f043fapproved8By
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved8By($f043fapproved8By)
    {
        $this->f043fapproved8By = $f043fapproved8By;

        return $this;
    }

    /**
     * Get f043fapproved8By
     *
     * @return integer
     */
    public function getF043fapproved8By()
    {
        return $this->f043fapproved8By;
    }

    /**
     * Set f043fstatus
     *
     * @param integer $f043fstatus
     *
     * @return T043fvehicleLoan
     */
    public function setF043fstatus($f043fstatus)
    {
        $this->f043fstatus = $f043fstatus;

        return $this;
    }

    /**
     * Get f043fstatus
     *
     * @return integer
     */
    public function getF043fstatus()
    {
        return $this->f043fstatus;
    }

    /**
     * Set f043fcreatedBy
     *
     * @param integer $f043fcreatedBy
     *
     * @return T043fvehicleLoan
     */
    public function setF043fcreatedBy($f043fcreatedBy)
    {
        $this->f043fcreatedBy = $f043fcreatedBy;

        return $this;
    }

    /**
     * Get f043fcreatedBy
     *
     * @return integer
     */
    public function getF043fcreatedBy()
    {
        return $this->f043fcreatedBy;
    }

    /**
     * Set f043fupdatedBy
     *
     * @param integer $f043fupdatedBy
     *
     * @return T043fvehicleLoan
     */
    public function setF043fupdatedBy($f043fupdatedBy)
    {
        $this->f043fupdatedBy = $f043fupdatedBy;

        return $this;
    }

    /**
     * Get f043fupdatedBy
     *
     * @return integer
     */
    public function getF043fupdatedBy()
    {
        return $this->f043fupdatedBy;
    }

    /**
     * Set f043fcreatedDtTm
     *
     * @param \DateTime $f043fcreatedDtTm
     *
     * @return T043fvehicleLoan
     */
    public function setF043fcreatedDtTm($f043fcreatedDtTm)
    {
        $this->f043fcreatedDtTm = $f043fcreatedDtTm;

        return $this;
    }

    /**
     * Get f043fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fcreatedDtTm()
    {
        return $this->f043fcreatedDtTm;
    }

    /**
     * Set f043fupdatedDtTm
     *
     * @param \DateTime $f043fupdatedDtTm
     *
     * @return T043fvehicleLoan
     */
    public function setF043fupdatedDtTm($f043fupdatedDtTm)
    {
        $this->f043fupdatedDtTm = $f043fupdatedDtTm;

        return $this;
    }

    /**
     * Get f043fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fupdatedDtTm()
    {
        return $this->f043fupdatedDtTm;
    }

    /**
     * Set f043fdrivingLicense
     *
     * @param string $f043fdrivingLicense
     *
     * @return T043fvehicleLoan
     */
    public function setF043fdrivingLicense($f043fdrivingLicense)
    {
        $this->f043fdrivingLicense = $f043fdrivingLicense;

        return $this;
    }

    /**
     * Get f043fdrivingLicense
     *
     * @return string
     */
    public function getF043fdrivingLicense()
    {
        return $this->f043fdrivingLicense;
    }

    /**
     * Set f043fchasisNo
     *
     * @param string $f043fchasisNo
     *
     * @return T043fvehicleLoan
     */
    public function setF043fchasisNo($f043fchasisNo)
    {
        $this->f043fchasisNo = $f043fchasisNo;

        return $this;
    }

    /**
     * Get f043fchasisNo
     *
     * @return string
     */
    public function getF043fchasisNo()
    {
        return $this->f043fchasisNo;
    }

    /**
     * Set f043fengineNo
     *
     * @param string $f043fengineNo
     *
     * @return T043fvehicleLoan
     */
    public function setF043fengineNo($f043fengineNo)
    {
        $this->f043fengineNo = $f043fengineNo;

        return $this;
    }

    /**
     * Get f043fengineNo
     *
     * @return string
     */
    public function getF043fengineNo()
    {
        return $this->f043fengineNo;
    }

    /**
     * Set f043fregistrationNo
     *
     * @param string $f043fregistrationNo
     *
     * @return T043fvehicleLoan
     */
    public function setF043fregistrationNo($f043fregistrationNo)
    {
        $this->f043fregistrationNo = $f043fregistrationNo;

        return $this;
    }

    /**
     * Get f043fregistrationNo
     *
     * @return string
     */
    public function getF0f43fregistrationNo()
    {
        return $this->f043fregistrationNo;
    }

    /**
     * Set f043fjkrAmount
     *
     * @param integer $f043fjkrAmount
     *
     * @return T043fvehicleLoan
     */
    public function setF043fjkrAmount($f043fjkrAmount)
    {
        $this->f043fjkrAmount = $f043fjkrAmount;

        return $this;
    }

    /**
     * Get f043fjkrAmount
     *
     * @return integer
     */
    public function getF043fjkrAmount()
    {
        return $this->f043fjkrAmount;
    }

    /**
     * Set f043ficNo
     *
     * @param string $f043ficNo
     *
     * @return T043fvehicleLoan
     */
    public function setF043ficNo($f043ficNo)
    {
        $this->f043ficNo = $f043ficNo;

        return $this;
    }

    /**
     * Get f043ficNo
     *
     * @return string
     */
    public function getF043ficNo()
    {
        return $this->f043ficNo;
    }

    /**
     * Set f043fjkrCertificateNo
     *
     * @param string $f043fjkrCertificateNo
     *
     * @return T043fvehicleLoan
     */
    public function setF043fjkrCertificateNo($f043fjkrCertificateNo)
    {
        $this->f043fjkrCertificateNo = $f043fjkrCertificateNo;

        return $this;
    }

    /**
     * Get f043fjkrCertificateNo
     *
     * @return string
     */
    public function getF043fjkrCertificateNo()
    {
        return $this->f043fjkrCertificateNo;
    }

    /**
     * Set f043fjkrPeriod
     *
     * @param string $f043fjkrPeriod
     *
     * @return T043fvehicleLoan
     */
    public function setF043fjkrPeriod($f043fjkrPeriod)
    {
        $this->f043fjkrPeriod = $f043fjkrPeriod;

        return $this;
    }

    /**
     * Get f043fjkrPeriod
     *
     * @return string
     */
    public function getF043fjkrPeriod()
    {
        return $this->f043fjkrPeriod;
    }

    /**
     * Set f043fapplyDate
     *
     * @param \DateTime $f043fapplyDate
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapplyDate($f043fapplyDate)
    {
        $this->f043fapplyDate = $f043fapplyDate;

        return $this;
    }

    /**
     * Get f043fapplyDate
     *
     * @return \DateTime
     */
    public function getF043fapplyDate()
    {
        return $this->f043fapplyDate;
    }

    /**
     * Set f043fstartDate
     *
     * @param \DateTime $f043fstartDate
     *
     * @return T043fvehicleLoan
     */
    public function setF043fstartDate($f043fstartDate)
    {
        $this->f043fstartDate = $f043fstartDate;

        return $this;
    }

    /**
     * Get f043fstartDate
     *
     * @return \DateTime
     */
    public function getF043fstartDate()
    {
        return $this->f043fstartDate;
    }

    /**
     * Set f043fendDate
     *
     * @param \DateTime $f043fendDate
     *
     * @return T043fvehicleLoan
     */
    public function setF043fendDate($f043fendDate)
    {
        $this->f043fendDate = $f043fendDate;

        return $this;
    }

    /**
     * Get f043fendDate
     *
     * @return \DateTime
     */
    public function getF043fendDate()
    {
        return $this->f043fendDate;
    }

    /**
     * Set f043flastDate
     *
     * @param \DateTime $f043flastDate
     *
     * @return T043fvehicleLoan
     */
    public function setF043flastDate($f043flastDate)
    {
        $this->f043flastDate = $f043flastDate;

        return $this;
    }

    /**
     * Get f043flastDate
     *
     * @return \DateTime
     */
    public function getF043flastDate()
    {
        return $this->f043flastDate;
    }

    /**
     * Set f043fapproved1Date
     *
     * @param \DateTime $f043fapproved1Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved1Date($f043fapproved1Date)
    {
        $this->f043fapproved1Date = $f043fapproved1Date;

        return $this;
    }

    /**
     * Get f043fapproved1Date
     *
     * @return \DateTime
     */
    public function getF043fapproved1Date()
    {
        return $this->f043fapproved1Date;
    }

    /**
     * Set f043fapproved2Date
     *
     * @param \DateTime $f043fapproved2Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved2Date($f043fapproved2Date)
    {
        $this->f043fapproved2Date = $f043fapproved2Date;

        return $this;
    }

    /**
     * Get f043fapproved2Date
     *
     * @return \DateTime
     */
    public function getF043fapproved2Date()
    {
        return $this->f043fapproved2Date;
    }

     /**
     * Set f043fapproved3Date
     *
     * @param \DateTime $f043fapproved3Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved3Date($f043fapproved3Date)
    {
        $this->f043fapproved3Date = $f043fapproved3Date;

        return $this;
    }

    /**
     * Get f043fapproved3Date
     *
     * @return \DateTime
     */
    public function getF043fapproved3Date()
    {
        return $this->f043fapproved3Date;
    }

    /**
     * Set f043fapproved4Date
     *
     * @param \DateTime $f043fapproved4Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved4Date($f043fapproved4Date)
    {
        $this->f043fapproved4Date = $f043fapproved4Date;

        return $this;
    }

    /**
     * Get f043fapproved4Date
     *
     * @return \DateTime
     */
    public function getF043fapproved4Date()
    {
        return $this->f043fapproved4Date;
    }

    /**
     * Set f043fapproved5Date
     *
     * @param \DateTime $f043fapproved5Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved5Date($f043fapproved5Date)
    {
        $this->f043fapproved5Date = $f043fapproved5Date;

        return $this;
    }

    /**
     * Get f043fapproved5Date
     *
     * @return \DateTime
     */
    public function getF043fapproved5Date()
    {
        return $this->f043fapproved5Date;
    }

    /**
     * Set f043fapproved6Date
     *
     * @param \DateTime $f043fapproved6Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved6Date($f043fapproved6Date)
    {
        $this->f043fapproved6Date = $f043fapproved6Date;

        return $this;
    }

    /**
     * Get f043fapproved6Date
     *
     * @return \DateTime
     */
    public function getF043fapproved6Date()
    {
        return $this->f043fapproved6Date;
    }

    /**
     * Set f043fapproved7Date
     *
     * @param \DateTime $f043fapproved7Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved7Date($f043fapproved7Date)
    {
        $this->f043fapproved7Date = $f043fapproved7Date;

        return $this;
    }

    /**
     * Get f043fapproved7Date
     *
     * @return \DateTime
     */
    public function getF043fapproved7Date()
    {
        return $this->f043fapproved7Date;
    }

    /**
     * Set f043fapproved8Date
     *
     * @param \DateTime $f043fapproved8Date
     *
     * @return T043fvehicleLoan
     */
    public function setF043fapproved8Date($f043fapproved8Date)
    {
        $this->f043fapproved8Date = $f043fapproved8Date;

        return $this;
    }

    /**
     * Get f043fapproved8Date
     *
     * @return \DateTime
     */
    public function getF043fapproved8Date()
    {
        return $this->f043fapproved8Date;
    }

    /**
     * Get f043insuranceAmt
     *
     * @return integer
     */
    public function getF043insuranceAmt()
    {
        return $this->f043insuranceAmt;
    }

    /**
     * Set f043insuranceAmt
     *
     * @param integer $f043insuranceAmt
     *
     * @return T043fvehicleLoan
     */
    public function setF043insuranceAmt($f043insuranceAmt)
    {
        $this->f043insuranceAmt = $f043insuranceAmt;

        return $this;
    }

    /**
     * Get f043ftotalLoan
     *
     * @return integer
     */
    public function getF043ftotalLoan()
    {
        return $this->f043ftotalLoan;
    }

    /**
     * Set f043ftotalLoan
     *
     * @param integer $f043ftotalLoan
     *
     * @return T043fvehicleLoan
     */
    public function setF043ftotalLoan($f043ftotalLoan)
    {
        $this->f043ftotalLoan = $f043ftotalLoan;

        return $this;
    }

    /**
     * Get f043finterestRate
     *
     * @return integer
     */
    public function getF043finterestRate()
    {
        return $this->f043finterestRate;
    }

    /**
     * Set f043finterestRate
     *
     * @param integer $f043finterestRate
     *
     * @return T043fvehicleLoan
     */
    public function setF043finterestRate($f043finterestRate)
    {
        $this->f043finterestRate = $f043finterestRate;

        return $this;
    }

    /**
     * Get f043fmonthInterestRate
     *
     * @return integer
     */
    public function getF043fmonthInterestRate()
    {
        return $this->f043fmonthInterestRate;
    }

    /**
     * Set f043fmonthInterestRate
     *
     * @param integer $f043fmonthInterestRate
     *
     * @return T043fvehicleLoan
     */
    public function setF043fmonthInterestRate($f043fmonthInterestRate)
    {
        $this->f043fmonthInterestRate = $f043fmonthInterestRate;

        return $this;
    }

    /**
     * Get f043fprofitRatio
     *
     * @return integer
     */
    public function getF043fprofitRatio()
    {
        return $this->f043fprofitRatio;
    }

    /**
     * Set f043fprofitRatio
     *
     * @param integer $f043fprofitRatio
     *
     * @return T043fvehicleLoan
     */
    public function setF043fprofitRatio($f043fprofitRatio)
    {
        $this->f043fprofitRatio = $f043fprofitRatio;

        return $this;
    }

    /**
     * Get f043fmonthlyInstallment
     *
     * @return integer
     */
    public function getF043fmonthlyInstallment()
    {
        return $this->f043fmonthlyInstallment;
    }

    /**
     * Set f043fmonthlyInstallment
     *
     * @param integer $f043fmonthlyInstallment
     *
     * @return T043fvehicleLoan
     */
    public function setF043fmonthlyInstallment($f043fmonthlyInstallment)
    {
        $this->f043fmonthlyInstallment = $f043fmonthlyInstallment;

        return $this;
    }

    /**
     * Get f043fprincipal
     *
     * @return integer
     */
    public function getF043fprincipal()
    {
        return $this->f043fprincipal;
    }

    /**
     * Set f043fprincipal
     *
     * @param integer $f043fprincipal
     *
     * @return T043fvehicleLoan
     */
    public function setF043fprincipal($f043fprincipal)
    {
        $this->f043fprincipal = $f043fprincipal;

        return $this;
    }

    /**
     * Get f043fprofitAmount
     *
     * @return integer
     */
    public function getF043fprofitAmount()
    {
        return $this->f043fprofitAmount;
    }

    /**
     * Set f043fprofitAmount
     *
     * @param integer $f043fprofitAmount
     *
     * @return T043fvehicleLoan
     */
    public function setF043fprofitAmount($f043fprofitAmount)
    {
        $this->f043fprofitAmount = $f043fprofitAmount;

        return $this;
    }

    /**
     * Get f043floanYear
     *
     * @return string
     */
    public function getF043floanYear()
    {
        return $this->f043floanYear;
    }

    /**
     * Set f043floanYear
     *
     * @param string $f043floanYear
     *
     * @return T043fvehicleLoan
     */
    public function setF043floanYear($f043floanYear)
    {
        $this->f043floanYear = $f043floanYear;

        return $this;
    }

    /**
     * Get f043finsuranceRate
     *
     * @return integer
     */
    public function getF043finsuranceRate()
    {
        return $this->f043finsuranceRate;
    }

    /**
     * Set f043finsuranceRate
     *
     * @param integer $f043finsuranceRate
     *
     * @return T043fvehicleLoan
     */
    public function setF043finsuranceRate($f043finsuranceRate)
    {
        $this->f043finsuranceRate = $f043finsuranceRate;

        return $this;
    }

    /**
     * Set f043fpaid
     *
     * @param integer $f043fpaid
     *
     * @return T043fvehicleLoan
     */
    public function setF043fpaid($f043fpaid)
    {
        $this->f043fpaid = $f043fpaid;

        return $this;
    }

    /**
     * Get f043fpaid
     *
     * @return integer
     */
    public function getF043fpaid()
    {
        return $this->f043fpaid;
    }

    /**
     * Set f043fbalance
     *
     * @param integer $f043fbalance
     *
     * @return T043fvehicleLoan
     */
    public function setF043fbalance($f043fbalance)
    {
        $this->f043fbalance = $f043fbalance;

        return $this;
    }

    /**
     * Get f043fbalance
     *
     * @return integer
     */
    public function getF043fbalance()
    {
        return $this->f043fbalance;
    }

}



    

  

    

     

   



   


    
