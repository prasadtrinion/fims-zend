<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T022fclaimCustomers
 *
 * @ORM\Table(name="t022fclaim_customers")
 * @ORM\Entity(repositoryClass="Application\Repository\ClaimCustomerRepository")
 */
class T022fclaimCustomers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f022fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f022ffirst_name", type="string", length=50, nullable=false)
     */
    private $f022ffirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="f022flast_name", type="string", length=100, nullable=true)
     */
    private $f022flastName;

    /**
     * @var string
     *
     * @ORM\Column(name="f022femail_id", type="string", length=50, nullable=true)
     */
    private $f022femailId;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fpassword", type="string", length=200, nullable=true)
     */
    private $f022fpassword;

    /**
     * @var string
     *
     * @ORM\Column(name="f022faddress1", type="string", length=255, nullable=true)
     */
    private $f022faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fcity", type="string", length=50, nullable=true)
     */
    private $f022fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fstate", type="integer", nullable=true)
     */
    private $f022fstate;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fzip_code", type="string", length=20, nullable=true)
     */
    private $f022fzipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f022fcontact", type="string", length=20, nullable=true)
     */
    private $f022fcontact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f022fsupplier", type="integer", nullable=true)
     */
    private $f022fsupplier;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f022ftax_free", type="integer", nullable=true)
     */
    private $f022ftaxFree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f022fstatus", type="integer", nullable=true)
     */
    private $f022fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcreated_by", type="integer", nullable=true)
     */
    private $f022fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fupdated_by", type="integer", nullable=true)
     */
    private $f022fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid_supplier", type="integer", nullable=true)
     */
    private $f022fidSupplier;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid_tax_free", type="integer", nullable=true)
     */
    private $f022fidTaxFree;

    /**
     * @var string
     *
     * @ORM\Column(name="f022faddress2", type="text", length=65535, nullable=true)
     */
    private $f022faddress2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcountry", type="integer", nullable=true)
     */
    private $f022fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcustomer_debit", type="integer", nullable=true)
     */
    private $f022fcustomerDebit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcustomer_crebit", type="integer", nullable=true)
     */
    private $f022fcustomerCrebit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcustomer_balance", type="integer", nullable=true)
     */
    private $f022fcustomerBalance;



    /**
     * Get f022fid
     *
     * @return integer
     */
    public function getF022fid()
    {
        return $this->f022fid;
    }

    /**
     * Set f022ffirstName
     *
     * @param string $f022ffirstName
     *
     * @return T022fclaimCustomers
     */
    public function setF022ffirstName($f022ffirstName)
    {
        $this->f022ffirstName = $f022ffirstName;

        return $this;
    }

    /**
     * Get f022ffirstName
     *
     * @return string
     */
    public function getF022ffirstName()
    {
        return $this->f022ffirstName;
    }

    /**
     * Set f022flastName
     *
     * @param string $f022flastName
     *
     * @return T022fclaimCustomers
     */
    public function setF022flastName($f022flastName)
    {
        $this->f022flastName = $f022flastName;

        return $this;
    }

    /**
     * Get f022flastName
     *
     * @return string
     */
    public function getF022flastName()
    {
        return $this->f022flastName;
    }

    /**
     * Set f022femailId
     *
     * @param string $f022femailId
     *
     * @return T022fclaimCustomers
     */
    public function setF022femailId($f022femailId)
    {
        $this->f022femailId = $f022femailId;

        return $this;
    }

    /**
     * Get f022femailId
     *
     * @return string
     */
    public function getF022femailId()
    {
        return $this->f022femailId;
    }

    /**
     * Set f022fpassword
     *
     * @param string $f022fpassword
     *
     * @return T022fclaimCustomers
     */
    public function setF022fpassword($f022fpassword)
    {
        $this->f022fpassword = $f022fpassword;

        return $this;
    }

    /**
     * Get f022fpassword
     *
     * @return string
     */
    public function getF022fpassword()
    {
        return $this->f022fpassword;
    }

    /**
     * Set f022faddress1
     *
     * @param string $f022faddress1
     *
     * @return T022fclaimCustomers
     */
    public function setF022faddress1($f022faddress1)
    {
        $this->f022faddress1 = $f022faddress1;

        return $this;
    }

    /**
     * Get f022faddress1
     *
     * @return string
     */
    public function getF022faddress1()
    {
        return $this->f022faddress1;
    }

    /**
     * Set f022fcity
     *
     * @param string $f022fcity
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcity($f022fcity)
    {
        $this->f022fcity = $f022fcity;

        return $this;
    }

    /**
     * Get f022fcity
     *
     * @return string
     */
    public function getF022fcity()
    {
        return $this->f022fcity;
    }

    /**
     * Set f022fstate
     *
     * @param integer $f022fstate
     *
     * @return T022fclaimCustomers
     */
    public function setF022fstate($f022fstate)
    {
        $this->f022fstate = $f022fstate;

        return $this;
    }

    /**
     * Get f022fstate
     *
     * @return integer
     */
    public function getF022fstate()
    {
        return $this->f022fstate;
    }

    /**
     * Set f022fzipCode
     *
     * @param string $f022fzipCode
     *
     * @return T022fclaimCustomers
     */
    public function setF022fzipCode($f022fzipCode)
    {
        $this->f022fzipCode = $f022fzipCode;

        return $this;
    }

    /**
     * Get f022fzipCode
     *
     * @return string
     */
    public function getF022fzipCode()
    {
        return $this->f022fzipCode;
    }

    /**
     * Set f022fcontact
     *
     * @param string $f022fcontact
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcontact($f022fcontact)
    {
        $this->f022fcontact = $f022fcontact;

        return $this;
    }

    /**
     * Get f022fcontact
     *
     * @return string
     */
    public function getF022fcontact()
    {
        return $this->f022fcontact;
    }

    /**
     * Set f022fsupplier
     *
     * @param boolean $f022fsupplier
     *
     * @return T022fclaimCustomers
     */
    public function setF022fsupplier($f022fsupplier)
    {
        $this->f022fsupplier = $f022fsupplier;

        return $this;
    }

    /**
     * Get f022fsupplier
     *
     * @return boolean
     */
    public function getF022fsupplier()
    {
        return $this->f022fsupplier;
    }

    /**
     * Set f022ftaxFree
     *
     * @param boolean $f022ftaxFree
     *
     * @return T022fclaimCustomers
     */
    public function setF022ftaxFree($f022ftaxFree)
    {
        $this->f022ftaxFree = $f022ftaxFree;

        return $this;
    }

    /**
     * Get f022ftaxFree
     *
     * @return boolean
     */
    public function getF022ftaxFree()
    {
        return $this->f022ftaxFree;
    }

    /**
     * Set f022fstatus
     *
     * @param boolean $f022fstatus
     *
     * @return T022fclaimCustomers
     */
    public function setF022fstatus($f022fstatus)
    {
        $this->f022fstatus = $f022fstatus;

        return $this;
    }

    /**
     * Get f022fstatus
     *
     * @return boolean
     */
    public function getF022fstatus()
    {
        return $this->f022fstatus;
    }

    /**
     * Set f022fcreatedBy
     *
     * @param integer $f022fcreatedBy
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcreatedBy($f022fcreatedBy)
    {
        $this->f022fcreatedBy = $f022fcreatedBy;

        return $this;
    }

    /**
     * Get f022fcreatedBy
     *
     * @return integer
     */
    public function getF022fcreatedBy()
    {
        return $this->f022fcreatedBy;
    }

    /**
     * Set f022fupdatedBy
     *
     * @param integer $f022fupdatedBy
     *
     * @return T022fclaimCustomers
     */
    public function setF022fupdatedBy($f022fupdatedBy)
    {
        $this->f022fupdatedBy = $f022fupdatedBy;

        return $this;
    }

    /**
     * Get f022fupdatedBy
     *
     * @return integer
     */
    public function getF022fupdatedBy()
    {
        return $this->f022fupdatedBy;
    }

    /**
     * Set f022fcreatedDtTm
     *
     * @param \DateTime $f022fcreatedDtTm
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcreatedDtTm($f022fcreatedDtTm)
    {
        $this->f022fcreatedDtTm = $f022fcreatedDtTm;

        return $this;
    }

    /**
     * Get f022fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fcreatedDtTm()
    {
        return $this->f022fcreatedDtTm;
    }

    /**
     * Set f022fupdatedDtTm
     *
     * @param \DateTime $f022fupdatedDtTm
     *
     * @return T022fclaimCustomers
     */
    public function setF022fupdatedDtTm($f022fupdatedDtTm)
    {
        $this->f022fupdatedDtTm = $f022fupdatedDtTm;

        return $this;
    }

    /**
     * Get f022fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fupdatedDtTm()
    {
        return $this->f022fupdatedDtTm;
    }

    /**
     * Set f022fidSupplier
     *
     * @param integer $f022fidSupplier
     *
     * @return T022fclaimCustomers
     */
    public function setF022fidSupplier($f022fidSupplier)
    {
        $this->f022fidSupplier = $f022fidSupplier;

        return $this;
    }

    /**
     * Get f022fidSupplier
     *
     * @return integer
     */
    public function getF022fidSupplier()
    {
        return $this->f022fidSupplier;
    }

    /**
     * Set f022fidTaxFree
     *
     * @param integer $f022fidTaxFree
     *
     * @return T022fclaimCustomers
     */
    public function setF022fidTaxFree($f022fidTaxFree)
    {
        $this->f022fidTaxFree = $f022fidTaxFree;

        return $this;
    }

    /**
     * Get f022fidTaxFree
     *
     * @return integer
     */
    public function getF022fidTaxFree()
    {
        return $this->f022fidTaxFree;
    }

    /**
     * Set f022faddress2
     *
     * @param string $f022faddress2
     *
     * @return T022fclaimCustomers
     */
    public function setF022faddress2($f022faddress2)
    {
        $this->f022faddress2 = $f022faddress2;

        return $this;
    }

    /**
     * Get f022faddress2
     *
     * @return string
     */
    public function getF022faddress2()
    {
        return $this->f022faddress2;
    }

    /**
     * Set f022fcountry
     *
     * @param integer $f022fcountry
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcountry($f022fcountry)
    {
        $this->f022fcountry = $f022fcountry;

        return $this;
    }

    /**
     * Get f022fcountry
     *
     * @return integer
     */
    public function getF022fcountry()
    {
        return $this->f022fcountry;
    }

    /**
     * Set f022fcustomerDebit
     *
     * @param integer $f022fcustomerDebit
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcustomerDebit($f022fcustomerDebit)
    {
        $this->f022fcustomerDebit = $f022fcustomerDebit;

        return $this;
    }

    /**
     * Get f022fcustomerDebit
     *
     * @return integer
     */
    public function getF022fcustomerDebit()
    {
        return $this->f022fcustomerDebit;
    }

    /**
     * Set f022fcustomerCrebit
     *
     * @param integer $f022fcustomerCrebit
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcustomerCrebit($f022fcustomerCrebit)
    {
        $this->f022fcustomerCrebit = $f022fcustomerCrebit;

        return $this;
    }

    /**
     * Get f022fcustomerCrebit
     *
     * @return integer
     */
    public function getF022fcustomerCrebit()
    {
        return $this->f022fcustomerCrebit;
    }

    /**
     * Set f022fcustomerBalance
     *
     * @param integer $f022fcustomerBalance
     *
     * @return T022fclaimCustomers
     */
    public function setF022fcustomerBalance($f022fcustomerBalance)
    {
        $this->f022fcustomerBalance = $f022fcustomerBalance;

        return $this;
    }

    /**
     * Get f022fcustomerBalance
     *
     * @return integer
     */
    public function getF022fcustomerBalance()
    {
        return $this->f022fcustomerBalance;
    }
}
