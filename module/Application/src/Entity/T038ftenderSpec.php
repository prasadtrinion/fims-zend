<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T038ftenderSpec
 *
 * @ORM\Table(name="t038ftender_spec")
 * @ORM\Entity(repositoryClass="Application\Repository\TenderQuotationRepository")
 */
class T038ftenderSpec
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f038fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f038fid;

   /**
     * @var integer
     *
     * @ORM\Column(name="f038fid_item", type="integer", nullable=false)
     */
    private $f038fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f038ftax_code", type="string",length=30, nullable=true)
     */
    private $f038ftaxCode;
 
    /**
     * @var string
     *
     * @ORM\Column(name="f038ffund_code", type="string",length=20, nullable=false)
     */
    private $f038ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f038fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f038fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038factivity_code", type="string",length=20, nullable=false)
     */
    private $f038factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038faccount_code", type="string",length=20, nullable=false)
     */
    private $f038faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038fbudget_fund_code", type="string",length=20, nullable=false)
     */
    private $f038fbudgetFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f038fbudget_department_code", type="string",length=20, nullable=false)
     */
    private $f038fbudgetDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038fbudget_activity_code", type="string",length=20, nullable=false)
     */
    private $f038fbudgetActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f038fbudget_account_code", type="string",length=20, nullable=false)
     */
    private $f038fbudgetAccountCode;


    /**
     * @var integer
     *
     * @ORM\Column(name="f038fquantity", type="integer", nullable=false)
     */
    private $f038fquantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fprice", type="integer", nullable=false)
     */
    private $f038fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038ftotal", type="integer", nullable=false)
     */
    private $f038ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038ftotal_inc_tax", type="integer", nullable=false)
     */
    private $f038ftotalIncTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038frequired_date", type="datetime", nullable=false)
     */
    private $f038frequiredDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fso_code", type="string",length=20, nullable=true)
     */
    private $f038fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038ftax_amount", type="integer", nullable=true)
     */
    private $f038ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fstatus", type="integer", nullable=false)
     */
    private $f038fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038funit", type="string",length=50, nullable=false)
     */
    private $f038funit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fpercentage", type="integer", nullable=false)
     */
    private $f038fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fcreated_by", type="integer", nullable=false)
     */
    private $f038fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f038fupdated_by", type="integer", nullable=false)
     */
    private $f038fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f038fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f038fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f038fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f038fupdatedDtTm;

   /**
     * @var /integer
     *
     * @ORM\Column(name="f038fid_tender", type="integer", nullable=false)
     */
    private $f038fidTender;

    /**
     * Get f038fid
     *
     * @return integer
     */
    public function getF038fid()
    {
        return $this->f038fid;
    }

    /**
     * Set f038fidItem
     *
     * @param integer $f038fidItem
     *
     * @return T038finvoiceDetails
     */
    public function setF038fidItem($f038fidItem)
    {
        $this->f038fidItem = $f038fidItem;

        return $this;
    }

    /**
     * Get f038fidItem
     *
     * @return integer
     */
    public function getF038fidItem()
    {
        return $this->f038fidItem;
    }


    /**
     * Set f038ftaxCode
     *
     * @param string $f038ftaxCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038ftaxCode($f038ftaxCode)
    {
        $this->f038ftaxCode = $f038ftaxCode;

        return $this;
    }

    /**
     * Get f038ftaxCode
     *
     * @return string
     */
    public function getF038ftaxCode()
    {
        return $this->f038ftaxCode;
    }

    /**
     * Set f038ffundCode
     *
     * @param integer $f038ffundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038ffundCode($f038ffundCode)
    {
        $this->f038ffundCode = $f038ffundCode;

        return $this;
    }

    /**
     * Get f038ffundCode
     *
     * @return integer
     */
    public function getF038ffundCode()
    {
        return $this->f038ffundCode;
    }

    /**
     * Set f038faccountCode
     *
     * @param integer $f038faccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038faccountCode($f038faccountCode)
    {
        $this->f038faccountCode = $f038faccountCode;

        return $this;
    }

    /**
     * Get f038faccountCode
     *
     * @return integer
     */
    public function getF038faccountCode()
    {
        return $this->f038faccountCode;
    }

    /**
     * Set f038factivityCode
     *
     * @param integer $f038factivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038factivityCode($f038factivityCode)
    {
        $this->f038factivityCode = $f038factivityCode;

        return $this;
    }

    /**
     * Get f038factivityCode
     *
     * @return integer
     */
    public function getF038factivityCode()
    {
        return $this->f038factivityCode;
    }

    /**
     * Set f038fdepartmentCode
     *
     * @param integer $f038fdepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038fdepartmentCode($f038fdepartmentCode)
    {
        $this->f038fdepartmentCode = $f038fdepartmentCode;

        return $this;
    }

    /**
     * Get f038fdepartmentCode
     *
     * @return integer
     */
    public function getF038fdepartmentCode()
    {
        return $this->f038fdepartmentCode;
    }


    /**
     * Set f038fbudgetFundCode
     *
     * @param integer $f038fbudgetFundCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038fbudgetFundCode($f038fbudgetFundCode)
    {
        $this->f038fbudgetFundCode = $f038fbudgetFundCode;

        return $this;
    }

    /**
     * Get f038fbudgetFundCode
     *
     * @return integer
     */
    public function getF038fbudgetFundCode()
    {
        return $this->f038fbudgetFundCode;
    }

    /**
     * Set f038fbudgetAccountCode
     *
     * @param integer $f038fbudgetAccountCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038fbudgetAccountCode($f038fbudgetAccountCode)
    {
        $this->f038fbudgetAccountCode = $f038fbudgetAccountCode;

        return $this;
    }

    /**
     * Get f038fbudgetAccountCode
     *
     * @return integer
     */
    public function getF038fbudgetAccountCode()
    {
        return $this->f038fbudgetAccountCode;
    }

    /**
     * Set f038fbudgetActivityCode
     *
     * @param integer $f038fbudgetActivityCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038fbudgetActivityCode($f038fbudgetActivityCode)
    {
        $this->f038fbudgetActivityCode = $f038fbudgetActivityCode;

        return $this;
    }

    /**
     * Get f038fbudgetActivityCode
     *
     * @return integer
     */
    public function getF038fbudgetActivityCode()
    {
        return $this->f038fbudgetActivityCode;
    }

    /**
     * Set f038fbudgetDepartmentCode
     *
     * @param integer $f038fbudgetDepartmentCode
     *
     * @return T074frequestInvoiceDetails
     */
    public function setF038fbudgetDepartmentCode($f038fbudgetDepartmentCode)
    {
        $this->f038fbudgetDepartmentCode = $f038fbudgetDepartmentCode;

        return $this;
    }

    /**
     * Set f038fquantity
     *
     * @param integer $f038fquantity
     *
     * @return T038finvoiceDetails
     */
    public function setF038fquantity($f038fquantity)
    {
        $this->f038fquantity = $f038fquantity;

        return $this;
    }

    /**
     * Get f038fquantity
     *
     * @return integer
     */
    public function getF038fquantity()
    {
        return $this->f038fquantity;
    }

    /**
     * Set f038fprice
     *
     * @param integer $f038fprice
     *
     * @return T038finvoiceDetails
     */
    public function setF038fprice($f038fprice)
    {
        $this->f038fprice = $f038fprice;

        return $this;
    }

    /**
     * Get f038fprice
     *
     * @return integer
     */
    public function getF038fprice()
    {
        return $this->f038fprice;
    }

    /**
     * Set f038ftotal
     *
     * @param integer $f038ftotal
     *
     * @return T038finvoiceDetails
     */
    public function setF038ftotal($f038ftotal)
    {
        $this->f038ftotal = $f038ftotal;

        return $this;
    }

    /**
     * Get f038ftotal
     *
     * @return integer
     */
    public function getF038ftotal()
    {
        return $this->f038ftotal;
    }

     /**
     * Set f038ftotalIncTax
     *
     * @param integer $f038ftotalIncTax
     *
     * @return T038finvoiceDetails
     */
    public function setF038ftotalIncTax($f038ftotalIncTax)
    {
        $this->f038ftotalIncTax = $f038ftotalIncTax;

        return $this;
    }

    /**
     * Get f038ftotalIncTax
     *
     * @return integer
     */
    public function getF038ftotalIncTax()
    {
        return $this->f038ftotalIncTax;
    }

    /**
     * Set f038fstatus
     *
     * @param integer $f038fstatus
     *
     * @return T038finvoiceDetails
     */
    public function setF038fstatus($f038fstatus)
    {
        $this->f038fstatus = $f038fstatus;

        return $this;
    }

    /**
     * Get f038fstatus
     *
     * @return integer
     */
    public function getF038fstatus()
    {
        return $this->f038fstatus;
    }

    /**
     * Set f038fcreatedBy
     *
     * @param integer $f038fcreatedBy
     *
     * @return T038finvoiceDetails
     */
    public function setF038fcreatedBy($f038fcreatedBy)
    {
        $this->f038fcreatedBy = $f038fcreatedBy;

        return $this;
    }

    /**
     * Get f038fcreatedBy
     *
     * @return integer
     */
    public function getF038fcreatedBy()
    {
        return $this->f038fcreatedBy;
    }

    /**
     * Set f038fupdatedBy
     *
     * @param integer $f038fupdatedBy
     *
     * @return T038finvoiceDetails
     */
    public function setF038fupdatedBy($f038fupdatedBy)
    {
        $this->f038fupdatedBy = $f038fupdatedBy;

        return $this;
    }

    /**
     * Get f038fupdatedBy
     *
     * @return integer
     */
    public function getF038fupdatedBy()
    {
        return $this->f038fupdatedBy;
    }

    /**
     * Set f038funit
     *
     * @param integer $f038funit
     *
     * @return T038finvoiceDetails
     */
    public function setF038funit($f038funit)
    {
        $this->f038funit = $f038funit;

        return $this;
    }

    /**
     * Get f038funit
     *
     * @return integer
     */
    public function getF038funit()
    {
        return $this->f038funit;
    }

    /**
     * Set f038fcreatedDtTm
     *
     * @param \DateTime $f038fcreatedDtTm
     *
     * @return T038finvoiceDetails
     */
    public function setF038fcreatedDtTm($f038fcreatedDtTm)
    {
        $this->f038fcreatedDtTm = $f038fcreatedDtTm;

        return $this;
    }

    /**
     * Get f038fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF038fcreatedDtTm()
    {
        return $this->f038fcreatedDtTm;
    }

    /**
     * Set f038fupdatedDtTm
     *
     * @param \DateTime $f038fupdatedDtTm
     *
     * @return T038finvoiceDetails
     */
    public function setF038fupdatedDtTm($f038fupdatedDtTm)
    {
        $this->f038fupdatedDtTm = $f038fupdatedDtTm;

        return $this;
    }

    /**
     * Get f038fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF038fupdatedDtTm()
    {
        return $this->f038fupdatedDtTm;
    }
    
    /**
     * Set f038fidTender
     *
     * @param integer $f038fidTender
     *
     * @return T038finvoiceDetails
     */
    public function setF038fidTender($f038fidTender )
    {
        $this->f038fidTender = $f038fidTender;

        return $this;
    }

    /**
     * Get f038fidTender
     *
     * @return \Application\Entity\T071finvoice
     */
    public function getF038fidTender()
    {
        return $this->f038fidTender;
    }

    /**
     * Set f038frequiredDate
     *
     * @param integer $f038frequiredDate
     *
     * @return T038finvoiceDetails
     */
    public function setF038frequiredDate($f038frequiredDate)
    {
        $this->f038frequiredDate = $f038frequiredDate;

        return $this;
    }

    /**
     * Get f038frequiredDate
     *
     * @return integer
     */
    public function getF038frequiredDate()
    {
        return $this->f038frequiredDate;
    }

    /**
     * Set f038fsoCode
     *
     * @param integer $f038fsoCode
     *
     * @return T038finvoiceDetails
     */
    public function setF038fsoCode($f038fsoCode)
    {
        $this->f038fsoCode = $f038fsoCode;

        return $this;
    }

    /**
     * Get f038fsoCode
     *
     * @return integer
     */
    public function getF038fsoCode()
    {
        return $this->f038fsoCode;
    }

    /**
     * Set f038ftaxAmount
     *
     * @param integer $f038ftaxAmount
     *
     * @return T038finvoiceDetails
     */
    public function setF038ftaxAmount($f038ftaxAmount)
    {
        $this->f038ftaxAmount = $f038ftaxAmount;

        return $this;
    }

    /**
     * Get f038ftaxAmount
     *
     * @return integer
     */
    public function getF038ftaxAmount()
    {
        return $this->f038ftaxAmount;
    }

    /**
     * Set f038fpercentage
     *
     * @param integer $f038fpercentage
     *
     * @return T038finvoiceDetails
     */
    public function setF038fpercentage($f038fpercentage)
    {
        $this->f038fpercentage = $f038fpercentage;

        return $this;
    }

    /**
     * Get f038fpercentage
     *
     * @return integer
     */
    public function getF038fpercentage()
    {
        return $this->f038fpercentage;
    }
}
