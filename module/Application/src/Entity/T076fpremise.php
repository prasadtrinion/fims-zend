<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T076fpremise
 *
 * @ORM\Table(name="t076fpremise" , uniqueConstraints={@ORM\UniqueConstraint(name="f076fname_UNIQUE", columns={"f076fname"})})
 * @ORM\Entity(repositoryClass="Application\Repository\PremiseRepository")
 */
class T076fpremise
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f076fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f076fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f076fname", type="string", length=50, nullable=false)
     */
    private $f076fname;

    /**
     * @var string
     *
     * @ORM\Column(name="f076faddress1", type="text", length=65535, nullable=false)
     */
    private $f076faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f076faddress2", type="text", length=65535, nullable=false)
     */
    private $f076faddress2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fcountry", type="integer", length=20, nullable=false)
     */
    private $f076fcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fstate", type="integer", length=20, nullable=false)
     */
    private $f076fstate;

    /**
     * @var string
     *
     * @ORM\Column(name="f076fcity", type="string", length=20, nullable=false)
     */
    private $f076fcity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fzip", type="integer", length=50, nullable=false)
     */
    private $f076fzip;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fstatus", type="integer", nullable=false)
     */
    private $f076fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fcreated_by", type="integer", nullable=false)
     */
    private $f076fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fupdated_by", type="integer", nullable=false)
     */
    private $f076fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f076fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f076fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f076fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f076fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fid_premise_type", type="integer", nullable=false)
     */
    private $f076fidPremiseType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f076fpremise_code", type="string", length=50, nullable=false)
     */
    private $f076fpremiseCode;


     /**
     * Get f076fid
     *
     * @return integer
     */
    public function getF076fid()
    {
        return $this->f076fid;
    }

    /**
     * Set f076fname
     *
     * @param string $f076fname
     *
     * @return T076fpremise
     */
    public function setF076fname($f076fname)
    {
        $this->f076fname = $f076fname;

        return $this;
    }

    /**
     * Get f076fname
     *
     * @return string
     */
    public function getF076fname()
    {
        return $this->f076fname;
    }

    /**
     * Set f076faddress1
     *
     * @param string $f076faddress1
     *
     * @return T076fpremise
     */
    public function setF076faddress1($f076faddress1)
    {
        $this->f076faddress1 = $f076faddress1;

        return $this;
    }

    /**
     * Get f076faddress1
     *
     * @return string
     */
    public function getF076faddress1()
    {
        return $this->f076faddress1;
    }

    /**
     * Set f076faddress2
     *
     * @param string $f076faddress2
     *
     * @return T076fpremise
     */
    public function setF076faddress2($f076faddress2)
    {
        $this->f076faddress2 = $f076faddress2;

        return $this;
    }

    /**
     * Get f076faddress2
     *
     * @return string
     */
    public function getF076faddress2()
    {
        return $this->f076faddress2;
    }

    /**
     * Set f076fcountry
     *
     * @param string $f076fcountry
     *
     * @return T076fpremise
     */
    public function setF076fcountry($f076fcountry)
    {
        $this->f076fcountry = $f076fcountry;

        return $this;
    }

    /**
     * Get f076fcountry
     *
     * @return string
     */
    public function getF076fcountry()
    {
        return $this->f076fcountry;
    }

    /**
     * Set f076fstate
     *
     * @param string $f076fstate
     *
     * @return T076fpremise
     */
    public function setF076fstate($f076fstate)
    {
        $this->f076fstate = $f076fstate;

        return $this;
    }

    /**
     * Get f076fstate
     *
     * @return string
     */
    public function getF076fstate()
    {
        return $this->f076fstate;
    }

    /**
     * Set f076fidPremiseType
     *
     * @param integer $f076fidPremiseType
     *
     * @return T076fpremise
     */
    public function setF076fidPremiseType($f076fidPremiseType)
    {
        $this->f076fidPremiseType = $f076fidPremiseType;

        return $this;
    }

    /**
     * Get f076fidPremiseType
     *
     * @return integer
     */
    public function getF076fidPremiseType()
    {
        return $this->f076fidPremiseType;
    }

    /**
     * Set f076fcity
     *
     * @param string $f076fcity
     *
     * @return T076fpremise
     */
    public function setF076fcity($f076fcity)
    {
        $this->f076fcity = $f076fcity;

        return $this;
    }

    /**
     * Get f076fcity
     *
     * @return string
     */
    public function getF076fcity()
    {
        return $this->f076fcity;
    }

    /**
     * Set f076fpremiseCode
     *
     * @param string $f076fpremiseCode
     *
     * @return T076fpremise
     */
    public function setF076fpremiseCode($f076fpremiseCode)
    {
        $this->f076fpremiseCode = $f076fpremiseCode;

        return $this;
    }

    /**
     * Get f076fpremiseCode
     *
     * @return string
     */
    public function getF076fpremiseCode()
    {
        return $this->f076fpremiseCode;
    }


    /**
     * Set f076fzip
     *
     * @param string $f076fzip
     *
     * @return T076fpremise
     */
    public function setF076fzip($f076fzip)
    {
        $this->f076fzip = $f076fzip;

        return $this;
    }

    /**
     * Get f076fzip
     *
     * @return string
     */
    public function getF076fzip()
    {
        return $this->f076fzip;
    }

    /**
     * Set f076fstatus
     *
     * @param integer $f076fstatus
     *
     * @return T076fpremise
     */
    public function setF076fstatus($f076fstatus)
    {
        $this->f076fstatus = $f076fstatus;

        return $this;
    }

    /**
     * Get f076fstatus
     *
     * @return integer
     */
    public function getF076fstatus()
    {
        return $this->f076fstatus;
    }

    /**
     * Set f076fcreatedBy
     *
     * @param integer $f076fcreatedBy
     *
     * @return T076fpremise
     */
    public function setF076fcreatedBy($f076fcreatedBy)
    {
        $this->f076fcreatedBy = $f076fcreatedBy;

        return $this;
    }

    /**
     * Get f076fcreatedBy
     *
     * @return integer
     */
    public function getF076fcreatedBy()
    {
        return $this->f076fcreatedBy;
    }

    /**
     * Set f076fupdatedBy
     *
     * @param integer $f076fupdatedBy
     *
     * @return T076fpremise
     */
    public function setF076fupdatedBy($f076fupdatedBy)
    {
        $this->f076fupdatedBy = $f076fupdatedBy;

        return $this;
    }

    /**
     * Get f076fupdatedBy
     *
     * @return integer
     */
    public function getF076fupdatedBy()
    {
        return $this->f076fupdatedBy;
    }

    /**
     * Set f076fcreatedDtTm
     *
     * @param \DateTime $f076fcreatedDtTm
     *
     * @return T076fpremise
     */
    public function setF076fcreatedDtTm($f076fcreatedDtTm)
    {
        $this->f076fcreatedDtTm = $f076fcreatedDtTm;

        return $this;
    }

    /**
     * Get f076fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF076fcreatedDtTm()
    {
        return $this->f076fcreatedDtTm;
    }

    /**
     * Set f076fupdatedDtTm
     *
     * @param \DateTime $f076fupdatedDtTm
     *
     * @return T076fpremise
     */
    public function setF076fupdatedDtTm($f076fupdatedDtTm)
    {
        $this->f076fupdatedDtTm = $f076fupdatedDtTm;

        return $this;
    }

    /**
     * Get f076fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF076fupdatedDtTm()
    {
        return $this->f076fupdatedDtTm;
    }
}

