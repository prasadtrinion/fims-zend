<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T087fassetCategory
 *
 * @ORM\Table(name="t087fasset_category", uniqueConstraints={@ORM\UniqueConstraint(name="f087fcategory_code_UNIQUE", columns={"f087fcategory_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AssetCategoryRepository")
 */
class T087fassetCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f087fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f087fcategory_code", type="string", length=50, nullable=false)
     */
    private $f087fcategoryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f087fcategory_discription", type="string", length=50, nullable=false)
     */
    private $f087fcategoryDiscription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_item_group", type="integer", nullable=false)
     */
    private $f087fidItemGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fstatus", type="integer", nullable=false)
     */
    private $f087fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fpercentage", type="integer", nullable=false)
     */
    private $f087fpercentage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f087fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fupdated_by", type="integer", nullable=false)
     */
    private $f087fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fcreated_by", type="integer", nullable=false)
     */
    private $f087fcreatedBy;



    /**
     * Get f087fid
     *
     * @return integer
     */
    public function getF087fid()
    {
        return $this->f087fid;
    }

    /**
     * Set f087fcategoryCode
     *
     * @param string $f087fcategoryCode
     *
     * @return T087fassetCategory
     */
    public function setF087fcategoryCode($f087fcategoryCode)
    {
        $this->f087fcategoryCode = $f087fcategoryCode;

        return $this;
    }

    /**
     * Get f087fcategoryCode
     *
     * @return string
     */
    public function getF087fcategoryCode()
    {
        return $this->f087fcategoryCode;
    }

    /**
     * Set f087fcategoryDiscription
     *
     * @param string $f087fcategoryDiscription
     *
     * @return T087fassetCategory
     */
    public function setF087fcategoryDiscription($f087fcategoryDiscription)
    {
        $this->f087fcategoryDiscription = $f087fcategoryDiscription;

        return $this;
    }

    /**
     * Get f087fcategoryDiscription
     *
     * @return string
     */
    public function getF087fcategoryDiscription()
    {
        return $this->f087fcategoryDiscription;
    }

    /**
     * Set f087fidItemGroup
     *
     * @param integer $f087fidItemGroup
     *
     * @return T087fassetCategory
     */
    public function setF087fidItemGroup($f087fidItemGroup)
    {
        $this->f087fidItemGroup = $f087fidItemGroup;

        return $this;
    }

    /**
     * Get f087fidItemGroup
     *
     * @return integer
     */
    public function getF087fidItemGroup()
    {
        return $this->f087fidItemGroup;
    }

    /**
     * Set f087fstatus
     *
     * @param integer $f087fstatus
     *
     * @return T087fassetCategory
     */
    public function setF087fstatus($f087fstatus)
    {
        $this->f087fstatus = $f087fstatus;

        return $this;
    }

    /**
     * Get f087fstatus
     *
     * @return integer
     */
    public function getF087fstatus()
    {
        return $this->f087fstatus;
    }

    /**
     * Set f087fpercentage
     *
     * @param integer $f087fpercentage
     *
     * @return T087fassetCategory
     */
    public function setF087fpercentage($f087fpercentage)
    {
        $this->f087fpercentage = $f087fpercentage;

        return $this;
    }

    /**
     * Get f087fpercentage
     *
     * @return integer
     */
    public function getF087fpercentage()
    {
        return $this->f087fpercentage;
    }

    /**
     * Set f087fcreatedDtTm
     *
     * @param \DateTime $f087fcreatedDtTm
     *
     * @return T087fassetCategory
     */
    public function setF087fcreatedDtTm($f087fcreatedDtTm)
    {
        $this->f087fcreatedDtTm = $f087fcreatedDtTm;

        return $this;
    }

    /**
     * Get f087fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fcreatedDtTm()
    {
        return $this->f087fcreatedDtTm;
    }

    /**
     * Set f087fupdatedDtTm
     *
     * @param \DateTime $f087fupdatedDtTm
     *
     * @return T087fassetCategory
     */
    public function setF087fupdatedDtTm($f087fupdatedDtTm)
    {
        $this->f087fupdatedDtTm = $f087fupdatedDtTm;

        return $this;
    }

    /**
     * Get f087fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fupdatedDtTm()
    {
        return $this->f087fupdatedDtTm;
    }

    /**
     * Set f087fupdatedBy
     *
     * @param integer $f087fupdatedBy
     *
     * @return T087fassetCategory
     */
    public function setF087fupdatedBy($f087fupdatedBy)
    {
        $this->f087fupdatedBy = $f087fupdatedBy;

        return $this;
    }

    /**
     * Get f087fupdatedBy
     *
     * @return integer
     */
    public function getF087fupdatedBy()
    {
        return $this->f087fupdatedBy;
    }

    /**
     * Set f087fcreatedBy
     *
     * @param integer $f087fcreatedBy
     *
     * @return T087fassetCategory
     */
    public function setF087fcreatedBy($f087fcreatedBy)
    {
        $this->f087fcreatedBy = $f087fcreatedBy;

        return $this;
    }

    /**
     * Get f087fcreatedBy
     *
     * @return integer
     */
    public function getF087fcreatedBy()
    {
        return $this->f087fcreatedBy;
    }
}
