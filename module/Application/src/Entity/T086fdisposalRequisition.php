<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T086fdisposalRequisition
 *
 * @ORM\Table(name="t086fdisposal_requisition")
 * @ORM\Entity(repositoryClass="Application\Repository\DisposalRequisitionRepository")
 */
class T086fdisposalRequisition
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f086fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fbatch_id", type="string", length=30, nullable=true)
     */
    private $f086fbatchId;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdescription", type="string", length=100, nullable=true)
     */
    private $f086fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fverification_status", type="string",length=20, nullable=true)
     */
    private $f086fverificationStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086frequest_by", type="integer", nullable=true)
     */
    private $f086frequestBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fis_endorsement", type="integer", nullable=true)
     */
    private $f086fisEndorsement;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fstatus", type="integer", nullable=true)
     */
    private $f086fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fcreated_by", type="integer", nullable=true)
     */
    private $f086fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fupdated_by", type="integer", nullable=true)
     */
    private $f086fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fupdatedDtTm;



    /**
     * Get f086fid
     *
     * @return integer
     */
    public function getF086fid()
    {
        return $this->f086fid;
    }

    /**
     * Set f086fbatchId
     *
     * @param string $f086fbatchId
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fbatchId($f086fbatchId)
    {
        $this->f086fbatchId = $f086fbatchId;

        return $this;
    }

    /**
     * Get f086fbatchId
     *
     * @return string
     */
    public function getF086fbatchId()
    {
        return $this->f086fbatchId;
    }


    /**
     * Set f086fdescription
     *
     * @param string $f086fdescription
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fdescription($f086fdescription)
    {
        $this->f086fdescription = $f086fdescription;

        return $this;
    }

    /**
     * Get f086fdescription
     *
     * @return string
     */
    public function getF086fdescription()
    {
        return $this->f086fdescription;
    }

    /**
     * Set f086fverificationStatus
     *
     * @param integer $f086fverificationStatus
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fverificationStatus($f086fverificationStatus)
    {
        $this->f086fverificationStatus = $f086fverificationStatus;

        return $this;
    }

    /**
     * Get f086fverificationStatus
     *
     * @return integer
     */
    public function getF086fverificationStatus()
    {
        return $this->f086fverificationStatus;
    }

    /**
     * Set f086fstatus
     *
     * @param integer $f086fstatus
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fstatus($f086fstatus)
    {
        $this->f086fstatus = $f086fstatus;

        return $this;
    }

    /**
     * Get f086frequestBy
     *
     * @return integer
     */
    public function getF086frequestBy()
    {
        return $this->f086frequestBy;
    }

    /**
     * Set f086frequestBy
     *
     * @param integer $f086frequestBy
     *
     * @return T086fdisposalRequisition
     */
    public function setF086frequestBy($f086frequestBy)
    {
        $this->f086frequestBy = $f086frequestBy;

        return $this;
    }

    

    /**
     * Set f086fcreatedBy
     *
     * @param integer $f086fcreatedBy
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fcreatedBy($f086fcreatedBy)
    {
        $this->f086fcreatedBy = $f086fcreatedBy;

        return $this;
    }

    /**
     * Get f086fcreatedBy
     *
     * @return integer
     */
    public function getF086fcreatedBy()
    {
        return $this->f086fcreatedBy;
    }

    /**
     * Set f086fupdatedBy
     *
     * @param integer $f086fupdatedBy
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fupdatedBy($f086fupdatedBy)
    {
        $this->f086fupdatedBy = $f086fupdatedBy;

        return $this;
    }

    /**
     * Get f086fupdatedBy
     *
     * @return integer
     */
    public function getF086fupdatedBy()
    {
        return $this->f086fupdatedBy;
    }

    /**
     * Set f086fisEndorsement
     *
     * @param integer $f086fisEndorsement
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fisEndorsement($f086fisEndorsement)
    {
        $this->f086fisEndorsement = $f086fisEndorsement;

        return $this;
    }

    /**
     * Get f086fisEndorsement
     *
     * @return integer
     */
    public function getF086fisEndorsement()
    {
        return $this->f086fisEndorsement;
    }

    /**
     * Set f086fcreatedDtTm
     *
     * @param \DateTime $f086fcreatedDtTm
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fcreatedDtTm($f086fcreatedDtTm)
    {
        $this->f086fcreatedDtTm = $f086fcreatedDtTm;

        return $this;
    }

    /**
     * Get f086fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fcreatedDtTm()
    {
        return $this->f086fcreatedDtTm;
    }

    /**
     * Set f086fupdatedDtTm
     *
     * @param \DateTime $f086fupdatedDtTm
     *
     * @return T086fdisposalRequisition
     */
    public function setF086fupdatedDtTm($f086fupdatedDtTm)
    {
        $this->f086fupdatedDtTm = $f086fupdatedDtTm;

        return $this;
    }

    /**
     * Get f086fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fupdatedDtTm()
    {
        return $this->f086fupdatedDtTm;
    }
}
