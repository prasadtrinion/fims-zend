<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T011femployeeSalary
 *
 * @ORM\Table(name="t011femployee_salary")
 * @ORM\Entity(repositoryClass="Application\Repository\EmployeeSalaryRepository")
 */
class T011femployeeSalary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f011fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f011femployee_id", type="integer", nullable=false)
     */
    private $f011femployeeId;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fid_salary_component", type="string",length=10, nullable=false)
     */
    private $f011fidSalaryComponent;

     /**
     * @var integer
     *
     * @ORM\Column(name="f011famount", type="integer", nullable=false)
     */
    private $f011famount;

     /**
     * @var string
     *
     * @ORM\Column(name="f011fcurrency", type="string", length=20, nullable=true)
     */
    private $f011fcurrency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f011fupdatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011ffinancial_period", type="string",length=20, nullable=false)
     */
    private $f011ffinancialPeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="f011ffinancial_year", type="integer",  nullable=false)
     */
    private $f011ffinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fstatus", type="integer", nullable=true)
     */
    private $f011fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fupdated_by", type="integer", nullable=true)
     */
    
    private $f011fupdatedBy;

    /**
     * Get f011fid
     *
     * @return integer
     */

    public function getF011fid()
    {
        return $this->f011fid;
    }

    /**
     * Set f011femployeeId
     *
     * @param string $f011femployeeId
     *
     * @return T011femployeeSalary
     */
    public function setF011femployeeId($f011femployeeId)
    {
        $this->f011femployeeId = $f011femployeeId;

        return $this;
    }

    /**
     * Get f011femployeeId
     *
     * @return string
     */
    public function getF011femployeeId()
    {
        return $this->f011femployeeId;
    }

    /**
     * Set f011fidSalaryComponent
     *
     * @param string $f011fidSalaryComponent
     *
     * @return T011femployeeSalary
     */
    public function setF011fidSalaryComponent($f011fidSalaryComponent)
    {
        $this->f011fidSalaryComponent = $f011fidSalaryComponent;

        return $this;
    }

    /**
     * Get f011fidSalaryComponent
     *
     * @return string
     */
    public function getF011fidSalaryComponent()
    {
        return $this->f011fidSalaryComponent;
    }

    /**
     * Set f011fcurrency
     *
     * @param string $f011fcurrency
     *
     * @return T011femployeeSalary
     */
    public function setF011fcurrency($f011fcurrency)
    {
        $this->f011fcurrency = $f011fcurrency;

        return $this;
    }

    /**
     * Get f011fcurrency
     *
     * @return string
     */
    public function getF011fcurrency()
    {
        return $this->f011fcurrency;
    }

    /**
     * Set f011famount
     *
     * @param integer $f011famount
     *
     * @return T011femployeeSalary
     */
    public function setF011famount($f011famount)
    {
        $this->f011famount = $f011famount;

        return $this;
    }

    /**
     * Get f011famount
     *
     * @return integer
     */
    public function getF011famount()
    {
        return $this->f011famount;
    }

    /**
     * Set f011ffinancialPeriod
     *
     * @param string $f011ffinancialPeriod
     *
     * @return T011femployeeSalary
     */
    public function setF011ffinancialPeriod($f011ffinancialPeriod)
    {
        $this->f011ffinancialPeriod = $f011ffinancialPeriod;

        return $this;
    }

    /**
     * Get f011ffinancialPeriod
     *
     * @return string
     */
    public function getF011ffinancialPeriod()
    {
        return $this->f011ffinancialPeriod;
    }

    /**
     * Set f011ffinancialYear
     *
     * @param string $f011ffinancialYear
     *
     * @return T011femployeeSalary
     */
    public function setF011ffinancialYear($f011ffinancialYear)
    {
        $this->f011ffinancialYear = $f011ffinancialYear;

        return $this;
    }

    /**
     * Get f011ffinancialYear
     *
     * @return string
     */
    public function getF011ffinancialYear()
    {
        return $this->f011ffinancialYear;
    }

    /**
     * Set f011fupdatedDtTm
     *
     * @param \DateTime $f011fupdatedDtTm
     *
     * @return T011femployeeSalary
     */
    public function setF011fupdatedDtTm($f011fupdatedDtTm)
    {
        $this->f011fupdatedDtTm = $f011fupdatedDtTm;

        return $this;
    }

    /**
     * Get f011fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF011fupdatedDtTm()
    {
        return $this->f011fupdatedDtTm;
    }

    /**
     * Set f011fstatus
     *
     * @param boolean $f011fstatus
     *
     * @return T011femployeeSalary
     */
    public function setF011fstatus($f011fstatus)
    {
        $this->f011fstatus = $f011fstatus;

        return $this;
    }

    /**
     * Get f011fstatus
     *
     * @return boolean
     */
    public function getF011fstatus()
    {
        return $this->f011fstatus;
    }

    /**
     * Set f011fupdatedBy
     *
     * @param string $f011fupdatedBy
     *
     * @return T011femployeeSalary
     */
    public function setF011fupdatedBy($f011fupdatedBy)
    {
        $this->f011fupdatedBy = $f011fupdatedBy;

        return $this;
    }

    
    public function getF011fupdatedBy()
    {
        return $this->f011fupdatedBy;
    }

    /**
     * Set f011fidCountry
     *
     * @param string $f011fidCountry
     *
     * @return T011femployeeSalary
     */
    public function setF011fidCountry($f011fidCountry )
    {
        $this->f011fidCountry = $f011fidCountry;

        return $this;
    }

    
    public function getF011fidCountry()
    {
        return $this->f011fidCountry;
    }
}
