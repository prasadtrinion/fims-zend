<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T018flawyer
 *
 * @ORM\Table(name="t018flawyer", uniqueConstraints={@ORM\UniqueConstraint(name="f018flawyer_name_UNIQUE", columns={"f018flawyer_name"})})
 * @ORM\Entity
 */
class T018flawyer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f018fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f018fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f018flawyer_name", type="string", length=45, nullable=false)
     */
    private $f018flawyerName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fdate_of_surrender", type="date", nullable=true)
     */
    private $f018fdateOfSurrender;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fclaims", type="string", length=255, nullable=false)
     */
    private $f018fclaims;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fattorneys_fees", type="string", length=255, nullable=false)
     */
    private $f018fattorneysFees;

    /**
     * @var string
     *
     * @ORM\Column(name="f018ftotal", type="string", length=255, nullable=false)
     */
    private $f018ftotal;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fdate_of_claim", type="string", length=255, nullable=false)
     */
    private $f018fdateOfClaim;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fdescription", type="string", length=255, nullable=false)
     */
    private $f018fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fstatus", type="string", length=30, nullable=false)
     */
    private $f018fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fcreated_by", type="string", length=50, nullable=false)
     */
    private $f018fcreatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="f018fupdated_by", type="string", length=50, nullable=false)
     */
    private $f018fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f018fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f018fupdateDtTm;



    /**
     * Get f018fid
     *
     * @return integer
     */
    public function getF018fid()
    {
        return $this->f018fid;
    }

    /**
     * Set f018flawyerName
     *
     * @param string $f018flawyerName
     *
     * @return T018flawyer
     */
    public function setF018flawyerName($f018flawyerName)
    {
        $this->f018flawyerName = $f018flawyerName;

        return $this;
    }

    /**
     * Get f018flawyerName
     *
     * @return string
     */
    public function getF018flawyerName()
    {
        return $this->f018flawyerName;
    }

    /**
     * Set f018fdateOfSurrender
     *
     * @param \DateTime $f018fdateOfSurrender
     *
     * @return T018flawyer
     */
    public function setF018fdateOfSurrender($f018fdateOfSurrender)
    {
        $this->f018fdateOfSurrender = $f018fdateOfSurrender;

        return $this;
    }

    /**
     * Get f018fdateOfSurrender
     *
     * @return \DateTime
     */
    public function getF018fdateOfSurrender()
    {
        return $this->f018fdateOfSurrender;
    }

    /**
     * Set f018fclaims
     *
     * @param string $f018fclaims
     *
     * @return T018flawyer
     */
    public function setF018fclaims($f018fclaims)
    {
        $this->f018fclaims = $f018fclaims;

        return $this;
    }

    /**
     * Get f018fclaims
     *
     * @return string
     */
    public function getF018fclaims()
    {
        return $this->f018fclaims;
    }

    /**
     * Set f018fattorneysFees
     *
     * @param string $f018fattorneysFees
     *
     * @return T018flawyer
     */
    public function setF018fattorneysFees($f018fattorneysFees)
    {
        $this->f018fattorneysFees = $f018fattorneysFees;

        return $this;
    }

    /**
     * Get f018fattorneysFees
     *
     * @return string
     */
    public function getF018fattorneysFees()
    {
        return $this->f018fattorneysFees;
    }

    /**
     * Set f018ftotal
     *
     * @param string $f018ftotal
     *
     * @return T018flawyer
     */
    public function setF018ftotal($f018ftotal)
    {
        $this->f018ftotal = $f018ftotal;

        return $this;
    }

    /**
     * Get f018ftotal
     *
     * @return string
     */
    public function getF018ftotal()
    {
        return $this->f018ftotal;
    }

    /**
     * Set f018fdateOfClaim
     *
     * @param string $f018fdateOfClaim
     *
     * @return T018flawyer
     */
    public function setF018fdateOfClaim($f018fdateOfClaim)
    {
        $this->f018fdateOfClaim = $f018fdateOfClaim;

        return $this;
    }

    /**
     * Get f018fdateOfClaim
     *
     * @return string
     */
    public function getF018fdateOfClaim()
    {
        return $this->f018fdateOfClaim;
    }

    /**
     * Set f018fdescription
     *
     * @param string $f018fdescription
     *
     * @return T018flawyer
     */
    public function setF018fdescription($f018fdescription)
    {
        $this->f018fdescription = $f018fdescription;

        return $this;
    }

    /**
     * Get f018fdescription
     *
     * @return string
     */
    public function getF018fdescription()
    {
        return $this->f018fdescription;
    }

    /**
     * Set f018fstatus
     *
     * @param string $f018fstatus
     *
     * @return T018flawyer
     */
    public function setF018fstatus($f018fstatus)
    {
        $this->f018fstatus = $f018fstatus;

        return $this;
    }

    /**
     * Get f018fstatus
     *
     * @return string
     */
    public function getF018fstatus()
    {
        return $this->f018fstatus;
    }

    /**
     * Set f018fcreatedBy
     *
     * @param string $f018fcreatedBy
     *
     * @return T018flawyer
     */
    public function setF018fcreatedBy($f018fcreatedBy)
    {
        $this->f018fcreatedBy = $f018fcreatedBy;

        return $this;
    }

    /**
     * Get f018fcreatedBy
     *
     * @return string
     */
    public function getF018fcreatedBy()
    {
        return $this->f018fcreatedBy;
    }

    /**
     * Set f018fupdatedBy
     *
     * @param string $f018fupdatedBy
     *
     * @return T018flawyer
     */
    public function setF018fupdatedBy($f018fupdatedBy)
    {
        $this->f018fupdatedBy = $f018fupdatedBy;

        return $this;
    }

    /**
     * Get f018fupdatedBy
     *
     * @return string
     */
    public function getF018fupdatedBy()
    {
        return $this->f018fupdatedBy;
    }

    /**
     * Set f018fcreateDtTm
     *
     * @param \DateTime $f018fcreateDtTm
     *
     * @return T018flawyer
     */
    public function setF018fcreateDtTm($f018fcreateDtTm)
    {
        $this->f018fcreateDtTm = $f018fcreateDtTm;

        return $this;
    }

    /**
     * Get f018fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF018fcreateDtTm()
    {
        return $this->f018fcreateDtTm;
    }

    /**
     * Set f018fupdateDtTm
     *
     * @param \DateTime $f018fupdateDtTm
     *
     * @return T018flawyer
     */
    public function setF018fupdateDtTm($f018fupdateDtTm)
    {
        $this->f018fupdateDtTm = $f018fupdateDtTm;

        return $this;
    }

    /**
     * Get f018fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF018fupdateDtTm()
    {
        return $this->f018fupdateDtTm;
    }
}
