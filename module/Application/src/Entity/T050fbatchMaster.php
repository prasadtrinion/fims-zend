<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T050fbatchMaster
 *
 * @ORM\Table(name="t050fbatch_master")
 * @ORM\Entity(repositoryClass="Application\Repository\BatchRepository")
 */
class T050fbatchMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f050fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f050fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fbatch_number", type="string",length=50, nullable=false)
     */
    private $f050fbatchNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fpayment_type", type="string",length=50, nullable=false)
     */
    private $f050fpaymentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fdate", type="datetime", nullable=false)
     */
    private $f050fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050freference_number", type="string",length=100, nullable=true)
     */
    private $f050freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fpayment_bank", type="integer", nullable=false)
     */
    private $f050fpaymentBank;


    /**
     * @var integer
     *
     * @ORM\Column(name="f050fstatus", type="integer", nullable=false)
     */
    private $f050fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f050fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f050fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f050fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f050fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f050fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f050fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f050fupdateDtTm;



    /**
     * Get f050fid
     *
     * @return integer
     */
    public function getF050fid()
    {
        return $this->f050fid;
    }

    /**
     * Set f050fbatchNumber
     *
     * @param string $f050fbatchNumber
     *
     * @return T050fbatchMaster
     */
    public function setF050fbatchNumber($f050fbatchNumber)
    {
        $this->f050fbatchNumber = $f050fbatchNumber;

        return $this;
    }

    /**
     * Get f050fbatchNumber
     *
     * @return string
     */
    public function getF050fbatchNumber()
    {
        return $this->f050fbatchNumber;
    }

    /**
     * Set f050fdate
     *
     * @param integer $f050fdate
     *
     * @return T050fbatchMaster
     */
    public function setF050fdate($f050fdate)
    {
        $this->f050fdate = $f050fdate;

        return $this;
    }

    /**
     * Get f050fdate
     *
     * @return integer
     */
    public function getF050fdate()
    {
        return $this->f050fdate;
    }

    /**
     * Set f050freferenceNumber
     *
     * @param integer $f050freferenceNumber
     *
     * @return T050fbatchMaster
     */
    public function setF050freferenceNumber($f050freferenceNumber)
    {
        $this->f050freferenceNumber = $f050freferenceNumber;

        return $this;
    }

    /**
     * Get f050freferenceNumber
     *
     * @return integer
     */
    public function getF050freferenceNumber()
    {
        return $this->f050freferenceNumber;
    }

    /**
     * Set f050fpaymentBank
     *
     * @param integer $f050fpaymentBank
     *
     * @return T050fbatchMaster
     */
    public function setF050fpaymentBank($f050fpaymentBank)
    {
        $this->f050fpaymentBank = $f050fpaymentBank;

        return $this;
    }

    /**
     * Get f050fpaymentBank
     *
     * @return integer
     */
    public function getF050fpaymentBank()
    {
        return $this->f050fpaymentBank;
    }


    /**
     * Get f050fstatus
     *
     * @return integer
     */
    public function getF050fstatus()
    {
        return $this->f050fstatus;
    }

     /**
     * Set f050fstatus
     *
     * @param integer $f050fstatus
     *
     * @return T050fbatchMaster
     */
    public function setF050fstatus($f050fstatus)
    {
        $this->f050fstatus = $f050fstatus;

        return $this;
    }

    /**
     * Set f050fcreatedBy
     *
     * @param integer $f050fcreatedBy
     *
     * @return T050fbatchMaster
     */
    public function setF050fcreatedBy($f050fcreatedBy)
    {
        $this->f050fcreatedBy = $f050fcreatedBy;

        return $this;
    }
   
    /**
     * Get f050fcreatedBy
     *
     * @return integer
     */
    public function getF050fcreatedBy()
    {
        return $this->f050fcreatedBy;
    }

    /**
     * Set f050fupdatedBy
     *
     * @param integer $f050fupdatedBy
     *
     * @return T050fbatchMaster
     */
    public function setF050fupdatedBy($f050fupdatedBy)
    {
        $this->f050fupdatedBy = $f050fupdatedBy;

        return $this;
    }

    /**
     * Get f050fupdatedBy
     *
     * @return integer
     */
    public function getF050fupdatedBy()
    {
        return $this->f050fupdatedBy;
    }

    /**
     * Set f050fcreateDtTm
     *
     * @param \DateTime $f050fcreateDtTm
     *
     * @return T050fbatchMaster
     */
    public function setF050fcreateDtTm($f050fcreateDtTm)
    {
        $this->f050fcreateDtTm = $f050fcreateDtTm;

        return $this;
    }

    /**
     * Get f050fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF050fcreateDtTm()
    {
        return $this->f050fcreateDtTm;
    }

    /**
     * Set f050fupdateDtTm
     *
     * @param \DateTime $f050fupdateDtTm
     *
     * @return T050fbatchMaster
     */
    public function setF050fupdateDtTm($f050fupdateDtTm)
    {
        $this->f050fupdateDtTm = $f050fupdateDtTm;

        return $this;
    }

    /**
     * Get f050fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF050fupdateDtTm()
    {
        return $this->f050fupdateDtTm;
    }
    /**
     * Set f050fpaymentType
     *
     * @param \DateTime $f050fpaymentType
     *
     * @return T050fbatchMaster
     */
    public function setF050fpaymentType($f050fpaymentType)
    {
        $this->f050fpaymentType = $f050fpaymentType;

        return $this;
    }

    /**
     * Get f050fpaymentType
     *
     * @return \DateTime
     */
    public function getF050fpaymentType()
    {
        return $this->f050fpaymentType;
    }
}

























