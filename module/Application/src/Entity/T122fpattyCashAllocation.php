<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T122fpattyCashAllocation
 *
 * @ORM\Table(name="T122fpatty_cash_allocation")
 * @ORM\Entity(repositoryClass="Application\Repository\PattyCashAllocationRepository")
 */
class T122fpattyCashAllocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f122fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f122fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f122fapprover", type="integer", nullable=true)
     */
    private $f122fapprover;

    /**
     * @var string
     *
     * @ORM\Column(name="f122fdepartment_code", type="string",length=50, nullable=true)
     */
    private $f122fdepartmentCode;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fallocation_year", type="integer", nullable=true)
     */
    private $f122fallocationYear;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmin_amount", type="integer", nullable=true)
     */
    private $f122fminAmount;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmax_amount", type="integer", nullable=true)
     */
    private $f122fmaxAmount;

    /** 
     * @var string
     *
     * @ORM\Column(name="f122fpayment_mode", type="integer", nullable=true)
     */
    private $f122fpaymentMode;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmin_reimbursement", type="integer", nullable=true)
     */
    private $f122fminReimbursement;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmax_reimbursement", type="integer", nullable=true)
     */
    private $f122fmaxReimbursement;


    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmin_reimbursement_percentage", type="integer", nullable=true)
     */
    private $f122fminReimbursementPercentage;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fmax_reimbursement_percentage", type="integer", nullable=true)
     */
    private $f122fmaxReimbursementPercentage;

    /** 
     * @var integer
     *
     * @ORM\Column(name="f122famount", type="integer", nullable=true)
     */
    private $f122famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f122freference_no", type="string",length=50, nullable=true)
     */
    private $f122freferenceNo;


    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fid_petty_cash", type="integer", nullable=true)
     */
    private $f122fidPettyCash;


    /** 
     * @var integer
     *
     * @ORM\Column(name="f122fstatus", type="integer", nullable=true)
     */
    private $f122fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f122fcreated_by", type="integer", nullable=true)
     */
    private $f122fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f122fupdated_by", type="integer", nullable=true)
     */
    private $f122fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f122fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f122fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f122fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f122fupdatedDtTm;



    /**
     * Get f122fid
     *
     * @return integer
     */
    public function getF122fid()
    {
        return $this->f122fid;
    }

    /**
     * Set f122fapprover
     *
     * @param \DateTime $f122fapprover
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fapprover($f122fapprover)
    {
        $this->f122fapprover = $f122fapprover;

        return $this;
    }

    /**
     * Get f122fapprover
     *
     * @return \DateTime
     */
    public function getF122fapprover()
    {
        return $this->f122fapprover;
    }

    /**
     * Set f122fdepartmentCode
     *
     * @param integer $f122fdepartmentCode
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fdepartmentCode($f122fdepartmentCode)
    {
        $this->f122fdepartmentCode = $f122fdepartmentCode;

        return $this;
    }

    /**
     * Get f122fdepartmentCode
     *
     * @return integer
     */
    public function getF122fdepartmentCode()
    {
        return $this->f122fdepartmentCode;
    }

    /**
     * Set f122fallocationYear
     *
     * @param integer $f122fallocationYear
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fallocationYear($f122fallocationYear)
    {
        $this->f122fallocationYear = $f122fallocationYear;

        return $this;
    }

    /**
     * Get f122fallocationYear
     *
     * @return integer
     */
    public function getF122fallocationYear()
    {
        return $this->f122fallocationYear;
    }

    /**
     * Set f122fminAmount
     *
     * @param integer $f122fminAmount
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fminAmount($f122fminAmount)
    {
        $this->f122fminAmount = $f122fminAmount;

        return $this;
    }

    /**
     * Get f122fminAmount
     *
     * @return integer
     */
    public function getF122fminAmount()
    {
        return $this->f122fminAmount;
    }

    /**
     * Set f122fmaxAmount
     *
     * @param integer $f122fmaxAmount
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fmaxAmount($f122fmaxAmount)
    {
        $this->f122fmaxAmount = $f122fmaxAmount;

        return $this;
    }

    /**
     * Get f122fmaxAmount
     *
     * @return integer
     */
    public function getF122fmaxAmount()
    {
        return $this->f122fmaxAmount;
    }

    /**
     * Set f122fpaymentMode
     *
     * @param integer $f122fpaymentMode
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fpaymentMode($f122fpaymentMode)
    {
        $this->f122fpaymentMode = $f122fpaymentMode;

        return $this;
    }

    /**
     * Get f122fpaymentMode
     *
     * @return integer
     */
    public function getF122fpaymentMode()
    {
        return $this->f122fpaymentMode;
    }

    /**
     * Set f122fminReimbursement
     *
     * @param integer $f122fminReimbursement
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fminReimbursement($f122fminReimbursement)
    {
        $this->f122fminReimbursement = $f122fminReimbursement;
        return $this;
    }

    /**
     * Get f122fminReimbursement
     *
     * @return integer
     */
    public function getF122fminReimbursement()
    {
        return $this->f122fminReimbursement;
    }

    /**
     * Set f122fmaxReimbursement
     *
     * @param integer $f122fmaxReimbursement
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fmaxReimbursement($f122fmaxReimbursement)
    {
        $this->f122fmaxReimbursement = $f122fmaxReimbursement;

        return $this;
    }

    /**
     * Get f122fmaxReimbursement
     *
     * @return integer
     */
    public function getF122fmaxReimbursement()
    {
        return $this->f122fmaxReimbursement;
    }

    /**
     * Set f122fstatus
     *
     * @param integer $f122fstatus
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fstatus($f122fstatus)
    {
        $this->f122fstatus = $f122fstatus;

        return $this;
    }

    /**
     * Get f122fstatus
     *
     * @return integer
     */
    public function getF122fstatus()
    {
        return $this->f122fstatus;
    }


    /**
     * Set f122fidPettyCash
     *
     * @param integer $f122fidPettyCash
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fidPettyCash($f122fidPettyCash)
    {
        $this->f122fidPettyCash = $f122fidPettyCash;

        return $this;
    }

    /**
     * Get f122fidPettyCash
     *
     * @return integer
     */
    public function getF122fidPettyCash()
    {
        return $this->f122fidPettyCash;
    }


    /**
     * Set f122famount
     *
     * @param integer $f122famount
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122famount($f122famount)
    {
        $this->f122famount = $f122famount;

        return $this;
    }

    /**
     * Get f122famount
     *
     * @return integer
     */
    public function getF122famount()
    {
        return $this->f122famount;
    }

    /**
     * Set f122fminReimbursementPercentage
     *
     * @param integer $f122fminReimbursementPercentage
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fminReimbursementPercentage($f122fminReimbursementPercentage)
    {
        $this->f122fminReimbursementPercentage = $f122fminReimbursementPercentage;

        return $this;
    }

    /**
     * Get f122fminReimbursementPercentage
     *
     * @return integer
     */
    public function getF122fminReimbursementPercentage()
    {
        return $this->f122fminReimbursementPercentage;
    }


    /**
     * Set f122fmaxReimbursementPercentage
     *
     * @param integer $f122fmaxReimbursementPercentage
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fmaxReimbursementPercentage($f122fmaxReimbursementPercentage)
    {
        $this->f122fmaxReimbursementPercentage = $f122fmaxReimbursementPercentage;

        return $this;
    }

    /**
     * Get f122fmaxReimbursementPercentage
     *
     * @return integer
     */
    public function getF122fmaxReimbursementPercentage()
    {
        return $this->f122fmaxReimbursementPercentage;
    }


    /**
     * Set f122freferenceNo
     *
     * @param integer $f122freferenceNo
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122freferenceNo($f122freferenceNo)
    {
        $this->f122freferenceNo = $f122freferenceNo;

        return $this;
    }

    /**
     * Get f122freferenceNo
     *
     * @return integer
     */
    public function getF122freferenceNo()
    {
        return $this->f122freferenceNo;
    }


    /**
     * Set f122fcreatedBy
     *
     * @param integer $f122fcreatedBy
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fcreatedBy($f122fcreatedBy)
    {
        $this->f122fcreatedBy = $f122fcreatedBy;

        return $this;
    }

    /**
     * Get f122fcreatedBy
     *
     * @return integer
     */
    public function getF122fcreatedBy()
    {
        return $this->f122fcreatedBy;
    }

    /**
     * Set f122fupdatedBy
     *
     * @param integer $f122fupdatedBy
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fupdatedBy($f122fupdatedBy)
    {
        $this->f122fupdatedBy = $f122fupdatedBy;

        return $this;
    }

    /**
     * Get f122fupdatedBy
     *
     * @return integer
     */
    public function getF122fupdatedBy()
    {
        return $this->f122fupdatedBy;
    }

    /**
     * Set f122fcreatedDtTm
     *
     * @param \DateTime $f122fcreatedDtTm
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fcreatedDtTm($f122fcreatedDtTm)
    {
        $this->f122fcreatedDtTm = $f122fcreatedDtTm;

        return $this;
    }

    /**
     * Get f122fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF122fcreatedDtTm()
    {
        return $this->f122fcreatedDtTm;
    }

    /**
     * Set f122fupdatedDtTm
     *
     * @param \DateTime $f122fupdatedDtTm
     *
     * @return T122fpattyCashAllocation
     */
    public function setF122fupdatedDtTm($f122fupdatedDtTm)
    {
        $this->f122fupdatedDtTm = $f122fupdatedDtTm;

        return $this;
    }

    /**
     * Get f122fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF122fupdatedDtTm()
    {
        return $this->f122fupdatedDtTm;
    }
}
