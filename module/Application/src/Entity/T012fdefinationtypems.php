<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T012fdefinationtypems
 *
 * @ORM\Table(name="t012fdefinationtypems")
 * @ORM\Entity(repositoryClass="Application\Repository\DefinationtypemsRepository")
 */
class T012fdefinationtypems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f012fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f012fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fdef_type_desc", type="string", length=45, nullable=false)
     */
    private $f012fdefTypeDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="f012fstatus", type="integer", nullable=false)
     */
    private $f012fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fdescription", type="string", length=30, nullable=true)
     */
    private $f012fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fdefinition_type_desc", type="string", length=30, nullable=true)
     */
    private $f012fdefinitionTypeDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fbahasa_indonesia", type="string", length=30, nullable=true)
     */
    private $f012fbahasaIndonesia;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fcreated_by", type="string", length=50, nullable=true)
     */
    private $f012fcreatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="f012fupdated_by", type="string", length=50, nullable=true)
     */
    private $f012fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f012fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f012fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f012fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f012fupdateDtTm;



    /**
     * Get f012fid
     *
     * @return integer
     */
    public function getF012fid()
    {
        return $this->f012fid;
    }

    /**
     * Set f012fdefTypeDesc
     *
     * @param string $f012fdefTypeDesc
     *
     * @return T012fdefinationtypems
     */
    public function setF012fdefTypeDesc($f012fdefTypeDesc)
    {
        $this->f012fdefTypeDesc = $f012fdefTypeDesc;

        return $this;
    }

    /**
     * Get f012fdefTypeDesc
     *
     * @return string
     */
    public function getF012fdefTypeDesc()
    {
        return $this->f012fdefTypeDesc;
    }

    /**
     * Set f012fstatus
     *
     * @param integer $f012fstatus
     *
     * @return T012fdefinationtypems
     */
    public function setF012fstatus($f012fstatus)
    {
        $this->f012fstatus = $f012fstatus;

        return $this;
    }

    /**
     * Get f012fstatus
     *
     * @return integer
     */
    public function getF012fstatus()
    {
        return $this->f012fstatus;
    }

    /**
     * Set f012fdescription
     *
     * @param string $f012fdescription
     *
     * @return T012fdefinationtypems
     */
    public function setF012fdescription($f012fdescription)
    {
        $this->f012fdescription = $f012fdescription;

        return $this;
    }

    /**
     * Get f012fdescription
     *
     * @return string
     */
    public function getF012fdescription()
    {
        return $this->f012fdescription;
    }

    /**
     * Set f012fdefinitionTypeDesc
     *
     * @param string $f012fdefinitionTypeDesc
     *
     * @return T012fdefinationtypems
     */
    public function setF012fdefinitionTypeDesc($f012fdefinitionTypeDesc)
    {
        $this->f012fdefinitionTypeDesc = $f012fdefinitionTypeDesc;

        return $this;
    }

    /**
     * Get f012fdefinitionTypeDesc
     *
     * @return string
     */
    public function getF012fdefinitionTypeDesc()
    {
        return $this->f012fdefinitionTypeDesc;
    }

    /**
     * Set f012fbahasaIndonesia
     *
     * @param string $f012fbahasaIndonesia
     *
     * @return T012fdefinationtypems
     */
    public function setF012fbahasaIndonesia($f012fbahasaIndonesia)
    {
        $this->f012fbahasaIndonesia = $f012fbahasaIndonesia;

        return $this;
    }

    /**
     * Get f012fbahasaIndonesia
     *
     * @return string
     */
    public function getF012fbahasaIndonesia()
    {
        return $this->f012fbahasaIndonesia;
    }

    /**
     * Set f012fcreatedBy
     *
     * @param string $f012fcreatedBy
     *
     * @return T012fdefinationtypems
     */
    public function setF012fcreatedBy($f012fcreatedBy)
    {
        $this->f012fcreatedBy = $f012fcreatedBy;

        return $this;
    }

    /**
     * Get f012fcreatedBy
     *
     * @return string
     */
    public function getF012fcreatedBy()
    {
        return $this->f012fcreatedBy;
    }

    /**
     * Set f012fupdatedBy
     *
     * @param string $f012fupdatedBy
     *
     * @return T012fdefinationtypems
     */
    public function setF012fupdatedBy($f012fupdatedBy)
    {
        $this->f012fupdatedBy = $f012fupdatedBy;

        return $this;
    }

    /**
     * Get f012fupdatedBy
     *
     * @return string
     */
    public function getF012fupdatedBy()
    {
        return $this->f012fupdatedBy;
    }

    /**
     * Set f012fcreateDtTm
     *
     * @param \DateTime $f012fcreateDtTm
     *
     * @return T012fdefinationtypems
     */
    public function setF012fcreateDtTm($f012fcreateDtTm)
    {
        $this->f012fcreateDtTm = $f012fcreateDtTm;

        return $this;
    }

    /**
     * Get f012fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF012fcreateDtTm()
    {
        return $this->f012fcreateDtTm;
    }

    /**
     * Set f012fupdateDtTm
     *
     * @param \DateTime $f012fupdateDtTm
     *
     * @return T012fdefinationtypems
     */
    public function setF012fupdateDtTm($f012fupdateDtTm)
    {
        $this->f012fupdateDtTm = $f012fupdateDtTm;

        return $this;
    }

    /**
     * Get f012fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF012fupdateDtTm()
    {
        return $this->f012fupdateDtTm;
    }
}
