<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T011fdefinationms
 *
 * @ORM\Table(name="t011fdefinationms")
 * @ORM\Entity(repositoryClass="Application\Repository\DefinationmsRepository")
 */
class T011fdefinationms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f011fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f011fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdefinition_code", type="string", length=100, nullable=false)
     */
    private $f011fdefinitionCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdefinition_desc", type="string", length=45, nullable=true)
     */
    private $f011fdefinitionDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fbahasa_indonesia", type="string", length=45, nullable=true)
     */
    private $f011fbahasaIndonesia;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdescription", type="string", length=100, nullable=true)
     */
    private $f011fdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="f011fdef_order", type="string", length=10, nullable=true)
     */
    private $f011fdefOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f011fstatus", type="integer", nullable=true)
     */
    private $f011fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fcreated_by", type="integer", length=50, nullable=true)
     */
    private $f011fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f011fupdated_by", type="integer", length=50, nullable=true)
     */
    private $f011fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f011fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f011fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f011fupdateDtTm;

     /**
     * @var integer
     *
     * @ORM\Column(name="f011fid_definitiontypems", type="integer", length=45, nullable=false)
     */
    private $f011fidDefinitiontypems;

    /**
     * Get f011fid
     *
     * @return integer
     */
    public function getF011fid()
    {
        return $this->f011fid;
    }

    /**
     * Set f011fdefinitionCode
     *
     * @param string $f011fdefinitionCode
     *
     * @return T011fdefinationms
     */
    public function setF011fdefinitionCode($f011fdefinitionCode)
    {
        $this->f011fdefinitionCode = $f011fdefinitionCode;

        return $this;
    }

    /**
     * Get f011fdefinitionCode
     *
     * @return string
     */
    public function getF011fdefinitionCode()
    {
        return $this->f011fdefinitionCode;
    }

    /**
     * Set f011fdefinitionDesc
     *
     * @param string $f011fdefinitionDesc
     *
     * @return T011fdefinationms
     */
    public function setF011fdefinitionDesc($f011fdefinitionDesc)
    {
        $this->f011fdefinitionDesc = $f011fdefinitionDesc;

        return $this;
    }

    /**
     * Get f011fdefinitionDesc
     *
     * @return string
     */
    public function getF011fdefinitionDesc()
    {
        return $this->f011fdefinitionDesc;
    }

    /**
     * Set f011fbahasaIndonesia
     *
     * @param string $f011fbahasaIndonesia
     *
     * @return T011fdefinationms
     */
    public function setF011fbahasaIndonesia($f011fbahasaIndonesia)
    {
        $this->f011fbahasaIndonesia = $f011fbahasaIndonesia;

        return $this;
    }

    /**
     * Get f011fbahasaIndonesia
     *
     * @return string
     */
    public function getF011fbahasaIndonesia()
    {
        return $this->f011fbahasaIndonesia;
    }

    /**
     * Set f011fdescription
     *
     * @param string $f011fdescription
     *
     * @return T011fdefinationms
     */
    public function setF011fdescription($f011fdescription)
    {
        $this->f011fdescription = $f011fdescription;

        return $this;
    }

    /**
     * Get f011fdescription
     *
     * @return string
     */
    public function getF011fdescription()
    {
        return $this->f011fdescription;
    }

    /**
     * Set f011fdefOrder
     *
     * @param string $f011fdefOrder
     *
     * @return T011fdefinationms
     */
    public function setF011fdefOrder($f011fdefOrder)
    {
        $this->f011fdefOrder = $f011fdefOrder;

        return $this;
    }

    /**
     * Get f011fdefOrder
     *
     * @return string
     */
    public function getF011fdefOrder()
    {
        return $this->f011fdefOrder;
    }

    /**
     * Set f011fstatus
     *
     * @param boolean $f011fstatus
     *
     * @return T011fdefinationms
     */
    public function setF011fstatus($f011fstatus)
    {
        $this->f011fstatus = $f011fstatus;

        return $this;
    }

    /**
     * Get f011fstatus
     *
     * @return boolean
     */
    public function getF011fstatus()
    {
        return $this->f011fstatus;
    }

    /**
     * Set f011fcreatedBy
     *
     * @param integer $f011fcreatedBy
     *
     * @return T011fdefinationms
     */
    public function setF011fcreatedBy($f011fcreatedBy)
    {
        $this->f011fcreatedBy = $f011fcreatedBy;

        return $this;
    }

    /**
     * Get f011fcreatedBy
     *
     * @return integer
     */
    public function getF011fcreatedBy()
    {
        return $this->f011fcreatedBy;
    }

    /**
     * Set f011fupdatedBy
     *
     * @param integer $f011fupdatedBy
     *
     * @return T011fdefinationms
     */
    public function setF011fupdatedBy($f011fupdatedBy)
    {
        $this->f011fupdatedBy = $f011fupdatedBy;

        return $this;
    }

    /**
     * Get f011fupdatedBy
     *
     * @return integer
     */
    public function getF011fupdatedBy()
    {
        return $this->f011fupdatedBy;
    }

    /**
     * Set f011fcreateDtTm
     *
     * @param \DateTime $f011fcreateDtTm
     *
     * @return T011fdefinationms
     */
    public function setF011fcreateDtTm($f011fcreateDtTm)
    {
        $this->f011fcreateDtTm = $f011fcreateDtTm;

        return $this;
    }

    /**
     * Get f011fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF011fcreateDtTm()
    {
        return $this->f011fcreateDtTm;
    }

    /**
     * Set f011fupdateDtTm
     *
     * @param \DateTime $f011fupdateDtTm
     *
     * @return T011fdefinationms
     */
    public function setF011fupdateDtTm($f011fupdateDtTm)
    {
        $this->f011fupdateDtTm = $f011fupdateDtTm;

        return $this;
    }

    /**
     * Get f011fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF011fupdateDtTm()
    {
        return $this->f011fupdateDtTm;
    }

    /**
     * Set f011fidDefinitiontypems
     *
     * @param integer $f011fidDefinationtypems
     *
     * @return T011fdefinationms
     */
    public function setF011fidDefinitiontypems($f011fidDefinitiontypems)
    {
        $this->f011fidDefinitiontypems = $f011fidDefinitiontypems;

        return $this;
    }

    /**
     * Get f011fidDefinitiontypems
     *
     * @return integer
     */
    public function getF011fidDefinitiontypems()
    {
        return $this->f011fidDefinitiontypems;
    }
}





