<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T017fjournal
 *
 * @ORM\Table(name="t017fjournal")
 * @ORM\Entity(repositoryClass="Application\Repository\JournalRepository")
 */
class T017fjournal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f017fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f017fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f017freference_number", type="string", length=50, nullable=false)
     */
    private $f017freferenceNumber;

    

    /**
     * @var string
     *
     * @ORM\Column(name="f017fdescription", type="string", length=100, nullable=true)
     */
    private $f017fdescription;


    /**
     * @var string
     *
     * @ORM\Column(name="f017fmodule", type="string", length=50, nullable=false)
     */
    private $f017fmodule;

    /**
     * @var string
     *
     * @ORM\Column(name="f017fapprover", type="integer", nullable=false)
     */
    private $f017fapprover;

        /**
     * @var integer
     *
     * @ORM\Column(name="f017fid_financial_year", type="integer", nullable=false)
     */
    private $f017fidFinancialYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="f017ftotal_amount", type="integer", nullable=false)
     */
    private $f017ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f017fapproved_status", type="integer", nullable=false)
     */
    private $f017fapprovedStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f017fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f017fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f017fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f017fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f017fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f017fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f017fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f017fupdateDtTm;



    /**
     * Get f017fid
     *
     * @return integer
     */
    public function getF017fid()
    {
        return $this->f017fid;
    }

    /**
     * Get f017freferenceNumber
     *
     * @return string
     */
    public function getF017freferenceNumber()
    {
        return $this->f017freferenceNumber;
    }

        /**
     * Set f017freferenceNumber
     *
     * @param string $f017freferenceNumber
     *
     * @return T017fjournal
     */
    public function setF017freferenceNumber($f017freferenceNumber)
    {
        $this->f017freferenceNumber = $f017freferenceNumber;

        return $this;
    }



    /**
     * Set f017fdescription
     *
     * @param string $f017fdescription
     *
     * @return T017fjournal
     */
    public function setF017fdescription($f017fdescription)
    {
        $this->f017fdescription = $f017fdescription;

        return $this;
    }

    /**
     * Get f017fdescription
     *
     * @return string
     */
    public function getF017fdescription()
    {
        return $this->f017fdescription;
    }


    /**
     * Set f017fmodule
     *
     * @param string $f017fmodule
     *
     * @return T017fjournal
     */
    public function setF017fmodule($f017fmodule)
    {
        $this->f017fmodule = $f017fmodule;

        return $this;
    }

    /**
     * Get f017fmodule
     *
     * @return string
     */
    public function getF017fmodule()
    {
        return $this->f017fmodule;
    }

    /**
     * Set f017fapprover
     *
     * @param string $f017fapprover
     *
     * @return T017fjournal
     */
    public function setF017fapprover($f017fapprover)
    {
        $this->f017fapprover = $f017fapprover;

        return $this;
    }

    /**
     * Get f017fapprover
     *
     * @return string
     */
    public function getF017fapprover()
    {
        return $this->f017fapprover;
    }

     /**
     * Set f017fidFinancialYear
     *
     * @param integer $f017fidFinancialYear
     *
     * @return T017fjournal
     */
    public function setF017fidFinancialYear($f017fidFinancialYear)
    {
        $this->f017fidFinancialYear = $f017fidFinancialYear;

        return $this;
    }

    /**
     * Get f017fapprover
     *
     * @return integer
     */
    public function getF017fidFinancialYear()
    {
        return $this->f017fidFinancialYear;
    }

    /**
     * Set f017ftotalAmount
     *
     * @param integer $f017ftotalAmount
     *
     * @return T017fjournal
     */
    public function setF017ftotalAmount($f017ftotalAmount)
    {
        $this->f017ftotalAmount = $f017ftotalAmount;

        return $this;
    }

    /**
     * Get f017ftotalAmount
     *
     * @return integer
     */
    public function getF017ftotalAmount()
    {
        return $this->f017ftotalAmount;
    }

    /**
     * Set f017fapprovedStatus
     *
     * @param integer $f017fapprovedStatus
     *
     * @return T017fjournal
     */
    public function setF017fapprovedStatus($f017fapprovedStatus)
    {
        $this->f017fapprovedStatus = $f017fapprovedStatus;

        return $this;
    }

    /**
     * Get f017fapprovedStatus
     *
     * @return integer
     */
    public function getF017fapprovedStatus()
    {
        return $this->f017fapprovedStatus;
    }

    /**
     * Set f017fcreatedBy
     *
     * @param string $f017fcreatedBy
     *
     * @return T017fjournal
     */
    public function setF017fcreatedBy($f017fcreatedBy)
    {
        $this->f017fcreatedBy = $f017fcreatedBy;

        return $this;
    }

    /**
     * Get f017fcreatedBy
     *
     * @return string
     */
    public function getF017fcreatedBy()
    {
        return $this->f017fcreatedBy;
    }

    /**
     * Set f017fupdatedBy
     *
     * @param string $f017fupdatedBy
     *
     * @return T017fjournal
     */
    public function setF017fupdatedBy($f017fupdatedBy)
    {
        $this->f017fupdatedBy = $f017fupdatedBy;

        return $this;
    }

    /**
     * Get f017fupdatedBy
     *
     * @return string
     */
    public function getF017fupdatedBy()
    {
        return $this->f017fupdatedBy;
    }

    /**
     * Set f017fcreateDtTm
     *
     * @param \DateTime $f017fcreateDtTm
     *
     * @return T017fjournal
     */
    public function setF017fcreateDtTm($f017fcreateDtTm)
    {
        $this->f017fcreateDtTm = $f017fcreateDtTm;

        return $this;
    }

    /**
     * Get f017fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF017fcreateDtTm()
    {
        return $this->f017fcreateDtTm;
    }

    /**
     * Set f017fupdateDtTm
     *
     * @param \DateTime $f017fupdateDtTm
     *
     * @return T017fjournal
     */
    public function setF017fupdateDtTm($f017fupdateDtTm)
    {
        $this->f017fupdateDtTm = $f017fupdateDtTm;

        return $this;
    }

    /**
     * Get f017fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF017fupdateDtTm()
    {
        return $this->f017fupdateDtTm;
    }
}