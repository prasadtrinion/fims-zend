<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T054fbudgetDepartAllocation
 *
 * @ORM\Table(name="t054fbudget_depart_allocation")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetDepartAllocationRepository")
 */
class T054fbudgetDepartAllocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f054fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid_budget_activity", type="integer", nullable=false)
     */
    private $f054fidBudgetActivity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fopening_balance", type="integer",scale = 2, nullable=false)
     */
    private $f054fopeningBalance;

    /**
     * @var float
     *
     * @ORM\Column(name="f054fcr_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $f054fcrAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f054fdr_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $f054fdrAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f054fbalance_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $f054fbalanceAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fstatus", type="integer", nullable=false)
     */
    private $f054fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fcreated_by", type="integer", nullable=false)
     */
    private $f054fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fupdated_by", type="integer", nullable=false)
     */
    private $f054fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fupdatedDtTm;



    /**
     * Get f054fid
     *
     * @return integer
     */
    public function getF054fid()
    {
        return $this->f054fid;
    }

    /**
     * Set f054fidBudgetActivity
     *
     * @param integer $f054fidBudgetActivity
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fidBudgetActivity($f054fidBudgetActivity)
    {
        $this->f054fidBudgetActivity = $f054fidBudgetActivity;

        return $this;
    }

    /**
     * Get f054fidBudgetActivity
     *
     * @return integer
     */
    public function getF054fidBudgetActivity()
    {
        return $this->f054fidBudgetActivity;
    }

    /**
     * Set f054fopeningBalance
     *
     * @param integer $f054fopeningBalance
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fopeningBalance($f054fopeningBalance)
    {
        $this->f054fopeningBalance = $f054fopeningBalance;

        return $this;
    }

    /**
     * Get f054fopeningBalance
     *
     * @return integer
     */
    public function getF054fopeningBalance()
    {
        return $this->f054fopeningBalance;
    }

    /**
     * Set f054fcrAmount
     *
     * @param float $f054fcrAmount
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fcrAmount($f054fcrAmount)
    {
        $this->f054fcrAmount = $f054fcrAmount;

        return $this;
    }

    /**
     * Get f054fcrAmount
     *
     * @return float
     */
    public function getF054fcrAmount()
    {
        return $this->f054fcrAmount;
    }

    /**
     * Set f054fdrAmount
     *
     * @param float $f054fdrAmount
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fdrAmount($f054fdrAmount)
    {
        $this->f054fdrAmount = $f054fdrAmount;

        return $this;
    }

    /**
     * Get f054fdrAmount
     *
     * @return float
     */
    public function getF054fdrAmount()
    {
        return $this->f054fdrAmount;
    }

    /**
     * Set f054fbalanceAmount
     *
     * @param float $f054fbalanceAmount
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fbalanceAmount($f054fbalanceAmount)
    {
        $this->f054fbalanceAmount = $f054fbalanceAmount;

        return $this;
    }

    /**
     * Get f054fbalanceAmount
     *
     * @return float
     */
    public function getF054fbalanceAmount()
    {
        return $this->f054fbalanceAmount;
    }

    /**
     * Set f054fstatus
     *
     * @param integer $f054fstatus
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fstatus($f054fstatus)
    {
        $this->f054fstatus = $f054fstatus;

        return $this;
    }

    /**
     * Get f054fstatus
     *
     * @return integer
     */
    public function getF054fstatus()
    {
        return $this->f054fstatus;
    }

    /**
     * Set f054fcreatedBy
     *
     * @param integer $f054fcreatedBy
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fcreatedBy($f054fcreatedBy)
    {
        $this->f054fcreatedBy = $f054fcreatedBy;

        return $this;
    }

    /**
     * Get f054fcreatedBy
     *
     * @return integer
     */
    public function getF054fcreatedBy()
    {
        return $this->f054fcreatedBy;
    }

    /**
     * Set f054fupdatedBy
     *
     * @param integer $f054fupdatedBy
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fupdatedBy($f054fupdatedBy)
    {
        $this->f054fupdatedBy = $f054fupdatedBy;

        return $this;
    }

    /**
     * Get f054fupdatedBy
     *
     * @return integer
     */
    public function getF054fupdatedBy()
    {
        return $this->f054fupdatedBy;
    }

    /**
     * Set f054fcreatedDtTm
     *
     * @param \DateTime $f054fcreatedDtTm
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fcreatedDtTm($f054fcreatedDtTm)
    {
        $this->f054fcreatedDtTm = $f054fcreatedDtTm;

        return $this;
    }

    /**
     * Get f054fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fcreatedDtTm()
    {
        return $this->f054fcreatedDtTm;
    }

    /**
     * Set f054fupdatedDtTm
     *
     * @param \DateTime $f054fupdatedDtTm
     *
     * @return T054fbudgetDepartAllocation
     */
    public function setF054fupdatedDtTm($f054fupdatedDtTm)
    {
        $this->f054fupdatedDtTm = $f054fupdatedDtTm;

        return $this;
    }

    /**
     * Get f054fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fupdatedDtTm()
    {
        return $this->f054fupdatedDtTm;
    }
}
