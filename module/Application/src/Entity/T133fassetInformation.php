<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T133fassetInformation
 *
 * @ORM\Table(name="t133fasset_information")
 * @ORM\Entity(repositoryClass="Application\Repository\DuplicateAssetInformationRepository")
 */
class T133fassetInformation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f133fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fasset_code", type="string", length=225, nullable=true)
     */
    private $f133fassetCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fasset_description", type="string", length=50, nullable=true)
     */
    private $f133fassetDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid_department", type="string",length=20, nullable=true)
     */
    private $f133fidDepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid_asset_category", type="integer", nullable=true)
     */
    private $f133fidAssetCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f133f_acquisition_date", type="datetime", nullable=true)
     */
    private $f133fAcquisitionDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f133forder_number", type="string", length=20, nullable=true)
     */
    private $f133forderNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fstore_code", type="string", length=20, nullable=true)
     */
    private $f133fstoreCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f133faccount_code", type="string", length=20, nullable=true)
     */
    private $f133faccountCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f133fapprove_date", type="datetime", nullable=true)
     */
    private $f133fapproveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f133fexpire_date", type="datetime", nullable=true)
     */
    private $f133fexpireDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid_asset_type", type="integer", nullable=true)
     */
    private $f133fidAssetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133finstall_cost",  type="integer", nullable=true)
     */
    private $f133finstallCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133faccum_depr_cost",  type="integer", nullable=true)
     */
    private $f133faccumDeprCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fnet_value",  type="integer", nullable=true)
     */
    private $f133fnetValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fyear_depr_cost",  type="integer", nullable=true)
     */
    private $f133fyearDeprCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fresidual_value",  type="integer", nullable=true)
     */
    private $f133fresidualValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fstatus", type="integer", nullable=true)
     */
    private $f133fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcreated_by", type="integer", nullable=true)
     */
    private $f133fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fupdated_by", type="integer", nullable=true)
     */
    private $f133fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f133fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f133fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f133fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f133fupdatedDtTm;

  /**
     * @var integer
     *
     * @ORM\Column(name="f133fasset_owner", type="string",length=20, nullable=true)
     */
    private $f133fassetOwner;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fbar_code", type="string", length=20, nullable=true)
     */
    private $f133fbarCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fserial_number", type="string", length=20, nullable=true)
     */
    private $f133fserialNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fbrand_name", type="string", length=20, nullable=true)
     */
    private $f133fbrandName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcurrent_building", type="integer", nullable=true)
     */
    private $f133fcurrentBuilding;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcurrent_room", type="string",length=60, nullable=true)
     */
    private $f133fcurrentRoom;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcurrent_block", type="string", length=60, nullable=true)
     */
    private $f133fcurrentBlock;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcurrent_campus", type="string", length=60, nullable=true)
     */
    private $f133fcurrentCampus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fsub_category", type="integer", nullable=true)
     */
    private $f133fsubCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fpo_number", type="string",length=50, nullable=true)
     */
    private $f133fpoNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fgrn_number", type="integer", nullable=true)
     */
    private $f133fgrnNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f133fso_code", type="string",length=50, nullable=true)
     */
    private $f133fsoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f133freferrer", type="string",length=50, nullable=true)
     */
    private $f133freferrer;

     /**
     * @var string
     *
     * @ORM\Column(name="f133fitem_name", type="string",length=50, nullable=true)
     */
    private $f133fitemName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid_item", type="integer", nullable=true)
     */
    private $f133fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fid_grn_detail", type="integer", nullable=true)
     */
    private $f133fidGrnDetail;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fapproved_date", type="datetime", nullable=true)
     */
    private $f133fapprovedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f133fcategory_type", type="string", length=50, nullable=true)
     */
    private $f133fcategoryType;


    /**
     * @var integer
     *
     * @ORM\Column(name="f133fstaff_incharge", type="integer", nullable=true)
     */
    private $f133fstaffIncharge;




    


    /**
     * Get f133fid
     *
     * @return integer
     */
    public function getF133fid()
    {
        return $this->f133fid;
    }

    /**
     * Set f133fassetCode
     *
     * @param string $f133fassetCode
     *
     * @return T133fassetInformation
     */
    public function setF133fassetCode($f133fassetCode)
    {
        $this->f133fassetCode = $f133fassetCode;

        return $this;
    }

    /**
     * Get f133fassetCode
     *
     * @return string
     */
    public function getF133fassetCode()
    {
        return $this->f133fassetCode;
    }

    /**
     * Set f133fassetDescription
     *
     * @param string $f133fassetDescription
     *
     * @return T133fassetInformation
     */
    public function setF133fassetDescription($f133fassetDescription)
    {
        $this->f133fassetDescription = $f133fassetDescription;

        return $this;
    }

    /**
     * Get f133fassetDescription
     *
     * @return string
     */
    public function getF133fassetDescription()
    {
        return $this->f133fassetDescription;
    }

    /**
     * Set f133fidDepartment
     *
     * @param integer $f133fidDepartment
     *
     * @return T133fassetInformation
     */
    public function setF133fidDepartment($f133fidDepartment)
    {
        $this->f133fidDepartment = $f133fidDepartment;

        return $this;
    }

    /**
     * Get f133fidDepartment
     *
     * @return integer
     */
    public function getF133fidDepartment()
    {
        return $this->f133fidDepartment;
    }

    /**
     * Set f133fidAssetCategory
     *
     * @param integer $f133fidAssetCategory
     *
     * @return T133fassetInformation
     */
    public function setF133fidAssetCategory($f133fidAssetCategory)
    {
        $this->f133fidAssetCategory = $f133fidAssetCategory;

        return $this;
    }

    /**
     * Get f133fidAssetCategory
     *
     * @return integer
     */
    public function getF133fidAssetCategory()
    {
        return $this->f133fidAssetCategory;
    }

    /**
     * Set f133fAcquisitionDate
     *
     * @param \DateTime $f133fAcquisitionDate
     *
     * @return T133fassetInformation
     */
    public function setF133fAcquisitionDate($f133fAcquisitionDate)
    {
        $this->f133fAcquisitionDate = $f133fAcquisitionDate;

        return $this;
    }

    /**
     * Get f133fAcquisitionDate
     *
     * @return \DateTime
     */
    public function getF133fAcquisitionDate()
    {
        return $this->f133fAcquisitionDate;
    }

    /**
     * Set f133forderNumber
     *
     * @param string $f133forderNumber
     *
     * @return T133fassetInformation
     */
    public function setF133forderNumber($f133forderNumber)
    {
        $this->f133forderNumber = $f133forderNumber;

        return $this;
    }

    /**
     * Get f133forderNumber
     *
     * @return string
     */
    public function getF133forderNumber()
    {
        return $this->f133forderNumber;
    }

    /**
     * Set f133fstoreCode
     *
     * @param string $f133fstoreCode
     *
     * @return T133fassetInformation
     */
    public function setF133fstoreCode($f133fstoreCode)
    {
        $this->f133fstoreCode = $f133fstoreCode;

        return $this;
    }

    /**
     * Get f133fstoreCode
     *
     * @return string
     */
    public function getF133fstoreCode()
    {
        return $this->f133fstoreCode;
    }

    /**
     * Set f133faccountCode
     *
     * @param string $f133faccountCode
     *
     * @return T133fassetInformation
     */
    public function setF133faccountCode($f133faccountCode)
    {
        $this->f133faccountCode = $f133faccountCode;

        return $this;
    }

    /**
     * Get f133faccountCode
     *
     * @return string
     */
    public function getF133faccountCode()
    {
        return $this->f133faccountCode;
    }

    /**
     * Set f133fapproveDate
     *
     * @param \DateTime $f133fapproveDate
     *
     * @return T133fassetInformation
     */
    public function setF133fapproveDate($f133fapproveDate)
    {
        $this->f133fapproveDate = $f133fapproveDate;

        return $this;
    }

    /**
     * Get f133fapproveDate
     *
     * @return \DateTime
     */
    public function getF133fapproveDate()
    {
        return $this->f133fapproveDate;
    }

    /**
     * Set f133fexpireDate
     *
     * @param \DateTime $f133fexpireDate
     *
     * @return T133fassetInformation
     */
    public function setF133fexpireDate($f133fexpireDate)
    {
        $this->f133fexpireDate = $f133fexpireDate;

        return $this;
    }

    /**
     * Get f133fexpireDate
     *
     * @return \DateTime
     */
    public function getF133fexpireDate()
    {
        return $this->f133fexpireDate;
    }

    /**
     * Set f133fidAssetType
     *
     * @param integer $f133fidAssetType
     *
     * @return T133fassetInformation
     */
    public function setF133fidAssetType($f133fidAssetType)
    {
        $this->f133fidAssetType = $f133fidAssetType;

        return $this;
    }

    /**
     * Get f133fidAssetType
     *
     * @return integer
     */
    public function getF133fidAssetType()
    {
        return $this->f133fidAssetType;
    }

    /**
     * Set f133finstallCost
     *
     * @param integer $f133finstallCost
     *
     * @return T133fassetInformation
     */
    public function setF133finstallCost($f133finstallCost)
    {
        $this->f133finstallCost = $f133finstallCost;

        return $this;
    }

    /**
     * Get f133finstallCost
     *
     * @return integer
     */
    public function getF133finstallCost()
    {
        return $this->f133finstallCost;
    }

    /**
     * Set f133faccumDeprCost
     *
     * @param integer $f133faccumDeprCost
     *
     * @return T133fassetInformation
     */
    public function setF133faccumDeprCost($f133faccumDeprCost)
    {
        $this->f133faccumDeprCost = $f133faccumDeprCost;

        return $this;
    }

    /**
     * Get f133faccumDeprCost
     *
     * @return integer
     */
    public function getF133faccumDeprCost()
    {
        return $this->f133faccumDeprCost;
    }

    /**
     * Set f133fnetValue
     *
     * @param integer $f133fnetValue
     *
     * @return T133fassetInformation
     */
    public function setF133fnetValue($f133fnetValue)
    {
        $this->f133fnetValue = $f133fnetValue;

        return $this;
    }

    /**
     * Get f133fnetValue
     *
     * @return integer
     */
    public function getF133fnetValue()
    {
        return $this->f133fnetValue;
    }

    /**
     * Set f133fyearDeprCost
     *
     * @param integer $f133fyearDeprCost
     *
     * @return T133fassetInformation
     */
    public function setF133fyearDeprCost($f133fyearDeprCost)
    {
        $this->f133fyearDeprCost = $f133fyearDeprCost;

        return $this;
    }

    /**
     * Get f133fyearDeprCost
     *
     * @return integer
     */
    public function getF133fyearDeprCost()
    {
        return $this->f133fyearDeprCost;
    }

    /**
     * Set f133fresidualValue
     *
     * @param integer $f133fresidualValue
     *
     * @return T133fassetInformation
     */
    public function setF133fresidualValue($f133fresidualValue)
    {
        $this->f133fresidualValue = $f133fresidualValue;

        return $this;
    }

    /**
     * Get f133fresidualValue
     *
     * @return integer
     */
    public function getF133fresidualValue()
    {
        return $this->f133fresidualValue;
    }

    /**
     * Set f133fstatus
     *
     * @param integer $f133fstatus
     *
     * @return T133fassetInformation
     */
    public function setF133fstatus($f133fstatus)
    {
        $this->f133fstatus = $f133fstatus;

        return $this;
    }

    /**
     * Get f133fstatus
     *
     * @return integer
     */
    public function getF133fstatus()
    {
        return $this->f133fstatus;
    }

    /**
     * Set f133fcreatedBy
     *
     * @param integer $f133fcreatedBy
     *
     * @return T133fassetInformation
     */
    public function setF133fcreatedBy($f133fcreatedBy)
    {
        $this->f133fcreatedBy = $f133fcreatedBy;

        return $this;
    }

    /**
     * Get f133fcreatedBy
     *
     * @return integer
     */
    public function getF133fcreatedBy()
    {
        return $this->f133fcreatedBy;
    }

    /**
     * Set f133fupdatedBy
     *
     * @param integer $f133fupdatedBy
     *
     * @return T133fassetInformation
     */
    public function setF133fupdatedBy($f133fupdatedBy)
    {
        $this->f133fupdatedBy = $f133fupdatedBy;

        return $this;
    }

    /**
     * Get f133fupdatedBy
     *
     * @return integer
     */
    public function getF133fupdatedBy()
    {
        return $this->f133fupdatedBy;
    }

    /**
     * Set f133fcreatedDtTm
     *
     * @param \DateTime $f133fcreatedDtTm
     *
     * @return T133fassetInformation
     */
    public function setF133fcreatedDtTm($f133fcreatedDtTm)
    {
        $this->f133fcreatedDtTm = $f133fcreatedDtTm;

        return $this;
    }

    /**
     * Get f133fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF133fcreatedDtTm()
    {
        return $this->f133fcreatedDtTm;
    }

    /**
     * Set f133fupdatedDtTm
     *
     * @param \DateTime $f133fupdatedDtTm
     *
     * @return T133fassetInformation
     */
    public function setF133fupdatedDtTm($f133fupdatedDtTm)
    {
        $this->f133fupdatedDtTm = $f133fupdatedDtTm;

        return $this;
    }

    /**
     * Get f133fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF133fupdatedDtTm()
    {
        return $this->f133fupdatedDtTm;
    }
    /**
     * Set f133fassetOwner
     *
     * @param integer $f133fassetOwner
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fassetOwner($f133fassetOwner)
    {
        $this->f133fassetOwner = $f133fassetOwner;

        return $this;
    }

    /**
     * Get f133fassetOwner
     *
     * @return integer
     */
    public function getF133fassetOwner()
    {
        return $this->f133fassetOwner;
    }

    /**
     * Set f133fbarCode
     *
     * @param string $f133fbarCode
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fbarCode($f133fbarCode)
    {
        $this->f133fbarCode = $f133fbarCode;

        return $this;
    }

    /**
     * Get f133fbarCode
     *
     * @return string
     */
    public function getF133fbarCode()
    {
        return $this->f133fbarCode;
    }

    /**
     * Set f133fserialNumber
     *
     * @param string $f133fserialNumber
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fserialNumber($f133fserialNumber)
    {
        $this->f133fserialNumber = $f133fserialNumber;

        return $this;
    }

    /**
     * Get f133fserialNumber
     *
     * @return string
     */
    public function getF133fserialNumber()
    {
        return $this->f133fserialNumber;
    }

    /**
     * Set f133fbrandName
     *
     * @param string $f133fbrandName
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fbrandName($f133fbrandName)
    {
        $this->f133fbrandName = $f133fbrandName;

        return $this;
    }

    /**
     * Get f133fbrandName
     *
     * @return string
     */
    public function getF133fbrandName()
    {
        return $this->f133fbrandName;
    }

    /**
     * Set f133fcurrentBuilding
     *
     * @param integer $f133fcurrentBuilding
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fcurrentBuilding($f133fcurrentBuilding)
    {
        $this->f133fcurrentBuilding = $f133fcurrentBuilding;

        return $this;
    }

    /**
     * Get f133fcurrentBuilding
     *
     * @return integer
     */
    public function getF133fcurrentBuilding()
    {
        return $this->f133fcurrentBuilding;
    }

    /**
     * Set f133fcurrentRoom
     *
     * @param integer $f133fcurrentRoom
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fcurrentRoom($f133fcurrentRoom)
    {
        $this->f133fcurrentRoom = $f133fcurrentRoom;

        return $this;
    }

    /**
     * Get f133fcurrentRoom
     *
     * @return integer
     */
    public function getF133fcurrentRoom()
    {
        return $this->f133fcurrentRoom;
    }

     /**
     * Set f133fcurrentBlock
     *
     * @param integer $f133fcurrentBlock
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fcurrentBlock($f133fcurrentBlock)
    {
        $this->f133fcurrentBlock = $f133fcurrentBlock;

        return $this;
    }

    /**
     * Get f133fcurrentBlock
     *
     * @return integer
     */
    public function getF133fcurrentBlock()
    {
        return $this->f133fcurrentBlock;
    }

    /**
     * Set f133fcurrentCampus
     *
     * @param integer $f133fcurrentCampus
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fcurrentCampus($f133fcurrentCampus)
    {
        $this->f133fcurrentCampus = $f133fcurrentCampus;

        return $this;
    }

    /**
     * Get f133fcurrentCampus
     *
     * @return integer
     */
    public function getF133fcurrentCampus()
    {
        return $this->f133fcurrentCampus;
    }

    /**
     * Set f133fsubCategory
     *
     * @param integer $f133fsubCategory
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fsubCategory($f133fsubCategory)
    {
        $this->f133fsubCategory = $f133fsubCategory;

        return $this;
    }

    /**
     * Get f133fsubCategory
     *
     * @return integer
     */
    public function getF133fsubCategory()
    {
        return $this->f133fsubCategory;
    }
   
    /**
     * Set f133fpoNumber
     *
     * @param string $f133fpoNumber
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fpoNumber($f133fpoNumber)
    {
        $this->f133fpoNumber = $f133fpoNumber;

        return $this;
    }

    /**
     * Get f133fpoNumber
     *
     * @return string
     */
    public function getF133fpoNumber()
    {
        return $this->f133fpoNumber;
    }
    /**
     * Set f133fgrnNumber
     *
     * @param string $f133fgrnNumber
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fgrnNumber($f133fgrnNumber)
    {
        $this->f133fgrnNumber = $f133fgrnNumber;

        return $this;
    }

    /**
     * Get f133fgrnNumber
     *
     * @return string
     */
    public function getF133fgrnNumber()
    {
        return $this->f133fgrnNumber;
    }
    /**
     * Set f133fsoCode
     *
     * @param string $f133fsoCode
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fsoCode($f133fsoCode)
    {
        $this->f133fsoCode = $f133fsoCode;

        return $this;
    }

    /**
     * Get f133fsoCode
     *
     * @return string
     */
    public function getF133fsoCode()
    {
        return $this->f133fsoCode;
    }
    
    /**
     * Set f133freferrer
     *
     * @param string $f133freferrer
     *
     * @return T133fassetInfoDetails
     */
    public function setF133freferrer($f133freferrer)
    {
        $this->f133freferrer = $f133freferrer;

        return $this;
    }

    /**
     * Get f133freferrer
     *
     * @return string
     */
    public function getF133freferrer()
    {
        return $this->f133freferrer;
    }

    /**
     * Set f133fitemName
     *
     * @param string $f133fitemName
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fitemName($f133fitemName)
    {
        $this->f133fitemName = $f133fitemName;

        return $this;
    }

    /**
     * Get f133fitemName
     *
     * @return string
     */
    public function getF133fitemName()
    {
        return $this->f133fitemName;
    }

     /**
     * Set f133fidItem
     *
     * @param integer $f133fidItem
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fidItem($f133fidItem)
    {
        $this->f133fidItem = $f133fidItem;

        return $this;
    }

    /**
     * Get f133fidItem
     *
     * @return integer
     */
    public function getF133fidItem()
    {
        return $this->f133fidItem;
    }

    /**
     * Set f133fidGrnDetail
     *
     * @param integer $f133fidGrnDetail
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fidGrnDetail($f133fidGrnDetail)
    {
        $this->f133fidGrnDetail = $f133fidGrnDetail;

        return $this;
    }

    /**
     * Get f133fidGrnDetail
     *
     * @return integer
     */
    public function getF133fidGrnDetail()
    {
        return $this->f133fidGrnDetail;
    }


    /**
     * Set f133fapprovedDate
     *
     * @param integer $f133fapprovedDate
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fapprovedDate($f133fapprovedDate)
    {
        $this->f133fapprovedDate = $f133fapprovedDate;

        return $this;
    }

    /**
     * Get f133fapprovedDate
     *
     * @return integer
     */
    public function getF133fapprovedDate()
    {
        return $this->f133fapprovedDate;
    }

    /**
     * Set f133fcategoryType
     *
     * @param integer $f133fcategoryType
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fcategoryType($f133fcategoryType)
    {
        $this->f133fcategoryType = $f133fcategoryType;

        return $this;
    }

    /**
     * Get f133fcategoryType
     *
     * @return integer
     */
    public function getF133fcategoryType()
    {
        return $this->f133fcategoryType;
    }

    /**
     * Set f133fstaffIncharge
     *
     * @param integer $f133fstaffIncharge
     *
     * @return T133fassetInfoDetails
     */
    public function setF133fstaffIncharge($f133fstaffIncharge)
    {
        $this->f133fstaffIncharge = $f133fstaffIncharge;

        return $this;
    }

    /**
     * Get f133fstaffIncharge
     *
     * @return integer
     */
    public function getF133fstaffIncharge()
    {
        return $this->f133fstaffIncharge;
    }

    
}
