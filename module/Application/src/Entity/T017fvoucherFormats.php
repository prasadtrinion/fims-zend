<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T017fvoucherFormats
 *
 * @ORM\Table(name="t017fvoucher_formats", indexes={@ORM\Index(name="f017fupdated_by", columns={"f017fupdated_by"}), @ORM\Index(name="f017fid_voucher_type", columns={"f017fid_voucher_type"}), @ORM\Index(name="f017fparent_voucher_type", columns={"f017fparent_voucher_type"})})
 * @ORM\Entity
 */
class T017fvoucherFormats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f017fid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f017fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f017fvoucher_name", type="string", length=50, nullable=false)
     */
    private $f017fvoucherName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f017feffective_date", type="datetime", nullable=false)
     */
    private $f017feffectiveDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f017fauto_numbering", type="boolean", nullable=false)
     */
    private $f017fautoNumbering;

    /**
     * @var string
     *
     * @ORM\Column(name="f017fprefix", type="string", length=10, nullable=true)
     */
    private $f017fprefix;

    /**
     * @var string
     *
     * @ORM\Column(name="f017fsuffix", type="string", length=10, nullable=true)
     */
    private $f017fsuffix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f017fauto_print_after_saving", type="boolean", nullable=false)
     */
    private $f017fautoPrintAfterSaving;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f017fauto_narration", type="boolean", nullable=false)
     */
    private $f017fautoNarration;

    /**
     * @var string
     *
     * @ORM\Column(name="f017fauto_narration_description", type="string", length=255, nullable=true)
     */
    private $f017fautoNarrationDescription;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f017freset_number_every_fy", type="boolean", nullable=false)
     */
    private $f017fresetNumberEveryFy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f017fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f017fupdatedDtTm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f017fstatus", type="boolean", nullable=false)
     */
    private $f017fstatus = '1';

    /**
     * @var \Application\Entity\T014fuser
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T014fuser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f017fupdated_by", referencedColumnName="f014fid")
     * })
     */
    private $f017fupdatedBy;

    /**
     * @var \Application\Entity\T011fdefinationms
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T011fdefinationms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f017fid_voucher_type", referencedColumnName="f011fid")
     * })
     */
    private $f017fidVoucherType;

    /**
     * @var \Application\Entity\T011fdefinationms
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T011fdefinationms")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f017fparent_voucher_type", referencedColumnName="f011fid")
     * })
     */
    private $f017fparentVoucherType;



    /**
     * Get f017fid
     *
     * @return integer
     */
    public function getF017fid()
    {
        return $this->f017fid;
    }

    /**
     * Set f017fvoucherName
     *
     * @param string $f017fvoucherName
     *
     * @return T017fvoucherFormats
     */
    public function setF017fvoucherName($f017fvoucherName)
    {
        $this->f017fvoucherName = $f017fvoucherName;

        return $this;
    }

    /**
     * Get f017fvoucherName
     *
     * @return string
     */
    public function getF017fvoucherName()
    {
        return $this->f017fvoucherName;
    }

    /**
     * Set f017feffectiveDate
     *
     * @param \DateTime $f017feffectiveDate
     *
     * @return T017fvoucherFormats
     */
    public function setF017feffectiveDate($f017feffectiveDate)
    {
        $this->f017feffectiveDate = $f017feffectiveDate;

        return $this;
    }

    /**
     * Get f017feffectiveDate
     *
     * @return \DateTime
     */
    public function getF017feffectiveDate()
    {
        return $this->f017feffectiveDate;
    }

    /**
     * Set f017fautoNumbering
     *
     * @param boolean $f017fautoNumbering
     *
     * @return T017fvoucherFormats
     */
    public function setF017fautoNumbering($f017fautoNumbering)
    {
        $this->f017fautoNumbering = $f017fautoNumbering;

        return $this;
    }

    /**
     * Get f017fautoNumbering
     *
     * @return boolean
     */
    public function getF017fautoNumbering()
    {
        return $this->f017fautoNumbering;
    }

    /**
     * Set f017fprefix
     *
     * @param string $f017fprefix
     *
     * @return T017fvoucherFormats
     */
    public function setF017fprefix($f017fprefix)
    {
        $this->f017fprefix = $f017fprefix;

        return $this;
    }

    /**
     * Get f017fprefix
     *
     * @return string
     */
    public function getF017fprefix()
    {
        return $this->f017fprefix;
    }

    /**
     * Set f017fsuffix
     *
     * @param string $f017fsuffix
     *
     * @return T017fvoucherFormats
     */
    public function setF017fsuffix($f017fsuffix)
    {
        $this->f017fsuffix = $f017fsuffix;

        return $this;
    }

    /**
     * Get f017fsuffix
     *
     * @return string
     */
    public function getF017fsuffix()
    {
        return $this->f017fsuffix;
    }

    /**
     * Set f017fautoPrintAfterSaving
     *
     * @param boolean $f017fautoPrintAfterSaving
     *
     * @return T017fvoucherFormats
     */
    public function setF017fautoPrintAfterSaving($f017fautoPrintAfterSaving)
    {
        $this->f017fautoPrintAfterSaving = $f017fautoPrintAfterSaving;

        return $this;
    }

    /**
     * Get f017fautoPrintAfterSaving
     *
     * @return boolean
     */
    public function getF017fautoPrintAfterSaving()
    {
        return $this->f017fautoPrintAfterSaving;
    }

    /**
     * Set f017fautoNarration
     *
     * @param boolean $f017fautoNarration
     *
     * @return T017fvoucherFormats
     */
    public function setF017fautoNarration($f017fautoNarration)
    {
        $this->f017fautoNarration = $f017fautoNarration;

        return $this;
    }

    /**
     * Get f017fautoNarration
     *
     * @return boolean
     */
    public function getF017fautoNarration()
    {
        return $this->f017fautoNarration;
    }

    /**
     * Set f017fautoNarrationDescription
     *
     * @param string $f017fautoNarrationDescription
     *
     * @return T017fvoucherFormats
     */
    public function setF017fautoNarrationDescription($f017fautoNarrationDescription)
    {
        $this->f017fautoNarrationDescription = $f017fautoNarrationDescription;

        return $this;
    }

    /**
     * Get f017fautoNarrationDescription
     *
     * @return string
     */
    public function getF017fautoNarrationDescription()
    {
        return $this->f017fautoNarrationDescription;
    }

    /**
     * Set f017fresetNumberEveryFy
     *
     * @param boolean $f017fresetNumberEveryFy
     *
     * @return T017fvoucherFormats
     */
    public function setF017fresetNumberEveryFy($f017fresetNumberEveryFy)
    {
        $this->f017fresetNumberEveryFy = $f017fresetNumberEveryFy;

        return $this;
    }

    /**
     * Get f017fresetNumberEveryFy
     *
     * @return boolean
     */
    public function getF017fresetNumberEveryFy()
    {
        return $this->f017fresetNumberEveryFy;
    }

    /**
     * Set f017fupdatedDtTm
     *
     * @param \DateTime $f017fupdatedDtTm
     *
     * @return T017fvoucherFormats
     */
    public function setF017fupdatedDtTm($f017fupdatedDtTm)
    {
        $this->f017fupdatedDtTm = $f017fupdatedDtTm;

        return $this;
    }

    /**
     * Get f017fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF017fupdatedDtTm()
    {
        return $this->f017fupdatedDtTm;
    }

    /**
     * Set f017fstatus
     *
     * @param boolean $f017fstatus
     *
     * @return T017fvoucherFormats
     */
    public function setF017fstatus($f017fstatus)
    {
        $this->f017fstatus = $f017fstatus;

        return $this;
    }

    /**
     * Get f017fstatus
     *
     * @return boolean
     */
    public function getF017fstatus()
    {
        return $this->f017fstatus;
    }

    /**
     * Set f017fupdatedBy
     *
     * @param \Application\Entity\T014fuser $f017fupdatedBy
     *
     * @return T017fvoucherFormats
     */
    public function setF017fupdatedBy(\Application\Entity\T014fuser $f017fupdatedBy = null)
    {
        $this->f017fupdatedBy = $f017fupdatedBy;

        return $this;
    }

    /**
     * Get f017fupdatedBy
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF017fupdatedBy()
    {
        return $this->f017fupdatedBy;
    }

    /**
     * Set f017fidVoucherType
     *
     * @param \Application\Entity\T011fdefinationms $f017fidVoucherType
     *
     * @return T017fvoucherFormats
     */
    public function setF017fidVoucherType(\Application\Entity\T011fdefinationms $f017fidVoucherType = null)
    {
        $this->f017fidVoucherType = $f017fidVoucherType;

        return $this;
    }

    /**
     * Get f017fidVoucherType
     *
     * @return \Application\Entity\T011fdefinationms
     */
    public function getF017fidVoucherType()
    {
        return $this->f017fidVoucherType;
    }

    /**
     * Set f017fparentVoucherType
     *
     * @param \Application\Entity\T011fdefinationms $f017fparentVoucherType
     *
     * @return T017fvoucherFormats
     */
    public function setF017fparentVoucherType(\Application\Entity\T011fdefinationms $f017fparentVoucherType = null)
    {
        $this->f017fparentVoucherType = $f017fparentVoucherType;

        return $this;
    }

    /**
     * Get f017fparentVoucherType
     *
     * @return \Application\Entity\T011fdefinationms
     */
    public function getF017fparentVoucherType()
    {
        return $this->f017fparentVoucherType;
    }
}
