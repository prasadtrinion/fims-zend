<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T032fcreditNoteDetails
 *
 * @ORM\Table(name="t032fcredit_note_details", indexes={@ORM\Index(name="IDX_6355FC2E8BCA1F97", columns={"f032fid_credit_note"})})
 * @ORM\Entity(repositoryClass="Application\Repository\CreditNoteRepository")
 */
class T032fcreditNoteDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f032fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f032fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fid_item", type="integer", nullable=false)
     */
    private $f032fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fgst_value", type="integer", nullable=false)
     */
    private $f032fgstValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fquantity", type="integer", nullable=false)
     */
    private $f032fquantity;

    /**
     * @var float
     *
     * @ORM\Column(name="f032fprice", type="integer", nullable=false)
     */
    private $f032fprice;

    /**
     * @var float
     *
     * @ORM\Column(name="f032fto_cn_amount", type="integer", nullable=false)
     */
    private $f032ftoCnAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="f032ftotal", type="integer", nullable=false)
     */
    private $f032ftotal;

        /**
     * @var integer
     *
     * @ORM\Column(name="f032fid_tax_code", type="integer", nullable=true)
     */
    private $f032fidTaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032ftotal_exc", type="integer", nullable=true)
     */
    private $f032ftotalExc;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032ftax_amount", type="integer", nullable=true)
     */
    private $f032ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fstatus", type="integer", nullable=false)
     */
    private $f032fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fcreated_by", type="integer", nullable=false)
     */
    private $f032fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fupdated_by", type="integer", nullable=false)
     */
    private $f032fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f032fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f032fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f032fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f032fupdatedDtTm;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fgst_code", type="string", length=255, nullable=false)
     */
    private $f032fgstCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fdebit_fund_code", type="string",length=20, nullable=false)
     */
    private $f032fdebitFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f032fdebit_department_code", type="string",length=20, nullable=false)
     */
    private $f032fdebitDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fdebit_activity_code", type="string",length=20, nullable=false)
     */
    private $f032fdebitActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fdebit_account_code", type="string",length=20, nullable=false)
     */
    private $f032fdebitAccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fcredit_fund_code", type="string",length=20, nullable=false)
     */
    private $f032fcreditFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f032fcredit_department_code", type="string",length=20, nullable=false)
     */
    private $f032fcreditDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fcredit_activity_code", type="string",length=20, nullable=false)
     */
    private $f032fcreditActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f032fcredit_account_code", type="string",length=20, nullable=false)
     */
    private $f032fcreditAccountCode;

    /**
     * @var \Application\Entity\T031fcreditNote
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\T031fcreditNote")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="f032fid_credit_note", referencedColumnName="f031fid")
     * })
     */
    private $f032fidCreditNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fbalance_amount", type="integer", nullable=false)
     */ 
    private $f032fbalanceAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f032fid_invoice_detail", type="integer", nullable=false)
     */ 
    private $f032fidInvoiceDetail;


    /**
     * Get f032fid
     *
     * @return integer
     */
    public function getF032fid()
    {
        return $this->f032fid;
    }

    /**
     * Set f032fidItem
     *
     * @param integer $f032fidItem
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fidItem($f032fidItem)
    {
        $this->f032fidItem = $f032fidItem;

        return $this;
    }

    /**
     * Get f032fidItem
     *
     * @return integer
     */
    public function getF032fidItem()
    {
        return $this->f032fidItem;
    }

    /**
     * Set f032fgstValue
     *
     * @param integer $f032fgstValue
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fgstValue($f032fgstValue)
    {
        $this->f032fgstValue = $f032fgstValue;

        return $this;
    }

    /**
     * Get f032fgstValue
     *
     * @return integer
     */
    public function getF032fgstValue()
    {
        return $this->f032fgstValue;
    }

    /**
     * Set f032fquantity
     *
     * @param integer $f032fquantity
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fquantity($f032fquantity)
    {
        $this->f032fquantity = $f032fquantity;

        return $this;
    }

    /**
     * Get f032fquantity
     *
     * @return integer
     */
    public function getF032fquantity()
    {
        return $this->f032fquantity;
    }

    /**
     * Set f032fprice
     *
     * @param float $f032fprice
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fprice($f032fprice)
    {
        $this->f032fprice = $f032fprice;

        return $this;
    }

    /**
     * Get f032fprice
     *
     * @return float
     */
    public function getF032fprice()
    {
        return $this->f032fprice;
    }

    /**
     * Set f032ftoCnAmount
     *
     * @param float $f032ftoCnAmount
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032ftoCnAmount($f032ftoCnAmount)
    {
        $this->f032ftoCnAmount = $f032ftoCnAmount;

        return $this;
    }

    /**
     * Get f032ftoCnAmount
     *
     * @return float
     */
    public function getF032ftoCnAmount()
    {
        return $this->f032ftoCnAmount;
    }

    /**
     * Set f032ftotal
     *
     * @param float $f032ftotal
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032ftotal($f032ftotal)
    {
        $this->f032ftotal = $f032ftotal;

        return $this;
    }

    /**
     * Get f032ftotal
     *
     * @return float
     */
    public function getF032ftotal()
    {
        return $this->f032ftotal;
    }

    /**
     * Set f032fstatus
     *
     * @param integer $f032fstatus
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fstatus($f032fstatus)
    {
        $this->f032fstatus = $f032fstatus;

        return $this;
    }

    /**
     * Get f032fstatus
     *
     * @return integer
     */
    public function getF032fstatus()
    {
        return $this->f032fstatus;
    }

    /**
     * Set f032fcreatedBy
     *
     * @param integer $f032fcreatedBy
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreatedBy($f032fcreatedBy)
    {
        $this->f032fcreatedBy = $f032fcreatedBy;

        return $this;
    }

    /**
     * Get f032fcreatedBy
     *
     * @return integer
     */
    public function getF032fcreatedBy()
    {
        return $this->f032fcreatedBy;
    }

    /**
     * Set f032fupdatedBy
     *
     * @param integer $f032fupdatedBy
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fupdatedBy($f032fupdatedBy)
    {
        $this->f032fupdatedBy = $f032fupdatedBy;

        return $this;
    }

    /**
     * Get f032fupdatedBy
     *
     * @return integer
     */
    public function getF032fupdatedBy()
    {
        return $this->f032fupdatedBy;
    }

    /**
     * Set f032fcreatedDtTm
     *
     * @param \DateTime $f032fcreatedDtTm
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreatedDtTm($f032fcreatedDtTm)
    {
        $this->f032fcreatedDtTm = $f032fcreatedDtTm;

        return $this;
    }

    /**
     * Get f032fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF032fcreatedDtTm()
    {
        return $this->f032fcreatedDtTm;
    }

    /**
     * Set f032fupdatedDtTm
     *
     * @param \DateTime $f032fupdatedDtTm
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fupdatedDtTm($f032fupdatedDtTm)
    {
        $this->f032fupdatedDtTm = $f032fupdatedDtTm;

        return $this;
    }

    /**
     * Get f032fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF032fupdatedDtTm()
    {
        return $this->f032fupdatedDtTm;
    }

    /**
     * Set f032fgstCode
     *
     * @param string $f032fgstCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fgstCode($f032fgstCode)
    {
        $this->f032fgstCode = $f032fgstCode;

        return $this;
    }

    /**
     * Get f032fgstCode
     *
     * @return string
     */
    public function getF032fgstCode()
    {
        return $this->f032fgstCode;
    }

     /**
     * Set f032fdebitFundCode
     *
     * @param integer $f032fdebitFundCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fdebitFundCode($f032fdebitFundCode)
    {
        $this->f032fdebitFundCode = $f032fdebitFundCode;

        return $this;
    }

    /**
     * Get f032fdebitFundCode
     *
     * @return integer
     */
    public function getF032fdebitFundCode()
    {
        return $this->f032fdebitFundCode;
    }

    /**
     * Set f032fdebitAccountCode
     *
     * @param integer $f032fdebitAccountCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fdebitAccountCode($f032fdebitAccountCode)
    {
        $this->f032fdebitAccountCode = $f032fdebitAccountCode;

        return $this;
    }

    /**
     * Get f032fdebitAccountCode
     *
     * @return integer
     */
    public function getF032fdebitAccountCode()
    {
        return $this->f032fdebitAccountCode;
    }

    /**
     * Set f032fdebitActivityCode
     *
     * @param integer $f032fdebitActivityCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fdebitActivityCode($f032fdebitActivityCode)
    {
        $this->f032fdebitActivityCode = $f032fdebitActivityCode;

        return $this;
    }

    /**
     * Get f032fdebitActivityCode
     *
     * @return integer
     */
    public function getF032fdebitActivityCode()
    {
        return $this->f032fdebitActivityCode;
    }

    /**
     * Set f032fdebitDepartmentCode
     *
     * @param integer $f032fdebitDepartmentCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fdebitDepartmentCode($f032fdebitDepartmentCode)
    {
        $this->f032fdebitDepartmentCode = $f032fdebitDepartmentCode;

        return $this;
    }

    /**
     * Get f032fdebitDepartmentCode
     *
     * @return integer
     */
    public function getF032fdebitDepartmentCode()
    {
        return $this->f032fdebitDepartmentCode;
    }


    /**
     * Set f032fcreditFundCode
     *
     * @param integer $f032fcreditFundCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreditFundCode($f032fcreditFundCode)
    {
        $this->f032fcreditFundCode = $f032fcreditFundCode;

        return $this;
    }

    /**
     * Get f032fcreditFundCode
     *
     * @return integer
     */
    public function getF032fcreditFundCode()
    {
        return $this->f032fcreditFundCode;
    }

    /**
     * Set f032fcreditAccountCode
     *
     * @param integer $f032fcreditAccountCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreditAccountCode($f032fcreditAccountCode)
    {
        $this->f032fcreditAccountCode = $f032fcreditAccountCode;

        return $this;
    }

    /**
     * Get f032fcreditAccountCode
     *
     * @return integer
     */
    public function getF032fcreditAccountCode()
    {
        return $this->f032fcreditAccountCode;
    }

    /**
     * Set f032fcreditActivityCode
     *
     * @param integer $f032fcreditActivityCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreditActivityCode($f032fcreditActivityCode)
    {
        $this->f032fcreditActivityCode = $f032fcreditActivityCode;

        return $this;
    }

    /**
     * Get f032fcreditActivityCode
     *
     * @return integer
     */
    public function getF032fcreditActivityCode()
    {
        return $this->f032fcreditActivityCode;
    }

    /**
     * Set f032fcreditDepartmentCode
     *
     * @param integer $f032fcreditDepartmentCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fcreditDepartmentCode($f032fcreditDepartmentCode)
    {
        $this->f032fcreditDepartmentCode = $f032fcreditDepartmentCode;

        return $this;
    }

    /**
     * Set f032fidCreditNote
     *
     * @param \Application\Entity\T031fcreditNote $f032fidCreditNote
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fidCreditNote(\Application\Entity\T031fcreditNote $f032fidCreditNote = null)
    {
        $this->f032fidCreditNote = $f032fidCreditNote;

        return $this;
    }

    /**
     * Get f032fidCreditNote
     *
     * @return \Application\Entity\T031fcreditNote
     */
    public function getF032fidCreditNote()
    {
        return $this->f032fidCreditNote;
    }

    /**
     * Set f032fidTaxCode
     *
     * @param integer $f032fidTaxCode
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fidTaxCode($f032fidTaxCode)
    {
        $this->f032fidTaxCode = $f032fidTaxCode;

        return $this;
    }

    /**
     * Get f032fidTaxCode
     *
     * @return integer
     */
    public function getF032fidTaxCode()
    {
        return $this->f032fidTaxCode;
    }

    /**
     * Set f032ftotalExc
     *
     * @param integer $f032ftotalExc
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032ftotalExc($f032ftotalExc)
    {
        $this->f032ftotalExc = $f032ftotalExc;

        return $this;
    }

    /**
     * Get f032ftotalExc
     *
     * @return integer
     */
    public function getF032ftotalExc()
    {
        return $this->f032ftotalExc;
    }

    /**
     * Set f032ftaxAmount
     *
     * @param integer $f032ftaxAmount
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032ftaxAmount($f032ftaxAmount)
    {
        $this->f032ftaxAmount = $f032ftaxAmount;

        return $this;
    }

    /**
     * Get f032ftaxAmount
     *
     * @return integer
     */
    public function getF032ftaxAmount()
    {
        return $this->f032ftaxAmount;
    }

    /**
     * Set f032fbalanceAmount
     *
     * @param integer $f032fbalanceAmount
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fbalanceAmount($f032fbalanceAmount)
    {
        $this->f032fbalanceAmount = $f032fbalanceAmount;

        return $this;
    }

    /**
     * Get f032fbalanceAmount
     *
     * @return integer
     */
    public function getF032fbalanceAmount()
    {
        return $this->f032fbalanceAmount;
    }

    /**
     * Set f032fidInvoiceDetail
     *
     * @param integer $f032fidInvoiceDetail
     *
     * @return T032fcreditNoteDetails
     */
    public function setF032fidInvoiceDetail($f032fidInvoiceDetail)
    {
        $this->f032fidInvoiceDetail = $f032fidInvoiceDetail;

        return $this;
    }

    /**
     * Get f032fidInvoiceDetail
     *
     * @return integer
     */
    public function getF032fidInvoiceDetail()
    {
        return $this->f032fidInvoiceDetail;
    }
}