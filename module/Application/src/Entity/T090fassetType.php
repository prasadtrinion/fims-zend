<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T090fassetType
 *
 * @ORM\Table(name="t090fasset_type", uniqueConstraints={@ORM\UniqueConstraint(name="f090ftype_code_UNIQUE", columns={"f090ftype_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\AssetTypeRepository")
 */ 
class T090fassetType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f090fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f090fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fid_sub_category", type="integer", nullable=true)
     */
    private $f090fidSubCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f090fcode", type="string", length=50, nullable=true)
     */
    private $f090fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fid_item_group", type="integer", length=50, nullable=true)
     */
    private $f090fidItemGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fid_category", type="integer", length=50, nullable=true)
     */
    private $f090fidCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f090ftype_code", type="string", length=50, nullable=true)
     */
    private $f090ftypeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f090ftype_description", type="string", length=100, nullable=true)
     */
    private $f090ftypeDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fstatus", type="integer", nullable=true)
     */
    private $f090fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f090fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f090fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f090fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f090fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fupdated_by", type="integer", length=50, nullable=true)
     */
    private $f090fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f090fcreated_by", type="integer", length=50, nullable=true)
     */
    private $f090fcreatedBy;


    /**
     * Get f090fid
     *
     * @return integer
     */
    public function getF090fid()
    {
        return $this->f090fid;
    }

    /**
     * Set f090fidSubCategory
     *
     * @param integer $f090fidSubCategory
     *
     * @return T090fassetType
     */
    public function setF090fidSubCategory($f090fidSubCategory)
    {
        $this->f090fidSubCategory = $f090fidSubCategory;

        return $this;
    }

    
    public function getF090fidSubCategory()
    {
        return $this->f090fidSubCategory;
    }

    /**
     * Set f090fcode
     *
     * @param string $f090fcode
     *
     * @return T090fassetType
     */
    public function setF090fcode($f090fcode)
    {
        $this->f090fcode = $f090fcode;

        return $this;
    }

    /**
     * Get f090fcode
     *
     * @return string
     */
    public function getF090fcode()
    {
        return $this->f090fcode;
    }


    /**
     * Set f090fidItemGroup
     *
     * @param integer $f090fidItemGroup
     *
     * @return T090fassetType
     */
    public function setF090fidItemGroup($f090fidItemGroup)
    {
        $this->f090fidItemGroup = $f090fidItemGroup;

        return $this;
    }

    /**
     * Get f090fidItemGroup
     *
     * @return integer
     */
    public function getF090fidItemGroup()
    {
        return $this->f090fidItemGroup;
    }



    /**
     * Set f090fidCategory
     *
     * @param integer $f090fidCategory
     *
     * @return T090fassetType
     */
    public function setF090fidCategory($f090fidCategory)
    {
        $this->f090fidCategory = $f090fidCategory;

        return $this;
    }

    /**
     * Get f090fidCategory
     *
     * @return integer
     */
    public function getF090fidCategory()
    {
        return $this->f090fidCategory;
    }


    /**
     * Set f090ftypeCode
     *
     * @param integer $f090ftypeCode
     *
     * @return T090fassetType
     */
    public function setF090ftypeCode($f090ftypeCode)
    {
        $this->f090ftypeCode = $f090ftypeCode;

        return $this;
    }

    /**
     * Get f090ftypeCode
     *
     * @return integer
     */
    public function getF090ftypeCode()
    {
        return $this->f090ftypeCode;
    }



    /**
     * Set f090ftypeDescription
     *
     * @param integer $f090ftypeDescription
     *
     * @return T090fassetType
     */
    public function setF090ftypeDescription($f090ftypeDescription)
    {
        $this->f090ftypeDescription = $f090ftypeDescription;

        return $this;
    }

    /**
     * Get f090ftypeDescription
     *
     * @return integer
     */
    public function getF090ftypeDescription()
    {
        return $this->f090ftypeDescription;
    }

    


    /**
     * Set f090fstatus
     *
     * @param integer $f090fstatus
     *
     * @return T090fassetType
     */
    public function setF090fstatus($f090fstatus)
    {
        $this->f090fstatus = $f090fstatus;

        return $this;
    }

    /**
     * Get f090fstatus
     *
     * @return integer
     */
    public function getF090fstatus()
    {
        return $this->f090fstatus;
    }

    /**
     * Set f090fcreatedDtTm
     *
     * @param \DateTime $f090fcreatedDtTm
     *
     * @return T090fassetType
     */
    public function setF090fcreatedDtTm($f090fcreatedDtTm)
    {
        $this->f090fcreatedDtTm = $f090fcreatedDtTm;

        return $this;
    }

    /**
     * Get f090fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF090fcreatedDtTm()
    {
        return $this->f090fcreatedDtTm;
    }

    /**
     * Set f090fupdatedDtTm
     *
     * @param \DateTime $f090fupdatedDtTm
     *
     * @return T090fassetType
     */
    public function setF090fupdatedDtTm($f090fupdatedDtTm)
    {
        $this->f090fupdatedDtTm = $f090fupdatedDtTm;

        return $this;
    }

    /**
     * Get f090fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF090fupdatedDtTm()
    {
        return $this->f090fupdatedDtTm;
    }

    /**
     * Set f090fupdatedBy
     *
     * @param integer $f090fupdatedBy
     *
     * @return T090fassetType
     */
    public function setF090fupdatedBy($f090fupdatedBy)
    {
        $this->f090fupdatedBy = $f090fupdatedBy;

        return $this;
    }

    
    public function getF090fupdatedBy()
    {
        return $this->f090fupdatedBy;
    }

    /**
     * Set f090fcreatedBy
     *
     * @param  integer $f090fcreatedBy
     *
     * @return T090fassetType
     */
    public function setF090fcreatedBy($f090fcreatedBy)
    {
        $this->f090fcreatedBy = $f090fcreatedBy;

        return $this;
    }

    
    public function getF090fcreatedBy()
    {
        return $this->f090fcreatedBy;
    }
}
