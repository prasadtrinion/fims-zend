<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T054factivityDetails
 *
 * @ORM\Table(name="t054factivity_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetActivityRepository")
 */
class T054factivityDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f054fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fid_activity_master", type="integer", nullable=false)
     */
    private $f054fidActivityMaster;

      /**
     * @var string
     *
     * @ORM\Column(name="f054ffund_code", type="string",length=20, nullable=false)
     */
    private $f054ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f054fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f054fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f054factivity_code", type="string",length=20, nullable=false)
     */
    private $f054factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f054faccount_code", type="string",length=20, nullable=false)
     */
    private $f054faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054famount", type="integer", nullable=false)
     */
    private $f054famount;

     /**
     * @var string
     *
     * @ORM\Column(name="f054fso_code", type="string",length=10, nullable=false)
     */
    private $f054fsoCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fq1", type="integer", nullable=false)
     */
    private $f054fq1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fq2", type="integer", nullable=false)
     */
    private $f054fq2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fq3", type="integer", nullable=false)
     */
    private $f054fq3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fq4", type="integer", nullable=false)
     */
    private $f054fq4;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fcreated_by", type="integer", nullable=false)
     */
    private $f054fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fupdated_by", type="integer", nullable=false)
     */
    private $f054fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f054fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f054fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fstatus", type="integer", nullable=false)
     */
    private $f054fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f054fbalance_amount", type="integer", nullable=false)
     */
    private $f054fbalanceAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="f054fbudget_type", type="string",length=10, nullable=true)
     */
    private $f054fbudgetType;

    /**
     * Get f054fidDetails
     *
     * @return integer
     */
    public function getF054fidDetails()
    {
        return $this->f054fidDetails;
    }

    /**
     * Set f054fidActivityMaster
     *
     * @param integer $f054fidActivityMaster
     *
     * @return T054factivityDetails
     */
    public function setF054fidActivityMaster($f054fidActivityMaster)
    {
        $this->f054fidActivityMaster = $f054fidActivityMaster;

        return $this;
    }

    /**
     * Get f054fidActivityMaster
     *
     * @return integer
     */
    public function getF054fidActivityMaster()
    {
        return $this->f054fidActivityMaster;
    }

    /**
     * Set f054fsoCode
     *
     * @param integer $f054fsoCode
     *
     * @return T054factivityDetails
     */
    public function setF054fsoCode($f054fsoCode)
    {
        $this->f054fsoCode = $f054fsoCode;

        return $this;
    }

    /**
     * Get f054fsoCode
     *
     * @return integer
     */
    public function getF054fsoCode()
    {
        return $this->f054fsoCode;
    }

    /**
     * Set f054fbalanceAmount
     *
     * @param integer $f054fbalanceAmount
     *
     * @return T051fbudgetCostcenter
     */
    public function setF054fbalanceAmount($f054fbalanceAmount)
    {
        $this->f054fbalanceAmount = $f054fbalanceAmount;

        return $this;
    }

    /**
     * Get f054fbalanceAmount
     *
     * @return integer
     */
    public function getF054fbalanceAmount()
    {
        return $this->f054fbalanceAmount;
    }


    /**
     * Set f054ffundCode
     *
     * @param integer $f054ffundCode
     *
     * @return T054fjournalDetails
     */
    public function setF054ffundCode($f054ffundCode)
    {
        $this->f054ffundCode = $f054ffundCode;

        return $this;
    }

    /**
     * Get f054ffundCode
     *
     * @return integer
     */
    public function getF054ffundCode()
    {
        return $this->f054ffundCode;
    }

    /**
     * Set f054faccountCode
     *
     * @param integer $f054faccountCode
     *
     * @return T054fjournalDetails
     */
    public function setF054faccountCode($f054faccountCode)
    {
        $this->f054faccountCode = $f054faccountCode;

        return $this;
    }

    /**
     * Get f054faccountCode
     *
     * @return integer
     */
    public function getF054faccountCode()
    {
        return $this->f054faccountCode;
    }

    /**
     * Set f054factivityCode
     *
     * @param integer $f054factivityCode
     *
     * @return T054fjournalDetails
     */
    public function setF054factivityCode($f054factivityCode)
    {
        $this->f054factivityCode = $f054factivityCode;

        return $this;
    }

    /**
     * Get f054factivityCode
     *
     * @return integer
     */
    public function getF054factivityCode()
    {
        return $this->f054factivityCode;
    }

    /**
     * Set f054fdepartmentCode
     *
     * @param integer $f054fdepartmentCode
     *
     * @return T054fjournalDetails
     */
    public function setF054fdepartmentCode($f054fdepartmentCode)
    {
        $this->f054fdepartmentCode = $f054fdepartmentCode;

        return $this;
    }

    /**
     * Get f054fdepartmentCode
     *
     * @return integer
     */
    public function getF054fdepartmentCode()
    {
        return $this->f054fdepartmentCode;
    }

    /**
     * Set f054famount
     *
     * @param integer $f054famount
     *
     * @return T054factivityDetails
     */
    public function setF054famount($f054famount)
    {
        $this->f054famount = $f054famount;

        return $this;
    }

    /**
     * Get f054famount
     *
     * @return integer
     */
    public function getF054famount()
    {
        return $this->f054famount;
    }

    /**
     * Set f054fq1
     *
     * @param integer $f054fq1
     *
     * @return T054factivityDetails
     */
    public function setF054fq1($f054fq1)
    {
        $this->f054fq1 = $f054fq1;

        return $this;
    }

    /**
     * Get f054fq1
     *
     * @return integer
     */
    public function getF054fq1()
    {
        return $this->f054fq1;
    }

    /**
     * Set f054fq2
     *
     * @param integer $f054fq2
     *
     * @return T054factivityDetails
     */
    public function setF054fq2($f054fq2)
    {
        $this->f054fq2 = $f054fq2;

        return $this;
    }

    /**
     * Get f054fq2
     *
     * @return integer
     */
    public function getF054fq2()
    {
        return $this->f054fq2;
    }

    /**
     * Set f054fq3
     *
     * @param integer $f054fq3
     *
     * @return T054factivityDetails
     */
    public function setF054fq3($f054fq3)
    {
        $this->f054fq3 = $f054fq3;

        return $this;
    }

    /**
     * Get f054fq3
     *
     * @return integer
     */
    public function getF054fq3()
    {
        return $this->f054fq3;
    }

    /**
     * Set f054fq4
     *
     * @param integer $f054fq4
     *
     * @return T054factivityDetails
     */
    public function setF054fq4($f054fq4)
    {
        $this->f054fq4 = $f054fq4;

        return $this;
    }

    /**
     * Get f054fq4
     *
     * @return integer
     */
    public function getF054fq4()
    {
        return $this->f054fq4;
    }

    
    /**
     * Set f054fcreatedBy
     *
     * @param integer $f054fcreatedBy
     *
     * @return T054factivityDetails
     */
    public function setF054fcreatedBy($f054fcreatedBy)
    {
        $this->f054fcreatedBy = $f054fcreatedBy;

        return $this;
    }

    /**
     * Get f054fcreatedBy
     *
     * @return integer
     */
    public function getF054fcreatedBy()
    {
        return $this->f054fcreatedBy;
    }

    /**
     * Set f054fupdatedBy
     *
     * @param integer $f054fupdatedBy
     *
     * @return T054factivityDetails
     */
    public function setF054fupdatedBy($f054fupdatedBy)
    {
        $this->f054fupdatedBy = $f054fupdatedBy;

        return $this;
    }

    /**
     * Get f054fupdatedBy
     *
     * @return integer
     */
    public function getF054fupdatedBy()
    {
        return $this->f054fupdatedBy;
    }

    /**
     * Set f054fcreatedDtTm
     *
     * @param \DateTime $f054fcreatedDtTm
     *
     * @return T054factivityDetails
     */
    public function setF054fcreatedDtTm($f054fcreatedDtTm)
    {
        $this->f054fcreatedDtTm = $f054fcreatedDtTm;

        return $this;
    }

    /**
     * Get f054fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fcreatedDtTm()
    {
        return $this->f054fcreatedDtTm;
    }

    /**
     * Set f054fupdatedDtTm
     *
     * @param \DateTime $f054fupdatedDtTm
     *
     * @return T054factivityDetails
     */
    public function setF054fupdatedDtTm($f054fupdatedDtTm)
    {
        $this->f054fupdatedDtTm = $f054fupdatedDtTm;

        return $this;
    }

    /**
     * Get f054fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF054fupdatedDtTm()
    {
        return $this->f054fupdatedDtTm;
    }

    /**
     * Set f054fstatus
     *
     * @param integer $f054fstatus
     *
     * @return T054factivityDetails
     */
    public function setF054fstatus($f054fstatus)
    {
        $this->f054fstatus = $f054fstatus;

        return $this;
    }

    /**
     * Get f054fstatus
     *
     * @return integer
     */
    public function getF054fstatus()
    {
        return $this->f054fstatus;
    }

    /**
     * Set f054fbudgetType
     *
     * @param string $f054fbudgetType
     *
     * @return T051fbudgetCostcenter
     */
    public function setF054fbudgetType($f054fbudgetType)
    {
        $this->f054fbudgetType = $f054fbudgetType;

        return $this;
    }

    /**
     * Get f054fbudgetType
     *
     * @return string
     */
    public function getF054fbudgetType()
    {
        return $this->f054fbudgetType;
    }
}
