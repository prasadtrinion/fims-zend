<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T091fpayroll
 *
 * @ORM\Table(name="t091fpayroll")
 * @ORM\Entity(repositoryClass="Application\Repository\PayrollRepository")
 */
class T091fpayroll
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f091fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f091fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fstaff_type", type="integer", nullable=false)
     */
    private $f091fstaffType;

    /**
     * @var string
     *
     * @ORM\Column(name="f091fmonth", type="string", length=25, nullable=false)
     */
    private $f091fmonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fbank", type="integer", nullable=false)
     */
    private $f091fbank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fstatus", type="integer", nullable=false)
     */
    private $f091fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fcreated_by", type="integer", nullable=false)
     */
    private $f091fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f091fupdated_by", type="integer", nullable=false)
     */
    private $f091fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f091fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f091fupdatedDtTm;



    /**
     * Get f091fid
     *
     * @return integer
     */
    public function getF091fid()
    {
        return $this->f091fid;
    }

    /**
     * Set f091fstaffType
     *
     * @param string $f091fstaffType
     *
     * @return T091fpayroll
     */
    public function setF091fstaffType($f091fstaffType)
    {
        $this->f091fstaffType = $f091fstaffType;

        return $this;
    }

    /**
     * Get f091fstaffType
     *
     * @return string
     */
    public function getF091fstaffType()
    {
        return $this->f091fstaffType;
    }

    /**
     * Set f091fmonth
     *
     * @param string $f091fmonth
     *
     * @return T091fpayroll
     */
    public function setF091fmonth($f091fmonth)
    {
        $this->f091fmonth = $f091fmonth;

        return $this;
    }

    /**
     * Get f091fmonth
     *
     * @return string
     */
    public function getF091fmonth()
    {
        return $this->f091fmonth;
    }

    /**
     * Set f091fbank
     *
     * @param integer $f091fbank
     *
     * @return T091fpayroll
     */
    public function setF091fbank($f091fbank)
    {
        $this->f091fbank = $f091fbank;

        return $this;
    }

    /**
     * Get f091fbank
     *
     * @return integer
     */
    public function getF091fbank()
    {
        return $this->f091fbank;
    }

    /**
     * Set f091fstatus
     *
     * @param integer $f091fstatus
     *
     * @return T091fpayroll
     */
    public function setF091fstatus($f091fstatus)
    {
        $this->f091fstatus = $f091fstatus;

        return $this;
    }

    /**
     * Get f091fstatus
     *
     * @return integer
     */
    public function getF091fstatus()
    {
        return $this->f091fstatus;
    }

    /**
     * Set f091fcreatedBy
     *
     * @param integer $f091fcreatedBy
     *
     * @return T091fpayroll
     */
    public function setF091fcreatedBy($f091fcreatedBy)
    {
        $this->f091fcreatedBy = $f091fcreatedBy;

        return $this;
    }

    /**
     * Get f091fcreatedBy
     *
     * @return integer
     */
    public function getF091fcreatedBy()
    {
        return $this->f091fcreatedBy;
    }

    /**
     * Set f091fupdatedBy
     *
     * @param integer $f091fupdatedBy
     *
     * @return T091fpayroll
     */
    public function setF091fupdatedBy($f091fupdatedBy)
    {
        $this->f091fupdatedBy = $f091fupdatedBy;

        return $this;
    }

    /**
     * Get f091fupdatedBy
     *
     * @return integer
     */
    public function getF091fupdatedBy()
    {
        return $this->f091fupdatedBy;
    }

    /**
     * Set f091fcreatedDtTm
     *
     * @param \DateTime $f091fcreatedDtTm
     *
     * @return T091fpayroll
     */
    public function setF091fcreatedDtTm($f091fcreatedDtTm)
    {
        $this->f091fcreatedDtTm = $f091fcreatedDtTm;

        return $this;
    }

    /**
     * Get f091fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fcreatedDtTm()
    {
        return $this->f091fcreatedDtTm;
    }

    /**
     * Set f091fupdatedDtTm
     *
     * @param \DateTime $f091fupdatedDtTm
     *
     * @return T091fpayroll
     */
    public function setF091fupdatedDtTm($f091fupdatedDtTm)
    {
        $this->f091fupdatedDtTm = $f091fupdatedDtTm;

        return $this;
    }

    /**
     * Get f091fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF091fupdatedDtTm()
    {
        return $this->f091fupdatedDtTm;
    }
}
