<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="student",uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})})
 * @ORM\Entity(repositoryClass="Application\Repository\StudentRepository")
 */
class Student 
{
    const MALE = 0;
    const FEMALE = 1;
        
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=45, nullable=true)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=45, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="phone_number", type="string", length=45, nullable=false)
	 */
    private $phoneNumber;
    

    /**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="string", length=250, nullable=false)
	 */
    private $address;
    

	/**
	 * @var string
	 *
	 * @ORM\Column(name="city", type="string", length=100, nullable=false)
	 */
    private $city;


    /**
	 * @var string
	 *
	 * @ORM\Column(name="state", type="string",length=10, nullable=false)
	 */
	private $state;


    /**
	 * @var string
	 *
	 * @ORM\Column(name="gender",type="boolean")
	 */
	private $gender = self::MALE;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="delete_flag", type="boolean", nullable=false)
	 */
	private $deleteFlag = '0';


	public function getId() {
		return $this->id;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function getCreateDtTm() {
		return $this->createDtTm;
	}

	public function setCreateDtTm(\DateTime $createDtTm) {
		$this->createDtTm = $createDtTm;
		return $this;
	}

	public function getUpdateDtTm() {
		return $this->updateDtTm;
	}

	public function setUpdateDtTm(\DateTime $updateDtTm) {
		$this->updateDtTm = $updateDtTm;
		return $this;
	}

	public function getDeleteFlag() {
		return $this->deleteFlag;
	}

	public function setDeleteFlag($deleteFlag) {
		$this->deleteFlag = $deleteFlag;
		return $this;
	}

	public function getPhoneNumber() {
		return $this->phoneNumber;
	}
	
	public function setPhoneNumber($phoneNumber) {
		$this->phoneNumber = $phoneNumber;
		return $this;
    }

    public function getGender() {
		return $this->gender;
	}

	public function setGender($gender) {
		$this->gender = $gender;
		return $this;
	}

    public function getAddress() {
		return $this->address;
	}

	public function setAddress($address) {
		$this->address = $address;
		return $this;
	}
    
    public function getCity() {
		return $this->city;
	}

	public function setCity($city) {
		$this->city = $city;
		return $this;
	}

	public function getState() {
		return $this->state;
	}

	public function setState($state) {
		$this->state = $state;
		return $this;
	}
}
