<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T046fstudentDiscount
 *
 * @ORM\Table(name="t046fstudent_discount")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentDiscountRepository")
 */
class T046fstudentDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f046fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f046fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f046fdiscount_type", type="string", length=200, nullable=false)
     */
    private $f046fdiscountType;

    /**
     * @var string
     *
     * @ORM\Column(name="f046fcustomer_type", type="string", length=200, nullable=false)
     */
    private $f046fcustomerType;

    /**
     * @var string
     *
     * @ORM\Column(name="f046frevenue_category", type="integer", nullable=false)
     */
    private $f046fidRevenueCategory;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046frevenue_code", type="integer", nullable=false)
     */
    private $f046fidRevenueCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fdiscount_rate_type", type="integer", nullable=false)
     */
    private $f046fdiscountRateType;


    /**
     * @var integer
     *
     * @ORM\Column(name="f046fdiscount_rate", type="integer", nullable=false)
     */
    private $f046fdiscountRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fminimum_outstanding", type="integer", nullable=false)
     */
    private $f046fminimumOutstanding;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fcreated_by", type="integer", nullable=false)
     */
    private $f046fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fupdated_by", type="integer", nullable=false)
     */
    private $f046fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f046fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046foutstanding_as", type="string", length=255, nullable=false)
     */
    private $f046foutstandingAs;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fdiscount_gen_date", type="datetime", nullable=false)
     */
    private $f046fdiscountGenerationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f046fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f046fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f046fstatus", type="integer", nullable=false)
     */
    private $f046fstatus;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046fdiscount_gen_type",  type="string", length=200, nullable=false)
     */
    private $f046fdiscountGenerationType;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046fid_customer", type="integer", nullable=false)
     */
    private $f046fidCustomer;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046ffund",  type="string", length=200, nullable=false)
     */
    private $f046ffund;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046fdepartment",  type="string", length=200, nullable=false)
     */
    private $f046fdepartment;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046factivity",  type="string", length=200, nullable=false)
     */
    private $f046factivity;

     /**
     * @var integer
     *
     * @ORM\Column(name="f046faccount",  type="string", length=200, nullable=false)
     */
    private $f046faccount;



    /**
     * Get f046fid
     *
     * @return integer
     */
    public function getF046fid()
    {
        return $this->f046fid;
    }

    /**
     * Set f046ffund
     *
     * @param string $f046ffund
     *
     * @return T046fstudentDiscount
     */
    public function setF046ffund($f046ffund)
    {
        $this->f046ffund = $f046ffund;

        return $this;
    }

    /**
     * Get f046ffund
     *
     * @return string
     */
    public function getF046ffund()
    {
        return $this->f046ffund;
    }

    /**
     * Set f046fdepartment
     *
     * @param string $f046fdepartment
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdepartment($f046fdepartment)
    {
        $this->f046fdepartment = $f046fdepartment;

        return $this;
    }

    /**
     * Get f046fdepartment
     *
     * @return string
     */
    public function getF046fdepartment()
    {
        return $this->f046fdepartment;
    }

    /**
     * Set f046factivity
     *
     * @param string $f046factivity
     *
     * @return T046fstudentDiscount
     */
    public function setF046factivity($f046factivity)
    {
        $this->f046factivity = $f046factivity;

        return $this;
    }

    /**
     * Get f046factivity
     *
     * @return string
     */
    public function getF046factivity()
    {
        return $this->f046factivity;
    }

    /**
     * Set f046faccount
     *
     * @param string $f046faccount
     *
     * @return T046fstudentDiscount
     */
    public function setF046faccount($f046faccount)
    {
        $this->f046faccount = $f046faccount;

        return $this;
    }

    /**
     * Get f046faccount
     *
     * @return string
     */
    public function getF046faccount()
    {
        return $this->f046faccount;
    }

    /**
     * Set f046fdiscountType
     *
     * @param string $f046fdiscountType
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdiscountType($f046fdiscountType)
    {
        $this->f046fdiscountType = $f046fdiscountType;

        return $this;
    }

    /**
     * Get f046fdiscountType
     *
     * @return string
     */
    public function getF046fdiscountType()
    {
        return $this->f046fdiscountType;
    }

    /**
     * Set f046fcustomerType
     *
     * @param string $f046fcustomerType
     *
     * @return T046fstudentDiscount
     */
    public function setF046fcustomerType($f046fcustomerType)
    {
        $this->f046fcustomerType = $f046fcustomerType;

        return $this;
    }

    /**
     * Get f046fcustomerType
     *
     * @return string
     */
    public function getF046fcustomerType()
    {
        return $this->f046fcustomerType;
    }

    /**
     * Set f046fidRevenueCategory
     *
     * @param string $f046fidRevenueCategory
     *
     * @return T046fstudentDiscount
     */
    public function setF046fidRevenueCategory($f046fidRevenueCategory)
    {
        $this->f046fidRevenueCategory = $f046fidRevenueCategory;

        return $this;
    }

    /**
     * Get f046fidRevenueCategory
     *
     * @return string
     */
    public function getF046fidRevenueCategory()
    {
        return $this->f046fidRevenueCategory;
    }

    /**
     * Set f046fidRevenueCode
     *
     * @param integer $f046fidRevenueCode
     *
     * @return T046fstudentDiscount
     */
    public function setF046fidRevenueCode($f046fidRevenueCode)
    {
        $this->f046fidRevenueCode = $f046fidRevenueCode;

        return $this;
    }

    /**
     * Get f046fidRevenueCode
     *
     * @return integer
     */
    public function getF046fidRevenueCode()
    {
        return $this->f046fidRevenueCode;
    }

    /**
     * Set f046fdiscountRateType
     *
     * @param integer $f046fdiscountRateType
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdiscountRateType($f046fdiscountRateType)
    {
        $this->f046fdiscountRateType = $f046fdiscountRateType;

        return $this;
    }

    /**
     * Get f046fdiscountRateType
     *
     * @return integer
     */
    public function getF046fdiscountRateType()
    {
        return $this->f046fdiscountRateType;
    }
    /**
     * Set f046fcreatedBy
     *
     * @param integer $f046fcreatedBy
     *
     * @return T046fstudentDiscount
     */
    public function setF046fcreatedBy($f046fcreatedBy)
    {
        $this->f046fcreatedBy = $f046fcreatedBy;

        return $this;
    }

    /**
     * Get f046fcreatedBy
     *
     * @return integer
     */
    public function getF046fcreatedBy()
    {
        return $this->f046fcreatedBy;
    }


    /**
     * Set f046fupdatedBy
     *
     * @param integer $f046fupdatedBy
     *
     * @return T046fstudentDiscount
     */
    public function setF046fupdatedBy($f046fupdatedBy)
    {
        $this->f046fupdatedBy = $f046fupdatedBy;

        return $this;
    }

    /**
     * Get f046fupdatedBy
     *
     * @return integer
     */
    public function getF046fupdatedBy()
    {
        return $this->f046fupdatedBy;
    }

    /**
     * Set f046fcreatedDtTm
     *
     * @param \DateTime $f046fcreatedDtTm
     *
     * @return T046fstudentDiscount
     */
    public function setF046fcreatedDtTm($f046fcreatedDtTm)
    {
        $this->f046fcreatedDtTm = $f046fcreatedDtTm;

        return $this;
    }

    /**
     * Get f046fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF046fcreatedDtTm()
    {
        return $this->f046fcreatedDtTm;
    }

    /**
     * Set f046fupdatedDtTm
     *
     * @param \DateTime $f046fupdatedDtTm
     *
     * @return T046fstudentDiscount
     */
    public function setF046fupdatedDtTm($f046fupdatedDtTm)
    {
        $this->f046fupdatedDtTm = $f046fupdatedDtTm;

        return $this;
    }

    /**
     * Get f046fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF046fupdatedDtTm()
    {
        return $this->f046fupdatedDtTm;
    }

    /**
     * Set f046fstatus
     *
     * @param integer $f046fstatus
     *
     * @return T046fstudentDiscount
     */
    public function setF046fstatus($f046fstatus)
    {
        $this->f046fstatus = $f046fstatus;

        return $this;
    }

    /**
     * Get f046fstatus
     *
     * @return integer
     */
    public function getF046fstatus()
    {
        return $this->f046fstatus;
    }

    /**
     * Set f046fdiscountRate
     *
     * @param integer $f046fdiscountRate
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdiscountRate($f046fdiscountRate)
    {
        $this->f046fdiscountRate = $f046fdiscountRate;

        return $this;
    }

    /**
     * Get f046fdiscountRate
     *
     * @return integer
     */
    public function getF046fdiscountRate()
    {
        return $this->f046fdiscountRate;
    }

    /**
     * Set f046fminimumOutstanding
     *
     * @param integer $f046fminimumOutstanding
     *
     * @return T046fstudentDiscount
     */
    public function setF046fminimumOutstanding($f046fminimumOutstanding)
    {
        $this->f046fminimumOutstanding = $f046fminimumOutstanding;

        return $this;
    }

    /**
     * Get f046fminimumOutstanding
     *
     * @return integer
     */
    public function getF046fminimumOutstanding()
    {
        return $this->f046fminimumOutstanding;
    }

    /**
     * Set f046foutstandingAs
     *
     * @param integer $f046foutstandingAs
     *
     * @return T046fstudentDiscount
     */
    public function setF046foutstandingAs($f046foutstandingAs)
    {
        $this->f046foutstandingAs = $f046foutstandingAs;

        return $this;
    }

    /**
     * Get f046foutstandingAs
     *
     * @return integer
     */
    public function getF046foutstandingAs()
    {
        return $this->f046foutstandingAs;
    }

    /**
     * Set f046fdiscountGenerationType
     *
     * @param integer $f046fdiscountGenerationType
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdiscountGenerationType($f046fdiscountGenerationType)
    {
        $this->f046fdiscountGenerationType = $f046fdiscountGenerationType;

        return $this;
    }

    /**
     * Get f046fdiscountGenerationType
     *
     * @return integer
     */
    public function getF046fdiscountGenerationType()
    {
        return $this->f046fdiscountGenerationType;
    }

    /**
     * Set f046fidCustomer
     *
     * @param integer $f046fidCustomer
     *
     * @return T046fstudentDiscount
     */
    public function setF046fidCustomer($f046fidCustomer)
    {
        $this->f046fidCustomer = $f046fidCustomer;

        return $this;
    }

    /**
     * Get f046fidCustomer
     *
     * @return integer
     */
    public function getF046fidCustomer()
    {
        return $this->f046fidCustomer;
    }

    /**
     * Set f046fdiscountGenerationDate
     *
     * @param integer $f046fdiscountGenerationDate
     *
     * @return T046fstudentDiscount
     */
    public function setF046fdiscountGenerationDate($f046fdiscountGenerationDate)
    {
        $this->f046fdiscountGenerationDate = $f046fdiscountGenerationDate;

        return $this;
    }

    /**
     * Get f046fdiscountGenerationDate
     *
     * @return integer
     */
    public function getF046fdiscountGenerationDate()
    {
        return $this->f046fdiscountGenerationDate;
    }
}

