<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T093fpaymentVoucher
 *
 * @ORM\Table(name="t093fpayment_voucher")
 * @ORM\Entity(repositoryClass="Application\Repository\PaymentVoucherRepository")
 */
class T093fpaymentVoucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f093fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f093fvoucher_no", type="string", nullable=false)
     */
    private $f093fvoucherNo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fpayment_type", type="integer", nullable=false)
     */
    private $f093fpaymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="f093fdescription", type="string", length=200, nullable=true)
     */
    private $f093fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fpayment_mode", type="integer", nullable=true)
     */
    private $f093fpaymentMode;

    /**
     * @var string
     *
     * @ORM\Column(name="f093fpayment_ref_no", type="string", length=50, nullable=true)
     */
    private $f093fpaymentRefNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f093fsource", type="string", length=50, nullable=true)
     */
    private $f093fsource;

     /**
     * @var integer
     *
     * @ORM\Column(name="f093fapproval_status", type="integer", nullable=true)
     */
    private $f093fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fstatus", type="integer", nullable=true)
     */
    private $f093fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fcreated_by", type="integer", nullable=false)
     */
    private $f093fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fupdated_by", type="integer", nullable=false)
     */
    private $f093fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f093fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f093fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f093fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f093fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid_bank", type="integer", nullable=true)
     */
    private $f093fidBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f093fid_company_bank", type="integer", nullable=true)
     */
    private $f093fidCompanyBank;

    /**
     * @var string
     *
     * @ORM\Column(name="f093freason", type="string",length=30, nullable=true)
     */
    private $f093freason;

    /**
     * @var string
     *
     * @ORM\Column(name="f093fid_supplier", type="integer", nullable=true)
     */
    private $f093fidSupplier;

    /**
     * @var string
     *
     * @ORM\Column(name="f093ftotal_amount", type="integer", nullable=true)
     */
    private $f093ftotalAmount;


    /**
     * Get f093fid
     *
     * @return integer
     */
    public function getF093fid()
    {
        return $this->f093fid;
    }

    /**
     * Set f093fvoucherNo
     *
     * @param string $f093fvoucherNo
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fvoucherNo($f093fvoucherNo)
    {
        $this->f093fvoucherNo = $f093fvoucherNo;

        return $this;
    }

    /**
     * Get f093fvoucherNo
     *
     * @return string
     */
    public function getF093fvoucherNo()
    {
        return $this->f093fvoucherNo;
    }

     /**
     * Set f093freason
     *
     * @param string $f093freason
     *
     * @return T093fpaymentVoucher
     */
    public function setF093freason($f093freason)
    {
        $this->f093freason = $f093freason;

        return $this;
    }

    /**
     * Get f093freason
     *
     * @return string
     */
    public function getF093freason()
    {
        return $this->f093freason;
    }

    /**
     * Set f093fpaymentType
     *
     * @param integer $f093fpaymentType
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fpaymentType($f093fpaymentType)
    {
        $this->f093fpaymentType = $f093fpaymentType;

        return $this;
    }

    /**
     * Get f093fpaymentType
     *
     * @return integer
     */
    public function getF093fpaymentType()
    {
        return $this->f093fpaymentType;
    }

    /**
     * Set f093fdescription
     *
     * @param string $f093fdescription
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fdescription($f093fdescription)
    {
        $this->f093fdescription = $f093fdescription;

        return $this;
    }

    /**
     * Get f093fdescription
     *
     * @return string
     */
    public function getF093fdescription()
    {
        return $this->f093fdescription;
    }

    /**
     * Set f093fpaymentMode
     *
     * @param integer $f093fpaymentMode
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fpaymentMode($f093fpaymentMode)
    {
        $this->f093fpaymentMode = $f093fpaymentMode;

        return $this;
    }

    /**
     * Get f093fpaymentMode
     *
     * @return integer
     */
    public function getF093fpaymentMode()
    {
        return $this->f093fpaymentMode;
    }

    /**
     * Set f093fpaymentRefNo
     *
     * @param string $f093fpaymentRefNo
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fpaymentRefNo($f093fpaymentRefNo)
    {
        $this->f093fpaymentRefNo = $f093fpaymentRefNo;

        return $this;
    }

    /**
     * Get f093fpaymentRefNo
     *
     * @return string
     */
    public function getF093fpaymentRefNo()
    {
        return $this->f093fpaymentRefNo;
    }

    /**
     * Set f093fsource
     *
     * @param string $f093fsource
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fsource($f093fsource)
    {
        $this->f093fsource = $f093fsource;

        return $this;
    }

    /**
     * Get f093fsource
     *
     * @return string
     */
    public function getF093fsource()
    {
        return $this->f093fsource;
    }


    /**
     * Set f093fstatus
     *
     * @param integer $f093fstatus
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fstatus($f093fstatus)
    {
        $this->f093fstatus = $f093fstatus;

        return $this;
    }

    /**
     * Get f093fstatus
     *
     * @return integer
     */
    public function getF093fstatus()
    {
        return $this->f093fstatus;
    }

    /**
     * Set f093fcreatedBy
     *
     * @param integer $f093fcreatedBy
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fcreatedBy($f093fcreatedBy)
    {
        $this->f093fcreatedBy = $f093fcreatedBy;

        return $this;
    }

    /**
     * Get f093fcreatedBy
     *
     * @return integer
     */
    public function getF093fcreatedBy()
    {
        return $this->f093fcreatedBy;
    }

    /**
     * Set f093fupdatedBy
     *
     * @param integer $f093fupdatedBy
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fupdatedBy($f093fupdatedBy)
    {
        $this->f093fupdatedBy = $f093fupdatedBy;

        return $this;
    }

    /**
     * Get f093fupdatedBy
     *
     * @return integer
     */
    public function getF093fupdatedBy()
    {
        return $this->f093fupdatedBy;
    }

    /**
     * Set f093fcreatedDtTm
     *
     * @param \DateTime $f093fcreatedDtTm
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fcreatedDtTm($f093fcreatedDtTm)
    {
        $this->f093fcreatedDtTm = $f093fcreatedDtTm;

        return $this;
    }

    /**
     * Get f093fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF093fcreatedDtTm()
    {
        return $this->f093fcreatedDtTm;
    }

    /**
     * Set f093fupdatedDtTm
     *
     * @param \DateTime $f093fupdatedDtTm
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fupdatedDtTm($f093fupdatedDtTm)
    {
        $this->f093fupdatedDtTm = $f093fupdatedDtTm;

        return $this;
    }

    /**
     * Get f093fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF093fupdatedDtTm()
    {
        return $this->f093fupdatedDtTm;
    }

    /**
     * Set f093fapprovalStatus
     *
     * @param integer $f093fapprovalStatus
     *
     * @return T055fbudgetIncrement
     */
    public function setF093fapprovalStatus($f093fapprovalStatus)
    {
        $this->f093fapprovalStatus = $f093fapprovalStatus;

        return $this;
    }

    /**
     * Get f093fapprovalStatus
     *
     * @return integer
     */
    public function getF093fapprovalStatus()
    {
        return $this->f093fapprovalStatus;
    }

    /**
     * Set f093fidBank
     *
     * @param integer $f093fidBank
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fidBank($f093fidBank)
    {
        $this->f093fidBank = $f093fidBank;

        return $this;
    }

    /**
     * Get f093fidBank
     *
     * @return integer
     */
    public function getF093fidBank()
    {
        return $this->f093fidBank;
    }
    

    /**
     * Set f093fidCompanyBank
     *
     * @param integer $f093fidCompanyBank
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fidCompanyBank($f093fidCompanyBank)
    {
        $this->f093fidCompanyBank = $f093fidCompanyBank;

        return $this;
    }

    /**
     * Get f093fidCompanyBank
     *
     * @return integer
     */
    public function getF093fidCompanyBank()
    {
        return $this->f093fidCompanyBank;
    }


    /**
     * Set f093fidSupplier
     *
     * @param integer $f093fidSupplier
     *
     * @return T093fpaymentVoucher
     */
    public function setF093fidSupplier($f093fidSupplier)
    {
        $this->f093fidSupplier = $f093fidSupplier;

        return $this;
    }

    /**
     * Get f093fidSupplier
     *
     * @return integer
     */
    public function getF093fidSupplier()
    {
        return $this->f093fidSupplier;
    }


    /**
     * Set f093ftotalAmount
     *
     * @param integer $f093ftotalAmount
     *
     * @return T093fpaymentVoucher
     */
    public function setF093ftotalAmount($f093ftotalAmount)
    {
        $this->f093ftotalAmount = $f093ftotalAmount;

        return $this;
    }

    /**
     * Get f093ftotalAmount
     *
     * @return integer
     */
    public function getF093ftotalAmount()
    {
        return $this->f093ftotalAmount;
    }

}

