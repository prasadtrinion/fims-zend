<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T048ffeeStructureProgram
 *
 * @ORM\Table(name="t048ffee_structure_program")
 * @ORM\Entity(repositoryClass="Application\Repository\FeeStructureRepository")
 */
class T048ffeeStructureProgram
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_struct_program", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f048fidStructProgram;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_fee_program", type="integer", nullable=true)
     */
    private $f048fidFeeProgram;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fid_programme", type="string",length=20, nullable=true)
     */
    private $f048fidProgramme;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fstatus", type="integer", nullable=true)
     */
    private $f048fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fcreated_by", type="integer", nullable=true)
     */
    private $f048fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f048fupdated_by", type="integer", nullable=true)
     */
    private $f048fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f048fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f048fupdatedDtTm;



    /**
     * Get f048fidStructProgram
     *
     * @return integer
     */
    public function getF048fidStructProgram()
    {
        return $this->f048fidStructProgram;
    }

    /**
     * Set f048fidFeeProgram
     *
     * @param integer $f048fidFeeProgram
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fidFeeProgram($f048fidFeeProgram)
    {
        $this->f048fidFeeProgram = $f048fidFeeProgram;

        return $this;
    }

    /**
     * Get f048fidFeeProgram
     *
     * @return integer
     */
    public function getF048fidFeeProgram()
    {
        return $this->f048fidFeeProgram;
    }

    /**
     * Set f048fidProgramme
     *
     * @param integer $f048fidProgramme
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fidProgramme($f048fidProgramme)
    {
        $this->f048fidProgramme = $f048fidProgramme;

        return $this;
    }

    /**
     * Get f048fidProgramme
     *
     * @return integer
     */
    public function getF048fidProgramme()
    {
        return $this->f048fidProgramme;
    }

    
    /**
     * Set f048fstatus
     *
     * @param integer $f048fstatus
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fstatus($f048fstatus)
    {
        $this->f048fstatus = $f048fstatus;

        return $this;
    }

    /**
     * Get f048fstatus
     *
     * @return integer
     */
    public function getF048fstatus()
    {
        return $this->f048fstatus;
    }

    /**
     * Set f048fcreatedBy
     *
     * @param integer $f048fcreatedBy
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fcreatedBy($f048fcreatedBy)
    {
        $this->f048fcreatedBy = $f048fcreatedBy;

        return $this;
    }

    /**
     * Get f048fcreatedBy
     *
     * @return integer
     */
    public function getF048fcreatedBy()
    {
        return $this->f048fcreatedBy;
    }

    /**
     * Set f048fupdatedBy
     *
     * @param integer $f048fupdatedBy
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fupdatedBy($f048fupdatedBy)
    {
        $this->f048fupdatedBy = $f048fupdatedBy;

        return $this;
    }

    /**
     * Get f048fupdatedBy
     *
     * @return integer
     */
    public function getF048fupdatedBy()
    {
        return $this->f048fupdatedBy;
    }

    /**
     * Set f048fcreatedDtTm
     *
     * @param \DateTime $f048fcreatedDtTm
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fcreatedDtTm($f048fcreatedDtTm)
    {
        $this->f048fcreatedDtTm = $f048fcreatedDtTm;

        return $this;
    }

    /**
     * Get f048fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fcreatedDtTm()
    {
        return $this->f048fcreatedDtTm;
    }

    /**
     * Set f048fupdatedDtTm
     *
     * @param \DateTime $f048fupdatedDtTm
     *
     * @return T048ffeeStructureProgram
     */
    public function setF048fupdatedDtTm($f048fupdatedDtTm)
    {
        $this->f048fupdatedDtTm = $f048fupdatedDtTm;

        return $this;
    }

    /**
     * Get f048fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF048fupdatedDtTm()
    {
        return $this->f048fupdatedDtTm;
    }
}
