<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T071fvoucher
 *
 * @ORM\Table(name="t071fvoucher")
 * @ORM\Entity(repositoryClass="Application\Repository\VoucherRepository")
 */
class T071fvoucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f071fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f071fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fvoucher_number", type="string", length=50, nullable=false)
     */
    private $f071fvoucherNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f071fid_customer", type="integer", nullable=true)
     */
    private $f071fidCustomer;

    

    /**
     * @var string
     *
     * @ORM\Column(name="f071fvoucher_type", type="string", length=50, nullable=false)
     */
    private $f071fvoucherType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fvoucher_date", type="datetime", nullable=false)
     */
    private $f071fvoucherDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fapproved_by", type="integer", nullable=false)
     */
    private $f071fapprovedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fstatus", type="integer", nullable=false)
     */
    private $f071fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fcreated_by", type="integer", nullable=false)
     */
    private $f071fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fupdated_by", type="integer", nullable=false)
     */
    private $f071fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f071fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f071fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f071fvoucher_total", type="integer", nullable=false)
     */
    private $f071fvoucherTotal;

     /**
     * @var integer
     *
     * @ORM\Column(name="f071fid_financial_year", type="integer", nullable=false)
     */
    private $f071fidFinancialYear;

    /**
     * Get f071fid
     *
     * @return integer
     */
    public function getF071fid()
    {
        return $this->f071fid;
    }

    /**
     * Set f071fvoucherNumber
     *
     * @param string $f071fvoucherNumber
     *
     * @return T071fvoucher
     */
    public function setF071fvoucherNumber($f071fvoucherNumber)
    {
        $this->f071fvoucherNumber = $f071fvoucherNumber;

        return $this;
    }

    /**
     * Get f071fvoucherNumber
     *
     * @return string
     */
    public function getF071fvoucherNumber()
    {
        return $this->f071fvoucherNumber;
    }

    /**
     * Set f071fidCustomer
     *
     * @param integer $f071fidCustomer
     *
     * @return T071fvoucher
     */
    public function setF071fidCustomer($f071fidCustomer)
    {
        $this->f071fidCustomer = $f071fidCustomer;

        return $this;
    }

    /**
     * Get f071fidCustomer
     *
     * @return integer
     */
    public function getF071fidCustomer()
    {
        return $this->f071fidCustomer;
    }

    /**
     * Set f071fvoucherType
     *
     * @param string $f071fvoucherType
     *
     * @return T071fvoucher
     */
    public function setF071fvoucherType($f071fvoucherType)
    {
        $this->f071fvoucherType = $f071fvoucherType;

        return $this;
    }

    /**
     * Get f071fvoucherType
     *
     * @return string
     */
    public function getF071fvoucherType()
    {
        return $this->f071fvoucherType;
    }

    /**
     * Set f071fvoucherDate
     *
     * @param \DateTime $f071fvoucherDate
     *
     * @return T071fvoucher
     */
    public function setF071fvoucherDate($f071fvoucherDate)
    {
        $this->f071fvoucherDate = $f071fvoucherDate;

        return $this;
    }

    /**
     * Get f071fvoucherDate
     *
     * @return \DateTime
     */
    public function getF071fvoucherDate()
    {
        return $this->f071fvoucherDate;
    }

    /**
     * Set f071fapprovedBy
     *
     * @param integer $f071fapprovedBy
     *
     * @return T071fvoucher
     */
    public function setF071fapprovedBy($f071fapprovedBy)
    {
        $this->f071fapprovedBy = $f071fapprovedBy;

        return $this;
    }

    /**
     * Get f071fapprovedBy
     *
     * @return integer
     */
    public function getF071fapprovedBy()
    {
        return $this->f071fapprovedBy;
    }

    /**
     * Set f071fstatus
     *
     * @param integer $f071fstatus
     *
     * @return T071fvoucher
     */
    public function setF071fstatus($f071fstatus)
    {
        $this->f071fstatus = $f071fstatus;

        return $this;
    }

    /**
     * Get f071fstatus
     *
     * @return integer
     */
    public function getF071fstatus()
    {
        return $this->f071fstatus;
    }

    /**
     * Set f071fcreatedBy
     *
     * @param integer $f071fcreatedBy
     *
     * @return T071fvoucher
     */
    public function setF071fcreatedBy($f071fcreatedBy)
    {
        $this->f071fcreatedBy = $f071fcreatedBy;

        return $this;
    }

    /**
     * Get f071fcreatedBy
     *
     * @return integer
     */
    public function getF071fcreatedBy()
    {
        return $this->f071fcreatedBy;
    }

    /**
     * Set f071fupdatedBy
     *
     * @param integer $f071fupdatedBy
     *
     * @return T071fvoucher
     */
    public function setF071fupdatedBy($f071fupdatedBy)
    {
        $this->f071fupdatedBy = $f071fupdatedBy;

        return $this;
    }

    /**
     * Get f071fupdatedBy
     *
     * @return integer
     */
    public function getF071fupdatedBy()
    {
        return $this->f071fupdatedBy;
    }

    /**
     * Set f071fcreatedDtTm
     *
     * @param \DateTime $f071fcreatedDtTm
     *
     * @return T071fvoucher
     */
    public function setF071fcreatedDtTm($f071fcreatedDtTm)
    {
        $this->f071fcreatedDtTm = $f071fcreatedDtTm;

        return $this;
    }

    /**
     * Get f071fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fcreatedDtTm()
    {
        return $this->f071fcreatedDtTm;
    }

    /**
     * Set f071fupdatedDtTm
     *
     * @param \DateTime $f071fupdatedDtTm
     *
     * @return T071fvoucher
     */
    public function setF071fupdatedDtTm($f071fupdatedDtTm)
    {
        $this->f071fupdatedDtTm = $f071fupdatedDtTm;

        return $this;
    }

    /**
     * Get f071fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF071fupdatedDtTm()
    {
        return $this->f071fupdatedDtTm;
    }

    /**
     * Set f071fvoucherTotal
     *
     * @param integer $f071fvoucherTotal
     *
     * @return T071fvoucher
     */
    public function setF071fvoucherTotal($f071fvoucherTotal)
    {
        $this->f071fvoucherTotal = $f071fvoucherTotal;

        return $this;
    }

     /**
     * Set f071fidFinancialYear
     *
     * @param integer $f071fidFinancialYear
     *
     * @return T071fvoucher
     */
    public function setF071fidFinancialYear($f071fidFinancialYear)
    {
        $this->f071fidFinancialYear = $f071fidFinancialYear;

        return $this;
    }

    /**
     * Get f071fidFinancialYear
     *
     * @return integer
     */
    public function getF071fidFinancialYear()
    {
        return $this->f071fidFinancialYear;
    }
}

