<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T022froleDetails
 *
 * @ORM\Table(name="t022frole_details", indexes={@ORM\Index(name="f022fid_role", columns={"f022fid_role"}), @ORM\Index(name="f022fid_menu", columns={"f022fid_menu"}), @ORM\Index(name="f022fupdated_by", columns={"f022fupdated_by"}), @ORM\Index(name="f022fcreated_by", columns={"f022fcreated_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\RoleDetailsRepository")
 */
class T022froleDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f022fid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f022fstatus", type="boolean", nullable=false)
     */
    private $f022fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f022fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f022fupdatedDtTm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid_role", type="integer", nullable=false)
     */
    private $f022fidRole;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fid_menu", type="integer", nullable=false)
     */
    private $f022fidMenu;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fupdated_by", type="integer", nullable=false)
     */
    private $f022fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f022fcreated_by", type="integer", nullable=false)
     */
    private $f022fcreatedBy;
  

    /**
     * Get f022fid
     *
     * @return integer
     */
    public function getF022fid()
    {
        return $this->f022fid;
    }

    /**
     * Set f022fstatus
     *
     * @param boolean $f022fstatus
     *
     * @return T022froleDetails
     */
    public function setF022fstatus($f022fstatus)
    {
        $this->f022fstatus = $f022fstatus;

        return $this;
    }

    /**
     * Get f022fstatus
     *
     * @return boolean
     */
    public function getF022fstatus()
    {
        return $this->f022fstatus;
    }

    /**
     * Set f022fcreatedDtTm
     *
     * @param \DateTime $f022fcreatedDtTm
     *
     * @return T022froleDetails
     */
    public function setF022fcreatedDtTm($f022fcreatedDtTm)
    {
        $this->f022fcreatedDtTm = $f022fcreatedDtTm;

        return $this;
    }

    /**
     * Get f022fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fcreatedDtTm()
    {
        return $this->f022fcreatedDtTm;
    }

    /**
     * Set f022fupdatedDtTm
     *
     * @param \DateTime $f022fupdatedDtTm
     *
     * @return T022froleDetails
     */
    public function setF022fupdatedDtTm($f022fupdatedDtTm)
    {
        $this->f022fupdatedDtTm = $f022fupdatedDtTm;

        return $this;
    }

    /**
     * Get f022fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF022fupdatedDtTm()
    {
        return $this->f022fupdatedDtTm;
    }

    /**
     * Set f022fidRole
     *
     * @param integer $f022fidRole
     *
     * @return T022froleDetails
     */
    public function setF022fidRole($f022fidRole)
    {
        $this->f022fidRole = $f022fidRole;

        return $this;
    }

    
    public function getF022fidRole()
    {
        return $this->f022fidRole;
    }

    /**
     * Set f022fidMenu
     *
     * @param integer $f022fidMenu
     *
     * @return T022froleDetails
     */
    public function setF022fidMenu($f022fidMenu )
    {
        $this->f022fidMenu = $f022fidMenu;

        return $this;
    }

    public function getF022fidMenu()
    {
        return $this->f022fidMenu;
    }

    /**
     * Set f022fupdatedBy
     *
     * @param integer $f022fupdatedBy
     *
     * @return T022froleDetails
     */
    public function setF022fupdatedBy($f022fupdatedBy )
    {
        $this->f022fupdatedBy = $f022fupdatedBy;

        return $this;
    }

    
    public function getF022fupdatedBy()
    {
        return $this->f022fupdatedBy;
    }

    /**
     * Set f022fcreatedBy
     *
     * @param integer $f022fcreatedBy
     *
     * @return T022froleDetails
     */
    public function setF022fcreatedBy($f022fcreatedBy )
    {
        $this->f022fcreatedBy = $f022fcreatedBy;

        return $this;
    }

    /**
     * Get f022fcreatedBy
     *
     * @return \Application\Entity\T014fuser
     */
    public function getF022fcreatedBy()
    {
        return $this->f022fcreatedBy;
    }
}
