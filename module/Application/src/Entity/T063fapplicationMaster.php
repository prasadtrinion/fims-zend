<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fapplicationMaster
 *
 * @ORM\Table(name="t063fapplication_master")
 * @ORM\Entity(repositoryClass="Application\Repository\InvestmentApplicationRepository")
 */
class T063fapplicationMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fapplication_id", type="string", length=25, nullable=true)
     */
    private $f063fapplicationId;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fdescription", type="string", length=255, nullable=true)
     */
    private $f063fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063finvestment_amount", type="integer",  nullable=false)
     */
    private $f063finvestmentAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fstatus", type="integer", nullable=false)
     */
    private $f063fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fis_online", type="integer", nullable=true)
     */
    private $f063fisOnline;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=false)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=false)
     */
    private $f063fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fupdatedDtTm;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fuum_bank", type="integer", nullable=true)
     */
    private $f063fuumBank;


    /**
     * @var integer
     *
     * @ORM\Column(name="f063ffile_upload", type="string", length=60, nullable=true)
     */
    private $f063ffileUpload;

    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063fapplicationId
     *
     * @param string $f063fapplicationId
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapplicationId($f063fapplicationId)
    {
        $this->f063fapplicationId = $f063fapplicationId;

        return $this;
    }

    /**
     * Get f063fapplicationId
     *
     * @return string
     */
    public function getF063fapplicationId()
    {
        return $this->f063fapplicationId;
    }

    /**
     * Set f063fdescription
     *
     * @param string $f063fdescription
     *
     * @return T063fapplicationMaster
     */
    public function setF063fdescription($f063fdescription)
    {
        $this->f063fdescription = $f063fdescription;

        return $this;
    }

    /**
     * Get f063fdescription
     *
     * @return string
     */
    public function getF063fdescription()
    {
        return $this->f063fdescription;
    }

    /**
     * Set f063finvestmentAmount
     *
     * @param integer $f063finvestmentAmount
     *
     * @return T063fapplicationMaster
     */
    public function setF063finvestmentAmount($f063finvestmentAmount)
    {
        $this->f063finvestmentAmount = $f063finvestmentAmount;

        return $this;
    }

    /**
     * Get f063finvestmentAmount
     *
     * @return integer
     */
    public function getF063finvestmentAmount()
    {
        return $this->f063finvestmentAmount;
    }

    
    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fapplicationMaster
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fapplicationMaster
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }

    /**
     * Set f063fisOnline
     *
     * @param integer $f063fisOnline
     *
     * @return T063fapplicationMaster
     */
    public function setF063fisOnline($f063fisOnline)
    {
        $this->f063fisOnline = $f063fisOnline;

        return $this;
    }

    /**
     * Get f063fisOnline
     *
     * @return integer
     */
    public function getF063fisOnline()
    {
        return $this->f063fisOnline;
    }

    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fapplicationMaster
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fapplicationMaster
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fapplicationMaster
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063fapproved1Status
     *
     * @param integer $f063fapproved1Status
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved1Status($f063fapproved1Status)
    {
        $this->f063fapproved1Status = $f063fapproved1Status;

        return $this;
    }

    /**
     * Get f063fapproved1Status
     *
     * @return integer
     */
    public function getF063fapproved1Status()
    {
        return $this->f063fapproved1Status;
    }

    /**
     * Set f063fapproved2Status
     *
     * @param integer $f063fapproved2Status
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved2Status($f063fapproved2Status)
    {
        $this->f063fapproved2Status = $f063fapproved2Status;

        return $this;
    }

    /**
     * Get f063fapproved2Status
     *
     * @return integer
     */
    public function getF063fapproved2Status()
    {
        return $this->f063fapproved2Status;
    }

    /**
     * Set f063fapproved3Status
     *
     * @param integer $f063fapproved3Status
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved3Status($f063fapproved3Status)
    {
        $this->f063fapproved3Status = $f063fapproved3Status;

        return $this;
    }

    /**
     * Get f063fapproved3Status
     *
     * @return integer
     */
    public function getF063fapproved3Status()
    {
        return $this->f063fapproved3Status;
    }

    /**
     * Set f063freason1
     *
     * @param string $f063freason1
     *
     * @return T063fapplicationMaster
     */
    public function setF063freason1($f063freason1)
    {
        $this->f063freason1 = $f063freason1;

        return $this;
    }

    /**
     * Get f063freason1
     *
     * @return string
     */
    public function getF063freason1()
    {
        return $this->f063freason1;
    }

    /**
     * Set f063freason2
     *
     * @param string $f063freason2
     *
     * @return T063fapplicationMaster
     */
    public function setF063freason2($f063freason2)
    {
        $this->f063freason2 = $f063freason2;

        return $this;
    }

    /**
     * Get f063freason2
     *
     * @return string
     */
    public function getF063freason2()
    {
        return $this->f063freason2;
    }

    /**
     * Set f063freason3
     *
     * @param string $f063freason3
     *
     * @return T063fapplicationMaster
     */
    public function setF063freason3($f063freason3)
    {
        $this->f063freason3 = $f063freason3;

        return $this;
    }

    /**
     * Get f063freason3
     *
     * @return string
     */
    public function getF063freason3()
    {
        return $this->f063freason3;
    }

    /**
     * Set f063fapproved1By
     *
     * @param integer $f063fapproved1By
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved1By($f063fapproved1By)
    {
        $this->f063fapproved1By = $f063fapproved1By;

        return $this;
    }

    /**
     * Get f063fapproved1By
     *
     * @return integer
     */
    public function getF063fapproved1By()
    {
        return $this->f063fapproved1By;
    }

    /**
     * Set f063fapproved2By
     *
     * @param integer $f063fapproved2By
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved2By($f063fapproved2By)
    {
        $this->f063fapproved2By = $f063fapproved2By;

        return $this;
    }

    /**
     * Get f063fapproved2By
     *
     * @return integer
     */
    public function getF063fapproved2By()
    {
        return $this->f063fapproved2By;
    }

    /**
     * Set f063fapproved3By
     *
     * @param integer $f063fapproved3By
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved3By($f063fapproved3By)
    {
        $this->f063fapproved3By = $f063fapproved3By;

        return $this;
    }

    /**
     * Get f063fapproved3By
     *
     * @return integer
     */
    public function getF063fapproved3By()
    {
        return $this->f063fapproved3By;
    }

    /**
     * Set f063fapproved1UpdatedBy
     *
     * @param integer $f063fapproved1UpdatedBy
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved1UpdatedBy($f063fapproved1UpdatedBy)
    {
        $this->f063fapproved1UpdatedBy = $f063fapproved1UpdatedBy;

        return $this;
    }

    /**
     * Get f063fapproved1UpdatedBy
     *
     * @return integer
     */
    public function getF063fapproved1UpdatedBy()
    {
        return $this->f063fapproved1UpdatedBy;
    }

    /**
     * Set f063fapproved2UpdatedBy
     *
     * @param integer $f063fapproved2UpdatedBy
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved2UpdatedBy($f063fapproved2UpdatedBy)
    {
        $this->f063fapproved2UpdatedBy = $f063fapproved2UpdatedBy;

        return $this;
    }

    /**
     * Get f063fapproved2UpdatedBy
     *
     * @return integer
     */
    public function getF063fapproved2UpdatedBy()
    {
        return $this->f063fapproved2UpdatedBy;
    }

    /**
     * Set f063fapproved3UpdatedBy
     *
     * @param integer $f063fapproved3UpdatedBy
     *
     * @return T063fapplicationMaster
     */
    public function setF063fapproved3UpdatedBy($f063fapproved3UpdatedBy)
    {
        $this->f063fapproved3UpdatedBy = $f063fapproved3UpdatedBy;

        return $this;
    }

    /**
     * Get f063fapproved3UpdatedBy
     *
     * @return integer
     */
    public function getF063fapproved3UpdatedBy()
    {
        return $this->f063fapproved3UpdatedBy;
    }

    /**
     * Set f063ffileUpload
     *
     * @param integer $f063ffileUpload
     *
     * @return T063fapplicationMaster
     */
    public function setF063ffileUpload($f063ffileUpload)
    {
        $this->f063ffileUpload = $f063ffileUpload;

        return $this;
    }

    /**
     * Get f063ffileUpload
     *
     * @return integer
     */
    public function getF063ffileUpload()
    {
        return $this->f063ffileUpload;
    }


     /**
     * Set f063fuumBank
     *
     * @param integer $f063fuumBank
     *
     * @return T063fapplicationMaster
     */
    public function setF063fuumBank($f063fuumBank)
    {
        $this->f063fuumBank = $f063fuumBank;

        return $this;
    }

    /**
     * Get f063fuumBank
     *
     * @return integer
     */
    public function getF063fuumBank()
    {
        return $this->f063fuumBank;
    }

}
