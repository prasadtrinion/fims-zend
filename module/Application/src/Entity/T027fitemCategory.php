<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T027fitemCategory
 *
 * @ORM\Table(name="t027fitem_category", uniqueConstraints={@ORM\UniqueConstraint(name="f027fcategory_name_UNIQUE", columns={"f027fcategory_name"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ItemCategoryRepository")
 */
class T027fitemCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f027fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f027fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f027fcategory_name", type="string", length=50, nullable=false)
     */
    private $f027fcategoryName;

     /**
     * @var string
     *
     * @ORM\Column(name="f027fcode", type="string", length=50, nullable=false)
     */
    private $f027fcode;

    /**
     * @var string
     *
     * @ORM\Column(name="f027fmaintenance", type="integer", nullable=false)
     */
    private $f027fmaintenance;

    /**
     * @var integer
     *
     * @ORM\Column(name="f027fstatus", type="integer", nullable=false)
     */
    private $f027fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f027fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f027fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f027fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f027fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f027fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f027fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f027fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f027fcreatedBy;


    /**
     * Get f027fid
     *
     * @return integer
     */
    public function getF027fid()
    {
        return $this->f027fid;
    }

    /**
     * Set f027fcategoryName
     *
     * @param string $f027fcategoryName
     *
     * @return T027fitemCategory
     */
    public function setF027fcategoryName($f027fcategoryName)
    {
        $this->f027fcategoryName = $f027fcategoryName;

        return $this;
    }

    /**
     * Get f027fcategoryName
     *
     * @return string
     */
    public function getF027fcategoryName()
    {
        return $this->f027fcategoryName;
    }

     /**
     * Set f027fcode
     *
     * @param string $f027fcode
     *
     * @return T027fitemCategory
     */
    public function setF027fcode($f027fcode)
    {
        $this->f027fcode = $f027fcode;

        return $this;
    }

    /**
     * Get f027fcode
     *
     * @return string
     */
    public function getF027fcode()
    {
        return $this->f027fcode;
    }

     /**
     * Set f027fmaintenance
     *
     * @param string $f027fmaintenance
     *
     * @return T027fitemCategory
     */
    public function setF027fmaintenance($f027fmaintenance)
    {
        $this->f027fmaintenance = $f027fmaintenance;

        return $this;
    }

    /**
     * Get f027fmaintenance
     *
     * @return string
     */
    public function getF027fmaintenance()
    {
        return $this->f027fmaintenance;
    }


    /**
     * Set f027fstatus
     *
     * @param integer $f027fstatus
     *
     * @return T027fitemCategory
     */
    public function setF027fstatus($f027fstatus)
    {
        $this->f027fstatus = $f027fstatus;

        return $this;
    }

    /**
     * Get f027fstatus
     *
     * @return integer
     */
    public function getF027fstatus()
    {
        return $this->f027fstatus;
    }

    /**
     * Set f027fcreatedDtTm
     *
     * @param \DateTime $f027fcreatedDtTm
     *
     * @return T027fitemCategory
     */
    public function setF027fcreatedDtTm($f027fcreatedDtTm)
    {
        $this->f027fcreatedDtTm = $f027fcreatedDtTm;

        return $this;
    }

    /**
     * Get f027fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getf027fcreatedDtTm()
    {
        return $this->f027fcreatedDtTm;
    }

    /**
     * Set f027fupdatedDtTm
     *
     * @param \DateTime $f027fupdatedDtTm
     *
     * @return T027fitemCategory
     */
    public function setf027fupdatedDtTm($f027fupdatedDtTm)
    {
        $this->f027fupdatedDtTm = $f027fupdatedDtTm;

        return $this;
    }

    /**
     * Get f027fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getf027fupdatedDtTm()
    {
        return $this->f027fupdatedDtTm;
    }

    /**
     * Set f027fupdatedBy
     *
     * @param integer $f027fupdatedBy
     *
     * @return T027fitemCategory
     */
    public function setf027fupdatedBy($f027fupdatedBy)
    {
        $this->f027fupdatedBy = $f027fupdatedBy;

        return $this;
    }

    
    public function getf027fupdatedBy()
    {
        return $this->f027fupdatedBy;
    }

    /**
     * Set f027fcreatedBy
     *
     * @param  integer $f027fcreatedBy
     *
     * @return T027fitemCategory
     */
    public function setf027fcreatedBy($f027fcreatedBy)
    {
        $this->f027fcreatedBy = $f027fcreatedBy;

        return $this;
    }

    
    public function getf027fcreatedBy()
    {
        return $this->f027fcreatedBy;
    }
}
