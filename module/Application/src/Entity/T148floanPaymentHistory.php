<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T148floanPaymentHistory
 *
 * @ORM\Table(name="t148floan_payment_history")
 * @ORM\Entity(repositoryClass="Application\Repository\LoanPaymentHistoryRepository")
 */
class T148floanPaymentHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f148fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f148fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f148fid_loan", type="integer", nullable=false)
     */
    private $f148fidLoan;

    /**
     * @var string
     *
     * @ORM\Column(name="f148fstaff", type="integer", nullable=false)
     */
    private $f148fstaff;

    /**
     * @var string
     *
     * @ORM\Column(name="f148fdeduct_date", type="datetime", nullable=false)
     */
    private $f148fdeductDate;

    /**
     * @var string
     *
     * @ORM\Column(name="f148famount", type="integer", nullable=false)
     */
    private $f148famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f148fstatus", type="integer", nullable=false)
     */
    private $f148fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f148fcreated_by", type="integer", nullable=false)
     */
    private $f148fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f148fupdated_by", type="integer", nullable=false)
     */
    private $f148fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f148fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f148fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f148fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f148fupdatedDtTm;

    /**
     * Get f148fid
     *
     * @return integer
     */
    public function getF148fid()
    {
        return $this->f148fid;
    }

    /**
     * Set f148fidLoan
     *
     * @param string $f148fidLoan
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fidLoan($f148fidLoan)
    {
        $this->f148fidLoan = $f148fidLoan;

        return $this;
    }

    /**
     * Get f148fidLoan
     *
     * @return string
     */
    public function getF148fidLoan()
    {
        return $this->f148fidLoan;
    }

    /**
     * Set f148fstaff
     *
     * @param string $f148fstaff
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fstaff($f148fstaff)
    {
        $this->f148fstaff = $f148fstaff;

        return $this;
    }

    /**
     * Get f148fstaff
     *
     * @return string
     */
    public function getF148fstaff()
    {
        return $this->f148fstaff;
    }

      /**
     * Set f148famount
     *
     * @param string $f148famount
     *
     * @return T148floanPaymentHistory
     */
    public function setF148famount($f148famount)
    {
        $this->f148famount = $f148famount;

        return $this;
    }

    /**
     * Get f148famount
     *
     * @return string
     */
    public function getF148famount()
    {
        return $this->f148famount;
    }


      /**
     * Set f148fdeductDate
     *
     * @param string $f148fdeductDate
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fdeductDate($f148fdeductDate)
    {
        $this->f148fdeductDate = $f148fdeductDate;

        return $this;
    }

    /**
     * Get f148fdeductDate
     *
     * @return string
     */
    public function getF148fdeductDate()
    {
        return $this->f148fdeductDate;
    }

    

    /**
     * Set f148fcreatedBy
     *
     * @param integer $f148fcreatedBy
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fcreatedBy($f148fcreatedBy)
    {
        $this->f148fcreatedBy = $f148fcreatedBy;

        return $this;
    }

    /**
     * Get f148fcreatedBy
     *
     * @return integer
     */
    public function getF148fcreatedBy()
    {
        return $this->f148fcreatedBy;
    }

    /**
     * Set f148fupdatedBy
     *
     * @param integer $f148fupdatedBy
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fupdatedBy($f148fupdatedBy)
    {
        $this->f148fupdatedBy = $f148fupdatedBy;

        return $this;
    }

    /**
     * Get f148fupdatedBy
     *
     * @return integer
     */
    public function getF148fupdatedBy()
    {
        return $this->f148fupdatedBy;
    }

    /**
     * Set f148fcreatedDtTm
     *
     * @param \DateTime $f148fcreatedDtTm
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fcreatedDtTm($f148fcreatedDtTm)
    {
        $this->f148fcreatedDtTm = $f148fcreatedDtTm;

        return $this;
    }

    /**
     * Get f148fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF148fcreatedDtTm()
    {
        return $this->f148fcreatedDtTm;
    }

    /**
     * Set f148fupdatedDtTm
     *
     * @param \DateTime $f148fupdatedDtTm
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fupdatedDtTm($f148fupdatedDtTm)
    {
        $this->f148fupdatedDtTm = $f148fupdatedDtTm;

        return $this;
    }

    /**
     * Get f148fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF148fupdatedDtTm()
    {
        return $this->f148fupdatedDtTm;
    }

    /**
     * Set f148fstatus
     *
     * @param boolean $f148fstatus
     *
     * @return T148floanPaymentHistory
     */
    public function setF148fstatus($f148fstatus)
    {
        $this->f148fstatus = $f148fstatus;

        return $this;
    }

    /**
     * Get f148fstatus
     *
     * @return boolean
     */
    public function getF148fstatus()
    {
        return $this->f148fstatus;
    }

}
