<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T081fsalaryDetails
 *
 * @ORM\Table(name="t081fsalary_details")
 * @ORM\Entity(repositoryClass="Application\Repository\SalaryRepository")
 */
class T081fsalaryDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f081fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fid_salary", type="integer", nullable=true)
     */
    private $f081fidSalary;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstaff_type", type="integer", nullable=true)
     */
    private $f081fstaffType;

    /**
     * @var string
     *
     * @ORM\Column(name="f081faccount_code_debit", type="string", length=100, nullable=true)
     */
    private $f081faccountCodeDebit;

    /**
     * @var string
     *
     * @ORM\Column(name="f081faccount_code_credit", type="string", length=100, nullable=true)
     */
    private $f081faccountCodeCredit;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstaff_status", type="integer", nullable=true)
     */
    private $f081fstaffStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fstatus", type="integer", nullable=true)
     */
    private $f081fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fcreated_by", type="integer", nullable=true)
     */
    private $f081fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f081fupdated_by", type="integer", nullable=true)
     */
    private $f081fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f081fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f081fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f081fupdatedDtTm;



    /**
     * Get f081fidDetails
     *
     * @return integer
     */
    public function getF081fidDetails()
    {
        return $this->f081fidDetails;
    }

    /**
     * Set f081fidSalary
     *
     * @param integer $f081fidSalary
     *
     * @return T081fsalaryDetails
     */
    public function setF081fidSalary($f081fidSalary)
    {
        $this->f081fidSalary = $f081fidSalary;

        return $this;
    }

    /**
     * Get f081fidSalary
     *
     * @return integer
     */
    public function getF081fidSalary()
    {
        return $this->f081fidSalary;
    }

     /**
     * Set f081fstaffType
     *
     * @param integer $f081fstaffType
     *
     * @return T081fsalaryDetails
     */
    public function setF081fstaffType($f081fstaffType)
    {
        $this->f081fstaffType = $f081fstaffType;

        return $this;
    }

    /**
     * Get f081fstaffType
     *
     * @return integer
     */
    public function getF081fstaffType()
    {
        return $this->f081fstaffType;
    }

   
    /**
     * Set f081faccountCodeCredit
     *
     * @param string $f081faccountCodeCredit
     *
     * @return T081fsalaryDetails
     */
    public function setF081faccountCodeCredit($f081faccountCodeCredit)
    {
        $this->f081faccountCodeCredit = $f081faccountCodeCredit;

        return $this;
    }

    /**
     * Get f081faccountCodeCredit
     *
     * @return string
     */
    public function getF081faccountCodeCredit()
    {
        return $this->f081faccountCodeCredit;
    }

    /**
     * Set f081faccountCodeDebit
     *
     * @param string $f081faccountCodeDebit
     *
     * @return T081fsalaryDetails
     */
    public function setF081faccountCodeDebit($f081faccountCodeDebit)
    {
        $this->f081faccountCodeDebit = $f081faccountCodeDebit;

        return $this;
    }

    /**
     * Get f081faccountCodeDebit
     *
     * @return string
     */
    public function getF081faccountCodeDebit()
    {
        return $this->f081faccountCodeDebit;
    }

    /**
     * Set f081fstaffStatus
     *
     * @param string $f081fstaffStatus
     *
     * @return T081fsalaryDetails
     */
    public function setF081fstaffStatus($f081fstaffStatus)
    {
        $this->f081fstaffStatus = $f081fstaffStatus;

        return $this;
    }

    /**
     * Get f081fstaffStatus
     *
     * @return string
     */
    public function getF081fstaffStatus()
    {
        return $this->f081fstaffStatus;
    }


    /**
     * Set f081fstatus
     *
     * @param integer $f081fstatus
     *
     * @return T081fsalaryDetails
     */
    public function setF081fstatus($f081fstatus)
    {
        $this->f081fstatus = $f081fstatus;

        return $this;
    }

    /**
     * Get f081fstatus
     *
     * @return integer
     */
    public function getF081fstatus()
    {
        return $this->f081fstatus;
    }

    /**
     * Set f081fcreatedBy
     *
     * @param integer $f081fcreatedBy
     *
     * @return T081fsalaryDetails
     */
    public function setF081fcreatedBy($f081fcreatedBy)
    {
        $this->f081fcreatedBy = $f081fcreatedBy;

        return $this;
    }

    /**
     * Get f081fcreatedBy
     *
     * @return integer
     */
    public function getF081fcreatedBy()
    {
        return $this->f081fcreatedBy;
    }

    /**
     * Set f081fupdatedBy
     *
     * @param integer $f081fupdatedBy
     *
     * @return T081fsalaryDetails
     */
    public function setF081fupdatedBy($f081fupdatedBy)
    {
        $this->f081fupdatedBy = $f081fupdatedBy;

        return $this;
    }

    /**
     * Get f081fupdatedBy
     *
     * @return integer
     */
    public function getF081fupdatedBy()
    {
        return $this->f081fupdatedBy;
    }

    /**
     * Set f081fcreatedDtTm
     *
     * @param \DateTime $f081fcreatedDtTm
     *
     * @return T081fsalaryDetails
     */
    public function setF081fcreatedDtTm($f081fcreatedDtTm)
    {
        $this->f081fcreatedDtTm = $f081fcreatedDtTm;

        return $this;
    }

    /**
     * Get f081fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fcreatedDtTm()
    {
        return $this->f081fcreatedDtTm;
    }

    /**
     * Set f081fupdatedDtTm
     *
     * @param \DateTime $f081fupdatedDtTm
     *
     * @return T081fsalaryDetails
     */
    public function setF081fupdatedDtTm($f081fupdatedDtTm)
    {
        $this->f081fupdatedDtTm = $f081fupdatedDtTm;

        return $this;
    }

    /**
     * Get f081fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF081fupdatedDtTm()
    {
        return $this->f081fupdatedDtTm;
    }
}
