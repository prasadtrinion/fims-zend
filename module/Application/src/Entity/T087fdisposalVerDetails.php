<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T087fdisposalVerDetails
 *
 * @ORM\Table(name="t087fdisposal_ver_details")
* @ORM\Entity(repositoryClass="Application\Repository\DisposalVerificationRepository")
 */
class T087fdisposalVerDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f087fidDetails;


    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_disposal", type="integer", nullable=false)
     */
    private $f087fidDisposal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fid_asset", type="integer", nullable=false)
     */
    private $f087fidAsset;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fasset_code", type="string",length=105, nullable=false)
     */
    private $f087fassetCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f087fdescription", type="string", length=100, nullable=true)
     */
    private $f087fdescription;

     /**
     * @var string
     *
     * @ORM\Column(name="f087freason", type="string", length=100, nullable=true)
     */
    private $f087freason;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fnet_value", type="integer", nullable=true)
     */
    private $f087fnetValue;


    /**
     * @var integer
     *
     * @ORM\Column(name="f087ftype", type="integer", nullable=true)
     */
    private $f087ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fstatus", type="integer", nullable=true)
     */
    private $f087fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fcreated_by", type="integer", nullable=true)
     */
    private $f087fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f087fupdated_by", type="integer", nullable=true)
     */
    private $f087fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f087fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f087fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f087fupdatedDtTm;

    /**
     * Get f087fidDetails
     *
     * @return integer
     */
    public function getF087fidDetails()
    {
        return $this->f087fidDetails;
    }


    /**
     * Set f087fidDisposal
     *
     * @param integer $f087fidDisposal
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fidDisposal($f087fidDisposal)
    {
        $this->f087fidDisposal = $f087fidDisposal;

        return $this;
    }

    /**
     * Get f087fidDisposal
     *
     * @return integer
     */
    public function getF087fidDisposal()
    {
        return $this->f087fidDisposal;
    }

    /**
     * Set f087fassetCode
     *
     * @param integer $f087fassetCode
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fassetCode($f087fassetCode)
    {
        $this->f087fassetCode = $f087fassetCode;

        return $this;
    }

    /**
     * Get f087fassetCode
     *
     * @return integer
     */
    public function getF087fassetCode()
    {
        return $this->f087fassetCode;
    }

    /**
     * Set f087fdescription
     *
     * @param string $f087fdescription
     *
     * @return T087fdisposalDisposal
     */
    public function setF087fdescription($f087fdescription)
    {
        $this->f087fdescription = $f087fdescription;

        return $this;
    }

    /**
     * Get f087fdescription
     *
     * @return string
     */
    public function getF087fdescription()
    {
        return $this->f087fdescription;
    }

    /**
     * Set f087fcreatedBy
     *
     * @param integer $f087fcreatedBy
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fcreatedBy($f087fcreatedBy)
    {
        $this->f087fcreatedBy = $f087fcreatedBy;

        return $this;
    }

    /**
     * Get f087fcreatedBy
     *
     * @return integer
     */
    public function getF087fcreatedBy()
    {
        return $this->f087fcreatedBy;
    }


    /**
     * Set f087fnetValue
     *
     * @param integer $f087fnetValue
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fnetValue($f087fnetValue)
    {
        $this->f087fnetValue = $f087fnetValue;

        return $this;
    }

    /**
     * Get f087fnetValue
     *
     * @return integer
     */
    public function getF087fnetValue()
    {
        return $this->f087fnetValue;
    }


    /**
     * Set f087ftype
     *
     * @param integer $f087ftype
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087ftype($f087ftype)
    {
        $this->f087ftype = $f087ftype;

        return $this;
    }

    /**
     * Get f087ftype
     *
     * @return integer
     */
    public function getF087ftype()
    {
        return $this->f087ftype;
    }

    /**
     * Set f087fupdatedBy
     *
     * @param integer $f087fupdatedBy
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fupdatedBy($f087fupdatedBy)
    {
        $this->f087fupdatedBy = $f087fupdatedBy;

        return $this;
    }

    /**
     * Get f087fupdatedBy
     *
     * @return integer
     */
    public function getF087fupdatedBy()
    {
        return $this->f087fupdatedBy;
    }

    /**
     * Set f087fcreatedDtTm
     *
     * @param \DateTime $f087fcreatedDtTm
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fcreatedDtTm($f087fcreatedDtTm)
    {
        $this->f087fcreatedDtTm = $f087fcreatedDtTm;

        return $this;
    }

    /**
     * Get f087fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fcreatedDtTm()
    {
        return $this->f087fcreatedDtTm;
    }

    /**
     * Set f087fupdatedDtTm
     *
     * @param \DateTime $f087fupdatedDtTm
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fupdatedDtTm($f087fupdatedDtTm)
    {
        $this->f087fupdatedDtTm = $f087fupdatedDtTm;

        return $this;
    }

    /**
     * Get f087fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF087fupdatedDtTm()
    {
        return $this->f087fupdatedDtTm;
    }

    /**
     * Set f087fstatus
     *
     * @param integer $f087fstatus
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087fstatus($f087fstatus)
    {
        $this->f087fstatus = $f087fstatus;

        return $this;
    }

    /**
     * Get f087fstatus
     *
     * @return integer
     */
    public function getF087fstatus()
    {
        return $this->f087fstatus;
    }

     /**
     * Set f087freason
     *
     * @param integer $f087freason
     *
     * @return T087fdisposalVerDetails
     */
    public function setF087freason($f087freason)
    {
        $this->f087freason = $f087freason;

        return $this;
    }

    /**
     * Get f087freason
     *
     * @return integer
     */
    public function getF087freason()
    {
        return $this->f087freason;
    }

    /**
     * Set f087fidAsset
     *
     * @param integer $f087fidAsset
     *
     * @return T087fdisposalReqDetails
     */
    public function setF087fidAsset($f087fidAsset)
    {
        $this->f087fidAsset = $f087fidAsset;

        return $this;
    }

    /**
     * Get f087fidAsset
     *
     * @return integer
     */
    public function getF087fidAsset()
    {
        return $this->f087fidAsset;
    }
    
}
