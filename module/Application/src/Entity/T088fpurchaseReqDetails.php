<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T088fpurchaseReqDetails
 *
 * @ORM\Table(name="t088fpurchase_req_details")
 * @ORM\Entity(repositoryClass="Application\Repository\PurchaseRequisitionRepository")
 */
class T088fpurchaseReqDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f088fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_purchase", type="integer", nullable=true)
     */
    private $f088fidPurchase;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fid_item", type="integer", nullable=true)
     */
    private $f088fidItem;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fso_code", type="string", length=15, nullable=true)
     */
    private $f088fsoCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088frequired_date", type="datetime", nullable=true)
     */
    private $f088frequiredDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fquantity", type="integer", nullable=true)
     */
    private $f088fquantity;

    /**
     * @var string
     *
     * @ORM\Column(name="f088funit", type="string", length=10, nullable=true)
     */
    private $f088funit;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fprice", type="integer", nullable=true)
     */
    private $f088fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088total", type="integer",  nullable=true)
     */
    private $f088total;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088ftax_code", type="integer", nullable=true)
     */
    private $f088ftaxCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fpercentage", type="integer", nullable=true)
     */
    private $f088fpercentage;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088ftax_amount", type="integer", nullable=true)
     */
    private $f088ftaxAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088ftotal_inc_tax", type="integer", nullable=true)
     */
    private $f088ftotalIncTax;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fstatus", type="integer", nullable=true)
     */
    private $f088fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fcreated_by", type="integer", nullable=true)
     */
    private $f088fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f088fupdated_by", type="integer", nullable=true)
     */
    private $f088fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f088fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f088fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f088fupdatedDtTm;

     /**
     * @var string
     *
     * @ORM\Column(name="f088ffund_code", type="string",length=20, nullable=false)
     */
    private $f088ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f088fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f088fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088factivity_code", type="string",length=20, nullable=false)
     */
    private $f088factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088faccount_code", type="string",length=20, nullable=false)
     */
    private $f088faccountCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fbudget_fund_code", type="string",length=20, nullable=false)
     */
    private $f088fbudgetFundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f088fbudget_department_code", type="string",length=20, nullable=false)
     */
    private $f088fbudgetDepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fbudget_activity_code", type="string",length=20, nullable=false)
     */
    private $f088fbudgetActivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fbudget_account_code", type="string",length=20, nullable=false)
     */
    private $f088fbudgetAccountCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_type", type="string",length=60, nullable=true)
     */
    private $f088fitemType;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_category", type="string",length=60, nullable=true)
     */
    private $f088fitemCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_sub_category", type="string",length=60, nullable=true)
     */
    private $f088fitemSubCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fitem_code", type="string",length=60, nullable=true)
     */
    private $f088fitemCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fcategory_type", type="string",length=60, nullable=true)
     */
    private $f088fcategoryType;

    /**
     * @var string
     *
     * @ORM\Column(name="f088fasset_code", type="string",length=255, nullable=true)
     */
    private $f088fassetCode;


    /**
     * Get f088fidDetails
     *
     * @return integer
     */
    public function getF088fidDetails()
    {
        return $this->f088fidDetails;
    }

    /**
     * Set f088fitemType
     *
     * @param integer $f088fitemType
     *
     * @return T088fgrnDetails
     */
    public function setF088fitemType($f088fitemType)
    {
        $this->f088fitemType = $f088fitemType;

        return $this;
    }

    /**
     * Get f088fitemType
     *
     * @return integer
     */
    public function getF088fitemType()
    {
        return $this->f088fitemType;
    }

    /**
     * Set f088fidPurchase
     *
     * @param integer $f088fidPurchase
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fidPurchase($f088fidPurchase)
    {
        $this->f088fidPurchase = $f088fidPurchase;

        return $this;
    }

    /**
     * Get f088fidPurchase
     *
     * @return integer
     */
    public function getF088fidPurchase()
    {
        return $this->f088fidPurchase;
    }

    /**
     * Set f088fidItem
     *
     * @param integer $f088fidItem
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fidItem($f088fidItem)
    {
        $this->f088fidItem = $f088fidItem;

        return $this;
    }

    /**
     * Get f088fidItem
     *
     * @return integer
     */
    public function getF088fidItem()
    {
        return $this->f088fidItem;
    }

    /**
     * Set f088fsoCode
     *
     * @param string $f088fsoCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fsoCode($f088fsoCode)
    {
        $this->f088fsoCode = $f088fsoCode;

        return $this;
    }

    /**
     * Get f088fsoCode
     *
     * @return string
     */
    public function getF088fsoCode()
    {
        return $this->f088fsoCode;
    }

    /**
     * Set f088frequiredDate
     *
     * @param \DateTime $f088frequiredDate
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088frequiredDate($f088frequiredDate)
    {
        $this->f088frequiredDate = $f088frequiredDate;

        return $this;
    }

    /**
     * Get f088frequiredDate
     *
     * @return \DateTime
     */
    public function getF088frequiredDate()
    {
        return $this->f088frequiredDate;
    }

    /**
     * Set f088fquantity
     *
     * @param integer $f088fquantity
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fquantity($f088fquantity)
    {
        $this->f088fquantity = $f088fquantity;

        return $this;
    }

    /**
     * Get f088fquantity
     *
     * @return integer
     */
    public function getF088fquantity()
    {
        return $this->f088fquantity;
    }

    /**
     * Set f088funit
     *
     * @param string $f088funit
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088funit($f088funit)
    {
        $this->f088funit = $f088funit;

        return $this;
    }

    /**
     * Get f088funit
     *
     * @return string
     */
    public function getF088funit()
    {
        return $this->f088funit;
    }

    /**
     * Set f088fprice
     *
     * @param integer $f088fprice
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fprice($f088fprice)
    {
        $this->f088fprice = $f088fprice;

        return $this;
    }

    /**
     * Get f088fprice
     *
     * @return integer
     */
    public function getF088fprice()
    {
        return $this->f088fprice;
    }

    /**
     * Set f088total
     *
     * @param integer $f088total
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088total($f088total)
    {
        $this->f088total = $f088total;

        return $this;
    }

    /**
     * Get f088total
     *
     * @return integer
     */
    public function getF088total()
    {
        return $this->f088total;
    }

    /**
     * Set f088ftaxCode
     *
     * @param integer $f088ftaxCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088ftaxCode($f088ftaxCode)
    {
        $this->f088ftaxCode = $f088ftaxCode;

        return $this;
    }

    /**
     * Get f088ftaxCode
     *
     * @return integer
     */
    public function getF088ftaxCode()
    {
        return $this->f088ftaxCode;
    }

    /**
     * Set f088fpercentage
     *
     * @param integer $f088fpercentage
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fpercentage($f088fpercentage)
    {
        $this->f088fpercentage = $f088fpercentage;

        return $this;
    }

    /**
     * Get f088fpercentage
     *
     * @return integer
     */
    public function getF088fpercentage()
    {
        return $this->f088fpercentage;
    }

    /**
     * Set f088ftaxAmount
     *
     * @param integer $f088ftaxAmount
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088ftaxAmount($f088ftaxAmount)
    {
        $this->f088ftaxAmount = $f088ftaxAmount;

        return $this;
    }

    /**
     * Get f088ftaxAmount
     *
     * @return integer
     */
    public function getF088ftaxAmount()
    {
        return $this->f088ftaxAmount;
    }

    /**
     * Set f088ftotalIncTax
     *
     * @param integer $f088ftotalIncTax
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088ftotalIncTax($f088ftotalIncTax)
    {
        $this->f088ftotalIncTax = $f088ftotalIncTax;

        return $this;
    }

    /**
     * Get f088ftotalIncTax
     *
     * @return integer
     */
    public function getF088ftotalIncTax()
    {
        return $this->f088ftotalIncTax;
    }

    /**
     * Set f088fstatus
     *
     * @param integer $f088fstatus
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fstatus($f088fstatus)
    {
        $this->f088fstatus = $f088fstatus;

        return $this;
    }

    /**
     * Get f088fstatus
     *
     * @return integer
     */
    public function getF088fstatus()
    {
        return $this->f088fstatus;
    }

    /**
     * Set f088fcreatedBy
     *
     * @param integer $f088fcreatedBy
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fcreatedBy($f088fcreatedBy)
    {
        $this->f088fcreatedBy = $f088fcreatedBy;

        return $this;
    }

    /**
     * Get f088fcreatedBy
     *
     * @return integer
     */
    public function getF088fcreatedBy()
    {
        return $this->f088fcreatedBy;
    }

    /**
     * Set f088fupdatedBy
     *
     * @param integer $f088fupdatedBy
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fupdatedBy($f088fupdatedBy)
    {
        $this->f088fupdatedBy = $f088fupdatedBy;

        return $this;
    }

    /**
     * Get f088fupdatedBy
     *
     * @return integer
     */
    public function getF088fupdatedBy()
    {
        return $this->f088fupdatedBy;
    }

    /**
     * Set f088fcreatedDtTm
     *
     * @param \DateTime $f088fcreatedDtTm
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fcreatedDtTm($f088fcreatedDtTm)
    {
        $this->f088fcreatedDtTm = $f088fcreatedDtTm;

        return $this;
    }

    /**
     * Get f088fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fcreatedDtTm()
    {
        return $this->f088fcreatedDtTm;
    }

    /**
     * Set f088fupdatedDtTm
     *
     * @param \DateTime $f088fupdatedDtTm
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fupdatedDtTm($f088fupdatedDtTm)
    {
        $this->f088fupdatedDtTm = $f088fupdatedDtTm;

        return $this;
    }

    /**
     * Get f088fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF088fupdatedDtTm()
    {
        return $this->f088fupdatedDtTm;
    }

    /**
     * Set f088ffundCode
     *
     * @param string $f088ffundCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088ffundCode($f088ffundCode)
    {
        $this->f088ffundCode = $f088ffundCode;

        return $this;
    }

    /**
     * Get f088ffundCode
     *
     * @return string
     */
    public function getF088ffundCode()
    {
        return $this->f088ffundCode;
    }

    /**
     * Set f088factivityCode
     *
     * @param string $f088factivityCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088factivityCode($f088factivityCode)
    {
        $this->f088factivityCode = $f088factivityCode;

        return $this;
    }

    /**
     * Get f088factivityCode
     *
     * @return string
     */
    public function getF088factivityCode()
    {
        return $this->f088factivityCode;
    }

    /**
     * Set f088fdepartmentCode
     *
     * @param string $f088fdepartmentCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fdepartmentCode($f088fdepartmentCode)
    {
        $this->f088fdepartmentCode = $f088fdepartmentCode;

        return $this;
    }

    /**
     * Get f088fdepartmentCode
     *
     * @return string
     */
    public function getF088fdepartmentCode()
    {
        return $this->f088fdepartmentCode;
    }

    /**
     * Set f088faccountCode
     *
     * @param string $f088faccountCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088faccountCode($f088faccountCode)
    {
        $this->f088faccountCode = $f088faccountCode;

        return $this;
    }

    /**
     * Get f088faccountCode
     *
     * @return string
     */
    public function getF088faccountCode()
    {
        return $this->f088faccountCode;
    }

    /**
     * Set f088fbudgetFundCode
     *
     * @param string $f088fbudgetFundCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fbudgetFundCode($f088fbudgetFundCode)
    {
        $this->f088fbudgetFundCode = $f088fbudgetFundCode;

        return $this;
    }

    /**
     * Get f088fbudgetFundCode
     *
     * @return string
     */
    public function getF088fbudgetFundCode()
    {
        return $this->f088fbudgetFundCode;
    }

    /**
     * Set f088fbudgetActivityCode
     *
     * @param string $f088fbudgetActivityCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fbudgetActivityCode($f088fbudgetActivityCode)
    {
        $this->f088fbudgetActivityCode = $f088fbudgetActivityCode;

        return $this;
    }

    /**
     * Get f088fbudgetActivityCode
     *
     * @return string
     */
    public function getF088fbudgetActivityCode()
    {
        return $this->f088fbudgetActivityCode;
    }

    /**
     * Set f088fbudgetDepartmentCode
     *
     * @param string $f088fbudgetDepartmentCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fbudgetDepartmentCode($f088fbudgetDepartmentCode)
    {
        $this->f088fbudgetDepartmentCode = $f088fbudgetDepartmentCode;

        return $this;
    }

    /**
     * Get f088fbudgetDepartmentCode
     *
     * @return string
     */
    public function getF088fbudgetDepartmentCode()
    {
        return $this->f088fbudgetDepartmentCode;
    }

    /**
     * Set f088fbudgetAccountCode
     *
     * @param string $f088fbudgetAccountCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fbudgetAccountCode($f088fbudgetAccountCode)
    {
        $this->f088fbudgetAccountCode = $f088fbudgetAccountCode;

        return $this;
    }

    /**
     * Get f088fbudgetAccountCode
     *
     * @return string
     */
    public function getF088fbudgetAccountCode()
    {
        return $this->f088fbudgetAccountCode;
    }


    /**
     * Set f088fitemCategory
     *
     * @param string $f088fitemCategory
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fitemCategory($f088fitemCategory)
    {
        $this->f088fitemCategory = $f088fitemCategory;

        return $this;
    }

    /**
     * Get f088fitemCategory
     *
     * @return string
     */
    public function getF088fitemCategory()
    {
        return $this->f088fitemCategory;
    }

    /**
     * Set f088fitemSubCategory
     *
     * @param string $f088fitemSubCategory
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fitemSubCategory($f088fitemSubCategory)
    {
        $this->f088fitemSubCategory = $f088fitemSubCategory;

        return $this;
    }

    /**
     * Get f088fitemSubCategory
     *
     * @return string
     */
    public function getF088fitemSubCategory()
    {
        return $this->f088fitemSubCategory;
    }


    /**
     * Set f088fitemCode
     *
     * @param string $f088fitemCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fitemCode($f088fitemCode)
    {
        $this->f088fitemCode = $f088fitemCode;

        return $this;
    }

    /**
     * Get f088fitemCode
     *
     * @return string
     */
    public function getF088fitemCode()
    {
        return $this->f088fitemCode;
    }


    /**
     * Set f088fcategoryType
     *
     * @param string $f088fcategoryType
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fcategoryType($f088fcategoryType)
    {
        $this->f088fcategoryType = $f088fcategoryType;

        return $this;
    }

    /**
     * Get f088fcategoryType
     *
     * @return string
     */
    public function getF088fcategoryType()
    {
        return $this->f088fcategoryType;
    }

    /**
     * Set f088fassetCode
     *
     * @param string $f088fassetCode
     *
     * @return T088fpurchaseReqDetails
     */
    public function setF088fassetCode($f088fassetCode)
    {
        $this->f088fassetCode = $f088fassetCode;

        return $this;
    }

    /**
     * Get f088fassetCode
     *
     * @return string
     */
    public function getF088fassetCode()
    {
        return $this->f088fassetCode;
    }
}
 

      
    
    

    


     