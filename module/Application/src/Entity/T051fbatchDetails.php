<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T051fbatchDetails
 *
 * @ORM\Table(name="t051fbatch_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BatchRepository")
 */
class T051fbatchDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f051fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid_batch", type="integer", nullable=false)
     */
    private $f051fidBatch;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fid_receipt", type="integer", nullable=false)
     */
    private $f051fidReceipt;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fstatus", type="integer", nullable=false)
     */
    private $f051fstatus;


    /**
     * @var integer
     *
     * @ORM\Column(name="f051fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f051fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f051fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f051fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fcreate_dt_tm", type="datetime", nullable=false)
     */
    private $f051fcreateDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f051fupdate_dt_tm", type="datetime", nullable=false)
     */
    private $f051fupdateDtTm;



    /**
     * Get f051fid
     *
     * @return integer
     */
    public function getF051fid()
    {
        return $this->f051fid;
    }

    /**
     * Set f051fidBatch
     *
     * @param string $f051fidBatch
     *
     * @return T051fbatchDetails
     */
    public function setF051fidBatch($f051fidBatch)
    {
        $this->f051fidBatch = $f051fidBatch;

        return $this;
    }

    /**
     * Get f051fidBatch
     *
     * @return string
     */
    public function getF051fidBatch()
    {
        return $this->f051fidBatch;
    }

    /**
     * Set f051fidReceipt
     *
     * @param integer $f051fidReceipt
     *
     * @return T051fbatchDetails
     */
    public function setF051fidReceipt($f051fidReceipt)
    {
        $this->f051fidReceipt = $f051fidReceipt;

        return $this;
    }

    /**
     * Get f051fidReceipt
     *
     * @return integer
     */
    public function getF051fidReceipt()
    {
        return $this->f051fidReceipt;
    }


    /**
     * Get f051fstatus
     *
     * @return integer
     */
    public function getF051fstatus()
    {
        return $this->f051fstatus;
    }

     /**
     * Set f051fstatus
     *
     * @param integer $f051fstatus
     *
     * @return T051fbatchDetails
     */
    public function setF051fstatus($f051fstatus)
    {
        $this->f051fstatus = $f051fstatus;

        return $this;
    }

     /**
     * Set f051fcreatedBy
     *
     * @param integer $f051fcreatedBy
     *
     * @return T051fbatchDetails
     */
    public function setF051fcreatedBy($f051fcreatedBy)
    {
        $this->f051fcreatedBy = $f051fcreatedBy;

        return $this;
    }
   
   
    /**
     * Get f051fcreatedBy
     *
     * @return integer
     */
    public function getF051fcreatedBy()
    {
        return $this->f051fcreatedBy;
    }

    /**
     * Set f051fupdatedBy
     *
     * @param integer $f051fupdatedBy
     *
     * @return T051fbatchDetails
     */
    public function setF051fupdatedBy($f051fupdatedBy)
    {
        $this->f051fupdatedBy = $f051fupdatedBy;

        return $this;
    }

    /**
     * Get f051fupdatedBy
     *
     * @return integer
     */
    public function getF051fupdatedBy()
    {
        return $this->f051fupdatedBy;
    }

    /**
     * Set f051fcreateDtTm
     *
     * @param \DateTime $f051fcreateDtTm
     *
     * @return T051fbatchDetails
     */
    public function setF051fcreateDtTm($f051fcreateDtTm)
    {
        $this->f051fcreateDtTm = $f051fcreateDtTm;

        return $this;
    }

    /**
     * Get f051fcreateDtTm
     *
     * @return \DateTime
     */
    public function getF051fcreateDtTm()
    {
        return $this->f051fcreateDtTm;
    }

    /**
     * Set f051fupdateDtTm
     *
     * @param \DateTime $f051fupdateDtTm
     *
     * @return T051fbatchDetails
     */
    public function setF051fupdateDtTm($f051fupdateDtTm)
    {
        $this->f051fupdateDtTm = $f051fupdateDtTm;

        return $this;
    }

    /**
     * Get f051fupdateDtTm
     *
     * @return \DateTime
     */
    public function getF051fupdateDtTm()
    {
        return $this->f051fupdateDtTm;
    }
}

























