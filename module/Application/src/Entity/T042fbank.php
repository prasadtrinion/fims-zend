<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T042fbank
 *
 * @ORM\Table(name="t042fbank",uniqueConstraints={@ORM\UniqueConstraint(name="f042fswift_code_UNIQUE", columns={"f042fswift_code"})})
 * @ORM\Entity(repositoryClass="Application\Repository\BankRepository")
 */
class T042fbank
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f042fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f042fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fbank_name", type="string", length=255, nullable=true)
     */
    private $f042fbankName;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fbranch_name", type="string", length=255, nullable=true)
     */
    private $f042fbranchName;

    /**
     * @var string
     *
     * @ORM\Column(name="f042faccount_number", type="string", length=50, nullable=true)
     */
    private $f042faccountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f042faccount_type", type="string", length=255, nullable=true)
     */
    private $f042faccountType;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fswift_code", type="string", length=50, nullable=true)
     */
    private $f042fswiftCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f042fbrs_code", type="string", length=50, nullable=true)
     */
    private $f042fbrsCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fcreated_by", type="integer", nullable=true)
     */
    private $f042fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fupdated_by", type="integer", nullable=true)
     */
    private $f042fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f042fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f042fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f042fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f042fstatus", type="integer", nullable=true)
     */
    private $f042fstatus = '1';



    /**
     * Get f042fid
     *
     * @return integer
     */
    public function getF042fid()
    {
        return $this->f042fid;
    }

    /**
     * Set f042fbankName
     *
     * @param string $f042fbankName
     *
     * @return T042fbank
     */
    public function setF042fbankName($f042fbankName)
    {
        $this->f042fbankName = $f042fbankName;

        return $this;
    }

    /**
     * Get f042fbankName
     *
     * @return string
     */
    public function getF042fbankName()
    {
        return $this->f042fbankName;
    }

    /**
     * Set f042fbranchName
     *
     * @param string $f042fbranchName
     *
     * @return T042fbank
     */
    public function setF042fbranchName($f042fbranchName)
    {
        $this->f042fbranchName = $f042fbranchName;

        return $this;
    }

    /**
     * Get f042fbranchName
     *
     * @return string
     */
    public function getF042fbranchName()
    {
        return $this->f042fbranchName;
    }

    /**
     * Set f042faccountNumber
     *
     * @param string $f042faccountNumber
     *
     * @return T042fbank
     */
    public function setF042faccountNumber($f042faccountNumber)
    {
        $this->f042faccountNumber = $f042faccountNumber;

        return $this;
    }

    /**
     * Get f042faccountNumber
     *
     * @return string
     */
    public function getF042faccountNumber()
    {
        return $this->f042faccountNumber;
    }

    /**
     * Set f042faccountType
     *
     * @param string $f042faccountType
     *
     * @return T042fbank
     */
    public function setF042faccountType($f042faccountType)
    {
        $this->f042faccountType = $f042faccountType;

        return $this;
    }

    /**
     * Get f042faccountType
     *
     * @return string
     */
    public function getF042faccountType()
    {
        return $this->f042faccountType;
    }

    /**
     * Set f042fswiftCode
     *
     * @param string $f042fswiftCode
     *
     * @return T042fbank
     */
    public function setF042fswiftCode($f042fswiftCode)
    {
        $this->f042fswiftCode = $f042fswiftCode;

        return $this;
    }

    /**
     * Get f042fswiftCode
     *
     * @return string
     */
    public function getF042fswiftCode()
    {
        return $this->f042fswiftCode;
    }

    /**
     * Set f042fbrsCode
     *
     * @param string $f042fbrsCode
     *
     * @return T042fbank
     */
    public function setF042fbrsCode($f042fbrsCode)
    {
        $this->f042fbrsCode = $f042fbrsCode;

        return $this;
    }

    /**
     * Get f042fbrsCode
     *
     * @return string
     */
    public function getF042fbrsCode()
    {
        return $this->f042fbrsCode;
    }

    /**
     * Set f042fcreatedBy
     *
     * @param integer $f042fcreatedBy
     *
     * @return T042fbank
     */
    public function setF042fcreatedBy($f042fcreatedBy)
    {
        $this->f042fcreatedBy = $f042fcreatedBy;

        return $this;
    }

    /**
     * Get f042fcreatedBy
     *
     * @return integer
     */
    public function getF042fcreatedBy()
    {
        return $this->f042fcreatedBy;
    }

    /**
     * Set f042fupdatedBy
     *
     * @param integer $f042fupdatedBy
     *
     * @return T042fbank
     */
    public function setF042fupdatedBy($f042fupdatedBy)
    {
        $this->f042fupdatedBy = $f042fupdatedBy;

        return $this;
    }

    /**
     * Get f042fupdatedBy
     *
     * @return integer
     */
    public function getF042fupdatedBy()
    {
        return $this->f042fupdatedBy;
    }

    /**
     * Set f042fcreatedDtTm
     *
     * @param \DateTime $f042fcreatedDtTm
     *
     * @return T042fbank
     */
    public function setF042fcreatedDtTm($f042fcreatedDtTm)
    {
        $this->f042fcreatedDtTm = $f042fcreatedDtTm;

        return $this;
    }

    /**
     * Get f042fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fcreatedDtTm()
    {
        return $this->f042fcreatedDtTm;
    }

    /**
     * Set f042fupdatedDtTm
     *
     * @param \DateTime $f042fupdatedDtTm
     *
     * @return T042fbank
     */
    public function setF042fupdatedDtTm($f042fupdatedDtTm)
    {
        $this->f042fupdatedDtTm = $f042fupdatedDtTm;

        return $this;
    }

    /**
     * Get f042fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF042fupdatedDtTm()
    {
        return $this->f042fupdatedDtTm;
    }

    /**
     * Set f042fstatus
     *
     * @param integer $f042fstatus
     *
     * @return T042fbank
     */
    public function setF042fstatus($f042fstatus)
    {
        $this->f042fstatus = $f042fstatus;

        return $this;
    }

    /**
     * Get f042fstatus
     *
     * @return integer
     */
    public function getF042fstatus()
    {
        return $this->f042fstatus;
    }
}
