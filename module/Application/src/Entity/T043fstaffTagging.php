<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T043fstaffTagging
 *
 * @ORM\Table(name="t043fstaff_tagging")
 * @ORM\Entity(repositoryClass="Application\Repository\StaffTaggingRepository")
 */
class T043fstaffTagging
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f043fid;

    // /**
    //  * @var integer
    //  *
    //  * @ORM\Column(name="f043fic_number", type="string",length=20,nullable=true)
    //  */
    // private $f043ficNumber;

    // *
    //  * @var integer
    //  *
    //  * @ORM\Column(name="f043fname", type="string",length=50, nullable=true)
     
    // private $f043fname;

    // /**
    //  * @var string
    //  *
    //  * @ORM\Column(name="f043fdesignation", type="string", length=30, nullable=true)
    //  */
    // private $f043fdesignation;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fdepartment", type="string",length=20, nullable=true)
     */
    private $f043fdepartment;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fid_staff", type="integer", nullable=true)
     */
    private $f043fidStaff;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fcost_center", type="string",length=20, nullable=true)
     */
    private $f043fcostCenter;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043factivity", type="string",length=20, nullable=true)
     */
    private $f043factivity;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043faccount_number", type="string",length=20, nullable=true)
     */
    private $f043faccountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043ffund", type="string",length=20, nullable=true)
     */
    private $f043ffund;

    /**
     * @var string
     *
     * @ORM\Column(name="f043faddress1", type="string", length=100, nullable=true)
     */
    private $f043faddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="f043faddress2", type="string", length=100, nullable=true)
     */
    private $f043faddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="f043faddress3", type="string", length=100, nullable=true)
     */
    private $f043faddress3;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fcountry", type="integer", nullable=true)
     */
    private $f043fcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fstate", type="integer", nullable=true)
     */
    private $f043fstate;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fzip_code", type="string", length=50, nullable=true)
     */
    private $f043fzipCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fbank", type="integer", nullable=true)
     */
    private $f043fbank;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fid_epf", type="integer", nullable=true)
     */
    private $f043fidEpf;

    /**
     * @var string
     *
     * @ORM\Column(name="f043fid_socso", type="integer", nullable=true)
     */
    private $f043fidSocso;

    /**
     * @var string
     *
     * @ORM\Column(name="f043ftaxable",type="integer", nullable=true)
     */
    private $f043ftaxable;


    /**
     * @var integer
     *
     * @ORM\Column(name="f043fstatus", type="integer", nullable=true)
     */
    private $f043fstatus= '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fcreated_by", type="integer", nullable=true)
     */
    private $f043fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f043fupdated_by", type="integer", nullable=true)
     */
    private $f043fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f043fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f043fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f043fupdatedDtTm;


    /**
     * Get f043fid
     *
     * @return integer
     */
    public function getF043fid()
    {
        return $this->f043fid;
    }

    // /**
    //  * Set f043fdesignation
    //  *
    //  * @param string $f043fdesignation
    //  *
    //  * @return T043fstaffTagging
    //  */
    // public function setF043fdesignation($f043fdesignation)
    // {
    //     $this->f043fdesignation = $f043fdesignation;

    //     return $this;
    // }

    // /**
    //  * Get f043fdesignation
    //  *
    //  * @return string
    //  */
    // public function getF043fdesignation()
    // {
    //     return $this->f043fdesignation;
    // }

    // /**
    //  * Set f043ficNumber
    //  *
    //  * @param integer $f043ficNumber
    //  *
    //  * @return T043fstaffTagging
    //  */
    // public function setF043ficNumber($f043ficNumber)
    // {
    //     $this->f043ficNumber = $f043ficNumber;

    //     return $this;
    // }

    // /**
    //  * Get f043ficNumber
    //  *
    //  * @return integer
    //  */
    // public function getF043ficNumber()
    // {
    //     return $this->f043ficNumber;
    // }

    // /**
    //  * Set f043fname
    //  *
    //  * @param integer $f043fname
    //  *
    //  * @return T043fstaffTagging
    //  */
    // public function setF043fname($f043fname)
    // {
    //     $this->f043fname = $f043fname;

    //     return $this;
    // }

    // /**
    //  * Get f043fname
    //  *
    //  * @return integer
    //  */
    // public function getF043fname()
    // {
    //     return $this->f043fname;
    // }

    /**
     * Set f043fdepartment
     *
     * @param integer $f043fdepartment
     *
     * @return T043fstaffTagging
     */
    public function setF043fdepartment($f043fdepartment)
    {
        $this->f043fdepartment = $f043fdepartment;

        return $this;
    }

    /**
     * Get f043fdepartment
     *
     * @return integer
     */
    public function getF043fdepartment()
    {
        return $this->f043fdepartment;
    }

    /**
     * Set f043ffund
     *
     * @param integer $f043ffund
     *
     * @return T043fstaffTagging
     */
    public function setF043ffund($f043ffund)
    {
        $this->f043ffund = $f043ffund;

        return $this;
    }

    /**
     * Get f043ffund
     *
     * @return integer
     */
    public function getF043ffund()
    {
        return $this->f043ffund;
    }

    /**
     * Set f043fidStaff
     *
     * @param integer $f043fidStaff
     *
     * @return T043fstaffTagging
     */
    public function setF043fidStaff($f043fidStaff)
    {
        $this->f043fidStaff = $f043fidStaff;

        return $this;
    }

    /**
     * Get f043fidStaff
     *
     * @return integer
     */
    public function getF043fidStaff()
    {
        return $this->f043fidStaff;
    }

    /**
     * Set f043fcostCenter
     *
     * @param integer $f043fcostCenter
     *
     * @return T043fstaffTagging
     */
    public function setF043fcostCenter($f043fcostCenter)
    {
        $this->f043fcostCenter = $f043fcostCenter;

        return $this;
    }

    /**
     * Get f043fcostCenter
     *
     * @return integer
     */
    public function getF043fcostCenter()
    {
        return $this->f043fcostCenter;
    }

    /**
     * Set f043factivity
     *
     * @param integer $f043factivity
     *
     * @return T043fstaffTagging
     */
    public function setF043factivity($f043factivity)
    {
        $this->f043factivity = $f043factivity;

        return $this;
    }

    /**
     * Get f043factivity
     *
     * @return integer
     */
    public function getF043factivity()
    {
        return $this->f043factivity;
    }

    /**
     * Set f043faccountNumber
     *
     * @param integer $f043faccountNumber
     *
     * @return T043fstaffTagging
     */
    public function setF043faccountNumber($f043faccountNumber)
    {
        $this->f043faccountNumber = $f043faccountNumber;

        return $this;
    }

    /**
     * Get f043faccountNumber
     *
     * @return integer
     */
    public function getF043faccountNumber()
    {
        return $this->f043faccountNumber;
    }

    /**
     * Set f043faddress1
     *
     * @param string $f043faddress1
     *
     * @return T043fstaffTagging
     */
    public function setF043faddress1($f043faddress1)
    {
        $this->f043faddress1 = $f043faddress1;

        return $this;
    }

    /**
     * Get f043faddress1
     *
     * @return string
     */
    public function getF043faddress1()
    {
        return $this->f043faddress1;
    }

     public function setF043faddress2($f043faddress2)
    {
        $this->f043faddress2 = $f043faddress2;

        return $this;
    }

    /**
     * Get f043faddress2
     *
     * @return string
     */
    public function getF043faddress2()
    {
        return $this->f043faddress2;
    }

     public function setF043faddress3($f043faddress3)
    {
        $this->f043faddress3 = $f043faddress3;

        return $this;
    }

    /**
     * Get f043faddress3
     *
     * @return string
     */
    public function getF043faddress3()
    {
        return $this->f043faddress3;
    }

    /**
     * Set f043fcountry
     *
     * @param string $f043fcountry
     *
     * @return T043fstaffTagging
     */
    public function setF043fcountry($f043fcountry)
    {
        $this->f043fcountry = $f043fcountry;

        return $this;
    }

    /**
     * Get f043fcountry
     *
     * @return string
     */
    public function getF043fcountry()
    {
        return $this->f043fcountry;
    }

    /**
     * Set f043fstate
     *
     * @param string $f043fstate
     *
     * @return T043fstaffTagging
     */
    public function setF043fstate($f043fstate)
    {
        $this->f043fstate = $f043fstate;

        return $this;
    }

    /**
     * Get f043fstate
     *
     * @return string
     */
    public function getF043fstate()
    {
        return $this->f043fstate;
    }

    /**
     * Set f043fzipCode
     *
     * @param string $f043fzipCode
     *
     * @return T043fstaffTagging
     */
    public function setF043fzipCode($f043fzipCode)
    {
        $this->f043fzipCode = $f043fzipCode;

        return $this;
    }

    /**
     * Get f043fzipCode
     *
     * @return string
     */
    public function getF043fzipCode()
    {
        return $this->f043fzipCode;
    }

    /**
     * Set f043fbank
     *
     * @param integer $f043fbank
     *
     * @return T043fstaffTagging
     */
    public function setF043fbank($f043fbank)
    {
        $this->f043fbank = $f043fbank;

        return $this;
    }

    /**
     * Get f043fbank
     *
     * @return integer
     */
    public function getF043fbank()
    {
        return $this->f043fbank;
    }

    /**
     * Set f043fidEpf
     *
     * @param string $f043fidEpf
     *
     * @return T043fstaffTagging
     */
    public function setF043fidEpf($f043fidEpf)
    {
        $this->f043fidEpf = $f043fidEpf;

        return $this;
    }

    /**
     * Get f043fidEpf
     *
     * @return string
     */
    public function getF043fidEpf()
    {
        return $this->f043fidEpf;
    }

    /**
     * Set f043fidSocso
     *
     * @param string $f043fidSocso
     *
     * @return T043fstaffTagging
     */
    public function setF043fidSocso($f043fidSocso)
    {
        $this->f043fidSocso = $f043fidSocso;

        return $this;
    }

    /**
     * Get f043fidSocso
     *
     * @return string
     */
    public function getF043fidSocso()
    {
        return $this->f043fidSocso;
    }

    /**
     * Set f043ftaxable
     *
     * @param string $f043ftaxable
     *
     * @return T043fstaffTagging
     */
    public function setF043ftaxable($f043ftaxable)
    {
        $this->f043ftaxable = $f043ftaxable;

        return $this;
    }

    /**
     * Get f043ftaxable
     *
     * @return string
     */
    public function getF043ftaxable()
    {
        return $this->f043ftaxable;
    }

    public function setF043fstatus($f043fstatus)
    {
        $this->f043fstatus = $f043fstatus;

        return $this;
    }

    /**
     * Get f043fstatus
     *
     * @return integer
     */
    public function getF043fstatus()
    {
        return $this->f043fstatus;
    }

    /**
     * Set f043fcreatedBy
     *
     * @param integer $f043fcreatedBy
     *
     * @return T043fstaffTagging
     */
    public function setF043fcreatedBy($f043fcreatedBy)
    {
        $this->f043fcreatedBy = $f043fcreatedBy;

        return $this;
    }

    /**
     * Get f043fcreatedBy
     *
     * @return integer
     */
    public function getF043fcreatedBy()
    {
        return $this->f043fcreatedBy;
    }

    /**
     * Set f043fupdatedBy
     *
     * @param integer $f043fupdatedBy
     *
     * @return T043fstaffTagging
     */
    public function setF043fupdatedBy($f043fupdatedBy)
    {
        $this->f043fupdatedBy = $f043fupdatedBy;

        return $this;
    }

    /**
     * Get f043fupdatedBy
     *
     * @return integer
     */
    public function getF043fupdatedBy()
    {
        return $this->f043fupdatedBy;
    }

    /**
     * Set f043fcreatedDtTm
     *
     * @param \DateTime $f043fcreatedDtTm
     *
     * @return T043fstaffTagging
     */
    public function setF043fcreatedDtTm($f043fcreatedDtTm)
    {
        $this->f043fcreatedDtTm = $f043fcreatedDtTm;

        return $this;
    }

    /**
     * Get f043fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fcreatedDtTm()
    {
        return $this->f043fcreatedDtTm;
    }

    /**
     * Set f043fupdatedDtTm
     *
     * @param \DateTime $f043fupdatedDtTm
     *
     * @return T043fstaffTagging
     */
    public function setF043fupdatedDtTm($f043fupdatedDtTm)
    {
        $this->f043fupdatedDtTm = $f043fupdatedDtTm;

        return $this;
    }

    /**
     * Get f043fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF043fupdatedDtTm()
    {
        return $this->f043fupdatedDtTm;
    }

}



    

  

    

     

   



   


    
