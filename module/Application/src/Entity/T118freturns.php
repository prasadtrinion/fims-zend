<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T118freturns
 *
 * @ORM\Table(name="t118freturns")
 * @ORM\Entity(repositoryClass="Application\Repository\ReturnsRepository")
 */
class T118freturns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f118fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f118fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118famount", type="integer", length=50, nullable=false)
     */
    private $f118famount;

    /**
     * @var string
     *
     * @ORM\Column(name="f118famount_number", type="string", length=10, nullable=false)
     */
    private $f118famountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f118freceipt_number", type="string", length=10, nullable=false)
     */
    private $f118freceiptNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118fstudent", type="integer", length=50, nullable=false)
     */
    private $f118fstudent;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118fapproval_status", type="integer", length=50, nullable=false)
     */
    private $f118fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118fstatus", type="integer", nullable=false)
     */
    private $f118fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f118fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f118fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f118fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f118fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118fupdated_by", type="integer", length=50, nullable=false)
     */
    private $f118fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f118fcreated_by", type="integer", length=50, nullable=false)
     */
    private $f118fcreatedBy;


    /**
     * Get f118fid
     *
     * @return integer
     */
    public function getF118fid()
    {
        return $this->f118fid;
    }

    /**
     * Set f118famount
     *
     * @param integer $f118famount
     *
     * @return T118freturns
     */
    public function setF118famount($f118famount)
    {
        $this->f118famount = $f118famount;

        return $this;
    }

    /**
     * Get f118famount
     *
     * @return integer
     */
    public function getF118famount()
    {
        return $this->f118famount;
    }


    /**
     * Set f118famountNumber
     *
     * @param string $f118famountNumber
     *
     * @return T118freturns
     */
    public function setF118famountNumber($f118famountNumber)
    {
        $this->f118famountNumber = $f118famountNumber;

        return $this;
    }

    /**
     * Get f118famountNumber
     *
     * @return string
     */
    public function getF118famountNumber()
    {
        return $this->f118famountNumber;
    }


    /**
     * Set f118freceiptNumber
     *
     * @param string $f118freceiptNumber
     *
     * @return T118freturns
     */
    public function setF118freceiptNumber($f118freceiptNumber)
    {
        $this->f118freceiptNumber = $f118freceiptNumber;

        return $this;
    }

    /**
     * Get f118freceiptNumber
     *
     * @return string
     */
    public function getF118freceiptNumber()
    {
        return $this->f118freceiptNumber;
    }


    /**
     * Set f118fstudent
     *
     * @param integer $f118fstudent
     *
     * @return T118freturns
     */
    public function setF118fstudent($f118fstudent)
    {
        $this->f118fstudent = $f118fstudent;

        return $this;
    }

    /**
     * Get f118fstudent
     *
     * @return integer
     */
    public function getF118fstudent()
    {
        return $this->f118fstudent;
    }


    /**
     * Set f118fapprovalStatus
     *
     * @param integer $f118fapprovalStatus
     *
     * @return T118freturns
     */
    public function setF118fapprovalStatus($f118fapprovalStatus)
    {
        $this->f118fapprovalStatus = $f118fapprovalStatus;

        return $this;
    }

    /**
     * Get f118fapprovalStatus
     *
     * @return integer
     */
    public function getF118fapprovalStatus()
    {
        return $this->f118fapprovalStatus;
    }




    /**
     * Set f118fstatus
     *
     * @param integer $f118fstatus
     *
     * @return T118freturns
     */
    public function setF118fstatus($f118fstatus)
    {
        $this->f118fstatus = $f118fstatus;

        return $this;
    }

    /**
     * Get f118fstatus
     *
     * @return integer
     */
    public function getF118fstatus()
    {
        return $this->f118fstatus;
    }

    /**
     * Set f118fcreatedDtTm
     *
     * @param \DateTime $f118fcreatedDtTm
     *
     * @return T118freturns
     */
    public function setF118fcreatedDtTm($f118fcreatedDtTm)
    {
        $this->f118fcreatedDtTm = $f118fcreatedDtTm;

        return $this;
    }

    /**
     * Get f118fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF118fcreatedDtTm()
    {
        return $this->f118fcreatedDtTm;
    }

    /**
     * Set f118fupdatedDtTm
     *
     * @param \DateTime $f118fupdatedDtTm
     *
     * @return T118freturns
     */
    public function setF118fupdatedDtTm($f118fupdatedDtTm)
    {
        $this->f118fupdatedDtTm = $f118fupdatedDtTm;

        return $this;
    }

    /**
     * Get f118fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF118fupdatedDtTm()
    {
        return $this->f118fupdatedDtTm;
    }

    /**
     * Set f118fupdatedBy
     *
     * @param integer $f118fupdatedBy
     *
     * @return T118freturns
     */
    public function setF118fupdatedBy($f118fupdatedBy)
    {
        $this->f118fupdatedBy = $f118fupdatedBy;

        return $this;
    }

    
    public function getF118fupdatedBy()
    {
        return $this->f118fupdatedBy;
    }

    /**
     * Set f118fcreatedBy
     *
     * @param  integer $f118fcreatedBy
     *
     * @return T118freturns
     */
    public function setF118fcreatedBy($f118fcreatedBy)
    {
        $this->f118fcreatedBy = $f118fcreatedBy;

        return $this;
    }

    
    public function getF118fcreatedBy()
    {
        return $this->f118fcreatedBy;
    }
}
