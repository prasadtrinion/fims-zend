<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T061fcredit_note
 *
 * @ORM\Table(name="t061fsponsor_cn", indexes={@ORM\Index(name="f061fapproved_by", columns={"f061fapproved_by"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorCnRepository")
 */
class T061fsponsorCn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f061fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_invoice", type="integer", nullable=true)
     */
    private $f061fidInvoice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_financial_year", type="integer", nullable=true)
     */
    private $f061fidFinancialYear;

     /**
     * @var string
     *
     * @ORM\Column(name="f061fdescription", type="string", length=100, nullable=true)
     */
    private $f061fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fid_customer", type="integer", nullable=true)
     */
    private $f061fidCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="f061ftype", type="string", length=10, nullable=true)
     */
    private $f061ftype;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061ftotal_amount", type="integer", nullable=true)
     */
    private $f061ftotalAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fbalance", type="integer", nullable=true)
     */
    private $f061fbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="f061freference_number", type="string", length=50, nullable=true)
     */
    private $f061freferenceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f061freason", type="string", length=60, nullable=true)
     */
    private $f061freason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061fdate", type="datetime", nullable=true)
     */
    private $f061fdate;


    /**
     * @var string
     *
     * @ORM\Column(name="f061fvoucher_type", type="string", length=10, nullable=true)
     */
    private $f061fvoucherType;

    /**
     * @var string
     *
     * @ORM\Column(name="f061fvoucher_number", type="string", length=50, nullable=true)
     */
    private $f061fvoucherNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="f061fnarration", type="string", length=50, nullable=true)
     */
    private $f061fnarration;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fvoucher_days", type="integer", nullable=true)
     */
    private $f061fvoucherDays = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fupdated_by", type="integer", nullable=true)
     */
    private $f061fupdatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fcreated_by", type="integer", nullable=true)
     */
    private $f061fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fapproved_by", type="integer", nullable=true)
     */
    private $f061fapprovedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061created_dt_tm", type="datetime", nullable=true)
     */
    private $f061createdDtTm;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f061updated_dt_tm", type="datetime", nullable=true)
     */
    private $f061updatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f061fstatus", type="integer", nullable=true)
     */
    private $f061fstatus;



    /**
     * Get f061fid
     *
     * @return integer
     */
    public function getF061fid()
    {
        return $this->f061fid;
    }

    /**
     * Set f061fidInvoice
     *
     * @param integer $f061fidInvoice
     *
     * @return T061fsponsorCn
     */
    public function setF061fidInvoice($f061fidInvoice)
    {
        $this->f061fidInvoice = $f061fidInvoice;

        return $this;
    }

    /**
     * Get f061fidInvoice
     *
     * @return integer
     */
    public function getF061fidInvoice()
    {
        return $this->f061fidInvoice;
    }

    /**
     * Set f061fidCustomer
     *
     * @param integer $f061fidCustomer
     *
     * @return T061fsponsorCn
     */
    public function setF061fidCustomer($f061fidCustomer)
    {
        $this->f061fidCustomer = $f061fidCustomer;

        return $this;
    }

    /**
     * Get f061fidCustomer
     *
     * @return integer
     */
    public function getF061fidCustomer()
    {
        return $this->f061fidCustomer;
    }

    /**
     * Set f061ftotalAmount
     *
     * @param integer $f061ftotalAmount
     *
     * @return T061fsponsorCn
     */
    public function setF061ftotalAmount($f061ftotalAmount)
    {
        $this->f061ftotalAmount = $f061ftotalAmount;

        return $this;
    }

    /**
     * Get f061ftotalAmount
     *
     * @return integer
     */
    public function getF061ftotalAmount()
    {
        return $this->f061ftotalAmount;
    }

    /**
     * Set f061fbalance
     *
     * @param integer $f061fbalance
     *
     * @return T061fsponsorCn
     */
    public function setF061fbalance($f061fbalance)
    {
        $this->f061fbalance = $f061fbalance;

        return $this;
    }

    /**
     * Get f061fbalance
     *
     * @return integer
     */
    public function getF061fbalance()
    {
        return $this->f061fbalance;
    }

     /**
     * Set f061fidFinancialYear
     *
     * @param integer $f061fidFinancialYear
     *
     * @return T061fsponsorCn
     */
    public function setF061fidFinancialYear($f061fidFinancialYear)
    {
        $this->f061fidFinancialYear = $f061fidFinancialYear;

        return $this;
    }

    /**
     * Get f061fidFinancialYear
     *
     * @return integer
     */
    public function getF061fidFinancialYear()
    {
        return $this->f061fidFinancialYear;
    }

     /**
     * Set f061ftype
     *
     * @param string $f061ftype
     *
     * @return T061fsponsorCn
     */
    public function setF061ftype($f061ftype)
    {
        $this->f061ftype = $f061ftype;

        return $this;
    }

    /**
     * Get f061ftype
     *
     * @return string
     */
    public function getF061ftype()
    {
        return $this->f061ftype;
    }

    /**
     * Set f061freferenceNumber
     *
     * @param string $f061freferenceNumber
     *
     * @return T061fsponsorCn
     */
    public function setF061freferenceNumber($f061freferenceNumber)
    {
        $this->f061freferenceNumber = $f061freferenceNumber;

        return $this;
    }

    /**
     * Get f061freferenceNumber
     *
     * @return string
     */
    public function getF061freferenceNumber()
    {
        return $this->f061freferenceNumber;
    }

    /**
     * Set f061fdate
     *
     * @param \DateTime $f061fdate
     *
     * @return T061fsponsorCn
     */
    public function setF061fdate($f061fdate)
    {
        $this->f061fdate = $f061fdate;

        return $this;
    }

    /**
     * Get f061fdate
     *
     * @return \DateTime
     */
    public function getF061fdate()
    {
        return $this->f061fdate;
    }


    /**
     * Set f061fvoucherType
     *
     * @param string $f061fvoucherType
     *
     * @return T061fsponsorCn
     */
    public function setF061fvoucherType($f061fvoucherType)
    {
        $this->f061fvoucherType = $f061fvoucherType;

        return $this;
    }

    /**
     * Get f061fvoucherType
     *
     * @return string
     */
    public function getF061fvoucherType()
    {
        return $this->f061fvoucherType;
    }

    /**
     * Set f061fvoucherNumber
     *
     * @param string $f061fvoucherNumber
     *
     * @return T061fsponsorCn
     */
    public function setF061fvoucherNumber($f061fvoucherNumber)
    {
        $this->f061fvoucherNumber = $f061fvoucherNumber;

        return $this;
    }

    /**
     * Get f061fvoucherNumber
     *
     * @return string
     */
    public function getF061fvoucherNumber()
    {
        return $this->f061fvoucherNumber;
    }
    /**
     * Get f061fnarration
     *
     * @return string
     */
    public function getF061fnarration()
    {
        return $this->f061fnarration;
    }


    /**
     * Set f061fnarration
     *
     * @param string $f061fnarration
     *
     * @return T061fsponsorCn
     */
    public function setF061fnarration($f061fnarration)
    {
        $this->f061fnarration = $f061fnarration;

        return $this;
    }


    /**
     * Set f061fvoucherDays
     *
     * @param integer $f061fvoucherDays
     *
     * @return T061fvoucher
     */
    public function setF061fvoucherDays($f061fvoucherDays)
    {
        $this->f061fvoucherDays = $f061fvoucherDays;

        return $this;
    }

    /**
     * Get f061fvoucherDays
     *
     * @return integer
     */
    public function getF061fvoucherDays()
    {
        return $this->f061fvoucherDays;
    }

    /**
     * Set f061fupdatedBy
     *
     * @param integer $f061fupdatedBy
     *
     * @return T061fsponsorCn
     */
    public function setF061fupdatedBy($f061fupdatedBy)
    {
        $this->f061fupdatedBy = $f061fupdatedBy;

        return $this;
    }

    /**
     * Get f061fupdatedBy
     *
     * @return integer
     */
    public function getF061fupdatedBy()
    {
        return $this->f061fupdatedBy;
    }

    /**
     * Set f061fcreatedBy
     *
     * @param integer $f061fcreatedBy
     *
     * @return T061fsponsorCn
     */
    public function setF061fcreatedBy($f061fcreatedBy)
    {
        $this->f061fcreatedBy = $f061fcreatedBy;

        return $this;
    }

    /**
     * Get f061fcreatedBy
     *
     * @return integer
     */
    public function getF061fcreatedBy()
    {
        return $this->f061fcreatedBy;
    }

    /**
     * Set f061fapprovedBy
     *
     * @param integer $f061fapprovedBy
     *
     * @return T061fsponsorCn
     */
    public function setF061fapprovedBy($f061fapprovedBy)
    {
        $this->f061fapprovedBy = $f061fapprovedBy;

        return $this;
    }

    /**
     * Get f061fapprovedBy
     *
     * @return integer
     */
    public function getF061fapprovedBy()
    {
        return $this->f061fapprovedBy;
    }

    /**
     * Set f061createdDtTm
     *
     * @param \DateTime $f061createdDtTm
     *
     * @return T061fsponsorCn
     */
    public function setF061createdDtTm($f061createdDtTm)
    {
        $this->f061createdDtTm = $f061createdDtTm;

        return $this;
    }

    /**
     * Get f061createdDtTm
     *
     * @return \DateTime
     */
    public function getF061createdDtTm()
    {
        return $this->f061createdDtTm;
    }


    /**
     * Set f061updatedDtTm
     *
     * @param \DateTime $f061updatedDtTm
     *
     * @return T061fsponsorCn
     */
    public function setF061updatedDtTm($f061updatedDtTm)
    {
        $this->f061updatedDtTm = $f061updatedDtTm;

        return $this;
    }

    /**
     * Get f061updatedDtTm
     *
     * @return \DateTime
     */
    public function getF061updatedDtTm()
    {
        return $this->f061updatedDtTm;
    }
    /**
     * Set f061fstatus
     *
     * @param integer $f061fstatus
     *
     * @return T061fsponsorCn
     */
    public function setF061fstatus($f061fstatus)
    {
        $this->f061fstatus = $f061fstatus;

        return $this;
    }

    /**
     * Get f061fstatus
     *
     * @return integer
     */
    public function getF061fstatus()
    {
        return $this->f061fstatus;
    }

     /**
     * Set f061fdescription
     *
     * @param string $f061fdescription
     *
     * @return T061fsponsorCn
     */
    public function setF061fdescription($f061fdescription)
    {
        $this->f061fdescription = $f061fdescription;

        return $this;
    }

    /**
     * Get f061fdescription
     *
     * @return string
     */
    public function getF061fdescription()
    {
        return $this->f061fdescription;
    }

     /**
     * Set f061freason
     *
     * @param string $f061freason
     *
     * @return T061fsponsorCn
     */
    public function setF061freason($f061freason)
    {
        $this->f061freason = $f061freason;

        return $this;
    }

    /**
     * Get f061freason
     *
     * @return string
     */
    public function getF061freason()
    {
        return $this->f061freason;
    }

}