<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T034fstaffMaster
 *
 * @ORM\Table(name="vw_staff_master")
 * @ORM\Entity(repositoryClass="Application\Repository\StaffMasterRepository")
 */
class T034fstaffMaster
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f034fstaff_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f034fstaffId;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fid_department", type="string",length=20, nullable=true)
     */
    private $f034fidDepartment;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fname", type="string", length=50, nullable=true)
     */
    private $f034fname;

     /**
     * @var string
     *
     * @ORM\Column(name="f034fgroup_id", type="string", length=30, nullable=true)
     */
    private $f034fgroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="f034fgrade_id", type="string", length=30, nullable=true)
     */
    private $f034fgradeId;

     /**
     * @var string
     *
     * @ORM\Column(name="f034freporting_to", type="integer", nullable=true)
     */
    private $f034freportingTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fstatus", type="integer", nullable=true)
     */
    private $f034fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fcreated_by", type="integer", nullable=true)
     */
    private $f034fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f034fupdated_by", type="integer", nullable=true)
     */
    private $f034fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f034fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f034fupdatedDtTm;



    /**
     * Get f034fid
     *
     * @return integer
     */
    public function getF034fid()
    {
        return $this->f034fid;
    }

    /**
     * Set f034fstaffId
     *
     * @param string $f034fstaffId
     *
     * @return T034fstaffMaster
     */
    public function setF034fstaffId($f034fstaffId)
    {
        $this->f034fstaffId = $f034fstaffId;

        return $this;
    }

    /**
     * Get f034fstaffId
     *
     * @return string
     */
    public function getF034fstaffId()
    {
        return $this->f034fstaffId;
    }

    /**
     * Set f034fidDepartment
     *
     * @param integer $f034fidDepartment
     *
     * @return T034fstaffMaster
     */
    public function setF034fidDepartment($f034fidDepartment)
    {
        $this->f034fidDepartment = $f034fidDepartment;

        return $this;
    }

    /**
     * Get f034fidDepartment
     *
     * @return integer
     */
    public function getF034fidDepartment()
    {
        return $this->f034fidDepartment;
    }

    /**
     * Set f034fname
     *
     * @param string $f034fname
     *
     * @return T034fstaffMaster
     */
    public function setF034fname($f034fname)
    {
        $this->f034fname = $f034fname;

        return $this;
    }

    /**
     * Get f034fname
     *
     * @return string
     */
    public function getF034fname()
    {
        return $this->f034fname;
    }

    /**
     * Set f034fgroupId
     *
     * @param string $f034fgroupId
     *
     * @return T034fstaffMaster
     */
    public function setF034fgroupId($f034fgroupId)
    {
        $this->f034fgroupId = $f034fgroupId;

        return $this;
    }

    /**
     * Get f034fgroupId
     *
     * @return string
     */
    public function getF034fgroupId()
    {
        return $this->f034fgroupId;
    }

     /**
     * Set f034fgradeId
     *
     * @param string $f034fgradeId
     *
     * @return T034fstaffMaster
     */
    public function setF034fgradeId($f034fgradeId)
    {
        $this->f034fgradeId = $f034fgradeId;

        return $this;
    }

    /**
     * Get f034fgradeId
     *
     * @return string
     */
    public function getF034fgradeId()
    {
        return $this->f034fgradeId;
    }

    /**
     * Set f034freportingTo
     *
     * @param string $f034freportingTo
     *
     * @return T034fstaffMaster
     */
    public function setF034freportingTo($f034freportingTo)
    {
        $this->f034freportingTo = $f034freportingTo;

        return $this;
    }

    /**
     * Get f034freportingTo
     *
     * @return string
     */
    public function getF034freportingTo()
    {
        return $this->f034freportingTo;
    }

    /**
     * Set f034fstatus
     *
     * @param integer $f034fstatus
     *
     * @return T034fstaffMaster
     */
    public function setF034fstatus($f034fstatus)
    {
        $this->f034fstatus = $f034fstatus;

        return $this;
    }

    /**
     * Get f034fstatus
     *
     * @return integer
     */
    public function getF034fstatus()
    {
        return $this->f034fstatus;
    }

    /**
     * Set f034fcreatedBy
     *
     * @param integer $f034fcreatedBy
     *
     * @return T034fstaffMaster
     */
    public function setF034fcreatedBy($f034fcreatedBy)
    {
        $this->f034fcreatedBy = $f034fcreatedBy;

        return $this;
    }

    /**
     * Get f034fcreatedBy
     *
     * @return integer
     */
    public function getF034fcreatedBy()
    {
        return $this->f034fcreatedBy;
    }

    /**
     * Set f034fupdatedBy
     *
     * @param integer $f034fupdatedBy
     *
     * @return T034fstaffMaster
     */
    public function setF034fupdatedBy($f034fupdatedBy)
    {
        $this->f034fupdatedBy = $f034fupdatedBy;

        return $this;
    }

    /**
     * Get f034fupdatedBy
     *
     * @return integer
     */
    public function getF034fupdatedBy()
    {
        return $this->f034fupdatedBy;
    }

    /**
     * Set f034fcreatedDtTm
     *
     * @param \DateTime $f034fcreatedDtTm
     *
     * @return T034fstaffMaster
     */
    public function setF034fcreatedDtTm($f034fcreatedDtTm)
    {
        $this->f034fcreatedDtTm = $f034fcreatedDtTm;

        return $this;
    }

    /**
     * Get f034fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fcreatedDtTm()
    {
        return $this->f034fcreatedDtTm;
    }

    /**
     * Set f034fupdatedDtTm
     *
     * @param \DateTime $f034fupdatedDtTm
     *
     * @return T034fstaffMaster
     */
    public function setF034fupdatedDtTm($f034fupdatedDtTm)
    {
        $this->f034fupdatedDtTm = $f034fupdatedDtTm;

        return $this;
    }

    /**
     * Get f034fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF034fupdatedDtTm()
    {
        return $this->f034fupdatedDtTm;
    }
}
