<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T092fnonPoInvoiceDetails
 *
 * @ORM\Table(name="t092fnon_po_invoice_details")
 * @ORM\Entity
 */
class T092fnonPoInvoiceDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f092fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_master", type="integer", nullable=false)
     */
    private $f092fidMaster;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_item", type="integer", nullable=false)
     */
    private $f092fidItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fid_glcode", type="integer", nullable=false)
     */
    private $f092fidGlcode;

    /**
     * @var float
     *
     * @ORM\Column(name="f092fprice", type="integer", nullable=false)
     */
    private $f092fprice;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fquantity", type="integer", nullable=false)
     */
    private $f092fquantity;

    /**
     * @var float
     *
     * @ORM\Column(name="f092fid_tax", type="integer", nullable=false)
     */
    private $f092fidTax;

    /**
     * @var float
     *
     * @ORM\Column(name="f092fgst_value", type="integer", nullable=false)
     */
    private $f092fgstValue;

    /**
     * @var float
     *
     * @ORM\Column(name="f092fdiscount", type="integer", nullable=false)
     */
    private $f092fdiscount;

    /**
     * @var float
     *
     * @ORM\Column(name="f092ftotal", type="integer", nullable=false)
     */
    private $f092ftotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fstatus", type="integer", nullable=false)
     */
    private $f092fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fcreated_by", type="integer", nullable=false)
     */
    private $f092fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f092fupdated_by", type="integer", nullable=false)
     */
    private $f092fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f092fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f092fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f092fupdatedDtTm;



    /**
     * Get f092fid
     *
     * @return integer
     */
    public function getF092fid()
    {
        return $this->f092fid;
    }

    /**
     * Set f092fidMaster
     *
     * @param integer $f092fidMaster
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fidMaster($f092fidMaster)
    {
        $this->f092fidMaster = $f092fidMaster;

        return $this;
    }

    /**
     * Get f092fidMaster
     *
     * @return integer
     */
    public function getF092fidMaster()
    {
        return $this->f092fidMaster;
    }

    /**
     * Set f092fidItem
     *
     * @param integer $f092fidItem
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fidItem($f092fidItem)
    {
        $this->f092fidItem = $f092fidItem;

        return $this;
    }

    /**
     * Get f092fidItem
     *
     * @return integer
     */
    public function getF092fidItem()
    {
        return $this->f092fidItem;
    }

    /**
     * Set f092fidGlcode
     *
     * @param integer $f092fidGlcode
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fidGlcode($f092fidGlcode)
    {
        $this->f092fidGlcode = $f092fidGlcode;

        return $this;
    }

    /**
     * Get f092fidGlcode
     *
     * @return integer
     */
    public function getF092fidGlcode()
    {
        return $this->f092fidGlcode;
    }

    /**
     * Set f092fprice
     *
     * @param float $f092fprice
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fprice($f092fprice)
    {
        $this->f092fprice = $f092fprice;

        return $this;
    }

    /**
     * Get f092fprice
     *
     * @return float
     */
    public function getF092fprice()
    {
        return $this->f092fprice;
    }

    /**
     * Set f092fquantity
     *
     * @param integer $f092fquantity
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fquantity($f092fquantity)
    {
        $this->f092fquantity = $f092fquantity;

        return $this;
    }

    /**
     * Get f092fquantity
     *
     * @return integer
     */
    public function getF092fquantity()
    {
        return $this->f092fquantity;
    }

    /**
     * Set f092fidTax
     *
     * @param float $f092fidTax
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fidTax($f092fidTax)
    {
        $this->f092fidTax = $f092fidTax;

        return $this;
    }

    /**
     * Get f092fidTax
     *
     * @return float
     */
    public function getF092fidTax()
    {
        return $this->f092fidTax;
    }

    /**
     * Set f092fgstValue
     *
     * @param float $f092fgstValue
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fgstValue($f092fgstValue)
    {
        $this->f092fgstValue = $f092fgstValue;

        return $this;
    }

    /**
     * Get f092fgstValue
     *
     * @return float
     */
    public function getF092fgstValue()
    {
        return $this->f092fgstValue;
    }

    /**
     * Set f092fdiscount
     *
     * @param float $f092fdiscount
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fdiscount($f092fdiscount)
    {
        $this->f092fdiscount = $f092fdiscount;

        return $this;
    }

    /**
     * Get f092fdiscount
     *
     * @return float
     */
    public function getF092fdiscount()
    {
        return $this->f092fdiscount;
    }

    /**
     * Set f092ftotal
     *
     * @param float $f092ftotal
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092ftotal($f092ftotal)
    {
        $this->f092ftotal = $f092ftotal;

        return $this;
    }

    /**
     * Get f092ftotal
     *
     * @return float
     */
    public function getF092ftotal()
    {
        return $this->f092ftotal;
    }

    /**
     * Set f092fstatus
     *
     * @param integer $f092fstatus
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fstatus($f092fstatus)
    {
        $this->f092fstatus = $f092fstatus;

        return $this;
    }

    /**
     * Get f092fstatus
     *
     * @return integer
     */
    public function getF092fstatus()
    {
        return $this->f092fstatus;
    }

    /**
     * Set f092fcreatedBy
     *
     * @param integer $f092fcreatedBy
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fcreatedBy($f092fcreatedBy)
    {
        $this->f092fcreatedBy = $f092fcreatedBy;

        return $this;
    }

    /**
     * Get f092fcreatedBy
     *
     * @return integer
     */
    public function getF092fcreatedBy()
    {
        return $this->f092fcreatedBy;
    }

    /**
     * Set f092fupdatedBy
     *
     * @param integer $f092fupdatedBy
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fupdatedBy($f092fupdatedBy)
    {
        $this->f092fupdatedBy = $f092fupdatedBy;

        return $this;
    }

    /**
     * Get f092fupdatedBy
     *
     * @return integer
     */
    public function getF092fupdatedBy()
    {
        return $this->f092fupdatedBy;
    }

    /**
     * Set f092fcreatedDtTm
     *
     * @param \DateTime $f092fcreatedDtTm
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fcreatedDtTm($f092fcreatedDtTm)
    {
        $this->f092fcreatedDtTm = $f092fcreatedDtTm;

        return $this;
    }

    /**
     * Get f092fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fcreatedDtTm()
    {
        return $this->f092fcreatedDtTm;
    }

    /**
     * Set f092fupdatedDtTm
     *
     * @param \DateTime $f092fupdatedDtTm
     *
     * @return T092fnonPoInvoiceDetails
     */
    public function setF092fupdatedDtTm($f092fupdatedDtTm)
    {
        $this->f092fupdatedDtTm = $f092fupdatedDtTm;

        return $this;
    }

    /**
     * Get f092fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF092fupdatedDtTm()
    {
        return $this->f092fupdatedDtTm;
    }
}
