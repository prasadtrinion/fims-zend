<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T063fsponsorBill
 *
 * @ORM\Table(name="t063fsponsor_bill", indexes={@ORM\Index(name="f063fid_sponsor", columns={"f063fid_sponsor"})})
 * @ORM\Entity(repositoryClass="Application\Repository\SponsorBillRepository")
 */
class T063fsponsorBill
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f063fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fid_sponsor", type="integer", nullable=false)
     */
    private $f063fidSponsor;

    /**
     * @var string
     *
     * @ORM\Column(name="f063fbill_no", type="string", length=50, nullable=false)
     */
    private $f063fbillNo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fdate", type="datetime", nullable=false)
     */
    private $f063fdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063famount", type="integer",  nullable=false)
     */
    private $f063famount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="f063fstatus", type="boolean", nullable=false)
     */
    private $f063fstatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f063fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f063fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fcreated_by", type="integer", nullable=false)
     */
    private $f063fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f063fupdated_by", type="integer", nullable=false)
     */
    private $f063fupdatedBy;

    /**
     * Get f063fid
     *
     * @return integer
     */
    public function getF063fid()
    {
        return $this->f063fid;
    }

    /**
     * Set f063fidSponsor
     *
     * @param integer $f063fidSponsor
     *
     * @return T063fsponsorBill
     */
    public function setF063fidSponsor($f063fidSponsor)
    {
        $this->f063fidSponsor = $f063fidSponsor;

        return $this;
    }

    /**
     * Get f063fidSponsor
     *
     * @return integer
     */
    public function getF063fidSponsor()
    {
        return $this->f063fidSponsor;
    }

    /**
     * Set f063fbillNo
     *
     * @param string $f063fbillNo
     *
     * @return T063fsponsorBill
     */
    public function setF063fbillNo($f063fbillNo)
    {
        $this->f063fbillNo = $f063fbillNo;

        return $this;
    }

    /**
     * Get f063fbillNo
     *
     * @return string
     */
    public function getF063fbillNo()
    {
        return $this->f063fbillNo;
    }

    /**
     * Set f063fdate
     *
     * @param \DateTime $f063fdate
     *
     * @return T063fsponsorBill
     */
    public function setF063fdate($f063fdate)
    {
        $this->f063fdate = $f063fdate;

        return $this;
    }

    /**
     * Get f063fdate
     *
     * @return \DateTime
     */
    public function getF063fdate()
    {
        return $this->f063fdate;
    }

    /**
     * Set f063famount
     *
     * @param integer $f063famount
     *
     * @return T063fsponsorBill
     */
    public function setF063famount($f063famount)
    {
        $this->f063famount = $f063famount;

        return $this;
    }

    /**
     * Get f063famount
     *
     * @return integer
     */
    public function getF063famount()
    {
        return $this->f063famount;
    }

    /**
     * Set f063fstatus
     *
     * @param integer $f063fstatus
     *
     * @return T063fsponsorBill
     */
    public function setF063fstatus($f063fstatus)
    {
        $this->f063fstatus = $f063fstatus;

        return $this;
    }

    /**
     * Get f063fstatus
     *
     * @return integer
     */
    public function getF063fstatus()
    {
        return $this->f063fstatus;
    }

    /**
     * Set f063fcreatedDtTm
     *
     * @param \DateTime $f063fcreatedDtTm
     *
     * @return T063fsponsorBill
     */
    public function setF063fcreatedDtTm($f063fcreatedDtTm)
    {
        $this->f063fcreatedDtTm = $f063fcreatedDtTm;

        return $this;
    }

    /**
     * Get f063fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fcreatedDtTm()
    {
        return $this->f063fcreatedDtTm;
    }

    /**
     * Set f063fupdatedDtTm
     *
     * @param \DateTime $f063fupdatedDtTm
     *
     * @return T063fsponsorBill
     */
    public function setF063fupdatedDtTm($f063fupdatedDtTm)
    {
        $this->f063fupdatedDtTm = $f063fupdatedDtTm;

        return $this;
    }

    /**
     * Get f063fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF063fupdatedDtTm()
    {
        return $this->f063fupdatedDtTm;
    }

    /**
     * Set f063fcreatedBy
     *
     * @param integer $f063fcreatedBy
     *
     * @return T063fsponsorBill
     */
    public function setF063fcreatedBy($f063fcreatedBy)
    {
        $this->f063fcreatedBy = $f063fcreatedBy;

        return $this;
    }

    /**
     * Get f063fcreatedBy
     *
     * @return integer
     */
    public function getF063fcreatedBy()
    {
        return $this->f063fcreatedBy;
    }

    /**
     * Set f063fupdatedBy
     *
     * @param integer $f063fupdatedBy
     *
     * @return T063fsponsorBill
     */
    public function setF063fupdatedBy($f063fupdatedBy)
    {
        $this->f063fupdatedBy = $f063fupdatedBy;

        return $this;
    }

    /**
     * Get f063fupdatedBy
     *
     * @return integer
     */
    public function getF063fupdatedBy()
    {
        return $this->f063fupdatedBy;
    }
}

