<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T064finvestmentRegistration
 *
 * @ORM\Table(name="t064finvestment_registration", uniqueConstraints={@ORM\UniqueConstraint(name="f064fname_UNIQUE", columns={"f064fname"})})
* @ORM\Entity(repositoryClass="Application\Repository\InvestmentRegistrationRepository")
 */
class T064finvestmentRegistration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f064fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f064freference_number", type="string", length=255, nullable=true)
     */
    private $f064freferenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fid_application", type="integer", nullable=true)
     */
    private $f064fidApplication;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fuum_bank", type="integer", nullable=true)
     */
    private $f064fuumBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064finvestment_bank", type="integer", nullable=true)
     */
    private $f064finvestmentBank;


    /**
     * @var string
     *
     * @ORM\Column(name="f064fname", type="string", nullable=true)
     */
    private $f064fname;

    /**
     * @var float
     *
     * @ORM\Column(name="f064fsum", type="integer", nullable=true)
     */
    private $f064fsum;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064frate_of_interest", type="integer", nullable=true)
     */
    private $f064frateOfInterest;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064finterest_to_bank", type="integer", nullable=true)
     */
    private $f064finterstToBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064finterest_day", type="integer", nullable=true)
     */
    private $f064finterestDay;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064finvestment_status", type="integer", nullable=true)
     */
    private $f064finvestmentStatus;

     /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved1_status", type="integer", nullable=true)
     */
    private $f064fapproved1Status=0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved2_status", type="integer", nullable=true)
     */
    private $f064fapproved2Status=0;

     /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved3_status", type="integer", nullable=true)
     */
    private $f064fapproved3Status=0;

    /**
     * @var string
     *
     * @ORM\Column(name="f064freason1", type="string", length=300, nullable=true)
     */
    private $f064freason1 = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f064freason2", type="string", length=300, nullable=true)
     */
    private $f064freason2= 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="f064freason3", type="string", length=300, nullable=true)
     */
    private $f064freason3 = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved1_by", type="integer", nullable=true)
     */
    private $f064fapproved1By = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved2_by", type="integer", nullable=true)
     */
    private $f064fapproved2By = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved3_by", type="integer", nullable=true)
     */
    private $f064fapproved3By = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved1_updated_by", type="integer", nullable=true)
     */
    private $f064fapproved1UpdatedBy = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved2_updated_by", type="integer", nullable=true)
     */
    private $f064fapproved2UpdatedBy = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fapproved3_updated_by", type="integer", nullable=true)
     */
    private $f064fapproved3UpdatedBy = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fis_online", type="integer", nullable=true)
     */
    private $f064fisOnline = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fcreated_by", type="integer", nullable=true)
     */
    private $f064fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fupdated_by", type="integer", nullable=true)
     */
    private $f064fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f064fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fstatus", type="integer", nullable=true)
     */
    private $f064fstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcontact_person", type="string", length=25, nullable=true)
     */
    private $f064fcontactPerson;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064feffective_date", type="datetime", nullable=true)
     */
    private $f064feffectiveDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f064fmaturity_date", type="datetime", nullable=true)
     */
    private $f064fmaturityDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fprevious_status", type="integer", nullable=true)
     */
    private $f064fpreviousStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fprevious_id", type="integer", nullable=true)
     */
    private $f064fpreviousId;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcontact_person2", type="string", length=25, nullable=true)
     */
    private $f064fcontactPerson2;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcontact_email", type="string", length=25, nullable=true)
     */
    private $f064fcontactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fcontact_email2", type="string", length=25, nullable=true)
     */
    private $f064fcontactEmail2;

     /**
     * @var string
     *
     * @ORM\Column(name="f064ffund_code", type="string",length=100, nullable=true)
     */
    private $f064ffundCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064factivity_code", type="string",length=100, nullable=true)
     */
    private $f064factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fdepartment_code", type="string",length=100, nullable=true)
     */
    private $f064fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f064faccount_code", type="string",length=100, nullable=true)
     */
    private $f064faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f064fis_withdraw", type="integer", nullable=true)
     */
    private $f064fisWithdraw = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="f064freason", type="string", length=150, nullable=true)
     */
    private $f064freason ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064ffile", type="string", length=150, nullable=true)
     */
    private $f064ffile ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fprofit_amount", type="integer", nullable=true)
     */
    private $f064fprofitAmount ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fperiod_type", type="integer", nullable=true)
     */
    private $f064fperiodType ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064fduration_type", type="string",length=50, nullable=true)
     */
    private $f064fdurationType ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064ffinal_status", type="integer", nullable=true)
     */
    private $f064ffinalStatus = 0 ;

    /**
     * @var string
     *
     * @ORM\Column(name="f064ffinal_reason", type="string", length=255, nullable=true)
     */
    private $f064ffinalReason;

    

    /**
     * Get f064fid
     *
     * @return integer
     */
    public function getF064fid()
    {
        return $this->f064fid;
    }

    /**
     * Set f064freferenceNumber
     *
     * @param string $f064freferenceNumber
     *
     * @return T064finvestmentRegistration
     */
    public function setF064freferenceNumber($f064freferenceNumber)
    {
        $this->f064freferenceNumber = $f064freferenceNumber;

        return $this;
    }

    /**
     * Get f064freferenceNumber
     *
     * @return string
     */
    public function getF064freferenceNumber()
    {
        return $this->f064freferenceNumber;
    }

    /**
     * Set f064fidApplication
     *
     * @param integer $f064fidApplication
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fidApplication($f064fidApplication)
    {
        $this->f064fidApplication = $f064fidApplication;

        return $this;
    }

    /**
     * Get f064fidApplication
     *
     * @return integer
     */
    public function getF064fidApplication()
    {
        return $this->f064fidApplication;
    }

    /**
     * Set f064fuumBank
     *
     * @param integer $f064fuumBank
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fuumBank($f064fuumBank)
    {
        $this->f064fuumBank = $f064fuumBank;

        return $this;
    }

    /**
     * Get f064fuumBank
     *
     * @return integer
     */
    public function getF064fuumBank()
    {
        return $this->f064fuumBank;
    }

    /**
     * Set f064finvestmentBank
     *
     * @param integer $f064finvestmentBank
     *
     * @return T064finvestmentRegistration
     */
    public function setF064finvestmentBank($f064finvestmentBank)
    {
        $this->f064finvestmentBank = $f064finvestmentBank;

        return $this;
    }

    /**
     * Get f064finvestmentBank
     *
     * @return integer
     */
    public function getF064finvestmentBank()
    {
        return $this->f064finvestmentBank;
    }

    /**
     * Set f064fidGlcode
     *
     * @param integer $f064fidGlcode
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fidGlcode($f064fidGlcode)
    {
        $this->f064fidGlcode = $f064fidGlcode;

        return $this;
    }

    /**
     * Get f064fidGlcode
     *
     * @return integer
     */
    public function getF064fidGlcode()
    {
        return $this->f064fidGlcode;
    }

    /**
     * Set f064finterstToBank
     *
     * @param integer $f064finterstToBank
     *
     * @return T064finvestmentRegistration
     */
    public function setF064finterstToBank($f064finterstToBank)
    {
        $this->f064finterstToBank = $f064finterstToBank;

        return $this;
    }

    /**
     * Get f064finterstToBank
     *
     * @return integer
     */
    public function getF064finterstToBank()
    {
        return $this->f064finterstToBank;
    }

    /**
     * Set f064finterestDay
     *
     * @param integer $f064finterestDay
     *
     * @return T064finvestmentRegistration
     */
    public function setF064finterestDay($f064finterestDay)
    {
        $this->f064finterestDay = $f064finterestDay;

        return $this;
    }

    /**
     * Get f064finterestDay
     *
     * @return integer
     */
    public function getF064finterestDay()
    {
        return $this->f064finterestDay;
    }

    /**
     * Set f064finvestmentStatus
     *
     * @param integer $f064finvestmentStatus
     *
     * @return T064finvestmentRegistration
     */
    public function setF064finvestmentStatus($f064finvestmentStatus)
    {
        $this->f064finvestmentStatus = $f064finvestmentStatus;

        return $this;
    }

    /**
     * Get f064finvestmentStatus
     *
     * @return integer
     */
    public function getF064finvestmentStatus()
    {
        return $this->f064finvestmentStatus;
    }

    /**
     * Set f064fpreviousStatus
     *
     * @param string $f064fpreviousStatus
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fpreviousStatus($f064fpreviousStatus)
    {
        $this->f064fpreviousStatus = $f064fpreviousStatus;

        return $this;
    }

    /**
     * Get f064fpreviousStatus
     *
     * @return string
     */
    public function getF064fpreviousStatus()
    {
        return $this->f064fpreviousStatus;
    }

    /**
     * Set f064fpreviousId
     *
     * @param integer $f064fpreviousId
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fpreviousId($f064fpreviousId)
    {
        $this->f064fpreviousId = $f064fpreviousId;

        return $this;
    }

    /**
     * Get f064fpreviousId
     *
     * @return integer
     */
    public function getF064fpreviousId()
    {
        return $this->f064fpreviousId;
    }

    /**
     * Set f064fname
     *
     * @param string $f064fname
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fname($f064fname)
    {
        $this->f064fname = $f064fname;

        return $this;
    }

    /**
     * Get f064fname
     *
     * @return string
     */
    public function getF064fname()
    {
        return $this->f064fname;
    }

    /**
     * Set f064fsum
     *
     * @param integer $f064fsum
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fsum($f064fsum)
    {
        $this->f064fsum = $f064fsum;

        return $this;
    }

    /**
     * Get f064fsum
     *
     * @return integer
     */
    public function getF064fsum()
    {
        return $this->f064fsum;
    }

    /**
     * Set f064frateOfInterest
     *
     * @param integer $f064frateOfInterest
     *
     * @return T064finvestmentRegistration
     */
    public function setF064frateOfInterest($f064frateOfInterest)
    {
        $this->f064frateOfInterest = $f064frateOfInterest;

        return $this;
    }

    /**
     * Get f064frateOfInterest
     *
     * @return integer
     */
    public function getF064frateOfInterest()
    {
        return $this->f064frateOfInterest;
    }

    /**
     * Set f064fcreatedBy
     *
     * @param integer $f064fcreatedBy
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcreatedBy($f064fcreatedBy)
    {
        $this->f064fcreatedBy = $f064fcreatedBy;

        return $this;
    }

    /**
     * Get f064fcreatedBy
     *
     * @return integer
     */
    public function getF064fcreatedBy()
    {
        return $this->f064fcreatedBy;
    }

    /**
     * Set f064fupdatedBy
     *
     * @param integer $f064fupdatedBy
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fupdatedBy($f064fupdatedBy)
    {
        $this->f064fupdatedBy = $f064fupdatedBy;

        return $this;
    }

    /**
     * Get f064fupdatedBy
     *
     * @return integer
     */
    public function getF064fupdatedBy()
    {
        return $this->f064fupdatedBy;
    }

    /**
     * Set f064fcreatedDtTm
     *
     * @param \DateTime $f064fcreatedDtTm
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcreatedDtTm($f064fcreatedDtTm)
    {
        $this->f064fcreatedDtTm = $f064fcreatedDtTm;

        return $this;
    }

    /**
     * Get f064fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fcreatedDtTm()
    {
        return $this->f064fcreatedDtTm;
    }

    /**
     * Set f064fupdatedDtTm
     *
     * @param \DateTime $f064fupdatedDtTm
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fupdatedDtTm($f064fupdatedDtTm)
    {
        $this->f064fupdatedDtTm = $f064fupdatedDtTm;

        return $this;
    }

    /**
     * Get f064fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF064fupdatedDtTm()
    {
        return $this->f064fupdatedDtTm;
    }

    /**
     * Set f064fstatus
     *
     * @param integer $f064fstatus
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fstatus($f064fstatus)
    {
        $this->f064fstatus = $f064fstatus;

        return $this;
    }

    /**
     * Get f064fstatus
     *
     * @return integer
     */
    public function getF064fstatus()
    {
        return $this->f064fstatus;
    }

    /**
     * Set f064fcontactPerson
     *
     * @param string $f064fcontactPerson
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcontactPerson($f064fcontactPerson)
    {
        $this->f064fcontactPerson = $f064fcontactPerson;

        return $this;
    }

    /**
     * Get f064fcontactPerson
     *
     * @return string
     */
    public function getF064fcontactPerson()
    {
        return $this->f064fcontactPerson;
    }

    /**
     * Set f064fcontactPerson2
     *
     * @param string $f064fcontactPerson2
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcontactPerson2($f064fcontactPerson2)
    {
        $this->f064fcontactPerson2 = $f064fcontactPerson2;

        return $this;
    }

    /**
     * Get f064fcontactPerson2
     *
     * @return string
     */
    public function getF064fcontactPerson2()
    {
        return $this->f064fcontactPerson2;
    }

    /**
     * Set f064fcontactEmail
     *
     * @param string $f064fcontactEmail
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcontactEmail($f064fcontactEmail)
    {
        $this->f064fcontactEmail = $f064fcontactEmail;

        return $this;
    }

    /**
     * Get f064fcontactEmail
     *
     * @return string
     */
    public function getF064fcontactEmail()
    {
        return $this->f064fcontactEmail;
    }

    /**
     * Set f064fcontactEmail2
     *
     * @param string $f064fcontactEmail2
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fcontactEmail2($f064fcontactEmail2)
    {
        $this->f064fcontactEmail2 = $f064fcontactEmail2;

        return $this;
    }

    /**
     * Get f064fcontactEmail2
     *
     * @return string
     */
    public function getF064fcontactEmail2()
    {
        return $this->f064fcontactEmail2;
    }

    /**
     * Set f064feffectiveDate
     *
     * @param \DateTime $f064feffectiveDate
     *
     * @return T064finvestmentRegistration
     */
    public function setF064feffectiveDate($f064feffectiveDate)
    {
        $this->f064feffectiveDate = $f064feffectiveDate;

        return $this;
    }

    /**
     * Get f064feffectiveDate
     *
     * @return \DateTime
     */
    public function getF064feffectiveDate()
    {
        return $this->f064feffectiveDate;
    }

    /**
     * Set f064fmaturityDate
     *
     * @param \DateTime $f064fmaturityDate
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fmaturityDate($f064fmaturityDate)
    {
        $this->f064fmaturityDate = $f064fmaturityDate;

        return $this;
    }

    /**
     * Get f064fmaturityDate
     *
     * @return \DateTime
     */
    public function getF064fmaturityDate()
    {
        return $this->f064fmaturityDate;
    }

   /**
     * Set f064ffundCode
     *
     * @param string $f064ffundCode
     *
     * @return T064finvestmentRegistration
     */
    public function setF064ffundCode($f064ffundCode)
    {
        $this->f064ffundCode = $f064ffundCode;

        return $this;
    }

    /**
     * Get f064ffundCode
     *
     * @return string
     */
    public function getF064ffundCode()
    {
        return $this->f064ffundCode;
    }

    /**
     * Set f064factivityCode
     *
     * @param string $f064factivityCode
     *
     * @return T064finvestmentRegistration
     */
    public function setF064factivityCode($f064factivityCode)
    {
        $this->f064factivityCode = $f064factivityCode;

        return $this;
    }

    /**
     * Get f064factivityCode
     *
     * @return string
     */
    public function getF064factivityCode()
    {
        return $this->f064factivityCode;
    }

    /**
     * Set f064fdepartmentCode
     *
     * @param string $f064fdepartmentCode
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fdepartmentCode($f064fdepartmentCode)
    {
        $this->f064fdepartmentCode = $f064fdepartmentCode;

        return $this;
    }

    /**
     * Get f064fdepartmentCode
     *
     * @return string
     */
    public function getF064fdepartmentCode()
    {
        return $this->f064fdepartmentCode;
    }

    /**
     * Set f064faccountCode
     *
     * @param string $f064faccountCode
     *
     * @return T064finvestmentRegistration
     */
    public function setF064faccountCode($f064faccountCode)
    {
        $this->f064faccountCode = $f064faccountCode;

        return $this;
    }

    /**
     * Get f064faccountCode
     *
     * @return string
     */
    public function getF064faccountCode()
    {
        return $this->f064faccountCode;
    }

    /**
     * Set f064fisOnline
     *
     * @param integer $f064fisOnline
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fisOnline($f064fisOnline)
    {
        $this->f064fisOnline = $f064fisOnline;

        return $this;
    }

    /**
     * Get f064fisOnline
     *
     * @return integer
     */
    public function getF064fisOnline()
    {
        return $this->f064fisOnline;
    }

    /**
     * Set f064fapproved1Status
     *
     * @param integer $f064fapproved1Status
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved1Status($f064fapproved1Status)
    {
        $this->f064fapproved1Status = $f064fapproved1Status;

        return $this;
    }

    /**
     * Get f064fapproved1Status
     *
     * @return integer
     */
    public function getF064fapproved1Status()
    {
        return $this->f064fapproved1Status;
    }

    /**
     * Set f064fapproved2Status
     *
     * @param integer $f064fapproved2Status
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved2Status($f064fapproved2Status)
    {
        $this->f064fapproved2Status = $f064fapproved2Status;

        return $this;
    }

    /**
     * Get f064fapproved2Status
     *
     * @return integer
     */
    public function getF064fapproved2Status()
    {
        return $this->f064fapproved2Status;
    }

    /**
     * Set f064fapproved3Status
     *
     * @param integer $f064fapproved3Status
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved3Status($f064fapproved3Status)
    {
        $this->f064fapproved3Status = $f064fapproved3Status;

        return $this;
    }

    /**
     * Get f064fapproved3Status
     *
     * @return integer
     */
    public function getF064fapproved3Status()
    {
        return $this->f064fapproved3Status;
    }

    /**
     * Set f064freason1
     *
     * @param string $f064freason1
     *
     * @return T064finvestmentRegistration
     */
    public function setF064freason1($f064freason1)
    {
        $this->f064freason1 = $f064freason1;

        return $this;
    }

    /**
     * Get f064freason1
     *
     * @return string
     */
    public function getF064freason1()
    {
        return $this->f064freason1;
    }

    /**
     * Set f064freason2
     *
     * @param string $f064freason2
     *
     * @return T064finvestmentRegistration
     */
    public function setF064freason2($f064freason2)
    {
        $this->f064freason2 = $f064freason2;

        return $this;
    }

    /**
     * Get f064freason2
     *
     * @return string
     */
    public function getF064freason2()
    {
        return $this->f064freason2;
    }

    /**
     * Set f064freason3
     *
     * @param string $f064freason3
     *
     * @return T064finvestmentRegistration
     */
    public function setF064freason3($f064freason3)
    {
        $this->f064freason3 = $f064freason3;

        return $this;
    }

    /**
     * Get f064freason3
     *
     * @return string
     */
    public function getF064freason3()
    {
        return $this->f064freason3;
    }

    /**
     * Set f064fapproved1By
     *
     * @param integer $f064fapproved1By
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved1By($f064fapproved1By)
    {
        $this->f064fapproved1By = $f064fapproved1By;

        return $this;
    }

    /**
     * Get f064fapproved1By
     *
     * @return integer
     */
    public function getF064fapproved1By()
    {
        return $this->f064fapproved1By;
    }

    /**
     * Set f064fapproved2By
     *
     * @param integer $f064fapproved2By
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved2By($f064fapproved2By)
    {
        $this->f064fapproved2By = $f064fapproved2By;

        return $this;
    }

    /**
     * Get f064fapproved2By
     *
     * @return integer
     */
    public function getF064fapproved2By()
    {
        return $this->f064fapproved2By;
    }

    /**
     * Set f064fapproved3By
     *
     * @param integer $f064fapproved3By
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved3By($f064fapproved3By)
    {
        $this->f064fapproved3By = $f064fapproved3By;

        return $this;
    }

    /**
     * Get f064fapproved3By
     *
     * @return integer
     */
    public function getF064fapproved3By()
    {
        return $this->f064fapproved3By;
    }

    /**
     * Set f064fapproved1UpdatedBy
     *
     * @param integer $f064fapproved1UpdatedBy
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved1UpdatedBy($f064fapproved1UpdatedBy)
    {
        $this->f064fapproved1UpdatedBy = $f064fapproved1UpdatedBy;

        return $this;
    }

    /**
     * Get f064fapproved1UpdatedBy
     *
     * @return integer
     */
    public function getF064fapproved1UpdatedBy()
    {
        return $this->f064fapproved1UpdatedBy;
    }

    /**
     * Set f064fapproved2UpdatedBy
     *
     * @param integer $f064fapproved2UpdatedBy
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved2UpdatedBy($f064fapproved2UpdatedBy)
    {
        $this->f064fapproved2UpdatedBy = $f064fapproved2UpdatedBy;

        return $this;
    }

    /**
     * Get f064fapproved2UpdatedBy
     *
     * @return integer
     */
    public function getF064fapproved2UpdatedBy()
    {
        return $this->f064fapproved2UpdatedBy;
    }

    /**
     * Set f064fapproved3UpdatedBy
     *
     * @param integer $f064fapproved3UpdatedBy
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fapproved3UpdatedBy($f064fapproved3UpdatedBy)
    {
        $this->f064fapproved3UpdatedBy = $f064fapproved3UpdatedBy;

        return $this;
    }

    /**
     * Get f064fapproved3UpdatedBy
     *
     * @return integer
     */
    public function getF064fapproved3UpdatedBy()
    {
        return $this->f064fapproved3UpdatedBy;
    }

    /**
     * Set f064ffile
     *
     * @param string $f064ffile
     *
     * @return T064finvestmentRegistration
     */
    public function setF064ffile($f064ffile)
    {
        $this->f064ffile = $f064ffile;

        return $this;
    }

    /**
     * Get f064ftoken
     *
     * @return string
     */
    public function getF064ftoken()
    {
        return $this->f064ftoken;
    }

    /**
     * Set f064fisWithdraw
     *
     * @param integer $f064fisWithdraw
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fisWithdraw($f064fisWithdraw)
    {
        $this->f064fisWithdraw = $f064fisWithdraw;

        return $this;
    }

    /**
     * Get f064fisWithdraw
     *
     * @return integer
     */
    public function getF064fisWithdraw()
    {
        return $this->f064fisWithdraw;
    }

    /**
     * Set f064freason
     *
     * @param string $f064freason
     *
     * @return T064finvestmentRegistration
     */
    public function setF064freason($f064freason)
    {
        $this->f064freason = $f064freason;

        return $this;
    }

    /**
     * Get f064freason
     *
     * @return string
     */
    public function getF064freason()
    {
        return $this->f064freason;
    }

    /**
     * Set f064fprofitAmount
     *
     * @param string $f064fprofitAmount
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fprofitAmount($f064fprofitAmount)
    {
        $this->f064fprofitAmount = $f064fprofitAmount;

        return $this;
    }

    /**
     * Get f064fprofitAmount
     *
     * @return string
     */
    public function getF064fprofitAmount()
    {
        return $this->f064fprofitAmount;
    }

    /**
     * Set f064fperiodType
     *
     * @param string $f064fperiodType
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fperiodType($f064fperiodType)
    {
        $this->f064fperiodType = $f064fperiodType;

        return $this;
    }

    /**
     * Get f064fperiodType
     *
     * @return string
     */
    public function getF064fperiodType()
    {
        return $this->f064fperiodType;
    }

    /**
     * Set f064fdurationType
     *
     * @param string $f064fdurationType
     *
     * @return T064finvestmentRegistration
     */
    public function setF064fdurationType($f064fdurationType)
    {
        $this->f064fdurationType = $f064fdurationType;

        return $this;
    }

    /**
     * Get f064fdurationType
     *
     * @return string
     */
    public function getF064fdurationType()
    {
        return $this->f064fdurationType;
    }

    /**
     * Set f064ffinalStatus
     *
     * @param string $f064ffinalStatus
     *
     * @return T064finvestmentRegistration
     */
    public function setF064ffinalStatus($f064ffinalStatus)
    {
        $this->f064ffinalStatus = $f064ffinalStatus;

        return $this;
    }

    /**
     * Get f064ffinalStatus
     *
     * @return string
     */
    public function getF064ffinalStatus()
    {
        return $this->f064ffinalStatus;
    }

    /**
     * Set f064ffinalReason
     *
     * @param string $f064ffinalReason
     *
     * @return T064finvestmentRegistration
     */
    public function setF064ffinalReason($f064ffinalReason)
    {
        $this->f064ffinalReason = $f064ffinalReason;

        return $this;
    }

    /**
     * Get f064ffinalReason
     *
     * @return string
     */
    public function getF064ffinalReason()
    {
        return $this->f064ffinalReason;
    }

}
