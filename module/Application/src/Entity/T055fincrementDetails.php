<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T055fincrementDetails
 *
 * @ORM\Table(name="t055fincrement_details")
 * @ORM\Entity(repositoryClass="Application\Repository\BudgetIncrementRepository")
 */
class T055fincrementDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid_details", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f055fidDetails;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fid_master", type="integer", nullable=false)
     */
    private $f055fidMaster;

 
     /**
     * @var string
     *
     * @ORM\Column(name="f055ffund_code", type="string",length=20, nullable=false)
     */
    private $f055ffundCode;

     /**
     * @var string
     *
     * @ORM\Column(name="f055fdepartment_code", type="string",length=20, nullable=false)
     */
    private $f055fdepartmentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f055factivity_code", type="string",length=20, nullable=false)
     */
    private $f055factivityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="f055faccount_code", type="string",length=20, nullable=false)
     */
    private $f055faccountCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055famount", type="integer",  nullable=false)
     */
    private $f055famount;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fstatus", type="integer", nullable=false)
     */
    private $f055fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fcreated_by", type="integer", nullable=false)
     */
    private $f055fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fupdated_by", type="integer", nullable=false)
     */
    private $f055fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f055fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f055fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq1", type="integer",  nullable=true)
     */
    private $f055fq1;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq2", type="integer",  nullable=true)
     */
    private $f055fq2;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq3", type="integer",  nullable=true)
     */
    private $f055fq3;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fq4", type="integer",  nullable=true)
     */
    private $f055fq4;

     /**
     * @var string
     *
     * @ORM\Column(name="f055fadjustment_type", type="string", length=255, nullable=true)
     */
    private $f055fadjustmentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="f055fveriment_amount", type="integer",  nullable=true)
     */
    private $f055fverimentAmount;

    /**
     * Get f055fidDetails
     *
     * @return integer
     */
    public function getF055fidDetails()
    {
        return $this->f055fidDetails;
    }

     /**
     * Set f055fverimentAmount
     *
     * @param integer $f055fverimentAmount
     *
     * @return T055fverimentDetails
     */
    public function setF055fverimentAmount($f055fverimentAmount)
    {
        $this->f055fverimentAmount = $f055fverimentAmount;

        return $this;
    }

    /**
     * Get f055fverimentAmount
     *
     * @return integer
     */
    public function getF055fverimentAmount()
    {
        return $this->f055fverimentAmount;
    }

    /**
     * Set f055fadjustmentType
     *
     * @param string $f055fadjustmentType
     *
     * @return T055fincrementMaster
     */
    public function setF055fadjustmentType($f055fadjustmentType)
    {
        $this->f055fadjustmentType = $f055fadjustmentType;

        return $this;
    }
 
    /**
     * Get f055fadjustmentType
     *
     * @return string
     */
    public function getF055fadjustmentType()
    {
        return $this->f055fadjustmentType;
    }
    /**
     * Set f055fidMaster
     *
     * @param integer $f055fidMaster
     *
     * @return T055fincrementDetails
     */
    public function setF055fidMaster($f055fidMaster)
    {
        $this->f055fidMaster = $f055fidMaster;

        return $this;
    }

    /**
     * Get f055fidMaster
     *
     * @return integer
     */
    public function getF055fidMaster()
    {
        return $this->f055fidMaster;
    }
    
    /**
     * Set f055ffundCode
     *
     * @param string $f055ffundCode
     *
     * @return T055fincrementDetails
     */
    public function setF055ffundCode($f055ffundCode)
    {
        $this->f055ffundCode = $f055ffundCode;

        return $this;
    }

    /**
     * Get f055ffundCode
     *
     * @return string
     */
    public function getF055ffundCode()
    {
        return $this->f055ffundCode;
    }

    /**
     * Set f055faccountCode
     *
     * @param string $f055faccountCode
     *
     * @return T055fincrementDetails
     */
    public function setF055faccountCode($f055faccountCode)
    {
        $this->f055faccountCode = $f055faccountCode;

        return $this;
    }

    /**
     * Get f055faccountCode
     *
     * @return string
     */
    public function getF055faccountCode()
    {
        return $this->f055faccountCode;
    }

    /**
     * Set f055factivityCode
     *
     * @param string $f055factivityCode
     *
     * @return T055fincrementDetails
     */
    public function setF055factivityCode($f055factivityCode)
    {
        $this->f055factivityCode = $f055factivityCode;

        return $this;
    }

    /**
     * Get f055factivityCode
     *
     * @return string
     */
    public function getF055factivityCode()
    {
        return $this->f055factivityCode;
    }

    /**
     * Set f055fdepartmentCode
     *
     * @param string $f055fdepartmentCode
     *
     * @return T055fincrementDetails
     */
    public function setF055fdepartmentCode($f055fdepartmentCode)
    {
        $this->f055fdepartmentCode = $f055fdepartmentCode;

        return $this;
    }

    /**
     * Get f055fdepartmentCode
     *
     * @return string
     */
    public function getF055fdepartmentCode()
    {
        return $this->f055fdepartmentCode;
    }


    /**
     * Set f055famount
     *
     * @param string $f055famount
     *
     * @return T055fincrementDetails
     */
    public function setF055famount($f055famount)
    {
        $this->f055famount = $f055famount;

        return $this;
    }

    /**
     * Get f055famount
     *
     * @return string
     */
    public function getF055famount()
    {
        return $this->f055famount;
    }

    /**
     * Set f055fapprovalStatus
     *
     * @param integer $f055fapprovalStatus
     *
     * @return T055fincrementDetails
     */
    public function setF055fapprovalStatus($f055fapprovalStatus)
    {
        $this->f055fapprovalStatus = $f055fapprovalStatus;

        return $this;
    }

    /**
     * Get f055fapprovalStatus
     *
     * @return integer
     */
    public function getF055fapprovalStatus()
    {
        return $this->f055fapprovalStatus;
    }

    /**
     * Set f055fstatus
     *
     * @param integer $f055fstatus
     *
     * @return T055fincrementDetails
     */
    public function setF055fstatus($f055fstatus)
    {
        $this->f055fstatus = $f055fstatus;

        return $this;
    }

    /**
     * Get f055fstatus
     *
     * @return integer
     */
    public function getF055fstatus()
    {
        return $this->f055fstatus;
    }

    /**
     * Set f055fcreatedBy
     *
     * @param integer $f055fcreatedBy
     *
     * @return T055fincrementDetails
     */
    public function setF055fcreatedBy($f055fcreatedBy)
    {
        $this->f055fcreatedBy = $f055fcreatedBy;

        return $this;
    }

    /**
     * Get f055fcreatedBy
     *
     * @return integer
     */
    public function getF055fcreatedBy()
    {
        return $this->f055fcreatedBy;
    }

    /**
     * Set f055fupdatedBy
     *
     * @param integer $f055fupdatedBy
     *
     * @return T055fincrementDetails
     */
    public function setF055fupdatedBy($f055fupdatedBy)
    {
        $this->f055fupdatedBy = $f055fupdatedBy;

        return $this;
    }

    /**
     * Get f055fupdatedBy
     *
     * @return integer
     */
    public function getF055fupdatedBy()
    {
        return $this->f055fupdatedBy;
    }

    /**
     * Set f055fcreatedDtTm
     *
     * @param \DateTime $f055fcreatedDtTm
     *
     * @return T055fincrementDetails
     */
    public function setF055fcreatedDtTm($f055fcreatedDtTm)
    {
        $this->f055fcreatedDtTm = $f055fcreatedDtTm;

        return $this;
    }

    /**
     * Get f055fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fcreatedDtTm()
    {
        return $this->f055fcreatedDtTm;
    }

    /**
     * Set f055fupdatedDtTm
     *
     * @param \DateTime $f055fupdatedDtTm
     *
     * @return T055fincrementDetails
     */
    public function setF055fupdatedDtTm($f055fupdatedDtTm)
    {
        $this->f055fupdatedDtTm = $f055fupdatedDtTm;

        return $this;
    }

    /**
     * Get f055fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF055fupdatedDtTm()
    {
        return $this->f055fupdatedDtTm;
    }

    /**
     * Set f055fq1
     *
     * @param string $f055fq1
     *
     * @return T055fincrementDetails
     */
    public function setF055fq1($f055fq1)
    {
        $this->f055fq1 = $f055fq1;

        return $this;
    }

    /**
     * Get f055fq1
     *
     * @return string
     */
    public function getF055fq1()
    {
        return $this->f055fq1;
    }

    /**
     * Set f055fq2
     *
     * @param string $f055fq2
     *
     * @return T055fincrementDetails
     */
    public function setF055fq2($f055fq2)
    {
        $this->f055fq2 = $f055fq2;

        return $this;
    }

    /**
     * Get f055fq2
     *
     * @return string
     */
    public function getF055fq2()
    {
        return $this->f055fq2;
    }

    /**
     * Set f055fq3
     *
     * @param string $f055fq3
     *
     * @return T055fincrementDetails
     */
    public function setF055fq3($f055fq3)
    {
        $this->f055fq3 = $f055fq3;

        return $this;
    }

    /**
     * Get f055fq3
     *
     * @return string
     */
    public function getF055fq3()
    {
        return $this->f055fq3;
    }

    /**
     * Set f055fq4
     *
     * @param string $f055fq4
     *
     * @return T055fincrementDetails
     */
    public function setF055fq4($f055fq4)
    {
        $this->f055fq4 = $f055fq4;

        return $this;
    }

    /**
     * Get f055fq4
     *
     * @return string
     */
    public function getF055fq4()
    {
        return $this->f055fq4;
    }
}
