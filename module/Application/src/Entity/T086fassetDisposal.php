<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T086fassetDisposal
 *
 * @ORM\Table(name="t086fasset_disposal")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetDisposalRepository")
 */
class T086fassetDisposal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f086fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f086fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdisposal_name", type="string", length=50, nullable=true)
     */
    private $f086fdisposalName;

    /**
     * @var string
     *
     * @ORM\Column(name="f086fdescription", type="string", length=50, nullable=true)
     */
    private $f086fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fverification_status", type="integer", nullable=true)
     */
    private $f086fverificationStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fstatus", type="integer", nullable=true)
     */
    private $f086fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fcreated_by", type="integer", nullable=true)
     */
    private $f086fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f086fupdated_by", type="integer", nullable=true)
     */
    private $f086fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f086fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f086fupdatedDtTm;


    /**
     * Get f086fid
     *
     * @return integer
     */
    public function getF086fid()
    {
        return $this->f086fid;
    }

    /**
     * Set f086fdisposalName
     *
     * @param string $f086fdisposalName
     *
     * @return T086fassetDisposal
     */
    public function setF086fdisposalName($f086fdisposalName)
    {
        $this->f086fdisposalName = $f086fdisposalName;

        return $this;
    }

    /**
     * Get f086fdisposalName
     *
     * @return string
     */
    public function getF086fdisposalName()
    {
        return $this->f086fdisposalName;
    }

    /**
     * Set f086fdescription
     *
     * @param string $f086fdescription
     *
     * @return T086fassetDisposal
     */
    public function setF086fdescription($f086fdescription)
    {
        $this->f086fdescription = $f086fdescription;

        return $this;
    }

    /**
     * Get f086fdescription
     *
     * @return string
     */
    public function getF086fdescription()
    {
        return $this->f086fdescription;
    }

    /**
     * Set f086fverificationStatus
     *
     * @param integer $f086fverificationStatus
     *
     * @return T086fassetDisposal
     */
    public function setF086fidDepartment($f086fverificationStatus)
    {
        $this->f086fverificationStatus = $f086fverificationStatus;

        return $this;
    }

    /**
     * Get f086fverificationStatus
     *
     * @return integer
     */
    public function getF086fidDepartment()
    {
        return $this->f086fverificationStatus;
    }

    /**
     * Set f086fstatus
     *
     * @param integer $f086fstatus
     *
     * @return T086fassetDisposal
     */
    public function setF086fstatus($f086fstatus)
    {
        $this->f086fstatus = $f086fstatus;

        return $this;
    }

    /**
     * Get f086fstatus
     *
     * @return integer
     */
    public function getF086fstatus()
    {
        return $this->f086fstatus;
    }

    /**
     * Set f086fcreatedBy
     *
     * @param integer $f086fcreatedBy
     *
     * @return T086fassetDisposal
     */
    public function setF086fcreatedBy($f086fcreatedBy)
    {
        $this->f086fcreatedBy = $f086fcreatedBy;

        return $this;
    }

    /**
     * Get f086fcreatedBy
     *
     * @return integer
     */
    public function getF086fcreatedBy()
    {
        return $this->f086fcreatedBy;
    }

    /**
     * Set f086fupdatedBy
     *
     * @param integer $f086fupdatedBy
     *
     * @return T086fassetDisposal
     */
    public function setF086fupdatedBy($f086fupdatedBy)
    {
        $this->f086fupdatedBy = $f086fupdatedBy;

        return $this;
    }

    /**
     * Get f086fupdatedBy
     *
     * @return integer
     */
    public function getF086fupdatedBy()
    {
        return $this->f086fupdatedBy;
    }

    /**
     * Set f086fcreatedDtTm
     *
     * @param \DateTime $f086fcreatedDtTm
     *
     * @return T086fassetDisposal
     */
    public function setF086fcreatedDtTm($f086fcreatedDtTm)
    {
        $this->f086fcreatedDtTm = $f086fcreatedDtTm;

        return $this;
    }

    /**
     * Get f086fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fcreatedDtTm()
    {
        return $this->f086fcreatedDtTm;
    }

    /**
     * Set f086fupdatedDtTm
     *
     * @param \DateTime $f086fupdatedDtTm
     *
     * @return T086fassetDisposal
     */
    public function setF086fupdatedDtTm($f086fupdatedDtTm)
    {
        $this->f086fupdatedDtTm = $f086fupdatedDtTm;

        return $this;
    }

    /**
     * Get f086fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF086fupdatedDtTm()
    {
        return $this->f086fupdatedDtTm;
    }
}
