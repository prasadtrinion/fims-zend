<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * T145fbatchWithdraw
 *
 * @ORM\Table(name="t145fbatch_withdraw")
 * @ORM\Entity(repositoryClass="Application\Repository\BatchWithdrawRepository")
 */
class T145fbatchWithdraw
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f145fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f145fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f145fbatch_no", type="string", length=255, nullable=false)
     */
    private $f145fbatchNo;

    /**
     * @var string
     *
     * @ORM\Column(name="f145finvoice_bank", type="integer", nullable=false)
     */
    private $f145finvoiceBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f145fuum_bank", type="integer", nullable=false)
     * 
     */
    private $f145fuumBank;

    /**
     * @var integer
     *
     * @ORM\Column(name="f145fmaturity_date", type="datetime", nullable=false)
     * 
     */
    private $f145fmaturityDate;

     /**
     * @var integer
     *
     * @ORM\Column(name="f145fcertificate_date", type="datetime", nullable=false)
     * 
     */
    private $f145fcertificateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="f145fstatus", type="integer", nullable=false)
     */
    private $f145fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f145fcreated_by", type="integer", nullable=false)
     * 
     */
    private $f145fcreatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f145fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f145fcreatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="f145fupdated_by", type="integer", nullable=false)
     * 
     */
    private $f145fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f145fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f145fupdatedDtTm;
 	

    /**
     * Get f145fid
     *
     * @return integer
     */
    public function getF145fid()
    {
        return $this->f145fid;
    }

    /**
     * Set f145fbatchNo
     *
     * @param string $f145fbatchNo
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fbatchNo($f145fbatchNo)
    {
        $this->f145fbatchNo = $f145fbatchNo;

        return $this;
    }

    /**
     * Get f145fbatchNo
     *
     * @return string
     */
    public function getF145fbatchNo()
    {
        return $this->f145fbatchNo;
    }

    /**
     * Set f145finvoiceBank
     *
     * @param string $f145finvoiceBank
     *
     * @return T145fbatchWithdraw
     */
    public function setF145finvoiceBank($f145finvoiceBank)
    {
        $this->f145finvoiceBank = $f145finvoiceBank;

        return $this;
    }

    /**
     * Get f145finvoiceBank
     *
     * @return string
     */
    public function getF145finvoiceBank()
    {
        return $this->f145finvoiceBank;
    }


    /**
     * Set f145fmaturityDate
     *
     * @param integer $f145fmaturityDate
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fmaturityDate($f145fmaturityDate)
    {
        $this->f145fmaturityDate = $f145fmaturityDate;

        return $this;
    }

    /**
     * Get f145fmaturityDate
     *
     * @return integer
     */
    public function getF145fmaturityDate()
    {
        return $this->f145fmaturityDate;
    }

    /**
     * Set f145fcertificateDate
     *
     * @param integer $f145fcertificateDate
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fcertificateDate($f145fcertificateDate)
    {
        $this->f145fcertificateDate = $f145fcertificateDate;

        return $this;
    }

    /**
     * Get f145fcertificateDate
     *
     * @return integer
     */
    public function getF145fcertificateDate()
    {
        return $this->f145fcertificateDate;
    }

    /**
     * Set f145fuumBank
     *
     * @param integer $f145fuumBank
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fuumBank($f145fuumBank)
    {
        $this->f145fuumBank = $f145fuumBank;

        return $this;
    }

    /**
     * Get f145fuumBank
     *
     * @return integer
     */
    public function getF145fuumBank()
    {
        return $this->f145fuumBank;
    }


    /**
     * Set f145fupdatedDtTm
     *
     * @param \DateTime $f145fupdatedDtTm
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fupdatedDtTm($f145fupdatedDtTm)
    {
        $this->f145fupdatedDtTm = $f145fupdatedDtTm;

        return $this;
    }

    /**
     * Get f145fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF145fupdatedDtTm()
    {
        return $this->f145fupdatedDtTm;
    }

      /**
     * Set f145fcreatedDtTm
     *
     * @param \DateTime $f145fcreatedDtTm
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fcreatedDtTm($f145fcreatedDtTm)
    {
        $this->f145fcreatedDtTm = $f145fcreatedDtTm;

        return $this;
    }

    /**
     * Get f145fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF145fcreatedDtTm()
    {
        return $this->f145fcreatedDtTm;
    }

    /**
     * Set f145fstatus
     *
     * @param integer $f145fstatus
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fstatus($f145fstatus)
    {
        $this->f145fstatus = $f145fstatus;

        return $this;
    }

    /**
     * Get f145fstatus
     *
     * @return integer
     */
    public function getF145fstatus()
    {
        return $this->f145fstatus;
    }

    /**
     * Set f145fupdatedBy
     *
     * @param integer $f145fupdatedBy
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fupdatedBy($f145fupdatedBy)
    {
        $this->f145fupdatedBy = $f145fupdatedBy;

        return $this;
    }

    /**
     * Get f145fupdatedBy
     *
     * @return integer
     */
    public function getF145fupdatedBy()
    {
        return $this->f145fupdatedBy;
    }

     /**
     * Set f145fcreatedBy
     *
     * @param integer $f145fcreatedBy
     *
     * @return T145fbatchWithdraw
     */
    public function setF145fcreatedBy($f145fcreatedBy)
    {
        $this->f145fcreatedBy = $f145fcreatedBy;

        return $this;
    }

    /**
     * Get f145fcreatedBy
     *
     * @return integer
     */
    public function getF145fcreatedBy()
    {
        return $this->f145fcreatedBy;
    }

}
