<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T089fassetMaintenance
 *
 * @ORM\Table(name="t089fasset_maintenance")
 * @ORM\Entity(repositoryClass="Application\Repository\AssetMaintenanceRepository")
 */
class T089fassetMaintenance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f089fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f089fid;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fid_assets", type="integer", nullable=true)
     */
    private $f089fidAssets;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f089fdate_of_maintenance", type="datetime", nullable=false)
     */
    private $f089fdateOfMaintenance;

    /**
     * @var string
     *
     * @ORM\Column(name="f089fdescription", type="string", length=100, nullable=false)
     */
    private $f089fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fapproval_status", type="integer", nullable=true)
     */
    private $f089fapprovalStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fstatus", type="integer", nullable=true)
     */
    private $f089fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fcreated_by", type="integer", nullable=true)
     */
    private $f089fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f089fupdated_by", type="integer", nullable=true)
     */
    private $f089fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f089fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f089fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f089fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f089fupdatedDtTm;

    /**
     * @var \string
     *
     * @ORM\Column(name="f089freason", type="string", length=255, nullable=false)
     */
    private $f089freason;



    /**
     * Get f089fid
     *
     * @return integer
     */
    public function getF089fid()
    {
        return $this->f089fid;
    }

    /**
     * Set f089fdateOfMaintenance
     *
     * @param \DateTime $f089fdateOfMaintenance
     *
     * @return T089fassetMaintenance
     */
    public function setF089fdateOfMaintenance($f089fdateOfMaintenance)
    {
        $this->f089fdateOfMaintenance = $f089fdateOfMaintenance;

        return $this;
    }

    /**
     * Get f089fdateOfMaintenance
     *
     * @return \DateTime
     */
    public function getF089fdateOfMaintenance()
    {
        return $this->f089fdateOfMaintenance;
    }

    /**
     * Set f089fdescription
     *
     * @param string $f089fdescription
     *
     * @return T089fassetMaintenance
     */
    public function setF089fdescription($f089fdescription)
    {
        $this->f089fdescription = $f089fdescription;

        return $this;
    }

    /**
     * Get f089fdescription
     *
     * @return string
     */
    public function getF089fdescription()
    {
        return $this->f089fdescription;
    }

    /**
     * Set f089fapprovalStatus
     *
     * @param integer $f089fapprovalStatus
     *
     * @return T089fassetMaintenance
     */
    public function setF089fapprovalStatus($f089fapprovalStatus)
    {
        $this->f089fapprovalStatus = $f089fapprovalStatus;

        return $this;
    }

    /**
     * Get f089fapprovalStatus
     *
     * @return integer
     */
    public function getF089fapprovalStatus()
    {
        return $this->f089fapprovalStatus;
    }

    /**
     * Set f089fidAssets
     *
     * @param integer $f089fidAssets
     *
     * @return T089fassetMaintenance
     */
    public function setF089fidAssets($f089fidAssets)
    {
        $this->f089fidAssets = $f089fidAssets;

        return $this;
    }

    /**
     * Get f089fidAssets
     *
     * @return integer
     */
    public function getF089fidAssets()
    {
        return $this->f089fidAssets;
    }

    /**
     * Set f089fstatus
     *
     * @param integer $f089fstatus
     *
     * @return T089fassetMaintenance
     */
    public function setF089fstatus($f089fstatus)
    {
        $this->f089fstatus = $f089fstatus;

        return $this;
    }

    /**
     * Get f089fstatus
     *
     * @return integer
     */
    public function getF089fstatus()
    {
        return $this->f089fstatus;
    }

    /**
     * Set f089fcreatedBy
     *
     * @param integer $f089fcreatedBy
     *
     * @return T089fassetMaintenance
     */
    public function setF089fcreatedBy($f089fcreatedBy)
    {
        $this->f089fcreatedBy = $f089fcreatedBy;

        return $this;
    }

    /**
     * Get f089fcreatedBy
     *
     * @return integer
     */
    public function getF089fcreatedBy()
    {
        return $this->f089fcreatedBy;
    }

    /**
     * Set f089fupdatedBy
     *
     * @param integer $f089fupdatedBy
     *
     * @return T089fassetMaintenance
     */
    public function setF089fupdatedBy($f089fupdatedBy)
    {
        $this->f089fupdatedBy = $f089fupdatedBy;

        return $this;
    }

    /**
     * Get f089fupdatedBy
     *
     * @return integer
     */
    public function getF089fupdatedBy()
    {
        return $this->f089fupdatedBy;
    }

    /**
     * Set f089fcreatedDtTm
     *
     * @param \DateTime $f089fcreatedDtTm
     *
     * @return T089fassetMaintenance
     */
    public function setF089fcreatedDtTm($f089fcreatedDtTm)
    {
        $this->f089fcreatedDtTm = $f089fcreatedDtTm;

        return $this;
    }

    /**
     * Get f089fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF089fcreatedDtTm()
    {
        return $this->f089fcreatedDtTm;
    }

    /**
     * Set f089fupdatedDtTm
     *
     * @param \DateTime $f089fupdatedDtTm
     *
     * @return T089fassetMaintenance
     */
    public function setF089fupdatedDtTm($f089fupdatedDtTm)
    {
        $this->f089fupdatedDtTm = $f089fupdatedDtTm;

        return $this;
    }

    /**
     * Get f089fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF089fupdatedDtTm()
    {
        return $this->f089fupdatedDtTm;
    }

    /**
     * Set f089freason
     *
     * @param \DateTime $f089freason
     *
     * @return T089fassetMaintenance
     */
    public function setF089freason($f089freason)
    {
        $this->f089freason = $f089freason;

        return $this;
    }

    /**
     * Get f089freason
     *
     * @return \DateTime
     */
    public function getF089freason()
    {
        return $this->f089freason;
    }
}
