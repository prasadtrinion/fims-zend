<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T125fstudentDetails
 *
 * @ORM\Table(name="t125fstudent_details")
 * @ORM\Entity(repositoryClass="Application\Repository\StudentDetailRepository")
 */
class T125fstudentDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f125fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f125fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f125fname", type="string", length=25, nullable=true)
     */
    private $f125fname;

    /**
     * @var integer
     *
     * @ORM\Column(name="f125fsem_sequence", type="integer", nullable=true)
     */
    private $f125fsemSequence;

    /**
     * @var integer
     *
     * @ORM\Column(name="f125fintake_year", type="integer", nullable=true)
     */
    private $f125fintakeYear;

    /**
     * @var string
     *
     * @ORM\Column(name="f125fdescription", type="string", length=50, nullable=true)
     */
    private $f125fdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="f125fstatus", type="integer", nullable=true)
     */
    private $f125fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f125fcreated_by", type="integer", nullable=true)
     */
    private $f125fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f125fupdated_by", type="integer", nullable=true)
     */
    private $f125fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f125fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f125fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f125fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f125fupdatedDtTm;



    /**
     * Get f125fid
     *
     * @return integer
     */
    public function getF125fid()
    {
        return $this->f125fid;
    }

    /**
     * Set f125fname
     *
     * @param string $f125fname
     *
     * @return T125fstudentDetails
     */
    public function setF125fname($f125fname)
    {
        $this->f125fname = $f125fname;

        return $this;
    }

    /**
     * Get f125fname
     *
     * @return string
     */
    public function getF125fname()
    {
        return $this->f125fname;
    }

    /**
     * Set f125fsemSequence
     *
     * @param integer $f125fsemSequence
     *
     * @return T125fstudentDetails
     */
    public function setF125fsemSequence($f125fsemSequence)
    {
        $this->f125fsemSequence = $f125fsemSequence;

        return $this;
    }

    /**
     * Get f125fsemSequence
     *
     * @return integer
     */
    public function getF125fsemSequence()
    {
        return $this->f125fsemSequence;
    }

    /**
     * Set f125fintakeYear
     *
     * @param integer $f125fintakeYear
     *
     * @return T125fstudentDetails
     */
    public function setF125fintakeYear($f125fintakeYear)
    {
        $this->f125fintakeYear = $f125fintakeYear;

        return $this;
    }

    /**
     * Get f125fintakeYear
     *
     * @return integer
     */
    public function getF125fintakeYear()
    {
        return $this->f125fintakeYear;
    }

    /**
     * Set f125fdescription
     *
     * @param string $f125fdescription
     *
     * @return T125fstudentDetails
     */
    public function setF125fdescription($f125fdescription)
    {
        $this->f125fdescription = $f125fdescription;

        return $this;
    }

    /**
     * Get f125fdescription
     *
     * @return string
     */
    public function getF125fdescription()
    {
        return $this->f125fdescription;
    }

    /**
     * Set f125fstatus
     *
     * @param integer $f125fstatus
     *
     * @return T125fstudentDetails
     */
    public function setF125fstatus($f125fstatus)
    {
        $this->f125fstatus = $f125fstatus;

        return $this;
    }

    /**
     * Get f125fstatus
     *
     * @return integer
     */
    public function getF125fstatus()
    {
        return $this->f125fstatus;
    }

    /**
     * Set f125fcreatedBy
     *
     * @param integer $f125fcreatedBy
     *
     * @return T125fstudentDetails
     */
    public function setF125fcreatedBy($f125fcreatedBy)
    {
        $this->f125fcreatedBy = $f125fcreatedBy;

        return $this;
    }

    /**
     * Get f125fcreatedBy
     *
     * @return integer
     */
    public function getF125fcreatedBy()
    {
        return $this->f125fcreatedBy;
    }

    /**
     * Set f125fupdatedBy
     *
     * @param integer $f125fupdatedBy
     *
     * @return T125fstudentDetails
     */
    public function setF125fupdatedBy($f125fupdatedBy)
    {
        $this->f125fupdatedBy = $f125fupdatedBy;

        return $this;
    }

    /**
     * Get f125fupdatedBy
     *
     * @return integer
     */
    public function getF125fupdatedBy()
    {
        return $this->f125fupdatedBy;
    }

    /**
     * Set f125fcreatedDtTm
     *
     * @param \DateTime $f125fcreatedDtTm
     *
     * @return T125fstudentDetails
     */
    public function setF125fcreatedDtTm($f125fcreatedDtTm)
    {
        $this->f125fcreatedDtTm = $f125fcreatedDtTm;

        return $this;
    }

    /**
     * Get f125fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF125fcreatedDtTm()
    {
        return $this->f125fcreatedDtTm;
    }

    /**
     * Set f125fupdatedDtTm
     *
     * @param \DateTime $f125fupdatedDtTm
     *
     * @return T125fstudentDetails
     */
    public function setF125fupdatedDtTm($f125fupdatedDtTm)
    {
        $this->f125fupdatedDtTm = $f125fupdatedDtTm;

        return $this;
    }

    /**
     * Get f125fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF125fupdatedDtTm()
    {
        return $this->f125fupdatedDtTm;
    }
}
