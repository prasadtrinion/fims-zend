<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T101finsuranceCompany
 *
 * @ORM\Table(name="t101finsurance_company")
 * @ORM\Entity(repositoryClass="Application\Repository\InsuranceCompanyRepository")
 */
class T101finsuranceCompany
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f101fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f101fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f101fproduct_name", type="string", length=50, nullable=true)
     */
    private $f101fproductName;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fid_vendor", type="integer", nullable=true)
     */
    private $f101fidVendor;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fstatus", type="integer", nullable=true)
     */
    private $f101fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fcreated_by", type="integer", nullable=true)
     */
    private $f101fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f101fupdated_by", type="integer", nullable=true)
     */
    private $f101fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f101fcreated_dt_tm", type="datetime", nullable=true)
     */
    private $f101fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f101fupdated_dt_tm", type="datetime", nullable=true)
     */
    private $f101fupdatedDtTm;



    /**
     * Get f101fid
     *
     * @return integer
     */
    public function getF101fid()
    {
        return $this->f101fid;
    }

    /**
     * Set f101fproductName
     *
     * @param string $f101fproductName
     *
     * @return T101finsuranceCompany
     */
    public function setF101fproductName($f101fproductName)
    {
        $this->f101fproductName = $f101fproductName;

        return $this;
    }

    /**
     * Get f101fproductName
     *
     * @return string
     */
    public function getF101fproductName()
    {
        return $this->f101fproductName;
    }

    /**
     * Set f101fidVendor
     *
     * @param integer $f101fidVendor
     *
     * @return T101finsuranceCompany
     */
    public function setF101fidVendor($f101fidVendor)
    {
        $this->f101fidVendor = $f101fidVendor;

        return $this;
    }

    /**
     * Get f101fidVendor
     *
     * @return integer
     */
    public function getF101fidVendor()
    {
        return $this->f101fidVendor;
    }


    /**
     * Set f101fstatus
     *
     * @param integer $f101fstatus
     *
     * @return T101finsuranceCompany
     */
    public function setF101fstatus($f101fstatus)
    {
        $this->f101fstatus = $f101fstatus;

        return $this;
    }

    /**
     * Get f101fstatus
     *
     * @return integer
     */
    public function getF101fstatus()
    {
        return $this->f101fstatus;
    }

    /**
     * Set f101fcreatedBy
     *
     * @param integer $f101fcreatedBy
     *
     * @return T101finsuranceCompany
     */
    public function setF101fcreatedBy($f101fcreatedBy)
    {
        $this->f101fcreatedBy = $f101fcreatedBy;

        return $this;
    }

    /**
     * Get f101fcreatedBy
     *
     * @return integer
     */
    public function getF101fcreatedBy()
    {
        return $this->f101fcreatedBy;
    }

    /**
     * Set f101fupdatedBy
     *
     * @param integer $f101fupdatedBy
     *
     * @return T101finsuranceCompany
     */
    public function setF101fupdatedBy($f101fupdatedBy)
    {
        $this->f101fupdatedBy = $f101fupdatedBy;

        return $this;
    }

    /**
     * Get f101fupdatedBy
     *
     * @return integer
     */
    public function getF101fupdatedBy()
    {
        return $this->f101fupdatedBy;
    }

    /**
     * Set f101fcreatedDtTm
     *
     * @param \DateTime $f101fcreatedDtTm
     *
     * @return T101finsuranceCompany
     */
    public function setF101fcreatedDtTm($f101fcreatedDtTm)
    {
        $this->f101fcreatedDtTm = $f101fcreatedDtTm;

        return $this;
    }

    /**
     * Get f101fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF101fcreatedDtTm()
    {
        return $this->f101fcreatedDtTm;
    }

    /**
     * Set f101fupdatedDtTm
     *
     * @param \DateTime $f101fupdatedDtTm
     *
     * @return T101finsuranceCompany
     */
    public function setF101fupdatedDtTm($f101fupdatedDtTm)
    {
        $this->f101fupdatedDtTm = $f101fupdatedDtTm;

        return $this;
    }

    /**
     * Get f101fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF101fupdatedDtTm()
    {
        return $this->f101fupdatedDtTm;
    }
}

