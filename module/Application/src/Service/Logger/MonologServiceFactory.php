<?php
namespace Application\Service\Logger;

use Monolog\Logger;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class MonologServiceFactory implements FactoryInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $logger = new Logger('name');
        
        $config = $container->get('Config');
        $logConfig = $config['logger'];
        $handler = $logConfig['handler'];
        $filename = $handler['filename'];
        $type = $handler['type'];
        $maxFiles = $handler['max_files'];
        
        switch (strtolower($type)) {
            case 'stream':
                $handlerObj = new \Monolog\Handler\StreamHandler($filename);
                break;
            case 'rotating_file':
                $handlerObj = new \Monolog\Handler\RotatingFileHandler($filename, $maxFiles);
                break;
        }
        
        $logger->pushHandler($handlerObj);
        
        return new LoggerService($logConfig, $logger);
    }
}