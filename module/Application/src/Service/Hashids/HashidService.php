<?php
namespace Application\Service\Hashids;

use Hashids\Hashids;

/**
 * Hashids Service
 */
class HashidService
{

    private $hashids;

    public function __construct(array $config)
    {
        $this->hashids = new Hashids($config['salt'], $config['length']);
    }

    /**
     * Encode numbers to hash
     *
     * @param mixed $numbers Array of numbers to encode
     *            
     */
    public function encode($numbers)
    {
        return $this->hashids->encode($numbers);
    }

    /**
     * Decode hash to numbers
     *
     * @param string $hash Encoded hash
     */
    public function decode($hash)
    {
        $numbers = $this->hashids->decode($hash);
        
        // Return array if count is more than one
        if (count($numbers) > 1) {
            return $numbers;
        } else {
            return $numbers[0];
        }
    }
}