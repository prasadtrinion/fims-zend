<?php
namespace Application\Service\File;

class Uploader
{
    protected $config;
    
    public function __construct($config)
    {
        $this->config = $config;    
    }
    
    public function uploadFile($file = array(), $destinationDir, $newFilename = null)
    {
        $storageDir = $this->config['local']['storage_dir']  . $destinationDir;
        
        // normalize filename
        if(!empty($newFilename)){
            $filename = $this->normalizeFilename($newFilename);
        }
        else {
            $pathInfo = pathinfo($file['name']);
            $filename = $this->normalizeFilename($pathInfo['filename']) . '.' . $pathInfo['extension'];
        }
        
        if(!file_exists($storageDir)){
            mkdir($storageDir, 0777, true);
        }
    
        if(move_uploaded_file($file['tmp_name'],  $storageDir . $filename)){
            $file = array(
                'name' => $filename,
                'type' => $file['type'],
                'size' => $file['size']
            );

            return $file;            
        } else {
            throw new \Exception('[FileUploader]: Error occurred while uploading file.');
        }
        
    }
    
    protected function normalizeFilename($filename)
    {
        // replace non letter or digits by -
        $filename = preg_replace('~[^\pL\d]+~u', '-', $filename);
    
        // transliterate
        $filename = iconv('utf-8', 'us-ascii//TRANSLIT', $filename);
    
        // remove unwanted characters
        $filename = preg_replace('~[^-\w]+~', '', $filename);
    
        // trim
        $filename = trim($filename, '-');
    
        // remove duplicate -
        $filename = preg_replace('~-+~', '-', $filename);
    
        // lowercase
        $filename = strtolower($filename);
    
        if (empty($filename))
        {
            return 'n-a';
        }
    
        return $filename;
    }
}