<?php 
namespace Application\Service\Token;

use Firebase\JWT\JWT;

class Token
{

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * [generateAuthToken description]
     * @return [type] [description]
     */
    public function generateUserAuthToken($user)
    {  
        $config     = $this->config;
        $tokenId    = base64_encode(password_hash($config['JWT']['jwtKey'],PASSWORD_DEFAULT));
        $issuedAt   = time();
        $notBefore  = $issuedAt + 0;
        $expire     = $notBefore + $config['JWT']['jwtUserExpDays'] * 86400; // seconds in a day
        $serverName = $config['JWT']['serverName'];
        
        $status = "Inactive";
        if($user['status'])
        {
            $status = "Active";
        }
        $data       = array(
            'iat'  => time(),
            'jti'  => $tokenId,
            'iss'  => $serverName,
            'nbf'  => $notBefore,
            'exp'  => $expire,
            'data' => array(
                'accountId'   => $user['accountId'],
                'userName' => $user['name'],
                'status' =>  $status
 
            )
        );

        return JWT::encode(
            $data,
            $config['JWT']['jwtKey'],
            $config['JWT']['jwtEncryptAlgorithm']
        );
    }

    public function validateUserAuthToken($token)
    {
        try{
            $config     = $this->config;
            $result=  JWT::decode($token, $config['JWT']['jwtKey'], array('HS256'));
            //$result['status'] = '200';
            //print_r();exit;
        }catch(\Exception $ex){
            $result['status'] = '401';
            $result['message'] = $ex->getMessage();
        }
        
        return $result;
    }
}