<?php
namespace Application\Service\Mailer;

use Zend\Mail\Message;
use Zend\Mime;

class Mailer
{

    private $transport;

    private $viewRenderer;

    private $options;

    public function __construct($transport, $viewRenderer, $options = array())
    {
        $this->transport = $transport;
        $this->viewRenderer = $viewRenderer;
        $this->options = $options;
    }

    /**
     * Send mail using SMPT transport
     *
     * @param array $to
     *            string|array
     * @param string $subject
     *            Subject for email message
     * @param string $template
     *            Template name for email content
     * @param array $data
     *            Data to be sent to template
     * @param array $options
     *            Additional parameters
     */
    public function send($to, $subject, $template, $data = array(), $options = array())
    {
        if (empty($to)) {
            throw new \Exception('"to" address should not be blank.');
        }
        
        if (empty(trim($subject))) {
            throw new \Exception('"subject" should not be blank.');
        }
        
        if (empty(trim($template))) {
            throw new \Exception('"template" should not be blank.');
        }
        
        $message = new Message();
        $message->addTo($to);
        $message->addFrom($this->options['admin']['email'], $this->options['admin']['name']);
        $message->setSubject($subject);
        
        // Get template output
        $htmlOutput = $this->viewRenderer->render($template, $data);
        
        $html = new Mime\Part($htmlOutput);
        $html->type = "text/html";
        
        $mimeMessage = new Mime\Message();
        $mimeMessage->setParts(array(
            $html
        ));
        
        $message->setBody($mimeMessage);
        
        try {
            $this->transport->send($message);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}