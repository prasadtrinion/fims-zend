<?php
namespace Application\Service\Mailer;

use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class MailerServiceFactory implements FactoryInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        
        $smtpOptions = new SmtpOptions($config['mail']['smtp']);
        $transport = new Smtp($smtpOptions);
        
        $viewRenderer = $container->get('ViewRenderer');
        
        $options = array(
            'admin' => $config['admin']
        );
        
        return new Mailer($transport, $viewRenderer, $options);
    }
}