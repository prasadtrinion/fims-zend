<?php
namespace Application\Service\Excel;

class Excel
{

    /**
     * Create new workbook
     *
     * @return \PHPExcel
     */
    public function createWorkbook()
    {
        $workbook = new \PHPExcel();
        
        return $workbook;
    }

    /**
     * Read existing workbook
     *
     * @param string $filename
     * @throws \Exception
     * @return PHPExcel
     */
    public function readWorkbook($filename){
        $filetype = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($filetype);
    
        try{
            $workbook = $reader->load($filename);
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    
        return $workbook;
    }
    
    /**
     * Save workbook to file
     *
     * @param object $workbook            
     * @param array $data            
     * @param string $filename            
     */
    public function saveWorkbook($workbook, $filename)
    {
        $writer = \PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
        
        try {
            $writer->save($filename);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Download workbook
     *
     * @param object $workbook            
     * @param string $filename            
     */
    public function downloadWorkbook($workbook, $filename)
    {
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=\"{$filename}\"");
        header("Cache-Control: max-age=0");
        
        $writer = \PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
        
        try {
            $writer->save('php://output');
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        
        exit();
    }
    
    /**
     * Convert from Excel cell value to DateTime object
     * 
     * @param string $cellValue
     * @return \DateTime
     */
    public function convertToDateObject($cellValue)
    {
        return \PHPExcel_Shared_Date::ExcelToPHPObject($cellValue);
    }
}