<?php
namespace Application\Service\Recaptcha;

use GuzzleHttp\Client;

class Recaptcha
{

    private $config;

    private $response;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getElement()
    {
        $element = '<div class="g-recaptcha" data-sitekey="' . $this->config['site_key'] . '"></div>';
        
        return $element;
    }

    public function isValid()
    {
        $client = new Client(array(
            'base_uri' => 'https://www.google.com',
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false
            )
        ));
        
        $response = $client->request('POST', '/recaptcha/api/siteverify', array(
            'form_params' => array(
                'secret' => $this->config['secret_key'],
                'response' => $this->getResponse()
            )
        ));
        
        $response = json_decode($response->getBody(), true);
        
        if (! empty($response) && $response['success']) {
            return true;
        } else {
            return false;
        }
    }
}