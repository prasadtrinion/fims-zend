<?php
namespace Application\Service\Authentication;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthResult;

class Adapter implements AdapterInterface {

	private $entityRepository;

	private $passwordEncoder;

	private $identity;

	private $credential;

	private $identityColumn;

	/**
	 *
	 * @return void
	 */
	public function __construct($entityRepository, $identityColumn, $passwordEncoder) {
		$this->entityRepository = $entityRepository;
		$this->identityColumn = $identityColumn;
		$this->passwordEncoder = $passwordEncoder;
	}

	/**
	 * Performs an authentication attempt
	 *
	 * @return \Zend\Authentication\Result
	 * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
	 */
	public function authenticate() {
		$accountIndentity = $this->entityRepository->findOneBy(array(
			$this->identityColumn => $this->getIdentity(),
			'deleteFlag' => 0,
		));

		$status = 'FAILED';
		if ($accountIndentity) {
			if (!$accountIndentity->isActive()) {
				$status = 'DISABLED';
			} elseif($accountIndentity->isRejected()){
				$status = 'REJECTED';
			}else {
				$accountId = $accountIndentity->getId();

				$isVerified = $this->passwordEncoder->verify($this->credential, $accountIndentity->getPassword());

				if ($isVerified) {
					$status = 'SUCCESS';

					$userIdentity = $this->entityRepository->getUsersByAccountId($accountId);
					$this->setIdentity($userIdentity[0]);
				}
			}
		}

		return $this->authenticationResult($status);
	}

	public function getIdentity() {
		return $this->identity;
	}

	public function setIdentity($identity) {
		$this->identity = $identity;
		return $this;
	}

	public function getCredential() {
		return $this->credential;
	}

	public function setCredential($credential) {
		$this->credential = $credential;
		return $this;
	}

	protected function authenticationResult($status) {
		if ($status == 'SUCCESS') {
			$code = AuthResult::SUCCESS;
			$message = 'Authentication successful.';
		} elseif ($status == 'DISABLED') {
			$code = AuthResult::FAILURE_UNCATEGORIZED;
			$message = 'Account has been disabled.';
		} elseif ($status == 'FAILED') {
			$code = AuthResult::FAILURE_CREDENTIAL_INVALID;
			$message = 'Invalid credentials.';
		} elseif($status == 'REJECTED'){
			$code = AuthResult::FAILURE_IDENTITY_AMBIGUOUS;
			$message = 'Invalid credentials.';
		}

		return new AuthResult($code, $this->getIdentity(), array(
			$message,
		));
	}

	public function getEntityRepository() {
		return $this->entityRepository;
	}

	public function setEntityRepository($entityRepository) {
		$this->entityRepository = $entityRepository;
		return $this;
	}

	public function getPasswordEncoder() {
		return $this->passwordEncoder;
	}

	public function setPasswordEncoder($passwordEncoder) {
		$this->passwordEncoder = $passwordEncoder;
		return $this;
	}

	public function getIdentityColumn() {
		return $this->identityColumn;
	}

	public function setIdentityColumn($identityColumn) {
		$this->identityColumn = $identityColumn;
		return $this;
	}
}