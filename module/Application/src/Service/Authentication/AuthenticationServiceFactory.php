<?php
namespace Application\Service\Authentication;

use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class AuthenticationServiceFactory implements FactoryInterface
{

    /**
     *
     * {@inheritdoc}
     *
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        
        // Configure Storage
        $sessionOptions = $config['authentication']['session'];
        $storage = new Storage();
        $storage->setRememberMeTime($sessionOptions['remember_me_ttl']);
        
        // Configure Adapter
        $adapterOptions = $config['authentication']['doctrine'];
        $em = $container->get('Doctrine\ORM\EntityManager');
        $entityRepository = $em->getRepository($adapterOptions['entity_class']);
        $passwordEncoder = $container->get('app.password_encoder');
        
        $adapter = new Adapter($entityRepository, $adapterOptions['identity_property'], $passwordEncoder);
        
        // Set Storage and Adapter object to Authentication service
        return new AuthenticationService($storage, $adapter);
    }
}