<?php

namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SponsorCnController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function get($id)
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $sponsorCnRepository = $this->getRepository('T061fsponsorCn');

        try {
            $sponsorCn = $sponsorCnRepository->find((int)$id);
        } catch (\Exception $e) {
            echo $e;
        }
        if (!$sponsorCn) {
            return $this->resourceNotFound();
        }

        $result = $sponsorCnRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }

    public function getList()
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $sponsorCnRepository = $this->getRepository('T061fsponsorCn');
        try {
            $sponsorCnDetails = $sponsorCnRepository->getList();
        } catch (\Exception $e) {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data'   => $sponsorCnDetails,
        ]);
    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        
        if ($this->isJSON($rawBody)) {

            try
            {

                $sponsorCnRepository = $this->getRepository('T061fsponsorCn');
                $sponsorCnDetailRepository = $this->getRepository('T062fsponsorCnDetails');
                if (array_key_exists("f061fid", $postData)) 
                    {
                    $sponsorCnObj = $sponsorCnRepository->find((int)$postData['f061fid']);
                        
                    $sponsorCnObj = $sponsorCnRepository->updateMaster($sponsorCnObj, $postData);
                $details = $postData['details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f062fid", $detail)) 
                    {
    
                        $detailObj = $sponsorCnDetailRepository->find((int) $detail['f062fid']);
    
                        if ($detailObj) {
                            $sponsorCnDetailRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $sponsorCnDetailRepository->createDetail($sponsorCnObj, $detail);
                    }
                }
            }
            else{

                $sponsorCn = $sponsorCnRepository->findBy(array("f061freferenceNumber"=>$postData['f061freferenceNumber']));
                if ($sponsorCn)
                {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'SponsorCn Already exist.',
                    ]);
                }
                $sponsorCnObj = $sponsorCnRepository->createNewData($postData);
            }

            } catch (\Exception $e) {
                echo $e;
                // if ($e->getErrorCode() == 1062) {
                //     return new JsonModel([
                //         'status'  => 200,
                //         'message' => 'SponsorDn Already exist.',
                //     ]);
                // }
            }

            return new JsonModel([
                'status'       => 200,
                'message'      => 'SponsorCn Created successfully',
                // 'sponsorCnId' => $sponsorCnObj->getF061fId(),
            ]);

        } else {
            return $this->invalidInputType();
        }

    }

    public function approvedsponsorCnAction()
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $sponsorCnRepository = $this->getRepository('T061fsponsorCn');


        $result = $sponsorCnRepository->approvedsponsorCns((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }
    public function approveAction()
    {

        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);
        $journalRepository = $this->getRepository('T017fjournal');
        $sponsorCnRepository = $this->getRepository('T061fsponsorCn');
        $sponsorCnRepository->approve($postData,$journalRepository);
        return new JsonModel([
            'status'  => 200,
            'message' => 'SponsorDn Approved successfully',
        ]);
    }
}
