<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;


class MasterController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }   


    public function getList()
    {
        $token = $this->params()->fromRoute('repository');

        $request = $this->getRequest();

        try
        {
            $masterRepository = $this->getRepository($token);
        } catch (\Exception $e) {
            echo $e;
        }

        $result = $masterRepository->getList();

        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {
        $id      = (int) $id;
        $token   = $this->params()->fromRoute('repository'); 
        $request = $this->getRequest();
        try
        {

            $masterRepository = $this->getRepository($token);
        } catch (\Exception $e) {
            $e->getErrorCode();

        }
        if ($token == "T016fopeningbalance") {
            $master = $masterRepository->findBy(array("f016fidFinancialyear" => (int)$id));
        } 
        else if($token == "T034fstaffMaster"){
            // $master = $masterRepository->findBy(array("f034fstaffId" =>$id));
        }else {

            $approvedStatus = $this->params()->fromRoute('approvedStatus');

            if ($approvedStatus == (int) '') {

                $master = $masterRepository->find((int)$id);

                if (!$master) {
                    return new JsonModel([
                        'status'  => 404,
                        'Message' => "Resource Not Found",
                    ]);
                }
            }
        }

        $result = $masterRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        $token   = $this->params()->fromRoute('repository');

        if ($this->isJSON($rawBody)) {

            $masterRepository = $this->getRepository($token);

            switch($token){
                case "T057ffund":
                $master = $masterRepository->findBy(array("f057fcode"=>$postData['f057fcode']));
                break;
                case "T015fdepartment":
                $master = $masterRepository->findBy(array("f015fdepartmentCode"=>$postData['f015fdepartmentCode']));
                break;
                case "T042fbank":
                $master = $masterRepository->findBy(array("f042fswiftCode"=>strtolower($postData['f042fswiftCode'])));
                break;
                case "T012fstate":
                $master = $masterRepository->findBy(array("f012fstateName"=>strtolower($postData['f012fstateName'])));
                break;
                case "T013fcountry":
                $master = $masterRepository->findBy(array("f013fcountryName"=>strtolower($postData['f013fcountryName'])));
                break;
                case "T041fbankSetupAccount":
                $master = $masterRepository->findBy(array("f041faccountNumber"=>$postData['f041faccountNumber']));
                break;
                case "T110fbudgetyear":
                $master = $masterRepository->findBy(array("f110fyear"=>$postData['f110fyear']));
                break;
                case "T038finvestmentType":
                $master = $masterRepository->findBy(array("f038ftype"=>strtolower($postData['f038ftype'])));
                break;
                case "T021frole":
                $master = $masterRepository->findBy(array("f021froleName"=>strtolower($postData['f021froleName'])));
                break;
                // case "T016fopeningbalance":
                // $master = $masterRepository->findBy(array("f016fidFinancialyear"=>$postData['f016fidFinancialyear']));
                // break;
                case "T015ffinancialyear":
                $master = $masterRepository->findBy(array("f015fname"=>$postData['f015fname']));
                break;
                case "T027fitemCategory":
                $master = $masterRepository->findBy(array("f027fcode"=>strtolower($postData['f027fcode'])));
                break;
                case "T028fitemSubCategory":
                $master = $masterRepository->findBy(array("f028fcode"=>strtolower($postData['f028fcode'])));
                break;
                case "T029fitemSetUp":
                $master = $masterRepository->findBy(array("f029fitemName"=>strtolower($postData['f029fitemName'])));
                break;
                case "T109fprocurementLimit":
                $master = $masterRepository->findBy(array("f109ftype"=>strtolower($postData['f109ftype'])));
                break;
                case "T020floanName":
                $master = $masterRepository->findBy(array("f020floanName"=>strtolower($postData['f020floanName'])));
                break;
                // case "T089fassetSubCategories":
                // $master = $masterRepository->findBy(array("f089fsubcode"=>strtolower($postData['f089fsubcode'])));
                // break;
                case "T087fassetCategory":
                $master = $masterRepository->findBy(array("f087fcategoryCode"=>strtolower($postData['f087fcategoryCode'])));
                break;
                case "T088fitem":
                $master = $masterRepository->findBy(array("f088fitemCode"=>strtolower($postData['f088fitemCode'])));
                break;
                case "T033fdisposalAccount":
                $master = $masterRepository->findBy(array("f033fcode"=>strtolower($postData['f033fcode'])));
                break;
                case "T073fcategory":
                $master = $masterRepository->findBy(array("f073fprefixCode"=>strtolower($postData['f073fprefixCode'])));
                break;
                case "T018fonlinePayment":
                $master = $masterRepository->findBy(array("f018fcode"=>strtolower($postData['f018fcode'])));
                break;
                case "T081ftextCode":
                $master = $masterRepository->findBy(array("f081fcode"=>strtolower($postData['f081fcode'])));
                break;
                // case "T090fassetType":
                // print_r($postData);exit;
                // $master = $masterRepository->findBy(array("f090ftypeCode"=>strtolower($postData['f090ftypeCode'])));
                // break;
                case "T062fcurrency":
                $master = $masterRepository->findBy(array("f062fcurCode"=>strtolower($postData['f062fcurCode'])));
                break;

                case "T049fcreditCardTerminal":
                $master = $masterRepository->findBy(array("f049fterminalNo"=>strtolower($postData['f049fterminalNo'])));
                break;
                case "T099fstudentInsurance":
                $master = $masterRepository->findBy(array("f099finsCode"=>strtolower($postData['f099finsCode'])));
                break;
                case "T090fassetType":
                $master = $masterRepository->findBy(array("f090fcode"=>strtolower($postData['f090fcode']),"f090fidSubCategory"=>$postData['f090fidSubCategory']));
                break;
                case "T051fcashAdvanceType":
                $master = $masterRepository->findBy(array("f051fdescription"=>strtolower($postData['f051fdescription'])));
                break;
                // case "T046fstudentDiscount":
                // $master = $masterRepository->findBy(array("f046fcode"=>strtolower($postData['f046fcode'])));
                // break;
                case "T053fsemester":
                $master = $masterRepository->findBy(array("f053fcode"=>strtolower($postData['f053fcode'])));
                break;
                case "T060fstudent":
                $master = $masterRepository->findBy(array("f060fmatricNumber"=>strtolower($postData['f060fmatricNumber'])));
                break;

            }

            if($master)
            {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
            }
            // try{
            //     
            // }
            // catch(\Exception $e){
            //     echo $e;
            // } 
            // 
                try
                {

                    $masterObject = $masterRepository->createNewData($postData);

                }  
                catch (\Exception $e)
                {
                    echo $e;
                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Saving'
                        ]);
                }   

                 return new JsonModel([
                    'status'  => 200,
                    'message' => 'Added successfully',

                    ]);
        }
        else 
        {
            return $this->invalidInputType();
        }
    }


    public function update($id, $postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        $token   = $this->params()->fromRoute('repository');

        try
        {
            $masterRepository = $this->getRepository($token);
        } catch (\Exception $e) {
            echo $e;
        }

        $master = $masterRepository->find((int) $id);

        if (!$master) {
            return $this->resourceNotFound();
        }
        
        if ($this->isJSON($rawBody)) {
            try
            {
                $masterRepository = $this->getRepository($token);
            } catch (\Exception $e) {
                echo $e;
            }
            
            try
            {
                $masterObject = $masterRepository->updateData($master, $postData);
            } catch (\Exception $e) {
                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 409,
                        'message' => 'Already exist.',
                    ]);
                }
            }




            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        }

         
    }
    public function generateNumberAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $initialRepository = $this->getRepository('T011finitialConfig');
        $number            = $initialRepository->generateFIMS($postData);
        return new JsonModel([
            'number' => $number,
        ]);
    }
    public function generateStudentFinanceNumberAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $initialRepository = $this->getRepository('T011finitialConfig');
        $number            = $initialRepository->generateSF($postData);
        return new JsonModel([
            'number' => $number,
        ]);
    }
    public function activeListAction()
    {
     {
        $token = $this->params()->fromRoute('repository');

        $request = $this->getRequest();

        try
        {
            $masterRepository = $this->getRepository($token);
        } catch (\Exception $e)
        {
            echo $e;
        }

        $result = $masterRepository->getActiveList();

        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }   
    }
    public function deleteAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $initialRepository = $this->getRepository('T011finitialConfig');
        $initialRepository->delete($postData);
        return new JsonModel([
            'status' => "Deleted Succesfully",
        ]);
    }
    public function checkDuplicationAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $initialRepository = $this->getRepository('T011finitialConfig');
        $result = $initialRepository->checkDuplication($postData);
        return new JsonModel([
           'status' => 200,
            'result' => $result
        ]);
    }

    public function getZeroDepartmentsAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $departmentRepository = $this->getRepository('T015fdepartment');
        $result = $departmentRepository->getZeroDepartments();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function getByUnitCodeAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $code   = $this->params()->fromRoute('code');

        $departmentRepository = $this->getRepository('T015fdepartment');
        $result = $departmentRepository->getByUnitCode($code);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function getItemSubCategoriesByCategoryAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $id   = $this->params()->fromRoute('id');

        $itemRepository = $this->getRepository('T028fitemSubCategory');
        $result = $itemRepository->getItemSubCategoriesByCategory($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
      public function getTablesAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();

        $itemRepository = $this->getRepository('T011finitialConfig');
        $result = $itemRepository->getTables();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
      public function getColumnsAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $table   = $this->params()->fromRoute('table');

        $itemRepository = $this->getRepository('T011finitialConfig');
        $result = $itemRepository->getColumns($table);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function sendmailAction()
    {

        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        $itemRepository = $this->getRepository('T030fvendorRegistration');
        $itemRepository->mail();
        return new JsonModel([
            'status' => 200,
            'result' => 'Mail Sent'
        ]);
    }
}
