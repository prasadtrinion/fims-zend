<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CashAdvanceRateController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $cashAdvanceRepository = $this->getRepository('T117fcashAdvanceRate');
        $result = $cashAdvanceRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $cashAdvanceRepository = $this->getRepository('T117fcashAdvanceRate');
        $cashAdvance = $cashAdvanceRepository->find((int)$id);
        if(!$cashAdvance)
        {
            return $this->resourceNotFound();
        }
        $result = $cashAdvanceRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        // print_r("gds");exit;

        
        if($this->isJSON($rawBody))
        {
           
            $cashAdvanceRepository = $this->getRepository('T117fcashAdvanceRate');
             // $cash = $cashAdvanceRepository->findBy(array("f117fstaffType"=>$postData['f117fstaffType']));

            // if($cash)
            // {
            //             return new JsonModel([
            //                 'status'  => 409,
            //                 'message' => 'Already exist.',
            //             ]);
            // }
            try
            {
                $cashAdvanceObj = $cashAdvanceRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashAdvanceRepository = $this->getRepository('T117fcashAdvanceRate');
        $cashAdvance = $cashAdvanceRepository->find((int)$id);

        if(!$cashAdvance)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $cashAdvanceObj = $cashAdvanceRepository->updateData($cashAdvance,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

}