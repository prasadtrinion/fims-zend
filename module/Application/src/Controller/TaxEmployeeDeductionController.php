<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class TaxEmployeeDeductionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $taxEmployeeRepository = $this->getRepository('T082ftaxEmployeeDeduction');
        $result = $taxEmployeeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
        $taxEmployeeRepository = $this->getRepository('T082ftaxEmployeeDeduction');

            try
            {
                foreach($postData as $data){
                if(array_key_exists("f082fid",$data)){
                    $tax = $taxEmployeeRepository->find((int)$data['f082fid']);
                    if($tax){
                        $taxEmployeeRepository->updateMaster($tax,$data);
                    }
                }
                else{
                $taxEmployeeRepository->createNewData($data);
                }
            }
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Updated successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

}