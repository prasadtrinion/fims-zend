<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class MasterInvoiceController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T071finvoice');
        $result            = $invoiceRepository->getMasterInvoiceList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em             = $this->getEntityManager();
        $request        = $this->getRequest();
        $approvedStatus = $this->params()->fromRoute('approvedStatus');

        $invoiceRepository = $this->getRepository('T071finvoice');
        if (!isset($approvedStatus)) {

            $invoice = $invoiceRepository->find((int) $id);

            if (!$invoice) {
                return $this->resourceNotFound();
            }
        }
        $result = $invoiceRepository->getMasterInvoiceById((int) $id, $approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $invoiceRepository = $this->getRepository('T071finvoice');

            try
            {
                $details = $postData['invoice-details'];
                $totalAmount = 0 ;
                foreach($details as $detail)
                {
                    $totalAmount = $totalAmount + $detail['f072ftotal'];
                }
                $postData['f071finvoiceTotal'] = $totalAmount;
                $invoiceObj = $invoiceRepository->createNewMasterInvoice($postData);
            } catch (\Exception $e) {
                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'Master Invoice Already exist.',
                    ]);
                }
            }
            return new JsonModel([
                'status'    => 200,
                'message'   => 'Master Invoice Added successfully',
                'invoiceId' => $invoiceObj->getF071fid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $invoiceRepository       = $this->getRepository('T071finvoice');
        $invoiceDetailRepository = $this->getRepository('T072finvoiceDetails');
        $invoice                 = $invoiceRepository->find((int)$id);

        if (!$invoice) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $invoiceObj     = $invoiceRepository->updateMasterInvoice($invoice, $postData);
            $invoiceDetails = $postData['invoice-details'];
            foreach ($invoiceDetails as $invoiceDetail) {

                if (array_key_exists("f071fid", $invoiceDetail)) {

                    $invoiceDetailObj = $invoiceDetailRepository->find((int) $invoiceDetail['f072fid']);

                    // if(!$master)
                    // {
                    //     return $this->resourceNotFound();
                    // }
                    if ($invoiceDetailObj) {
                        $invoiceDetailRepository->updateMasterInvoiceDetail($invoiceDetailObj, $invoiceDetail);
                    }
                } else {

                    $invoiceDetailRepository->createMasterInvoiceDetail($invoiceObj, $invoiceDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
    public function approveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $invoiceRepository = $this->getRepository('T071finvoice');
        $journalRepository = $this->getRepository('T017fjournal');
        $invoiceRepository->masterInvoiceApprove($postData,$journalRepository);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Master Invoices Approved successfully',
        ]);
    }

   

    public function customerMasterInvoicesAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T071finvoice');
        $result            = $invoiceRepository->getCustomerMasterInvoices($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function customerMasterInvoiceDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T071finvoice');
        $result            = $invoiceRepository->customerMasterInvoiceDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function invoiceCronAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->invoiceCron($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Invoice generated successfully',
        ]);
    }

    public function generateUtilityInvoiceAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->generateUtilityInvoice($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Utility Invoice Generated successfully',
        ]);
    }

    public function generateRecurringSetupInvoiceAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->generateRecurringSetupInvoice($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Recurring Setup Invoice Generated successfully',
        ]);
    }
    public function generateStudentFeeInvoiceAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->generateStudentInvoice($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Student Invoice Generated successfully',
        ]);
    }

    public function generateStudentFeeInvoiceUpdatedAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->generateStudentInvoice1($postData);
        
        return new JsonModel([
            'status'  => 200,
            'result' => 'Student Invoice Generated successfully',
        ]);
    }

    public function invoicesByTypeAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->invoicesByType($postData);
        
        return new JsonModel([
            'status'  => 200,
            'result' => $data
        ]);
    }

     public function paidInvoiceAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $invoiceRepository = $this->getRepository('T071finvoice');
        $data = $invoiceRepository->paidInvoice($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Paid successfully',
        ]);
    }

    public function customerMasterInvoices1Action()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T071finvoice');
        $result            = $invoiceRepository->getCustomerMasterInvoices1($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function deleteMasterInvoiceDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T072finvoiceDetails');
        $result            = $invoiceRepository->deleteMasterInvoiceDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Invoice Details Data Deleted successfully',
        ]);
    }

    public function getMasterInvoiceListMasterAction()
    {
        $request = $this->getRequest();
        $invoiceRepository = $this->getRepository('T071finvoice');
        // print_r("expression");exit;
        $result = $invoiceRepository->getMasterInvoiceListMaster();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

}
