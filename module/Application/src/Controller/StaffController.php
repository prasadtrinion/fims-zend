<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StaffController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getGroupListAction()
    {

        $request = $this->getRequest();
        $staffRepository = $this->getRepository('T034fstaffMaster');
        $result = $staffRepository->getGroupList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getLoanStaffAction()
    {

        $request = $this->getRequest();
        $staffRepository = $this->getRepository('T034fstaffMaster');
        $result = $staffRepository->getFullList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
     public function loanStaffAction()
    {

        $request = $this->getRequest();
        $staffRepository = $this->getRepository('T034fstaffMaster');
        $result = $staffRepository->loanStaff();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function nonBlacklistAction()
    {

        $request = $this->getRequest();
        $staffRepository = $this->getRepository('T034fstaffMaster');
        $result = $staffRepository->nonBlackList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function getNewStaffAction()
    {

        $request = $this->getRequest();
        $staffRepository = $this->getRepository('T034fstaffMaster');
        $result = $staffRepository->getNewStaff();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

   

    public function staffListBasedOnDepartmentAction()
    {
        
         $em = $this->getEntityManager();
         $request = $this->getRequest();

	 $id = $this->params()->fromRoute('id');
       
         $staffRepository = $this->getRepository('T034fstaffMaster');
         $staff = $staffRepository->findAll($id);
         if(!$staff)
         {
             return $this->resourceNotFound();
         }
         $result = $staffRepository->getStaffList($id);
         return new JsonModel([
             'status' => 200,
        'result' => $result
        ]);
    }
    public function getEmployeeEmailAction()
    {
        
         $em = $this->getEntityManager();
         $request = $this->getRequest();

        $id = $this->params()->fromRoute('id');
       
         $staffRepository = $this->getRepository('T034fstaffMaster');
         
         $result = $staffRepository->getEmployeeEmail($id);
         return new JsonModel([
             'status' => 200,
        'result' => $result
        ]);
    }

    // public function create($postData)
    // {

    //     $em = $this->getEntityManager();
    //     $request = $this->getRequest();
        

    //     $rawBody = file_get_contents("php://input");

        
    //     if($this->isJSON($rawBody))
    //     {
            
    //         $utilityRepository = $this->getRepository('T078futility');

    //         try
    //         {
    //             $utilityObj = $utilityRepository->createNewData($postData);
    //         }

           
    //         catch (\Exception $e)
    //         {
    //             if($e->getErrorCode()== 1062)
    //             {
    //                     return new JsonModel([
    //                         'status' => 200,
    //                         'message' => 'Utility Already exist.'
    //                     ]);
    //             }
    //         }
    //         return new JsonModel([
    //                 'status' => 200,
    //                 'message' => 'Utility Added successfully',
    //                 'utilityId' => $utilityObj->getF078fid()
    //             ]);

    //     }
    //     else
    //     {
    //         return $this->invalidInputType();
    //     }
    // }

    // public function update($id, $postData)
    // {
       
    //     $em = $this->getEntityManager();
    //     $request = $this->getRequest();
    //     $utilityRepository = $this->getRepository('T078futility');
    //     $utility = $utilityRepository->find((int)$id);

    //     if(!$utility)
    //     {
    //         return $this->resourceNotFound();
    //     }

    //     $rawBody = file_get_contents("php://input");

    //     if($this->isJSON($rawBody))
    //     {

    //         $utilityObj = $utilityRepository->updateData($utility,$postData);
    //         return new JsonModel([
    //             'status' => 200,
    //             'message' => 'Updated successfully'
    //         ]);
    //     }
    //     else
    //     {
    //         return $this->invalidInputType();
    //     }
      
    // }

}
