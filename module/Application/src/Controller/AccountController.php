<?php
namespace Application\Controller;

use Application\Entity\Account;
use Application\Form\LoginForm;
use Application\Form\UserForm;
use Application\Repository\LocationRepository;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Entity\Role;

/**
 * Account Controller
 *
 * @package Controller
 */
class AccountController extends AbstractAppController
{
    protected $authService;
    
    public function __construct(
        ServiceManager $sm,
        AuthenticationService $authService
    ){
        $this->authService = $authService;
        
        parent::__construct($sm);
    }
    
    /**
     * Default action
     *
     * URL: /
     */
    public function indexAction()
    {
        return $this->redirect('login');
    }

    /**
     * Action to handle user authentication request
     *
     * URL: /login
     */
    public function loginAction()
    {   
        $request = $this->getRequest();
        $viewParams = array();
        $form = new LoginForm();
        

        if ($request->isPost()) {
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {

                $accountRepository = $this->getRepository('Account');
                $userRepository = $this->getRepository('User');
                $postData = json_decode($rawBody,true);
               // print_r($postData);exit;

                if(empty($postData['email']) || empty($postData['password']))
                {
                    return $this->invalidInputType(); 
                }

                $this->authService->getAdapter()->setIdentity($postData['email']);
                $this->authService->getAdapter()->setCredential($postData['password']);
                
                $result = $this->authService->authenticate();
                $identity = $result->getIdentity();
                if ($result->isValid()) {    
                    $tokenService = $this->sm->get('app.token');
                    $identity['roleDisplayName'] = $roleNames[$identity['roleID']];
                    $userToken = $tokenService->generateUserAuthToken($identity);
                    $accountEntity = $accountRepository->find($identity['accountId']);
                    $accountEntity->setSsoToken($userToken);
                    $accountRepository->update($accountEntity);
                    return new JsonModel([
                        'status' => 200,
                        'message' => "login successful",
                        'access-token' => "$userToken",
                    ]);
                }
                
                if ($result->getCode() == Result::FAILURE_CREDENTIAL_INVALID) {
                    $msg = 'The email address and password do not match. Please re-enter and try again.';
                } elseif ($result->getCode() == Result::FAILURE_UNCATEGORIZED) {
                    $msg= 'Your Account has been disabled please contact Diversey hang allowance administrator.';
                } elseif ($result->getCode() == Result::FAILURE_IDENTITY_AMBIGUOUS) {
                    $msg = 'The email address and password do not match. Please re-enter and try again.';
                }
            
                $this->getResponse()->setStatusCode(401);
                return new JsonModel([
                    'status' => 401,
                    'message' => "$msg"
                ]);
                
            }else{

                return $this->invalidInputType();
            }
          //$header = $request->getHeader('X-Auth-Client');
        } else {
            return $this->methodNotAllowed();
        }
    }
    
    /**
     * Action to handle new user registration
     *
     * URL: /register
     */
    public function registerAction() {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {

                $accountRepository = $this->getRepository('Account');
                $userRepository = $this->getRepository('User');
                $postData = json_decode($rawBody,true);
               // print_r($postData);exit;

                if(empty($postData['name']) || empty($postData['email']) || empty($postData['password']) || empty($postData['mobile']) )
                {
                    return $this->invalidInputType(); 
                }
                
                $encoder = $this->sm->get('app.password_encoder');
                $passwordEncode = $encoder->encode($postData['password']);
                $postData['password'] = $passwordEncode;
                //print_r($postData);exit;
                
                // Create account
                try{
                    $accountObj = $accountRepository->createAccount($postData);
                } catch (\Exception $e) {
                    //$errorCode = $e->errorInfo[1];     
                    //echo ; exit;     
                    if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Username Already exist.'
                        ]);
                    }
                }
                $roleId = Role::ROLE_DSR;
                $postData['roleId'] = $roleId;
                
                // Create user
                $userRepository = $this->getRepository('User');
                $userRepository->createUser($accountObj, $postData);

            
                return new JsonModel([
                    'status' => 200,
                    'message' => 'User Created successfully.Please verify email'
                ]);
                
            }else{

                return $this->invalidInputType();
            }
          //$header = $request->getHeader('X-Auth-Client');
        } else {
            return $this->methodNotAllowed();
        }
    }
    
    /**
     * Show confirmation message after registration
     */
    public function registerSuccessAction()
    {
        return new ViewModel();
    }
    
    /**
     * Action to manage user profile
     *
     * URL: /profile
     */
    public function profileAction() {
        $authUser = $this->authService->getIdentity();
        $viewParams = [];
        $isEmailModified = false;
        
        $userRepository = $this->getRepository('User');
        $accountRepository = $this->getRepository('Account');
        $accountEntity = $accountRepository->find($authUser['accountId']);
        
        $user = $userRepository->find($authUser['userId']);
        
        $em = $this->getEntityManager();
        $stateList = LocationRepository::getStatesByCountry($user->getCountry());
       
        $formOptions = [
            'state' => $stateList,
            'action' => 'edit-profile'
        ];
        
        $form = new UserForm($em, $formOptions);
        $form->get('username')->setValue($accountEntity->getUsername());
        $form->get('state')->setAttribute('options', LocationRepository::getStatesByCountry($user->getCountry()));
        $form->get('distState')->setAttribute('options', LocationRepository::getStatesByCountry($user->getDistCountry()));
        $form->bind($user);
        
        $request = $this->getRequest();
        
        if($request->isPost()){

            $form->bindValues($request->getPost()->toArray());
            
            if($form->isValid()){
                $postParams = $request->getPost()->toArray();
                
                if($accountEntity->getUsername() != $postParams['username']){
                    $isAccountExists = $accountRepository->isAccountExist($postParams['username']);
                    
                    if($isAccountExists){
                        $this->flashMessenger()->addErrorMessage('Username is already registered.');
                        return $this->redirect()->toRoute('profile');
                    } else {
                        $isEmailModified = true;
                        $token = md5(uniqid(time()));
                        
                        $accountEntity->setUsername($postParams['username'])
                            ->setVerificationToken($token)
                            ->setEmailStatus(Account::STATUS_UNVERIFIED);
                    }
                }
                
                if(!empty($postParams['password'])){
                    $encoder = $this->sm->get('app.password_encoder');
                    $password = $encoder->encode($postParams['password']);
                    $accountEntity->setPassword($password);
                }
              
                // Update account
                $accountRepository->update($accountEntity);
                
                // Update user
                $userRepository->updateUser($user, $postParams);
                
                // Update user session
                $authUser['firstName'] = $postParams['firstName'];
                $authUser['lastName'] = $postParams['lastName'];
                $authUser['username'] = $accountEntity->getUsername();
                
                $this->authService->getStorage()->write($authUser);
                
                // Trigger event if email has been modified
                if ($isEmailModified) {
                    $this->getEventManager()->trigger('account.email.modified', $this, [
                        'user' => $postParams,
                        'account' => $accountEntity
                    ]);
                }
                
                $this->flashMessenger()->addSuccessMessage('Your profile updated successfully');
                
                return $this->redirect()->toRoute('profile');
            }
        }
        
        $viewParams['form'] = $form;
        $viewParams['user'] = $user;
        $viewParams['dsrTypes'] = $userRepository->getDsrTypes();
        
        return new ViewModel($viewParams);
    }
    
    /**
     * Action to logs out user from system
     */
    public function logoutAction() {
        $this->authService->getStorage()->forgetMe();
        $this->authService->clearIdentity();
    
        return $this->redirect()->toRoute('login');
    }

    /**
     * Action to handle forgot password request
     *
     * URL: /forgot-password
     */
    public function forgotPasswordAction()
    {
        $request = $this->getRequest();
        $errors = null;
        $msg = null;
        
        if ($request->isPost()) {
            $username = $this->params()->fromPost('username');
            $accountRepository = $this->getRepository('Account');
            $account = $accountRepository->getAccountByUsername($username);

            if (! empty($account) && $account != null) {
                $token = md5(uniqid(time()));
                
                $resetUrl = $this->url()->fromRoute('resetPassword', array(
                    'token' => $token
                ), array(
                    'force_canonical' => true
                ));
                
                $account->setResetToken($token);
                $accountRepository->update($account);
                
                $userRepository = $this->getRepository('User');
                $user = $userRepository->getUserByAccount($account->getId());
                
                // Trigger event
                $this->getEventManager()->trigger('account.password.request', $this, [
                    'resetUrl' => $resetUrl,
                    'user' => $user
                ]);
                
                $msg = "A reset password link has been emailed to " . $username;
                $this->flashMessenger()->addSuccessMessage($msg);
            } else {
                $errors = 'This Email address ' . $username . ' is not registerd with us.';
                $this->flashMessenger()->addErrorMessage($errors);
            }
            
            return $this->redirect()->toRoute('forgotPassword');
        }
        
        return new ViewModel();
    }
    
    /**
     * Action to reset user's password
     *
     * URL: /reset-password/:token
     */
    public function resetPasswordAction() 
    {
        $token = $this->params()->fromRoute('token');
        $accountRepository = $this->getRepository('Account');
        $userDetails = $accountRepository->getAccountByToken($token);
        $errors = '';
        $msg = '';
        $email = '';
        $status = false;
        
        if (empty($userDetails) || $userDetails == null) {
            return $this->redirect()->toRoute('login');
        } else {
            $email = $userDetails->getUsername();
        }
        
        if ($this->getRequest()->isPost() && ! empty($userDetails)) {
            $password = $this->params()->fromPost('password');
            
            if ($password) {
                $encoder = $this->sm->get('app.password_encoder');
                $password = $encoder->encode($password);
                $userDetails->setPassword($password);
                $userDetails->setResetToken(null);
                $accountRepository->update($userDetails);
                $msg = "Your password has been updated successfully.";
                $status = true;
            }
        }
        
        $viewParams = array(
            'error' => $errors,
            'message' => $msg,
            'email' => $email,
            'status' => $status
        );
        
        return new ViewModel($viewParams);
    }
    
    /**
     * Action to verify email address
     *
     * URL: /verify-email/:token
     */
    public function verifyEmailAction() 
    {
        $token = $this->params()->fromRoute('token');
        $accountRepository = $this->getRepository('Account');
        $account = $accountRepository->findOneBy(array(
            'verificationToken' => $token
        ));
        
        if ($account) {
            $account->setEmailStatus(Account::STATUS_VERIFIED);
            $account->setStatus(Account::STATUS_VERIFIED);
            $account->setVerificationToken(null);
            $accountRepository->update($account);
            
            $authService = $this->sm->get('app.authentication');
            
            if ($authService->hasIdentity()) {
                $identity = $authService->getIdentity();
                $identity['emailStatus'] = Account::STATUS_VERIFIED;
                $authService->getStorage()->write($identity);
            }
        } else {
            return $this->redirect()->toRoute('login');
        }
        
        $viewParams = [];
        $msg = "Your Email has been verified successfully.";
        $viewParams['message'] = $msg;
        
        return new ViewModel($viewParams);
    }
    public function getAccountlistAction()
    {
        $this->getRequest();
        $accountRepository=$this->getRepository('Account');
        $accountObj=$accountRepository->getAllUsers();
        return new JsonModel([
                    'status' => 200,
                    'result' => $accountObj
                ]);
    }
}
