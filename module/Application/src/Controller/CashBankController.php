<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CashBankController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $cashbankRepository = $this->getRepository('T096fcashBank');
        $result = $cashbankRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $cashbankRepository = $this->getRepository('T096fcashBank');
        $cashbank = $cashbankRepository->find($id);
        if(!$cashbank)
        {
            return $this->resourceNotFound();
        }
        $result = $cashbankRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $cashbankRepository = $this->getRepository('T096fcashBank');

            try
            {
                $cashbankObj = $cashbankRepository->createCashbank($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'cashbank Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'cashbank Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $cashbankRepository       = $this->getRepository('T096fcashBank');
        $cashbankDetailRepository = $this->getRepository('T097fcashBankDetails');
        $cashbank                 = $cashbankRepository->find($id);
        
        if (!$cashbank) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $cashbankObj = $cashbankRepository->updateCashbank($cashbank, $postData);

            $cashbankDetails = $postData['cashbank-details'];
            foreach ($cashbankDetails as $cashbankDetail) 
            {

                if (array_key_exists("f097fid", $cashbankDetail)) 
                {

                    $cashbankDetailObj = $cashbankDetailRepository->find((int) $cashbankDetail['f097fid']);

                    if ($cashbankDetailObj) {
                        $cashbankDetailRepository->updateCashbankDetail($cashbankDetailObj, $cashbankDetail);
                    }
                } else {

                    $cashbankDetailRepository->createCashbankDetail($cashbankObj, $cashbankDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function getCashBankDetailAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $cashRepository = $this->getRepository('T096fcashBank');
       
        $result = $cashRepository->getCashBankDetail($postData);


        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function cashBankApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $cashRepository = $this->getRepository('T096fcashBank');
        $cash = $postData['id'];
        $status = $postData['status'];
        $reason = $postData['reason'];
        $cashObjs=array();
        foreach($cash as $id)
        {
            $cashObj =$cashRepository->find((int)$id);
            if(!$cashObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Application does not exist.'
                ]);
            }
            array_push($cashObjs, $cashObj);
        }
        $cashApplication = $cashRepository->cashBankApproval($cashObjs,$status,$reason);

        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getManualCashBankDetailAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $cashRepository = $this->getRepository('T096fcashBank');
       
        $result = $cashRepository->getManualCashBankDetail($postData);


        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}