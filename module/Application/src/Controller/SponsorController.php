<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class SponsorController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $sponsorRepository = $this->getRepository('T063fsponsor');
        $result = $sponsorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $sponsorRepository = $this->getRepository('T063fsponsor');
        $sponsor = $sponsorRepository->find((int)$id);
        if(!$sponsor)
        {
            return $this->resourceNotFound();
        }
        $result = $sponsorRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }


    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $sponsorRepository = $this->getRepository('T063fsponsor');

        
            $sponsorObj =$sponsorRepository->createSponsor($postData);
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $sponsorRepository = $this->getRepository('T063fsponsor');
        $sponsorDetailRepository = $this->getRepository('T063fsponsorDetails');
        $sponsor         =$sponsorRepository->find($id);

        if (!$sponsor) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $sponsorObj     = $sponsorRepository->updateSponsor($sponsor, $postData);
            $sponsorDetails = $postData['sponsor-details'];
            // print_r($legalObj);
            // exit();
            foreach ($sponsorDetails as $sponsorDetail) 
            {

                if (array_key_exists("f063fidDetails", $sponsorDetail)) 
                {
                    
                    $sponsorDetailObj = $sponsorDetailRepository->find((int) $sponsorDetail['f063fidDetails']);
                

                    if ($sponsorDetailObj)
                    {

                        try{
                        $sponsorDetailRepository->updateSponsorDetail($sponsorDetailObj, $sponsorDetail);
                        }
                        catch(\Exception $e)
                        {
                            echo $e;
                        }
                    }
                } else {

                    $sponsorDetailRepository->createSponsorDetail($sponsorObj, $sponsorDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function sponsorStudentInvoicesAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $sponsorRepository = $this->getRepository('T063fsponsor');
        $result = $sponsorRepository->sponsorStudentInvoices((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }

    public function deleteSponsorDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $sponsorDetailRepository = $this->getRepository('T063fsponsorDetails');
        // print_r($postData);exit;
        $result            = $sponsorDetailRepository->deleteSponsorDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Sponsor Details Deleted successfully',
        ]);
    }
}
