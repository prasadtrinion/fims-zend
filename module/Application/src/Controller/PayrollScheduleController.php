<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class PayrollScheduleController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $scheduleRepository = $this->getRepository('T062fpayrollSchedule');
        $result = $scheduleRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $scheduleRepository = $this->getRepository('T062fpayrollSchedule');
        $schedule = $scheduleRepository->find((int)$id);
        if(!$schedule)
        {
            return $this->resourceNotFound();
        }
        $result = $scheduleRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         // $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $scheduleRepository = $this->getRepository('T062fpayrollSchedule');
            $schedules = $postData['data'];
            
            foreach ($schedules as $data) 
            {
            
                if(array_key_exists("f062fid",$data))
                {

                    $schedule = $scheduleRepository->find((int)$data['f062fid']);
                    if($schedule)
                    {
                        $scheduleObject = $scheduleRepository->updateData($schedule,$data);
                    }
                }   
                else
                {
                 
                    $scheduleObject = $scheduleRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

   
}