<?php 
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LicenseController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $LicenseRepository = $this->getRepository('T091flicense');
        
        $result = $LicenseRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    
    public function get($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $LicenseRepository = $this->getRepository('T091flicense');
        $License = $LicenseRepository->find((int)$id);
        if(!$License)
        {
            return $this->resourceNotFound();
        }
        $result = $LicenseRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        if($this->isJSON($rawBody))
        {
            $LicenseRepository = $this->getRepository('T091flicense');


            $master = $LicenseRepository->findBy(array("f091flicense"=>strtolower($postData['f091flicense'])));
                
            
            if($master){
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                }

            try
            {
                $LicenseObj = $LicenseRepository->createNewData($postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                    return new JsonModel([
                    'status' => 409,
                    'message' => 'Already exist.'
                    ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $LicenseRepository = $this->getRepository('T091flicense');
        $License = $LicenseRepository->find((int)$id);
        if(!$License)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            $LicenseObj = $LicenseRepository->updateData($License,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }

    public function getActiveLicenseAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $LicenseRepository = $this->getRepository('T091flicense');
        
        $result = $LicenseRepository->getActiveLicense((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
}