<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class GrnController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fgrn');
        $result = $purchaseRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function awaitingGrnsAction()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fgrn');
        $result = $purchaseRepository->awaitingGrns();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

      public function expenseGrnsAction()
    {

        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $purchaseRepository = $this->getRepository('T034fgrn');
        $result = $purchaseRepository->expenseGrns($postData);
        return new JsonModel([
            'status' => 200,
            'result' => "Updated successfully" 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $purchaseRepository = $this->getRepository('T034fgrn');
        $purchase = $purchaseRepository->find($id);
        if(!$purchase)
        {
            return $this->resourceNotFound();
        }
        $result = $purchaseRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getGrnApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $grnRepository = $this->getRepository('T034fgrn');
        
        $result = $grnRepository->getListByApprovedStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function GrnApprovalAction()
    {
       // print_r()
        $em = $this->getEntityManager();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
 
        $grnRepository = $this->getRepository('T034fgrnDetails');
        
        $grn = $grnRepository->updateGrnDetailApprovalStatus($postData);



        // $bill = $cashRepository->createBill($cashApplication);

        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        
        if($this->isJSON($rawBody))
        {
            try
            {
             
            $grnRepository = $this->getRepository('T034fgrn');
            $grnDetailsRepository = $this->getRepository('T034fgrnDetails');
            if (array_key_exists("f034fid", $postData)) 
            {
                // print_r($postData);
                // exit();
                $grnObj = $grnRepository->find((int)$postData['f034fid']);
                
                $grnObj =  $grnRepository->updateGrn($grnObj, $postData);
                $details = $postData['grn-details'];
        
                foreach ($details as $detail) 
                {
                   
                    if (array_key_exists("f034fidDetails", $detail)) 
                    {

                        $detailObj = $grnDetailsRepository->find((int) $detail['f034fidDetails']);

                        if ($detailObj) 
                        {
                            $grnDetailsRepository->updateDetail($detailObj, $detail);
                        }
                    } 
                    else 
                    {
                        $grnDetailsRepository->createDetail($grnObj, $detail);
                    }
                }
            }
            else
            {
                $grnObj = $grnRepository->createGrn($postData);
            }
            }
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'GRN Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'GRN Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $purchaseRepository       = $this->getRepository('T034fgrn');
        $purchaseDetailRepository = $this->getRepository('T034fgrnDetails');
        $purchase                 = $purchaseRepository->find($id);

        if (!$purchase) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $purchaseObj     = $purchaseRepository->updateGrn($purchase, $postData);
            $purchaseDetails = $postData['grn-details'];
            foreach ($purchaseDetails as $purchaseDetail) 
            {

                if (array_key_exists("f034idDetails", $purchaseDetail)) 
                {

                    $purchaseDetailObj = $purchaseDetailRepository->find((int) $purchaseDetail['f034idDetails']);

                    if ($purchaseDetailObj) {
                        $purchaseDetailRepository->updateDetail($purchaseDetailObj, $purchaseDetail);
                    }
                } else {

                    $purchaseDetailRepository->createDetail($purchaseObj, $purchaseDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    // public function GrnApprovalAction()
    // {

    //     // $rawBody           = file_get_contents("php://input");
    //     // $postData          = json_decode($rawBody, true);
    //     // $grnRepository = $this->getRepository('T034fgrn');

    //     // $grn = $grnRepository->updateApprovalStatus($postData);
    //     // return new JsonModel([
    //     //     'status'  => 200,
    //     //     'message' => 'GRNs Approved successfully',
    //     // ]);


    //     // {
       
    //     $em = $this->getEntityManager();
    //     $rawBody = file_get_contents("php://input");
    //     $postData = json_decode($rawBody,true);

    //     $grnRepository = $this->getRepository('T034fgrn');
    //     $grn = $postData['id'];

    //     $grnObjs=array();
    //     foreach($grn as $id)
    //     {
            
    //         $grnObj =$grnRepository->find((int)$id);
    //         if(!$grnObj)
    //         {
    //             return new JsonModel([
    //                 'status' => 410,
    //                 'message' => ' Does not exist.'
    //             ]);
    //         }
    //         array_push($grnObjs, $grnObj);
    //     }
    //     $grn = $grnRepository->updateApprovalStatus($grnObjs);



    //     // $bill = $cashRepository->createBill($cashApplication);

    //     return new JsonModel([
    //         'status' => 200,
    //         'message' => 'approved successfully'
    //     ]);
    
    // }

     public function donorGrnsAction()
    {

        $rawBody           = file_get_contents("php://input");
        // $postData          = json_decode($rawBody, true);
        $grnRepository = $this->getRepository('T034fgrn');

        $grn = $grnRepository->donorGrns();
        return new JsonModel([
            'status'  => 200,
            'result' => $grn
        ]);
    }

    public function GrnDetailApprovalAction()
    {

       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $grnRepository = $this->getRepository('T034fgrn');
        $journalRepository = $this->getRepository('T017fjournal');
        $grn = $postData['id'];
        $grnObjs=array();
        foreach($grn as $id)
        {
// print_r($id);exit;

            
            $grnObj =$grnRepository->find((int)$id);
            if(!$grnObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Does not exist.'
                ]);
            }
            array_push($grnObjs, $grnObj);
        }
        $grn = $grnRepository->updateGrnDetailApprovalStatus($postData,$journalRepository);



        // $bill = $cashRepository->createBill($cashApplication);

        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    
    }

    public function grnNotInAction()
    {
        // $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fgrn');
        $result = $purchaseRepository->grnNotIn();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deleteGRNDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $GrnRepository = $this->getRepository('T034fgrnDetails');
        $result            = $GrnRepository->deleteGRNDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'GRN Details Data Deleted successfully',
        ]);
    }

    public function getGrnByIdAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $GrnRepository = $this->getRepository('T034fgrnDetails');
        $result            = $GrnRepository->getGrnById($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function updateGrnBillRegistrationAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $GrnRepository = $this->getRepository('T034fgrn');
        $result            = $GrnRepository->updateGrnBillRegistration($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Bill Generated',
        ]);
    }

    public function listOfGrnByStatusAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $GrnRepository = $this->getRepository('T034fgrn');
        $result            = $GrnRepository->listOfGrnByStatus((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getGrnDetailsListByIdAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $GrnRepository = $this->getRepository('T034fgrn');
        $result            = $GrnRepository->getGrnDetailsListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getGrnByAssetAction()
    {
         $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $GrnRepository = $this->getRepository('T034fgrn');
        $result            = $GrnRepository->getGrnByAsset();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}