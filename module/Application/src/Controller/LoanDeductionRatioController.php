<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LoanDeductionRatioController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $loanDeductionRatioRepository = $this->getRepository('T131loanDeductionRatio');

        $result = $loanDeductionRatioRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $loanDeductionRatioRepository = $this->getRepository('T131loanDeductionRatio');
        $loanRatio = $loanDeductionRatioRepository->find((int)$id);
        if(!$loanRatio)
        {
            return $this->resourceNotFound();
        }
        $result = $loanDeductionRatioRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $loanDeductionRatioRepository = $this->getRepository('T131loanDeductionRatio');
             
            try
            {


                 if (array_key_exists("f131fid", $data)) 
                {

                    $licenseObj = $loanDeductionRatioRepository->find((int)$data['f131fid']);

                    if ($licenseObj)
                    {
                    // print_r($licenseObj);exit;

                        $loanDeductionRatioRepository->updateData($licenseObj, $data);
                    }
                } else
                {

                $loanratioObj = $loanDeductionRatioRepository->createNewData($data);
                }
            }

           
            catch (Exception $e)
            {
                
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

	public function getLoanDeductionRatioActiveAction()
    {
        $request = $this->getRequest();
        $loanDeductionRatioRepository = $this->getRepository('T131loanDeductionRatio');
        $id = $this->params()->fromRoute('id');

        $list=$loanDeductionRatioRepository->getLoanDeductionRatioActive((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

}