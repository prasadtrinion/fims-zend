<?php

namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class CreditNoteController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function get($id)
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $creditNoteRepository = $this->getRepository('T031fcreditNote');

        try {
            $creditNote = $creditNoteRepository->find((int)$id);
        } catch (\Exception $e) {
            echo $e;
        }
        if (!$creditNote) {
            return $this->resourceNotFound();
        }

        $result = $creditNoteRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }

    public function getList()
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $creditNoteRepository = $this->getRepository('T031fcreditNote');
        try {
            $creditNoteDetails = $creditNoteRepository->getList();
        } catch (\Exception $e) {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data'   => $creditNoteDetails,
        ]);
    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        
        $details = $postData['details'];
        $totalAmount = 0;
        foreach ($details as $detail) {
            $totalAmount = $totalAmount + $detail['f032ftotal'];
        }
        // try{
        //     $invoiceRepository = $this->getRepository('T071finvoice');

        //         $invoiceObj = $invoiceRepository->find((int)$postData['f031fidInvoice']);
        // }
        // catch(\Exception $e){
        //     echo $e;
        // }
        // $invoiceAmount = $invoiceObj->getF071fbalance();
        // $balance = $invoiceAmount + $totalAmount;
        
        // $invoiceObj->setF071fbalance((float)$balance);

        // try{
        // $em->persist($invoiceObj);
        //         $em->flush();
        //     }
        // catch(\Exception $e){
        //     echo $e;
        // }
        if ($this->isJSON($rawBody)) {

            try
            {

                $creditNoteRepository = $this->getRepository('T031fcreditNote');
                $creditNoteDetailRepository = $this->getRepository('T032fcreditNoteDetails');
                if (array_key_exists("f031fid", $postData)) 
                    {
                    $creditNoteObj = $creditNoteRepository->find((int)$postData['f031fid']);
                        
                    $creditNoteObj = $creditNoteRepository->updateMaster($creditNoteObj, $postData);
                $details = $postData['details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f032fid", $detail)) 
                    {
    
                        $detailObj = $creditNoteDetailRepository->find((int) $detail['f032fid']);
    
                        if ($detailObj) {
                            $creditNoteDetailRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $creditNoteDetailRepository->createDetail($creditNoteObj, $detail);
                    }
                }
            }
            else{
                $creditNoteObj = $creditNoteRepository->createNewData($postData);
            }

            } catch (\Exception $e) {

                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'Credit Note Already exist.',
                    ]);
                }
            }

            return new JsonModel([
                'status'       => 200,
                'message'      => 'Credit Note Created successfully',
                'creditNoteId' => $creditNoteObj->getF031fId(),
            ]);

        } else {
            return $this->invalidInputType();
        }

    }

    public function approvedCreditNotesAction()
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $creditNoteRepository = $this->getRepository('T031fcreditNote');


        $result = $creditNoteRepository->approvedCreditNotes((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }
    public function approveAction()
    {

        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);
        $journalRepository = $this->getRepository('T017fjournal');
        $creditNoteRepository = $this->getRepository('T031fcreditNote');
        $creditNoteRepository->approve($postData,$journalRepository);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Credit Notes Approved successfully',
        ]);
    }

    public function getCreditNoteApprovalsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $creditNoteRepository = $this->getRepository('T031fcreditNote');


        $result = $creditNoteRepository->getCreditNoteApproval((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);
    }

}
