<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class InsuranceCompanyController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $insuranceRepository = $this->getRepository('T101finsuranceCompany');

        $result = $insuranceRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
     public function vendorNotInInsuranceCompanyAction()
    {

        $request = $this->getRequest();

        $insuranceRepository = $this->getRepository('T101finsuranceCompany');

        $result = $insuranceRepository->vendorNotInInsuranceCompany();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $insuranceRepository = $this->getRepository('T101finsuranceCompany');
        $insurance = $insuranceRepository->find((int)$id);
        if(!$insurance)
        {
            return $this->resourceNotFound();
        }
        $result = $insuranceRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $insuranceRepository = $this->getRepository('T101finsuranceCompany');
             
            try
            {
                
                $insuranceObj = $insuranceRepository->createNewData($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $insuranceRepository = $this->getRepository('T101finsuranceCompany');
        $insuranceDetailRepository = $this->getRepository('T101finsuranceCompanyDetails');
        $insurance          =$insuranceRepository->find($id);

        if (!$insurance)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody))
        {

            $insuranceObj = $insuranceRepository->updateData($insurance,$postData);
            $insuranceDetails = $postData['insurance-details'];
        
            foreach ($insuranceDetails as $insuranceDetail) 
            {

                if (array_key_exists("f101fidDetails", $insuranceDetail)) 
                {

                    $insuranceDetailObj = $insuranceDetailRepository->find((int) $insuranceDetail['f101fidDetails']);

                    if ($insuranceDetailObj) {
                        
                        $insuranceDetailRepository->updateInsuranceDetail($insuranceDetailObj, $insuranceDetail);
                    }
                } else {

                    $insuranceDetailRepository->createInsuranceDetail($insuranceObj, $insuranceDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
     public function getInsuranceValueAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $age = $postData['age'];
        $period = floor((int)$postData['period']/12);
        $year = 'getF101fyear'.$period;
        
        $insuranceRepository = $this->getRepository('T101finsuranceCompanyDetails');
        $detailObj = $insuranceRepository->findBy(array('f101fage'=>(int)$age));
        
        $detailObj = $detailObj[0];
        if($detailObj && $period!=0)
        {
            $value = $detailObj->$year();
        }
        
        return new JsonModel([
            'status'  => 200,
            'value' => $value
        ]);
    }
}
