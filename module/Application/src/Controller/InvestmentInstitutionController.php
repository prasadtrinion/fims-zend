<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class InvestmentInstitutionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $InstitutionRepository = $this->getRepository('T061finvestmentInstitution');
        $result = $InstitutionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $InstitutionRepository = $this->getRepository('T061finvestmentInstitution');
        $Institution = $InstitutionRepository->find((int)$id);
        if(!$Institution)
        {
            return $this->resourceNotFound();
        }
        $result = $InstitutionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getAllInvestmentTypeAction()
    {

        $institution = $this->params()->fromRoute('institution');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $investmentInstitutionRepository = $this->getRepository('T061finvestmentInstitution');
        
        $result = $investmentInstitutionRepository->getListByInvestmentType($institution);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    } 

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $InstitutionRepository = $this->getRepository('T061finvestmentInstitution');

            $institute=$InstitutionRepository->findBy(array("f061finstitutionCode"=>strtolower($postData['f061finstitutionCode'])));
                
            
            if($institute)
            {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
            }

            else
            {
                $InstitutionObj = $InstitutionRepository->createInstitution($postData);
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Registered successfully',
                    
                ]);
            }
            

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $InstitutionMasterRepository = $this->getRepository('T061finvestmentInstitution');
        $InstitutionDetailRepository = $this->getRepository('T061finstituteDetails');
        $Institution           =$InstitutionMasterRepository->find((int)$id);

        if (!$Institution) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");
       
        if ($this->isJSON($rawBody)) {

            $InstitutionObj     = $InstitutionMasterRepository->updateInstitution($Institution ,$postData);
            $InstitutionDetails = $postData['institute-details'];
            foreach ($InstitutionDetails as $InstitutionDetail) 
            {

                if (array_key_exists("f061fidDetails", $InstitutionDetail)) 
                {

                    $InstitutionDetailObj = $InstitutionDetailRepository->find((int) $InstitutionDetail['f061fidDetails']);

                    if ($InstitutionDetailObj) {
                       
                        $InstitutionDetailRepository->updateInstitutionDetail($InstitutionDetailObj, $InstitutionDetail);
                    }
                } else {

                    $InstitutionDetailRepository->createInstitutionDetail($InstitutionObj, $InstitutionDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteInvestmentInstitutionAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $InstitutionDetailRepository = $this->getRepository('T061finstituteDetails');
        $result            = $InstitutionDetailRepository->deleteInvestmentInstitution($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Investment Institution Details Deleted successfully',
        ]);
    }

}

?>