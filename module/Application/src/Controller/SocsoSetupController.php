<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class SocsoSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $socsoRepository = $this->getRepository('T091fsocsoSetup');
        $result = $socsoRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $socsoRepository = $this->getRepository('T091fsocsoSetup');
        $socso = $socsoRepository->find((int)$id);
        if(!$socso)
        {
            return $this->resourceNotFound();
        }
        $result = $socsoRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $socsoRepository = $this->getRepository('T091fsocsoSetup');

            $socso=$socsoRepository->findBy(array("f091fcode"=>strtolower($postData['f091fcode'])));

            if($socso)
            {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
            }

            else
            {
                $socsoObj = $socsoRepository->createNewData($postData);
                    return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
            }
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($Id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $socsoSetUpRepository = $this->getRepository('T091fsocsoSetup');
        $id=(int)$Id;
        $socsoSetUp = $socsoSetUpRepository->find($id);

        if(!$socsoSetUp)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $socsoSetUpObj = $socsoSetUpRepository->updateData($socsoSetUp,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}