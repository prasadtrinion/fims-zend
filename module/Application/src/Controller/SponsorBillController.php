<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class SponsorBillController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $sponsorRepository = $this->getRepository('T063fsponsorBill');
        $result = $sponsorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $sponsorRepository = $this->getRepository('T063fsponsorBill');
        $sponsor = $sponsorRepository->find((int)$id);
        if(!$sponsor)
        {
            return $this->resourceNotFound();
        }
        $result = $sponsorRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $sponsorRepository = $this->getRepository('T063fsponsorBill');

            try
            {
                $sponsorObj = $sponsorRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Sponsor Bill Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Sponsor Bill Added successfully',
                    'sponsorId' => $sponsorObj->getF063fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $utilityRepository = $this->getRepository('T078futility');
        $utility = $utilityRepository->find((int)$id);

        if(!$utility)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $utilityObj = $utilityRepository->updateData($utility,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function sponsorHasBillsAction()
    {

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        
        $sponsorRepository = $this->getRepository('T063fsponsorBill');
        $result = $sponsorRepository->getBills((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function sponsorHasInvoicesAction()
    {

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        
        $sponsorRepository = $this->getRepository('T063fsponsorBill');
        $result = $sponsorRepository->getInvoices((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function getSponsorInvoiceAction()
    {
        
        $em = $this->getEntityManager();
        $id = $this->params()->fromRoute('id');
        $id=(int)$id;
        $request = $this->getRequest();
       
        $sponsorRepository = $this->getRepository('T063fsponsorBill');
        $sponsor = $sponsorRepository->find($id);
        if(!$sponsor)
        {
            return $this->resourceNotFound();
        }
        $result = $sponsorRepository->getSponsorInvoiceList($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    

}