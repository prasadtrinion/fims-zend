<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class DisposalRequisitionController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T086fdisposalRequisition');
        
        $result = $assetRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $assetRepository = $this->getRepository('T086fdisposalRequisition');
        $asset = $assetRepository ->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            $disposalRepository = $this->getRepository('T086fdisposalRequisition');
            $disposalDetailRepository = $this->getRepository('T086fdisposalReqDetails');
            
            if(array_key_exists("f086fid", $postData))
            {

            $disposalObj         =$disposalRepository->find($postData['f086fid']);
            $disposalObj     = $disposalRepository->updateDisposal($disposalObj, $postData);
            $disposalDetails = $postData['disposal-details'];
            
            foreach ($disposalDetails as $disposalDetail) 
                {

                if (array_key_exists("f086fidDetails", $disposalDetail)) 
                    {

                    $disposalDetailObj = $disposalDetailRepository->find((int) $disposalDetail['f086fidDetails']);

                    if ($disposalDetailObj)
                        {
                        $disposalDetailRepository->updateDisposalDetail($disposalDetailObj, $disposalDetail);
                        }
                    }
                    else
                    {
                        // print_r($disposalDetail);exit;

                    $disposalDetailRepository->createDisposalDetail($disposalObj, $disposalDetail);
                    }
                }
            }
            else
            {
                $disposalData = $disposalRepository->findBy(array("f086fdescription"=>strtolower($postData['f086fdescription'])));

                if($disposalData)
                {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                }
                 try
                {
                    $disposalRepository->createDisposal($postData);
                }
                catch (\Exception $e)
                {

                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Saving'
                        ]);
                }   
            }
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $disposalRepository = $this->getRepository('T086fdisposalRequisition');
        $disposalDetailRepository = $this->getRepository('T086fdisposalReqDetails');
        $disposal         =$disposalRepository->find($id);

        if (!$disposal) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $disposalObj     = $disposalRepository->updateDisposal($disposal, $postData);
            $disposalDetails = $postData['disposal-details'];
            // print_r($incrementObj);
            // exit();
            foreach ($disposalDetails as $disposalDetail) 
            {

                if (array_key_exists("f086fidDetails", $disposalDetail)) 
                {

                    $disposalDetailObj = $disposalDetailRepository->find((int) $disposalDetail['f086fidDetails']);

                    if ($disposalDetailObj) {
                        $disposalDetailRepository->updateDisposalDetail($disposalDetailObj, $disposalDetail);
                    }
                } else {

                    $disposalDetailRepository->createDisposalDetail($disposalObj, $disposalDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    
    public function assetDisposalVerifyAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $assetRepository = $this->getRepository('T086fdisposalRequisition');
        $asset = $postData['id'];
        $assetObjs=array();
        foreach($asset as $id)
        {
            $assetObj =$assetRepository->find((int)$id);
            if(!$assetObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Asset doesnot exist.'
                ]);
            }
            array_push($assetObjs, $assetObj);
        }
        $asset = $assetRepository->updateVerficationStatus($assetObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }
    public function endorseDisposalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $assetRepository = $this->getRepository('T086fdisposalRequisition');
        
        $asset = $assetRepository->updateEndorsementStatus($postData);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function allDisposalDetailsAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T086fdisposalRequisition');
        
        $result = $assetRepository->getAllDisposalDetails();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deleteDisposalReqDetailAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
               
        $disposalDetailRepository = $this->getRepository('T086fdisposalReqDetails');
        $disposalDetail = $disposalDetailRepository->find((int)$id);
        if(!$disposalDetail)
        {
            return $this->resourceNotFound();
        }
        $result = $disposalDetailRepository->deleteDisposalReqDetail((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => 'Deleted successfully',
        ]);
    }

    public function assetNotInDisposalRequistionAction()
    {
        $request = $this->getRequest();

        $disposalDetailRepository = $this->getRepository('T086fdisposalReqDetails');

        $result = $disposalDetailRepository->assetNotInDisposalRequistion();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    
  
}