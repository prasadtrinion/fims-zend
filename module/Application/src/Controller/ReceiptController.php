<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReceiptController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request           = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        $result            = $receiptRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em             = $this->getEntityManager();
        $request        = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        
        $receipt = $receiptRepository->find((int)$id);
       
        if (!$receipt) {
            return $this->resourceNotFound();
        }

        $result = $receiptRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $receiptRepository = $this->getRepository('T082freceipt');
            $receiptPaymentRepository = $this->getRepository('T083freceiptPaymentInfo');
            $receiptNonInvoiceRepository = $this->getRepository('T084freceiptNonInvoice');
            $receiptInvoiceRepository = $this->getRepository('T084freceiptInvoice');
            $bankRepository = $this->getRepository('T041fbankSetupAccount');
            $bank = $bankRepository->find((int)$postData['f082fpaymentBank']);
            $postData['f084fdebitFundCode'] = $bank->getF041ffundCode();
            $postData['f084fdebitAccountCode'] = $bank->getF041factivityCode();
            $postData['f084fdebitActivityCode'] = $bank->getF041faccountCode();
            $postData['f084fdebitDepartmentCode'] = $bank->getF041fdepartmentCode();

            try
            {
                if(array_key_exists("f082fid", $postData)){

                    $receipt = $receiptRepository->find((int)$postData['f082fid']);
                    $receipt = $receiptRepository->updateReceipt($receipt,$postData);
                    
                    $paymentDetails = $postData['receipt-payments'];
                    $invoiceDetails = $postData['receipt-invoice'];
                    $nonInvoiceDetails = $postData['receipt-non-invoice'];
                    foreach ($paymentDetails as $paymentDetail) {
                        
                        $paymentDetailObj = $receiptPaymentRepository->find((int)$paymentDetail['f083fid']);
                        if($paymentDetailObj){
                            $receiptPaymentRepository->updatePayment($paymentDetailObj,$paymentDetail);
                        }
                        else{
                            $receiptPaymentRepository->createPayment($receipt,$paymentDetail);
                        }
                    }
                    foreach ($invoiceDetails as $invoiceDetail) {
                        $invoiceDetail['f084fdebitFundCode'] = $postData['f084fdebitFundCode'];
                        $invoiceDetail['f084fdebitAccountCode'] = $postData['f084fdebitAccountCode'];
                        $invoiceDetail['f084fdebitActivityCode'] = $postData['f084fdebitActivityCode'];
                        $invoiceDetail['f084fdebitDepartmentCode'] = $postData['f084fdebitDepartmentCode'];
                        $invoiceDetailObj = $receiptInvoiceRepository->find((int)$invoiceDetail['f084fid']);
                        if($invoiceDetailObj){
                            $receiptInvoiceRepository->updateInvoice($invoiceDetailObj,$invoiceDetail);
                        }
                        else{
                            $receiptInvoiceRepository->createInvoice($receipt,$invoiceDetail);
                        }
                    }

                    foreach ($nonInvoiceDetails as $nonInvoiceDetail) {
                        $nonInvoiceDetail['f084fdebitFundCode'] = $postData['f084fdebitFundCode'];
                        $nonInvoiceDetail['f084fdebitAccountCode'] = $postData['f084fdebitAccountCode'];
                        $nonInvoiceDetail['f084fdebitActivityCode'] = $postData['f084fdebitActivityCode'];
                        $nonInvoiceDetail['f084fdebitDepartmentCode'] = $postData['f084fdebitDepartmentCode'];
                        $nonInvoiceDetailObj = $receiptNonInvoiceRepository->find((int)$nonInvoiceDetail['f084fid']);
                        if($nonInvoiceDetailObj){
                            $receiptNonInvoiceRepository->updateNonInvoice($nonInvoiceDetailObj,$nonInvoiceDetail);
                        }
                        else{
                            $receiptNonInvoiceRepository->createNonInvoice($receipt,$nonInvoiceDetail);
                        }
                    }
                }
                else{
                $receipt = $receiptRepository->createNewData($postData);
                }
            } catch (\Exception $e) {
                echo $e;

                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'Receipt Already exist.',
                    ]);
                }
            }
            return new JsonModel([
                'status'    => 200,
                'message'   => 'Receipt Added successfully',
                'receiptId' => $receipt->getF082fid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $receiptRepository       = $this->getRepository('T073frequestInvoice');
        $receiptDetailRepository = $this->getRepository('T073frequestInvoice');
        $invoice                 = $invoiceRepository->find($id);

        if (!$invoice) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $invoiceObj     = $invoiceRepository->updateInvoice($invoice, $postData);
            $invoiceDetails = $postData['invoice-details'];
            foreach ($invoiceDetails as $invoiceDetail) {

                if (array_key_exists("f071fid", $invoiceDetail)) {

                    $invoiceDetailObj = $invoiceDetailRepository->find((int) $invoiceDetail['f072fid']);

                    // if(!$master)
                    // {
                    //     return $this->resourceNotFound();
                    // }
                    if ($invoiceDetailObj) {
                        $invoiceDetailRepository->updateInvoiceDetail($invoiceDetailObj, $invoiceDetail);
                    }
                } else {

                    $invoiceDetailRepository->createInvoiceDetail($invoiceObj, $invoiceDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
    
    public function receiptsAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        $result            = $receiptRepository->getReceipts((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
     public function vendorReceiptsAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        $result            = $receiptRepository->vendorReceipts((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function approveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $receiptRepository = $this->getRepository('T082freceipt');
        $journalRepository = $this->getRepository('T017fjournal');
        $receiptRepository->approve($postData,$journalRepository);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Receipts Approved successfully',
        ]);
    }

    public function nonApprovedReceiptsAction()
    {

        $request           = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        $result            = $receiptRepository->getNonApprovedReceipts();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function approveReceiptAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $receiptRepository = $this->getRepository('T082freceipt');
        $receiptRepository->approveReceipt($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Receipts Approved successfully',
        ]);
    }

    public function getreceiptStatusAction()
    {

        $rawBody           = file_get_contents("php://input");
        $receiptId   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $receiptRepository = $this->getRepository('T082freceipt');
        $result            = $receiptRepository->getreceiptStatus($receiptId);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function receiptByBankDateAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $receiptRepository = $this->getRepository('T082freceipt');
        $result = $receiptRepository->receiptByBankDate($postData);
        return new JsonModel([
            'status'  => 200,
            'result' => $result,
        ]);
    }
}
