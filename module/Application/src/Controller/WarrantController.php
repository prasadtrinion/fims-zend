<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class WarrantController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
     {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $warrantRepository = $this->getRepository('T143fwarrant');
        try
        {
        	$warrantDetails = $warrantRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $warrantDetails
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $warrantRepository = $this->getRepository('T143fwarrant');
        try
        {
                $warrant = $warrantRepository->find((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        if(!$warrant)
        {
            return $this->resourceNotFound();
        }
        
        $result = $warrantRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {
                

                $warrantRepository = $this->getRepository('T143fwarrant');
                $warrantDetailRepository = $this->getRepository('T144fwarrantDetails');
                if (array_key_exists("f143fid", $postData)) 
                    {
                    $warrantObj = $warrantRepository->find((int)$postData['f143fid']);
                        
                    $warrantObj =  $warrantRepository->updateMaster($warrantObj, $postData);
                    $details = $postData['details'];
            
                    	foreach ($details as $detail) 
                    	{
    
                        	if (array_key_exists("f144fid", $detail)) 
                        	{
    
                            	$detailObj = $warrantDetailRepository->find((int) $detail['f144fid']);
    
                            	if ($detailObj)
                            	{
                                	$warrantDetailRepository->updateDetail($detailObj, $detail);
                            	}
                        	}
                        	else
                        	{
                        		$warrantDetailRepository->createDetail($warrantObj, $detail);
                        	}
                    	}
                	}
            		else
            		{
                	try
                	{
                    	// print_r($postData);exit;
                    	$warrantObj = $warrantRepository->createNewData($postData);
                	}
                	catch (Exception $e)
                	{
                    	echo $e;
                	}
                
            	}
                
        	} 
              
               return new JsonModel([
                    'status' => 200,
                    'message' => 'Warrant Created successfully',
                    'warrantId' => $warrantObj->getF143fId()
                ]);
    }

    public function getWarrantActiveListAction()
     {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $warrantRepository = $this->getRepository('T143fwarrant');
        try
        {
        	$warrantDetails = $warrantRepository->getWarrantActiveList((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $warrantDetails
        ]);        
    }

    public function approveWarrantAction()
     {

       $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $warrantRepository = $this->getRepository('T143fwarrant');
        try
        {
        	$warrantDetails = $warrantRepository->approveWarrant($postData);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => 'Warrant Approved Successfully'
        ]);        
    }

    public function deleteWarrantDetailsAction()
     {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $warrantRepository = $this->getRepository('T143fwarrant');
        try
        {
            $warrantDetails = $warrantRepository->deleteWarrantDetails($postData);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => 'Warrant Detail Deleted Successfully'
        ]);        
    }

}