<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SellInvestmentController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }
    public function getSellApprovedListAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentRepository = $this->getRepository('T070fsellInvestment');
        
        $result = $investmentRepository->getSellListApproved();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function sellApproveOneAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
       
        $applicationRepository = $this->getRepository('T070fsellInvestment');
        $applicationIds        = $postData['id'];
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);
        }
        $application = $applicationRepository->approveOneApplication($applicationObjs);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Level1 Applications approved successfully',
        ]);

    }

    public function sellApproveTwoAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
       
        $applicationRepository = $this->getRepository('T070fsellInvestment');
        $applicationIds        = $postData['id'];
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);
        }
        $application = $applicationRepository->approveTwoApplication($applicationObjs);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Level2 Applications approved successfully',
        ]);

    }

    public function sellRejectOneAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);

        $applicationRepository = $this->getRepository('T070fsellInvestment');

        $applicationIds        = $postData['id'];
        $reason = $postData['reason'];
        
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);

        }
         
        $application = $applicationRepository->rejectOneReason($applicationObjs,$reason);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Applications Rejected by level1 Approver'
        ]);


    }

    public function sellRejectTwoAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);

        $applicationRepository = $this->getRepository('T070fsellInvestment');

        $applicationIds        = $postData['id'];
        $reason = $postData['reason'];
        
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);

        }
         
        $application = $applicationRepository->rejectTwoReason($applicationObjs,$reason);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Applications Rejected by level2 Approver'
        ]);


    }

    public function getSellApprovalOneListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T070fsellInvestment');
        
        $result = $applicationRepository->getListByApprovedOneStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getSellApprovalTwoListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T070fsellInvestment');
        
        $result = $applicationRepository->getListByApprovedTwoStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        $applicationRepository = $this->getRepository('T063finvestmentApplication');
        $applicationIds        = $postData['id'];
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }

        }
        if($this->isJSON($rawBody))
        {

            
            $investmentRepository = $this->getRepository('T070fsellInvestment');
        //     print_r($investmentRepository);
        // exit();
            try
            {
                $investmentObj = $investmentRepository->createInvestment($applicationIds);

            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Investment Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

}

?>