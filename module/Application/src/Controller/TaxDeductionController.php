<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class TaxDeductionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $TaxDeductionRepository = $this->getRepository('T082ftaxDeductionMaster');

        $result = $TaxDeductionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $TaxDeductionRepository = $this->getRepository('T082ftaxDeductionMaster');
        $asset = $TaxDeductionRepository->find((int)$id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $TaxDeductionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
 
    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {             
             try
            {
             $TaxRepository = $this->getRepository('T082ftaxDeductionMaster');
             $TaxDetailsRepository = $this->getRepository('T082ftaxDeductionDetail');
            if (array_key_exists('f082fid', $postData)) 
            {
            $taxObj = $TaxRepository->find((int)$postData['f082fid']);
                
            $taxObj =  $TaxRepository->updateMaster($taxObj, $postData);
        $details = $postData['tax-details'];
        
        foreach ($details as $detail) 
        {

            if (array_key_exists("f082fidDetails", $detail)) 
            {

                $detailObj = $TaxDetailsRepository->find((int) $detail['f082fidDetails']);

                if ($detailObj) {
                    $TaxDetailsRepository->updateDetail($detailObj, $detail);
                }
            } else {
 

                $TaxDetailsRepository->createDetail($taxObj, $detail);
            }
        }
    }
    else{
                $TaxDeducitonObj = $TaxRepository->createNewData($data);
            }

         }  
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }



    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $TaxRepository = $this->getRepository('T082ftaxDeductionMaster');
        $TaxDetailRepository = $this->getRepository('T082ftaxDeductionDetail');
        $Tax         =$TaxRepository->find($id);

        if (!$Tax) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $TaxObj     = $TaxRepository->updateData($Tax, $postData);
            $TaxDetails = $postData['taxDeduction-details'];
            // print_r($TaxObj);
            // exit();
            foreach ($TaxDetails as $TaxDetail) 
            {

                if (array_key_exists("f082fidDetails", $TaxDetail)) 
                {

                    $TaxDetailObj = $TaxDetailRepository->find((int) $TaxDetail['f082fidDetails']);


                    if ($TaxDetailObj) {

                        $TaxDetailRepository->updateTaxDetail($TaxDetailObj, $TaxDetail);
                    }
                } else {
                   
                    $TaxDetailRepository->createTaxDetail($TaxObj, $TaxDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
}