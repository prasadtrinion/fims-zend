<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PreOrderLevelSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $preOrderRepository = $this->getRepository('T098fpreOrderLevelSetup');
        $result = $preOrderRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $preOrderRepository = $this->getRepository('T098fpreOrderLevelSetup');
        $preOrder = $preOrderRepository->find((int)$id);
        if(!$preOrder)
        {
            return $this->resourceNotFound();
        }
        $result = $preOrderRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $preOrderRepository = $this->getRepository('T098fpreOrderLevelSetup');

            try
            {


                $stock = $preOrderRepository->findBy(array("f098fkodStock"=>strtolower($postData['f098fkodStock'])));
                // print_r($stock);exit;
                 if($stock)
                    {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                    }
                $preOrderObj = $preOrderRepository->createNewData($postData);
            }
            

           
            catch (\Exception $e)
            {
               return new JsonModel([
                            'status'  => 411,
                            'message' => 'Error While adding Data',
                        ]);
            }

            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $preOrderRepository = $this->getRepository('T098fpreOrderLevelSetup');
        $preOrder = $preOrderRepository->find((int)$id);

        if(!$preOrder)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $preOrderObj = $preOrderRepository->updateData($preOrder,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}