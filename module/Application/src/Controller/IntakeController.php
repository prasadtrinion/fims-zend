<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class IntakeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $intakeRepository = $this->getRepository('T067fintake');
        $result = $intakeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $intakeRepository = $this->getRepository('T067fintake');
        $intake = $intakeRepository->find($id);
        if(!$intake)
        {
            return $this->resourceNotFound();
        }
        $result = $intakeRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getIntakesAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $type = $this->params()->fromRoute('type');
       
        $intakeRepository = $this->getRepository('T067fintake');
        $result = $intakeRepository->getIntakes($type);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $intakeRepository = $this->getRepository('T067fintake');
            $intakeDetailRepository = $this->getRepository('T067fintakeDetails');

            if (array_key_exists("f067fid", $postData)) 
                {

                    $intakeObj = $intakeRepository->find((int)$postData['f067fid']);

                    if ($intakeObj)
                    {
                        // print_r($details);exit;
                        $intakeData = $intakeRepository->updateIntake($intakeObj, $postData);
                        $details = $postData['intake-details'];
                        foreach ($details as $detail)
                        {

                             if (array_key_exists("f067fidDetails", $detail))
                             {

                                $detailObj = $intakeDetailRepository->find((int)$detail['f067fidDetails']);
    
                                if ($detailObj)
                                {
                                    // print_r($detailObj);exit;
                                    $intakeDetailRepository->updateIntakeDetail($detailObj, $detail);
                                }
                            }
                            else
                            {  
                             // print_r($postData);exit;
                                $intakeDetailRepository->createIntakeDetail($intakeData, $detail);
                            }
                        }
                    }
                }
                else
                {

                    $intake = $intakeRepository->findBy(array("f067fname"=>strtolower($postData['f067fname'])));
                    if ($intake)
                    {
                        return new JsonModel([
                    'status' => 409,
                    'message' => 'Intake Already Exist',
                    ]);
                        
                    }
                    // print_r($postData);exit;
                    $intakeObj = $intakeRepository->createIntake($postData);
                }

                return new JsonModel([
                    'status' => 200,
                    'message' => 'Intake Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $intakeRepository       = $this->getRepository('T067fintake');
        $intakeDetailRepository = $this->getRepository('T067fintakeDetails');
        $intake                 = $intakeRepository->find($id);

        if (!$intake) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody))
        {

            $intakeObj     = $intakeRepository->updateIntake($intake, $postData);
            $intakeDetails = $postData['intake-details'];
            foreach ($intakeDetails as $intakeDetail) 
            {

                if (array_key_exists("f067fidDetails", $intakeDetail)) 
                {

                    $intakeDetailObj = $intakeDetailRepository->find((int) $intakeDetail['f067fidDetails']);

                    if ($intakeDetailObj) {
                        $intakeDetailRepository->updateIntakeDetail($intakeDetailObj, $intakeDetail);
                    }
                } else {

                    $intakeDetailRepository->createIntakeDetail($intakeObj, $intakeDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteIntakeDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $intakeDetailRepository = $this->getRepository('T067fintakeDetails');
        $result            = $intakeDetailRepository->deleteIntakeDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Intake Details Deleted successfully',
        ]);
    }

}