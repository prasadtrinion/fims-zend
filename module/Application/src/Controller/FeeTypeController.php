<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;


class FeeTypeController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;   
        parent::__construct($sm);
    }

    public function getList()
    {
        $request = $this->getRequest();
        $feeTypeRepository = $this->getRepository('T120ffeeType');
        $result = $feeTypeRepository->getList();
        return new JsonModel([
            'status'=> 200,
            'result'=>$result
            ]);
    }

    public function get($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $feeTypeRepository = $this->getRepository('T120ffeeType');
        $feeType = $feeTypeRepository->find((int)$id);
        if(!$feeType)
        {
            return $this->resourceNotFound();
        }
        $result = $feeTypeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $feeTypeRepository = $this->getRepository('T120ffeeType');

            try
            {
            	$fee = $feeTypeRepository->findBy(array("f120fcode"=>strtolower($postData['f120fcode'])));

            	if ($fee)
            	{
            		return new JsonModel([
                            'status' => 409,
                            'message' => 'feeType Already exist.'
                        ]);
            	}
                $feeTypeObj = $feeTypeRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 412,
                            'message' => 'error While Adding FeeType.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'feeType Added successfully',
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $feeTypeRepository = $this->getRepository('T120ffeeType');
        $feeType = $feeTypeRepository->find((int)$id);

        if(!$feeType)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $feeTypeObj = $feeTypeRepository->updateData($feeType,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }
    
    public function getFeeTypeByStatusAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $feeTypeRepository = $this->getRepository('T120ffeeType');
              
                
        $result            = $feeTypeRepository->getFeeTypeByStatus($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}

