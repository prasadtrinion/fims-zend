<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class BudgetSummaryController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function budgetSummaryAction()
    {

        $request = $this->getRequest();
         $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
        $budgetSummaryRepository = $this->getRepository('T108fbudgetSummary');
        $result = $budgetSummaryRepository->getList($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $budgetSummaryRepository = $this->getRepository('T108fbudgetSummary');
        $budgetSummary = $budgetSummaryRepository->find($id);
        if(!$budgetSummary)
        {
            return $this->resourceNotFound();
        }
        $result = $budgetSummaryRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getBalanceStatusAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        
       
        $budgetSummaryRepository = $this->getRepository('T108fbudgetSummary');
       
        $result = $budgetSummaryRepository->getBalanceStatus($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
     public function getBudgetGlcodeAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        
       
        $budgetSummaryRepository = $this->getRepository('T108fbudgetSummary');
       
        $result = $budgetSummaryRepository->getBudgetGlcode($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
  

}