<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class NotificationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $notificationRepository = $this->getRepository('T135fnotification');

        $result = $notificationRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $notificationRepository = $this->getRepository('T135fnotification');
        $notification = $notificationRepository->find((int)$id);
        if(!$notification)
        {
            return $this->resourceNotFound();
        }
        $result = $notificationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $notificationRepository = $this->getRepository('T135fnotification');
             $notificationEmailRepository = $this->getRepository('T136fnotificationEmail');
             
            try
            { 
                if (array_key_exists("f135fid", $data)) 
                {

                    $notificationObj = $notificationRepository->find((int)$data['f135fid']);

                    if ($notificationObj)
                    {
                    // print_r($notificationObj);exit;

                       $notificationUpdatedData =  $notificationRepository->updateData($notificationObj, $data);
                    }

                    $emailNotifications = $data['notification-email'];
                    foreach ($emailNotifications as $emailData)
                    {

                         if (array_key_exists("f136fid", $emailData)) 
                            {
                                $notificationEmailObj = $notificationEmailRepository->find((int)$emailData['f136fid']);
                    // print_r($notificationEmailObj);exit;
                                
                                if ($notificationEmailObj)
                                {
                                    // print_r($emailData);exit;
                                   $notificationEmailRepository->updateNotificationEmailData($notificationEmailObj, $emailData);
                                }
                               
                            }
                             else
                            {

                                   $notificationEmailRepository->createNotificationEmailData($notificationObj, $emailData);
                            }
                            
                    }

                } else
                {
                    // print_r($data);exit;
                // $category = $notificationRepository->findBy(array("f135frevenueName"=>strtolower($data['f135frevenueName'])));
                // if ($category)
                // {
                //     return new JsonModel([
                //             'status' => 409,
                //             'message' => 'Already exist.'
                //         ]);
                // }

                $notificationObj = $notificationRepository->createNewData($data);
                }
            }

           
            catch (Exception $e)
            {
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

	public function notificationActiveListAction()
    {
        $request = $this->getRequest();
        $notificationRepository = $this->getRepository('T135fnotification');
        $id = $this->params()->fromRoute('id');

        $list=$notificationRepository->notificationActiveList((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }
    

}