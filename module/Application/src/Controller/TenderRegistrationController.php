<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class TenderRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        $result = $tendorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        $tendor = $tendorRepository->find((int)$id);
        if(!$tendor)
        {
            return $this->resourceNotFound();
        }
        $result = $tendorRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            try
                {
        // foreach ($postData['f037fidVendor'] as $vendor) {

                $tendorRepository = $this->getRepository('T037ftenderRegMaster');
                // $tendorDetailRepository = $this->getRepository('T038ftenderRegDetails');
                // if (array_key_exists("f037fid", $postData)) 
                //     {
                //     $tendorObj = $tendorRepository->find((int)$postData['f037fid']);
                        
                //     $tendorObj =  $tendorRepository->updateMaster($tendorObj, $postData);
                // $details = $postData['details'];
            
                // foreach ($details as $detail) 
                // {
    
                //     if (array_key_exists("f038fid", $detail)) 
                //     {
    
                //         $detailObj = $tendorDetailRepository->find((int) $detail['f038fid']);
    
                //         if ($detailObj) {
                //             $tendorDetailRepository->updateDetail($detailObj, $detail);
                //         }
                //     } else {
    
                        // $tendorDetailRepository->createDetail($tendorObj, $detail);
                    // }
                // }
            // }
            // else{
                $tendorObj = $tendorRepository->createNewData($postData);
            // }
        // }
            }
           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'tendor Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    'Id' => $tendorObj->getF037fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function getTendorDetailsAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getTenderDetails((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

     public function shortlistedVendorsByTenderAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->shortlistedVendorsByTender((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function awardedVendorsByTenderAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->awardedVendorsByTender((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

     public function shortlistVendorsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
            $tendorRepository = $this->getRepository('T037ftenderRegMaster');

        $tendorRepository->shortlistVendors($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Vendors shortlisted successfully',
        ]);
    }

    public function awardVendorsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);

            $tendorRepository = $this->getRepository('T037ftenderRegMaster');
            $tendorDetailsRepository = $this->getRepository('T038ftenderRegDetails');
            foreach ($postData['id'] as $row) 
            {
            if($row['statuscheckbox']==true)
            {
                $details = $row['details'];
                foreach ($details as $detail) 
                {
                    $tenderDetailObj = $tendorDetailsRepository->find((int)$detail['f038fid']);
                    $tendorDetailsRepository->updateDetail($tenderDetailObj,$detail);
                    $tendorRepository->awardVendors($row['f037fid']);
                     
                }
            }
        }
        return new JsonModel([
            'status'  => 200,
            'message' => 'Vendors awarded successfully',
        ]);
    }


    public function finalAwardTenderAction()
    {
        $request = $this->getRequest();
        
        $tendorRepository = $this->getRepository('T037ftenderRegMaster');
        
        $list=$tendorRepository->getFinalAwardTender();
        
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }


}
