<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ApplyLoanController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $applyLoanRepository = $this->getRepository('T045fapplyLoan');

        $result = $applyLoanRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $applyLoanRepository = $this->getRepository('T045fapplyLoan');
        $applyLoan = $applyLoanRepository->find((int)$id);
        if(!$applyLoan)
        {
            return $this->resourceNotFound();
        }
        $result = $applyLoanRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $applyLoanRepository = $this->getRepository('T045fapplyLoan');
             
            try
            {
                $applyObj = $applyLoanRepository->createLoan($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $applyLoanRepository = $this->getRepository('T045fapplyLoan');
        $applyLoanDetailRepository = $this->getRepository('T045fapplyLoanDetails');
        $applyLoan         =$applyLoanRepository->find($id);

        if (!$applyLoan) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $applyLoanObj     = $applyLoanRepository->updateData($applyLoan, $postData);
            $applyLoanDetails = $postData['disposal-details'];
            // print_r($incrementObj);
            // exit();
            foreach ($applyLoanDetails as $applyLoanDetail) 
            {

                if (array_key_exists("f045fidDetails", $applyLoanDetail)) 
                {

                    $applyLoanDetailObj = $applyLoanDetailRepository->find((int) $applyLoanDetail['f086fidDetails']);

                    if ($applyLoanDetailObj) {
                        $applyLoanDetailRepository->updateLoanDetail($applyLoanDetailObj, $applyLoanDetail);
                    }
                } else {

                    $applyLoanDetailRepository->createLoanDetail($applyLoanObj, $applyLoanDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function loanApplicationApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $applyLoanRepository = $this->getRepository('T045fapplyLoan');
        $applyLoan = $postData['id'];
        $applyLoanObjs=array();
        foreach($applyLoan as $id)
        {
            $applyLoanObj =$applyLoanRepository->find((int)$id);
            if(!$applyLoanObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Loan not Applied.'
                ]);
            }
            array_push($applyLoanObjs, $applyLoanObj);
        }
        $applyLoan = $applyLoanRepository->loanApproval($applyLoanObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

     public function getLoanGradesAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $gradeRepository = $this->getRepository('T046fgradeSetup');
        $result = $gradeRepository->getLoanGrades($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getLoanApprovedListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applyLoanRepository = $this->getRepository('T045fapplyLoan');
        
        $result = $applyLoanRepository->getLoanApprovedList($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    

}