<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class OpeningBalanceController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
       
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $openingbalanceRepository = $this->getRepository('T016fopeningbalance');
	$financialRepository = $this->getRepository('T015ffinancialyear');
        $result = $openingbalanceRepository->getList();
        for($i=0;$i<count($result);$i++){
	$obj = $financialRepository->find((int)$result[$i]['f016fidFinancialyear']);
	$result[$i]['financialyear'] = $obj->getF015fname();
}

	return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    } 
    

    public function get($id)
    {
        $id = (int)$id;
       $request = $this->getRequest();
        try
        {

            $openingBalanceRepository = $this->getRepository('T016fopeningbalance');
            $openingBalance = $openingBalanceRepository->findBy(array("f016fidFinancialyear"=>(int)$id));
        }
        catch(\Exception $e)
        {
            $e->getErrorCode();

        }
        if(!$openingBalance)

                {
                    return new JsonModel([
                    'status' => 404,
                    'Message' => "Resource Not Found"
                        ]);
                }
                     
                
            
        $result = $openingBalanceRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

     public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
// print_r($postData);
// die();
        if($this->isJSON($rawBody))
        {

            $openingBalanceRepository = $this->getRepository('T016fopeningbalance');
            
                $glcodes = $postData['glcode'];
                foreach ($glcodes as $glcode) {
                if(array_key_exists("f016fid",$glcode))
                {

                    $openingBalance = $openingBalanceRepository->find((int)$glcode['f016fid']);
                
                    // if(!$master)
                    // {
                    //     return $this->resourceNotFound();
                    // }
                    if($openingBalance)
                    {
                    $openingBalanceObject = $openingBalanceRepository->updateData($openingBalance,(int)$glcode);
                    }
                }   
                else{
                 
                    $openingBalanceObject = $openingBalanceRepository->createNewData($postData['f016fidFinancialyear'],(int)$glcode);
                }
                }
                return new JsonModel([
                            'status' => 200,
                            'message' => 'Opening Balance updated.'
                        ]);
                

        }
        else
        {
            return $this->invalidInputType();
        }
    }

     public function update($id,$postData)
    {
       
      
    }
    public function generateNumberAction(){
       
         $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         
         $initialRepository = $this->getRepository('T011finitialConfig');
         $number = $initialRepository->generate($postData);
         return new JsonModel([
                'number' => $number
            ]);
    }

    public function getOpeningbalanceApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');

        $em                       = $this->getEntityManager();
        $request                  = $this->getRequest();
        $openingbalanceRepository = $this->getRepository('T016fopeningbalance');

        
        $result = $openingbalanceRepository->getListByApprovedStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function openingBalanceApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        
        $openingBalanceRepository = $this->getRepository('T016fopeningbalance');

        $openingBalance = $postData['id'];
        $status = $postData['status'];

        $openingBalanceObjs=array();

        foreach($openingBalance as $id)
        {
            
            $openingBalanceObj = $openingBalanceRepository->findBy(array('f016fidFinancialyear'=>(int)$id));

           

            if(!$openingBalanceObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' opening balance does not exist.'
                ]);
            }
            foreach($openingBalanceObj as $obj){
                array_push($openingBalanceObjs, $obj);
            }
        }

        $openingBalance = $openingBalanceRepository->openingBalanceApprovalStatus($openingBalanceObjs,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }
}
