<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class SponsorDnController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $sponsorDnRepository = $this->getRepository('T063fsponsorDn');
       
        
        try{
                $sponsorDn = $sponsorDnRepository->find((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        if(!$sponsorDn)
        {
            return $this->resourceNotFound();
        }
        
        $result = $sponsorDnRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $sponsorDnRepository = $this->getRepository('T063fsponsorDn');
        try{
        $sponsorDnDetails = $sponsorDnRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => $sponsorDnDetails
        ]);        
    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");
        if($this->isJSON($rawBody))
        {
            
        // try{
        //     $invoiceRepository = $this->getRepository('T071finvoice');

        //         $invoiceObj = $invoiceRepository->find((int)$postData['f063fidInvoice']);

        // }
        // catch(\Exception $e){
        //     echo $e;
        // }

        // $invoiceAmount = $invoiceObj->getF071fbalance();

        // $balance = $invoiceAmount - $totalAmount;
        
        // $invoiceObj->setF071fbalance((float)$balance);

        // try{
        // $em->persist($invoiceObj);
        //         $em->flush();
        //     }
        // catch(\Exception $e){
        //     echo $e;
        // }                
            try
            {

                $sponsorDnRepository = $this->getRepository('T063fsponsorDn');
                $sponsorDnDetailRepository = $this->getRepository('T064fsponsorDnDetails');
                if (array_key_exists("f063fid", $postData)) 
                {
                    $sponsorDnObj = $sponsorDnRepository->find((int)$postData['f063fid']);
                        
                    $sponsorDnObj =  $sponsorDnRepository->updateMaster($sponsorDnObj, $postData);
                    $details = $postData['details'];
            
                    foreach ($details as $detail) 
                    {
    
                    if (array_key_exists("f064fid", $detail)) 
                        {
    
                        $detailObj = $sponsorDnDetailRepository->find((int) $detail['f064fid']);
    
                         if ($detailObj)
                            {
                                $sponsorDnDetailRepository->updateDetail($detailObj, $detail);
                            }
                        }
                        else
                        {
                            $sponsorDnDetailRepository->createDetail($sponsorDnObj, $detail);
                        }
                    }
                }
                else
                {
                  
                    $sponsorDn = $sponsorDnRepository->findBy(array("f063freferenceNumber"=>$postData['f063freferenceNumber']));
                    if ($sponsorDn)
                {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'SponsorDn Already exist.',
                    ]);
                }
                    $sponsorDnObj = $sponsorDnRepository->createNewData($postData);
                }
                
            } 
              
            catch (\Exception $e)
            {

                if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'SponsorDn Already exist.'
                        ]);
            }
        }
                return new JsonModel([
                    'status' => 200,
                    'message' => 'SponsorDn Created successfully',
                    // 'sponsorDnId' => $sponsorDnObj->getF063fId()
                ]);

        }
        else
        {
                return $this->invalidInputType();
        }
       
    }
    
    public function approvedsponsorDnsAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status = $this->params()->fromRoute('status');
        $sponsorDnRepository = $this->getRepository('T063fsponsorDn');
       
        
        $result = $sponsorDnRepository->approvedsponserDns((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }
    public function approveAction(){

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $sponsorDnRepository = $this->getRepository('T063fsponsorDn');
         $journalRepository = $this->getRepository('T017fjournal');         
         $sponsorDnRepository->approve($postData,$journalRepository);
         return new JsonModel([
                'status' => 200,
                'message' => 'SponsorDn Approved successfully'
            ]);
    }

    public function getSponsorDNApprovalsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $sponsorDnRepository = $this->getRepository('T063fsponsorDn');


        $result = $sponsorDnRepository->getsponsorDnApproval((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);
    }


}