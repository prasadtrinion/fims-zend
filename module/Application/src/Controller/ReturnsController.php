<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ReturnsController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()  
    {
         
        $request = $this->getRequest();
        $returnsRepository = $this->getRepository('T118freturns');
        $result = $returnsRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $returnsRepository = $this->getRepository('T118freturns');
        $returns = $returnsRepository->find((int)$id);
        if(!$returns)
        {
            return $this->resourceNotFound();
        }
        $result = $returnsRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $returnsRepository = $this->getRepository('T118freturns');

            try
            {
                $returnsObj = $returnsRepository->createNewData($postData);
            }

            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $returnsRepository = $this->getRepository('T118freturns');
        $returns = $returnsRepository->find((int)$id);

        if(!$returns)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $returnsObj = $returnsRepository->updateData($returns,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }

    public function getReturnsApprovedListAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('approvedStatus');
        $returnsRepository       = $this->getRepository('T118freturns');

        $result = $returnsRepository->returnsByApproved((int)$id);
        return new JsonModel([
            'status'  => 200,
            'result' => $result
        ]);
    }
    public function approveReturnsAction()
    {
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $returnsRepository       = $this->getRepository('T118freturns');

        $result = $returnsRepository->approveReturns($postData);
        return new JsonModel([
                'status' => 200,
                'message' => 'Returns Approved successfully'
            ]);
    }
}