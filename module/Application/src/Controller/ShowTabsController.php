<?php

namespace Application\Controller;

use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class ShowTabsController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }
    public function get($id)
    {
        $group = null;
        $showTabs = null;
        $showTabsRepository = null;
        $groupRepository = null;
        $showTabsObj = null;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $showTabsRepository = $this->getRepository('T045fshowTabs');
        $showTabs = $showTabsRepository->findBy(array( 'f045fgroupId' => $id));        
    
        if(!$showTabs[0])
         {
            $groupRepository = $this->getRepository('T012fgroup');
            $group = $groupRepository->find($id);
            if(!$group)
            {
                    return $this->resourceNotFound();

            }
            while($group->getF012fidParent()!=0)
            {
                $groupId = $group->getF012fidParent(); 
                $showTabs = $showTabsRepository->findBy(array( 'f045fgroupId' => $groupId )); 
                if(!$showTabs[0])
                {
                        $group = $groupRepository->find($groupId);
                }
                else{
                    break;
                }       
                
        
              }
            
            //print($showTabs[0]->getF045fid());
            //die();
            if($group->getF012fidParent()==0)
            {
                    return $this->resourceNotFound();

            }      
        
        }
        
        $result = $showTabsRepository->getListById($showTabs[0]->getF045fgroupId());
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);

    }
    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $showTabsRepository = $this->getRepository('T045fshowTabs');
        $result = $showTabsRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);        
    }
    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){  
             
                /*foreach ($postData as $key => $value) {
                    
                    if($value == ''){
                        return new JsonModel([
                            'status' => 199,
                            'message' => "Please provide the ".$key
                        ]);
                    }
                }*/
                 try{
                
                $groupRepository = $this->getRepository('T012fgroup');
                $groupObj = $groupRepository->find($postData['f045fgroup_id']);
               // print_r($groupObj);
                //die();
                if(!$groupObj)
                    {
                        return new JsonModel([
                            'status' => 410,
                            'message' => 'Group doesnt exist.'
                        ]);
                    }
                }
                catch(\Exception $e){
                    echo $e;
                }
                try
                {

                    $showTabsRepository = $this->getRepository('T045fshowTabs');
                   
                    $showTabsObj = $showTabsRepository->createShowTab($postData,$groupObj);
                    
                } 
                catch (\Exception $e) {

                    if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Show Tab Already exist.'
                        ]);
                    }
                }
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Show Tab Created successfully',
                    'showTabObjId' => $showTabsObj->getF045fid()
                ]);

            }else{
                return $this->invalidInputType();
            }
       
    }

}