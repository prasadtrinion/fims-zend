<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractAppController
{
    public function __construct(
        ServiceManager $sm
    ){
        parent::__construct($sm);
    }
    
    public function indexAction()
    {
        return $this->redirect()->toRoute('login');
    }
    
    public function faqAction()
    {
        return new ViewModel();
    }
}
