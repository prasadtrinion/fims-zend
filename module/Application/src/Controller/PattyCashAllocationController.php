<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PattyCashAllocationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()  
    {
         
        $request = $this->getRequest();
        $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');
        $result = $pattyCashRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');
        $pattyCash = $pattyCashRepository->find((int)$id);
        if(!$pattyCash)
        {
            return $this->resourceNotFound();
        }
        $result = $pattyCashRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)

    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');

            try
            {
                $pattyCashObj = $pattyCashRepository->createNewData($postData);
            }

            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');
        $pattyCash = $pattyCashRepository->find((int)$id);

        if(!$pattyCash)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $pattyCashObj = $pattyCashRepository->updateData($pattyCash,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
    
    public function getPendingPettyCashAllocationAction()
    {
        
        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);
       
        $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');
        $result = $pattyCashRepository->getApprovedData($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function approvePettyCashAllocationAction()
    {
        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);

       
        $pattyCashRepository = $this->getRepository('T122fpattyCashAllocation');
        $result = $pattyCashRepository->approvePettyCashAllocation($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'updated Successfully',
        ]);
    }
}