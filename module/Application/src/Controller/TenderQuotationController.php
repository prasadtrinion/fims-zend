<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class TenderQuotationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {
        $request          = $this->getRequest();
        $tenderRepository = $this->getRepository('T035ftenderQuotation');
        $result           = $tenderRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $tenderRepository = $this->getRepository('T035ftenderQuotation');
        $tender           = $tenderRepository->find((int) $id);
        if (!$tender) {
            return $this->resourceNotFound();
        }
        $result = $tenderRepository->getListById((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            try
            {
                $tenderRepository   = $this->getRepository('T035ftenderQuotation');
                $specRepository     = $this->getRepository('T038ftenderSpec');
                $commiteeRepository = $this->getRepository('T036ftenderCommitee');
                $initialRepository = $this->getRepository('T011finitialConfig');

                $data=array();

                if($data['type']='TND')
                {
                    $generateNumber = $initialRepository->generateFIMS($data);
                    $postData['f035fquotationNumber'] = $generateNumber;
                }
                elseif($data['type']='QN')
                {
                    $generateNumber = $initialRepository->generateFIMS($data);
                    $postData['f035fquotationNumber'] = $generateNumber;
                }
                

                if (array_key_exists("f035fid", $postData))
                {

                    $tenderObj = $tenderRepository->find((int) $postData['f035fid']);

                    $tenderObj = $tenderRepository->updateData($tenderObj, $postData);

                    $specDetails     = $postData['spec-details'];

                    $commiteeDetails = $postData['commitee-details'];


                    foreach ($specDetails as $detail)
                    {

                        if (array_key_exists("f038fid", $detail))
                        {

                            $specObj = $specRepository->find((int) $detail['f038fid']);
                            if ($specObj)
                            {
                                $specRepository->updateSpec($specObj, $detail);
                            }
                        }
                        else
                        {

                            $specRepository->createSpec($tenderObj, $detail);
                        }
                    }

                    foreach ($commiteeDetails as $detail)
                    {

                        if (array_key_exists("f036fid", $detail))
                        {

                            $commiteeObj = $commiteeRepository->find((int) $detail['f036fid']);

                            if ($commiteeObj)
                            {
                                $commiteeRepository->updateCommitee($commiteeObj, $detail);
                            }
                        } else
                        {

                            $commiteeRepository->createCommitee($tenderObj, $detail);
                        }
                    }

                }
                else
                {
                    $tenderObj = $tenderRepository->createNewData($postData);
                }

            } catch (\Exception $e) {
                echo $e;
                // if ($e->getErrorCode() == 1062) {
                //     return new JsonModel([
                //         'status'  => 200,
                //         'message' => 'Tender Info Already exist.',
                //     ]);
                // }
            }
            return new JsonModel([
                'status'   => 200,
                'message'  => 'Tender Info Added successfully',
                'tenderId' => $tenderObj->getF035fid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function getListBasedOnTendorAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T035ftenderQuotation');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getListBasedOnTendor((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function deleteTenderSpecAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $TenderSpecRepository = $this->getRepository('T038ftenderSpec');
        $result            = $TenderSpecRepository->deleteTenderSpec($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Tender Specification Data Deleted successfully',
        ]);
    }

    public function deleteTenderComiteeAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $TenderComiteeRepository = $this->getRepository('T051ftenderCommitee');
        $result            = $TenderComiteeRepository->deleteTenderComitee($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Tender Commitee Data Deleted successfully',
        ]);
    }

    public function getTenderSubmissionAction()
    {
        $request = $this->getRequest();
        $tendorRepository = $this->getRepository('T035ftenderQuotation');
        $id = $this->params()->fromRoute('status');

        $list=$tendorRepository->getTenderSubmission((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function getTenderShortlistedAction()
    {
        $request = $this->getRequest();
        $tendorRepository = $this->getRepository('T035ftenderQuotation');
        $id = $this->params()->fromRoute('status');

        $list=$tendorRepository->getTenderShortlisted((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function getTenderAwardedAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T035ftenderQuotation');
        $id = $this->params()->fromRoute('status');

        $list=$tendorRepository->getTenderAwarded((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }


}


