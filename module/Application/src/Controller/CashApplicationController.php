<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CashApplicationController extends AbstractAppController
{

    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {


        $request = $this->getRequest();
       
        $cashRepository = $this->getRepository('T052fcashApplication');

        
      
        $result = $cashRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $cashRepository = $this->getRepository('T052fcashApplication');
        $cash = $cashRepository->find((int)$id);
        if(!$cash)
        {
            return $this->resourceNotFound();
        }
        $result = $cashRepository->getListById((int)$id);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }


    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {


            $idStaff = $postData['f052fidStaff'];
            foreach ($postData['cash-details'] as $data)
            {
                $cashId = $data['f052fidCashType'];
                $amount = $data['f052ftotalAmount'];

                    $cashRepository = $this->getRepository('T052fcashApplication');

                    $getStaffAmount = $cashRepository->staffConditionCheck($idStaff,$cashId);
               
                    $staffAmount = $getStaffAmount[0]['f119famount'];
                     
            
                      // echo $staffAmount.'/n';
                      // echo $amount;
                      // die();
                        if ($amount >= $staffAmount)
                        {
                             return new JsonModel([
                                'status'  => 411,
                                'message' => 'Amount Exceed',
                               ]);
                        }
               
            }


            $data=array();
            $data['type']='Cash';
            $initialRepository = $this->getRepository('T011finitialConfig');
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f052fcashAdvance'] = $generateNumber;

            
            try
            {
                $cashObj =$cashRepository->createCash($postData);
            }
            catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $cashRepository = $this->getRepository('T052fcashApplication');
        $cashDetailRepository = $this->getRepository('T052fcashApplicationDetails');
        $cash = $cashRepository->find($id);

        if(!$cash) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody))
        {

            $idStaff = $postData['f052fidStaff'];
            foreach ($postData['cash-details'] as $data)
            {
                $cashId = $data['f052fidCashType'];
                $amount = $data['f052ftotalAmount'];

                    $cashRepository = $this->getRepository('T052fcashApplication');

                    $getStaffAmount = $cashRepository->staffConditionCheck($idStaff,$cashId);
               
                    $staffAmount = $getStaffAmount[0]['f119famount'];
                     
            
                      // echo $staffAmount.'/n';
                      // echo $amount;
                      // die();
                        if ($amount >= $staffAmount)
                        {
                             return new JsonModel([
                                'status'  => 411,
                                'message' => 'Amount Exceed',
                               ]);
                        }
               
            }

            $cashObj     = $cashRepository->updateCash($cash, $postData);
            $cashDetails = $postData['cash-details'];
            
            foreach ($cashDetails as $cashDetail) 
            {

                if (array_key_exists("f052fidDetails", $cashDetail)) 
                {

                    $cashDetailObj = $cashDetailRepository->find((int) $cashDetail['f052fidDetails']);

                    if ($cashDetailObj) 
                    {
                        $cashDetailRepository->updateCashDetail($cashDetailObj, $cashDetail);
                    }
                } else {

                    $cashDetailRepository->createCashDetail($cashObj, $cashDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function cashApplicationApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $cashRepository = $this->getRepository('T052fcashApplication');
        $cash = $postData['id'];
        $cashObjs=array();
        foreach($cash as $id)
        {
            $cashObj =$cashRepository->find((int)$id);
            if(!$cashObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Application does not exist.'
                ]);
            }
            array_push($cashObjs, $cashObj);
        }
        $cashApplication = $cashRepository->cashApplicationApproval($cashObjs);



        // $bill = $cashRepository->createBill($cashApplication);

        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getCashApplicationApprovedListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashRepository = $this->getRepository('T052fcashApplication');
        
        $result = $cashRepository->getListByApprovedStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getCashApplicationStaffListAction()
    {

        $id = $this->params()->fromRoute('id');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashRepository = $this->getRepository('T052fcashApplication');
        
        $result = $cashRepository->getCashApplicationStaffList((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getCashApplicationNonProcessedListAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashRepository = $this->getRepository('T052fcashApplication');
        
        $result = $cashRepository->getNonProcessedList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getCashApplicationProcessedListAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashRepository = $this->getRepository('T052fcashApplication');
        
        $result = $cashRepository->getProcessedList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function cashApplicationProcessAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $cashRepository = $this->getRepository('T052fcashApplication');
        $cash = $postData['id'];
        $cashObjs=array();
        foreach($cash as $id)
        {
            $cashObj =$cashRepository->find((int)$id);
            if(!$cashObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Application does not exist.'
                ]);
            }
            array_push($cashObjs, $cashObj);
        }
        $cashApplication = $cashRepository->cashApplicationProcess($cashObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'Application Processed'
        ]);
    }

    public function getStaffCashAdvancesAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=3067;
        $cashRepository = $this->getRepository('T052fcashApplication');
        
        $result = $cashRepository->getStaffCashAdvances($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function cashApplicationRejectAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);

        $cashRepository = $this->getRepository('T052fcashApplication');

        $applicationIds        = $postData['id'];
        $reason = $postData['reason'];

        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            
            $applicationObj = $cashRepository->find((int) $id);

            if (!$applicationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);

        }
         
        $application = $cashRepository->rejectReason($applicationObjs,$reason);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Applications Rejected'
        ]);
    }

    public function deleteCashApplicationDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $cashDetailRepository = $this->getRepository('T052fcashApplicationDetails');
        $result            = $cashDetailRepository->deleteCashApplicationDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Cash Application Details Deleted successfully',
        ]);
    }
}
