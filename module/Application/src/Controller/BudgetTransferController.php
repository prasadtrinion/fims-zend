<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;


class BudgetTransferController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
    	// print_r("expression");exit;

        $em = $this->getEntityManager();

        $request = $this->getRequest();
        $budgetTransferRepository = $this->getRepository('T141fbudgetTransfer');
        
        $result = $budgetTransferRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $budgetTransferRepository = $this->getRepository('T141fbudgetTransfer');
        $budgetTransfer = $budgetTransferRepository ->find($id);
        if(!$budgetTransfer)
        {
            return $this->resourceNotFound();
        }
        $result = $budgetTransferRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        $query = "truncate table t141fbudget_transfer";
        $em->getConnection()->executeQuery($query);
        // $query = "sp_chgattribute t141fbudget_transfer, 'identity_burn_max', 0, '0'";
        // $em->getConnection()->executeQuery($query);
        
        if($this->isJSON($rawBody))
        {
            

             try
            {

                $budgetTransferRepository = $this->getRepository('T141fbudgetTransfer'); 
                $datas = $postData['funds'];

                // print_r($datas[0][0]);exit;

                for ($i = 0; $i<count($datas); $i++)
                {
                 
                 // print_r($datas[$i]);exit;
                    $data = $datas[$i];
                    for ($j=0; $j <count($data) ; $j++)
                    { 
                        // print_r($data[$j]);exit;

                        $budgetDatas = $data[$j];

                        // $budgetData['f141fid'] = $budgetData['id'];
                        $budgetData['f141fstatus'] = $budgetDatas['f141fstatus'];
                        $budgetData['f141ffromFund'] = $budgetDatas['fromFund'];
                        $budgetData['f141ftoFund'] = $budgetDatas['toFund'];
                        // print_r($budgetData);exit;
                    
                
                        if (array_key_exists("f141fid", $budgetData)) 
                        {
                            $budgetTransferObj = $budgetTransferRepository->find((int)$budgetData['f141fid']);
                            if (!$budgetTransferObj)
                            {
                    	       return $this->resourceNotFound();
                            }
                        
                            $budgetTransferObj = $budgetTransferRepository->updatebudgetTransfer($budgetTransferObj, $budgetData);
                    
                        }
                        else
                        {

                            $budgetTransferObj = $budgetTransferRepository->createbudgetTransfer($budgetData);
                        }
                    }
                }
            }
            catch (Exception $e)
            {
                echo $e;
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }
    public function getFromTransferFundsAction()
    {

        $fund = $this->params()->fromRoute('fund');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $budgetTransferRepository = $this->getRepository('T141fbudgetTransfer');
        
        $result = $budgetTransferRepository->getFromTransferFunds($fund);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function getToTransferFundsAction()
    {

        $fund = $this->params()->fromRoute('fund');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $budgetTransferRepository = $this->getRepository('T141fbudgetTransfer');
        
        $result = $budgetTransferRepository->getToTransferFunds($fund);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}