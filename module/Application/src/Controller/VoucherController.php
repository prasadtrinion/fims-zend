<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class VoucherController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T071fvoucher');
        $result            = $voucherRepository->getVoucherList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em             = $this->getEntityManager();
        $request        = $this->getRequest();

        $voucherRepository = $this->getRepository('T071fvoucher');
            $voucher = $voucherRepository->find((int) $id);

            if (!$voucher) {
                return $this->resourceNotFound();
            }
        $result = $voucherRepository->getVoucherById((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function approvedVouchersAction()
    {

        $em             = $this->getEntityManager();
        $request        = $this->getRequest();
        $approvedStatus = $this->params()->fromRoute('approvedStatus');

        $voucherRepository = $this->getRepository('T071fvoucher');
        if (!isset($approvedStatus)) {

            $voucher = $voucherRepository->find((int) $id);

            if (!$voucher) {
                return $this->resourceNotFound();
            }
        }
        $result = $voucherRepository->getVoucherById(1,(int) $approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $voucherRepository = $this->getRepository('T071fvoucher');

            try
            {
                $voucherObj = $voucherRepository->createNewVoucher($postData);
            } catch (\Exception $e) {
                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'Voucher Already exist.',
                    ]);
                }
            }
            return new JsonModel([
                'status'    => 200,
                'message'   => 'Voucher Added successfully',
                'voucherId' => $voucherObj->getF071fid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $voucherRepository       = $this->getRepository('T071fvoucher');
        $voucherDetailRepository = $this->getRepository('T072fvoucherDetails');
        $voucher                 = $voucherRepository->find($id);

        if (!$voucher) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $voucherObj     = $voucherRepository->updateVoucher($voucher, $postData);
            $voucherDetails = $postData['voucher-details'];
            foreach ($voucherDetails as $voucherDetail) {

                if (array_key_exists("f071fid", $voucherDetail)) {

                    $voucherDetailObj = $voucherDetailRepository->find((int) $voucherDetail['f072fid']);

                    // if(!$master)
                    // {
                    //     return $this->resourceNotFound();
                    // }
                    if ($voucherDetailObj) {
                        $voucherDetailRepository->updateVoucherDetail($voucherDetailObj, $voucherDetail);
                    }
                } else {

                    $voucherDetailRepository->createVoucherDetail($voucherObj, $voucherDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
    public function approveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $voucherRepository = $this->getRepository('T071fvoucher');
        $journalRepository = $this->getRepository('T017fjournal');
        $voucherRepository->voucherApprove($postData,$journalRepository);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Vouchers Approved successfully',
        ]);
    }

   

    public function customerVouchersAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T071fvoucher');
        $result            = $voucherRepository->getCustomerVouchers((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    

}
