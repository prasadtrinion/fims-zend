<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ApplyStockController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $applyLoanRepository = $this->getRepository('T099fapplyStock');

        $result = $applyLoanRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $applyLoanRepository = $this->getRepository('T099fapplyStock');
        $applyLoan = $applyLoanRepository->find((int)$id);
        if(!$applyLoan)
        {
            return $this->resourceNotFound();
        }
        $result = $applyLoanRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $applyLoanRepository = $this->getRepository('T099fapplyStock');
             
            try
            {
                $applyObj = $applyLoanRepository->createStock($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true); 

        $applyStockRepository = $this->getRepository('T099fapplyStock');
        $applyStockDetailRepository = $this->getRepository('T099fapplyStockDetails');
        $applyStock         =$applyStockRepository->find($id);


        if (!$applyStock) {
            return $this->resourceNotFound();
        }


        //$rawBody = file_get_contents("php://input");
        //$postData = json_decode($rawBody,true); 

        if ($this->isJSON($rawBody)) {

            $applyStockObj     = $applyStockRepository->updateStock($applyStock, $postData);
            $applyStockDetails = $postData['stock-details'];
            
            foreach ($applyStockDetails as $applyStockDetail) 
            {

                if (array_key_exists("f099fidDetails", $applyStockDetail)) 
                {

                    $applyStockDetailObj = $applyStockDetailRepository->find((int) $applyStockDetail['f099fidDetails']);

                    if ($applyStockDetailObj) {
                        $applyStockDetailRepository->updateStockDetail($applyStockDetailObj, $applyStockDetail);
                    }
                } else {

                    $applyStockDetailRepository->createStockDetail($applyStock, $applyStockDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function ApplyStockApprovalAction()
    {

       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $applyStockRepository = $this->getRepository('T099fapplyStock');
        $applyStock = $postData['id'];
        $status = $postData['status'];
        $reason = $postData['reason'];
        $stockObjs=array();

        foreach($applyStock as $id)
        {
           
            $stockObj =$applyStockRepository->find((int)$id);
           
            if(!$stockObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Stock does not exist.'
                ]);
            }
            array_push($stockObjs, $stockObj);
           
        }
        $stock = $applyStockRepository->applyStockApproval($stockObjs,$status,$reason);
        return new JsonModel([
            'status' => 200,
            'message' => 'Approved successfully'
        ]);
    }

    public function getApplyStockApprovedListAction()
    {

      
        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applyStockRepository = $this->getRepository('T099fapplyStock');
        
        $result = $applyStockRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function stockDistributionAction()
    {

      
        $Id = $this->params()->fromRoute('id');
        $id=(int)$Id;
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applyStockRepository = $this->getRepository('T099fapplyStock');
        
        $result = $applyStockRepository->stockDistribution($id);
        return new JsonModel([
            'status' => 200,
            'message' => 'Stock Distributed'
        ]);
    }

    public function deleteApplyStockDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $applyStockRepository = $this->getRepository('T099fapplyStock');
        $result            = $applyStockRepository->deleteApplyStockDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Apply Stock Data Deleted successfully',
        ]);
    }
    

}
