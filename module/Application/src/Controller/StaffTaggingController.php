<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StaffTaggingController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $staffTaggingRepository = $this->getRepository('T043fstaffTagging');
        $result = $staffTaggingRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $staffTaggingRepository = $this->getRepository('T043fstaffTagging');
        $staffTagging = $staffTaggingRepository->find((int)$id);
        if(!$staffTagging)
        {
            return $this->resourceNotFound();
        }
        $result = $staffTaggingRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        // print_r($postData);
        // exit();

        
        if($this->isJSON($rawBody))
        {
            $staffTaggingRepository = $this->getRepository('T043fstaffTagging');
            
            try
            {
                // print_r("hi");
                // exit();
                $staffTaggingObj = $staffTaggingRepository->createNewData($postData);
                
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Tagging Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Staff Tagging Added successfully',
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }


    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $staffTagging = $this->getRepository('T043fstaffTagging');
        $staff = $staffTagging->find((int)$id);

        if(!$staff)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $staffTaggingObj = $staffTagging->updateData($staff,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }


    public function approvestaffTaggingAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $staffTaggingRepository       = $this->getRepository('T043fstaffTagging');

        $staffTaggings = $staffTaggingRepository->staffTaggingApproval($postData);

        $id=$postData['id'];



        $staffTagging = $staffTaggingRepository->find((int)$id);

        $secondGuarantor=$staffTagging->getF043fsecondGuarantor();

        if(empty($secondGuarantor))
        {   
            $data = $staffTaggingRepository->updateLevel2($staffTagging);
        } 

        return new JsonModel([
            'status' => 200,
            'message' => 'Approved successfully'
        ]);
    }

    public function approveLoanGurantorAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $staffTaggingRepository       = $this->getRepository('T043fstaffTagging');
        $loanGurantorRepository       = $this->getRepository('T044floanGurantors');
        $loanGurantors = array();
        foreach ($postData['id'] as $id) {
            $loan = $loanGurantorRepository->find((int)$id);
            array_push($loanGurantors, $loan);
        }
        $staffTaggings = $staffTaggingRepository->loanGurantorApproval($loanGurantors);

        return new JsonModel([
            'status' => 200,
            'message' => 'Approved successfully'
        ]);
    }

     public function processLoanAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $staffTaggingRepository       = $this->getRepository('T043fstaffTagging');

        $staffTaggings = $staffTaggingRepository->processLoan($postData);

        return new JsonModel([
            'status' => 200,
            'message' => 'Loans Processed successfully'
        ]);
    }



}