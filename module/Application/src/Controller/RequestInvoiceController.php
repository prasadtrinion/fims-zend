<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class RequestInvoiceController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T073frequestInvoice');
        $result            = $invoiceRepository->getRequestInvoiceList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function requestInvoiceByTypeAction()
    {

        $request           = $this->getRequest();
          $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $type = $postData['type'];
        $user = $postData['userId'];
        $invoiceRepository = $this->getRepository('T073frequestInvoice');
        $result            = $invoiceRepository->getRequestInvoiceListByType($type,$user);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em             = $this->getEntityManager();
        $request        = $this->getRequest();
        $approvedStatus = $this->params()->fromRoute('approvedStatus');

        $invoiceRepository = $this->getRepository('T073frequestInvoice');
        if (!isset($approvedStatus)) {

            $invoice = $invoiceRepository->find((int) $id);

            if (!$invoice) {
                return $this->resourceNotFound();
            }
        }
        $result = $invoiceRepository->getRequestInvoiceById((int) $id, $approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $invoiceRepository = $this->getRepository('T073frequestInvoice');

            try
            {
                $invoiceObj = $invoiceRepository->createNewRequestInvoice($postData);
            } catch (\Exception $e) {
                echo $e;
            }
            return new JsonModel([
                'status'    => 200,
                'message'   => 'Request Invoice Added successfully',
                'invoiceId' => $invoiceObj->getF071fid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $invoiceRepository       = $this->getRepository('T073frequestInvoice');
        $invoiceDetailRepository = $this->getRepository('T074frequestInvoiceDetails');
        $invoice                 = $invoiceRepository->find((int)$id);

        if (!$invoice) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $invoiceObj     = $invoiceRepository->updateRequestInvoice($invoice, $postData);
            $invoiceDetails = $postData['invoice-details'];
            foreach ($invoiceDetails as $invoiceDetail) {

                if (array_key_exists("f071fid", $invoiceDetail)) {

                    $invoiceDetailObj = $invoiceDetailRepository->find((int) $invoiceDetail['f072fid']);

                    // if(!$master)
                    // {
                    //     return $this->resourceNotFound();
                    // }
                    if ($invoiceDetailObj) {
                        $invoiceDetailRepository->updateRequestInvoiceDetail($invoiceDetailObj, $invoiceDetail);
                    }
                } else {

                    $invoiceDetailRepository->createRequestInvoiceDetail($invoiceObj, $invoiceDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
    public function approveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $invoiceRepository = $this->getRepository('T073frequestInvoice');
        $invoiceRepository->requestInvoiceApprove($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Request Invoices Approved successfully',
        ]);
    }

    
    public function customerRequestInvoicesAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T073frequestInvoice');
        $result            = $invoiceRepository->getCustomerRequestInvoices((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function deleteRequestInvoiceDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $invoiceRepository = $this->getRepository('T074frequestInvoiceDetails');
        $result            = $invoiceRepository->deleteRequestInvoiceDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Data Deleted successfully',
        ]);
    }

}
