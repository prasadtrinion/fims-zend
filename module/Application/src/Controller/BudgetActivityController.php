<?php
namespace Application\Controller;

use Application\Entity\T015fdepartment;
use Application\Entity\T014fglcode;
use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class BudgetActivityController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();
        //         $budgetActivityRepository = $this->getRepository('T054factivityDetails');
        // $result            = $budgetActivityRepository->budgetAvtivityImportdataasd();


        // try
        // {
            $BudgetActivityRepository = $this->getRepository('T054factivityMaster');
        // }
        // catch(\Exception $e)
        // {
        //     echo $e;
        // }
        $result = $BudgetActivityRepository->getActivity();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $activityRepository = $this->getRepository('T054factivityMaster');
        $activity = $activityRepository ->find($id);
        if(!$activity)
        {
            return $this->resourceNotFound();
        }
        $result = $activityRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    
    public function ActivityApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        $activity = $postData['id'];
        $reason = $postData['reason'];
        $status = $postData['status'];
        $activityObjs=array();
        foreach($activity as $id)
        {
            $activityObj =$budgetActivityRepository->find((int)$id);
            if(!$activityObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'budget activity doesnot exist.'
                ]);
            }
            array_push($activityObjs, $activityObj);
        }
        $activity = $budgetActivityRepository->updateApprovalStatus($activityObjs,$status,$reason);
        return new JsonModel([
            'status' => 200,
            'message' => 'updated'
        ]);
    }
    public function getActivityApprovalListAction()
    {

        $ApprovedStatus = $this->params()->fromRoute('approvedStatus');
        $approvedStatus = (int)$ApprovedStatus;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $result = $budgetActivityRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    // public function create($postData)
    // {

    //     $em = $this->getEntityManager();
    //     $request = $this->getRequest();
        

    //     $rawBody = file_get_contents("php://input");

        
    //     if($this->isJSON($rawBody))
    //     {
            
    //         $activityRepository = $this->getRepository('T054factivityMaster');

        
    //         $activityObj = $activityRepository->createActivity($postData);
            
    //         return new JsonModel([
    //                 'status' => 200,
    //                 'message' => 'Added successfully',
                    
    //             ]);

    //     }
    //     else
    //     {
    //         return $this->invalidInputType();
    //     }
    // }

    public function create($postData)
    {
        
        $flag = 0;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        $postData = json_decode($rawBody,true);
            
            // $activityDatas=$postData['data'];
            
        $department=$postData['f054fdepartment'];
        $fund=$postData['f054ffund'];
        $financialyear=$postData['f054fidFinancialyear'];
 
        // $idDepartment=(int$Department;
        $idFinancialyear=(int)$Financialyear;
        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        $budget = $budgetActivityRepository->findBy(array('f054fdepartment'=>$department,'f054ffund'=>$fund,'f054fidFinancialyear'=>(int)$financialyear));
        if($budget){
            return new JsonModel([
                'status' => 409,
                'message' => 'Already Exist',
                    
            ]);
        }
        $BudgetActivityObj = $budgetActivityRepository->createActivity($postData);


        // $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        // $budgetActivity = $budgetActivityRepository->findBy(array('f054fidFinancialyear' => $idFinancialyear,'f054fdepartment' => $Department) );

        // if($budgetActivity)
        // {
        //     $flag =1;
        // }
        // else
        // {
        //     $BudgetActivityObj = $budgetActivityRepository->createActivity($postData);
        // }
        return new JsonModel([
                'status' => 200,
                'message' => 'Created successfully',
                    
            ]);
                    
            
        // if($flag == 0)
        // {
        //     return new JsonModel([
        //         'status' => 200,
        //         'message' => 'Created successfully',
                    
        //     ]);
        // }
        // else
        // {
        //     return new JsonModel([
        //         'status' => 200,
        //         'message' => 'Already exist.'
        //     ]);
        // }

            
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $activityRepository = $this->getRepository('T054factivityMaster');
        $activityDetailRepository = $this->getRepository('T054factivityDetails');
        $activity          =$activityRepository->find($id);

        if (!$activity) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {
           
            $activityObj     = $activityRepository->updateActivity($activity, $postData);
            $activityDetails = $postData['activity-details'];
        
            foreach ($activityDetails as $activityDetail) 
            {

                if (array_key_exists("f054fidDetails", $activityDetail)) 
                {

                    $activityDetailObj = $activityDetailRepository->find((int) $activityDetail['f054fidDetails']);

                    if ($activityDetailObj) {
                        $activityDetailRepository->updateActivityDetail($activityDetailObj, $activityDetail);
                    }
                } else {

                    $activityDetailRepository->createActivityDetail($activityObj, $activityDetail);
                }
                 $deletedIds=$postData['deletedIds'];
            

            $deleteactivityDetails = $activityDetailRepository->updateActivityDetailStatus($deletedIds);

            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
    public function getActivityAmountAction()
    {

        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        // $idGlcode = $this->params()->fromRoute('idGlcode');
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        // $idDepartment=(int)$Department;
        $idFinancialyear=(int)$Financialyear;

        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $result = $budgetActivityRepository->getActivityAmount($Department, $idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getActivityGlcodeAction()
    {

        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        // $idGlcode = $this->params()->fromRoute('idGlcode');
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        // $idDepartment=(int)$Department;
        $idFinancialyear=(int)$Financialyear;

        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $result = $budgetActivityRepository->getActivityGlcode($Department, $idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getActivityDetailsAction()
    {

        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        

        // $idDepartment=(int)$Department;
        $idFinancialyear=(int)$Financialyear;
        

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $budgetActivityRepository = $this->getRepository('T054factivityDetails');
        
        $result = $budgetActivityRepository->getActivityDetails($Department, $idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function budgetSummaryAction(){
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $idDepartment = $postData['idDepartment'];
         $idFinancialYear = $postData['idFinancialYear'];
        $activityMasterRepository = $this->getRepository('T054factivityMaster');
        $result = $activityMasterRepository->budgetSummary($Department,(int)$idFinancialYear);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function accountJournalsAction(){
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
        $accountId = $this->params()->fromRoute('accountId');
         
        $activityMasterRepository = $this->getRepository('T054factivityMaster');
        $result = $activityMasterRepository->accountJournals((int)$accountId);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getActivityDetailsAmountAction(){
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         
        $activityRepository = $this->getRepository('T054factivityMaster');
        $result = $activityRepository->getActivityDetailsAmount($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getNonAllocatedCostcenterAction()
    {

        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $idFinancialyear=(int)$Financialyear;

        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $result = $budgetActivityRepository->getNonAllocatedCostcenter($idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function getAllGlAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $Financialyear = $this->params()->fromRoute('idFinancialyear');

        $idFinancialyear=(int)$Financialyear;

        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $result = $budgetActivityRepository->getAllGl($idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function budgetFileImportAction()
    {
        
        $em = $this->getEntityManager();
        // $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        $postData = json_decode($rawBody,true);
        $name = $postData['name'];


        $budgetActivityRepository = $this->getRepository('T054factivityMaster');
        
        $budgetActivityRepository->budgetFileImport($name);

        return new JsonModel([
            'status' => 200,
            'message' => 'Data imported successfully'
        ]);
    }

    public function getActivityAccountCodesAction()
    {
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         
        $activityRepository = $this->getRepository('T054factivityMaster');
        $result = $activityRepository->getActivityAccountCodes($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deleteBudgetActivityDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $budgetActivityRepository = $this->getRepository('T054factivityDetails');
        $result            = $budgetActivityRepository->deleteBudgetActivityDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Budget Activity Details Deleted successfully',
        ]);
    }

    public function budgetAvtivityImportdataAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $budgetActivityRepository = $this->getRepository('T054factivityDetails');
        $result            = $budgetActivityRepository->budgetAvtivityImportdata();
        return new JsonModel([
            'status' => 200,
            'result' => 'Budget ...',
        ]);
    }

}