<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CurrencyRateSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $currencyRateSetupRepository = $this->getRepository('T064fcurrencyRateSetup');
        $result = $currencyRateSetupRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $currencyRateSetupRepository = $this->getRepository('T064fcurrencyRateSetup');
        $currencyRate= $currencyRateSetupRepository->find($id);
        if(!$currencyRate)
        {
            return $this->resourceNotFound();
        }
        $result = $currencyRateSetupRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $currencyRateSetupRepository = $this->getRepository('T064fcurrencyRateSetup');

            try
            {
                $currencyRateObj = $currencyRateSetupRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Effective date Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Currency Rate Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $currencyRateSetupRepository = $this->getRepository('T064fcurrencyRateSetup');
        $currencyRateSetup = $currencyRateSetupRepository->find($id);

        if(!$currencyRateSetup)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $currencyRateSetupObj = $currencyRateSetupRepository->updateData($currencyRateSetup, $postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

}