<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class JournalController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request           = $this->getRequest();
        $journalRepository = $this->getRepository('T017fjournal');
        $result            = $journalRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em                = $this->getEntityManager();
        $request           = $this->getRequest();
        $journalRepository = $this->getRepository('T017fjournal');
        $journal           = $journalRepository->find((int) $id);
        if (!$journal) {
            return $this->resourceNotFound();
        }
        $result = $journalRepository->getListById((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) 
        {
            $data=array();
            $data['type']='GL';
            $initialRepository = $this->getRepository('T011finitialConfig');
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f017freferenceNumber'] = $generateNumber;


            $journalRepository = $this->getRepository('T017fjournal');
            $journalDetailRepository = $this->getRepository('T018fjournalDetails');

            $journalDetails = $postData['journal-details'];

            $crAmount       = 0;
            $drAmount       = 0;
            foreach ($journalDetails as $journalDetail) {
                $crAmount = $crAmount + (float) $journalDetail['f018fcrAmount'];
                $drAmount = $drAmount + (float) $journalDetail['f018fdrAmount'];
                // $glcode=$journalDetail['f018fidGlcode'];
               
            }
            if ($crAmount != $drAmount) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Balance Error.',
                ]);
            }
            $postData['f017ftotalAmount'] = (float) $crAmount;

            try
            {
                if(array_key_exists('f017fid', $postData)){
                    $journal = $journalRepository->find((int)$postData['f017fid']);
                    $journal = $journalRepository->updateJournal($journal,$postData);
                    $journalDetails = $postData['journal-details'];
                    foreach ($journalDetails as $detail) {
                       if(array_key_exists('f018fid', $detail)){
                        $detailObj = $journalDetailRepository->find((int)$detail['f018fid']);
                        if($detailObj){
                            $journalDetailRepository->updateDetail($detailObj,$detail);
                        }
                       }
                       else{
                            $journalDetailRepository->createDetail($journal,$detail);
                       }
                    }
                }
                else{
                $journalObj = $journalRepository->createNewData($postData,$glcode);
                }
            } catch (\Exception $e) {
                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 200,
                        'message' => 'Already exist.',
                    ]);
                }
            }
            if ($journalObj == 0) {
                return new JsonModel([
                    'status'  => 401,
                    'message' => 'Insufficient Balance',
                ]);
            }
            return new JsonModel([
                'status'    => 200,
                'message'   => 'Added successfully',
                // 'journalId' => $journalObj->getFfid(),
            ]);

        } else {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                = $this->getEntityManager();
        $request           = $this->getRequest();
        $journalRepository = $this->getRepository('Journal');
        $journal           = $journalRepository->find((int) $id);

        if (!$journal) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            if (empty($postData['ffdescription'])) {
                return $this->invalidInputType();
            }

            $journalObj = $journalRepository->updateJournal($journal, $postData);
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function approveAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
        $openingBalanceRepository = $this->getRepository('T016fopeningbalance');

        $journalRepository = $this->getRepository('T017fjournal');
        $journalIds        = $postData['id'];
        // print_r($journalIds);exit;
        $journal = $journalRepository->approveJournals($journalIds);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Journals approved successfully',
        ]);

    }
    public function monthlySummaryAction()
    {

        $journalRepository = $this->getRepository("T019fjournalMonthlySummary");
        $month             = $this->params()->fromRoute('month');
        $result            = $journalRepository->updateCrDrByMonth($month);
        if (!result) {
            return new JsonModel([
                'status'  => 200,
                'message' => 'Month Summary Add Failed',
            ]);
        }
        return new JsonModel([
            'status'  => 200,
            'message' => 'Month Summary Added successfully',
        ]);
    }
    public function getJournalsAction()
    {
        
        $approvedStatus    = $this->params()->fromRoute('approvedStatus');
        $em                = $this->getEntityManager();
        $request           = $this->getRequest();
        $journalRepository = $this->getRepository('T017fjournal');
        
        $result = $journalRepository->getApprovedJournals((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function cashBookAction()
    {
        
        $em                = $this->getEntityManager();
        $request           = $this->getRequest();
        $journalRepository = $this->getRepository('T017fjournal');
        
        $result = $journalRepository->getCashBook();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function journalReversalAction()
    {
        $em                       = $this->getEntityManager();
        $request                  = $this->getRequest();
        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);


        $journalRepository = $this->getRepository('T017fjournal');

        $data=array();
        $data['type']='GL';
        $initialRepository = $this->getRepository('T011finitialConfig');
        $generateNumber = $initialRepository->generateFIMS($data);
        $postData['f017freferenceNumber'] = $generateNumber;
                   
        $journalIds        = $postData['id'];
        $description = $postData['description'];
        $reference= $postData['f017freferenceNumber'];
        $journalObjs       = array();

        $reversaljournal = $journalRepository->createJournal($journalIds,$description,$reference);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Journals updated successfully'
        ]);


    }
}
