<?php

namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class DiscountController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $discountRepository = $this->getRepository('T024fdiscount');
        
        $discount= $discountRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data'   => $discount,
        ]);
    }

    public function get($id)
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $discountRepository = $this->getRepository('T024fdiscount');

        $discount = $discountRepository->find((int)$id);
       
        if (!$discount) {
            return $this->resourceNotFound();
        }

        $result = $discountRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }

    public function DiscountApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $request              = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $discountRepository = $this->getRepository('T024fdiscount');
        $journalRepository = $this->getRepository('T017fjournal');
         $discountRepository->updateApprovalStatus($postData,$journalRepository);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }
    public function getDiscountApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $discountRepository = $this->getRepository('T024fdiscount');
        
        $result = $discountRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            $discountRepository = $this->getRepository('T024fdiscount');
            $discountDetailRepository = $this->getRepository('T025fdiscountDetails');
            if (array_key_exists("f024fid", $postData)) 
                {
            $discount         =$discountRepository->find((int)$postData['f024fid']);
            $discountObj     = $discountRepository->updateDiscount($discount, $postData);
            $discountDetails = $postData['discount-details'];
        
            foreach ($discountDetails as $discountDetail) 
            {

                if (array_key_exists("f025fid", $discountDetail)) 
                {

                    $discountDetailObj = $discountDetailRepository->find((int) $discountDetail['f025fid']);

                    if ($discountDetailObj) {
                        $discountDetailRepository->updateDiscountDetail($discountDetailObj, $discountDetail);
                    }
                } else {

                    $discountDetailRepository->createDiscountDetail($discountObj, $discountDetail);
                }
            }
        }
        else{
            $discountObj = $discountRepository ->createDiscount($postData);
        }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }
    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $discountRepository = $this->getRepository('T024fdiscount');
        $discountDetailRepository = $this->getRepository('T025fdiscountDetails');
        $discount         =$discountRepository->find((int)$id);

        if (!$discount) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $discountObj     = $discountRepository->updateDiscount($discount, $postData);
            $discountDetails = $postData['discount-details'];
        
            foreach ($discountDetails as $discountDetail) 
            {

                if (array_key_exists("f025fid", $discountDetail)) 
                {

                    $discountDetailObj = $discountDetailRepository->find((int) $discountDetail['f025fid']);

                    if ($discountDetailObj) {
                        $discountDetailRepository->updateDiscountDetail($discountDetailObj, $discountDetail);
                    }
                } else {

                    $discountDetailRepository->createDiscountDetail($discountObj, $discountDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

}
