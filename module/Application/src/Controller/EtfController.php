<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class EtfController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $etfRepository = $this->getRepository('T136fetf');
       
        
        try{
                $etf = $etfRepository->find((int)$id);
        }
        catch(\Exception $e){
            echo $e;
        }
        if(!$etf)
        {
            return $this->resourceNotFound();
        }
        
        $result = $etfRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $etfRepository = $this->getRepository('T136fetf');
        try{
        $etfDetails = $etfRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => $etfDetails
        ]);        
    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {
                

                $etfRepository = $this->getRepository('T136fetf');
                $etfDetailRepository = $this->getRepository('T137fetfDetails');
                if (array_key_exists("f136fid", $postData)) 
                    {
                    $etfObj = $etfRepository->find((int)$postData['f136fid']);
                        
                    $etfObj =  $etfRepository->updateMaster($etfObj, $postData);
                    $details = $postData['details'];
            
                    foreach ($details as $detail) 
                    {
    
                        if (array_key_exists("f137fid", $detail)) 
                        {
    
                            $detailObj = $etfDetailRepository->find((int) $detail['f137fid']);
    
                            if ($detailObj)
                            {
                                $etfDetailRepository->updateDetail($detailObj, $detail);
                            }
                        }
                        else
                        {
    
                        $etfDetailRepository->createDetail($etfObj, $detail);
                        }
                    }
                }
            else
            {
                try {
                    // print_r($postData);exit;
                    $etfObj = $etfRepository->createNewData($postData);
                } catch (Exception $e) {
                    echo $e;
                }
                
            }
                
        } 
              
               return new JsonModel([
                    'status' => 200,
                    'message' => 'ETf Created successfully',
                    'etfId' => $etfObj->getF136fId()
                ]);

       
    }
    public function approvedEtfsAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status = $this->params()->fromRoute('status');
        $etfRepository = $this->getRepository('T136fetf');
       
        
        $result = $etfRepository->approvedEtfs((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }
    public function approveEtfDataAction()
    {

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $etfRepository = $this->getRepository('T136fetf');
        $etfRepository->approveEtfData($postData);

        return new JsonModel([
                'status' => 200,
                'message' => 'ETFs Approved successfully'
            ]);
    }

    public function getEtfApprovalsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $etfRepository = $this->getRepository('T136fetf');

        $result = $etfRepository->getEtfApprovals((int)$status);
        return new JsonModel([
            'status' => 200,
            'data' => $result,

        ]);
    }


}