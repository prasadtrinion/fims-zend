<?php
namespace Application\Controller;

use Application\Entity\T059fassetLocation;
use Application\Repository\AccountCodeRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

// print_r("hello");
//  exit();

class AssetLocationController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()

    {
        $refCode = $this->params()->fromRoute('refcode');
        
        $request = $this->getRequest();
        
        $assetLocationRepository = $this->getRepository('T059fassetLocation');

        try
        {
            $assetLocation = $assetLocationRepository->find($refCode);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $assetLocationRepository->getListByRef($refCode);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function create($postData)
    {
        

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {

           
            
            $assetLocationRepository = $this->getRepository('T059fassetLocation');
            try
            {
                if($postData['f059flevelStatus'] == 2 || $postData['f059flevelStatus'] == 1)
                {

                    $assetLocationObj1 = $assetLocationRepository->find((int)$postData['f059fparentCode']);
                    $parentId = $assetLocationObj1->getF059fparentCode();
                    $assetLocation1 = $assetLocationObj1->getF059fcode();
                    $assetLocationObj2 = $assetLocationRepository->find((int)$parentId);
                    $assetLocation2 = $assetLocationObj2->getF059fcode();
                    $postData['f059fcompleteCode'] = $assetLocation2.$assetLocation1.$postData['f059fcode'];
                }
            }
            catch(\Exception $e)
            {
                echo $e;
            }  

            try
            {
                $assetLocationObj = $assetLocationRepository->createNewData($postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function newAssetCodeAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $assetLocationRepository = $this->getRepository('T059fassetLocation');
            $accounts = $postData['data'];
            
            foreach ($accounts as $data) 
            {
            

                try
                {
                    if($data['f059flevelStatus'] == 2)
                    {
                        
                        $assetLocationObj1 = $assetLocationRepository->find((int)$data['f059fparentCode']);
                        $parentId = $assetLocationObj1->getF059fparentCode();
                        $assetLocation1 = $assetLocationObj1->getF059fcode();
                        $assetLocationObj2 = $assetLocationRepository->find((int)$parentId);
                        $assetLocation2 = $assetLocationObj2->getF059fcode();
                        $data['f059fcompleteCode'] = $assetLocation2.$assetLocation1.$data['f059fcode'];

                    }
                }
                catch(\Exception $e)
                {
                    echo $e;
                }
                if(array_key_exists("f059fid",$data))
                {

                    $assetLocation = $assetLocationRepository->find((int)$data['f059fid']);
                    if($assetLocation)
                    {
                        $assetLocationObject = $assetLocationRepository->updateData($assetLocation,$data);
                    }
                }   
                else
                {
                    $assetLocation = $assetLocationRepository->findBy(array("f059fcode"=>strtolower($postData['f059fcode']),"f059fcategory"=>strtolower($postData['f059fcategory']),"f059fparentCode"=>$postData['f059fparentCode'],"f059flevelStatus"=>$postData['f059flevelStatus']));
                    if ($assetLocation)
                    {
                   return new JsonModel([
                            'status' => 409,
                            'message' => 'Already Exist.'
                        ]);
                    }
                 
                    $assetLocationObject = $assetLocationRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        try
        {
            $assetLocationRepository = $this->getRepository('T059fassetLocation');
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        $assetLocation = $assetLocationRepository->find((int)$id);

        if(!$assetLocation)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            if(empty($postData['f059fcode']))
            {
                return $this->invalidInputType(); 
            }
            try
            {
                $assetLocationObj = $assetLocationRepository->updateAssetLocation($assetLocation,$postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function assetLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        
        $request = $this->getRequest();
        
        $assetLocationRepository = $this->getRepository('T059fassetLocation');

        try
        {
            $assetLocation = $assetLocationRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $assetLocationRepository->getListByLevel((int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function assetParentLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        $parent = $this->params()->fromRoute('parent');
        
        $request = $this->getRequest();
        
        $assetLocationRepository = $this->getRepository('T059fassetLocation');

        try
        {
            $assetLocation = $assetLocationRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $assetLocationRepository->getListByParentAndLevel((int)$parent,(int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deleteAssetLocationAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $id = $this->params()->fromRoute('id');
        $accountRepository = $this->getRepository('T059fassetLocation');
        $account = $accountRepository->find((int)$id);

        if(!$account)
        {
           return $this->resourceNotFound();
        }

        
        $accountRepository->updateAccount($account);
        return new JsonModel([
            'status' => 200,
            'message' => 'Deleted successfully'
        ]);
    }

    public function assetLocationStatusAction()

    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $id=$postData['f059fid'];
       
        $id =(int)$id;
       
        $accountRepository = $this->getRepository('T059fassetLocation');
        $account =  $accountRepository ->find($id);
        if(!$account)
        {
            return $this->resourceNotFound();
        }
        $result = $accountRepository->assetLocationStatus($id);
        return new JsonModel([
            'status' => 200,
            'message' => 'Updated Successfully'
        ]);
    }

    public function deleteAssetLocationDataAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $accountRepository = $this->getRepository('T059fassetLocation');
        $result            = $accountRepository->deleteAssetLocationData($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'AssetLocation Deleted successfully',
        ]);
    }
}