<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class SetupDisposalController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T086fsetupDisposal');
        
        $result = $assetRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $assetRepository = $this->getRepository('T086fsetupDisposal');
        $asset = $assetRepository ->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
                          
            $assetRepository = $this->getRepository('T086fsetupDisposal');
            $asset = $assetRepository->findBy(array("f086fdisposalCode"=>strtolower($postData['f086fdisposalCode'])));
            if ($asset)
            {
                                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
            }
            
            try
            {
                $assetObj =$assetRepository->createDisposal($postData);
            }
            catch (\Exception $e)
            {
                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Saving'
                        ]);
              

            }
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $assetRepository = $this->getRepository('T086fsetupDisposal');
        $asset = $assetRepository->find((int)$id);

        if(!$asset)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            try
            {
                $assetObj = $assetRepository->updateData($asset,$postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      

    }   

    
  
}