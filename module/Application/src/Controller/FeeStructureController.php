<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;


class FeeStructureController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $feeRepository = $this->getRepository('T048ffeeStructureMaster');
        $result = $feeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $feeRepository = $this->getRepository('T048ffeeStructureMaster');
        $fee = $feeRepository->find($id);
        if(!$fee)
        {
            return $this->resourceNotFound();
        }
        $result = $feeRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            // $feeRepository = $this->getRepository('T048ffeeStructureMaster');
            

            try
            {
            $feeMasterRepository = $this->getRepository('T048ffeeStructureMaster');
            $feeDetailRepository = $this->getRepository('T048ffeeStructureDetails');
            $feeProgramRepository = $this->getRepository('T048ffeeStructureProgram');
            $programCourseRepository = $this->getRepository('T048fprogramCourseFee');

            
                if (array_key_exists("f048fid", $postData)) 
                {
                $feeMasterObj = $feeMasterRepository->find((int)$postData['f048fid']);
                    
                $feeMasterObj =  $feeMasterRepository->updateFeeMaster($feeMasterObj, $postData);

            $feeDetails = $postData['fee-details'];
            $feePrograms = $postData['fee-program'];
            $programCourses = $postData['course-fee'];
        
            foreach ($feeDetails as $detail) 
            {

                if (array_key_exists("f048fidDetails", $detail)) 
                {

                    $feeDetailObj = $feeDetailRepository->find((int) $detail['f048fidDetails']);

                    if ($feeDetailObj) {
                        $feeDetailRepository->updateFeeDetail($feeDetailObj, $detail);
                    }
                } else {

                    $feeDetailRepository->createFeeDetail($feeMasterObj, $detail);
                }
            }

            foreach ($feePrograms as $detail) 
            {

                if (array_key_exists("f048fidStructProgram", $detail)) 
                {

                    $feeProgramObj = $feeProgramRepository->find((int) $detail['f048fidStructProgram']);

                    if ($feeProgramObj) {
                        $feeProgramRepository->updateFeeProgram($feeProgramObj, $detail);
                    }
                } else {

                    $feeProgramRepository->createFeeProgram($feeMasterObj, $detail);
                }
            }

            foreach ($programCourses as $detail) 
            {

                if (array_key_exists("f048fidProgramCourse", $detail)) 
                {

                    $programCourseObj = $programCourseRepository->find((int) $detail['f048fidProgramCourse']);

                    if ($programCourseObj) {
                        $programCourseRepository->updateProgramCourse($programCourseObj, $detail);
                    }
                } else {

                    $programCourseRepository->createProgramCourse($feeMasterObj, $detail);
                }
            }
        }
        else{
                $feeObj = $feeMasterRepository->createFee($postData);
        }

            }

           
            catch (\Exception $e)
            {
                // echo $e;

                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Fee Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Fee Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $feeRepository = $this->getRepository('T048ffeeStructureMaster');
        $feeDetailRepository = $this->getRepository('T048ffeeStructureDetails');
        $fee                 = $feeRepository->find($id);

        if (!$debt) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $feeObj     = $feeRepository->updateFee($fee, $postData);
            $feeDetails = $postData['fee-details'];
            foreach ($feeDetails as $feeDetail) 
            {

                if (array_key_exists("f048fidDetails", $feeDetail)) 
                {

                    $feeDetailObj = $feeDetailRepository->find((int) $feeDetail['f098fidDetails']);

                    if ($feeDetailObj) {
                        $feeDetailRepository->updateFeeDetail($feeDetailObj, $feeDetail);
                    }
                } else {

                    $feeDetailRepository->createFeeDetail($feeObj, $feeDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteFeeStructureDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $feeDetailRepository = $this->getRepository('T048ffeeStructureDetails');
        $result            = $feeDetailRepository->deleteFeeStructureDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Fee Structure Details Deleted successfully',
        ]);
    }

    public function deleteFeeStructureProgramAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $feeStructureProgrammeRepository = $this->getRepository('T048ffeeStructureProgram');
        // print_r($postData);exit;
        $result            = $feeStructureProgrammeRepository->deleteFeeStructureProgram($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Fee Structure Programme Details Deleted successfully',
        ]);
    }

    public function deleteProgramCourceFeeAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $programmeCourceFeeRepository = $this->getRepository('T048fprogramCourseFee');
        // print_r($postData);exit;
        $result            = $programmeCourceFeeRepository->deleteProgramCourceFee($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Programme Cource Fee Details Deleted successfully',
        ]);
    }

}
