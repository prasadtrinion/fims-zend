<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class BatchWithdrawController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
     {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
        	$batchWithdrawDetails = $batchWithdrawRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $batchWithdrawDetails
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
                $batchWithdraw = $batchWithdrawRepository->find((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        if(!$batchWithdraw)
        {
            return $this->resourceNotFound();
        }
        
        $result = $batchWithdrawRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {
                

                $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
                $batchWithdrawDetailRepository = $this->getRepository('T146fbatchWithdrawDetails');
                if (array_key_exists("f145fid", $postData)) 
                    {
                    $batchWithdrawObj = $batchWithdrawRepository->find((int)$postData['f145fid']);
                        
                    $batchWithdrawObj =  $batchWithdrawRepository->updateMaster($batchWithdrawObj, $postData);
                    $details = $postData['details'];
            
                    	foreach ($details as $detail) 
                    	{
    
                        	if (array_key_exists("f146fid", $detail)) 
                        	{
    
                            	$detailObj = $batchWithdrawDetailRepository->find((int) $detail['f146fid']);
    
                            	if ($detailObj)
                            	{
                                	$batchWithdrawDetailRepository->updateDetail($detailObj, $detail);
                            	}
                        	}
                        	else
                        	{
                        		$batchWithdrawDetailRepository->createDetail($batchWithdrawObj, $detail);
                        	}
                    	}
                	}
            		else
            		{
                	try
                	{
                    	// print_r($postData);exit;
                    	$batchWithdrawObj = $batchWithdrawRepository->createNewData($postData);
                	}
                	catch (Exception $e)
                	{
                    	echo $e;
                	}
                
            	}
                
        	} 
              
               return new JsonModel([
                    'status' => 200,
                    'message' => 'Batch Withdraw Created successfully',
                    'batchWithdrawId' => $batchWithdrawObj->getF145fId()
                ]);
    }

    public function getBatchWithdrawActiveListAction()
     {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
        	$batchWithdrawDetails = $batchWithdrawRepository->getBatchWithdrawActiveList((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $batchWithdrawDetails
        ]);        
    }

    public function approveBatchWithdrawAction()
     {

       $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
        	$batchWithdrawDetails = $batchWithdrawRepository->approveBatchWithdraw($postData);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => 'Batch Withdraw Approved Successfully'
        ]);        
    }

     public function deleteBatchWithdrawDetailsAction()
     {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
            $batchWithdrawDetails = $batchWithdrawRepository->deleteBatchWithdrawDetails($postData);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => 'Batch Withdraw Detail Deleted Successfully'
        ]);        
    }

    public function getInvestmentRegNotInBatchWithdrawAction()
     {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $batchWithdrawRepository = $this->getRepository('T145fbatchWithdraw');
        try
        {
            $batchWithdrawDetails = $batchWithdrawRepository->getInvestmentRegNotInBatchWithdraw($postData);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $batchWithdrawDetails
        ]);        
    }

}