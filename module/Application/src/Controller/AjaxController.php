<?php
namespace Application\Controller;

use Application\Entity\Account;
use Application\Repository\LocationRepository;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceManager;
use Zend\Http\Response;

class AjaxController extends AbstractAppController
{

    public function __construct(
        ServiceManager $sm
    ){
        parent::__construct($sm);
    }
    
    public function indexAction()
    {
        return new JsonModel();
    }

    /**
     * Get list of states
     * 
     * @return \Zend\View\Model\JsonModel
     */
    public function getStateListAction()
    {
        $postDataArray = $this->request->getPost()->toArray();
        $countryCode = $postDataArray['country'];
        $stateList = LocationRepository::getStatesByCountry($countryCode);
        
        return new JsonModel($stateList);
    }

    /**
     * Send email verification link to user
     * 
     * @return \Zend\View\Model\JsonModel
     */
    public function sendEmailVerificationAction()
    {
        $authService = $this->sm->get('app.authentication');
        $authUser = $authService->getIdentity();
        $accountRepository = $this->getRepository('Account');
        $accountEntity = $accountRepository->find($authUser['accountId']);
        $result = array(
            'Result' => 0,
            'message' => 'Email sending failed',
        );

        if ($authUser['emailStatus'] != Account::STATUS_VERIFIED) {
            $token = md5(uniqid(time()));
            $accountEntity->setVerificationToken($token);
            $accountRepository->update($accountEntity);
            $subject = 'Your Email Verification';
            $mailer = $this->sm->get('app.mailer');
            $mailer->send($authUser['username'], $subject, 'email/verify-email', array(
                'user' => $authUser,
                'account' => $accountEntity,
            ));
            $message = 'Verification link has been sent.';
            
            $result = array(
                'result' => 1,
                'message' => $message,
            );
        }

        return new JsonModel($result);
    }

    /**
     * Check username available in the system
     * 
     * @return \Zend\Http\Response
     */
    public function isUsernameAvailableAction()
    {
        $postDataArray = $this->request->getPost()->toArray();
        $username = $postDataArray['username'];

        $accountRepository = $this->getRepository('Account');
        $authService = $this->sm->get('app.authentication');
        $authUser = $authService->getIdentity();
        
        if ($username == $authUser['username']) {
            $output = 'true';
        } else {
            $result = $accountRepository->isAccountExist($username);
            if ($result) {
                $output = 'false';
            } else {
                $output = 'true';
            }
        }
        
        $response = new Response();
        $response->setContent($output);
        
        return $response;
    }

    /**
     * Get static page
     * 
     * @return \Zend\View\Model\ViewModel
     */
    public function getStaticPageAction()
    {
        $name = $this->params()->fromRoute('name');
        $claimId = $this->params()->fromRoute('id');
       
        //echo $id;exit;
        if ($name == 'claim-terms') {
            $claimRepository = $this->getRepository('RebateClaim');
            $claimEntity = $claimRepository->find($claimId);
            $rebateDescription = $claimEntity->getRebate()->getTerms();
            
            //echo $rebateDescription;exit;
            //$this->layout()->setTemplate(false);
            $view = new ViewModel(array('description'=>$rebateDescription));
            $view->setTemplate('static/claim-terms');
            $view->setTerminal(true);

            return $view;
        }

    }
}
