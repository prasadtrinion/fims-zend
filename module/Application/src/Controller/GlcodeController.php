<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class GlcodeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {
        $em               = $this->getEntityManager();
        $request          = $this->getRequest();
        $glcodeRepository = $this->getRepository('T014fglcode');
        $result           = $glcodeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);

    }

    public function get($id)
    {
        $em               = $this->getEntityManager();
        $request          = $this->getRequest();
        $glcodeRepository = $this->getRepository('T014fglcode');
        $result           = $glcodeRepository->getList((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function create($postData)
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $glcodeRepository = $this->getRepository("T014fglcode");

            $fundArray       = array();
            $departmentArray = array();
            $activityArray   = array();
            $accountArray    = array();
            if ($postData['f014ffundId'] == "All") {
                $fundArray = $glcodeRepository->getAllFundCodes();
            } elseif (is_array($postData['f014ffundId'])) {
                foreach ($postData['f014ffundId'] as $fundId) {
                    array_push($fundArray, $glcodeRepository->getFundCode((int)$fundId));
                }
            } else {
                array_push($fundArray, $glcodeRepository->getFundCode($postData['f014ffundId']));
            }

            if ($postData['f014fdepartmentId'] == "All") {
                $departmentArray = $glcodeRepository->getAllDepartmentCodes();
            } elseif (is_array($postData['f014fdepartmentId'])) {
                foreach ($postData['f014fdepartmentId'] as $departmentId) {
                    array_push($departmentArray, $glcodeRepository->getDepartmentCode((int)$departmentId));
                }
            } else {
                array_push($departmentArray, $glcodeRepository->getDepartmentCode((int)$postData['f014fdepartmentId']));
            }

            if ($postData['f014factivityId'] == "All") {
                $activityArray = $glcodeRepository->getAllActivityCodes();
            } elseif (is_array($postData['f014factivityId'])) {
                foreach ($postData['f014factivityId'] as $activityId) {
                    array_push($activityArray, $glcodeRepository->getActivityCode((int)$activityId));
                }
            } else {
                array_push($activityArray, $glcodeRepository->getActivityCode((int)$postData['f014factivityId']));
            }

            if ($postData['f014faccountId'] == "All") {
                $accountArray = $glcodeRepository->getAllAccountCodes();
            } elseif (is_array($postData['f014faccountId'])) {
                foreach ($postData['f014faccountId'] as $accountId) {
                    array_push($accountArray, $glcodeRepository->getAccountCode((int)$activityId));
                }
            } else {
                array_push($accountArray, $glcodeRepository->getAccountCode((int)$postData['f014faccountId']));
            }

            $combinations = [[]];
            $data         = [
                $fundArray,
                $activityArray,
                $departmentArray,
                $accountArray,
            ];
            $length = count($data);

            for ($count = 0; $count < $length; $count++) {
                $tmp = [];
                foreach ($combinations as $v1) {
                    foreach ($data[$count] as $v2) {
                        $tmp[] = array_merge($v1, [$v2]);
                    }

                }
                $combinations = $tmp;
            }

            foreach ($combinations as $glcode) {
                $finalCode    = "";
                $last_element = count($glcode);
                $id           = 0;
                $count = 0;
                foreach ($glcode as $code) {

                    $id = $id . "," . $code['id'];
                    if ($count == $last_element-1) {
                        $finalCode = $finalCode . $code['code'];
                    } else {
                        $finalCode = $finalCode . $code['code'] . '-';
                    }
                    $count++;
                }
                $idArray      = explode(',', $id);
                $glcodeObject = $glcodeRepository->findBy(array('f014fglCode' => $finalCode));
                if (!$glcodeObject) {

                    try
                    {
                        $glcodeData['f014ffundId']       = $idArray[1];
                        $glcodeData['f014factivityId'] = $idArray[2];
                         $glcodeData['f014fdepartmentId']   = $idArray[3];
                        $glcodeData['f014faccountId']    = $idArray[4];
                        $glcodeData['f014fglCode']       = $finalCode;
                        $glcodeObject                    = $glcodeRepository->createNewData($glcodeData);

                    } catch (\Exception $e) {

                        if ($e->getErrorCode() == 1062) {
                            return new JsonModel([
                                'status'  => 200,
                                'message' => 'Already exist.',
                            ]);
                        }
                    }
                }
            }

            return new JsonModel([
                'status'  => 200,
                'message' => 'Added successfully',

            ]);

        } else {
            return $this->invalidInputType();
        }
    }
    public function update($id, $postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        try
        {
            $glcodeRepository = $this->getRepository("T014fglcode");
        } catch (\Exception $e) {
            echo $e;
        }

        $glcode = $glcodeRepository->find((int) $id);

        if (!$glcode) {
            return $this->resourceNotFound();
        }
        try {

            //Fund
            $fundRepository = $this->getRepository("T057ffund");
            $fundObj        = $fundRepository->find($postData['f014ffundId']);
            if (!$fundObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Fund Code doesnt exist.',
                ]);
            }
            $fundCode = $fundObj->getF057fcode();

            //Department
            $departmentRepository = $this->getRepository("T015fdepartment");
            $departmentObj        = $departmentRepository->find($postData['f014fdepartmentId']);
            if (!$departmentObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Department Code doesnt exist.',
                ]);
            }
            $departmentCode = $departmentObj->getF015fdepartmentCode();

            //Activity
            $activityRepository = $this->getRepository("T058factivityCode");
            $activityObj        = $activityRepository->find($postData['f014factivityId']);
            if (!$activityObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Activity Code doesnt exist.',
                ]);
            }
            $activityCode = $activityObj->getF058fcompleteCode();
            //Account
            $accountRepository = $this->getRepository("T059faccountCode");
            $accountObj        = $accountRepository->find($postData['f014faccountId']);
            $accountCode       = $accountObj->getF059fcompleteCode();
            if (!$accountObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Account Code doesnt exist.',
                ]);
            }
            $glcode           = trim($fundCode) . "-" . trim($activityCode) . "-" . trim($departmentCode) . "-" . trim($accountCode);
            $glcodeRepository = $this->getRepository("T014fglcode");
            $glcodeObj        = $glcodeRepository->findBy(array('f014fglCode' => $glcode));
            if ($glcodeObj) {
                return new JsonModel([
                    'status'  => 200,
                    'message' => 'Glcode Already exist.',
                ]);
            }

            $postData['f014fglCode'] = $glcode;
        } catch (\Exception $e) {
            echo $e;
        }
        try
        {
            $glcodeRepository->updateData($glcode, $postData);
        } catch (\Exception $e) {
            if ($e->getErrorCode() == 1062) {
                return new JsonModel([
                    'status'  => 200,
                    'message' => 'Glcode Already exist.',
                ]);
            }
        }
        return new JsonModel([
            'status'  => 200,
            'message' => 'Glcode Updated successfully',
        ]);

    }

}
