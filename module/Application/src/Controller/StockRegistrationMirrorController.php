<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class StockRegistrationMirrorController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $stockregistrationRepository = $this->getRepository('T134fstockRegistration');
        $result = $stockregistrationRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $stockregistrationRepository = $this->getRepository('T134fstockRegistration');
        $stockregistration = $stockregistrationRepository->find((int)$id);
        if(!$stockregistration)
        {
            return $this->resourceNotFound();
        }
        $result = $stockregistrationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }





    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $stockregistrationRepository = $this->getRepository('T134fstockRegistration');
             
            
                $variable = $data['data'];
                foreach ($variable as $data)
                {
                   
                    if (array_key_exists("f134fid", $data)) 
                    {

                        $stockregistrationObj = $stockregistrationRepository->find((int)$data['f134fid']);

                        if ($stockregistrationObj)
                        {
                    // print_r($licenseObj);exit;

                            $stockregistrationRepository->updateData($stockregistrationObj, $data);
                        }
                    }
                    else
                    {
                        $stockregistrationObj = $stockregistrationRepository->createNewData($data);
                    }
                }
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }


    public function getMirrorStockRegistrationActiveListAction()
    {
        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        // $assetRepository = $this->getRepository('T085fassetInformation');
        $stockRegistrationRepository = $this->getRepository('T134fstockRegistration');
        $result            = $stockRegistrationRepository->getMirrorStockRegistrationActiveList((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

     public function approveStockRegistrationMirrorDataAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $stockRegistrationRepository = $this->getRepository('T134fstockRegistration');
        $result            = $stockRegistrationRepository->approveStockRegistrationMirrorData($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Stock Registration Updated successfully',
        ]);
    }
}