<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StudentDetailsController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $request = $this->getRequest();


        $studRepository = $this->getRepository('T125fstudentDetails');
        

        $result = $studRepository->getList();
         return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $studentDetailsRepository = $this->getRepository('T125fstudentDetails');
        $intake = $studentDetailsRepository->find($id);
        if(!$intake)
        {
            return $this->resourceNotFound();
        }
        $result = $studentDetailsRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $studentDetailsRepository = $this->getRepository('T125fstudentDetails');

            try
            {
                $studentObj = $studentDetailsRepository->createStudent($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Intake Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Intake Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $studentDetailsRepository       = $this->getRepository('T125fstudentDetails');
        $student                 = $studentDetailsRepository->find($id);

        if (!$student) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody))
        {

            $studentDetailObj     = $studentDetailsRepository->updateStudent($student, $postData);
            
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else
        {
            return $this->invalidInputType();
        }

    }

}