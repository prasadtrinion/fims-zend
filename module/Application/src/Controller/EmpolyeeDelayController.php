<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class EmpolyeeDelayController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        // echo "hi";exit;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $DempolyeeRepository = $this->getRepository('T134femployeeDelay');
        $result = $DempolyeeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $DempolyeeRepository = $this->getRepository('T134femployeeDelay');
        $demp = $DempolyeeRepository->find((int)$id);
        if(!$demp)
        {
            return $this->resourceNotFound();
        }
        $result = $DempolyeeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        

        if($this->isJSON($rawBody))
        {
            
        $DempolyeeRepository = $this->getRepository('T134femployeeDelay');

            try
            {
                $dempObj = $DempolyeeRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }
     public function update($id, $data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $DempolyeeRepository = $this->getRepository('T134femployeeDelay');
        $demp = $DempolyeeRepository->find((int)$id);
        if(!$demp)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $dempObj = $DempolyeeRepository->updateData($demp,$data);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    
}		

?>