<?php
namespace Application\Controller;

use Application\Entity\T015fdepartment;
use Application\Entity\T014fglcode;
use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class BudgetIncrementController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        // try
        // {
            $incrementRepository = $this->getRepository('T055fincrementMaster');
        // }
        // catch(\Exception $e)
        // {
        //     echo $e;
        // }
        $result = $incrementRepository->getIncrement();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $incrementRepository = $this->getRepository('T055fincrementMaster');
        $increment = $incrementRepository ->find($id);
        if(!$increment)
        {
            return $this->resourceNotFound();
        }
        $result = $incrementRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    
    public function IncrementApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $incrementRepository = $this->getRepository('T055fincrementMaster');
        $increment = $postData['id'];
        $reason = $postData['reason'];
        $status = $postData['status'];
        $incrementObjs=array();
        foreach($increment as $id)
        {
            $incrementObj =$incrementRepository->find((int)$id);
            if(!$incrementObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'increment doesnot exist.'
                ]);
            }
            array_push($incrementObjs, $incrementObj);
        }
        $increment = $incrementRepository->updateApprovalStatus($incrementObjs,$status,$reason);
        return new JsonModel([
            'status' => 200,
            'message' => 'Updated successfully'
        ]);
    }

    public function getIncrementApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        $approvedStatus=(int)$approvedStatus;
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $incrementRepository = $this->getRepository('T055fincrementMaster');
        
        $result = $incrementRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $incrementRepository = $this->getRepository('T055fincrementMaster');

        
            $incrementObj =$incrementRepository->createIncrement($postData);
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $incrementRepository = $this->getRepository('T055fincrementMaster');
        $incrementDetailRepository = $this->getRepository('T055fincrementDetails');
        $increment         =$incrementRepository->find($id);

        if (!$increment) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $incrementObj     = $incrementRepository->updateIncrement($increment, $postData);
            $incrementDetails = $postData['increment-details'];
            // print_r($incrementObj);
            // exit();
            foreach ($incrementDetails as $incrementDetail) 
            {

                if (array_key_exists("f055fidDetails", $incrementDetail)) 
                {

                    $incrementDetailObj = $incrementDetailRepository->find((int) $incrementDetail['f055fidDetails']);

                    if ($incrementDetailObj) {
                        $incrementDetailRepository->updateIncrementDetail($incrementDetailObj, $incrementDetail);
                    }
                } else {

                    $incrementDetailRepository->createIncrementDetail($incrementObj, $incrementDetail);
                }
            $deletedIds=$postData['deletedIds'];
            

            $deleteIncrementDetails = $incrementDetailRepository->updateIncrementDetailStatus($deletedIds);

            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteIncrementAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $incrementRepository = $this->getRepository('T055fincrementDetails');
        $increment = $postData['id'];
        $incrementObjs=array();
        foreach($increment as $id)
        {
            $incrementObj =$incrementRepository->find((int)$id);
            if(!$incrementObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'increment doesnot exist.'
                ]);
            }
            array_push($incrementObjs, $incrementObj);
        }
        $increment = $incrementRepository->deleteIncrementStatus($incrementObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'Deleted successfully'
        ]);
    }

    public function getIncrementAmountAction()
    {


        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        $Glcode = $this->params()->fromRoute('idGlcode');

        $idDepartment=(int)$Department;
        $idFinancialyear=(int)$Financialyear;
        $idGlcode=(int)$Glcode;

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $budgetIncrementRepository = $this->getRepository('T055fincrementMaster');
        
        $result = $budgetIncrementRepository->getIncrementAmount($idFinancialyear,$idDepartment);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deleteBudgetIncrementDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $incrementRepository = $this->getRepository('T055fincrementDetails');
        $result            = $incrementRepository->deleteBudgetIncrementDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Budget Increment Details Deleted successfully',
        ]);
    }
  
}