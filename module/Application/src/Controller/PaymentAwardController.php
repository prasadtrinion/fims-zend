<?php

namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PaymentAwardController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }


    public function getList()
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $paymentAwardRepository = $this->getRepository('T128fpaymentAward');
        try
        {
        	$award = $paymentAwardRepository->getList();
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data'   => $award,
        ]);
    }

    public function get($id)
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $paymentAwardRepository = $this->getRepository('T128fpaymentAward');

        try
        {
        	$award = $paymentAwardRepository->find((int)$id);
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        if (!$award)
        {
            return $this->resourceNotFound();
        }

        $result = $paymentAwardRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        
        if ($this->isJSON($rawBody))
        {

            try
            {

            	$paymentAwardRepository = $this->getRepository('T128fpaymentAward');
                $awardDetailRepository = $this->getRepository('T129fpaymentAwardDetails');
            if (array_key_exists("f128fid", $postData)) 
            {
                    $awardObj = $paymentAwardRepository->find((int)$postData['f128fid']);
                        
                    $awardObj = $paymentAwardRepository->updateAwardMaster($awardObj, $postData);
                    $details = $postData['award-details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f129fid", $detail)) 
                    {
                        $detailObj = $awardDetailRepository->find((int) $detail['f129fid']);
    
                        if ($detailObj)
                        {
                            $awardDetailRepository->updateAwardDetail($detailObj, $detail);
                        }
                    }
                    else
                    {
    
                        $awardDetailRepository->createAwardDetail($awardObj, $detail);
                    }
                }
            }
            else
            {

                // $sponsorCn = $paymentAwardRepository->findBy(array("f061freferenceNumber"=>$postData['f061freferenceNumber']));
                // if ($sponsorCn)
                // {
                //     return new JsonModel([
                //         'status'  => 200,
                //         'message' => 'PAymentAward Already exist.',
                //     ]);
                // }
                $sponsorCnObj = $paymentAwardRepository->createNewData($postData);
            }

            }
            catch (\Exception $e)
            {
                echo $e;
            }

            return new JsonModel([
                'status'       => 200,
                'message'      => 'Payment Created successfully',
            ]);

        }
        else
        {
            return $this->invalidInputType();
        }

    }

  }