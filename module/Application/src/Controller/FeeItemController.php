<?php
namespace Application\Controller;

use Application\Entity\T013fcompany;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class FeeItemController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $feeItemRepository = $this->getRepository('T066ffeeItem');
        $result = $feeItemRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $Id =(int)$id;
       
        $feeItemRepository = $this->getRepository('T066ffeeItem');
        
        $feeItem =  $feeItemRepository->find($Id);
        
        if(!$feeItem)
        {
            return $this->resourceNotFound();
        }
        $result = $feeItemRepository->getListById($Id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            
             try
                {

                $feeItemRepository = $this->getRepository('T066ffeeItem');
                $feeItemDetailRepository = $this->getRepository('T066ffeeItemDetails');
                if (array_key_exists("f066fid", $postData)) 
                    {
                    $feeItemObj = $feeItemRepository->find((int)$postData['f066fid']);
                        
                    $feeItemObj =  $feeItemRepository->updateData($feeItemObj, $postData);
                $details = $postData['fee-details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f066fidDetails", $detail)) 
                    {
    
                        $detailObj = $feeItemDetailRepository->find((int) $detail['f066fidDetails']);
                        if ($detailObj) {
                            $feeItemDetailRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $feeItemDetailRepository->createDetail($feeItemObj, $detail);
                    }
                }
            }
            else
            {

                $Fee = $feeItemRepository->findBy(array("f066fcode"=>strtolower($postData['f066fcode'])));
                if ($Fee)
                {
                   return new JsonModel([
                            'status' => 409,
                            'message' => 'Fee Item Already exist.'
                        ]);
                }
                $feeItemObj = $feeItemRepository->createNewData($postData);
            }
                
                }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Fee Item Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Fee Item Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $feeItemRepository = $this->getRepository('T066ffeeItem');
        $feeItem = $feeItemRepository->find($id);

        if(!$feeItem)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $feeItemObj = $feeItemRepository->updateData($feeItem,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function getFeeItemByCategoryAction()
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $category   = $this->params()->fromRoute('id');
       
        $feeItemRepository = $this->getRepository('T066ffeeItem');
        $result = $feeItemRepository->getFeeItemByCategory($category);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deleteFeeItemDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $feeItemDetailRepository = $this->getRepository('T066ffeeItemDetails');
        $result            = $feeItemDetailRepository->deleteFeeItemDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'FeeItem Details Deleted successfully',
        ]);
    }

    public function getCreditDebitByFeeAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $feeItemDetailRepository = $this->getRepository('T066ffeeItemDetails');
        
        $result            = $feeItemDetailRepository->getCreditDebitByFee($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getFeeItemByStatusAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $feeItemRepository = $this->getRepository('T066ffeeItem');
        
                
        $result            = $feeItemRepository->getFeeItemByStatus($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}
