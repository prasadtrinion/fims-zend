<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class OnlineBankPaymentController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    { 

        $request = $this->getRequest();
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $result = $paymentRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $date = $this->params()->fromRoute('date');
        $bank = $this->params()->fromRoute('id');

        $result = $paymentRepository->getListById($date,$bank);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function onlineBankPaymentByApproveAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $id = $this->params()->fromRoute('id');

        $result = $paymentRepository->onlineBankPaymentByApprove((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $paymentRepository = $this->getRepository('T078fonlineBankPayment');

            try
            {
                $paymentObj = $paymentRepository->fileRead($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Payment Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Payment Added successfully',
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $payment = $paymentRepository->find((int)$id);

        if(!$payment)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $paymentObj = $paymentRepository->updateData($payment,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function approveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $payment = $paymentRepository->approve($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Online Bank Payments Approved successfully',
        ]);
    }

    public function knockOnlinePaymentsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $paymentRepository = $this->getRepository('T078fonlineBankPayment');
        $payment = $paymentRepository->knockOnlinePayments($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Online Bank Payments Knocked off successfully',
        ]);
    }

}