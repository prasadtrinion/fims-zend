<?php

namespace Application\Controller;

use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class LedgerController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }
    public function get($id)
    {
     
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        try{
        $ledgerRepository = $this->getRepository('T041fledger');
        $ledger = $ledgerRepository->find($id);        
        }
        catch(\Exception $e){
            echo $e;
        }
        if(!$ledger)
         {
            return $this->resourceNotFound();
        }
        $result = $ledgerRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);

    }
    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $ledgerRepository = $this->getRepository('T041fledger');
        $result = $ledgerRepository->getLedgers();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);        
    }
    public function create($postData)
    {   
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){  
             /*   try{
                    $userRepository = $this->getRepository('T014fuser');
                    $userObj = $userRepository->find($postData['user']);
                }
                catch(\Exception $e){
                    echo $e;
                }
                try{
                    $groupRepository = $this->getRepository('T012fgroup');
                    $groupObj = $groupRepository->find($postData['f041fgroup_name']);
                }
                catch(\Exception $e){
                    echo $e;
                }
                try{
                    $glcodeRepository = $this->getRepository('T014fglcode');
                    $glcodeObj = $glcodeRepository->find($postData['f041fglcode']);
                }
                catch(\Exception $e){
                    echo $e;
                }*/
                foreach ($postData as $key => $value) {
                    
                    if($value == ''){
                        return new JsonModel([
                            'status' => 199,
                            'message' => "Please provide the ".$key
                        ]);
                    }
                }
                
                try
                {
                    $ledgerRepository = $this->getRepository('T041fledger');                
                    $ledgerObj = $ledgerRepository->createLedger($postData);
                } 
                catch (\Exception $e) {

                    if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Ledger Already exist.'
                        ]);
                    }
                }
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Ledger Created successfully',
                    'ledgerObjId' => $ledgerObj->getF041fid()
                ]);

            }else{
                return $this->invalidInputType();
            }
       
    }

}