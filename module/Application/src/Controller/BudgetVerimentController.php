<?php

namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class BudgetVerimentController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request                   = $this->getRequest();
        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');
        $result                    = $budgetVerimentRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');
        $budgetVeriment = $budgetVerimentRepository ->find($id);
        if(!$budgetVeriment)
        {
            return $this->resourceNotFound();
        }
        $result = $budgetVerimentRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        // print_r($postData);
        // exit();
        
       
        
        if($this->isJSON($rawBody))
        {
            
            $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');

        
            $budgetVerimentObj = $budgetVerimentRepository->createVeriment($postData);
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Veriment Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');
        $verimentDetailRepository = $this->getRepository('T056fverimentDetails');
        $budgetVeriment           =$budgetVerimentRepository->find((int)$id);

        if (!$budgetVeriment ) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $budgetVerimentObj     = $budgetVerimentRepository->updateVeriment($budgetVeriment , $postData);
            $verimentDetails = $postData['veriment-details'];
            foreach ($verimentDetails as $verimentDetail) 
            {

                if (array_key_exists("f056fidDetails", $verimentDetail)) 
                {

                    $verimentDetailObj = $verimentDetailRepository->find((int) $verimentDetail['f056fidDetails']);

                    if ($verimentDetailObj) {
                        $verimentDetailRepository->updateVerimentDetail($verimentDetailObj, $verimentDetail);
                    }
                } else {

                    $verimentDetailRepository->createVerimentDetail($budgetVerimentObj, $verimentDetail);
                }
            $deletedIds=$postData['deletedIds'];
            

            $deleteVerimentDetails = $verimentDetailRepository->updateVerimentDetailStatus($deletedIds);
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function VerimentApprovalStatusAction()
    {

        $em       = $this->getEntityManager();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');
        $veriment                 = $postData['id'];
        $reason = $postData['reason'];
        $status = $postData['status'];
        $verimentObjs             = array();
        foreach ($veriment as $id) {
            $verimentObj = $budgetVerimentRepository->find((int) $id);
            if (!$verimentObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => ' budget veriment does not exist.',
                ]);
            }
            array_push($verimentObjs, $verimentObj);
        }
        $veriment = $budgetVerimentRepository->VerimentApprovalStatus($verimentObjs,$status,$reason);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Updated successfully',
        ]);
    }

    public function getVerimentApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');

        $approvedStatus=(int)$approvedStatus;

        $em                       = $this->getEntityManager();
        $request                  = $this->getRequest();
        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');

        
        $result = $budgetVerimentRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function getVerimentAmountAction()
    {


        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        $Glcode = $this->params()->fromRoute('idGlcode');

        $idDepartment=(int)$Department;
        $idFinancialyear=(int)$Financialyear;
        $idGlcode=(int)$Glcode;

        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $budgetVerimentRepository = $this->getRepository('T056fverimentMaster');
        
        $result = $budgetVerimentRepository->getVerimentAmount($idFinancialyear,$idDepartment,$idGlcode);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deleteBudgetVerimentDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $verimentDetailRepository = $this->getRepository('T056fverimentDetails');
        $result            = $verimentDetailRepository->deleteBudgetVerimentDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Budget Veriment Details Deleted successfully',
        ]);
    }

}
