<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class AssetExampleController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $AssetExampleRepository = $this->getRepository('T135fassetExample');
        $result = $AssetExampleRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $AssetExampleRepository = $this->getRepository('T135fassetExample');
        $sdetails = $AssetExampleRepository->find((int)$id);
        if(!$sdetails)
        {
            return $this->resourceNotFound();
        }
        $result = $AssetExampleRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        
         
          if($this->isJSON($rawBody))
        {
            
        $AssetExampleRepository = $this->getRepository('T135fassetExample');

            try
            {
                $sdObj = $AssetExampleRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $AssetExampleRepository = $this->getRepository('T135fassetExample');
        $asset = $AssetExampleRepository->find((int)$id);


        if(!$asset)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $assetObj = $AssetExampleRepository->updateData($asset,$postData);
          
           return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function getAssetByAssetSubCategoryAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $assetCategoryRepository = $this->getRepository('T087fassetCategory');

        $result            = $assetCategoryRepository->getAssetByAssetSubCategory($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getAssetSubCategoryByAssetCategoryAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $assetSubCategoryRepository = $this->getRepository('T089fassetSubCategories');

        $result            = $assetSubCategoryRepository->getAssetSubCategoryByAssetCategory($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getAssetTypeByAssetSubCategoryAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $assetTypeRepository = $this->getRepository('T090fassetType');

        $result            = $assetTypeRepository->getAssetTypeByAssetSubCategory($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    
}