<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ProcessTypeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T103fprocessType');
        $result = $purchaseRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $purchaseRepository = $this->getRepository('T103fprocessType');
        $purchase = $purchaseRepository->find($id);
        if(!$purchase)
        {
            return $this->resourceNotFound();
        }
        $result = $purchaseRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            try
            {
            
            $processRepository = $this->getRepository('T103fprocessType');
            $processDetailsRepository = $this->getRepository('T103fprocessTypeDetails');
            if (array_key_exists('f103fid', $postData)) 
            {
            $processObj = $processRepository->find((int)$postData['f103fid']);
                
            $processObj =  $processRepository->updateProcess($processObj, $postData);
        $details = $postData['process-details'];
        
        foreach ($details as $detail) 
        {

            if (array_key_exists("f103fidDetails", $detail)) 
            {

                $detailObj = $processDetailsRepository->find((int) $detail['f103fidDetails']);

                if ($detailObj) {
                    $processDetailsRepository->updateDetail($detailObj, $detail);
                }
            } else {
 

                $processDetailsRepository->createDetail($processObj, $detail);
            }
        }
    }
    else{
        $processObj = $processRepository->createNewData($postData);
    }
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => ' Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => ' Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    
   

}