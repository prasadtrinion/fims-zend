<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class UpdateAssetController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

       $request = $this->getRequest();
        $assetRepository = $this->getRepository('T091fupdateAsset');
        $result = $assetRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        // echo "asf";
        // exit;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
       
        $assetRepository = $this->getRepository('T091fupdateAsset');
        $asset = $assetRepository->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $assetRepository = $this->getRepository('T091fupdateAsset');

            try
            {
                $assetObj = $assetRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'asset Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'asset Added successfully'
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T091fupdateAsset');
        $asset = $assetRepository->find((int)$id);

        if(!$asset)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            try {
            $assetObj = $assetRepository->updateData($asset,$postData);
        }  catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'asset Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function updateAssetApprovalAction()
    {

       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $assetMovementRepository = $this->getRepository('T091fupdateAsset');
        $asset = $postData['id'];
        $assetObjs=array();

        foreach($asset as $id)
        {
           
            $assetObj =$assetMovementRepository->find((int)$id);
           
            if(!$assetObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Asset does not exist.'
                ]);
            }
            array_push($assetObjs, $assetObj);
        }
        $asset = $assetMovementRepository->assetMovementApproval($assetObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getListByApprovedStatusAction()
    {

      
        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T091fupdateAsset');
        
        $result = $assetRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

}