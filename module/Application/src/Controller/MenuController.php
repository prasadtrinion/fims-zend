<?php
namespace Application\Controller;

use Application\Entity\T011fmenu;
use Application\Repository\MenuRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class MenuController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }
    
    public function parentIdAction()
    {
        $request = $this->getRequest();
        $menuRepository = $this->getRepository('T011fmenu');
    	$result = $menuRepository->getParentId();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getModuleListAction()
    {
        $request = $this->getRequest();
        $menuRepository = $this->getRepository('T011fmenu');
        $modules = $menuRepository->getModule();
        $newModule = array();
        foreach ($modules['data'] as $module) 
        {
            
            $list=$menuRepository->getMenuList($module['f011fmodule']);
            $newModule[$module['f011fmodule']] = $list;

        }
        // print_r($newModule);
        // exit();
        return new JsonModel([
            'status' => 200,
            'result' => $newModule 
        ]);

    }

    public function getGroupMenuAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $menuRepository = $this->getRepository('T011fmenu');
        
        $result            = $menuRepository->getGroupMenu($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    
}