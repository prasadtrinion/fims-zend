<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StoreSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
         
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $storeRepository = $this->getRepository('T097fstoreSetup');
        $result = $storeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $storeRepository = $this->getRepository('T097fstoreSetup');
        $store = $storeRepository->find((int)$id);
        if(!$store)
        {
            return $this->resourceNotFound();
        }
        $result = $storeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $storeRepository = $this->getRepository('T097fstoreSetup');

            try
            {


            $Istore = $storeRepository->findBy(array("f097fstoreCode"=>strtolower($postData['f097fstoreCode'])));
             // print_r($Istore);exit;
             if($Istore)
            {
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
            }
                $storeObj = $storeRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 419,
                            'message' => 'Error While Adding.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $storeRepository = $this->getRepository('T097fstoreSetup');
        $store = $storeRepository->find((int)$id);

        if(!$store) 
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $storeObj = $storeRepository->updateData($store,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }

    public function getActiveStoreAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        // print_r($id);exit;
        $storeRepository = $this->getRepository('T097fstoreSetup');
        $result = $storeRepository->getActiveStore((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}