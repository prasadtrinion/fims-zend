<?php
namespace Application\Controller;

use Application\Entity\T058factivityCode;
use Application\Repository\ActivityCodeRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class ActivityCodeController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()

    {
        $refCode = $this->params()->fromRoute('refcode');
        
        $request = $this->getRequest();
        
        $activityCodeRepository = $this->getRepository('T058factivityCode');
        
        try
        {
            $activityCode = $activityCodeRepository->find((int)$refCode);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $activityCodeRepository->getListByRef((int)$refCode);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {

           
            
            $activityCodeRepository = $this->getRepository('T058factivityCode');
            try{
            if($postData['f058flevelStatus'] == 2 || $postData['f058flevelStatus'] == 1){
                $activityCodeObj1 = $activityCodeRepository->find((int)$postData['f058fparentCode']);
                $parentId = $activityCodeObj1->getF058fparentCode();
                $activityCode1 = $activityCodeObj1->getF058fcode();
                $activityCodeObj2 = $activityCodeRepository->find((int)$parentId);
                $activityCode2 = $activityCodeObj2->getF058fcode();
                $postData['f058fcompleteCode'] = $activityCode2.$activityCode1.$postData['f058fcode'];

            }
            }
            catch(\Exception $e){
                echo $e;
            }

            try
            {
                $activityCode = $activityCodeRepository->findBy(array("f058fcode"=>strtolower($postData['f058fcode']),"f058fparentCode"=>$postData['f058fparentCode'],"f058flevelStatus"=>$postData['f058flevelStatus']));
                
                if ($activityCode)
                {
                   return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
                $activityCodeObj = $activityCodeRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully'
                    // 'ActivityCodeId' => $activityCodeObj->getF058fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

     public function newActivityCodeAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $activityCodeRepository = $this->getRepository('T058factivityCode');
            $activities = $postData['data'];
            
            foreach ($activities as $data) 
            {
            

                try
                {
                    if($data['f058flevelStatus'] == 2)
                    {
                        
                        $activityCodeObj1 = $activityCodeRepository->find((int)$data['f058fparentCode']);
                        $parentId = $activityCodeObj1->getF058fparentCode();
                        $activityCode1 = $activityCodeObj1->getF058fcode();
                        $activityCodeObj2 = $activityCodeRepository->find((int)$parentId);
                        $activityCode2 = $activityCodeObj2->getF058fcode();
                        $data['f058fcompleteCode'] = $activityCode2.$activityCode1.$data['f058fcode'];

                    }
                }
                catch(\Exception $e)
                {
                    echo $e;
                }
                if(array_key_exists("f058fid",$data))
                {

                    $activityCode = $activityCodeRepository->find((int)$data['f058fid']);
                    if($activityCode)
                    {
                        $activityCodeObject = $activityCodeRepository->updateData($activityCode,$data);
                    }
                }   
                else
                {
                    $activity = $activityCodeRepository->findBy(array('f058fcode' => $code,'f058flevelStatus' => $level,'f058fparentCode' => (int)$data['f058fparentCode']) );

                    if($activity)
                    {
                        $flag =1;
                    }
                    else
                    {
                        $activityCodeObject = $activityCodeRepository->createNewData($data);
                    }
                    
                }
            }
            if($flag == 0)
            {
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Updated successfully',
                    
                ]);
            }
            else
            {
                return new JsonModel([
                    'status' => 409,
                    'message' => 'Already exist.'
                ]);
            }
            
            // return new JsonModel([
            //     'status' => 200,
            //     'message' => 'Activity Code updated.'
            //     ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }
    public function activityLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        
        $request = $this->getRequest();
        
        $activityCodeRepository = $this->getRepository('T058factivityCode');

        try
        {
            $activityCode = $activityCodeRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $activityCodeRepository->getListByLevel((int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function activityParentLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        $parent = $this->params()->fromRoute('parent');
        
        $request = $this->getRequest();
        
        $activityCodeRepository = $this->getRepository('T058factivityCode');

        try
        {
            $activityCode = $activityCodeRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $activityCodeRepository->getListByParentAndLevel((int)$parent,(int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function activityCodeStatusAction()

    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $id=$postData['f058fid'];
       
        $id =(int)$id;
       
        $activityRepository = $this->getRepository('T058factivityCode');
        $activity =  $activityRepository ->find($id);
        if(!$activity)
        {
            return $this->resourceNotFound();
        }
        $result = $activityRepository->activityCodeStatus($id);
        return new JsonModel([
            'status' => 200,
            'message' => 'Updated Successfully'
        ]);
    }
    public function deleteActivityCodeAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $id = $this->params()->fromRoute('id');
        $activityRepository = $this->getRepository('T058factivityCode');
        $activity = $activityRepository->find((int)$id);

        if(empty($activity))
        {
           return $this->resourceNotFound();
        }

        
        $activityRepository->updateActivity($activity);
        return new JsonModel([
            'status' => 200,
            'message' => 'Deleted successfully'
        ]);
    }

}