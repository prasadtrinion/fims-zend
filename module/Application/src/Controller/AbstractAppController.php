<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;


class AbstractAppController extends AbstractRestfulController
{
    /**
     * @var Zend\ServiceManager\ServiceManager
     */
    protected $sm;
    
    public function __construct(
        ServiceManager $sm
    ){
        $this->sm = $sm;
        $this->setUserId();
    }
    
    /**
     * Returns Doctrine Entity Manager object
     * 
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->sm->get('Doctrine\ORM\EntityManager');
    }
    
    /**
     * Get entity repository object for a given entity name
     *
     * @param string $entityName
     * @return object EntityRepository object
     *
     * @example $this->getRespository('User')
     */
    public function getRepository($entityName)
    {

        $em = $this->getEntityManager();
        
        $pos = strstr($entityName, '\\');

        if(!$pos){
            $entityName = 'Application\Entity\\' . $entityName;
        }
    
        $repo = $em->getRepository($entityName);
    
        return $repo;
    }
    
    /**
     * Get logger service
     * 
     * @return Application\Service\Logger\LoggerService
     */
    public function getLogger()
    {
        return $this->sm->get('app.logger');    
    }
    
    /**
     * Get hashids service object
     * 
     * @return Application\Service\Hashids\HashidService
     */    
    public function getHashids()
    {
        $hashids = $this->sm->get('app.hashids');
    
        return $hashids;
    }

    protected function methodNotAllowed()
    {
        $this->getResponse()->setStatusCode(405);
        return new JsonModel([
            'status' => 405,
            'message' => 'Method not allowed'
        ]);
    }

    protected function resourceNotFound()
    {
        $this->getResponse()->setStatusCode(410);
        return new JsonModel([
            'status' => 410,
            'message' => 'Resource Not Found'
        ]);
    }

    protected function invalidInputType()
    {
        $this->getResponse()->setStatusCode(422);
        return new JsonModel([
            'status' => 422,
            'message' => 'Invalid Input format'
        ]);
    }

    protected function isJSON($string){
        //echo is_array(json_decode($string, true));exit;
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
     }

     protected function setUserId()
    {
        $configRepository = $this->getRepository('T011finitialConfig');
        // echo $_SESSION['f014ftoken'];
        // die();
        $configRepository->setUserId($_SESSION['f014ftoken']);
    }
    
}