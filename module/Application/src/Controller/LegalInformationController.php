<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LegalInformationController extends AbstractAppController
{

    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $request = $this->getRequest();
        $legalRepository = $this->getRepository('T026flegalInformation');
        $result = $legalRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $legalRepository = $this->getRepository('T026flegalInformation');
        $legal = $legalRepository->find((int)$id);
        if(!$legal)
        {
            return $this->resourceNotFound();
        }
        $result = $legalRepository->getListById((int)$id);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }


    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $legalRepository = $this->getRepository('T026flegalInformation');
            
            $legal = $legalRepository->findBy(array("f026fname"=>strtolower($postData['f026fname'])));
            if($legal)
            {
                return new JsonModel([
                    'status' => 409,
                    'message' => 'Legal Data Already Exist',
                    
                ]);
            }

            try
            {
                $legalObj =$legalRepository->createLegal($postData);
            }

            catch (\Exception $e)
            {
                return new JsonModel([
                            'status' => 411,
                            'message' => 'Error While Adding Data.'
                        ]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $legalRepository = $this->getRepository('T026flegalInformation');
        $legalDetailRepository = $this->getRepository('T026flegalDetails');
        $legal = $legalRepository->find($id);

        if(!$legal) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $legalObj     = $legalRepository->updateLegal($legal, $postData);
            $legalDetails = $postData['legal-details'];
            // print_r($legalObj);
            // exit();
            foreach ($legalDetails as $legalDetail) 
            {

                if (array_key_exists("f026fidDetails", $legalDetail)) 
                {

                    $legalDetailObj = $legalDetailRepository->find((int) $legalDetail['f026fidDetails']);

                    if ($legalDetailObj) {
                        $legalDetailRepository->updatelegalDetail($legalDetailObj, $legalDetail);
                    }
                } else {

                    $legalDetailRepository->createlegalDetail($legalObj, $legalDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteLegalInformationDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $legalDetailRepository = $this->getRepository('T026flegalDetails');
        // print_r($postData);exit;
        $result            = $legalDetailRepository->deleteLegalInformationDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Legal Information Details Deleted successfully',
        ]);
    }
    
  
}
