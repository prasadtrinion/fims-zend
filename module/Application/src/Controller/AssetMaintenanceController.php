<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class AssetMaintenanceController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');

        $result = $assetMaintenanceRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');
        $assetMaintain = $assetMaintenanceRepository->find((int)$id);
        if(!$assetMaintain)
        {
            return $this->resourceNotFound();
        }
        $result = $assetMaintenanceRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');
             
            try
            {
                $assetObj = $assetMaintenanceRepository->createNewData($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');
        $assetMaintain = $assetMaintenanceRepository->find((int)$id);

        if(!$assetMaintain)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $assetObj = $assetMaintenanceRepository->updateData($assetMaintain,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function assetMaintenanceApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');
        $asset = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        // print_r($reason);exit;
        $assetObjs=array();
        foreach($asset as $id)
        {
            $assetObj =$assetMaintenanceRepository->find((int)$id);
            if(!$assetObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Does not exist.'
                ]);
            }
            array_push($assetObjs, $assetObj);
        }
        $asset = $assetMaintenanceRepository->assetMaintenanceApproval($assetObjs,$reason,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getMaintenanceApprovedListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T089fassetMaintenance');
        
        $result = $assetRepository->getMaintenanceApprovedList((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function assetNotInMaintenanceAction()
    {
        $request = $this->getRequest();

        $assetMaintenanceRepository = $this->getRepository('T089fassetMaintenance');

        $result = $assetMaintenanceRepository->assetNotInMaintenance();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    

}