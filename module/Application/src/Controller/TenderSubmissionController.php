<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class TenderSubmissionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $result = $tendorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $tendorRepository = $this->getRepository('T050ftenderSubmission');
        // $tendor = $tendorRepository->findBy(array('f050fidVendor'=>(int)$id));
        // if(!$tendor)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $tendorRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    // public function create($postData)
    // {

    //     $em = $this->getEntityManager();
    //     $request = $this->getRequest();
        

    //     $rawBody = file_get_contents("php://input");

        
    //     if($this->isJSON($rawBody))
    //     {
            
    //         try
    //             {
    //     // foreach ($postData['f037fidVendor'] as $vendor) {

    //             $tendorRepository = $this->getRepository('T050ftenderSubmission');
    //             // $tendorDetailRepository = $this->getRepository('T038ftenderRegDetails');
    //             // if (array_key_exists("f037fid", $postData)) 
    //             //     {
    //             //     $tendorObj = $tendorRepository->find((int)$postData['f037fid']);
                        
    //             //     $tendorObj =  $tendorRepository->updateMaster($tendorObj, $postData);
    //             // $details = $postData['details'];
            
    //             // foreach ($details as $detail) 
    //             // {
    
    //             //     if (array_key_exists("f038fid", $detail)) 
    //             //     {
    
    //             //         $detailObj = $tendorDetailRepository->find((int) $detail['f038fid']);
    
    //             //         if ($detailObj) {
    //             //             $tendorDetailRepository->updateDetail($detailObj, $detail);
    //             //         }
    //             //     } else {
    
    //                     // $tendorDetailRepository->createDetail($tendorObj, $detail);
    //                 // }
    //             // }
    //         // }
    //         // else{
    //             $tendorObj = $tendorRepository->createNewData($postData);
    //         // }
    //     // }
    //         }
           
    //         catch (\Exception $e)
    //         {
    //             if($e->getErrorCode()== 1062)
    //             {
    //                     return new JsonModel([
    //                         'status' => 200,
    //                         'message' => 'tendor Already exist.'
    //                     ]);
    //             }
    //         }
    //         return new JsonModel([
    //                 'status' => 200,
    //                 'message' => 'Added successfully',
    //                 'Id' => $tendorObj->getF050fid()
    //             ]);

    //     }
    //     else
    //     {
    //         return $this->invalidInputType();
    //     }
    // }

    public function getTendorDetailsAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getTenderDetails((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function getNewTendersAction()
    {
        
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        // $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getNewTenders();
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

     public function shortlistedVendorsByTenderAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->shortlistedVendorsByTender((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function awardedVendorsByTenderAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->awardedVendorsByTender((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

     public function shortlistVendorsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
            $tendorRepository = $this->getRepository('T050ftenderSubmission');

        $tendorRepository->shortlistVendors($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Vendors shortlisted successfully',
        ]);
    }

    public function awardVendorsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);

            $tendorRepository = $this->getRepository('T050ftenderSubmission');


        $tendorRepository->awardVendors($postData);
          
        return new JsonModel([
            'status'  => 200,
            'message' => 'Tenders awarded successfully',
        ]);
    }

    public function getTendorShortlistedAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getTendorShortlistedList((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

   public function getTendorAwardAction()
    {
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->getTendorAwardList((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();


        $rawBody = file_get_contents("php://input");
        $submissionRepository = $this->getRepository('T050ftenderSubmission');
        $commiteeRepository = $this->getRepository('T051ftenderCommitee');
        $remarksRepository = $this->getRepository('T053ftenderRemarks');
        
        $submissionObj = null;
        if($this->isJSON($rawBody))
        {
            
            $vendorDetails = $postData['vendor-details'];


                    foreach ($vendorDetails as $postDatas)
                    {



                        $postDatas['f050fidTendor'] =  $postData['f050fidTendor'];
                        $postDatas['f050fstatus'] =0;
                        

                        if (array_key_exists("f050fid", $postDatas)) 
                        {
                            $submissionObj = $submissionRepository->find((int)$postDatas['f050fid']);


                            if($submissionObj)
                            {    
                               
                                $submissionRepository->updateSubmission($submissionObj, $postDatas);
                            }
                           
                
                        } else
                            {

                               
                                $submissionObj = $submissionRepository->createSubmission($postDatas);
                            }
                    }

                    
                    $commiteeDetails = $postData['commitee-details'];           
        
                    foreach ($commiteeDetails as $detail) 
                    {

                        $detail['f051fidTender'] =  $postData['f050fidTendor'];
                        $detail['f051fstatus'] =0;


                        if (array_key_exists("f051fid", $detail)) 
                        {

                            $commiteeObj = $commiteeRepository->find((int) $detail['f051fid']);

                            if ($commiteeObj)   
                            {
                                $commiteeRepository->updateCommitee($commiteeObj, $detail);
                            }
                        } 
                        else 
                        {

                            $commiteeRepository->createCommitee($submissionObj, $detail);
                        }
                    } 
                    $remarkDetails = $postData['remarks'];
                          
        
                    foreach ($remarkDetails as $remark) 
                    {
                        

                        
                    if (array_key_exists("f053fid", $remark)) 
                    {
                        $remark['f050fidTendor'] =  $postData['f050fidTendor'];
                        $remarksObj = $remarksRepository->find((int)$remark['f053fid']);

                        if ($remarksObj)
                        {   
                            $remarksRepository->updateRemarks($remarksObj, $remark);
                        }
                    }
                    else 
                    {

                        $remark['f050fidTendor'] =  $postData['f050fidTendor'];
                        // print_r($remark);exit;
                        
                        
                        $remarksRepository->createRemarks($submissionObj,$remark);
                    }
                }
            //           print_r($postDatas);
            // exit;


            // if (array_key_exists("f053fid", $postData)) 
            // {
            //     $remarksObj = $remarksRepository->find((int)$postData['f053fid']);
                    
            //     $remarksRepository->updateRemarks($remarksObj, $postData);
            
                
            //         $vendorDetails = $postData['vendor-details'];
            //         foreach ($vendorDetails as $postData)
            //         {
            //             if (array_key_exists("f050fid", $postData)) 
            //             {
            //                 $submissionObj = $submissionRepository->find((int)$postData['f050fid']);

            //                 print_r($submissionObj);
            //                 exit();

            //                 if($submissionObj)
            //                 {    
            //                     $submissionRepository->updateSubmission($submissionObj, $postData);
            //                 }
            //                 else
            //                 {
            //                     $submissionRepository->createSubmission($submissionObj, $postData);
            //                 }
                
            //             }
            //         }
            //         $commiteeDetails = $postData['commitee-details'];           
        
            //         foreach ($commiteeDetails as $detail) 
            //         {

            //             if (array_key_exists("f051fid", $detail)) 
            //             {

            //                 $commiteeObj = $commiteeRepository->find((int) $detail['f051fid']);

            //                 if ($commiteeObj)   
            //                 {
            //                     $commiteeRepository->updateCommitee($commiteeObj, $detail);
            //                 }
            //             } 
            //             else 
            //             {

            //                 $commiteeRepository->createCommitee($commiteeObj, $detail);
            //             }
            //         }
                    
            // }
        

            // else
            // {

            //     $submissionObj = $submissionRepository->createNewData($postData);
            // }
            return new JsonModel([
            'status' => 200,
                    'message' => 'Updated successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }


    public function deleteTenderRemarksAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $TenderComiteeRepository = $this->getRepository('T053ftenderRemarks');
        $result            = $TenderComiteeRepository->deleteTenderRemarks($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Tender Remarks Data Deleted successfully',
        ]);
    }

    public function deleteTenderSubmissionAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $TenderSubmissionRepository = $this->getRepository('T050ftenderSubmission');
        $result            = $TenderSubmissionRepository->deleteTenderSubmission($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Tender Submission Data Deleted successfully',
        ]);
    }

    public function awardedTenderAction()
    {
        
        $request = $this->getRequest();
            $tendorRepository = $this->getRepository('T050ftenderSubmission');
        // $id = $this->params()->fromRoute('id');

            $list=$tendorRepository->awardedTender();
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }
}
