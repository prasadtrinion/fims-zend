<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class EaFormProcessController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        parent::__construct($sm);
    }
    public function getList()
    {
        $request = $this->getRequest();
        $eaFormProcessRepository = $this->getRepository('T093feaFormProcess');
        $result = $eaFormProcessRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function get($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $eaFormProcessRepository = $this->getRepository('T093feaFormProcess');
        $eaFormProcess = $eaFormProcessRepository->find((int)$id);
        if(!$eaFormProcess)
        {
            return $this->resourceNotFound();
        }
        $result = $eaFormProcessRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        if($this->isJSON($rawBody))
        {
            $eaFormProcessRepository = $this->getRepository('T093feaFormProcess');
            try
            {
                $eaFormProcessObj = $eaFormProcessRepository->createNewData($postData);
            }
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                    return new JsonModel([
                    'status' => 200,
                    'message' => 'Already exist.'
                    ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }
    public function update($id, $postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $eaFormProcessRepository = $this->getRepository('T093feaFormProcess');
        $eaFormProcess = $eaFormProcessRepository->find((int)$id);

        if(!$eaFormProcess)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            $eaFormProcessObj = $eaFormProcessRepository->updateData($eaFormProcess,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}