<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StudentBondController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $studentBondRepository = $this->getRepository('T065fstudentBondInformation');
        $result = $studentBondRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $studentBondRepository = $this->getRepository('T065fstudentBondInformation');
        $studentBond = $studentBondRepository->find((int)$id);
        if(!$studentBond)
        {
            return $this->resourceNotFound();
        }
        $result = $studentBondRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $studentBondRepository = $this->getRepository('T065fstudentBondInformation');

            try
            {
                $studentBondObj = $studentBondRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Student Bond Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Student Bond Added successfully',
                    'utilityId' => $studentBondObj->getF065fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $utilityRepository = $this->getRepository('T078futility');
        $utility = $utilityRepository->find((int)$id);

        if(!$utility)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $utilityObj = $utilityRepository->updateData($utility,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function getByApprovalStatusAction()
    {

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $studentBondRepository = $this->getRepository('T065fstudentBondInformation');
        $result = $studentBondRepository->getByApprovedStatus((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function studentBondApproveAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $studentBondRepository = $this->getRepository('T065fstudentBondInformation');
        $studentBondRepository->approve($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => 'Student Bond Approved successfully',
        ]);
    }

}