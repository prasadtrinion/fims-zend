<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class BudgetDepartAllocationController extends AbstractAppController
{
	protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {

        $request = $this->getRequest();
        $testRepository = $this->getRepository('T013fcountry');
        $test = $testRepository->find($id);
        if(!$test)
        {
            return $this->resourceNotFound();
        }
        $result = $testRepository->getCountryById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    
    
}

