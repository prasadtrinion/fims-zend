<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class SalaryCalculationController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
          $this->payrollScheduleRepository = $this->getRepository('T062fpayrollSchedule');
        $this->taxDetailsResult = $this->payrollScheduleRepository->getList();
        parent::__construct($sm);
        

    }
    public function processSalaryAction()
    {
      
        
        $employeeResult = $this->payrollScheduleRepository->getEmployeeDetails();

        // foreach($employeeResult as $employee) {
        //   //total Enumeration 
        //   $totalRenumerationArray = $this->payrollScheduleRepository->totalRenumerationForEmployee($employee['MP_STAFF_ID']);
        //    $totalRenumeration = $totalRenumerationArray[0]['totalPaid'];

        //   $accumulatedEPFAndOtherFundArray = $this->payrollScheduleRepository->accumulatedEPFAndOtherFund($employee['MP_STAFF_ID']);
        //    $accumulatedEPFAndOtherFund = $accumulatedEPFAndOtherFundArray[0]['totalPaid'];
        //   $accumulatedZakathArray = $this->payrollScheduleRepository->accumulatedZakat($employee['MP_STAFF_ID']);
        //    $accumulatedZakath = $accumulatedZakathArray[0]['totalPaid']; 

        //    $accumulatedMTDArray = $this->payrollScheduleRepository->accumulatedMTD($employee['MP_STAFF_ID']);
        //    $accumulatedMTD = $accumulatedMTDArray[0]['totalPaid']; 


           
        // }

        

        $totalRenumeration = 138000;
        $accumulatedEPFAndOtherFund = 0;
        $netAccumulatedRenumeration = ($totalRenumeration-$accumulatedEPFAndOtherFund);
        $accumulatedMTD = 0;
        $accumulatedZakath = 0;


        $currentMonthRenumeration = 11500;
        $currentMonthEPFAndOtherFund = 0;
        $currentMonthNetRenumberation = ($currentMonthRenumeration - $currentMonthEPFAndOtherFund);

        $numberOfChildrenEligible = 0;
        $marialStatus = 2;
        $personIsDisabled = false;

        if($personIsDisabled==true) {
        $deductionForDisabledIndividual = 6000;

        } else {
                  $deductionForDisabledIndividual = 0;

        }

        if($marialStatus==1) {
                  $deductionForSpouse = 0;

                } else  if($marialStatus==2) {
                  $deductionForSpouse = 4000;


                } else  if($marialStatus==3) {
                  $deductionForSpouse = 0;

                }
        $renaminingNumberOfMonth = (12 - date('m'));
        $deductionForIndividual = 9000;
        $deductionForDisabledSpouse = 0;
        $deductionForEligibleChildren = 2000;
        $otherDeductionFromPreviousEmployee = 0;
        $otherAllowableDeductionForCurrentMonth = 0;
        // Calculation for P


echo "<br/> Total Renumeration - " .$totalRenumeration;
echo "<br/> accumulatedEPFAndOtherFund - ". $accumulatedEPFAndOtherFund;
echo "<br/> currentMonthRenumeration - ". $currentMonthRenumeration;
echo "<br/> currentMonthEPFAndOtherFund - ". $currentMonthEPFAndOtherFund;
echo "<br/> currentMonthRenumeration - ". $currentMonthRenumeration;
echo "<br/> currentMonthEPFAndOtherFund - ". $currentMonthEPFAndOtherFund;
echo "<br/> renaminingNumberOfMonth - ". $renaminingNumberOfMonth;
echo "<br/> deductionForIndividual - ". $deductionForIndividual;
echo "<br/> deductionForSpouse - ". $deductionForSpouse;
echo "<br/> deductionForDisabledIndividual - ". $deductionForDisabledIndividual;
echo "<br/> deductionForDisabledSpouse - ". $deductionForDisabledSpouse;
echo "<br/> deductionForEligibleChildren - ". $deductionForEligibleChildren;
echo "<br/> numberOfChildrenEligible - ". $numberOfChildrenEligible;
echo "<br/> otherDeductionFromPreviousEmployee - ". $otherDeductionFromPreviousEmployee;
echo "<br/> otherAllowableDeductionForCurrentMonth - ". $otherAllowableDeductionForCurrentMonth;



echo "(($totalRenumeration-$accumulatedEPFAndOtherFund) 
              + ($currentMonthRenumeration-$currentMonthEPFAndOtherFund)
              + (($currentMonthRenumeration - $currentMonthEPFAndOtherFund) * $renaminingNumberOfMonth)
              - (   $deductionForIndividual 
                  + $deductionForSpouse 
                  + $deductionForDisabledIndividual 
                  + $deductionForDisabledSpouse 
                  + ($deductionForEligibleChildren * $numberOfChildrenEligible)
                  + $otherDeductionFromPreviousEmployee
                  + $otherAllowableDeductionForCurrentMonth
                )
              )";
        echo "<br/>".$p = (($totalRenumeration-$accumulatedEPFAndOtherFund) 
              + ($currentMonthRenumeration-$currentMonthEPFAndOtherFund)
              + (($currentMonthRenumeration - $currentMonthEPFAndOtherFund) * $renaminingNumberOfMonth)
              - (   $deductionForIndividual 
                  + $deductionForSpouse 
                  + $deductionForDisabledIndividual 
                  + $deductionForDisabledSpouse 
                  + ($deductionForEligibleChildren * $numberOfChildrenEligible)
                  + $otherDeductionFromPreviousEmployee
                  + $otherAllowableDeductionForCurrentMonth
                )
              );

        echo "<br/>".$m = $this->getMamount($p);
        echo "<br/>".$r = $this->getTaxPercentage($p);
        echo "<br/>".$b = $this->getAmountOnTax($p);
        echo "<br/>".$z = $accumulatedZakath;
        echo "<br/>".$x = $accumulatedMTD;
        echo "<br/>".$currentMonthMTD = (($p-$m)*$r + $b - ($z+$x))/($renaminingNumberOfMonth+1);

        $m;exit;

    }

    public function getTaxPercentage($amount){
       //print_r($this->taxDetailsResult);
        for($i=0;$i<count($this->taxDetailsResult['data']);$i++) {
            
            if(floatval($this->taxDetailsResult['data'][$i]['f062fpStart'])<=$amount &&
               floatval($this->taxDetailsResult['data'][$i]['f062fpEnd'])>=$amount) {
              return (floatval($this->taxDetailsResult['data'][$i]['f062fr'])/100);
            }
        }
    }

    public function getMamount($amount){
       //print_r($this->taxDetailsResult);exit;
        for($i=0;$i<count($this->taxDetailsResult['data']);$i++) {
            
            if(floatval($this->taxDetailsResult['data'][$i]['f062fpStart'])<=$amount &&
               floatval($this->taxDetailsResult['data'][$i]['f062fpEnd'])>=$amount) {
              return (floatval($this->taxDetailsResult['data'][$i]['f062fm']));
            }
        }
    }


    public function getAmountOnTax($amount){
       //print_r($this->taxDetailsResult);
        for($i=0;$i<count($this->taxDetailsResult['data']);$i++) {
            
            if(floatval($this->taxDetailsResult['data'][$i]['f062fpStart'])<=$amount &&
               floatval($this->taxDetailsResult['data'][$i]['f062fpEnd'])>=$amount) {
              return (floatval($this->taxDetailsResult['data'][$i]['f062fc1']));
            }
        }
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $salaryRepository = $this->getRepository('T081fsalary');
        
        $result = $salaryRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $salaryRepository = $this->getRepository('T081fsalary');
        $salary = $salaryRepository ->find($id);
        if(!$salary)
        {
            return $this->resourceNotFound();
        }
        $result = $salaryRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            

             try
            {

                $salaryRepository = $this->getRepository('T081fsalary');               
                $salaryDetailsRepository = $this->getRepository('T081fsalaryDetails');  
                
                if (array_key_exists("f081fid", $postData)) 
                {
                    $salaryObj = $salaryRepository->find((int)$postData['f081fid']);
                        
                    $salaryObj = $salaryRepository->updateSalary($salaryObj, $postData);
                    $details = $postData['salary-details'];
                    foreach ($details as $detail) 
                    {
                        
    
                        if (array_key_exists("f081fidDetails", $detail)) 
                        {
    
                            $detailObj = $salaryDetailsRepository->find((int) $detail['f081fidDetails']);
    
                            if ($detailObj) 
                            {
                                $salaryDetailsRepository->updateDetail($detailObj, $detail);
                            }
                        } 
                        else 
                        {

    
                            $salaryDetailsRepository->createDetail($salaryObj, $detail);
                        }
                    }
                }
                else
                {
                    $salary = $salaryRepository->findBy(array("f081fcode"=>strtolower($postData['f081fcode'])));
                    if ($salary)
                    {
                         return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                       
                    }
                    

                    $salaryObj = $salaryRepository->createSalary($postData);
                }
            }
            catch (Exception $e)
            {
                echo $e;
                // if($e->getErrorCode()== 1062)
                // {
                //         return new JsonModel([
                //             'status' => 409,
                //             'message' => 'Already exist.'
                //         ]);
                // }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        $id=(int)$id;
        $salaryRepository = $this->getRepository('T081fsalary');               
        $salaryDetailsRepository = $this->getRepository('T081fsalaryDetails');  


        $postData['f081fid'] = $id;
        if($this->isJSON($rawBody))
        {
            

             try
            {

          
                if (array_key_exists("f081fid", $postData)) 
                    {
                    $salaryObj = $salaryRepository->find((int)$postData['f081fid']);
                        
                    $salaryObj = $salaryRepository->updateSalary($salaryObj, $postData);
                 $details = $postData['salary-details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f081fidDetails", $detail)) 
                    {
    
                        $detailObj = $salaryDetailsRepository->find((int) $detail['f081fidDetails']);
    
                        if ($detailObj) {
                            $salaryDetailsRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $salaryDetailsRepository->createDetail($salaryObj, $detail);
                    }
                }
            }
            else{
                $salaryObj =$salaryRepository->createSalary($postData);
            }
        }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Updated successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }

    }

     public function epfCalculationAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);

        $salaryRepository = $this->getRepository('T081fsalary');
        $salary = $salaryRepository ->fetchDataFromView($postData);
        // return new JsonModel([
        //     'status' => 200,
        //     'message' => "Epf Calculated" 
        // ]);
    }

     public function moveSalaryAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);

        $salaryRepository = $this->getRepository('T081fsalary');
        $salary = $salaryRepository ->moveSalary($postData);
        return new JsonModel([
            'status' => 200,
            'message' => "Salary moved successfully" 
        ]);
    }

    public function kwapCalculationAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $salaryRepository = $this->getRepository('T081fsalary');
       
        $salary = $salaryRepository-> kwapCalculation();
        return new JsonModel([
            'status' => 200,
            'message' => "calculated successfully"
        ]);
    }

    public function getEmployeePayslipAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
                $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
       
        $salaryRepository = $this->getRepository('T081fsalary');
       
        $list = $salaryRepository-> getEmployeePayslipData($postData);
        return new JsonModel([
            'status' => 200,
            'salary' => $list
        ]);
    }

    public function employeeSalaryCalculationAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
       
        $salaryRepository = $this->getRepository('T081fsalary');
       
        $list = $salaryRepository->employeeSalaryCalculation($postData);
        return new JsonModel([
            'status' => 200,
            'salary' => $list
        ]);
    }
    
}