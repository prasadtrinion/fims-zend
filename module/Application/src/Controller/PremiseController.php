<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PremiseController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $premiseRepository = $this->getRepository('T076fpremise');
        $result = $premiseRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $premiseRepository = $this->getRepository('T076fpremise');
        $premise = $premiseRepository->find((int)$id);
        if(!$premise)
        {
            return $this->resourceNotFound();
        }
        $result = $premiseRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $premiseRepository = $this->getRepository('T076fpremise');

            $premise = $premiseRepository->findBy(array("f076fname"=>strtolower($postData['f076fname'])));
            if ($premise)
            {
                 return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
            }
            

            try
            {
                $premiseObj = $premiseRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                       echo $e;
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Premise Added successfully',
                    'premiseId' => $premiseObj->getF076fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $premiseRepository = $this->getRepository('T076fpremise');
        $premise = $premiseRepository->find((int)$id);

        if(!$premise)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $premiseObj = $premiseRepository->updateData($premise,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function approveAction(){
        
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $premiseRepository = $this->getRepository('T076fpremise');
         $premiseRepository->approve($postData);
         return new JsonModel([
                'status' => 200,
                'message' => 'Premises Approved successfully'
            ]);
    }

}