<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class VehicleLoanController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');
        $result = $vehicleLoanRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function nonEligibleGurantorsAction()
    {

        $request = $this->getRequest();
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');
        $result = $vehicleLoanRepository->nonEligibleGurantors();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
     public function getAllStaffAction()
    {

        $request = $this->getRequest();
        $vehicleLoanRepository = $this->getRepository('T034fstaffMaster');
        $result = $vehicleLoanRepository->getFullList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');
        $vehicleLoan = $vehicleLoanRepository->find((int)$id);
        if(!$vehicleLoan)
        {
            return $this->resourceNotFound();
        }
        $result = $vehicleLoanRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        // print_r($postData);
        // exit();

        
        if($this->isJSON($rawBody))
        {
            $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');
            
            try
            {
                // print_r("hi");
                // exit();
                $vehicleLoanObj = $vehicleLoanRepository->createNewData($postData);
                
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Loan Already exist.'
                        ]);
                }
            }

            if($postData['f043ffirstGuarantor']=='0'&& $postData['f043fsecondGuarantor']=='0')
            {   
                $id=$vehicleLoanObj->getF043fid();

                $vehicleObj =$vehicleLoanRepository->find((int)$id);

                $data = $vehicleLoanRepository->updateStatus($vehicleObj);
            } 
            
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Loan Added successfully',
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }


    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $vehicleLoan = $this->getRepository('T043fvehicleLoan');
        $vehicle = $vehicleLoan->find((int)$id);

        if(!$vehicle)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $vehicleLoanObj = $vehicleLoan->updateData($vehicle,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }


    public function approveVehicleLoanAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $vehicleLoans = $vehicleLoanRepository->vehicleLoanApproval($postData);

        // $id=$postData['id'];



        // $vehicleLoan = $vehicleLoanRepository->find((int)$id);

        // $secondGuarantor=$vehicleLoan->getF043fsecondGuarantor();

        // if(empty($secondGuarantor))
        // {   
        //     $data = $vehicleLoanRepository->updateLevel2($vehicleLoan);
        // } 

        return new JsonModel([
            'status' => 200,
            'message' => 'status updated successfully'
        ]);
    }

    public function approveLoanGurantorAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');
        $loanGurantorRepository       = $this->getRepository('T044floanGurantors');
        // $id= (int)$postData['id'];
        // $loan = $vehicleLoanRepository->find((int)$id);
        // if(!$loan )
        // {
        //      $this->resourceNotFound();
        // }
    
        $vehicleLoans = $vehicleLoanRepository->loanGurantorApproval($postData);

        return new JsonModel([
            'status' => 200,
            'message' => 'Approved successfully'
        ]);
    }

     public function processLoanAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');
    
        $vehicleLoans = $vehicleLoanRepository->processLoan($postData);

        return new JsonModel([
            'status' => 200,
            'message' => 'Loans Processed successfully'
        ]);
    }

     public function approvedVehicleLoansAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $result = $vehicleLoanRepository->approvedVehicleLoans();

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }



     public function getLoanGurantorsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $result = $vehicleLoanRepository->vehicleLoanList();

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

     public function getEmployeeLoansAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $result = $vehicleLoanRepository->employeeLoanList();

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
     public function getStaffLoansAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $rawBody  = file_get_contents("php://input");

        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $result = $vehicleLoanRepository->staffLoanList($id);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
     public function getLoanAmountAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $rawBody  = file_get_contents("php://input");

        $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

        $result = $vehicleLoanRepository->getLoanAmount($id);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getLoanApprovalListAction()

    {   
        
        $levelId = $this->params()->fromRoute('level');
        $statusId = $this->params()->fromRoute('status');

       

        $level=(int)$levelId;
        $status=(int)$statusId;

    
        $request = $this->getRequest();

        
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');

        
    
        $result = $vehicleLoanRepository->getLoanApprovalList($level,$status);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getLoanPendingStatusAction()

    {   
        
        $Id = $this->params()->fromRoute('id');

        $id=(int)$Id;
        
        $request = $this->getRequest();

        
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');

        
    
        $result = $vehicleLoanRepository->getLoanPendingStatus($id);

        if($result==0)
        {
           return new JsonModel([
            'status' => 210,
            'message' => "Deduct the amount"
         ]); 
        }
        else
        {
            return new JsonModel([
            'status' => 211,
            'message' => "Deduct the amount"
        ]);
        }

        
    }

    public function GuarantorListAction()

    {   
        
        
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');

        $staff1=$postData['staff'];

        $staff2=$postData['staff'];


        $result=$vehicleLoanRepository->getGuarantorList($staff1,$staff2);

        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    



    // public function rejectVehicleLoanAction()
    // {

    //     $rawBody           = file_get_contents("php://input");
    //     $postData          = json_decode($rawBody, true);
    //     $vehicleLoanRepository       = $this->getRepository('T043fvehicleLoan');

    //     $applicationIds        = $postData['id'];
    //     $level = $postData['level'];
    //     $status = $postData['status'];
    //     $reason = $postData['reason'];

    //     $applicationObjs       = array();
    //     foreach ($applicationIds as $id) 
    //     {
    //         $applicationObj = $vehicleLoanRepository->find((int) $id);

    //         if (!$applicationObj) 
    //         {
    //             return new JsonModel([
    //                 'status'  => 410,
    //                 'message' => 'Application doesnt exist.',
    //             ]);
    //         }
    //         array_push($applicationObjs, $applicationObj);

    //     }
         
    //     $application = $applicationRepository->vehicleLoanApproval($applicationObjs,$level,$status,$reason);

    //     return new JsonModel([
    //         'status'  => 200,
    //         'message' => 'Applications Rejected by level1 Approver'
    //     ]);

        
    // }



}