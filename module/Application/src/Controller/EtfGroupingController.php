<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class EtfGroupingController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $etfGroupingRepository = $this->getRepository('T138fetfGrouping');
       
        
        try{
                $etfGrouping = $etfGroupingRepository->find((int)$id);
        }
        catch(\Exception $e){
            echo $e;
        }
        if(!$etfGrouping)
        {
            return $this->resourceNotFound();
        }
        
        $result = $etfGroupingRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $etfGroupingRepository = $this->getRepository('T138fetfGrouping');
        try{
        $etfGroupingDetails = $etfGroupingRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => $etfGroupingDetails
        ]);        
    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){
               

                $etfGroupingRepository = $this->getRepository('T138fetfGrouping');
                $etfGroupingDetailRepository = $this->getRepository('T139fetfGroupingDetails');


                if (array_key_exists("f138fid", $postData)) 
                {
                    $etfGroupingObj = $etfGroupingRepository->find((int)$postData['f138fid']);
                        
                    $etfGroupingObj =  $etfGroupingRepository->updateMaster($etfGroupingObj, $postData);
                    
                    $details = $postData['details'];
            
                    foreach ($details as $detail) 
                    {
    
                        if (array_key_exists("f139fid", $detail)) 
                        {
    
                            $detailObj = $etfGroupingDetailRepository->find((int) $detail['f139fid']);
    
                            if ($detailObj)
                            {
                                $etfGroupingDetailRepository->updateDetail($detailObj, $detail);
                            }
                        }
                        else
                        {
    
                            $etfGroupingDetailRepository->createDetail($etfGroupingObj, $detail);
                        }
                    }
                }
                else
                {
                    $etfGroupingObj = $etfGroupingRepository->createNewData($postData);
                }
                
                return new JsonModel([
                    'status' => 200,
                    'message' => 'ETF Created successfully',
                    'etfGroupingId' => $etfGroupingObj->getF138fId()
                ]);

            }
            else
            {
                return $this->invalidInputType();
            }
       
    }
    public function approvedEtfGroupingsAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status = $this->params()->fromRoute('status');
        $etfGroupingRepository = $this->getRepository('T138fetfGrouping');
       
        
        $result = $etfGroupingRepository->approvedEtfGroupings((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }
    public function approveAction(){

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $etfGroupingRepository = $this->getRepository('T138fetfGrouping');
         $etfGroupingRepository->approve($postData);
         return new JsonModel([
                'status' => 200,
                'message' => 'Etfs Approved successfully'
            ]);
    }

    public function getEtfGroupingApprovalsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $etfGroupingRepository = $this->getRepository('T138fetfGrouping');


        $result = $etfGroupingRepository->getEtfGroupingApproval((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);
    }


}