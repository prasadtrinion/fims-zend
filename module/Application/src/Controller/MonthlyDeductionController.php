<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class MonthlyDeductionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $monthlyDeductionRepository = $this->getRepository('T082fmonthlyDeductionMaster');

        $result = $monthlyDeductionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $monthlyDeductionRepository = $this->getRepository('T082fmonthlyDeductionMaster');
        $asset = $monthlyDeductionRepository->find((int)$id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $monthlyDeductionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $monthlyRepository = $this->getRepository('T082fmonthlyDeductionMaster');
             
            try
            {
                $monthlyDeducitonObj = $monthlyRepository->createNewData($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }



    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $monthlyRepository = $this->getRepository('T082fmonthlyDeductionMaster');
        $monthlyDetailRepository = $this->getRepository('T082fmonthlyDeductionDetail');
        $monthly         =$monthlyRepository->find((int)$id);

        if (!$monthly)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $monthlyObj     = $monthlyRepository->updateData($monthly, $postData);
            $monthlyDetails = $postData['monthlyDeduction-details'];
            // print_r($monthlyObj);
            // exit();
            foreach ($monthlyDetails as $monthlyDetail) 
            {

                if (array_key_exists("f082fidDetails", $monthlyDetail)) 
                {

                    $monthlyDetailObj = $monthlyDetailRepository->find((int) $monthlyDetail['f082fidDetails']);


                    if ($monthlyDetailObj) {

                        $monthlyDetailRepository->updateMonthlyDetail($monthlyDetailObj, $monthlyDetail);
                    }
                } else {
                   
                    $monthlyDetailRepository->createMonthlyDetail($monthlyObj, $monthlyDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteMonthlyDeductionDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $monthlyDetailRepository = $this->getRepository('T082fmonthlyDeductionDetail');
        

        // print_r($postData);exit;
        $result            = $monthlyDetailRepository->deleteMonthlyDeductionDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Monthly Deduction Details Deleted successfully',
        ]);
    }
}