<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class StudentController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('T060fstudent');
        $result = $studentRepository->getStudents();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       // print_r($result);exit;
    }

    public function get($id)
    {
    
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->find($id);
        if(!$student)
        {
            return $this->resourceNotFound();
        }

        $gender = 'male';
        if($student->getGender() == Student::FEMALE)
        {
            $gender = 'female';
        }
        return new JsonModel([
            'status' => 200,
            'id' => $id,
            'name' =>  $student->getName(),
            'gender' => $gender,
            'email' => $student->getEmail(),
            'phone' => $student->getPhoneNumber(),
            'address' => $student->getAddress(),
            'city' => $student->getCity(),
            'state' => $student->getState(),
        ]);

    }

    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){
                if(empty($postData['name']) || empty($postData['email']) || empty($postData['city'])  || empty($postData['state']) || empty($postData['phone']) || empty($postData['address']) || empty($postData['gender']) ){
                    return $this->invalidInputType(); 
                }
                $studentRepository = $this->getRepository('T060fstudent');
                // Create account
                try{
                    $postData['gender'] = Student::MALE;
                    if($postData['gender'] == 'female')
                    {
                        $postData['gender'] = Student::FEMALE;
                    }

                    $student = $studentRepository->findBy(array("f060fmatricNumber"=>strtolower($postData['f060fmatricNumber'])));
                    if ($student)
                    {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Student Already exist.'
                        ]);
                    }

                    $studentObj = $studentRepository->createStudent($postData);
                } 
                catch (\Exception $e)
                {
                    
                        return new JsonModel([
                            'status' => 411,
                            'message' => 'Error While Adding Student.'
                        ]);
                }

                return new JsonModel([
                    'status' => 200,
                    'message' => 'Student Created successfully',
                    'studentId' => $studentObj->getId()
                ]);

            }else{
                return $this->invalidInputType();
            }
       
    }
    
    public function update($id, $postData)
    {
       //print_r($postData);exit;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->find($id);
        if(!$student)
        {
            return $this->resourceNotFound();
        }

        $gender = 'male';
        if($student->getGender() == Student::FEMALE)
        {
            $gender = 'female';
        }

        $rawBody = file_get_contents("php://input");
        if($this->isJSON($rawBody)){
            if(empty($postData['name']) || empty($postData['email']) || empty($postData['city'])  || empty($postData['state']) || empty($postData['phone']) || empty($postData['address']) || empty($postData['gender']) ){
                return $this->invalidInputType(); 
            }

            $postData['gender'] = Student::MALE;
            if($postData['gender'] == 'female')
            {
                $postData['gender'] = Student::FEMALE;
            }
            $studentObj = $studentRepository->updateStudent($student,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Student updated successfully'
            ]);
        }else{
            return $this->invalidInputType();
        }
      
    }

    public function delete($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->findBy(array('id'=>$id,'deleteFlag'=>'0'));
        if(empty($student))
        {
           return $this->resourceNotFound();
        }

        $student->setDeleteFlag('1');
        $studentRepository->update($student);
        return new JsonModel([
            'status' => 200,
            'message' => 'Student deleted successfully'
        ]);
    }

    public function studentLoginAction(){
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $studentRepository = $this->getRepository('T060fstudent');
        try
            {

                $studentObject = $studentRepository->findBy(array("f060femail" => $postData['email']));
                $studentObject = $studentObject[0];
            } catch (\Exception $e) {
                echo $e;
            }
        if(!$studentObject){
            return new JsonModel([
                'status'  => 410,
                'message' => 'Student Doesnt exist',
            ]);
        }
        $password = $studentObject->getF060fpassword();
        if(trim($password) == trim(md5($postData['password']))){
            return new JsonModel([
                'status'  => 200,
                'message' => 'Success',
                'name' => $studentObject->getF060fname(),
                'id' => $studentObject->getF060fid(),
            ]);
        }
        else{
            return new JsonModel([
                'status'  => 412,
                'message' => 'Failure'
            ]);
        }


    }

    public function generateStudentInvoiceAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->getStudent($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $student 
        ]);
    }

    public function generateRefundableStudentInvoiceAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->getRefundableStudent($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $student 
        ]);
    }
    public function sponsorStudentsAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $studentRepository = $this->getRepository('T060fstudent');
        $result = $studentRepository->sponserStudents((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }
    public function getStudentsAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $type = $this->params()->fromRoute('type');
        $studentRepository = $this->getRepository('T060fstudent');
        $result = $studentRepository->getStudents($type);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }

    public function studentInvoicesAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $studentRepository = $this->getRepository('T060fstudent');
        $result = $studentRepository->studentInvoices((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }

    public function studentIntakeBatchAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->studentIntakeBatch($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $student 
        ]);
    }

    public function taggingStudentSponsorAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $studentRepository = $this->getRepository('T060fstudent');

        $postData = json_decode($rawBody, true);

        $student = $postData['id'];
        $studentObjs=array();
        foreach($student as $id)
        {
            $studentObj =$studentRepository->find((int)$id);
            if(!$studentObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Student does not exist.'
                ]);
            }
            array_push($studentObjs, $studentObj);

        $student = $studentRepository->taggingStudentSponsor($studentObjs);
        }
        // $student = $studentRepository->taggingStudentSponsor($studentObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'Student Tagged With Sponsor.'
        ]);

    }
    public function studentInvoiceDetailAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $studentRepository = $this->getRepository('T060fstudent');
        $result = $studentRepository->studentInvoiceDetail((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }    
        
    public function getSemestersAction(){

        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $type = $this->params()->fromRoute('type');
        $semesterRepository = $this->getRepository('T053fsemester');
        $result = $semesterRepository->getSemesters($type);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);

    }

    public function studentInvoicesDetailAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $studentRepository = $this->getRepository('T060fstudent');
        $student = $studentRepository->studentInvoicesDetail($postData);

        return new JsonModel([
            'status' => 200,
            'result' => $student 
        ]);
    }

    public function getStudentByStatusAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $studentRepository = $this->getRepository('T060fstudent');
                
        $result            = $studentRepository->getStudentByStatus($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}