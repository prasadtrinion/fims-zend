<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class DisposalVerificationController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T087fdisposalVerification');
        
        $result = $assetRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $assetRepository = $this->getRepository('T087fdisposalVerification');
        // $asset = $assetRepository ->find($id);
        // if(!$asset)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $assetRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            $disposalRepository = $this->getRepository('T087fdisposalVerification');
            $disposalDetailRepository = $this->getRepository('T087fdisposalVerDetails');
            
            if(array_key_exists("f087fid", $postData)){
            $disposalObj         =$disposalRepository->find($postData['f087fid']);
            if (!$disposalObj)
            {
                $disposalRepository->createDisposal($postData);
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
               
            }
            $disposalObj     = $disposalRepository->updateDisposal($disposalObj, $postData);
            $disposalDetails = $postData['verification-details'];
            
            foreach ($disposalDetails as $disposalDetail) 
            {

                if (array_key_exists("f087fidDetails", $disposalDetail)) 
                {

                    $disposalDetailObj = $disposalDetailRepository->find((int) $disposalDetail['f087fidDetails']);

                    if ($disposalDetailObj) {
                        $disposalDetailRepository->updateDisposalDetail($disposalDetailObj, $disposalDetail);
                    }
                } else {

                    $disposalDetailRepository->createDisposalDetail($disposalObj, $disposalDetail);
                }
                }
            }
            else{
                    $disposalRepository->createDisposal($postData);
            }
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $disposalRepository = $this->getRepository('T087fdisposalVerification');
        $disposalDetailRepository = $this->getRepository('T087fdisposalVerDetails');
        $disposal         =$disposalRepository->find($id);

        if (!$disposal) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $disposalObj     = $disposalRepository->updateDisposal($disposal, $postData);
            $disposalDetails = $postData['disposal-details'];
            // print_r($incrementObj);
            // exit();
            foreach ($disposalDetails as $disposalDetail) 
            {

                if (array_key_exists("f086fidDetails", $disposalDetail)) 
                {

                    $disposalDetailObj = $disposalDetailRepository->find((int) $disposalDetail['f086fidDetails']);

                    if ($disposalDetailObj) {
                        $disposalDetailRepository->updateDisposalDetail($disposalDetailObj, $disposalDetail);
                    }
                } else {

                    $disposalDetailRepository->createDisposalDetail($disposalObj, $disposalDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    
    public function disposalGetByStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $status = $this->params()->fromRoute('id');

        $assetRepository = $this->getRepository('T087fdisposalVerification');
        
        $asset = $assetRepository->getListByVerificationStatus((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $asset
        ]);
    }
     public function verifyDisposalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $disposalRepository = $this->getRepository('T087fdisposalVerification');
        
        $disposalRepository->createDisposal($postData);
        

        return new JsonModel([
            'status' => 200,
            'message' => 'created successfully'
        ]);
    }
     public function disposalApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $disposalRepository = $this->getRepository('T087fdisposalVerification');
        
        $disposalRepository->approveDisposal($postData);
        

        return new JsonModel([
            'status' => 200,
            'message' => 'created successfully'
        ]);
    }

    public function allVerifiedDetailsAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $assetRepository = $this->getRepository('T087fdisposalVerification');
        
        $result = $assetRepository->getAllVerifiedDetails((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deleteDisposalVerDetailAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
               
        $disposalDetailRepository = $this->getRepository('T087fdisposalVerDetails');
        $disposalDetail = $disposalDetailRepository->find((int)$id);
        if(!$disposalDetail)
        {
            return $this->resourceNotFound();
        }
        $result = $disposalDetailRepository->deleteDisposalVerDetail((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => 'Deleted successfully',
        ]);
    }

    public function getEndorsmentListAction()
    {
                $em = $this->getEntityManager();
        $request = $this->getRequest();
        $disposalVerRepository = $this->getRepository('T087fdisposalVerification');
        
        $result = $disposalVerRepository->getEndorsmentList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
}