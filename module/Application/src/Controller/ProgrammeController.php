<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ProgrammeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $programmeRepository = $this->getRepository('T068fprogramme');
        $result = $programmeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function getProgrammesAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $type = $this->params()->fromRoute('type');
       
        $programmeRepository = $this->getRepository('T068fprogramme');
        $result = $programmeRepository->getProgrammes($type);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $programmeRepository = $this->getRepository('T068fprogramme');
        $programme = $programmeRepository->find($id);
        if(!$programme)
        {
            return $this->resourceNotFound();
        }
        $result = $programmeRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $programmeRepository = $this->getRepository('T068fprogramme');

            try
            {
                $programme = $programmeRepository->findBy(array("f068fcode"=>strtolower($postData['f068fcode'])));
                if ($programme)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Programme Already exist.'
                        ]);
                }
                $programmeObj = $programmeRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                        return new JsonModel([
                            'status' => 411,
                            'message' => 'Error While Adding Data'
                        ]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Programme Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $programmeRepository = $this->getRepository('T068fprogramme');
        $programme = $programmeRepository->find($id);

        if(!$programme)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $programmeObj = $programmeRepository->updateData($programme,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function getProgrammeByStatusAction()
    {

        $rawBody           = file_get_contents("php://input");
        $category   = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $programmeRepository = $this->getRepository('T068fprogramme');
                
        $result            = $programmeRepository->getProgrammeByStatus($category);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

}