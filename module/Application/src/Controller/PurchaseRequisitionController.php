<?php
namespace Application\Controller;

use Application\Entity\T015fdepartment;
use Application\Entity\T014fglcode;
use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class PurchaseRequisitionController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

    
        $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');
    
        $result = $purchaseRepository->getList();
        // print_r($result);
        // exit();
        return new JsonModel([ 
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');
        $purchase = $purchaseRepository->find($id);
        if(!$purchase)
        {
            return $this->resourceNotFound();
        }
        $result = $purchaseRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');

        
            $purchaseObj =$purchaseRepository->createPurchase($postData);
            // $Id=$purchaseObj->getF088fid();
            // $id=(int)$Id;
            // // print_r($id);
            // // exit();
            // $commitment = $purchaseRepository->updateBudgetSummary($id);
            
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $purchaseRepository       = $this->getRepository('T088fpurchaseRequisition');
        $purchaseDetailRepository = $this->getRepository('T088fpurchaseReqDetails');
        $purchase                 = $purchaseRepository->find($id);

        if (!$purchase) {
            return $this->resourceNotFound();
        }
// print_r($purchase);
// die();
        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $purchaseObj     = $purchaseRepository->updatePurchase($purchase, $postData);
            $purchaseDetails = $postData['purchase-details'];
            foreach ($purchaseDetails as $purchaseDetail) 
            {

                if (array_key_exists("f088fidDetails", $purchaseDetail)) 
                {

                    $purchaseDetailObj = $purchaseDetailRepository->find((int) $purchaseDetail['f088fidDetails']);

                    if ($purchaseDetailObj)
                    {
                        // print_r($purchaseDetailObj);
                        // die();
                        $purchaseDetailRepository->updatePurchaseDetail($purchaseDetailObj, $purchaseDetail);
                    }
                } 
                else  
                {

                    $purchaseDetailRepository->createPurchaseDetail($purchaseObj, $purchaseDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function purchaseRequisitionApproveAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $purchaseOrderRepository = $this->getRepository('T034fpurchaseOrder');
        $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');
        $purchase = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        $purchaseObjs=array();
        foreach($purchase as $id)
        {
            $purchaseObj =$purchaseRepository->find((int)$id);
            if(!$purchaseObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Purchase does not exist.'
                ]);
            }
            array_push($purchaseObjs, $purchaseObj);
        }

        $purchase = $purchaseRepository->purchaseApprove($purchaseObjs,$reason,$status,$purchaseOrderRepository);
        // echo $purchase;
        // die();
        if($purchase == 0){
            return new JsonModel([
            'status' => 411,
            'message' => 'Amount Exceeded'
        ]);
        }
        else{
           return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]); 
        }
        
    }


    public function getPurchaseApprovedListAction() 
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $ApprovedStatus = $this->params()->fromRoute('approvedStatus');
        $approvedStatus =(int)$ApprovedStatus;
        $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');
        
        $result = $purchaseRepository->getPurchaseApprovedList($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getPurchaseDetailAmountAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $purchaseRepository = $this->getRepository('T088fpurchaseRequisition');
        
        $result = $purchaseRepository->getDetailsAmount($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }



    public function getAllPurchaseGlCodeAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $Financialyear = $this->params()->fromRoute('idFinancialyear');

        $idFinancialyear=(int)$Financialyear;

        $purchaseRepository = $this->getRepository('T088fpurchaseReqDetails');
        
        $result = $purchaseRepository->getAllPurchaseGlCode($idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function deletePurchaseRequistionDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $PRRepository = $this->getRepository('T088fpurchaseReqDetails');
        $result            = $PRRepository->deletePurchaseRequistionDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Purchase Requistion Details Data Deleted successfully',
        ]);
    }

}
