<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StudentTaggingController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $studentTaggingRepository = $this->getRepository('T050fstudentTagging');
        $result = $studentTaggingRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $studentTaggingRepository = $this->getRepository('T050fstudentTagging');
        $studentTagging = $studentTaggingRepository->find((int)$id);
        if(!$studentTagging)
        {
            return $this->resourceNotFound();
        }
        $result = $studentTaggingRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $studentTaggingRepository = $this->getRepository('T050fstudentTagging');

            try
            {

                $studenttagging = $studentTaggingRepository->findBy(array("f050fpolicyNo"=>strtolower($postData['f050fpolicyNo'])));
                if ($studenttagging)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }

                $studentTaggingObj = $studentTaggingRepository->createStudentTagging($postData);
            }

           
            catch (\Exception $e)
            {
              
                        return new JsonModel([
                            'status' => 411,
                            'message' => 'Already exist.'
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentTaggingRepository = $this->getRepository('T050fstudentTagging');
        $studentTagging = $studentTaggingRepository->find((int)$id);

        if(!$studentTagging)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $studentTaggingObj = $studentTaggingRepository->updateStudentTagging($studentTagging,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

}