<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class AssetInformationController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T085fassetInformation');
        
        $result = $assetRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $assetRepository = $this->getRepository('T085fassetInformation');
        $asset = $assetRepository ->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $assetRepository = $this->getRepository('T085fassetInformation');

            if(array_key_exists('f085fid', $postData)){
            $assetObj =$assetRepository->find((int)$postData['f085fid']);
            // print_r($assetObj);
            // die();
            $assetObj = $assetRepository->updateAsset($assetObj,$postData);

            }
            else{
            $assetObj =$assetRepository->createAsset($postData);
            }
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $assetRepository = $this->getRepository('T085fassetInformationNew');


        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {
             if(array_key_exists('f085fid', $postData)){
            $assetObj =$assetRepository->find((int)$postData['f085fid']);
            // print_r($assetObj);
            // die();
            $assetObj = $assetRepository->updateNewAsset($assetObj,$postData);

            }
            else{
            $assetObj =$assetRepository->createNewAsset($postData);
            }

            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } 
        else 
        {
            return $this->invalidInputType();
        }

    }

    public function approveAssetUpdateAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $assetRepository = $this->getRepository('T085fassetInformation');
        $assetRepository->approveAssetUpdate($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'updated successfully',
        ]);
    }

     public function grnAssetAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $assetRepository = $this->getRepository('T085fassetInformation');
        $assetRepository->grnAsset($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'updated successfully',
        ]);
    }
    public function assetInformationDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $assetRepository = $this->getRepository('T085fassetInformation');
        $result = $assetRepository->assetInformationDetails((int)$id);
        
        return new JsonModel([
            'status'  => 200,
            'result' => $result,
        ]);
    }
     public function assetInformationDetailsByIdAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        
        $assetRepository = $this->getRepository('T085fassetInformation');
        $result = $assetRepository->assetInformationDetailsById($id);
        
        return new JsonModel([
            'status'  => 200,
            'result' => $result,
        ]);
    }

    public function getAssetInformationAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T085fassetInformation');
        
        $result = $assetRepository->getAssetInformation();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

     public function approveAssetInformationMirrorDataAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        // $assetRepository = $this->getRepository('T085fassetInformation');
        $depriciationRepository = $this->getRepository('T133fassetInformation');
        $result            = $depriciationRepository->approveAssetInformationMirrorData($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Asset Informaton Updated successfully',
        ]);
    }

    public function createAssetMirrorAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            // $rawBody           = file_get_contents("php://input");
            $data          = json_decode($rawBody, true);
            $assetRepository = $this->getRepository('T133fassetInformation');
            
            $data = $data['data'];
            // print_r($data);exit;

            foreach ($data as $postData)
            {
                // print_r($postData);exit;
                if(array_key_exists('f133fid', $postData)){
                $assetObj =$assetRepository->find((int)$postData['f133fid']);
            // print_r($assetObj);
            // die();
                $assetObj = $assetRepository->updateAsset($assetObj,$postData);

                }
                else
                {
                    $assetObj =$assetRepository->createAsset($postData);
                }
            
               
            }
             return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }


    public  function getAssetInformationMirrorDataAction()
    {
        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $assetRepository = $this->getRepository('T133fassetInformation');
        $result = $assetRepository->getAssetInformationMirrorData((int)$id);
        
        return new JsonModel([
            'status'  => 200,
            'result' => $result,
        ]);
    }

     public function getAssetPreRegistrationNotInAssetAction()
    {
        // print_r("fg");exit;
        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        // $assetRepository = $this->getRepository('T085fassetInformation');
        $assetRepository = $this->getRepository('T133fassetInformation');
        $result            = $assetRepository->getAssetPreRegistrationNotInAsset((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    
}