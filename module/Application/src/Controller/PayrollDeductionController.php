<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class PayrollDeductionController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $deductionRepository = $this->getRepository('T064fpayrollDeduction');
        $result = $deductionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $deductionRepository = $this->getRepository('T064fpayrollDeduction');
        $deduction = $deductionRepository->find((int)$id);
        if(!$deduction)
        {
            return $this->resourceNotFound();
        }
        $result = $deductionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         // $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $deductionRepository = $this->getRepository('T064fpayrollDeduction');
            $deductions = $postData['data'];
            
            foreach ($deductions as $data) 
            {
            
                if(array_key_exists("f064fid",$data))
                {

                    $deduction = $deductionRepository->find((int)$data['f064fid']);
                    if($deduction)
                    {
                        $deductionObject = $deductionRepository->updateData($deduction,$data);
                    }
                }   
                else
                {
                 
                    $deductionObject = $deductionRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }


   
}