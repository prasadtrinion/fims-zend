<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class VendorRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
       
        $request = $this->getRequest();
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        $result = $vendorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        $vendor = $vendorRepository->find((int)$id);
        if(!$vendor)
        {
            return $this->resourceNotFound();
        }
        $result = $vendorRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

     public function create($postData)
    {
       
        $em = $this->getEntityManager();
        
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

               
        if($this->isJSON($rawBody))
        {
            
            $vendorRepository = $this->getRepository('T030fvendorRegistration');
            $invoiceRepository = $this->getRepository('T071finvoice');
            $initialConfigRepository = $this->getRepository('T011finitialConfig');  
            
            $vendorEmail = $vendorRepository->findBy(array("f030femail"=>strtolower($postData['f030femail'])));
            if ($vendorEmail)
            {
                return new JsonModel([
                            'status' => 409,
                            'message' => 'Vendor Email Already exist.'
                        ]);
            }
            try
            {
                $vendorObj = $vendorRepository->createVendor($postData);
                $id=$vendorObj->getF030fid();
                $postData=array();
                $postData['id']=$id;

                $data=array();
                $data['type']='DOC';
        
                $invoiceNumber=$initialConfigRepository->generateFIMS($data);
        
                $invoice = $vendorRepository->createSupplierInvoice($postData,$invoiceNumber);

                
            }

           
            catch (\Exception $e)
            {
                        return new JsonModel([
                            'status' => 411,
                            'message' => 'Erroe While Adding data.'
                        ]);
             
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'vendor Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        $licenseRepository = $this->getRepository('T031flicenseInformation');
        $contactRepository = $this->getRepository('T112fcontactDetails');
        $businessRepository = $this->getRepository('T113fnatureOfBusiness');
        $mofRepository = $this->getRepository('T115fvendorMof');
        $popRepository = $this->getRepository('T127fpaytoProfile');
        $ownerRepository = $this->getRepository('T031fvendorOwner');
        $vendor = $vendorRepository->find($id);

        if(!$vendor) {
            return $this->resourceNotFound();
        }

        // $vendorEmail = $vendorRepository->findBy(array("f030femail"=>strtolower($postData['f030femail'])));
        // if ($vendorEmail)
        //     {
        //         return new JsonModel([
        //                     'status' => 409,
        //                     'message' => 'Vendor Email Already exist.'
        //                 ]);
        //     }


        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $vendorObj     = $vendorRepository->updateVendor($vendor, $postData);
            $licenseDetails = $postData['license-details'];
            $contactDetails = $postData['contact-details'];
            $businessNatures = $postData['business-nature'];
            $mofs = $postData['mof-details'];
            $pops = $postData['payto-profile'];
            $owners = $postData['owner-details'];
            
            foreach ($licenseDetails as $licenseDetail) 
            {


                if (array_key_exists("f031fid", $licenseDetail)) 
                {

                    $licenseObj = $licenseRepository->find((int) $licenseDetail['f031fid']);

                    if ($licenseObj) {
                        $licenseRepository->updateLicense($licenseObj, $licenseDetail);
                    }
                } else {
                        
                    $licenseRepository->createLicense($vendorObj, $licenseDetail);
                }
            }

            foreach ($contactDetails as $contactDetail) 
            {

                if (array_key_exists("f112fid", $contactDetail)) 
                {

                    $contactObj = $contactRepository->find((int) $contactDetail['f112fid']);

                    if ($contactObj) {
                       $contactRepository->updateContact($contactObj, $contactDetail);
                    }
                } else {
                        
                    $contactRepository->createContact($vendorObj, $contactDetail);
                }
            }

            foreach ($businessNatures as $businessNature) 
            {

                if (array_key_exists("f113fid", $businessNature)) 
                {

                    $businessNatureObj = $businessRepository->find((int) $businessNature['f113fid']);

                    if ($businessNatureObj) {
                       $businessRepository->updateBusiness($businessNatureObj, $businessNature);
                    }
                } else {
                        
                    $businessRepository->createBusiness($vendorObj, $businessNature);
                }
            }

            foreach ($mofs as $mof) 
            {

                if (array_key_exists("f115fid", $mof)) 
                {

                    $mofObj = $mofRepository->find((int) $mof['f115fid']);

                    if ($mofObj)
                    {
                       $mofRepository->updateMof($mofObj, $mof);
                    }
                } else
                {
                        
                    $mofRepository->createMof($vendorObj, $mof);
                }
            }

            foreach ($pops as $pop) 
            {

                if (array_key_exists("f127fid", $pop)) 
                {

                    $popObj = $popRepository->find((int)$pop['f127fid']);

                    if ($popObj)
                    {
                       $popRepository->updatePaytoProfile($popObj, $pop);
                    }
                } else
                {
                        
                    $popRepository->createPaytoProfile($vendorObj, $mof);
                }
            }
            foreach ($owners as $owner) 
            {

                if (array_key_exists("f031fid", $owner)) 
                {

                    $ownerObj = $ownerRepository->find((int)$owner['f031fid']);

                    if ($ownerObj)
                    {
                       $ownerRepository->updateOwner($ownerObj, $owner);
                    }
                } else
                {
                        
                    $ownerRepository->createOwner($vendorObj, $owner);
                }
            }
            
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function vendorApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        $vendor = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        $vendorObjs=array();
        foreach($vendor as $id)
        {
            $vendorObj =$vendorRepository->find((int)$id);
            if(!$vendorObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Vendor does not exist.'
                ]);
            }
            array_push($vendorObjs, $vendorObj);
        }

        $vendor = $vendorRepository->vendorApproval($vendorObjs,$reason,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getVendorApprovedListAction()
    {

        
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        
        $result = $vendorRepository->getVendorApprovedList($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function checkEmailAction()
    {

        

       $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $email = $postData['email'];

        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        
        $result = $vendorRepository->checkEmail($email);
        
        if($result){
            return new JsonModel([
            'status' => 201,
            'result' => 'Already Exist'
        ]);
        }
        else{
           return new JsonModel([
            'status' => 200,
            'result' => "Doesnt Exist"
        ]); 
        }
        
    }

    public function generateSupplierInvoiceAction()
    {

        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $invoiceRepository = $this->getRepository('T071finvoice');
        $initialConfigRepository = $this->getRepository('T011finitialConfig');

        $vendorRepository = $this->getRepository('T030fvendorRegistration');

        $data=array();
        $data['type']='DOC';
        // $type='DOC';
        // $data= $type['DOC'];
        // array_push($data, $type);
        $invoiceNumber=$initialConfigRepository->generateFIMS($data);
        
        $invoice = $vendorRepository->createSupplierInvoice($postData,$invoiceNumber);

        return new JsonModel([
            'status' => 200,
            'message' => 'Invoice generated successfully'
        ]);

    }

    public function vendorPremisesAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id   = $this->params()->fromRoute('id');
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        $result = $vendorRepository->getvendorPremises((int)$id);

            return new JsonModel([
                'status' => 200,
                'result' => $result
            ]);
       
    }

    public function vendorloginAction(){
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        try
            {

                $vendorObject = $vendorRepository->findBy(array("f030femail" => $postData['f014fuserName']));
                $vendorObject = $vendorObject[0];
            } catch (\Exception $e) {
                echo $e;
            }
        if(!$vendorObject){
            return new JsonModel([
                'status'  => 410,
                'message' => 'Vendor Doesnt exist',
            ]);
        }
        $password = $vendorObject->getF030fpassword();
        $approvedStatus = $vendorObject->getF030fapprovalStatus();
        $endDate = $vendorObject->getF030fendDate();
        $endDate1 = date("Y-m-d",strtotime($endDate));
        $later = new \DateTime();

        $diff = $later->diff($endDate)->format("%a");

        if($approvedStatus == 0){
            return new JsonModel([
                'status'  => 413,
                'message' => 'Not Approved'
            ]);
        }
        // if($diff < 14){
        //     return new JsonModel([
        //         'status'  => 414,
        //         'message' => 'Renewal'
        //     ]);
        // }
        
        if(trim($password) == trim(md5($postData['f014fpassword']))){
            return new JsonModel([
                'status'  => 200,
                'message' => 'Success',
                'token' => $vendorObject->getF030ftoken(),
                'username' => $vendorObject->getF030fcompanyName(),
                'id' => $vendorObject->getF030fid(),
                'endDate' =>$endDate,
                'endDate1' =>$endDate1,

            ]);
        }
        else{
            return new JsonModel([
                'status'  => 412,
                'message' => 'Failure'
            ]);
        }
    }

    public function getVendorActiveListAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        
        $result = $vendorRepository->getVendorList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function sendMailAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $vendorRepository = $this->getRepository('T030fvendorRegistration');
        
        $vendorRepository->sendMail();
        return new JsonModel([
            'status' => 200,
            'message' => 'mail sent'
        ]);
    }

    public function deleteVendorRegistrationAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $vendorRegistrationRepository = $this->getRepository('T030fvendorRegistration');
        $result            = $vendorRegistrationRepository->deleteVendorRegistration($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Vendor Registration Data Deleted successfully',
        ]);
    }

    public function deleteLicenseDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $licenseInformationRepository = $this->getRepository('T031flicenseInformation');
        $result            = $licenseInformationRepository->deleteLicenseDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'License Deleted successfully',
        ]);
    }

    public function deleteContactDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $contactDetailsRepository = $this->getRepository('T112fcontactDetails');
        $result            = $contactDetailsRepository->deleteContactDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Contact Deleted successfully',
        ]);
    }

    public function deleteNatureOfBusinessAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $NofBusinessRepository = $this->getRepository('T113fnatureOfBusiness');
        $result            = $NofBusinessRepository->deleteNatureOfBusiness($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'NatureOfBusiness Deleted successfully',
        ]);
    }

    public function deletevendorMofAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $vendorMofRepository = $this->getRepository('T115fvendorMof');
        $result            = $vendorMofRepository->deletevendorMof($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'vendorMof Deleted successfully',
        ]);
    }

    public function deletePaytoProfileDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $vendorPopRepository = $this->getRepository('T127fpaytoProfile');
        $result            = $vendorPopRepository->deletePaytoProfileDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'vendor Pay-To-Profile Deleted successfully',
        ]);
    }

}