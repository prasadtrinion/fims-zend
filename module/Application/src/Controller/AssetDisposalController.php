<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class AssetDisposalController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $assetDisposalRepository = $this->getRepository('T086fassetDisposal');
        $result = $assetDisposalRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $assetDisposalRepository = $this->getRepository('T086fassetDisposal');
        $asset = $assetDisposalRepository ->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetDisposalRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $assetDisposalRepository = $this->getRepository('T086fassetDisposal');
            try
            {
            	$assetObj =$assetDisposalRepository->createAsset($postData);
            }
            catch(\Exception $e)
    		{
    			return new JsonModel([
                'status'  => 409,
                'message' => 'Asset Disposal Already Exist',
            		]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);


        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();

        $assetDisposalRepository = $this->getRepository('T086fassetDisposal');
		$asset = $assetDisposalRepository->find((int)$id);

        if(!$asset)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $assetObj = $assetDisposalRepository->updateAsset($asset,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }

    }

    public function getVerfiedListAction($approvedStatus)
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $assetDisposalRepository = $this->getRepository('T086fassetDisposal');
        $assetDisposal= $assetDisposalRepository->approveAssetUpdate($approvedStatus);
        
        return new JsonModel([
            'status'  => 200,
            'message' => $assetDisposal
        ]);
    }

}