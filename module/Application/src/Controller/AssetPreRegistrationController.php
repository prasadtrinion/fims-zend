<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class AssetPreRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $assetRepository = $this->getRepository('T092fassetPreRegistration');

        $result = $assetRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $assetRepository = $this->getRepository('T092fassetPreRegistration');
        $asset = $assetRepository->find((int)$id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $assetRepository = $this->getRepository('T092fassetPreRegistration');
             
            try
            {
                $assetsObj = $assetRepository->createNewData($data);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    
    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $assetRepository = $this->getRepository('T092fassetPreRegistration');
        $assetDetailRepository = $this->getRepository('T092fpreRegistrationDetails');
        $asset         =$assetRepository->find($id);

        if (!$asset) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $assetObj     = $assetRepository->updateData($asset, $postData);
            $assetDetails = $postData['registration-details'];
            // print_r($incrementObj);
            // exit();
            foreach ($assetDetails as $assetDetail) 
            {

                if (array_key_exists("f092fidDetails", $assetDetail)) 
                {

                    $assetDetailObj = $assetDetailRepository->find((int) $assetDetail['f092fidDetails']);

                    if ($assetDetailObj) {
                        $assetDetailRepository->updateRegistrationDetail($assetDetailObj, $assetDetail);
                    }
                } else {
                   
                    $assetDetailRepository->createRegistartionDetail($assetObj, $assetDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }
}