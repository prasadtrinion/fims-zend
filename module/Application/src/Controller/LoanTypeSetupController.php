<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LoanTypeSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $LoanTypeSetupRepository = $this->getRepository('T044floanTypeSetup');
        $result = $LoanTypeSetupRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $LoanTypeSetupRepository = $this->getRepository('T044floanTypeSetup');
        $LoanTypeSetup = $LoanTypeSetupRepository->find((int)$id);
        if(!$LoanTypeSetup)
        {
            return $this->resourceNotFound();
        }
        $result = $LoanTypeSetupRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $LoanTypeSetupRepository = $this->getRepository('T044floanTypeSetup');

            try
            {
                $LoanTypeSetupObj = $LoanTypeSetupRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $LoanTypeSetupRepository = $this->getRepository('T044floanTypeSetup');
        $LoanTypeSetup = $LoanTypeSetupRepository->find((int)$id);

        if(!$LoanTypeSetup)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $LoanTypeSetupObj = $LoanTypeSetupRepository->updateData($LoanTypeSetup,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }

     public function vehicleLoanListAction()
    {

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $vehicleLoanRepository = $this->getRepository('T043fvehicleLoan');
        $result = $vehicleLoanRepository->vehicleLoanList((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
     public function loanNameNotInProcessingFeeAction()
    {

        $request = $this->getRequest();

        $vehicleLoanRepository = $this->getRepository('T021fprocessingFee');
        $result = $vehicleLoanRepository->loanNameNotInProcessingFee();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function loanNameNotInLoanGradeAction()
    {

        $request = $this->getRequest();

        $vehicleLoanRepository = $this->getRepository('T021fprocessingFee');
        $result = $vehicleLoanRepository->loanNameNotInLoanGrade();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
     public function loanNameNotInLoanParametersAction()
    {

        $request = $this->getRequest();

        $vehicleLoanRepository = $this->getRepository('T021fprocessingFee');
        $result = $vehicleLoanRepository->loanNameNotInLoanParameters();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getListByLoanNameAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $id = $this->params()->fromRoute('id');
       
        $LoanTypeSetupRepository = $this->getRepository('T044floanTypeSetup');
        // $LoanTypeSetup = $LoanTypeSetupRepository->find((int)$id);
        // if(!$LoanTypeSetup)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $LoanTypeSetupRepository->getListByLoanName((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getUserRoleAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $roleRepository = $this->getRepository('T044floanTypeSetup');
        $result = $roleRepository->getUserRole();

        return new JsonModel([ 
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getUserRoleByIdAction()
    {
        
        $em = $this->getEntityManager();
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        $roleRepository = $this->getRepository('T044floanTypeSetup');
        $result = $roleRepository->getUserRoleById($id);

        return new JsonModel([ 
            'status' => 200,
            'result' => $result 
        ]);
    }
    
}
