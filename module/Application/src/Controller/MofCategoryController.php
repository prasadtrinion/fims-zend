<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class MofCategoryController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()  
    {
         
        $request = $this->getRequest();
        $mofCategoryRepository = $this->getRepository('T114fmofCategory');
        $result = $mofCategoryRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $mofCategoryRepository = $this->getRepository('T114fmofCategory');
        $mofCategory = $mofCategoryRepository->find((int)$id);
        if(!$mofCategory)
        {
            return $this->resourceNotFound();
        }
        $result = $mofCategoryRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $mofCategoryRepository = $this->getRepository('T114fmofCategory');

            try
            {
                $mofCategoryObj = $mofCategoryRepository->createNewData($postData);
            }

            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $mofCategoryRepository = $this->getRepository('T114fmofCategory');
        $mofCategory = $mofCategoryRepository->find($id);

        if(!$mofCategory)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $mofCategoryObj = $mofCategoryRepository->updateData($mofCategory,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}