<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class InvestmentApplicationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationMaster');
        $result = $applicationRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationMaster');
        $application = $applicationRepository->find((int)$id);
        if(!$application)
        {
            return $this->resourceNotFound();
        }
        $result = $applicationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
       

        $rawBody = file_get_contents("php://input");

        $data=array();
        $data['type']='IBN';
        $initialRepository = $this->getRepository('T011finitialConfig');
        $generateNumber = $initialRepository->generateFIMS($data);
        $postData['f063fapplicationId'] = $generateNumber;
        

        
        if($this->isJSON($rawBody))
        {
            
            $applicationRepository = $this->getRepository('T063fapplicationMaster');



            try
            {
                $applicationObj = $applicationRepository->createApplication($postData);
            }

            catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => ' Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $applicationMasterRepository = $this->getRepository('T063fapplicationMaster');
        $applicationDetailRepository = $this->getRepository('T063fapplicationDetails');
        $application           =$applicationMasterRepository->find((int)$id);

        if (!$application ) 
        {
            return $this->resourceNotFound();
        }

         $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) 
        {
            try
            { 
                $applicationObj     = $applicationMasterRepository->updateApplication($application , $postData);
            }
            catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
            $applicationDetails = $postData['application-details'];
            foreach ($applicationDetails as $applicationDetail) 
            {

                if (array_key_exists("f063fidDetails", $applicationDetail)) 
                {

                    $applicationDetailObj = $applicationDetailRepository->find((int) $applicationDetail['f063fidDetails']);

                    if ($applicationDetailObj) {
                        $applicationDetailRepository->updateApplicationDetail($applicationDetailObj, $applicationDetail);
                    }
                } else {

                    $applicationDetailRepository->createApplicationDetail($applicationObj, $applicationDetail);
                }
                 $deletedIds=$postData['deletedIds'];
            

            $deleteactivityDetails = $applicationDetailRepository->updateApplicationDetailStatus($deletedIds);
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }


    public function getApprovedListAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentApplicationRepository = $this->getRepository('T063fapplicationMaster');
        
        $result = $investmentApplicationRepository->getListApproved();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }
    public function getInvestmentAccountCodesAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentApplicationRepository = $this->getRepository('T063fapplicationMaster');
        
        $result = $investmentApplicationRepository->getInvestmentAccountCodes();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function approveAllAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
       
        $applicationRepository = $this->getRepository('T063fapplicationMaster');
        $applicationIds        = $postData['id'];
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);
        }
        $application = $applicationRepository->approveAll($applicationObjs);

        return new JsonModel([
            'status'  => 200,
            'message' => 'Applications approved successfully',
        ]);

    }

    

    public function detailApprovalAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);

       
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $applicationIds        = $postData['id'];

        // $status = $postData['status'];
        // $reason = $postData['reason'];
        $applicationObjs       = array();
        foreach ($applicationIds as $id) 
        {
            $applicationObj = $applicationRepository->find((int) $id);

            if (!$applicationObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Application doesnt exist.',
                ]);
            }
            array_push($applicationObjs, $applicationObj);
        }

        $application = $applicationRepository->detailApproval($postData);

        return new JsonModel([
            'status'  => 200,
            'message' => 'status updated successfully',
        ]);

    }

    public function getInvestmentApplicationApprovalListAction()
    {   
        
        $levelId = $this->params()->fromRoute('level');
        $statusId = $this->params()->fromRoute('status');

        $level=(int)$levelId;
        $status=(int)$statusId;

        $request = $this->getRequest();

        $applicationRepository = $this->getRepository('T063fapplicationMaster');

        $result = $applicationRepository->getInvestmentApplicationApprovalList($level,$status);

        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getApplicationListAction()
    {

        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationMaster');
        $result = $applicationRepository->getApplicationList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deleteInvestmentApplicationDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result            = $applicationRepository->deleteInvestmentApplicationDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Investment Application Details Deleted successfully',
        ]);
    }

    public function getTypeOfInvestmentAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->getTypeOfInvestment((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getInvestmentBankListByMasterIdAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->getInvestmentBankListByMasterId((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function investmentGetByApprovalDetailsAction()
    {
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->investmentGetByApprovalDetails();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function investmentToPaymentVoucharAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $initialRepository = $this->getRepository('T011finitialConfig');
        $result = $applicationRepository->investmentToPaymentVouchar((int)$id,$initialRepository);
        return new JsonModel([
            'status' => 200,
            'result' => 'Investment Approved Successfully',
        ]);
    }

    public function getInvestmentSummaryAction()
    {
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->getInvestmentSummary();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function investmentApplicationDetailsAction()
    {
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->investmentApplicationDetails();
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }

    public function investmentApplicationDetailsApprovedAction()
    {
        $request = $this->getRequest();
        $applicationRepository = $this->getRepository('T063fapplicationDetails');
        $result = $applicationRepository->investmentApplicationDetailsApproved();
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }
}

?>
