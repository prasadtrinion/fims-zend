<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CreditorDebtorController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');
        $result = $creditorDebtorRepository->getCreditList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function debtorListAction()
    {

        $request = $this->getRequest();
        $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');
        $result = $creditorDebtorRepository->getDebitList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');
        if(!isset($approvedStatus)){
        $creditorDebtor = $creditorDebtorRepository->find((int)$id);
        if(!$creditorDebtor)
        {
            return $this->resourceNotFound();
        }
        }
        $result = $creditorDebtorRepository->getListById((int)$id,$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');

            try
            {
                $creditorDebtorObj = $creditorDebtorRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    'Id' => $creditorDebtorObj->getF079fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');
        $creditorDebtor = $creditorDebtorRepository->find((int)$id);

        if(!$creditorDebtor)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $creditorDebtorObj = $creditorDebtorRepository->updateCreditorDebtor($debtor,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function approveAction(){
        
        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $creditorDebtorRepository = $this->getRepository('T079fcreditorDebtor');
         $creditorDebtorRepository->approve($postData);
         return new JsonModel([
                'status' => 200,
                'message' => 'Approved successfully'
            ]);
    }

}