<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class LoanPaymentHistoryController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
     {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $loanPaymentHistoryRepository = $this->getRepository('T148floanPaymentHistory');
        try
        {
        	$loanPaymentHistoryDetails = $loanPaymentHistoryRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result' => $loanPaymentHistoryDetails
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $loanPaymentHistoryRepository = $this->getRepository('T148floanPaymentHistory');
        try
        {
                $loanPaymentHistory = $loanPaymentHistoryRepository->find((int)$id);
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        if(!$loanPaymentHistory)
        {
            return $this->resourceNotFound();
        }
        
        $result = $loanPaymentHistoryRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody))
            {
                

                $loanPaymentHistoryRepository = $this->getRepository('T148floanPaymentHistory');
              
                	try
                	{
                    	// print_r($postData);exit;
                    	$loanPaymentHistoryObj = $loanPaymentHistoryRepository->createNewData($postData);
                	}
                	catch (Exception $e)
                	{
                    	echo $e;
                	}
                
                
        	} 
              
               return new JsonModel([
                    'status' => 200,
                    'message' => 'Payment Created successfully',
                ]);
    }
    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $loanPaymentHistoryRepository = $this->getRepository('T148floanPaymentHistory');
        $loanPaymentHistory = $loanPaymentHistoryRepository->find((int)$id);

        if(!$loanPaymentHistory)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $loanPaymentHistoryObj = $loanPaymentHistoryRepository->updateData($loanPaymentHistory,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}