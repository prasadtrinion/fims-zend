<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;
 

class InvestmentRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {

        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $registration = $registrationRepository->find((int)$id);
        if(!$registration)
        {
            return $this->resourceNotFound();
        }
        $result = $registrationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $registrationRepository = $this->getRepository('T064finvestmentRegistration');
            $data=array();
            $data['type']='FD';
            $initialRepository = $this->getRepository('T011finitialConfig');
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f064freferenceNumber'] = $generateNumber;


            $master = $registrationRepository->findBy(array("f064fname"=>strtolower($postData['f064fname'])));
                
            
            if($master){
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                }
            

            try
            {
                $registrationObj = $registrationRepository->createRegistartion($postData);


            }

            catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Registered successfully',
                    
                ]);
        //     print_r("hello");
        // exit();

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function updateStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $registration = $postData['id'];
        $status = $postData['status'];
        
        $registrationObjs       = array();
        foreach ($registration as $id) 
        {
            $registrationObj = $registrationRepository->find((int) $id);

            if (!$registrationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Doesnt exist.',
                ]);
            }
            array_push($registrationObjs, $registrationObj);

        }
        $registration = $registrationRepository->updateStatus($registrationObjs,(int)$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'Status Updated successfully'
        ]);
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $registration = $registrationRepository->find((int)$id);

        if(!$registration)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            try
            {
                $registrationObj = $registrationRepository->updateRegistration($registration,$postData);
            }
            catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function getRegistrationApprovedListAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentregistrationRepository = $this->getRepository('T064finvestmentRegistration');
        
        $result = $investmentregistrationRepository->getListApproved();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    

    public function createApproveAction()
    {

        // $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody  = file_get_contents("php://input");
        $postData  = json_decode($rawBody, true);


        
        if($this->isJSON($rawBody))
        {
            
            $registrationRepository = $this->getRepository('T064finvestmentRegistration');

            try
            {
                $registrationObj = $registrationRepository->createApproval($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Registered successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

     public function reinvestmentAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");
        $postData  = json_decode($rawBody, true);


        $data=array();
        $data['type']='FD';
        $initialRepository = $this->getRepository('T011finitialConfig');
        $generateNumber = $initialRepository->generateFIMS($data);
        $postData['f064freferenceNumber'] = $generateNumber;
        
        $investmentregistrationRepository = $this->getRepository('T064finvestmentRegistration');
        
        $master = $investmentregistrationRepository->findBy(array("f064fname"=>strtolower($postData['f064fname'])));
                
            
            if($master){
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                }

        try
        {
            $result = $investmentregistrationRepository->reinvestment($postData);
        }

        catch (\Exception $e)
            {

                return new JsonModel([
                            'status' => 241,
                            'message' => 'pass two digits after decimal',
                        ]);
                
            }
        return new JsonModel([
            'status' => 200,
            'message' => 'added successfully',
        ]);        
    }
    // public function update($id, $postData)
    // {

    //     $em                      = $this->getEntityManager();
    //     $request                 = $this->getRequest();
    //     $id=(int)$id;
    //     $registrationMasterRepository = $this->getRepository('T064finvestmentRegistration');
    //     $registrationDetailRepository = $this->getRepository('T064fregistrationDetails');
    //     $registration           =$registrationMasterRepository->find((int)$id);

    //     if (!$registration) {
    //         return $this->resourceNotFound();
    //     }

    //     $rawBody = file_get_contents("php://input");

    //     if ($this->isJSON($rawBody)) {

    //         $registrationObj     = $registrationMasterRepository->updateRegistration($registration ,$postData);
    //         $registrationDetails = $postData['registration-details'];
    //         foreach ($registrationDetails as $registrationDetail) 
    //         {

    //             if (array_key_exists("f064fidDetails", $registrationDetail)) 
    //             {

    //                 $registrationDetailObj = $registrationDetailRepository->find((int) $registrationDetail['f064fidDetails']);

    //                 if ($registrationDetailObj) {
    //                     $registrationDetailRepository->updateRegistrationDetail($registrationDetailObj, $registrationDetail);
    //                 }
    //             } else {

    //                 $registrationDetailRepository->createRegistrationDetail($registrationObj, $registrationDetail);
    //             }
    //         }
    //         return new JsonModel([
    //             'status'  => 200,
    //             'message' => 'Updated successfully',
    //         ]);
    //     } else {
    //         return $this->invalidInputType();
    //     }

    // }

    public function registrationApprovalStatusAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
       
        $journalRepository = $this->getRepository('T017fjournal');
        $registrationRepository = $this->getRepository('T147finvestmentRegistrationWithdraw');
        $registrationIds        = $postData['id'];
        // $status = $postData['status'];
        // $reason = $postData['reason'];
        $registrationObjs       = array();
        foreach ($registrationIds as $id) 
        {
            $registrationObj = $registrationRepository->find((int) $id);

            if (!$registrationObj) {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Registartion doesnt exist.',
                ]);
            }
            array_push($registrationObjs, $registrationObj);
        }
        $registration = $registrationRepository->registrationApproval($postData,$journalRepository);

        return new JsonModel([
            'status'  => 200,
            'message' => 'status updated successfully',
        ]);

    }

    public function getInvestmentRegistrationApprovalListAction()
    {   
        
        $levelId = $this->params()->fromRoute('level');
        $statusId = $this->params()->fromRoute('status');

        $level=(int)$levelId;
        $status=(int)$statusId;

        $request = $this->getRequest();

        $applicationRepository = $this->getRepository('T064finvestmentRegistration');

        $result = $applicationRepository->getInvestmentRegistrationApprovalList($level,$status);

        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function updateWithdrawlStatusAction()
    {
       

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);


        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $registration = $postData['id'];
        $status = (int)$postData['status'];
        $reason=$postData['reason'];
        
        $registrationObjs       = array();
        foreach ($registration as $id) 
        {
            $registrationObj = $registrationRepository->find((int) $id);

            if (!$registrationObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'Doesnt exist.',
                ]);
            }
            array_push($registrationObjs, $registrationObj);

        }
        $registration = $registrationRepository->updateWithdrawlStatus($registrationObjs,$status,$reason);
        return new JsonModel([
            'status' => 200,
            'message' => 'Application Approved successfully'
        ]);
    }

    public function getRegistrationWithdrawlListAction()
    {   
        
        $statusId = $this->params()->fromRoute('status');

        $status=(int)$statusId;

        $request = $this->getRequest();

        $applicationRepository = $this->getRepository('T064finvestmentRegistration');

        $result = $applicationRepository->getRegistrationWithdrawlList($status);

        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getWithdrawListAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentregistrationRepository = $this->getRepository('T064finvestmentRegistration');
        
        $result = $investmentregistrationRepository->getWithdrawList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function getRegistrationListAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        
        $investmentregistrationRepository = $this->getRepository('T064finvestmentRegistration');
        
        $result = $investmentregistrationRepository->getRegistrationList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function getReinvestmentAction()
    {

        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->getReinvestmentList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function investmentRegistrationBankIdCheckAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->investmentRegistrationBankIdCheck($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }

    public function investmentRegistrationDetailsAction()
    {
        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->investmentRegistrationDetails();
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }

    public function investmentRegistrationDetailsForApprovalsAction()
    {
        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->investmentRegistrationDetailsForApprovals();
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }

    public function investmentRegistrationAllApprovalsAction()
    {
        $request = $this->getRequest();
        $registrationRepository = $this->getRepository('T064finvestmentRegistration');
        $result = $registrationRepository->investmentRegistrationAllApprovals();
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }
}

?>





