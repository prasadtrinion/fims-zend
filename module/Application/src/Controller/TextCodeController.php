<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class TextCodeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $textCodeRepository = $this->getRepository('T081ftextCode');
        $result = $textCodeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $textCodeRepository = $this->getRepository('T081ftextCode');
        $textCode = $textCodeRepository->find((int)$id);
        if(!$textCode)
        {
            return $this->resourceNotFound();
        }
        $result = $textCodeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        // $token   = $this->params()->fromRoute('repository');
        
        if($this->isJSON($rawBody))
        {
            
            $textCodeRepository = $this->getRepository('T081ftextCode');


                $master = $textCodeRepository->findBy(array("f081fcode"=>strtolower($postData['f081fcode'])));
                
            
            if($master){
                        return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
                }



            try
            {
                $textCodeObj = $textCodeRepository->createNewData($postData);
            }

            catch (\Exception $e)
            {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'TextCode Already exist.'
                        ]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'TextCode Added successfully',
                    'textCodeId' => $textCodeObj->getF081fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData) 
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $textCodeRepository = $this->getRepository('T081ftextCode');
        $textCode = $textCodeRepository->find((int)$id);

        if(!$textCode)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            try
            {
                $textCodeObj = $textCodeRepository->updateData($textCode,$postData);
            }
            catch (\Exception $e)
            {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'TextCode Already exist.'
                        ]);
            }

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function getTaxListAction()
    {   
        $em = $this->getEntityManager();
        $em       = $this->getEntityManager();
        $request  = $this->getRequest();
        // $rawBody  = file_get_contents("php://input");
        // $postData = json_decode($rawBody, true);
        $companyRepository = $this->getRepository('T013fcompany');
        $company = $companyRepository->getTaxType();

        // print_r($company);
        // exit();

        $textCodeRepository = $this->getRepository('T081ftextCode');

        $result = $textCodeRepository->taxList((int)$company['f013ftaxType']);

        return new JsonModel([
            'tax' => $result,
        ]);
        
    }



}