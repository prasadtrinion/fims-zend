<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LoanProcessingFeeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $loanProcessingFeeRepository = $this->getRepository('T142floanProcessingFee');

        $result = $loanProcessingFeeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $loanProcessingFeeRepository = $this->getRepository('T142floanProcessingFee');
        $loanProcessingFee = $loanProcessingFeeRepository->find((int)$id);
        if(!$loanProcessingFee)
        {
            return $this->resourceNotFound();
        }
        $result = $loanProcessingFeeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $loanProcessingFeeRepository = $this->getRepository('T142floanProcessingFee');
             
            try
            { 
                if (array_key_exists("f142fid", $data)) 
                {

                    $loanProcessingFeeObj = $loanProcessingFeeRepository->find((int)$data['f142fid']);

                    if ($loanProcessingFeeObj)
                    {
                    // print_r($loanProcessingFeeObj);exit;

                       $loanProcessingFeeUpdatedData =  $loanProcessingFeeRepository->updateLoan($loanProcessingFeeObj, $data);
                    }

                }
                else
                {
                	$loanProcessingFeeObj = $loanProcessingFeeRepository->createLoan($data);
                }
            }

           
            catch (Exception $e)
            {
                echo $e;
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

	public function loanProcessingFeeActiveListAction()
    {
        $request = $this->getRequest();
        $loanProcessingFeeRepository = $this->getRepository('T142floanProcessingFee');
        $id = $this->params()->fromRoute('id');

        $list=$loanProcessingFeeRepository->loanProcessingFeeActiveList((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }
    

}