<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class InsuranceSetupController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $insuranceSetupRepository = $this->getRepository('T069finsuranceSetup');
        $result = $insuranceSetupRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $insuranceSetupRepository = $this->getRepository('T069finsuranceSetup');
        $insurance = $insuranceSetupRepository ->find($id);
        if(!$insurance )
        {
            return $this->resourceNotFound();
        }
        $result = $insuranceSetupRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $insuranceSetupRepository = $this->getRepository('T069finsuranceSetup');

            try
            {
                $insuranceObj = $insuranceSetupRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Insurance Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Insurance Setup Added successfully'
                    // 'textCodeId' => $textCodeObj->getF081fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $insuranceSetupRepository = $this->getRepository('T069finsuranceSetup');
        $insurance = $insuranceSetupRepository->find($id);

        if(!$insurance)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $insuranceObj = $insuranceSetupRepository->updateData($insurance,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

}