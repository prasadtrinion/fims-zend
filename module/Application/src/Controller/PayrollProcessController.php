<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class PayrollProcessController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $payrollProcessRepository = $this->getRepository('T044fpayrollProcess');
        $result = $payrollProcessRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $payrollProcessRepository = $this->getRepository('T044fpayrollProcess');
        $payroll = $payrollProcessRepository->find((int)$id);
        if(!$payroll)
        {
            return $this->resourceNotFound();
        }
        $result = $payrollProcessRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        

        if($this->isJSON($rawBody))
        {
            
        $payrollProcessRepository = $this->getRepository('T044fpayrollProcess');

            try
            {
                $payrollObj = $payrollProcessRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $payrollProcessRepository = $this->getRepository('T044fpayrollProcess');
        $payroll = $payrollProcessRepository->find((int)$id);

        if(!$payroll)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $payrollObj = $payrollProcessRepository->updateData($payroll,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    

}