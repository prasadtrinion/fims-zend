<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class BatchController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $batchRepository = $this->getRepository('T050fbatchMaster');
        $result = $batchRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $batchRepository = $this->getRepository('T050fbatchMaster');
        $batch = $batchRepository->find((int)$id);
        if(!$batch)
        {
            return $this->resourceNotFound();
        }
        $result = $batchRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $batchRepository = $this->getRepository('T050fbatchMaster');

            try
            {
                $batchObj = $batchRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Batch Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Batch Added successfully',
                    'batchId' => $batchObj->getF050fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $batchRepository = $this->getRepository('T050fbatchMaster');
        $batch = $batchRepository->find((int)$id);

        if(!$batch)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $batchObj = $batchRepository->updateData($batch,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function nonBatchReceiptsAction()
    {

        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $batchRepository = $this->getRepository('T050fbatchMaster');
       
        $result = $batchRepository->getNonBatchReceipts($postData);

       
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);

    }

}