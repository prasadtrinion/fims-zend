<?php
namespace Application\Controller;

use Application\Entity\T021fcustomers;
use Application\Repository\CustomerRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CustomerController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T021fcustomers');
        $result = $customerRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T021fcustomers');
        $customer = $customerRepository->find((int)$id);
        if(!$customer)
        {
            return $this->resourceNotFound();
        }
        $result = $customerRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {

            if( empty($postData['f021ffirstName']))
            {
                
                return $this->invalidInputType();
            }
            
            $customerRepository = $this->getRepository('T021fcustomers');
            // $encoder = $this->sm->get('app.password_encoder');
            $pas = $this->generateRandomString();
            $passwordEncode = md5($pas);
            $postData['temp_password'] = $pas;
            $postData['f021fpassword'] = $passwordEncode;
            $customer = $customerRepository->findBy(array("f021fdebtorCode"=>strtolower($postData['f021fdebtorCode'])));
            if ($customer)
            {
                return new JsonModel([
                            'status'  => 409,
                            'message' => 'Already exist.',
                        ]);
            }


            try
            {
                $customerObj = $customerRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Saving'
                        ]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully.'
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T021fcustomers');
        $customer = $customerRepository->find((int)$id);

        if(!$customer)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            if( empty($postData['f021ffirstName']))
            {
                
                return $this->invalidInputType();
            }

            $customerObj = $customerRepository->updateCustomer($customer,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function customerLoginAction()
    {   
        $request = $this->getRequest();
        
        if ($request->isPost())
        {
            $customerRepository = $this->getRepository('T021fcustomers');
            
            $rawBody = file_get_contents("php://input");
            // print_r($rawBody);
            // exit();
            if($this->isJSON($rawBody))
            {   
            
                $postData = json_decode($rawBody,true);
                if(empty($postData['f021femailId']) || empty($postData['f021fpassword']))
                {
                    return $this->invalidInputType(); 
                }


                $email = $postData['f021femailId'];
                $password = $postData['f021fpassword'];

                $emailId = $customerRepository->findBy(array('f021femailId'=>$email));
                $pwd = $customerRepository->findBy(array('f021fpassword'=>$password));
                
                if(!$emailId)
                {
                    return new JsonModel([
                            'message' => 'Invalid EmailId'
                        ]);
                }
                elseif(!$pwd)
                {
                    return new JsonModel([
                            'message' => 'Invalid Password'
                        ]);   
                }
                $result = $customerRepository->getCustomerDetails($email,$password);
                    return new JsonModel([
                        'status' => 200,
                        'message' => "login successful",
                    ]);
                
            }
            else
            {

                return $this->invalidInputType();
            }
       
        } 
        else 
        {
            return $this->methodNotAllowed();
        }
    }
    public function customerPaymentAction(){
        $request = $this->getRequest();
        if ($request->isPost()){
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $customerRepository = $this->getRepository('T021fcustomers');
        $customer = $customerRepository->find($postData['f022fcustomer']);
        if(!$customer)
        {
            return $this->resourceNotFound();
        }
        $customerObj = $customerRepository->createNewPayment($customer,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Payment Added successfully'
            ]);
        }
        else {
            $id = $this->params()->fromRoute('id');
            $customerRepository = $this->getRepository('T021fcustomers');
            $result = $customerRepository->getPaymentData($id);
            return new JsonModel([
                'status' => 200,
                'result' => $result
            ]);
        }
    }
    
}