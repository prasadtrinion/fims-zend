<?php

namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class DepreciationSetupController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function getList()
    {
        // print_r("expression");exit;
        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $depriciationRepository = $this->getRepository('T015fdepreciationSetup');
        try
        {
            $sponsorCnDetails = $depriciationRepository->getList();
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'result'   => $sponsorCnDetails,
        ]);
    }

    public function get($id)
    {

        $em                   = $this->getEntityManager();
        $request              = $this->getRequest();
        $depriciationRepository = $this->getRepository('T015fdepreciationSetup');

        try
        {
            $depriciation = $depriciationRepository->find((int)$id);
        }
        catch (\Exception $e)
        {
            echo $e;
        }
        if (!$depriciation)
        {
            return $this->resourceNotFound();
        }

        $result = $depriciationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);

    }

    public function create($postData)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
        
        if ($this->isJSON($rawBody))
        {
            $depriciationData = $postData;

            try
            {
            	// $depriciationDatas = $postData['data'];
            	// foreach ($depriciationDatas as $depriciationData)
            	// {
            		$depriciationRepository = $this->getRepository('T015fdepreciationSetup');

            		if (array_key_exists("f015fid", $depriciationData)) 
                    {
                    	$depriciationObj = $depriciationRepository->find((int)$depriciationData['f015fid']);
                    	if (!$depriciationObj)
                    	{
                    		return $this->resourceNotFound();
                    	}
                    	$depriciationObj = $depriciationRepository->updateData($depriciationObj, $depriciationData);
                	}
                	else
                		{
                			// print_r($depriciationData);exit;
                			$depriciationObj = $depriciationRepository->createNewData($depriciationData);
            			}

            	// }
            }
            catch (\Exception $e)
            {
            	echo $e;
            }

            return new JsonModel([
                'status'       => 200,
                'message'      => 'Depriation Created successfully',
                // 'sponsorCnId' => $sponsorCnObj->getF061fId(),
            ]);

        }
        else
        {
            return $this->invalidInputType();
        }

    }

    public function deleteDepreciationSetupAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $depriciationRepository = $this->getRepository('T015fdepreciationSetup');
        $result            = $depriciationRepository->deleteDepreciationSetup($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Depriciation Deleted successfully',
        ]);
    }
}