<?php 
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;  

class ItemController extends AbstractAppController
{
    protected $sm;  


    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        parent::__construct($sm);
    }

    public function getList()
    {
        

        $request = $this->getRequest();
        $itemRepository = $this->getRepository('T088fitem');
        
        $result = $itemRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function allItemsAction()
    {
        

        $request = $this->getRequest();
        $itemRepository = $this->getRepository('T088fitem');
        
        $result = $itemRepository->allItems();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function allCategoriesAction()
    {
        

        $request = $this->getRequest();
        $itemRepository = $this->getRepository('T088fitem');
        
        $result = $itemRepository->allCategories();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }


    
    public function get($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $itemRepository = $this->getRepository('T088fitem');
        $item = $itemRepository->find((int)$id);
        if(!$item)
        {
            return $this->resourceNotFound();
        }
        $result = $itemRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        if($this->isJSON($rawBody))
        {
            $itemRepository = $this->getRepository('T088fitem');

            try
            {

                $item = $itemRepository->findBy(array("f088fitemCode"=>strtolower($postData['f088fitemCode'])));
                if ($item)
                {
                    return new JsonModel([
                    'status' => 409,
                    'message' => 'Already exist.'
                    ]);
                }
                $itemObj = $itemRepository->createNewData($postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                    return new JsonModel([
                    'status' => 409,
                    'message' => 'Already exist.'
                    ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $itemRepository = $this->getRepository('T088fitem');
        $item = $itemRepository->find((int)$id);
        if(!$item)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            $itemObj = $itemRepository->updateData($item,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
    public function getSubCategoryAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $category   = $this->params()->fromRoute('category');
        $itemSubRepository = $this->getRepository('T028fitemSubCategory');
        
        
        $result = $itemSubRepository->getSubCategory((int)$category);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getItemBySubCategoryAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $category   = $this->params()->fromRoute('subCategory');
        $itemSubRepository = $this->getRepository('T028fitemSubCategory');
        
        
        $result = $itemSubRepository->getItemBySubCategory((int)$category);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
      public function assetAmountIncreaseAction()
    {

        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $itemRepository = $this->getRepository('T088fitem');
        $result = $itemRepository->assetAmountIncrease($postData);
        return new JsonModel([
            'status' => 200,
            'result' => "Updated successfully" 
        ]);
    }
      public function assetLifeIncreaseAction()
    {

        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $itemRepository = $this->getRepository('T088fitem');
        $result = $itemRepository->assetLifeIncrease($postData);
        return new JsonModel([
            'status' => 200,
            'result' => "Updated successfully" 
        ]);
    }

    public function getItemSetUpBySubCategoryAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $rawBody           = file_get_contents("php://input");
        $itemSetupRepository = $this->getRepository('T029fitemSetUp');
        $result = $itemSetupRepository->getItemSetUpBySubCategory($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result, 
        ]);
    }
}