<?php

namespace Application\Controller;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class DebitNoteController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $debitNoteRepository = $this->getRepository('T031fdebitNote');
       
        
        try{
                $debitNote = $debitNoteRepository->find((int)$id);
        }
        catch(\Exception $e){
            echo $e;
        }
        if(!$debitNote)
        {
            return $this->resourceNotFound();
        }
        
        $result = $debitNoteRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }

    public function getList(){

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $debitNoteRepository = $this->getRepository('T031fdebitNote');
        try{
        $debitNoteDetails = $debitNoteRepository->getList();
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        return new JsonModel([
            'status' => 200,
            'data' => $debitNoteDetails
        ]);        
    }

    public function create($postData)
    {   

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){
                $details = $postData['details'];
        $totalAmount = 0;
        foreach ($details as $detail) {
            $totalAmount = $totalAmount + $detail['f032ftotal'];
        }
        // try{
        //     $invoiceRepository = $this->getRepository('T071finvoice');

        //         $invoiceObj = $invoiceRepository->find((int)$postData['f031fidInvoice']);

        // }
        // catch(\Exception $e){
        //     echo $e;
        // }

        // $invoiceAmount = $invoiceObj->getF071fbalance();

        // $balance = $invoiceAmount - $totalAmount;
        
        // $invoiceObj->setF071fbalance((float)$balance);

        // try{
        // $em->persist($invoiceObj);
        //         $em->flush();
        //     }
        // catch(\Exception $e){
        //     echo $e;
        // }                
                try
                {

                $debitNoteRepository = $this->getRepository('T031fdebitNote');
                $debitNoteDetailRepository = $this->getRepository('T032fdebitNoteDetails');
                if (array_key_exists("f031fid", $postData)) 
                    {
                    $debitNoteObj = $debitNoteRepository->find((int)$postData['f031fid']);
                        
                    $debitNoteObj =  $debitNoteRepository->updateMaster($debitNoteObj, $postData);
                $details = $postData['details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f032fid", $detail)) 
                    {
    
                        $detailObj = $debitNoteDetailRepository->find((int) $detail['f032fid']);
    
                        if ($detailObj) {
                            $debitNoteDetailRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $debitNoteDetailRepository->createDetail($debitNoteObj, $detail);
                    }
                }
            }
            else{
                $debitNoteObj = $debitNoteRepository->createNewData($postData);
            }
                
                } 
              
                catch (\Exception $e) {

                    if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Debit Note Already exist.'
                        ]);
                    }
                }



                return new JsonModel([
                    'status' => 200,
                    'message' => 'Debit Note Created successfully',
                    'debitNoteId' => $debitNoteObj->getF031fId()
                ]);

            }else{
                return $this->invalidInputType();
            }
       
    }
    public function approvedDebitNotesAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status = $this->params()->fromRoute('status');
        $debitNoteRepository = $this->getRepository('T031fdebitNote');
       
        
        $result = $debitNoteRepository->approvedDebitNotes((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result

        ]);

    }
    public function approveAction(){

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);
         $debitNoteRepository = $this->getRepository('T031fdebitNote');
         $journalRepository = $this->getRepository('T017fjournal');         
         $debitNoteRepository->approve($postData,$journalRepository);
         return new JsonModel([
                'status' => 200,
                'message' => 'Debit Notes Approved successfully'
            ]);
    }

    public function getDebitNoteApprovalsAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $debitNoteRepository = $this->getRepository('T031fdebitNote');


        $result = $debitNoteRepository->getDebitNoteApproval((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result,

        ]);
    }


}