<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PaymentVoucherController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result = $paymentVoucherRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');
        $paymentVoucher = $paymentVoucherRepository->find($id);
        if(!$paymentVoucher)
        {
            return $this->resourceNotFound();
        }
        $result = $paymentVoucherRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');

            try
            {

                $paymentvouchar = $paymentVoucherRepository->findBy(array('f093fpaymentRefNo'=>strtolower($postData['f093fpaymentRefNo'])));
               
                if ($paymentvouchar)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Payment Voucher Already exist.'
                        ]);

                }
                $paymentVoucherObj = $paymentVoucherRepository->createPaymentVoucher($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Adding Payment Voucher.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Payment Voucher Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');
        $paymentVoucherDetailsRepository = $this->getRepository('T094fpaymentVoucherDetails');
        $paymentVoucher = $paymentVoucherRepository->find($id);

        if (!$paymentVoucher) 
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {
	
            $paymentVoucherObj     = $paymentVoucherRepository->updatePaymentVoucher($paymentVoucher, $postData);
            $paymentVoucherDetails = $postData['paymentVoucher-details'];
            foreach ($paymentVoucherDetails as $paymentVoucherDetail) 
            {

                if (array_key_exists("f094fid", $paymentVoucherDetail)) 
                {
			//$id= $paymentVoucherDetail['f094fid'];
                    $paymentVoucherDetailObj = $paymentVoucherDetailsRepository->find((int)$paymentVoucherDetail['f094fid']);
                    // print_r($paymentVoucherDetailObj);exit;

                    if ($paymentVoucherDetailObj) {
                        $paymentVoucherDetailsRepository->updatePaymentVoucherDetail($paymentVoucherDetailObj, $paymentVoucherDetail);
                    }
                } else {

                    $paymentVoucherDetailsRepository->createPaymentVoucherDetail($paymentVoucherObj, $paymentVoucherDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function PaymentApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');
        $paymentVoucher = $postData['id'];
        $status = $postData['status'];
        $reason = $postData['reason'];
        $paymentVoucherObjs=array();
        foreach($paymentVoucher as $id)
        {

            $paymentVoucherObj =$paymentVoucherRepository->find((int)$id);
            if(!$paymentVoucherObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Payment Voucher doesnot exist.'
                ]);
            }
            array_push($paymentVoucherObjs, $paymentVoucherObj);
        }
        $paymentVoucher = $paymentVoucherRepository->updateApprovalStatus($paymentVoucherObjs,$status,$reason);

        $journalData = $paymentVoucherRepository->postGl($postData);
        
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }
    public function getPaymentApprovalListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $paymentVoucherRepository = $this->getRepository('T093fpaymentVoucher');
        // print_r($approvedStatus);exit;
        
        $result = $paymentVoucherRepository->getListByApprovedStatus((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

   public function getClaimcustomerVoucherAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getClaimcustomerVouchers((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function getClaimcustomerTypeAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getClaimcustomerType((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function getStudentVoucherAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getStudentVouchers((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function getStudentTypeAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getStudentType((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function getVoucherListAction()
    {

        $id                = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getVoucherDetails((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function getPaymentVoucherByVendorAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);
        $voucherRepository = $this->getRepository('T093fpaymentVoucher');
        $result            = $voucherRepository->getPaymentVoucherByVendor($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

}
