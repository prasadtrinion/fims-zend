<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class PayrollContributionController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $contributionRepository = $this->getRepository('T061fpayrollContribution');
        $result = $contributionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $contributionRepository = $this->getRepository('T061fpayrollContribution');
        $contribution = $contributionRepository->find((int)$id);
        if(!$contribution)
        {
            return $this->resourceNotFound();
        }
        $result = $contributionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         // $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $contributionRepository = $this->getRepository('T061fpayrollContribution');
            $contributions = $postData['data'];
            
            foreach ($contributions as $data) 
            {
            
                if(array_key_exists("f061fid",$data))
                {

                    $contribution = $contributionRepository->find((int)$data['f061fid']);
                    if($contribution)
                    {
                        $contributionObject = $contributionRepository->updateData($contribution,$data);
                    }
                }   
                else
                {
                 
                    $contributionObject = $contributionRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    
   
}