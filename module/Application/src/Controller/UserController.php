<?php
namespace Application\Controller;

use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class UserController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    /**
     * Action to display user listing page
     *
     * URL: /users/
     */
    public function indexAction()
    {
        $this->layout()->setTemplate('layout/left-sidebar-layout');
    }

    /**
     * Action to call via Ajax for DataTable server-side processing.
     *
     * URL: /users/list
     */
    public function listAction()
    {
        $request = $this->getRequest();
        
        if (! $request->isXmlHttpRequest() && ! $request->isPost()) {
            return $this->createHttpNotFoundModel($this->getResponse());
        }
        
        $authService = $this->sm->get('app.authentication');
        $user = $authService->getIdentity();
        $userRepository = $this->getRepository('User');
        $roleDisplayNames = $userRepository->getRoleDisplayNames();
        $postData = $this->getRequest()
            ->getPost()
            ->toArray();
        $columns = $this->params()->fromPost('columns', array());
        $order = $this->params()->fromPost('order');
        $filters = $this->params()->fromPost('filters', array());
        $userRepo = $this->getRepository('User');
        $order = $order[0];
        $sort = array(
            'column' => $columns[$order['column']]['name'],
            'order' => $order['dir']
        );
        
        $search = $postData['search']['value'];
        
        if ($search) {
            switch ($search) {
                case stripos($search, 'distributor'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 2;
                    }
                    break;
                case stripos($search, 'system admin'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 1;
                    }
                    break;
                case stripos($search, 'end user'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 3;
                    }
                    break;
                
                case stripos($search, 'Unverified'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 0;
                    }
                    break;
                
                case stripos($search, 'Rejected'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = - 1;
                    }
                    break;
                case stripos($search, 'user'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 3;
                    }
                    break;
                case stripos($search, 'admin'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 4;
                    }
                    break;
                case stripos($search, 'read-only user'):
                    if (! isset($filters['status'])) {
                        $search = null;
                        $filters['status'] = 1;
                        $filters['role'] = 5;
                    }
                    break;
                
                default:
                    break;
            }
        }
        
        $users = $userRepo->getUsers($filters, $search, $sort, $postData['start'], $postData['length'], $user['userId']);
        $data = $users['data'];
        $total = $users['total'];
        
        $hashids = $this->getHashids();
        
        foreach ($data as &$row) {
            $userId = $hashids->encode($row['id']);
            $roleName = $roleDisplayNames[$row['roleId']];
            
            $status = array(
                'label' => $roleName,
                'label_class' => 'label-success',
                'icon_class' => 'fa-check'
            );
            
            if ($row['status'] == - 1) {
                $status = array(
                    'label' => 'Rejected',
                    'label_class' => 'label-warning',
                    'icon_class' => 'fa-warning'
                );
            }
            
            if (! $row['status']) {
                $status = array(
                    'label' => 'Unverified',
                    'label_class' => 'label-warning',
                    'icon_class' => 'fa-warning'
                );
            }
            
            $row['id'] = $userId;
            $row['updateDtTm'] = $row['updateDtTm']->format('m-d-Y');
            $row['viewUrl'] = $this->url()->fromRoute('users/view', array(
                'id' => $userId
            ));
            $row['editUrl'] = $this->url()->fromRoute('users/edit', array(
                'id' => $userId
            ));
            $row['cloneUrl'] = $this->url()->fromRoute('users/create') . '?copy=' . $userId;
            $row['status'] = '<span class="label ' . $status['label_class'] . '"><i class="fa ' . $status['icon_class'] . '"></i>&nbsp;' . $status['label'] . '</span>';
        }

        $viewParams = array(
            "draw" => $postData['draw'],
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            'data' => $data
        );
        
        return new JsonModel($viewParams);
    }

    /**
     * Action to view user information
     *
     * URL: /users/view/:id
     */
    public function viewAction() 
    {
        $hashId = $this->params()->fromRoute('id');
        $id = $this->getHashids()->decode($hashId);
    
        $userRepository = $this->getRepository('User');
        $roleDisplayNames = $userRepository->getRoleDisplayNames();
		$dsrTypes = $userRepository->getDsrTypes();
        $user = $userRepository->getUserDetails($id);
    
        $viewParams = [
            'user' => $user,
            'roleDisplayNames' => $roleDisplayNames,
		    'dsrTypes' => $dsrTypes
        ];
    
        $this->layout()->setTemplate('layout/left-sidebar-layout');
        $view = new ViewModel($viewParams);
    
        return $view;
    }
    
    /**
     * Action to create user information
     *
     * URL: /users/create
     */
    public function createAction() {
        $authService = $this->sm->get('app.authentication');
        $authUser = $authService->getIdentity();
        $em = $this->getEntityManager();
        $stateList = LocationRepository::getStatesByCountry();
        $formOptions = array(
            'state' => $stateList,
            'action' => 'admin_user_create',
            'userRole' => $authUser['role']
        );
        $form = new UserForm($em, $formOptions);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $postParams = $request->getPost()->toArray();
            $form->setData($postParams);
            
            if ($form->isValid()) {
                $accountRepository = $this->getRepository('Account');
                
                if (! $accountRepository->isAccountExist($postParams['username'])) {
                    $encoder = $this->sm->get('app.password_encoder');
                    $plainPassword = $postParams['password'];
                    $passwordEncode = $encoder->encode($plainPassword);
                    $postParams['password'] = $passwordEncode;
                    
                    if(empty($postParams['diverseyId'])){
                        $postParams['diverseyId'] = 0;
                    }
                    
                    // Create account
                    $accountObj = $accountRepository->createAccount($postParams);
                    
                    $postParams['roleId'] = $postParams['accountType'];
                    
                    // Create user
                    $userRepository = $this->getRepository('User');
                    
                    // Set user DSR type
                    $userRepository->createUser($accountObj, $postParams);
                    
                    // Trigger event
                    $this->getEventManager()->trigger('admin.user.create', $this, [
                        'user' => $postParams,
                        'account' => $accountObj,
                        'userRole' => $authUser['role'],
                        'plainPassword' => $plainPassword
                    ]);
                    
                    $this->flashMessenger()->addSuccessMessage(
                        sprintf('User <strong>%s %s</strong> has been successfully created.', $postParams['firstName'], $postParams['lastName'])
                    );
                    return $this->redirect()->toRoute('users');
                } else {
                    $this->flashMessenger()->addErrorMessage('Username is already registered.');
                    return $this->redirect()->toRoute('login');
                }
            }
        }
        
        $viewParams = [];
        $viewParams['form'] = $form;
        
        return new ViewModel($viewParams);
    }
    
    /**
     * Action to edit user information
     *
     * URL: /users/edit/:id
     */
    public function editAction() {
        $authService = $this->sm->get('app.authentication');
        $authUser = $authService->getIdentity();
        $request = $this->getRequest();
        $hashId = $this->params()->fromRoute('id');
        $id = $this->getHashids()->decode($hashId);
        
        if (!$id) {
            return $this->notFoundAction();
        }
        
        $viewParams = [];
        $isEmailModified = false;
        
        $userRepository = $this->getRepository('User');
        $accountRepository = $this->getRepository('Account');
        
        $user = $userRepository->find($id);
        $account = $user->getAccount();
        $roleId = $userRepository->getRoleByUser($id);
        $roleKey = $userRepository->getRoleKey($id);
        
        $em = $this->getEntityManager();
        $stateList = LocationRepository::getStatesByCountry($user->getCountry());
        $claimCount = 0;//$this->getRepository('AllowanceClaim')->getClaimCountByUser($id);
        
        $formOptions = [
            'state' => $stateList,
            'action' => 'admin_user_edit',
            'userRole' => $authUser['role']
        ];
        
        $form = new UserForm($em, $formOptions);
        $form->get('username')->setValue($account->getUsername());
        $form->get('status')->setValue($account->getStatus());
        $form->get('accountType')->setAttribute('value', $roleId);
        $form->get('state')->setAttribute('options', LocationRepository::getStatesByCountry($user->getCountry()));
        $form->get('distState')->setAttribute('options', LocationRepository::getStatesByCountry($user->getDistCountry()));
        
        $form->bind($user);
        
        $request = $this->getRequest();
        
        $prevStatus = $account->getStatus();
        
        if($request->isPost()){
            $postParams = $request->getPost()->toArray();
            
            if(empty($postParams['diverseyId'])){
                $postParams['diverseyId'] = 0;
            }
            
            $form->bindValues($postParams);
            
            if($form->isValid()){
                if($account->getUsername() != $postParams['username']){
                    $isAccountExists = $accountRepository->isAccountExist($postParams['username']);
                    
                    if($isAccountExists){
                        $this->flashMessenger()->addErrorMessage('Username is already registered.');
                        return $this->redirect()->toRoute('users/edit', [
                            'id' => $hashId
                        ]);
                    } else {
                        $isEmailModified = true;
                        $token = md5(uniqid(time()));
                        
                        $account->setUsername($postParams['username'])
                            ->setVerificationToken($token)
                            ->setEmailStatus(Account::STATUS_UNVERIFIED);
                    }
                }
                
                if(!empty($postParams['password'])){
                    $encoder = $this->sm->get('app.password_encoder');
                    $password = $encoder->encode($postParams['password']);
                    $account->setPassword($password);
                }
              
                // Update account
                $account->setStatus($postParams['status']);
                $accountRepository->update($account);
                
                // Update user
                $userRepository->updateUser($user, $postParams);
                
                // Update user role
                $userRepository->updateRole($roleKey, $postParams['accountType']);
                
                // Trigger event
                $this->getEventManager()->trigger('admin.user.edit', $this, [
                    'prevStatus' => $prevStatus,
                    'user' => $postParams,
                    'account' => $account
                ]);
                
                // Trigger event if email has been modified
                if ($isEmailModified) {
                    $this->getEventManager()->trigger('account.email.modified', $this, [
                        'user' => $postParams,
                        'account' => $account
                    ]);
                }
                
                $this->flashMessenger()->addSuccessMessage(
                    sprintf('User <strong>%s %s</strong> details has been successfully updated.', $user->getFirstName(), $user->getLastName())
                );
                
                return $this->redirect()->toRoute('users');
            }
        }
        
        $viewParams['form'] = $form;
        $viewParams['clamCount'] = $claimCount;
        $viewParams['userId'] = $hashId;
        $viewParams['claimCount'] = $claimCount;
        
        return new ViewModel($viewParams);
    }
    
    /**
     * Action to export DataTable server-side processing.
     *
     * URL: /users/export
     */
    public function exportAction() 
    {
        $request = $this->getRequest();
        $userRepository = $this->getRepository('User');
        $roleDisplayNames = $userRepository->getRoleDisplayNames();
        $postData = $request->getQuery()->toArray();
        
        $filters = $this->params()->fromQuery('filters', array());
        
        $userRepo = $this->getRepository('User');
        $search = $postData['search']['value'];
        $users = $userRepo->getUsers($filters, $search, array(), 0, - 1);
       
        $excelRows = [];
        
        array_push($excelRows, array(
            'Status',
            'Email',
            'Company Name',
            'Last Name',
            'First Name',
            'Diversey ID',
            'Updated'
        ));
        
        $data = $users['data'];
        
        foreach ($data as $row) {
            $roleName = $roleDisplayNames[$row['roleId']];
            
            $status = $roleName;
            
            if (! $row['status']) {
                $status = 'Unverified';
            }
            
            $row['updateDtTm'] = $row['updateDtTm']->format('m-d-Y');
            
            // Data to insert to csv
            $line = array(
                $status,
                $row['email'],
                $row['companyName'],
                $row['lastName'],
                $row['firstName'],
                $row['diverseyId'],
                $row['updateDtTm']
            );
            
            array_push($excelRows, $line);
        }
        
        $filename = 'users-list-' . date('Y-m-d-His') . '.xlsx';
        
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToFile('php://output');
        $writer->addRows($excelRows);
        $writer->openToBrowser($filename);
        $writer->close();
        exit;
    }

    /**
     * Action to delete user account
     *
     * URL: /users/delete
     */
    public function deleteAction() 
    {
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $postDataArray = $request->getPost()->toArray();
            $hashId = $postDataArray['userId'];
            $id = $this->getHashids()->decode($postDataArray['userId']);
            
            $userRepository = $this->getRepository('User');
            $accountRepository = $this->getRepository('Account');
            
            $user = $userRepository->find($id);
            $fullName = $user->getFirstName() . ' ' . $user->getLastName();
            $claimCount = 0;// $this->getRepository('AllowanceClaim')->getClaimCountByUser($id);
            
            if ($claimCount > 0) {
                $this->flashMessenger()->addSuccessMessage("This user has claims in the system and cannot be Deleted.");
                return $this->redirect()->toRoute('users/edit', array(
                    'id' => $hashId
                ));
            }

            $accountId = $user->getAccount()->getId();
            $accountEntity = $accountRepository->find($accountId);
            $accountEntity->setDeleteFlag(true);
            $accountRepository->update($accountEntity);
            $user->setDeleteFlag(true);
            $userRepository->update($user);
            
            $this->flashMessenger()->addSuccessMessage("User <strong> $fullName </strong> has been successfully deleted.");
            
            return $this->redirect()->toRoute('users');
        }
    }
    public function loginAction(){
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $userRepository = $this->getRepository('T014fuser');
        try
            {

                $userObject = $userRepository->findBy(array("f014fuserName" => $postData['f014fuserName']));
                $userObject = $userObject[0];
            } catch (\Exception $e) {
                echo $e;
            }
        if(!$userObject){
            return new JsonModel([
                'status'  => 410,
                'message' => 'User Name Doesnt exist',
            ]);
        }
        if($userObject->getF014fstatus() == 0){
            return new JsonModel([
                'status'  => 411,
                'message' => 'Pending for approval',
            ]);
        }
        $password = $userObject->getF014fpassword();
        if(trim($password) == trim(md5($postData['f014fpassword']))){
            return new JsonModel([
                'status'  => 200,
                'message' => 'Success',
                'token' => $userObject->getF014ftoken(),
                'username' => $userObject->getF014fusername(),
                'id' => $userObject->getF014fid(),
        'role' => $userObject->getF014fidRole(),   
		'staffId' => $userObject->getF014fidStaff()   
            ]);
        }
        else{
            return new JsonModel([
                'status'  => 412,
                'message' => 'Failure'
            ]);
        }
    }
    public function registerAction(){
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $token = md5(uniqid(time()));
        $postData['f014ftoken'] = $token;
        $userRepository = $this->getRepository('T014fuser');
        try
            {

                $userObject = $userRepository->createNewData($postData);

            } catch (\Exception $e) {

                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 409,
                        'message' => 'Already exist.',
                    ]);
                }
            }

            return new JsonModel([
                'status'  => 200,
                'message' => 'Added successfully',

            ]);
    }

    public function userApproveAction(){
        $em      = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        $userRepository = $this->getRepository('T014fuser');
        try
            {

                $userRepository->approveUser($postData);

            } catch (\Exception $e) {

                if ($e->getErrorCode() == 1062) {
                    return new JsonModel([
                        'status'  => 409,
                        'message' => 'Already exist.',
                    ]);
                }
            }

            return new JsonModel([
                'status'  => 200,
                'message' => 'Added successfully',

            ]);
    }

    public function getApproveUserAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        // try
        // {
            $userRepository = $this->getRepository('T014fuser');
        // }
        // catch(\Exception $e)
        // {
        //     echo $e;
        // }
        $result = $userRepository->getApproveUser();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function getRejectUserAction()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        // try
        // {
            $userRepository = $this->getRepository('T014fuser');
        // }
        // catch(\Exception $e)
        // {
        //     echo $e;
        // }
        $result = $userRepository->getRejectUser();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function userApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $userRepository = $this->getRepository('T014fuser');
        $user = $postData['id'];
        $userObjs=array();
        foreach($user as $id)
        {
            $userObj =$userRepository->find((int)$id);
            if(!$userObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'User doesnot exist.'
                ]);
            }
            array_push($userObjs, $userObj);
        }
        $user = $userRepository->updateApproval($userObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function userRejectionAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $userRepository = $this->getRepository('T014fuser');
        $user = $postData['id'];
        $userObjs=array();
        foreach($user as $id)
        {
            $userObj =$userRepository->find((int)$id);
            if(!$userObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'User doesnot exist.'
                ]);
            }
            array_push($userObjs, $userObj);
        }
        $user = $userRepository->updateRejection($userObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'Rejected successfully'
        ]);
    }
}
