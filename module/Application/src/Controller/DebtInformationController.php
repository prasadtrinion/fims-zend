<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;


class DebtInformationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $debtRepository = $this->getRepository('T098fdebtInformation');
        $result = $debtRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $debtRepository = $this->getRepository('T098fdebtInformation');
        $debt = $debtRepository->find($id);
        if(!$debt)
        {
            return $this->resourceNotFound();
        }
        $result = $debtRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $debtRepository = $this->getRepository('T098fdebtInformation');

            try
            {

                $debt = $debtRepository->findBy(array("f098fcompanyName"=>strtolower($postData['f098fcompanyName'])));
                if ($debt)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Debt Already exist.'
                        ]);
                }
                $debtObj = $debtRepository->createDebt($postData);
            }

           
            catch (\Exception $e)
            {
              
                        return new JsonModel([
                            'status' => 411,
                            'message' => 'Error While Adding Data.'
                        ]);
               
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Debt Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $debtRepository = $this->getRepository('T098fdebtInformation');
        $debtDetailRepository = $this->getRepository('T098fdebtDetails');
        $debt                 = $debtRepository->find($id);

        if (!$debt) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $debtObj     = $debtRepository->updateDebt($debt, $postData);
            // print_r($debtObj);exit;
            $debtDetails = $postData['debt-details'];
            foreach ($debtDetails as $debtDetail) 
            {

                if (array_key_exists("f098fidDetails", $debtDetail)) 
                {

                    $debtDetailObj = $debtDetailRepository->find((int)$debtDetail['f098fidDetails']);
                    // print_r($debtDetailObj);exit;
                    if ($debtDetailObj) {
                        $debtDetailRepository->updateDebtDetail($debtDetailObj, $debtDetail);
                    }
                } else {

                    $debtDetailRepository->createDebtDetail($debtObj, $debtDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteDebtInformationDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $debtDetailRepository = $this->getRepository('T098fdebtDetails');
        // print_r($postData);exit;
        $result            = $debtDetailRepository->deleteDebtInformationDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Debt Information Details Deleted successfully',
        ]);
    }

}