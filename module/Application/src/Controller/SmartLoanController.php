<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class SmartLoanController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

    	$request = $this->getRequest();
        $smartLoanRepository = $this->getRepository('T043fsmartLoan');
        $result = $smartLoanRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $smartLoanRepository = $this->getRepository('T043fsmartLoan');
        $smartLoan = $smartLoanRepository->find((int)$id);
        if(!$smartLoan)
        {
            return $this->resourceNotFound();
        }
        $result = $smartLoanRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        

        if($this->isJSON($rawBody))
        {
            
        $smartLoanRepository = $this->getRepository('T043fsmartLoan');

            try
            {
                $smartLoanObj = $smartLoanRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $smartLoanRepository = $this->getRepository('T043fsmartLoan');
        $smartLoan = $smartLoanRepository->find((int)$id);

        if(!$smartLoan)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $smartLoanObj = $smartLoanRepository->updateData($smartLoan,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    

}