<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class LoanCancelRequestController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {


        $request = $this->getRequest();
        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');
        $result = $loanCancelRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');
        $loanCancel = $loanCancelRepository->find((int)$id);
        if(!$loanCancel)
        {
            return $this->resourceNotFound();
        }
        $result = $loanCancelRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');

             
            try
            {
                $loanCancelObj = $loanCancelRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        
        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');
        $loanCancel = $loanCancelRepository->find((int)$id);

        if(!$loanCancel)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $loanCancelObj = $loanCancelRepository->updateData($loanCancel,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function loanCancelApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');
        $loan = $postData['id'];
        $loanObjs=array();
        foreach($loan as $id)
        {
            $loanObj =$loanCancelRepository->find((int)$id);
            if(!$loanObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Does not exist.'
                ]);
            }
            array_push($loanObjs, $loanObj);
        }
        $asset = $loanCancelRepository->loanCancelApproval($loanObjs);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getListByApprovedStatusAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $loanCancelRepository = $this->getRepository('T070floanCancelRequest');
        
        $result = $loanCancelRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    

}