<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PattyCashEntryController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()  
    {
         
        $request = $this->getRequest();
        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $result = $pattyCashRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $pattyCash = $pattyCashRepository->find((int)$id);
        if(!$pattyCash)
        {
            return $this->resourceNotFound();
        }
        $result = $pattyCashRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)

    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');

            try
            {
                $pattyCashObj = $pattyCashRepository->createNewData($postData);
            }

            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $pattyCash = $pattyCashRepository->find((int)$id);

        if(!$pattyCash)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $pattyCashObj = $pattyCashRepository->updateData($pattyCash,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }

    public function getApprovedPattyCashEntryAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $result = $pattyCashRepository->getApprovedPattyCashEntry((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function approvePettyCashEntryAction()
    {
        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);
        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $result = $pattyCashRepository->approvePettyCashEntry($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Petty cash entry updated successfully', 
        ]);
    }

    public function pettyCashEntryReambersmentAction()
    {
        $rawBody              = file_get_contents("php://input");
        $postData             = json_decode($rawBody, true);
        $pattyCashRepository = $this->getRepository('T123fpattyCashEntry');
        $result = $pattyCashRepository->pettyCashEntryReambersment($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Petty cash Reambersed successfully', 
        ]);
    }

}