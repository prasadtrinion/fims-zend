<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class BillRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
         
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $billRepository = $this->getRepository('T091fbillRegistration');
        $result = $billRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $billRepository = $this->getRepository('T091fbillRegistration');
        $bill = $billRepository->find($id);
        if(!$bill)
        {
            return $this->resourceNotFound();
        }
        $result = $billRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
	 public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
        $billRepository = $this->getRepository('T091fbillRegistration');
        $billDetailRepository = $this->getRepository('T091fbillRegistrationDetails');
            

             if (array_key_exists("f091fid", $postData)) 
            {
                // print_r($postData);
                // exit();
                $billObj = $billRepository->find((int)$postData['f091fid']);
                
                $billObj =  $billRepository->updateBillMaster($billObj, $postData);
                $details = $postData['bill-details'];
        
                foreach ($details as $detail) 
                {
                   
                    if (array_key_exists("f091fidDetails", $detail)) 
                    {

                        $detailObj = $billDetailRepository->find((int) $detail['f091fidDetails']);

                        if ($detailObj) 
                        {
                            $billDetailRepository->updateBillDetail($detailObj, $detail);
                        }
                    } 
                    else 
                    {
                        $billDetailRepository->createBillDetail($billObj, $detail);
                    }
                }
            }
            else
            {
                try
                {
                    $billRepository->createNewData($postData);
                }
                catch (Exception $e)
                {
                    echo $e;            
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }
    public function billRegistrationApprovalAction()
    {
   
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $billRepository = $this->getRepository('T091fbillRegistration');
        $bill = $billRepository->billRegistrationApproval($postData);

        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    
    }
    
    public function getBillRegistrationApprovedListAction()
    {

        $approvedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $billRepository = $this->getRepository('T091fbillRegistration');
        
        $result = $billRepository->getBillRegistrationApprovedList((int)$approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

     public function getBillRegistrationPendingListAction()
    {
 
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $billRepository = $this->getRepository('T091fbillRegistration');
        
        $result = $billRepository->getBillRegistrationPendingList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    
     public function getBillRegistrationTypeListAction()
    {
 
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $billRepository = $this->getRepository('T091fbillRegistration');
        
        $result = $billRepository->getBillRegistrationTypeList();
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getGrnBasedOnVendorAction()
    {
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $billRepository = $this->getRepository('T091fbillRegistration');
        $result = $billRepository->getGrnBasedOnVendor($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);

    }


    public function getBillRegistrationByVendorAction()
    {
        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $request           = $this->getRequest();
        $billRepository = $this->getRepository('T091fbillRegistration');
        $result = $billRepository->getBillRegistrationByVendor($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);

    }
}
