<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class AdvancePaymentController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $advanceRepository = $this->getRepository('T020fadvancePayment');
        $result = $advanceRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $advanceRepository = $this->getRepository('T020fadvancePayment');
        $advance = $advanceRepository->find((int)$id);
        if(!$advance)
        {
            return $this->resourceNotFound();
        }
        $result = $advanceRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $advanceRepository = $this->getRepository('T020fadvancePayment');

            try
            {
                $advanceObj = $advanceRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'advance Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Advance Payment Added successfully',
                    'advanceId' => $advanceObj->getF020fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $advanceRepository = $this->getRepository('T020fadvancePayment');
        $advance = $advanceRepository->find((int)$id);

        if(!$advance)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $advanceObj = $advanceRepository->updateData($advance,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function advancePaymentInvoiceUpdateAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $advanceRepository = $this->getRepository('T020fadvancePayment');
        $data = $advanceRepository->advancePaymentInvoiceUpdate($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Amount updated successfully',
        ]);
    }

     public function knockOffAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        
        $advanceRepository = $this->getRepository('T020fadvancePayment');
        $data = $advanceRepository->knockOff($postData);
        
        return new JsonModel([
            'status'  => 200,
            'message' => 'Amount updated successfully',
        ]);
    }

}