<?php
namespace Application\Controller;

use Application\Entity\T011fdefinationms;
use Application\Entity\T012fdefinationtypems;
use Application\Repository\DefinationmsRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class DefinationmsController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }
    
    public function getDefListAction()
    {
        $defCode = $this->params()->fromRoute('defcode');
        $request = $this->getRequest();
        $definationmsRepository = $this->getRepository('T011fdefinationms');
    	$result = $definationmsRepository->getDefCode($defCode);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function getList()
    {

        $request = $this->getRequest();
        $definationmsRepository = $this->getRepository('T011fdefinationms');
        $result = $definationmsRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $definationmsRepository = $this->getRepository('T011fdefinationms');
        $definationms= $definationmsRepository->find($id);
        if(!$definationms)
        {
            return $this->resourceNotFound();
        }
        $result = $definationmsRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    
}