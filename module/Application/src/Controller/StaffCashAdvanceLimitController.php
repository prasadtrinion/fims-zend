<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class StaffCashAdvanceLimitController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $cashAdvanceLimitRepository = $this->getRepository('T119fstaffCashAdvanceLimit');
        $result = $cashAdvanceLimitRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $cashAdvanceLimitRepository = $this->getRepository('T119fstaffCashAdvanceLimit');
        $cashAdvance = $cashAdvanceLimitRepository->find((int)$id);
        if(!$cashAdvance)
        {
            return $this->resourceNotFound();
        }
        $result = $cashAdvanceLimitRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        


        

        
        if($this->isJSON($rawBody))
        {
            $staff = $postData['f119fstaffId'];
            $cashAdvanceLimitRepository = $this->getRepository('T119fstaffCashAdvanceLimit');
            // $getStaffAmount = $cashAdvanceLimitRepository->staffConditionCheck($staff);
            // $getStaffAmount = $getStaffAmount[0];
            // $amount = $getStaffAmount['f052ftotal'];

            // if ($postData['f119famount'] > $amount)
            // {
            //      return new JsonModel([
            //                'status'  => 409,
            //               'message' => '',
            //            ]);
            // }

                try
                {
                    $cashAdvanceObj = $cashAdvanceLimitRepository->createNewData($postData);
                }

           
                catch (\Exception $e)
                {
                
                       return new JsonModel([
                           'status'  => 409,
                          'message' => 'Already exist.',
                       ]);
                
                }
                return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

            }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $cashAdvanceLimitRepository = $this->getRepository('T119fstaffCashAdvanceLimit');
        $cashAdvance = $cashAdvanceLimitRepository->find((int)$id);

        if(!$cashAdvance)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $cashAdvanceObj = $cashAdvanceLimitRepository->updateData($cashAdvance,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

}