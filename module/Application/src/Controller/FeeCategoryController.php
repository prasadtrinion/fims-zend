<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class FeeCategoryController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $feeCategoryRepository = $this->getRepository('T065ffeeCategory');
        $result = $feeCategoryRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $feeCategoryRepository = $this->getRepository('T065ffeeCategory');
        $feeCategory = $feeCategoryRepository->find($id);
        if(!$feeCategory)
        {
            return $this->resourceNotFound();
        }
        $result = $feeCategoryRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            // print_r($postData);exit;
            
            $feeCategoryRepository = $this->getRepository('T065ffeeCategory');
            $fee = $feeCategoryRepository->findBy(array("f065fcode"=>strtolower($postData['f065fcode'])));
            // print_r($fee);exit;

            if ($fee)
            {
                return new JsonModel([
                            'status' => 409,
                            'message' => 'Fee category Already exist.'
                        ]);
            }

            try
            {
                $feeCategoryObj = $feeCategoryRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Adding'
                        ]);
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Fee category Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;

        $feeCategoryRepository = $this->getRepository('T065ffeeCategory');
        $feeCategory = $feeCategoryRepository->find($id);

        if(!$feeCategory)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $feeCategoryObj = $feeCategoryRepository->updateData($feeCategory,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function getFeeCategoryAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $status   = $this->params()->fromRoute('status');

        $feeCategoryRepository = $this->getRepository('T065ffeeCategory');
        $result = $feeCategoryRepository->getFeeCategory((int)$status);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

}