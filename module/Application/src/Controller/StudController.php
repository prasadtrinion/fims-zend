<?php
namespace Application\Controller;

use Application\Entity\Student;
use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class StudentController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('Student');
        $result = $studentRepository->getStudents();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
       // print_r($result);exit;
    }

    public function get($id)
    {
    
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('Student');
        $student = $studentRepository->find($id);
        if(!$student)
        {
            return $this->resourceNotFound();
        }

        $gender = 'male';
        if($student->getGender() == Student::FEMALE)
        {
            $gender = 'female';
        }
        return new JsonModel([
            'status' => 200,
            'id' => $id,
            'name' =>  $student->getName(),
            'gender' => $gender,
            'email' => $student->getEmail(),
            'phone' => $student->getPhoneNumber(),
            'address' => $student->getAddress(),
            'city' => $student->getCity(),
            'state' => $student->getState(),
        ]);

    }

    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");
            if($this->isJSON($rawBody)){
                if(empty($postData['name']) || empty($postData['email']) || empty($postData['city'])  || empty($postData['state']) || empty($postData['phone']) || empty($postData['address']) || empty($postData['gender']) ){
                    return $this->invalidInputType(); 
                }
                $studentRepository = $this->getRepository('Student');
                // Create account
                try{
                    $postData['gender'] = Student::MALE;
                    if($postData['gender'] == 'female')
                    {
                        $postData['gender'] = Student::FEMALE;
                    }
                    $studentObj = $studentRepository->createStudent($postData);
                } 
                catch (\Exception $e) {

                    if($e->getErrorCode()== 1062){
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Student Already exist.'
                        ]);
                    }
                }

                return new JsonModel([
                    'status' => 200,
                    'message' => 'Student Created successfully',
                    'studentId' => $studentObj->getId()
                ]);

            }else{
                return $this->invalidInputType();
            }
       
    }
    
    public function update($id, $postData)
    {
       //print_r($postData);exit;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('Student');
        $student = $studentRepository->find($id);
        if(!$student)
        {
            return $this->resourceNotFound();
        }

        $gender = 'male';
        if($student->getGender() == Student::FEMALE)
        {
            $gender = 'female';
        }

        $rawBody = file_get_contents("php://input");
        if($this->isJSON($rawBody)){
            if(empty($postData['name']) || empty($postData['email']) || empty($postData['city'])  || empty($postData['state']) || empty($postData['phone']) || empty($postData['address']) || empty($postData['gender']) ){
                return $this->invalidInputType(); 
            }

            $postData['gender'] = Student::MALE;
            if($postData['gender'] == 'female')
            {
                $postData['gender'] = Student::FEMALE;
            }
            $studentObj = $studentRepository->updateStudent($student,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Student updated successfully'
            ]);
        }else{
            return $this->invalidInputType();
        }
      
    }

    public function delete($id)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $studentRepository = $this->getRepository('Student');
        $student = $studentRepository->findBy(array('id'=>$id,'deleteFlag'=>'0'));
        if(empty($student))
        {
           return $this->resourceNotFound();
        }

        $student->setDeleteFlag('1');
        $studentRepository->update($student);
        return new JsonModel([
            'status' => 200,
            'message' => 'Student deleted successfully'
        ]);
    }

}