<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class CashDeclarationController extends AbstractAppController
{

    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {


        $request = $this->getRequest();
       
        $cashRepository = $this->getRepository('T052fcashDeclaration');

        
      
        $result = $cashRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $cashRepository = $this->getRepository('T052fcashDeclaration');
        $cash = $cashRepository->find((int)$id);
        // if(!$cash)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $cashRepository->getListById((int)$id);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

       {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
        $cashRepository = $this->getRepository('T052fcashDeclaration');
        $cashDetailRepository = $this->getRepository('T052fcashDeclarationDetails');
       

        $rawBody = file_get_contents("php://input");
        // print_r($postData);
        // exit();

        if ($this->isJSON($rawBody)) 
        {

            if (array_key_exists("f052fidDeclare", $postData)) 
            {

                    $cashObj = $cashRepository->find((int) $postData['f052fidDeclare']);

                    if ($cashObj) 
                    {
                        $cash=$cashRepository->updateCash($cashObj, $postData);
                    }
            } 
            else 
            {
                $cashId = $cashRepository->findBy(array("f052fidCash"=>(int)$postData['f052fidCash']));
                    if($cashId){
                         return new JsonModel([
                            'status'  => 409,
                            'message' => 'Cash ID already exist',
                        ]);
                    }
                $cash=$cashRepository->createCash($postData);
            }
            $cashDetails = $postData['cash-details'];
            
            foreach ($cashDetails as $cashDetail) 
            {

                if (array_key_exists("f052fidDeclareDetails", $cashDetail)) 
                {

                    $cashDetailObj = $cashDetailRepository->find((int) $cashDetail['f052fidDeclareDetails']);

                    if ($cashDetailObj) 
                    {
                        $cashDetailRepository->updateCashDetail($cashDetailObj, $cashDetail);
                    }
                } else {
                    
                    $cashDetailRepository->createCashDetail($cash, $cashDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    } 
    }

    public function approvedCashDeclarationsAction()
    {
        $request = $this->getRequest();
            $cashRepository = $this->getRepository('T052fcashDeclaration');
        $id = $this->params()->fromRoute('id');

            $list=$cashRepository->getApprovalList((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }

    public function approveCashDeclarationAction()
    {
        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody  = file_get_contents("php://input");

        $postData = json_decode($rawBody, true);
        $cashRepository       = $this->getRepository('T052fcashDeclaration');

        $cashRepository->approve($postData);
        return new JsonModel([
            'status' => 200,
            'message' => 'Approved successfully'
        ]);
    }

    
    
}
