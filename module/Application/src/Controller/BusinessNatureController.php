<?php
namespace Application\Controller;

use Application\Entity\T059fbusinessNature;
use Application\Repository\AccountCodeRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

// print_r("hello");
//  exit();

class BusinessNatureController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()

    {
        $refCode = $this->params()->fromRoute('refcode');
        
        $request = $this->getRequest();
        
        $accountCodeRepository = $this->getRepository('T059fbusinessNature');

        try
        {
            $accountCode = $accountCodeRepository->find($refCode);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $accountCodeRepository->getListByRef($refCode);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function create($postData)
    {
        

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {

           
            
            $accountCodeRepository = $this->getRepository('T059fbusinessNature');
            try
            {
                if($postData['f059flevelStatus'] == 2 || $postData['f059flevelStatus'] == 1)
                {

                    $accountCodeObj1 = $accountCodeRepository->find((int)$postData['f059fparentCode']);
                    $parentId = $accountCodeObj1->getF059fparentCode();
                    $accountCode1 = $accountCodeObj1->getF059fcode();
                    $accountCodeObj2 = $accountCodeRepository->find((int)$parentId);
                    $accountCode2 = $accountCodeObj2->getF059fcode();
                    $postData['f059fcompleteCode'] = $accountCode2.$accountCode1.$postData['f059fcode'];
                }
            }
            catch(\Exception $e)
            {
                echo $e;
            }  

            try
            {
                $accountCodeObj = $accountCodeRepository->createNewData($postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function newBusinessNatureAction()
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $accountCodeRepository = $this->getRepository('T059fbusinessNature');
            $accounts = $postData['data'];
            
            foreach ($accounts as $data) 
            {
            

                try
                {
                    if($data['f059flevelStatus'] == 2)
                    {
                        
                        $accountCodeObj1 = $accountCodeRepository->find((int)$data['f059fparentCode']);
                        $parentId = $accountCodeObj1->getF059fparentCode();
                        $accountCode1 = $accountCodeObj1->getF059fcode();
                        $accountCodeObj2 = $accountCodeRepository->find((int)$parentId);
                        $accountCode2 = $accountCodeObj2->getF059fcode();
                        $data['f059fcompleteCode'] = $accountCode2.$accountCode1.$data['f059fcode'];

                    }
                }
                catch(\Exception $e)
                {
                    echo $e;
                }
                if(array_key_exists("f059fid",$data))
                {

                    $accountCode = $accountCodeRepository->find((int)$data['f059fid']);
                    if($accountCode)
                    {
                        $accountCodeObject = $accountCodeRepository->updateData($accountCode,$data);
                    }
                }   
                else
                {
                 
                    $accountCodeObject = $accountCodeRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        try
        {
            $accountCodeRepository = $this->getRepository('T059fbusinessNature');
        }
        catch(\Exception $e)
        {
            echo $e;
        }
        $accountCode = $accountCodeRepository->find((int)$id);

        if(!$accountCode)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            if(empty($postData['f059fcode']))
            {
                return $this->invalidInputType(); 
            }
            try
            {
                $accountCodeObj = $accountCodeRepository->updateAccountCode($accountCode,$postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function businessNatureLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        
        $request = $this->getRequest();
        
        $accountCodeRepository = $this->getRepository('T059fbusinessNature');

        try
        {
            $accountCode = $accountCodeRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $accountCodeRepository->getListByLevel((int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function businessNatureParentLevelListAction()

    {   
        
        $level = $this->params()->fromRoute('level');
        $parent = $this->params()->fromRoute('parent');
        
        $request = $this->getRequest();
        
        $accountCodeRepository = $this->getRepository('T059fbusinessNature');

        try
        {
            $accountCode = $accountCodeRepository->find((int)$level);
        }

        catch (\Exception $e)
        {
            echo $e;
        }
        $result = $accountCodeRepository->getListByParentAndLevel((int)$parent,(int)$level);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deleteBusinessNaatureAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $id = $this->params()->fromRoute('id');
        $accountRepository = $this->getRepository('T059fbusinessNature');
        $account = $accountRepository->find((int)$id);

        if(!$account)
        {
           return $this->resourceNotFound();
        }

        
        $accountRepository->updateAccount($account);
        return new JsonModel([
            'status' => 200,
            'message' => 'Deleted successfully'
        ]);
    }

    public function businessNatureStatusAction()

    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $id=$postData['f059fid'];
       
        $id =(int)$id;
       
        $accountRepository = $this->getRepository('T059fbusinessNature');
        $account =  $accountRepository ->find($id);
        if(!$account)
        {
            return $this->resourceNotFound();
        }
        $result = $accountRepository->accountCodeStatus($id);
        return new JsonModel([
            'status' => 200,
            'message' => 'Updated Successfully'
        ]);
    }
}