<?php

namespace Application\Controller;

use Application\Entity\T051fbudgetCostcenter;
use Application\Entity\T015fdepartment;
use Application\Entity\T014fglcode;
use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;

class BudgetCostcenterController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function CostcenterApprovalStatusAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        $costcenter = $postData['id'];
        $reason = $postData['reason'];
        $status = $postData['status'];
        $costcenterObjs=array();
        foreach($costcenter as $id)
        {
            $costcenterObj =$budgetCostcenterRepository->find((int)$id);
            if(!$costcenterObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' budget department does not exist.'
                ]);
            }
            array_push($costcenterObjs, $costcenterObj);
        }
        $costcenter = $budgetCostcenterRepository->CostcenterApprovalStatus($costcenterObjs,$status,$reason);
        return new JsonModel([
            'status' => 200,
            'message' => 'updated successfully'
        ]);
    }

    public function getCostcenterApprovalListAction()
    {

        $ApprovedStatus = $this->params()->fromRoute('approvedStatus');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        $approvedStatus = (int)$ApprovedStatus;
        $result = $budgetCostcenterRepository->getCostcenterApprovalList($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $CostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        $result = $CostcenterRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $CostcenterRepository = $this->getRepository('T051fbudgetCostcenter');

        $Costcenter = $CostcenterRepository->find($id);
        
        if(!$Costcenter)
        {
            return $this->resourceNotFound();
        }
        $result = $CostcenterRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {
        
        $flag = 0;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
            $rawBody = file_get_contents("php://input");

            // $postData = json_decode($rawBody,true);
            
            // $costcenterDatas=$postData;
            
        
               

                $department=$postData['f051fdepartment'];
                $fund=$postData['f051ffund'];
                $financialyear=$postData['f051fidFinancialyear'];

                $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
                $budgetCostcenter = $budgetCostcenterRepository->findBy(array('f051fidFinancialyear' => (int)$financialyear,'f051fdepartment' =>$department,'f051ffund'=>$fund));
                
                if($budgetCostcenter)
                {
                    return new JsonModel([
                        'status' => 409,
                        'message' => 'Already Exist',
                            
                    ]);
                }
                else{
                    $BudgetCostcenterObj = $budgetCostcenterRepository->createCostcenter($postData);
                }
                    
           
            return new JsonModel([
                        'status' => 200,
                        'message' => 'Created successfully',
                    
                    ]);
        //     if($flag == 0){
        //     return new JsonModel([
        //                 'status' => 200,
        //                 'message' => 'Created successfully',
                    
        //             ]);
        // }
        // else
        // {
        //     return new JsonModel([
        //                     'status' => 200,
        //                     'message' => 'Already exist.'
        //                 ]);
        // }

    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        $budgetCostcenter = $budgetCostcenterRepository->find((int)$id);


        if(!$budgetCostcenter)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            $budgetCostcenterObj = $budgetCostcenterRepository->updateData($budgetCostcenter,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    public function getCostcenterAmountAction()
    {


        $Department = $this->params()->fromRoute('idDepartment');
        $Financialyear = $this->params()->fromRoute('idFinancialyear');
        $Fund = $this->params()->fromRoute('fund');

        $idDepartment=$Department;
        $idFinancialyear=(int)$Financialyear;
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        
        $result = $budgetCostcenterRepository->getCostcenterAmount($idDepartment,$idFinancialyear,$Fund);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function getCostcenterAction()
    {

        $Financialyear = $this->params()->fromRoute('idFinancialyear');

        $idFinancialyear=(int)$Financialyear;
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');
        
        $result = $budgetCostcenterRepository->getCostcenter($idFinancialyear);

        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function CostcenterRejectAction()
    {

        $rawBody                  = file_get_contents("php://input");
        $postData                 = json_decode($rawBody, true);

        $budgetCostcenterRepository = $this->getRepository('T051fbudgetCostcenter');

        $costcenterIds        = $postData['id'];
        $reason = $postData['reason'];
        
        $costcenterObjs       = array();
        foreach ($costcenterObjs as $id) 
        {
            $costcenterObj = $budgetCostcenterRepository->find((int) $id);

            if (!$costcenterObj) 
            {
                return new JsonModel([
                    'status'  => 410,
                    'message' => 'costcenter doesnt exist.',
                ]);
            }
            array_push($costcenterObjs, $costcenterObj);

        }
         
        $costcenter = $budgetCostcenterRepository->costcenterRejection($costcenterObjs,$reason);

        return new JsonModel([
            'status'  => 200,
            'message' => 'rejected'
        ]);


    }
    
}