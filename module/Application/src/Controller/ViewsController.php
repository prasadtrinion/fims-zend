<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ViewsController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
         
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function departmentCodeViewAction()
    {
        // $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        $viewRepository = $this->getRepository('T015ftemporaryDepartmentCode');
        $result = $viewRepository->getDepartmentCodes();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function unitCodeViewAction()
    {
        // $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        $viewRepository = $this->getRepository('T015ftemporaryUnitCode');
        $result = $viewRepository->getUnitCodes();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
	 public function viewAction()
    {
        $name = $this->params()->fromRoute('name');
        $request = $this->getRequest();
        $viewRepository = $this->getRepository('T015ftemporaryUnitCode');
        $result = $viewRepository->getViewData($name);
        
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
   

}
