<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class RevenueTypeController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $revenueTypeRepository = $this->getRepository('T132frevenueType');

        $result = $revenueTypeRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $revenueTypeRepository = $this->getRepository('T132frevenueType');
        $revenueType = $revenueTypeRepository->find((int)$id);
        if(!$revenueType)
        {
            return $this->resourceNotFound();
        }
        $result = $revenueTypeRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $revenueTypeRepository = $this->getRepository('T132frevenueType');
             
            try
            { 
                if (array_key_exists("f132fid", $data)) 
                {

                    $revenueTypeObj = $revenueTypeRepository->find((int)$data['f132fid']);

                    if ($revenueTypeObj)
                    {
                    // print_r($licenseObj);exit;

                        $revenueTypeRepository->updateData($revenueTypeObj, $data);
                    }
                } else
                {
                    // print_r($data);exit;
                $category = $revenueTypeRepository->findBy(array("f132frevenueName"=>strtolower($data['f132frevenueName'])));
                if ($category)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }

                $revenueTypeObj = $revenueTypeRepository->createNewData($data);
                }
            }

           
            catch (Exception $e)
            {
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

	public function getRevenueTypeActiveAction()
    {
        $request = $this->getRequest();
        $revenueTypeRepository = $this->getRepository('T132frevenueType');
        $id = $this->params()->fromRoute('id');

        $list=$revenueTypeRepository->getRevenueTypeActive((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }
    

}