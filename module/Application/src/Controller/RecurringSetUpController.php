<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class RecurringSetUpController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $recurringSetUpRepository = $this->getRepository('T077frecurringSetUp');
        $result = $recurringSetUpRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $recurringSetUpRepository = $this->getRepository('T077frecurringSetUp');
        $recurringSetUp = $recurringSetUpRepository->find((int)$id);
        if(!$recurringSetUp)
        {
            return $this->resourceNotFound();
        }
        // print_r($id);exit;
        $result = $recurringSetUpRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $recurringSetUpRepository = $this->getRepository('T077frecurringSetUp');

            try
            {
                $recurringSetUpObj = $recurringSetUpRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'RecurringSetUp Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'RecurringSetUp Added successfully',
                    'recurringSetUpId' => $recurringSetUpObj->getF077fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $recurringRepository = $this->getRepository('T077frecurringSetUp');
        $recurringDetailRepository = $this->getRepository('T077frecurringSetupDetails');
        $recurring         =$recurringRepository->find($id);

        if (!$recurring) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");



        if ($this->isJSON($rawBody)) {

            $recurringObj     = $recurringRepository->updateData($recurring, $postData);
            
            $recurringDetails = $postData['recurring-details'];
            
            foreach ($recurringDetails as $recurringDetail) 
            {

                if (array_key_exists("f077fidDetails", $recurringDetail)) 
                {

                    $recurringDetailObj = $recurringDetailRepository->find((int) $recurringDetail['f077fidDetails']);

                    if ($recurringDetailObj) {
                        $recurringDetailRepository->updateRecurringDetail($recurringDetailObj, $recurringDetail);
                    }
                } else {
                   
                    $recurringDetailRepository->createRecurringDetail($recurringObj, $recurringDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function deleteRecurringSetupDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $recurringDetailRepository = $this->getRepository('T077frecurringSetupDetails');
        $result            = $recurringDetailRepository->deleteRecurringSetupDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Recurring Setup Details Deleted successfully',
        ]);
    }
}