<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class NonPoInvoiceController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $nonPoInvoiceRepository = $this->getRepository('T091fnonPoInvoiceMaster');
        $result = $nonPoInvoiceRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $nonPoInvoiceRepository = $this->getRepository('T091fnonPoInvoiceMaster');
        $nonPoInvoice = $nonPoInvoiceRepository->find($id);
        if(!$nonPoInvoice)
        {
            return $this->resourceNotFound();
        }
        $result = $nonPoInvoiceRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $nonPoInvoiceRepository = $this->getRepository('T091fnonPoInvoiceMaster');

            try
            {
                $nonPoInvoiceObj = $nonPoInvoiceRepository->createNonInvoice($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 209,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }
    public function getApprovalListAction()
    {

        $status = $this->params()->fromRoute('status');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $nonPoInvoiceRepository = $this->getRepository('T091fnonPoInvoiceMaster');
        
        $result = $nonPoInvoiceRepository->getListByStatus($status);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function approvalStatusAction()
    {
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $nonPoInvoiceRepository = $this->getRepository('T091fnonPoInvoiceMaster');
        $nonPoInvoice = $postData['id'];
        $nonPoInvoiceObjs=array();
        foreach($nonPoInvoice as $id)
        {
            $nonPoInvoiceObj =$nonPoInvoiceRepository->find((int)$id);
            if(!$nonPoInvoiceObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => 'Invoice doesnot exist.'
                ]);
            }
            array_push($nonPoInvoiceObjs, $nonPoInvoiceObj);
        }
            $nonPoInvoice = $nonPoInvoiceRepository->updateStatus($nonPoInvoiceObjs);
        
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }
}