<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class RevenueSetUpController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    { 

        $request = $this->getRequest();
        $revenueRepository = $this->getRepository('T085frevenueSetUp');
        $result = $revenueRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $revenueRepository = $this->getRepository('T085frevenueSetUp');
        $revenue = $revenueRepository->find((int)$id);
        if(!$revenue)
        {
            return $this->resourceNotFound();
        }
        $result = $revenueRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $revenueRepository = $this->getRepository('T085frevenueSetUp');

            // $master = $revenueRepository->findBy(array("f085fcode"=>strtolower($postData['f085fcode'])));
                
            
            // if($master){
            //             return new JsonModel([
            //                 'status'  => 409,
            //                 'message' => 'Already exist.',
            //             ]);
            //     }


            try
            {
                $revenueObj = $revenueRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $revenueSetUpRepository = $this->getRepository('T085frevenueSetUp');
        $revenueSetUp = $revenueSetUpRepository->find((int)$id);

        if(!$revenueSetUp)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $revenueSetUpObj = $revenueSetUpRepository->updateData($revenueSetUp,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function getAccountCodeByRevenueCodeAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $revenueSetUpRepository = $this->getRepository('T085frevenueSetUp');
        
        $result = $revenueSetUpRepository->getAccountCodeByRevenueCode($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function getRevenueByCategoryAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $revenueSetUpRepository = $this->getRepository('T085frevenueSetUp');
        
        $result = $revenueSetUpRepository->getRevenueByCategory($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

}