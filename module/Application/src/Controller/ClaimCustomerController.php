<?php
namespace Application\Controller;

use Application\Entity\T022fclaimCustomers;
use Application\Repository\CustomerRepository;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class ClaimCustomerController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T022fclaimCustomers');
        $result = $customerRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T022fclaimCustomers');
        $customer = $customerRepository->find((int)$id);
        if(!$customer)
        {
            return $this->resourceNotFound();
        }
        $result = $customerRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {

            if( empty($postData['f022ffirstName']))
            {
                
                return $this->invalidInputType();
            }
            
            $customerRepository = $this->getRepository('T022fclaimCustomers');
            // $encoder = $this->sm->get('app.password_encoder');
            $passwordEncode = md5($postData['f022fpassword']);
            $postData['f022fpassword'] = $passwordEncode;

            try
            {
                $customerObj = $customerRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully.'
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $customerRepository = $this->getRepository('T022fclaimCustomers');
        $customer = $customerRepository->find((int)$id);

        if(!$customer)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
            if( empty($postData['f022ffirstName']))
            {
                
                return $this->invalidInputType();
            }

            $customerObj = $customerRepository->updateCustomer($customer,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    
    
}