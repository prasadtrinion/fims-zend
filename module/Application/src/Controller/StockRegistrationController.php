<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class StockRegistrationController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $stockregistrationRepository = $this->getRepository('T124fstockRegistration');
        $result = $stockregistrationRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $stockregistrationRepository = $this->getRepository('T124fstockRegistration');
        $stockregistration = $stockregistrationRepository->find((int)$id);
        if(!$stockregistration)
        {
            return $this->resourceNotFound();
        }
        $result = $stockregistrationRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");
        

        if($this->isJSON($rawBody))
        {
             // print_r($postData);exit;
            
        $stockregistrationRepository = $this->getRepository('T124fstockRegistration');

        $stockregistration = $stockregistrationRepository->findBy(array("f124fassetCategory"=>(int)$postData['f124fassetCategory'],"f124fassetSubCategory"=>(int)$postData['f124fassetSubCategory'],"f124fassetItem"=>(int)$postData['f124fassetItem']));
            if ($stockregistration)
            {
                $stock = $stockregistration[0]->getF124fquantity();
                $stockObj = $stockregistration[0];
                $postData['f124fquantity'] = $postData['f124fquantity'] + $stock;
                // print_r($stockObj);exit;
                // $stockregistration = $stockregistrationRepository->find((int)$id);
                $stockregistrationObj = $stockregistrationRepository->updateData($stockObj,$postData);
                            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);
            }

            try
            {
                $stockregistrationObj = $stockregistrationRepository->createNewData($postData);
            }

           
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
            
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        
        $stockregistrationRepository = $this->getRepository('T124fstockRegistration');
        $stockregistration = $stockregistrationRepository->find((int)$id);

        if(!$stockregistration)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $stockregistrationObj = $stockregistrationRepository->updateData($stockregistration,$postData);

            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }
    

}