<?php
namespace Application\Controller;

use Application\Entity\T012fstate;
use Application\Entity\T013fcountry;


use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class CountryController extends AbstractAppController
{
	protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function get($id)
    {

        $request = $this->getRequest();
        $testRepository = $this->getRepository('T013fcountry');
        $test = $testRepository->find($id);
        if(!$test)
        {
            return $this->resourceNotFound();
        }
        $result = $testRepository->getCountryById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function countryStatesAction()
    {

        $request = $this->getRequest();
        $testRepository = $this->getRepository('T013fcountry');
        $id = $this->params()->fromRoute('id');

        // $test = $testRepository->find($id);
        // if(!$test)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $testRepository->getCountryStates($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getActiveCountryAction()
    {
        $request = $this->getRequest();
        $CountryRepository = $this->getRepository('T013fcountry');
        $result = $CountryRepository->getActiveCountry();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    
}

