<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class PurchaseOrderController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

     public function purchaseOrderWarrantiesAction()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getWarrantyPos();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function purchaseOrderWarrantiesApprovalAction()
    {

        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->purchaseOrderWarrantiesApproval();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest(); 
        $id =(int)$id;
       
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $purchase = $purchaseRepository->find($id);
        if(!$purchase)
        {
            return $this->resourceNotFound();
        }
        $result = $purchaseRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            $data=array();

            $data['type'] = $postData['f034forderType'];
            $initialRepository = $this->getRepository('T011finitialConfig');
            $generateNumber = $initialRepository->generateFIMS($data);
            $postData['f034freferenceNumber'] = $generateNumber;
            $purchaseRepository = $this->getRepository('T034fpurchaseOrder'); 

            try
            {
                $purchaseObj =$purchaseRepository->createPurchase($postData);
            }
            catch (Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'purchase Already exist.'
                        ]);
                }
            }

            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $id=(int)$id;
        $purchaseRepository       = $this->getRepository('T034fpurchaseOrder');
        $purchaseDetailRepository = $this->getRepository('T034fpurchaseOrderDetails');
        $purchase                 = $purchaseRepository->find($id);
        
        if (!$purchase) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $purchaseObj     = $purchaseRepository->updatePurchase($purchase, $postData);
            $purchaseDetails = $postData['purchase-details'];
            foreach ($purchaseDetails as $purchaseDetail) 
            {

                if (array_key_exists("f034fidDetails", $purchaseDetail)) 
                {

                    $purchaseDetailObj = $purchaseDetailRepository->find((int) $purchaseDetail['f034fidDetails']);

                    if ($purchaseDetailObj) {
                        $purchaseDetailRepository->updatePurchaseDetail($purchaseDetailObj, $purchaseDetail);
                    }
                } else {

                    $purchaseDetailRepository->createPurchaseDetail($purchaseObj, $purchaseDetail);
                }
            }
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function purchaseOrderApproveAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $purchase = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        $purchaseObjs=array();
        foreach($purchase as $id)
        {
            $purchaseObj =$purchaseRepository->find((int)$id);
            if(!$purchaseObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' purchase Order does not exist.'
                ]);
            }
            array_push($purchaseObjs, $purchaseObj);
        }

        $purchase = $purchaseRepository->purchaseOrderApprove($purchaseObjs,$reason,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);

    }

    public function getPurchaseOrderApprovedListAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('approvedStatus');
        $purchaseRepository       = $this->getRepository('T034fpurchaseOrder');

        $result = $purchaseRepository->purchaseOrderByApproved((int)$id);
        return new JsonModel([
            'status'  => 200,
            'result' => $result
        ]);
    }

    public function supplierPurchaseOrdersAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('id');
        $purchaseRepository       = $this->getRepository('T034fpurchaseOrder');

        $result = $purchaseRepository->supplierPurchaseOrders((int)$id);
        return new JsonModel([
            'status'  => 200,
            'result' => $result
        ]);
    }

    public function getListByOrderTypeAction()

    {
        $orderType = $this->params()->fromRoute('orderType');
        
        $request = $this->getRequest();
        
        $purchaseOrderRepository = $this->getRepository('T034fpurchaseOrder');

        $result = $purchaseOrderRepository->getListByOrderType($orderType);


        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function purchaseOrderNotInAction()
    {
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getListNotIn();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function purchaseOrderWarrantNotInAction()
    {
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getPOListNotIn($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getWarrantListNotInAction()
    {
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getWarrantListNotIn();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }


    public function getPurchaseOrderWarrantiesApprovedListAction()
    {

        $rawBody           = file_get_contents("php://input");
        $id = $this->params()->fromRoute('approvedStatus');
        $purchaseRepository       = $this->getRepository('T034fpurchaseOrder');

        $result = $purchaseRepository->purchaseOrderWarrantiesByApproved((int)$id);
        return new JsonModel([
            'status'  => 200,
            'result' => $result
        ]);
    }

    public function PONotInGrnAction()
    {
        // $id = $this->params()->fromRoute('id');
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->PONotInGrn($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function getPurchaseOrderWarrantAction()
    {
        $request = $this->getRequest();
        $purchaseRepository = $this->getRepository('T034fpurchaseOrder');
        $result = $purchaseRepository->getPurchaseOrderWarrant();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function deletePurchaseOrderDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $PORepository = $this->getRepository('T034fpurchaseOrderDetails');
        $result            = $PORepository->deletePurchaseOrderDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Purchase Order Details Data Deleted successfully',
        ]);
    }
}