<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class GrnDonationController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $grnDonationRepository = $this->getRepository('T121fgrnDonation');
        
        $result = $grnDonationRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $grnDonationRepository = $this->getRepository('T121fgrnDonation');
        $grnDonation = $grnDonationRepository ->find($id);
        if(!$grnDonation)
        {
            return $this->resourceNotFound();
        }
        $result = $grnDonationRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            $grnDonationRepository = $this->getRepository('T121fgrnDonation');
            $grnDonationDetailRepository = $this->getRepository('T121fgrnDonationDetails');
            
            if(array_key_exists("f121fid", $postData))
            {
            	// print_r($postData);exit;

            $grnDonationObj         =$grnDonationRepository->find((int)$postData['f121fid']);
            $grnDonationObj     = $grnDonationRepository->updateGrnDonation($grnDonationObj, $postData);
            $grnDonationDetails = $postData['grnDonation-details'];
            
            foreach ($grnDonationDetails as $grnDonationDetail) 
                {


                if (array_key_exists("f121fidDetails", $grnDonationDetail)) 
                    {


                    $grnDonationDetailsObj = $grnDonationDetailRepository->find((int)$grnDonationDetail['f121fidDetails']);
                    // print_r($grnDonationDetail);exit;

                    if ($grnDonationDetailsObj)
                        {
                        $grnDonationDetailRepository->updateGrnDonationDetail($grnDonationDetailsObj, $grnDonationDetail);
                        }
                    }
                    else
                    {

                    $grnDonationDetailRepository->createGrnDonationDetail($grnDonationObj, $grnDonationDetail);
                    }
                }
            }
            else
            {
                // $grnDonatonData = $grnDonatonRepository->findBy(array("f086fdescription"=>$postData['f086fdescription']));

                // if($grnDonatonData)
                // {
                //         return new JsonModel([
                //             'status'  => 409,
                //             'message' => 'Already exist.',
                //         ]);
                // }
                 try
                {
                	// print_r($postData);exit;
                    $grnDonationObj = $grnDonationRepository->createGrnDonation($postData);
                }
                catch (\Exception $e)
                {

                        return new JsonModel([
                            'status' => 412,
                            'message' => 'Already exist'
                        ]);
                }   
            }
            
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    // public function update($id, $postData)
    // {

    //     $em                      = $this->getEntityManager();
    //     $request                 = $this->getRequest();
    //     $id=(int)$id;
    //     $disposalRepository = $this->getRepository('T121fgrnDonation');
    //     $disposalDetailRepository = $this->getRepository('T086fdisposalReqDetails');
    //     $disposal         =$disposalRepository->find($id);

    //     if (!$disposal) {
    //         return $this->resourceNotFound();
    //     }

    //     $rawBody = file_get_contents("php://input");

    //     if ($this->isJSON($rawBody)) {

    //         $disposalObj     = $disposalRepository->updateDisposal($disposal, $postData);
    //         $disposalDetails = $postData['disposal-details'];
    //         // print_r($incrementObj);
    //         // exit();
    //         foreach ($disposalDetails as $disposalDetail) 
    //         {

    //             if (array_key_exists("f086fidDetails", $disposalDetail)) 
    //             {

    //                 $disposalDetailObj = $disposalDetailRepository->find((int) $disposalDetail['f086fidDetails']);

    //                 if ($disposalDetailObj) {
    //                     $disposalDetailRepository->updateDisposalDetail($disposalDetailObj, $disposalDetail);
    //                 }
    //             } else {

    //                 $disposalDetailRepository->createDisposalDetail($disposalObj, $disposalDetail);
    //             }
    //         }
    //         return new JsonModel([
    //             'status'  => 200,
    //             'message' => 'Updated successfully',
    //         ]);
    //     } else {
    //         return $this->invalidInputType();
    //     }

    // }

    public function deleteGrnDonationDetailAction()
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
               
        $grnDonationRepository = $this->getRepository('T121fgrnDonationDetails');
        $grnDonation = $grnDonationRepository ->find((int)$id);
        if(!$grnDonation)
        {
            return $this->resourceNotFound();
        }
        $result = $grnDonationRepository->deleteGrnDonationDetail((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => 'Deleted successfully',
        ]);
    }
  
}