<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class DebtorCategoryController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();

        $debtorCategoryRepository = $this->getRepository('T130fdebtorCategory');

        $result = $debtorCategoryRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $debtorCategoryRepository = $this->getRepository('T130fdebtorCategory');
        $applyLoan = $debtorCategoryRepository->find((int)$id);
        if(!$applyLoan)
        {
            return $this->resourceNotFound();
        }
        $result = $debtorCategoryRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($data)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            
             $debtorCategoryRepository = $this->getRepository('T130fdebtorCategory');
             
            try
            { 
                if (array_key_exists("f130fid", $data)) 
                {

                    $categoryObj = $debtorCategoryRepository->find((int)$data['f130fid']);

                    if ($categoryObj)
                    {
                    // print_r($licenseObj);exit;

                        $debtorCategoryRepository->updateData($categoryObj, $data);
                    }
                }
                else
                {

                $category = $debtorCategoryRepository->findBy(array("f130fdebtorCategoryName"=>strtolower($data['f130fdebtorCategoryName'])));
                if ($category)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }

                $categoryObj = $debtorCategoryRepository->createNewData($data);
                }
            }

           
            catch (Exception $e)
            {
                
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    // 'revenueId' => $revenueObj->getF036fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

	public function getDebtorCategoryActiveAction()
    {
        $request = $this->getRequest();
        $debtorCategoryRepository = $this->getRepository('T130fdebtorCategory');
        $id = $this->params()->fromRoute('id');

        $list=$debtorCategoryRepository->getDebtorCategoryActive((int)$id);
       return new JsonModel([
            'status' => 200,
            'result' => $list 
        ]);

    }
    

}