<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class UtilityController extends AbstractAppController
{

    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $utilityRepository = $this->getRepository('T078futility');
        $result = $utilityRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $utilityRepository = $this->getRepository('T078futility');
        $utility = $utilityRepository->find((int)$id);
        if(!$utility)
        {
            return $this->resourceNotFound();
        }
        $result = $utilityRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $utilityRepository = $this->getRepository('T078futility');

            try
            {
                // $utilityCheck = $utilityRepository->findBy(array("f078fpremise"=>(int)$postData['f078fpremise']));
                // if ($utilityCheck)
                // {
                //     return new JsonModel([
                //             'status' => 409,
                //             'message' => 'Utility Already exist.'
                //         ]);
                // }
                $utilityObj = $utilityRepository->createNewData($postData);
            }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 419,
                            'message' => 'Error While Adding Details'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Utility Added successfully',
                    'utilityId' => $utilityObj->getF078fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $utilityRepository = $this->getRepository('T078futility');
        $utility = $utilityRepository->find((int)$id);

        if(!$utility)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $utilityObj = $utilityRepository->updateData($utility,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function utilityPremisesAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id   = $this->params()->fromRoute('id');
        $utilityRepository = $this->getRepository('T078futility');
        $result = $utilityRepository->getUtilityPremises((int)$id);

            return new JsonModel([
                'status' => 200,
                'result' => $result
            ]);
       
    }
    public function getUtilityRateAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $unit   = $this->params()->fromRoute('unit');
        $utilityRepository = $this->getRepository('T078futility');
        $result = $utilityRepository->getUtilityRate((int)$id);

            return new JsonModel([
                'status' => 200,
                'result' => $result
            ]);
       
    }

}