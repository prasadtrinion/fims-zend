<?php
namespace Application\Controller;

use Application\Entity\T011fmenu;
use Application\Entity\T014fuser;
use Application\Entity\T021frole;
use Application\Entity\T022frole_details;
use Application\Entity\T023fuser_roles;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class RoleController extends AbstractAppController
{
	protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function roleMenusAction()
    {
        $id = $this->params()->fromRoute('id');

        $request = $this->getRequest();
        $RoleRepository = $this->getRepository('T022froleDetails');
    

        // $role = $RoleRepository->find($id);

        // if(!$role)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $RoleRepository->getRoleMenus((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function getMenuAction()
    {
        $token = $this->params()->fromRoute('token');
        $module = $this->params()->fromRoute('module');

        $request = $this->getRequest();
        $RoleRepository = $this->getRepository('T021frole');
    

        // $role = $RoleRepository->find($id);


        // if(!$role)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $RoleRepository->getMenuById($token,$module);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getMenu1Action()
    {
        $token = $this->params()->fromRoute('token');
        $module = $this->params()->fromRoute('module');

        $request = $this->getRequest();
        $RoleRepository = $this->getRepository('T021frole');
    

        // $role = $RoleRepository->find($id);


        // if(!$role)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $RoleRepository->getMenu1ById($token,$module);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function getModuleAction()
    {
        $token = $this->params()->fromRoute('token');
        $request = $this->getRequest();
        $RoleRepository = $this->getRepository('T021frole');
    

        // $role = $RoleRepository->find($id);


        // if(!$role)
        // {
        //     return $this->resourceNotFound();
        // }
        $result = $RoleRepository->getModuleById($token);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function getUserAction()
    {
        $id = $this->params()->fromRoute('id');

        $request = $this->getRequest();
        $UserRepository = $this->getRepository('T021frole');


        $user = $UserRepository->find((int)$id);


        if(!$user)
        {
            return $this->resourceNotFound();
        }
        $result = $UserRepository->getUserById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    public function userRolesAction()
    {
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $roleRepository = $this->getRepository('T021frole');
        $userRepository = $this->getRepository('T014fuser');
        if(array_key_exists('f014fid',$postData)){
            
            $userObj = $userRepository->find((int)$postData['f014fid']);
            if($userObj){
                $roleRepository->updateUser($userObj,$postData);
            }
        }
        else{
        $roleRepository->createUser($postData);
        }
      
        return new JsonModel([
            'status' => 200,
            'message' => 'User Updated Successfully'
        ]);
    
    }

    public function createUserRoleAction()
    {
         $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);
        $roleRepository = $this->getRepository('T021frole');
        $roleRepository->createUserRole($postData);
        return new JsonModel([
            'status' => 200,
            'message' => 'User Created Successfully'
        ]);
    }
}

