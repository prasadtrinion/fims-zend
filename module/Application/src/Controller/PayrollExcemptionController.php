<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;



class PayrollExcemptionController extends AbstractAppController
{
 
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $ExcemptionRepository = $this->getRepository('T063fpayrollExcemption');
        $result = $ExcemptionRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $ExcemptionRepository = $this->getRepository('T063fpayrollExcemption');
        $Excemption = $ExcemptionRepository->find((int)$id);
        if(!$Excemption)
        {
            return $this->resourceNotFound();
        }
        $result = $ExcemptionRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();

        $rawBody = file_get_contents("php://input");
         // $postData = json_decode($rawBody,true);


        if($this->isJSON($rawBody))
        {

            $ExcemptionRepository = $this->getRepository('T063fpayrollExcemption');
            $Excemptions = $postData['data'];
            // echo "<pre>";
            // print_r($Excemptions);
            // die();
            foreach ($Excemptions as $data) 
            {
            
                if(array_key_exists("f063fid",$data))
                {

                    $Excemption = $ExcemptionRepository->find((int)$data['f063fid']);
                    if($Excemption)
                    {
                        $ExcemptionObject = $ExcemptionRepository->updateData($Excemption,$data);
                    }
                }   
                else
                {
                 
                    $ExcemptionObject = $ExcemptionRepository->createNewData($data);
                }
            }
            
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated Successfully.'
                ]);
        }
        else
        {
            return $this->invalidInputType();
        }
    }

   
}