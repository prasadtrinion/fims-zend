<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class AssetMovementController extends AbstractAppController
{

    protected $sm; 

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T087fassetMovement');
        $result = $assetRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id=(int)$id;
       
        $assetRepository = $this->getRepository('T087fassetMovement');
        $asset = $assetRepository->find($id);
        if(!$asset)
        {
            return $this->resourceNotFound();
        }
        $result = $assetRepository->getListById((int)$id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            
            $assetRepository = $this->getRepository('T087fassetMovement');

            try
            {
                $assetObj = $assetRepository->createNewData($postData);
            }

           
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 200,
                            'message' => 'asset Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'asset Added successfully',
                    'assetId' => $assetObj->getF087fid()
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T087fassetMovement');
        $asset = $assetRepository->find((int)$id);

        if(!$asset)
        {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {

            $assetObj = $assetRepository->updateData($asset,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        }
      
    }

    public function assetMovementApprovalAction()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $assetMovementRepository = $this->getRepository('T087fassetMovement');
        $asset = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        $assetObjs=array();
        foreach($asset as $id)
        {
            $assetObj =$assetMovementRepository->find((int)$id);
            if(!$assetObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Asset does not exist.'
                ]);
            }
            array_push($assetObjs, $assetObj);
        }
        $asset = $assetMovementRepository->assetMovementApproval($assetObjs,$reason,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getListByApproved1StatusAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $ApprovedStatus = $this->params()->fromRoute('approvedStatus');
        $approvedStatus =(int)$ApprovedStatus;
        $assetRepository = $this->getRepository('T087fassetMovement');
        
        $result = $assetRepository->getListByApprovedStatus($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function assetMovementApproval2Action()
    {
       
        $em = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");
        $postData = json_decode($rawBody,true);

        $assetMovementRepository = $this->getRepository('T087fassetMovement');
        $asset = $postData['id'];
        $reason = $postData['reason'];
        $status = (int)$postData['status'];
        $assetObjs=array();
        foreach($asset as $id)
        {
            $assetObj =$assetMovementRepository->find((int)$id);
            if(!$assetObj)
            {
                return new JsonModel([
                    'status' => 410,
                    'message' => ' Does not exist.'
                ]);
            }
            array_push($assetObjs, $assetObj);
        }
        $asset = $assetMovementRepository->assetMovementApproval2($assetObjs,$reason,$status);
        return new JsonModel([
            'status' => 200,
            'message' => 'approved successfully'
        ]);
    }

    public function getListByApproved2StatusAction()
    {

        
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $ApprovedStatus = $this->params()->fromRoute('approvedStatus');
        $approvedStatus =(int)$ApprovedStatus;
        $assetRepository = $this->getRepository('T087fassetMovement');
        
        $result = $assetRepository->getListByApproved2Status($approvedStatus);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function assetNotInMovementAction()
    {
        // $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        $assetRepository = $this->getRepository('T087fassetMovement');
        $result = $assetRepository->assetNotInMovement();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }
    
    public function getCategoryItemAction()
    {

        $request = $this->getRequest();
        $itemSubCategoryRepository = $this->getRepository('T028fitemSubCategory');
        $result = $itemSubCategoryRepository->getCategoryItem();
        return new JsonModel([
            'status' => 200,
            'result' => $result 
        ]);
    }

}