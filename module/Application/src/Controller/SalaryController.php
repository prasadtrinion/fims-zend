<?php
namespace Application\Controller;

use Application\Repository\LocationRepository;
use Box\Spout\Writer\WriterFactory;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Box\Spout\Common\Type;


class SalaryController extends AbstractAppController
{
    protected $sm;

    public function __construct(ServiceManager $sm)
    {
        
        $this->sm = $sm;
        
        parent::__construct($sm);
    }

    public function getList()
    {

        $em = $this->getEntityManager();

        $request = $this->getRequest();

        $salaryRepository = $this->getRepository('T081fsalary');
        
        $result = $salaryRepository->getList();
        
        return new JsonModel([
            'status' => 200,
            'data' => $result
        ]);        
    }

    public function get($id)
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $id =(int)$id;
       
        $salaryRepository = $this->getRepository('T081fsalary');
        $salary = $salaryRepository ->find($id);
        if(!$salary)
        {
            return $this->resourceNotFound();
        }
        $result = $salaryRepository->getListById($id);
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }

    public function create($postData)
    {

        $em = $this->getEntityManager();
        $request = $this->getRequest();
        

        $rawBody = file_get_contents("php://input");

        
        if($this->isJSON($rawBody))
        {
            

             try
            {

                $salaryRepository = $this->getRepository('T081fsalary');               
                $salaryDetailsRepository = $this->getRepository('T081fsalaryDetails');  
                
                if (array_key_exists("f081fid", $postData)) 
                {
                    $salaryObj = $salaryRepository->find((int)$postData['f081fid']);
                        
                    $salaryObj = $salaryRepository->updateSalary($salaryObj, $postData);
                    $details = $postData['salary-details'];
                    foreach ($details as $detail) 
                    {
                        
    
                        if (array_key_exists("f081fidDetails", $detail)) 
                        {
    
                            $detailObj = $salaryDetailsRepository->find((int) $detail['f081fidDetails']);
    
                            if ($detailObj) 
                            {
                                $salaryDetailsRepository->updateDetail($detailObj, $detail);
                            }
                        } 
                        else 
                        {
    
                            $salaryDetailsRepository->createDetail($salaryObj, $detail);
                        }
                    }
                }
                else
                {
                $salary = $salaryRepository->findBy(array("f081fcode"=>strtolower($postData['f081fcode'])));
                if ($salary)
                {
                    return new JsonModel([
                            'status' => 409,
                            'message' => 'Salary Already exist.'
                        ]);
                }

                    $salaryObj = $salaryRepository->createSalary($postData);
                }
            }
            catch (Exception $e)
            {
                echo $e;
                // if($e->getErrorCode()== 1062)
                // {
                //         return new JsonModel([
                //             'status' => 409,
                //             'message' => 'Already exist.'
                //         ]);
                // }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }
    }

    public function update($id, $postData)
    {

        $em                      = $this->getEntityManager();
        $request                 = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        $id=(int)$id;
        $salaryRepository = $this->getRepository('T081fsalary');               
        $salaryDetailsRepository = $this->getRepository('T081fsalaryDetails');  


        $postData['f081fid'] = $id;
        if($this->isJSON($rawBody))
        {
            

             try
            {

          
                if (array_key_exists("f081fid", $postData)) 
                    {
                    $salaryObj = $salaryRepository->find((int)$postData['f081fid']);
                        
                    $salaryObj = $salaryRepository->updateSalary($salaryObj, $postData);
                 $details = $postData['salary-details'];
            
                foreach ($details as $detail) 
                {
    
                    if (array_key_exists("f081fidDetails", $detail)) 
                    {
    
                        $detailObj = $salaryDetailsRepository->findBy((int) $detail['f081fidDetails']);
    
                        if ($detailObj) {
                            $salaryDetailsRepository->updateDetail($detailObj, $detail);
                        }
                    } else {
    
                        $salaryDetailsRepository->createDetail($salaryObj, $detail);
                    }
                }
            }
            else{
                $salaryObj =$salaryRepository->createSalary($postData);
            }
        }
            catch (\Exception $e)
            {
                if($e->getErrorCode()== 1062)
                {
                        return new JsonModel([
                            'status' => 409,
                            'message' => 'Already exist.'
                        ]);
                }
            }
            return new JsonModel([
                    'status' => 200,
                    'message' => 'Added successfully',
                    
                ]);

        }
        else
        {
            return $this->invalidInputType();
        }

    }

    public function epfCalculationAction()
    {
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);

        $salaryRepository = $this->getRepository('T081fsalary');
        $salary = $salaryRepository ->epfCalculation($postData);
        return new JsonModel([
            'status' => 200,
            'message' => "Epf Calculated" 
        ]);
    }
     
    public function kwapCalculationAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
       
        $salaryRepository = $this->getRepository('T081fsalary');
       
        $salary = $salaryRepository-> kwapCalculation();
        return new JsonModel([
            'status' => 200,
            'message' => "calculated successfully"
        ]);
    }

    public function getEmployeePayslipAction()
    {
       
        $em = $this->getEntityManager();
        $request = $this->getRequest();
                $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
       
        $salaryRepository = $this->getRepository('T081fsalary');
       
        $list = $salaryRepository-> getEmployeePayslipData($postData);
        return new JsonModel([
            'status' => 200,
            'result' => $list
        ]);
    }


    public function deleteSalaryDetailsAction()
    {

        $rawBody           = file_get_contents("php://input");
        $postData          = json_decode($rawBody, true);
        $request           = $this->getRequest();
        $salaryDetailsRepository = $this->getRepository('T081fsalaryDetails');  

        // print_r($postData);exit;
        $result            = $salaryDetailsRepository->deleteSalaryDetails($postData);
        return new JsonModel([
            'status' => 200,
            'result' => 'Salary Details Deleted successfully',
        ]);
    }
}
