<?php
namespace Application\Controller;

use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;
use Box\Spout\Common\Type;

class BlockVendorController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;
        parent::__construct($sm);
    }
    public function getList()
    {
    	// print_r("hhd");exit;
        $request = $this->getRequest();
        $blockVendorRepository = $this->getRepository('T111fblockVendor');
        $blockVendor = $blockVendorRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $blockVendor 
        ]);
    }
    public function get($id)
    {
    	// print_r($id);exit;
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $blockVendorRepository = $this->getRepository('T111fblockVendor');
        $block_vendor = $blockVendorRepository->find((int)$id);
        // print_r($block_vendor);exit();
        if(!$block_vendor)
        {
            return $this->resourceNotFound();
        }
        $result = $blockVendorRepository->getListById((int)$id);
        // print_r($result);exit;
        return new JsonModel([
            'status' => 200,
            'result' => $result
        ]);
    }
    public function create($postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $rawBody = file_get_contents("php://input");
        
        if($this->isJSON($rawBody))
        {


            $Currentdate = date('Y-m-d', time());
            $vendorid = (int)$postData['f111fvendor'];


            $blockVendorRepository = $this->getRepository('T111fblockVendor');

            $vendor = $blockVendorRepository->findBy(array('f111fvendor'=>$vendorid));
            if (!$vendor)
            {
                try
                {
                 $blockvendorObj = $blockVendorRepository->createNewData($postData);
                }
                catch (Exception $e)
                    {
                        if($e->getErrorCode()== 1062)
                        {
                            return new JsonModel([
                            'status' => 412,
                            'message' => 'Error While Inserting'
                            ]);
                        }
                    }
                 return new JsonModel([
                        'status' => 200,
                        'message' => 'Added successfully',
                        ]);
            }

            $vendor = $vendor[0];         
            $vendorRelease = $vendor->getF111freleaseDate(); 
            $vendorRelease = $vendorRelease->format('Y-m-d');


            // $dat = $vendorRelease->date;
            // var_dump($dat);exit;            
            // $vendorRelease   = date('Y-m-d',strtotime($vendorRelease['date']));
            // print_r($vendorRelease);exit;
                if($vendorRelease > $Currentdate)
                {
                    // print_r($vendorRelease);exit;
                    return new JsonModel([
                            'status' => 200,
                            'message' => 'This User Already in block List',
                        ]);
                }
                else
                {
                    try
                    {
                        $blockvendorObj = $blockVendorRepository->createNewData($postData);

                    }
                    catch (Exception $e)
                    {
                        if($e->getErrorCode()== 1062)
                        {
                            return new JsonModel([
                            'status' => 200,
                            'message' => 'Already exist.'
                            ]);
                        }
                    }
                    
                    return new JsonModel([
                        'status' => 200,
                        'message' => 'Added successfully',
                        ]);
                }
        }
    }
    public function update($id, $postData)
    {
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $blockVendorRepository = $this->getRepository('T111fblockVendor');
        $blockVendor = $blockVendorRepository->find((int)$id);

        if(!$blockVendor)
        {
            return $this->resourceNotFound();
        }
        $rawBody = file_get_contents("php://input");

        if($this->isJSON($rawBody))
        {
            $blockvendorObj = $blockVendorRepository->updateData($blockVendor,$postData);
            return new JsonModel([
                'status' => 200,
                'message' => 'Updated successfully'
            ]);
        }
        else
        {
            return $this->invalidInputType();
        } 
    }
}
