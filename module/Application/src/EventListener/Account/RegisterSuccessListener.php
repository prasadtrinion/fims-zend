<?php
namespace Application\EventListener\Account;

use Interop\Container\ContainerInterface;
use Zend\EventManager\Event;

class RegisterSuccessListener
{
    
    protected $container;
    
    protected $options;
    
    protected $mailer;
    
    protected $logger;
    
    public function __construct(ContainerInterface $container, $options = [])
    {
        $this->container = $container;
        $this->options = $options;
        $this->mailer = $container->get('app.mailer');
        $this->logger = $container->get('app.logger');
    }
    
    public function __invoke(Event $e)
    {
        $data = $e->getParams();
        $user = $data['user'];
        $subject = 'Your Diversey Hang Allowance account has been created.';
        $template = 'email/register-success';
        
        try {
            $this->mailer->send($user['username'], $subject, $template, $data);
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
        
    }
}