<?php
namespace Application\EventListener\Admin;

use Interop\Container\ContainerInterface;
use Zend\EventManager\Event;
use Application\Entity\Account;
use Application\Entity\Role;

class UserEditListener
{
    
    protected $container;
    
    protected $options;
    
    protected $mailer;
    
    protected $logger;
    
    public function __construct(ContainerInterface $container, $options = [])
    {
        $this->container = $container;
        $this->options = $options;
        $this->mailer = $container->get('app.mailer');
        $this->logger = $container->get('app.logger');
    }
    
    public function __invoke(Event $e)
    {
        $data = $e->getParams();
        $user = $data['user'];
        $prevStatus = $data['prevStatus'];
        
        $sendMail = false;
        
        if ($user['accountType'] == Role::ROLE_DSR && $prevStatus != $user['status']) {
            if ($user['status'] == Account::STATUS_UNVERIFIED) {
                $sendMail = true;
                $subject = 'Account Creation Denied';
                $template = 'email/account-denied.phtml';
            } elseif ($user['status'] == Account::STATUS_VERIFIED) {
                $sendMail = true;
                $subject = 'Account Creation Approved';
                $template = 'email/account-approved.phtml';
            } elseif ($user['status'] == Account::STATUS_REJECTED) {
                $sendMail = true;
                $subject = 'Your diversey  account access is denied';
                $template = 'email/account-rejected.phtml';
            }
        }
        
        try {
            if($sendMail){
                $this->mailer->send($user['username'], $subject, $template, $data);
            }
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
        
    }
}