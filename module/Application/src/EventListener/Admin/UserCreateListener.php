<?php
namespace Application\EventListener\Admin;

use Interop\Container\ContainerInterface;
use Zend\EventManager\Event;
use Application\Entity\Account;
use Application\Entity\Role;

class UserCreateListener
{
    
    protected $container;
    
    protected $options;
    
    protected $mailer;
    
    protected $logger;
    
    public function __construct(ContainerInterface $container, $options = [])
    {
        $this->container = $container;
        $this->options = $options;
        $this->mailer = $container->get('app.mailer');
        $this->logger = $container->get('app.logger');
    }
    
    public function __invoke(Event $e)
    {
        $data = $e->getParams();
        $user = $data['user'];
        $role = $data['userRole'];
        
        $sendMail = false;
        
        if($user['status'] == Account::STATUS_VERIFIED){
            $sendMail = true;
            $subject = 'Welcome  Demo App';
            $template = 'email/account-approved.phtml';
        } else if($user['status'] == Account::STATUS_REJECTED){
            $sendMail = true;
            $subject = 'Your  account access is denied';
            $template = 'email/account-rejected.phtml';
        }
        
        if($role == 'SYSTEM_ADMIN' && $user['status'] == Account::STATUS_VERIFIED){
            $sendMail = true;
            $subject = 'Your Care Account has been created';
            $template = 'email/account-created.phtml';
        }
        
        try {
            if($sendMail){
                $this->mailer->send($user['username'], $subject, $template, $data);
            }
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
        
    }
}