<?php

namespace Itwine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupplierMST
 *
 * @ORM\Table(name="t024supplier")
 * @ORM\Entity(repositoryClass="Itwine\Repository\SupplierRepository")
 */
class SupplierMST
{
    /**
     * @var string
     *
     * @ORM\Column(name="f024idsupplier", type="string", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $IdSupplier;

    /**
     * @var string
     *
     * @ORM\Column(name="f024companyname", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Companyname;

    /**
     * @var string
     *
     * @ORM\Column(name="f024companyregnnumber", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Companyregnum;

    /**
     * @var string
     *
     * @ORM\Column(name="f024gst", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Gst;

    /**
     * @var string
     *
     * @ORM\Column(name="f024itnumber", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Itnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f024contactname", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Contactname;

    /**
     * @var string
     *
     * @ORM\Column(name="f024designation", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Designation;

    /**
     * @var string
     *
     * @ORM\Column(name="f024department", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Department;

    /**
     * @var string
     *
     * @ORM\Column(name="f024address1", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Address1;

    /**
     * @var string
     *
     * @ORM\Column(name="f024address2", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Address2;

    /**
     * @var string
     *
     * @ORM\Column(name="f024country", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Country;

    /**
     * @var string
     *
     * @ORM\Column(name="f024city", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $City;

    /**
     * @var string
     *
     * @ORM\Column(name="f024pincode", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Pincode;

    /**
     * @var string
     *
     * @ORM\Column(name="f024landlinenumber", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Landlinenumb;

    /**
     * @var string
     *
     * @ORM\Column(name="f024mobilenumber", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $MobileNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="f024fax", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Fax;

    /**
     * @var string
     *
     * @ORM\Column(name="f024emailid", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Email;

    /**
     * @var string
     *
     * @ORM\Column(name="f024altemailid", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Email2;

    /**
     * @var string
     *
     * @ORM\Column(name="f024accholdername", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Accholdername;

    /**
     * @var string
     *
     * @ORM\Column(name="f024accnumber", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Accnum;

    /**
     * @var string
     *
     * @ORM\Column(name="f024bankname", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $BankName;

    /**
     * @var string
     *
     * @ORM\Column(name="f024branchname", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Branchname;

    /**
     * @var string
     *
     * @ORM\Column(name="f024createdbyflag", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $Flag;

    /**
     * @var string
     *
     * @ORM\Column(name="f024createdon", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $createdon;

    /**
     * @var string
     *
     * @ORM\Column(name="f024modifiedon", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $modifiedon;

    /**
     * @var string
     *
     * @ORM\Column(name="f024categorylist", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $categorylist;

    /**
     * @var string
     *
     * @ORM\Column(name="f024suppliercode", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $suppliercode;


    /**
     * Get idSupplier
     *
     * @return string
     */
    public function getIdSupplier()
    {
        return $this->IdSupplier;
    }

    /**
     * Set companyname
     *
     * @param string $companyname
     *
     * @return SupplierMST
     */
    public function setCompanyname($companyname)
    {
        $this->Companyname = $companyname;

        return $this;
    }

    /**
     * Get companyname
     *
     * @return string
     */
    public function getCompanyname()
    {
        return $this->Companyname;
    }

    /**
     * Set companyregnum
     *
     * @param string $companyregnum
     *
     * @return SupplierMST
     */
    public function setCompanyregnum($companyregnum)
    {
        $this->Companyregnum = $companyregnum;

        return $this;
    }

    /**
     * Get companyregnum
     *
     * @return string
     */
    public function getCompanyregnum()
    {
        return $this->Companyregnum;
    }

    /**
     * Set gst
     *
     * @param string $gst
     *
     * @return SupplierMST
     */
    public function setGst($gst)
    {
        $this->Gst = $gst;

        return $this;
    }

    /**
     * Get gst
     *
     * @return string
     */
    public function getGst()
    {
        return $this->Gst;
    }

    /**
     * Set itnumber
     *
     * @param string $itnumber
     *
     * @return SupplierMST
     */
    public function setItnumber($itnumber)
    {
        $this->Itnumber = $itnumber;

        return $this;
    }

    /**
     * Get itnumber
     *
     * @return string
     */
    public function getItnumber()
    {
        return $this->Itnumber;
    }

    /**
     * Set contactname
     *
     * @param string $contactname
     *
     * @return SupplierMST
     */
    public function setContactname($contactname)
    {
        $this->Contactname = $contactname;

        return $this;
    }

    /**
     * Get contactname
     *
     * @return string
     */
    public function getContactname()
    {
        return $this->Contactname;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return SupplierMST
     */
    public function setDesignation($designation)
    {
        $this->Designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->Designation;
    }

    /**
     * Set department
     *
     * @param string $department
     *
     * @return SupplierMST
     */
    public function setDepartment($department)
    {
        $this->Department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->Department;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return SupplierMST
     */
    public function setAddress1($address1)
    {
        $this->Address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->Address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return SupplierMST
     */
    public function setAddress2($address2)
    {
        $this->Address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->Address2;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return SupplierMST
     */
    public function setCountry($country)
    {
        $this->Country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return SupplierMST
     */
    public function setCity($city)
    {
        $this->City = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * Set pincode
     *
     * @param string $pincode
     *
     * @return SupplierMST
     */
    public function setPincode($pincode)
    {
        $this->Pincode = $pincode;

        return $this;
    }

    /**
     * Get pincode
     *
     * @return string
     */
    public function getPincode()
    {
        return $this->Pincode;
    }

    /**
     * Set landlinenumb
     *
     * @param string $landlinenumb
     *
     * @return SupplierMST
     */
    public function setLandlinenumb($landlinenumb)
    {
        $this->Landlinenumb = $landlinenumb;

        return $this;
    }

    /**
     * Get landlinenumb
     *
     * @return string
     */
    public function getLandlinenumb()
    {
        return $this->Landlinenumb;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     *
     * @return SupplierMST
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->MobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->MobileNumber;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return SupplierMST
     */
    public function setFax($fax)
    {
        $this->Fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->Fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SupplierMST
     */
    public function setEmail($email)
    {
        $this->Email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * Set email2
     *
     * @param string $email2
     *
     * @return SupplierMST
     */
    public function setEmail2($email2)
    {
        $this->Email2 = $email2;

        return $this;
    }

    /**
     * Get email2
     *
     * @return string
     */
    public function getEmail2()
    {
        return $this->Email2;
    }

    /**
     * Set accholdername
     *
     * @param string $accholdername
     *
     * @return SupplierMST
     */
    public function setAccholdername($accholdername)
    {
        $this->Accholdername = $accholdername;

        return $this;
    }

    /**
     * Get accholdername
     *
     * @return string
     */
    public function getAccholdername()
    {
        return $this->Accholdername;
    }

    /**
     * Set accnum
     *
     * @param string $accnum
     *
     * @return SupplierMST
     */
    public function setAccnum($accnum)
    {
        $this->Accnum = $accnum;

        return $this;
    }

    /**
     * Get accnum
     *
     * @return string
     */
    public function getAccnum()
    {
        return $this->Accnum;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     *
     * @return SupplierMST
     */
    public function setBankName($bankName)
    {
        $this->BankName = $bankName;

        return $this;
    }

    /**
     * Get bankName
     *
     * @return string
     */
    public function getBankName()
    {
        return $this->BankName;
    }

    /**
     * Set branchname
     *
     * @param string $branchname
     *
     * @return SupplierMST
     */
    public function setBranchname($branchname)
    {
        $this->Branchname = $branchname;

        return $this;
    }

    /**
     * Get branchname
     *
     * @return string
     */
    public function getBranchname()
    {
        return $this->Branchname;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return SupplierMST
     */
    public function setFlag($flag)
    {
        $this->Flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->Flag;
    }

    /**
     * Set createdon
     *
     * @param string $createdon
     *
     * @return SupplierMST
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return string
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set modifiedon
     *
     * @param string $modifiedon
     *
     * @return SupplierMST
     */
    public function setModifiedon($modifiedon)
    {
        $this->modifiedon = $modifiedon;

        return $this;
    }

    /**
     * Get modifiedon
     *
     * @return string
     */
    public function getModifiedon()
    {
        return $this->modifiedon;
    }

    /**
     * Set categorylist
     *
     * @param string $categorylist
     *
     * @return SupplierMST
     */
    public function setCategorylist($categorylist)
    {
        $this->categorylist = $categorylist;

        return $this;
    }

    /**
     * Get categorylist
     *
     * @return string
     */
    public function getCategorylist()
    {
        return $this->categorylist;
    }

    /**
     * Set suppliercode
     *
     * @param string $suppliercode
     *
     * @return SupplierMST
     */
    public function setSuppliercode($suppliercode)
    {
        $this->suppliercode = $suppliercode;

        return $this;
    }

    /**
     * Get suppliercode
     *
     * @return string
     */
    public function getSuppliercode()
    {
        return $this->suppliercode;
    }
}
