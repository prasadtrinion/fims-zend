<?php
namespace Application\Command\Account;

use Application\Entity\Account;
use Application\Entity\User;
use Application\Entity\UserHasRole;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Zend\Validator\EmailAddress;
use Application\Command\AbstractCommand;
use Interop\Container\ContainerInterface;

/**
 * Command to create new user
 */
class CreateUserCommand extends AbstractCommand
{
    protected $container;

    protected static $allowedRoles = array(
        1 => 'SYSTEM_ADMIN',
        'ADMIN',
        'DSR'
    );

    public function __construct(ContainerInterface $container, $options = [])
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setCommandName('user:create')->setDescription('Create new user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = new QuestionHelper();
        $question = new ChoiceQuestion('<comment>Select a role</comment>', self::$allowedRoles);
        $role = $helper->ask($input, $output, $question);
        
        // Get email
        $question = new Question('Enter email address: ');
        $question->setValidator(function ($email) {
            $emailValidator = new EmailAddress();
            if (! $emailValidator->isValid($email)) {
                throw new \RuntimeException('Email address is invalid.');
            } elseif ($this->isAccountExists($email)) {
                throw new \RuntimeException('This email address is already in-use.');
            }
            
            return $email;
        });
        $question->setMaxAttempts(5);
        
        $email = $helper->ask($input, $output, $question);
        
        // Get password
        $question = new Question('Enter a password: ');
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $question->setValidator(function ($pass) {
            if (empty($pass) || strlen($pass) < 6) {
                throw new \RuntimeException('Password is required with minimum 6 characters.');
            }
            
            return $pass;
        });
        
        $question->setMaxAttempts(2);
        
        $password = $helper->ask($input, $output, $question);
        
        $this->createUser($email, $password, $role);
        
        $output->writeln('<info>User created successfully.</info>');
    }

    private function isAccountExists($email)
    {
        $em = $this->getEntityManager();
        
        $account = $em->getRepository('Application\Entity\Account')->findOneBy(array(
            'username' => $email
        ));
        
        if ($account) {
            return true;
        }
        
        return false;
    }

    private function createUser($username, $password, $role)
    {
        $password = password_hash($password, PASSWORD_BCRYPT, array(
            'cost' => 10
        ));
        
        $em = $this->getEntityManager();
        
        // Create Account
        $account = new Account();
        $account->setUsername($username)
            ->setPassword($password)
            ->setEmailStatus(1)
            ->setStatus(1)
            ->setCreateDtTm(new \DateTime())
            ->setLastLoggedInDtTm(new \DateTime());
        
        try {
            $em->persist($account);
            $em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        
        // Create User
        $user = new User();
        $user->setAccount($account)
            ->setFirstName('Application')
            ->setLastName('User')
            ->setEmail($username)
            ->setPhoneNumber('262-631-4001')
            ->setContactType(1)
            ->setAddressLine1('8310 16th Street')
            ->setAddressLine2('P.O. Box 902')
            ->setCountry('USA')
            ->setState('WI')
            ->setCity('Sturtevant')
            ->setZipCode('53177-0902')
            ->setSalesExecutive('ABC');
        
        $user->setCompanyName('Aero')
            ->setDistPhoneNumber('262-631-4001')
            ->setDistAddressLine1('8310 16th Street')
            ->setDistAddressLine2('P.O. Box 902')
            ->setDistCountry('USA')
            ->setDistState('WI')
            ->setDistCity('Sturtevant')
            ->setDistZipCode('53177-0902');
        
        $user->setCreateDtTm(new \DateTime())
            ->setUpdateDtTm(new \DateTime());
        
        try {
            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        
        // Create role
        $roleConstant = constant('Application\Entity\Role::ROLE_' . $role);
        $userHasRole = new UserHasRole();
        $userHasRole->setUserId($user->getId())
            ->setRoleId($roleConstant);
        
        try {
            $em->persist($userHasRole);
            $em->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
