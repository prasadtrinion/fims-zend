<?php
use Application\EventListener as Listeners;
use Application\Service as Services;
use Zend\Authentication\AuthenticationService;

return [
    'service_manager' => [
        'factories' => [
            
            // Services
            AuthenticationService::class => Application\Service\Authentication\AuthenticationServiceFactory::class,
            Services\Logger\LoggerService::class => Application\Service\Logger\MonologServiceFactory::class,
            // Services\Mailer\Mailer::class => Application\Service\Mailer\MailerServiceFactory::class,
          ],
        'aliases' => [
            'app.authentication' => AuthenticationService::class,
            'app.logger' => Services\Logger\LoggerService::class,
        ]
    ],
];



