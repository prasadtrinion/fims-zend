<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'home' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index'
            ]
        ]
    ],
    'login' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/login',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'login'
            ]
        ]
    ],
    'delete' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/delete',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'delete'
            ]
        ]
    ],
    'sendmail' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/sendmail',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'sendmail'
            ]
        ]
    ],
    'userApprove' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/userApprove',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'userApprove'
            ]
        ]
    ],
    'vendorlogin' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/vendorlogin',
            'defaults' => [
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'vendorlogin'
            ]
        ]
    ],
    'checkEmail' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/checkEmail',
            'defaults' => [
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'checkEmail'
            ]
        ]
    ],
    'sendMail' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/sendMail',
            'defaults' => [
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'sendMail'
            ]
        ]
    ],
    'studentLogin' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/studentLogin',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'studentLogin'
            ]
        ]
    ],
    'register' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/v1/register',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'register'
            ]
        ]
    ],
    
    'logout' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/v1/logout',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'logout'
            ]
        ]
    ],
    'dashboard' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/dashboard',
            'defaults' => [
                'controller' => Controller\DashboardController::class,
                'action' => 'index'
            ]
        ]
    ],
    'registerSuccess' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/register-success',
            'defaults' => [
                'controller' => Controller\AccountController::class,
                'action' => 'registerSuccess'
            ]
        ]
    ],
    'verifyEmail' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/verify-email/:token',
            'defaults' => [
                'controller' => Controller\AccountController::class,
                'action' => 'verifyEmail'
            ]
        ]
    ],
    'profile' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/v1/profile',
            'defaults' => [
                'controller' => Controller\AccountController::class,
                'action' => 'profile'
            ]
        ]
    ],
    'forgotPassword' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/v1/forgot-password',
            'defaults' => [
                'controller' => Controller\AccountController::class,
                'action' => 'forgotPassword'
            ]
        ]
    ],
    'resetPassword' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/reset-password/[:token]',
            'defaults' => [
                'controller' => Controller\AccountController::class,
                'action' => 'resetPassword'
            ]
        ]
    ],
    'master' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/master/[:repository][/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\MasterController::class
            ]
        ]
    ],
    'openingbalance' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/master/T016fopeningbalance[/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\OpeningbalanceController::class
            ]
        ]
    ],
    'role' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/role[/:id]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
            ]
        ]

    ],
    'roleMenus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/roleMenus[/:id]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
                'action' => 'roleMenus'
            ]
        ]

    ],

    'getMenu' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/menuRole[/:module][/:token]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
                'action' => 'getMenu'
            ]
        ]

    ],

    'getMenu1' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/menuRole1[/:module][/:token]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
                'action' => 'getMenu1'
            ]
        ]

    ],

    'getModule' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/moduleRole[/:token]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
                'action' => 'getModule'
            ]
        ]

    ],

    'getUser' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/userRole[/:id]',
            'defaults'=>[
                'controller' => Controller\RoleController::class,
                'action' => 'getUser'
            ]
        ]

    ],

    'Country' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/Country[/:id]',
            'defaults'=>[
                'controller' => Controller\CountryController::class,
                
            ]
        ]
    ],

    'getActiveCountry' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/activeCountry[/:id]',
            'defaults'=>[
                'controller' => Controller\CountryController::class,
                'action'=>'getActiveCountry'
            ]
        ]

    ],
    
    'getDefList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/defList[/:defcode]',
            'defaults'=>[
                'controller' => Controller\DefinationmsController::class,
                'action'=>'getDefList'
            ]
        ]

    ],

    'parentId' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/parentId[/:id]',
            'defaults'=>[
                'controller' => Controller\MenuController::class,
                'action'=>'parentId'
            ]
        ]

    ],

    'student' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/student[/:id]',
            'defaults' => [
                'controller' => Controller\StudentController::class,
              //  'action' => 'resetPassword'
            ]
        ]
    ],
    

    'college' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/college[/:id]',
            'defaults'=>[
                'controller' => Controller\CollegeController::class,
            ]
        ]

    ],

    'textCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/textCode[/:id]',
            'defaults'=>[
                'controller' => Controller\TextCodeController::class,
            ]
        ]

    ],

    'customer' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/customer[/:id]',
            'defaults'=>[
                'controller' => Controller\CustomerController::class,
            ]
        ]

    ],

    'customerPayment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/customerPayment[/:id]',
            'defaults'=>[
                'controller' => Controller\CustomerController::class,
                'action' => 'customerPayment'
            ]
        ]

    ],

    'menu' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/menu[/:id]',
            'defaults'=>[
                'controller' => Controller\MenuController::class,
            ]
        ]

    ],

    'country' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/country[/:id]',
            'defaults'=>[
                'controller' => Controller\CountryController::class,
            ]
        ]

    ],

    'group' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/group[/:id]',
            'defaults'=>[
                'controller' => Controller\GroupController::class,
            ]
        ]

    ],

    'company' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/company[/:id]',
            'defaults'=>[
                'controller' => Controller\CompanyController::class,
            ]
        ]

    ],

    'definationtypems' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/definationtypems[/:id]',
            'defaults'=>[
                'controller' => Controller\DefinationtypemsController::class,
            ]
        ]

    ],


    'definationms' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/definationms[/:id]',
            'defaults'=>[
                'controller' => Controller\DefinationmsController::class,
            ]
        ]

    ],

    'glcode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/glcode[/:id]',
            'defaults'=>[
                'controller' => Controller\GlcodeController::class,
            ]
        ]

    ],

    'financialyear' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/financialyear[/:id]',
            'defaults'=>[
                'controller' => Controller\FinancialyearController::class,
            ]
        ]

    ],

    'openingbalance' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/openingbalance[/:id]',
            'defaults'=>[
                'controller' => Controller\OpeningBalanceController::class,
            ]
        ]

    ],

    'getOpeningbalanceApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getOpeningbalanceApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\OpeningBalanceController::class,
                'action' => 'getOpeningbalanceApprovalList',
            ]
        ]
    ],



    'openingBalanceApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/openingBalanceApprovalStatus',
            'defaults' => [
                'controller' => Controller\OpeningBalanceController::class, 
                'action' => 'openingBalanceApprovalStatus'   
            ]
        ]
    ],


    'journal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/journal[/:id]',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
            ]
        ]
    ],

    'journalApprove' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/journalApprove[/:id]',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
                'action' => 'approve'
            ]
        ]
    ],

    'monthlySummary' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/monthlySummary[/:month]',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
                'action' => 'monthlySummary'
            ]
        ]
    ],
    'getJournals' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getJournals[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
                'action' => 'getJournals'
            ]
        ]
    ],
    'cashBook' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashBook',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
                'action' => 'cashBook'
            ]
        ]
    ],

    'journalReversal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/journalReversal',
            'defaults'=>[
                'controller' => Controller\JournalController::class,
                'action' => 'journalReversal'
            ]
        ]
    ],

    'lawyer' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/lawyer[/:id]',
            'defaults'=>[
                'controller' => Controller\LawyerController::class,
            ]
        ]

    ],

    'currencyRateSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/currencyRateSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\CurrencyRateSetupController::class,
            ]
        ]

    ],

    'faq' => [
        'type' => 'Literal',
        'options' => [
            'route' => '/faq',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'faq'
            ]
        ]
    ],
    'application' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/application[/:action]',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index'
            ]
        ]
    ],
    'account' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/account[/:action]',
            'defaults' => [
                'controller' => Controller\AccountController::class,
            ]
        ]
    ],
    'activityCode' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/activityCode[/:refcode]',
            'defaults' => [
                'controller' => Controller\ActivityCodeController::class,
            ]
        ]
    ],
    'activityLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/activityLevelList[/:level]',
            'defaults'=>[
                'controller' => Controller\ActivityCodeController::class,
                'action'=>'activityLevelList',
            ]
        ]

    ],

    'activityCodeStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/activityCodeStatus',
            'defaults'=>[
                'controller' => Controller\ActivityCodeController::class,
                'action'=>'activityCodeStatus',
            ]
        ]

    ],
    'activityParentLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/activityParentLevelList[/:parent][/:level]',
            'defaults'=>[
                'controller' => Controller\ActivityCodeController::class,
                'action'=>'activityParentLevelList',
            ]
        ]

    ],
    'newActivityCode' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/newActivityCode',
            'defaults' => [
                'controller' => Controller\ActivityCodeController::class,
                'action'=>'newActivityCode',
            ]
        ]
    ],
    'deleteActivityCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteActivityCode[/:id]',
            'defaults'=>[
                'controller' => Controller\ActivityCodeController::class,
                'action'=>'deleteActivityCode',
            ]
        ]

    ],

    'accountCode' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/accountCode[/:refcode]',
            'defaults' => [
                'controller' => Controller\AccountCodeController::class,
            ]
        ]
    ],

    'newAccountCode' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/newAccountCode',
            'defaults' => [
                'controller' => Controller\AccountCodeController::class,
                'action'=>'newAccountCode',
            ]
        ]
    ],

    'accountLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/accountLevelList[/:level]',
            'defaults'=>[
                'controller' => Controller\AccountCodeController::class,
                'action'=>'accountLevelList',
            ]
        ]

    ],
    'accountParentLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/accountParentLevelList[/:parent][/:level]',
            'defaults'=>[
                'controller' => Controller\AccountCodeController::class,
                'action'=>'accountParentLevelList',
            ]
        ]

    ],

    
    'ledger' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/ledger[/:id]',
            'defaults' => [
                'controller' => Controller\LedgerController::class
            ]
        ]
    ],
    
    'customerLogin' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/customerLogin',
            'defaults' => [
                'controller' => Controller\CustomerController::class,
                'action' => 'customerLogin'
            ]
        ]
    ],
    'budgetDepartmentActivity' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetDepartmentActivity[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class          
            ]
        ]
    ],

    'budgetIncrement' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetIncrement[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetIncrementController::class          
            ]
        ]
    ],

    'ActivityApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/ActivityApprovalStatus',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class, 
                'action' => 'ActivityApprovalStatus'   
            ]
        ]
    ],

    'getActivityApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActivityApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'getActivityApprovalList',
            ]
        ]
    ],

    'getActivityAmount' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActivityAmount[/:idDepartment][/:idFinancialyear]',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'getActivityAmount',
            ]
        ]
    ],

    'getActivityDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActivityDetails[/:idDepartment][/:idFinancialyear]',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'getActivityDetails',
            ]
        ]
    ],

    'getActivityGlcode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActivityGlcode[/:idDepartment][/:idFinancialyear]',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'getActivityGlcode',
            ]
        ]
    ],

    'updateVeriment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/updateVeriment',
            'defaults'=>[
                'controller' => Controller\BudgetVerimentController::class,
                'action' => 'updateVeriment',
            ]
        ]
    ],

    'IncrementApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/IncrementApprovalStatus',
            'defaults' => [
                'controller' => Controller\BudgetIncrementController::class, 
                'action' => 'IncrementApprovalStatus'   
            ]
        ]
    ],

    'deleteIncrement' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteIncrement',
            'defaults' => [
                'controller' => Controller\BudgetIncrementController::class, 
                'action' => 'deleteIncrement'   
            ]
        ]
    ],

    'getIncrementApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getIncrementApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\BudgetIncrementController::class,
                'action' => 'getIncrementApprovalList',
            ]
        ]
    ],

    'budgetCostcenter' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetCostcenter[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetCostcenterController::class, 

            ]
        ]
    ],


    'CostcenterApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/CostcenterApprovalStatus[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetCostcenterController::class, 
                'action' => 'CostcenterApprovalStatus'   
            ]
        ]
    ],

    'getCostcenter' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getCostcenter[/:idFinancialyear]',
            'defaults' => [
                'controller' => Controller\BudgetCostcenterController::class, 
                'action' => 'getCostcenter'   
            ]
        ]
    ],

    'getCostcenterApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCostcenterApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\BudgetCostcenterController::class,
                'action' => 'getCostcenterApprovalList',
            ]
        ]
    ],

    'getCostcenterAmount' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCostcenterAmount[/:idFinancialyear][/:idDepartment][/:fund]',
            'defaults'=>[
                'controller' => Controller\BudgetCostcenterController::class,
                'action' => 'getCostcenterAmount',
            ]
        ]
    ],

    'veriment' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/veriment[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetVerimentController::class,   
            ]
        ]
    ],

    'VerimentApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/VerimentApprovalStatus',
            'defaults' => [
                'controller' => Controller\BudgetVerimentController::class, 
                'action' => 'VerimentApprovalStatus'   
            ]
        ]
    ],

    'getVerimentApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getVerimentApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\BudgetVerimentController::class,
                'action' => 'getVerimentApprovalList',
            ]
        ]
    ],

    'getVerimentAmount' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getVerimentAmount[/:idFinancialyear][/:idDepartment][/:idGlcode]',
            'defaults'=>[
                'controller' => Controller\BudgetVerimentController::class,
                'action' => 'getVerimentAmount',
            ]
        ]
    ],

    'getIncrementAmount' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getIncrementAmount[/:idFinancialyear][/:idDepartment]',
            'defaults'=>[
                'controller' => Controller\BudgetIncrementController::class,
                'action' => 'getIncrementAmount',
            ]
        ]
    ],



    'showTabs' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/showTabs[/:id]',
            'defaults' => [
                'controller' => Controller\ShowTabsController::class
            ]
        ]
    ],
    'feecategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/feecategory',
            'defaults' => [
                'controller' => Controller\FeeCategoryController::class,
            ]
        ]
    ],

    'insuranceSetup' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/insuranceSetup[/:id]',
            'defaults' => [
                'controller' => Controller\InsuranceSetupController::class,
            ]
        ]
    ],
    'creditorDebtor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/creditorDebtor[/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\CreditorDebtorController::class,
            ]
        ]
    ],
    'creditorDebtorApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/creditorDebtorApprove',
            'defaults' => [
                'controller' => Controller\CreditorDebtorController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'premise' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/premise[/:id]',
            'defaults' => [
                'controller' => Controller\PremiseController::class,
            ]
        ]
    ],
    'recurringSetUp' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/recurringSetUp[/:id]',
            'defaults' => [
                'controller' => Controller\RecurringSetUpController::class,
            ]
        ]
    ],
    'utility' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/utility[/:id]',
            'defaults' => [
                'controller' => Controller\UtilityController::class,
            ]
        ]
    ],
    'utilityPremises' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/utilityPremises[/:id]',
            'defaults' => [
                'controller' => Controller\UtilityController::class,
                'action' => 'utilityPremises'
            ]
        ]
    ],
    'utilityRateSetup' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/utilityRateSetup[/:id]',
            'defaults' => [
                'controller' => Controller\UtilityRateSetupController::class,
            ]
        ]
    ],
    'utilityBill' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/utilityBill[/:id]',
            'defaults' => [
                'controller' => Controller\UtilityBillController::class,
            ]
        ]
    ],

    'feecategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/feecategory[/:id]',
            'defaults' => [
                'controller' => Controller\FeeCategoryController::class,
            ]
        ]
    ],

    'feeItem' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/feeItem[/:id]',
            'defaults' => [
                'controller' => Controller\FeeItemController::class,
            ]
        ]
    ],

    'getTaxList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getTaxList[/:id]',
            'defaults' => [
                'controller' => Controller\TextCodeController::class,
                'action'=> 'getTaxList'
            ]
        ]
    ],

    'activeList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/activeList/[:repository]',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'activeList'
            ]
        ]
    ],

    'generateNumber' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/generateNumber',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'generateNumber'
            ]
        ]
    ],

    'generateStudentFinanceNumber' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/generateStudentFinanceNumber',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'generateStudentFinanceNumber'
            ]
        ]
    ],

    'getApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getApprovalList[/:status]',
            'defaults'=>[
                'controller' => Controller\NonPoInvoiceController::class,
                'action' => 'getApprovalList',
            ]
        ]
    ],

    'approvalStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approvalStatus',
            'defaults'=>[
                'controller' => Controller\NonPoInvoiceController::class,
                'action' => 'approvalStatus',
            ]
        ]
    ],
    'receipt' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/receipt[/:id]',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
            ]
        ]
    ],
    'receiptApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/receiptApprove',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'approve'
            ]
        ]
    ],

    'nonPoInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/nonPoInvoice[/:id]',
            'defaults' => [
                'controller' => Controller\NonPoInvoiceController::class,
            ]
        ]
    ],
    'getReceipts' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getReceipts[/:id]',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'receipts'
            ]
        ]
    ],
    'vendorReceipts' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/vendorReceipts[/:id]',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'vendorReceipts'
            ]
        ]
    ],

    'requestInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/requestInvoice[/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\RequestInvoiceController::class,
            ]
        ]
    ],
    'requestInvoiceByType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/requestInvoiceByType',
            'defaults' => [
                'controller' => Controller\RequestInvoiceController::class,
                'action' => 'requestInvoiceByType'
            ]
        ]
    ],
    'requestInvoiceApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/requestInvoiceApprove',
            'defaults' => [
                'controller' => Controller\RequestInvoiceController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'customerRequestInvoices' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/customerRequestInvoices[/:id]',
            'defaults' => [
                'controller' => Controller\RequestInvoiceController::class,
                'action' => 'customerRequestInvoices'
            ]
        ]
    ],
    'masterInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/masterInvoice[/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
            ]
        ]
    ],
    'invoiceCron' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/invoiceCron',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'invoiceCron'
            ]
        ]
    ],
    'masterInvoiceApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/masterInvoiceApprove',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'customerMasterInvoices' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/customerMasterInvoices[/:id]',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'customerMasterInvoices'
            ]
        ]
    ],
    'customerMasterInvoiceDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/customerMasterInvoiceDetails[/:id]',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'customerMasterInvoiceDetails'
            ]
        ]
    ],
    'creditNote' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/creditNote[/:id]',
            'defaults' => [
                'controller' => Controller\CreditNoteController::class
            ]
        ]
    ],
    'approvedCreditNotes' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvedCreditNotes[/:status]',
            'defaults' => [
                'controller' => Controller\CreditNoteController::class,
                'action' => 'approvedCreditNotes'
            ]
        ]
    ],
    'creditNoteApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/creditNoteApprove',
            'defaults' => [
                'controller' => Controller\CreditNoteController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'debitNote' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/debitNote[/:id]',
            'defaults' => [
                'controller' => Controller\DebitNoteController::class
            ]
        ]
    ],
    'debitNoteApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/debitNoteApprove',
            'defaults' => [
                'controller' => Controller\DebitNoteController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'approvedDebitNotes' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvedDebitNotes[/:status]',
            'defaults' => [
                'controller' => Controller\DebitNoteController::class,
                'action' => 'approvedDebitNotes'
            ]
        ]
    ],
    'programme' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/programme[/:id]',
            'defaults' => [
                'controller' => Controller\ProgrammeController::class,
            ]
        ]
    ],

    'intake' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/intake[/:id]',
            'defaults' => [
                'controller' => Controller\IntakeController::class,
            ]
        ]
    ],

    'debtInformation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/debtInformation[/:id]',
            'defaults' => [
                'controller' => Controller\DebtInformationController::class,
            ]
        ]
    ],

    'paymentVoucher' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/paymentVoucher[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
            ]
        ]
    ],

    'PaymentApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/PaymentApprovalStatus',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class, 
                'action' => 'PaymentApprovalStatus'   
            ]
        ]
    ],

    'getPaymentApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getPaymentApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getPaymentApprovalList',
            ]
        ]
    ],

    'getModuleList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getModuleList',
            'defaults'=>[
                'controller' => Controller\MenuController::class,
                'action' => 'getModuleList',
            ]
        ]
    ],

    'approveAll' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approveAll',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'approveAll',
            ]
        ]
    ],

    'getApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getApprovedList',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'getApprovedList',
            ]
        ]
    ],

    'cashbank' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashbank[/:id]',
            'defaults'=>[
                'controller' => Controller\CashBankController::class,
            ]
        ]
    ],

    'claimCustomer' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/claimCustomer[/:id]',
            'defaults'=>[
                'controller' => Controller\ClaimCustomerController::class,
            ]
        ]
    ],

    'sellInvestment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sellInvestment',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,

            ]
        ]
    ],

    'sellApproveOne' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sellApproveOne',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'sellApproveOne',
            ]
        ]
    ],

    'sellApproveTwo' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sellApproveTwo',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'sellApproveTwo',
            ]
        ]
    ],

    'sellRejectOne' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sellRejectOne',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'sellRejectOne',
            ]
        ]
    ],

    'sellRejectTwo' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sellRejectTwo',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'sellRejectTwo',
            ]
        ]
    ],

    'getSellApprovalOneList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSellApprovalOneList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'getSellApprovalOneList',
            ]
        ]
    ],

    'getSellApprovalTwoList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSellApprovalTwoList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'getSellApprovalTwoList',
            ]
        ]
    ],

    'getSellApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSellApprovedList',
            'defaults'=>[
                'controller' => Controller\SellInvestmentController::class,
                'action' => 'getSellApprovedList',
            ]
        ]
    ],

    'voucher' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/voucher[/:id][/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\VoucherController::class,
            ]
        ]
    ],
    'approvedVouchers' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvedVouchers[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\VoucherController::class,
                'action' => 'approvedVouchers'
            ]
        ]
    ],
    'voucherApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/voucherApprove',
            'defaults' => [
                'controller' => Controller\VoucherController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'customerVouchers' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/customerVouchers[/:id]',
            'defaults' => [
                'controller' => Controller\VoucherController::class,
                'action' => 'customerVouchers'

            ]
        ]
    ],

    'getClaimcustomerVoucher' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getClaimcustomerVoucher[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getClaimcustomerVoucher'

            ]
        ]
    ],
    'getClaimcustomerType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getClaimcustomerType[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getClaimcustomerType'

            ]
        ]
    ],
    'getStudentVoucher' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getStudentVoucher[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getStudentVoucher'

            ]
        ]
    ],
    'getStudentType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getStudentType[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getStudentType'

            ]
        ]
    ],

    'getVoucherList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getVoucherList[/:id]',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getVoucherList'

            ]
        ]
    ],
    'budgetSummary' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetSummary',
            'defaults' => [
                'controller' => Controller\BudgetSummaryController::class,
                'action' => 'budgetSummary'
            ]
        ]
    ],

    'discount' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/discount[/:id]',
            'defaults' => [
                'controller' => Controller\DiscountController::class,

            ]
        ]
    ],

    'DiscountApprovalStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/DiscountApprovalStatus',
            'defaults' => [
                'controller' => Controller\DiscountController::class,
                'action' => 'DiscountApprovalStatus'

            ]
        ]
    ],

    'getDiscountApprovalList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getDiscountApprovalList[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\DiscountController::class,
                'action' => 'getDiscountApprovalList'

            ]
        ]
    ],
    'accountJournals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/accountJournals[/:accountId]',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'accountJournals'

            ]
        ]
    ],

    'feeStructure' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/feeStructure[/:id]',
            'defaults' => [
                'controller' => Controller\FeeStructureController::class,

            ]
        ]
    ],

    'generateInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/generateInvoice[/:id]',
            'defaults' => [
                'controller' => Controller\GenerateInvoiceController::class,

            ]
        ]
    ],

    'investmentRegistration' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentRegistration[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,

            ]
        ]
    ],

    'updateStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/updateStatus[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'updateStatus'

            ]
        ]
    ],

    'legalInformation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/legalInformation[/:id]',
            'defaults' => [
                'controller' => Controller\LegalInformationController::class,
            ]
        ]
    ],

    'generateStudentInvoice' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/generateStudentInvoice',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'generateStudentInvoice'
            ]
        ]
    ],

    'generateRefundableStudentInvoice' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/generateRefundableStudentInvoice',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'generateRefundableStudentInvoice'
            ]
        ]
    ],

    'sponsorStudents' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorStudents[/:id]',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'sponsorStudents'
            ]
        ]
    ],

    'sponsorStudentInvoices' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorStudentInvoices[/:id]',
            'defaults' => [
                'controller' => Controller\SponsorController::class,
                'action' => 'sponsorStudentInvoices'
            ]
        ]
    ],

    'studentInvoices' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentInvoices[/:id]',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'studentInvoices'
            ]
        ]
    ],
    'invoicesByType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/invoicesByType',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'invoicesByType'
            ]
        ]
    ],

    'studentTagging' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentInsuranceTagging[/:id]',
            'defaults' => [
                'controller' => Controller\StudentTaggingController::class,
            ]
        ]
    ],

    'sponsorBill' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorBill[/:id]',
            'defaults' => [
                'controller' => Controller\SponsorBillController::class,
            ]
        ]
    ],

    'sponsorHasBills' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorHasBills[/:id]',
            'defaults' => [
                'controller' => Controller\SponsorBillController::class,
                'action' => 'sponsorHasBills',
            ]
        ]
    ],

    'tempspnsorinv' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/tempspnsorinv[/:id]',
            'defaults' => [
                'controller' => Controller\SponsorBillController::class,
                'action' => 'sponsorHasInvoices'
            ]
        ]
    ],

    'studentBond' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentBond[/:id]',
            'defaults' => [
                'controller' => Controller\StudentBondController::class,

            ]
        ]
    ],

    'studentBondByApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentBondByApproval[/:id]',
            'defaults' => [
                'controller' => Controller\StudentBondController::class,
                'action' => 'getByApprovalStatus'

            ]
        ]
    ],

    'studentBondApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentBondApprove',
            'defaults' => [
                'controller' => Controller\StudentBondController::class,
                'action' => "studentBondApprove"

            ]
        ]
    ],

    'revenueSetup' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/revenueSetup[/:id]',
            'defaults' => [
                'controller' => Controller\RevenueSetUpController::class,

            ]
        ]
    ],

    'purchaseLimit' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseLimit[/:id]',
            'defaults' => [
                'controller' => Controller\PurchaseLimitController::class,

            ]
        ]
    ],

    'purchaseRequisition' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseRequisition[/:id]',
            'defaults' => [
                'controller' => Controller\PurchaseRequisitionController::class,

            ]
        ]
    ],

    'purchaseRequisitionApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseRequisitionApprove',
            'defaults' => [
                'controller' => Controller\PurchaseRequisitionController::class,
                'action' => 'purchaseRequisitionApprove'

            ]
        ]
    ],

    'getPurchaseApprovedList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getPurchaseApprovedList[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\PurchaseRequisitionController::class,
                'action' => 'getPurchaseApprovedList'

            ]
        ]
    ],

    'purchaseOrder' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrder[/:id]',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,

            ]
        ]
    ],

    'purchaseOrderWarranties' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrderWarranties',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'purchaseOrderWarranties'

            ]
        ]
    ],

    'supplierPurchaseOrders' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/supplierPurchaseOrders[/:id]',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'supplierPurchaseOrders'

            ]
        ]
    ],

    'purchaseOrderApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrderApprove',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'purchaseOrderApprove'

            ]
        ]
    ],

    'getPurchaseOrderApprovedList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getPurchaseOrderApprovedList[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'getPurchaseOrderApprovedList'

            ]
        ]
    ],

    'vendorRegistration' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/vendorRegistration[/:id]',
            'defaults' => [
                'controller' => Controller\VendorRegistrationController::class,

            ]
        ]
    ],

    'grn' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/grn[/:id]',
            'defaults' => [
                'controller' => Controller\GrnController::class,

            ]
        ]
    ],
    'awaitingGrns' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/awaitingGrns',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'awaitingGrns'
            ]
        ]
    ],
    'expenseGrns' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/expenseGrns',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'expenseGrns'
            ]
        ]
    ],

    'donorGrns' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/donorGrns',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'donorGrns'

            ]
        ]
    ],

    'getApproveUser' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getApproveUser',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'getApproveUser'
            ]
        ]
    ],

    'getRejectUser' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getRejectUser',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'getRejectUser'
            ]
        ]
    ],


    'departmentCodeView' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/departmentCodeView[/:id]',
            'defaults' => [
                'controller' => Controller\ViewsController::class,
                'action' => 'departmentCodeView'
            ]
        ]
    ],

    'unitCodeView' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/unitCodeView[/:id]',
            'defaults' => [
                'controller' => Controller\ViewsController::class,
                'action' => 'unitCodeView'
            ]
        ]
    ],
    'view' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/view[/:name]',
            'defaults' => [
                'controller' => Controller\ViewsController::class,
                'action' => 'view'
            ]
        ]
    ],


    'userApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/userApproval',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'userApproval'
            ]
        ]
    ],

    'userRejection' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/userRejection',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'userRejection'
            ]
        ]
    ], 
    
    'assetDepreciation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/assetDepreciation[/:id]',
            'defaults' => [
                'controller' => Controller\AssetDepreciationController::class,

            ]
        ]
    ],

    'premiseType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/premiseType[/:id]',
            'defaults' => [
                'controller' => Controller\PremiseTypeSetupController::class,
            ]
        ]
    ],

    'investmentApplication' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentApplication[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
            ]
        ]
    ],

    'investmentInstitute' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentInstitute[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentInstitutionController::class,
            ]
        ]
    ],

    'revenue' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/revenue[/:id]',
            'defaults' => [
                'controller' => Controller\RevenueItemSetupController::class,
            ]
        ]
    ],


    'assetMaintenance' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/assetMaintenance[/:id]',
            'defaults' => [
                'controller' => Controller\AssetMaintenanceController::class,
            ]
        ]
    ],

    'registrationApproveAll' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationApproveAll',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationApproveAll',
            ]
        ]
    ],

    'registrationApproveOne' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationApproveOne',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationApproveOne',
            ]
        ]
    ],

    'registrationApproveTwo' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationApproveTwo',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationApproveTwo',
            ]
        ]
    ],

    'registrationApproveThree' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationApproveThree',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationApproveThree',
            ]
        ]
    ],

    'registrationRejectOne' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationRejectOne',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationRejectOne',
            ]
        ]
    ],

    'registrationRejectTwo' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationRejectTwo',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationRejectTwo',
            ]
        ]
    ],

    'registrationRejectThree' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationRejectThree',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'registrationRejectThree',
            ]
        ]
    ],



    'getRegistrationApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getRegistrationApprovedList',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'getRegistrationApprovedList',
            ]
        ]
    ],

    'createApprove' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/createApprove',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'createApprove',
            ]
        ]
    ],
    'reinvestment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/reinvestment',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'reinvestment',
            ]
        ]
    ],
    
    'loanCancelRequest' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/loanCancelRequest[/:id]',
            'defaults'=>[
                'controller' => Controller\LoanCancelRequestController::class,
            ]
        ]
    ],
    'blacklistEmp' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/blacklistEmp[/:id]',
            'defaults'=>[
                'controller' => Controller\BlacklistEmpController::class,
            ]
        ]
    ],
    'tender' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/tender[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderQuotationController::class,
            ]
        ]
    ],

    'assetMovement' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetMovement[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
            ]
        ]
    ],

    'tenderSuppliers' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/tenderSuppliers[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSuppliersController::class,
            ]
        ]
    ],

    'assetMovementApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetMovementApproval',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
                'action' => 'assetMovementApproval',
            ]
        ]
    ],

    'getListByApproved1Status' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListByApproved1Status[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
                'action' => 'getListByApproved1Status',
            ]
        ]
    ],

    'assetMaintenanceApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetMaintenanceApproval',
            'defaults'=>[
                'controller' => Controller\AssetMaintenanceController::class,
                'action' => 'assetMaintenanceApproval',
            ]
        ]
    ],

    'getMaintenanceApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getMaintenanceApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\AssetMaintenanceController::class,
                'action' => 'getMaintenanceApprovedList',
            ]
        ]
    ],
    'generateUtilityInvoice' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/generateUtilityInvoice',
            'defaults'=>[
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'generateUtilityInvoice',
            ]
        ]
    ],
    'generateRecurringSetupInvoice' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/generateRecurringSetupInvoice',
            'defaults'=>[
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'generateRecurringSetupInvoice',
            ]
        ]
    ],
    'generateStudentFeeInvoice' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/generateStudentFeeInvoice',
            'defaults'=>[
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'generateStudentFeeInvoice',
            ]
        ]
    ],
    'generateStudentFeeInvoiceUpdated' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/generateStudentFeeInvoiceUpdated',
            'defaults'=>[
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'generateStudentFeeInvoiceUpdated',
            ]
        ]
    ],

    'assetDisposal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetDisposal[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetDisposalController::class,
            ]
        ]
    ],
    'assetDisposalVerify' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetDisposalVerify',
            'defaults'=>[
                'controller' => Controller\AssetDisposalController::class,
                'action' => 'assetDisposalVerify',
            ]
        ]
    ],

    'getVerfiedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getVerfiedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\AssetDisposalController::class,
                'action' => 'getVerfiedList',
            ]
        ]
    ],
    
    'socsoSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/socsoSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\SocsoSetupController::class,
                
            ]
        ]
    ],
    'salaryEntry' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/salaryEntry[/:id]',     
            'defaults'=>[
                'controller' => Controller\SalaryEntryController::class,
            ]
        ]
    ],

    'dateMaintenance' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/dateMaintenance[/:id]',
            'defaults'=>[
                'controller' => Controller\DateMaintenanceController::class,
            ]
        ]
    ],

    'epfSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/epfSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\EpfSetupController::class,
            ]
        ]
    ],

    'payrollProcess' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/payrollProcess[/:id]',
            'defaults'=>[
                'controller' => Controller\PayrollProcessController::class,
            ]
        ]
    ],

    'sponsor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsor[/:id]',
            'defaults' => [
                'controller' => Controller\SponsorController::class,

            ]
        ]
    ],

    'payroll' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/payroll[/:id]',
            'defaults' => [
                'controller' => Controller\PayrollController::class,

            ]
        ]
    ],
    'eaFormProcess' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/eaFormProcess[/:id]',
            'defaults' => [
                'controller' => Controller\EaFormProcessController::class,

            ]
        ]
    ],

    'getGrnApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getGrnApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\GrnController::class,
                'action' => 'getGrnApprovalList',
            ]
        ]
    ],

    'subject' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/subject[/:id]',
            'defaults' => [
                'controller' => Controller\SubjectController::class,
            ]
        ]
    ],

    'GrnApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/GrnApproval[/:id]',
            'defaults'=>[
                'controller' => Controller\GrnController::class,
                'action' => 'GrnApproval',
            ]
        ]
    ],

    'batchGeneration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/batchGeneration[/:id]',
            'defaults'=>[
                'controller' => Controller\BatchController::class,
            ]
        ]
    ],

    'nonBatchReceipts' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/nonBatchReceipts',
            'defaults'=>[
                'controller' => Controller\BatchController::class,
                'action' => 'nonBatchReceipts'
            ]
        ]
    ],

    'eaForm' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/eaForm[/:id]',
            'defaults'=>[
                'controller' => Controller\EaformController::class,
            ]
        ]
    ],

    'onlineBankPayment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/onlineBankPayment[/:id][/:date]',
            'defaults'=>[
                'controller' => Controller\OnlineBankPaymentController::class,
            ]
        ]
    ],

    'onlineBankPaymentByApprove' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/onlineBankPaymentByApprove[/:id]',
            'defaults'=>[
                'controller' => Controller\OnlineBankPaymentController::class,
                'action' => 'onlineBankPaymentByApprove'
            ]
        ]
    ],

    'onlineBankPaymentApprove' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/onlineBankPaymentApprove[/:id]',
            'defaults'=>[
                'controller' => Controller\OnlineBankPaymentController::class,
                'action' => 'approve'
            ]
        ]
    ],

    'applyLoan' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/applyLoan[/:id]',
            'defaults'=>[
                'controller' => Controller\ApplyLoanController::class,
            ]
        ]
    ],

    'getLoanGrades' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getLoanGrades',
            'defaults'=>[
                'controller' => Controller\ApplyLoanController::class,
                'action' => 'getLoanGrades'
            ]
        ]
    ],

    'getLoanApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getLoanApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\ApplyLoanController::class,
                'action' => 'getLoanApprovedList'
            ]
        ]
    ],

    'loanApplicationApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/loanApplicationApproval',
            'defaults'=>[
                'controller' => Controller\ApplyLoanController::class,
                'action' => 'loanApplicationApproval'
            ]
        ]
    ],

    'license' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/license[/:id]',
            'defaults'=>[
                'controller' => Controller\LicenseController::class,
            ]
        ]
    ],


    'orderType' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/orderType[/:id]',
            'defaults'=>[
                'controller' => Controller\OrderTypeController::class,
            ]
        ]
    ],
    
    'getSponsorInvoice' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSponsorInvoice[/:id]',
            'defaults'=>[
                'controller' => Controller\SponsorBillController::class,
                'action' => 'getSponsorInvoice'
            ]
        ]
    ],
    
    'smartLoan' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/smartLoan[/:id]',
            'defaults'=>[
                'controller' => Controller\SmartLoanController::class,
            ]
        ]
    ],

    'VehicleType' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/VehicleType[/:id]',
            'defaults'=>[
                'controller' => Controller\VehicleTypeController::class,

            ]
        ]
    ],

    'VehicleLoan' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/VehicleLoan[/:id]',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,

            ]
        ]
    ],

    'getAllStaff' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAllStaff',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'getAllStaff'
            ]
        ]
    ],

    'getLoanGurantors' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getLoanGurantors',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'getLoanGurantors'        
            ]
        ]
    ],

    'getEmployeeLoans' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getEmployeeLoans',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'getEmployeeLoans'        
            ]
        ]
    ],
    'staffLoans' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/staffLoans[/:id]',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'getStaffLoans'        
            ]
        ]
    ],

    'approveVehicleLoan' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approveVehicleLoan[/:id]',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'approveVehicleLoan'

            ]
        ]
    ],

    'approveLoanGurantor' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approveLoanGurantor',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'approveLoanGurantor'

            ]
        ]
    ],

    'approvedVehicleLoans' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approvedVehicleLoans',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'approvedVehicleLoans'

            ]
        ]
    ],

    'processLoan' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/processLoan',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'processLoan'

            ]
        ]
    ],

    'getLoanApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getLoanApprovalList[/:level][/:status]',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'getLoanApprovalList',
            ]
        ]

    ],

    'sponsorCN' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sponsorCN[/:id]',
            'defaults'=>[
                'controller' => Controller\SponsorCnController::class,

            ]
        ]
    ],

    'sponsorDN' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/sponsorDN[/:id]',
            'defaults'=>[
                'controller' => Controller\SponsorDnController::class,

            ]
        ]
    ],

    'getSponsorCnApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSponsorCnApprovalList[/:status]',
            'defaults'=>[
                'controller' => Controller\SponsorCnController::class,
                'action' => 'approvedsponsorCn'
            ]
        ]
    ],

    'SponsorCnApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/SponsorCnApproval',
            'defaults'=>[
                'controller' => Controller\SponsorCnController::class,
                'action' => 'approve'
            ]
        ]
    ],

    'getSponsorDnApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSponsorDnApprovalList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\SponsorDnController::class,
                'action' => 'approvedsponsorDns'
            ]
        ]
    ],

    'SponsorDnApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/SponsorDnApproval',
            'defaults'=>[
                'controller' => Controller\SponsorDnController::class,
                'action' => 'approve'
            ]
        ]
    ],
    'advancePayment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/advancePayment[/:id]',
            'defaults'=>[
                'controller' => Controller\AdvancePaymentController::class,
            ]
        ]
    ],
    'advancePaymentInvoiceUpdate' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/advancePaymentInvoiceUpdate',
            'defaults'=>[
                'controller' => Controller\AdvancePaymentController::class,
                'action' => 'advancePaymentInvoiceUpdate'
            ]
        ]
    ],

    'getVendorApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getVendorApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'getVendorApprovedList'
            ]
        ]
    ],

    'vendorApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/vendorApproval',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'vendorApproval'
            ]
        ]
    ],

    'cashApplication' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashApplication[/:id]',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,

            ]
        ]
    ],

    'getCashApplicationApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCashApplicationApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'getCashApplicationApprovedList'
            ]
        ]
    ],

    'cashApplicationApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashApplicationApproval',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'cashApplicationApproval'
            ]
        ]
    ],

    'cashApplicationProcess' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashApplicationProcess',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'cashApplicationProcess'
            ]
        ]
    ],

    'getCashApplicationStaffList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCashApplicationStaffList[/:id]',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'getCashApplicationStaffList'
            ]
        ]
    ],

    'knockOff' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/knockOff',
            'defaults'=>[
                'controller' => Controller\AdvancePaymentController::class,
                'action' => 'knockOff'
            ]
        ]
    ],

    'loanTypeSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/loanTypeSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\LoanTypeSetupController::class,
            ]
        ]
    ],


    'knockOnlinePayments' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/knockOnlinePayments',
            'defaults'=>[
                'controller' => Controller\OnlineBankPaymentController::class,
                'action' => 'knockOnlinePayments'
            ]
        ]
    ],

    'getListByLoanName' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListByLoanName[/:id]',
            'defaults'=>[
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'getListByLoanName'
            ]
        ]
    ],

    'loanCancelApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/loanCancelApproval',
            'defaults'=>[
                'controller' => Controller\LoanCancelRequestController::class,
                'action' => 'loanCancelApproval'
            ]
        ]
    ],

    'getListByApprovedStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListByApprovedStatus[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\LoanCancelRequestController::class,
                'action' => 'getListByApprovedStatus'
            ]
        ]
    ],

    'vehicleLoanList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/vehicleLoanList[/:id]',
            'defaults'=>[
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'vehicleLoanList'
            ]
        ]
    ],

    'getGroupList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getGroupList',
            'defaults'=>[
                'controller' => Controller\StaffController::class,
                'action' => 'getGroupList'
            ]
        ]
    ],
    'loanStaff' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/loanStaff',
            'defaults'=>[
                'controller' => Controller\StaffController::class,
                'action' => 'loanStaff'
            ]
        ]
    ],

    'nonBlackList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/nonBlackList',
            'defaults'=>[
                'controller' => Controller\StaffController::class,
                'action' => 'nonBlackList'
            ]
        ]
    ],


    'nonBlackList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/nonBlackList',
            'defaults'=>[
                'controller' => Controller\StaffController::class,
                'action' => 'nonBlackList'
            ]
        ]
    ],

    'generateSupplierInvoice' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/generateSupplierInvoice',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'generateSupplierInvoice'
            ]
        ]
    ],

    'getVendorActiveList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getVendorActiveList',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'getVendorActiveList'
            ]
        ]
    ],

    'tenderRegistration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/tenderRegistration[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderRegistrationController::class,
            ]
        ]
    ],

    'getTendorDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getTendorDetails[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'getTendorDetails'
            ]
        ]
    ],

    'getNewTenders' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getNewTenders',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'getNewTenders'
            ]
        ]
    ],

    'businessNature' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/businessNature[/:refcode]',
            'defaults' => [
                'controller' => Controller\BusinessNatureController::class,
            ]
        ]
    ],

    'newBusinessNature' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/newBusinessNature',
            'defaults' => [
                'controller' => Controller\BusinessNatureController::class,
                'action'=>'newBusinessNature',
            ]
        ]
    ],

    'businessNatureLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/businessNatureLevelList[/:level]',
            'defaults'=>[
                'controller' => Controller\BusinessNatureController::class,
                'action'=>'businessNatureLevelList',
            ]
        ]

    ],
    'businessNatureParentLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/businessNatureParentLevelList[/:parent][/:level]',
            'defaults'=>[
                'controller' => Controller\BusinessNatureController::class,
                'action'=>'businessNatureParentLevelList',
            ]
        ]

    ],
    'shortlistVendors' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/shortlistVendors',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action'=>'shortlistVendors',
            ]
        ]

    ],
    'awardVendors' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/awardVendors',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action'=>'awardVendors',
            ]
        ]

    ],
    'shortlistedVendorsByTender' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/shortlistedVendorsByTender[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action'=>'shortlistedVendorsByTender',
            ]
        ]

    ],
    'awardedVendorsByTender' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/awardedVendorsByTender[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action'=>'awardedVendorsByTender',
            ]
        ]

    ],
    'assetInformation' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetInformation[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
            ]
        ]

    ],

    'getListByOrderType' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListByOrderType[/:orderType]',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action'=>'getListByOrderType'
            ]
        ]

    ],
    'finalAwardTender' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/finalAwardTender',
            'defaults'=>[
                'controller' => Controller\TenderRegistrationController::class,
                'action'=>'finalAwardTender'
            ]
        ]

    ],
    'tenderSubmission' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/tenderSubmission[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
            ]
        ]

    ],
    'countryStates' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/countryStates[/:id]',
            'defaults'=>[
                'controller' => Controller\CountryController::class,
                'action' => 'countryStates'
            ]
        ]

    ],

    'getListBasedOnTendor' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListBasedOnTendor[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'getListBasedOnTendor'
            ]
        ]

    ],

    'getTendorShortlisted' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getTendorShortlisted[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'getTendorShortlisted'
            ]
        ]

    ],

    'getTendorAward' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getTendorAward[/:id]',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'getTendorAward'
            ]
        ]

    ],

    'assetItem' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetItem[/:id]',
            'defaults'=>[
                'controller' => Controller\ItemController::class,
            ]
        ]
    ],
    'assetAmountIncrease' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetAmountIncrease',
            'defaults'=>[
                'controller' => Controller\ItemController::class,
                'action' => 'assetAmountIncrease'
            ]
        ]
    ],
    'assetLifeIncrease' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetLifeIncrease',
            'defaults'=>[
                'controller' => Controller\ItemController::class,
                'action' => 'assetLifeIncrease'
            ]
        ]
    ],


    'UpdateAsset' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/UpdateAsset[/:id]',
            'defaults'=>[
                'controller' => Controller\UpdateAssetController::class,
            ]
        ]

    ],

    'assetPreRegistration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetPreRegistration[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetPreRegistrationController::class,
            ]
        ]

    ],

    'updateAssetApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/UpdateAssetApproval',
            'defaults'=>[
                'controller' => Controller\UpdateAssetController::class,
                'action' => 'UpdateAssetApproval',

            ]
        ]
    ],

    'disposalRequisition' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/disposalRequisition[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalRequisitionController::class,
            ]
        ]
    ],
    'allDisposalDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/allDisposalDetails',
            'defaults'=>[
                'controller' => Controller\DisposalRequisitionController::class,
                'action' => 'allDisposalDetails'
            ]
        ]
    ],
    'disposalVerification' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/disposalVerification[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
            ]
        ]
    ],
    'allVerifiedDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/allVerifiedDetails[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action' => 'allVerifiedDetails'
            ]
        ]
    ]
    ,
    'disposalGetByStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/disposalGetByStatus[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action' => 'disposalGetByStatus'
            ]
        ]
    ],

    'AssetLocation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/AssetLocation[/:refcode]',
            'defaults' => [
                'controller' => Controller\AssetLocationController::class,
            ]
        ]
    ],

    'newAssetCode' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/newAssetCode',
            'defaults' => [
                'controller' => Controller\AssetLocationController::class,
                'action'=>'newAssetCode',
            ]
        ]
    ],

    'assetLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetLevelList[/:level]',
            'defaults'=>[
                'controller' => Controller\AssetLocationController::class,
                'action'=>'assetLevelList',
            ]
        ]

    ],
    'assetParentLevelList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetParentLevelList[/:parent][/:level]',
            'defaults'=>[
                'controller' => Controller\AssetLocationController::class,
                'action'=>'assetParentLevelList',
            ]
        ]

    ],
    'endorseDisposal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/endorseDisposal',
            'defaults'=>[
                'controller' => Controller\DisposalRequisitionController::class,
                'action'=>'endorseDisposal',
            ]
        ]

    ],

    'setupDisposal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/setupDisposal[/:id]',
            'defaults'=>[
                'controller' => Controller\SetupDisposalController::class,
            ]
        ]
    ],

    'verifyDisposal' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/verifyDisposal',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action'=>'verifyDisposal',
            ]
        ]

    ],

    'disposalApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/disposalApproval',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action'=>'disposalApproval',
            ]
        ]

    ],

    'assetMovementApproval2' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetMovementApproval2',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
                'action' => 'assetMovementApproval2',
            ]
        ]
    ],

    'getListByApproved2Status' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getListByApproved2Status[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
                'action' => 'getListByApproved2Status',
            ]
        ]
    ],

    'approveAssetUpdate' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approveAssetUpdate',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
                'action' => 'approveAssetUpdate',
            ]
        ]
    ],

    'grnAsset' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/grnAsset',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
                'action' => 'grnAsset',
            ]
        ]
    ],

    'assetInformationDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetInformationDetails[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
                'action' => 'assetInformationDetails',
            ]
        ]
    ],

    'assetInformationDetailsById' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetInformationDetailsById[/:id]',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
                'action' => 'assetInformationDetailsById',
            ]
        ]
    ],
    'storeSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/storeSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\StoreSetupController::class,
            ]
        ]
    ],
    'preOrderLevelSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/preOrderLevelSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\PreOrderLevelSetupController::class,
            ]
        ]
    ],

    'applyStock' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/applyStock[/:id]',
            'defaults'=>[
                'controller' => Controller\ApplyStockController::class,
            ]
        ]
    ],

    'ApplyStockApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/ApplyStockApproval',
            'defaults'=>[
                'controller' => Controller\ApplyStockController::class,
                'action' => 'ApplyStockApproval',
            ]
        ]
    ],

    'getApplyStockApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getApplyStockApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\ApplyStockController::class,
                'action' => 'getApplyStockApprovedList',
            ]
        ]
    ],

    'getCashApplicationNonProcessedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCashApplicationNonProcessedList',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'getCashApplicationNonProcessedList',
            ]
        ]
    ],

    'getCashApplicationProcessedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getCashApplicationProcessedList',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'getCashApplicationProcessedList',
            ]
        ]
    ],

    'studentIntakeBatch' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentIntakeBatch',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'studentIntakeBatch',
            ]
        ]
    ],

    'cashApplicationReject' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/cashApplicationReject',
            'defaults' => [
                'controller' => Controller\CashApplicationController::class,
                'action' => 'cashApplicationReject',
            ]
        ]
    ],

    'taggingStudentSponsor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/taggingStudentSponsor',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'taggingStudentSponsor',
            ]
        ]
    ],

    'monthlyDeduction' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/monthlyDeduction[/:id]',
            'defaults' => [
                'controller' => Controller\MonthlyDeductionController::class,
            ]
        ]
    ],

    'getStaffCashAdvances' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getStaffCashAdvances',
            'defaults'=>[
                'controller' => Controller\CashApplicationController::class,
                'action' => 'getStaffCashAdvances'

            ]
        ]
    ],

    'payrollContribution' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/payrollContribution[/:id]',
            'defaults'=>[
                'controller' => Controller\PayrollContributionController::class,

            ]
        ]
    ],
    'payrollDeduction' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/payrollDeduction[/:id]',
            'defaults'=>[
                'controller' => Controller\PayrollDeductionController::class,
            ]
        ]
    ],
    'payrollSchedule' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/payrollSchedule[/:id]',
            'defaults'=>[
                'controller' => Controller\PayrollScheduleController::class,

            ]
        ]
    ],

    'payrollExcemption' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/payrollExcemption[/:id]',
            'defaults'=>[
                'controller' => Controller\PayrollExcemptionController::class,

            ]
        ]
    ],

    'cashDeclaration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashDeclaration[/:id]',
            'defaults'=>[
                'controller' => Controller\CashDeclarationController::class,

            ]
        ]
    ],

    'approvedCashDeclarations' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approvedCashDeclarations[/:id]',
            'defaults'=>[
                'controller' => Controller\CashDeclarationController::class,
                'action' => 'approvedCashDeclarations'
            ]
        ]
    ],

    'approveCashDeclaration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/approveCashDeclaration',
            'defaults'=>[
                'controller' => Controller\CashDeclarationController::class,
                'action' => 'approveCashDeclaration'
            ]
        ]
    ],

    'taxDeduction' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/taxDeduction[/:id]',
            'defaults'=>[
                'controller' => Controller\TaxDeductionController::class,

            ]
        ]
    ],

    'salary' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/salary[/:id]',
            'defaults'=>[
                'controller' => Controller\SalaryCalculationController::class,
            ]
        ]
    ],

    'staffContribution' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/staffContribution[/:id]',
            'defaults'=>[
                'controller' => Controller\StaffContributionController::class,
            ]
        ]
    ],

    'GuarantorList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/GuarantorList',
            'defaults'=>[
                'controller' => Controller\VehicleLoanController::class,
                'action'=>'GuarantorList'

            ]
        ]
    ],

    'billRegistration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/billRegistration[/:id]',
            'defaults'=>[
                'controller' => Controller\BillRegistrationController::class,
                

            ]
        ]
    ],
    'billRegistrationApproval' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/billRegistrationApproval',
            'defaults'=>[
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'billRegistrationApproval'
                

            ]
        ]
    ],
    
    'getBillRegistrationApprovedList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getBillRegistrationApprovedList[/:approvedStatus]',
            'defaults'=>[
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'getBillRegistrationApprovedList'
                

            ]
        ]
    ],

    'getBillRegistrationPendingList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getBillRegistrationPendingList',
            'defaults'=>[
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'getBillRegistrationPendingList'
                

            ]
        ]
    ],

    'getBillRegistrationTypeList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getBillRegistrationTypeList',
            'defaults'=>[
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'getBillRegistrationTypeList'
                

            ]
        ]
    ],

    'insuranceCompany' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/insuranceCompany[/:id]',
            'defaults'=>[
                'controller' => Controller\InsuranceCompanyController::class,
                

            ]
        ]
    ],

    'getInsuranceValue' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getInsuranceValue',
            'defaults'=>[
                'controller' => Controller\InsuranceCompanyController::class,
                'action' => 'getInsuranceValue'
                

            ]
        ]
    ],
    'view' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/view[/:name]',
            'defaults' => [
                'controller' => Controller\ViewsController::class,
                'action' => 'view'
            ]
        ]
    ],
    'staffTagging' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/staffTagging[/:id]',
            'defaults' => [
                'controller' => Controller\StaffTaggingController::class,
            ]
        ]
    ],
    'kwap' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/kwap[/:id]',
            'defaults' => [
                'controller' => Controller\KwapController::class,
            ]
        ]
    ],
    'epfCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/epfCategory[/:id]',
            'defaults' => [
                'controller' => Controller\EpfCategoryController::class,
            ]
        ]
    ],
    'processSalary' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/processSalary',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'processSalary'
            ]
        ]
    ],

    'socsoCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/socsoCategory[/:id]',
            'defaults' => [
                'controller' => Controller\SocsoCategorycontroller::class,
            ]
        ]
    ],

    'processType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/processType[/:id]',
            'defaults' => [
                'controller' => Controller\ProcessTypeController::class,
            ]
        ]
    ],
    'epfCalculation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/epfCalculation',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'epfCalculation'
            ]
        ]
    ],
    'staffListBasedOnDepartment' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/staffListBasedOnDepartment[/:id]',
            'defaults' => [
                'controller' => Controller\StaffController::class,
                'action' => 'staffListBasedOnDepartment'

            ]
        ]
    ],
    
    'kwapCalculation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/kwapCalculation',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'kwapCalculation'
            ]
        ]
    ],
    
    'getEmployeePayslip' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getEmployeePayslip',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'getEmployeePayslip'
            ]
        ]
    ],
    
    'moveSalary' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/moveSalary',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'moveSalary'
            ]
        ]
    ],
    
    'taxEmployeeDeduction' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/taxEmployeeDeduction',
            'defaults' => [
                'controller' => Controller\TaxEmployeeDeductionController::class,
            ]
        ]
    ],
    
    'userRoles' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/userRoles',
            'defaults' => [
                'controller' => Controller\RoleController::class,
                'action' =>'userRoles'
            ]
        ]
    ],

    'getActivityDetailsAmount' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getActivityDetailsAmount',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class,
                'action' =>'getActivityDetailsAmount'
            ]
        ]
    ],

    'getNonAllocatedCostcenter' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getNonAllocatedCostcenter[/:idFinancialyear]',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class,
                'action' =>'getNonAllocatedCostcenter'
            ]
        ]
    ],

    'detailApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/detailApproval',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' =>'detailApproval'
            ]
        ]
    ],

    'getAllInvestmentType' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAllInvestmentType[/:institution]',
            'defaults'=>[
                'controller' => Controller\InvestmentInstitutionController::class,
                'action' => 'getAllInvestmentType',
            ]
        ]
    ],

    'getInvestmentApplicationApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getInvestmentApplicationApprovalList[/:level][/:status]',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action'=>'getInvestmentApplicationApprovalList',
            ]
        ]

    ],

    'getZeroDepartments' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getZeroDepartments',
            'defaults'=>[
                'controller' => Controller\MasterController::class,
                'action'=>'getZeroDepartments',
            ]
        ]

    ],

    'getByUnitCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getByUnitCode[/:code]',
            'defaults'=>[
                'controller' => Controller\MasterController::class,
                'action'=>'getByUnitCode',
            ]
        ]

    ],

    'getAllGl' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAllGl[/:idFinancialyear]',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action'=>'getAllGl',
            ]
        ]

    ],

    'budgetFileImport' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/budgetFileImport',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action'=>'budgetFileImport',
            ]
        ]

    ],

    'getActivityAccountCodes' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActivityAccountCodes',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action'=>'getActivityAccountCodes',
            ]
        ]

    ],

    'getNewStaff' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getNewStaff',
            'defaults'=>[
                'controller' => Controller\StaffController::class,
                'action'=>'getNewStaff',
            ]
        ]

    ],

    'getInvestmentAccountCodes' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getInvestmentAccountCodes',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action'=>'getInvestmentAccountCodes',
            ]
        ]

    ],


    'registrationApprovalStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/registrationApprovalStatus',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action'=>'registrationApprovalStatus',
            ]
        ]

    ],

    'getInvestmentRegistrationApprovalList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getInvestmentRegistrationApprovalList[/:level][/:status]',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action'=>'getInvestmentRegistrationApprovalList',
            ]
        ]
    ],

    'getAllPurchaseGlCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAllPurchaseGlCode[/:idFinancialyear]',
            'defaults'=>[
                'controller' => Controller\PurchaseRequisitionController::class,
                'action'=>'getAllPurchaseGlCode',

            ]
        ]

    ],

    'updateWithdrawlStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/updateWithdrawlStatus',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action'=>'updateWithdrawlStatus',

            ]
        ]

    ],
    'blockVendor' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/blockVendor[/:id]',
            'defaults'=>[
                'controller' => Controller\BlockVendorController::class,
            ]
        ]

    ],

    'getRegistrationWithdrawlList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getRegistrationWithdrawlList[/:status]',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action'=>'getRegistrationWithdrawlList',

            ]
        ]

    ],
    'purchaseOrderNotIn' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/purchaseOrderNotIn[/:id]',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action'=>'purchaseOrderNotIn',
            ]
        ]
    ],

    'purchaseOrderWarrantNotIn' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/purchaseOrderWarrantNotIn',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action'=>'purchaseOrderWarrantNotIn',
            ]
        ]
    ],

    'mofCategory' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/mofCategory[/:id]',
            'defaults'=>[
                'controller' => Controller\MofCategoryController::class,
            ]
        ]

    ],
    'insuranceSetups' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/insuranceSetups[/:id]',
            'defaults'=>[
                'controller' => Controller\InsuranceSetupsController::class,
            ]
        ]
    ],

    'getWithdrawList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getWithdrawList',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'getWithdrawList',
            ]
        ]
    ],

    'getRegistrationList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getRegistrationList',
            'defaults'=>[
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'getRegistrationList',
            ]
        ]
    ],

    'cashAdvance' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashAdvance[/:id]',
            'defaults'=>[
                'controller' => Controller\CashAdvanceController::class,
            ]
        ]
    ],

    'cashAdvanceRate' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/cashAdvanceRate[/:id]',
            'defaults'=>[
                'controller' => Controller\CashAdvanceRateController::class,
            ]
        ]
    ],

    'returns' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/returns[/:id]',
            'defaults'=>[
                'controller' => Controller\ReturnsController::class,
            ]
        ]
    ],

    'getReturnsApprovedList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getReturnsApprovedList[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\ReturnsController::class,
                'action' => 'getReturnsApprovedList'

            ]
        ]
    ],

    'getPurchaseOrderWarrantiesApprovedList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getPurchaseOrderWarrantiesApprovedList[/:approvedStatus]',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'getPurchaseOrderWarrantiesApprovedList'

            ]
        ]
    ],

    'GrnDetailApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/GrnDetailApproval',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'GrnDetailApproval'

            ]
        ]
    ],
    'getItemSubCategoriesByCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getItemSubCategoriesByCategory[/:id]',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'getItemSubCategoriesByCategory'

            ]
        ]
    ],
    'getUtilityRate' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getUtilityRate[/:unit]',
            'defaults' => [
                'controller' => Controller\UtilityController::class,
                'action' => 'getUtilityRate'

            ]
        ]
    ],

    'getReinvestment' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getReinvestment',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'getReinvestment'

            ]
        ]
    ],
    'getCreditNoteApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getCreditNoteApprovals[/:status]',
            'defaults' => [
                'controller' => Controller\CreditNoteController::class,
                'action' => 'getCreditNoteApprovals'

            ]
        ]
    ],
    'getDebitNoteApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getDebitNoteApprovals[/:status]',
            'defaults' => [
                'controller' => Controller\DebitNoteController::class,
                'action' => 'getDebitNoteApprovals'

            ]
        ]
    ],

    'getApplicationList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getApplicationList',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'getApplicationList'
            ]
        ]
    ],

    'getSponsorCNApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getSponsorCNApprovals',
            'defaults' => [
                'controller' => Controller\SponsorCnController::class,
                'action' => 'getSponsorCNApprovals'

            ]
        ]
    ],
    'getSponsorDNApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getSponsorDNApprovals[/:status]',
            'defaults' => [
                'controller' => Controller\SponsorDnController::class,
                'action' => 'getSponsorDNApprovals'


            ]
        ]
    ],
    'getTables' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getTables',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'getTables'


            ]
        ]
    ],
    'getColumns' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getColumns[/:table]',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'getColumns'


            ]
        ]
    ],


    'getBalanceStatus' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getBalanceStatus',
            'defaults'=>[
                'controller' => Controller\BudgetSummaryController::class,
                'action' => 'getBalanceStatus'
            ]
        ]
    ],

    'getLoanStaff' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getLoanStaff',
            'defaults' => [
                'controller' => Controller\StaffController::class,
                'action' => 'getLoanStaff'
            ]
        ]
    ],

    'getSubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getSubCategory[/:category]',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'getSubCategory'

            ]
        ]
    ],

    'getItemBySubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getItemBySubCategory[/:subCategory]',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'getItemBySubCategory'

            ]
        ]
    ],

    'staffCashAdvanceLimit' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/staffCashAdvanceLimit[/:id]',
            'defaults' => [
                'controller' => Controller\StaffCashAdvanceLimitController::class,

            ]
        ]
    ],

    'nonApprovedReceipts' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/nonApprovedReceipts',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'nonApprovedReceipts'
            ]
        ]
    ],

    'approveReceipt' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveReceipt',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'approveReceipt'
            ]
        ]
    ],

    'getLoanPendingStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getLoanPendingStatus[/:id]',
            'defaults' => [
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'getLoanPendingStatus'
            ]
        ]
    ],

    'getFeeItemByCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getFeeItemByCategory[/:id]',
            'defaults' => [
                'controller' => Controller\FeeItemController::class,
                'action' => 'getFeeItemByCategory'
            ]
        ]
    ],

    'feeType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/feeType[/:id]',
            'defaults' => [
                'controller' => Controller\FeeTypeController::class,
            ]
        ]
    ],

    'approveReturns' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveReturns',
            'defaults' => [
                'controller' => Controller\ReturnsController::class,
                'action' => 'approveReturns'

            ]
        ]
    ],

    'paidInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/paidInvoice',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'paidInvoice'

            ]
        ]
    ],

    'customerMasterInvoices1' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/customerMasterInvoices1',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'customerMasterInvoices1'

            ]
        ]
    ],

    'getCashBankDetail' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getCashBankDetail',
            'defaults' => [
                'controller' => Controller\CashBankController::class,
                'action' => 'getCashBankDetail'

            ]
        ]
    ],

    'grnDonation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/grnDonation[/:id]',
            'defaults' => [
                'controller' => Controller\GrnDonationController::class,

            ]
        ]
    ],

    'pattyCashAllocation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/pattyCashAllocation[/:id]',
            'defaults' => [
                'controller' => Controller\PattyCashAllocationController::class,
            ]
        ]
    ],

    'pattyCashEntry' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/pattyCashEntry[/:id]',
            'defaults' => [
                'controller' => Controller\PattyCashEntryController::class,
            ]
        ]
    ],

    'getApprovedPattyCashEntry' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getApprovedPattyCashEntry[/:status]',
            'defaults' => [
                'controller' => Controller\PattyCashEntryController::class,
                'action' => 'getApprovedPattyCashEntry'
            ]
        ]
    ],

    'approvePettyCashEntry' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvePettyCashEntry',
            'defaults' => [
                'controller' => Controller\PattyCashEntryController::class,
                'action' => 'approvePettyCashEntry'
            ]
        ]
    ],

    'cashBankApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/cashBankApproval',
            'defaults' => [
                'controller' => Controller\CashBankController::class,
                'action' => 'cashBankApproval'
            ]
        ]
    ],

    'getPendingPettyCashAllocation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getPendingPettyCashAllocation',
            'defaults' => [
                'controller' => Controller\PattyCashAllocationController::class,
                'action' => 'getPendingPettyCashAllocation'
            ]
        ]
    ],

    'approvePettyCashAllocation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvePettyCashAllocation',
            'defaults' => [
                'controller' => Controller\PattyCashAllocationController::class,
                'action' => 'approvePettyCashAllocation'
            ]
        ]
    ],

    'getManualCashBankDetail' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getManualCashBankDetail',
            'defaults' => [
                'controller' => Controller\CashBankController::class,
                'action' => 'getManualCashBankDetail'

            ]
        ]
    ],

    'stockRegistration' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/stockRegistration[/:id]',
            'defaults' => [
                'controller' => Controller\StockRegistrationController::class,
            ]
        ]
    ],

    'stockDistribution' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/stockDistribution[/:id]',
            'defaults'=>[
                'controller' => Controller\ApplyStockController::class,
                'action' => 'stockDistribution'
            ]
        ]
    ],

    'studentInvoiceDetail' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/studentInvoiceDetail[/:id]',
            'defaults'=>[
                'controller' => Controller\StudentController::class,
                'action' => 'studentInvoiceDetail'
            ]
        ]
    ],

    'getActiveStore' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActiveStore[/:id]',
            'defaults'=>[
                'controller' => Controller\StoreSetupController::class,
                'action' => 'getActiveStore'
            ]
        ]
    ],

    'grnNotIn' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/grnNotIn',
            'defaults'=>[
                'controller' => Controller\GrnController::class,
                'action' => 'grnNotIn'
            ]
        ]
    ],

    'getEndorsmentList' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getEndorsmentList',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action' => 'getEndorsmentList'
            ]
        ]
    ],

    'assetNotInMovement' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetNotInMovement',
            'defaults'=>[
                'controller' => Controller\AssetMovementController::class,
                'action' => 'assetNotInMovement'
            ]
        ]
    ],

    'assetNotInMaintenance' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetNotInMaintenance',
            'defaults'=>[
                'controller' => Controller\AssetMaintenanceController::class,
                'action' => 'assetNotInMaintenance'
            ]
        ]
    ],

    'deleteGrnDonationDetail' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteGrnDonationDetail[/:id]',
            'defaults'=>[
                'controller' => Controller\GrnDonationController::class,
                'action' => 'deleteGrnDonationDetail'
            ]
        ]
    ],

    'deleteDisposalVerDetail' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteDisposalVerDetail[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalVerificationController::class,
                'action' => 'deleteDisposalVerDetail'
            ]
        ]
    ],

    'deleteDisposalReqDetail' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteDisposalReqDetail[/:id]',
            'defaults'=>[
                'controller' => Controller\DisposalRequisitionController::class,
                'action' => 'deleteDisposalReqDetail'
            ]
        ]
    ],

    'assetNotInDisposalRequistion' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/assetNotInDisposalRequistion',
            'defaults'=>[
                'controller' => Controller\DisposalRequisitionController::class,
                'action' => 'assetNotInDisposalRequistion'
            ]
        ]
    ],

    'PONotInGrn' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/PONotInGrn',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'PONotInGrn'
            ]
        ]
    ],

    'getPurchaseOrderWarrant' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getPurchaseOrderWarrant',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'getPurchaseOrderWarrant'
            ]
        ]
    ],

    'getAssetInformation' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAssetInformation',
            'defaults'=>[
                'controller' => Controller\AssetInformationController::class,
                'action' => 'getAssetInformation'
            ]
        ]
    ],

    'deleteRequestInvoiceDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteRequestInvoiceDetails',
            'defaults'=>[
                'controller' => Controller\RequestInvoiceController::class,
                'action' => 'deleteRequestInvoiceDetails'
            ]
        ]
    ],

    'deleteMasterInvoiceDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteMasterInvoiceDetails',
            'defaults'=>[
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'deleteMasterInvoiceDetails'
            ]
        ]
    ],

    'deletePurchaseOrderDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deletePurchaseOrderDetails',
            'defaults'=>[
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'deletePurchaseOrderDetails'
            ]
        ]
    ],

    'deletePurchaseRequistionDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deletePurchaseRequistionDetails',
            'defaults'=>[
                'controller' => Controller\PurchaseRequisitionController::class,
                'action' => 'deletePurchaseRequistionDetails'
            ]
        ]
    ],

    'deleteGRNDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteGRNDetails',
            'defaults'=>[
                'controller' => Controller\GrnController::class,
                'action' => 'deleteGRNDetails'
            ]
        ]
    ],

    'deleteTenderSpec' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteTenderSpec',
            'defaults'=>[
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'deleteTenderSpec'
            ]
        ]
    ],

    'deleteTenderComitee' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteTenderComitee',
            'defaults'=>[
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'deleteTenderComitee'
            ]
        ]
    ],

    'deleteTenderRemarks' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteTenderRemarks',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'deleteTenderRemarks'
            ]
        ]
    ],

    'deleteVendorRegistration' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteVendorRegistration',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deleteVendorRegistration'
            ]
        ]
    ],

    'deleteTenderSubmission' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteTenderSubmission',
            'defaults'=>[
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'deleteTenderSubmission'
            ]
        ]
    ],

    'deleteApplyStockDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteApplyStockDetails',
            'defaults'=>[
                'controller' => Controller\ApplyStockController::class,
                'action' => 'deleteApplyStockDetails'
            ]
        ]
    ],

    'deleteLicenseDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteLicenseDetails',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deleteLicenseDetails'
            ]
        ]
    ],

    'deleteContactDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteContactDetails',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deleteContactDetails'
            ]
        ]
    ],

    'deleteNatureOfBusiness' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteNatureOfBusiness',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deleteNatureOfBusiness'
            ]
        ]
    ],

    'deletevendorMof' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deletevendorMof',
            'defaults'=>[
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deletevendorMof'
            ]
        ]
    ],

    'deleteAssetLocationData' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteAssetLocationData',
            'defaults'=>[
                'controller' => Controller\AssetLocationController::class,
                'action' => 'deleteAssetLocationData'
            ]
        ]
    ],

    'studentDetail' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/studentDetail[/:id]',
            'defaults'=>[
                'controller' => Controller\StudentDetailsController::class,
            ]
        ]
    ],

    'getFeeCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getFeeCategory[/:status]',
            'defaults' => [
                'controller' => Controller\FeeCategoryController::class,
                'action' => 'getFeeCategory'
            ]
        ]
    ],

    'deleteBudgetActivityDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteBudgetActivityDetails',
            'defaults'=>[
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'deleteBudgetActivityDetails'
            ]
        ]
    ],

    'deleteBudgetIncrementDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteBudgetIncrementDetails',
            'defaults'=>[
                'controller' => Controller\BudgetIncrementController::class,
                'action' => 'deleteBudgetIncrementDetails'
            ]
        ]
    ],

    'deleteBudgetVerimentDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteBudgetVerimentDetails',
            'defaults'=>[
                'controller' => Controller\BudgetVerimentController::class,
                'action' => 'deleteBudgetVerimentDetails'
            ]
        ]
    ],

    'deleteInvestmentApplicationDetails' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteInvestmentApplicationDetails',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'deleteInvestmentApplicationDetails'
            ]
        ]
    ],

    'tenderAggrement' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/tenderAggrement[/:id]',
            'defaults' => [
                'controller' => Controller\TenderAggrementController::class,

            ]
        ]
    ],


    'getTenderSubmission' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getTenderSubmission[/:status]',
            'defaults' => [
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'getTenderSubmission'
            ]
        ]
    ],

    'getTenderShortlisted' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getTenderShortlisted[/:status]',
            'defaults' => [
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'getTenderShortlisted'
            ]
        ]
    ],

    'getTenderAwarded' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getTenderAwarded[/:status]',
            'defaults' => [
                'controller' => Controller\TenderQuotationController::class,
                'action' => 'getTenderAwarded'
            ]
        ]
    ],

    'deleteInvestmentInstitution' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/deleteInvestmentInstitution',
            'defaults'=>[
                'controller' => Controller\InvestmentInstitutionController::class,
                'action' => 'deleteInvestmentInstitution'
            ]
        ]
    ],

    'getTypeOfInvestment' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getTypeOfInvestment[/:id]',
            'defaults'=>[
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'getTypeOfInvestment'
            ]
        ]
    ],

    'getActiveLicense' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getActiveLicense[/:id]',
            'defaults'=>[
                'controller' => Controller\LicenseController::class,
                'action' => 'getActiveLicense'
            ]
        ]
    ],

    'getAccountCodeByRevenueCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAccountCodeByRevenueCode[/:id]',
            'defaults'=>[
                'controller' => Controller\RevenueSetUpController::class,
                'action' => 'getAccountCodeByRevenueCode'
            ]
        ]
    ],
    'getRevenueByCategory' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getRevenueByCategory[/:id]',
            'defaults'=>[
                'controller' => Controller\RevenueSetUpController::class,
                'action' => 'getRevenueByCategory'
            ]
        ]
    ],

    'getAccountCodeByRevenueCode' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getAccountCodeByRevenueCode[/:id]',
            'defaults'=>[
                'controller' => Controller\RevenueSetUpController::class,
                'action' => 'getAccountCodeByRevenueCode'
            ]
        ]
    ],

    'purchaseOrderWarrantiesApproval' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrderWarrantiesApproval',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'purchaseOrderWarrantiesApproval'

            ]
        ]
    ],

    'employeeDelay' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/employeeDelay[/:id]',
            'defaults' => [
                'controller' => Controller\EmpolyeeDelayController::class,
            ]
        ]
    ],

    'deleteRecurringSetupDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteRecurringSetupDetails',
            'defaults' => [
                'controller' => Controller\RecurringSetUpController::class,
                'action' => 'deleteRecurringSetupDetails'
            ]
        ]
    ],

    'assetExample' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/assetExample[/:id]',
            'defaults' => [
                'controller' => Controller\AssetExampleController::class,
            ]
        ]
    ],

    'deleteCashApplicationDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteCashApplicationDetails',
            'defaults' => [
                'controller' => Controller\CashApplicationController::class,
                'action' => 'deleteCashApplicationDetails'
            ]
        ]
    ],

    'receipts' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/receipts[/:id]',
            'defaults'=>[
                'controller' => Controller\ReceiptController::class,
                'action' => 'receipts'
            ]
        ]
    ],
    'getIntakes' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getIntakes[/:type]',
            'defaults'=>[
                'controller' => Controller\IntakeController::class,
                'action' => 'getIntakes'
            ]
        ]
    ],
    'getProgrammes' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getProgrammes[/:type]',
            'defaults'=>[
                'controller' => Controller\ProgrammeController::class,
                'action' => 'getProgrammes'
            ]
        ]
    ],
    'getStudents' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getStudents[/:type]',
            'defaults'=>[
                'controller' => Controller\StudentController::class,
                'action' => 'getStudents'
            ]
        ]
    ],
    'getSemesters' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/getSemesters[/:type]',
            'defaults'=>[
                'controller' => Controller\StudentController::class,
                'action' => 'getSemesters'
            ]
        ]
    ],

    'deleteFeeItemDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteFeeItemDetails',
            'defaults' => [
                'controller' => Controller\FeeItemController::class,
                'action' => 'deleteFeeItemDetails'
            ]
        ]
    ],

    'deleteIntakeDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteIntakeDetails',
            'defaults' => [
                'controller' => Controller\IntakeController::class,
                'action' => 'deleteIntakeDetails'
            ]
        ]
    ],

    'deleteFeeStructureDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteFeeStructureDetails',
            'defaults' => [
                'controller' => Controller\FeeStructureController::class,
                'action' => 'deleteFeeStructureDetails'
            ]
        ]
    ],

    'deleteFeeStructureProgram' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteFeeStructureProgram',
            'defaults' => [
                'controller' => Controller\FeeStructureController::class,
                'action' => 'deleteFeeStructureProgram'
            ]
        ]
    ],

    'deleteProgramCourceFee' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteProgramCourceFee',
            'defaults' => [
                'controller' => Controller\FeeStructureController::class,
                'action' => 'deleteProgramCourceFee'
            ]
        ]
    ],

    'deleteDebtInformationDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteDebtInformationDetails',
            'defaults' => [
                'controller' => Controller\DebtInformationController::class,
                'action' => 'deleteDebtInformationDetails'
            ]
        ]
    ],

    'studentInvoicesDetail' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/v1/studentInvoicesDetail',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'studentInvoicesDetail'
            ]
        ]
    ],

    'deleteLegalInformationDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteLegalInformationDetails',
            'defaults' => [
                'controller' => Controller\LegalInformationController::class,
                'action' => 'deleteLegalInformationDetails'
            ]
        ]
    ],

    'deleteSponsorDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteSponsorDetails',
            'defaults' => [
                'controller' => Controller\SponsorController::class,
                'action' => 'deleteSponsorDetails'
            ]
        ]
    ],

    'getCreditDebitByFee' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getCreditDebitByFee[/:id]',
            'defaults' => [
                'controller' => Controller\FeeItemController::class,
                'action' => 'getCreditDebitByFee'
            ]
        ]
    ],

    'getGroupMenu' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getGroupMenu[/:id]',
            'defaults' => [
                'controller' => Controller\MenuController::class,
                'action' => 'getGroupMenu'
            ]
        ]
    ],

    'deleteSalaryDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteSalaryDetails',
            'defaults' => [
                'controller' => Controller\SalaryController::class,
                'action' => 'deleteSalaryDetails'
            ]
        ]
    ],

    'deleteMonthlyDeductionDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteMonthlyDeductionDetails',
            'defaults' => [
                'controller' => Controller\MonthlyDeductionController::class,
                'action' => 'deleteMonthlyDeductionDetails'
            ]
        ]
    ],

    'getStudentByStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getStudentByStatus[/:id]',
            'defaults' => [
                'controller' => Controller\StudentController::class,
                'action' => 'getStudentByStatus'
            ]
        ]
    ],

    'getProgrammeByStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getProgrammeByStatus[/:id]',
            'defaults' => [
                'controller' => Controller\ProgrammeController::class,
                'action' => 'getProgrammeByStatus'
            ]
        ]
    ],

    'getFeeItemByStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getFeeItemByStatus[/:id]',
            'defaults' => [
                'controller' => Controller\FeeItemController::class,
                'action' => 'getFeeItemByStatus'
            ]
        ]
    ],

    'getFeeTypeByStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getFeeTypeByStatus[/:id]',
            'defaults' => [
                'controller' => Controller\FeeTypeController::class,
                'action' => 'getFeeTypeByStatus'
            ]
        ]
    ],

    'deletePaytoProfileDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deletePaytoProfileDetails',
            'defaults' => [
                'controller' => Controller\VendorRegistrationController::class,
                'action' => 'deletePaytoProfileDetails'
            ]
        ]
    ],

    'getCategoryItem' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getCategoryItem',
            'defaults' => [
                'controller' => Controller\AssetMovementController::class,
                'action' => 'getCategoryItem'
            ]
        ]
    ],

    'deleteDepreciationSetup' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteDepreciationSetup',
            'defaults' => [
                'controller' => Controller\DepreciationSetupController::class,
                'action' => 'deleteDepreciationSetup'
            ]
        ]
    ],

    'depreciationSetup' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/depreciationSetup[/:id]',
            'defaults'=>[
                'controller' => Controller\DepreciationSetupController::class,
            ]
        ]
    ],

    'getAssetByAssetSubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getAssetByAssetSubCategory[/:id]',
            'defaults' => [
                'controller' => Controller\AssetExampleController::class,
                'action' => 'getAssetByAssetSubCategory'
            ]
        ]
    ],

    'getAssetSubCategoryByAssetCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getAssetSubCategoryByAssetCategory[/:id]',
            'defaults' => [
                'controller' => Controller\AssetExampleController::class,
                'action' => 'getAssetSubCategoryByAssetCategory'
            ]
        ]
    ],

    'getAssetTypeByAssetSubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getAssetTypeByAssetSubCategory[/:id]',
            'defaults' => [
                'controller' => Controller\AssetExampleController::class,
                'action' => 'getAssetTypeByAssetSubCategory'
            ]
        ]
    ],

    'getreceiptStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getreceiptStatus[/:id]',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'getreceiptStatus'
            ]
        ]
    ],

    'paymentAward' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/paymentAward[/:id]',
            'defaults'=>[
                'controller' => Controller\PaymentAwardController::class,
            ]
        ]
    ],

    'awardedTender' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/awardedTender[/:id]',
            'defaults' => [
                'controller' => Controller\TenderSubmissionController::class,
                'action' => 'awardedTender'
            ]
        ]
    ],

    'allItems' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/allItems',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'allItems'
            ]
        ]
    ],
    'allCategories' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/allCategories',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'allCategories'
            ]
        ]
    ],

    'pettyCashEntryReambersment' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/pettyCashEntryReambersment',
            'defaults' => [
                'controller' => Controller\PattyCashEntryController::class,
                'action' => 'pettyCashEntryReambersment'
            ]
        ]
    ],

    'budgetAvtivityImportdata' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetAvtivityImportdata',
            'defaults' => [
                'controller' => Controller\BudgetActivityController::class,
                'action' => 'budgetAvtivityImportdata'
            ]
        ]
    ],

    'debtorCategory' =>[
        'type' => Segment::class,
        'options'=>[
            'route' =>'/v1/debtorCategory[/:id]',
            'defaults'=>[
                'controller' => Controller\DebtorCategoryController::class,
            ]
        ]
    ],

    'getDebtorCategoryActive' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getDebtorCategoryActive[/:id]',
            'defaults' => [
                'controller' => Controller\DebtorCategoryController::class,
                'action' => 'getDebtorCategoryActive'
            ]
        ]
    ],

    'loanDeductionRatio' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanDeductionRatio[/:id]',
            'defaults' => [
                'controller' => Controller\LoanDeductionRatioController::class,
            ]
        ]
    ],

    'getLoanDeductionRatioActive' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getLoanDeductionRatioActive[/:id]',
            'defaults' => [
                'controller' => Controller\LoanDeductionRatioController::class,
                'action' => 'getLoanDeductionRatioActive'
            ]
        ]
    ],

    'employeeSalaryCalculation' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/employeeSalaryCalculation[/:id]',
            'defaults' => [
                'controller' => Controller\SalaryCalculationController::class,
                'action' => 'employeeSalaryCalculation'
            ]
        ]
    ],

    'getWarrantListNotIn' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getWarrantListNotIn',
            'defaults' => [
                'controller' => Controller\PurchaseOrderController::class,
                'action' => 'getWarrantListNotIn'
            ]
        ]
    ],

    'getGrnBasedOnVendor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getGrnBasedOnVendor',
            'defaults' => [
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'getGrnBasedOnVendor'
            ]
        ]
    ],

    'revenueType' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/revenueType[/:id]',
            'defaults' => [
                'controller' => Controller\RevenueTypeController::class,
            ]
        ]
    ],

    'getRevenueTypeActive' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getRevenueTypeActive[/:id]',
            'defaults' => [
                'controller' => Controller\RevenueTypeController::class,
                'action' => 'getRevenueTypeActive'
            ]
        ]
    ],

    'getGrnById' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getGrnById',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'getGrnById'
            ]
        ]
    ],

    'updateGrnBillRegistration' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/updateGrnBillRegistration',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'updateGrnBillRegistration'
            ]
        ]
    ],

    'listOfGrnByStatus' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/listOfGrnByStatus[/:id]',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'listOfGrnByStatus'
            ]
        ]
    ],

    'getGrnDetailsListById' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getGrnDetailsListById[/:id]',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'getGrnDetailsListById'
            ]
        ]
    ],

    'getInvestmentBankListByMasterId' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getInvestmentBankListByMasterId[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'getInvestmentBankListByMasterId'
            ]
        ]
    ],

    'approveAssetInformationMirrorData' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveAssetInformationMirrorData',
            'defaults' => [
                'controller' => Controller\AssetInformationController::class,
                'action' => 'approveAssetInformationMirrorData'
            ]
        ]
    ],

    'createAssetMirror' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/createAssetMirror',
            'defaults' => [
                'controller' => Controller\AssetInformationController::class,
                'action' => 'createAssetMirror'
            ]
        ]
    ],

    'getAssetInformationMirrorData' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getAssetInformationMirrorData[/:id]',
            'defaults' => [
                'controller' => Controller\AssetInformationController::class,
                'action' => 'getAssetInformationMirrorData'
            ]
        ]
    ],

    'getAssetPreRegistrationNotInAsset' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getAssetPreRegistrationNotInAsset[/:id]',
            'defaults' => [
                'controller' => Controller\AssetInformationController::class,
                'action' => 'getAssetPreRegistrationNotInAsset'
            ]
        ]
    ],

    'stockRegistrationMirror' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/stockRegistrationMirror[/:id]',
            'defaults' => [
                'controller' => Controller\StockRegistrationMirrorController::class,
            ]
        ]
    ],

    'getMirrorStockRegistrationActiveList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getMirrorStockRegistrationActiveList[/:id]',
            'defaults' => [
                'controller' => Controller\StockRegistrationMirrorController::class,
                'action' => 'getMirrorStockRegistrationActiveList'
            ]
        ]
    ],

    'approveStockRegistrationMirrorData' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveStockRegistrationMirrorData',
            'defaults' => [
                'controller' => Controller\StockRegistrationMirrorController::class,
                'action' => 'approveStockRegistrationMirrorData'
            ]
        ]
    ],
    'getBillRegistrationByVendor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getBillRegistrationByVendor[/:id]',
            'defaults' => [
                'controller' => Controller\BillRegistrationController::class,
                'action' => 'getBillRegistrationByVendor'
            ]
        ]
    ],
    'etf' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/etf[/:id]',
            'defaults' => [
                'controller' => Controller\EtfController::class,
            ]
        ]
    ],

    'approveEtfData' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveEtfData',
            'defaults' => [
                'controller' => Controller\EtfController::class,
                'action' => 'approveEtfData'
            ]
        ]
    ],

    'getEtfApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getEtfApprovals[/:status]',
            'defaults' => [
                'controller' => Controller\EtfController::class,
                'action' => 'getEtfApprovals'
            ]
        ]
    ],

    'etfGrouping' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/etfGrouping[/:id]',
            'defaults' => [
                'controller' => Controller\EtfGroupingController::class,
            ]
        ]
    ],

    'etfGroupingApprove' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/etfGroupingApprove',
            'defaults' => [
                'controller' => Controller\EtfGroupingController::class,
                'action' => 'approve'
            ]
        ]
    ],
    
    'approvedEtfGroupings' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approvedEtfGroupings[/:status]',
            'defaults' => [
                'controller' => Controller\EtfGroupingController::class,
                'action' => 'approvedEtfGroupings'
            ]
        ]
    ],

    'notification' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/notification[/:id]',
            'defaults' => [
                'controller' => Controller\NotificationController::class,
            ]
        ]
    ],

    'notificationActiveList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/notificationActiveList[/:id]',
            'defaults' => [
                'controller' => Controller\NotificationController::class,
                'action' => 'notificationActiveList'
            ]
        ]
    ],

    'investmentGetByApprovalDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentGetByApprovalDetails',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'investmentGetByApprovalDetails'
            ]
        ]
    ],

    'investmentToPaymentVouchar' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentToPaymentVouchar[/:id]',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'investmentToPaymentVouchar'
            ]
        ]
    ],

    'getMasterInvoiceListMaster' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getMasterInvoiceListMaster',
            'defaults' => [
                'controller' => Controller\MasterInvoiceController::class,
                'action' => 'getMasterInvoiceListMaster'
            ]
        ]
    ],

    'getInvestmentSummary' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getInvestmentSummary',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'getInvestmentSummary'
            ]
        ]
    ],

    'budgetTransfer' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/budgetTransfer[/:id]',
            'defaults' => [
                'controller' => Controller\BudgetTransferController::class
            ]
        ]
    ],
    'getFromTransferFunds' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getFromTransferFunds[/:fund]',
            'defaults' => [
                'controller' => Controller\BudgetTransferController::class,
                'action' => 'getFromTransferFunds'
            ]
        ]
    ],
    'getToTransferFunds' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getToTransferFunds[/:fund]',
            'defaults' => [
                'controller' => Controller\BudgetTransferController::class,
                'action' => 'getToTransferFunds'
            ]
        ]
    ],

    'getPaymentVoucherByVendor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getPaymentVoucherByVendor',
            'defaults' => [
                'controller' => Controller\PaymentVoucherController::class,
                'action' => 'getPaymentVoucherByVendor'
            ]
        ]
    ],

    'createUserRole' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/createUserRole',
            'defaults' => [
                'controller' => Controller\RoleController::class,
                'action' => 'createUserRole'
            ]
        ]
    ],
    'checkDuplication' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/checkDuplication',
            'defaults' => [
                'controller' => Controller\MasterController::class,
                'action' => 'checkDuplication'
            ]
        ]
    ],
    'loanNameNotInProcessingFee' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanNameNotInProcessingFee',
            'defaults' => [
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'loanNameNotInProcessingFee'
            ]
        ]
    ],
    'loanNameNotInLoanGrade' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanNameNotInLoanGrade',
            'defaults' => [
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'loanNameNotInLoanGrade'
            ]
        ]
    ],
    'loanNameNotInLoanParameters' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanNameNotInLoanParameters',
            'defaults' => [
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'loanNameNotInLoanParameters'
            ]
        ]
    ],
    'vendorNotInInsuranceCompany' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/vendorNotInInsuranceCompany',
            'defaults' => [
                'controller' => Controller\InsuranceCompanyController::class,
                'action' => 'vendorNotInInsuranceCompany'
            ]
        ]
    ],

    'getUserRole' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getUserRole',
            'defaults' => [
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'getUserRole'
            ]
        ]
    ],

    'getUserRoleById' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getUserRoleById[/:id]',
            'defaults' => [
                'controller' => Controller\LoanTypeSetupController::class,
                'action' => 'getUserRoleById'
            ]
        ]
    ],

    'loanProcessingFee' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanProcessingFee[/:id]',
            'defaults' => [
                'controller' => Controller\LoanProcessingFeeController::class,
            ]
        ]
    ],

    'receiptByBankDate' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/receiptByBankDate',
            'defaults' => [
                'controller' => Controller\ReceiptController::class,
                'action' => 'receiptByBankDate'
            ]
        ]
    ],
    'getEmployeeEmail' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getEmployeeEmail[/:id]',
            'defaults' => [
                'controller' => Controller\StaffController::class,
                'action' => 'getEmployeeEmail'
            ]
        ]
    ],

    'investmentApplicationDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentApplicationDetails',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'investmentApplicationDetails'
            ]
        ]
    ],

    'investmentApplicationDetailsApproved' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentApplicationDetailsApproved',
            'defaults' => [
                'controller' => Controller\InvestmentApplicationController::class,
                'action' => 'investmentApplicationDetailsApproved'
            ]
        ]
    ],

    'investmentRegistrationBankIdCheck' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentRegistrationBankIdCheck',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'investmentRegistrationBankIdCheck'
            ]
        ]
    ],

    'investmentRegistrationDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentRegistrationDetails',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'investmentRegistrationDetails'
            ]
        ]
    ],

    'investmentRegistrationDetailsForApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentRegistrationDetailsForApprovals',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'investmentRegistrationDetailsForApprovals'
            ]
        ]
    ],

    'investmentRegistrationAllApprovals' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/investmentRegistrationAllApprovals',
            'defaults' => [
                'controller' => Controller\InvestmentRegistrationController::class,
                'action' => 'investmentRegistrationAllApprovals'
            ]
        ]
    ],

    'warrant' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/warrant[/:id]',
            'defaults' => [
                'controller' => Controller\WarrantController::class,
            ]
        ]
    ],

    'getWarrantActiveList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getWarrantActiveList[/:id]',
            'defaults' => [
                'controller' => Controller\WarrantController::class,
                'action' => 'getWarrantActiveList'
            ]
        ]
    ],

    'approveWarrant' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveWarrant',
            'defaults' => [
                'controller' => Controller\WarrantController::class,
                'action' => 'approveWarrant'
            ]
        ]
    ],

    'deleteWarrantDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteWarrantDetails',
            'defaults' => [
                'controller' => Controller\WarrantController::class,
                'action' => 'deleteWarrantDetails'
            ]
        ]
    ],

     'nonEligibleGurantors' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/nonEligibleGurantors',
            'defaults' => [
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'nonEligibleGurantors'
            ]
        ]
    ],

    'batchWithdraw' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/batchWithdraw[/:id]',
            'defaults' => [
                'controller' => Controller\BatchWithdrawController::class,
            ]
        ]
    ],

    'getBatchWithdrawActiveList' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getBatchWithdrawActiveList[/:id]',
            'defaults' => [
                'controller' => Controller\BatchWithdrawController::class,
                'action' => 'getBatchWithdrawActiveList'
            ]
        ]
    ],

    'approveBatchWithdraw' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/approveBatchWithdraw',
            'defaults' => [
                'controller' => Controller\BatchWithdrawController::class,
                'action' => 'approveBatchWithdraw'
            ]
        ]
    ],

    'deleteBatchWithdrawDetails' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/deleteBatchWithdrawDetails',
            'defaults' => [
                'controller' => Controller\BatchWithdrawController::class,
                'action' => 'deleteBatchWithdrawDetails'
            ]
        ]
    ],

    'getInvestmentRegNotInBatchWithdraw' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getInvestmentRegNotInBatchWithdraw',
            'defaults' => [
                'controller' => Controller\BatchWithdrawController::class,
                'action' => 'getInvestmentRegNotInBatchWithdraw'
            ]
        ]
    ],

    'getItemSetUpBySubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getItemSetUpBySubCategory[/:id]',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'getItemSetUpBySubCategory'
            ]
        ]
    ],

    'getItemSetUpBySubCategory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getItemSetUpBySubCategory[/:id]',
            'defaults' => [
                'controller' => Controller\ItemController::class,
                'action' => 'getItemSetUpBySubCategory'
            ]
        ]
    ],

    'getGrnByAsset' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getGrnByAsset',
            'defaults' => [
                'controller' => Controller\GrnController::class,
                'action' => 'getGrnByAsset'
            ]
        ]
    ],
    'loanPaymentHistory' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/loanPaymentHistory[/:id]',
            'defaults' => [
                'controller' => Controller\LoanPaymentHistoryController::class,
            ]
        ]
    ],
     'getLoanAmount' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/getLoanAmount[/:id]',
            'defaults' => [
                'controller' => Controller\VehicleLoanController::class,
                'action' => 'getLoanAmount'
            ]
        ]
    ],
];