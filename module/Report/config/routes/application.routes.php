<?php
namespace Report;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/report[/:id]',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                //'action' => 'index'
            ]
        ]
    ],
    'invoiceReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/invoiceReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'invoiceReport'
            ]
        ]
    ],
    'TBReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/TBReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'TBReport'
            ]
        ]
    ],
    'BalanceReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/BalanceReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'BalanceReport'
            ]
        ]
    ],
    'gst' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/gst',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'gst'
            ]
        ]
    ],
    'item' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/item',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'item'
            ]
        ]
    ],
    'htmlReport1' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/htmlReport1',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'htmlReport1'
            ]
        ]
    ],
    'htmlReport2' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/htmlReport2',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'htmlReport2'
            ]
        ]
    ],
    'studentInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentInvoice',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'studentInvoice'
            ]
        ]
    ],
    'tenderAward' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/tenderAward[/:id]',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'tenderAward'
            ]
        ]
    ],
    'onlineUpdate' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/onlineUpdate',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'onlineUpdate'
            ]
        ]
    ],
    'vehicleLoanReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/vehicleLoanReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'vehicleLoanReport'
            ]
        ]
    ],
    'smartPhoneLoan' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/smartPhoneLoan',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'smartPhoneLoan'
            ]
        ]
    ],
    'cashAdvanceReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/cashAdvanceReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'cashAdvanceReport'
            ]
        ]
    ],
    'invoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/invoice',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'invoice'
            ]
        ]
    ],
    'accountCodeReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/accountCodeReport',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'accountCodeReport'
            ]
        ]
    ],
   'salarySlip' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/salarySlip',
        'defaults' => [
            'controller' => Controller\SalarySlipController::class,
            'action' => 'salarySlip'
            ]
        ]
    ],
    'salaryStatement' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/salaryStatement',
        'defaults' => [
            'controller' => Controller\SalaryStatementController::class,
            'action' => 'salaryStatement'
            ]
        ]
    ],
    'accountCode' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/accountCode',
        'defaults' => [
            'controller' => Controller\AccountcodeController::class,
            'action' => 'accountCode'
            ]
        ]
    ],
    'budgetReport' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/budgetReport',
        'defaults' => [
            'controller' => Controller\BudgetReportController::class,
            'action' => 'budgetReport'
            ]
        ]
    ],
    'remainingBalance' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/remainingBalance',
        'defaults' => [
            'controller' => Controller\RemainingBalanceController::class,
            'action' => 'remainingBalance'
            ]
        ]
    ],
    'summaryStatement' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/summaryStatement',
        'defaults' => [
            'controller' => Controller\SummaryStatementController::class,
            'action' => 'summaryStatement'
            ]
        ]
    ],
    'expenditureReport' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/expenditureReport',
        'defaults' => [
            'controller' => Controller\ExpenditureReportController::class,
            'action' => 'expenditureReport'
            ]
        ]
    ],
    'investmentInstructionForWithdrawl' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/investmentInstructionForWithdrawl',
        'defaults' => [
            'controller' => Controller\InvestmentInstructionForWithdrawlController::class,
            'action' => 'investmentInstructionForWithdrawl'
            ]
        ]
    ],
    'letterOfInstruction' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/letterOfInstruction',
        'defaults' => [
            'controller' => Controller\LetterOfInstructionController::class,
            'action' => 'letterOfInstruction'
            ]
        ]
    ],
    'letterOfApplicationInvestment' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/letterOfApplicationInvestment',
        'defaults' => [
            'controller' => Controller\LetterOfApplicationInvestmentController::class,
            'action' => 'letterOfApplicationInvestment'
            ]
        ]
    ],
    'reportByReceiptName' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/reportByReceiptName',
        'defaults' => [
            'controller' => Controller\ReportByReceiptNameController::class,
            'action' => 'reportByReceiptName'
            ]
        ]
    ],
    'paymentInfoLetter' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/paymentInfoLetter',
        'defaults' => [
            'controller' => Controller\PaymentInfoLetterController::class,
            'action' => 'paymentInfoLetter'
            ]
        ]
    ],
    'reportBaucarPayment' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/reportBaucarPayment',
        'defaults' => [
            'controller' => Controller\ReportBaucarPaymentController::class,
            'action' => 'reportBaucarPayment'
            ]
        ]
    ],
    'underPayableListedReport' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/underPayableListedReport',
        'defaults' => [
            'controller' => Controller\UnderPayableListedReportController::class,
            'action' => 'underPayableListedReport'
            ]
        ]
    ],
    'paymentPerformanceReport' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/paymentPerformanceReport',
        'defaults' => [
            'controller' => Controller\PaymentPerformanceReportController::class,
            'action' => 'paymentPerformanceReport'
            ]
        ]
    ],
    'paymentReportWithoutPO' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/paymentReportWithoutPO',
        'defaults' => [
            'controller' => Controller\PaymentReportWithoutPOController::class,
            'action' => 'paymentReportWithoutPO'
            ]
        ]
    ],
    'reportpaymentOver7days' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/reportpaymentOver7days',
        'defaults' => [
            'controller' => Controller\ReportpaymentOver7daysController::class,
            'action' => 'reportpaymentOver7days'
            ]
        ]
    ],
    'reportpaymentOver7daysDepartment' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/reportpaymentOver7daysDepartment',
        'defaults' => [
            'controller' => Controller\ReportpaymentOver7daysDepartmentController::class,
            'action' => 'reportpaymentOver7daysDepartment'
            ]
        ]
    ],
    'cancellationRevenueNo' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/cancellationRevenueNo',
        'defaults' => [
            'controller' => Controller\CancellationRevenueNoController::class,
            'action' => 'cancellationRevenueNo'
            ]
        ]
    ],

    'penyataAkaunReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/penyataAkaunReport',
            'defaults' => [
                'controller' => Controller\PenyataAkaunReportController::class,
                'action' => 'penyataAkaunReport'
            ]
        ]
    ],

    'computerPurchaseFinancingScheme' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/computerPurchaseFinancingScheme',
        'defaults' => [
            'controller' => Controller\ComputerPurchaseFinancingSchemeController::class,
            'action' => 'computerPurchaseFinancingScheme'
            ]
        ]
    ],

    'buyingBillingFinancingScheme' => [
    'type' => Segment::class,
    'options' => [
        'route' => '/v1/buyingBillingFinancingScheme',
        'defaults' => [
            'controller' => Controller\BuyingBillingFinancingSchemeController::class,
            'action' => 'buyingBillingFinancingScheme'
            ]
        ]
    ],
    'accountCodeReport1' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/accountCodeReport1',
            'defaults' => [
                'controller' => Controller\AccountCodeReportController::class,
                'action' => 'accountCodeReport1'
            ]
        ]
    ],

    'journalEntryReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/journalEntryReport',
            'defaults' => [
                'controller' => Controller\JournalEntryReportController::class,
                'action' => 'journalEntryReport'
            ]
        ]
    ],

    'debtReportBySponsor' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/debtReportBySponsor',
            'defaults' => [
                'controller' => Controller\DebtReportBySponsorController::class,
                'action' => 'debtReportBySponsor'
                ]
        ]
    ],    

    'studentSponsoringSketch' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentSponsoringSketch',
            'defaults' => [
                'controller' => Controller\StudentSponsoringSketchController::class,
                'action' => 'studentSponsoringSketch'
            ]
        ]
    ],

    'studentDebtInvoiceToSponser' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentDebtInvoiceToSponser',
            'defaults' => [
                'controller' => Controller\StudentDebtInvoiceToSponserController::class,
                'action' => 'studentDebtInvoiceToSponser'

            ]
        ]
    ],

    'sponsershipListForTheSemesterEnd' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsershipListForTheSemesterEnd',
            'defaults' => [
                'controller' => Controller\SponsershipListForTheSemesterEndController::class,
                'action' => 'sponsershipListForTheSemesterEnd'

            ]
        ]
    ],


    'furtherSponseredSystem' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/furtherSponseredSystem',
            'defaults' => [
                'controller' => Controller\FurtherSponseredSystemController::class,
                'action' => 'furtherSponseredSystem'

            ]
        ]
    ],

    'grnReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/grnReport',
            'defaults' => [
                'controller' => Controller\GrnReportController::class,
                'action' => 'grnReport'

            ]
        ]
    ],

    'purchaseOrderReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrderReport',
            'defaults' => [
                'controller' => Controller\PurchaseOrderReportController::class,
                'action' => 'purchaseOrderReport'

            ]
        ]
    ],

    'purchaseRequistionEntryReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseRequistionEntryReport',
            'defaults' => [
                'controller' => Controller\PurchaseRequistionEntryReportController::class,
                'action' => 'purchaseRequistionEntryReport'

            ]
        ]
    ],

    'suratTawaranKompInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/suratTawaranKompInvoice',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'suratTawaranKompInvoice'
            ]
        ]
    ],
    'suratTawaranPinjamanInvoice' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/suratTawaranPinjamanInvoice',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'suratTawaranPinjamanInvoice'
            ]
        ]
    ],

    'purchaseOrderIDReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/purchaseOrderIDReport',
            'defaults' => [
                'controller' => Controller\PurchaseOrderIDReportController::class,
                'action' => 'purchaseOrderIDReport'
            ]
        ]
    ],


     'listofChangesDataB15Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/listofChangesDataB15Report',
            'defaults' => [
                'controller' => Controller\ListofChangesDataB15ReportController::class,
                'action' => 'listofChangesDataB15Report'
            ]
        ]
    ],

    'claimPaymentC3Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimPaymentC3Report',
            'defaults' => [
                'controller' => Controller\ClaimPaymentC3ReportController::class,
                'action' => 'claimPaymentC3Report'
            ]
        ]
    ],

    'studentAffairsDepartmentG1Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentAffairsDepartmentG1Report',
            'defaults' => [
                'controller' => Controller\StudentAffairsDepartmentG1ReportController::class,
                'action' => 'studentAffairsDepartmentG1Report'
            ]
        ]
    ],

    'claimTuitionFeesG2Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimTuitionFeesG2Report',
            'defaults' => [
                'controller' => Controller\ClaimTuitionFeesG2ReportController::class,
                'action' => 'claimTuitionFeesG2Report'
            ]
        ]
    ],

    'listOfDefendantClaimsListH1Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/listOfDefendantClaimsListH1Report',
            'defaults' => [
                'controller' => Controller\ListOfDefendantClaimsListH1ReportController::class,
                'action' => 'listOfDefendantClaimsListH1Report'
            ]
        ]
    ],

    'ListofDefendantClaimsListA6Reoprt' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/ListofDefendantClaimsListA6Reoprt',
            'defaults' => [
                'controller' => Controller\ListofDefendantClaimsListA6ReoprtController::class,
                'action' => 'ListofDefendantClaimsListA6Reoprt'
            ]
        ]
    ],

    'claimTuitionFeesForLibyansSudentA12Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimTuitionFeesForLibyansSudentA12Report',
            'defaults' => [
                'controller' => Controller\ClaimTuitionFeesForLibyansSudentA12ReportController::class,
                'action' => 'claimTuitionFeesForLibyansSudentA12Report'
            ]
        ]
    ],

    'reportsOfSlabAndSkpdLessonRepealsA24Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/reportsOfSlabAndSkpdLessonRepealsA24Report',
            'defaults' => [
                'controller' => Controller\ReportsOfSlabAndSkpdLessonRepealsA24ReportController::class,
                'action' => 'reportsOfSlabAndSkpdLessonRepealsA24Report'
            ]
        ]
    ],

    'reportsOfSlabAndSkpdLessonRepealsA27Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/reportsOfSlabAndSkpdLessonRepealsA27Report',
            'defaults' => [
                'controller' => Controller\ReportsOfSlabAndSkpdLessonRepealsA27ReportController::class,
                'action' => 'reportsOfSlabAndSkpdLessonRepealsA27Report'
            ]
        ]
    ],

    'studentDebtInvoiceToSponsorsA17Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentDebtInvoiceToSponsorsA17Report',
            'defaults' => [
                'controller' => Controller\StudentDebtInvoiceToSponsorsA17ReportController::class,
                'action' => 'studentDebtInvoiceToSponsorsA17Report'
            ]
        ]
    ],

    'projectionReportA19Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/projectionReportA19Report',
            'defaults' => [
                'controller' => Controller\ProjectionReportA19ReportController::class,
                'action' => 'projectionReportA19Report'
            ]
        ]
    ],

    'provisionOfDomesticStudyFeeAndFlightTicketsA21Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/provisionOfDomesticStudyFeeAndFlightTicketsA21Report',
            'defaults' => [
                'controller' => Controller\ProvisionOfDomesticStudyFeeAndFlightTicketsA21ReportController::class,
                'action' => 'provisionOfDomesticStudyFeeAndFlightTicketsA21Report'
            ]
        ]
    ],

    'receiptsA23Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/receiptsA23Report',
            'defaults' => [
                'controller' => Controller\ReceiptsA23ReportController::class,
                'action' => 'receiptsA23Report'
            ]
        ]
    ],

    'listOfStudentsA25Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/listOfStudentsA25Report',
            'defaults' => [
                'controller' => Controller\ListOfStudentsA25ReportController::class,
                'action' => 'listOfStudentsA25Report'
            ]
        ]
    ],

    'listOfStudentsA30Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/listOfStudentsA30Report',
            'defaults' => [
                'controller' => Controller\ListOfStudentsA30ReportController::class,
                'action' => 'listOfStudentsA30Report'
            ]
        ]
    ],

    'sponsorListofClaimsA10Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorListofClaimsA10Report',
            'defaults' => [
                'controller' => Controller\sponsorListofClaimsA10ReportController::class,
                'action' => 'sponsorListofClaimsA10Report'
            ]
        ]
    ],

    'sponsorListofClaimsA11Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/sponsorListofClaimsA11Report',
            'defaults' => [
                'controller' => Controller\SponsorListofClaimsA11ReportController::class,
                'action' => 'sponsorListofClaimsA11Report'
            ]
        ]
    ],

    'claimTuitionFeesA13Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimTuitionFeesA13Report',
            'defaults' => [
                'controller' => Controller\ClaimTuitionFeesA13ReportController::class,
                'action' => 'claimTuitionFeesA13Report'
            ]
        ]
    ],

    'studentPaymentReportA31Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentPaymentReportA31Report',
            'defaults' => [
                'controller' => Controller\StudentPaymentReportA31ReportController::class,
                'action' => 'studentPaymentReportA31Report'
            ]
        ]
    ],

    'studentPersonalInformationA4Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentPersonalInformationA4Report',
            'defaults' => [
                'controller' => Controller\StudentPersonalInformationA4ReportController::class,
                'action' => 'studentPersonalInformationA4Report'
            ]
        ]
    ],

    'studentDebtInvoiceA8Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentDebtInvoiceA8Report',
            'defaults' => [
                'controller' => Controller\StudentDebtInvoiceA8ReportController::class,
                'action' => 'studentDebtInvoiceA8Report'
            ]
        ]
    ],

    'claimPaymentA9Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimPaymentA9Report',
            'defaults' => [
                'controller' => Controller\ClaimPaymentA9ReportController::class,
                'action' => 'claimPaymentA9Report'
            ]
        ]
    ],

    'studentAffairsD1Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/studentAffairsD1Report',
            'defaults' => [
                'controller' => Controller\StudentAffairsD1ReportController::class,
                'action' => 'studentAffairsD1Report'
            ]
        ]
    ],

    'myBrainPhdD2Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/myBrainPhdD2Report',
            'defaults' => [
                'controller' => Controller\MyBrainPhdD2ReportController::class,
                'action' => 'myBrainPhdD2Report'
            ]
        ]
    ],

    'claimPaymentD4Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimPaymentD4Report',
            'defaults' => [
                'controller' => Controller\ClaimPaymentD4ReportController::class,
                'action' => 'claimPaymentD4Report'
            ]
        ]
    ],

    'claimPaymentD5Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimPaymentD5Report',
            'defaults' => [
                'controller' => Controller\ClaimPaymentD5ReportController::class,
                'action' => 'claimPaymentD5Report'
            ]
        ]
    ],

    'claimPaymentD6Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/claimPaymentD6Report',
            'defaults' => [
                'controller' => Controller\ClaimPaymentD6ReportController::class,
                'action' => 'claimPaymentD6Report'
            ]
        ]
    ],

    'departmentOfDepartmentE4Report' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/departmentOfDepartmentE4Report',
            'defaults' => [
                'controller' => Controller\DepartmentOfDepartmentE4ReportController::class,
                'action' => 'departmentOfDepartmentE4Report'
            ]
        ]
    ],
    
    'updateTable' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/updateTable[/:table]',
            'defaults' => [
                'controller' => Controller\ReportController::class,
                'action' => 'updateTable'
            ]
        ]
    ],

    'invoiceEntryReport' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/invoiceEntryReport[/:table]',
            'defaults' => [
                'controller' => Controller\InvoiceEntryReportController::class,
                'action' => 'invoiceEntryReport'
            ]
        ]
    ],

    'warrantPrint' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/warrantPrint',
            'defaults' => [
                'controller' => Controller\WarrantPrintController::class,
                'action' => 'warrantPrint'
            ]
        ]
    ],

    'letterForInvestmentToUUMBank' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/v1/letterForInvestmentToUUMBank',
            'defaults' => [
                'controller' => Controller\LetterForInvestmentToUUMBankController::class,
                'action' => 'letterForInvestmentToUUMBank'
            ]
        ]
    ],
    
];