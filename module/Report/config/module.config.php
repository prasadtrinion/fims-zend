<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Report;

use Zend\Mvc\Controller\LazyControllerAbstractFactory;
use Zend\Stdlib\ArrayUtils;

$routes = [];

foreach(glob(__DIR__ . '/routes/*.routes.php') as $filename){
    $routes = ArrayUtils::merge($routes, require $filename);
}

return [
    'controllers' => [
        'factories' => [
        Controller\ReportController::class => LazyControllerAbstractFactory::class,
		Controller\SalarySlipController::class => LazyControllerAbstractFactory::class,
        Controller\SalaryStatementController::class => LazyControllerAbstractFactory::class,
        Controller\AccountcodeController::class => LazyControllerAbstractFactory::class,
        Controller\BudgetReportController::class => LazyControllerAbstractFactory::class,
        Controller\RemainingBalanceController::class => LazyControllerAbstractFactory::class,
        Controller\SummaryStatementController::class => LazyControllerAbstractFactory::class,
        Controller\ExpenditureReportController::class => LazyControllerAbstractFactory::class,
        Controller\InvestmentInstructionForWithdrawlController::class => LazyControllerAbstractFactory::class,
        Controller\LetterOfInstructionController::class => LazyControllerAbstractFactory::class,
        Controller\LetterOfApplicationInvestmentController::class => LazyControllerAbstractFactory::class,
        Controller\ReportByReceiptNameController::class => LazyControllerAbstractFactory::class,
        Controller\PaymentInfoLetterController::class => LazyControllerAbstractFactory::class,
        Controller\ReportBaucarPaymentController::class => LazyControllerAbstractFactory::class,
        Controller\UnderPayableListedReportController::class => LazyControllerAbstractFactory::class,
        Controller\PaymentPerformanceReportController::class => LazyControllerAbstractFactory::class,
        Controller\PaymentReportWithoutPOController::class => LazyControllerAbstractFactory::class,
        Controller\ReportpaymentOver7daysController::class => LazyControllerAbstractFactory::class,
        Controller\ReportpaymentOver7daysDepartmentController::class => LazyControllerAbstractFactory::class,
        Controller\PenyataAkaunReportController::class => LazyControllerAbstractFactory::class,
        Controller\CancellationRevenueNoController::class => LazyControllerAbstractFactory::class,
        Controller\ComputerPurchaseFinancingSchemeController::class => LazyControllerAbstractFactory::class,
        Controller\BuyingBillingFinancingSchemeController::class => LazyControllerAbstractFactory::class,
        Controller\AccountCodeReportController::class => LazyControllerAbstractFactory::class,
        Controller\JournalEntryReportController::class => LazyControllerAbstractFactory::class,
        Controller\DebtReportBySponsorController::class => LazyControllerAbstractFactory::class,
        Controller\StudentSponsoringSketchController::class => LazyControllerAbstractFactory::class,
        Controller\StudentDebtInvoiceToSponserController::class => LazyControllerAbstractFactory::class,
        Controller\SponsershipListForTheSemesterEndController::class => LazyControllerAbstractFactory::class,
        Controller\FurtherSponseredSystemController::class => LazyControllerAbstractFactory::class,
        Controller\GrnReportController::class => LazyControllerAbstractFactory::class,
        Controller\PurchaseOrderReportController::class => LazyControllerAbstractFactory::class,
        Controller\PurchaseRequistionEntryReportController::class => LazyControllerAbstractFactory::class,
        Controller\PurchaseOrderIDReportController::class => LazyControllerAbstractFactory::class,
        Controller\SponsorListReportController::class => LazyControllerAbstractFactory::class,
        Controller\SponsorTaggingTotalSponsorshipReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentBillSponsorReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentInvoiceReportController::class => LazyControllerAbstractFactory::class,
        Controller\InvoicetoSponsorReportController::class => LazyControllerAbstractFactory::class,
        Controller\CoverLettertoSponsorController::class => LazyControllerAbstractFactory::class,
        Controller\SponsorInvoiceController::class => LazyControllerAbstractFactory::class,
        Controller\BilltoSponsorController::class => LazyControllerAbstractFactory::class,
        Controller\ApplicationForAllocationController::class => LazyControllerAbstractFactory::class,
        Controller\PaymentOfAllocationController::class => LazyControllerAbstractFactory::class,
        Controller\PaymentReceiptsatTheSemesterEndController::class => LazyControllerAbstractFactory::class,
        Controller\StudentAccountSystemController::class => LazyControllerAbstractFactory::class,
        Controller\StudentAffliatementC1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\SlipBankM1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimTuitionA15ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListDataB9ReportController::class => LazyControllerAbstractFactory::class,
        Controller\JunrnalBaucerE4ReportController::class => LazyControllerAbstractFactory::class,
        Controller\DepartmentA14ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StatementofRevenueA32ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StatementsOfReceiptsA26ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ImplementationscheduleA29ReportController::class => LazyControllerAbstractFactory::class,
        Controller\DepartmentOfDepartmentA15ReportController::class => LazyControllerAbstractFactory::class,
        Controller\RetirementStatementO1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\DepartmentOfDepartmentE4ReportController::class => LazyControllerAbstractFactory::class,
        Controller\TuitionFeesC2ReportController::class => LazyControllerAbstractFactory::class,
        Controller\PalejarAccSystemF1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\InvoiceStudentsF2ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ReportRequiredF3ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentAccSystemJ1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentCodeA16ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentLoanReportI1ReportController::class => LazyControllerAbstractFactory::class, 
        Controller\ClaimPaymentC3ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListofChangesDataB15ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentAffairsDepartmentG1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimTuitionFeesG2ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListOfDefendantClaimsListH1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListofDefendantClaimsListA6ReoprtController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimTuitionFeesForLibyansSudentA12ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ReportsOfSlabAndSkpdLessonRepealsA24ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ReportsOfSlabAndSkpdLessonRepealsA27ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentDebtInvoiceToSponsorsA17ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ProjectionReportA19ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ProvisionOfDomesticStudyFeeAndFlightTicketsA21ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ReceiptsA23ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListOfStudentsA25ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ListOfStudentsA30ReportController::class => LazyControllerAbstractFactory::class,
        Controller\sponsorListofClaimsA10ReportController::class => LazyControllerAbstractFactory::class,
        Controller\SponsorListofClaimsA11ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimTuitionFeesA13ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentPaymentReportA31ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentPersonalInformationA4ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentDebtInvoiceA8ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimPaymentA9ReportController::class => LazyControllerAbstractFactory::class,
        Controller\MyBrainPhdD2ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimPaymentD4ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimPaymentD5ReportController::class => LazyControllerAbstractFactory::class,
        Controller\ClaimPaymentD6ReportController::class => LazyControllerAbstractFactory::class,
        Controller\StudentAffairsD1ReportController::class => LazyControllerAbstractFactory::class,
        Controller\InvoiceEntryReportController::class => LazyControllerAbstractFactory::class,
        Controller\WarrantPrintController::class => LazyControllerAbstractFactory::class,
        Controller\LetterForInvestmentToUUMBankController::class => LazyControllerAbstractFactory::class,
        
        ],
        
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'report/index/index' => __DIR__ . '/../view/report/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
           __DIR__ . '/../view',
           __DIR__ . '/../view/application',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'router' => [
        'routes' => $routes
    ],
    'console' => [
        'router' => [
            'routes' => []
        ]
    ]
];

