<?php
return array(
    'GUEST' => [
        'login',
        'register',
        'registerSuccess',
        'forgotPassword',
        'resetPassword',
        'verifyEmail',
        'ajax/stateList',
        'ajax/isUsernameAvailable',
        'ajax/getStaticPage',
        'ajax/sendEmailVerification',
        'student',
        'college',
        'menu',
        'group',
        'company',
        'definationtypems',
        'definationms',
        'glcode',
        'financialyear',
        'master',
        'openingbalance',
        'journal',
        'lawyer',
        'role',
        'country',
    
        
    ],
    'DSR' => [
        'dashboard',
        'profile',
        // 'student',
    ],
    'CSR' => [
        'users',
        'users/list',
        'users/export'
    ],
    'SYSTEM_ADMIN' => [
        'users/create',
        'users/edit',
        'users/delete'   
         ]
);
