<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class DebtReportBySponsorController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report5
    public function DebtReportBySponsorAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='80%'><font size='2'>LAPORAN HUTANG MENGIKUT PENAJA<br>NAMA PENAJA : MYBRAIN<br> KOD PENAJA 
             : E 255 </font></td>
            </tr>
            </table>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>Bil</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TARIKH INVOIS </font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>RUJUKAN COVER LETTER/RUJUKAN INVOICE </font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>SEMESTER</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>KOD SEMESTER</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>JUMLAM INVOICE</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>JUMLAM BAYARAN </font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>NO EFT: RUJUKAN BAYARAN DARIPADA PENAJA</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>BAKI TUNTUTAN YANG BELUM DI BAYAR</font></td>
           </tr>
            <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>1.</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>Tarikh penyediaan invoice</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>RAJ KAMI: UUM/HEP/UA/K-27/KBXXX</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>Sep-16</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>A161</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>50,000.00</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>45,000.00</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>EFT 11025220077</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='4'>5,000.00</font></td>
           </tr>

           <tr>
          <td style='text-align:center' width='25%' valign='top' colspan='5'><font size='4'>JUMLAH KESELURUHAN TUNTUTAN</font></td>
          <td style='text-align:center' width='25%' valign='top'><font size='4'>50,000.00</font></td>
          <td style='text-align:center' width='25%' valign='top'><font size='4'> 45,000.00</font></td>
          <td style='text-align:center' width='25%' valign='top'><font size='4'></font></td>
          <td style='text-align:center' width='25%' valign='top'><font size='4'>5,000.00</font></td>
          </tr>

           </table>
           
        
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}