<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PalejarAccSystemF1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // PalejarAccSystemF1Report F-1
    public function palejarAccSystemF1ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       
       $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = $file_data ."
<html>
<head>
  <title>F1</title>
  <style>
     th {

      border: 1px solid black;
    }
    
  </style>
</head>
<body>
<table align='center' width='100%'>
  <tr>
     <td style='text-align: center; width: 90% '><font size='3'>Sistem Akaun Palejar</font></td>
     <td  style='text-align: right ; '><font size='3'>3/8/2018</font></td>
  </tr>
  <tr>
     <td style='text-align: center;'><font size='3'>Laporan Invois Setiap Palejar</font></td>
  </tr>
</table><br><br>

<table align='center' width='100%'>

  <tr>
     <td style='text-align: left;'><font size='3'><b>No. Matrik : 242911</b></font></td>
      <td style='text-align: left;'><font size='3'><b>Nama : SITI NUR AISYAH BT AHMAD ALI</b></font></td>
  </tr>
</table><br>

<table  align='center' width='100%' >
<tr>
  <th style='text-align: center; width: 5% '><font size='3'><b>Bill</b></font></th>
    <th  style='text-align: center; width: 40%' ><font size='3'><b>Ourref</b></font></th>
    <th  style='text-align: center; width: 20%'><font size='3'><b>No. Invois</b></font></th>
    <th  style='text-align: center; width: 20%' ><font size='3'><b>Sesi/Sem </b></font></th>
    <th  style='text-align: center; width: 5%'><font size='3'><b>Jumlah Tuntutan(RM)</b></font></th>
  </tr>

  <tr>
   <td style='text-align: center; '><font size='3'>1</font></td>
     <td  style='text-align: center; ' ><font size='3'>UUM/HEP/U-ASSIST/K-2</font></td>
     <td  style='text-align: center; '><font size='3'>153/A171</font></td>
     <td  style='text-align: center; ' ><font size='3'>2017/2018 & A171<br></font></td>
     <td  style='text-align: center; ' ><font size='3'>753.00</font><b></b></td>
</tr>
</table><br>
<hr align='right' width='150px'>
<table table align='center' width='100%'>
  <tr>
    <td  style='text-align: right; width: 90%'><font size='3'>Jumlah Keseluruhan : </font></td>
    <td  style='text-align:center; width: 10%'><font size='3'>3012.00 </font></td>
  </tr>
</table>
</body>
</html>";
   
    
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}