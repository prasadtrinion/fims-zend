<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class RemainingBalanceController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function remainingBalanceAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {

        $year = $postData['financialYear'];
        $department = $postData['departmentCode'];
         // print_r($year);exit;
        $reportRepository = $this->getRepository('T019fsavedReports');
        if(isset($department)){
        $departmentRepository = $this->getRepository('Application\Entity\T015fdepartment');
        $department_name = $departmentRepository->findBy(array('f015fdepartmentCode'=>$department));
        $department_name = $department_name[0];
        $department_name = $department_name->getF015fdepartmentName();
        $dept_code_name =  $department."-".$department_name;
        }
        $yearBalance = $reportRepository->getBalanceYear($year);
        $yearBalance = $yearBalance[0]['f110fyear'];
        // print_r($yearBalance);exit;

        $pay_date = date("d/m/Y", time());             
        // $pay_date   = date("d/m/Y", strtotime($pay_date));

        // print_r($pay_date);exit;

        $pay_time = date('h:i:s a', time());
        // print_r($pay_time);exit;

        $file_data = $file_data . "
    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
       baki peruntukan bagi  period
        <tr>
         <td style='text-align: center'  font-size='13' width='100%' valign='top'><b>UNIVERSITI UTARA MALAYSIA<br>I F A S<br>BAKI PERUNTUKAN BAGI PERIOD: $yearBalance</b><BR><B>$dept_code_name</B></td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
             <td style='text-align: left'><font size='3'>LAPORAN :<br>MASA : $pay_time </font></td>
             <td style='text-align: right;'><font size='3'>TARIKH: $pay_date</font></td>
            </tr>
    </table>
    <br>
      <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
       
        <tr>
         <td style='text-align: left;' width='10%'><font size='2'>KOD AKAUN</font></td>
         <td style='text-align: center;' width='10%'><font size='2'>SUB/SO</font></td>
         <td style='text-align: right' width='10%'><font size='2'>JENIS VOT</font></td>
         <td style='text-align: right' width='10%'><font size='2'>PERUNTUKAN</font></td>
         <td style='text-align: right' width='10%'><font size='2'>TANGGUNGAN</font></td>
         <td style='text-align: right' width='10%'><font size='2'>BELANJA</font></td>
         <td style='text-align: right' width='10%'><font size='2'>PENINGKATAN</font></td>
         <td style='text-align: right' width='10%'><font size='2'>PEMULANGAN</font></td>
         <td style='text-align: right' width='10%'><font size='2'>PERA</font></td>
        </tr>
        ";
        

        $result = $reportRepository->getBudgetSummary($year,$department);
        // print_r($result);exit;



        $allocated_total = 0;
        $adjustment_total = 0;
        $increment_total = 0;
        $decrement_total = 0;
        $Expenditure_total = 0;
        $pending_total = 0;
        $fromAmount_total = 0;
        $toAmount_total = 0;
        foreach ($result as $budget)
        {
          // print_r($budget);exit;
            $dept_name = $budget['f015fdepartment_name'];
            $sub_so = $budget['so_code'];
            $department_code = $budget['f015fdepartment_code'];
            $allocated_amount = $budget['f108fallocated_amount'];
            $adjustmentAmount = $budget['adjustmentAmount'];
            $incrementAmount = $budget['incrementAmount'];
            $decrementAmount = $budget['decrementAmount'];
            $Expenditure_amount = $budget['Expenditure'];
            $pending_amount = $budget['pending'];
            $accountCode = $budget['f108faccount'];
            $verimentFrom = $budget['f108fveriment_from'];
            $verimentTo = $budget['f108fveriment_to'];
            // print_r($dept_name);exit;

            $allocated = $budget['f108fallocated_amount'];
            $adjustment = $budget['adjustmentAmount'];
            $increment = $budget['incrementAmount'];
            $decrement = $budget['decrementAmount'];
            $Expenditure = $budget['Expenditure'];
            $pending = $budget['pending'];
            $verimentFromAmount = $budget['f108fveriment_from'];
            $verimentToAmount = $budget['f108fveriment_to'];

            $allocated_amount = number_format($allocated_amount, 2, '.', ',');
            $adjustmentAmount = number_format($adjustmentAmount, 2, '.', ',');
            $incrementAmount = number_format($incrementAmount, 2, '.', ',');
            $decrementAmount = number_format($decrementAmount, 2, '.', ',');
            $Expenditure_amount = number_format($Expenditure_amount, 2, '.', ',');
            $pending_amount = number_format($pending_amount, 2, '.', ',');
            $verimentFrom = number_format($verimentFrom, 2, '.', ',');
            $verimentTo = number_format($verimentTo, 2, '.', ',');


            $file_data = $file_data . "<tr>
         <td style='text-align: left;' width='10%'><font size='2'>$accountCode</font></td>
         <td style='text-align: left;' width='10%'><font size='2'>$sub_so</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$allocated_amount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$adjustmentAmount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$verimentFrom</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$verimentTo</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$incrementAmount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$decrementAmount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$pending_amount</font></td>
        </tr>";
        $allocated_total = floatval($allocated_total) + floatval($allocated);
        $adjustment_total = floatval($adjustment_total) + floatval($adjustment);
        $increment_total = floatval($increment_total) + floatval($increment);
        $decrement_total = floatval($decrement_total) + floatval($decrement);
        $Expenditure_total = floatval($Expenditure_total) + floatval($Expenditure);
        $pending_total = floatval($pending_total) + floatval($pending);
        $fromAmount_total = floatval($fromAmount_total) + floatval($verimentFromAmount);
        $toAmount_total = floatval($toAmount_total) + floatval($verimentToAmount);

        }
        $allocated_total = number_format($allocated_total, 2, '.', ',');
        $adjustment_total = number_format($adjustment_total, 2, '.', ',');
        $increment_total = number_format($increment_total, 2, '.', ',');
        $decrement_total = number_format($decrement_total, 2, '.', ',');
        $Expenditure_total = number_format($Expenditure_total, 2, '.', ',');
        $pending_total = number_format($pending_total, 2, '.', ',');
        $fromAmount = number_format($fromAmount_total, 2, '.', ',');
        $toAmount = number_format($toAmount_total, 2, '.', ',');
        
        $file_data = $file_data . "
        <tr>
         <td style='text-align: right' width='20%' colspan='2'><font size='2'>JUMLAH(RM):</font></td>
         <td style='text-align: right;' width='10%' ><font size='2'>$allocated_total </font></td>
         <td style='text-align: right' width='10%'><font size='2'>$adjustment_total</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$fromAmount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$toAmount</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$increment_total</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$decrement_total</font></td>
         <td style='text-align: right' width='10%'><font size='2'>$pending_total</font></td>
        </tr>
    </table>";
          
        
        
            
        }

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

    }
}