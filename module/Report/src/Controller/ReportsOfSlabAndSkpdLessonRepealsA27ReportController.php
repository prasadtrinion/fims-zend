<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportsOfSlabAndSkpdLessonRepealsA27ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function reportsOfSlabAndSkpdLessonRepealsA27ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title>A27</title>
</head>
<body>
<form>
  <table width='100%'>
    <tr>
      <td style='width: 90%;text-align: left;'>
        INSTITUSI :<br>
        BULAN     :JULAI-DISEMBER 2015
        
      </td>
      <td style='width: 10%;text-align: left;' valign='top'>
        Borang SPBB3
      </td>
    </tr>
    <tr>
      <td style='width: 90%;text-align: center;'>
      <p><center><b>LAPORAN BAYARAN PELAJARAN TAJAAN SLAB & SKPD BAGI TEMPOH BERAKHIR 31 DISEMEBER 2015</b></center></p> 
      </td>
    </tr>
  </table><br>
  <table align='center' width='100%' border='1'>
    <tr>
      <td style=' text-align: center; width:5%; ' rowspan='2'>BIL</td>
      <td style=' text-align: center; width:20%; ' rowspan='2'>NAMA PELAJAR</td>
      <td style=' text-align: center; width:10%; ' rowspan='2'>NO:KAD PENGENALAN</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>NO MARIK</td>
      <td style=' text-align: center; width:10%; ' colspan='2'>TEMPOH TAJAAN</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>STATUS</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>NAMA PROGRAM</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>BAYARAN (RM)</td>
      <td style=' text-align: center; width:10%; ' rowspan='2'>RUJUKAN BAYARAN</td>
      <td style=' text-align: center; width:15%; ' rowspan='2'>JENIS TUNTUTAN</td>
      <td style=' text-align: center; width:10; ' rowspan='2'>RUJUKAN PTB SPBB2 (LAMPIBAN)</td>
    </tr>
    <tr>
      
      <td style=' text-align: center; width:; '>MULA</td>
      <td style=' text-align: center; width:; '>TAMAT</td>
      
    </tr>
    <tr>
      <td style=' text-align: center; width:; '>BIL</td>
      <td style=' text-align: center; width:; '>NAMA PELAJAR</td>
      <td style=' text-align: center; width:; '>NO:KAD PENGENALAN</td>
      <td style=' text-align: center; width:; '>NO MARIK</td>
      <td style=' text-align: center; width:; '></td>
      <td style=' text-align: center; width:; '></td>
      <td style=' text-align: center; width:; '>STATUS</td>
      <td style=' text-align: center; width:; '>NAMA PROGRAM</td>
      <td style=' text-align: center; width:; '>BAYARAN (RM)</td>
      <td style=' text-align: center; width:; '>RUJUKAN BAYARAN</td>
      <td style=' text-align: center; width:; '>JENIS TUNTUTAN</td>
      <td style=' text-align: center; width:; '>RUJUKAN PTB SPBB2 (LAMPIBAN)</td>
    </tr>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}