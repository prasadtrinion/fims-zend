<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class MyBrainPhdD2ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function myBrainPhdD2ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <font size='5px'>
    <table width='100%'>
      <tr>
        <td style='width: 100%; text-align: center;'>
          <h2 >MYBRAIN PHD A171 <br>
        UUM/HEP/U-ASSIST/K-27/4/1(255)-KB0309
        </h2>
      </td>
    </tr>
    </table>
  <table border='1px solid black' cellpadding='8px' cellspacing='6px' width='100%' style='border-collapse: collapse;'>
    <tr>
      <th>BIL</th>; 
      <th>NO.MATRIK</th>
      <th>NAMA</th>
      <th>TAJAAN</th>
      <th>IPT</th>
      <th>JUMLAH</th>
        </td>
      </tr>
    </tr>
      <tr>
        <td style='text-align: center;'>1</td>
        <td style='text-align: center;'>900214</td>
        <td style='text-align: center;'>Nursakirah Ab Rahman Muton</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>2</td>
        <td style='text-align: center;'>95974</td>
        <td style='text-align: center;'>Nazni Bin Noordin</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>3</td>
        <td style='text-align: center;'>900809</td>
        <td style='text-align: center;'>Mohd Ammyrul Ashraf bin Sairan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>4</td>
        <td style='text-align: center;'>900803</td>
        <td style='text-align: center;'>Nur Azriati binti Mat</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>5</td>
        <td style='text-align: center;'>900591</td>
        <td style='text-align: center;'>Vimala a/p Darvmanathan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>6</td>
        <td style='text-align: center;'>900772</td>
        <td style='text-align: center;'>Mohamad Shahbani Bin Sekh Bidin</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>7</td>
        <td style='text-align: center;'>900727</td>
        <td style='text-align: center;'>Zulianis binti Alias</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>8</td>
        <td style='text-align: center;'>901151</td>
        <td style='text-align: center;'>Nor Hafizah binti Abdul Razak</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>9</td>
        <td style='text-align: center;'>900758</td>
        <td style='text-align: center;'>Mohd Zhafri bin Mohd Zuk</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>10</td>
        <td style='text-align: center;'>900986</td>
        <td style='text-align: center;'>NURUL HUSNA BT MAHADZIR</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>11</td>
        <td style='text-align: center;'>900586</td>
        <td style='text-align: center;'>ANITA A/P KANESTION</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>12</td>
        <td style='text-align: center;'>99241</td>
        <td style='text-align: center;'>Nik Nor Amalina Binti Nik Mohd Sukrri</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>13</td>
        <td style='text-align: center;'>99237</td>
        <td style='text-align: center;'>Samihah binti Suhail</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>14</td>
        <td style='text-align: center;'>99231</td>
        <td style='text-align: center;'>Malini A/P Thiagra</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>15</td>
        <td style='text-align: center;'>99224</td>
        <td style='text-align: center;'>Masita Binti Mt. Zin</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>488.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>16</td>
        <td style='text-align: center;'>99219</td>
        <td style='text-align: center;'>Nur Hidayah Binti Zaidun</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>17</td>
        <td style='text-align: center;'>99217</td>
        <td style='text-align: center;'>Noor Fatihah Binti Mat Radzi</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>18</td>
        <td style='text-align: center;'>99141</td>
        <td style='text-align: center;'>Nor Rabiatul Adawiyah Bt Nor Azam</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>19</td>
        <td style='text-align: center;'>99071</td>
        <td style='text-align: center;'>Mohd Zulfabli bin Hasan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>20</td>
        <td style='text-align: center;'>900810</td>
        <td style='text-align: center;'>Mohd Azudin bin Mohd Arif</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>21</td>
        <td style='text-align: center;'>96214</td>
        <td style='text-align: center;'>Norhanim binti Alwi</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>22</td>
        <td style='text-align: center;'>96199</td>
        <td style='text-align: center;'>Adawiah binti Idris</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>23</td>
        <td style='text-align: center;'>900949</td>
        <td style='text-align: center;'>Mime Azrina binti Jaafa</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>24</td>
        <td style='text-align: center;'>96061</td>
        <td style='text-align: center;'>Soong Cai Juan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>25</td>
        <td style='text-align: center;'>902318</td>
        <td style='text-align: center;'>Siti Rasifah Binti Ahmad Roshidi</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>26</td>
        <td style='text-align: center;'>901852</td>
        <td style='text-align: center;'>AHMAD SUBHI BIN ZOLKAFLY</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>27</td>
        <td style='text-align: center;'>901490</td>
        <td style='text-align: center;'>ATHIFAH NAJWANI BINTI SHAHIDAN</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>28</td>
        <td style='text-align: center;'>95980</td>
        <td style='text-align: center;'>Kogilavani A/P Apadore</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>29</td>
        <td style='text-align: center;'>95973</td>
        <td style='text-align: center;'>Mohd Zool Hilmie Bin Mohamed Sawal</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>30</td>
        <td style='text-align: center;'>901135</td>
        <td style='text-align: center;'>ZANARIAH BINTI IDRUS</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>31</td>
        <td style='text-align: center;'>901120</td>
        <td style='text-align: center;'>RAMESH S/O THANGAVELU</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>32</td>
        <td style='text-align: center;'>901089</td>
        <td style='text-align: center;'>Rosseriyany Bt Don</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>33</td>
        <td style='text-align: center;'>901086</td>
        <td style='text-align: center;'>Noor Hasnita binti Abdul Talib</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>34</td>
        <td style='text-align: center;'>901083</td>
        <td style='text-align: center;'>Jasmin Ilyani binti Ahmad</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>35</td>
        <td style='text-align: center;'>901069</td>
        <td style='text-align: center;'>MELISA A/P CHARLES BENEDICT</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>36</td>
        <td style='text-align: center;'>900952</td>
        <td style='text-align: center;'>Puteri Azwa binti Ahmad</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>37</td>
        <td style='text-align: center;'>900923</td>
        <td style='text-align: center;'>Zulrina Efriza Binti Zardi</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>38</td>
        <td style='text-align: center;'>900921</td>
        <td style='text-align: center;'>Faridzah binti Jamaluddin</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>39</td>
        <td style='text-align: center;'>900915</td>
        <td style='text-align: center;'>NORLILA BINTI MAHIDIN</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>40</td>
        <td style='text-align: center;'>900073</td>
        <td style='text-align: center;'>Braham Rahul Ram A/L Jamnadas</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>41</td>
        <td style='text-align: center;'>900870</td>
        <td style='text-align: center;'>Tham Poh Seng</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>42</td>
        <td style='text-align: center;'>900805</td>
        <td style='text-align: center;'>Sharifah Shuthairah bt Syed Abdullah</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>43</td>
        <td style='text-align: center;'>900800</td>
        <td style='text-align: center;'>LIM YAI FUNG</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>44</td>
        <td style='text-align: center;'>900773</td>
        <td style='text-align: center;'>Muhamad Hafizi Bin Hassan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>45</td>
        <td style='text-align: center;'>900771</td>
        <td style='text-align: center;'>Maryadi Bin Hasan</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>46</td>
        <td style='text-align: center;'>900765</td>
        <td style='text-align: center;'>AZNIRA BINTI ZAKARIA</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>47</td>
        <td style='text-align: center;'>900716</td>
        <td style='text-align: center;'>Kasim Bin Malik</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>48</td>
        <td style='text-align: center;'>900712</td>
        <td style='text-align: center;'>Mohd Firdaus Bin Zakaria</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>49</td>
        <td style='text-align: center;'>900710</td>
        <td style='text-align: center;'>Nur Ain Binti Saad</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>50</td>
        <td style='text-align: center;'>900576</td>
        <td style='text-align: center;'>SITI ZURAINI BT MAT SALLEH</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>51</td>
        <td style='text-align: center;'>900575</td>
        <td style='text-align: center;'>Muhammad Fakhirin bin Che Majid</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>52</td>
        <td style='text-align: center;'>900545</td>
        <td style='text-align: center;'>Khaimerol bin Omar</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>53</td>
        <td style='text-align: center;'>900544</td>
        <td style='text-align: center;'>Syarliza llyana Binti Sansul Bahri</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>54</td>
        <td style='text-align: center;'>900381</td>
        <td style='text-align: center;'>Ropizah binti Rohani</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>55</td>
        <td style='text-align: center;'>900274</td>
        <td style='text-align: center;'>NOORAIN OMAR</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>56</td>
        <td style='text-align: center;'>900538</td>
        <td style='text-align: center;'>SHELENA A/P SOOSAY NATHAN</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>57</td>
        <td style='text-align: center;'>900527</td>
        <td style='text-align: center;'>Fairil bin Hasbullah</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>58</td>
        <td style='text-align: center;'>900519</td>
        <td style='text-align: center;'>SITI NUR AISYAH BINTI ALIAS</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>59</td>
        <td style='text-align: center;'>900514</td>
        <td style='text-align: center;'>SITI NOOR AISHAH BINTI MOHD SIDIK</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>60</td>
        <td style='text-align: center;'>900515</td>
        <td style='text-align: center;'>ADILAH BINTI OTHMAN</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>61</td>
        <td style='text-align: center;'>900437</td>
        <td style='text-align: center;'>Azura Binti Mohd Noor</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>62</td>
        <td style='text-align: center;'>900059</td>
        <td style='text-align: center;'>Mohd Jaffry Bin Mohd</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>63</td>
        <td style='text-align: center;'>900562</td>
        <td style='text-align: center;'>Anusha A/P Aurasu</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>64</td>
        <td style='text-align: center;'>900711</td>
        <td style='text-align: center;'>Annuar Aswan B. Mohd Noor</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>65</td>
        <td style='text-align: center;'>901132</td>
        <td style='text-align: center;'>ADDY BIN MAT</td>
        <td style='text-align: center;'>255/A171</td>
        <td style='text-align: center;'>UUM</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
  </table>
  </font>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}