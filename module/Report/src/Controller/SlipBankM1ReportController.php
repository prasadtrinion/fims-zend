<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SlipBankM1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// slipBankM1Report M-1
    public function slipBankM1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();
            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        
        $file_data = "
    <html>
<head>
    <title>UUM</title>
</head>
<body>
    <form>
        <table align='center' width='100%'>
            <tr>
            <th style='text-align: center; width: 50%'>UNIVERSITI UTARA MALAYSIA</th>
            <td style='text-align: center;width: 10%'>TARIKH : 17/10/2018</td>
            </tr>
            <tr>
            <th style='text-align: center;width: 10%'>SISTEM IFAS</th>
            <td style='text-align: center;width: 5%'>MUKA SURAT :1</td>
            </tr><br>
            <tr>
            <th style='text-align: center; width: 20%'>SLIP BANK HARI BAGI TARIKH : 17/10/2018</th>
            </tr>
        </table>
        <hr>
        <table align='center' width='100%'> 
        <tr>
            <td style='text-align: center; width: 10%'><font size='3'>NO.SLIP BANK</font></td>
            <td style='text-align: center; width: 30%'><font size='3'>KETERANGAN</font></td>
            <td style='text-align: center; width: 20%'><font size='3'>NO.CEK </font></td>
            <td style='text-align: right; width: 40%'><font size='3'>AMAUN(RM)</font></td>
        </tr>
    </table>
<hr>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 10%'><font size='3'>S2486/18</font></td>
            <td style='text-align: center; width: 30%'><font size='3'>TUNAI</font></td>
            <td style='text-align: center; width: 20%'><font size='3'>0</font></td>
            <td style='text-align:  right; width: 40%'><font size='3'>5,164.20</font></td>
        </tr>
    </table><br><br>
        <table align='center' width='100%'>
        <hr width='200px' align='right'>
        <tr>    
        <td style='text-align: center; width: 5%'><font size='3'></font></td>
        <td style='text-align: center; width: 40%'><font size='3'></font></td>
        <td style='text-align: right; width: 25%'><font size='3'>JUMLHA</font></td>
        <td style='text-align:  right; width: 30%'><font size='3'> 5,164.20</font></td>
        </tr>
    </table>
    <hr width='200px' align='right'>
    <hr width='200px' align='right'>
    </form>
</body>
</html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}