<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ListofChangesDataB15ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function listofChangesDataB15ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
          <!DOCTYPE html>
<html>
<head>
  <title>B1-5</title>
</head>
<body>
<form>
  <table align='center' width='100%'>
    <tr>
      <td style='text-align: left; width: 30%'>KOD LAPORAN :
        <input type='text' name='kod' style='width: 10%; text-align: center;'>
        <br><br>
        TARHIK : <input type='date' name='tarhik' style='width: 10%; text-align: center;'>
      </td>
      <td style='text-align: center; width: 50%'>
      <strong>UNIVERSITI UTARA MALAYSIA</strong>
        <br><br>
      <strong>SENARAI PERUBAHAN DATA</strong>
        <br><br>
      <strong>UNTUK BULAN :</strong><input type='date' name='bulan' style='width: 10%; text-align: center;'>
      </td>
      <td style='text-align: left; width: 20%'>
        M/S :
        <br><br>
        MASA :
      </td>
    </tr>
  </table><br><br>
  <table  width='100%' align='center'>
    <tr>
      <td style='text-align: left; width: 25%'><input type='text' name='pindaoatan' placeholder='PINDAPATAN'></td>
    </tr>
    <tr>
      <td style='text-align: left; width: 5%'><b><input type='text' name='' placeholder='KOD:'></b></td>
      <td style='text-align: center; width: 10%'><input type='text' name='' placeholder='001'></td>
      <td style='text-align: center; width: 50%'><b><input type='text' name='' placeholder='Gaji Pokok'></b></td>
      <td style='text-align: center; width: 10%'><input type='text' name='' placeholder='1101'></b></td>
    </tr>
  </table>
  <br>
  <table  width='100%' align='center'>
    <thead>
    <tr>
      <th style='text-align: center;width: 5%;'>Bil</th>
      <th style='text-align: center;width: 10%;'>No.Staf</th>
      <th style='text-align: center;width: 30%;'>Nama Staf</th>
      <th style='text-align: center;right: 15%;'>Jumlah</th>
      <th style='text-align: center;width: 10%;'>Bulan Akhir</th>
      <th style='text-align: center;width: 30%;'>TARAF KHIDMAT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan='6'>
        <hr>
      </td>
    </tr>
    <tr>
      <td style='text-align: center;width: 5%;'>Bil</td>
      <td style='text-align: center;width: 10%;'>No.Staf</td>
      <td style='text-align: center;width: 30%;'>Nama Staf</td>
      <td style='text-align: right;width: 15%;'>Jumlah</td>
      <td style='text-align: center;width: 10%;'>Bulan Akhir</td>
      <td style='text-align: center;width: 30%;'>TARAF KHIDMAT</td>
    </tr>
  </tbody>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}