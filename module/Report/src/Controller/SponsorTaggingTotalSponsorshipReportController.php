<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SponsorTaggingTotalSponsorshipReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Sponsor List Report A-5
    public function sponsorTaggingTotalSponsorshipReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	// $journalId = $postData['journalId'];

        	// $reportRepository = $this->getRepository('T019fsavedReports');
        	// $journalData = $reportRepository->getJournalEntryById((int)$journalId);

        	// // print_r($journalData);exit;
         //  $JournalDescription = $journalData[0]['f017fdescription'];
         //  $Approver = $journalData[0]['originalName'];
          $pay_date = date('d/m/Y h:i:s a', time());
        $pay_date   = date("d/m/Y", strtotime($pay_date));


        	$journal_data = "
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: right' width='100%' >$pay_date</td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b>UNIVERSITI UTARA MALAYSIA<br>Sistem Akuan Pelajar<br><br>SENARAI PELAJAR TAJAAN SISWAZAH<br>pada Tarikh : $pay_date</b><font></td>
        </tr>
    </table>
    <br>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: left' width='10%' valign='top'><font size='2'>PENAJA
          <font></td>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>:<font></td>
          <td style='text-align: left' width='85%' valign='top'><font size='2'>NAMA PENAJA</td>
        </tr>
       <tr>
          <td style='text-align: left' width='10%' valign='top'><font size='2'>SEM
          <font></td>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>:<font></td>
          <td style='text-align: left' width='85%' valign='top'><font size='2'>A151</td>
        </tr>
      </table>
      <br>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>BIL<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>MATRIK<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>NO KP<font></td>
          <td style='text-align: center' width='30%' valign='top'><font size='2'>NAMA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Kod Prog.<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Th. Mula<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Th. Akhir<font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Tajaan (RM)<font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top' colspan='8'><font size='2'><hr><font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>BIL<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>MATRIK<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>NO KP<font></td>
          <td style='text-align: center' width='30%' valign='top'><font size='2'>NAMA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Kod Prog.<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Th. Mula<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>Th. Akhir<font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Tajaan (RM)<font></td>
        </tr>
    </table>";
            }
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
        }
}