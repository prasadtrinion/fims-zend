<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class AccountCodeReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }


    public function accountCodeReport1Action()
    {
        // print_r("fss");exit;

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);


        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

        $file_data = "
       <table align='center' style='border:1px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: center'  width='35%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='45%' valign='top'><font size='2'>A/C<br>Level0</font></td>
        			<td style='text-align: center'  width='30%' valign='top'><font size='2'>Nature</font></td>
        			<td style='text-align: center'  width='25%' valign='top'><font size='2'>Blc<br>type</font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>A/C<br>Level1</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>description</font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>A/C<br>Level2</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>description</font></td>
        		</tr>
        		</table>
        	</td>
        </tr>
        <tr>
        <td colspan='3'><hr></td>
        </tr>
        <tr>
        	<td style='text-align: center'  width='35%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='45%' valign='top'><font size='2'>100000<br>SHARE CAPITAL</font></td>
        			<td style='text-align: center'  width='30%' valign='top'><font size='2'>EQUITY</font></td>
        			<td style='text-align: center'  width='25%' valign='top'><font size='2'>CREDIT</font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>110000</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>SHARE EQUITY</font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>110100</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>SHARE EQUITY1</font></td>
        		</tr>
        		</table>
        	</td>
        </tr>
        </tr>
                <tr>
        	<td style='text-align: center'  width='35%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='45%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='30%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='25%' valign='top'><font size='2'></font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'></font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>110200</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>Equity2</font></td>
        		</tr>
        		</table>
        	</td>
        </tr>
        <tr>
        	<td style='text-align: center'  width='35%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='45%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='30%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='25%' valign='top'><font size='2'></font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>120000</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>GRANT</font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>120100</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>Grant1</font></td>
        		</tr>
        		</table>
        	</td>
        </tr>
                <tr>
        	<td style='text-align: center'  width='35%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='45%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='30%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='25%' valign='top'><font size='2'></font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'></font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'></font></td>
        		</tr>
        		</table>
        	</td>
        	<td style='text-align: center'  width='30%' valign='top'>
        		<table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        		<tr>
        			<td style='text-align: left'  width='40%' valign='top'><font size='2'>120200</font></td>
        			<td style='text-align: center'  width='60%' valign='top'><font size='2'>Grant2</font></td>
        		</tr>
        		</table>
        	</td>
        </tr>

        </table>
                  ";
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}