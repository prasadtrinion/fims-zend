<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentAffairsDepartmentG1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function studentAffairsDepartmentG1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
          <!DOCTYPE html>
<html>
<head>
  <title>G1</title>
</head>
<body>
<form>
  <table width='100%' align='center'>
    <tr>
      <td style='width: 10%;text-align: left;'><img src='' style='width: 120px;height: 100px'></td>
      <td style='width: 60%;text-align: left;'>
        <strong>
          JABATAN HAL PELAJAR<br>
          STUDENT AFFAIRS DEPARTMENT<br>
        </strong>
          Universiti Utara Malaysia<br>
          06010 UUM SINTOK <br>
          KEDAH DARUL AMAN<br>
          MALAYSIA
      </td>
      <td style='width: 30%;text-align: left;'>
        <img src='' style='width: 120px;height: 100px'><br>
        Tel:(604) 928 7777<br>
        Farks:(604) 928 4141<br>
        E-Mail:uassit@uum.edu.my
      </td>
    </tr>
    <tr>
      <td colspan='3'><hr></td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: center;'><B>'MUAFAKAT KEDAH'</B></td>
    </tr>   
    <tr>
      <td style='width: 100%;text-align: right;'><B>UUM/HEP/U-ASSIST20161215-KKB9759<br>15 DECEMBER 2016</B></td>
    </tr>
    <tr>
      <td style='width: 100%;text-align: left;'>
        Cultural Attache<br>
        Embassy of The Republic of Yemen<br>
        No 73 Lorange Lai Tet Loke<br>
        Off Jalan Semarak<br>
        54100 Kual Lumpur<br><br>
        Dear Sir<br><br>
        <b>CLAIM TUITION FEES FOR THE YEMENI STUDENT FOR SEESION 2016/2017</b><br><br>
        With reference to the above matter, the total fees charged for your sponsored student is
        <b>USD 96,000.00</b> as per attached Invoice.<br><br>
        As such, please make payment to our account: Universiti Utara Malaysia at Bank Islam
        MalaysiaBerhad, Universiti Utara Malaysia Branch, 06010 Sintok, Kedah
        (Account No. 020-9301-00000-10), Swift Code: BIMBMYKL, Bank Telephone No
        604-9286766 and send the payment advice for our reference to nazila@uum.edu.my/
        scholarship@uum.edu.my<br><br><br></td>
    </tr>
      <tr>
        <td style='width: 100%;text-align: left;'>
          Thank You.<br><br>
          <br><br>
          <br><br>
        </td>       
      </tr>
        <tr>
          <td style='width: 100%;text-align: left;'>
            Yours Sincerely,<br><br><br><br>
          </td>
        </tr>
  </table>
      <table width='100%'>
        <tr>
          <td style='width: 50%;text-align: left;'>
            <b>(NOOR NAZILA NAZREN)</b><br>
          Assistant Bursar<br>
          For Director U-Assist<br>
          Universiti Utara Malaysia
          </td>
          <td style='width: 50%;text-align: left;'>
            <b>BURSAR<br></b>
            <b>BURSAR DEPARTMENT<br></b>
            UNIVERSITI UTARA MALAYSIA<br>
            06010 Sintok, Kedah, Malaysia.<br>
            Tel: 604-9283121<br>
          </td>
        </tr>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}