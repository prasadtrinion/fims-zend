<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReceiptsA26ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// receiptsA26Report A-26
    public function receiptsA26ReporttAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        
        $file_data = $file_data . "
   <html>
<head>
    <title>A26</title>
</head>
<body>
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: left;' width='5%'><font size='3'>INSTITUSI</font></td>
          <td style='text-align: left;' width='5%'><font size='3'>:</font></td>
          <td style='text-align: left;' width='70%'><font size='3'>UNIVERSITI UTARA MALAYSIA</font></td>
          <td style='text-align: left;' width='5%'><font size='3'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='5%'><font size='3'>BULAN</font></td>
          <td style='text-align: left;' width='5%'><font size='3'>:</font></td>
          <td style='text-align: left;' width='5%'><font size='3'></font></td>
        </tr>
    </table>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: left;' width='5%' ><font size='3'></font></td>
            <td style='text-align: left;' width='5%'  ><font size='3'></font></td>
            <td style='text-align: center;'width='50%'> LAPORAN PENYATA TERIMAAN DAN BAYARAN (PTB) YURAN & TIKET PENERBANGAN PROGRAM SLAB & SKPD PADA 31 DISEMBER 2015</td>
        </tr>
    </table>
    <table border='1px' align='center' width='100%' style='border-collapse: collapse;'>
        <tr>
            <td colspan='4' align='center'>TERIMAAN</td>
            <td colspan='4' align='center'>BAYARAN</td>
        </tr>
        <tr>
            <td style='text-align: center;' width='5%'>TARIKHA</td> 
            <td style='text-align: center;' width='20%'>PERKARA</td>
            <td style='text-align: center;' width='10%'>RUJAN</td>
            <td style='text-align: center;' width='10%'>RM</td>
            <td style='text-align: center;' width='10%'>TARIKHA</td> 
            <td style='text-align: center;' width='20%'>PERKARA</td>
            <td style='text-align: center;' width='10%'>RUJAN</td>
            <td style='text-align: center;' width='15%'>RM</td>
        </tr>
        <tr>
            <td style='text-align: center;' width='5%'>16.72015</td> 
            <td style='text-align: center;' width='20%'>PERUNTUKAN DARBI BAHANGIAN BIASISWAT KPT</td>
            <td style='text-align: center;' width='10%'>EF1125220253359</td>
            <td style='text-align: center;' width='10%'>130,600.00</td>
            <td style='text-align: center;' width='10%'>15/09/2015</td> 
            <td style='text-align: center;' width='20%'>YARAN SLAB (FEB 2015-A142)SEMII 2014/2015</td>
            <td style='text-align: center;' width='10%'>LAMPIRAN JURNAL J3056/15</td>
            <td style='text-align: center;' width='15%'>48,634.00</td>
        </tr>
        <tr>
            <td style='text-align: center;' width='5%'>JUMHAL TERIMAAN</td> 
            <td style='text-align: center;' width='20%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td colspan='3' width='10'>JUMLAH BAYARANI(ii)</td> 
            <td style='text-align: center;' width='20%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='15%'></td>
        </tr>
        
        <tr>
            <td style='text-align: center;' width='5%'></td> 
            <td style='text-align: center;' width='20%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td colspan='3' width='10'>BAKI PERUNTUKAN PADA 31.12.2015[(iii)=(i)-(ii)]</td> 
            <td style='text-align: right;' width='20%'></td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='15%'></td>
        </tr>
        <tr>
            <td style='text-align: center;' width='5%'></td> 
            <td style='text-align: center;' width='20%'>JUMLHA</td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='10%'>130,600.00</td>
            <td style='text-align: center;' width='10%'>15/09/2015</td> 
            <td style='text-align: center;' width='20%'>JUMLHA</td>
            <td style='text-align: center;' width='10%'></td>
            <td style='text-align: center;' width='15%'>48,634.00</td>
        </tr>
        </table>
        <table>
            <tr>
                <td>*LAMPIRAN adalah makulmat lengkap bayaran sepreti Borang SPBB3</td>
            </tr><br><br>
            <tr>
                <td>i) Disahkan makulumat diatas adalah benar dan bayaran telah dibuat sweajararanya.</td>
            </tr>
            <tr>
                <td>ii) Disahkan Telah menarimas  peruntukan RM 130,600.00 dan wang tersebut telah direkodkan akuaun kontra No.Rujukan 3006008229</td>
            </tr>
            <tr>
                <td>iii) Disahkan sehingga 31/12/2015 peruntakan telah berbaki RM 36,216.00</td>
            </tr>
        </table><br><br>
         <table align='center' width='100%'>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Disediakan</font></td>
          <td style='text-align: left;' width='5%'  ><font size='3'>:</font></td>
          <td style='text-align: left;' width='20%'  ><font size='3'>MarsuzilaHarun</font></td>
          <td style='text-align: center;' width='20%'  ><font size='3'></font></td>

          <td style='text-align: right;' width='10%'  ><font size='3'>Disemakoleh</font></td>
          <td style='text-align: center;' width='5%'  ><font size='3'>:</font></td>
          <td style='text-align: right;' width='20%'  ><font size='3'>Noor nazila Nazren</font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Cop & tandatangan</font></td>
          <td style='text-align: left;' width='5%'  ><font size='3'></font></td>
          <td style='text-align: left;' width='20%'  ><font size='3'>Student Liaison officer(SLO) U-asst</font></td>
          <td style='text-align: center;' width='20%'  ><font size='3'></font></td>

          <td style='text-align: right;' width='10%'  ><font size='3'>Cop & tandatangan</font></td>
          <td style='text-align: center;' width='5%'  ><font size='3'></font></td>
          <td style='text-align: right;' width='20%'  ><font size='3'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Tarikha</font></td>
          <td style='text-align: left;' width='5%'  ><font size='3'></font></td>
          <td style='text-align: left;' width='20%'  ><font size='3'></font></td>
          <td style='text-align: center;' width='20%'  ><font size='3'></font></td>

          <td style='text-align: right;' width='10%'  ><font size='3'>Tarikha</font></td>
          <td style='text-align: center;' width='5%'  ><font size='3'></font></td>
          <td style='text-align: right;' width='20%'  ><font size='3'></font></td>
        </tr>
        </table>
 </body>
</html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}