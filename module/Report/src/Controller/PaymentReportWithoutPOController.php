<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PaymentReportWithoutPOController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report6
    public function paymentReportWithoutPOAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'>UNIVERSITI UTARA MALAYSIA<br> MODUL BAYARAN<br>LAPORAN BAYARAN MELEBIHI AMAUN RM 2500 TANPA PO</td>
            </tr>
            <tr>
             <td style='text-align: center;' width='100%'><font size='3'>DARI : $pay_date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HINGGA : $pay_date</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='80%'><font size='3'>LAPORAN: </font></td>
             <td style='text-align: left' width='20%'><font size='3'>TARIKH: $pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='80%'><font size='3'>MASA: $pay_time</font></td>
             <td style='text-align: left' width='20%'><font size='3'></font></td>
            </tr>
            </table>

          <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='100%'><font size='3'>PTJ : 0100 - CANSOLORI (CANS) </font></td>
             </tr>
          </table>
          <br>

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='3'>Bil</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='3'>NO. BAUCER</font></td>
           <td style='text-align:center' width='25%' valign='top'><font size='3'>NAMA PENERIMA</font></td>
           <td style='text-align:center' width='40%' valign='top'><font size='3'>TUJUAN BAYARAN</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='3'>AMAUN(RM)</font></td>
           </tr>
           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='3'><hr></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='3'><hr></font></td>
           <td style='text-align:center' width='25%' valign='top'><font size='3'><hr></font></td>
           <td style='text-align:center' width='40%' valign='top'><font size='3'><hr></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='3'><hr></font></td>
           </tr>
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='3'>1.</font></td>
           <td style='text-align:left' width='15%' valign='top'><font size='3'>B7725/18</font></td>
           <td style='text-align:left' width='25%' valign='top'><font size='3'>DELTA CHENANG RESORT SDN BHD</font></td>
           <td style='text-align:left' width='40%' valign='top'><font size='3'>BAYARAN PAKEJ BANK MUAMALAT LEADERSHIP CAMP MUAMALAT-A171-4,PADA 1-3/03/2018,</font></td>
           <td style='text-align:right' width='15%' valign='top'><font size='3'>13,298.00</font></td>
           </tr>
          <tr>
          <td style='text-align:right' width='25%' valign='top' colspan='4'><font size='3'>JUMLAH BESAP:</font></td>
          <td style='text-align:center' width='25%' valign='top'><font size='3'> 13,298.00</font></td>
          </tr>

        </table> 
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}