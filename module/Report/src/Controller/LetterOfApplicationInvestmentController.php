<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class LetterOfApplicationInvestmentController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Letter Of Application Investment
    public function letterOfApplicationInvestmentAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);


        if($this->isJSON($rawBody))
        {
            // print_r($postData);exit;
        $applicationId = $postData['applicationId'];
        $reportRepository = $this->getRepository('T019fsavedReports');

        $fdSecurity = $reportRepository->getFixedSecurityAccounts($applicationId);
         $uumName = $reportRepository->getuumBank($applicationId);
         // print_r($fdSecurity);exit;

         $uum = $uumName['f042fbank_name'];
         $address = $uumName['f041faddress'];
         $uumBranch = $uumName['f041fbranch'];
         $accountNumber = $uumName['f041faccount_number'];


            foreach ($fdSecurity as $details)
            {
                $reference_no = $details['f063fapplicationId'];
                $id_application = $details['f064fid_application'];
                $bank_name = $details['f042fbankName'];
                $branch_name = $details['f042fbranchName'];
                $sum = $details['f063famount'];
                $account_code = $details['f064faccount_code'];
                $effective_date = $details['f064feffective_date'];
                $maturity_date = $details['f064fmaturity_date'];
                $applicationReference = $details['applicationReference'];
                $institutionCode = $details['f061finstitutionCode'];
                $institutionName = $details['f061fname'];
                $institutionAddress = $details['f061faddress'];
                $institutionContactNo = $details['f061fcontactNo'];
                $institutionContactPerson = $details['f061fcontactPerson'];
                $investmentType = $details['investmentType'];
                $rateOfInterest = $details['f064frateOfInterest'];
                $interstToBank = $details['f064finterstToBank'];
                $periodType = $details['f063fdurationType'];
                $durationType = $details['f063fduration'];
                $contactPerson = $details['f064fcontactPerson'];
                $contactPerson2 = $details['f064fcontactPerson2'];
                $contactEmail = $details['f064fcontactEmail'];
                $contactEmail2 = $details['f064fcontactEmail2'];






                $rateOfInterest = number_format($rateOfInterest, 2, '.', ',');

                $effective_date   = date("m/d/Y", strtotime($effective_date));
                $maturity_date   = date("m/d/Y", strtotime($maturity_date));

                $diff = $reportRepository->diffMonth($effective_date, $maturity_date);
                // print_r($diff);exit;
                 // print_r($maturity_date);exit;


            $print_date = date("m/d/Y h:i:s a", time());    
                  
            $pay_month   = date("m", strtotime($print_date));
            $pay_date   = date("d", strtotime($print_date));
            $pay_year   = date("Y", strtotime($print_date));
            

           
            $months = array (1=>'Januari',2=>'Februari',3=>'Mac',4=>'April',5=>'Mei',6=>'Jun',7=>'Julai',8=>'Ogos',9=>'September',10=>'Oktober',11=>'November',12=>'Disember');
            $pay_month = $months[(int)$pay_month];
            // print_r($pay_month);exit;

        

            // $limit = 5;
            // $page_count = ceil($sum/$limit);
            

            // $extra_amount = $sum%$limit;

        // print_r($address);exit;
        $fd_data = '';
            while($sum > 0)
            {
                if($sum > 5000000)
                {
                    $greater_5 =  "5 keping";
                    $sum = $sum -5000000;
                    $limit = '5.0';
                    $juta = 'JUTA';
                }
                else
                {
                    $greater_5 =  " ";
                    $limit = $sum.'.0';
                    $sum = 0;
                    $juta = '';
                    $limit = number_format($limit, 2, '.', ',');

                }
                        $fd_data = $fd_data."
    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
         <tr>
            <td style='text-align: center' width='100%' valign='top'><font size=4><b>UTARA UNIVERSITI MALAYSIA</b></font></td>
        </tr>
    </table>
    <br>



    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: right' width='100%' valign='top'>$reference_no</td>
        </tr>
        <tr>
          <td style='text-align: right' width='100%' valign='top'>$pay_date $pay_month $pay_year</td>
        </tr>
        <br><br>
        <tr>
         <td style='text-align: left' width='100%' valign='top'>$institutionName<br>$institutionCode<br>$institutionAddress<br>UP: $institutionContactPerson<br>$institutionContactNo<br><br> Taun/Puan</td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td width='100%' valign='top'><b>PELABURAN SEMENTARA KE AKAUN $investmentType UUM SEBANYAK RM$limit $juta</b></td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Adalah dengan hormatnya dimaklumkan bahawa pihak Universiti telah mengarahkan $uum , $address supaya memindahkan wang berjumlah <b>RM$limit $juta</b> ke akaun simpanan tetap UUM di bank cawangan tuan/puan</td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Surat kami kepada BIMB Cawangan Universiti Utara Malaysia No. Rujukan:<b>$reference_no</b> bertarikh $pay_date $pay_month $pay_year adalah dirujuk. Sila keluarkan sijil/resit simpanan tetap atas nama Universiti Utara Malaysia dalam <b>$greater_5 berjumlah $limit $juta setiap satu bagi tempoh $durationType $periodType dengan kadar $rateOfInterest%.</b></td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Apabila tiba tarikh matang sijl barkanaan, sila keluarkanamaun kauntungan dan dipindahkan ke akaun bank <b>$uum</b> seperti berikut</td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: right'  width='16%' valign='top'></td>
        	<td style='text-align: left'  width='20%' valign='top'>Nama Akaun</td>
        	<td style='text-align: centre'  width='4%' valign='top'>:</td>
        	<td style='text-align: left'  width='60%' valign='top'>$uum</td>
        </tr>
         <tr>
         	<td style='text-align: right'  width='16%' valign='top'></td>
        	<td style='text-align: left'  width='20%' valign='top'>No Akaun</td>
        	<td style='text-align: centre'  width='4%' valign='top'>:</td>
        	<td style='text-align: left'  width='60%' valign='top'>$accountNumber</td>
        </tr>
         <tr>
         	<td style='text-align: right'  width='16%' valign='top'></td>
        	<td style='text-align: left'  width='20%' valign='top'>Nama Bank</td>
        	<td style='text-align: centre'  width='4%' valign='top'>:</td>
        	<td style='text-align: left'  width='60%' valign='top'>$bank_name, $branch_name</td>
        </tr>
        <tr>
        	<td style='text-align: right'  width='16%' valign='top'></td>
        	<td style='text-align: left'  width='20%' valign='top'>Alamat Bank</td>
        	<td style='text-align: centre'  width='4%' valign='top'><b>:</b></td>
        	<td style='text-align: left'  width='60%' valign='top'>$address</td>
        </tr>
    </table>
    <br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td width='100%' valign='top'>Bagi <b>amaun pokok</b> sijil pelaburan berkenaan, sekiranya pihak Universiti Utara Malaysia tidak mengemukakan surat pengeluaran pelaburan tersebut, sila perbaharui mengikut pelaburan asal <b>(Islamic)</b> dengan <b>tempoh yang sama dan kadar yang tidak kurang dari pelaburan asal</b>. Sekiranya kadar yang ditawarkan adalah lebih rendah dari kadar pelaburan asal, mohon pihak tuan/puan dapatkan persetujuan pihak Bendahari UUM terlebih dahulu.</td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Mohon juga pihak tuan/puan megemukakan surat makluman bagi setiap kali pembayaran Keuntungan danialau pembaharun pelaburan kepad ndahar UUM di alamat Jaba Bendahari, Universiti Utara Malaysia, 06010 Sintok, Kedah</b></td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Sebarang pertanyaan sila hubungi pegawai kami, $contactPerson (<u>$contactEmail</u>) di talian 04-9283212/091-5703929 atau $contactPerson2 (<u>$contactEmail2</u>) di talian 04-9283234 </td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left'  font-size='20' width='100%' valign='top'> Sekian, terima kasih</td>
        </tr>
    </table>
    <br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='100%' valign='top'><b>'KEDAH AMAN MAKMUR-HARAPAN BERSAMA MAKMURKAN KEDAH'<br>'ILMU BUDI BAKTI'</b></td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='100%' valign='top'>Saya yang menjalankan amanah,</td>
        </tr>
    </table>
    <br><br><br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='60%' valign='top'><b>(PROF. DR. AHMAD BASHAWİR BIN <br>ABDUL GHANI)</b><br>Naib Canselor<br>Universiti Utara Malaysia</td>
        	<td style='text-align: left' width='40%' valign='top'><b>(HAJI AMRON MAN)</b><br>Bendahari<br>Universiti Utara Malaysia</td>
        </tr>
    </table>
            <pagebreak>
    ";
    
            }
            
        }
    }

    $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($fd_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}