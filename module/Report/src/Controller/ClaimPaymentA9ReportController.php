<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimPaymentA9ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function claimPaymentA9ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <table align='center' width='100%'>
  <tr>
    <th>
      JABATAN BENDAHARI
    </th>
  </tr>
  <tr>
    <th>
      UNIVERSITI UTARA MALAYSIA
    </th>
  </tr>
  <tr>
    <td width='50%'>
      <p><font size='4px'>06010 UUM, Sintok,Kedah Darul Aman, Malaysla. Tel:04-9287777 / 04-9283135 <br>
      Fax:04-9285764(Bendahari) / 04-9287905 (U-Assist) E-mail:<a href='#'>scholarship@uum.edu.my</a></font></p>
    </td>
  </tr>
  </table>
  <table width='100%' style='text-align:center'>
    <tr>
    <td width='50%'>
      <h3>TUNTUTAN BAYARAN <i>(Invoice)</i></h3>
    </td>
  </tr>
  </table>
  <br>
  <table width='100%'>
     <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>NAMA:<br>(Name)<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='5'><font></td>
          <td style='text-align: center' width='40%' valign='top'><font size='5'>INVOICE NO:<br>(Invoice No.)<font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>NO.MATRIK:<br>(Matric no)<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='5'><font></td>
          <td style='text-align: center' width='40%' valign='top'><font size='5'>TARIKH:<br>(Date)<font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>NO.K.P./NO.PASSPORT:<br>(I.C.No/Passport No.)<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='5'><font></td>
          <td style='text-align: center' width='40%' valign='top'><font size='5'>RUJUKAN KAMI:<br>(Our Ref.)<font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>TEMPOH PENGAJIAN:<br>(Poriod Of Study)<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='5'><font></td>
          <td style='text-align: center' width='40%' valign='top'><font size='5'>RUJUKAN LAIN:<br>(Other Ref.)<font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>TEMPOH TAJAAN:<br>(Period Of Sponsorship)<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='5'><font></td>
          <td style='text-align: center' width='40%' valign='top'><font size='5'>BAKI TAJAAN (RM):<br>(Balance)<font></td>
        </tr>
  </table>
  <p><hr style='height: 2px; background-color: black'></p>
  <table width='100%'>
    <tr>
      <th style='text-align: left;' width='5%'></th>
      <th style='text-align: left;' width='15%'><font size='5px'>KETERANGAN<i>(Description)</i></font></th>
      <th style='text-align: center;' width='40%'><font size='5px'>AMAUN<i>(Amount)(RM)</i></font></th>
    </tr>
    <tr></tr>
    <tr>
      <td style='text-align: left;' width='5%'><font size='5%'>1.</td>
      <td style='text-align: left;' width='15%'><font size='5%'>Kebajikan (Welfare)</td>
      <td style='text-align: center;' width='40%'><font size='5%'>150.00</td>
    </tr>
    <tr>
      <td style='text-align: left;' width='5%'><font size='5%'>2.</td>
      <td style='text-align: left;' width='15%'><font size='5%'>Pengajian (Tuition)</td>
      <td style='text-align: center;' width='40%'><font size='5%'>2,405.00</td>
    </tr>
    <tr>
      <td style='text-align: left;' width='5%'><font size='5%'>3.</td>
      <td style='text-align: left;' width='15%'><font size='5%'>Perpustakaan (Library)</td>
      <td style='text-align: center;' width='40%'><font size='5%'>100.00</td>
    </tr>
    <tr>
      <td style='text-align: left;' width='5%'><font size='5%'>4.</td>
      <td style='text-align: left;' width='15%'><font size='5%'>Sukan (Sport)</td>
      <td style='text-align: center;' width='40%'><font size='5%'>50.00</td>
    </tr>
    <tr>
      <td style='text-align: left;' width='5%'><font size='5%'>5.</td>
      <td style='text-align: left;' width='15%'><font size='5%'>Komputer (Computer)</font></td>
      <td style='text-align: center;' width='40%'><font size='5%'>150.00</td>
    </tr>
    <tr></tr>
    <tr>
      <td></td>
      <td style='width: 40%;text-align:left;'><font size='5px'><b>Jumlah <i>(Total)</i></b></font></td>
      <td style='width: 40%;' align='center'><font size='5%'><b>2,855.00</b></td>
    </tr>
  </table>
  <p><b><font size='5px'>Ringgit Malaysia :<br>
  DUA RIBU LAPAN RATUS LIMA PULUH LIMA DAN SEN O SAHAJA</font></b></p>
  <br>
  <hr style='height: 2px;background-color: black;'>
  <font size='4px'>
    
    <ol>
      <li>
        Bayaran menggunakan cek hendaklah dipalang dan dibayar kepada <b>Universiti Utara Malaysia.</b><br>
          <i>(Payment by cheque must be crossed and made payable to<b> Universiti Utara Malaysia)</b></i>
      </li>
      <li>
        Sila maklumkan ke Jabatan Bendahari (seperti alamat di atas) sekiranya bayaran dibuat secara pindahan elektronik (EFT).<br>
          <i>(Please send Payment Advice to the above address if payment is made by electronic fund transer-EFT)</i>
      </li>
      <li>
        Nama Bank / No. akaun adalah seperti berikut: Bank Islam Malaysia Berhad Cawangan UUM, Account No.02093010000010. <br>
        swift code-BIMBMYKL
      </li>
      <br><br>
      <table style='text-align:center' width='100%'>
      <tr>
      <td>
          <i>Inoice ini adalah cetakan komputer dan tidak memerlukan tandatangan<br> 
         This invoice is Computer generated and no signature is required</i>
      </td>
    </tr>
    </table>
    </ol>
  </font>

</body>
</html>";

      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}