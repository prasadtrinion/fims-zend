<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SalarySlipController extends AbstractAppController
{
    protected $sm;

       public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }


    public function salarySlipAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {
          $file_data = $file_data . "";
       
            

          $employeeid = $postData['employeeid'];
          $month = $postData['paymonth'];
       
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getSalarySlipData($employeeid,$month);
        
    

      //  foreach ($result as $payee)
      //     {
      //       print_r($payee);
      // exit();

        $pay_date = date('m/d/Y h:i:s a', time());          

            $payslip_employee_name = $result[0]['f034fname'];
            $payslip_designation = $result[0]['f034fdesignation'];
            $payslip_name = $result[0]['f081fname'];
            $payslip_amount   = $result[0]['MPD_PAID_AMT'];
            $payslip_type_of_compomemt   = $result[0]['f081ftype_of_component'];
            $financial_period   = $result[0]['MPD_PAY_MONTH'];
            // $financial_year   = $result[0]['f015fname'];
            $department_name   = $result[0]['MP_DEPT_DESC'];
 
            
            
             $pay_date   = date("d/m/Y", strtotime($pay_date));
         

        $file_data = $file_data . "


  <table align='center' width='100%'>
      <tr>
         <td align='left' width='10%'><img src='' height='50' width='50' ></td>
         <td style='text-align: left'  font-size='13' width='90%' valign='top'><b>UNIVERSITI UTARA MALAYSIA<br>Sintok,Kedah Darul Aman,Malaysia</b></td>
      </tr>
  </table>
         <br><br>

  <table align='center' width='100%'>
      <tr>
        <td style='text-align: left' width='50%'>Pay Advice</td>
        <td style='text-align: right' width='50%'>Private & Confidential</td>
      </tr>
  </table>

    <br><br><br>

  <table align='center' width='100%'>
       <tr>
            <td style='text-align: left' width='15%'>Name:</td>
            <td style='text-align: left' width='55%'>$payslip_employee_name</td>
            <td style='text-align: left' width='20%'>Pay Advice For:</td>
            <td style='text-align: left' width='10%'>$invoice_date</td>
        </tr>
        <tr>
            <td style='text-align: left' width='15%'>Department:</td>
            <td style='text-align: left' width='55%'>$department_name</td>
            <td style='text-align: left' width='20%'>Pay Date:</td>
            <td style='text-align: left' width='10%'>$pay_date</td>
        </tr>
        <tr>
            <td style='text-align: left'  width='15%'>Position:</td>
            <td style='text-align: left'  width='55%'>$payslip_designation</td>
            <td style='text-align: left'  width='20%'></td>
            <td style='text-align: left'  width='10%'></td>
        </tr>
  </table>
         <br><br>      
        
  <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
    <tr>    
      <td style='text-align:center' colspan='2'><b> EARNINGS</b></td>
      <td style='text-align:center' colspan='2'><b> DEDUCTION</b></td>
    </tr>
    <tr>    
      <td colspan='4'> <hr/></td>
    </tr>
    <tr>
      <td colspan='2' valign='top'>
          <table width='100%' valign='top'>";
       //                       print_r($payslip_amount);
       // exit();

        foreach ($result as $row)
        {
          // print_r($row);
          // exit();
                        
            $payslip_name = $row['f081fname'];
            $payslip_amount   = $row['MPD_PAID_AMT'];
            $original_amount   = $row['MPD_PAID_AMT'];

            $payslip_type_of_compomemt   = $row['f081ftype_of_component'];
            $payslip_amount = number_format($payslip_amount, 2, '.', ',');
            if ($payslip_type_of_compomemt == '1')
            {

            $file_data  = $file_data . " 
                <tr>
                  <td style='text-align: left' width='50%'> $payslip_name :</td>
                  <td style='text-align: right' width='50%'>$payslip_amount </td>
                </tr>
               ";


               $earning_total  =   $earning_total + $original_amount;

            }     
        }

        $file_data  = $file_data . "
              </table>
          </td>
          <td colspan='2'  valign='top'> 
              <table width='100%'>";

        foreach ($result as $row)
         {
                        
           $payslip_name = $row['f081fname'];
            $payslip_amount   = $row['MPD_PAID_AMT'];
            $original_amount   = $row['MPD_PAID_AMT'];

            $payslip_type_of_compomemt   = $row['f081ftype_of_component'];
            $payslip_amount = number_format($payslip_amount, 2, '.', ',');

            if ($payslip_type_of_compomemt == '2')
            {
            
                 $file_data  = $file_data . " 
                <tr>
                  <td style='text-align: left' width='50%'> $payslip_name :</td>
                  <td style='text-align: right' width='50%'>$payslip_amount </td>
                </tr>
               ";
               $deduction_total  =  $deduction_total + $original_amount;
            }
        }


              $file_data  = $file_data . "
                    </table>
                </td>

              </tr>";

        $net_pay = $earning_total - $deduction_total;

        $earning_total = number_format($earning_total, 2, '.', ',');  
         $deduction_total = number_format($deduction_total, 2, '.', ',');
        


    $file_data = $file_data . " 
        <tr >    
                 <td colspan='4'> <hr></td>
        </tr>

         <tr>
            <td style='text-align: left' width='25%'> Total Earnings :</td>
            <td style='text-align: right' width='25%'> $earning_total </td>
            <td style='text-align: left' width='25%'> Total Deduction:</td>
            <td style='text-align: right' width='25%'> $deduction_total</td>
        </tr>
      </table>
        <br>";

        $file_data = $file_data . "
      <table width='100%'>
        <tr>
          <td colspan='2' valign='top'>
              <table width='100%' valign='top'>";

         foreach ($result as $row)
         {
//           print_r($row);
// exit;
                        
           $payslip_name = $row['f081fname'];
            $payslip_amount   = $row['MPD_PAID_AMT'];
            $original_amount   = $row['MPD_PAID_AMT'];
            $Contribution   = $row['f081fshort_name'];

            $payslip_type_of_compomemt   = $row['f081ftype_of_component'];
            $payslip_amount = number_format($payslip_amount, 2, '.', ',');
            if ($payslip_type_of_compomemt == '3')
            {



        $file_data = $file_data . "
              <tr>
                <td style='text-align: left' width='25%'> $payslip_name :</td>
                <td style='text-align: left' width='25%'>$payslip_amount </td>
              </tr>";
              }

            }

        $net_pay = number_format($net_pay, 2, '.', ',');

        $file_data = $file_data . "
            </table>
          </td>
          <td valign='top' style='text-align: right'>Net Pay:</td>
          <td valign='top' style='text-align: right'>$net_pay</td>
        </tr>
        </table>
        <hr>";
        
     

        $file_data = $file_data . "
        <table align='center' width='100%'>
          <tr>
            <td style='text-align: left'>Note:</td>
          </tr>
        </table>
        
        <br>
        
        <table align='center' width='100%'>
          <tr>
            <td style='text-align: left'>Informaion provided here is personal and conildential and you should be responsibie for any breach of the conflidentiality Access to pay advice by anyone else is unauthiorised If you are not the named recipient or addresses, you are hereby noticed that any use,review,Disclosure or copying of the contents herein is strictly prohibited and unlawful, No Assumption of responsibility or liability whatsever is Undertaken by METEOR group human resource management Division is respect of prohibited and unauthorised use by any other person.</td>
         </tr>
        </table>

       <pagebreak>";  
           
        

          // print_r($file_data);exit;
          $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
    
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);

      }  
         
   
    }



    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();

        $mpdf = new Mpdf();
        
        $mpdf->setTitle('Sample PDF');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');
        
        $mpdf->WriteHTML($pdfData);
        
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;

        $mpdf->Output($path, 'F');
        
    }
}
?>