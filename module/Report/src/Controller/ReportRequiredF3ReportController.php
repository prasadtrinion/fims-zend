<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportRequiredF3ReportController extends AbstractAppController
{
    
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

     
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');

        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // ReportRequiredF3Report F-3

    public function reportRequiredF3ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = $file_data ."
<html>
<head>
	<title>F3</title>
</head>
<body>

<table align='center' width='100%'>
   <tr>	
     <td style='text-align: left;'><fontsize='2'>LAPORAN INVOIS SETIAP PELAJAR (INDIVIDU)</font></td>
	</tr>
	 <tr>	
     <td style='text-align: left; color: red;'><fontsize='2'>LAPORAN YANG DIPERLUKAN</font></td>
	</tr>
  <tr>  
     <td style='text-align: left; color: blue;'><fontsize='2'>NAMA PELAJAR : SITI NUR AISYAH BT AHMAD ALI</font></td>
  </tr>
   <tr> 
     <td style='text-align: left; color: blue;'><fontsize='2'>NO MATRIK : 242911 </font></td>
  </tr>
</table>

<table  align='center' width='100%' border='1' cellpadding='0' cellspacing='0'>

  <tr>
	  <th style='text-align: center; width: 3%' ><fontsize='2'><b>Bil</b></font></th>
    <th  style='text-align: center; width: 10%'><fontsize='2'><b>NO INVOIS</b></font></th>
    <th  style='text-align: center; width: 10%' ><fontsize='2'><b>TARIKH INVOIS</b></font></th>
    <th  style='text-align: center; width: 18%' ><fontsize='2'><b>RUJUKAN</b></font></th>
    <th  style='text-align: center; width: 9%' ><fontsize='2'><b>SEMESTER</b></font></th>
    <th  style='text-align: center; width: 9%' ><fontsize='2'><b>KOD SEMESTER</b></font></th>
    <th  style='text-align: center; width: 7%' ><fontsize='2'><b>JUMLAH INVOIS </b></font></th>
    <th  style='text-align: center; width: 7%' ><fontsize='2'><b>JUMLAH BAYARAN</b></font></th>
    <th  style='text-align: center; width: 9%' ><fontsize='2'><b>NO EFT RUJUKAN BAYARAN DARIPADA PENAJA </b></font></th>
    <th  style='text-align: center; width: 9%'><fontsize='2'><b>RUJUKAN BAYARAN Cth:NO JURNAL/NO RESIT</b></font></th>
    <th style='text-align: center; width: 9%'><fontsize='2'><b>JBAKI TUNTUTAN YANG BELUM DI BAYAR</b></font></th>
	</tr>

	<tr>
	  <td style='text-align: center; '><font size='2'>1</font></td>
    <td  style='text-align: center; ' ><font size='2'>153/A161/242911</font></td>
    <td  style='text-align: center; '><fontsize='2'>Tarikh penyediaan invois</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>RUJ KAMI : UUM/HEP/U-ASSIST/K-27/KBXXX</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>SEP-16</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>A161 </font></td>
    <td  style='text-align: center; ' ><fontsize='2'>753</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>753</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>EFT 1102522007</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>JXXX/18</font></td>
    <td  style='text-align: center; ' ><fontsize='2'>-</font></td>
</tr>
<tr >
  <td style='text-align: center;' colspan='6'><fontsize='2'><b>JUMLAH KESELURUHAN TUNTUTAN</b></font></td>
  <td  style='text-align: center; ' ><fontsize='2'><b>3012</b></font></td>
  <td  style='text-align: center; ' ><fontsize='2'><b>2806</b></font></td>
  <td  style='text-align: center; ' ><fontsize='2'></font></td>
  <td  style='text-align: center; ' ><fontsize='2'></font></td>
  <td  style='text-align: center; ' ><fontsize='2'><b>206</b></font></td>
</tr>
</table>

<p style='text-align: left;color: red'>Nota : Untuk rujukan No Invois, 153/a161/242911-153 merujuk kod penaja/A161 merujuk kod semester/242911 no matrik</p>
</body>

</html>";
   
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}