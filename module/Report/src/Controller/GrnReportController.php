<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class GrnReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //GRN Report
    public function grnReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$fromDate = $postData['fromDate'];
        	$toDate = $postData['toDate'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$GrnData = $reportRepository->getGrnData($fromDate, $toDate);

        	
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

          $grn_data = "
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b>GOODS RECEIVED NOTE DETAILS</b></font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='2'>From Date: $fromDate &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date: $toDate</font></td>
        </tr>
        <tr>
        <td><br></td>
        </tr>
    </table>
    <br>

     
    <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
          <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>Sl. No</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>Reference No.</font></td>
           <td style='text-align:center' width='13%' valign='top'><font size='2'>Order Type</font></td>
           <td style='text-align:center' width='13%' valign='top'><font size='2'>Company Name</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'>Description</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Department</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>Financial Year</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Date</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>Approved Date</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>Amount(RM)</font></td>
           </tr>";

           $i = 1;
         foreach ($GrnData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $purNo = $details['f034fid_purchase_order'];
           $approveRef = $details['f034fapproved_ref_num'];
           $approvedDate = $details['f034fapproved_date'];
           $donor = $details['f034fdonor'];


           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $grn_data = $grn_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='13%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='13%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='15%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$approvedDate</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $grn_data = $grn_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($grn_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}