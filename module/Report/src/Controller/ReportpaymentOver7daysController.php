<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportpaymentOver7daysController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report7
    public function reportpaymentOver7daysAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'>UNIVERSITI UTARA MALAYSIA<br> MODUL BAYARAN<br>LAPORAN PRESTASI BAYARAN BIL</td>
            </tr>
            <tr>
             <td style='text-align: center;' width='100%'><font size='2'>DARI : $pay_date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HINGGA : $pay_date<br>BILANGAN HARI MELEBIHI 7 HARI</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='80%'><font size='2'>LAPORAN: </font></td>
             <td style='text-align: left' width='20%'><font size='2'>TARIKH: $pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='80%'><font size='2'>MASA: $pay_time</font></td>
             <td style='text-align: left' width='20%'><font size='2'></font></td>
            </tr>
            </table>

          <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='100%'><font size='2'>PTJ : 0100 - CANSOLORI (CANS) </font></td>
             </tr>
          </table>
          <br><br>

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>Bil</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. INVOIS PEMBEKAL</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. TERIMA INVOICE DI PTJ</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. DAFTAR BIL</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>KOD PENGHUTANG</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'> TH. TERIMA BAUCAR DI JB</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. QUERY</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. BATAL</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>NO. BAUCER</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>TH. PROSES</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>NAMA PENERIMA</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>AMAUN</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'>BIL HARI</font></td>
           </tr>
           <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='4'><hr></font></td>
           </tr>
            <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>1.</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>30/11/2017</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>30/11/2018</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>30/11/2018</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>C12917</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'> 18/11/2018</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'></font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='4'>B1504/18</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>30/11/2018</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>UNIUTAMA PROPERTY SDN BHD</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='4'>635.00</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='4'>11</font></td>
           </tr>
           </table>
           <br><br>

            <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: right' width='100%'><font size='2'>DISEMAK DAN DISAHKAN KETUA PTJ</font></td>
             </tr>
             <br><br>
             <tr>
             <td style='text-align: right' width='100%'><font size='2'>____________________________<br>Tarikh:</font></td>
             </tr>
            </table>
          
        
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}