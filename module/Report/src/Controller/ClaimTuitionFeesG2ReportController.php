<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimTuitionFeesG2ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function claimTuitionFeesG2ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
          <!DOCTYPE html>
<html>
<head>
  <title>G2</title>
  <style type='text/css'>
  </style>
</head>
<body>
<form>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: center;'>
        <h2>UNIVERSITI UTARA MALAYSIA<br>
        CLAIM TUITION FEES FOR YEMENI STUDENT 2016/2017</h2>
      </td >
    </tr>
  </table>
  <table style='border:1px solid black;border-collapse:collapse;' width='100%'>
    <tr>
      <th style='border:1px solid black;width: 10%;text-align: center;'>NO</th>
      <th style='border:1px solid black;width: 20%;text-align: center;'>MATRIC NO</th>
      <th style='border:1px solid black;width: 20%;text-align: center;'>PASSPORT</th>
      <th style='border:1px solid black;width: 30%;text-align: center;'>NAME</th>
      <th style='border:1px solid black;width: 20%;text-align: center;'>TOTAL USD</th>
    </tr>
    <tr>
      <td style='border:1px solid black;width: 10%;text-align: center;'>NO</td>
      <td style='border:1px solid black;width: 20%;text-align: center;'>MATRIC NO</td>
      <td style='border:1px solid black;width: 20%;text-align: center;'>PASSPORT</td>
      <td style='border:1px solid black;width: 30%;text-align: center;'>NAME</td>
      <td style='border:1px solid black;width: 20%;text-align: right;'>TOTAL USD</td>
    </tr>
    <tr>
      <td style='width: 10%;text-align: center;'></td>
      <td style='width: 20%;text-align: center;'></td>
      <td style='width: 20%;text-align: center;'></td>
      <td style='width: 30%;text-align: center;'></td>
      <td style='width: 20%;text-align: right;' rowspan='5'>96,000.00</td>
    </tr>
</table>
<table width='100%'>
<tr>
<td style='width: 100%;text-align: left;'>REVISED BY : </td>
</tr>
</table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}