<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimPaymentC3ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function claimPaymentC3ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$id = $postData['id'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$POData = $reportRepository->getPODataById((int)$id);

        	// print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
          <!DOCTYPE html>
<html>
<head>
  <title>C3</title>
</head>
<body>
<form>
  <table width='100%' align='center'>
    <tr>
      <td style='width: 100%;text-align: left;'>
      <center>
    <h2>
    JABATAN BENDAHARI
    <br>
    UNIVERSITI UTARA MALAYSIA
    </h2>
    <p>06010 UUM, Sintok,Kedah Darul Aman, Malaysla. Tel:04-92877771/ 04-9283135</p>
    Fax:04-9285764(Bendahari) / 04-9287905 (U-Assist) E-mail:<a href='#'>scholarship@uum.edu.my</a>
  </center>
    </td>
    </tr>
  </table>
  <br><br>
  <table width='100%'>
    <caption>
      <strong>TUNTUTAN BAYARAN</strong>(Invoice)
    </caption>
    <tr>
      <td style='width: 50%;text-align: left;'>
        NAMA (Name):<input type='text' name='name' placeholder='ESRAA JAFFAR BAKER'><br><br><br>
        NO.MATRIK (Matric no):<input type='text' name='' placeholder='95659'><br><br><br>
        NO.K.P./NO.PASSPORT (I.C.No/Passport No.):<input type='text' name='' placeholder='A5853113'><br><br><br>
        TEMPOH PENGAJIAN (Poriod Of Study):<input type='text' name='' placeholder='04/09/2017-25/01/2018'><br><br><br>
        TEMPOH TAJAAN(Period Of Sponsorship):<input type='text' name='' placeholder='01/02/2015-31/08/2018'>
      </td>
      <td style='width: 50%;text-align: left;'>
        INVOICE NO:<input type='text' name='name' placeholder='330A171/90059'><br>(Invoice No)<br><br>
        TARIKH    :<input type='text' name='name' placeholder='04/10/2017'><br>(Date)<br><br>
        RUJUKAN KAMI:<input type='text' name='' placeholder='4/1(330 K30348'><br>(Our Ref.)<br><br>
        RUJUKAN LAIN :<input type='text' name='' placeholder='UUM/HEP/U-ASSISTK-27'><br>(Other.Ref)<br><br>
        BAKI TAJAAN (RM):<br>(Balance)
      </td>
    </tr>
    <tr>
      <td colspan='2'>
        <hr style='height: 1px;background-color: black;'>
      </td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
      <th style='width: 1%;'></th>
      <th style='width: 45%;text-align:left'>KETERANGAN(Description)</th>
      <th style='width: 45%;text-align:right;'>AMAUN (AmountyRM)</th>
    </tr>
    <tr>
      <td style='width: 1%'>1.</td>
      <td style='width: 50%;text-align:left;'>Kebajikan (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>2.</td>
      <td style='width: 50%;text-align:left;'>Pengajian (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>3.</td>
      <td style='width: 50%;text-align:left;'>Perpustakaan (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>4.</td>
      <td style='width: 50%;text-align:left;'>Pergigian (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>5.</td>
      <td style='width: 50%;text-align:left;'>Sukan (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>6.</td>
      <td style='width: 50%;text-align:left;'>Komputer (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>7.</td>
      <td style='width: 50%;text-align:left;'>Kesihatan (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%'>8.</td>
      <td style='width: 50%;text-align:left;'>Kitaran Kampus (Welfare)</td>
      <td style='width: 50%;text-align:right;'>150.00</td>
    </tr>
    <tr>
      <td style='width: 1%; text-align: left;'><h4> Jumlah(Total</h4></td>
      <td style='width: 49%; text-align: left;'></td>
      <td style='width: 50%; text-align: right;'><strong>3,085.00</strong></td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: left;'><h3>Ringgit Malaysia :<br>
                          TIGA RIBU LAPAN PULUH LIMA DAN SEN O SAHAJA</h3>
      </td>
    </tr>
    <tr>
      <td colspan='3'>
        <hr style='height: 1px;background-color: black;'>
      </td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
       <td style='width: 100%;text-align:left;'>
       1. Bayaran menggunakan cek hendaklah dipalang dan dibayar kepada <b>Universiti Utara Malaysia.</b><br>
       <i>(Payment by cheque must be crossed and made payable to<b> Universiti Utara Malaysia)</b></i><br>
    2 Sila maklumkan ke Jabatan Bendahari (seperti alamat di atas) sekiranya bayaran dibuat secara pindahan elektronik (EFT).<br>
      <i>(Please send Payment Advice to the above address if payment is made by electronic fund transerEFT)</i><br>
    3 Nama Bank / No. akaun adalah seperti berikut Bank Islam Malaysia Bertad Cawangan UUM,Account Na.0200301 00001
       </td>
    </tr>
    <tr>
     <td style='width: 100%;text-align:center;'>
    <center><i>Inoice ini adalah cetakan komputerdan tidak memerlukan tandatangan<br> This invoice is Computer generated and no signature is required</i></center>
    </td>
    </tr>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}