<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class TuitionFeesC2ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // TuitionFeesC2Report C-2
    public function tuitionFeesC2ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 



        $file_data = $file_data ."
<table align='center' width='100%'>
	<tr>
		 <td style='text-align: center;'><font size='3'>TUITION FEES FOR SPONSORED STUDENTS(IRAQ)</font></td>
	</tr>
	<tr>
		 <td style='text-align: center;'><font size='3'>UUM/HEP/U-ASSIST/K-27/4/1(339)/KB0348</font><br><br><br></td>
	</tr>
</table>

<table align='center' width='100%' border='1' cellpadding='0' cellspacing='0'>
<tr>
  <th style='text-align: center; width: 5% '><font size='3'><b>NO</b></font></th>
  <th  style='text-align: center; width: 10%'><font size='3'><b>MATRIC</b></font></th>
  <th  style='text-align: center; width: 40%'><font size='3'><b>NAME</b></font></th>
  <th  style='text-align: center; width: 10%' ><font size='3'><b>CODE</b></font></th>
  <th  style='text-align: center; width: 5%' ><font size='3'><b>IPT</b></font></th>
  <th  style='text-align: center; width: 30%' ><font size='3'><b>AMOUNT(RM)</b></font></th>
</tr>

<tr>	
  <td style='text-align: center;  '><font size='3'>1</font></td>
  <td  style='text-align: center; ' ><font size='3'>95659</font></td>
  <td  style='text-align: center; '><font size='3'>Esraa Jaffar Baker </font></td>
  <td  style='text-align: center; ' ><font size='3'>339/A171</font></td>
  <td  style='text-align: center; ' ><font size='3'>UUM</font></td>
  <td  style='text-align: center; ' ><font size='3'>3,085.00</font></td>
</tr>
</table><br><br><br>

<table align='center' width='100%'>
	<tr>
		 <td style='text-align: center; float: left; width: 15%' valign='top'><font size='3'>APPROVED BY:</font></td>
		 <td style='text-align: center; width: 25%'><font size='3'>Noor Nazila Nazren<br>Assistant Bursar<br>U-Assist<br>Universiti Utara Malaysia </font><br><br><br></td>
		 <td style='text-align: center; width: 65%'></td>
			</tr>
</table>";
   
    
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}