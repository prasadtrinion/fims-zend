<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentLoanReportI1ReportController extends AbstractAppController
{
    
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // StudentLoanReportI1Report I-1

    public function studentLoanReportI1ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = $file_data ."
<html>
<head>
  <title>I1</title>
</head>
<body>

<table align='center' width='100%'>
   <tr> 
     <td style='text-align: left;'><font size='3'>FORMAT LAPORAN HUTANG PELAJAR MENGIKUT</font></td>
  </tr>
   <tr> 
     <td style='text-align: left; '><font size='3'>NAMA PENAJA : MYBRAIN</font></td>
  </tr>
   <tr> 
     <td style='text-align: left;'><font size='3'>KOD PENAJA : E 255</font></td>
  </tr>
</table>

<table  align='center' width='100%' border='1' cellpadding='0' cellspacing='0'>

  <tr >
    <td  style='text-align: center; width: 2%' rowspan='2' ><font size='3'><b>Bil</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>NAMA</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>NO MATRIK</b></font></td> 
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>NO KP / NO PASSPORT</b></font></td>
    <td  style='text-align: center; width: 15%' colspan='2'><font size='3'><b>SESI MASUK</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>PERINGKAT PENGAJIAN</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>PROGRAM </b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>PUSAT PENGAJIAN</b></font></td>
    <td  style='text-align: center; width: 15%' colspan='2' ><font size='3'><b>TEMPOH TAJAAN </b></font></td>
    <td  style='text-align: center; width: 3%' rowspan='2' ><font size='3'><b>STATUS</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>WARGANERA</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>NEGARA</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>NAMA PENAJA</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>KOD PENAJA</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>HUTANG PORTION PENAJA<br>(A)</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>PORTION PELAJAR <br>(B)</b></font></td>
    <td  style='text-align: center; width: 5%' rowspan='2'><font size='3'><b>JUMLAH HUTANG<br>(A+B)</b></font></td>
  </tr>

   <tr>
     <td  style='text-align: center;' ><font size='3'><b>SEMESTER</b></font></td>
     <td  style='text-align: center;' ><font size='3'><b> KOD SEMESTER</b></font></td>
     <td  style='text-align: center;' ><font size='3'><b>MULA</b></font></td>
      <td style='text-align: center;' ><font size='3'><b>TAMAT</b></font></td>
   </tr>
 
  <tr>
     <td  style='text-align: center; ' ><font size='2'>1</font></td>
     <td  style='text-align: center; ' ><font size='2'>ALI</font></td>
     <td  style='text-align: center; ' ><font size='2'>812253</font></td>
     <td  style='text-align: center; ' ><font size='2'>7755533333</font></td>
     <td  style='text-align: center; ' ><font size='2'>Feb-18</font></td>
     <td  style='text-align: center; ' ><font size='2'>A 172</font></td>
     <td  style='text-align: center; ' ><font size='2'>SARJANA MUDA/MASTER/PHD</font></td>
     <td  style='text-align: center; ' ><font size='2'>MBA</font></td>
     <td  style='text-align: center; ' ><font size='2'>OYA</font></td>
     <td  style='text-align: center; ' ><font size='2'>1/1/2018</font></td>
     <td  style='text-align: center; ' ><font size='2'>31/12/2021</font></td>
     <td  style='text-align: center; ' ><font size='2'>AKTIF</font></td>
     <td  style='text-align: center; ' ><font size='2'>MALAYSIA</font></td>
     <td  style='text-align: center; ' ><font size='2'>MALAYSIA</font></td>
     <td  style='text-align: center; ' ><font size='2'>MYBRAIN</font></td>
     <td  style='text-align: center; ' ><font size='2'>E255</font></td>
     <td  style='text-align: center; ' ><font size='2'>2158.00</font></td>
     <td  style='text-align: center; ' ><font size='2'>100.00</font></td>
     <td  style='text-align: center; ' ><font size='2'>2258.00</font></td>
</tr>
</table>
</body>
</html>";
   
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}