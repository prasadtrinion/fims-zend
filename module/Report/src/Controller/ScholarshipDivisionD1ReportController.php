<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ScholarshipDivisionD1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function scholarshipDivisionD1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title></title>
</head>

<body>
  <table width='100%' align='center'>
    <tr>
      <td><img src='sas' width='240' height='120'></td>
      <td style='text-align: left' width='10%'>
        <font size='4px'>
        <b>Jabatan Hal Ehwal Pelajar & Alumni<br>
        06010 UUM Sintok <br>
        Kedah Darul Aman, Malaysia <br>
        Tel: (604) 928 3205 <br>
        Faks: (604) 928 3216 <br>
        www.uum.edu.my</b>
        </font>
      </td>
    </tr>
    <tr>
      <td><hr width='111%'></td>
      <tr>
        <td style='text-align: center'><font size='5px'><b>'MUAFAKAT KEDAH'</font></td>
      </tr>
      <tr>
        <td style='text-align: right'>
        <font face='sans-serif' size='4px'>
          POS LAJU <br>
          <b>UUM/HEP/U-ASSIST/K-27/4/1(255) <br>
          29 SEPTEMBER 2017 </b><br></font></td>
      </tr>
      
      <tr></tr>
      <tr>
        <td><font size='5px'>
            Ketua Setiausaho <br>
            Bahagian Biasiswa <br>
            Kementerian Pendidikan Tinggi Malaysia <br>
            Aras 2 No 2 Menara 2 <br>
            Jalan P 5/6 Presint 5 <br>
            Pusat Pentadbiran Kerajaan Persekutuan <br>
            62100 Putrajayo <br>
            (u/p:Pn Nazatul Shima Md Noor) <br><br>
        </td>
      </tr>
      <tr>
        <td><font size='5px'>Tuan,</font></td>
      </tr>
      <tr><td><br></td></tr>
      <tr>
        <td><font size='5px'><b>TUNTUTAN YURAN PENGAJIAN PELAJAR PhD DI BAWAH TAJAAN KPM (MYBRAIN15) UNTUK SEMESTER A171 (SEPT 2017-JAN 2018)</b></font>
        </td>
      </tr>
      <tr>
        <td><font size='5'>
          Dengan hormatnya saya merujuk kepada perkara di atas<br><br>
          2. Disertakan di sini penyata tuntutan yuran 65 orang pelajar PhD di bawah tajaan <br>
          pihak tuan berjumlah <b>RM 123,295.00</b> untuk dibuat bayaran <br>
          <br>
          3. Sila keluarkan cek atas nama <b>'Universiti Utara Malaysia'</b> can dipalang <b>'Akaun <br>
          Penerima Sahaja'</b> atau email kepada <a href='nozila@uum.edu.my'>nozila@uum.edu.my</a>/
          <a href=''>scholarship@uum.edu.my</a> untuk makluman bayaran yang telah dibuat kepada pihak universiti. Harap maklum.
          <br>
          <br>
          Sekian, terima kasih.

          <br>
          <br>
          <b>'BERKHIDMAT UNTUK NEGARA'</b><br>
          <b>'ILMU BUDI BAKTI'</b><br>
          <br>
            Saya yang menurut perintah
          </font>
        </td>
      </tr>
      <tr>
        <td valign='top'><font size='5px'><b>(NOOR NAZILA NAZREN)</b><br>Penolong Bendahari<br>b/p Pengarah U-Assist<br>Universiti Utara Malaysia<br>s.k.: Fall <br></font>
        </td>
      </tr>
      <tr><td><br></td></tr>
      <tr>
        <td><br><br><br></td>
        <td><img src='sds' align='right'><img src='sas' align='right'></td>
      </tr>
      <tr>
        <td></td>
        <td style='text-align: right'><b>Universiti Pengurusan Terkemuka</b></td>
      </tr>
  </table>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}