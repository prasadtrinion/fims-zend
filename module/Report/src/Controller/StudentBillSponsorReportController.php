<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentBillSponsorReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Sponsor List Report A-7
    public function studentBillSponsorReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	// $journalId = $postData['journalId'];

        	// $reportRepository = $this->getRepository('T019fsavedReports');
        	// $journalData = $reportRepository->getJournalEntryById((int)$journalId);

        	// print_r($journalData);exit;
          $JournalDescription = $journalData[0]['f017fdescription'];
          $Approver = $journalData[0]['originalName'];


          $pay_date = date('d/m/Y h:i:s a', time());
        $pay_date   = date("d/m/Y", strtotime($pay_date));


        	$journal_data = "
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b>UNIVERSITI UTARA MALAYSIA<br>Invois Hutang Pelajar Kepada Penaja</b><font></td>
        </tr>
    </table>
    <br><br>

     <table align='center' width='100%'>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>No. Invois:<font></td>
          <td style='text-align: left' width='15%' valign='top'><font size='2'><font></td>
          <td style='text-align: center' width='20%' valign='top'><font size='2'><font></td>
          <td style='text-align: center' width='20%' valign='top'><font size='2'><font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='2'>Tarikh:<font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='2'>$pay_date<font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Matrik:<font></td>
          <td style='text-align: left' width='15%' valign='top'><font size='2'><font></td>
          <td style='text-align: right' width='35%' valign='top' colspan='2'><font size='2'>Tempoh Tejaan:<font></td>
          <td style='text-align: left' width='35%' valign='top' colspan='2'><font size='2'>$pay_date - $pay_date<font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Nama:<font></td>
          <td style='text-align: left' width='85%' valign='top' colspan='5'><font size='2'><font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>No. KP/No Passport:<font></td>
          <td style='text-align: left' width='15%' valign='top'><font size='2'><font></td>
          <td style='text-align: right' width='35%' valign='top' colspan='2'><font size='2'>Tempoh Pengajian<font></td>
          <td style='text-align: left' width='35%' valign='top' colspan='2'><font size='2'>$pay_dat$pay_date<font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Yourref:<font></td>
          <td style='text-align: right' width='35%' valign='top' colspan='2'><font size='2'><font></td>
          <td style='text-align: left' width='15%' valign='top'><font size='2'>Oureff<font></td>
          <td style='text-align: left' width='35%' valign='top' colspan='2'><font size='2'><font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Jumlah Tuntutan:<font></td>
          <td style='text-align: left' width='18%' valign='top'><font size='2'>36,121<font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='2'>Baki Tejaan (RM):<font></td>
          <td style='text-align: left' width='18%' valign='top'><font size='2'>36,999<font></td>
          <td style='text-align: right' width='17%' valign='top'><font size='2'>Jumlah Tejaan(RM):<font></td>
          <td style='text-align: left' width='17%' valign='top'><font size='2'>36,888<font></td>
        </tr>
        <tr>
          <td style='text-align: right' width='20%' valign='top'><font size='2'>Baki terkini(RM):<font></td>
          <td style='text-align: left' width='80%' valign='top' colspan='5'><font size='2'>4,015<font></td>
        </tr>
    </table>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b><hr></b><font></td>
        </tr>
    </table>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>No.<font></td>
          <td style='text-align: left' width='70%' valign='top'><font size='2'>KETERANGAN(Description)<font></td>
          <td style='text-align: right' width='25%' valign='top'><font size='2'>AMAUN(Amount)(RM)<font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='2'>1.<font></td>
          <td style='text-align: left' width='70%' valign='top'><font size='2'><font></td>
          <td style='text-align: right' width='25%' valign='top'><font size='2'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='75%' valign='top' colspan='2'><font size='2'>Jumlah (Total)<font></td>
          <td style='text-align: right' width='25%' valign='top'><font size='2'>0.00<font></td>
        </tr>
    </table>
     ";
        }
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
    }
}