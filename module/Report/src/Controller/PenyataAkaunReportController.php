<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PenyataAkaunReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }


    public function PenyataAkaunReportAction()
    {
        

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 

       

             <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>

            <tr>
            <td style='text-align:left' width='10%' valign='top'><font size='2'>LAPORAN:MS174b_new<br>MASA:10:43:14<br>TARIKH : 31/10/2018 </td>

            <td style='text-align:center' width='10%' valign='top'><font size='2'>UNIVERSITI UTARA MALAYSIA<br> SISTEM PINJAMAN KENDERAAN<br>PENYATA AKAUN</td>

            <td style='text-align:right' width='10%' valign='top'><font size='2'>MUKA SURAT:1<br><br>SULIT & PERSENDIRIAN</td>
            </table>

            <br><br><br>

        <table  align='center' border='0' style='width:100%;height:50px;'>
           <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>NORHANIZA BINTI HUSSAIN<br><br> JABATAN BENDAHARI<br><br><br><br><br><br>KOD PENGHUTANG:3762<br>SEHINGGA TARIKH:31:10:2018<br>BAG PERIOD:10/2018</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>KAEDAH PINJAMAN<br>TEMPOH PINJAMAN<br>JUMLAH DILULUSKAN<br>JUMLAH INSURAN<br>JUMLAH PINJAMAN<br>JUMLAH FAEDAH POKOK<br>JUMLAH FAEDAH INSURAN<br>JUMLAH FAEDAH<br>JUMLAH PERLU DIBAYAR<br>BULAN MULA BAYAR<br>BULAN AKHIR BAYAR<br>NO.DAFTAR KEND</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>:ISLAM<br>:BULAN<br>:RM 290000<br>:RM 17600<br>: RM 290000<br>:RM 4954000<br>:RM21000<br>:RM 42356<br>:RM 2345678<br>:05/2016<br> :05/2016<br>:KEC8374</font></td>
           </tr>
           </table>

           <table align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'><tr>
            <th rowspan='2'>TARIKH/BULAN</th>
            <th rowspan='2'>DOKUMEN RUJUKAN</th>
            <th colspan='5'>BAYARAN BALIK</th>
            <th rowspan='2'>BENKI POKOK DAN INSURAN(I) (P+T) (RM)</th>
            <th rowspan='2'>BAKI FAEDAH POKOK(P) DAN INS(I) (RM)</th>
            <th rowspan='2'>BAKI KESELURUHAN (RM)</th>
    
            </tr>
            <tr>
            <td>POKOK(RM)</td>
            <td>INSURAN(RM)</td>
            <td>FAEDAH POKOK(RM)</td>
            <td>FAEDAH INSURAN(RM)</td>
            <td>JUMLAH POTONGAN(RM)</td>

            </tr>
        </table>

        <table  align='center' border='0' style='width:100%;height:50px;'>
           <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>***Nota***<br><br>Sebarang Pertanyaan Sila Hubungi :<br>1. En.Nikmal Musal Mohd Muhaiyuddin -Tel. 04-9283209<br>2.Pn. Norhanisa Hussain -Tel. 04-9283233 BENDAHARI<br>3.En. Mohammad Fais Abdul Malik -Tel. 04-9283253 UNIVERSITI UTARA MALAYSIA</font></td>
           </tr>
           </table>
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}