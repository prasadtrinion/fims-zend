<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimTuitionFeesA13ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// claimTuitionA15Report A-13
    public function claimTuitionFeesA13ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();


          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

        $file_data = "
   <!DOCTYPE html>
<html>
<head>
  <title>A13</title>
</head>
<body>
<form>
  <table width='100%' align='center'>
    <tr>
      <td style='width: 10%;text-align: left;'><img src='' style='width: 120px;height: 100px'></td>
      <td style='width: 60%;text-align: right;'>
        <strong>
          JABATAN HAL PELAJAR<br>
          STUDENT AFFAIRS DEPARTMENT<br>
        </strong>
          Universiti Utara Malaysia<br>
          06010 UUM SINTOK <br>
          KEDAH DARUL AMAN<br>
          MALAYSIA
      </td>
    </tr>
    <tr>
      <td colspan='2'><hr></td>
    </tr>
  </table>
  <table width='100%' align='center'>
    <tr>
      <td style='text-align: center; width: 100%;'><b>'KEDAH AMAN MAKMUR BERSAMA MEMACU TRANSPORMASI'</b></td>
    </tr>   
    <tr>
      <td style='width: 100%;text-align: left;'><B>UUM/HEP/U-ASSIST20160410-KKB7963<br>07 APRIL 2016</B></td>
    </tr>
    <tr>
      <td style='width: 100%;text-align: left;'>
          <br>Academic Attache <br>
          Block B-3 A-15 <br>
          Megan Avenue 2 <br>
          Jin Yap Kwan Seng <br>
          50450 Kuala Lumpur <br>
          Attn: Dr Samir A. Karshman/Dr Adoulmajid H.M. Ali <br>
          Ms. Baizurz Alias/Nurul Shazrina <br><br>
        Dear Sir,<br><br>

        <b>CLAIM TUITION FEES FOR LIBYAN SPONSORED PHD STUDENT A152 (FEB 2015 - AUGUST 2015)</b><br><br>
        With reference to the above matter, the total fees charged for your sponsored studtent is <b>RM28,550.00/USD6,797.62</b> (based on standard exchange rate USD1=MYR4.20) per attached invoice.
        <br><br>

        As such, please make payment to our account : <b>Universiti Utara Malaysia</b> at Bank Islam Malaysia Berhad, Universiti Utara Malaysia Branch, 06010 Sintok, Kedah <b>(Account No, 020-9301-00000-1O),</b> Swift Code: BIMBMYKL, Bank Telephone No 604-9286766 and send the payment advice for our reference to <a href='nazila@uum.edu.my'>nazila@uum.edu.my</a>/<a href=''>scholarship@uum.edu.my</a>.<br><br><br>
      </td>
    </tr>
      <tr>
        <td style='width: 100%;text-align: left;'>
          Thank You.<br><br>
        </td>       
      </tr>
        <tr>
          <td style='width: 100%;text-align: left;'>
            Yours Sincerely,<br><br><br><br>
          </td>
        </tr>
  </table>
      <table width='100%'>
        <tr>
          <td style='width: 70%;text-align: left;'>
            <b>(NOOR NAZILA NAZREN)</b><br>
          Assistant Bursar<br>
          For Director U-Assist<br>
          Universiti Utara Malaysia
          </td>
          <td style='width: 30%;text-align: left;'>
            <b>BURSAR<br></b>
            <b>BURSAR DEPARTMENT<br></b>
            UNIVERSITI UTARA MALAYSIA<br>
            06010 Sintok, Kedah, Malaysia.<br>
            Tel: 604-9283121<br>
          </td>
        </tr>
  </table>
</form>
</body>
</html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}