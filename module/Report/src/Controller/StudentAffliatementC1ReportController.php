<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentAffliatementC1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

    
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // studentAffliatementC1Report C-1
    public function studentAffliatementC1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       
            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        // print_r("jjj");
        // die();

        $file_data = "
<html>
<head>
	<title>C1</title>
</head>
<body>
<table align='center' width='100%'>
	<tr>
		<td style='text-align: left; width: 20%'><img src='' width='100px' height='100px'></td>
		<td style='text-align: left; width: 50%'><font size='3'><b>JABATAN HAL EHWAL PELAJAR</b><br><b>STUDENT AFFAIRS DEPARTMENT</b><br>Universiti Utara Malaysia<br>06010 UUM SINTOK<br>KEDAH DARULAMAN<br>MALAYSIA</font></td>
		<td style='text-align: left; width: 15%'></td>
		<td style='text-align: right; width: 15%'><img src='' width='20px' height='20px'><font size='2'><br><br>Tel: (604) 9287777<br>Faks: (604) 9284141<br>E-mail: uassist@uum.edu.my </font></td>
	</tr>
</table><hr></hr><br>
<table align='center' width='100%'>
   <tr>
	<td style='text-align: center; '><font size='3'>'MUAFAKAT KEDAH'</font><br><br></td>
   </tr>

    <tr>
	<td style='text-align: right;'><font size='3'>UUM/HEP/U-ASSIST/K-27/4/1(339)</font></td>
   </tr>	
    <tr>
	<td style='text-align: right;'><font size='3'>04 OCTOBER 2017</font></td>
    </tr>
</table>

<table align='center' width='100%'>
	<tr>
    <td style='text-align: left; '><font size='3'>The Cultural Attache</font></td>
	</tr>
	<tr>
    <td style='text-align: left; '><font size='3'>Embassy of the Republic of Iraq</font></td>
	</tr>
	<tr>
    <td style='text-align: left; '><font size='3'>Unit 5.07,Level 5</font></td>
	</tr>
	<tr>
    <td style='text-align: left; '><font size='3'>North Block Ampwalk</font></td>
	</tr>
	<tr>
    <td style='text-align: left; '><font size='3'>218 Jln Ampang</font></td>
	</tr>
	<tr>
    <td style='text-align: left;'><font size='3'>50450 Kudla Lumpur</font><br><br></td>
	</tr>
</table>

<table align='center' width='100%'>
	<tr>
		 <td style='text-align: left; '><font size='3'>Dear Sir,</font><br><br></td>
	</tr>
     <tr>
		 <td style='text-align: left; '><font size='3'><b> CLAIM TUITION FEES FOR THE IRAQI SPONSORED STUDENT A171(SEPT 2017/2018)</b>
		</font><br></td>
	</tr>
	<tr>
		<td style='text-align: left; '><font size='3'><p>With reference to the above matter, the total fees charged for your sponsored student in UUM is <b>RM24,680.00/USD5,876.19</b> as per attached invoice. (based on standard exchange rate USD1=MYR4.20)</p></font></td>
	</tr>

	<tr>
		 <td style='text-align: left; '><font size='3'><p> As such,please make payment to our account <b>: Universiti Utara Malaysia </b>at Bank Islam Malaysia Berhad, Universiti Utara Malaysia Branch, 06010 Sintok,Kedah<b>(Account No. 020-9301-00000-10),</b> Swift Code: BIMBMYKL, Bank Telephone No.: 604-9286766 and send the payment advice for our reference to <a href='#'><b>nazila@uum.edu.my/scholarship@uum.edu.my</b></a></p></font><br><br></td>
	</tr>
     <tr>
		 <td style='text-align: left; '><font size='3'>Thank You.</font><br><br></td>
	</tr>
	<tr>
		 <td style='text-align: left; '><font size='3'>Your Sincerely</font><br><br><br></td>
	</tr>
</table>

<table width='100%'>
				<tr>
					<td style='width: 20%;text-align: left;'>
						<b>(NOOR NAZILA NAZREN)</b><br>
					Assistant Bursar<br>
					For Director U-Assist<br>
					Universiti Utara Malaysia
					</td>
					<td style='width: 80%;text-align: right;'>
						<b>BURSAR<br></b>
						<b>BURSAR DEPARTMENT<br></b>
						UNIVERSITI UTARA MALAYSIA<br>
						06010 Sintok, Kedah, Malaysia.<br>
						Tel: 604-9283121<br>
					</td>
				</tr>
	</table>
</body>
</html>";
   
    
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}