<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimPaymentD5ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function claimPaymentD5ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <table width='100%'>
    <tr>
      <td style='text-align: center;width: 100%'>
        <center>
    <h2>
    JABATAN BENDAHARI
    <br>
    UNIVERSITI UTARA MALAYSIA
    </h2>
    <p><font size='4px'>06010 UUM, Sintok,Kedah Darul Aman, Malaysla. Tel:04-9287777 / 04-9283135 <br>
Fax:04-9285764(Bendahari) / 04-9287905 (U-Assist) E-mail:<a href='#'>scholarship@uum.edu.my</a></font></p>
  </center>
  <br><br>
      </td>
    </tr>
  </table>
  <table width='100%'>
    <font size='5px'>
    <h3 align='center'><b>TUNTUTAN BAYARAN &nbsp&nbsp <i>(Invoice)</i></b></h3>
    </font>
    </caption>
    <tr>
      <td style='width: 50%;text-align: left;'>
        <font size='5px'>
        NAMA (Name):&nbsp&nbsp  NAZNI BIN NOORDIN<br><br>
        NO.MATRIK (Matric no):&nbsp&nbsp  95974<br><br>
        NO.K.P./NO.PASSPORT (I.C.No/Passport No.):&nbsp&nbsp  700804025367<br><br>
        TEMPOH PENGAJIAN (Poriod Of Study):&nbsp&nbsp  04/09/2017 - 25/01/2018<br><br>
        TEMPOH TAJAAN(Period Of Sponsorship):&nbsp&nbsp   01/09/2015 - 28/02/2018
        </font>
      </td>
      <td style='width: 50%;'>
        <font size='5px'>
        INVOICE NO &nbsp&nbsp&nbsp:   255/A171/95974<br>(Invoice No.)<br>
        TARIKH &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:&nbsp   29/09/2017<br>(Date)<br>
        RUJUKAN KAMI &nbsp&nbsp&nbsp&nbsp&nbsp:   (255)-KB0309<br>(Our Ref.)<br>
        RUJUKAN LAIN &nbsp&nbsp&nbsp&nbsp&nbsp:   UUM/HEP/U-ASSIST/K-27<br>(Other Ref.)<br>
        BAKI TAJAAN (RM) &nbsp&nbsp&nbsp&nbsp&nbsp:   16,010.00<br>(Balance)
      </font>
      </td>
    </tr>
  </table>
  <p><hr style='height: 2px; background-color: black'></p>

  <table width='100%'>
    <tr>
      <th style='width: 1%;'></th>
      <th style='width: 100%;text-align:left'><font size='5px'>KETERANGAN<i>(Description)</i></font></th>
      <th style='width: 45%;text-align:right;'><font size='5px'>AMAUN<i>(Amount)(RM)</i></font></th>
    </tr>
    <tr></tr>
    <tr>
      <td style='width: 1%'><font size='5%'>1.&nbsp&nbsp</td>
      <td style='width: 50%;text-align:left;'><font size='5%'>Komputer/Ict (Computer/Ict)</font></td>
      <td style='width: 50%;' align='center'><font size='5%'>75.00</td>
    </tr>
    <tr>
      <td style='width: 1%'><font size='5%'>2.&nbsp&nbsp</td>
      <td style='width: 50%;text-align:left;'><font size='5%'>Kebajikan (Welfare)</td>
      <td style='width: 50%;' align='center'><font size='5%'>113.00</td>
    </tr>
    <tr>
      <td style='width: 1%'><font size='5%'>3.&nbsp&nbsp</td>
      <td style='width: 50%;text-align:left;'><font size='5%'>Kesihatan (Health)</td>
      <td style='width: 50%;' align='center'><font size='5%'>30.00</td>
    </tr>
    <tr>
      <td style='width: 1%'><font size='5%'>4.&nbsp&nbsp</td>
      <td style='width: 50%;text-align:left;'><font size='5%'>Perpustakaan (Library)</td>
      <td style='width: 50%;' align='center'><font size='5%'>30.00</td>
    </tr>
    <tr>
      <td style='width: 1%'><font size='5%'>5.&nbsp&nbsp</td>
      <td style='width: 50%;text-align:left;'><font size='5%'>Pengajian (Tuition)</td>
      <td style='width: 50%;' align='center'><font size='5%'>1,350.00</td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
      <td></td>
      <td style='width: 50%;text-align:left;'><font size='5px'><b>Jumlah <i>(Total)</i></b></font></td>
      <td style='width: 50%;' align='center'><font size='5%'><b>1,598.00</b></td>
    </tr>
  </table>

  <p><b><font size='5px'>Ringgit Malaysia :<br>
  DUA RIBU SATU RATUS LIMA PULUH LAPAN DAN SEN O SAHAJA</font></b></p>
  <br><br>
  <hr style='height: 2px;background-color: black;'>

  <font size='4px'>
    
    <ol>
      <li>
        Bayaran menggunakan cek hendaklah dipalang dan dibayar kepada <b>Universiti Utara Malaysia.</b><br>
          <i>(Payment by cheque must be crossed and made payable to<b> Universiti Utara Malaysia)</b></i>
      </li>
      <li>
        Sila maklumkan ke Jabatan Bendahari (seperti alamat di atas) sekiranya bayaran dibuat secara pindahan elektronik (EFT).<br>
          <i>(Please send Payment Advice to the above address if payment is made by electronic fund transer-EFT)</i>
      </li>
      <li>
        Nama Bank / No. akaun adalah seperti berikut: Bank Islam Malaysia Berhad Cawangan UUM, Account No.02093010000010. <br>
        swift code-BIMBMYKL
      </li>
    </ol>
  </font>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: center;'><center><i>Inoice ini adalah cetakan komputer dan tidak memerlukan tandatangan<br> 
    This invoice is Computer generated and no signature is required</i></center></td>
    </tr>
  </table>

</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}