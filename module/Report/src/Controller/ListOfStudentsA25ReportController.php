<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ListOfStudentsA25ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function listOfStudentsA25ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title>A25</title>
</head>
<body>
<form>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: center;'>
        <b>SENARAI PELAJAR SLAB & SKPD BAGI SEMESTER BERAKHIR SEPTEMBER 2016 (SEPTEMBER 2016-JANUARI 2017)</b>
      </td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
      <td style='width: 10%;text-align: left;'>
        TUSI
      </td>
      <td style='width: 0%;text-align: center;'>:</td>
      <td style='width: 90%;text-align: left;'>
        UNIVERSITI UTARA MALAYSIA
      </td>
    </tr>
    <tr>
      <td style='width: 10%;text-align: left;'>
        PENERIMA
      </td>
      <td style='width: 0%;text-align: center;'>:</td>
      <td style='width: 90%;text-align: left;'>
        UNIVERSITI UTARA MALAYSIA
      </td>
    </tr>
    <tr>
      <td style='width: 10%;text-align: left;'>
        
      </td>
      <td style='width: 0%;text-align: center;'>:</td>
      <td style='width: 90%;text-align: left;'>
        BANK ISLAM MALAYSIA
      </td>
    </tr>
    <tr>
      <td style='width: 10%;text-align: left;'>
        kAUN
      </td>
      <td style='width: 0%;text-align: center;'>:</td>
      <td style='width: 90%;text-align: left;'>
        02093010000010
      </td>
    </tr>
    <tr>
      <td style='width: 0%;text-align: left;'>
        
      </td>
      <td style='width: 0%;text-align: left;'></td>
      <td style='width: 100%;text-align: left;'>
        kan salinan penyata akaun bank untuk rujukan pembayaran)<sup>***</sup>
      </td>
    </tr>
  </TABLE>
  <table width='100%' border='1'>
    <tr>
      <td style=' text-align: center; width:15%; ' rowspan='2'>NAMA PELAJAR</td>
      <td style=' text-align: center; width:10%; ' rowspan='2'>NO.KAD PENGANALAN</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>NO MATRIK</td>
      <td style=' text-align: center; width:10%; ' colspan='2'>TEMPOH TAJAAN</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>NAMA PROGRAM</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>STATUS</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 2 2013/2014 (A132) Februaria 2014</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 1 2014/2015 (A141) September 2014</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 2 2014/2015 (A142) Februaria 2015</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 1 2015/2016 (A151) September 2015</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 2 2015/2016 (A152) Februaria 2016</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>SEMESTER 1 2016/2017 (A161) September 2016</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>JUMLAH (RM)</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>JENIS TUNTUTAN</td>
      <td style=' text-align: center; width:5%; ' rowspan='2'>CATATAN</td>
    </tr>
    <tr>
      <td>MULA</td>
      <td>TAMAT</td>
    </tr>
    <tr>
      <td style=' text-align: center; width:15%; ' >NAMA PELAJAR</td>
      <td style=' text-align: center; width:10%; ' >NO.KAD PENGANALAN</td>
      <td style=' text-align: center; width:5%; ' >NO MATRIK</td>
      <td></td>
      <td></td>
      <td style=' text-align: center; width:5%; ' >NAMA PROGRAM</td>
      <td style=' text-align: center; width:5%; ' >STATUS</td>
      <td style=' text-align: center; width:5%; ' >SEMESTER 2 2013/2014 (A132) Februaria 2014</td>
      <td style=' text-align: center; width:5%; ' >SEMESTER 1 2014/2015 (A141) September 2014</td>
      <td style=' text-align: center; width:5%; ' >SEMESTER 2 2014/2015 (A142) Februaria 2015</td>
      <td style=' text-align: center; width:5%;'>SEMESTER 1 2015/2016 (A151) September 2015</td>
      <td style=' text-align: center; width:5%; ' >SEMESTER 2 2015/2016 (A152) Februaria 2016</td>
      <td style=' text-align: center; width:5%; ' >SEMESTER 1 2016/2017 (A161) September 2016</td>
      <td style=' text-align: center; width:5%; ' >JUMLAH (RM)</td>
      <td style=' text-align: center; width:5%; ' >JENIS TUNTUTAN</td>
      <td style=' text-align: center; width:5%; ' >CATATAN</td>
    </tr>
  </table>
  Kan data pelajara diatas adalah merupakan maklumat pelajara yang telah/sedang dalam proses penawaran dan pelajar sedia ada/lanjutan yang dilushkan oleh Bahagian Biasiswa KPT.<br><br>
  <table width='100%'>
    <tr>
      <td style='width: 30%;text-align: left;'>
        Disadiakan oleh:<br>
        Cop & tandatangan<br><br>
        Tarikh:
      </td>
      <td style='width: 20%;text-align: right;'>
        Disadiakan oleh:<br>
        Cop & tandatangan<br><br>
        Tarikh:
      </td>
      <td style='width: 50%;text-align: center;'>
        Noor Nazila Nazren<br>
        Assistant Bursar<br>
        U-Assist<br>
        Universiti Utara Malaysia
      </td>
    </tr>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}