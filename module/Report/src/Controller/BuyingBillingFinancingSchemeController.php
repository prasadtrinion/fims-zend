<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class BuyingBillingFinancingSchemeController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }


    public function BuyingBillingFinancingSchemeAction()
    {
        

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

    
        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        	<tr>
            	<td style='text-align: center'><font size='4'><b>SKIM PEMBIAYAAN PEMBELIAN BASIKAL UUM </b><br>BORANG PERSETUJUAN MEMBELLI BASIKAL</font></td>
            </tr>
        </table>

        <br><br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
        	<tr>
            	<td style='text-align: left'  colspan='3' width='100%'><font size='4'><b><u>BUTIRAN PEMOHON</u></b></font></td>
            </tr>
            <tr>    
            	<td style='text-align: left' width='30%'><font size='4'>NAMA</font></td>
            	<td style='text-align: left' width='70%' colspan='2' ><font size='4'>$name</font></td>
            </tr>  
            <tr>
            	<td style='text-align: left' width='30%'><font size='4'>NO KAD PENGENALAN</font></td>
            	<td style='text-align: left' width='35%'><font size='4'></font></td>
            	<td style='text-align: left' width='35%'><font size='4'>ALAMAT PENGHANTARAN</font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='30%'><font size='4'>NO STAF</font></td>
            	<td style='text-align: left' width='35%'><font size='4'>2222854</font></td>
            	<td style='text-align: left' width='35%' rowspan='2'><font size='4'>JABATAN PENDAFTAR,UUM<br>06010 SINTOK, KEDAH </font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='30%'><font size='4'>PUSAT TANGGUNGJAWAB</font></td>
            	<td style='text-align: left' width='35%'><font size='4'>$JABATAN PENDAFTAR</font></td>
            </tr> 
         </table>
         <br>

         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        	<tr>
            	<td style='text-align: left'  colspan='2'><font size='4'><b><u>PESANAN PEMBELIAN</u></b></font><br><font size='4'>SILA TUAN BEKALKAN BASIKAL SEPERTI BERIKUT KE ALAMAT DI ATAS</font></td>
            </tr>
        </table>
        
        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'> 
            <tr>
                <td style='text-align: left'  width='40%'><font size='4'><b>SPESIFIKASI</b></font></td>
                <td style='text-align: left'  width='60%'><font size='4'><b>HARGA(RM)</b></font></td>
            </tr>
            <tr>
                <td style='text-align: left'  width='40%'><font size='4'>BMX 123</font></td>
                <td style='text-align: left'  width='60%'><font size='4'>HARGA BELIAN(RM)<br>MENGIKUT SEBUTHARGA:5,492,00</font></td>
            </tr>
            <tr>
                <td style='text-align: left'  width='40%'><font size='4'><br><br></font></td>
                <td style='text-align: left'  width='60%'><font size='4'><br><br></font></td>
            </tr>
            <tr>
                <td style='text-align: center'  width='40%'><font size='4'>TANDATANGAN</font></td>
                <td style='text-align: center'  width='60%'><font size='4'>TARIHK</font></td>
            </tr>
        </table>
        <br><br>
         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        	<tr>
            	<td style='text-align: left' width='100%'><font size='4'><b><u>UNTUK DISIL OLEH PEMBEKALAN</u></b><br> &nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;&nbsp; PENGESAHAN PESENAN BELIAN<br>DENGAN INI DISAHKAN BAHAWA PESANAN TELAH DIBUAT OLEH PEMOHON SEPERTI DINYATAKAN DI RUANGAN A DI ATAS</font></td>
            </tr>
        </table>
        <br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
        <tr>
                <td style='text-align: left' width='50%'><font size='4'><br><br></font></td>
                <td style='text-align: left' width='50%'><font size='4'><br><br></font></td>
            </tr> 
            <tr>
            	<td style='text-align: left' width='70%'><font size='4'>TANDATANGAN & COP PEMBEKAL</font></td>
            	<td style='text-align: left' width='30%'><font size='4'>TARIKH</font></td>
            </tr>
         </table>

         <br><br>
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        	<tr>
            	<td style='text-align: left' width='100%'><font size='4'><b><u>UNTUK KENGUNAAN JABATAN BENDAHARI</u></b></font></td>
            </tr>
        </table>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'> 
            <tr>
            	<td style='text-align: left' width='100%' colspan='2'><font size='4'>DENGAN INI DISAHKAN BAHAWA PEMBELI BERKANAAN TELAH DILULUSKAN PEBIAYAAN SEBANYAK RM________ UNTUK PERBELIAAN BASIKAL DENGAN SYARIKAT TUAN. PERBEZAAN HARGA HARGA BASIKAL DENGAN AMAUN PEMBIAYAAN UUM SEBANYAK RM_______ HENDAKLAN DITUNTU DARIPADA PEMBELI BERKENAAN .<br><br>SILA BEKALKAN BARANG SEPERTI DINYATAKAN  DI RUANGAN A DI ATAS</font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='50%'><font size='4'><br><br></font></td>
            	<td style='text-align: left' width='50%'><font size='4'><br><br></font></td>
            </tr>
            <tr>
            	<td style='text-align: center' width='50%'><font size='4'>TANDATANGAN</font></td>
            	<td style='text-align: center' width='50%'><font size='4'>TARIKH</font></td>
            </tr>
            </table>";

     	

            
                 $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}