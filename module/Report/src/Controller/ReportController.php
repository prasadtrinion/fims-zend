<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportController extends AbstractAppController
{
    protected $sm;
    public $columnNames = [
        "f014fid"              => "GL Code Id",
        "f015fid"              => "Department Code Id",
        "f057fid"              => "Fund Code Id",
        "f058fid"              => "Activity Code Id",
        "f059fid"              => "Account Code Id",
        "f014fgl_code"         => "GL Code",
        "f014ffund_id"         => "Fund Id",
        "f014factivity_id"     => "Activity Id",
        "f014fdepartment_id"   => "Department Id",
        "f014faccount_id"      => "Account Id",
        "f057fname"            => "Fund Name",
        "f057fcode"            => "Fund Code",
        "f058fcode"            => "Activity Code",
        "f058fname"            => "Activity Name",
        "f058ftype"            => "Activity Type",
        "f058fdescription"     => "Activity Description",
        "f058fshort_code"      => "Activity Short Code",
        "f058fparent_code"     => "Activity parent Code",
        "f058flevel_status"    => "Activity Level Status",
        "f058fcomplete_code"   => "Activity Complete Code",
        "f015fdepartment_name" => "Department Name",
        "f015fdepartment_code" => "Department Code",
        "f059fcode"            => "Account Code",
        "f059fname"            => "Account Name",
        "f059ftype"            => "Account Type",
        "f059fdescription"     => "Account Description",
        "f059fshort_code"      => "Account Short Code",
        "f059fref_code"        => "Account Reference Code",
        "f059fparent_code"     => "Account Parent Code",
        "f059flevel_status"    => "Account Level Status",
        "f059fcomplete_code"   => "Account Complete Code",
        "item_name"            => "Item Name",
        "f072fquantity"        => "Quantity",
        "f072fprice"           => "Price",
        "f072ftotal"           => "Total",
        "gst_code"             => "GST",
        "cr_amount"            => "Credit Amount",
        "dr_amount"            => "Debit Amount",
        ""                     => "",
    ];
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function create($postData)
    {
        $request = $this->getRequest();
        $em      = $this->getEntityManager();
        $rawBody = file_get_contents("php://input");

        $reportRepository = $this->getRepository('T019fsavedReports');

        if ($postData['download'] == "no") {
            //$reportRepository->createNewData($postData);
        }
        $reportResult = $reportRepository->getData($postData);

        $file_data = "<br><br><br><br><br><table style='font-size:15px;' border=1><tr>";
        $header    = $reportResult[0];

        foreach ($header as $key => $value) {
            $file_data = $file_data . "<th>" . $this->columnNames[$key] . "</th>";
        }
        $file_data = $file_data . "</tr>";

        foreach ($reportResult as $report) {
            $file_data = $file_data . "<tr>";
            foreach ($report as $key => $value) {
                $file_data = $file_data . "<td>" . $value . "</td>";
            }
            $file_data = $file_data . "</tr>";
        }
        $file_data = $file_data . "</table>";

        if ($postData['download'] == "yes") {
            if ($postData['type'] == "pdf") {
                $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
            } else {
                $name = gmdate("YmdHis") . ".xls";
                $this->generateCsv($reportResult, $name);
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
            }
        } else {
            return new JsonModel([
                'status' => 200,
                'result' => $reportResult,
            ]);
        }

    }

    public function invoiceReportAction()
    {
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $reportRepository = $this->getRepository('T019fsavedReports');

        $reportResult   = $reportRepository->getInvoiceData($postData);
        $invoice_number = $reportResult[0]['f071finvoice_number'];
        $invoice_date   = $reportResult[0]['f071finvoice_date'];
        $invoice_date   = date("d/m/Y", strtotime($invoice_date));
        $file_data      = "<html><body>    <center>        <h3 style='text-align:center;'>NO.PENDAFTARAN GST:000326074368</h3><br>        <h2 style='text-align:center;'><b>INVOIS BERCUKAI (TAX INVOICE)</b></h2>    </center>    <table border='1' align='center'>        <tr>            <td rowspan='4' colspan='3'>                <b>KEPADA:</b><br>                PUSAT KESIHATAN UNIVERSITI<br>                UNIVERSITI UTARA MALAYSIA<br>                06010 SINTOK<br>                KEDAH            </td>        </tr>        <tr>            <td>                NO. INVOIS            </td>            <td>                $invoice_number            </td>        </tr>        <tr>            <td>                KOD PENGHUTANG            </td>            <td>                D11            </td>        </tr>        <tr>            <td>                TARIKH            </td>            <td>                $invoice_date            </td>        </tr>        <tr>            <td><b>BUTIRAN TUNTUTAN</b></td>            <td><b>AMAUN (RM)</b></td>            <td><b>JENIS GST</b></td>            <td><b>AMAUN GST (RM)</b></td>            <td><b>JUMLAH (RM)</b></td>        </tr>        ";
        foreach ($reportResult as $row) {
            $name       = $row['f066fname'];
            $exc_amount = number_format((float) $row['f072ftotal_exc'], 2, '.', '');
            $gst_code   = $row['f081fcode'];
            $tax_amount = number_format((float) $row['f072ftax_amount'], 2, '.', '');
            $total      = number_format((float) $row['f072ftotal'], 2, '.', '');
            $file_data  = $file_data . "<tr style='border:0px;'>            <td style='text-align:center;'>$name</td>            <td style='text-align:right;'>$exc_amount</td>            <td style='text-align:center;'>$gst_code</td>            <td style='text-align:right;'>$tax_amount</td>            <td style='text-align:right;'>$total</td>        </tr>";
        }
        $file_data = $file_data . "        </tr>    </table></body></html>";

        if ($postData['download'] == "yes") {
            if ($postData['type'] == "pdf") {
                $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
            } else {
                $name = gmdate("YmdHis") . ".xls";
                $this->generateCsv($reportResult, $name);
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
            }
        } else {
            return new JsonModel([
                'status' => 200,
                'result' => $reportResult,
            ]);
        }

    }
    public function BalanceReportAction()
    {

        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $reportRepository = $this->getRepository('T019fsavedReports');
        $type             = $postData['type'];

        $fileData = $reportRepository->getBalanceReportData($postData);

        $fileName = gmdate("YmdHis") . ".xlsx";
        $level    = $postData['level'];
        if ($type == "PL") {
            $this->PLReportExcel($fileData, $fileName, $level);
        } else {
            $this->BalanceReportExcel($fileData, $fileName, $level);
        }
 
        return new JsonModel([
            'status' => 200,
            'name'   => $fileName,
        ]);

    }
    public function TBReportAction()
    { 

        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);
        
        $reportRepository = $this->getRepository('T019fsavedReports');
        $type             = $postData['type'];

        $fileData = $reportRepository->getTBData($postData);

        $fileName = gmdate("YmdHis") . ".xlsx";
        $this->TBReportExcel($fileData, $fileName);

        return new JsonModel([
            'status' => 200,
            'name'   => $fileName,
        ]);

    }

    public function BalanceReportExcel($fileData, $fileName, $level)
    {

        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);

        $reportRepository = $this->getRepository('T019fsavedReports');
        $excelData        = array();
        // $gdImage          = imagecreatefromjpeg('/var/www/html/fims-zend/public/download/logo.png');
        $styleArray       = array(
            'font' => array(
                'bold' => true,
            ),
        );
        $excel->getActiveSheet()->setCellValue('G1', 'Universiti Utara Malaysia');
        $excel->getActiveSheet()->setCellValue('G2', 'Imbangan Duga Kumpulan Wang');
        $excel->getActiveSheet()->setCellValue('G3', 'Mengikut Objek Lanjut');
        // $excel->getActiveSheet()->setCellValue('G4', 'Sehingga 31 Disember 2017');

        $excel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($styleArray);

        $objDrawing = new \PHPExcel_Worksheet_Drawing(); //create object for Worksheet drawing
        $objDrawing->setName('Customer Signature'); //set name to image
        $objDrawing->setDescription('Customer Signature'); //set description to image
        $signature = '/var/www/html/fims-zend/public/download/logo.png'; //Path to signature .jpg file
        $objDrawing->setPath($signature);
        $objDrawing->setOffsetX(25); //setOffsetX works properly
        $objDrawing->setOffsetY(2); //setOffsetY works properly
        $objDrawing->setCoordinates('D1'); //set image to cell
        $objDrawing->setWidth(90); //set width, height
        $objDrawing->setHeight(80);
        $objDrawing->setWorksheet($excel->getActiveSheet()); //save

        $path   = "/var/www/html/fims-zend/public/download/" . $fileName;
        $header = array();

        $empty              = array('', '', '', '', '');
        $finalMonthCrAmount = 0;
        $finalMonthDrAmount = 0;
        $finalYearCrAmount  = 0;
        $finalYearDrAmount  = 0;

        foreach ($fileData as $fund) {
            $fundMonthCrAmount = 0;
            $fundMonthDrAmount = 0;
            $fundYearCrAmount  = 0;
            $fundYearDrAmount  = 0;

            // Fund Name
            $fundName = array($fund['f015fdepartment_name'], '', '', '', '');
            array_push($excelData, $fundName);

            // header
            if ($level == 3) {
                $head_row = array('Level One', 'Level Two', 'Level Three', 'Opening Balance', 'Period Credit', 'Period Debit', 'YTD Credit', 'YTD Debit');
            } else if ($level == 2) {
                $head_row = array('Level One', 'Level Two', 'Opening Balance', 'Period Credit', 'Period Debit', 'YTD Credit', 'YTD Debit');
            } else {
                $head_row = array('Level One', 'Opening Balance', 'Period Credit', 'Period Debit', 'YTD Credit', 'YTD Debit');
            }

            array_push($excelData, $head_row);
            // All Periods Sum
            $fundMonthCrAmount += $fund['month_cr_amount'];
            $fundMonthDrAmount += $fund['month_dr_amount'];

            // All YTD Sum
            $fundYearCrAmount += $fund['cr_amount'];
            $fundYearDrAmount += $fund['dr_amount'];

            $finalMonthCrAmount += $fundMonthCrAmount;
            $finalMonthDrAmount += $fundMonthDrAmount;

            $finalYearCrAmount += $fundYearCrAmount;
            $finalYearDrAmount += $fundYearDrAmount;

            foreach ($fund['two_digit_result'] as $one) {

                //Account Name - Level-01
                $name = $reportRepository->getAccountName1($one['one']);

                //Main Account Level-01 Row
                if ($level == 3) {
                    $row = array($one['one'] * 10000 . '-' . $name, '', '', '', '', '', '', '');
                } else if ($level == 2) {
                    $row = array($one['one'] * 10000 . '-' . $name, '', '', '', '', '', '');
                } else {
                    $row = array($one['one'] * 10000 . '-' . $name, '0', $this->isEmpty($one['month_cr_amount']), $this->isEmpty($one['month_dr_amount']), $this->isEmpty($one['cr_amount']), $this->isEmpty($one['dr_amount']));
                }
                array_push($excelData, $row);

                //Fund Final Total Row
                $one_sum_row = array('TOTAL', '', '', '0', $one['month_cr_amount'], $one['month_dr_amount'], $one['cr_amount'], $one['dr_amount']);

                foreach ($one['three_digit_result'] as $two) {
                    //Account Name - Level-02
                    $name = $reportRepository->getAccountName($one['one'], $two['two']);

                    //Main Account Level-02 Row
                    if ($level == 3) {
                        $row = array('', ($one['one'] * 10000 + $two['two'] * 1000) . '-' . $name, '', '', '', '', '', '');
                        array_push($excelData, $row);
                    } else if ($level == 2) {
                        $row = array('', ($one['one'] * 10000 + $two['two'] * 1000) . '-' . $name, '0', $this->isEmpty($two['month_cr_amount']), $this->isEmpty($two['month_dr_amount']), $this->isEmpty($two['cr_amount']), $this->isEmpty($two['dr_amount']));
                        array_push($excelData, $row);
                    } else {

                    }

                    foreach ($two['four_digit_result'] as $three) {
                    $name = $reportRepository->getAccountName2($one['one'], $two['two'],$three['three']);

                        if ($level == 3) {
                            $row = array('', '', ((($one['one'] * 10000) + $two['two'] * 1000) + $three['three']) . '-' . $name, '0', $this->isEmpty($three['month_cr_amount']), $this->isEmpty($three['month_dr_amount']), $this->isEmpty($three['cr_amount']), $this->isEmpty($three['dr_amount']));
                            array_push($excelData, $row);
                        }
                    }

                    // Level - 02 Total Sum Row
                    // $two_sum_row = array('','TOTAL','','0',$two['month_cr_amount'],$two['month_dr_amount'],$two['cr_amount'],$two['dr_amount']);
                    // fputcsv($df1, $empty);
                    // fputcsv($df1, $two_sum_row);

                    array_push($excelData, $empty);

                    // Fund Final Sum Row

                }
                if ($level == 3) {
                    $fund_sum_row = array('TOTAL', '', '', '0', $this->isEmpty($fundMonthCrAmount), $this->isEmpty($fundMonthDrAmount), $this->isEmpty($fundYearCrAmount), $this->isEmpty($fundYearDrAmount));
                } else if ($level == 2) {
                    $fund_sum_row = array('TOTAL', '', '0', $this->isEmpty($fundMonthCrAmount), $this->isEmpty($fundMonthDrAmount), $this->isEmpty($fundYearCrAmount), $this->isEmpty($fundYearDrAmount));
                } else {
                    $fund_sum_row = array('TOTAL', '0', $this->isEmpty($fundMonthCrAmount), $this->isEmpty($fundMonthDrAmount),$this->isEmpty($fundYearCrAmount), $this->isEmpty($fundYearDrAmount));
                }

                // fputcsv($df1, $empty);
                // fputcsv($df1, $one_sum_row);

            }
            array_push($excelData, $fund_sum_row);
            array_push($excelData, $empty);
        }
        // Grand Final Row

        if ($level == 3) {
            $final_sum_row = array('FINAL TOTAL', '', '', '0', $this->isEmpty($finalMonthCrAmount), $this->isEmpty($finalMonthDrAmount), $this->isEmpty($finalYearCrAmount), $this->isEmpty($finalYearDrAmount));
        } else if ($level == 2) {
            $final_sum_row = array('FINAL TOTAL', '', '0', $this->isEmpty($finalMonthCrAmount), $this->isEmpty($finalMonthDrAmount), $this->isEmpty($finalYearCrAmount), $this->isEmpty($finalYearDrAmount));
        } else {
            $final_sum_row = array('FINAL TOTAL', '0', $this->isEmpty($finalMonthCrAmount), $this->isEmpty($finalMonthDrAmount), $this->isEmpty($finalYearCrAmount), $this->isEmpty($finalYearDrAmount));
        }
        array_push($excelData, $final_sum_row);
        $row_count = $excel->getActiveSheet()->getHighestRow() + 2;

        // print_r($excelData);
        // die();
        foreach ($excelData as $row) {
            $excel->getActiveSheet()->fromArray($row, null, 'A' . $row_count);
            $row_count++;
        }

        $excel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(false);

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($path);
    }
    public function isEmpty($value){
        if((int)$value == 0){
            return '0';
        }
        else{
            return $value;
        }
    }
    public function TBReportExcel($fileData, $fileName)
    {

        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);

        $reportRepository = $this->getRepository('T019fsavedReports');
        $excelData        = array();
        // $gdImage = imagecreatefromjpeg('/var/www/html/fims-zend/public/download/logo.png');
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
        );
        $excel->getActiveSheet()->setCellValue('G1', 'Universiti Utara Malaysia');
        $excel->getActiveSheet()->setCellValue('G2', 'Imbangan Duga Kumpulan Wang');
        $excel->getActiveSheet()->setCellValue('G3', 'Mengikut Objek Lanjut');
        $excel->getActiveSheet()->setCellValue('G4', 'Sehingga 31 Disember 2017');

        $excel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($styleArray);

        $objDrawing = new \PHPExcel_Worksheet_Drawing(); //create object for Worksheet drawing
        $objDrawing->setName('Customer Signature'); //set name to image
        $objDrawing->setDescription('Customer Signature'); //set description to image
        $signature = '/var/www/html/fims-zend/public/download/logo.png'; //Path to signature .jpg file
        $objDrawing->setPath($signature);
        $objDrawing->setOffsetX(25); //setOffsetX works properly
        $objDrawing->setOffsetY(2); //setOffsetY works properly
        $objDrawing->setCoordinates('D1'); //set image to cell
        $objDrawing->setWidth(90); //set width, height
        $objDrawing->setHeight(80);
        $objDrawing->setWorksheet($excel->getActiveSheet()); //save

        $path   = "/var/www/html/fims-zend/public/download/" . $fileName;
        $header = array('Account Name', 'Account Code', 'Month Credit Amount', 'Month Debit Amount', 'Year Credit Amount', 'Year Debit Amount');
        array_push($excelData, $header);
        $empty        = array('', '', '', '', '');
        $totalCrMonth = 0;
        $totalDrMonth = 0;
        $totalCrYear  = 0;
        $totalDrYear  = 0;
        foreach ($fileData as $row) {
            $totalCrMonth = $totalCrMonth + $row['cr_amount'];
            $totalDrMonth = $totalDrMonth + $row['dr_amount'];
            $totalCrYear  = $totalCrYear + $row['year_cr_sum'];
            $totalDrYear  = $totalDrYear + $row['year_dr_sum'];
            $one          = array($row['f059fname'], $row['f018faccount_code'], $row['cr_amount'], $row['dr_amount'], $row['year_cr_sum'], $row['year_dr_sum']);
            array_push($excelData, $one);
        }
        $final_sum = array('Total', '', $totalCrMonth, $totalDrMonth, $totalCrYear, $totalDrYear);
        array_push($excelData, $final_sum);

        // print_r($excelData);
        // die();
        $row_count = $excel->getActiveSheet()->getHighestRow() + 2;
        foreach ($excelData as $row) {
            $excel->getActiveSheet()->fromArray($row, null, 'A' . $row_count);
            $row_count++;
        }

        $excel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(false);

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($path);
    }

    public function PLReportExcel($fileData, $fileName, $level)
    {

        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);

        $reportRepository = $this->getRepository('T019fsavedReports');
        $excelData        = array();
        // $gdImage          = imagecreatefromjpeg('/var/www/html/fims-zend/public/download/logo.png');
        $styleArray       = array(
            'font' => array(
                'bold' => true,
            ),
        );
        $excel->getActiveSheet()->setCellValue('G1', 'Universiti Utara Malaysia');
        $excel->getActiveSheet()->setCellValue('G2', 'Imbangan Duga Kumpulan Wang');
        $excel->getActiveSheet()->setCellValue('G3', 'Mengikut Objek Lanjut');
        $excel->getActiveSheet()->setCellValue('G4', 'Sehingga 31 Disember 2017');

        $excel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G2')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($styleArray);
        $excel->getActiveSheet()->getStyle('G4')->applyFromArray($styleArray);

        $objDrawing = new \PHPExcel_Worksheet_Drawing(); //create object for Worksheet drawing
        $objDrawing->setName('Customer Signature'); //set name to image
        $objDrawing->setDescription('Customer Signature'); //set description to image
        $signature = '/var/www/html/fims-zend/public/download/logo.png'; //Path to signature .jpg file
        $objDrawing->setPath($signature);
        $objDrawing->setOffsetX(2); //setOffsetX works properly
        $objDrawing->setOffsetY(2); //setOffsetY works properly
        $objDrawing->setCoordinates('D1'); //set image to cell
        $objDrawing->setWidth(90); //set width, height
        $objDrawing->setHeight(80);
        $objDrawing->setWorksheet($excel->getActiveSheet()); //save

        $path   = "/var/www/html/fims-zend/public/download/" . $fileName;
        $header = array();

        $empty              = array('', '', '', '', '');
        $finalMonthCrAmount = 0;
        $finalMonthDrAmount = 0;
        $finalYearCrAmount  = 0;
        $finalYearDrAmount  = 0;

        foreach ($fileData as $fund) {
            $fundMonthCrAmount = 0;
            $fundMonthDrAmount = 0;
            $fundYearCrAmount  = 0;
            $fundYearDrAmount  = 0;

            // Fund Name
            $fundName = array($fund['f015fdepartment_name'], '', '', '', '');
            array_push($excelData, $fundName);

            // header
            if ($level == 3) {
                $head_row = array('Level One', 'Level Two', 'Level Three', 'Opening Balance', 'Year Total');
            } else if ($level == 2) {
                $head_row = array('Level One', 'Level Two', 'Opening Balance', 'Year Total');
            } else {
                $head_row = array('Level One', 'Opening Balance', 'Year Total');
            }

            array_push($excelData, $head_row);

            foreach ($fund['two_digit_result'] as $one) {

                // All Periods Sum
                // $fundMonthAmount += abs($one['month_cr_amount'] - $one['month_dr_amount']);

                // // All YTD Sum
                // $fundYearAmount += abs($one['cr_amount'] - $one['dr_amount']);

                //Account Name - Level-01
                $name = $reportRepository->getAccountName1($one['one']);

                //Main Account Level-01 Row
                if ($level == 3) {
                    $row = array($one['one'] * 10000 . '-' . $name, '', '', '', '', '');
                } else if ($level == 2) {
                    $row = array($one['one'] * 10000 . '-' . $name, '', '', '', '');
                } else {
                    $row = array($one['one'] * 10000 . '-' . $name, '0', abs($one['cr_amount'] - $one['dr_amount']));
                }
                array_push($excelData, $row);

                //Fund Final Total Row
                $one_sum_row = array('TOTAL', '', '', '0', abs($one['cr_amount'] - $one['dr_amount']));

                foreach ($one['three_digit_result'] as $two) {
                    //Account Name - Level-02
                    $name = $reportRepository->getAccountName($one['one'], $two['two']);

                    //Main Account Level-02 Row
                    if ($level == 3) {
                        $row = array('', ($one['one'] * 10000 + $two['two'] * 1000) . '-' . $name, '', '', '', '', '', '');
                        array_push($excelData, $row);
                    } else if ($level == 2) {
                        $row = array('', ($one['one'] * 10000 + $two['two'] * 1000) . '-' . $name, '0', abs($two['cr_amount'] - $two['dr_amount']));
                        array_push($excelData, $row);
                    } else {

                    }

                    foreach ($two['four_digit_result'] as $three) {
                    $name = $reportRepository->getAccountName2($one['one'], $two['two'],$three['three']);
                        if ($level == 3) {
                            $row = array('', '', ((($one['one'] * 10000) + $two['two'] * 1000) + $three['three']) . '-' . $name, '0', abs($three['cr_amount'] - $three['dr_amount']));
                            array_push($excelData, $row);
                        }
                    }

                    // Level - 02 Total Sum Row
                    // $two_sum_row = array('','TOTAL','','0',$two['month_cr_amount'],$two['month_dr_amount'],$two['cr_amount'],$two['dr_amount']);
                    // fputcsv($df1, $empty);
                    // fputcsv($df1, $two_sum_row);

                    array_push($excelData, $empty);

                    // Fund Final Sum Row

                }
                if ($level == 3) {
                    $fund_sum_row = array('TOTAL', '', '', '0', $fundYearAmount);
                } else if ($level == 2) {
                    $fund_sum_row = array('TOTAL', '', '0', $fundYearAmount);
                } else {
                    $fund_sum_row = array('TOTAL', '0', $fundYearAmount);
                }

                // fputcsv($df1, $empty);
                // fputcsv($df1, $one_sum_row);

            }
            $fundMonthAmount += abs($fund['month_cr_amount'] - $fund['month_dr_amount']);

            // All YTD Sum
            $fundYearAmount += abs($fund['cr_amount'] - $fund['dr_amount']);

            $finalMonthAmount += $fundMonthAmount;

            $finalYearAmount += $fundYearAmount;
            // array_push($excelData, $fund_sum_row);
            // array_push($excelData, $empty);
        }
        // Grand Final Row

        if ($level == 3) {
            $final_sum_row = array('FINAL TOTAL', '', '', '0', $finalYearAmount);
        } else if ($level == 2) {
            $final_sum_row = array('FINAL TOTAL', '', '0', $finalYearAmount);
        } else {
            $final_sum_row = array('FINAL TOTAL', '0', $finalYearAmount);
        }
        array_push($excelData, $final_sum_row);
        $row_count = $excel->getActiveSheet()->getHighestRow() + 2;

        // print_r($excelData);
        // die();
        foreach ($excelData as $row) {
            $excel->getActiveSheet()->fromArray($row, null, 'A' . $row_count);
            $row_count++;
        }

        $excel->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('B')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('C')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('D')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('E')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('F')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('G')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('H')->setAutoSize(false);
        $excel->getActiveSheet()->getColumnDimensionByColumn('I')->setAutoSize(false);

        $objWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $objWriter->save($path);
    }
    public function tenderAwardAction()
    {

        $rawBody  = file_get_contents("php://input");

        $reportRepository = $this->getRepository('T019fsavedReports');
        $id = $this->params()->fromRoute('id');

        $tenderdata = $reportRepository->getTenderData((int)$id);
        // print_r($tenderdata);exit;

        $title = $tenderdata['f035ftitle'];
        $quotationNo = $tenderdata['f035fquotation_number'];
        $total = $tenderdata['f035ftotal_amount'];

        $total = number_format($total, 2, '.', ',');

        $pay_date = date('mdY-his', time());

        $currentDate = date('d/m/Y h:i:s a', time());              
        $currentDate   = date("d/m/Y", strtotime($currentDate));

        $bend = "BEND" . $pay_date ; 
        




        $file_data = "
<html>

<body>
    <table width='100%'>
        <tr>
            <td style='text-align:right'>$bend
                <br/>$currentDate </td>
        </tr>
    </table>
    <table width='100%'>
        <tr>
            <td style='text-align:left'>Managing Director
                <br/>Topbit System Sdn Bhd
                <br/>No. 47G, Bangunan Tabung Haji
                <br/>Jalan Perai Utama 1, Taman Perai Utama
                <br/>13600 Perai, Pulau Pinang
                <br/> </td>
        </tr>
    </table>
    <table width='100%'>
    <tr>
    <td><br></td>
    </tr>
        <tr>
            <td>$title </td>
        </tr>
        <tr>
            <td>TENDER NO: $quotationNo</td>
        </tr>
        <tr>
        <td><br></td>
        </tr>
        <tr>
            <td>I respectfully referred to the above matter.</td>
        </tr>
        <tr>
            <td>2.Please be informed that Universiti Utara Malaysia has agreed to offer the above tender(Package A and Package B) to your company at the following rates The overall price of RM $total (including GST)</td>
        </tr>
        <tr>
            <td>3.Please enclose the execution bonds either in the form of cash, bank checks or bankguarantees worth</td>
        </tr>
        <tr>
            <td>4.The offer letter is part of the contract being prepared. This letter is sent in two (2) copies.Please return the original copy of the Letter of Acceptance to the Treasurer Department afterbeing signed and witnessed in full within two (2) weeks from the date of this letter.</td>
        </tr>
        <tr>
            <td>5.Perhatian dan kerjasama pihak tuan dalam hal ini, saya dahului dengan ucapan terimakasih.</td>
        </tr>
    </table>
    <table>
    <tr>
    <td><br></td>
    </tr>
        <tr>
            <td>SERVICE FOR COUNTRY
                <br/>KEDAH AMAN MAKMUR-HARAPAN BERSAMA MAKMURKAN KEDAH
                <br/>ILMU BUDI BAKTI
                <br/>I who am following orders
                <br/>(HJ AMRON MAN)
                <br/>Treasurer
                <br/>Universiti Utara Malaysia
                <br/>s.k: Director
                <br/>UUM Information Technology (UUMIT)
                <br/>
            </td>
        </tr>
    </table>
</body>

</html>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);

    }
    public function generatePdf($pdfData, $name)
    {
        $em = $this->getEntityManager();
        // $query = "select f011faddress1,f011faddress2 from t011finitial_config where f011fid = 1";
        // $select = $em->getConnection()->executeQuery($query);
        // $result = $select->fetch();

        // $address1 = $result['f011faddress1'];
        // $address2 = $result['f011faddress2'];

        $mpdf = new Mpdf();
        //metadata
        $mpdf->setTitle('Sample PDF');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');
        //header
        // $mpdf->setHeader("$address1</strong><br><small style='font-size:15px;'>$address2</p></small>");
        //data
        $pdfData = mb_convert_encoding($pdfData, 'UTF-8', 'UTF-8');
        $mpdf->WriteHTML($pdfData);
        //footer
        // $mpdf->setFooter("<h3 align='center'>&copy;Trinion Technologies 2018</h3>");
        $path = "/var/www/html/fims-zend/public/download/" . $name;

        $mpdf->Output($path, 'F');
        // $mpdf->Output();
    }
    public function generateCsv($fileData, $name)
    {
        ob_start();
        $path   = "/var/www/html/fims-zend/public/download/" . $name;
        $df1    = fopen($path, 'w');
        $header = array();

        foreach (array_keys(reset($fileData)) as $key) {
            array_push($header, $this->columnNames[$key]);
        }
        //fputcsv($df, $header);
        fputcsv($df1, $header);
        $totalAmount = 0;
        foreach ($fileData as $row) {
            $totalAmount = $totalAmount + $row['f072ftotal'];
            fputcsv($df1, $row);
        }
        $row = array('', '', '', '', 'Total', $totalAmount);
        fputcsv($df1, $row);
        fclose($df1);

    }

    public function getList()
    {

        $request          = $this->getRequest();
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result           = $reportRepository->getList();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function get($id)
    {

        $em      = $this->getEntityManager();
        $request = $this->getRequest();

        $reportRepository = $this->getRepository('T019fsavedReports');
        $report           = $reportRepository->find($id);
        if (!$report) {
            return $this->resourceNotFound();
        }
        $result = $reportRepository->getListById((int) $id);
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function update($id, $postData)
    {

        $em               = $this->getEntityManager();
        $request          = $this->getRequest();
        $reportRepository = $this->getRepository('T019fsavedReports');
        $report           = $reportRepository->find($id);

        if (!$report) {
            return $this->resourceNotFound();
        }

        $rawBody = file_get_contents("php://input");

        if ($this->isJSON($rawBody)) {

            $reportObj = $reportRepository->updateReport($report, $postData);
            return new JsonModel([
                'status'  => 200,
                'message' => 'Updated successfully',
            ]);
        } else {
            return $this->invalidInputType();
        }

    }

    public function gstAction()
    {
        $em               = $this->getEntityManager();
        $request          = $this->getRequest();
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result           = $reportRepository->getGst();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }
    public function itemAction()
    {
        $em               = $this->getEntityManager();
        $request          = $this->getRequest();
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result           = $reportRepository->getItem();
        return new JsonModel([
            'status' => 200,
            'result' => $result,
        ]);
    }

    public function htmlReport2Action()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $fileData         = $reportRepository->getHtml1ReportData((int) $postData['id']);
        $currentDate      = date('d F Y');

        $file_data = "<html>   <head>   </head>   <body>      <table align='right'>         <tr>            <td>Ruj.Kami: BEND20180930-KB1671</td>         </tr>         <tr>            <td>Tarikh: $currentDate</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>            <td style='padding-left: 10%;'>Ahli Jawatankuasa Penyimpan Wang Lebihan</td>         </tr>         <tr>            <td style='padding-left: 10%;'>Universiti Utara Malaysia</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>Y.Bhg. Prof./Tuan,</td>         </tr>      </table>      <table align='center' style='border:0px solid black;width: 80%;font-weight: bold;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>CADANGAN PELABURAN SIMPANAN TETAP</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td><b>1. TUJUAN </b></td>         </tr>         <tr>            <td>            <td></td>            <td>               Mendapatkan kelulusan melabur sejumlah RM30 juta ke dalam simpanan tetap di bank-bank berikut :            </td>         </tr>      </table>      <br><br>      <table  align='center' border='1' style='border-collapse:collapse;width:80%;height:50px;text-align:center;margin-top:-1%;'>         <tr>            <th>BIL.</th>            <th>NAMA BANK</th>            <th>KADAR KEUNTUNGAN (ISLAMIC)</th>            <th>JUMLAH (RM)</th>            <th>CADANGAN PELABURAN</th>         </tr>";

        $i     = 1;
        $one   = $fileData['one'];
        $two   = $fileData['two'];
        $three = $fileData['three'];
        $total = 0;
        foreach ($fileData['data'] as $item) {
            $name      = $item['f061fname'];
            $branch    = $item['f061fbranch'];
            $date      = $item['f063fmaturity_date'];
            $amount    = number_format((float) $item['f063famount'], 2);
            $duration  = $item['f063fduration'];
            $profit    = $item['f063fprofit_rate'];
            $total     = $total + (float) $item['f063famount'];
            $file_data = $file_data . "<tr><td>$i </td><td>$name ($branch)</td><td>$profit %</td><td>$amount</td><td>$duration </td></tr>";
            $i++;
        }
        $total     = number_format((float) $total, 2);
        $file_data = $file_data . "<tr><td></td><td></td><td><b>JUMLAH</d></td><td><b>$total </b></td><td></td></tr></table><br><br><table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>               2.  Apabila setiap pelaburan di atas telah matang, dan sekiranya Ahli Jawatankuasa Penyimpan Wang Lebihan tidak meluluskan sebarang pengeluaran pelaburan tersebut, amaun pokok pelaburan tersebut boleh diperbaharui mengikut pelaburan asal (Islamic) dengan tempoh yang sama dan kadar yang tidak kurang dari pelaburan asal. Sekiranya kadaryang ditawarkan adalah lebih rendah dari kadar pelaburan asal, pihak bank perlu memaklumkan/mendapatkan persetujuan pihak Bendahari UUM terlebih dahulu. <br>3.   Keputusan Jawatankuasa Penyimpan Wang Lebihan Universiti Utara Malaysia.            </td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>               Dipersetujui.            </td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>            <td><b> $one</b></td>         </tr>         <tr>            <td>Naib Canselor </td>         </tr><hr>         <tr>            <td> <b> $two</b></td>         </tr>         <tr>            <td> Pendaftar </td>         </tr>    <hr>     <tr>            <td> <b>$three</b></td>         </tr>         <tr>            <td>Bendahari </td>         </tr> <hr>     </table>";

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);

    }

    public function htmlReport1Action()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $fileData         = $reportRepository->getHtml1ReportData((int) $postData['id']);

        $currentDate = date('d/m/Y');
        $file_data   = "<html>   <head>   </head>   <body>      <table align='right'>         <tr>            <td>BEND20180705-KB1</td>         </tr>         <tr>            <td>$currentDate</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>            <td style='padding-left: 10%;'>Ahli Jawatankuasa Penyimpan Wang Lebihan</td>         </tr>         <tr>            <td style='padding-left: 10%;'>Universiti Utara Malaysia</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>Y.Bhg. Dato'/Tuan,</td>         </tr>      </table>      <table align='center' style='border:0px solid black;width: 80%;font-weight: bold;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>CADANGAN PENGELUARAN SIMPANAN TETAP</td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td><b>1.TUJUAN </b></td>         </tr>         <tr>            <td>            <td></td>            <td>               Mendapatkan kelulusan mengeluarkan simpanan tetap UUM di bank-bank berikut dan dimasukkan ke Akaun Semasa UUM (nombor akaun 02093010000010) di Bank Islam Malaysia Berhad, cawangan UUM:-            </td>         </tr>      </table>      <br><br>      <table  align='center' border='1' style='border-collapse:collapse;width:80%;height:50px;text-align:center;margin-top:-1%;'>         <tr>            <th>BIL.</th>            <th>NAMA BANK</th>            <th>NO. SIJIL</th>            <th>TARIKH MATANG</th>            <th>JUMLAH(RM)</th>         </tr>";
        $i           = 1;
        $one         = $fileData['one'];
        $two         = $fileData['two'];
        $three       = $fileData['three'];
        $total       = 0;
        foreach ($fileData['data'] as $item) {
            $name      = $item['f061fname'];
            $branch    = $item['f061fbranch'];
            $date      = $item['f063fmaturity_date'];
            $amount    = number_format((float) $item['f063famount'], 2);
            $total     = $total + (float) $item['f063famount'];
            $total     = $total + $amount;
            $file_data = $file_data . "<tr><td>$i </td><td>$name ($branch)</td><td>000000000000</td><td>$date</td><td>$amount</td></tr>";
            $i++;
        }
        $total = number_format((float) $total, 2, '.', '');

        $file_data = $file_data . "<tr><td></td><td></td><td></td><td><b>JUMLAH</d></td><td><b>$total </b></td></tr></table><br><br><table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>               2. Keputusan Jawatankuasa Penyimpan Wang Lebihan Universiti Utara Malaysia.            </td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>         <tr>            <td>               Dipersetujui.            </td>         </tr>      </table>      <br><br>      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>         <tr>            <td><b> $one</b></td>         </tr>         <tr>            <td>Naib Canselor </td>         </tr><hr>         <tr>            <td> <b> $two</b></td>         </tr>         <tr>            <td> Bendahari </td>         </tr>    <hr>     <tr>            <td> <b>$three</b></td>         </tr>         <tr>            <td>Pemangku Pendaftar </td>         </tr> <hr>     </table>";

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);

    }

    public function studentInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $fileData         = $reportRepository->getStudentInvoiceData((int) $postData['id']);
        // $file_data = "<html>'    <head>'    </head>'    <body>"
        // $currentDate = date('d/m/Y');
        // foreach ($fileData as $student) {
        //     $invoiceNumber = $student['f071finvoice_number'];
        //     $invoiceDate   = $student['f071finvoice_date'];
        //     $invoiceTotal  = $student['f071finvoice_total'];
        //     $balance       = $student['f071fbalance'];

        //     $file_data = "        <table border='1'>'            <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>'                <tr>'                    <td style='padding-left: 30%;'>'                        JABATAN BENDAHARI'                    </td>'                </tr>'                <tr>'                    <td style='padding-left: 30%;'>'                        UNIVERSITI UTARA MALAYSIA'                    </td>'                </tr>'                <tr>'                    <td style='padding-left: 20%;'>'                        08010 UUM ,Sintok,Kedah Darul Aman, Malaysia  Tel :04-9287777/04-92883135'                    </td>'                </tr>'                <tr>'                    <td style='padding-left: 20%;'>'                        Fax:04-9285764/04-9287905(U-Assist) E-Mail: scholarship@uum.edu.my'                    </td>'                </tr>'            </table>'            <table align='center' style='border:0px solid black;;width: 80%;height:50px;margin-top: 4;'>'                <tr>'                    <td style='padding-left: 40%;'>'                        TUNTUTAN BAYARAN (Invoice)'                    </td>'                </tr>'            </table>'            <br>'                <br>'                    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>'                        <tr>'                            <td>'                                NAMA(Name): MUSTAFA A ABDUENABE HASAN'                            </td>'                            <td>'                                INVOICE NO:'                                <br>'                                    (invoice No.)'                                </br>'                            </td>'                            <td>'                                $invoiceNumber'                            </td>'                        </tr>'                        <tr>'                            <td>'                                NO. MATRIK(Matric No): $matricNumber'                            </td>'                            <td>'                                TARIKH :'                                <br>'                                    (Date)'                                </br>'                            </td>'                            <td>'                                $invoiceDate'                            </td>'                        </tr>'                        <tr>'                            <td>'                                NO K.P/NO. PASSPORT (I.C NO/passport No): $passportNumber'                            </td>'                            <td>'                                RUJUKAN KAMI: $ourReferenceNumber'                                <br>'                                    (Our Ref.)'                                </br>'                            </td>'                        </tr>'                        <tr>'                            <td>'                                TEMPOH PENGAJIAN(Peirod of study) : $periodOfStudy'                            </td>'                            <td>'                                RUJUKAN LAIN:'                                <br>'                                    (Other Ref.)'                                </br>'                            </td>'                            <td>'                                $otherReferenceNumber'                            </td>'                        </tr>'                        <tr>'                            <td>'                                TEMPOH TAJAAN(Period of Sponsorship): $periodOfSponsorship'                            </td>'                            <td>'                                BAKI TAJAAN(RM): $balance'                                <br>'                                    (Balance)'                                </br>'                            </td>'                            <td>'                            </td>'                        </tr>'                    </table>'                    <hr>'                        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>'                        </table>'                    </hr>'                </br>'            </br>'        </table>'    </body>'</html>";

        //     foreach ($student['details'] as $item) {
        //         $itemName  = $item['f066fname'];
        //         $amount    = number_format((float) $item['f072ftotal'], 2);
        //         $total     = $total + (float) $item['f072ftotal'];
        //         $total     = $total + $amount;
        //         $file_data = $file_data . "<tr>    <td>    </td>    <td>        <b>            KETERANGAN(Description)        </b>    </td>    <td>        <b>            AMAUN(Amount)(RM)        </b>    </td></tr><tr>    <td style='padding - left:150px'>        1    </td>    <td style='padding - right:400px'>        $itemName($nickName)    </td>    <td>        $amount    </td></tr>";
        //         $i++;
        //     }
        //     $totalAmount = number_format((float) $total, 2, '.', '');

        //     $file_data = $file_data . "<tr>    <td>        <b>            Iumiah(Total)        </b>    </td>    <td>    </td>    <td>        <b>            $totalAmount        </b>    </td></tr><br>    <br>        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>            <tr>                <tr>                    <td>                        Rinngit Malaysia :                    </td>                </tr>                <tr>                    <td>                        RUA RIBU LAPAN RATUS LIMA PULUH LIMA DAN SEN 0 SAHAJA                    </td>                </tr>            </tr>        </table>        <hr>            <br>                <br>                    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>                        <tr>                            <td>                                1. Bayaran menggunakan cek hendakiah dipafang dan dibayar kepada Universiti utara Malayasia.                            </td>                        </tr>                        <tr>                            <td>                                payment nt cheque must be crossed and made payable to universitit Utara Malaysia                            </td>                        </tr>                        <tr>                            <td>                                2. Sila makluumakan ke jabalan Bendahari (seperti alamat di atas) sekiranya bayaran dibual secara pindahan elektronik(EFT).                            </td>                        </tr>                        <tr>                            <td>                                please send the payment advice to the above address if payment is made by electronic fund tranfer(EFT)./td>                            </td>                        </tr>                        <tr>                            <td>                                3. Nama Bank /No. akaun adalah seperti berikut: Bank Islam Malaysia  Berhad Cawangan UUM ,Account No. 02093010000010.Swift Code-BIMBMKYL                            </td>                        </tr>                    </table>                    <br>                        <br>                            <table align='center' style='border:0px solid black;width: 50%;height:50px;margin-top: 4;'>                                <tr>                                    <td>                                        Invois ini adalah cetakan komputer dan lidak mernerlukan landalangan.                                    </td>                                </tr>                                <tr>                                    <td>                                        This invoice is computer generated and no signiture is required.                                    </td>                                </tr>                            </table>                            <br>                                <br>";
        // }
        // print_r($file_data);
        // die();
        $file_data = "<html><head></head><body> <h1>HI</h1></body></html>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);

    }
    public function onlineUpdateAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $reportRepository->updateStatus($postData);
        return new JsonModel([
            'status'  => 200,
            'message' => "Updated Successfully",
        ]);

    }

    public function stdInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        $fileData         = $reportRepository->getStdInvoiceData((int) $postData['id']);        
        // print_r($fileData);
        // exit();


        $invoice_number = $fileData[0]['f071finvoice_number'];
        $invoice_date = $fileData[0]['f071finvoice_date'];
        $invoice_date = date("d/m/y", strtotime($invoice_date));
        $invoice_gst_code = $fileData[0]['f072fgst_code'];
        $invoice_name = $fileData[0]['f066fname'];
        $invoice_code = $fileData[0]['f081fcode'];
        

        $file_data = "<table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
      
      
        <tr>
         <td style='padding-left: 30%;'>JABATAN BENDAHARI</td>
        </tr>
        <tr>
        <td style='padding-left: 30%;'>UNIVERSITI UTARA MALAYSIA</td>
        </tr>
        <tr>
        <td style='padding-left: 20%;'>08010 UUM ,Sintok,Kedah Darul Aman, Malaysia  Tel :04-9287777/04-92883135</td>
      </tr>
      <tr>
         <td style='padding-left: 20%;'>Fax:04-9285764/04-9287905(U-Assist) E-Mail: scholarship@uum.edu.my </td>
      </tr>
      
   </table>

        <table align='center' style='border:0px solid black;;width: 80%;height:50px;margin-top: 4;'>
      
      <tr>
        <td style='padding-left: 40%;'>TUNTUTAN BAYARAN (Invoice)</td>
         
      </tr>
   </table>

      <br><br>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      
      <tr>
           <td>INVOICE NO: $invoice_number </td>
      
         <td>Invoice Date :$invoice_date</td>
      </tr>
      <tr>
         <td>Invoice Name :$invoice_name</td>
         <td> GST Code : $invoice_gst_code</td>
      </tr>
      <tr>
        
         <td> F Code : $invoice_code</td>
      </tr>

    
   </table>
     <hr>
       <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
        <td></td>
         <td><b>QUANTITY </b></td>
           <td><b> PRICE </b></td>
           <td><b> TAX </b></td>
           <td><b> Total </b></td>
           <td><b> Total Amount</b></td>
           <td><b> Total Exe</b></td>
        </tr>
      <tr>
        
   </table>";
   foreach ($fileData as $invoice)
   {
        $invoice_quantity = $fileData[0]['f072fquantity'];
        $invoice_price = $fileData[0]['f072fprice'];
        $invoice_total = $fileData[0]['f072ftotal']; 
        $invoice_amount = $fileData[0]['f072ftax_amount'];
        $invoice_total_exc = $fileData[0]['f072ftotal_exc'];

        $file_data = $file_data."<table align='center' style='border:0px solid black;width:80%;hight:50px;margin-top: 4;'>
        <tr>
         <td style='padding-right: 150px'>$invoice_quantity</td>
         <td style='padding-right: 150px'>$invoice_price</td>
         <td style='padding-right: 150px'>$invoice_total</td>
         <td style='padding-right: 150px'>$invoice_amount</td>
         <td style='padding-right: 150px'>$invoice_total_exc</td>
         </tr>
         </table>



    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td>Rinngit Malaysia :</td></tr><tr>
         <td>RUA RIBU LAPAN RATUS LIMA PULUH LIMA DAN SEN 0 SAHAJA</td>
      </tr>
   </table>
   <hr>
     <br><br>

     <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
    
      <tr>
         <td>1. Bayaran menggunakan cek hendakiah dipafang dan dibayar kepada Universiti utara Malayasia.</td>
       </tr>
       <tr>
          <td>payment nt cheque must be crossed and made payable to universitit Utara Malaysia</td>
         </tr>

      <tr>
         <td>2. Sila makluumakan ke jabalan Bendahari (seperti alamat di atas) sekiranya bayaran dibual secara pindahan elektronik(EFT). </td></tr><tr>
          <td>please send the payment advice to the above address if payment is made by electronic fund tranfer(EFT)./td>
        
         
      </tr>

      <tr>
         <td>3. Nama Bank /No. akaun adalah seperti berikut: Bank Islam Malaysia  Berhad Cawangan UUM ,Account No. 02093010000010.Swift Code-BIMBMKYL </td></tr>        
         
    
   </table>
     
  <br><br>
      
       <table align='center' style='border:0px solid black;width: 50%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Invois ini adalah cetakan komputer dan lidak mernerlukan landalangan.</td></tr><tr>
          <td>This invoice is computer generated and no signiture is required.</td>
      </tr>
   </table>";
   }


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);

    }
    public function activityInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();
        
        $file_data = "<table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
        <tr>
         <td style='padding-left: 30%;'>ACTIVITY CODE</td>
        </tr>
        </table>
        <table align='center' style='border:0px solid black;;width: 80%;height:50px;margin-top: 4;'>
        </table>
        <br><br>
        <tr>
        <td>Complete Code - Activity Name</td>
        </tr>
        </table>
        <hr>
        <table width='100%'>";
       
        foreach ($result as $data)
        {
            $complete_code = $data['f058fcomplete_code'];
            $name = $data['f058fname'];
            $file_data.= "<tr>
            <td>$complete_code - $name</td>
            </tr>";
            
        }

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }

    public function PekelililingPinjamanInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();

        $invoice_number = $vehicle[0]['f043freference_number'];
            $invoice_date   = $vehicle[0]['f043fcreated_dt_tm'];
            $invoice_date   = date("d/m/Y", strtotime($invoice_date));
            $invoice_name   = $vehicle[0]['f020floan_name'];
            $invoice_amount   = $vehicle[0]['f043floan_amount'];
        
        $file_data = "<table style='border:0px solid black;width: 100%;height:30px;margin-top: 4;'>
        <tr>
            <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        <td style='padding-right : 480px'><b>JABATAN BENDAHARI<br><i>BURSAR's OFFICE</i><br></b> Universiti Utara Malaysia<br>06010 UUM SINTOK<br>KEDAH DARUL AMAN<br>MALAYSIA</td>
        <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        </tr>
        <tr>
        <td></td>
        <td></td>
        <td><font size='2'>Tel:604-928 3200/3221/3255<br>Faks(Fax):604-928 3299<br>Laman Web(web):www.uum.edu.my</td>
        </tr>
        </table>
        <hr>    
        <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'>MUAFAKAT  KEDAH</td>
        </tr><tr></tr>
        </table>

        <table align='center' style='border:0px solid black;;width: 100%;height:30px;margin-top: 4;'>
        <tr>
        <td></td>
        <td></td>
        <td><font size='4'>Ruj. Kami: BEND20170507-KB0880<br>Tarikh: $invoice_date</td>
        </tr>
        <tr>  
            <td></td>
            <td style='padding-right : 100px'><font size='4'><b>PEKELILING BENDAHARI BIL.$invoice_date</b><br><br><br></td>
        </tr>
        <tr>
            <td></td>
            <td><font size='4'>Semua Ketua Pusat Tanggungjawab<br>Universiti Ultra Malaysia<br> <br>Tuan/Puan<br><br><br>
            </td>
        </tr>
        </table>
        <table align='center' style='border:0px solid black;;width: 100%;height:30px;margin-top: 4;'>
        <tr>
            <td></td>
            <td><b><font size='4'>HAD AMAUN MAKSIMUM SKIM PEMBIAYAAN PEMBELIAN KENDERAAN-MOTOSIKAL</b><br><br><p>Dengan hormatya merujuk kepada perkara tersebut di atas.<br><br>Adalah dimaklumkan bahawa Mesyuarat Lembaga Pengarah Universiti Ke-106 Bil. $invoice_date telah meluluskan had amaun maksimum skim pembiayaan pembelian kenderaan- motosikal ke RM10,000 (Ringgit Malaysia : Sepuluh Ribu Sahaja) selaras dengan Pekeliling Perbendaharaan WP 9.3/ 2017, Permohonan pembiayaan pembelian kenderaan adalah secara online mengikut portal staf masing-masing.<br><br>Peraturan ini terpakai berkuatkuasa dari tarikh pekeliling ini dikeluarkan.<br><br>Sekian, terima kasih<br><br><b>'BERKHIDMAT UNTUK NEGARA'<br>'ILMU, BUDI, BAKTI'</b><br><br>Saya yang menurut perintah,<br><br><br><br><br><b>(HJ AMRON BIN MAN)</b><br>Bendahari<br><br></p></td>
        </tr>
    </table>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Timbalan-Timbalan Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Edaran</td>
      </tr>
    </table>";
       

        $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }

    public function pekelilingMotosikalInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        // $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();
        
        $file_data = "<table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        <td align='center'><font size='6'>UNIVERSITI UTARA MALAYSIA</font><br><font size='4'>06010 UUM Sintok Kedah Darul Aman Malaysia Tel:604-928 4000</td>
        </tr>
        <td></td>
        <td></td>
        <td></td>
        <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        </tr>
        <tr>
        </table>
        <hr> 
        <table style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'><font size='4'><b>KEDAH SEJAHTERA</b></td>
        </tr><tr></tr>
        </table><br><br>
        <table style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
        <tr>  
            <td></td>
            <td style='padding-left: ' width='10%'></td>
            <td><b>BEND20090615-KB1457</b><br><br><br>15 Jun 2009<br><b>PEKELILING BENDAHARI BIL. 9/2009</b><br> <br>Semua Penolong Naib Canselor/Ketua Jabatan/Pengarah<br>Universlti Utara Malaysia<br><br><br>YBhg. Dato'/Prof/Tuan/Puan<br>
            </td>
        </tr>
        </table>
        <table align='center' style='border:0px solid black;;width: 60%;height:30px;margin-top: 4;'>
        <tr>
            <td></td>
            <td><b><font size='4'>MENERIMAPAKAI SURAT PEKELILING PERBENDAHARAAN BIL. 12/2008<br> PINDAAN HAD KELAYAKAN PINJAMAN/PEMBIAYAAN MEMBELI KERETA<br>BAGI PEGAWAI PERKHIDMATAN AWAM</b><br><br><p>Dengan hormatnya dimaklumkan bahawa Mesyuarat Jawatankuasa Tetap Kewangan<br> ke 143/2008 dan mesyuarat Lembaga Pengarah Universiti<br> Bil. 1/2009 ke 59 telah menerimapakai surat Pekeliling Perbendaharaan bil.<br>12/2008 mengenai pindaan had pinjaman/pembiayaan membeli kereta bagi<br> Pegawai Perkhidmatan Awam.<br><br>Bersama-sama Inl disertakan surat pekelillng berkenaan sepertl di <b>Lamplran</b><br> untuk rujukan staf di pusat tanggungjawab.<br><br>Seklan, terima kasih.<br><br><br><b>ILMU BUDI BAKTI'</b><br><br>Saya yang menurut perintah,<br><br><br><br><br><b>(HJ AMRON BIN MAN)</b><br>Bendahari<br>Universiti Utara Malaysia<br><br></p></td>
        </tr>
    </table>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>Yang Berbahagia Tan Sri Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Yang Berusaha Timbalan-Timbalan Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Edaran</td>
      </tr>
      </table><br><br><br><br><br><br><br><br><br><br><br>
      <table>
      <tr>
        <td>KK/BP/WA1/10/173/1/(2)</td>
      </tr>
      </table>

     <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        </tr>
        </tr>
        </table>


    <table style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'><font size='4'><b>KEMENTERIAN KEWANGAN<br><br>SURAT PEKELILING PERBENDAHARAAN BIL. 12 TAHUN 2008</b></td>
        </tr><tr></tr>
        </table><br><br><br>
        <table style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
        
        <tr>  
            <td></td>
            <td style='padding-left: ' width='2%'></td>
            <td>Semua Ketua Setiausaha Kementerian<br>Semua YB Setiausaha Kerajaan Negeri<br>Semua YB Pegawai Kewangan Negeri<br>Semua etua Jabatan Persekutuan<br><br><br></td>
        </tr>
        </table>
        <table style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'><font size='4'><b>PINDAAN HAD KELAYAKAN PINJAMAN/PEMBIAYAAN<br>MEMBELI KERETA BAGI PEGAWAI PERKHIDMATAN AWAM AM PERSEKUTUAN,<br>PEGAWAI PERKHIDMATAN PENDIDIKAN DAN PERKHIDMATAN POLIS</b></td>
        </tr><tr></tr>  
        </table><br><br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: ' width='10%'><b>1.</b></td>
         <td><b>TUJUAN</b></td>
         </tr>
     </table>


        <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         
         <td>1.1Surat Pekeliling ini bertujuan memaklumkan pindaan had kelayakan
            pinjaman/pembiayaan membeli kereta pegawai Perkhidmatan Awam Am
            Persekutuan, pegawai Perkhidmatan Pendidikan dan Perkhidmatan Polis.</td>
       </tr>
    </table><br>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: ' width='10%'><b>2.</b></td>
         <td><b>PINDAAN HAD KELAYAKAN PINJAMAN/PEMBIAYAAN</b></td>
         </tr>
     </table>

     <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
     <tr>
         
         <td>2.1Had kelayakan pinjaman/pembiayaan mengikut perenggan 5.1 Pekeliling
Perbendaharaan Bil. 7 Tahun 1993, Bil. 4 Tahun 1994, perenggan 6.1
Pekeliling Perbendaharaan Bil. 8 Tahun 1993, Bil. 5 Tahun 1994 dan
perenggan 5 Surat Pekeliling Perbendaharaan Bil. 17 Tahun 2001 adalah
dipinda seperti berikut:alplnaa soperti borlkut</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         <td>(i)Untuk Membeli Kereta</td>
         <td></td>
         <td></td>
         <td>Had Pinjaman/Pembiayaan</td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(a) Jawatan-jawatan Utama dan Jawatan-<br>jawatan Gred Khas atau Timbalan<br>Pesuruhjaya Polis hingga Ketua Polis Negara</td>
         <td></td>
         <td></td>
         <td>RM70,000</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(b)Kumpulan Pengurusan dan Profesional<br>atau Penolong Penguasa Polis<br>hingga Penolong Kanan Pesuruhjaya Polis</td>
         <td></td>
         <td></td>
         <td>RM65,000</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(c)Kumpulan Sokongan atau Ketua<br>Inspektor Polis dan ke bawah<br>(Kelulusan khas bagi gaji RM1,620ke bawah</td>
         <td></td>
         <td></td>
         <td>RM55,000</td>
       </tr>
    </table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(i) Membeli Motosikal</td>
         <td></td>
         <td></td>
         <td>RM5,000</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(i) Membeli Motobot</td>
         <td></td>
         <td></td>
         <td>RM4,000</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(iv) Membeli Enjin Motobot</td>
         <td></td>
         <td></td>
         <td>RM3,500</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(v) Membeli Bot</td>
         <td></td>
         <td></td>
         <td>RM700</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(vi) Membeli Basikal</td>
         <td></td>
         <td></td>
         <td>RM500</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
     <tr>
         
         <td>2.2Syarat kelayakan pinjaman/pembiayaan mengikut perenggan 3 Pekeliling
        Perbendaharaan Bil. 17 Tahun 2001 adalah dipinda seperti berikut:<br><br>A Pegawai Perkhidmatan Pendidikan yang layak memohon
        pinjaman/pembiayaan kereta adalah Pegawai yang berjawatan tetap dan
        sesungguhnya memerlukan kereta untuk menjalankan tugas rasmi
        Mereka yang bergaji pokok tidak kurang daripada RM1,620 darn
        memegang jawatan penting seperti Pengetua, Guru Besar, Penolong
        Kanan, Penyelia Petang, Guru Hal Ehwal Murid dan Ko-kurikulum dan
        Warden Asrama layak memohon.</td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
     <tr>
         
         <td>2.3Permohonan pinjaman/pembiayaan untuk membeli kereta oleh
        seseorang pegawai bergaji kurang daripada RM1,620.00 sebulan hanya
        boleh dipertimbangkan dan diluluskan oleh Perbendaharaan dengan
        syarat:</td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         <td>(i)Ketua Jabatan memperakukan bahawa:</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(a) pegawai itu tidak dapat menjalankan tugas rasmi dengan
        cekapnya setiap hari tanpa menggunakan sebuah kereta
        sendiri atau pun pegawai yang berkenaan telah disahkan oleh
        Lembaga Perubatan (Perintah Am Bab F) sebagai tidak boleh
        menunggang motosikal,</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(b) pegawai mempunyai lesen memandu yang sah,</td>
         <td></td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(c) jumlah amaun potongan bulanan pinjaman tidak boleh
        melebihi YS daripada gaji pokok serta elaun-elaun, dan
        </td>
         <td></td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>(d) jumlah perjalanannya kerana bertugas rasmi adalah melebihi
        daripada 200 km tiap-tiap bulan selama 3 bulan berturut-turut.</td>
         <td></td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         <td>(i) Pegawai yang berkenaan telah diberi kelulusan untuk menuntut
        Elaun Hitungan Batu Kelas C</td>
         <td></td>
       </tr>
    </table><br>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: ' width='10%'><b>3.</b></td>
         <td><b>SYARAT-SYARAT PINJAMAN/PEMBIAYAAN</b></td>
         </tr>
     </table><br>

    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>3.1Pinjaman/pembiayaan dan bayaran perkhidmatan/keuntungan hendaklah
        dibayar balik sebanyak yang sama jumlahnya pada tiap-tiap bulan:</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         <td>(a) Bagi kereta baru tidak melebihi 108 bulan</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 70%;height:50px;margin-top: 4;'>
       <tr>
         <td>(b) Bagi kereta yang sudah terpakai tidak melebihi 96 bulan</td>
         <td></td>
       </tr>
    </table><br>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>3.2Jadual bayaran balik adalah seperti di Lampiran A.</td>
         <td></td>
       </tr>
    </table><br><br><br><br><br><br><br><br><br><br><br><br>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>3.3 Lain-lain syarat pinjaman/pembiayaan sama seperti Pekeliling
        Perbendaharaan Bil. 7 Tahun 1993, Bil. 8 Tahun 1993, Bil. 4 Tahun 1994, Bil. 5 Tahun 1994, Surat Pekeliling Perbendaharaan Bil. 5 Tahun 2001
        dan Bil.17 Tahun 2001.</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>3.4 Surat perjanjian pembiayaan mengikut Prinsip Al-Bai 'Bithaman' Ajiladalah dikehendaki disetem sebanyak RM3.00.</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: ' width='10%'><b>4.</b></td>
         <td><b>PENYEDIAAN BAUCERICEK SEBELUM KENDERAAN DIBELI</b></td>
         </tr>
     </table>

    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>4.1Untuk mengelakkan peminjam menyimpan cek terlalu lama, peminjam
        hendaklah mendapatkan surat pengesahan daripada pihak pembekal
        kereta, rkh sebenar kenderaan dapat dibekalkan dan
        mengemukakannya bersama dengan borang permohonan
        pinjaman/pembiayaan. Penyediaan baucer dan penyerahan cek
        hendaklah berpandukan tarikh kenderaan dibekalkan yang mana baucer
        bayaran boleh dikemukakan kepada pejabat pembayaran sebulan
        sebelum tarikh kenderaan dibekalkan</td>
         <td></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: ' width='10%'><b>5.</b></td>
         <td><b>TARIKH KUATKUASA</b></td>
         </tr>
     </table>

    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>5.1Surat Pekeliling Perbendaharaan ini berkuat kuasa serta-merta mulai
        tarikh ianya dikeluarkan.</td>
         <td></td>
       </tr>
    </table>
     <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>5.2 Dengan berkuat kuasanya Surat Pekeliing Perbendaharaan ini, Surat
        Pekeliling Perbendaharaan Bil. 7 Tahun 1996 dan Bil. 9 Tahun 2000
        adalah terbatal.</td>
         <td></td>
       </tr>
    </table>
    <table style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'><font size='4'>BERKHIDMAT UNTUK NEGARA</td>
        </tr><tr></tr>
        </table>

        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
        <tr>
            <td><font size='4'></b><br><br>Saya yang menurut perintah,<br><br><br><br><br><b>(Tan Sri Dr. Wan Abdul Aziz Wan Abdullah)</b><br>Ketua Setiausaha Perbendaharaan<br>28November 2008<br><br></td>
        </tr>
    </table>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>Yang Berbahagia Tan Sri Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Yang Berusaha Timbalan-Timbalan Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Edaran</td>
      </tr>
      </table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      <table  style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
         <td style='padding-left: 500px' >LAMPIRAN 'A'</td>
        </tr>
        </table>
        <table style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr>
        <td align='center'><font size='4'>JADUAL BAYARAN BALIK BULANAN PINJAMAN<br>DAN BAYARAN PERKHIDMATAN BAGI PINJAMAN BIASA<br>ATAU JUMLAH KEUNTUNGAN MENGIKUT PRINSIP<br>AL-BAI' BITHAMAN AJIL</td>
        </tr><tr></tr>
        </table><br><br>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>Tempoh Pinjaman/Pembiayaan Tahun</td>
         <td></td>
         <td></td>
         <td>Bayaran balik Bulanan Bagi Tiap-tiap RM1,000</td>
       </tr>
    </table><br><br>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
       <tr>
         <td>1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br></td>
         <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
         <td>85.15<br>43.42<br>29.52<br>22.58<br>18.42<br>15.65<br>13.67<br>12.19<br>11.04</td>
       </tr>
    </table>";
       

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }
    public function PekelilingBasikalAction()
    {
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        // $reportRepository = $this->getRepository('T019fsavedReports');

        // $vehicle = $reportRepository->getVehicleLoanList();

        // $invoice_number = $vehicle[0]['f043freference_number'];
        //     $invoice_date   = $vehicle[0]['f043fcreated_dt_tm'];
        //     $invoice_date   = date("d/m/Y", strtotime($invoice_date));
        //     $invoice_name   = $vehicle[0]['f020floan_name'];
        //     $invoice_amount   = $vehicle[0]['f043floan_amount'];



        $file_data      =  "


      
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td ><img src='/var/www/html/fims-zend/public/img/um.png' height='100' width='200'></img></td>
         <td style='padding-right : 480px'><b>JABAHAN BENDAHARI<br> BURSAR'S OFFICE </b><br> Universiti Utara Malayasia <br> 06010 UUM SINTOK <br> KEDAH DURAL AMAN <br> MALAYSIA</td>
         <td><img src='/var/www/html/fims-zend/public/img/uum.png' height='' width=''></img></td>
         </tr>
         <tr>
         <td></td>
         <td></td>
         <td><font size='2'>Tel: 604-928 3200/3211/3255<br>Faks(Fax): 604-928 3299<br>Laman Web (Web): www.uum.edu.my</font></td>
      </tr>

   </table>
    <hr>
       <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>KEDAH AMAN MAKMUR.BERSAMA MEMACU TRANFORMASI</td>
      </tr><tr></tr>
   </table>


   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr></tr>
       <tr></tr>
      <tr>
         <td></td>
         <td style='padding-left: 480px'>BEND20150812-KB1095<br>12Ogos2015</td>
      </tr>
   </table>

      <br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td><b>PEKELILING BENDAHARI BIL. $invoice_date</b></td>
      </tr>
   </table>

       <table align='left' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
         <td style='padding-left: 10%;'>Semua Penolong Naib Canselor! Ketua Pusat Tanggungiawab</td>
      </tr>
      <tr>
         <td style='padding-left: 10%;'>Universiti Utara Malaysia</td>
      </tr>
      <tr>
      
      </tr>
   </table>

      </p>
      <br><br>

       <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td>YBhg. Dato' Prof./ Tuan/ Puan</td>
      </tr>
   </table>
   <table align='center' style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td>PERLAKSANAAN SKIM PINJAMAN BASIKAL UNIVERSITI UTARA MALAYSIA(UUM)</td>
      </tr>
   </table>

     <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td>Dengan segala hormatnya saya merujuk kepada perkara di atas</td>
      </tr>
   </table>
      
       <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Mesyuarat Khas Lembaga Pengarah Universiti (LPU) Bil. 1/2015 pada 09 Mac 2015 telah bersetuju pelaksanaan Skim Pinjaman Basikal UUM. Skim tersebut perlu dipohon secara online dan akan mula beroperasi pada tarikh surat ini dikeluarkan</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Skim pinjaman basikal UUM dihadkan kepada RM5,000.00 (Ringgit Malaysia: Lima Ribu Sahaja) sepertimana syarat pinjaman motosikal dan tempoh bayaran balik adałah selana enam (6) tahun bersamaan 72 bulan.</td>
      </tr>
   </table>
     
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Staf boleh membuat permohonan pinjaman basikal melalui portal masing-masing Setelah kelulusan diperolehi dari Jawatankuasa Pinjaman Universiti barulah proses bayaran akan dilakukan kepada pembekal. Selepas itu, proses mengemaskini pinjaman akaun staf akan dilakukan untuk bayaran balk pinjaman melalui potongan gaj</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Berikut adalah pembekal luar yang dilantik sebagai panel UUM bagi pembelian basikal :</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td style='padding-left: 5%;'>i)</td>
         <td style='padding-right: : 5%;'><b>TM Bike Sdn. Bhd</b></td>
      </tr>
       
      <tr>
         <td style='padding-left: 10%;'></td>
         <td>No. 13 Jalan Tengku Maheran, Taman Tengku Maheran Fasa 4<br>06000 Jitra, Kedah. (019-4184481 dan 04-9190366)</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td style='padding-left: 5%;'>ii)</td>
         <td style='padding-right: : 5%;'><b>MU Wira Enterprise</b></td>
      </tr>
       
      <tr>
         <td style='padding-left: 10%;'></td>
         <td>16, Jalan Kompleks Perniagaan Jelatek, Kompleks Jelatek<br>05460 Alor Setar, Kedah. (012-4076963 dan 04-7309340)</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td style='padding-left: 5%;'>iii)</td>
         <td style='padding-right: : 5%;'><b>SAM Energy Cycle</b></td>
      </tr>
       
      <tr>
         <td style='padding-left: 10%;'></td>
         <td>No. 1J, Jalan Kodiang, Taman Utara Fasa 2, Guar Sanji<br>02600 Arau, Perlis. (017-4096910 dan 04-9860286)</td>
      </tr>
      </table>


      <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Tatacara permohonan yang perlu dipatuhi oleh setiap tafpemohon adalah seperti diłampiran 1-A dan 1-B</td>
      </tr>
   </table>


   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Oleh yang demiklan, dlharap kerjasama daripada YBhg. Dato'Prof Tuaní Puan membuat hebahan hal ini kepada semua staf di bawah Pusat Tanggungjawab masing-masing</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Sekian, terima kasih</td>
      </tr>
   </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><b>'ILMU BUDI BAKTI'</b></td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Saya yang menurut perintalh</td>
      </tr>
   </table> 
   <br><br><br>
      
      <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
      
         <tr>
            <td><b>(HJ AMRON BIN MAN) </b></td></tr>
            <tr><td>Bendahari</td></tr>
            <tr><td>Universiti Utara Malaysia</td></tr>
            </tr>
      </table>


      <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>YBhg. Prof. Dato' Wira Dr. Mohamed Mustafa Ishak</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>YBerusaha Timbalan-Timbalan Naib Canselor</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Edaran</td>
      </tr>
      </table>

      <table  style='border:0px solid black;width: 90%;height:50px;margin-top: 4;'>
        <tr>
         <td style='padding-left: 900px' >LAMPIRAN 1-A</td>
        </tr>
        </table>

       <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><b> TATACARA PERMOHONAN PINJAMAN BASIKAL UUM </b></td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>1.</td>
         <td>Pemohon hendaklah mendapatkan sebutharga pembellan basikal baru sahaja dari pembekal yang ditetapkan oleh universiti.</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>2.</td>
         <td>Pemohon hendaklah mengisl permohonan secara online untuk mendapaikarn kelulusan Ketua Jabatan dan penjamin (Pastikan potongan bulanan pembiayaan tidak melebihi 1/3 daripada gaji pokok dan jumlah semua potongan gajinya tidak melebihi 60% dari jumlah gaji dan lain-lain elaun)</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>3.</td>
         <td>Sekiranya potongan gaji melebihi 60%, pemohon tidak layak mengemukakan permohonan</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>4.</td>
         <td>Setelah permohonan diluluskan oleh Jawatankuasa Pinjaman Universiti borang permohonan perlu dihantar bersama-sama dokumen sokongan ke Jabatan Bendahari dan pemohon dikehandaki membayar wang proses sebanyak 0.1 % dari jumlah pinjaman atau minimun RM20 (mana yang lebih tinggi)</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>5.</td>
         <td>Sebarang permohonan yang tidak lengkap akan dikembalikan semula kepada peminjam</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>6.</td>
         <td>Permohonan yang lengkap akan diproses dan dikemukakan untuk kelulusan Jawatankuasa Pinjaman Universiti secara dalam talian</td>
      </tr>
      </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>7.</td>
         <td>Surat tawaran dan dokumen perjanjian akan diproses dalam tempoh seminggu selepas mendapat kelulusan</td>
      </tr>
    </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>8.</td>
         <td>Surat tawaran/memorandum penerimaan tawaran perlu ditandatangani oleh peminjam/pemohon dan hendaklah membuat pilihan skim perlindungan takaful iaitu Syarikat Takaful Malaysia Berhad dan Syarikat Etiga Takaful Berhad.Skim perlindungan takaful Ini akan ditanggung oleh peminjam dan akan di masukkan di dalam penyata penghutang peminjam masing-masing sebagai pinjaman.</td>
      </tr>
    </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>9.</td>
         <td>Bayaran pinjaman akan dibuat kepada pembekal selepas mendapat surat pengesahan bokalan (model basikal) dari pcmbokai sorta dokumen perjanjian telah ditandatangani dan dimatikan setem (Caj.mati setem/ RM25)</td>
      </tr>
    </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>10.</td>
         <td>Peminjam diberi tempoh iga puluh (30) hari untuk mengemukakan resit asal pembelian dan warranty card selepas-pembayaran-dibuat kepada.pembekal.</td>
      </tr>
    </table>

      <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
       <td>11.</td>
         <td>Surat peringatan akan dikeluarkan kepada peminjam yang gagal mengemukakan dokumen seperti di perkara 10 dalam tempoh yang ditetapkan</td>
      </tr>
    </table>";


    $file_data = $file_data . "</table>";
                $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
           
      

    }

    public function invoiceAction()
    {
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $reportRepository = $this->getRepository('T019fsavedReports');

                $result = $reportRepository->getMasterInvoiceList();

        // print_r($result);
        // exit();

            $invoice_number = $result[0]['f071finvoice_number'];
            $invoice_date   = $result[0]['f071finvoice_date'];
            $invoice_date   = date("d/m/Y", strtotime($invoice_date));
            $invoice_type   = $result[0]['f071finvoice_type'];
            $bill_type   = $result[0]['f071fbill_type'];
            $financial_year = $result[0]['f015fname'];  

        // print_r($bill_type);
        // exit();

        $file_data      = "
        <table style='border:0px solid black;width: 100%;height:30px;margin-top: 4;'>
        <tr>
            <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        <td style='padding-right : 480px'><b>JABATAN BENDAHARI<br><i>BURSAR's OFFICE</i><br></b> Universiti Utara Malaysia<br>06010 UUM SINTOK<br>KEDAH DARUL AMAN<br>MALAYSIA</td>
        <td></td><td></td>
        <td><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        </tr>
        <tr>
        <td></td><td></td>
        <td></td><td></td>
        <td><font size='2'>Tel:604-928 3200/3221/3255<br>Faks(Fax):604-928 3299<br>Laman Web(web):www.uum.edu.my</td>
        </tr>
        </table>
       <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>INVOICE</td>
      </tr><tr></tr>
    </table>
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
      
        <tr>
        <td> Invoice Date: $invoice_date</td>
        <td>Invoice NO: $invoice_number</td>   
        </tr>
        <tr>
        <td>Financial Year:$financial_year</td>
        </tr>
        <tr>
           <td>Invoice Type: $invoice_type </td>
           <td> Bill Type: $bill_type</td>
        </tr>
     
    </table>
    <hr>
       <table align='center' style='border:1px solid black;width: 100%;'>
      <tr>
        <td><b> Customer id </b></td>
        <td><b> Description </b></td>
        <td><b> Invoice From </b></td>
        <td><b> Balance </b></td>
        <td><b> Total </b></td>
       </tr>";
     
    foreach ($result as $row)
        {

            // print_r($row);
            // exit();


            $invoice_id_customer = $row['f071fid_customer'];
            $invoice_description = $row['f071fdescription'];
            $invoice_invoice_from  = $row['f071finvoice_from'];
            $invoice_balance   = $row['f071fbalance'];
            $invoice_total_paid   = $row['f071ftotal_paid'];


            $invoice_balance = number_format((float) $row['f071fbalance'], 2, '.', '');
            $invoice_total_paid = number_format((float) $row['f071ftotal_paid'], 2, '.', '');


            $invoice_balance = number_format($invoice_balance, 2, '.', ',');
            $invoice_total_paid = number_format($invoice_total_paid, 2, '.', ',');
            
          

            $file_data  = $file_data . "
            <tr>
        <td> $invoice_id_customer </td>
        <td> $invoice_description </td>
        <td> $invoice_invoice_from </td>
        <td style='text-align: right'> $invoice_balance </td>
        <td style='text-align: right'> $invoice_total_paid </td>>
        </tr>";
        }
        $file_data = $file_data . "</table>
        <br><br>";

        
                $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
    }

    public function smartPhoneLoanAction()
    {
        
        //suratTawaranKompInvoice  
        $rawBody          = file_get_contents("php://input");
        // $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        // $result         = $reportRepository->getActivityInvoice();   
        
        
        $file_data = "
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
      <tr>
         <td style='text-align: left' width='100%'>BEND20180321-KB0655</td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br></td>
      </tr>
      <tr>
      <td style='text-align: left' width='100%'>21/03/2018</td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br></td>
      </tr>
      <tr>
      <td style='text-align: left' width='100%'>MOHD NIZAM BIN ZAINAL ABIDIN (B5239)<br>JABATAN KESELAMATAN<br>UNIVERSITI UTARA MALAYSIA<br>06010 UUM SINTOK<br>Tuan/Puan<br></td>
      </tr>
    </table>

    <br><br>

    <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
      <tr>
         <td align='center'>TAWARAN PEMBIAYAAN PEMBELIAN KOMPUTER DAN TELEFON PINTAR (SMARTPHONE) UUM</td>
      </tr>
    </table>
    <br><br>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>   
      <tr>
         <td style='text-align: left' width='100%'>Dengan ini adalah dimaklumkan bahawa permohonan tuan/puan untuk mendapatkan kemudahan pinjaman bagi tujuan membiayai keseluruhan/sebahagian daripada harga pembelian sebuah komputer adalah diluluskan seperti berikut:</td>
      </tr>
    </table>
    <br><br>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td style='text-align: left' width='60%'>(a)Harga Belian Oleh UUM Bayaran Muka Oleh Tuan/Puan</td>
         <td style='text-align: left' width='40%'>:RM 3,149.00</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Amaun Pembiayaan Oleh UUM</td>
         <td style='text-align: left' width='40%'>:RM 3,100.00</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Muka Oleh Tuan/Puan</td>
         <td style='text-align: left' width='40%'>:RM 49.00</td>
       </tr>
       <tr>
       <td style='text-align: left' width='100%' colspan='2'><br><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>(b)Harga Jualan Kepada Pegawai</td>
         <td style='text-align: left' width='40%'>:RM 3,420.52</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Muka Oleh Tuan/Puan</td>
         <td style='text-align: left' width='40%'>:RM 49.00</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Amaun Pembiayaan dan<br>Keuntungan UUM</td>
         <td style='text-align: left' width='40%'>:RM 3,371.52</td>
       </tr>
        <tr>
         <td style='text-align: left' width='60%'>Jumlah Insuran Perlindungan</td>
         <td style='text-align: left' width='40%'>:RM 10.85</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Ansuran Bulanan</td>
         <td style='text-align: left' width='40%'>:RM 70.24</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Tempoh Bayaran Balik</td>
         <td style='text-align: left' width='40%'>:48 bulan</td>
       </tr>
       <tr>
       <td style='text-align: left' width='100%' colspan='2'><br><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>(c)Butir-butir</td>
         <td style='text-align: left' width='40%'></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Komputer dan Telefon Pintar (Smartphone)</td>
         <td style='text-align: left' width='40%'></td>
       </tr>
       <tr>
       <td style='text-align: left' width='100%' colspan='2'><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Model dan Buatan</td>
         <td style='text-align: left' width='40%'>:HUAWEI P10 PRO</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Nama Pembekal</td>
         <td style='text-align: left' width='40%'>:KOPERASI UNIVERSITI UTARA</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Spesifikasi</td>
         <td style='text-align: left' width='40%'>:Seperti Dalam Sebutharga</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Pencetak</td>
         <td style='text-align: left' width='40%'>:Seperti Dalam Sebutharga</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Edaran Bulan</td>
         <td style='text-align: left' width='40%'>:April</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bertarikh</td>
         <td style='text-align: left' width='40%'>:48 bulan</td>
       </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
      <tr>
         <td>2.Tawaran ini tertakluk kepada syarat-syarat berikut:</td>
      </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td style='text-align: left' width='100%'>(i) Tuan/Puan dikehendaki melengkapkan Perjanjian Pembiayaan
            Pembelian Komputer dan Telefon Pintar (smartphone) sebelum
            barang dapat dibekalkan,
            cukai setem ditanggung oleh
            tuan/puan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(ii) Tuan/Puan dilantik untuk mewakili UUM dalam urusan pembelian
        komputer dan telefon pintar (smartphone) tersebut;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(iii) Kemudahan pembiayaan pembelian komputer dan telefon pintar
        (smartphone) ini tidak boleh digunakan untuk tujuan yang lain;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(iv) Spesifikasi komputer dan telefon pintar (smartphone) yang
        dipersetujui untuk dibeli daripada syarikat yang disenaraikan tidak
        boleh dibatalkan atau ditukarkan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(v) Tuan/Puan bertanggungjawab untuk menjelaskan bayaran
        ansuran bulan pertama jika potongan dari gaji bulanan tidak
        sempat dibuat;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(vi) Tuan/Puan bertanggungjawab menyerahkan resit pembelian asal
        sebagai bukti pembelian komputer dan telefon pintar
        (smartphone) dalam masa satu (1) bulan dari pembayaran
        dilakukan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
        <td style='text-align: left' width='100%'>(vii) Tuan/Puan juga tertakluk kepada lain-lain syarat dan peraturan yang ditetapkan dalam Skim Pembiayaan Pembelian komputer
        dan telefon pintar (smartphone) UUM dan juga lain-lain arahan
        dan ketetapan yang dikeluarkan oleh Perbendaharaan dari masa
        ke semasa berkaitan dengan pembiayaan pembelian komputer
        dan telefon pintar (smartphone). Tindakan tatatertib dan/atau
        surcaj boleh diambil terhadap tuan/puan sekiranya tuan/puan
        didapati gagal mematuhi mana-mana peraturan yang ditetapkan
        dalam Pekeliling dan arahan tersebut.</td>
      </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td style='text-align: left' width='100%'>3.Sekiranya Tuan/Puan bersetuju dengan syarat-syarat pembiayaan sila
        tandatangani memorandum penerimaan tawaran dan kembalikan
        salinannya dalam tempoh 14 hari dari tarikh surat ini. Jika jawapan tidak
        diterima dalam tempoh tersebut, tawaran ini akan dianggap luput.</td>
    </tr>
    <tr>
    <td style='text-align: left' width='100%'><br><br></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'>Seklan, terima kasih.</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><br><br></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><b>ILMU BUDI BAKTI'</b></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'>Saya yang menurut perintah,</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><br><br><br><br><br>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><b>(NIKMAL MUZAL BIN MOHD MUHAIYUDDIN)</b><br>Penolong Bendahari Kanan<br>Bendahari</td>
    </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td style='text-align: left' width='10%'>s.k:</td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Pendaftar</td>
      </tr>
      <tr>
         <td style='text-align: left' width='10%'></td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Universiti Utara Malaysia</td>
      </tr>
      <tr>
        <td style='text-align: left' width='10%'></td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Unit Pinjaman</td>
      </tr>
    </table>

    <pagebreak>

    <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
            <td style='text-align: left' width='100%'><img src='' height='100' width='100'></img></td>
        </tr>
    </table>

    <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
      <tr>
         <td style='text-align: center' width='100%'>UNIVERSITI UTARA MALAYSIA</td>
      </tr>
    </table>
    <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td style='text-align: center' width='100%'>SURAT TAWARAN PEMBIAYAAN PEMBELIAN
        KOMPUTER DAN TELEFON PINTAR (SMARTPHONE)<br>UNIVERSITI UTARA MALAYSIA<br>MEMORANDUM PENERIMAAN</td>
      </tr>
      <tr>
        <td style='text-align: center' width='100%'><br><br></td>
      </tr>
    </table>

    <table align='center' width='80%'>
       
      <tr>
         <td style='text-align: center' width='100%'>Merujuk kepada surat tawaran tuan bil (BEND________________________)<br>
        bertarikh _______________________ dengan ini saya menerima tawaran<br>
        tersebut dan bersetuju dengan syarat-syarat serta peraturan-peraturan yang<br>
        dikenakan seperti yang terkandung dalam surat berkenaan. Sila buat pilihan<br>
        Perlindungan Takaful seperti dibawah :</td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
    <tr>
         <td style='text-align: left' width='100%'><input type='checkbox'>Syarikat Takaful Malaysia Berhad</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><input type='checkbox'>Syarikat Etiqa Berhad</td>
    </tr>
    </table>

    <table align='center'width='60%'>
    <tr>
         <td style='text-align: left' width='50%'>Ditandatangani oleh</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>No. K/P</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>No. Pekerja</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Jawatan</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Jabatan</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Tarikh</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>";

          $file_data = $file_data . "</table>";


          // print_r($file_data);exit;
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }

    public function vehicleLoanReportAction()
    {
        //suratTawaranPinjamanInvoice
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);


        if($this->isJSON($rawBody))
        {
            
        $id = $postData['idLoan'];
        // print_r($id);exit;
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getVehicleLoanDetails((int)$id);   


        $pay_date = date('m/d/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        
        // print_r($result);exit;

        $totalLoan = $result['f043ftotalLoan'];
        $referenceNo = $result['f043freferenceNumber'];
        $monthlyProfitAmount = $result['f043fprofitAmount'];
        $insuranceAmount = $result['f043insuranceAmt'];
        $monthlyInstallment = $result['f043fmonthlyInstallment'];
        $installmentPeriod = $result['f043finstallmentPeriod'];
        $manufacturer = $result['f043fmanufacturer'];
        $cc = $result['f043fcc'];
        $applyDate = $result['f043fapplyDate'];


        $purchasePrice = $result['f043fpurchasePrice'];
        $loanAmount = $result['f043floanAmount'];
        $Amount = $result['f043fjkrAmount'];
        $CertificateNo = $result['f043fjkrCertificateNo'];



        $model = $result['f043fmodel'];
        $vehicleCondition = $result['f043fvehicleCondition'];
        $lastDate = $result['f043flastDate'];
        $applyDate = $result['f043fapplyDate'];
        $startDate = $result['f043fstartDate'];
        $endDate = $result['f043fendDate'];
        // $purchasePrice = $result['f043fpurchasePrice'];
        // $purchasePrice = $result['f043fpurchasePrice'];
        // $purchasePrice = $result['f043fpurchasePrice'];

        $lastDate   = date("d-m-Y", strtotime($lastDate));
        $applyDate   = date("d-m-Y", strtotime($applyDate));
        $startDate   = date("d-m-Y", strtotime($startDate));
        $endDate   = date("d-m-Y", strtotime($endDate));

        // print_r($startDate);
        // print_r($endDate);exit;
        $timeDiff = $reportRepository->diffMonth($startDate,$endDate);


        $loanAmount = number_format($loanAmount, 2, '.', ',');
        $Amount = number_format($Amount, 2, '.', ',');
        $profitAmount = number_format($profitAmount, 2, '.', ',');
        $purchasePrice = number_format($purchasePrice, 2, '.', ',');

        $totalLoan = number_format($totalLoan, 2, '.', ',');
        $monthlyProfitAmount = number_format($monthlyProfitAmount, 2, '.', ',');
        $insuranceAmount = number_format($insuranceAmount, 2, '.', ',');
        $monthlyInstallment = number_format($monthlyInstallment, 2, '.', ',');

        // $amount = number_format($amount, 2, '.', ',');


        if ($vehicleCondition == 0)
        {
            $vehicleCondition = "Used Vehicle";
        }
        else
        {
            $vehicleCondition = "New Vehicle";
        }


        $file_data = "
    <table align='center' width='100%'>
      <tr>
        <td></td>
      </tr>
    </table>";
        
        $file_data = $file_data . "


    <table align='center' width='100%'>
      <tr>
         <td style='text-align: left' width='100%'>$CertificateNo</td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br><br><br><br></td>
      </tr>
      <tr>
      <td style='text-align: left' width='100%'>$pay_date</td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br></td>
      </tr>
      <tr>
      <td style='text-align: left' width='100%'>HASIMAH BINTI SAPIRI (B1879)<br>JPUSAT PENGAJIAN SAINS KUANTITATIF<br>UNIVERSITI UTARA MALAYSIA<br>06010 SINTOK<br></td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br><br></td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'>Tuan/Puan</td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><br><br><br></td>
      </tr>
    </table>


    <table  align='center' width='100%'>
     <tr>
         <td style='text-align: left' width='100%'><b>TAWARAN PEMBIAYAAN PEMBELIAN KENDERAAN UUM</b></td>
      </tr>
      <tr>
         <td style='text-align: left' width='100%'><b>Reference No. : $referenceNo</b></td>
      </tr>
    </table>

    <br><br>
    
    <table align='center' width='100%'>   
      <tr>
         <td style='text-align: left' width='100%'>Dengan ini adalah dimaklumkan bahawa permohonan tuan/puan untuk mendapatkan kemudahan pinjaman bagi tujuan membiayai keseluruhan/sebahagian daripada harga pembelian sebuah kenderaan adalah diluluskan seperti berikut:<br></td>
      </tr>
    </table>
    <br><br>
    <table align='center' width='100%'>
       <tr>
         <td style='text-align: left' width='60%'>(a)Harga Belian Oleh UUM</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $purchasePrice </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Amaun Pembiayaan Oleh UUM</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $totalLoan </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Muka Oleh Tuan/Puan</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $Amount</td>
       </tr>
       <tr>
       <td style='text-align: left' width='100%' colspan='2'><br><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>(b)Harga Jualan Kepada Pegawai</td>
         <td style='text-align: left' width='40%' valign='top'>:RM </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Muka Oleh Tuan/Puan</td>
         <td style='text-align: left' width='40%' valign='top'>:RM </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Amaun Pembiayaan dan<br>Keuntungan UUM</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $monthlyProfitAmount</td>
       </tr>
        <tr>
         <td style='text-align: left' width='60%'>Jumlah Insuran Perlindungan</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $insuranceAmount</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bayaran Ansuran Bulanan</td>
         <td style='text-align: left' width='40%' valign='top'>:RM $monthlyInstallment </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Tempoh Bayaran Balik</td>
         <td style='text-align: left' width='40%' valign='top'>:$installmentPeriod</td>
       </tr>
       <tr>
       <td style='text-align: left' width='100%' colspan='2'><br><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>(c)Butir-butir</td>
         <td style='text-align: left' width='40%' valign='top'></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>PEMBIAYAAN PEMBELIAN</td>
         <td style='text-align: left' width='40%' valign='top'></td>
       </tr>
       <tr>
        <td style='text-align: left' width='100%' colspan='2'><br></td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Jenis Kenderaan</td>
         <td style='text-align: left' width='40%' valign='top'>:Car</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Keadaan Kenderaan</td>
         <td style='text-align: left' width='40%' valign='top'>:$vehicleCondition</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Model</td>
         <td style='text-align: left' width='40%' valign='top'>:$model</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Buatan</td>
         <td style='text-align: left' width='40%' valign='top'>: $manufacturer</td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Sukatan Silinder</td>
         <td style='text-align: left' width='40%' valign='top'>: $cc </td>
       </tr>
       <tr>
         <td style='text-align: left' width='60%'>Bertarikh Kelulusan</td>
         <td style='text-align: left' width='40%' valign='top'>:$applyDate</td>
       </tr>
    </table>

    <table align='center' width='100%'>
      <tr>
         <td>2.Tawaran ini tertakluk kepada syarat-syarat berikut:</td>
      </tr>
    </table>

    <table align='center' width='100%'>
       
      <tr>
         <td style='text-align: left' width='100%'>(i) Tuan/Puan dikehendaki melengkapkan Perjanjian Pembelian Kenderaan sebelum barang dapat dibekalkan, cukai setem ditanggung oleh tuan/puan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(ii) Tuan/Puan dilantik untuk mewakili UUM dalam urusan Pembelian Kenderaan tersebut;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(iii) Kemudahan Pembelian Kenderaan ini tidak boleh digunakan untuk tujuan yang lain;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(iv) Spesifikasi Pembelian Kenderaan yang dipersetujui untuk dibeli daripada syarikat yang disenaraikan tidak boleh dibatalkan atau ditukarkan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(v) Tuan/Puan bertanggungjawab untuk menjelaskan bayaran ansuran bulan pertama jika potongan dari gaji bulanan tidak sempat dibuat;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
         <td style='text-align: left' width='100%'>(vi) Tuan/Puan bertanggungjawab menyerahkan resit pembelian asal sebagai bukti Pembelian Kenderaan dalam masa satu (1) bulan dari pembayaran dilakukan;</td>
      </tr>
      <tr>
       <td style='text-align: left' width='100%'><br></td>
       </tr>
      <tr>
        <td style='text-align: left' width='100%'>(vii) Tuan/Puan juga tertakluk kepada lain-lain syarat dan peraturan yang ditetapkan dalam Skim Pembiayaan Pembelian Kenderaan UUM dan juga lain-lain arahan dan ketetapan yang dikeluarkan oleh Perbendaharaan dari masa ke semasa berkaitan dengan pembiayaan Pembelian Kenderaan. Tindakan tatatertib dan/atau surcaj boleh diambil terhadap tuan/puan sekiranya tuan/puan didapati gagal mematuhi mana-mana peraturan yang ditetapkan dalam Pekeliling dan arahan tersebut.</td>
      </tr>
    </table>

    <table align='center' width='100%'>
       
      <tr>
         <td style='text-align: left' width='100%'>3.Sekiranya Tuan/Puan bersetuju dengan syarat-syarat pembiayaan sila tandatangani memorandum penerimaan tawaran dan kembalikan salinannya dalam tempoh 14 hari dari tarikh surat ini. Jika jawapan tidak diterima dalam tempoh tersebut, tawaran ini akan dianggap luput.</td>
    </tr>
    <tr>
    <td style='text-align: left' width='100%'><br><br></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'>Seklan, terima kasih.</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><br><br></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><b>ILMU BUDI BAKTI'</b></td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'>Saya yang menurut perintah,</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><br><br><br><br><br>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><b>(NIKMAL MUZAL BIN MOHD MUHAIYUDDIN)</b><br>Penolong Bendahari Kanan<br>Bendahari</td>
    </tr>
    </table>
    <table align='center' width='100%'>
      <tr>
         <td style='text-align: left' width='10%'>s.k:</td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Pendaftar</td>
      </tr>
      <tr>
         <td style='text-align: left' width='10%'></td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Universiti Utara Malaysia</td>
      </tr>
      <tr>
        <td style='text-align: left' width='10%'></td>
         <td style='text-align: left' width='2%'>-</td>
         <td style='text-align: left' width='87%'>Unit Pinjaman</td>
      </tr>
    </table>

    <pagebreak>

    <table align='center' width='100%'>
        <tr>
            <td style='text-align: left' width='100%'><img src='' height='100' width='100'></img></td>
        </tr>
    </table>

    <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
      <tr>
         <td style='text-align: center' width='100%'>UNIVERSITI UTARA MALAYSIA</td>
      </tr>
    </table>
    <table align='center' width='100%'>
       <tr>
      <tr>
         <td style='text-align: center' width='100%'>SURAT TAWARAN PEMBIAYAAN PEMBELIAN<br><br>UNIVERSITI UTARA MALAYSIA<br><br>MEMORANDUM PENERIMAAN</td>
      </tr>
      <tr>
        <td style='text-align: center' width='100%'><br><br></td>
      </tr>
    </table>

    <table align='center' width='100%'>
       
      <tr>
         <td style='text-align: center' width='100%'>Merujuk kepada surat tawaran tuan bil (BEND________________________)<br>
        bertarikh _______________________ dengan ini saya menerima tawaran<br>
        tersebut dan bersetuju dengan syarat-syarat serta peraturan-peraturan yang<br>
        dikenakan seperti yang terkandung dalam surat berkenaan. Sila buat pilihan<br>
        Perlindungan Takaful seperti dibawah :</td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
    <tr>
    <td><br><br></td>
    </tr>
    <tr>
         <td style='text-align: left' width='100%'><input type='checkbox'>Syarikat Takaful Malaysia Berhad</td>
    </tr>
    <tr>
        <td style='text-align: left' width='100%'><input type='checkbox'>Syarikat Etiqa Berhad</td>
    </tr>
    </table>

    <table align='center'width='60%'>
    <tr>
    <td><br><br></td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Ditandatangani oleh</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>No. K/P</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>No. Pekerja</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Jawatan</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Jabatan</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>
    <tr>
         <td style='text-align: left' width='50%'>Tarikh</td>
         <td style='text-align: left' width='50%'>:______________________________</td>
    </tr>";

        $file_data.= "</table>";

        }
        // print_r($file_data);exit;
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }

    public function borangPermohonanPendahuluanInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();

        $file_data = "
        <style>
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
        </style>
        <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
        <td ><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        <td style='text-align: center' colspan='5'><b> UNIVERSITI UTARA MALAYSIA <br>PENYATA PERBELANJAAN PENDAHULUAN</b></td>
        </tr>
        
        <tr>
        <td><font size='2'> NAMA PEMOHON</td>
        <td colspan='3'></td>
        <td><font size='2'> No. Pekerja</td>
        <td></td>
        </tr>
        <tr>
        <td> <font size='2'>JAWATAN/TARAF JAWATAN<br>(TETAP/KONTRAK)</td>
        <td colspan='1'></td>
        <td> <font size='2'>JABATAN </td>
        <td colspan='1'></td>
        <td> <font size='2'>NO.TELEFON</td>
        <td></td>
        </tr>

        <tr>
        <td><font size='2'> TUJUAN PENDAHULUAN</td>
        <td colspan='5'></td>
        </tr>

        <tr>
        <td><font size='2'> PERUNTUKAN/VOT</td>
        <td colspan='5'></td>
        </tr>

        <tr>
        <td><font size='2'>JUMLAH PENDAHULUAN</td>
        <td colspan='1'><font size='2'> RM</td>
        <td><font size='2'>TARIKH DIPERLUKAN</td>
        <td colspan='3'></td>
        </tr>

        <tr>
        <td width='20%' colspan='4'><font size='1'>ANGGARAN PERBELANJAAN KESELAMATAN:<br>
        (slla Lampiran surat kelulusan atau keretas kerja yang berkaltan)</td>
        <td>RM</td>
        <td>SEN</td>
        </tr>

        <tr>
        <td width='20%' colspan='4'>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td></td>
        <td></td>
        </tr>

        <tr>
        <td colspan='4'><br><br></td>
        <td></td>
        <td></td>
        </tr>

        <tr>
        <td colspan='6'><font size='1'><b>PERAKUAN KETUA JABATAN DAN PEMOHONAN</b><br>1. Dengan Ini adalah disahkan bahawa pendahuiuan di atas diperlukan untuk urusan rasmil Universiti<br>2.pendahuluan Kepada Staf<br> Borang tuntutan perjakanan yang lengkap akan dkemukakan kepada Jabatan Bendaharl sebelum atau pada 10 harlbulan bulan berkutnya perjalonan disempumakan.<br>i. Penyata lengkap perbelanjaan sebenar serta dokumen sokongan okan dikemukakan dalam tempoh 30 hari darl tarikh pendahuluan dikeluarkan.<br>ii.Jika pendahuluan fidak digunakan atau tidak digunakan sepenuhnya, baki pendahuluan akan dipulangkan dengan serta merta.<br>iii.Sekiranya penyata tidak dapat dikemukakan dalam tempoh yang ditetapkan saya akan membuat permohonan perianjutan masa menghantar penyata perbelanjaan.<br>4.Jabatan Bendahari dibenarkan membuat potongan gail tanpa notis untuk mendapatkan keseluruhan pendahuluan ini sekiranya tuntutan perjalanan sebenar atau penyata perbelanjaan pendahuluan tldak dikemukakan seperti ai perenggan 2 dan 3<br>5. Borang permohonan pendahuluan mestilah dihantar 7 han sebelum tarkh pendahutuan diperlukan.<br><br>
        </tr>
        <footer>
        <tr>
        <td colspan='3' align='center' width='50%'><font size='2'><br><br>_________________________________<br>landatangan & Cop Ketua Jabatan</td>
        <td colspan='3' align='center' width='50%'><font size='2'><br><br>_______________________<br>Tandatangan Pemohon</td>
        </tr>
        </footer>

        <footer>
        <tr>
        <td colspan='3' align='center' width='50%'><font size='2'><b>PENGESAHAN JABATAN BENDAHARI :</b><br><br>Pemohon mempunyai/tidak mempunyai baki pendahuluan untuk <br>tujuan yang sama.
        <br><br></td>
        <td colspan='3' align='center' width='50%'><font size='2'><b>KELULUSAN BENDAHARI/NAIB CANSELOR</b><br><br>Pemohonan pendahuuan diluluskan<br><br></td>
        </tr>
        </footer>

        <footer>
        <tr>
        <td colspan='' align='center'><font size='2'><br><br>_______<br>Tarkh</td>
        <td colspan='2' align='center' ><font size='2'><br><br>_____________<br>Tandatangan</td>
        <td colspan='' align='center' ><font size='2'><br><br>_______<br>Tarkh</td>
        <td colspan='2' align='center'><font size='2'><br><br>______________<br>Tandatangan</td>
        </tr>
        </footer> 
        </table>";

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,  
        ]);
    }
   
public function cashAdvanceReportAction()
    {
//borangPermohonanPendahuluanInvoice
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();

        $file_data = "
        <style>
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
        </style>
        <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
        <td ><img src='/var/www/html/fims-zend/public/img/logo.png' height='100' width='100'></img></td>
        <td style='text-align: center' colspan='5'><b> UNIVERSITI UTARA MALAYSIA <br>PENYATA PERBELANJAAN PENDAHULUAN</b></td>
        </tr>
        
        <tr>
        <td><font size='2'> NAMA PEMOHON</td>
        <td colspan='3'></td>
        <td><font size='2'> No. Pekerja</td>
        <td></td>
        </tr>
        <tr>
        <td> <font size='2'>JAWATAN/TARAF JAWATAN<br>(TETAP/KONTRAK)</td>
        <td colspan='1'></td>
        <td> <font size='2'>JABATAN </td>
        <td colspan='1'></td>
        <td> <font size='2'>NO.TELEFON</td>
        <td></td>
        </tr>

        <tr>
        <td><font size='2'> TUJUAN PENDAHULUAN</td>
        <td colspan='5'></td>
        </tr>

        <tr>
        <td><font size='2'> PERUNTUKAN/VOT</td>
        <td colspan='5'></td>
        </tr>

        <tr>
        <td><font size='2'>JUMLAH PENDAHULUAN</td>
        <td colspan='1'><font size='2'> RM</td>
        <td><font size='2'>TARIKH DIPERLUKAN</td>
        <td colspan='3'></td>
        </tr>

        <tr>
        <td width='20%' colspan='4'><font size='1'>ANGGARAN PERBELANJAAN KESELAMATAN:<br>
        (slla Lampiran surat kelulusan atau keretas kerja yang berkaltan)</td>
        <td>RM</td>
        <td>SEN</td>
        </tr>

        <tr>
        <td width='20%' colspan='4'>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
        <td></td>
        <td></td>
        </tr>

        <tr>
        <td colspan='4'><br><br></td>
        <td></td>
        <td></td>
        </tr>

        <tr>
        <td colspan='6'><font size='1'><b>PERAKUAN KETUA JABATAN DAN PEMOHONAN</b><br>1. Dengan Ini adalah disahkan bahawa pendahuiuan di atas diperlukan untuk urusan rasmil Universiti<br>2.pendahuluan Kepada Staf<br> Borang tuntutan perjakanan yang lengkap akan dkemukakan kepada Jabatan Bendaharl sebelum atau pada 10 harlbulan bulan berkutnya perjalonan disempumakan.<br>i. Penyata lengkap perbelanjaan sebenar serta dokumen sokongan okan dikemukakan dalam tempoh 30 hari darl tarikh pendahuluan dikeluarkan.<br>ii.Jika pendahuluan fidak digunakan atau tidak digunakan sepenuhnya, baki pendahuluan akan dipulangkan dengan serta merta.<br>iii.Sekiranya penyata tidak dapat dikemukakan dalam tempoh yang ditetapkan saya akan membuat permohonan perianjutan masa menghantar penyata perbelanjaan.<br>4.Jabatan Bendahari dibenarkan membuat potongan gail tanpa notis untuk mendapatkan keseluruhan pendahuluan ini sekiranya tuntutan perjalanan sebenar atau penyata perbelanjaan pendahuluan tldak dikemukakan seperti ai perenggan 2 dan 3<br>5. Borang permohonan pendahuluan mestilah dihantar 7 han sebelum tarkh pendahutuan diperlukan.<br><br>
        </tr>
        <footer>
        <tr>
        <td colspan='3' align='center' width='50%'><font size='2'><br><br>_________________________________<br>landatangan & Cop Ketua Jabatan</td>
        <td colspan='3' align='center' width='50%'><font size='2'><br><br>_______________________<br>Tandatangan Pemohon</td>
        </tr>
        </footer>

        <footer>
        <tr>
        <td colspan='3' align='center' width='50%'><font size='2'><b>PENGESAHAN JABATAN BENDAHARI :</b><br><br>Pemohon mempunyai/tidak mempunyai baki pendahuluan untuk <br>tujuan yang sama.
        <br><br></td>
        <td colspan='3' align='center' width='50%'><font size='2'><b>KELULUSAN BENDAHARI/NAIB CANSELOR</b><br><br>Pemohonan pendahuuan diluluskan<br><br></td>
        </tr>
        </footer>

        <footer>
        <tr>
        <td colspan='' align='center'><font size='2'><br><br>_______<br>Tarkh</td>
        <td colspan='2' align='center' ><font size='2'><br><br>_____________<br>Tandatangan</td>
        <td colspan='' align='center' ><font size='2'><br><br>_______<br>Tarkh</td>
        <td colspan='2' align='center'><font size='2'><br><br>______________<br>Tandatangan</td>
        </tr>
        </footer> 
        </table>";

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,  
        ]);
    }

    public function accountCodeReportAction()
    {
        $rawBody  = file_get_contents("php://input");
        $postData = json_decode($rawBody, true);

        $reportRepository = $this->getRepository('T019fsavedReports');

                $accountCodes = $reportRepository->getAccountCodeList();

        // print_r($accountCodes);
        // exit();
        $file_data = "<table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'><tr>
            <td valign='top'><font size='4'>A/C<p>Level 0</p></font></td>
            <td valign='top'><font size='4'>Nature</td>        <td valign='top'><font size='4'><p>Blc&nbsp;</p><p>Type</p></font></td>  <td valign='top'><font size='4'><p>A/C</p><p>Level1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description</p></font></td>   <td valign='top'><font size='4'><p>A/C</p><p>Level 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description</p></font></td> </tr>";
        foreach($accountCodes as $accountCode){
            $level0_code = $accountCode['f059fcode'];
            $level0_name = $accountCode['f059fname'];
            $level0_blc = ($accountCode['f059fbalance_type'])?'DEBIT':'CREDIT';
            $level0_nature = 'NULL';//$accountCode['f059fcode'];
            $file_data = $file_data."<tr> <td span='left' valign='top'><font size='4'><p>$level0_code</p><p>$level0_name</p></font></td> <td valign='top'><font size='4'>$level0_nature</font></td><td valign='top'><font size='4'>$level0_blc</font></td>";
            
         
            $file_data = $file_data."<td span='left' valign='top'><font size='4'>";
                $file_data1 = "<td span='left'  valign='top'><font size='4'>";
         
            foreach($accountCode['level1'] as $level1){
                $level1_code = $level1['f059fcode'];
                $level1_name = $level1['f059fname'];
                $file_data = $file_data."<span>$level1_code</span> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<span style='float:right;'>$level1_name</span><br>";
                foreach($level1['level2'] as $level2){
                    $level2_code = $level2['f059fcomplete_code'];
                $level2_name = $level2['f059fname'];
                $file_data = $file_data."<br>";
                    $file_data1 = $file_data1."$level2_code&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;$level2_name<br>";
                }


            }
            $file_data = $file_data."<br></td></font>";
            $file_data1 = $file_data1."<br></td></font>";
            $file_data = $file_data.$file_data1;

            $file_data = $file_data."</tr>";

        }
        $file_data = $file_data."</table>";

        
                $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);
    }

    public function budgetFileImportAction()
    {
        $rawBody  = file_get_contents("php://input");
        // $postData = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $reportRepository->budgetFileImport();       
                return new JsonModel([
                    'status' => 200,
                    'message'   => 'File imported successfully',
                ]);
    }


    public function suratTawaranKompInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getVehicleLoanData((int)$postData['idLoan']);
       
        $purchase_amount = number_format((float) $result['f043fpurchasePrice'], 2, '.', '');
        $loan_amount = number_format((float)$result['f043floanAmount'], 2, '.', '');
        $deposit = $purchase_amount - $loan_amount;
        $insurance_amount = number_format((float)$result['f043insuranceAmt'], 2, '.', '');
        $monthly_installment = number_format((float)$result['f043fmonthlyInstallment'], 2, '.', '');
        $loan_period = $result['f043finstallmentPeriod'];
        $model =$result['f043finstallmentPeriod'];
        $vendor =$result['f030fcompanyName'];
        $offer_date = date("d/m/Y",strtotime($result['f043fapproved8Date']));
       
        $loan_insurance_deposit =  number_format((float)$result['f043ftotalLoan']+$insurance_amount+$deposit, 2, '.', '');
        $loan_insurance = number_format((float)$result['f043ftotalLoan']+$insurance_amount, 2, '.', '');

        $pay_date = date('mdY-his', time());              

        $bend = "BEND" . $pay_date ; 
        
        $file_data = "<table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><br><br><br>$bend<br><br><br>21/03/2018<br><br><br>MOHD NIZAM BIN ZAINAL ABIDIN (B5239)<br>JABATAN KESELAMATAN<br>UNIVERSITI UTARA MALAYSIA<br>06010 UUM SINTOK<br><br><br>Tuan/Puan<br></td>
      </tr>
   </table><br><br>
   <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>TAWARAN PEMBIAYAAN PEMBELIAN KOMPUTER DAN TELEFON PINTAR (SMARTPHONE) UUM</td>
      </tr><tr></tr>
   </table><br><br>
   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Dengan ini adalah dimaklumkan bahawa permohonan tuan/puan<br>
        untuk mendapatkan kemudahan pinjaman bagi tujuan membiayai<br>
        keseluruhan/sebahagian daripada harga pembelian sebuah<br>
        komputer adalah diluluskan seperti berikut:<br></td>
      </tr>
   </table><br><br>
   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(a)Harga Belian Oleh UUM<br>Amaun Pembiayaan Oleh UUM<br>Bayaran Muka Oleh Tuan/Puan<br></td>
         <td></td>
         <td></td>
         <td>:RM $purchase_amount<br>:RM $loan_amount<br>:RM $deposit<br></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(b)Harga Jualan Kepada Pegawai<br>Bayaran Muka Oleh Tuan/Puan<br>
            Bayaran Amaun Pembiayaan dan<br>Keuntungan UUM<br>Jumlah Insuran Perlindungan<br>Bayaran Ansuran Bulanan<br>Tempoh Bayaran Balik<br></td>
         <td></td>
         <td></td>
         <td>:RM $loan_insurance_deposit<br>:RM $deposit<br>:RM $loan_insurance<br><br>:RM $insurance_amount<br>:RM $monthly_installment<br>:$loan_period bulan<br></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(c)Butir-butir<br>Komputer dan Telefon Pintar (Smartphone)<br>Model dan Buatan<br>Nama Pembekal<br>Spesifikasi<br>Pencetak<br>Edaran Bulan<br>Bertarikh<br></td>
         <td></td>
         <td></td>
         <td><br><br>:$model<br>:$vendor<br>:Seperti Dalam Sebutharga<br>:Seperti Dalam Sebutharga<br>:April<br>: $offer_date<br></td>
       </tr>
    </table><br><br><br><br><br><br><br><br><br><br><br>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>2.Tawaran ini tertakluk kepada syarat-syarat berikut:</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(i) Tuan/Puan dikehendaki melengkapkan Perjanjian Pembiayaan
            Pembelian Komputer dan Telefon Pintar (smartphone) sebelum
            barang dapat dibekalkan,
            cukai setem ditanggung oleh
            tuan/puan;</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(ii) Tuan/Puan dilantik untuk mewakili UUM dalam urusan pembelian
        komputer dan telefon pintar (smartphone) tersebut;</td>
      </tr>
   </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(iii) Kemudahan pembiayaan pembelian komputer dan telefon pintar
        (smartphone) ini tidak boleh digunakan untuk tujuan yang lain;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(iv) Spesifikasi komputer dan telefon pintar (smartphone) yang
        dipersetujui untuk dibeli daripada syarikat yang disenaraikan tidak
        boleh dibatalkan atau ditukarkan;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(v) Tuan/Puan bertanggungjawab untuk menjelaskan bayaran
        ansuran bulan pertama jika potongan dari gaji bulanan tidak
        sempat dibuat;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(vi) Tuan/Puan bertanggungjawab menyerahkan resit pembelian asal
        sebagai bukti pembelian komputer dan telefon pintar
        (smartphone) dalam masa satu (1) bulan dari pembayaran
        dilakukan;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(vii) Tuan/Puan juga tertakluk kepada lain-lain syarat dan peraturan
        yang ditetapkan dalam Skim Pembiayaan Pembelian komputer
        dan telefon pintar (smartphone) UUM dan juga lain-lain arahan
        dan ketetapan yang dikeluarkan oleh Perbendaharaan dari masa
        ke semasa berkaitan dengan pembiayaan pembelian komputer
        dan telefon pintar (smartphone). Tindakan tatatertib dan/atau
        surcaj boleh diambil terhadap tuan/puan sekiranya tuan/puan
        didapati gagal mematuhi mana-mana peraturan yang ditetapkan
        dalam Pekeliling dan arahan tersebut.</td>
      </tr>
   </table>
   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>3.Sekiranya Tuan/Puan bersetuju dengan syarat-syarat pembiayaan sila
        tandatangani memorandum penerimaan tawaran dan kembalikan
        salinannya dalam tempoh 14 hari dari tarikh surat ini. Jika jawapan tidak
        diterima dalam tempoh tersebut, tawaran ini akan dianggap luput.<br><br>Seklan, terima kasih.<br><br><br><b>ILMU BUDI BAKTI'</b><br><br>Saya yang menurut perintah,<br><br><br><br><br><b>(NIKMAL MUZAL BIN MOHD MUHAIYUDDIN)</b><br>Penolong Bendahari Kanan<br>b.p. Bendahari<br><br></p></td>
        </tr>
    </table>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>Pendaftar</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Universiti Utara Malaysia</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Unit Pinjaman</td>
      </tr>
      </table><br><br><br><br><br><br><br><br><br><br><br><br>

      <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        </tr>
        </table>

        <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>UNIVERSITI UTARA MALAYSIA</td>
      </tr><tr></tr>
   </table>
   <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>SURAT TAWARAN PEMBIAYAAN PEMBELIAN
        KOMPUTER DAN TELEFON PINTAR (SMARTPHONE)
        UNIVERSITI UTARA MALAYSIA<br><br><br>MEMORANDUM PENERIMAAN</td>
      </tr><tr></tr>
   </table><br><br><br>
   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Merujuk kepada surat tawaran tuan bil (BEND________________________)<br>
        bertarikh _______________________ dengan ini saya menerima tawaran<br>
        tersebut dan bersetuju dengan syarat-syarat serta peraturan-peraturan yang<br>
        dikenakan seperti yang terkandung dalam surat berkenaan. Sila buat pilihan<br>
        Perlindungan Takaful seperti dibawah :<br><br></td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><input type='checkbox'>Syarikat Takaful Malaysia Berhad <br><br><input type='checkbox'>Syarikat Etiqa Berhad</td>
        </tr>
    </table><br><br><br>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
    <tr>
         <td>Ditandatangani oleh<br><br>No. K/P<br>
            <br>No. Pekerja<br><br>Jawatan<br><br>Jabatan<br><br>Tarikh<br><br></td>
         <td></td>
         <td></td>
         <td>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br></td>
       </tr>
    </table>




   ";

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }
    public function suratTawaranPinjamanInvoiceAction()
    {

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        $result         = $reportRepository->getVehicleLoanData((int)$postData['idLoan']);
       
        $purchase_amount = number_format((float) $result['f043fpurchasePrice'], 2, '.', '');
        $loan_amount = number_format((float)$result['f043floanAmount'], 2, '.', '');
        $deposit = $purchase_amount - $loan_amount;
        $insurance_amount = number_format((float)$result['f043insuranceAmt'], 2, '.', '');
        $monthly_installment = number_format((float)$result['f043fmonthlyInstallment'], 2, '.', '');
        $loan_period = $result['f043finstallmentPeriod'];
        $model =$result['f043finstallmentPeriod'];
        $vendor =$result['f030fcompanyName'];
        $offer_date = date("d/m/Y",strtotime($result['f043fapproved8Date']));
       
        $loan_insurance_deposit =  number_format((float)$result['f043ftotalLoan']+$insurance_amount+$deposit, 2, '.', '');
        $loan_insurance = number_format((float)$result['f043ftotalLoan']+$insurance_amount, 2, '.', '');
        // $result         = $reportRepository->getActivityInvoice();   
        // print_r($result);
        // exit();
        $pay_date = date('mdY-his', time());              

        $bend = "BEND" . $pay_date ; 
        
        $file_data = "<table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><br><br><br>$bend<br><br><br>21/03/2018<br><br><br>HASIMAH BINTI SAPIRI (B1879)<br>JPUSAT PENGAJIAN SAINS KUANTITATIF<br>UNIVERSITI UTARA MALAYSIA<br>06010 SINTOK<br><br><br>Tuan/Puan<br></td>
      </tr>
   </table><br><br>
   <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>TAWARAN PEMBIAYAAN PEMBELIAN KENDERAAN UUM</td>
      </tr><tr></tr>
   </table><br><br>
   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Dengan ini adalah dimaklumkan bahawa permohonan tuan/puan
        untuk mendapatkan kemudahan pinjaman bagi tujuan membiayai
        keseluruhan/sebahagian daripada harga pembelian sebuah
        kenderaan adalah diluluskan seperti berikut:<br></td>
      </tr>
   </table><br><br>
   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(a)Harga Belian Oleh UUM<br>Amaun Pembiayaan Oleh UUM<br>Bayaran Muka Oleh Tuan/Puan<br></td>
         <td></td>
         <td></td>
         <td>:RM $purchase_amount<br>:RM $loan_amount<br>:RM $deposit<br></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(b)Harga Jualan Kepada Pegawai<br>Bayaran Muka Oleh Tuan/Puan<br>
            Bayaran Amaun Pembiayaan dan<br>Keuntungan UUM<br>Jumlah Insuran Perlindungan<br>Bayaran Ansuran Bulanan<br>Tempoh Bayaran Balik<br></td>
         <td></td>
         <td></td>
         <td>:RM $loan_insurance_deposit<br>:RM $deposit<br>:RM $loan_insurance<br><br>:RM $insurance_amount<br>:RM $monthly_installment<br>:$loan_period bulan<br></td>
       </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       <tr>
         <td>(c)Butir-butir Kenderaan<br>Jenis Kenderaan<br>Keadaan Kenderaan<br>Model<br>Buatan<br>Sukatan Silinder<br>Bertarikh Kelulusan<br></td>
         <td></td>
         <td></td>
         <td><br><br>:$model<br>:$vendor  <br>:BIKE JUNGLE<br>:GERMANY <br>:-<br>: $offer_date<br></td>
       </tr>
    </table><br><br><br><br><br><br><br><br><br><br><br>
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>2.Tawaran ini tertakluk kepada syarat-syarat berikut:</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(i) Tuan/Puan dikehendaki melengkapkan Perjanjian Pembiayaan
            Pembelian Komputer dan Telefon Pintar (smartphone) sebelum
            barang dapat dibekalkan,
            cukai setem ditanggung oleh
            tuan/puan;</td>
      </tr>
   </table>

   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(ii) Tuan/Puan dilantik untuk mewakili UUM dalam urusan pembelian
        komputer dan telefon pintar (smartphone) tersebut;</td>
      </tr>
   </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(iii) Kemudahan pembiayaan pembelian komputer dan telefon pintar
        (smartphone) ini tidak boleh digunakan untuk tujuan yang lain;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(iv) Spesifikasi komputer dan telefon pintar (smartphone) yang
        dipersetujui untuk dibeli daripada syarikat yang disenaraikan tidak
        boleh dibatalkan atau ditukarkan;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(v) Tuan/Puan bertanggungjawab untuk menjelaskan bayaran
        ansuran bulan pertama jika potongan dari gaji bulanan tidak
        sempat dibuat;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(vi) Tuan/Puan bertanggungjawab menyerahkan resit pembelian asal
        sebagai bukti pembelian komputer dan telefon pintar
        (smartphone) dalam masa satu (1) bulan dari pembayaran
        dilakukan;</td>
      </tr>
    </table>
    <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>(vii) Tuan/Puan juga tertakluk kepada lain-lain syarat dan peraturan
        yang ditetapkan dalam Skim Pembiayaan Pembelian komputer
        dan telefon pintar (smartphone) UUM dan juga lain-lain arahan
        dan ketetapan yang dikeluarkan oleh Perbendaharaan dari masa
        ke semasa berkaitan dengan pembiayaan pembelian komputer
        dan telefon pintar (smartphone). Tindakan tatatertib dan/atau
        surcaj boleh diambil terhadap tuan/puan sekiranya tuan/puan
        didapati gagal mematuhi mana-mana peraturan yang ditetapkan
        dalam Pekeliling dan arahan tersebut.</td>
      </tr>
   </table>
   <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>3.Sekiranya Tuan/Puan bersetuju dengan syarat-syarat pembiayaan sila
        tandatangani memorandum penerimaan tawaran dan kembalikan
        salinannya dalam tempoh 14 hari dari tarikh surat ini. Jika jawapan tidak
        diterima dalam tempoh tersebut, tawaran ini akan dianggap luput.<br><br>Seklan, terima kasih.<br><br><br><b>ILMU BUDI BAKTI'</b><br><br>Saya yang menurut perintah,<br><br><br><br><br><b>(NIKMAL MUZAL BIN MOHD MUHAIYUDDIN)</b><br>Penolong Bendahari Kanan<br>b.p. Bendahari<br><br></p></td>
        </tr>
    </table>
        <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
      <tr>
         <td>s.k:</td>
         <td>-</td>
         <td>Pendaftar</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Universiti Utara Malaysia</td>
      </tr>
      <tr>
        <td></td>
        <td>-</td>
        <td>Unit Pinjaman</td>
      </tr>
      </table><br><br><br><br><br><br><br><br><br><br><br><br>

      <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
        <tr>
        <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        </tr>
        </table>

        <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>UNIVERSITI UTARA MALAYSIA</td>
      </tr><tr></tr>
   </table>
   <table  style='border:0px solid black;width: 100%;font-weight: bold;height:50px;margin-top: 4;'>
       <tr>
      <tr>
         <td align='center'>SURAT TAWARAN PEMBIAYAAN PEMBELIAN
        KOMPUTER DAN TELEFON PINTAR (SMARTPHONE)
        UNIVERSITI UTARA MALAYSIA<br><br><br>MEMORANDUM PENERIMAAN</td>
      </tr><tr></tr>
   </table><br><br><br>
   <table align='center' style='border:0px solid black;width: 80%;height:50px;margin-top: 4;'>
       
      <tr>
         <td>Merujuk kepada surat tawaran tuan bil (BEND________________________)<br>
            bertarikh _______________________ dengan ini saya menerima tawaran<br>
            tersebut dan bersetuju dengan syarat-syarat serta peraturan-peraturan yang<br>dikenakan seperti yang terkandung dalam surat berkenaan. Sila buat pilihan<br>Perlindungan Takaful seperti dibawah :<br><br></td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
      <tr>
         <td><input type='checkbox'>Syarikat Takaful Malaysia Berhad <br><br><input type='checkbox'>Syarikat Etiqa Berhad</td>
        </tr>
    </table><br><br><br><br>
    <table align='center' style='border:0px solid black;width: 60%;height:50px;margin-top: 4;'>
    <tr>
         <td>Ditandatangani oleh<br><br>No. K/P<br>
            <br>No. Pekerja<br><br>Jawatan<br><br>Jabatan<br><br>Tarikh<br><br></td>
         <td></td>
         <td></td>
         <td>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br>: ______________________________<br><br></td>
       </tr>
    </table>



   ";

          $file_data.= "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
        return new JsonModel([
            'status' => 200,
            'name'   => $name,
        ]);
    }

    public function updateTableAction()
    {

      
        $table = $this->params()->fromRoute('table');
        
        $em = $this->getEntityManager();
        $request = $this->getRequest();
        $reportRepository = $this->getRepository('T019fsavedReports');
        
        $reportRepository->updateTable($table);
        return new JsonModel([
            'status' => 200,
            'result' => "Updated Successfully"
        ]);
    }
}
?>
