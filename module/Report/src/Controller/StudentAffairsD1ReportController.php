<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentAffairsD1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function studentAffairsD1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          // $id = $postData['id'];

          // $reportRepository = $this->getRepository('T019fsavedReports');
          // $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $file_data = "
<html>
<head>
  <title></title>
</head>

<body>
  <table width='100%'>
    <tr>
      <td><img src='' width='260' height='160'/></td>
      <td></td>
      <td style='text-align: right'>
        <font size='4px'>
        Jabatan Hal Ehwal Pelajar & Alumni<br>
        06010 UUM Sintok <br>
        Kedah Darul Aman, Malaysia <br>
        Tel: (604) 928 3205 <br>
        Faks: (604) 928 3216 <br>
        www.uum.edu.my
        </font>
      </td>
    </tr>
    </table>
    <hr>
   <table width='100%'>
      <tr>
        <td style='text-align: center'><font size='5px'><b>'MUAFAKAT KEDAH'</font></td>
      </tr>
      <tr>
        <td style='text-align: right'>
        <font face='sans-serif' size='4px'>
          POS LAJU <br>
          <b>UUM/HEP/U-ASSIST/K-27/4/1(255) <br>
          29 SEPTEMBER 2017 </b><br></font></td>
      </tr>
   </table>
   <table>
      <tr>
        <td><font size='5px'>
            Ketua Setiausaho <br>
            Bahagian Biasiswa <br>
            Kementerian Pendidikan Tinggi Malaysia <br>
            Aras 2 No 2 Menara 2 <br>
            Jalan P 5/6 Presint 5 <br>
            Pusat Pentadbiran Kerajaan Persekutuan <br>
            62100 Putrajayo <br>
            (u/p:Pn Nazatul Shima Md Noor) <br><br>
        </td>
      </tr>
      <tr>
        <td><font size='5px'><br/>Tuan,</font></td>
      </tr>
      <tr>
        <td><font size='5px'><b>TUNTUTAN YURAN PENGAJIAN PELAJAR PhD DI BAWAH TAJAAN KPM (MYBRAIN15) UNTUK SEMESTER A171 (SEPT 2017-JAN 2018)</b></font>
        </td>
      </tr>
      </table>
      <table><font size='5'>
      tr>
        <td>
          Dengan hormatnya saya merujuk kepada perkara di atas
        </td>
      </tr>
      <tr>
        <td>
          2. Disertakan di sini penyata tuntutan yuran 65 orang pelajar PhD di bawah tajaan
          pihak tuan berjumlah <b>RM 123,295.00</b> untuk dibuat bayaran
        </td>
      </tr>
      <tr>
        <td>
          3. Sila keluarkan cek atas nama <b>'Universiti Utara Malaysia'</b> can dipalang <b>'Akaun
          Penerima Sahaja'</b> atau email kepada <a href='nozila@uum.edu.my'>nozila@uum.edu.my</a>/
          <a href='scholarship@uum.edu.my'>scholarship@uum.edu.my</a> untuk makluman bayaran yang telah dibuat kepada pihak universiti. Harap maklum.
        </td>
      </tr>
      <tr>
      <td>
          Sekian, terima kasih.
      </td>
      </tr>
      <tr>
        <td>
          <br>
          <b>'BERKHIDMAT UNTUK NEGARA'</b><br>
          <b>'ILMU BUDI BAKTI'</b><br>
          <br>
            Saya yang menurut perintah
        </td>
      </tr>
      </font>
    </table>
    <table>
    <tr>
        <td>
          As such, please make payment to our account : <b>Universiti Utara Malaysia</b> at Bank Islam Malaysia Berhad, Universiti Utara Malaysia Branch, 06010 Sintok, Kedah <b>(Accounl No, 020-9301-00000-1O), Swift Code: BIMBMYKL, Bank Telephone No 604-9286766 and send the payment advice for our reference to 
          <a href='nazila@uum.edu.my'>nazila@uum.edu.my</a> / <a href='scholarship@uum.edu.my'> scholarship@uum.edu.my</a>.
        </td>
    </tr>
    <tr>
        <td><font size='5'><br>Thank You.</font></td>
    </tr>
    <tr>
        <td>
          Yours Sincerely,
        </td>
    </tr>
    <tr>
        <td><b>(NOOR NAZILA NAZREN)</b><br>Penolong Bendahari<br>b/p Pengarah U-Assist<br>Universiti Utara Malaysia<br>s.k.: Fall <br>
    </td>
      </tr>
      <tr>
        <td></td>
      </tr>
      <tr>
        <td style='text-align: right'><b>Universiti Pengurusan Terkemuka</b></td>
      </tr>
  </table>
      
    ";

      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}