<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class RetirementStatementO1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// retirementStatementO1Report O-1
    public function retirementStatementO1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = "
   <html>
<head>
  <title>UUM</title>
</head>
<body>
  <form>
    <table align='center' width='100%'>
      <tr>
      <th style='text-align: center;  width: 20%'>UNIVERSITI UTARA MALAYSIA</th>
      </tr>
      <tr>
      <th style='text-align: center; width: 22%'>PENYATA SARAAN DAN POTONGAN</th>
      </tr><br>
      <tr>
      <th style='text-align: center; width: 22%'>BAGI TAHUN 2017</th>
      </tr>
    </table>
    <table align='center' width='100%'>
      <tr>
        <td>NAMA :</td>
        <td>DATO' MOHAMAD AKAHIR B HJ YUSUF</td>

      </tr>
      <tr>
        <td>NO STAF:</td>
        <td>122</td>
      </tr>
      <tr>
        <td>JAWATAN:</td>
        <td>PENDAFTAR (VU7)</td>
      </tr>
    
    </table>
    <table align='center' width='100%'>
      <tr>
       <td style='text-align: center; width:50%'></td>
        <td style='text-align: right;'>PUSAT   T/J :</td>
        <td style='text-align: center;'> JABATAN PENDAFTAR</td>

      </tr>
      <tr>
      <td style='text-align: center; width:50%'></td>
        <td style='text-align: right;'>NOK/P:</td>
        <td style='text-align: center;'>271109022612</td>
      </tr>
      <tr>
      <td style='text-align: center; width:50%'></td>
        <td style='text-align: right;'>NO AKAUN:</td>
        <td style='text-align: center;'>02120000440226</td>
        </tr>
    </table><br><br>

    <table border='1' align='center' width='100%' cellpadding='0' cellspacing='0'>
      <tr>
      <td style='text-align: center; width:40%'><font size='3'>PERKARA BULAN</font></td>
      <td style='text-align: center; width:3%'><font size='3'>JAN<br>(RM)</font></td>
      <td style='text-align: center; width:3%'><font size='3'>FEB<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>MAC<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>APR<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>MEI<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>JUN<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>JUL<br>(RM)</font></td>
      <td style='text-align:  center; width:3%'><font size='3'>OGOS<br>(RM)</font></td>
      <td style='text-align:  center; width:4%'><font size='3'>SEPT<br>(RM)</font></td>
      <td style='text-align:  center; width:4%'><font size='3'>OKT<br>(RM)</font></td>
      <td style='text-align:  center; width:4%'><font size='3'>NOV<br>(RM)</font></td>
      <td style='text-align:  center; width:4%'><font size='3'>DIS<br>(RM)</font></td>
      <td style='text-align:  center; width:20%'><font size='3'>JUMLAH<br>(RM)</font></td>
    </tr>


      <tr>
      <td style='text-align: center; '><font size='3'>PENNDAPATAN </font></td>
      <td style='text-align: center; '><font size='3'></font></td>
      <td style='text-align: center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      </tr>

      <tr>
      <td style='text-align: center; '><font size='3'>Gaji Pokok</font></td>
      <td style='text-align: center; '><font size='3'>17242.92</font></td>
      <td style='text-align: center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>4598.11</font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'>177,027.30</font></td>
    </tr>
      <tr>
      <td style='text-align: center; '><font size='3'>JUMLAH PENDAPATAN</font></td>
      <td style='text-align: center; '><font size='3'>17242.92</font></td>
      <td style='text-align: center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>17242.92</font></td>
      <td style='text-align:  center; '><font size='3'>24,287.34</font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      </tr>


      <tr>
      <td style='text-align: center; '><font size='3'>JUMLAH POTONGAN</font></td>
      <td style='text-align: center; '><font size='3'>7.00</font></td>
      <td style='text-align: center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      </tr>

      <tr>
      <td style='text-align: center; '><font size='3'>GAJI BERISH</font></td>
      <td style='text-align: center; '><font size='3'>7.00</font></td>
      <td style='text-align: center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      <td style='text-align:  center; '><font size='3'></font></td>
      <td style='text-align:  center; '><font size='3'>7.00</font></td>
      </tr>
    </table><br>

    <table align='center' width='100%'>
      <tr>
        <td style='text-align: left; width:10%'></td>
        <td style='text-align: left; width:90%'>BAKI POKOK PADA 31 DISEMBER 2017 :</td>
      </tr>
    </table>
    <table align='center' width='100%'>
      <tr>
        <td style='text-align: left; width:15%'></td>
        <td style='text-align: left; width:25%'>PINJAMAN KENDERAAN UUM</td>
        <td style='text-align: left; width:5%'>(RM)</td>
        <td style='text-align: left; width:5%'>:</td>
        <td style='text-align: left; width:50%'></td>
      </tr>
      <tr>
        <td style='text-align: left; width:15%'></td>
        <td style='text-align: left; width:25%'>PINJAMAN PERUMAHAN UUM</td>
        <td style='text-align: left; width:5%'>(RM)</td>
        <td style='text-align: left; width:5%'>:</td>
        <td style='text-align: left; width:50%'></td>
      </tr>
      <tr>
        <td style='text-align: left; width:15%'></td>
        <td style='text-align: left; width:25%'>PINJAMAN KOMPUTER UUM</td>
        <td style='text-align: left; width:5%'>(RM)</td>
        <td style='text-align: left; width:5%'>:</td>
        <td style='text-align: left; width:50%'></td>
      </tr>
      <tr>
        <td style='text-align: left; width:15%'></td>
        <td style='text-align: left; width:25%'>PENDAHULUAN BELUM SELESSAI</td>
        <td style='text-align: left; width:5%'>(RM)</td>
        <td style='text-align: left; width:5%'>:</td>
        <td style='text-align: left; width:50%'></td>
      </tr>
    </table>
      <table align='center' width='100%'>
      <tr>
        <td style='text-align: left; width:10%'></td>
        <td style='text-align: left; width:90%'>LAMPIRAN INI HANYA UNTUK RUJUKAN DAN SIMPANAN TUNAN/PUAN</td>
      </tr>
      <tr>
        <td style='text-align: left; width:10%'></td>
        <td style='text-align: left; width:90%'>No. SIRI : 155</td>
      </tr>
    </table>
  </form>
  </body>
  </html>




";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}