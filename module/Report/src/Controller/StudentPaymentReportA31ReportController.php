<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentPaymentReportA31ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function studentPaymentReportA31ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 

          $porder_data = "
        <!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<table width='100%'>
        <tr>
          <td>INSTITUSI:</td>
        </tr>
        <tr>
          <td>BULAN:</td>
        </tr>
        <br>
        <tr></tr><tr></tr><tr></tr><tr></tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='5'><b>LAPORAN BAYARAN PELAJAR MyMASTER TAJAAN BAHAGIAN BIASISWA KPT - BATCH 1/2016 INTAKE : 23 FEB 2016</b></font></td>
        </tr>
    </table>
    <br><br>

    <table align='center' width='100%' border='1' style='border-collapse:collapse;'>
        <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='4'><b>BIL<font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='4'><b>NAMA PELAJAR<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>NO. KAD PENGENALAN<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>TEMPOH TAJAAN<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>STATUS<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>NAMA PROGRAM<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>BAYARAN <br> (RM)<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>RUJUKAN BAYARAN<font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'><b>JENIS TUNTUTAN<font></td>
          <td style='text-align: center' width='15%' valign='center'><font size='4'><b>RUJUKAN PTB<br>SPBB3 <br>(LAMPIRAN)<font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>1<font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'>Ali<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>xx<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>01/03/2016-01/03/2019<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>Baru<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>MyMaster<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>3,500.00<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>No & tarikh baucar/pelarasan<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>YURAN SEM 1 2016<font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'>A<font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>3<font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'>Abu<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>xx<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>01/03/2016-01/03/2019<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>Baru<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>MyMaster<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>2,500.00<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>No & tarikh baucar/pelarasan<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>YURAN SEM 1 2016<font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'>A<font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>4<font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'>Ahmad<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>xx<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>01/03/2016-01/03/2019<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>Baru<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>MyMaster<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>3,000.00<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>No & tarikh baucar/pelarasan<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'>YURAN SEM 1 2016<font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'>B<font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>.<br><font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'><font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>.<br><font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'><font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'>.<br><font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'> <font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'> <font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='4'><font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='4'><b>JUMLAH</b><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><b>9,000.00</b><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='4'><font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='4'><font></td>
        </tr>
    </table>
    <br>
    <table width='100%'>
      <tr>
        <td><font size='4' width='100%'>
          i) Disahkan maklumat diatas adalah benar dan bayaran telah dibuat sewajarnya.<br>
        (Salinan baucar bayaran hendakaih difailkan Khas untak SALAR PERUNTUKAN MYMASTER di setiap UA -Kementerian Pendidikan Tinggi akan membuat permantuan/semakan dari  masa ke semasa).</font></td>
        <td width='10%'></td>
      </tr>
      <tr>
        <td><font size='4'>
          Disediakan oleh:</font>
        </td>
        <td style='text-align: left' width='30%'><font size='4'>
          Disemak oleh:</font>
        </td>
      </tr>
      <tr>
        <td><font size='4'>
          Cop & tandatangan</font>
        </td>
        <td style='text-align: left' width='30%'><font size='4'>
          Cop & tandatangan</font>
        </td>
      </tr>
      <tr>
        <td><font size='4'>Tarikh:</font></td>
        <td style='text-align: left' width='30%'><font size='4'>Tarikh:</font></td>
      </tr>
    </table>
    </body>
    </html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}