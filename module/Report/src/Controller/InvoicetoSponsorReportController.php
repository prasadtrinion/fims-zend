<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class InvoicetoSponsorReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Sponsor List Report A-10
    public function invoicetoSponsorReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$journalId = $postData['journalId'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$journalData = $reportRepository->getJournalEntryById((int)$journalId);

        	// print_r($journalData);exit;
          $JournalDescription = $journalData[0]['f017fdescription'];
          $Approver = $journalData[0]['originalName'];

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        


        	$journal_data = "
    <table align='center' style='border:0px solid black;width: 80%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><b>Journal Entry</b></td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
    	 <tr>
          <td style='text-align: left' width='25%' valign='top'>Journal Description</td>
          <td style='text-align: left' width='75%' valign='top'>: $JournalDescription</td>
        </tr>
        <tr>
          <td style='text-align: left' width='25%' valign='top'>Approver </td>
          <td style='text-align: left' width='75%' valign='top'>: $Approver</td>
        </tr>
        <br><br>
        <tr>
          <td style='text-align: left' width='100%' valign='top' colspan='2'><b>Journal Details:</b></td>
        </tr>
    </table>

     
    <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
          <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>Sl. No</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>Fund</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>Activity Code</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>Department</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>Account Code</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>CR Amount (RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>DR  Amount (RM)</font></td>
           </tr>";

           $i = 1;
         foreach ($journalData as $details)
         {
           $Fund = $details['Fund'];
           $Account = $details['Account'];
           $Department = $details['Department'];
           $Activity = $details['Activity'];
           $drAmount = $details['f018fdrAmount'];
           $crAmount = $details['f018fcrAmount'];

           $drTotal = $details['f018fdrAmount'];
           $crTotal = $details['f018fcrAmount'];

           $drAmount = number_format($drAmount, 2, '.', ',');
           $crAmount = number_format($crAmount, 2, '.', ',');

           $journal_data = $journal_data . "
           <tr>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='18%' valign='top'><font size='2'>$Fund</font></td>
           <td style='text-align:left' width='18%' valign='top'><font size='2'>$Activity</font></td>
           <td style='text-align:left' width='18%' valign='top'><font size='2'>$Department</font></td>
           <td style='text-align:left' width='18%' valign='top'><font size='2'>$Account</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$crAmount</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$drAmount</font></td>
           </tr>";

           $totaldr  =  $totaldr + $drTotal;
           $totalcr  =  $totalcr + $crTotal;
        $i++;
        }

      }

      $totaldr = number_format($totaldr, 2, '.', ',');
      $totalcr = number_format($totalcr, 2, '.', ',');

      $journal_data = $journal_data . "
      <tr>
      <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
      <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
      <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
      </tr>

      </table>";
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
        }
}