<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentCodeA16ReportController extends AbstractAppController
{
    
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

     
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // StudentCodeA16Report A -16

    public function studentCodeA16ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        
        $file_data = $file_data ."
<html>
<head>
  <title>A16</title>
</head>
<body>
<table align='center' width='100%'>
   <tr> 
     <td style='text-align:center; width:90% '><font size='3'><b>Universiti Utara Malaysia</b></font></td>
     <td  style='text-align: right ; width: 10% '><font size='3'>3/8/2018</font></td>
     </tr>
</table><br><br>

<table align='center' width='100%'>
  <tr>
    <td style='text-align:left;'><font size='2'>KOD PENAJA : </font><br></td>
    <td style='text-align:center;'></td>
    <td style='text-align:center;'></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>NAMA PENAJA : </font><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>NAMA RINGKAS : </font></td>
    <td style='text-align:left;'><font size='2'>KATEGORI  : </font><br><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>ALAMAT : </font><br></td>
  </tr>
  <tr>
    <td style='text-align:center;'><br></td>
  </tr>
  <tr>
    <td style='text-align:center;'><br></td>
  </tr>
  <tr>
    <td style='text-align:center;'><br></td>
  </tr>
  <tr>
    <td style='text-align:center;'><br><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>DESIGNASI : </font><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>PEGAWAN : </font><br><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>NO. TELEFON : </font></td>
    <td style='text-align:left;'><font size='2'>TUNTUTAN : </font><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><font size='2'>NO. FAX : </font><br><br></td>
  </tr>


</table>

</body>
</html>";
   
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}