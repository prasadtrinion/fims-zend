<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class LetterOfInstructionController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //letter Of Instruction Report
    public function letterOfInstructionAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$applicationId = $postData['applicationId'];

        	
        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$fdWithdrawl = $reportRepository->getFdWithdrawl($applicationId);

        	$fd_data = " <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
         <tr>
        </tr>
          </table>
              <br>";
        	// print_r($fdWithdrawl);exit;
    		$i= 1;
        	foreach ($fdWithdrawl as $details)
        	{
        		$batchNo = $details['f063fbank_batch'];
        		$applicationDate = $details['f063fcreated_dt_tm'];
        		$bankId = $details['f063fbank_id'];
        		$bankname = $details['f042fbank_name'];
        		$bankBranch = $details['f063fbranch'];
        		$certificateNo = $details['f061finstitution_code'];
        		$maturitydate = $details['f063fmaturity_date'];
        		$amount = $details['f063famount'];
            $account_number = $details['f041faccount_number'];


        	             
        	$applicationDate   = date("d/m/Y", strtotime($applicationDate));
        	$maturitydate   = date("d/m/Y", strtotime($maturitydate));

        	$amount = number_format($amount, 2, '.', ',');

        	$pay_time = date('h:i:s a', time());


        	$fd_data = $fd_data . "<table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
    	 <tr>
          <td style='text-align: right' width='100%' valign='top'>$batchNo</td>
        </tr>
        <tr>
          <td style='text-align: right' width='100%' valign='top'>$applicationDate</td>
        </tr>
    </table>
    <br><br>

    <table align='center' style='border:0px solid black;width: 80%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Ahli Jawatankuasa Penyimpan Wang Lebihan<br> Universiti Utara Malaysia</td>
        </tr>
    </table>
    <br><br><br>

     <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Y.Bhg. Dato'/Tuan,</td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'><b>CADANGAN PENGELUARAN SIMPANAN TETAP
          </b></td>
        </tr>
    </table>
    <br><br><br>

     <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: left' width='15%' valign='top'><b>1.TUJUAN</td>
          <td style='text-align: left' width='85%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='15%' valign='top'><b></td>
          <td style='text-align: left' width='85%' valign='top'>Mendapatkan kelulusan mengeluarkan simpanan tetap UUM di bank-bank berikut dan dimasukkan ke Akaun Semasa UUM (nombor akaun $account_number) di Bank Islam Malaysia Berhad, cawangan UUM:-</td>
        </tr>
    </table>
    <br><br>

    <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
	<tr>
   		<th width='10%'>BIL.</th>
   		<th width='25%'>NAMA BANK</th>
   		<th width='20%'>No. SIJIL</th>
   		<th width='25%'>TARIKH MATANG</th>
   		<th width='20%'>JUMLAH (RM)</th>

   	</tr>
   	<tr>
   		<td style='text-align: center' valign='top' width='10%'>$i.</td>
   		<td style='text-align: center' valign='top' width='25%'>$bankname<br>$bankBranch</td>
   		<td style='text-align: center' valign='top' width='20%'>$certificateNo</td>
   		<td style='text-align: center' valign='top' width='25%'>$maturitydate </td>
   		<td style='text-align: right' valign='top' width='20%'>$amount</td>
   	</tr>
   	<tr>
   		<td style='text-align: right' valign='top' width='80%' colspan='4'><b>JUMLAH(RM)</b></td>
   		<td style='text-align: right' valign='top' width='20%'><b>$amount</b></td>
   	</tr>
   	</table>
   	<br><br>

   	<table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>2. Keputusan Jawatankuasa Penyimpan Wang Lebihan Universiti Utara Malaysia.</td>
        </tr>
        <br><br><br><br>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Dipersetujui</td>
        </tr>
    </table>
    <br><br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td style='text-align: left' width='100%' valign='top'><b>admin</b></td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Naib Conselor</td>
        </tr>
        <hr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'><b>user</b></td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Bendahari</td>
        </tr>
        <hr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'><b>Noriza</b></td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top'>Pemangku Pendaftar</td>
        </tr>
        <hr>";
    $i ++;
		  }
    }
    $fd_data = $fd_data . "</table>";
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($fd_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}