<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class UnderPayableListedReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report4
    public function underPayableListedReportAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'><b>UNIVERSITI UTARA MALAYSIA<br> JABATAN BENDAHARI<br>LAPORAN DAFTAR BIL YANG SUDAH DIBAYAR</b></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='70%'><font size='3'>KUMPULAN WANG: </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='30%'><font size='3'>PUSAT TANGGUNGJAWAB: 0100 - 0100</font></td>
            </tr>
            <tr>
             <td style='text-align: left;' width='70%'><font size='3'>DARI : $pay_date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HINGGA : $pay_date</font></td>
            </tr>
            </table>
            <br>

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='3'><b>Bil</b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b>Tarikh D. Bil</b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b>No. Daftar Bil</b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b>Tahun Daftar Bil</b></font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='3'><b>No. Baucar</b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b>No. ABB</b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b>Tarikh Proses</b></font></td>
           <td style='text-align:left' width='30%' valign='top'><font size='3'><b>Nama Penerima</b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b>Amaun (RM)</b></font></td>
           <td style='text-align:center' width='9%' valign='top'><font size='3'><b>Bil Hari</b></font></td>
           </tr>
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='30%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'><b><hr></b></font></td>
           <td style='text-align:center' width='9%' valign='top'><font size='3'><b><hr></b></font></td>
           </tr>
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='3'>1.</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'>09/09/2019</font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'>209</font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'>2017</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='3'>B129/3</font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='3'>54</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='3'>24/10/2019</font></td>
           <td style='text-align:left' width='30%' valign='top'><font size='3'>TENGA NANAN</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='3'>2,345.00</font></td>
           <td style='text-align:center' width='9%' valign='top'><font size='3'>21</font></td>
           </tr>

        </table>
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}