<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class AccountcodeController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
        
    }
    public function accountCodeAction()
    {
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');
        
        // print_r($result);exit();
        $file_data = "
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
        <tr>
         <td align='left' width='10%'><img src='' height='50' width='50' ></td>
         <td style='text-align: left'  font-size='13' width='90%' valign='top'><b>UNIVERSITY UTARA MALAYSIA<br>Sintok,Kedah Darul Aman,Malaysia</b></td>
        </tr>
    </table>

    <table  style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
       
        <tr>
         <td><b>Name</b></td>
         <td><b> - </b></td>
         <td><b>Account Code</b></td>
        </tr>";
        $result         = $reportRepository->getAccountData();

         foreach ($result as $account)
         {
            // print_r($account);exit();
            $account_name = $account['f059fname'];
            $account_code = $account['f059fcode'];

            $file_data = $file_data . "
            <tr>
                <td> $account_name</td>
                <td> - </td>
                <td> $account_code</td>
            </tr>
            ";
         }

         $file_data = $file_data . "</table>";

         $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
    
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);

      
    }
}