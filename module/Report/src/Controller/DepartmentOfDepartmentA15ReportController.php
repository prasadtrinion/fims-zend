<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class DepartmentOfDepartmentA15ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// departmentOfDepartmentA15Report A-15
    public function departmentOfDepartmentA15ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();


            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        

        $file_data = "
    <html>
<head>
    <title></title>
</head>
<body>
    <center>
        <h2>
        JABATAN BENDAHARI
        <br>
        UNIVERSITI UTARA MALAYSIA
        </h2>
        <p><font size='4px'>06010 UUM, Sintok,Kedah Darul Aman, Malaysla. Tel:04-9287777 / 04-9283135 <br>
Fax:04-9285764(Bendahari) / 04-9287905 (U-Assist) E-mail:<a href='#'>scholarship@uum.edu.my</a></font></p>
    </center>
    <br><br>
    <table width='100%'>
        <font size='5px'>
        <h3 align='center'><b>TUNTUTAN BAYARAN &nbsp&nbsp <i>(Invoice)</i></b></h3>
        </font>
        </caption>
        <tr>
            <td style='width: 50%;text-align: left;'>
                <font size='5px'>
                NAMA (Name):&nbsp&nbsp MUSTAFAA ABOUENABE MASAN<br><br>
                NO.MATRIK (Matric no):&nbsp&nbsp 92583<br><br>
                NO.K.P./NO.PASSPORT (I.C.No/Passport No.):&nbsp&nbsp 650874<br><br>
                TEMPOH PENGAJIAN (Poriod Of Study):&nbsp&nbsp 14/02/2016 - 17/07/2016<br><br>
                TEMPOH TAJAAN(Period Of Sponsorship):&nbsp&nbsp 06/08/2016 - 01/03/2018
                </font>
            </td>
            <td style='width: 50%;'>
                <font size='5px'>
                INVOICE NO &nbsp&nbsp&nbsp:  217/A152/92583<br>(Invoice No.)<br>
                TARIKH &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:&nbsp 10/04/2016<br>(Date)<br>
                RUJUKAN KAMI &nbsp&nbsp&nbsp&nbsp&nbsp: (255)-KB0309<br>(Our Ref.)<br>
                RUJUKAN LAIN &nbsp&nbsp&nbsp&nbsp&nbsp: UUM/HEP/U-ASSIST/K-27<br>(Other Ref.)<br>
                BAKI TAJAAN (RM) &nbsp&nbsp&nbsp&nbsp&nbsp: 10,502.00<br>(Balance)
            </font>
            </td>
        </tr>
    </table>
    <p><hr style='height: 2px; background-color: black'></p>

    <table width='100%'>
        <tr>
            <th style='width: 1%;'></th>
            <th style='width: 100%;text-align:left'><font size='5px'>KETERANGAN<i>(Description)</i></font></th>
            <th style='width: 45%;text-align:right;'><font size='5px'>AMAUN<i>(Amount)(RM)</i></font></th>
        </tr>
        <tr></tr>
        <tr>
            <td style='width: 1%'><font size='5%'>1.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Kebajikan(Welfare)</font></td>
            <td style='width: 50%;' align='center'><font size='5%'>150.00</td>
        </tr>
        <tr>
            <td style='width: 1%'><font size='5%'>2.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Kebajikan (Welfare)</td>
            <td style='width: 50%;' align='center'><font size='5%'>113.00</td>
        </tr>
        <tr>
            <td style='width: 1%'><font size='5%'>3.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Pengajian(Tulkam)</td>
            <td style='width: 50%;' align='center'><font size='5%'>2,405.00</td>
        </tr>
        <tr>
            <td style='width: 1%'><font size='5%'>4.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Perpustakaan (Library)</td>
            <td style='width: 50%;' align='center'><font size='5%'>100.00</td>
        </tr>
        <tr>
            <td style='width: 1%'><font size='5%'>5.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Sukan (Sports)</td>
            <td style='width: 50%;' align='center'><font size='5%'>50.00</td>
        </tr>
        <tr>
            <td style='width: 1%'><font size='5%'>6.&nbsp&nbsp</td>
            <td style='width: 50%;text-align:left;'><font size='5%'>Komputer(Computer lab)</td>
            <td style='width: 50%;' align='center'><font size='5%'>150.00</td>
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td></td>
            <td style='width: 50%;text-align:left;'><font size='5px'><b>Jumlah <i>(Total)</i></b></font></td>
            <td style='width: 50%;' align='center'><font size='5%'><b>2,855.00</b></td>
        </tr>
    </table>

    <p><b><font size='5px'>Ringgit Malaysia :<br>
    DUA RIBU SATU RATUS LIMA PULUH LAPAN DAN SEN O SAHAJA</font></b></p>
    <br><br>
    <hr style='height: 2px;background-color: black;'>

    <font size='4px'>
        
        <ol>
            <li>
                Bayaran menggunakan cek hendaklah dipalang dan dibayar kepada <b>Universiti Utara Malaysia.</b><br>
                <i>(Payment by cheque must be crossed and made payable to<b> Universiti Utara Malaysia)</b></i>
            </li>
            <li>
                Sila maklumkan ke Jabatan Bendahari (seperti alamat di atas) sekiranya bayaran dibuat secara pindahan elektronik (EFT).<br>
                <i>(Please send Payment Advice to the above address if payment is made by electronic fund transer-EFT)</i>
            </li>
            <li>
                Nama Bank / No. akaun adalah seperti berikut: Bank Islam Malaysia Berhad Cawangan UUM, Account No.02093010000010. <br>
                swift code-BIMBMYKL
            </li>
            <center><i>Inoice ini adalah cetakan komputer dan tidak memerlukan tandatangan<br> 
        This invoice is Computer generated and no signature is required</i></center>
        </ol>
    </font>

</body>
</html>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}