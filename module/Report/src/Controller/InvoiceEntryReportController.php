<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class InvoiceEntryReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report1
    public function invoiceEntryReportAction()
    {
        // print_r("expression");exit;
        $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $id = $postData['invoiceId'];
        // print_r($id);exit;
        $reportRepository = $this->getRepository('T019fsavedReports');

        $result = $reportRepository->invoiceEntryReport($id);

        // print_r($result);exit;

        $demo  = '12345678';

         // $value=substr($data, -4);
                // $value=substr($value, 0,4);

        $demo = substr($demo,0,4);
        print_r($demo);exit;


       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 



        $invoice = $result['f071finvoiceNumber'];
        $f071finvoiceType = $result['f071finvoiceType'];
        $f071fbillType = $result['f071fbillType'];
        $f071fdescription = $result['f071fdescription'];
        $f071fidCustomer = $result['f071fidCustomer'];
        $f071fidFinancialYear = $result['f071fidFinancialYear'];
        $f071finvoiceDate = $result['f071finvoiceDate'];
        $f071fapprovedBy = $result['f071fapprovedBy'];
        $f071fstatus = $result['f071fstatus'];
        $f071finvoiceTotal = $result['f071finvoiceTotal'];
        $f071fbalance = $result['f071fbalance'];
        $f071fisRcp = $result['f071fisRcp'];
        $f071finvoiceFrom = $result['f071finvoiceFrom'];













        $file_data = " 
        <table align='center' width='100%'>
    	<tr>
        	<td style='width: 100%;text-align: center;'><b>INVOIS BERSUKAI (TAX INVOICE)</b></td>
    	</tr>
		</table>

		<table align='center' width='100%' border=1>
    	<tr>
        	<td style='width: 65%;text-align: center;' rowspan = '3'><b>INVOIS BERSUKAI (TAX INVOICE)</b></td>
        	<td style='width: 20%;text-align: center;'>NO. INVOIS</td>
        	<td style='width: 15%;text-align: center;'></td>
    	</tr>
    	<tr>
        	<td style='width: 20%;text-align: center;'>KOD PENGUTANG</td>
        	<td style='width: 15%;text-align: center;'>INVOIS BERSUKAI (TAX INVOICE)</td>
    	</tr>
    	<tr>
        	<td style='width: 20%;text-align: center;'>TARIKH</td>
        	<td style='width: 15%;text-align: center;'><b>INVOIS BERSUKAI (TAX INVOICE)</b></td>
    	</tr>
    	<tr>
        	<td style='width: 65%;text-align: center;'>BUTIRAN TUNTUTAN</td>
        	<td style='width: 20%;text-align: center;'>AMAUN<br>(RM)</td>
        	<td style='width: 15%;text-align: center;'>JUMLAH (RM)</td>
    	</tr>
    	<tr>
        	<td style='width: 100%;text-align: center;' colspan=3><hr></td>
    	</tr>
    	<tr>
        	<td style='width: 65%;text-align: center;'>BUTIRAN TUNTUTAN</td>
        	<td style='width: 20%;text-align: center;'>AMAUN<br>(RM)</td>
        	<td style='width: 15%;text-align: center;'>JUMLAH (RM)</td>
    	</tr>
		</table>

		<table align='center' width='100%' border=1>
		<tr>
        	<td style='width: 100%;text-align: left;'><b>NOTA:</b></td>

		</tr>
    	<tr>
        	<td style='width: 50%;text-align: center;'>
        		<table align='center' width='100%' border=1>
				<tr>
        			<td style='width: 5%;text-align: left;' valign=;'top'>1.</b></td>
        			<td style='width: 95%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 5%;text-align: left;' valign=;'top'>2.</b></td>
        			<td style='width: 95%;text-align: left;'>Bayaran Secara Pindahan Elektronik (EFT) Perlu mengemukakan satu salinan bukti pembayaran untuk tujuan kamaskini.</td>
				</tr>
				<tr>
        			<td style='width: 5%;text-align: left;' valign=;'top'>3.</b></td>
        			<td style='width: 95%;text-align: left;'>Sila nyatakan no. invois dengan jelas ketika membuat pembayaran.</td>
				</tr>
				<tr>
        			<td style='width: 5%;text-align: left;' valign=;'top'>4.</b></td>
        			<td style='width: 95%;text-align: left;'>Kegagalan mematuhi perkara di atas akan menyababkan tidak dapat dikemaskini</td>
				</tr>
				</table>
        	</td>
        	<td style='width: 50%;text-align: center;'>
        		<table align='center' width='100%' border=1>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>SR</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>6%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>ZRL</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>0%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>ZRE</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>0%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>ES43</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>0%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>DS</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>6%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>OS</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>0%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>GS</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>0%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				<tr>
        			<td style='width: 10%;text-align: left;' valign=;'top'>AJS</b></td>
        			<td style='width: 10%;text-align: left;' valign=;'top'>6%</b></td>
        			<td style='width: 80%;text-align: left;'>Bayaran Dengan cek hendaklah atas <b>nama 'Universiti Utara Malaysia',</td>
				</tr>
				</table>
        	</td>

    	</tr>
		</table>

		<table align='center' width='100%' border=1>
				<tr>
        			<td style='width: 10%;text-align: center;' valign=;'top'><b>INVOIS INI ADALHA CETAKAN KOMPUTER DAN TANDATANGAN TIDAK DIPERLUKAN</b></td>
				</tr>
				</table>
				

				";
        }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}