<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class DepartmentA14ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// departmentA14Report A-14
    public function departmentA14ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();


            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        

        $file_data = "

    <html>
<head>
    <title></title>
</head>

<body>
    <table width='100%'>
        <tr>
            <td><img src='' width='240' height='120'></td>
            <td style='text-align: right'>
                <font size='4px'>
                <b>Jabatan Hal Ehwal Pelajar & Alumni<br>
                06010 UUM Sintok <br>
                Kedah Darul Aman, Malaysia <br>
                Tel: (604) 928 3205 <br>
                Faks: (604) 928 3216 <br>
                www.uum.edu.my</b>
                </font>
            </td>
        </tr>
        </table>
        <hr>
        <table>
        <tr>
            <td style='text-align: center'><font size='5px'><b>'KEDAH AMAN MAKMUR <font size='10px'>.</font>BERSAMA MEMACU TRANSPORMASI'</b></font>
            </td>
        </tr>
        <tr>
           <td><font size='4px'><b>UUM/HEP/U-ASSIST20160410-KKB7963<br></font></b></td>
            <td style='text-align: center'>
                <font face='sans-serif' size='4px'>
                POS LAJU </font>
            </td>
            </tr>
        </table>
        <table>
            <tr>
                <td><font size='4px'><b>07 APRIL 2016<br></b></font></td>
            </tr>
            <tr>
                <td><font size='5px'><br>
                    Academic Attache <br>
                    Block B-3 A-15 <br>
                    Megan Avenue 2 <br>
                    Jin Yap Kwan Seng <br>
                    50450 Kuala Lumpur <br>
                    Attn: Dr Samir A. Karshman/Dr Adoulmajid H.M. Ali <br>
                    Ms. Baizurz Alias/Nurul Shazrina <br><br>
                </td>
            </tr>
            <tr>
                <td><font size='5px'>Dear Sir,</font></td>
            </tr>
            </table>
            <table>
            <tr><td><br></td></tr>
            <tr>
                <td><font size='5px'><b>CLAIM TUITION FEES FOR LIBYAN SPONSORED PHD STUDENT A152 (FEB 2015 - AUGUST 2015)</b></td></font>
            </tr>
            <tr>
                <td><font size='5'>
                    With reference to the above matter, the total fees charged for your sponsored studtent is RM28,550.00/USD6,797.62 (based on standard exchange rate USD1=MYR4.20) per attached invoice.<br></font>
                </td>
            </tr>
            <tr>
                <td><font size='5'>
                    As such, please make payment to our account : <b>Universiti Utara Malaysia</b> at Bank Islam Malaysia Berhad, Universiti Utara Malaysia Branch, 06010 Sintok, Kedah <b>(Accounl No, 020-9301-00000-1O), Swift Code: BIMBMYKL, Bank Telephone No 604-9286766 and send the payment advice for our reference to 
                    <a href='nazila@uum.edu.my'>nazila@uum.edu.my</a> / <a href=''>scholarship@uum.edu.my</a>.
                    </font>
                </td>
            </tr>
            <tr>
                <td><font size='5'><br>Thank You.<br><br></font></td>
            </tr>
            <tr>
                <td><font size='5'>
                    Yours Sincerely,<br><br><br><br><br>
                </font>
                </td>
            </tr>
            </table>
            <table>
            <tr>
                <td><font size='5px'><b>(NOOR NAZILA NAZREN)</b><br>Assistant Bursur<br>
                    b/p Director U-Assist<br>Universiti Utara Malaysia</font>
                </td>
            </tr>
            <tr><td><br></td></tr>
            </table>
            <table width='100%'>
            <tr>
                <td><br><br><br></td>
                <td><img src='' align='right'><img src='' align='right'></td>
            </tr>
            <tr>
                <td></td>
                <td style='text-align: right'><b>Universiti Pengurusan Terkemuka</b></td>
            </tr>
    </table>
</body>
</html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}