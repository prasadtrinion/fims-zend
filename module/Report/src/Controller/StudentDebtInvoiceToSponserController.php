<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentDebtInvoiceToSponserController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report2
    public function studentDebtInvoiceToSponserAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td style='text-align: center' valign='top'><font size='4'><b>Invois Hutang pelajar Kepada panaja</b></font></td>
            </tr>
         </table>
         <br><br>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='80%' valign='top'><font size='3'>MOD: </font></td>
             <td style='text-align: left' width='20%' valign='top'><font size='3'>PENAJA: </font></td>
             </tr>
             <tr> 
             <td style='text-align: left' width='20%' valign='top'><font size='3'>SEM:</font></td>
             <td style='text-align: left' width='80%' valign='top'><font size='3'></font></td>
            </tr>
        </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='3'>No Invois: </font></td>
             <td style='text-align: left' width='10%' valign='top'><font size='3'> </font></td>
             <td style='text-align: right' width='60%' valign='top'><font size='3'>Tarikh: </font></td>
             <td style='text-align: left' width='15%' valign='top'><font size='3'>$pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='3'>Matrikh: </font></td>
             <td style='text-align: left' width='10%' valign='top'><font size='3'></font></td>
             <td style='text-align: right' width='45%' valign='top'><font size='3'>Tempoh Tajaan: </font></td>
             <td style='text-align: left' width='30%' valign='top'><font size='3'>$pay_date - $pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='3'>Nama: </font></td>
             <td style='text-align: left' width='85%' colspan='3' valign='top'><font size='3'></font></td>
             </tr>
             <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='3'>No. KP/No Passport: </font></td>
             <td style='text-align: left' width='10%' valign='top'><font size='3'> </font></td>
             <td style='text-align: right' width='45%' valign='top'><font size='3'>Tempoh Pengajian: </font></td>
             <td style='text-align: left' width='30%' valign='top'><font size='3'>$pay_date - $pay_date </font></td>
             </tr>
            <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='3'>Youreff: </font></td>
             <td style='text-align: left' width='10%' valign='top'><font size='3'> </font></td>
             <td style='text-align: right' width='45%' valign='top'><font size='3'>Oureff:</font></td>
             <td style='text-align: left' width='30%' valign='top'><font size='3'> UITR0009</font></td>
             </tr>
        </table>

         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='2'>Jumlah Tuntutan (RM)</font></td>
             <td style='text-align: center' width='2%' valign='top'><font size='2'>:</font></td>
             <td style='text-align: left' width='15%' valign='top'><font size='2'> </font></td>

             <td style='text-align: right' width='15%' valign='top'><font size='2'>Baki Tajaan (RM)</font></td>
             <td style='text-align: center' width='2%' valign='top'><font size='2'>:</font></td>
             <td style='text-align: left' width='19%' valign='top'><font size='2'> </font></td>
             
             <td style='text-align: right' width='15%' valign='top'><font size='2'>Jumlah Tajaan (RM)</font></td>
             <td style='text-align: center' width='2%' valign='top'><font size='2'>: </font></td>
             <td style='text-align: left' width='15%' valign='top'><font size='2'>$pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: right' width='15%' valign='top'><font size='2'>Baki terkini(RM):</font></td>
             <td style='text-align: left' width='85%' colspan='8' valign='top'><font size='2'></font></td>
             </tr>
         </table>
        <br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='10%' valign='top' valign='top'><font size='3'>Bil</font></td>
           <td style='text-align:center' width='10%' valign='top' valign='top'><font size='3'>KOD</font></td>
           <td style='text-align:center' width='65%' valign='top' colspan='2' valign='top'><font size='3'>Keterangan</font></td>
           <td style='text-align:center' width='15%' valign='top' valign='top'><font size='3'>Amaun (RM)</font></td>
           </tr>
            <tr>
           <td style='text-align:center' width='10%' valign='top' valign='top'><font size='3'>1</font></td>
           <td style='text-align:left' width='10%' valign='top' valign='top'><font size='3'>1234</font></td>
           <td style='text-align:left' width='35%' valign='top' valign='top'><font size='3'>WELFARE</font></td>
           <td style='text-align:left' width='30%' valign='top' valign='top'><font size='3'>KAJIUY</font></td>
           <td style='text-align:right' width='15%' valign='top' valign='top'><font size='3'>12,000.00</font></td>
           </tr>
           </table>";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}