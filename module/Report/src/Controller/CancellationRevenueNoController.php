<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class CancellationRevenueNoController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report9
    public function cancellationRevenueNoAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));
        $pay_year   = date("Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: left'><font size='2'><b>TAHUN DAFTAR BIL : $pay_year</b></font></td>
            </tr>
         </table>
         <br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><b>Bil</b></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'><b>NO DAFTAR BIL</b></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'><b>TARIKH DAFTAR</b></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><b>PTJ</b></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'><b>TARIKH TERIMA</b></font></td>
           <td style='text-align:center' width='25%' valign='top'><font size='2'><b>NAMA PENERIMA<b></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><b><b></font></td>
           </tr>
            <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>1</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'>749</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'>23/10/2018</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>100</font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'>23/10/2018</font></td>
           <td style='text-align:center' width='25%' valign='top'><font size='2'>BERHANANDI</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><input type='checkbox'></font></td>
           </tr>
           </tr>
        </table>
        <br><br>
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: left'><font size='2'>Sila Pilih dan tekan proses untuk membetalkan daftar bil.</font></td>
            </tr>
            <tr>
            <td font-size='8' style='text-align: left'><font size='2'>Bilangan Daftar Bil : 943</font></td>
            </tr>
            <tr>
            <td font-size='8' style='text-align: left'><font size='2'>Bilangan Daftar Bil Untuk Dibatalkan ialah : 1</font></td>
            </tr>
         </table>
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}