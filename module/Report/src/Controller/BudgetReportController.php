<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class BudgetReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function budgetReportAction()
    {
    	$rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        
             $file_data = "
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       
        <tr>
         <td style='text-align: center'  font-size='13' width='100%' valign='top'><b>UNIVERSITY UTARA MALAYSIA<br>MODUL PERTANYAAN PENGURUSAN<br>LAPORAN PERBILINJAAN MENGIKUT PTJ SEHINGGA PERIOD 02/2018</b></td>
        </tr>
    </table>

    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left'><br>LAPORAN : dvr_mn6<br>MASA : $pay_time </td>
             <td style='text-align: right;'><br>TARIKH: $pay_date <br>MUKA SURAT :1/2</td>
            </tr>
    </table>

    <table  style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
       
        <tr>
         <td style='text-align: left' width='40%'><b>Jabatan</b></td>
         <td style='text-align: right'><b> Peruntukan </b></td>
         <td style='text-align: right'><b>Perbelanjaan</b></td>
         <td style='text-align: right'><b>Baki</b></td>
        </tr>
        <tr>
        <td colspan='4'><hr></td>
        </tr>";


            $accountDetail = $reportRepository->getBudgetAmountData();
             // print_r($accountDetail);exit;
            foreach ($accountDetail as $details)
            {

                $department_amount = $details['f051famount'];
                $department_name = $details['f015fdepartment_name'];
                $amount = $details['f051famount'];
                
                $department_amount = number_format($department_amount, 2, '.', ',');
                print_r($department_amount);


                $file_data = $file_data . "<tr>
         <td style='text-align: left' width='40%'>$department_name</td>
         <td style='text-align: right'> $department_amount </td>
         <td style='text-align: right'></td>
         <td style='text-align: right'></td>
        </tr>";
        $total = floatval($total) + floatval($amount);

            }

            $total = number_format($total, 2, '.', ',');

        $file_data = $file_data . "<tr>
        <td colspan='4'><hr></td>
        </tr>
        <tr>
        <td style='text-align: right' width='40%'>JUMLAH UNTUK M/S 1:</td>
        <td style='text-align: right'>$total</td>
        <td></td>
        <td></td>
        </tr>
    </table>
        ";

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

      
    }
}