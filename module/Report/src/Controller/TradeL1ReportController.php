<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class TradeL1ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // tradeReport--L1
    public function tradeL1ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        //$reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        //if($this->isJSON($rawBody))
        //{
        	        //$year = $postData['financialYear'];
        //$departmentCode = $postData['departmentCode'];
        //$fundCode = $postData['fundCode'];
       
        
            //$result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
        //$yearn = $reportRepository->getSummaryYear($year);
        //$yearn = $yearn[0]['f110fyear'];


        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = $file_data . "
        <font size='5px'>
		LAPORAN HUTANG MENGIKUT PENAJA <br>
		NAMA PENAJA : MYBRAIN <br>
		KOD PENAJA  : E 255</font></p> <br>
		<table border='1px solid black' cellpadding='12px' cellspacing='8px' style='border-collapse: collapse; font-size: 22px;'>
			<tr>
			<th>BIL</th>
			<th>TARIKH INVOIS</th>
			<th>RUJUKAN COVER LETTER/RUJUKAN<br>INVOIS</th>
			<th>SEMESTER</th>
			<th>KOD <br>SEMESTER</th>
			<th>JUMLAH<br>INVOIS</th>
			<th>JUMLAH<br>BAYARAN</th>
			<th>NO EFT :<br>RUJUKAN<br>BAYARAN<br>DARIPADA<br>PENAJA</th>
			<th>BAKI<br>TUNTUTAN<br>YANG BELUM<br>DI BAYAR</th>
			</tr>
				<tr>
					<td align='center'>1</td>
					<td align='center'>Tarikh penyediaan invois</td>
					<td align='center'>RUJ KAMI : UUM/HEP/UA/K-27/KBXXX</td>
					<td align='center'>Sep-16</td>
					<td align='center'>A161</td>
					<td align='center'>50,000.00</td>
					<td align='center'>45,000.00</td>
					<td align='center'>EFT 1102522007</td>
					<td align='center'>5,000.00</td>
				</tr>
				<tr>
					<td align='center'>2</td>
					<td align='center'>Tarikh penyediaan invois</td>
					<td align='center'>RUJ KAMI : UUM/HEP/UA/K-27/KBXXX</td>
					<td align='center'>Feb-17</td>
					<td align='center'>A162</td>
					<td align='center'>45,000.00</td>
					<td align='center'>45,000.00</td>
					<td align='center'>EFT 110225536</td>
					<td align='center'> -</td>
				</tr>
				<tr>
					<td align='center'>3</td>
					<td align='center'>Tarikh penyediaan invois</td>
					<td align='center'>RUJ KAMI : UUM/HEP/UA/K-27/KBXXX</td>
					<td align='center'>Sep-17</td>
					<td align='center'>A171</td>
					<td align='center'>80,000.00</td>
					<td align='center'>70,000.00</td>
					<td align='center'>EFT 1102XXXXX</td>
					<td align='center'>10,000.00</td>
				</tr>
				<tr>
					<td align='center'>4</td>
					<td align='center'>Tarikh penyediaan invois</td>
					<td align='center'>RUJ KAMI : UUM/HEP/UA/K-27/KB038</td>
					<td align='center'>Feb-18</td>
					<td align='center'>A172</td>
					<td align='center'>25,000.00</td>
					<td align='center'>20,000.00</td>
					<td align='center'>EFT 1102XXXXX</td>
					<td align='center'>5,000.00</td>
				</tr>
				<tr>
					<th colspan='5px'>JUMLAH KESELURUHAN TUNTUTAN</th>
					<th>200,000.00</th>
					<th>180,000.00</th>
					<th></th>
					<th>20,000.00</th>
				</tr>
		</table>
";

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}