<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SponsershipListForTheSemesterEndController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report4
    public function sponsershipListForTheSemesterEndAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
          <tr>
            <td style='text-align: left' width='18%'><font size='2'>INSTITUSI</font></td>
            <td style='text-align: center' width='2%'><font size='2'>:</font></td>
            <td style='text-align: left' width='80%'><font size='2'>UNIVERSITI UTARA MALAYSIA</font></td>
          </tr>
          <tr>
            <td style='text-align: left' width='18%'><font size='2'>NAMA PENERIMA</font></td>
            <td style='text-align: center' width='2%'><font size='2'>:</font></td>
            <td style='text-align: left' width='80%'><font size='2'>BENDAHARI UNIVERSITI TUN HUSSEIN ONN MALAYSIA</font></td>
          </tr>
          <tr>
            <td style='text-align: left' width='18%'><font size='2'>BANK</font></td>
            <td style='text-align: center' width='2%'><font size='2'>:</font></td>
            <td style='text-align: left' width='80%'><font size='2'>BANK ISLAM MALAYSIA BERHAD</font></td>
          </tr>
          <tr>
            <td style='text-align: left' width='18%'><font size='2'>NO.AKAUN</font></td>
            <td style='text-align: center' width='2%'><font size='2'>:</font></td>
            <td style='text-align: left' width='80%'><font size='2'>02341000010010</font></td>
          </tr>
          <tr>
             <td style='text-align: left' width='100%' colspan='3'><font size='2'>(Sertaken salinan penyata akaun bank untuk rujukan pemboyaran)*** </font></td>
          </tr>
        </table>
        

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
          <tr>
             <td style='text-align: center' width='100%'><font size='2'>SENARAI PELAJAR TAJAAN SLAB & SKPD BAGI SEMESTER BERAKHIR SEPTEMBER 2017(SEPTEMBER 2017-JANUARI 2018</font></td>
          </tr>
        </table>
        <br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>Bil</font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>NAMA PELAJAR </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>NO.KAD PENGENALA </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>NO MATRIK</font></th>
           <th colspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>TEMPOH TAJAAN</font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>NAMA PROGRAM</font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>STATUS </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER II 2016/2017(A161)September 2016</font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER II 2016/2017(A161) Februari 2017 </font></th>

           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER II 2016/2017(A161) September 2017 </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS (RM) </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN </font></th>
           <th rowspan='2' style='text-align:center' width='10%' valign='top'><font size='2'>CATATAN</font></th>

           </tr>

           <tr>
    
    <td>MULA</td>
    <td>TAMAT</td>
  </tr>
            <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>1.</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>Farah Fahana Binti Hamzah</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>987654321</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>98765</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>27-aug-14</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>27-aug-17</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>SLAB</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>sedia ada</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>-</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>5,000.00</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>5,000.00</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>10,000.00</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>YURAN</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'></font></td>
           </tr>


           </table>


           
        
        ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}

