<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PaymentInfoLetterController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report2
    public function paymentInfoLetterAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $Id = $postData['Id'];
        $reportRepository = $this->getRepository('T019fsavedReports');
        $payment  = $reportRepository->getPaymentInfo($Id);
        // print_r($payment);exit;
       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        	<tr>
            <td font-size='8' style='text-align: left' width='15%'><font size='2'>No. SAB :837</font></td>
            <td font-size='8' style='text-align: center' width='85%'><font size='3'><b>SULIT DAN PERSENDIRIAN</b></font></td>
            </tr>
          
            <tr>
            <td font-size='8' style='text-align: left' colspan='2'><font size='2'>$pay_date</font></td>
            </tr>
         </table>
         <br>

         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td style='text-align: left' width='70%'><font size='2'>NOOR SHUHADA BINTI MUSTAPA<br>PUSAT KESIHATAN UNIVERSITI<br>UNIVERSITI UTARA MALAYSIA<br>0E010 SINTOK<br>KEDAH </font></td>
            </tr>
           </table>
           <br>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='100%'><font size='2'>Taun/Puan</font></td>
            </tr>
            <br>
            <tr>
             <td style='text-align: left;' width='100%'><font size='2'>Mukluman Bayarai</font></td>
            </tr>
            </table>
           

            <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left;' colspan='2'><font size='2'>Arahan Bayaran secara Electronic Fund Transfer (EFT) telah dihanta ke BIMS Cawangan UUM Sintok untuk diproses.<p> Maklumt-maklumat bayaran adalah seperti berikut </font></td>
             </tr>
         </table>
         <br>";


        

          $voucherNo  = $payment['f093fvoucher_no'];
          $paymentType  = $payment['f093fpayment_type'];
          $description  = $payment['f093fdescription'];
          $paymentRefNo  = $payment['f093fpayment_ref_no'];
          $idBank  = $payment['f093fid_bank'];
          $idCompanyBank  = $payment['f093fid_company_bank'];
          $totalAmount  = $payment['total_amount'];
          $AccountNumber  = $payment['f041faccount_number'];
          $bankName = $payment['f042fbank_name'];

             $totalAmount = number_format($totalAmount, 2, '.', ',');


          // $amount = $reportRepository->getAmount($Id);


         $file_data = $file_data . "
         

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        <tr>
           <td style='text-align:left' width='25%' valign='top'><font size='2'>No. Akaun Bank</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'> $AccountNumber </font></td>
        </tr>
        <tr>
           <td style='text-align:left' width='25%' valign='top'><font size='2'>Nama Bank</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'>$bankName</font></td>
        </tr>
        <tr>
           <td style='text-align:left' width='25%' valign='top'><font size='2'>Amaun Bayaran</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'> $totalAmount </font></td>
        </tr>
        <tr>
           <td style='text-align:left' width='25%'valign='top'><font size='2'>No. Baucer Bayarar</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'> $paymentRefNo </font></td>
        </tr>
        <tr>
           <td style='text-align:left' width='25%' valign='top'><font size='2'>Perihal Bayaran</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'>$description</font></td>
        </tr>
        <tr>
           <td style='text-align:left' width='25%' valign='top'><font size='2'>No. PO/Agrement</font></td>
           <td style='text-align:center' width='3%' valign='top'><font size='2'>:</font></td>
           <td style='text-align:left' width='72%' valign='top'><font size='2'> $voucherNo </font></td>
        </tr>
        </table>";
          
          


          $file_data = $file_data . "
        <br><br> <br><br> <br><br>
        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
        <tr>
           <td style='text-align:left' width='25%'><font size='2'>Sakiran, terima kasil</td>
           </tr>
           <br><br>
           <tr>
           <td style='text-align:left' width='25%'><font size='2'>Bandahari<br>Universiti Utara Malaysia<br>No. Tel: 04 - 9283204/ 3207 </td>
           </tr>
           <br><br>
           <tr>
           <td style='text-align:left' width='25%'><font size='2'>adalah ca.-Laken tomputer. T a tandatangran cian maklumbalaa d..parluka.  </td>
           </tr>
         </table>
        ";
        }
      // print_r($file_data);exit;
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}