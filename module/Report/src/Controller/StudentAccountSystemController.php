<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentAccountSystemController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // studentAccountSystem A-18
    public function studentAccountSystemAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' width='100%'>
            <tr>
             <td style='text-align: right;' width='100%'><font size='2'>$pay_date</font></td>
            </tr>
            <tr>
            <td font-size='8' style='text-align: center'><font size='3'><b>Universiti Utara Malaysia</b><br>Sistem Akaun Pelajar<br>Laporan Invois Setiap Pelajar</font></td>
            </tr>
         </table>

        <table align='center' width='100%'>
            <tr>
             <td style='text-align: right' width='15%'><font size='2'>No. Matrik: </font></td>
             <td style='text-align: left' width='85%'><font size='2'>A151</font></td>
            </tr>
         </table>


        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>Bil</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>No. Rujukan Invois</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>Sesi / Sem</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>Jumlah Tuntutan (RM)</font></td>
           </tr>
           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>1</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>Matrik</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>NO. KP</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>Tajan (RM)</font></td>
           </tr>
           </table>";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}