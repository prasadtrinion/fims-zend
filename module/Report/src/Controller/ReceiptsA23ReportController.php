<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReceiptsA23ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function receiptsA23ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title>A23</title>
</head>
<body>
<form>
  <table width='100%'>
    <tr>
      <td style='width: 100%;text-align: left;'>
        INSITITUSI :
        <br>
        BULAN :____________<br><br>
      </td>
    </tr>
    <tr>
      <td style='width: 100%;text-align: center;'>
        <b>LAPORAN PENYATA TERIMAAN DAN BAYARAN (PTB) YURAN & TIKET PENERBANGAN PROGRAM SLAB & SKPD PADA 31 DISEMBER 2015</b>
      </td>
    </tr>
  </table><br><br>
  <table width='100%' border='1'>
    <tr>
      <th colspan='4'>TERIMAAN</th>
      <th colspan='4'>BAYARAN</th>
    </tr>
    <tr>
      <th>TARIKH</th>
      <th>PERKARA</th>
      <th>RUJUKAN</th>
      <th>RM</th>
      <th>TARIKH</th>
      <th>PERKARA</th>
      <th>RUJUKAN</th>
      <th>RM</th>
    </tr>
    <tr>
      <td>TARIKH</td>
      <td>PERKARA</td>
      <td>RUJUKAN</td>
      <td>RM</td>
      <td>TARIKH</td>
      <td>PERKARA</td>
      <td>RUJUKAN</td>
      <td>RM</td>
    </tr>
    <tr>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
    </tr>
    <tr>
      <td></td>
      <td>JUMLAH TERIMAAN (i)</td>
      <td></td>
      <td>23,000.00</td>
      <td colspan='3'>JUMLAH BAYARAN (ii)</td>
      <td>23,000.00</td>
    </tr>
    <tr>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td colspan='3'>BAKI PERUNTUKAN PADA 32.12.2015 [(iii)=(i)-(ii)]</td>
      <td>1,500.00</td>
    </tr>
    <tr>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
      <td height='30px'></td>
    </tr>
    <tr>
      <td></td>
      <td>JUMLAH</td>
      <td></td>
      <td>23,000.00</td>
      <td></td>
      <td>JUMLAH</td>
      <td></td>
      <td>23,000.00</td>
    </tr>
  </table>
  <table width='100%'>
    <tr>
      <td style='width: 100%;'>* LAMPIRAN adalah makulumat lengakap bayaran seperti di Borang SPBB3<br><br></td>
    </tr>
    <tr>
      <td style='width: 100%;'>
        i) Disahkan makulmat diatas adalah benar dan bayaran telah dibuat sewajarnya.<br>
        ii) Disahkan telah menerima sejumlah peruntukan berujumlah .......RM23,000.00...dan wang tersebut telah direkodkan ..terimaan AKAUN Amanah atau ...<br>
        iii) Disahkan sehingga 31/12/2015 peruntukan telah berbi]aki RM...1,500.00.....<br><br><br><br>
      </td>
    </tr>
    <tr>
      <td style='width: 10%;text-align: left;'>
        Disediakan oleh:<br>
        Cop & tandatangan<br><br><br><br>
        Tarikh
      </td>
      <td style='width: 90%;text-align: center;'>
        Disediakan oleh:<br>
        Cop & tandatangan<br><br><br><br>
        Tarikh
      </td>
      
    </tr>
  </table>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}