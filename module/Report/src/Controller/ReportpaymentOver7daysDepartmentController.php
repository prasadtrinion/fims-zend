<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportpaymentOver7daysDepartmentController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report8
    public function reportpaymentOver7daysDepartmentAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $department = $postData['departmentId'];

        $reportRepository = $this->getRepository('T019fsavedReports');

        if(isset($department))
        {

          $departmentRepository = $this->getRepository('Application\Entity\T015fdepartment');
          $department_name = $departmentRepository->findBy(array('f015fdepartmentCode'=>$department));
          $department_name = $department_name[0];
          $department_name = $department_name->getF015fdepartmentName();
          $dept_code_name =  $department."-".$department_name;
        }
        // print_r($dept_code_name);exit;



        $payment = $reportRepository->getPaymentOverDetails($department);
        // print_r($payment);exit;
       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
         <table align='center' width:'100%'>
            <tr>
            <td style='text-align: center'><b>UNIVERSITI UTARA MALAYSIA<br> MODUL BAYARAN<br>LAPORAN PRESTASI DAYARAN BIL</b></td>
            </tr>
            <tr>
             <td style='text-align: center;' width='100%'><font size='3'><b>DARI : $pay_date &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HINGGA : $pay_date<br>BILANGAN HARI MELEBIHI 7 HARI DI JABATAN BENDAHARI</font></b></td>
            </tr>
         </table>

        <table align='center' width:'100%'>
            <tr>
             <td style='text-align: left' width='80%'><font size='3'>LAPORAN: </font></td>
             <td style='text-align: left' width='20%'><font size='3'>TARIKH: $pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='80%'><font size='3'>MASA: $pay_time</font></td>
             <td style='text-align: left' width='20%'><font size='3'></font></td>
            </tr>
            </table>
            <br>

          <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='100%'><font size='2'>PTJ :$dept_code_name</font></td>
             </tr>
          </table>
          <br>


        <table align='center' width:'100%'>
           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>Bil</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. INVOIS PEMBEKAL</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. TERIMA INVOICE DI PTJ</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. DAFTAR BIL</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>KOD PENGHUTANG</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'> TH. TERIMA BAUCAR DI JB</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. QUERY</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. BATAL</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>NO. BAUCER</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>TH. PROSES</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>NAMA PENERIMA</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>BIL HARI</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>AMAUN</font></td>
           </tr>

           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='1'></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(A)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(B)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(C)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(D)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(E)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(F)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(G)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'>(H)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='1'></font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='1'>(DI PTJ)<br>(I)</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='1'></font></td>
           </tr>

           <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'><b><hr></b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b><hr></b></font></td>
           </tr>";
           $i = 1;
           foreach ($payment as $payData)
           {

              $idItem = $payData['f072fid_item'];;
              $gstValue = $payData['f072fgst_value'];
              $quantity = $payData['f072fquantity'];
              $price = $payData['f072fprice'];
              $total = $payData['f072ftotal'];
              $gstCode = $payData['f072fgst_code'];
              $taxCode = $payData['f072fid_tax_code'];
              $totalExe = $payData['f072ftotal_exc'];
              $taxAmount = $payData['f072ftax_amount'];
              $debitFundCode = $payData['f072fdebit_fund_code'];
              $debitDeptCode = $payData['f072fdebit_department_code'];
              $debtActivityCode = $payData['f072fdebit_activity_code'];
              $debtAccCode = $payData['f072fdebit_account_code'];
              $creditFundCode = $payData['f072fcredit_fund_code'];
              $creditDeptCode = $payData['f072fcredit_department_code'];
              $creditActivityCode = $payData['f072fcredit_activity_code'];
              $creditAccCode = $payData['f072fcredit_account_code'];
              $isRefund = $payData['f072fis_refundable'];
              $Feename = $payData['f066fname'];
              // print_r($Feename);exit;
             

           $file_data = $file_data . " 

            <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$pay_date</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$pay_date</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$pay_date</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$pay_date</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'></font></td>
           <td style='text-align:right' width='8%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$pay_date</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'></font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'></font></td>
           <td style='text-align:right' width='8%' valign='top'><font size='2'></font></td>
           </tr>";
            $i++;
            }

          }
      $file_data = $file_data . "</table>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}