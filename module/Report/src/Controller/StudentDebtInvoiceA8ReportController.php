<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentDebtInvoiceA8ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function studentDebtInvoiceA8ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
<html>
<head>
  <title></title>
</head>
<body>
<table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='5'>UNIVERSITI UTARA MALAYSIA<br>
          Invois Hutang Pelajar kepada Penaja</b></font></td>
        </tr>
    </table>
    <br><br>

     <table align='center' width='100%'>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>No. Invois:</font></td>
          <td style='text-align: left' width='40%' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='30%' valign='top'><font size='5'>Tarikh:</font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>Matrik:</font></td>
          <td style='text-align: left' width='40%' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='30%' valign='top'><font size='5'>Tempoh Tajaan:</font></td>
          
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>Nama:</font></td>
          <td style='text-align: left' width='40%' colspan='2' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='30%' valign='top'><font size='5'></font></td>
          
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>No. KP/No<br>Passport:</font></td>
          <td style='text-align: left' width='40%' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='30%' valign='top'><font size='5'>Tempoh Pengajian:</font></td>
          
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='5'>Your ref.:</font></td>
          <td style='text-align: left' width='40%' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='30%' valign='top'><font size='5'>Our ref.:</font></td>
       
        </tr>
        <tr>
          <td style='text-align: left' width='15%' valign='top'><font size='5'>Jumalh <br>Tuntutan(RM):</font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='5'></font></td>
          <td style='text-align: center' width='15%' valign='top'><font size='5'>Baki Tajaan<br>(RM):</font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='5'></font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='5'>Jumlah<br>Tajaan(RM):</font></td>
          <td style='text-align: right' width='15%' valign='top'><font size='5'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='15%' valign='top'><font size='5'>Baki terkini (RM):</font></td>
          <td style='text-align: left' width='20%' valign='top'><font size='5'></font></td>
        </tr>
    </table>
    <br><br>

    <table align='center' width='100%' border='1' style='border-collapse: collapse;' cellpadding='4'>
        <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='4'>BIL</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='4'>Kod</font></td>
          <td style='text-align: center' width='40%' colspan='2' valign='center'><font size='4'>Keterangan</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='4'>Amaun</font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='5'>1</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>123</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>WELFARE</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>KEBAJIKAN</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='5'>150.00</font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='5'>2</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>319</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>TUITION</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>PENGAJIAN</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='5'>2,405.00</font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='5'>3</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>359</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>LIBRARY</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>PERPUSTAKAAN</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='5'>100.00</font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='5'>4</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>362</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>SPORT</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>SUKAN</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='5'>50.00</font></td>
        </tr>
         <tr>
          <td style='text-align: center' width='5%' valign='center'><font size='5'>5</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>363</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>COMPUTER LAB</font></td>
          <td style='text-align: center' width='20%' valign='center'><font size='5'>KOMPUTER</font></td>
          <td style='text-align: center' width='10%' valign='center'><font size='5'>150.00</font></td>
        </tr>
      </table>
    </body>
    </html>";

      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
  }
}