<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimTuitionFeesForLibyansSudentA12ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function claimTuitionFeesForLibyansSudentA12ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
          <!DOCTYPE html>
<html>
<head>
  <title>A12</title>
</head>
<body>
<form>
  <fieldset>
    <table width='100%'>
      <tr><br><br><br><br>
        <td style='width: 100%;text-align: center;'><h2>CLAIM TUITION FEES FOR LIBYAN STUDENT SEMESTER 11 SEESION 2015-2016<br>
          REF:UA20160410-KKB7963<br><br></h2>
        </td>
      </tr>
    </table>
    <table style='width: 100%;border: 1px solid black; border-collapse: collapse;'>
      <tr>
        <td style='border: 1px solid black; width: 10%;text-align: center;' >BIL</td>
        <td style='border: 1px solid black; width: 20%;text-align: center;' >MATRIC</td>
        <td style='border: 1px solid black; width: 40%;text-align: center;' >NAME OF STUDENT</td>
        <td style='border: 1px solid black; width: 20%;text-align: center;' >CODE</td>
        <td style='border: 1px solid black; width: 20%;text-align: right;' >AMOUNT(RM)</td>
      </tr>
      <tr>
        <td style='border: 1px solid black; width: 10%;text-align: center;' >BIL</td>
        <td style='border: 1px solid black; width: 20%;text-align: center;' >MATRIC</td>
        <td style='border: 1px solid black; width: 40%;text-align: center;' >NAME OF STUDENT</td>
        <td style='border: 1px solid black; width: 20%;text-align: center;' >CODE</td>
        <td style='border: 1px solid black; width: 20%;text-align: right;' >AMOUNT(RM)</td>
      </tr>
      <tr>
         <td  colspan='5' style='text-align: right; width: 10%'>AMOUNT(RM)</td>
      </tr>
    </table><br><br><br><br>
    <table width='100%'>
      <tr>
        <td style='width: 20%;text-align: left;'>
          APPROVED BY:<br><br><br><br><br><br>
        </td>
        <td style='width: 80%;text-align: center;'>
          Noor Nazilla Wazren<br>
          Assistant Bursar<br>
          U-Assist<br>
          Universiti Utara Malaysia
        </td>
      </tr>
    </table><br><br><br><br>
    <table width='100%'>
    </table>
  </fieldset>
</form>
</body>
</html>";

           $i = 1;
         foreach ($POData as $details)
         {
           $referenceNo = $details['f034freference_number'];
           $orderType = $details['f034forder_type'];
           $companyName = $details['f030fcompany_name'];
           $year = $details['f015fname'];
           $description = $details['f034fdescription'];
           $Amount = $details['f034ftotal_amount'];
           $department = $details['f015fdepartment_name'];
           $date1 = $details['f034forder_date'];
           $reqNo = $details['f034fid_purchasereq'];
           $quantity = $details['f034fquantity'];
           $unit = $details['f034funit'];
            
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d-m-Y", strtotime($date1));
            $dateOrder = date("d-m-Y", strtotime($dateOrder));


           $TotalAmount = $details['f034ftotal_amount'];

           $porder_data = $porder_data . "
           <tr>
           <td style='text-align:left' width='5%' valign='top'><font size='2'>$i</font></td>
           <td style='text-align:left' width='8%' valign='top'><font size='2'>$referenceNo</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$orderType</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$companyName</font></td>
           <td style='text-align:left' width='11%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$department</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$year</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$dateOrder</font></td>
           <td style='text-align:left' width='7%' valign='top'><font size='2'>$reqNo</font></td>
           <td style='text-align:right' width='5%' valign='top'><font size='2'>$quantity</font></td>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>$unit</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>$Amount</font></td>
           </tr>";
        $i++;
        }

      }


// <tr>
//       <td style='text-align:right' colspan='5' width='80%'><font size='2'>Total:</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totaldr</font></td>
//       <td style='text-align:right' width='10%'><font size='2'>$totalcr</font></td>
//       </tr>

      $porder_data = $porder_data . "
      </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}