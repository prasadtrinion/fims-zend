<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportByReceiptNameController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // AP Report1
    public function reportByReceiptNameAction()
    {
        $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $receiptId = $postData['Id'];
        // print_r($receiptId);exit;
        $reportRepository = $this->getRepository('T019fsavedReports');
        $receipt = $reportRepository->getReportByPeceipt($receiptId);
        // print_r($receipt);exit;

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 

        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr><td></td>
            </tr>
        </table>";

        $toDate = $receipt['f082fdate'];
        $fromDate = $receipt['f082fcreated_dt_tm'];

        $fromDate   = date("m/d/Y", strtotime($fromDate));
        $toDate   = date("m/d/Y", strtotime($toDate));


        $CustomerName = $receipt['CustomerName'];
        $receiptNumber = $receipt['f082freceipt_number'];
        $totalAmount = $receipt['f082ftotal_amount'];
        $description = $receipt['f082fdescription'];
        $idCustomer = $receipt['f031fid_customer'];
        $idInvoice = $receipt['f082fid_invoice'];
        $payeeType = $receipt['f082fpayee_type'];

        $totalAmount = number_format($totalAmount, 2, '.', ',');




        


        $file_data = $file_data . " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'><b>UNIVERSITI UTARA MALAYSIA<br> MODUL BAYARAN<br>LAPORAN BAUCAR BAYARAN MENGIKUT NAMA PENERIMA</b></td>
            </tr>
            <tr>
            <td font-size='8' style='text-align: center'><font size='2'>DARI TARIKH $fromDate &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HINGAA $toDate</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='80%'><font size='2'>LAPORAN : MR_60A</font></td>
             <td style='text-align: left' width='20%'><font size='2'>TARIKH : $pay_date</font></td>
            </tr>
            <tr>
             <td style='text-align: left;' width='70%'><font size='2'>MASA : $pay_time</font></td>
             <td style='text-align: left;' width='30%'><font size='2'></font></td>
            </tr>
            </table>
            
            <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left;' colspan='2'><font size='2'>NAMA PENERIMA: $CustomerName($idCustomer) </font></td>
             </tr>
         </table>
         

      <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>";


      $file_data = $file_data . "
            <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'><b>BIL.</b></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'><b> NO. BAUCER</b></font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'><b> NO. ABB</b></font></td>
           <td style='text-align:left' width='16%' valign='top'><font size='2'><b> TKH. PROCESS</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'><b>PERIHAL</b></font></td>
           <td style='text-align:left' width='26%' valign='top'><font size='2'><b>BAYARAN</b></font></td>
           <td style='text-align:right' width='15%' valign='top'><font size='2'><b>AMAUN</b></font></td>
        </tr>
        <tr>
        <td colspan='7'><hr></td>
        </tr>
        <tr>
           <td style='text-align:center' width='5%' valign='top'><font size='2'>1.</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$receiptNumber</font></td>
           <td style='text-align:center' width='8%' valign='top'><font size='2'> </font></td>
           <td style='text-align:left' width='16%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$description</font></td>
           <td style='text-align:left' width='26%' valign='top'><font size='3'>$payeeType</font></td>
           <td style='text-align:right' width='15%' valign='top'><font size='2'>$totalAmount</font></td>
        </tr>
        <tr>
           <td colspan='7'><hr></td>
        </tr>
        <tr>
           <td style='text-align:right' width='85%' colspan='6'><font size='2'>JUMLAH BAYARAN:</font></td>
           <td style='text-align:right' width='15%'><font size='2'>$totalAmount</font></td>
        </tr>
        </table>";
        }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}