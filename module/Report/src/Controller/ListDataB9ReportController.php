<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ListDataB9ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// listDataB1Report B6-10
    public function listDataB9ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();


            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

        $file_data = $file_data . "
    <html>
<head>
    <title>UUM</title>
</head>
<body>
    <form>
        <table align='center' width='100%'>
        <tr>
        <th style='text-align: center;width: 50%'><font size='3'>UNIVERSITI UTRA MALAYSIA</th>
        <td style='text-align: center; width: 5%'> M/S:19/52</td>

        </tr>
        <tr>
        <th>SENARAI PERUBAHAN DATA</th>
        <td style='text-align: center; width: 5%'> MASA:11.25 AM</td>
        </tr>
        <tr>
        <th>UNTUK BULAN :</th>
        <td style='text-align: center; width: 5%'>04/2018</td>
        </tr>
    </table>
    <table border='1' cellpadding='0' cellspacing='0'>
        <tr>
            <td>PENDAPATAN</td>
        </tr>
    </table><br>
    <table border='1' align='center' width='100%' cellpadding='0' cellspacing='0'>
        <tr>
            <td style='text-align: center; width: 5%'><font size='3'>KOD</font></td>
            <td style='text-align: center; width: 8%'><font size='3'>625</font></td>
            <td style='text-align: center; width:60%'><font size='3'>Elaun Buku (T) </font></td>
            <td style='text-align:  center; width:20%'><font size='3'>10305994101</font></td>
        </tr>
    </table><br><br>
    <table border='1' align='center' width='100%' cellpadding='0' cellspacing='0'>
        <tr>
            <td style='text-align: center; width: 10%'>Bill</td>
            <td style=' text-align:center; width: 15%' >NO.Staf</td>
            <td style='text-align: center;width: 45%'>Nama staf</td>
            <td style='text-align: center; width: 10%'>Jumlah</td>
            <td style='text-align: center;width: 10%'>BulanAkhir</td>
            <td style='text-align: center;width: 10%'>TARAF KHAIDMAT</td>
        </tr>
    </table><br>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 10%'>1</td>
            <td style=' text-align:center; width: 15%'>5150</td>
            <td style='text-align: center;width: 45%'>ASHRAF NAJMUDDIN BIN KAMARUDDIN</td>
            <td style='text-align: center; width: 10%'>825.00</td>
            <td style='text-align: center;width: 10%'>04/2018</td>
            <td style='text-align: center;width: 10%'>SEMENTARA</td>
        </tr>
    </table><br>
    <table align='center' width='100%' >
        <tr>
            <td style='text-align: center; width: 10%'></td>
            <td style=' text-align:center; width: 15%' ></td>
            <td style='text-align: center;width: 45%'></td>
            <td style='text-align: center; width: 10%'>825.00</td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
        </tr>
    </table>
    </form>
</body>
</html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}