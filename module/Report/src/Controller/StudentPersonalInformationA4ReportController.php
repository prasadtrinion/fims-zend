<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentPersonalInformationA4ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function studentPersonalInformationA4ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


          $porder_data = "
<table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='7'><b>UNIVERSITI UTARA MALAYSIA<br>
          w_sap111 Maklumat Peribadi Pelajar</b><font></td>
        </tr>
    </table>
    <br><br>

     <table align='center' width='100%'>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>No. Matrik:<font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'>Nama:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>No. KP Lama:<font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'>Program:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>No. KP Baru:<font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'>Kump. Yuran:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>Status Pengajian:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='30%' valign='top'><font size='7'>Kod Bank:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='25%' valign='top'><font size='7'><font>No.Akaun:</td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>Status Yuran:<font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'>Tempat Pengajian:<font></td>
            <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'>Alamat Tetap:<font></td>

          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
            <td style='text-align: center' width='50%' valign='top'><font size='7'>Alamat Kolej:<font></td>
          <td style='text-align: right;' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size=''><font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
          <td style='text-align: left' width='5%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='50%' valign='top'><font size='7'>Warga:<font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='7'><font></td>
        </tr>
    </table>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b>
            <hr>
          </b><font></td>
        </tr>
    </table>

    <table align='center' width='100%' border='1' style='border-collapse: collapse' cellpadding='4'>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='7'>KOD <br>PENAJA.<font></td>
          <td style='text-align: center' width='25%' valign='top'><font size='7'>NAMA PENAJA.<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>TARIKH <br>KEMASKINI<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>JUMLAH<br>TAJAAN<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>TARIKH<br>MULA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>TARIKH<br>TAMAT<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>TEMPOH<br>(bulan)<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>TAJAAN <br>PENUH<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>KEU<br>TAMAT<font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='7'>17<font></td>
          <td style='text-align: left' width='25%' valign='top'><font size='7'>TABUNG AMANAH PENDIDI KANNEGERI MELAKA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>03/04/55<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>01/01/513<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>30/06/55<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>41<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>YA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'><font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='5%' valign='top'><font size='7'>269<font></td>
          <td style='text-align: left' width='25%' valign='top'><font size='7'>KEMANTERIAN BELIA DAN SUKAN<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>03/04/55<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>01/02/530<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>28/02/519<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>48<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'>YA<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='7'><font></td>
        </tr>
    </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}