<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ApplicationForAllocationController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Sponsor List Report A-30
    public function applicationForAllocationA30Action()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$journalId = $postData['journalId'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$journalData = $reportRepository->getJournalEntryById((int)$journalId);

        	// print_r($journalData);exit;
          $JournalDescription = $journalData[0]['f017fdescription'];
          $Approver = $journalData[0]['originalName'];

          
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());


        	$journal_data = "
    <table align='center' width='100%' border='1'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><b>CONTON FORMAT BORANG PERMOHONAN PERUNTUKAN</b></td>
        </tr>
    </table>
    <br>

    <table align='center' width='100%'>
       <tr>
          <td style='text-align: left' width='20%' valign='top'>INSTITUSI</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>NAMA PENERIMA</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>BANK</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>NO. AKAUN</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top' colspan='3'>(Sertakan salinan penyata akaun untuk rujukan pembayaran)***</td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top' colspan='3'>SENERAI PELAJAR TAJAN SLAB & SKPD BAGI SEMESTER BERAKHIR________________</td>
        </tr>
    </table>

     
    <table  align='center' border='1' width='100%'>
       <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1&2 2016</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>

        <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1&2 2016</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>

        <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1&2 2016</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>
        </table>";
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
        }


    //Sponsor List Report A-22
    public function applicationForAllocationA22Action()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $journalId = $postData['journalId'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $journalData = $reportRepository->getJournalEntryById((int)$journalId);

          // print_r($journalData);exit;
          $JournalDescription = $journalData[0]['f017fdescription'];
          $Approver = $journalData[0]['originalName'];

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $journal_data = "
    <table align='center' width='100%' border='1'>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><b>CONTON FORMAT BORANG PERMOHONAN PERUNTUKAN</b></td>
        </tr>
    </table>
    <br>

    <table align='center' width='100%'>
       <tr>
          <td style='text-align: left' width='20%' valign='top'>INSTITUSI</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>NAMA PENERIMA</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>BANK</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='20%' valign='top'>NO. AKAUN</td>
          <td style='text-align: left' width='5%' valign='top'>:</td>
          <td style='text-align: left' width='77%' valign='top'></td>
        </tr>
        <tr>
          <td style='text-align: left' width='100%' valign='top' colspan='3'>(Sertakan salinan penyata akaun untuk rujukan pembayaran)***</td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top' colspan='3'>SENERAI PELAJAR TAJAN SLAB & SKPD BAGI SEMESTER BERAKHIR________________</td>
        </tr>
    </table>

     
    <table  align='center' border='1' width='100%'>
        <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2014/15</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>

        <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2014/15</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>

        <tr>
           <td style='text-align:center' width='8%' valign='top'><font size='2'>BIL</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PELAJAR</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NO. KAD PENGENALAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>TEMPOH TAJAAN</font></td>
           <td style='text-align:center' width='18%' valign='top'><font size='2'>NAMA PROGRAM</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>STATUS</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2014/15</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>1 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>SEMESTER<br>2 2015/16</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JUMLAH KOS<br>(RM)</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>JENIS TUNTUTAN</font></td>
        </tr>
        </table>";
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
    }
}