<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SummaryStatementController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function summaryStatementAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        $year = $postData['financialYear'];
        $departmentCode = $postData['departmentCode'];
        $fundCode = $postData['fundCode'];
       
        
            $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
        $yearn = $reportRepository->getSummaryYear($year);
        $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        

        $file_data = $file_data . "
    <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
       baki peruntukan bagi  period
        <tr>
            <td style='text-align:right'></td>
        <tr>
        <tr>
         <td style='text-align: center'  font-size='13' width='100%' valign='top'><b>PERUNTUKAN MENGURUS $yearn<br>JABATAN BENDAHARI</b><br></td>
        </tr>
    </table>
    <br>";
    
      
        foreach ($result as $statement)
        {

            // //Assigning Each Object of Statement to diifferent objects
        $department_name = $statement['f015fdepartment_name'];
        $department_id = $statement['f015fdepartment_code'];
        $fund_id = $statement['f054ffund'];
        // print_r($department_id);exit;

        $file_data = $file_data . "

    <table  style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
        <tr>
        <td style='text-align:left'><font size='3'>$department_name</font></td>
        </tr>
    </table>
    
     <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
        <tr>
           <td style='text-align:center' width='20%'><font size='3'><b> Kod Akaun</b></font></td>
           <td style='text-align:center' width='60%'><font size='3'><b> Butiran</b></font></td>
           <td style='text-align:center' width='20%' valign='top'><font size='3'><b> Peruntukan<br>(RM)</b></font></td>
        </tr>";
        

        $activity_data         = $reportRepository->getActivityData($department_id,$fundCode);
        
         
        $total = 0;
        foreach ($activity_data as $result1)
        {

            // print_r($result1);exit();
            $amount = $result1['f054famount'];
            $original_amount = $result1['f054famount'];
            $name = $result1['f059fname'];
            $code = $result1['f054faccount_code'];

            $amount = number_format($amount, 2, '.', ',');


            $file_data = $file_data . "<tr>
         <td style='text-align: center' width='20%'><font size='3'> $code</font></td>
         <td style='text-align: left' width='60%'><font size='3'>$name</font></td>
         <td style='text-align: right' width='20%'><font size='3'>$amount</font></td>
        </tr>";

        $total = floatval($total) + floatval($original_amount);


        }

        $total = number_format($total, 2, '.', ',');
        // print_r($total);exit();
        // print_r($total);
       
        $file_data = $file_data . "

        <tr>
         <td style='text-align: right' width='80%' colspan='2'>JUMLAH(RM):</td>
         <td style='text-align: right' width='20%'><font size='3'>$total</font></td>
         </tr>
         </table>
         <br>";
        }
    }

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

    }
}