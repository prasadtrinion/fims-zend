<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class InvestmentInstructionForWithdrawlController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Investment Instruction For Withdrawl
    public function investmentInstructionForWithdrawlAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

         if($this->isJSON($rawBody))
        {
            // print_r($postData);exit;
        $applicationId = (int)$postData['applicationId'];

        $reportRepository = $this->getRepository('T019fsavedReports');
         $uumName = $reportRepository->getuumBank($applicationId);

         $uumBank = $uumName['uumBank'];
         $investmentBank = $uumName['investmentBank'];
         $uumBankAccNo = $uumName['uumBankAccNo'];
         $uumBankAddress = $uumName['uumBankAddress'];
         $uumBankContactPerson = $uumName['uumBankContactPerson'];
         $uumBankPhone = $uumName['uumBankPhone'];
         $uumBankDescription = $uumName['uumBankDescription'];
         $uumBranch = $uumName['uumBranch'];
         $uumBankCity = $uumName['uumBankCity'];
         $invBankBranch = $uumName['invBankBranch'];
         $sum = $uumName['amount'];
         $batchNo = $uumName['f145fbatch_no'];


         // print_r($sum);exit;


            $print_date = date("m/d/Y h:i:s a", time());    
                  
            $pay_month   = date("m", strtotime($print_date));
            $pay_date   = date("d", strtotime($print_date));
            $pay_year   = date("Y", strtotime($print_date));
            

           
            $months = array (1=>'Januari',2=>'Februari',3=>'Mac',4=>'April',5=>'Mei',6=>'Jun',7=>'Julai',8=>'Ogos',9=>'September',10=>'Oktober',11=>'November',12=>'Disember');
            $pay_month = $months[(int)$pay_month];



            $fd_data = '';
            while($sum > 0)
            {
                if($sum > 5000000)
                {
                    
                    $sum = $sum -5000000;
                    $Amount = 5000000;
                    $limit = '5.0';
                    $juta = 'JUTA';
                }
                else
                {
                    
                    $limit = $sum.'.0';
                    $Amount = $sum;
                    $sum = 0;
                    $juta = '';
                    $limit = number_format($limit, 2, '.', ',');

                }

        $fd_data =$fd_data . "
        <table align='center' width='100%'>
         <tr>
         <td style='text-align: center' width='100%'><b>UTARA UNIVERSITI MALAYSIA</b></td>
        </tr>
    </table>
    <br><br><br>

        <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
          <td width='80%'></td>
          <td style='text-align: left' width='20%' valign='top'>$batchNo</td>
        </tr>
        <tr>
        <td width='80%'></td>
          <td style='text-align: left' width='20%' valign='top'>$pay_date $pay_month $pay_year</td>
        </tr>
        <br><br>
        <tr>
         <td style='text-align: left' width='100%' valign='top'>$investmentBank<br>$invBankBranch<br><br>06000 Jitra<br>Kedah<br><br><br>Taun</td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td width='100%' valign='top'><b>PENGELUARAN SIJIL SIMPANAN TETAP UUM BERJUMLAH RM$limit $juta</b></td>
        </tr>
        <br><br>
        <tr>
        	<td width='100%' valign='top'>Dengan hormatnya dimaklumkan bahawa sijil simpanan tetap di bawah tamat tempoh lakunya seperti berikut:</td>
        </tr>
    </table>
    <br>

    <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
	<tr>
   		<th>No. Sijil</th>
   		<th>Tarikh Matang</th>
   		<th>Jumlah (RM)</th>

   	</tr>";
    $fixedCert = $reportRepository->getFDCertificate($applicationId);
    // print_r($fixedCert);exit;
    foreach ($fixedCert as $Fds)
    {





      $certificate = $Fds['f064freference_number'];
      $amount = $Fds['f146famount'];
      $maturity_date = $Fds['f146fmaturity_date'];
      // $Amount = $Fds['f064fsum'];      
      $Amount = number_format($amount, 2, '.', ',');


            $maturitymonth   = date("m", strtotime($maturity_date));
            $maturitydate   = date("d", strtotime($maturity_date));
            $maturityyear   = date("Y", strtotime($maturity_date));
            

           
            $months = array (1=>'Januari',2=>'Februari',3=>'Mac',4=>'April',5=>'Mei',6=>'Jun',7=>'Julai',8=>'Ogos',9=>'September',10=>'Oktober',11=>'November',12=>'Disember');
            $maturitymonth = $months[(int)$maturitymonth];
    


    $fd_data =$fd_data . "
   	<tr>
   		<td style='text-align: left' valign='top'>$certificate</td>
   		<td style='text-align: center' valign='top'>$maturitydate $maturitymonth $maturityyear</td>
   		<td style='text-align: right' valign='top'>$amount</td>
   	</tr>";

    }


    $fd_data =$fd_data . "
   	</table>
   	<br>

   	<table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td width='100%' align='left'>Sila tuan keluarkan sijil simpanan tetap di atas ke dalam akaun semasa UUM bersama-sama dengan faedahnya seperti berikut:</td>
        </tr>
    </table>
    <br>
    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: right'  width='10%' valign='top'></td>
        	<td style='text-align: left'  width='26%' valign='top'><b>Nama Akaun</b></td>
        	<td style='text-align: centre'  width='4%' valign='top'><b>:</b></td>
        	<td style='text-align: left'  width='60%' valign='top'><b>$uumBank</b></td>
        </tr>
         <tr>
         	<td style='text-align: right'  width='10%' valign='top'></td>
        	<td style='text-align: left'  width='26%' valign='top'><b>No Akaun</b></td>
        	<td style='text-align: centre'  width='4%' valign='top'><b>:</b></td>
        	<td style='text-align: left'  width='60%' valign='top'><b>$uumBankAccNo</b></td>
        </tr>
         <tr>
         	<td style='text-align: right'  width='10%' valign='top'></td>
        	<td style='text-align: left'  width='26%' valign='top'><b>Nama Bank</b></td>
        	<td style='text-align: centre'  width='4%' valign='top'><b>:</b></td>
        	<td style='text-align: left'  width='60%' valign='top'><b>$uumBank</b></td>
        </tr>
        <tr>
        	<td style='text-align: right'  width='10%' valign='top'></td>
        	<td style='text-align: left'  width='26%' valign='top'><b>Alamat Bank</b></td>
        	<td style='text-align: centre'  width='4%' valign='top'><b>:</b></td>
        	<td style='text-align: left'  width='60%' valign='top'><b>$uumBankAddress,$uumBranch </b></td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td>Sebarang pertanyaan sila hubungi Pn.Siti Najiah Kamran (<u>sitinaliah@uum.edu.my</u>) di talian 04-9283212 atau En. Mohamad Nazaruddin Bin Mohamad Zaid (<u>nazar@uum.edu.my</u>) di talian 04-9283234</td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left'  font-size='20' width='100%' valign='top'> Sekian, terima kasih</td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='100%' valign='top'><b>'KEDAH AMAN MAKMUR-HARAPAN BERSAMA MAKMURKAN KEDAH'<br>'ILMU BUDI BAKTI'</b></td>
        </tr>
    </table>
    <br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='100%' valign='top'>Saya yang menjalankan amanah,</td>
        </tr>
    </table>
    <br><br>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        <tr>
        	<td style='text-align: left' width='60%' valign='top'><b>(PROF. DR. AHMAD BASHAWİR BIN<br> ABDUL GHANI)</b><br>Naib Canselor<br>Universiti Utara Malaysia</td>
        	<td style='text-align: left' width='40%' valign='top'><b>(HAJI AMRON MAN)</b><br>Bendahari<br>Universiti Utara Malaysia</td>
        </tr>
    </table>
    <pagebreak>
    ";

        }
      }
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($fd_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}