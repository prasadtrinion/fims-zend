<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SponsorListofClaimsA11ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function sponsorListofClaimsA11ReportAction()
    {

        // print_r("ert");exit;
       $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
          // print_r($postData);exit;
          $id = $postData['id'];

          $reportRepository = $this->getRepository('T019fsavedReports');
          $POData = $reportRepository->getPODataById((int)$id);

          // print_r($POData);exit;

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

          $porder_data = "
         <!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <table width='100%'>
    <tr>
      <td style='text-align: center'><font size='5'><b>Senarai Tuntutan Penaja</b></font></td>
    </tr>
    <tr>
      <td style='text-align: center'><font size='5'><b>Penaja: EMBASSY OF THE STATE OF LIBYA(217)</b></font></td>
    </tr>
    <tr>
      <td style='text-align: center'><font size='5'><b>Sem: A152</b></font></td>
    </tr>
  </table>
  <table border='1' cellpadding='8' cellspacing='6px' width='100%' style='border-collapse: collapse;'>
      <tr>
        <td style='text-align: center;'><font size='4'><b>Bil.</td>
        <td style='text-align: center;'><font size='4'><b>Matrik</td>
        <td style='text-align: center;'><font size='4'><b>Nama</td>
        <td style='text-align: center;'><font size='4'><b>Th. Invois</td>
        <td style='text-align: center;'><font size='4'><b>No. Inv</td>
        <td style='text-align: center;'><font size='4'><b>Amaun Tuntutan</td>
      </tr>
      <tr>
        <td style='text-align: center;'>1</td>
        <td style='text-align: center;'>81643</td>
        <td style='text-align: center;'>Nursakirah Ab Rahman Muton</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>2</td>
        <td style='text-align: center;'>815265</td>
        <td style='text-align: center;'>Nazni Bin Noordin</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>3</td>
        <td style='text-align: center;'>815638</td>
        <td style='text-align: center;'>Mohd Ammyrul Ashraf bin Sairan</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>4</td>
        <td style='text-align: center;'>816153</td>
        <td style='text-align: center;'>Nur Azriati binti Mat</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>5</td>
        <td style='text-align: center;'>817591</td>
        <td style='text-align: center;'>Vimala a/p Darvmanathan</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>6</td>
        <td style='text-align: center;'>817772</td>
        <td style='text-align: center;'>Mohamad Shahbani Bin Sekh Bidin</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>7</td>
        <td style='text-align: center;'>817727</td>
        <td style='text-align: center;'>Zulianis binti Alias</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>8</td>
        <td style='text-align: center;'>901151</td>
        <td style='text-align: center;'>Nor Hafizah binti Abdul Razak</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>9</td>
        <td style='text-align: center;'>817758</td>
        <td style='text-align: center;'>Mohd Zhafri bin Mohd Zuk</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>10</td>
        <td style='text-align: center;'>817986</td>
        <td style='text-align: center;'>NURUL HUSNA BT MAHADZIR</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>11</td>
        <td style='text-align: center;'>817586</td>
        <td style='text-align: center;'>ANITA A/P KANESTION</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>12</td>
        <td style='text-align: center;'>99241</td>
        <td style='text-align: center;'>Nik Nor Amalina Binti Nik Mohd Sukrri</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>13</td>
        <td style='text-align: center;'>99237</td>
        <td style='text-align: center;'>Samihah binti Suhail</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>14</td>
        <td style='text-align: center;'>99231</td>
        <td style='text-align: center;'>Malini A/P Thiagra</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>15</td>
        <td style='text-align: center;'>99224</td>
        <td style='text-align: center;'>Masita Binti Mt. Zin</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>488.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>16</td>
        <td style='text-align: center;'>99219</td>
        <td style='text-align: center;'>Nur Hidayah Binti Zaidun</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>17</td>
        <td style='text-align: center;'>99217</td>
        <td style='text-align: center;'>Noor Fatihah Binti Mat Radzi</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>18</td>
        <td style='text-align: center;'>99141</td>
        <td style='text-align: center;'>Nor Rabiatul Adawiyah Bt Nor Azam</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>19</td>
        <td style='text-align: center;'>99071</td>
        <td style='text-align: center;'>Mohd Zulfabli bin Hasan</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>20</td>
        <td style='text-align: center;'>817810</td>
        <td style='text-align: center;'>Mohd Azudin bin Mohd Arif</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>2,158.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>21</td>
        <td style='text-align: center;'>96214</td>
        <td style='text-align: center;'>Norhanim binti Alwi</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>22</td>
        <td style='text-align: center;'>96199</td>
        <td style='text-align: center;'>Adawiah binti Idris</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
      <tr>
        <td style='text-align: center;'>23</td>
        <td style='text-align: center;'>817949</td>
        <td style='text-align: center;'>Mime Azrina binti Jaafa</td>
        <td style='text-align: center;'>Apr 10 2016</td>
        <td style='text-align: center;'>217/A152</td>
        <td style='text-align: center;'>1,598.00</td>
      </tr>
  </table>
</body>
</html>";

      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}