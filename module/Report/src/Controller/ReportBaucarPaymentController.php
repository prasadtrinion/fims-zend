<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ReportBaucarPaymentController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //AP Report3
    public function reportBaucarPaymentAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $Id = $postData['Id'];
        $reportRepository = $this->getRepository('T019fsavedReports');
        $VocharData = $reportRepository->getVocharReport3($Id);
        // print_r($VocharData);exit;

       
       

        $current_date = date('d/m/Y h:i:s a', time());              
        $current_date   = date("d/m/Y", strtotime($current_date));

        $current_time = date('h:i:s a', time()); 

        $fromDate = $VocharData['f094fcreated_dt_tm'];
        $toDate = $VocharData['f094fvoucher_date'];
        $department = $VocharData['f015fdepartment_name'];


        $fromDate   = date("d/m/Y", strtotime($fromDate));
        $toDate   = date("d/m/Y", strtotime($toDate));

        // print_r($current_date);exit;



        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'><b>UNIVERSITI UTARA MALAYSIA<br> SISTEM BAYARAN<br>LAPORAN BAUCAR YANG SUDAH DIBAYAR</b></td>
            </tr>
            <tr>
            <td font-size='8' style='text-align: center'><font size='2'>DARI TARIKH $fromDate &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HINGAA $toDate</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='70%'><font size='2'>LAPORAN : MR_60A</font></td>
             <td style='text-align: left' width='30%'><font size='2'>TARIKH : $current_date</font></td>
            </tr>
            <tr>
             <td style='text-align: left;' width='70%'><font size='2'>MASA : $current_time</font></td>
             <td style='text-align: left;' width='30%'><font size='2'></font></td>
            </tr>
            </table>
            <hr>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='20%'><font size='2'>PUSAT T/JWB</font></td>
             <td style='text-align: left' width='2%'><font size='2'>:</font></td>
             <td style='text-align: left' width='80%'><font size='2'>$department</font></td>
            </tr>
            <tr>
             <td style='text-align: left' width='20%'><font size='2'>TEMPOH</font></td>
             <td style='text-align: left' width='2%'><font size='2'>:</font></td>
             <td style='text-align: left' width='80%'><font size='2'>$fromDate - $toDate</font></td>
            </tr>
         </table>
            <br>

        <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
           <tr>
           <td style='text-align:left' width='10%' valign='top'><font size='2'><b>TARIKH</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'><b>BAYAR KEPADA</b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'><b>BAUCAR</font></b></td>
           <td style='text-align:left' width='19%' valign='top'><font size='2'><b>INVOIS</font></b></td>
           <td style='text-align:left' width='6%' valign='top'><font size='2'><b>NO ABB</font></b></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'><b>NO PO/AGMT</b></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'><b>BAYARAN</b></font></td>
           <td style='text-align:center' width='15%' valign='top'><font size='2'><b>AMAUN (RM)</b></font></td>
           </tr>
           <tr>
           <td colspan='8'><hr></td>
           </tr>
           ";
           

            $voucherDate = $VocharData['f094fvoucher_date'];
            $totalAmount = $VocharData['total_amount'];
            $customer_name = $VocharData['f094fcustomer_name'];
            $payment_type = $VocharData['f011fdefinition_code'];
            $voucher_no = $VocharData['f093fvoucher_no'];
            $payment_mode = $VocharData['f093fpayment_mode'];
            $paymentRefNo = $VocharData['f093fpayment_ref_no'];

             // print($voucherDate);exit;

            $totalAmount = number_format($totalAmount, 2, '.', ',');


            $voucherDate   = date("d/m/Y", strtotime($voucherDate));

            // $sumamount = $report['total_amount'];
            
            

           $file_data = $file_data . "<tr>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$voucherDate</font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$customer_name</font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'> $voucher_no</font></td>
           <td style='text-align:left' width='19%' valign='top'><font size='2'>$paymentRefNo</font></td>
           <td style='text-align:left' width='6%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='10%' valign='top'><font size='2'>$payment_type</font></td>
           <td style='text-align:right' width='15%' valign='top'><font size='2'>$totalAmount</font></td>
            </tr>";

            // $total = $total + $sumamount;
          // }

          // $total = number_format($total, 2, '.', ',');

           $file_data = $file_data . "
          
           <tr>
           <td colspan='8'><hr></td>
           </tr>
        	<tr>
           <td style='text-align:right' width='85%' valign='top' colspan='7'><font size='2'>JUMLAH:</font></td>
           <td style='text-align:right' width='15%' valign='top'><font size='2'>$totalAmount</font></td>
        	</tr>
        </table>
        ";
        }
        

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}