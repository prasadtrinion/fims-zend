<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class FurtherSponseredSystemController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report3
    public function furtherSponseredSystemAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td style='text-align: center' valign='top'><font size='3'><b>JABATAN BENDAHARI<br>UNIVERSITI UTARA MALAYSIA</b><br>06011 UUM, Sintok, Kedah Darul Amaun, malaysia. Tel:04-9287777/04-9283135<br>Fax:04-9285764 (Bendahari)/04-9287905(U-Assist) E-mail: scholarship@uum.edu.my</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: center' width='50%' valign='top'><font size='3'><b>TUNTUAN BAYARAN(Invoice)</b></font></td>
             </tr>
        </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='60%' valign='top'><font size='3'>NAMA(Name): </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>INVOICE NO.(Invoice No) </font></td>
             <td style='text-align: center' width='4%' valign='top'><font size='3'>:</font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>$pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='60%' valign='top'><font size='3'>NO MATRIKH(Matric No.): 9437 </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>TARIKH(Date)</font></td>
             <td style='text-align: center' width='4%' valign='top'><font size='3'>:</font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>$pay_date </font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='60%' valign='top'><font size='3'>NO K.P./NO PASSPORT(IC. No/PassportNo.):8865 </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>RUJUKAN KAMI<br>(Our ref.)</font></td>
             <td style='text-align: center' width='4%' valign='top'><font size='3'>: </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'></font></td>
             </tr>
             <tr>
             <td style='text-align: left' width='60%' valign='top'><font size='3'>TEMPOH PENGAJIAN(Period Of Study):$pay_date - $pay_date</font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>RUJUKAN LAIN<br>(Other Ref.)</font></td>
             <td style='text-align: center' width='4%' valign='top'><font size='3'>: </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>UA20 </font></td>
             </tr>
            <tr>
            <td style='text-align: left' width='60%' valign='top'><font size='3'>TEMPOH TANJAAN(Period Of Sponsorship):$pay_date -$pay_date </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'>BAKI TAJAAN(RM)<br>(Balance)</font></td>
             <td style='text-align: center' width='4%' valign='top'><font size='3'>: </font></td>
             <td style='text-align: left' width='18%' valign='top'><font size='3'> </font></td>
             </tr>
        </table>
        <br>

         <table align='center' style='border:1px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: center' width='5%' valign='top'><font size='3'></font></td>
             <td style='text-align: left' width='75%' valign='top'><font size='3'><b>KETERANGAN(Description)</b></font></td>
             <td style='text-align: center' width='20%' valign='top'><font size='3'><b>Baki Tajaan(RM):</b></font></td>
             </tr>
             <tr>
             <td style='text-align: center' width='5%' valign='top'><font size='3'>1.</font></td>
             <td style='text-align: left' width='75%' valign='top'><font size='3'>Kebajikan(Welfare)</font></td>
             <td style='text-align: right' width='20%' valign='top'><font size='3'>150.00</font></td>
             </tr>
             <tr>
             <td style='text-align: right' width='80%' colspan='2' valign='top'><font size='3'>Jumlah(Total):</font></td>
             <td style='text-align: right' width='20%' valign='top'><font size='3'>150.00</font></td>
             </tr>
             <br><br>
             <tr>
             <td style='text-align: left' width='100%' colspan='3' valign='top'><font size='3'>Ringgit Malaysia:</font></td>
             </tr>
              <tr>
             <td style='text-align: left' width='100%' colspan='3' valign='top'><font size='3'>$AmountInWords</font></td>
             </tr>
             <br>
         </table>
         <br>

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:left' width='100%' valign='top'><font size='2'>Bayaran menggunakan cak hendaklah dipatang dan dibayar  kepada Universiti Utara Malaysia(Payable By checque must be crossed and made payable to Universiti Utara malaysia.)<p>Sila Maklumkan ke Jabatan Bendahari (Seperti alamat di alas) Sekiranya Bayaran dibual secara [indahan elektronik (EFT).(p___se send Payment Advice to the above address if payment is made by electronic fund transfer-EFT).<p>Nama Bank/ No. akaun adalha seperti berikut Bank Islamic Malaysia Berhad Cawangan UUM, Account No.0208208890. swift Code-BIMBYKL.</font></td>
           </tr>
           </table>
           <br><br><br>

        <table  align='center' border='0' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:left' width='30%' valign='top'><font size='2'></font></td>
           <td style='text-align:left' width='70%' valign='top'><font size='2'>Invois ini adalah cetakan komputer dan tidak memerlukan tandatangan<br>This Invoice is computer generated and no Signature is Required.</font></td>
           </tr>
           </table>

           ";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}