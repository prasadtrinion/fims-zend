<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ImplementationscheduleA29ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// implementationscheduleA29Report A-29
    public function implementationscheduleA29ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();


            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        

        $file_data = "
    <html>
<head>
    <title></title>
</head>
<body>
    <table align='center' width='100%'>
    <tr>
        <td style='width: 100%;text-align: center;'>
            <b>JADUAL PELAKSANAAN PENYALUPURAN PERUNTUKAN YURAN PENGAJIAN DALAM NEGERI DAN TIKET PENERBANGAN<br>
            PROGRAM SLAB & SKPD TAJAAN BAHAGIAN BIASISWA KPT TAHUN 2016-2017</b>
        </td>
    </tr>
</table><br>
    <table border='1' style='border-collapse: collapse' width='100%'>
    <tr>
            <td style='text-align: center' rowspan='2'>Perkara</td>
            <td style='text-align: center' rowspan='2'>Catatan</td>
            <td style='text-align: center' colspan='12'>Tahun 2016</td>
            <td style='text-align: center' colspan='2'>Tahun 2017</td>
        </tr>
        <tr>
            <td style='text-align: center'>Jan</td>
            <td style='text-align: center'>Feb</td>
            <td style='text-align: center'>Mac</td>
            <td style='text-align: center'>Apr</td>
            <td style='text-align: center'>Mei</td>
            <td style='text-align: center'>Jun</td>
            <td style='text-align: center'>Julai</td>
            <td style='text-align: center'>Ogos</td>
            <td style='text-align: center'>Sept</td>
            <td style='text-align: center'>Okt</td>
            <td style='text-align: center'>Nov</td>
            <td style='text-align: center'>Dis</td>

            <td style='text-align: center'>Jan</td>
            <td style='text-align: center'>Feb</td>
        </tr>
        <tr>
            <td rowspan='3'>1. Universiti sediakan unijuran yuran penerbanjian berdasarkan Data Penwaran Bhagian Biaiswa KPT<br>-Batch 1 sesi 2016/2017<br>-Batch 2 sesi 2016/2017<br>(bergantung bilangan penawaran oleh KPT) </td>
            <td><br></td>
            <td colspan='12'></td><td colspan='2'></td>
        </tr>
        <tr>
            <td rowspan='2' style='text-align: center'>(Lengkapan dan sahkhan <br>Borang SPBB1)</td>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td rowspan='2'>2. Bahagian Biasiswa Salur peruntukan</td>
            <td rowspan='2' style='text-align: center'>sekali setahun <br>(bagi  setiap sesi bergauntung kepada<br>penawarad oleh KPTs)</td>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td rowspan='2'>3. Universiti buta pembayaran dan kemsakhi lejeri peljar<br>dalam siatem esp</td>
            <td rowspan='2' style='text-align: center'>sepanjang tempoh penajaan <br> (Borang SpBB2 & <br>SpBB3)</td>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td rowspan='2'>4. Kemukakan Laporan Prestai Perbelanjaan kapada<br>Bahagian Biasiswa,KPT</td>
            <td rowspan='2' style='text-align: center'>setiap akhir Semester mengikut<br> Universiti<br>(Borang SPBB2&SPBB3)</td>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td rowspan='2'>5. Pemantuan secara bekala oleh Bahagian Biasiswa,KPT<br>ke Unversiti</td>
            <td rowspan='2' style='text-align: center'>pemilihan Universiti secara rawak <br>  <br></td>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr>
            <td><br></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            <td></td><td></td><td></td><td></td><td></td>
        </tr>
        
    </table>
    <table><br><br>
        <tr>
            <td>Catan:</td>
        </tr>
        <tr>
            <td>i)penyalura per dibuta kepada pelajari baru bagi sesi penwaran tahun semasa 2016/2017 berdasarkan data penawaran eleh Bahagian Biasswa KPT.</td>
        </tr>
        <tr>
            <td>ii)Peruntukhan diberi bagi bayaran yuran mengikut semester pengajian dalam tahun semasa sahaja.</td>
        </tr>
        <tr>
            <td>iii)Tuntutan elaun bantuan tesis dan jurnal hendaklah dituntut oleh pelajar kepada Universiti tertakluk kepada syarat dan silling yuran pengajian yang telah ditetapkhan dalam jadual Kadar.</td>
        </tr>
        <tr>
            <td>iv)setiap universiti perlu mengemashini lejer pelajar yang dibayar melalui http://esp.mohe.gov.my</td>
        </tr>
        <tr>        
            <td>v) Elaun sara Diri bagi Progaram MyphD akan dibayar oleh Bahagian Biasiswa,KPT.</td>
        </tr>
    </table>
</body>
</html>";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}