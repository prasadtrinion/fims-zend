<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class InvoiceStudentsF2ReportController extends AbstractAppController
{
    
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

     
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // InvoiceStudentsF2Report F-2

    public function invoiceStudentsF2ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
       
          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        


        $file_data =  $file_data . "
<html>
<head>
  <title>F2</title>
</head>
<body>

<table align='center' width='100%'>

   <tr> 

     <td style='text-align: left;'><font size='3'>LAPORAN INVOIS SETIAP PELAJAR (INDIVIDU)</font></td>
  </tr>
   <tr> 
     <td style='text-align: left; color: blue;'><font size='3'>LAPORAN SEDIA ADA</font></td>
  </tr>
</table>

<table  align='center' width='100%' border='1' cellpadding='0' cellspacing='0'>
<tr>
  <th style='text-align: center; width: 5% '><font size='3'><b>Bil</b></font></th>
    <th  style='text-align: center; width: 20%' ><font size='3'><b>NO MATRIK</b></font></th>
    <th  style='text-align: center; width: 10%'><font size='3'><b>NO INVOIS</b></font></th>
    <th  style='text-align: center; width: 20%' ><font size='3'><b>RUJUKAN</b></font></th>
    <th  style='text-align: center; width: 12%' ><font size='3'><b>JUMLAH INVOIS </b></font></th>
    <th  style='text-align: center; width: 10%' ><font size='3'><b>SEMESTER</b></font></th>
    <th  style='text-align: center; width: 10%' ><font size='3'><b>KOD SEMESTER</b></font></th>
    <th  style='text-align: center; width: 21%' ><font size='3'><b>NAMA</b></font></th>
  </tr>

  <tr>
   <td style='text-align: center; '><font size='3'>1</font></td>
     <td  style='text-align: center; ' ><font size='3'>242911</font></td>
     <td  style='text-align: center; '><font size='3'>153/A171</font></td>
     <td  style='text-align: center; ' ><font size='3'>UUM/HEP/U-ASSIST/K-27/4/1</font></td>
     <td  style='text-align: center; ' ><font size='3'>753.00</font></td>
     <td  style='text-align: center; ' ><font size='3'>2017/2018 </font></td>
     <td  style='text-align: center; ' ><font size='3'>A171</font></td>
     <td  style='text-align: center; ' ><font size='3'>SITI NUR AISYAH BT AHMAD ALI</font></td>
</tr>
</table>
</body>
</html>";
   
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}