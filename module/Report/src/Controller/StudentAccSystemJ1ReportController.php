<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentAccSystemJ1ReportController extends AbstractAppController
{
    
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

     
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    // StudentAccSystemJ1Report J-1

    public function studentAccSystemJ1ReportAction()
    {

      
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        
        if($this->isJSON($rawBody))
        {


        
            // $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
            // print_r($result);
            // die();
        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = $file_data ."
<html>
<head>
  <title>J1</title>
  <style>
     th,#i1,#i2 {

      border: 1px solid black;
    }
  </style>
</head>
<body>

<table align='center' width='100%'>
   <tr> 
     <td style='text-align:center;'><font size='3'>Universiti Utara Malaysia</font></td>
     <td style='text-align:right;'><font size='3'>MS : 1/1</font></td>
   </tr>
   <tr> 
     <td style='text-align:center; '><font size='3'>SISTEM AKAUN PELAJAR</font></td>
   </tr>
   <tr> 
     <td style='text-align:center;'><font size='3'>SENARAI PELAJAR BERHUTANG ADA PENAJA</font></td>
   </tr>
   <tr> 
     <td style='text-align:center;'><font size='3'>PELAJAR</font></td>
   </tr>
</table><br><br>

<table  align='center' width='100%' >
<tr>
  <th style='text-align: center; width: 5% '><font size='3'><b>Bill</b></font></th>
    <th  style='text-align: center; width: 10%' ><font size='3'><b>Matrik</b></font></th>
    <th  style='text-align: center; width: 30%'><font size='3'><b>Nama</b></font></th>
    <th  style='text-align: center; width: 5% '><font size='3'><b>Sesi Masuk</b></font></th>
    <th  style='text-align: center; width: 10%' ><font size='3'><b>Status</b></font></th>
    <th  style='text-align: center; width: 15%' ><font size='3'><b>Warga</b></font></th>
    <th  style='text-align: center; width: 15%' ><font size='3'><b>Penaja</b></font></th>
    <th  style='text-align: center; width: 10%' ><font size='3'><b>Baki</b></font></th>

  </tr>

  <tr>
    <td style='text-align: center; '><font size='2'>1</font></td>
      <td  style='text-align: center; ' ><font size='2'>900139</font></td>
      <td  style='text-align: center; '><font size='2'>S A M Manzur Hossain Khan</font></td>
      <td  style='text-align: center; ' ><font size='2'> A141<br></font></td>
      <td  style='text-align: center; ' ><font size='2'>Aktif</font></td>
      <td  style='text-align: center; ' ><font size='2'>Bangladesh</font></td>
      <td  style='text-align: center; ' ><font size='2'>BSPG-UUM</font></td>
      <td  style='text-align: center; ' ><font size='2'>3085.00</font></td>
</tr>
</table><br>

<table table align='center' width='100%'>
    <tr>
     <td   style='text-align: right; width: 55% '></td>
     <td  id='i1' style='text-align:center; right; width: 30% '><font size='2'>JUMLAH KESELURUHAN  </font></td>
  <td  id='i2'  style='text-align:right; width: 15% '><font size='2'>3085.00 </font></td>
    </tr>
</table>

</body>
</html>";
   
    

        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
    }
}