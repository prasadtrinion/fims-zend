<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StudentSponsoringSketchController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //SF Report1
    public function studentSponsoringSketchAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        // if($this->isJSON($rawBody))
        // {

        // $financialperiod = $postData['financialperiod'];
        // $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 


        $file_data = " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'>UNIVERSITI UTARA MALAYSIA<br>System Akaun Pelajar<br>SEHARAI PELAJAR TAJAN SISWAJAH<br> Pada Tarikh: $pay_date</td>
            </tr>
            <tr>
             <td style='text-align: right;' width='100%'><font size='2'>$pay_date</font></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left' width='20%'><font size='2'>PENAJA: </font></td>
             <td style='text-align: left' width='80%'><font size='2'>HGDAGHJDAJ</font></td>
             </tr>
             <tr> 
             <td style='text-align: left' width='20%'><font size='2'>SEM:</font></td>
             <td style='text-align: left' width='80%'><font size='2'>A151</font></td>
            </tr>
            </table>


        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
           <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Bil</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Matrik</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>NO. KP</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'>Nama</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>KOD Prog</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'> TH. Mula</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>TH. Akhir</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Tajan (RM)</font></td>
           </tr>
            <tr>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>Bil</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>TH. INVOIS PEMDEKAL</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'> DI PTJ</font></td>
           <td style='text-align:center' width='30%' valign='top'><font size='2'></font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>HUTANG</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'> BAUCAR DI JB</font></td>
           <td style='text-align:center' width='10%' valign='top'><font size='2'>TERY</font></td>
           <td style='text-align:right' width='10%' valign='top'><font size='2'>AL</font></td>
           </tr>
           </table>";
        // }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}