<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class PurchaseOrderReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    public function purchaseOrderReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
         //  $id = $postData['id'];
        	// $print = (int)$postData['printStatus'];
        	// $toDate = $postData['toDate'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	$POData = $reportRepository->getPOData($postData);

        	// print_r($POData[0]);exit;

          $pay_date = date('d/m/Y', time());              
        // $pay_date   = date("d/m/Y", strtotime($pay_date));


           $pid = $POData[0]['f034fid'];
           $referenceNo = $POData[0]['f034freferenceNumber'];
           $orderType = $POData[0]['f034forderType'];
           $companyName = $POData[0]['f030fcompanyName'];
           $year = $POData[0]['financialyear'];
           $description = $POData[0]['f034fdescription'];
           $Amount = $POData[0]['f034ftotalAmount'];
           $department = $POData[0]['department'];
           $department_code = $POData[0]['f034fidDepartment'];
           $date1 = $POData[0]['f034forderDate'];
           $reqNo = $POData[0]['f034fidPurchasereq'];
           $vendorAddress = $POData[0]['vendorAddress'];
           $stateName = $POData[0]['f012fstateName'];
           $countryName = $POData[0]['f013fcountryName'];
           $postCode = $POData[0]['f030fpostCode'];
           $printCount = $POData[0]['f034fprintCount'];
           $printDate = $POData[0]['f034fprintDate'];
           $rePrintDate = $POData[0]['f034frePrintDate'];
           // $description = $POData[0]['f034fdescription'];  


           // print_r($rePrintDate);exit;
           $Amount = number_format($Amount, 2, '.', ',');

           $dateOrder   = date("d/m/Y", strtotime($date1));

           if ($printDate)
           {
              $reprintedDoc = " Re-Printed Document ";
           }

          $porder_data = "<table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='20%' valign='top'><img src='' height='50' width='50' ></td>
          <td style='text-align: center' width='80%' valign='top'><font size='3'><b>UNIVERSITI UTARA MALAYSIA</b><br>06010 UUM Sintok, Kedah Darul Aman<br>Tel: 04-9284000 Fax: 04-9285764<br><b>BORANG PESANAN / PERKHIDMATAN</b></font></td>
        </tr>
   
    </table>


     <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='70%' valign='top'><font size='3'></font></td>
          <td style='text-align: left' width='30%' valign='top'><font size='3'>$reprintedDoc</font></td>
        </tr>
    </table>

    <table  align='center' border='1' style='border-collapse:collapse;width:100%;' height='85%'>
          <tr>
           <td style='text-align:left' width='60%' valign='top' rowspan='4'><font size='2'>Kod Pembekal<br>$vendorAddress<br>$stateName , $countryName - $postCode</font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'><b>No PESANAN</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$orderType</font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='20%' valign='top'><font size='4'><b>Tarikh Pesanan</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$dateOrder</font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='20%' valign='top'><font size='2'><b>No Rujukan</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$referenceNo</font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='20%' valign='top'><font size='2'><b>Tarikh Huntaran</b></font></td>
           <td style='text-align:left' width='20%' valign='top'><font size='2'>$pay_date</font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='80%' valign='top' colspan='2'><font size='2'>Butir Barang / Perkhidmatan<br>$description</font></td>
           <td style='text-align:center' width='20%' valign='top'><font size='2'>Harga (RM)</font></td>
          </tr>";

          $purchaseOrderData = $reportRepository->getPurchaseOrderDetails($pid);

          // print_r($purchaseOrderData);exit;
          $pototal = 0;
          $i = 0;
          foreach ($purchaseOrderData as $purchaseOrder)
          {
              $i++;
           $quantity = $purchaseOrder['f034fquantity'];
           $unit = $purchaseOrder['f034funit'];
           $price = $purchaseOrder['f034fprice'];
           $total = $purchaseOrder['f034total'];
           $taxCode = $purchaseOrder['f034ftax_code'];
           $percentage = $purchaseOrder['f034fpercentage'];
           $taxAmount = $purchaseOrder['f034ftax_amount'];
           $totalIncTax = $purchaseOrder['f034ftotal_inc_tax'];
           $itemName = $purchaseOrder['f029fitem_name'];
           $accountCode = $purchaseOrder['f029faccount_code'];
           $Ptotal = $purchaseOrder['f034total'];


           $price = number_format($price, 2, '.', ',');
           $total = number_format($total, 2, '.', ',');
           $taxAmount = number_format($taxAmount, 2, '.', ',');
           $totalIncTax = number_format($totalIncTax, 2, '.', ',');



            $porder_data = $porder_data . "
          <tr>
           <td style='text-align:left' width='80%' valign='top' colspan='2'><font size='2'>-$itemName &nbsp;&nbsp;&nbsp; $accountCode &nbsp;&nbsp;&nbsp; $taxCode<br>-QTY: $quantity x $price / $unit</font></td>
           <td style='text-align:right' width='20%' valign='top'><font size='2'>$total</font></td>
          </tr>";

          $pototal = $pototal + $Ptotal;
          }

          if($i<=10)
          {
            for($k=$i;$k<10;$k++)
            {
                $porder_data.="
                <tr>
                  <td colspan='2' style='height:60px;'>&nbsp;<br></td>
                  <td style='text-align:right' width='20%' valign='top' class='ex1'><font size='2'><br></font></td>
                </tr>";

            }
          }
          else if($i>10)
          {
            $porder_data = $porder_data ." <pagebreak> ";
          }

           $pototal = number_format($pototal, 2, '.', ',');


          $porder_data = $porder_data . "
          <tr>
           <td style='text-align:right' width='80%' valign='top' colspan='2'><font size='2'>Jumlah RM</font></td>
           <td style='text-align:right' width='20%' valign='top'><font size='2'>$pototal</font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='50%' valign='top' rowspan='2'><font size='2'>Pegawai Untuk Dihubungi</font></td>
           <td style='text-align:left' width='25%' valign='top'><font size='2'><b>No PESANAN</b></font></td>
           <td style='text-align:left' width='25%' valign='top'><font size='2'></font></td>
          </tr>
          <tr>
           <td style='text-align:right' width='25%' valign='top'><font size='2'>Jumlah</font></td>
           <td style='text-align:right' width='25%' valign='top'><font size='2'></font></td>
          </tr>
          <tr>
           <td style='text-align:left' width='80%' valign='top' colspan='2'><font size='2'>Halmat Hantar Barang / Perkhidmatan</font></td>
           <td style='text-align:center' width='20%' valign='top'><font size='2'>Disemak</font></td>
          </tr>
          <tr>
          <td style='text-align:center' width='70%' valign='top' colspan='2'><font size='3'><u>PERAKUAN KETUA PUSAT TANGGUUNGJAWAB</u></font><br><font size='1'>Dengan ini adalah disahkan bahaw barangan/ perkhidmatan yang terkandung di dalam pesanan ini telah diterima sepenuhanya dan mengikut spesifikasi yang ditetapkan. Barangan tersebut telah direkodkan ke dalam buku stok/Dafter No.Folio<br><br><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______________________<br><br>Tarikh:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tandatangan & Co.</td>
           <td style='text-align:center' width='30%' valign='top'><font size='1'>Diluluskan<br><br><br><br><br><br>Bendahari<br>Universiti Utara Malaysia</font></td>
          </tr>
        </table>";
      }
      $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($porder_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
        }
}