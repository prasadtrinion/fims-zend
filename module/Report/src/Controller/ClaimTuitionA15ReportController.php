<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ClaimTuitionA15ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// claimTuitionA15Report A-15
    public function claimTuitionA15ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();

            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        

        $file_data = "
    <html>
<head>
    <title>A3</title>
</head>
<body>
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='100%'><font size='3'><b>CLAIM TUITION FOR LIBYAN STUDENT SEMESTER 11 SESSION 2015/2016 <br>ner:UA20160410kk67963 </b></font></td>
        </tr>
    </table>
    <table border='1' align='center' width='100%' style='border-collapse: collapse;'>
            <tr>
            <td style='text-align: center; 'width='5%'><font size='3'>BILL</font></td>

            <td style='text-align: center; 'width=' 10%'><font size='3'>MATRIC</font></td>
            <td style='text-align: center; 'width='30%'><font size='3'> NAME  OF STUDENT</font></td>
            <td style='text-align:  center; 'width='15%'><font size='3'>CODE</font></td>
            <td style='text-align:  center; 'width='20%'><font size='3'>INSTIUTE</font></td>
            <td style='text-align:  right; 'width='20%'><font size='3'>AMOUNT(RM)</font></td>
        </tr>
        <tr>
            <td style='text-align: center; 'width='5%'><font size='3'>1</font></td>

            <td style='text-align: center; 'width=' 10%'><font size='3'>92586</font></td>
            <td style='text-align: center; 'width='30%'><font size='3'>Mustaf A. Abudaenabe Hasan</font></td>
            <td style='text-align:  center; 'width='15%'><font size='3'>217/A152</font></td>
            <td style='text-align:  center; 'width='20%'><font size='3'>UUM</font></td>
            <td style='text-align:  right; 'width='20%'><font size='3'>2.855.00</font></td>
        </tr>
        <tr>
            <td colspan='5'></td>
            <!-- <td style='text-align:  right; 'width='20%'><font size='3'>2.855.00</font></td> -->
            <td style='text-align:  right; 'width='20%'><font size='3'>2.855.00</font></td>
        </tr>
    </table><br><br>
        <table align='left'>
        <tr>
            <td style='text-align: left;'>APPROVED  BY :</td>
        </tr>
    </table>
    <br><br><br>
    <table style='text-align:  center;' width='100%'>
        <tr>
            <td style='text-align:  left;' width='20%'>Noor Dazali Nazaren</td>
        </tr>
        <tr>
            <td style='text-align:  left;' width='3%'></td>
        </tr>
        <tr>
            <td style='text-align:  left;' width='20%'>Assisant Buranar</td>
        </tr>
        <tr>
            <td style='text-align:  left;' width='5%'></td>
        </tr>
        <tr>
            <td style='text-align:  left;' width='20%'>U-Assisant</td>
        </tr>
        <tr>
            <td style='text-align:  left;' width='20%'>University Utara Malayasia</td>
        </tr>
    </table>
     </body>
    </html>";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}