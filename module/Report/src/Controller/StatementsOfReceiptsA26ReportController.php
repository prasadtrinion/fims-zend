<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class StatementsOfReceiptsA26ReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// statementsOfReceiptsA26Report A-26
    public function statementsOfReceiptsA26ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();

          $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());
        
        $file_data = "
    <html>
<head>
<title></title>
</head>
<body>
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: left;' width='5%'><font size='3'>INSTITUSI</font></td>
          <td style='text-align: left;' width='5%'><font size='3'>:</font></td>
          <td style='text-align: left;' width='70%'><font size='3'>UNIVERSITI UTARA MALAYSIA</font></td>
          <td style='text-align: left;' width='5%'><font size='3'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='5%'><font size='3'>BULAN</font></td>
          <td style='text-align: left;' width='5%'><font size='3'>:</font></td>
          <td style='text-align: left;' width='5%'><font size='3'></font></td>
        </tr>

    </table>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: left;' width='5%' ><font size='3'></font></td>
            <td style='text-align: left;' width='5%'  ><font size='3'></font></td>
            <td style='text-align: center;'width='50%'> LAPORAN PENYATA TERIMAAN DAN BAYARAN (PTB) YURAN & TIKET PENERBANGAN PROGRAM SLAB & SKPD PADA 31 DISEMBER 2015</td>
        </tr>
    </table>
     </table>
    <table border='1' align='center' width='100%' style='border-collapse: collapse;'>
      <tr>
        <td colspan='4' style='text-align: center;'>TERIMAAN</td>
      <td colspan='4' style='text-align: center;'>BAYARAN</td>
      </tr>
      <tr>
        <td style='text-align: center; width:5%'>TARIKHA</td> 
        <td style='text-align: center; width:20%'>PERKARA</td>
        <td style='text-align: center; width:10%'>RUJAN</td>
        <td style='text-align: center; width:10%'>RM</td>
        <td style='text-align: center; width:10%'>TARIKHA</td> 
        <td style='text-align: center; width:20%'>PERKARA</td>
        <td style='text-align: center; width:10%'>RUJAN</td>
        <td style='text-align: center; width:15%'>RM</td>
      </tr>
      <tr>
        <td style='text-align: center;' >20.04.2016</td> 
        <td style='text-align: center;' >PERUNTUKAN DARBI BAHANGIAN BIASISWAT KPT</td>
        <td style='text-align: center;' >EFT</td>
        <td style='text-align: center;' >24,000.00</td>
        <td style='text-align: center;' >APRIL 2016</td> 
        <td style='text-align: center;' >YARAN SEM 1/2016</td>
        <td style='text-align: center;' >LAMPIRAN A</td>
        <td style='text-align: center;' >6,000.00</td>
      </tr>
      <tr>
        <td style='text-align: center;' >JUMLAH TERIMAAN</td> 
        <td style='text-align: center;'></td>
        <td style='text-align: center;' ></td>
        <td style='text-align: center;' ></td>
        <td colspan='3' style='text-align: center;'>JUMLAH BAYARANI(ii)</td> 
        <td style='text-align: center;' ></td>
      </tr>
      
      <tr>
        <td style='text-align: center;' ></td> 
        <td style='text-align: center;' ></td>
        <td style='text-align: center;' ></td>
        <td style='text-align: center;' ></td>
        <td colspan='3' style='text-align: center;'>BAKI PERUNTUKAN PADA 31.12.2015[(iii)=(i)-(ii)]</td> 
        <td style='text-align: right;'></td>
      </tr>
      <tr>
        <td style='text-align: center;' ></td> 
        <td style='text-align: center;'>JUMLAH</td>
        <td style='text-align: center;' ></td>
        <td style='text-align: center;' >24,00.00</td>
        <td style='text-align: center;'></td> 
        <td style='text-align: center;' >JUMLHA</td>
        <td style='text-align: center;' ></td>
        <td style='text-align: center;'>6,000.00</td>
      </tr>
      </table>
      <table>
        <tr>
          <td>*LAMPIRAN adalah makulmat lengkap bayaran sepreti Borang SPBB3</td>
        </tr><br><br>
        <tr>
          <td>i) Disahkan makulumat diatas adalah benar dan bayaran telah dibuat sweajararanya.</td>
        </tr>
        <tr>
          <td>ii) Disahkan Telah menarimas  peruntukan RM 6,000.00 dan wang tersebut telah direkodkan sebagi terimaan akaun Amanah</td>
        </tr>
        <tr>

          <td>iii) Disahkan sehingga 31/12/2015 peruntakan telah berbaki RM ....1,500.00</td>
        </tr>
      </table><br><br>
        </table>
    
         <table align='center' width='100%'>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Disediakan:</font></td>
          <td style='text-align: left;' width='15%'  ><font size='3'></font></td>

          <td style='text-align: center;' width='40%'  ><font size='3'>Disemak oleh:</font></td>
          <td style='text-align: center;' width='15%'  ><font size='3'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Cop & tandatangan</font></td>
          <td style='text-align: left;' width='15%'  ><font size='3'></font></td>

          <td style='text-align: center;' width='40%'  ><font size='3'>Cop & tandatangan</font></td>
          <td style='text-align: center;' width='15%'  ><font size='3'></font></td>
        </tr>
        <tr>
          <td style='text-align: left;' width='10%' ><font size='3'>Tarikh:</font></td>
          <td style='text-align: left;' width='15%'  ><font size='3'></font></td>

          <td style='text-align: center;' width='40%' ><font size='3'>Tarikh:</font></td>
          <td style='text-align: center;' width='15%' ><font size='3'></font></td>
        </tr>
      </table>
    </body>
    </html>
     ";
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}