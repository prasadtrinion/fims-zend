<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ComputerPurchaseFinancingSchemeController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }


    public function computerPurchaseFinancingSchemeAction()
    {
        // $request = $this->getRequest();

        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $staffId = $postData['staffId'];
        $reportRepository = $this->getRepository('T019fsavedReports');
        $staff = $reportRepository->computerPurchasedetails($staffId);


                $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 
         $file_data = $file_data . ""; 

        foreach ($staff as $vehicle)
        {
        	// print_r($vehicle);exit;
        	$ic_no = $vehicle['f043fic_no'];
        	$loan_name = $vehicle['f020floan_name'];
       		$loan_amount = $vehicle['f043ftotal_loan'];
       		$staff_id = $vehicle['f034fstaff_id'];
       		$staff_name = $vehicle['f034fname'];
            $model = $vehicle['f043fmodel'];
            $Originalmodel = $vehicle['f043fmodel'];
            $company_name = $vehicle['f043fcompany_name'];

            $upper = strtoupper($Originalmodel);
            // print_r($upper);exit;

       		$amount = $vehicle['f043ftotal_loan'];
       		$amount = number_format($amount, 2, '.', ',');

        $file_data = " 
        <table align='center' style='border:0px solid black;width: 110%;height:50px;margin-top: 4;'>
        	<tr>
            	<td style='text-align: center'><font size='4'><b>SKIM PEMBIAYAAN PEMBELIAN $upper UUM </b><br>BORANG PERSETUJUAN MEMBELI $upper</font></td>
            </tr>
        </table>

        <br><br>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'>
        	<tr>
            	<td style='text-align: left'  colspan='3' width='100%'><font size='3'><b><u>BUTIRAN PEMOHON</u></b></font></td>
            </tr>
            <tr>    
            	<td style='text-align: left' width='30%'><font size='3'><b>NAMA</b></font></td>
            	<td style='text-align: left' width='70%' colspan='2' ><font size='3'>$staff_name</font></td>
            </tr>  
            <tr>
            	<td style='text-align: left' width='30%'><font size='3'><b>NO KAD PENGENALAN</b></font></td>
            	<td style='text-align: left' width='35%'><font size='3'>$ic_no</font></td>
            	<td style='text-align: left' width='35%'><font size='3'><b>ALAMAT PENGHANTARAN</b></font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='30%'><font size='3'><b>NO STAF</b></font></td>
            	<td style='text-align: left' width='35%'><font size='3'>$staff_id</font></td>
            	<td style='text-align: left' width='35%' rowspan='2'><font size='3'>JABATAN PENDAFTAR,UUM<br>06010 SINTOK, KEDAH PENGHANTARAN</font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='30%'><font size='3'><b>PUSAT TANGGUNGJAWAB</b></font></td>
            	<td style='text-align: left' width='35%'><font size='3'>JABATAN PENDAFTAR</font></td>
            </tr> 
         </table>
         <br>

         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 3;'>
        	<tr>
            	<td style='text-align: left'  colspan='2'><font size='3'><b><u>PESANAN PEMBELIAN.</u></b></font><br><font size='3'>SILA TUAN BEKALKAN <b>$model</b> SEPERTI BERIKUT KE ALAMAT DI ATAS.</font></td>
            </tr>
        </table>
        
        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'> 
            <tr>
                <td style='text-align: left'  width='40%'><font size='3'><b>SPESIFIKASI</b></font></td>
                <td style='text-align: left'  width='60%'><font size='3'><b>HARGA(RM):</b></font></td>
            </tr>
            <tr>
                <td style='text-align: left'  width='40%' valign='top'><font size='3'>$loan_name</font></td>
                <td style='text-align: left'  width='60%'><font size='3'>HARGA BELIAN(RM)<br>MENGIKUT SEBUTHARGA: <b><u>$amount</u></b></font></td>
            </tr>
            <tr>
                <td style='text-align: center'  width='40%'><font size='3'><br><br>___________________</font></td>
                <td style='text-align: center'  width='60%'><font size='3'><br><br>___________________</font></td>
            </tr>
            <tr>
                <td style='text-align: center'  width='40%'><font size='3'>TANDATANGAN</font></td>
                <td style='text-align: center'  width='60%'><font size='3'>TARIKH</font></td>
            </tr>
        </table>
        <br><br>
         <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 3;'>
        	<tr>
            	<td style='text-align: left' width='100%'><font size='3'><b><u>UNTUK DISIL OLEH PEMBEKALAN</u></b><br> &nbsp;&nbsp;&nbsp;- &nbsp;&nbsp;&nbsp; PENGESAHAN PESANAN BELIAN<br>DENGAN INI DISAHKAN BAHAWA PESANAN TELAH DIBUAT OLEH PEMOHON SEPERTI DINYATAKAN DI RUANGAN A DI ATAS.</font></td>
            </tr>
        </table>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'> 
            <tr>
                <td style='text-align: center' width='50%'><font size='3'><br><br>__________________________</font></td>
                <td style='text-align: center' width='50%'><font size='3'><br><br></font></td>
            </tr>
            <tr>
            	<td style='text-align: left' width='70%'><font size='3'>TANDATANGAN & COP PEMBEKAL</font></td>
            	<td style='text-align: left' width='30%'><font size='3'>TARIKH</font></td>
            </tr>
         </table>

         <br><br>
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 3;'>
        	<tr>
            	<td style='text-align: left' width='100%'><font size='3'><b><u>UNTUK KENGUNAAN JABATAN BENDAHARI</u></b></font></td>
            </tr>
        </table>

        <table  align='center' border='1' style='border-collapse:collapse;width:100%;height:50px;'> 
            <tr>
            	<td style='text-align: left' width='100%' colspan='2'><font size='3'>DENGAN INI DISAHKAN BAHAWA PEMBELI BERKENAAN TELAH DILULUSKAN PEMBIAYAAN SEBANYAK RM________ UNTUK PEMBELIAN $model DENGAN $company_name TUAN. PERBEZAAN HARGA $model DENGAN AMAUN PEMBIAYAAN UUM SEBANYAK RM_______ HENDAKLAH DITUNTUT DARIPADA PEMBELI BERKENAAN .<br><br>SILA BEKALKAN BARANG SEPERTI DINYATAKAN  DI RUANGAN A DI ATAS</font></td>
            </tr>
            <tr>
                <td style='text-align: center' width='50%'><font size='3'><br><br>_________________</font></td>
                <td style='text-align: center' width='50%'><font size='3'><br><br>_________________</font></td>
            </tr>
            <tr>
            	<td style='text-align: center' width='50%'><font size='3'>TANDATANGAN</font></td>
            	<td style='text-align: center' width='50%'><font size='3'>TARIKH</font></td>
            </tr>";
             }

     	}

     	$file_data = $file_data . "</table>";
     	// print_r($file_data);exit();
                 $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}