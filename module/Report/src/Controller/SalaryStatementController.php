<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SalaryStatementController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();

        $mpdf = new Mpdf();
        
        $mpdf->setTitle('Sample PDF');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');
        
        $mpdf->WriteHTML($pdfData);
        
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;

        $mpdf->Output($path, 'F');
        
    }


    public function salaryStatementAction()
    {
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);

        if($this->isJSON($rawBody))
        {

        $financialperiod = $postData['financialperiod'];
        $reportRepository = $this->getRepository('T019fsavedReports');

       
       

        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time()); 

        




        $file_data = $file_data . " 
        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
            <td font-size='8' style='text-align: center'><b>Universiti Utara Malayasia<br> SENARAI PERUBAHAN DATA</b></td>
            </tr>
         </table>

        <table align='center' style='border:0px solid black;width: 100%;height:50px;margin-top: 4;'>
            <tr>
             <td style='text-align: left'><br>KOD LAPORAN : MR_60A<br>TARIKH : $pay_date</td>
             <td style='text-align: right;'><br>M/S : 1/52<br>MASA : $pay_time</td>
            </tr>
         </table>
         <br>";
         $component_result         = $reportRepository->getComponent1($financialperiod);
         //  print_r($component_result);
         // exit();

            foreach ($component_result as $row)
            {
                
            $salary_code = $row['f081fcode'];
            $salary_month = $row['MPD_PAY_MONTH'];
            $account_code = $row['f081faccounts_code_deduction'];
            $salary_component_name = $row['f081fname'];

                $file_data = $file_data . "
         
         <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
                <td style='text-align: center'>UNTUK BULAN :<b> $salary_month</b></td>
            </tr>
            <tr>
            <td style='text-align: left' style='border:0px solid black'>PENDAPATAN</td>
            </tr>
        </table>
          
        <table align='center' style='border:1px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
            <td style='text-align: left' width='15%'> KOD:</td>
            <td style='text-align: left' width='20%'> $salary_code</td>
            <td style='text-align: left' width='40%'> $salary_component_name</td>
            <td style='text-align: center' width='25%'> $account_code</td>
            </tr>
        </table>

        <table align='center' style='border:1px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
            <th style='text-align: center;' width='8%'> Bil</th>
            <th style='text-align: center;' width='12%'> No.Staff</th>
            <th style='text-align: center;' width='35%'> Nama Staf</th>
            <th style='text-align: center;' width='15%'> Jumlah</th>
            <th style='text-align: center;' width='30%'> TARAF KHIDMAT</th>
         </tr>
         <tr>
            <td colspan='6'><hr></td>
        </tr>";


            $salaryStatement = $reportRepository->getpaySlipemployeeData($financialperiod,$salary_code);
           print_r($salaryStatement);exit;
                $total = 0;
                $code = 1;
               for($i=0;$i<count($salaryStatement);$i++)
               {
                $staff_name = $salaryStatement[$i]['f034fname'];
                $MPD_STAFF_ID = $salaryStatement[$i]['MPD_STAFF_ID'];
                $MPD_PAID_AMT = $salaryStatement[$i]['MPD_PAID_AMT'];
                $original_amount = $salaryStatement[$i]['MPD_PAID_AMT'];
                $MP_STAFF_TYPE = $salaryStatement[$i]['MP_STAFF_TYPE'];
                // print_r($MPD_PAID_AMT);
                // exit();
                if($MP_STAFF_TYPE=='1') {
                    $MP_STAFF_TYPE = "Permanet";
                } else {
                    $MP_STAFF_TYPE = "Contract";


                }
                $MPD_PAID_AMT = number_format($MPD_PAID_AMT, 2, '.', ',');
                $staff_name = $salaryStatement[$i]['f034fname'];

                   $file_data = $file_data . "<tr>
                        <td style='text-align: center;' width='8%'> $code</td>
                        <td style='text-align: center;' width='12%'> $MPD_STAFF_ID</td>
                        <td style='text-align: center;' width='35%'> $staff_name </td>
                        <td style='text-align: center;' width='15%'> $MPD_PAID_AMT</td>
                        <td style='text-align: center;' width='30%'> $MP_STAFF_TYPE </td>
                 </tr>";
                 $code++;

                $total  =   floatval($total) + floatval($original_amount);
                
               }
               $total = number_format($total, 2, '.', ',');

               $file_data = $file_data . "<tr>
               <td colspan='6'><hr></td>
         </tr>
         
         <tr>
            <td colspan='2'></td>
            <td style='text-align: right' > Earning Total : </td>
            <td style='text-align: center'>RM $total</td>
            <td colspan='2'></td>
         </tr>
         </table>";
        }


        $component_result2         = $reportRepository->getComponent2($financialperiod);
        // print_r($component_result2);
        // exit;

        foreach ($component_result2 as $component2)
        {
                
            $salary_code = $component2['f081fcode'];
            $salary_month = $component2['MPD_PAY_MONTH'];
            $account_code = $component2['f081faccounts_code_deduction'];
            $salary_component_name = $component2['f081fname'];

                $file_data = $file_data . "
         
         <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
                <td style='text-align: center'>UNTUK BULAN :<b> $salary_month</b></td>
            </tr>
            <tr>
            <td style='text-align: left' style='border:0px solid black'>POTANGAN</td>
            </tr>
        </table>
          
        <table align='center' style='border:1px solid black;width: 100%;height:10px;margin-top: 4;'>
            <tr>
            <td style='text-align: left' width='15%'> KOD:</td>
            <td style='text-align: left' width='20%'> $salary_code</td>
            <td style='text-align: left' width='40%'> $salary_component_name</td>
            <td style='text-align: center' width='25%'> $account_code</td>
            </tr>
        </table>
        <table align='center' style='border:1px solid black;width: 100%;height:10px;margin-top: 4;'>

            <tr>
            <th style='text-align: center;' width='8%'> Bil</th>
            <th style='text-align: center;' width='12%'> No. Staff</th>
            <th style='text-align: center;' width='35%'> Nama Staf</th>
            <th style='text-align: center;' width='15%'> Jumlah</th>
            <th style='text-align: center;' width='30%'> TARAF KHIDMAT</th>
         </tr>
         <tr>
            <td colspan='6'><hr></td>
        </tr>";


            $salaryStatement = $reportRepository->getpaySlipemployeeData($financialperiod,$salary_code);
            //print_r($salaryStatement);exit;
                $total = 0;
                $code = 1;
               for($i=0;$i<count($salaryStatement);$i++)
               {
                $staff_name = $salaryStatement[$i]['f034fname'];
                $MPD_STAFF_ID = $salaryStatement[$i]['MPD_STAFF_ID'];
                $MPD_PAID_AMT = $salaryStatement[$i]['MPD_PAID_AMT'];
                $original_amount = $salaryStatement[$i]['MPD_PAID_AMT'];
                $MP_STAFF_TYPE = $salaryStatement[$i]['MP_STAFF_TYPE'];
                // print_r($MPD_PAID_AMT);
                // exit();
                if($MP_STAFF_TYPE=='1') {
                    $MP_STAFF_TYPE = "Permanet";
                } else {
                    $MP_STAFF_TYPE = "Contract";


                }
                $MPD_PAID_AMT = number_format($MPD_PAID_AMT, 2, '.', ',');
                $staff_name = $salaryStatement[$i]['f034fname'];

                   $file_data = $file_data . "<tr>
                        <td style='text-align: center;' width='8%'> $code</td>
                        <td style='text-align: center;' width='12%'> $MPD_STAFF_ID</td>
                        <td style='text-align: center;' width='35%'> $staff_name </td>
                        <td style='text-align: center;' width='15%'> $MPD_PAID_AMT</td>
                        <td style='text-align: center;' width='30%'> $MP_STAFF_TYPE </td>
                 </tr>";
                 $code++;

                $total  =   floatval($total) + floatval($original_amount);
                
               }
               $total = number_format($total, 2, '.', ',');

               $file_data = $file_data . "<tr>
               <td colspan='6'><hr></td>
         </tr>
         
         <tr>
            <td colspan='2'></td>
            <td style='text-align: right' > Deduction Total : </td>
            <td style='text-align: center'>RM $total</td>
            <td colspan='2'></td>
         </tr>
         </table>";
        }
 



              $name = gmdate("YmdHis") . ".pdf";
                $this->generatePdf($file_data, $name);
                
                return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
                ]);

        }
    }


}