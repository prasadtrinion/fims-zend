<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class junrnalBaucerE4Report extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }
// junrnalBaucerE4Report E-4
    public function junrnalBaucerE4ReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        $reportRepository = $this->getRepository('T019fsavedReports');

        // print_r($postData);exit;
        if($this->isJSON($rawBody))
        {

        // $year = $postData['financialYear'];
        // $departmentCode = $postData['departmentCode'];
        // $fundCode = $postData['fundCode'];
       
        
        //     $result = $reportRepository->getDistinctDepartment($year,$departmentCode,$fundCode);
        //     // print_r($result);
        //     // die();
        // $yearn = $reportRepository->getSummaryYear($year);
        // $yearn = $yearn[0]['f110fyear'];


        // print_r($yearn);
        // die();



            $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        

        $file_data = $file_data . "
    <html>
<head>
    <title>UUM</title>
</head>
<body>
    <form>
        <table align='center' width='80%'>
            <tr>
            <th style='text-align: center; width: 50%'>JABATAN BENDAHARI</th>
            <td style='text-align: center;width: 10%'>TARIKH : 02/03/2017</td>
            </tr>
            <tr>
            <th style='text-align: center;width: 10%'>UNIVERSITI UTARA MALAYSIA</th>
            <td style='text-align: center;width: 5%'>MUKASURT :1/1</td>
            </tr>
            <tr>
            <th style='text-align: center; width: 20%'>LAPORAN PEMPROSESAN BAUCER JUNRNAL</th>
            </tr>
        </table>
        <table align='left'>
            <tr>
                <th>DATA DIINPUT :</th>
                <th>SYUKRI</th>

            </tr>
            <tr>
                <th>TARIKHA/MASA INPUT :</th>
                <th>04/10/2016</th>
            </tr>
            <tr>
                <th>DAFTAR JURNAL :</th>
                <th>7943</th>
                
            </tr>
            <tr>
                <th>TAHUN DAFTAR :</th>
                <th>2016</th>
            </tr>
            <tr>
                <th>NO.INV JURNAL:</th>
                <th></th>
            </tr>
        </table>
        <table align='right' width='20%'>
            <tr>
                <td>PERIOD : 10/2016</td>
            </tr>
            <tr>
                <td> NO. JURNAL :J29783/16</td>
            </tr>
        </table>

        <table border='1px' align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 20%'>KODAKAUN</td>
            <td style='text-align: center;width: 10%'>KOD DR/CR</td>
            <td style='text-align: center; width: 10%'>SA/JBT</td>
            <td style='text-align: center;width: 10%'>RUJUKAN</td>
            <td style='text-align: center;width: 10%'>KOD SO</td>
            <td style='text-align: center;width: 10%'>DR (RM)</td>
            <td style='text-align: center;width: 20%'>KR (RM)</td>
        </tr>
    </table>
        <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 20%'>10006009302</td>
            <td style='text-align: center;width: 10%'>A95072</td>
            <td style='text-align: center; width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 20%'>2,158.00</td>
        </tr>
    </table><br>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 20%'>10006009302</td>
            <td style='text-align: center;width: 10%'>A94217</td>
            <td style='text-align: center; width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 20%'>2,058.00</td>
        </tr>
    </table><br>
        <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 20%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center; width: 10%'>52/4103</td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'>65,376.00</td>
            <td style='text-align: center;width: 20%'></td>
        </tr>
    </table><br><br><br>
    <table align='center' width='100%'>
        <tr>
            <td style='text-align: center; width: 20%'></td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center; width: 10%'></td>
            <td style='text-align: center;width: 10%'>JUMLAH</td>
            <td style='text-align: center;width: 10%'></td>
            <td style='text-align: center;width: 10%'>65,376.00</td>
            <td style='text-align: center;width: 20%'>65,376.00</td>
        </tr>
    </table><br><br>
    <table>
        <tr>
            <td><b> PERI HAL : </b></td>
            <td>BIASIWA TAJAAN KPM SLAB BAGI SEM A152 UNTUK 23 PERLAJAR</td>
        </tr>
        <td><b>PELARASAN UNTUK :</b></td>
        <td>BIASIWA TAJAAN KPM SLAM BAGI SEM A152 UNTUK 23 PELAJAR</td>
    </table>
    </form>
</body>
</html>

";
    


        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);

        }
    }
}