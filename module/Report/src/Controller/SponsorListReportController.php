<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class SponsorListReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Sponsor List Report A-2
    public function sponsorListReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        	// print_r($postData);exit;
        	$journalId = $postData['journalId'];

        	$reportRepository = $this->getRepository('T019fsavedReports');
        	// $journalData = $reportRepository->getJournalEntryById((int)$journalId);

        	// print_r($journalData);exit;
          // $JournalDescription = $journalData[0]['f017fdescription'];
          // $Approver = $journalData[0]['originalName'];


        $pay_date = date('m/d/Y h:i:s a', time());
        $pay_date   = date("d/m/Y", strtotime($pay_date));



        	$journal_data = "
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: right' width='100%' valign='top'>$pay_date</td>
        </tr>
        <tr>
          <td style='text-align: center' width='100%' valign='top'><font size='3'><b>UNIVERSITI UTARA MALAYSIA<br>SISTEM AKAUN PELAJAR<br>SENARAI NAMA PENAJA</b><font></td>
        </tr>
    </table>
    <br><br>

    <table align='center' width='100%'>
        <tr>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>BIL
          <font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>KOD<font></td>
          <td style='text-align: center' width='80%' valign='top'><font size='2'>NAMA PENAJA</td>
        </tr>
        <tr>
          <td style='text-align: center' width='10%' valign='top'><font size='2'><hr><font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'><hr><font></td>
          <td style='text-align: center' width='80%' valign='top'><font size='2'><hr><font></td>
        </tr>
        <tr>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>BIL<font></td>
          <td style='text-align: center' width='10%' valign='top'><font size='2'>KOD<font></td>
          <td style='text-align: center' width='80%' valign='top'><font size='2'>NAMA PENAJA<font></td>
        </tr>
    </table>";

      // $totaldr = number_format($totaldr, 2, '.', ',');
      // $totalcr = number_format($totalcr, 2, '.', ',');
        }
    	$name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($journal_data, $name);
    
            return new JsonModel([
                    'status' => 180,
                    'name'   => $name,
            ]);
      }
}