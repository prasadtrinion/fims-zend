<?php
namespace Report\Controller;

use Mpdf\Mpdf;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\JsonModel;

class ExpenditureReportController extends AbstractAppController
{
    protected $sm;
    public function __construct(ServiceManager $sm)
    {
        $this->sm = $sm;

        parent::__construct($sm);
    }

    public function generatePdf($pdfData, $name)
    {

      // print_r($_SERVER);exit;
        $em = $this->getEntityManager();
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($pdfData);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $name;
        $mpdf->Output($path, 'F');
    }

    //Expenditure Report
    public function expenditureReportAction()
    {

        // print_r("ert");exit;
        $rawBody          = file_get_contents("php://input");
        $postData         = json_decode($rawBody, true);
        if($this->isJSON($rawBody))
        {
        
        $year = $postData['financialYear'];
        $department = $postData['departmentCode'];
        // print_r($department);exit;
        $reportRepository = $this->getRepository('T019fsavedReports');
        $yearn = $reportRepository->getExpenditureYear($year);
        // print_r($yearn);exit;
        $department_name = $department_name[0];
        $yearn = $yearn[0]['f110fyear'];
                // print_r($yearn);exit;



        $pay_date = date('d/m/Y h:i:s a', time());              
        $pay_date   = date("d/m/Y", strtotime($pay_date));

        $pay_time = date('h:i:s a', time());

        $file_data = $file_data . "
        <style>
        table, th, td
        {
        border: 1px solid black;
        border-collapse: collapse;
        }
        </style>

    <table align='center' style='border:0px solid black;width: 100%;height:10px;margin-top: 4;'>
        
        <tr>
         <td style='text-align: center'  font-size='13' width='100%' valign='top' colspan='5'><b>UNIVERSITI UTARA MALAYSIA</b></td>
        </tr>
        <tr>
         <td style='text-align: center'  font-size='13' width='100%' valign='top' colspan='5'><b>KEDUDUKAN PERUNTUKAN, PERBELANJAAN DAN BAKI BAGI TAHUN $yearn</b></td>
        </tr>
        <tr>
        <td colspan='5' height='50%'><table width='100%'>
        <tr><td></td><br></tr>
        </table></td>
        </tr>
        <tr>
         <td style='text-align: center'  font-size='3' width='15%'><b>Kod</td>
         <td style='text-align: center'  font-size='3' width='40%'><b>Jesis Perbelaanjan</td>
         <td style='text-align: center'  font-size='3' width='15%'><b>Peruntukan</td>
         <td style='text-align: center'  font-size='3' width='15%'><b>Perbelanjaan</td>
         <td style='text-align: center'  font-size='3' width='15%'><b>Baki</td>
        </tr>";
                $result = $reportRepository->getExpenditure($year,$department);

                // print_r($result);exit();


                $expensed_total = 0;
                $pending_total = 0;
                foreach ($result as $expenditure)
                {
                $department_code = $expenditure['f015fdepartment_code'];
                $department_name = $expenditure['f015fdepartment_name'];
                $expenced_amount = $expenditure['f108fbudget_expensed'];
                $pending_amount = $expenditure['f108fbudget_pending'];
                $provision_amount = $expenditure['peovisionAmount'];


                $expenced = $expenditure['f108fbudget_expensed'];
                $pending = $expenditure['f108fbudget_pending'];
                $provision = $expenditure['peovisionAmount'];

                $expenced_amount = number_format($expenced_amount, 2, '.', ',');
                $pending_amount = number_format($pending_amount, 2, '.', ',');
                $provision_amount = number_format($provision_amount, 2, '.', ',');





                    $file_data = $file_data . "<tr>
         <td style='text-align: center'  font-size='3' width='15%'>$department_code</td>
         <td style='text-align: left'  font-size='3' width='40%'>$department_name</td>
         <td style='text-align: right'  font-size='3' width='15%'>$provision_amount</td>
         <td style='text-align: right'  font-size='3' width='15%'>$expenced_amount</td>
         <td style='text-align: right'  font-size='3' width='15%'>$pending_amount</td>
        </tr>";

        $expensed_total = floatval($expensed_total) + floatval($expenced);
        $pending_total = floatval($pending_total) + floatval($pending);
        $provision_total = floatval($provision_total) + floatval($provision);
                
                }
                $expensed_total = number_format($expensed_total, 2, '.', ',');
                $pending_total = number_format($pending_total, 2, '.', ',');
                $provision_total = number_format($provision_total, 2, '.', ',');
        $file_data = $file_data . "
        <tr>
        <td style='text-align: right'  font-size='3' colspan='2'> JUMLAH (RM):</td>
         <td style='text-align: right'  font-size='3' width='15%'>$provision_total</td>
         <td style='text-align: right'  font-size='3' width='15%'>$expensed_total</td>
         <td style='text-align: right'  font-size='3' width='15%'>$pending_total</td>
        </tr>
    </table>";
            }
    
        $name = gmdate("YmdHis") . ".pdf";
        $this->generatePdf($file_data, $name);
    
            return new JsonModel([
                    'status' => 200,
                    'name'   => $name,
            ]);
    }
}