<?php

namespace Report\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * T019fsavedReports
 *
 * @ORM\Table(name="t019fsaved_reports")
 * @ORM\Entity(repositoryClass="Report\Repository\ReportRepository")
 */
class T019fsavedReports
{
    /**
     * @var integer
     *
     * @ORM\Column(name="f019fid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $f019fid;

    /**
     * @var string
     *
     * @ORM\Column(name="f019fjson", type="text", length=65535, nullable=false)
     */
    private $f019fjson;

    /**
     * @var string
     *
     * @ORM\Column(name="f019fsql", type="text", length=65535, nullable=false)
     */
    private $f019fsql;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fstatus", type="integer", nullable=false)
     */
    private $f019fstatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fcreated_by", type="integer", nullable=false)
     */
    private $f019fcreatedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="f019fupdated_by", type="integer", nullable=false)
     */
    private $f019fupdatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fcreated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fcreatedDtTm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="f019fupdated_dt_tm", type="datetime", nullable=false)
     */
    private $f019fupdatedDtTm;

    /**
     * @var integer
     *
     * @ORM\Column(name="t09fis_saved", type="integer", nullable=false)
     */
    private $f019fisSaved;



    /**
     * Get f019fid
     *
     * @return integer
     */
    public function getF019fid()
    {
        return $this->f019fid;
    }

    /**
     * Set f019fjson
     *
     * @param string $f019fjson
     *
     * @return T019fsavedReports
     */
    public function setF019fjson($f019fjson)
    {
        $this->f019fjson = $f019fjson;

        return $this;
    }

    /**
     * Get f019fjson
     *
     * @return string
     */
    public function getF019fjson()
    {
        return $this->f019fjson;
    }

    /**
     * Set f019fsql
     *
     * @param string $f019fsql
     *
     * @return T019fsavedReports
     */
    public function setF019fsql($f019fsql)
    {
        $this->f019fsql = $f019fsql;

        return $this;
    }

    /**
     * Get f019fsql
     *
     * @return string
     */
    public function getF019fsql()
    {
        return $this->f019fsql;
    }

    /**
     * Set f019fstatus
     *
     * @param integer $f019fstatus
     *
     * @return T019fsavedReports
     */
    public function setF019fstatus($f019fstatus)
    {
        $this->f019fstatus = $f019fstatus;

        return $this;
    }

    /**
     * Get f019fstatus
     *
     * @return integer
     */
    public function getF019fstatus()
    {
        return $this->f019fstatus;
    }

    /**
     * Set f019fcreatedBy
     *
     * @param integer $f019fcreatedBy
     *
     * @return T019fsavedReports
     */
    public function setF019fcreatedBy($f019fcreatedBy)
    {
        $this->f019fcreatedBy = $f019fcreatedBy;

        return $this;
    }

    /**
     * Get f019fcreatedBy
     *
     * @return integer
     */
    public function getF019fcreatedBy()
    {
        return $this->f019fcreatedBy;
    }

    /**
     * Set f019fupdatedBy
     *
     * @param integer $f019fupdatedBy
     *
     * @return T019fsavedReports
     */
    public function setF019fupdatedBy($f019fupdatedBy)
    {
        $this->f019fupdatedBy = $f019fupdatedBy;

        return $this;
    }

    /**
     * Get f019fupdatedBy
     *
     * @return integer
     */
    public function getF019fupdatedBy()
    {
        return $this->f019fupdatedBy;
    }

    /**
     * Set f019fcreatedDtTm
     *
     * @param \DateTime $f019fcreatedDtTm
     *
     * @return T019fsavedReports
     */
    public function setF019fcreatedDtTm($f019fcreatedDtTm)
    {
        $this->f019fcreatedDtTm = $f019fcreatedDtTm;

        return $this;
    }

    /**
     * Get f019fcreatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fcreatedDtTm()
    {
        return $this->f019fcreatedDtTm;
    }

    /**
     * Set f019fupdatedDtTm
     *
     * @param \DateTime $f019fupdatedDtTm
     *
     * @return T019fsavedReports
     */
    public function setF019fupdatedDtTm($f019fupdatedDtTm)
    {
        $this->f019fupdatedDtTm = $f019fupdatedDtTm;

        return $this;
    }

    /**
     * Get f019fupdatedDtTm
     *
     * @return \DateTime
     */
    public function getF019fupdatedDtTm()
    {
        return $this->f019fupdatedDtTm;
    }

    /**
     * Set f019fisSaved
     *
     * @param integer $f019fisSaved
     *
     * @return T019fsavedReports
     */
    public function setF019fisSaved($f019fisSaved)
    {
        $this->f019fisSaved = $f019fisSaved;

        return $this;
    }

    /**
     * Get f019fisSaved
     *
     * @return integer
     */
    public function getF019fisSaved()
    {
        return $this->f019fisSaved;
    }
}
