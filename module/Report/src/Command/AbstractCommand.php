<?php
namespace Report\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Abstract Command
 */
abstract class AbstractCommand extends Command
{

    const COMMAND_NAME_REFIX = 'app:';

    /**
     * Add default prefix to command name
     * 
     * @param string $name
     * @return \Report\Command\AbstractCommand
     */
    public function setCommandName($name)
    {
        $this->setName(self::COMMAND_NAME_REFIX . $name);
        
        return $this;
    }
    
    /**
     * Returns doctrine entity manager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        $em = $this->getHelper('em')->getEntityManager();
        
        return $em;
    }
    
    /**
     * Returns doctrine connection
     * 
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        $conn = $this->getHelper('db')->getConnection();
        
        return $conn;
    }
    
    /**
     * Returns current timestamp
     * This value is used for createDtTm and updateDtTm fields
     */
    public function getCurrentTimestamp()
    {
        $timestamp = (new \DateTime())->format('Y-m-d H:i:s');
    
        return $timestamp;
    }
    
    /**
     * Initialize progress bar
     */
    public function getProgressBar($output, $max)
    {
        if(!$max){
            throw new \Exception('Not records found. Please check your input parameters.');
        }
    
        $progressBar = new ProgressBar($output, $max);
        $progressBar->setFormat("%current%/%max% [%bar%] %percent:3s%% ");
    
        return $progressBar;
    }
}