<?php
namespace Report\Repository;

use Report\Entity\T019fsavedReports;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Application\Entity\T054factivityMaster;
use Application\Entity\T054factivityDetails;


class ReportRepository extends EntityRepository {

    public function getData($data){

        $query = "SELECT ";
        $columns = $data['columns'];
        $last_key = end($columns);
        foreach ($columns as $column) {
            if ($column == $last_key) {
                $query = $query.$column;
            } else {
                $query = $query.$column.",";
            }
            
        }
        $query = $query." 
        FROM t014fglcode 
            LEFT JOIN t057ffund ON t014fglcode.f014ffund_id = t057ffund.f057fid         
            LEFT JOIN t058factivity_code ON t014fglcode.f014factivity_id = t058factivity_code.f058fid
            LEFT JOIN t015fdepartment ON t014fglcode.f014fdepartment_id = t015fdepartment.f015fid
            LEFT JOIN t059faccount_code ON t014fglcode.f014faccount_id = t059faccount_code.f059fid
            LEFT JOIN t018fjournal_details ON t014fglcode.f014fid = t018fjournal_details.f018fid_glcode
            LEFT JOIN t017fjournal ON t017fjournal.f017fid = t018fjournal_details.f018fid_journal";
        if($data['where']!=''){
        $query = $query." WHERE ".$data['where'];
        } 
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getInvoiceData($data){
        $id = $data['id'];

        $query = "select f071finvoice_number,f071finvoice_date,f066fname,f072fquantity,f072fprice,f072ftotal,f072fgst_code,f072ftax_amount,f072ftotal_exc,f081fcode from t072finvoice_details left join t066ffee_item on f072fid_item = f066fid inner join t081ftext_code on f081fid = f072fgst_code  inner join t071finvoice on f072fid_invoice = f071fid where f072fid_invoice = $id";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;
    }
    public function getAccountName($oneCode,$twoCode){
           
            $query = "select a.f059fname from t059faccount_code as a inner join t059faccount_code as b on a.f059fparent_code = b.f059fid where b.f059fcode = '$oneCode' and a.f059fcode = '$twoCode'";
      
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        // echo $query;
        // print_r($result);
        // die();
        return $result['f059fname'];
    }
    public function getAccountName1($oneCode){
        $query = "select f059fname from t059faccount_code where f059fcode = '$oneCode' ";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        // echo $query;
        // print_r($result);
        // die();
        return $result['f059fname'];
    }
    public function getAccountName2($oneCode,$twoCode,$threeCode){
        $finalCode = $oneCode.$twoCode.$threeCode;
        $query = "select f059fname from t059faccount_code where f059fcomplete_code = '$finalCode' ";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        // echo $query;
        // print_r($result);
        // die();
        return $result['f059fname'];
    }
    public function getBalanceReportData($data){
       
        $em = $this->getEntityManager();
        $month = date('m');
        $level = $data['level'];
        $currentYear = date('Y');
        $type = $data['type'];
        if($type == "PL"){
            $table = "t018fmonthly_summary";
            $type_id = 1;
        }
        else{
            $table = "t018fjournal_details";
            $type_id = 2;
        }
        if(isset($data['departmentId'])){
            $department_query = " and f018fdepartment_code = '".$data['departmentId']."'";
        }
        else{
            $department_query = '';
        }
        $query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,f015fdepartment_name,f018fdepartment_code from $table inner join t015fdepartment on f018fdepartment_code = f015fdepartment_code inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where year(f018fcreate_dt_tm) = $currentYear and f059ftype = '$type_id' $department_query group by f018fdepartment_code,f015fdepartment_name";
        $select = $em->getConnection()->executeQuery($query);
        $one_digit_result = $select->fetchAll();
        // echo $query;
        // print_r($one_digit_result);
        // die();
        $month_query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,f015fdepartment_name,f018fdepartment_code from $table inner join t015fdepartment on f018fdepartment_code = f015fdepartment_code inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where year(f018fcreate_dt_tm) = $currentYear and month(f018fcreate_dt_tm) = $month and f059ftype = '$type_id' $department_query group by f018fdepartment_code,f015fdepartment_name";
        $select = $em->getConnection()->executeQuery($month_query);
        $month_result = $select->fetchAll();
        for($k=0;$k<count($one_digit_result);$k++){
            $one_digit_result[$k]['month_cr_amount'] = ($month_result[$k]['cr_amount'] == NULL)? 0:$month_result[$k]['cr_amount'];
            $one_digit_result[$k]['month_dr_amount'] = ($month_result[$k]['dr_amount'] == NULL)? 0:$month_result[$k]['dr_amount'];
            }
             
        $count1 = count($one_digit_result);
        for($i = 0;$i < $count1; $i++){
            $departmentCode = $one_digit_result[$i]['f018fdepartment_code'];
            $query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,1,1) as one from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where f018fdepartment_code = '$departmentCode' and year(f018fcreate_dt_tm) = $currentYear  $department_query  group by SUBSTRING(f018faccount_code,1, 1)";
    
            $select = $em->getConnection()->executeQuery($query);
            $two_digit_result = $select->fetchAll();
            
            $month_query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,1, 1) as one,f018faccount_code from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where f018fdepartment_code = '$departmentCode' and year(f018fcreate_dt_tm) = $currentYear and month(f018fcreate_dt_tm) = $month  $department_query group by SUBSTRING(f018faccount_code,1,1)";
            $select = $em->getConnection()->executeQuery($month_query);
            $month_result = $select->fetchAll();
            for($k=0;$k<count($two_digit_result);$k++){
                $two_digit_result[$k]['month_cr_amount'] = ($month_result[$k]['cr_amount'] == NULL)? 0:$month_result[$k]['cr_amount'];
                $two_digit_result[$k]['month_dr_amount'] = ($month_result[$k]['dr_amount'] == NULL)? 0:$month_result[$k]['dr_amount'];
                }

            $count2 = count($two_digit_result);
            for($j = 0;$j < $count2; $j++){
                $value1 = $two_digit_result[$j]['one'];
                $query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,2,1) as two  from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code   where  f018fdepartment_code = '$departmentCode' and SUBSTRING(f018faccount_code,1,1) like '$value1' and year(f018fcreate_dt_tm) = $currentYear  $department_query group by SUBSTRING(f018faccount_code,2,1)";
                $select = $em->getConnection()->executeQuery($query);
                $three_digit_result = $select->fetchAll();
                
                $month_query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,2,1) as two  from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code   where f018fdepartment_code = '$departmentCode' and SUBSTRING(f018faccount_code,1,1) like '$value1' and year(f018fcreate_dt_tm) = $currentYear and month(f018fcreate_dt_tm) = $month  $department_query group by SUBSTRING(f018faccount_code,2,1)";
                $select = $em->getConnection()->executeQuery($month_query);
                $month_result = $select->fetchAll();
                
                for($k=0;$k<count($three_digit_result);$k++){
                $three_digit_result[$k]['month_dr_amount'] = ($month_result[$k]['dr_amount'] == NULL)? 0:$month_result[$k]['dr_amount'];
                $three_digit_result[$k]['month_cr_amount'] = ($month_result[$k]['cr_amount']== NULL)? 0:$month_result[$k]['cr_amount'];
                }
                $count3 = count($three_digit_result);
            for($m = 0;$m < $count3; $m++){
                $value2 = $three_digit_result[$m]['two'];
                    
                $query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,3,3) as three,f018faccount_code from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where  f018fdepartment_code = '$departmentCode' and SUBSTRING(f018faccount_code,1,1) like '$value1' and SUBSTRING(f018faccount_code,2,1) like '$value2' and year(f018fcreate_dt_tm) = $currentYear  $department_query group by SUBSTRING(f018faccount_code,3,3),f018faccount_code";
        
                $select = $em->getConnection()->executeQuery($query);
                $four_digit_result = $select->fetchAll();
                
                $month_query = "select SUM(cast(f018fcr_amount as float)) as cr_amount, SUM(cast(f018fdr_amount as float)) as dr_amount,SUBSTRING(f018faccount_code,3,3) as three,f018faccount_code from $table inner join t059faccount_code on f018faccount_code = f059fcomplete_code  where f018fdepartment_code = '$departmentCode' and SUBSTRING(f018faccount_code,1,1) like '$value1' and SUBSTRING(f018faccount_code,2,1) like '$value2' and year(f018fcreate_dt_tm) = $currentYear and month(f018fcreate_dt_tm) = $month  $department_query group by SUBSTRING(f018faccount_code,3,3),f018faccount_code";
                $select = $em->getConnection()->executeQuery($month_query);
                $month_result = $select->fetchAll();
                
                for($k=0;$k<count($four_digit_result);$k++){
                $four_digit_result[$k]['month_dr_amount'] = ($month_result[$k]['dr_amount'] == NULL)? 0:$month_result[$k]['dr_amount'];
                $four_digit_result[$k]['month_cr_amount'] = ($month_result[$k]['cr_amount']== NULL)? 0:$month_result[$k]['cr_amount'];
                }
                $three_digit_result[$m]['four_digit_result'] = $four_digit_result;
                }
                $two_digit_result[$j]['three_digit_result'] = $three_digit_result;

                
            }

            
        $one_digit_result[$i]['two_digit_result'] = $two_digit_result;


        }
    
    // print_r($one_digit_result);
    //     die();
        
        return $one_digit_result;
        
    }
    public function getTBData($data){
       
        $em = $this->getEntityManager();
        $fromDay = $data['fromDay'];
        $toDay = $data['toDay'];
        $month = $data['month'];
        $year = $data['year'];
        $department = $data['department'];
        $fromDate = $year.'-'.$month.'-'.$fromDay;
        $fromDate = date("Y-m-d",strtotime($fromDate));
        $toDate = $year.'-'.$month.'-'.$toDay;
        $toDate = date("Y-m-d 00:00:00",strtotime($toDate));
        $defaultDate = $year.'-01-01';
        $defaultDate = date("Y-m-d 00:00:00",strtotime($defaultDate));

        if(isset($department)){
            $department_query = "and f018fdepartment_code = '$department'";
        }
        else{
            $department_query='';
        }
        $query = "select f059fname,SUM(f018fcr_amount) as cr_amount, SUM(f018fdr_amount) as dr_amount,f018faccount_code from t018fjournal_details inner join t059faccount_code on f059fcomplete_code = f018faccount_code where f018fcreate_dt_tm between  '$fromDate' and '$toDate' $department_query  group by f018faccount_code,f059fname order by f018faccount_code";
        $month_result = $em->getConnection()->executeQuery($query)->fetchAll();

       //  echo $query;
       // print_r($month_result);
       // die();
        $query = "select f059fname,SUM(f018fcr_amount) as cr_amount, SUM(f018fdr_amount) as dr_amount,f018faccount_code from t018fjournal_details inner join t059faccount_code on f059fcomplete_code = f018faccount_code and f018fcreate_dt_tm between '$defaultDate' and '$toDate' $department_query group by f018faccount_code,f059fname order by f018faccount_code";
        $year_result = $em->getConnection()->executeQuery($query)->fetchAll();
       
            for($i=0;$i<count($month_result);$i++){
                $month_result[$i]['year_cr_sum'] = $year_result[$i]['cr_amount'];
                $month_result[$i]['year_dr_sum'] = $year_result[$i]['dr_amount'];
            }
       // print_r($month_result);
       // die();
        return $month_result;
        
    }
    public function getList() 
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query
        $qb->select('r.f019fid,r.f019fjson,r.f019fsql,r.f019fstatus,r.t09fisSaved')
            ->from('Report\Entity\T019fsavedReports','r');

        $query = $qb->getQuery();

        $result = array(
            'data' => $query->getArrayResult(),
        );

        return $result;
        
    }

    /* to retrive the data from database using id*/

    public function getListById($id) 
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r.f019fid,r.f019fjson,r.f019fsql,r.f019fstatus,r.t09fisSaved')
            ->from('Report\Entity\T019fsavedReports','r')
            ->where('r.f019fid = :reportId')
            ->setParameter('reportId',$id);

        $query = $qb->getQuery();
        $result = $query->getSingleResult();

        return $result;
    }

   

    public function createNewData($data) 
    {
        $em = $this->getEntityManager();

        $reportObj = new T019fsavedReports();

        $reportObj->setF019fjson($data['report_json'])
             ->setF019fsql($data['report_sql'])
             ->setF019fstatus((int)1)
             ->setF019fcreatedBy("1")
             ->setF019fupdatedBy("1")
             ->setF019fisSaved("0");

        $reportObj->setF019fcreatedDtTm(new \DateTime())
                ->setF019fupdatedDtTm(new \DateTime());

 
        $em->persist($reportObj);
        $em->flush();

        return;

    }
    public function updateReport($report, $data = []) 
    {
        $em = $this->getEntityManager();

        $report->setT09fisSaved($data['is_save']);
                

        $report->setF019fupdatedDtTm(new \DateTime());
        
        $em->persist($report);
        $em->flush();

    }
    public function getItem(){

        $query = "select * from tbl_item";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;
    }

    

    public function getGst(){

        $query = "select * from tbl_gst";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getHtml1ReportData($id){
        
        $em = $this->getEntityManager();
        $query = "select f063famount,f063fmaturity_date,f063finstitution_id,f061fname,f061fbranch,f063fduration,f063fprofit_rate from t063fapplication_master inner join t063fapplication_details on f063fid=f063fid_master inner join t061finvestment_institution on f061fid=f063finstitution_id where f063fid = $id";
        $result1 = $em->getConnection()->executeQuery($query)->fetchAll();
        $data_result = array();
        foreach ($result1 as $item) {
            $date                        = $item['f063fmaturity_date'];
            $item['f063fmaturity_date'] = date("d F Y", strtotime($date));
            array_push($data_result, $item);
        }
        $query = "select f014fuser_name,f063fapproved1_by from t063fapplication_details inner join t014fuser on f014fid = f063fapproved1_by where f063fid_details = $id";
        $one_result = $em->getConnection()->executeQuery($query)->fetch();

        $query = "select f014fuser_name,f063fapproved2_by from t063fapplication_details inner join t014fuser on f014fid = f063fapproved2_by where f063fid_details = $id";
        $two_result = $em->getConnection()->executeQuery($query)->fetch();

        $query = "select f014fuser_name,f063fapproved3_by from t063fapplication_details inner join t014fuser on f014fid = f063fapproved3_by where f063fid_details = $id";
        $three_result = $em->getConnection()->executeQuery($query)->fetch();
        $result['one'] = $one_result['f014fuser_name'];
        $result['two'] = $two_result['f014fuser_name'];
        $result['three'] = $three_result['f014fuser_name'];
        $result['data'] = $data_result;
       
        return $result;   
    }

    public function getStudentInvoiceData($id){
        
        $em = $this->getEntityManager();
        try{
        $query = "select f071fid,f071finvoice_number,f071finvoice_date,f071finvoice_total,f071fbalance,f060fname from t063fsponsor_bill inner join t064fsponsor_bill_details on f064fid_sponser_bill = f063fid inner join t071finvoice on f064fid_invoice = f071fid inner join t060fstudent on f060fid = f064fid_student where f063fid = $id";
        $invoice_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        $detail_result = array();
        for($i=0;$i<count($invoice_result);$i++) {
            $invoice_id = $invoice_result[$i]['f071fid'];
            $query = "select f072fid_item,f072ftotal,f066fname from t072finvoice_details inner join t066ffee_item on f066fid = f072fid_item where f072fid_invoice = $invoice_id";
            $result = $em->getConnection()->executeQuery($query)->fetch();
            $invoice_result[$i]['details'] = $result;
        }
    }
    catch(\Exception $e){
        echo $e;
    }
       
        return $invoice_result;   
    }

    public function updateStatus($data){
        $id = $data['id'];
        $status = $data['status'];
        $em = $this->getEntityManager();
        $query = "update  t064finvestment_registration set f064fis_online = $status where f064fid = $id";
        $em->getConnection()->executeQuery($query);
       return;
    }
    public function getVehicleLoanData($id){
        $em = $this->getEntityManager();

        $loanRepository = $em->getRepository("Application\Entity\T043fvehicleLoan");
        $result = $loanRepository->getListById((int)$id);
        return $result;
    }
     public function getMasterInvoiceList()
    {

        //$id = 1;
        $query = "select f071finvoice_number,f071finvoice_date,f071finvoice_type,f071fbill_type,f015fname,f071fid_customer,f071fdescription,f071finvoice_from,f071fbalance,f071ftotal_paid from t071finvoice inner join t015ffinancialyear on f015fid=f071fid_financial_year";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        $result1 = array();
        foreach ($result as $invoice)
        {
            $date              = $invoice['f071finvoice_date'];
            $invoice['f071finvoice_date'] = date("Y-m-d", strtotime($date));
            // print_r($invoice);
            // exit();
            array_push($result1, $invoice);
        }

        return $result;    

    }

    public function getSalarySlipData($employeeid,$month)  
    {  
        $query = "select  f081fname, MPD_PAID_AMT, f081ftype_of_component, f034fname, MP_DEPT_DESC, MPD_PAY_MONTH,f081fshort_name  from t107fmonthly_payroll_details inner join t081fsalary on f081fcode = MPD_INCOME_CODE inner join t034fstaff_master on f034fstaff_id = MPD_STAFF_ID
            inner join t106fmonthly_payroll on MP_STAFF_ID = 
            MPD_STAFF_ID  where MPD_STAFF_ID = '$employeeid' and
            MPD_PAY_MONTH = '$month'";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
       public function getpaySlipemployeeData($financialperiod,$salary_code)
    {
        // $id = $data['id'];t106fmonthly_payroll
        // $id = 24;
         $query = "select  MPD_PAY_MONTH, MPD_STAFF_ID, MPD_INCOME_CODE,  MPD_PAID_AMT, f081ftype_of_component, f034fname, f081fname, f081fcode, MP_STAFF_TYPE, f059fcode  from t107fmonthly_payroll_details inner join t081fsalary on f081fcode = MPD_INCOME_CODE inner join t034fstaff_master on f034fstaff_id = MPD_STAFF_ID inner join t106fmonthly_payroll on MP_STAFF_ID = MPD_STAFF_ID inner join t059faccount_code on f059fcomplete_code = MP_STAFF_BANK_ACC_NO WHERE MPD_PAY_MONTH LIKE '$financialperiod' and f081fcode = '$salary_code'";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getComponent1($financialperiod)
    {

        $i = 1;
        $query = "select distinct(MPD_INCOME_CODE) as salarycode, f081fcode,t081fsalary.*,t107fmonthly_payroll_details.* from t081fsalary join t107fmonthly_payroll_details on  f081fcode = MPD_INCOME_CODE where MPD_PAY_MONTH LIKE '$financialperiod' and f081ftype_of_component = $i group by MPD_INCOME_CODE";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }
       public function getpaySlipStatementData($financialperiod,$salary_code)
    {
         $query = "select  MPD_PAY_MONTH, f081fname, MP_STAFF_TYPE, f059fcode  from t107fmonthly_payroll_details inner join t081fsalary on f081fcode = MPD_INCOME_CODE inner join t034fstaff_master on f034fstaff_id = MPD_STAFF_ID inner join t106fmonthly_payroll on MP_STAFF_ID = MPD_STAFF_ID inner join t059faccount_code on f059fcomplete_code = MP_STAFF_BANK_ACC_NO WHERE MPD_PAY_MONTH LIKE '$financialperiod' and f081fcode = '$salary_code'";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getComponent2($financialperiod)
    {

        $i = 2;
        $query = "select distinct(MPD_INCOME_CODE) as salarycode, f081fcode,t081fsalary.*,t107fmonthly_payroll_details.* from t081fsalary join t107fmonthly_payroll_details on  f081fcode = MPD_INCOME_CODE where MPD_PAY_MONTH LIKE '$financialperiod' and f081ftype_of_component = $i group by MPD_INCOME_CODE";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }
    public function getAccountData()
    {
        $id = 0;
        $query = "select *  from t059faccount_code where f059flevel_status = $id";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getBudgetAmountData()
    {
        $query = "select f051famount, f015fdepartment_name from t051fbudget_costcenter inner join t015fdepartment on f015fdepartment_code = f051fdepartment  ";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    //summaryStatement
       public function getSummaryYear($year) 
    {
        $query = "select f110fyear from t110fbudgetyear inner join t054factivity_master on f110fid = f054fid_financialyear where  f054fid_financialyear = $year";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }
    public function getDistinctDepartment($year,$departmentCode,$fundCode)
    {
        if(isset($fundCode)){
            $fund_query = " and f054ffund = '$fundCode'";
        }
        if(isset($departmentCode)){
            $department_query = " and f054fdepartment = '$departmentCode'";
        }
        // print_r($year);exit;
        $query = "select DISTINCT(dep.f015fdepartment_name), dep.f015fdepartment_code from t054factivity_master am join t015fdepartment dep on  am.f054fdepartment = dep.f015fdepartment_code where am.f054fid_financialyear = $year $fund_query $department_query";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }
    public function getActivityData($department_id,$fund_id) 
    {
        $department_id= substr($department_id,0,3);

       if($fund_id!=''){
           $fund_query = " and f054ffund_code = '$fund_id'";
       }
         // print_r($department_id);exit;
    $query = "select f054famount, f059fname, f054faccount_code from t054factivity_details inner join t059faccount_code on f054faccount_code = f059fcomplete_code where SUBSTRING(f054fdepartment_code,1,3) = '$department_id' $fund_query ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
        

    }
    //remainingBalanceReport
    public function getBalanceYear($year) 
    {
       $query = "select f110fyear from t110fbudgetyear where  f110fid = $year";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }

    public function getBudgetSummary($year,$department) 
    {

         if (isset($department))
        {
                $department= substr($department,0,3);
               $department_query = "and SUBSTRING(bs.f108fdepartment,1,3) = '$department'";
        

        }
       
            $query = "select dep.f015fdepartment_name, bs.f108fallocated_amount, bs.f108fincrement_amount as adjustmentAmount, bs.f108fveriment_from, bs.f108fveriment_to, bs.f108fbudget_expensed as Expenditure, bs.f108fbudget_pending as pending, dep.f015fdepartment_code, bs.f108faccount, bs.f108ffund, bs.f108factivity, bs.f108fdepartment, bs.f108fincrement_amount as incrementAmount, bs.f108fdecrement_amount as decrementAmount from t108fbudget_summary bs inner join t015fdepartment dep on dep.f015fdepartment_code = bs.f108fdepartment 

             where bs.f108fid_financial_year = $year $department_query";
           
       

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        // print_r($result);exit;
        // $arrayData = array();

        for ($i = 0; $i<count($result); $i++) 
        {
           

        $account = $result[$i]['f108faccount'];
        $fund = $result[$i]['f108ffund'];
        $activity = $result[$i]['f108factivity'];
        $department = $result[$i]['f108fdepartment'];
       
        

        $query1 = "select ad.f054fso_code from t054factivity_details ad inner join t054factivity_master am on am.f054fid = ad.f054fid_activity_master where ad.f054fdepartment_code = '$department' and ad.f054faccount_code = '$account' and ad.f054ffund_code = '$fund' and ad.f054factivity_code = '$activity' and am.f054fid_financialyear = $year  ";
            
       

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query1);
        $resultso = $select->fetch();

        $result[$i]['so_code'] = $resultso['f054fso_code'];


        }
        
        return $result;



        
    }
    //ExpenditureReport
    public function getExpenditureYear($year) 
    {
        $query = "select f110fyear from t110fbudgetyear inner join t108fbudget_summary on f110fid = f108fid_financial_year where  f108fid_financial_year = $year";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;

    }
    public function getExpenditure($year,$department) 
    {
        // $year = (int)$year;
         
        if ($department == NULL | '')
        {
            $query = "select f015fdepartment_name, f015fdepartment_code, f108fbudget_expensed, f108fbudget_pending from t108fbudget_summary inner join t015fdepartment on f015fdepartment_code = f108fdepartment where f108fid_financial_year = $year";
        
        }
        else
        {
            $department= substr($department,0,3);
            
            $query = "select f015fdepartment_name, f015fdepartment_code, f108fbudget_expensed, f108fbudget_pending from t108fbudget_summary inner join t015fdepartment on f015fdepartment_code = f108fdepartment where f108fid_financial_year = $year and SUBSTRING(f108fdepartment,1,3) like '$department'";
            // print_r($query);exit;
        }

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }    
    //Account Code Report
    public function getAccountCodeList()
    {
        $em = $this->getEntityManager();
        
        $query = "select f059fid,f059fbalance_type,f059fcode,f059fname from t059faccount_code where f059flevel_status = 0";
        $result = $em->getConnection()->executeQuery($query)->fetchAll();

        for($i=0;$i<count($result);$i++)
        {
            $parent1_id = $result[$i]['f059fid'];
            
            $tempcode = $result[$i]['f059fcode'];

            $result[$i]['f059fcode'] = str_pad($result[$i]['f059fcode'], 5, '0', STR_PAD_RIGHT);;
            // print_r($result[$i]);
            
            $query = "select f059fid,f059fbalance_type,f059fcode,f059fname from t059faccount_code where f059flevel_status = 1 and f059fparent_code = $parent1_id";
            $result1 = $em->getConnection()->executeQuery($query)->fetchAll();
            for($j=0;$j<count($result1);$j++)
            {
                $parent2_id = $result1[$j]['f059fid'];
            $result1[$j]['f059fcode'] = $tempcode . str_pad($result1[$j]['f059fcode'], 4, '0', STR_PAD_RIGHT);;


                // print_r($result[$i]);
                
                $query = "select f059fid,f059fbalance_type,f059fcode,f059fcomplete_code,f059fname from t059faccount_code where f059flevel_status = 2 and f059fparent_code = $parent2_id";
                $result2 = $em->getConnection()->executeQuery($query)->fetchAll();
                $result1[$j]['level2'] = $result2;
            }
            $result[$i]['level1']  = $result1;
        }

        return $result; 
    }
    //FDwithdrawlReport
    public function getFdWithdrawl($applicationId) 
    {
         // print_r($department_id);exit;
    $query = "select ad.f063fbank_batch, ad.f063fcreated_dt_tm, ad.f063fbank_id, bk.f042fbank_name, ad.f063fbranch, ad.f063finstitution_id, i.f061finstitution_code, ad.f063fmaturity_date, ad.f063famount from t063fapplication_details ad left join t042fbank bk on bk.f042fid = ad.f063fbank_id left join t061finvestment_institution i on i.f061fid = ad.f063finstitution_id where ad.f063fid_details = $applicationId";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        


        for ($i = 0; $i<count($result); $i++) 
        {
           

        $bankId = $result[$i]['f063fbank_id'];

        $query1 = "select f041faccount_number from t041fbank_setup_account where f041fid_bank = $bankId  ";
            
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query1);
        $resultso = $select->fetch();

        $result[$i]['f041faccount_number'] = $resultso['f041faccount_number'];


        }
        return $result;
    }
    //fixedSecurityAccountsReport
    public function diffMonth($effective_date,$maturity_date)
    {
                // print_r($maturity_date);exit;
                $year1   = date("Y", strtotime($effective_date));
                $year2 = date("Y", strtotime($maturity_date));
                
                
                $month1 = date("m", strtotime($effective_date));
                $month2 = date("m", strtotime($maturity_date));

                $day1 = date("d", strtotime($effective_date));
                $day2 = date("d", strtotime($maturity_date));

                
            $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

            // print_r($diff);exit;
            if($diff<=12)
            {
                return $diff." bulan";
            }
            else
            {
                $year = floor($diff / 12);
                $diff = $diff %12;

                if($year <= 1 )
                {
                $print_year = $year." tahun ";
                    if($diff != 0)
                    {
                        $print_month = $diff." bulan";
                    }
                }
                else
                {
                    $print_year = $year." tahun ";
                    if($diff != 0)
                    {
                        $print_month = $diff." bulan";
                    }
                }
                return $print_year.$print_month;
            }
    }
    public function getFixedSecurityAccounts($applicationId)
    {
       $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select("a.f063fid, a.f063fapplicationId, a.f063fdescription, a.f063finvestmentAmount, a.f063fstatus, a.f063fisOnline, d.f063fduration, d.f063fdurationType, d.f063fidDetails, d.f063fbranch, d.f063fidMaster, d.f063famount, d.f063finstitutionId, d.f063fprofitRate, d.f063fmaturityDate, d.f063fidInvestmentType, d.f063fbankId, d.f063fbankBatch,b.f042fbankName, a.f063ffileUpload, d.f063fapproved1Status, invt.f038ftype as investmentType, d.f063fapproved2Status, d.f063fapproved3Status, a.f063fuumBank, b.f042fbranchName, ii.f061finstitutionCode, ii.f061fname, ii.f061faddress, ii.f061fcontactNo, ii.f061fcontactPerson, ir.f064frateOfInterest, ir.f064finterstToBank, ir.f064fcontactPerson, ir.f064fcontactPerson2, ir.f064fcontactEmail, ir.f064fcontactEmail2")
          ->from('Application\Entity\T063fapplicationMaster','a')
          ->leftjoin('Application\Entity\T063fapplicationDetails','d','with','d.f063fidMaster= a.f063fid')
          ->leftjoin('Application\Entity\T042fbank','b','with','d.f063fbankId= b.f042fid')
          ->leftjoin('Application\Entity\T061finvestmentInstitution','ii','with','d.f063finstitutionId = ii.f061fid')
          ->leftjoin('Application\Entity\T038finvestmentType','invt',Expr\Join::INNER_JOIN,'invt.f038fid = d.f063fidInvestmentType')
          ->leftjoin('Application\Entity\T064finvestmentRegistration','ir','with','a.f063fid = ir.f064fidApplication')
          ->where('d.f063fidDetails = :applicationId')
          ->andWhere('d.f063fapproved1Status=1')
            ->andWhere('d.f063fapproved2Status=1')
            ->andWhere('d.f063fapproved3Status=1')
          ->setParameter('applicationId',(int)$applicationId);     

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;

    }

    public function getuumBank($applicationId)
    {
        $query = "SELECT  b.f145fbatch_no, b.f145fmaturity_date, bnk.f042fbank_name as uumBank, bnk1.f042fbank_name as investmentBank, bsa.f041faccount_number as uumBankAccNo, bsa.f041faddress as uumBankAddress, bsa.f041fcontact_person as uumBankContactPerson, bsa.f041fphone as uumBankPhone, bsa.f041fdescription as uumBankDescription, bsa.f041fbranch as uumBranch, bsa.f041fcity as uumBankCity, bsa.f041fstate as uumBankState, bsa.f041fcountry uumBankCountry, bnk1.f042fbranch_name as invBankBranch, bnk1.f042faccount_number as invBankAccNo, SUM(bwd.f146famount) as amount from t145fbatch_withdraw b inner join t041fbank_setup_account bsa on bsa.f041fid = b.f145fuum_bank inner join t042fbank bnk on bnk.f042fid = bsa.f041fid_bank  inner join t042fbank bnk1 on bnk1.f042fid = b.f145finvoice_bank inner join t146fbatch_withdraw_details bwd on bwd.f146fid_batch = b.f145fid where b.f145fid = $applicationId";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        return $result;

    }

    public function getFDCertificate($applicationId)
    {
        $query = "SELECT ir.f064freference_number,bwd.f146famount, bwd.f146fmaturity_date from t064finvestment_registration ir inner join t146fbatch_withdraw_details bwd on bwd.f146fapplication = ir.f064fid where bwd.f146fid_batch = $applicationId ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function budgetFileImport() 
    {
        $em = $this->getEntityManager();
        $workingfine = 0;
        $query = "select q1,q2,q3,q4,amount from budget_costcenter_dump"; 
        $sum1_result = $em->getConnection()->executeQuery($query)->fetchAll();
        foreach($sum1_result as $row)
        {
            if((int)$row['q1']+(int)$row['q2']+(int)$row['q3']+(int)$row['q4'] != (int)$row['amount'])
            {
                $workingfine = 1;
                break;
            }
        }
        if($workingfine == 0)
        {
        $query = "select sum(cast(amount as float)) as total_amount,financial_year,cost_center from budget_costcenter_dump group by financial_year,cost_center"; 
        $sum_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        foreach($sum_result as $row) {
             $financialYear = $row['financial_year'];
             $departmentCode = $row['cost_center'];
        
             $query = "select f051fdepartment,f051famount,f015fname from t051fbudget_costcenter inner join t015ffinancialyear on f015fid = f051fid_financialyear where f015fname='$financialYear' and f051fdepartment = '$departmentCode'";
            $costcenter_result = $em->getConnection()->executeQuery($query)->fetch();
        
        
             if($costcenter_result['f051famount'] == $row['total_amount']) {
                 $workingfine = 0;
             } else {
                 $workingfine = 1;
                 
                 break;
             }
        
         }
        }
         if($workingfine == 1) {
             print("please import proper xls there is mismath in amounts allocated ");
         }
         else{
            
            $query = 'select sum(cast(amount as float)) as total_amount,cost_center,financial_year from budget_costcenter_dump inner join t015ffinancialyear on f015fname = financial_year group by financial_year,cost_center,f015fid';
        $sum_result = $em->getConnection()->executeQuery($query)->fetchAll();
        
        for($i=0;$i<count($sum_result);$i++){
            $financialYear = $sum_result[$i]['financial_year'];
            $costCenter = $sum_result[$i]['cost_center'];
            $query = "select amount,fund_code,account_code,activity_code,department_code,q1,q2,q3,q4 from budget_costcenter_dump where financial_year = '$financialYear' and cost_center = '$costCenter'";
        $details_result = $em->getConnection()->executeQuery($query)->fetchAll();

        $query = "select f015fid from t015ffinancialyear where f015fname = '$financialYear'";
        $fn_result = $em->getConnection()->executeQuery($query)->fetch();
        $sum_result[$i]['f015fid'] = $fn_result['f015fid'];
            $sum_result[$i]['activity-details'] = $details_result;
        }
        //   print_r($sum_result);  
        //   die();
            foreach($sum_result as $data){
                $activity = new T054factivityMaster();

                $activity->setF054fdepartment($data['cost_center'])
                               ->setF054fidFinancialyear((int)$data['f015fid'])
                               ->setF054ftotalAmount((float)$data['total_amount'])
                               ->setF054fbalance((float)$data['total_amount'])
                               ->setF054fapprovalStatus((int)"1")
                               ->setF054fstatus((int)"1")
                               ->setF054fcreatedBy((int)$_SESSION['userId'])
                                ->setF054fupdatedBy((int)$_SESSION['userId']);
        
                $activity->setF054fcreatedDtTm(new \DateTime())
                               ->setF054fupdatedDtTm(new \DateTime());
                
                $em->persist($activity);
                $em->flush();
                
                $activityDetails = $data['activity-details'];
        
                foreach ($activityDetails as $activityDetail) {
        
                    $activityDetailObj = new T054factivityDetails();
        
                    $activityDetailObj->setF054fidActivityMaster((int)$activity->getF054fid())
                               ->setF054ffundCode( $activityDetail['fund_code'])
                                ->setF054faccountCode( $activityDetail['account_code'])
                                ->setF054factivityCode( $activityDetail['activity_code'])
                                ->setF054fdepartmentCode( $activityDetail['department_code'])
                                ->setF054famount((float)$activityDetail['amount'])
                               ->setF054fq1((float)$activityDetail['q1'])
                               ->setF054fq2((float)$activityDetail['q2'])
                               ->setF054fq3((float)$activityDetail['q3'])
                               ->setF054fq4((float)$activityDetail['q4'])
                               ->setF054fstatus((int)$activityDetail['f054fstatus'])
                               ->setF054fcreatedBy((int)$_SESSION['userId'])
                                ->setF054fupdatedBy((int)$_SESSION['userId']);
        
                    $activityDetailObj->setF054fcreatedDtTm(new \DateTime())
                               ->setF054fupdatedDtTm(new \DateTime());
        
                
        
                $em->persist($activityDetailObj);
        
                $em->flush();
        
                    
                }
                
            }
         }    
        //  die();
    }
    public function computerPurchasedetails($staffId)
    {
        $query = "select f043fic_no, f020floan_name,f034fstaff_id, f034fname, f043ftotal_loan, f043fmodel, f043fcompany_name from t043fvehicle_loan inner join t020floan_name on f043fid_loan_name = f020fid inner join t034fstaff_master on f034fstaff_id = f043femp_name where f043femp_name like '$staffId'";
        // print_r($query);exit;
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }

    public function getJournalEntryById($journalId)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

                $qb->select("jd.f018fid,(jd.f018ffundCode+'-'+fu.f057fname) as Fund, (jd.f018faccountCode+'-'+ac.f059fname) as Account,(jd.f018fdepartmentCode+'-'+dep.f015fdepartmentName) Department, (jd.f018factivityCode+'-'+act.f058fname) as Activity, j.f017fdescription,
                    s.f034fname as originalName, s.f034fstaffId, jd.f018fdrAmount, jd.f018fcrAmount")
            ->from('Application\Entity\T017fjournal','j')
            ->join('Application\Entity\T018fjournalDetails','jd', Expr\Join::INNER_JOIN, 'j.f017fid = jd.f018fidJournal')
            ->join('Application\Entity\T014fuser', 'u', Expr\Join::INNER_JOIN,'j.f017fapprover = u.f014fid')
            ->join('Application\Entity\T034fstaffMaster', 's', Expr\Join::INNER_JOIN,'u.f014fidStaff = s.f034fstaffId')
            ->join('Application\Entity\T057ffund', 'fu', Expr\Join::INNER_JOIN,'fu.f057fcode = jd.f018ffundCode')
            ->join('Application\Entity\T059faccountCode', 'ac', Expr\Join::INNER_JOIN,'ac.f059fcompleteCode = jd.f018faccountCode')
            ->join('Application\Entity\T015fdepartment', 'dep', Expr\Join::INNER_JOIN,'dep.f015fdepartmentCode = jd.f018fdepartmentCode')
            ->join('Application\Entity\T058factivityCode', 'act', Expr\Join::INNER_JOIN,'act.f058fcompleteCode = jd.f018factivityCode')
           ->where('j.f017fid = :journalId')
            ->setParameter('journalId',$journalId);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }
    //AP Report1
    public function getReportByPeceipt($receiptId)
    {
        $query = "select rec.f082freceipt_number, rec.f082fdate, rec.f082ftotal_amount, rec.f082fdescription, rec.f082fcreated_dt_tm, rec.f082fid_customer, rec.f082fid_invoice, rec.f082fpayee_type, (cus.f021ffirst_name+'-'+cus.f021flast_name) as CustomerName from t082freceipt rec inner join t021fcustomers cus on cus.f021fid = rec.f082fid_customer where f082fid = $receiptId";
        // print_r($query);exit;
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        return $result;

    }
    //AP Report2
    public function getPaymentInfo($Id)
    {
        $query = "select v.f093fvoucher_no, v.f093fpayment_type, v.f093fdescription, v.f093fpayment_ref_no, v.f093fid_bank, v.f093fid_company_bank, v.f093fpayment_ref_no, bs.f041faccount_number, b.f042fbank_name from t093fpayment_voucher v inner join t041fbank_setup_account bs on bs.f041fid_bank = v.f093fid_company_bank inner join t042fbank b on b.f042fid = bs.f041fid_bank where  v.f093fid = $Id ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        // print_r($result);exit;
       
        $query1 = "select sum(f094ftotal_amount) as total_amount from t094fpayment_voucher_details where f094fid_payment = $Id";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query1);
        $resul = $select->fetch();
        $result['total_amount'] = $resul['total_amount'];
        
        return $result;
    }
    //AP Report3
    public function getVocharReport3($Id)
    {
        $query = "select vd.f094fvoucher_date, vd.f094ftotal_amount, vd.f094fcustomer_name, vd.f094fid_payment, vd.f094fcreated_dt_tm, v.f093fpayment_type, v.f093fvoucher_no, v.f093fpayment_mode, v.f093fpayment_ref_no, dep.f015fdepartment_name, dms.f011fdefinition_code from t094fpayment_voucher_details vd inner join t093fpayment_voucher v on v.f093fid = vd.f094fid inner join t041fbank_setup_account bs on bs.f041fid = v.f093fid_company_bank inner join t015fdepartment dep on bs.f041fdepartment_code = dep.f015fdepartment_code inner join t011fdefinationms dms on dms.f011fid = v.f093fpayment_type  where vd.f094fid_payment = $Id ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();


        $query1 = "select sum(f094ftotal_amount) as total_amount from t094fpayment_voucher_details where f094fid_payment = $Id";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query1);
        $resul = $select->fetch();

        $result['total_amount'] = $resul['total_amount'];

        
        return $result;

    }
    //Report 8 reportpaymentOver7daysDepartment
    public function getPaymentOverDetails($department)
    {
        $department = substr($department,0,3);
        // print_r($department);exit;


        $query = "select invd.f072fid_item, invd.f072fgst_value, invd.f072fquantity, invd.f072fprice, invd.f072ftotal, invd.f072fgst_code, invd.f072fid_tax_code, invd.f072ftotal_exc, invd.f072ftax_amount, invd.f072fdebit_fund_code, invd.f072fdebit_department_code, invd.f072fdebit_activity_code, invd.f072fdebit_account_code, invd.f072fcredit_fund_code, invd.f072fcredit_department_code, invd.f072fcredit_activity_code, invd.f072fcredit_account_code, invd.f072fis_refundable, fi.f066fname from t072finvoice_details invd inner join t066ffee_item fi on fi.f066fid = invd.f072fid_item where SUBSTRING(invd.f072fdebit_department_code,1,3) = '$department' ";
        // print_r($query);exit;



        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;

    }
    public function getGrnData($fromDate, $toDate)
    {
         $query = "select  grn.f034forder_type, grn.f034forder_date, grn.f034fdescription, grn.f034freference_number, grn.f034fid_purchase_order, grn.f034ftotal_amount, grn.f034fapproved_ref_num, grn.f034fapproved_date, grn.f034fdonor, vr.f030fcompany_name, fy.f015fname, dep.f015fdepartment_name from t034fgrn grn inner join t015fdepartment dep on grn.f034fid_department = dep.f015fdepartment_code inner join t015ffinancialyear fy on grn.f034fid_financialyear = fy.f015fid inner join t030fvendor_registration vr on grn.f034fid_supplier = vr.f030fid where f034forder_date between '$fromDate' and '$toDate' ";

        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;

    }

    public function getPOData($data)
    {
        //  $query = "select po.f034forder_type, po.f034forder_date, po.f034fdescription, po.f034freference_number, po.f034fid_purchasereq,po.f034ftotal_amount, vr.f030fcompany_name, fy.f015fname, dep.f015fdepartment_name from t034fpurchase_order po inner join t015fdepartment dep on po.f034fid_department = dep.f015fdepartment_code inner join t015ffinancialyear fy on po.f034fid_financialyear = fy.f015fid inner join t030fvendor_registration vr on po.f034fid_supplier = vr.f030fid where f034forder_date between '$fromDate' and '$toDate' ";
        
        // $em = $this->getEntityManager();
        // $select = $em->getConnection()->executeQuery($query);
        // $result = $select->fetchAll();


        $id = (int)$data['id'];
        $print = (int)$data['type'];

        // $printDate = date('Y-m-d'); 

        // print_r($print);exit;



        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        // Query

        $qb->select("p.f034fid, p.f034forderType, p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier ,v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, d.f015fdepartmentName as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fapprovalStatus, p.f034ftotalAmount, p.f034fupdatedBy, u.f014fuserName as userName, p.f034fcreatedDtTm, p.f034fprintCount, p.f034fprintDate, p.f034frePrintDate, p.f034fapprovalStatus, (v.f030faddress1+' '+v.f030faddress2+' '+v.f030faddress3+' '+v.f030faddress4) as vendorAddress, st.f012fstateName, cnt.f013fcountryName, v.f030fpostCode ")
            ->from('Application\Entity\T034fpurchaseOrder', 'p')
            ->join('Application\Entity\T015fdepartment', 'd', Expr\Join::INNER_JOIN, 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->join('Application\Entity\T015ffinancialyear', 'f', Expr\Join::INNER_JOIN, 'p.f034fidFinancialyear = f.f015fid')
            ->join('Application\Entity\T030fvendorRegistration', 'v', Expr\Join::INNER_JOIN, 'p.f034fidSupplier = v.f030fid')
            ->join('Application\Entity\T012fstate', 'st', Expr\Join::INNER_JOIN, 'v.f030fstate = st.f012fid')
            ->join('Application\Entity\T013fcountry', 'cnt', Expr\Join::INNER_JOIN, 'v.f030fcountry = cnt.f013fid')
            ->join('Application\Entity\T014fuser', 'u', Expr\Join::INNER_JOIN, 'p.f034fupdatedBy = u.f014fid')
            ->where('p.f034fapprovalStatus=1')
            ->andWhere('p.f034fid = :poId')
            ->setParameter('poId',(int)$id);
            // echo $qb;
            // exit;

        $query = $qb->getQuery();
    $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

    // print_r($result);exit;
    $result1 = array();
        foreach ($result as $item) {
            // print_r($item);exit;
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }
        // print_r($result1);exit;
        $approver = (int)$_SESSION['userId'];



        if ($print == 1)
        {
            $query = "UPDATE t034fpurchase_order set f034fprint_date = getdate(), f034fprint_count = 1, f034fprinted_by = $approver  where f034fid = $id"; 
            
            $value=$em->getConnection()->executeQuery($query);
        }
        elseif ($print ==0)
        {



            $queryGet = "SELECT f034fprint_count from t034fpurchase_order where f034fid = $id"; 
            $valueGet =$em->getConnection()->executeQuery($queryGet)->fetch();

            $count = (int)$valueGet['f034fprint_count'] + 1;

            
            $query = "UPDATE t034fpurchase_order set f034fre_print_date = getdate(), f034fre_printed_by = $approver, f034fprint_count = $count  where f034fid = $id"; 
            $value=$em->getConnection()->executeQuery($query);
        }

        return $result1;
    }

    public function getPurchaseOrderDetails($id)
    {
        $id = (int)$id;
        $query = "select po.f034fquantity, po.f034funit, po.f034fprice, po.f034total, po.f034ftax_code, po.f034fpercentage, po.f034ftax_amount, po.f034ftotal_inc_tax, i.f029fitem_name, i.f029faccount_code from t034fpurchase_order_details po inner join t029fitem_set_up i on i.f029fid = po.f034fid_item where f034fid_purchase_order = $id";
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        return $result;
    }
    public function getPODataById($id)
    {
        $query = "select po.f034forder_type, po.f034forder_date, po.f034fdescription, po.f034freference_number, po.f034fid_purchasereq,po.f034ftotal_amount, vr.f030fcompany_name, fy.f015fname, dep.f015fdepartment_name, pod.f034fquantity, pod.f034funit, pod.f034fprice, pod.f034total from t034fpurchase_order po inner join t015fdepartment dep on po.f034fid_department = dep.f015fdepartment_code inner join t015ffinancialyear fy on po.f034fid_financialyear = fy.f015fid inner join t030fvendor_registration vr on po.f034fid_supplier = vr.f030fid inner join t034fpurchase_order_details pod on po.f034fid = pod.f034fid_purchase_order where po.f034fid = $id ";
        // print_r($query);exit;
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;
    }
    public function getPurchaseRequistionData($fromDate, $toDate)
    {
        $query = "select pre.f088freference_number, pre.f088forder_type, pre.f088fdate, vr.f030fcompany_name, fy.f015fname, pre.f088fdescription, pre.f088ftotal_amount, dep.f015fdepartment_name from t088fpurchase_requisition pre inner join t015fdepartment dep on pre.f088fid_department = dep.f015fdepartment_code inner join t015ffinancialyear fy on pre.f088fid_financialyear = fy.f015fid inner join t030fvendor_registration vr on pre.f088fid_supplier = vr.f030fid where f088fdate between '$fromDate' and '$toDate' ";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetchAll();
        
        return $result;
    }
    //Vehicle Loan
    public function getVehicleLoanDetails($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('v.f043fid, v.f043fidLoanName, v.f043fidVendor, v.f043freferenceNumber, v.f043ffinalStatus, v.f043fvehicleCondition, v.f043fmanufacturer, v.f043fmodel, v.f043fcc, v.f043fcompanyName, v.f043finsuranceComp, v.f043floanEntitlement, v.f043fpurchasePrice, v.f043floanAmount, v.f043finstallmentPeriod, v.f043fdrivingLicense, v.f043fchasisNo, v.f043fengineNo, v.f043fregistrationNo, v.f043fjkrAmount, v.f043ficNo, v.f043fjkrCertificateNo, v.f043fjkrPeriod, v.f043fapplyDate, v.f043fstartDate, v.f043fendDate, v.f043flastDate, v.f043fempName, v.f043ffirstGuarantor, v.f043fsecondGuarantor, v.f043insuranceAmt, v.f043ftotalLoan, v.f043finterestRate, v.f043fmonthInterestRate, v.f043fprofitRatio, v.f043fmonthlyInstallment, v.f043fprincipal, v.f043fprofitAmount, v.f043floanYear, v.f043finsuranceRate')
            ->from('Application\Entity\T043fvehicleLoan', 'v')
        //    ->join('Application\Entity\T011fdefinationms', 'd', Expr\Join::INNER_JOIN,'v.f043fvehicleCondition = d.f011fid')
          //  ->join('Application\Entity\T021fcustomers', 'c', Expr\Join::INNER_JOIN,'v.f043finsuranceComp = c.f021fid')
          //  ->join('Application\Entity\T034fstaffMaster', 's', Expr\Join::INNER_JOIN,'v.f043fempName = s.f034fid')
           // ->join('Application\Entity\T034fstaffMaster', 'fg', Expr\Join::INNER_JOIN,'v.f043ffirstGuarantor = fg.f034fid')
           // ->join('Application\Entity\T030fvendorRegistration', 've', Expr\Join::INNER_JOIN,'ve.f030fid = v.f043fidVendor')
            ->where('v.f043fid = :vehicleLoanId')
            ->setParameter('vehicleLoanId',$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // print_r($result);exit;
        $result = $result[0];
         $qb = $em->createQueryBuilder();
      $qb->select("f.f044fid,f.f044fidStaff as guaranteeId,f.f044fidLoan,s.f034fname,l.f043fidLoanName,l.f043freferenceNumber,f.f044fapprovedDate,f.f044fstatus")
            ->from('Application\Entity\T044floanGurantors','f')
            ->join('Application\Entity\T034fstaffMaster', 's', Expr\Join::INNER_JOIN,'f.f044fidStaff = s.f034fstaffId')
            ->join('Application\Entity\T043fvehicleLoan', 'l', Expr\Join::INNER_JOIN,'f.f044fidLoan = l.f043fid')
            ->where('f.f044fidLoan = :Id')
            ->setParameter('Id',$id);
               $query = $qb->getQuery();
        $gurantorList = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

            $result['guarantor-list'] = $gurantorList;

        return $result;
    }
    public function getTenderData($id)
    {
        $query = "select f035fquotation_number, f035ftitle, f035ftotal_amount from t035ftender_quotation  where f035fid = $id ";
        
        $em = $this->getEntityManager();
        $select = $em->getConnection()->executeQuery($query);
        $result = $select->fetch();
        
        return $result;
    }
     public function updateTable($table){
        // echo "hi";
        // die();
        $em = $this->getEntityManager();
        try{
        // echo $table;
        if($table == "t031flicense_information"){
        $query = "select f031fid,vendor,license from t031flicense_information";
        $license_result = $em->getConnection()->executeQuery($query)->fetchAll();
         // print_r($license_result);
         //    die();
        foreach ($license_result as $license) {
            // print_r($license);
            // die();
            $f031fid =  $license['f031fid'];
            $vendor_code =  $license['vendor'];
            $license_code =  $license['license'];
        $query = "select f030fid from t030fvendor_registration where f030fvendor_code = '$vendor_code'";
        $vendor_result = $em->getConnection()->executeQuery($query)->fetch();
        if($vendor_result){
            $vendor_id = $vendor_result['f030fid'];
             echo $query = "update t031flicense_information set f031fid_supplier_reg = $vendor_id where f031fid = $f031fid ";
             $em->getConnection()->executeQuery($query);
        }
        $query = "select f091fid from t091flicense where f091fcode = '$license_code'";
        $license_result1 = $em->getConnection()->executeQuery($query)->fetch();
        if($license_result1){
            $license_id = $license_result1['f091fid'];
             echo $query = "update t031flicense_information set f031fid_license = $license_id where f031fid = $f031fid ";
             $em->getConnection()->executeQuery($query);
        }
        }

        }
        if($table == "t030fvendor_registration"){
             $query = "select f030fid,bank_code from t030fvendor_registration";
             $vendor_result = $em->getConnection()->executeQuery($query)->fetchAll();
             foreach ($vendor_result as $vendor) {
            
            $f030fid =  $vendor['f030fid'];
            $bank_code =  $bank['bank_code'];
            $query = "select f042fbrs_code,f042fid from t042fbank where f042fbrs_code = '$bank_code'";
            $bank_result = $em->getConnection()->executeQuery($query)->fetch();
            if($bank_result){
            $bank_id = $bank_result['f042fid'];
             echo $query = "update t030fvendor_registration set t030fvendor_registration = $bank_id where f030fid = $f030fid ";
             $em->getConnection()->executeQuery($query);
        }
        }
        }
    }
    catch(\Exception $e){
        echo $e;
    }
       die();
        return $invoice_result;   
    }



    public function invoiceEntryReport($id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('i.f071fid,i.f071finvoiceNumber,i.f071finvoiceType,s.f060fname as studentName,i.f071fbillType,i.f071fdescription,i.f071fidCustomer,i.f071fidFinancialYear,i.f071finvoiceDate,i.f071fapprovedBy,i.f071fstatus,i.f071finvoiceTotal,i.f071fbalance,i.f071fisRcp,i.f071finvoiceFrom, i.f071freason')
            ->from('Application\Entity\T071finvoice', 'i')
                    // ->leftjoin('Application\Entity\T021fcustomers', 'c', 'with', 'i.f071fidCustomer = c.f021fid')
            ->leftjoin('Application\Entity\T060fstudent', 's', 'with', 'i.f071fidCustomer = s.f060fid')
            ->where('i.f071fstatus = 1')
            ->AndWhere('i.f071fid = :isRcp')
            ->orderby('i.f071fid','DESC')
            ->setParameter('isRcp', (int)$id);

        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);

        return $result;
    }

    public function warrantPrint($id)
    {
         $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

       $qb->select("p.f034fid, p.f034forderType,p.f034forderDate, p.f034fidFinancialyear, f.f015fname as financialyear, p.f034fidSupplier, v.f030fcompanyName, v.f030fvendorCode ,(v.f030fvendorCode+ '-' +v.f030fcompanyName) as supplier, p.f034fdescription, p.f034fidDepartment, (p.f034fidDepartment+'-'+d.f015fdepartmentName) as department, p.f034freferenceNumber, p. f034fidPurchasereq, p.f034fexpiredDate, p.f034fstatus, p.f034ftotalAmount, pd.f034fidDetails, pd.f034fidItem, pd.f034fsoCode, pd.f034frequiredDate, pd.f034fquantity, pd.f034funit, pd.f034fprice, pd.f034total, pd.f034ftaxCode, pd.f034fpercentage, pd.f034ftaxAmount, pd.f034ftotalIncTax, pd.f034ffundCode, pd.f034fdepartmentCode, pd.f034factivityCode, pd.f034faccountCode, pd.f034fbudgetFundCode, pd.f034fbudgetDepartmentCode, pd.f034fbudgetActivityCode, pd.f034fbudgetAccountCode, p.f034fapprovalStatus, p.f034fupdatedBy, u.f014fuserName as userName, p.f034fcreatedDtTm, pd.f034fitemType, pd.f034fitemCategory, pd.f034fitemSubCategory, pd.f034fassetCode, pd.f034fbalanceQuantity")
            ->from('Application\Entity\T034fpurchaseOrderDetails', 'pd')
            ->join('Application\Entity\T034fpurchaseOrder', 'p', Expr\Join::INNER_JOIN, 'p.f034fid = pd.f034fidPurchaseOrder')
            ->join('Application\Entity\T015fdepartment', 'd','with', 'p.f034fidDepartment = d.f015fdepartmentCode')
            ->join('Application\Entity\T015ffinancialyear', 'f', 'with', 'p.f034fidFinancialyear = f.f015fid')
            ->join('Application\Entity\T088fpurchaseRequisition', 'pr','with', 'p.f034fidPurchasereq = pr.f088fid')
            ->leftjoin('Application\Entity\T030fvendorRegistration', 'v','with', 'p.f034fidSupplier = v.f030fid')
            ->join('Application\Entity\T014fuser', 'u','with', 'p.f034fupdatedBy = u.f014fid')
            ->where('pd.f034fidDetails = :purchaseId')
            ->andWhere("p.f034forderType = 'Warrant'")
            ->setParameter('purchaseId',$id);
            
        $query = $qb->getQuery();
        $result = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
        // print_r($result);
        // die();
    $result1 = array();
        foreach ($result as $item) {
            $item['f034fidItem'] = $item['f034fidItem'].$item['f034fitemType'];
            $date                        = $item['f034forderDate'];
            $item['f034forderDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fexpiredDate'];
            $item['f034fexpiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034frequiredDate'];
            $item['f034frequiredDate'] = date("Y-m-d", strtotime($date));
            $date                        = $item['f034fcreatedDtTm'];
            $item['f034fcreatedDtTm'] = date("Y-m-d", strtotime($date));
            array_push($result1, $item);
        }

         $i=0;
        foreach ($result1 as $purchase)
        {
        // print_r($purchase);exit;

        $categoryType = $purchase['f034fitemType'];
        $category = (int)$purchase['f034fitemCategory'];
        $subCategory = (int)$purchase['f034fitemSubCategory'];
        $item = (int)$purchase['f034fidItem'];



                 $query = "select (f027fcode+'-'+f027fcategory_name) as itemCategory from t027fitem_category where f027fid = $category"; 
                $categoryResult=$em->getConnection()->executeQuery($query)->fetch();

                 $query = "select (f028fcode+'-'+f028fsub_category_name) as iemSubCategory from t028fitem_sub_category where f028fid = $subCategory"; 
                $subCategoryesult=$em->getConnection()->executeQuery($query)->fetch();

                $result1[$i]['categoryName'] = $categoryResult['itemCategory'];
                $result1[$i]['subCategoryName'] = $subCategoryesult['iemSubCategory'];

            if($categoryType == 'A')
            {
                // $em = $this->getEntityManager();

                

                 $query = "select (f088fitem_code+'-'+f088fitem_description) as assetItem from t088fitem where f088fid = $id"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
        // print_r($categoryResult);exit;
       
        $result1[$i]['itemName'] = $itemResult['assetItem'];


           
            }
            elseif($categoryType == 'P')
            {
                // $em = $this->getEntityManager();


                 $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $id"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();


        
        $result1[$i]['itemName'] = $itemResult['procurementItem'];
           
            }
            $i++;
        }

          $reference = (int)date('dmYhms', time());

           $query = "UPDATE t034fpurchase_order_details set t034fprint_running_no = $reference where f034fid_details = $id"; 
                $itemResult=$em->getConnection()->executeQuery($query);              

                $result1['reference'] = $reference;

        return $result1;
    }

    public function letterForInvestmentToUUMBank()
    {
        $em = $this->getEntityManager();
        $query = "select (f029fitem_code+'-'+f029fitem_name) as procurementItem from t029fitem_set_up where f029fid = $id"; 
                $itemResult=$em->getConnection()->executeQuery($query)->fetch();
    }
}