<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Report;

use Zend\Mvc\Console\Router\RouteMatch as ConsoleRouteMatch;
use Zend\Console\Request as ConsoleRequest;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Router\RouteMatch;
use Zend\Stdlib\ArrayUtils;
use Zend\View\Model\ViewModel;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;

class Module
{
    const VERSION = '3.0.2dev';

    protected $logger;
    
    protected $authService;
    
    protected $config;
    
    protected $sm;
    
    public function onBootstrap(MvcEvent $e)
    {
        $application = $e->getApplication();
        $eventManager = $application->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $serviceManager = $application->getServiceManager();
        $this->sm = $serviceManager;
        // Config
        $this->config = $serviceManager->get('Config');
        // $e->getViewModel()->setVariable('template_config', $this->config['template_config']);
        
        // Authentication Service
        // $this->authService = $serviceManager->get('app.authentication');
        
        // Logger service
        $this->logger = $serviceManager->get('app.logger');
        
        // Initialize ACL
        // $this->initAcl($e);
        
        $eventManager->attach(MvcEvent::EVENT_ROUTE, [$this, 'onRoute']);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatchError']);
        $eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, [$this, 'onRenderError']);

        // Initialize event listeners
        // $this->initEventListeners($serviceManager, $eventManager);
        
        // Override php.ini settings
        // $this->overrideIniSettings();
        
        // Load console commands
        // $this->registerConsoleCommands($serviceManager);
    }

    public function getConfig()
    {
        $config = require __DIR__ . '/../config/module.config.php';
        
        $config = ArrayUtils::merge($config, require __DIR__ . '/../config/services.config.php');
        
        return $config;
    }
    
    /**
     * MvcEvent "route" event handler
     *
     * @param MvcEvent $e
     */
    public function onRoute(MvcEvent $e)
    {
        $match = $e->getRouteMatch();
        $route = $match->getMatchedRouteName();
        
        if ($match instanceof ConsoleRouteMatch || $e->getRequest() instanceof ConsoleRequest) {
            return;
        }
        
        if (! $match instanceof RouteMatch) {
            return;
        }

        if($e->getRequest()->getHeaders()->has('X-Auth-Client')){
         //Validate the tokens
            $client = $e->getRequest()->getHeaders('X-Auth-Client')->getFieldValue();
         if($this->isValidRequest($client) == false){
            $response = array(
                'status' => 401,
                'message' => 'Un-authorization request.'
            );
            header('content-type: application/json');
            echo json_encode($response);
            exit();
         }
        }else{
            $response = array(
                'status' => 401,
                'message' => 'Un-authorization request.'
            );
            header('content-type: application/json');
            echo json_encode($response);
            exit();
        }
         $token = $e->getRequest()->getHeaders('X-Auth-Token')->getFieldValue();
            $_SESSION['f014ftoken'] = $token;
    }   
    

    // 

    // /**
    //  * MvcEvent "dispatch.error" event handler
    //  *
    //  * @param MvcEvent $e
    //  */
     public function onDispatchError(MvcEvent $e)
    {
        $exception = $e->getResult()->exception;
        // echo $exception;
            $this->logger->error($exception);
            $response = array(
                'status' => 500,
                'message' => 'Server Error,Check Log',
            );
            header('content-type: application/json');
            echo json_encode($response);
            exit();
    }
    
    /**
     * MvcEvent "render.error" event handler
     *
     * @param MvcEvent $e
     */
    public function onRenderError(MvcEvent $e)
    {

        $exception = $e->getResult()->exception;

        if ($exception) {
            $this->logger->error($exception);
        }
    }
    

    public function isValidRequest($headerKey)
    {
       $clientKey =   $this->config['client_key'];
      // echo $clientKey.'***'.$headerKey;exit;
        if($headerKey != $clientKey)
        {
            return false;
        }
       return true;
    }
}
